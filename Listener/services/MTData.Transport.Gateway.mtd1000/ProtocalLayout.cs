﻿using System;
using System.IO;

namespace MTData.Transport.Gateway.Message
{
    public class ProtocalLayout
    {
        internal string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
        internal byte _cMsgType;
        internal bool _bAckImmediately;    // whether this packet needs an immediate ACK
        internal bool _bAckRegardless;     // if this is set, ignore any prior missing sequence numbers
        internal bool _bSendFlash;         // Request / Allow / Disallow Flash unloading
        internal bool _bArchivalData;      // Data is not recent - don't update viewing clients
        internal bool _bUsingSecondaryServer = false;  // Data is being sent TO the device's second-choice server
        internal uint _iLength; // the length of mDataField
        internal byte _cFleetId;
        internal uint _iVehicleId;
        internal byte _cPacketNum;
        internal byte _cPacketTotal;
        internal byte _cOurSequence;
        internal byte _cAckSequence;
        internal byte[] _mDataField;
        internal uint _iChecksum;
        internal byte[] _mRawBytes;
        internal uint _iTotalLength;

        internal bool _bEngineData = false;
        internal bool _bExtraDetails = false;
        internal bool _bTrailerTrack = false;
        internal bool _bExtendedVariableDetails = false;
        internal bool _bECMAdvanced = false;
        internal bool _hasRoadTypeData = false;
        internal bool _AdditionalAdvancedECM = false;

        internal int[] _mExtendedValues = new int[] { 0, 0, 0, 0 };

        internal bool _receivedFlashAvailable = false;

        internal MemoryStream _message;
        internal BinaryReader _messageReader;
        internal uint _messageLength;

        internal bool _ValidPacket = true;
        internal string _ErrorMessage;
        internal GConstants.PACKET_ERROR _ErrorType;

        internal BaseDataFieldsLayout _ExpandedDataFields;
        internal int _mDataFieldLength;

        public BaseDataFieldsLayout ExpandedDataFields { get { return _ExpandedDataFields; } set { _ExpandedDataFields = value; } }

        public int mDataFieldLength { get { return _mDataFieldLength; } set { _mDataFieldLength = value; } }
        
        internal ProtocalLayout()
        {
        }

        internal ProtocalLayout(byte[] message)
        {
            _mRawBytes = message;
            _message = new MemoryStream(_mRawBytes);
            _messageReader = new BinaryReader(_message);
            _messageLength = Convert.ToUInt32(_message.Length);
        }

        internal bool PacketError(GConstants.PACKET_ERROR packet_error)
        {
            _ValidPacket = false;
            _ErrorType = packet_error;
            _ErrorMessage = packet_error.ToString();

            return _ValidPacket;
        }

        public byte cMsgType { get { return _cMsgType; } set { _cMsgType = value; } }

        public bool bAckImmediately { get { return _bAckImmediately; } set { _bAckImmediately = value; } }

        public bool bAckRegardless { get { return _bAckRegardless; } set { _bAckRegardless = value; } }

        public bool bSendFlash { get { return _bSendFlash; } set { _bSendFlash = value; } }

        public bool bArchivalData { get { return _bArchivalData; } set { _bArchivalData = value; } }

        public bool bUsingSecondaryServer { get { return _bUsingSecondaryServer; } set { _bUsingSecondaryServer = value; } }

        public uint iLength { get { return _iLength; } set { _iLength = value; } }

        public byte cFleetId { get { return _cFleetId; } set { _cFleetId = value; } }

        public uint iVehicleId { get { return _iVehicleId; } set { _iVehicleId = value; } }

        public byte cPacketNum { get { return _cPacketNum; } set { _cPacketNum = value; } }

        public byte cPacketTotal { get { return _cPacketTotal; } set { _cPacketTotal = value; } }

        public byte cOurSequence { get { return _cOurSequence; } set { _cOurSequence = value; } }

        public byte cAckSequence { get { return _cAckSequence; } set { _cAckSequence = value; } }

        public byte[] mDataField { get { return _mDataField; } set { _mDataField = value; } }

        public uint iChecksum { get { return _iChecksum; } set { _iChecksum = value; } }

        public bool bIsPopulated { get { return (_ErrorType == GConstants.PACKET_ERROR.None); } }

        public byte[] mRawBytes { get { return _mRawBytes; } set { _mRawBytes = value; } }

        public uint iTotalLength { get { return _iTotalLength; } set { _iTotalLength = value; }}

        public bool receivedFlashAvailable { get { return _receivedFlashAvailable; } set { _receivedFlashAvailable = value; } }

        public uint messageLength { get { return _messageLength; } set { _messageLength = value; } }

        public bool ValidPacket { get { return _ValidPacket; } set { _ValidPacket = value; } }

        public bool bEngineData { get { return _bEngineData; } set { _bEngineData = value; } }
        public bool bExtraDetails { get { return _bExtraDetails; } set { _bExtraDetails = value; } }
        public bool bTrailerTrack { get { return _bTrailerTrack; } set { _bTrailerTrack = value; } }
        public bool bExtendedVariableDetails { get { return _bExtendedVariableDetails; } set { _bExtendedVariableDetails = value; } }
        public bool bECMAdvanced { get { return _bECMAdvanced; } set { _bECMAdvanced = value; } }
        public bool hasRoadTypeData { get { return _hasRoadTypeData; } set { _hasRoadTypeData = value; } }
        public bool AdditionalAdvancedECM { get { return _AdditionalAdvancedECM; } set { _AdditionalAdvancedECM = value; } }

        public int[] mExtendedValues { get { return _mExtendedValues; } set { _mExtendedValues = value; } }

        public string ErrorMessage { get { return _ErrorMessage; } set { _ErrorMessage = value; } }
        
        public GConstants.PACKET_ERROR ErrorType { get { return _ErrorType; } set { _ErrorType = value; } }

        public string MsgTypeBinary { get { return Convert.ToString(cMsgType, 2).PadLeft(8, '0'); } }

        public string MsgTypeDescription { get { return GConstants.GSP_MESSAGES[cMsgType].GSP_Message_Description; } }

        internal Type MsgTypeClass { get { return GConstants.GSP_MESSAGES[cMsgType].GSP_Message_Class; } }
    }
}
﻿using MTData.Transport.Gateway.Message.DataFieldMessageLayouts;
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Message
{
    public class GConstants
    {
        //Message Types and constants

        public const string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";

        // GSP = Gateway System Protocol
        public const byte GSP_APP_PING = 0x77;
        public const byte GSP_SOH = 0x01; // Start Of Header
        public const byte GSP_SM = 0x02;
        public const byte GSP_EM = 0x03;
        public const byte GSP_EOT = 0x04;
        public const byte GSP_EOT2 = 0x04;
        public const byte GSP_PROTOCOL_VER = 0x01; // Start Of Header

        // Message types with trivial payloads:
        public const byte SYSTEM_PACKET_MASK = 0xF0;
        public const byte SYSTEM_SETUP = 0xF1;
        public const byte SYSTEM_SETPOINT = 0xF2;
        public const byte SYSTEM_ROUTEPOINT = 0xF3;
        public const byte SYSTEM_CONFIG = 0xF4;
        public const byte SYSTEM_GPS_FAILURE = 0xF5;
        public const byte SYSTEM_GPRS_FAILURE = 0xF6;
        public const byte SYSTEM_SENDBUFFER = 0xF7;
        public const byte SYSTEM_RECEIVEBUFFER = 0xF8;
        public const byte SYSTEM_SMS_FAILURE = 0xF9;
        public const byte SYSTEM_HARDWARE_FAILURE = 0xFA;
        public const byte SYSTEM_UNIT_RESET = 0xFB;
        public const byte SYSTEM_RESET = 0xFE;

        // GSP imposes 12 bytes of overhead due to headers and trailers
        public const int GSP_HEADER_LENGTH = 10;
        public const int GSP_TRAILER_LENGTH = 2;
        public const int GSP_OVERHEAD = GSP_HEADER_LENGTH + GSP_TRAILER_LENGTH;

        // GPS Flags
        public const byte GPS_FLG_NTH = 0x01;
        public const byte GPS_FLG_EAST = 0x02;
        public const byte GPS_FLG_VALID = 0x80;

        public const byte PACKET_FLAG_MASK = 0xF0;
        public const byte PACKET_FLAG_ACKI = 0x80;
        public const byte PACKET_FLAG_ACK_REGARDLESS = 0x40;
        public const byte PACKET_FLAG_SEND_FLASH = 0x20;
        public const byte PACKET_FLAG_ARCHIVAL_DATA = 0x10;

        public enum PACKET_ERROR
        {
            None,
            Packet_Under_Size,
            No_SOH_byte,
            Packet_Size_Mismatch,
            No_EOT_Byte,
            Checksum_Error,
            Data_Field_Under_Size,
            Data_Field_Over_Size,
            Unknown_Message,
            Unconfirgured_Message
        }

        public class GSP_Message_Processor
        {
            public string GSP_Message_Description;
            public Type GSP_Message_Class;
        }

        public static readonly GSP_Message_Processor[] GSP_MESSAGES = new GSP_Message_Processor[]
        {
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DSS_START", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="MASS_DECLARATION", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_G_FORCES_SUSTAINED_START", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_G_FORCES_SUSTAINED_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALARM_LOW_BATTERY", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ECM_HYSTERESIS_ERROR", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GFORCE_CALIBRATION", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GPS_SETTINGS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DIAGNOSTIC_LEVEL", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DIAGNOSTIC", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="POSSIBLE_ACCIDENT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="TAMPER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_CONFIG_DOWNLOADED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_NO_GPS_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_NO_GPS_ANT_SHORT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_SUSPECT_GPS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_IGNITION_DISCONNECT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_ENGINE_DATA_MISSING", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_SUSPEC_ENGINE_DATA", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_G_FORCE_MISSING", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_G_FORCE_OUT_OF_CALIBRATION", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_CONCETE_SENSOR_ALERT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_MDT_NOT_LOGGED_IN", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_MDT_NOT_BEING_USED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_UNIT_POWERUP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_VEHICLE_GPS_SPEED_DIFF", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ALRM_ENGINE_DATA_RE_CONNECT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_IF_CONFIG_RESTART", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_ZERO_BYTES_RECEIVED_RESTART", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_VEHICLE_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_VEHICLE_USAGE_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_VEHICLE_BREAK_START", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_VEHICLE_BREAK_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_MDT_STATUS_REPORT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="PENDENT_ALARM_TEST", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="PENDENT_ALARM", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="PENDENT_ALARM_CLEARED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="PENDENT_ALARM_ACKD_BY_USER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="PENDENT_ALARM_NOT_ACKD_BY_USER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SHELL_REQUEST_REBOOT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SAT_PING", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="EXTENDED_IO_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="EXTENDED_IO_BELOW_THRESHOLD", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="EXTENDED_IO_ABOVE_THRESHOLD", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_LOGIN", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_LOGOUT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_ATLUNCH", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_TIPPER_UP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_TIPPER_DOWN", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_PRESSURE_SENSOR_LOADED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_PRESSURE_SENSOR_UNLOADED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_JOB_PACKET", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_PHONECALL_PACKET", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_GLOBAL_STAR", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_RPM_UP_TO_SPEED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_RPM_STOPPED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_RPM_REVERSED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_RPM_FORWARD", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_MIX_STARTED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_NOT_MIXED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_LEFT_LOADER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_ABOVE_ONSITE_SPEED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_LOADING_STARTED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_CONCRETE_AGE_MINS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_CONCRETE_AGE_ROTATIONS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_DRIVER_ERROR_1", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="BARREL_DRIVER_ERROR_2", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_OVER_RPM", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_OVER_COOLANT_TEMP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_OVER_OIL_TEMP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_UNDER_OIL_PRESSURE_LOW", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_UNDER_OIL_PRESSURE_HIGH", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_G_FORCES_HIGH", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_COOLANT_LEVEL_LOW", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_FUEL_ECONOMY_ALERT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_G_FORCE_CALIBRATED_RPT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ENGINE_ERROR_CODE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ZONE_ALERT_MESSAGES", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="OVERSPEED_ZONE_2_START", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="OVERSPEED_ZONE_3_START", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="OVERSPEED_ZONE_1_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="OVERSPEED_ZONE_2_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="OVERSPEED_ZONE_3_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ANGEL_GEAR_ALERT_START", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="ANGEL_GEAR_ALERT_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="FATIGUE_REPORT_IGNON", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="FATIGUE_REPORT_24", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_IM_ALIVE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_NOGPS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_OVERSPEED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_EXCESSIVE_IDLE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_IM_ALIVE_IMEI_ESN", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_EXCESSIVE_IDLE_END", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_ALARM", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_IOONOFF", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_OUTPUT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_NAK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_CLEAR_SENDBUF", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_SLEEPING", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_STARTUP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_ACK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_RESET", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_ARRIVE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_DEPART", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_DURATION_OVER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_DURATION_UNDER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_OUTSIDE_HRS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_SPECIAL_1_ARRIVE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="SETPT_SPECIAL_1_DEPART", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_ARRIVE_DOCK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_DEPART_DOCK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_OVER_SPEED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_ARRIVE_WORKSHOP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_DEPART_WORKSHOP", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_ARRIVE_NOGO", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_DEPART_NOGO", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="WAYPT_HOLDING_EXPIRY", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_PACKET_MASK", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_REPORT", GSP_Message_Class=typeof(STATUS_REPORT)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_RAW_GPS", GSP_Message_Class=typeof(STATUS_RAW_GPS)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_POS_REPORT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_IGNITION_ON", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_IGNITION_OFF", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_IO_PULSES", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_RX_TX", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_TRAILER_HITCH", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_TRAILER_DEHITCH", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_ACCIDENT_NEW", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_DATA_USAGE_ALERT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_RX_TX_SATTELITE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="STATUS_DATA_USAGE_ALERT_SATELLITE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_IM_ALIVE_IMIE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GPS_HISTORY", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_TAMPER", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="REFRIG_REPORT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DRIVER_POINTS_SERVER_TRACKING_RULE_BROKEN", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_ECM_OVERSPEED", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="LOW_POWER_MODE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DRIVER_PERFORMANCE_METRICS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DRIVER_DATAACCUMLATOR_METRICS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DRIVER_PERSONAL_KMS", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="DRIVER_OVERSPEED_REPORT", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_NEW_NETWORK_FOUND", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="GEN_GFORCEAUTOCALIBRATE", GSP_Message_Class=typeof(UNCONFIGURED_MESSAGE)},
            new GSP_Message_Processor { GSP_Message_Description="UNKNOWN_MESSAGE", GSP_Message_Class=typeof(UNKNOWN_MESSAGE)}
        };
    }
}

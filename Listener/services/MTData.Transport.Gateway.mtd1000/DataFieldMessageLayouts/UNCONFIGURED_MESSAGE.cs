﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Message.DataFieldMessageLayouts
{
    class UNCONFIGURED_MESSAGE : IBaseDataFieldsMessage
    {
        public int MinDataFieldLength { get; } = 0;
        public int MaxDataFieldLength { get; } = 0;

        public bool  DecodeDataFields(ProtocalLayout lvpassed)
        {
            return lvpassed.PacketError(GConstants.PACKET_ERROR.Unconfirgured_Message);
        }
    }
}

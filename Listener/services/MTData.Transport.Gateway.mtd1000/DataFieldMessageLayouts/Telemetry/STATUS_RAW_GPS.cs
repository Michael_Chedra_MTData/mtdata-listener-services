﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Message
{
    public class STATUS_RAW_GPS_DFL : BaseDataFieldsLayout
    {
        public string sRawGPS;
    }

    public class STATUS_RAW_GPS : IBaseDataFieldsMessage
    {
        public int MinDataFieldLength { get; } = 1;
        public int MaxDataFieldLength { get; } = 80;

        public ProtocalLayout lv;
        public STATUS_RAW_GPS_DFL dfl;

        public bool DecodeDataFields(ProtocalLayout lvpassed)
        {
            lv = lvpassed;
            lv.ExpandedDataFields = new STATUS_RAW_GPS_DFL();

            ((STATUS_RAW_GPS_DFL)lv.ExpandedDataFields).sRawGPS = Encoding.ASCII.GetString(lv.mDataField);

            int sRawGPSLength = ((STATUS_RAW_GPS_DFL)lv.ExpandedDataFields).sRawGPS.Length;

            if (sRawGPSLength < MinDataFieldLength) return lv.PacketError(GConstants.PACKET_ERROR.Data_Field_Under_Size);
            if (sRawGPSLength > MaxDataFieldLength) return lv.PacketError(GConstants.PACKET_ERROR.Data_Field_Over_Size);

            return true;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Message
{
    public class STATUS_REPORT_GPS_DFL : BaseDataFieldsLayout
    {
        public string sRawGPS;

        public GPClock mCurrentClock;
        public GPClock mFixClock;
        public GPPositionFix mFix;
        public GPStatus mStatus;
        public GPDistance mDistance;
    }

    public class STATUS_REPORT : IBaseDataFieldsMessage
    {
        public int MinDataFieldLength { get; } = 1;
        public int MaxDataFieldLength { get; } = 135;

        public ProtocalLayout lv;
        public STATUS_REPORT_GPS_DFL dfl;

        public bool DecodeDataFields(ProtocalLayout lvpassed)
        {
            lv = lvpassed;
            lv.ExpandedDataFields = new STATUS_REPORT_GPS_DFL();
            ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mCurrentClock = new GPClock("GPSClock", GConstants._serverTime_DateFormat);
            ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mFixClock = new GPClock("GPSClock", GConstants._serverTime_DateFormat);
            ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mFix = new GPPositionFix();
            ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mStatus = new GPStatus();
            ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mDistance = new GPDistance();

            ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).sRawGPS = Encoding.ASCII.GetString(lv.mDataField);

            string retMsg;
            int pos = 0;
            if ((retMsg = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mCurrentClock.Decode(lv.mDataField, pos)) != null)
			{
                // most likely this is an invalid clock (i.e. we haven't
                // acquired GPS as yet). We will skip decoding the rest
                // of the packet except for the Engine info which, as it
                // is not derived from GPS, may be of some value
                pos += GPClock.Length + GPClock.Length + GPPositionFix.Length;
                if ((retMsg = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mStatus.Decode(lv.mDataField, pos)) != null) return false; //Implement Error Handling
                pos += ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mStatus.Length;
                pos += GPDistance.Length;
            }
			else
			{
                pos += GPClock.Length;
                if ((retMsg = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mFixClock.Decode(lv.mDataField, pos)) != null)
                {
                    //return retMsg;
                }
                pos += GPClock.Length;
                if ((retMsg = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mFix.Decode(lv.mDataField, pos)) != null) return false; //Implement Error Handling
                pos += GPPositionFix.Length;
                if ((retMsg = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mStatus.Decode(lv.mDataField, pos)) != null) return false; //Implement Error Handling
                pos += ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mStatus.Length;
                if ((retMsg = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).mDistance.Decode(lv.mDataField, pos)) != null) return false; //Implement Error Handling
                pos += GPDistance.Length;
            }

            int sRawGPSLength = ((STATUS_REPORT_GPS_DFL)lv.ExpandedDataFields).sRawGPS.Length;

            if (sRawGPSLength < MinDataFieldLength) return lv.PacketError(GConstants.PACKET_ERROR.Data_Field_Under_Size);
            if (sRawGPSLength > MaxDataFieldLength) return lv.PacketError(GConstants.PACKET_ERROR.Data_Field_Over_Size);

            return true;
        }
    }
}

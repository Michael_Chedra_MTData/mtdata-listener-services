﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Message
{
    public class BaseDataFieldsLayout
    {
    }

    public interface IBaseDataFieldsMessage
    {
        int MinDataFieldLength { get; }
        int MaxDataFieldLength { get; }

        bool DecodeDataFields(ProtocalLayout lvpassed);
    }
}

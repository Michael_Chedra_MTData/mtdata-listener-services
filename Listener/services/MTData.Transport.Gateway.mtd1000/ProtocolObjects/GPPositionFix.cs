using System;
using System.Text;

namespace MTData.Transport.Gateway.Message
{
    /// <summary>
    /// Wraps up a GPS position fix in the Gateway Protocol.
    /// </summary>
    [Serializable]
    public class GPPositionFix
    {
        public const Byte FLAGS_NTH_HEM		= 0x01;	// unit is N of equator
        public const Byte FLAGS_EAST_SIDE	= 0x02; // unit is E of Greenwich
        public const Byte FLAGS_GPS_VALID	= 0x80; // This fix is valid
        #region Member Variables
        public decimal dLatitude;
        public decimal dLongitude;
        public byte cFlags;
        public byte cSpeed;
        public int iDirection;
        public bool bIsPopulated;
        #endregion
        public const int Length = 10;
        public const int INVALID_SPEED = 30000;

        public GPPositionFix()
        {
            dLatitude = 0;
            dLongitude = 0;
            cFlags = 0;
            cSpeed = 0;
            iDirection = 0;
            bIsPopulated = false;
        }
    
        public GPPositionFix CreateCopy()
        {
            GPPositionFix oPositionFix = new GPPositionFix();
            oPositionFix.dLatitude = this.dLatitude;
            oPositionFix.dLongitude = this.dLongitude;
            oPositionFix.cFlags = this.cFlags;
            oPositionFix.cSpeed = this.cSpeed;
            oPositionFix.iDirection = this.iDirection;
            oPositionFix.bIsPopulated = this.bIsPopulated;
            return oPositionFix;
        }

        public string ToDisplayString()
        {
            return	this.ToString();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nGPS Data :"); 
                builder.Append("\r\n    Latitude : "); builder.Append(dLatitude);
                builder.Append("\r\n    Longitude : "); builder.Append(dLongitude);
                builder.Append("\r\n    Flags : "); builder.Append((int) cFlags);
                builder.Append("\r\n    Speed : "); builder.Append((int) cSpeed);
                builder.Append("\r\n    Heading : "); builder.Append(iDirection);
            }
            catch(System.Exception ex)
            {
                builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
            }
            return builder.ToString();
        }


        #region Decoding
        public string Decode(Byte[] aData)
        {
            return Decode(aData, 0);
        }

        // Method:	- reverse bytes
        //			- convert to decimal
        //			- / 1000
        //			- /60
        //			- number before decimal pt is degrees
        //			- multiply number after decimal by 60
        //			- gives minutes.decminutes (approx)
        private void CalculateLatitude(byte[] aLat, int aStartPos)
        {
            int decimalVal =	aLat[aStartPos] + 
                                (aLat[aStartPos+1] << 8) +
                                (aLat[aStartPos+2] << 16);
            dLatitude = Decimal.Round(new Decimal(((double) decimalVal) / (double) 60000), 5);
            if ((cFlags & FLAGS_NTH_HEM) == 0)
            {	// South of equator - negate the latitude
                dLatitude = (0 - dLatitude);
            }
        }
        private void CalculateLongitude(byte[] aLon, int aStartPos)
        {
            int decimalVal =	aLon[aStartPos] + 
                (aLon[aStartPos+1] << 8) +
                (aLon[aStartPos+2] << 16);

            dLongitude = Decimal.Round(new Decimal(((double) decimalVal) / (double) 60000),5);
            if ((cFlags & FLAGS_EAST_SIDE) == 0)
            {	// West of Greenwich - negate the longitude
                dLongitude = (0 - dLongitude);
            }
        }

        public string Decode(byte[] aData, int aStartPos)
        {
            // Ensure we've got a decent array:
            if (aData.Length < (aStartPos + Length)) return "GPS fix field too short";

            // Have to get Flags first - they tell us which quadrant we're in
            cFlags  = aData[aStartPos + 7];
            
            CalculateLatitude(aData, aStartPos);
            aStartPos += 3;
            CalculateLongitude(aData, aStartPos);
            aStartPos += 3;
            
            cSpeed	= aData[aStartPos++];
            aStartPos++; // jump over Flags field

            iDirection = aData[aStartPos++];
            iDirection += (aData[aStartPos++] << 8);

            if (iDirection == 65278)
            {
                iDirection = INVALID_SPEED;
            }
        
            bIsPopulated = true;
            return null;
        }
        #endregion
        #region Encoding
        private void EncodeLatitude(ref byte[] payload, int aStartPos)
        {
            decimal dTempVal = dLatitude;
            if (dTempVal > 0)
            {
                cFlags |= FLAGS_NTH_HEM;	// hemisphere conversion
            }
            else dTempVal = (0 - dTempVal);

            int decimalVal = Convert.ToInt32(dTempVal * 60000);
            payload[aStartPos++] = Convert.ToByte(decimalVal & 0xFF);
            payload[aStartPos++] = Convert.ToByte((decimalVal >> 8) & 0xFF);
            payload[aStartPos++] = Convert.ToByte((decimalVal >> 16) & 0xFF);
        }

        private void EncodeLongitude(ref byte[] payload, int aStartPos)
        {
            decimal dTempVal = dLongitude;
            if (dTempVal > 0)
            {
                cFlags |= FLAGS_EAST_SIDE;	// E-W conversion
            }
            else dTempVal = (0 - dTempVal);

            int decimalVal = Convert.ToInt32(dTempVal * 60000);
            payload[aStartPos++] = Convert.ToByte(decimalVal & 0xFF);
            payload[aStartPos++] = Convert.ToByte((decimalVal >> 8) & 0xFF);
            payload[aStartPos++] = Convert.ToByte((decimalVal >> 16) & 0xFF);
        }

        public void Encode(ref byte[] theData, int aStartPos)
        {
            // Ensure we've got a decent array:
            if (theData.Length >= (aStartPos + Length))
            {
                EncodeLatitude(ref theData, aStartPos);
                aStartPos += 3;
                EncodeLongitude(ref theData, aStartPos);
                aStartPos += 3;
                theData[aStartPos++] = cSpeed;
                theData[aStartPos++] = cFlags;
                theData[aStartPos++] = Convert.ToByte(iDirection & 0xFF);	
                theData[aStartPos++] = Convert.ToByte((iDirection >> 8)& 0xFF);	
            }
        }
        #endregion
    }
}

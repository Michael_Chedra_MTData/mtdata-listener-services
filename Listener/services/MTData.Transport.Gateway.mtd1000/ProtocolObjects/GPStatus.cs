using System;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Message
{
    /// <summary>
    /// GPSStatus - details of the status flags in the Standard Status Report
    /// Packet Format...
    ///	[InputStatus]	-	1 byte
    ///	[StatusFlagsEx]	-	1 byte
    ///	[OutputStatus]	-	1 byte
    ///	[DebugFlags1]	-	1 byte
    ///	[DebugFlags2]	-	1 byte
    ///	[DebugFlags3]	-	1 byte
    /// </summary>

    public enum ExtendedStatusFlag2
    {
        LOW_BATTERY = 0x01,
        MOVING = 0x02,
        BATTERY_CHARGING = 0x04,
        CRITICAL_BATTERY = 0x08,
        FLAT_BATTERY = 0x10,
        PTO_ON = 0x20,
        SEAT_BELT_ON = 0x40,
        ABS_ON = 0x80,
        PASSENGER_SEAT_BELT_ON = 0x100,
        FOUR_WHEEL_DRIVE_ON = 0x200,
        DOORS_OPEN = 0x400,
        HAND_BRAKE_ON = 0x800,
        CRUISE_CONTROL_ON = 0x1000,
        WIPERS_ON = 0x2000,
        HEAD_LIGHTS_ON = 0x4000,
        HIGH_BEAM_ON = 0x8000,
        LEFT_INDICATOR_ON = 0x10000,
        LOW_FUEL_INDICATOR_ON = 0x20000,
        EBS_ON = 0x40000,
        RIGHT_INDICATOR_ON = 0x80000,
    }

    [Serializable]
    public class GPStatus
    {
        public byte cInputStatus = (byte) 0x00;
        public byte cStatusFlag = (byte) 0x00;
        public byte cOutputStatus = (byte) 0x00;
        public byte cExtendedStatusFlag = (byte) 0x00;
        public short cButtonStatus = 0;	//	This will contain StatusFlagsEx, DebugFlags1
        public short cLightStatus = 0;	//	This will contain DebugFlags1, DebugFlags2
        public byte cSpareStatus = (byte) 0x00;
        public bool bIsPopulated;
        public int ExtendedStatusFlag2 = 0;
        public int Length
        {
            get
            {
                if (IsExtendedStatusFlag2Used)
                {
                    return 10;
                }
                return 6;
            }
        }

        public GPStatus()
        {
            cInputStatus = 0;
            cOutputStatus = 0;
            cButtonStatus = 0;
            cLightStatus = 0;
            bIsPopulated = false;
        }

        public GPStatus CreateCopy()
        {
            GPStatus oStatus = new GPStatus();
            oStatus.cInputStatus = this.cInputStatus;
            oStatus.cStatusFlag = this.cStatusFlag;
            oStatus.cOutputStatus = this.cOutputStatus;
            oStatus.cButtonStatus = this.cButtonStatus;
            oStatus.cLightStatus = this.cLightStatus;
            oStatus.cSpareStatus = this.cSpareStatus;
            oStatus.bIsPopulated = this.bIsPopulated;
            oStatus.ExtendedStatusFlag2 = this.ExtendedStatusFlag2;
            return oStatus;
        }

        public string Decode(Byte[] aData)
        {
            return Decode(aData, 0);
        }

        public string Decode(Byte[] aData, int aStartPos)
        {
            byte[] temp = new byte[2];
            // Ensure we've got a decent array:
            if (aData.Length < (aStartPos + Length)) return "Status field too short";
            cButtonStatus = 0;
            cInputStatus = aData[aStartPos++];
            cStatusFlag= aData[aStartPos++];			
            cOutputStatus = aData[aStartPos++];
            cExtendedStatusFlag = aData[aStartPos++];
            temp[0] = aData[aStartPos++];
            temp[1] = (byte)0x00;
            cLightStatus = BitConverter.ToInt16(temp, 0);
            cSpareStatus = aData[aStartPos++];	// MSB of cLightStatus (DebugFlags2)
            if (IsExtendedStatusFlag2Used)
            {
                if (aData.Length < (aStartPos + 4)) return "Status field too short";

                ExtendedStatusFlag2 = PacketUtilities.Read4ByteNumberFromBuffer(aData, aStartPos);
                aStartPos += 4;
            }
            bIsPopulated = true;
            return null;
        }
        public void Encode(ref byte[] theData, int aStartPos)
        {
            // Ensure we've got a decent array:
            if (theData.Length >= (aStartPos + Length))
            {
                theData[aStartPos++] = cInputStatus;
                theData[aStartPos++] = cStatusFlag;
                theData[aStartPos++] = cOutputStatus;
                theData[aStartPos++] = cExtendedStatusFlag;
                //theData[aStartPos++] = (byte) cButtonStatus;
                theData[aStartPos++] = (byte) cLightStatus;
                theData[aStartPos++] = cSpareStatus;	
                if (IsExtendedStatusFlag2Used)
                {
                    PacketUtilities.Write4ByteNumberToBuffer(theData, ExtendedStatusFlag2, aStartPos);
                    aStartPos += 4;
                }
            }
        }

        public string ToDisplayString()
        {
            return	this.ToString();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nStatus Data :"); 
                builder.Append("\r\n    Status Flags : "); builder.Append((int)cStatusFlag);
                builder.Append("\r\n    Extended Status Flags : "); builder.Append((int) cExtendedStatusFlag);
                builder.Append("\r\n    Input Status : "); builder.Append((int) cInputStatus);
                builder.Append("\r\n    Output Status : "); builder.Append((int) cOutputStatus);
                builder.Append("\r\n    Button Status : "); builder.Append((int) cButtonStatus);
                builder.Append("\r\n    Light Status : "); builder.Append((int) cLightStatus);
                builder.Append("\r\n    Extended Status Flags 2 : "); builder.Append(ExtendedStatusFlag2);
            }
            catch(System.Exception ex)
            {
                builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
            }
            return builder.ToString();
        }

        public bool IsExtendedStatusFlag2Used
        {
            get
            {
                if ((cSpareStatus & 0x80) == 0x80)
                {
                    return true;
                }
                return false;
            }
        }
    }
}

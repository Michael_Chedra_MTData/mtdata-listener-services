using System;
using System.Text;

namespace MTData.Transport.Gateway.Message
{
    /// <summary>
    /// Summary description for GPDistance.
    /// </summary>
    [Serializable]
    public class GPDistance
    {
        public byte cMaxSpeed;
        public long lSpeedAccumulator;
        public int iSamples;
        public Boolean bIsPopulated;

        public const int Length = 7;

        public GPDistance()
        {
            cMaxSpeed = 0;
            lSpeedAccumulator = 0;
            iSamples = 0;
            bIsPopulated = false;
        }

        public GPDistance CreateCopy()
        {
            GPDistance oDist = new GPDistance();
            oDist.cMaxSpeed = this.cMaxSpeed;
            oDist.lSpeedAccumulator = this.lSpeedAccumulator;
            oDist.iSamples = this.iSamples;
            oDist.bIsPopulated = this.bIsPopulated;
            return oDist;
        }

        public string Decode(Byte[] aData)
        {
            return Decode(aData, 0);
        }

        public string Decode(Byte[] aData, int aStartPos)
        {
            // Ensure we've got a decent array:
            if (aData.Length < (aStartPos + Length)) return "Distance field too short";

            cMaxSpeed= aData[aStartPos++];
            lSpeedAccumulator = (aData[aStartPos++]);
            lSpeedAccumulator += (aData[aStartPos++] << 8);
            lSpeedAccumulator += (aData[aStartPos++] << 16);
            lSpeedAccumulator += (aData[aStartPos++] << 24);
            
            iSamples = aData[aStartPos++];
            iSamples += (aData[aStartPos++] << 8);
            bIsPopulated = true;
            return null;
        }
        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;

            if (theData.Length >= (aStartPos + Length))
            {
                theData[aStartPos++] = cMaxSpeed;
                bTemp = BitConverter.GetBytes(lSpeedAccumulator);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                theData[aStartPos++] = bTemp[2];
                theData[aStartPos++] = bTemp[3];
                bTemp = BitConverter.GetBytes(iSamples);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
            }
        }

        public string ToDisplayString()
        {
            return	this.ToString();
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nDistance Data :"); 
                builder.Append("\r\n    Max Speed : "); builder.Append((int)cMaxSpeed);
                builder.Append("\r\n    SpeedAcc : "); builder.Append(lSpeedAccumulator);
                builder.Append("\r\n    Samples : "); builder.Append(iSamples);
            }
            catch(System.Exception ex)
            {
                builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
            }
            return builder.ToString();
        }

    }
}

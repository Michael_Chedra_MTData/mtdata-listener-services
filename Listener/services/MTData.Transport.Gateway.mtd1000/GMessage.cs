﻿using Newtonsoft.Json;
using System;
using System.Reflection;

namespace MTData.Transport.Gateway.Message
{
    public class GMessage
    {
        private ProtocalLayout lv;
        
        public bool DecodeHex(string theDataHex)
        {
            string[] theDataHexSplit = theDataHex.Split('-');
            int DataLength = theDataHexSplit.Length;

            byte[] theData = new byte[DataLength];
            for (uint i = 0; i < DataLength; i++)
            {
                theData[i] = Convert.ToByte(theDataHexSplit[i], 16);
            }

            return Decode(theData);
        }

        public bool Decode(byte[] theData)
        {
            lv = new ProtocalLayout(theData);

            // [SOP][Cmd Type][Flags][Length][FleetID][VehicleID][Packet Num][Packet Total][Our Seq][Unit Ack Seq][Data][Checksum][EOT]
            // [SOP] = 0x01
            // [Cmd Type] - 1 byte
            // [Flags] - 4 bits
            // [Length] - 12 bits
            // [FleetID] - 1 byte
            // [VehicleID] - 2 bytes
            // [Packet Num] - 4 bits
            // [Packet Total] - 4 bits
            // [Our Seq] - 1 byte
            // [Unit Ack Seq] - 1 byte
            // [Data] - Variable lenght payload
            // [Checksum] - 1 byte
            // [EOT] = 0x04

            uint checksum = 0, pos = 0;

            byte cControlByte = lv._messageReader.ReadByte();

            // Application-level PING
            if ((lv.messageLength == 1 || lv.messageLength == 4 || lv.messageLength == 20) &&
                (cControlByte == GConstants.GSP_APP_PING))
            {
                lv.cMsgType = GConstants.GSP_APP_PING;

                if (lv.messageLength > 1)
                {
                    lv.cFleetId = lv._messageReader.ReadByte();
                    lv.iVehicleId = lv._messageReader.ReadUInt16();
                }
                else
                {
                    lv.cFleetId = 0;
                    lv.iVehicleId = 0;
                }

                lv.bAckImmediately = true;
                lv.bAckRegardless = true;
                lv.bSendFlash = false;
                lv.bArchivalData = false;

                lv.iLength = 0;
                lv.iTotalLength = Convert.ToUInt32(lv._messageLength);
            }
            else
            {
                // Check the array is a reasonable length:
                if (lv.messageLength < GConstants.GSP_OVERHEAD) return lv.PacketError(GConstants.PACKET_ERROR.Packet_Under_Size);

                // Check we start and end properly:
                if (cControlByte != GConstants.GSP_SOH) return lv.PacketError(GConstants.PACKET_ERROR.No_SOH_byte);

                lv.cMsgType = lv._messageReader.ReadByte();

                byte cMessageFlag = lv._messageReader.ReadByte();

                // length is actually a 12-bit field, and we have a 4-bit flag area
                // in the MS nibble of the MS byte.
                // decode these bits now:
                lv.bAckImmediately = ((cMessageFlag & GConstants.PACKET_FLAG_ACKI) == GConstants.PACKET_FLAG_ACKI);
                // and the accept this sequence number, regardless, bir:
                lv.bAckRegardless = ((cMessageFlag & GConstants.PACKET_FLAG_ACK_REGARDLESS) == GConstants.PACKET_FLAG_ACK_REGARDLESS);
                // And check if the other end wants to unload their flash:
                lv.bSendFlash = ((cMessageFlag & GConstants.PACKET_FLAG_SEND_FLASH) == GConstants.PACKET_FLAG_SEND_FLASH);
                // See if this is historical data which should be quietly inserted into the database
                // without telling viewers:
                lv.bArchivalData = ((cMessageFlag & GConstants.PACKET_FLAG_ARCHIVAL_DATA) == GConstants.PACKET_FLAG_ARCHIVAL_DATA);

                // The Secondary Server flag is actually not a bit in the packet:
                // If a device ever sends
                // a packet with ARCHIVE set but no FLASH flag (which is impossible
                // if it's unloading from Flash) it actually means the device is
                // using the Secondary Server. The Listener can then take action to
                // move the device back using the same flag. jam 4/8/04
                if (!lv.bSendFlash && lv.bArchivalData)
                {   // This is the secondary server for this device
                    lv.bUsingSecondaryServer = true;
                    lv.bArchivalData = false; // the data is not actually archival :-)
                }

                // Read in the Length because it's very useful to check:
                lv.iLength = (uint)((cMessageFlag & (~GConstants.PACKET_FLAG_MASK)) << 8) + (lv._messageReader.ReadByte());
                lv.iTotalLength = lv._iLength + GConstants.GSP_OVERHEAD;

                if (lv.iTotalLength != lv._messageLength) return lv.PacketError(GConstants.PACKET_ERROR.Packet_Size_Mismatch); // There is no way this packet is gonna fit

                // Verify the checksum. 
                for (pos = 0; pos < (lv.iTotalLength - GConstants.GSP_TRAILER_LENGTH); pos++)
                {
                    checksum += theData[pos];
                }
                // Special case for the upper nibble of length, we ignore the packet flags when calculating the checksum
                checksum -= Convert.ToUInt16(theData[2] & 0xF0);

                // By now we have handled the most likely errors. Start copying over:
                lv.cFleetId = lv._messageReader.ReadByte();
                lv.iVehicleId = lv._messageReader.ReadUInt16();

                byte cPacketControl = lv._messageReader.ReadByte();

                lv.cPacketNum = Convert.ToByte((cPacketControl & 0xF0) >> 4); // Upper nibble
                lv.cPacketTotal = Convert.ToByte(cPacketControl & 0x0F); // Lower nibble

                if (lv.cPacketTotal == (byte)0x01)
                {
                    lv.bEngineData = ((lv.cPacketNum & (byte)0x01) == (byte)0x01);
                    lv.bExtraDetails = ((lv.cPacketNum & (byte)0x02) == (byte)0x02);
                    lv.bTrailerTrack = ((lv.cPacketNum & (byte)0x04) == (byte)0x04);
                    lv.bExtendedVariableDetails = ((lv.cPacketNum & (byte)0x08) == (byte)0x08);
                    lv.bECMAdvanced = false;
                }
                else if (lv.cPacketTotal == (byte)0x03)
                {
                    lv.bEngineData = false;
                    lv.bExtraDetails = false;
                    lv.bTrailerTrack = ((lv.cPacketNum & (byte)0x0C) == (byte)0x0C);
                    lv.bExtendedVariableDetails = true;
                    lv.bECMAdvanced = true;
                }
                else if (lv.cPacketTotal == (byte)0x04 || lv.cPacketTotal == (byte)0x05 || lv.cPacketTotal == (byte)0x06 || lv.cPacketTotal == (byte)0x07)
                {
                    lv.bEngineData = ((lv.cPacketNum & (byte)0x01) == (byte)0x01);
                    lv.bExtraDetails = ((lv.cPacketNum & (byte)0x02) == (byte)0x02);
                    lv.bTrailerTrack = ((lv.cPacketNum & (byte)0x04) == (byte)0x04);
                    lv.bExtendedVariableDetails = ((lv.cPacketNum & (byte)0x08) == (byte)0x08);
                    lv.bECMAdvanced = true;

                    if (lv.cPacketTotal == 0x07)
                    {
                        lv.hasRoadTypeData = true;
                    }
                }
                else if (lv.cPacketTotal == (byte)0x06)
                {
                    //need to skip 31 bytes of additional advanced ECM data that is not used at present
                    //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                    lv.AdditionalAdvancedECM = true;
                }

                lv._cOurSequence = lv._messageReader.ReadByte();
                lv._cAckSequence = lv._messageReader.ReadByte();

                lv.mDataFieldLength = Convert.ToInt32(lv._messageLength - lv._messageReader.BaseStream.Position) - GConstants.GSP_TRAILER_LENGTH;
                lv.mDataField = lv._messageReader.ReadBytes(lv.mDataFieldLength);

                // Read in the checksum now
                lv._iChecksum = lv._messageReader.ReadByte(); // theData[lv.iTotalLength - GConstants.GSP_TRAILER_LENGTH];

                // Trim off bits in excess of 255 and test:
                if (lv._iChecksum != (checksum & 0xFF))
                    return lv.PacketError(GConstants.PACKET_ERROR.Checksum_Error); // "Checksum error: Rxed " + iChecksum + ", calculated " + (checksum & 0xFF);

                if (lv._messageReader.ReadByte() != GConstants.GSP_EOT) return lv.PacketError(GConstants.PACKET_ERROR.No_EOT_Byte);

                MethodInfo theMethod = lv.MsgTypeClass.GetMethod("DecodeDataFields");
                object classInstance = Activator.CreateInstance(lv.MsgTypeClass, null);

                return (bool)theMethod.Invoke(classInstance, new object[] { lv });
            }

            return true;
        }

        public string ToJSON()
        {
            return JsonConvert.SerializeObject(lv);
        }
    }
}
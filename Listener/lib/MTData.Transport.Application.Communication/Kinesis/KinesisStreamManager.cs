﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace MTData.Transport.Application.Communication.Kinesis
{
    public abstract class KinesisStreamManager: IDisposable
    {
        private List<KinesisStream> _streams = new List<KinesisStream>();

        private object _statsSync = new object();
        private long _insertCount;
        private long _readCount;

        public List<long> InsertCount
        {
            get
            {
                List<long> result = new List<long>();
                lock (_statsSync)
                {
                    result.Add(_insertCount);
                    _insertCount = 0;
                }
                _streams.ForEach(s => result.Add(s.InsertCount));
                return result;
            }
        }


        protected void InsertObject(string jsonObject, string partitionKey)
        {
            lock (_statsSync)
            {
                _insertCount++;
            }
            _streams.ForEach(s => s.InsertObject(jsonObject, partitionKey));
        }

        protected void InsertObjectWithFilters(string jsonObject, string partitionKey, int? fleetId, int? vehicleId, int? driverId, int? reasonId)
        {
            lock (_statsSync)
            {
                _insertCount++;
            }
            _streams.ForEach(s =>
            {
                if (s.CheckFilters(fleetId, vehicleId, driverId, reasonId))
                {
                    s.InsertObject(jsonObject, partitionKey);
                }
            });
        }

        protected void CreateStreamsFromXml(XmlNodeList nodeList)
        {
            foreach (XmlNode n in nodeList)
            {
                KinesisStream s = new KinesisStream();
                s.CreateStreamFromXml(n);
                _streams.Add(s);
            }
        }

        protected string CreatePartitionKey(int fleetId, int vehicleId)
        {
            return string.Format("F{0} V{1}", fleetId, vehicleId);
        }

        public void Dispose()
        {
            _streams.ForEach(s => s.Dispose());
        }

        public void StartRead()
        {
            _streams.ForEach(s => 
            {
                s.OnRecordRead += Stream_OnRecordRead;
                s.StartRead();
            });
        }

        public abstract void Stream_OnRecordRead(string jsonObject);

        protected void AddStream(KinesisStream stream)
        {
            _streams.Add(stream);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;
using log4net;

namespace MTData.Transport.Application.Communication.Kinesis
{
    public class ByteArrayToKinesis : KinesisStreamManager, System.Configuration.IConfigurationSectionHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(ByteArrayToKinesis));

        public delegate void LiveUpdateReceived(byte[] update);
        public event LiveUpdateReceived OnUpdateReceived;

        public ByteArrayToKinesis()
        {
        }
        public ByteArrayToKinesis(KinesisStream stream)
        {
            AddStream(stream);
        }

        public void InsertLiveUpdate(byte[] update, int fleetId, int vehicleId)
        {
            try
            {
                string xml = JsonConvert.SerializeObject(update);
                string key = CreatePartitionKey(fleetId, vehicleId);
                InsertObjectWithFilters(xml, key, fleetId, vehicleId, null, null);
            }
            catch (Exception exp)
            {
                _log.ErrorFormat("Error inserting live update - {0}", exp.Message);
            }
        }

        public override void Stream_OnRecordRead(string jsonObject)
        {
            byte[] data = JsonConvert.DeserializeObject<byte[]>(jsonObject);
            if (data != null)
            {
                OnUpdateReceived?.Invoke(data);
            }
        }

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<ByteArrayUpdateToKinesis>
        /// 		<KinesisStream EndPoint="ap-southeast-2"
        /// 			AccessKey="key" SecretKey="skey"
        /// 			StreamName="stream"
        /// 			ShardPartitionKey="k"
        /// 			LogStats="false">
        /// 		</KinesisStream>
        /// 	</ByteArrayUpdateToKinesis>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("KinesisStream");
                base.CreateStreamsFromXml(list);
            }
            catch (Exception ex)
            {
                _log.Error("Error creating Kinesis streams", ex);
            }
            return this;
        }
        #endregion

    }
}

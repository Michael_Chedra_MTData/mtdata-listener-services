﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;
using log4net;

namespace MTData.Transport.Application.Communication.Kinesis
{
    public class StringToKinesis : KinesisStreamManager, System.Configuration.IConfigurationSectionHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(StringToKinesis));

        public delegate void LiveUpdateReceived(string update);
        public event LiveUpdateReceived OnUpdateReceived;

        public StringToKinesis()
        {
        }
        public StringToKinesis(KinesisStream stream)
        {
            AddStream(stream);
        }

        public void InsertLiveUpdate(string update, int fleetId, int vehicleId)
        {
            try
            {
                string key = CreatePartitionKey(fleetId, vehicleId);
                InsertObjectWithFilters(update, key, fleetId, vehicleId, null, null);
            }
            catch (Exception exp)
            {
                _log.ErrorFormat("Error inserting live update - {0}", exp.Message);
            }
        }

        public override void Stream_OnRecordRead(string jsonObject)
        {
            OnUpdateReceived?.Invoke(jsonObject);
        }

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<ByteArrayUpdateToKinesis>
        /// 		<KinesisStream EndPoint="ap-southeast-2"
        /// 			AccessKey="key" SecretKey="skey"
        /// 			StreamName="stream"
        /// 			ShardPartitionKey="k"
        /// 			LogStats="false">
        /// 		</KinesisStream>
        /// 	</ByteArrayUpdateToKinesis>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("KinesisStream");
                base.CreateStreamsFromXml(list);
            }
            catch (Exception ex)
            {
                _log.Error("Error creating Kinesis streams", ex);
            }
            return this;
        }
        #endregion

    }
}

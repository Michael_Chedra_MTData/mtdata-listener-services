﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Xml;
using Amazon;
using Amazon.Runtime;
using Amazon.Kinesis;
using Amazon.Kinesis.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MTData.Common.Threading;
using log4net;

namespace MTData.Transport.Application.Communication.Kinesis
{
    public class KinesisStream : IDisposable
    {
        #region private fields
        private ILog _log = LogManager.GetLogger(typeof(KinesisStream));
        private AmazonKinesisClient _kinesisClient;
        private object _statsSync = new object();
        private long _insertCount;
        private long _readCount;
        private List<EnhancedThread> _readThreads;
        private object _checkPointsSync = new object();
        private object _checkPointsFileSync = new object();
        private System.Timers.Timer _checkPointSaveTimer;
        #endregion

        #region properties
        public string StreamName { get; set; }
        public List<ShardCheckPoint> CheckPoints { get; set; }
        public string CheckPointsFileName { get; set; }

        public List<int> FleetIdFilter { get; set; }
        public List<Tuple<int,int>> VehicleIdFilter { get; set; }
        public List<Tuple<int, int>> DriverIdFilter { get; set; }
        public List<int> ReasonCodeIdFilter { get; set; }
        public List<int> ReasonCodeIdIgnoreFilter { get; set; }

        public bool LogStats { get; set; }
        public long InsertCount
        {
            get
            {
                long ret = 0;
                lock (_statsSync)
                {
                    ret = _insertCount;
                    _insertCount = 0;
                }
                return ret;
            }
        }
        public long ReadCount
        {
            get
            {
                long ret = 0;
                lock (_statsSync)
                {
                    ret = _readCount;
                    _readCount = 0;
                }
                return ret;
            }
        }
        #endregion

        #region events
        public delegate void RecordRead(string jsonObject);
        public event RecordRead OnRecordRead;
        #endregion

        #region Constructors
        public KinesisStream() { }

        public KinesisStream(AmazonKinesisClient client)
        {
            _kinesisClient = client;
        }
        #endregion

        public void InsertObject(string jsonObject, string partitionKey)
        {
            byte[] dataAsBytes = Encoding.UTF8.GetBytes(jsonObject);
            using (MemoryStream memoryStream = new MemoryStream(dataAsBytes))
            {
                PutRecordRequest requestRecord = new PutRecordRequest();
                requestRecord.StreamName = StreamName;
                requestRecord.PartitionKey = partitionKey;
                requestRecord.Data = memoryStream;

                PutRecordResponse responseRecord = _kinesisClient.PutRecord(requestRecord);
                if (LogStats)
                {
                    lock (_statsSync)
                    {
                        _insertCount++;
                    }
                }
            }
        }

        public bool CheckFilters(int? fleetId, int? vehicleId, int? driverId, int? reasonId)
        {
            if (fleetId.HasValue && FleetIdFilter != null && FleetIdFilter.Count > 0)
            {
                if (!FleetIdFilter.Contains(fleetId.Value))
                {
                    return false;
                }
            }

            if (fleetId.HasValue && vehicleId.HasValue && VehicleIdFilter != null && VehicleIdFilter.Count > 0)
            {
                if (!VehicleIdFilter.Contains(new Tuple<int, int>(fleetId.Value, vehicleId.Value)))
                {
                    return false;
                }
            }

            if (fleetId.HasValue && driverId.HasValue && DriverIdFilter != null && DriverIdFilter.Count > 0)
            {
                if (!DriverIdFilter.Contains(new Tuple<int, int>(fleetId.Value, driverId.Value)))
                {
                    return false;
                }
            }

            if (reasonId.HasValue && ReasonCodeIdFilter != null && ReasonCodeIdFilter.Count > 0)
            {
                if (!ReasonCodeIdFilter.Contains(reasonId.Value))
                {
                    return false;
                }
            }

            if (reasonId.HasValue && ReasonCodeIdIgnoreFilter != null && ReasonCodeIdIgnoreFilter.Count > 0)
            {
                if (ReasonCodeIdIgnoreFilter.Contains(reasonId.Value))
                {
                    return false;
                }
            }

            return true;

        }

        public void CreateStreamFromXml(XmlNode node)
        {
            RegionEndpoint endPoint = RegionEndpoint.GetBySystemName(node.Attributes.GetNamedItem("EndPoint").Value);
            
            XmlNode AccessKeyNode = node.Attributes.GetNamedItem("AccessKey");
            XmlNode SecretKeyNode = node.Attributes.GetNamedItem("SecretKey");
            if ((AccessKeyNode != null && !string.IsNullOrEmpty(AccessKeyNode.Value)) 
                || (SecretKeyNode != null && !string.IsNullOrEmpty(SecretKeyNode.Value)))
            {
                _kinesisClient = new AmazonKinesisClient(AccessKeyNode.Value, SecretKeyNode.Value, endPoint);
            }
            else
            {
                _kinesisClient = new AmazonKinesisClient(endPoint);
            }

            StreamName = node.Attributes.GetNamedItem("StreamName").Value;
            LogStats = Convert.ToBoolean(node.Attributes.GetNamedItem("LogStats").Value);

            FleetIdFilter = node.SelectNodes("fleetFilter").Cast<XmlNode>().
                Select(n => Convert.ToInt32(n.Attributes["fleetId"].Value)).ToList();

            VehicleIdFilter = node.SelectNodes("vehicleFilter").Cast<XmlNode>().
                Select(n => new Tuple<int, int>(
                    Convert.ToInt32(n.Attributes["fleetId"].Value), 
                    Convert.ToInt32(n.Attributes["vehicleId"].Value)
                    )).ToList();

            DriverIdFilter = node.SelectNodes("driverFilter").Cast<XmlNode>().
                Select(n => new Tuple<int, int>(
                    Convert.ToInt32(n.Attributes["fleetId"].Value),
                    Convert.ToInt32(n.Attributes["driverId"].Value)
                    )).ToList();

            ReasonCodeIdFilter = node.SelectNodes("reasonCodeFilter").Cast<XmlNode>().
                Select(n => Convert.ToInt32(n.Attributes["reasonId"].Value)).ToList();

            ReasonCodeIdIgnoreFilter = node.SelectNodes("reasonCodeIgnoreFilter").Cast<XmlNode>().
                Select(n => Convert.ToInt32(n.Attributes["reasonId"].Value)).ToList();

            CheckPointsFileName = node.Attributes.GetNamedItem("CheckPointsFileName")?.Value;
            LoadCheckPoints();
        }

        public void Dispose()
        {
            if (_kinesisClient != null)
            {
                _kinesisClient.Dispose();
            }

            if (_readThreads != null)
            {
                _readThreads.ForEach(t =>
                {
                    t.Stop();
                    while (t.Stopping)
                    {
                        Thread.Sleep(100);
                    }
                });

                SaveCheckPoints();
            }
        }

        public void StartRead()
        {
            if (CheckPoints == null)
            {
                CheckPoints = new List<ShardCheckPoint>();
            }

            _checkPointSaveTimer = new System.Timers.Timer(10000);
            _checkPointSaveTimer.Elapsed += (sender, e) => SaveCheckPoints();
            _checkPointSaveTimer.Start();

            DescribeStreamRequest describeRequest = new DescribeStreamRequest();
            describeRequest.StreamName = StreamName;

            DescribeStreamResponse describeResponse = _kinesisClient.DescribeStream(describeRequest);
            List<Shard> shards = describeResponse.StreamDescription.Shards;
            _readThreads = new List<EnhancedThread>();
            foreach (Shard shard in shards)
            {
                EnhancedThread t = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReadFromStream), shard);
                t.Name = string.Format("read Kinesis Stream - Shard {0}", shard.ShardId);
                t.Start();
                _readThreads.Add(t);
            }
        }

        public void LoadCheckPoints()
        {
            if (File.Exists(CheckPointsFileName))
            {
                string data;
                lock (_checkPointsFileSync)
                {
                    data = File.ReadAllText(CheckPointsFileName);
                }

                lock (_checkPointsSync)
                {
                    CheckPoints = JsonConvert.DeserializeObject<List<ShardCheckPoint>>(data);
                }
            }
        }

        public void SaveCheckPoints()
        {
            if (!string.IsNullOrEmpty(CheckPointsFileName))
            {
                string data = null;
                lock(_checkPointsSync)
                {
                    data = JsonConvert.SerializeObject(CheckPoints);
                }

                lock (_checkPointsFileSync)
                {
                    File.WriteAllText(CheckPointsFileName, data);
                }
            }
        }

        private object ReadFromStream(EnhancedThread t, object data)
        {
            Shard shard = data as Shard;

            ShardCheckPoint checkPoint = null;
            ShardCheckPoint.StartingPoints start = ShardCheckPoint.StartingPoints.Latest;
            string seq = null;
            lock (_checkPointsSync)
            {
                checkPoint = CheckPoints.FirstOrDefault(c => c.ShardId == shard.ShardId);
                if (checkPoint == null)
                {
                    //create a new check point
                    checkPoint = new ShardCheckPoint()
                    {
                        ShardId = shard.ShardId,
                        StartingPoint = ShardCheckPoint.StartingPoints.Latest
                    };
                    CheckPoints.Add(checkPoint);
                }
                start = checkPoint.StartingPoint;
                seq = checkPoint.SequenceNumber;
                //set checkPoint starting point to after sequence
                checkPoint.StartingPoint = ShardCheckPoint.StartingPoints.AfterSequenceNumber;
            }

            GetShardIteratorRequest iteratorRequest = new GetShardIteratorRequest();
            iteratorRequest.StreamName = StreamName;
            iteratorRequest.ShardId = shard.ShardId;
            switch (start)
            {
                case ShardCheckPoint.StartingPoints.AfterSequenceNumber:
                    if (seq != null)
                    {
                        iteratorRequest.ShardIteratorType = ShardIteratorType.AFTER_SEQUENCE_NUMBER;
                        iteratorRequest.StartingSequenceNumber = seq;
                    }
                    else
                    {
                        iteratorRequest.ShardIteratorType = ShardIteratorType.LATEST;
                    }
                    break;
                case ShardCheckPoint.StartingPoints.AtSequenceNumber:
                    if (seq != null)
                    {
                        iteratorRequest.ShardIteratorType = ShardIteratorType.AT_SEQUENCE_NUMBER;
                        iteratorRequest.StartingSequenceNumber = seq;
                    }
                    else
                    {
                        iteratorRequest.ShardIteratorType = ShardIteratorType.LATEST;
                    }
                    break;
                case ShardCheckPoint.StartingPoints.Latest:
                    iteratorRequest.ShardIteratorType = ShardIteratorType.LATEST;
                    break;
                case ShardCheckPoint.StartingPoints.Start:
                    iteratorRequest.ShardIteratorType = ShardIteratorType.TRIM_HORIZON;
                    break;
            }

            GetShardIteratorResponse iteratorResponse = _kinesisClient.GetShardIterator(iteratorRequest);
            string iteratorId = iteratorResponse.ShardIterator;

            while (!t.Stopping && !string.IsNullOrEmpty(iteratorId))
            {
                try
                {
                    GetRecordsRequest getRequest = new GetRecordsRequest();
                    getRequest.Limit = 500;
                    getRequest.ShardIterator = iteratorId;

                    GetRecordsResponse getResponse = _kinesisClient.GetRecords(getRequest);
                    string nextIterator = getResponse.NextShardIterator;
                    List<Record> records = getResponse.Records;

                    string lastSequence = null;
                    if (records.Count > 0)
                    {
                        foreach (Record record in records)
                        {
                            if (t.Stopping)
                            {
                                break;
                            }
                            string json = Encoding.UTF8.GetString(record.Data.ToArray());
                            OnRecordRead?.Invoke(json);
                            lastSequence = record.SequenceNumber;
                        }
                    }
                    if (lastSequence != null)
                    {
                        lock (_checkPointsSync)
                        {
                            checkPoint.SequenceNumber = lastSequence;
                        }
                    }
                    iteratorId = nextIterator;
                }
                catch (Exception exp)
                {
                    _log.ErrorFormat("Error reading Stream {0}, Shard {1}, error {2}", StreamName, shard.ShardId, exp.Message);
                }
            }

            return null;
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Xml;
using Newtonsoft.Json;
using log4net;

namespace MTData.Transport.Application.Communication.Kinesis
{
    public class LiveUpdateToKinesis : KinesisStreamManager, System.Configuration.IConfigurationSectionHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(LiveUpdateToKinesis));

        public delegate void LiveUpdateReceived(LiveUpdate update);
        public event LiveUpdateReceived OnUpdateReceived;

        public LiveUpdateToKinesis()
        {
        }
        public LiveUpdateToKinesis(KinesisStream stream)
        {
            AddStream(stream);
        }

        public void InsertLiveUpdate(LiveUpdate update)
        {
            try
            {
                string xml = JsonConvert.SerializeObject(update);
                string key = CreatePartitionKey(update.iFleetID, update.iVehicleID);
                InsertObjectWithFilters(xml, key, update.iFleetID, update.iVehicleID, null, update.iReasonID);
            }
            catch (Exception exp)
            {
                _log.ErrorFormat("Error inserting live update - {0}", exp.Message);
            }
        }

        public override void Stream_OnRecordRead(string jsonObject)
        {
            LiveUpdate update = JsonConvert.DeserializeObject<LiveUpdate>(jsonObject);
            if (update != null)
            {
                OnUpdateReceived?.Invoke(update);
            }
        }

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<LiveUpdateToKinesis>
        /// 		<KinesisStream EndPoint="ap-southeast-2"
        /// 			AccessKey="key" SecretKey="skey"
        /// 			StreamName="stream"
        /// 			ShardPartitionKey="k"
        /// 			LogStats="false">
        /// 		</KinesisStream>
        /// 	</LiveUpdateToKinesis>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("KinesisStream");
                base.CreateStreamsFromXml(list);
            }
            catch (Exception ex)
            {
                _log.Error("Error creating Kinesis streams", ex);
            }
            return this;
        }
        #endregion

    }
}

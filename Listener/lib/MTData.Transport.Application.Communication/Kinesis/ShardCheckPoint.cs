﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Application.Communication.Kinesis
{
    public class ShardCheckPoint
    {
        public enum StartingPoints
        {
            AfterSequenceNumber,
            AtSequenceNumber,
            Latest,
            Start
        }

        public string ShardId { get; set; }

        public StartingPoints StartingPoint { get; set; } 

        public string SequenceNumber { get; set; }
    }
}

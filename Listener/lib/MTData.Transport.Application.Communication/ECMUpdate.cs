using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
    public class ECMUpdateItemData
    {
        public byte SourceType;
        public byte Flags;
        public ushort SPN_ID;
        public byte[] SPN_Data;

        public ECMUpdateItemData(byte _sourceType, ushort _SPN_ID, byte bFlags, byte[] _SPN_Data)
        {
            SourceType = _sourceType;
            SPN_ID = _SPN_ID;
            SPN_Data = _SPN_Data;
            Flags = bFlags;
        }

        public ECMUpdateItemData(MemoryStream oStream)
        {
            Decode(oStream);
        }

        public void Encode(MemoryStream oStream)
        {
            PacketUtilities.WriteToStream(oStream, SourceType); // Source Type (1 byte)
            PacketUtilities.WriteToStream(oStream, SPN_ID); // SPN ID (4 byte int)
            PacketUtilities.WriteToStream(oStream, Flags); // Flags (1 byte int)
            byte bTemp = (byte) this.SPN_Data.Length;
            PacketUtilities.WriteToStream(oStream, bTemp); // Data Length (1 byte)
            PacketUtilities.WriteToStream(oStream, this.SPN_Data); // Data (variable byte[])
        }

        public void Decode(MemoryStream oStream)
        {
            byte bDataLen = (byte)0x00;
            PacketUtilities.ReadFromStream(oStream, ref this.SourceType);				        // Source Type (1 byte)
            PacketUtilities.ReadFromStream(oStream, ref this.SPN_ID);				            // SPN ID (4 byte int)
            PacketUtilities.ReadFromStream(oStream, ref this.Flags);				            // Flags (1 byte)
            PacketUtilities.ReadFromStream(oStream, ref bDataLen);				                // Data Length (1 byte)
            this.SPN_Data = new byte[(int)bDataLen];
            PacketUtilities.ReadFromStream(oStream, (int)bDataLen, ref this.SPN_Data);          // Data (variable byte[])
        }
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("\r\n        SPN ID : "); builder.Append(SPN_ID);
            builder.Append("\r\n        Source : "); builder.Append(SourceType);
            builder.Append("\r\n        Flags : "); builder.Append(Flags);
            builder.Append("\r\n        Data : "); builder.Append(BitConverter.ToString(SPN_Data));
            return builder.ToString();
        }
    }

    public class ECMUpdate
    {
        public enum ECMUpdateSubCmd : byte
        {
            AddToUpdateList = (byte)'A',
            RemoveFromUpdateList = (byte)'R',
            ClientUpdate = (byte)'U'
        };

        public char bCmd = 'E';
        public byte bSep = (byte) 0xAA;
        public ECMUpdateSubCmd bSubCmd = ECMUpdateSubCmd.ClientUpdate;
        public short iPacketVersion = 1;
        public int iFleetID = 0;
        public int iVehicleID = 0;
        public DateTime dtDeviceTime = System.DateTime.MinValue;
        public string Location = "";
        public float Latitude = 0;
        public float Longitude = 0;

        public bool bIsTrackingUnit = true;
        public bool bIsRefrigerationUnit = false;
        public List<ECMUpdateItemData> _ECMValues;

        public ECMUpdate()
        {

        }

        public ECMUpdate(ECMUpdateSubCmd subCmd, int fleetID, int vehicleID)
        {
            if (subCmd != ECMUpdateSubCmd.AddToUpdateList && subCmd != ECMUpdateSubCmd.RemoveFromUpdateList)
                throw (new Exception("The sub command type supplied does not match the parameter list"));
            bSubCmd = subCmd;
            iFleetID = fleetID;
            iVehicleID = vehicleID;
        }

        public ECMUpdate(int fleetID, int vehicleID, DateTime deviceTime, string _location, float _latitude, float _longitude)
        {
            bSubCmd = ECMUpdateSubCmd.ClientUpdate;
            iFleetID = fleetID;
            iVehicleID = vehicleID;
            dtDeviceTime = deviceTime;
            Location = _location;
            Latitude = _latitude;
            Longitude = _longitude;
            _ECMValues = new List<ECMUpdateItemData>();
        }

        public void AddECMValue(ushort iSPN_ID, byte bSourceType, byte bFlags, byte[] bData)
        {
            if(_ECMValues == null)
                return;
            _ECMValues.Add(new ECMUpdateItemData(bSourceType, iSPN_ID, bFlags, bData));
        }

        public byte[] EncodeByStream(ref string sErrMsg)
        {
            byte bEOP = (byte)0x07;
            byte bSOP = (byte)0x01;
            byte[] bResult = null;
            byte bValueCount = (byte) 0x00;
            MemoryStream oStream = null;

            try
            {
                oStream = new MemoryStream();
                PacketUtilities.WriteToStream(oStream, bSOP);						// SOP
                PacketUtilities.WriteToStream(oStream, (byte)0x00);				    // Length 1
                PacketUtilities.WriteToStream(oStream, (byte)0x00);				    // Length 2
                PacketUtilities.WriteToStream(oStream, bCmd);						// Packet Type (1 byte)
                PacketUtilities.WriteToStream(oStream, (byte)bSubCmd);				// Sub Command (1 byte)
                PacketUtilities.WriteToStream(oStream, iPacketVersion);             // Packet Version (2 byte int)
                if (bSubCmd == ECMUpdateSubCmd.AddToUpdateList || bSubCmd == ECMUpdateSubCmd.RemoveFromUpdateList)
                {
                    PacketUtilities.WriteToStream(oStream, iFleetID);				// Fleet ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, iVehicleID);				// Vehicle ID (4 byte int)
                }
                else if (bSubCmd == ECMUpdateSubCmd.ClientUpdate) // If this is an update to send to the clients
                {
                    PacketUtilities.WriteToStream(oStream, iFleetID);               // Fleet ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, iVehicleID);             // Vehicle ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, dtDeviceTime.ToOADate()); // Device Time (8 byte double)
                    PacketUtilities.WriteToStream(oStream, bSep, Location);         // The location at the report time (var string)
                    PacketUtilities.WriteToStream(oStream, Latitude);              // Latitude (4 byte float)
                    PacketUtilities.WriteToStream(oStream, Longitude);             // Longitude (4 byte float)
                    if(_ECMValues == null || _ECMValues.Count == 0)
                        PacketUtilities.WriteToStream(oStream, bValueCount);         // Number of ECM Entries
                    else
                    {
                        bValueCount = (byte) _ECMValues.Count;
                        PacketUtilities.WriteToStream(oStream, bValueCount);         // Number of ECM Entries
                        for (int X = 0; X < _ECMValues.Count; X++)
                            _ECMValues[X].Encode(oStream);                           // ECM Data object (Variable)
                    }
                }
                PacketUtilities.WriteToStream(oStream, bEOP);						// End of Packet (1 byte)
                bResult = oStream.ToArray();
                // Write in the length bytes
                bResult[1] = ((byte[])BitConverter.GetBytes(bResult.Length))[0];
                bResult[2] = ((byte[])BitConverter.GetBytes(bResult.Length))[1];
            }
            catch (System.Exception ex)
            {
                sErrMsg = "Error encoding update : " + ex.Message;
                bResult = null;
            }
            return bResult;
        }
        public byte[] Encode(ref string sErrMsg)
        {
            return EncodeByStream(ref sErrMsg);
        }
        public bool DecodeFromStream(byte[] bData, ref string sErrMsg)
        {
            bool bRet = false;
            byte bTemp = (byte)0x00;
            byte bEOP = (byte)0x07;
            byte bValue = (byte)0x00;
            char cType = (char)0x00;
            short iLength = 0;
            double dValue = 0;
            MemoryStream oStream = null;

            try
            {
                oStream = new MemoryStream(bData, 0, bData.Length, false, false);
                PacketUtilities.ReadFromStream(oStream, ref bValue);
                if (bValue != (byte)0x01)														    // SOP
                {
                    sErrMsg = "Error decoding : SOP not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromStream(oStream, ref iLength);							    // Length
                if (bData.Length != iLength)
                {
                    sErrMsg = "Error decoding : Packet length is incorrect.";
                    return bRet;
                }
                if (bData[iLength - 1] != bEOP)
                {
                    sErrMsg = "Error decoding : EOP was not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromStream(oStream, ref cType);								    // Packet Type (1 byte)
                if (cType != bCmd)
                {
                    sErrMsg = "Error decoding : Packet type is incorrect.";
                    return bRet;
                }
                PacketUtilities.ReadFromStream(oStream, ref bTemp);								    // Sub Command (1 byte)
                if (bTemp != (byte)ECMUpdateSubCmd.AddToUpdateList && bTemp != (byte)ECMUpdateSubCmd.ClientUpdate && bTemp != (byte)ECMUpdateSubCmd.RemoveFromUpdateList)
                {
                    sErrMsg = "Error decoding : Packet sub command is not valid.";
                    return bRet;
                }
                bSubCmd = (ECMUpdateSubCmd)bTemp;
                PacketUtilities.ReadFromStream(oStream, ref this.iPacketVersion);				    // Packet Version (2 byte int)

                if (bSubCmd == ECMUpdateSubCmd.AddToUpdateList || bSubCmd == ECMUpdateSubCmd.RemoveFromUpdateList)
                {
                    PacketUtilities.ReadFromStream(oStream, ref this.iFleetID);					    // Fleet ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref this.iVehicleID);				    // Vehicle ID (4 byte int)
                }
                else if (bSubCmd == ECMUpdateSubCmd.ClientUpdate)
                {
                    PacketUtilities.ReadFromStream(oStream, ref this.iFleetID);					    // Fleet ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref this.iVehicleID);				    // Vehicle ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref dValue);
                    this.dtDeviceTime = DateTime.FromOADate(dValue);							    // Device Time (8 byte double)

                    PacketUtilities.ReadFromStream(oStream, bSep, ref Location);                    // The location at the report time (var string)
                    PacketUtilities.ReadFromStream(oStream, ref Latitude);                          // Latitude (4 byte float)
                    PacketUtilities.ReadFromStream(oStream, ref Longitude);                         // Longitude (4 byte float)
                    PacketUtilities.ReadFromStream(oStream, ref bTemp);								// Number of ECM Values (1 byte)
                    if ((int)bTemp > 0)
                    {
                        _ECMValues = new List<ECMUpdateItemData>();
                        for(int X = 0; X < (int) bTemp; X++)
                            _ECMValues.Add(new ECMUpdateItemData(oStream));
                    }
                    else
                    {
                        _ECMValues = new List<ECMUpdateItemData>();
                    }
                }

                bRet = true;
            }
            catch (System.Exception ex)
            {
                sErrMsg = "Error decoding : " + ex.Message;
                bRet = false;
            }
            return bRet;
        }
        public bool Decode(byte[] bData, ref string sErrMsg)
        {
            return DecodeFromStream(bData, ref sErrMsg);
        }
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                if (bSubCmd == ECMUpdateSubCmd.AddToUpdateList || bSubCmd == ECMUpdateSubCmd.RemoveFromUpdateList)
                {
                    if (bSubCmd == ECMUpdateSubCmd.AddToUpdateList)
                        builder.Append("\r\nData Persist AddToUpdateList Packet :");
                    else
                        builder.Append("\r\nData Persist RemoveFromUpdateList Packet :");
                    builder.Append("\r\n    Version : "); builder.Append(iPacketVersion);
                    builder.Append("\r\n    Fleet ID : "); builder.Append(iFleetID);
                    builder.Append("\r\n    Vehicle ID : "); builder.Append(iVehicleID);
                }
                else if (bSubCmd == ECMUpdateSubCmd.ClientUpdate)
                {
                    builder.Append("\r\nData Persist ClientUpdate Packet :");
                    builder.Append("\r\n    Version : "); builder.Append(iPacketVersion);
                    builder.Append("\r\n    Fleet ID : "); builder.Append(iFleetID);
                    builder.Append("\r\n    Vehicle ID : "); builder.Append(iVehicleID);
                    builder.Append("\r\n    Device Time : "); builder.Append(dtDeviceTime.ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    Locaion : "); builder.Append(Location);
                    builder.Append("\r\n    Latitude : "); builder.Append(Latitude);
                    builder.Append("\r\n    Longitude : "); builder.Append(Longitude);
                    builder.Append("\r\n    ECM Values : ");
                    for (int X = 0; X < _ECMValues.Count; X++)
                        builder.Append(_ECMValues[X].ToDisplayString());

                }
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Data Persist Update Packet : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

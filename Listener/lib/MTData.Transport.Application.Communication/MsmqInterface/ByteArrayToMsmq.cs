﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using log4net;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class ByteArrayToMsmq : MsmqListManager, System.Configuration.IConfigurationSectionHandler
    {        
        #region Private Vars
        private static ILog _log = LogManager.GetLogger(typeof(ByteArrayToMsmq));
        #endregion
        #region Public Properties
        public new Dictionary<int, byte[]> PeekAsyncData
        {
            get
            {
                Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
                Dictionary<int, byte[]> queueData;
                queueData = base.PeekAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, queueData[key]);
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, byte[]> ReadAsyncData
        {
            get
            {
                Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
                Dictionary<int, byte[]> queueData;
                queueData = base.ReadAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, queueData[key]);
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, byte[]> ReadFromQueues
        {
            get
            {
                Dictionary<int, byte[]> ret = null;
                try
                {
                    ret = new Dictionary<int, byte[]>();
                    MsmqAsyncItem[] items = base.ReadFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, (byte[])item.Payload);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ReadFromQueues", ex);
                }
                return ret;
            }
        }
        public Dictionary<int, byte[]> PeekQueues
        {
            get
            {
                Dictionary<int, byte[]> ret = null;
                try
                {
                    ret = new Dictionary<int, byte[]>();
                    MsmqAsyncItem[] items = base.PeekFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, (byte[])item.Payload);
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".PeekQueues", ex);
                }
                return ret;
            }
        }
        #endregion
        public ByteArrayToMsmq()
        {
            
        }
        #region Insert Methods
        public new void InsertIntoQueuesAsync(byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                base.InsertIntoQueuesAsync(lupdate, fleetId, vehicleId, driverId, reasonId);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }
        public new void InsertIntoQueueAsync(int index, byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                base.InsertIntoQueueAsync(index, lupdate, fleetId, vehicleId, driverId, reasonId);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int index, byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }
        public new void InsertIntoQueues(byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                base.InsertIntoQueues(lupdate, fleetId, vehicleId, driverId, reasonId);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueues(byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }
        public new void InsertIntoQueue(int index, byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                base.InsertIntoQueues(lupdate, fleetId, vehicleId, driverId, reasonId);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int index, byte[] lupdate, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }

        #endregion

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<ByteArrayToMsmq>
        /// 		<MSMQ name="localhost\private$\MTData.Listener.LiveUpdates"
        /// 			queueIsRecoverable="false"
        /// 			maxQueueSize="10000"
        /// 			pauseOnFailedMsmqInsert="2000"
        /// 			logWrites="false"
        /// 			logStats="false">
        /// 		</MSMQ>
        /// 	</ByteArrayToMsmq>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("MSMQ");
                base.CreateQueueInterfaces(list, typeof(byte[]));
                bool startReadThread = false;
                bool startWriteThread = false;
                for (int X = 0; X < base.Count; X++)
                {
                    if (base[X].IsReadQueue)
                        startReadThread = true;
                    if (base[X].IsWriteQueue)
                        startWriteThread = true;
                }
                if (startReadThread)
                    StartAsyncRecvThread();
                if (startWriteThread)
                    StartAsyncSendThread();
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }
        #endregion

    }
}

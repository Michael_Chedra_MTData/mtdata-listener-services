using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Xml;
using log4net;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class SerializedToMsmq : MsmqListManager, System.Configuration.IConfigurationSectionHandler
    {        
        #region Private Vars
        private static ILog _log = LogManager.GetLogger(typeof(SerializedToMsmq));
        #endregion
        #region Public Properties
        public new Dictionary<int, object> PeekAsyncData
        {
            get
            {
                Dictionary<int, object> result = new Dictionary<int, object>();
                Dictionary<int, byte[]> queueData;
                queueData = base.PeekAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToObject(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, object> ReadAsyncData
        {
            get
            {
                Dictionary<int, object> result = new Dictionary<int, object>();
                Dictionary<int, byte[]> queueData;
                queueData = base.ReadAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToObject(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, object> ReadFromQueues
        {
            get
            {
                Dictionary<int, object> ret = null;
                try
                {
                    ret = new Dictionary<int, object>();
                    MsmqAsyncItem[] items = base.ReadFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToObject((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ReadFromQueues", ex);
                }
                return ret;
            }
        }
        public Dictionary<int, object> PeekQueues
        {
            get
            {
                Dictionary<int, object> ret = null;
                try
                {
                    ret = new Dictionary<int, object>();
                    MsmqAsyncItem[] items = base.PeekFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToObject((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".PeekQueues", ex);
                }
                return ret;
            }
        }
        #endregion
        public SerializedToMsmq()
        {
            
        }
        #region Insert Methods
        public new void InsertIntoQueuesAsync(object lupdate)
        {
            try
            {
                base.InsertIntoQueuesAsync(lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(object lupdate)", ex);
            }
        }
        public new void InsertIntoQueueAsync(int index, object lupdate)
        {
            try
            {
                base.InsertIntoQueueAsync(index, lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int index, object lupdate)", ex);
            }
        }
        public new void InsertIntoQueues(object lupdate)
        {
            try
            {
                base.InsertIntoQueues(lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueues(object lupdate)", ex);
                throw new Exception("Failed to insert object", ex);
            }
        }
        public void InsertIntoQueue(int index, object lupdate)
        {
            try
            {
                base.InsertIntoQueues(lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int index, object lupdate)", ex);
            }
        }
        #endregion
        private object ConvertByteArrayToObject(byte[] bData)
        {
            object lupdate = null;
            try
            {
                BinaryFormatter formatter = new BinaryFormatter();
                MemoryStream stream = new MemoryStream(bData, 0, bData.Length);

                lupdate = formatter.Deserialize(stream);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ConvertByteArrayToobject(byte[] bData)", ex);
            }
            return lupdate;
        }
        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<PositionUpdateToMSMQ>
        /// 		<MSMQ name="localhost\private$\MTData.Listener.objects"
        /// 			queueIsRecoverable="false"
        /// 			maxQueueSize="10000"
        /// 			pauseOnFailedMsmqInsert="2000"
        /// 			logWrites="false"
        /// 			logStats="false">
        /// 		</MSMQ>
        /// 	</PositionUpdateToMSMQ>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                if (section.Attributes.GetNamedItem("Enabled") != null && section.Attributes.GetNamedItem("Enabled").Value.ToUpper().Trim() == "FALSE")
                {
                    return null;
                }
                else
                {
                    XmlNodeList list = section.SelectNodes("MSMQ");
                    base.CreateQueueInterfaces(list, typeof(object));
                    bool startReadThread = false;
                    bool startWriteThread = false;
                    for (int X = 0; X < base.Count; X++)
                    {
                        if (base[X].IsReadQueue)
                            startReadThread = true;
                        if (base[X].IsWriteQueue)
                            startWriteThread = true;
                    }
                    if (startReadThread)
                        StartAsyncRecvThread();
                    if (startWriteThread)
                        StartAsyncSendThread();
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }
        #endregion

    }
}

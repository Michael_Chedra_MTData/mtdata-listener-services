using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class MsmqAsyncItem
    {
        public int Index;
        public object Payload;
        public int FleetId;
        public int VehicleId;
        public int DriverId;
        public int ReasonId;
        public MsmqAsyncItem(int index, object payload)
        {
            Index = index;
            Payload = payload;
        }
        public MsmqAsyncItem(int index, object payload, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            Index = index;
            Payload = payload;
            FleetId = fleetId;
            VehicleId = vehicleId;
            DriverId = driverId;
            ReasonId = reasonId;
        }
    }
}

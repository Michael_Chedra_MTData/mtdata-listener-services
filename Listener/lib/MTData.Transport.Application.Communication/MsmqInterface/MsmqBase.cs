using System;
using System.Collections.Generic;
using System.IO;
using System.Messaging;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Threading.Tasks;
using log4net;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class MsmqBase
    {
        #region Variables
        private Type _queueItemType;
        private static ILog _log = LogManager.GetLogger(typeof(MsmqBase));
        private string _queueName;
        private bool _isHTTPConnection;
        private MessageQueue _queue;
        private static object _syncRoot = new object();
        private int _queueCount;
        private int _maxQueueSize;
        private bool _isRecoverable;
        private int _dequeueNumOfMsgsOnOverflow;
        private int _pauseOnFailedMsmqInsert = 2000;
        private bool _logReads;
        private bool _logPeeks;
        private bool _logWrites;
        private bool _logStats;
        private bool _isWriteInstance = true;
        private bool _isReadInstance;
        private List<int> _fleetIdFilter;
        private List<ulong> _vehicleIdFilter;
        private List<ulong> _driverIdFilter;
        private List<int> _reasonCodeIdFilter;
        private List<int> _reasonCodeIdIgnoreFilter;
        private Dictionary<int, List<int>> _spnFilter;
        private bool inPeekErrorState = false;
        #region Stats
        private ulong _peeks = 0;
        private ulong _peeksTimeouts = 0;
        private ulong _writes = 0;
        private ulong _reads = 0;
        private ulong _writeTimeouts = 0;
        private ulong _readTimeouts = 0;
        private ulong _queueOverflows = 0;
        private ulong _overflowMessagesDitched = 0;
        private Task queueCountTask;
        private object queueCountSyncRoot = new object();
        private object queueCountTaskCreationLock = new object();
        private bool stopQueueCountTask = false;
        #endregion

        private object _statsSync = new object();
        private object _fleetIdFilterSync = new object();
        private object _vehicleIdFilterSync = new object();
        private object _driverIdFilterSync = new object();
        private object _reasonCodeIdFilterSync = new object();
        private object _reasonCodeIdIgnoreFilterSync = new object();
        private object _spnFilterSync = new object();

        #endregion
        #region Properties
        public Type QueueItemType
        {
            get { return _queueItemType; }
            set { _queueItemType = value; }
        }
        public MessageQueue Q
        {
            get { return this._queue; }
        }
        public string Name
        {
            get { return this._queueName; }
        }
        public int Count
        {
            get
            {
                lock (queueCountSyncRoot)
                {
                    return _queueCount;
                }
            }
        }
        public int PauseOnFailedMsmqInsert
        {
            get { return _pauseOnFailedMsmqInsert; }
            set { _pauseOnFailedMsmqInsert = value; }
        }
        public bool IsRecoverable
        {
            get { return _isRecoverable; }
            set { _isRecoverable = value; }
        }
        public int DequeueNumOfMsgsOnOverflow
        {
            get { return _dequeueNumOfMsgsOnOverflow; }
            set { _dequeueNumOfMsgsOnOverflow = value; }
        }
        public bool LogStats
        {
            get { return _logStats; }
            set { _logStats = value; }
        }
        public bool LogReads
        {
            get { return _logReads; }
            set { _logReads = value; }
        }
        public bool LogPeeks
        {
            get { return _logPeeks; }
            set { _logPeeks = value; }
        }
        public bool LogWrites
        {
            get { return _logWrites; }
            set { _logWrites = value; }
        }
        public bool IsReadQueue
        {
            get { return _isReadInstance; }
            set { _isReadInstance = value; }
        }
        public bool IsWriteQueue
        {
            get { return _isWriteInstance; }
            set { _isWriteInstance = value; }
        }
        public int[] FleetIdFilter
        {
            get
            {
                int[] ret = new int[0];
                if (_fleetIdFilter != null)
                {
                    lock (_fleetIdFilterSync)
                    {
                        ret = new int[_fleetIdFilter.Count];
                        _fleetIdFilter.CopyTo(ret);
                    }
                }
                return ret;
            }
            set
            {
                lock (_fleetIdFilterSync)
                {
                    _fleetIdFilter = new List<int>();
                    foreach (int val in value)
                    {
                        if (!_fleetIdFilter.Contains(val))
                            _fleetIdFilter.Add(val);
                    }
                }
            }
        }
        public ulong[] VehicleIdFilter
        {
            get
            {
                ulong[] ret = new ulong[0];
                if (_vehicleIdFilter != null)
                {
                    lock (_vehicleIdFilterSync)
                    {
                        ret = new ulong[_vehicleIdFilter.Count];
                        _vehicleIdFilter.CopyTo(ret);
                    }
                }
                return ret;
            }
            set
            {
                lock (_vehicleIdFilterSync)
                {
                    _vehicleIdFilter = new List<ulong>();
                    foreach (ulong val in value)
                    {
                        if (!_vehicleIdFilter.Contains(val))
                            _vehicleIdFilter.Add(val);
                    }
                }
            }
        }
        public ulong[] DriverIdFilter
        {
            get
            {
                ulong[] ret = new ulong[0];
                if (_driverIdFilter != null)
                {
                    lock (_driverIdFilterSync)
                    {
                        ret = new ulong[_driverIdFilter.Count];
                        _driverIdFilter.CopyTo(ret);
                    }
                }
                return ret;
            }
            set
            {
                lock (_driverIdFilterSync)
                {
                    _driverIdFilter = new List<ulong>();
                    foreach (ulong val in value)
                    {
                        if (!_driverIdFilter.Contains(val))
                            _driverIdFilter.Add(val);
                    }
                }
            }
        }
        public int[] ReasonCodeIdFilter
        {
            get
            {
                int[] ret = new int[0];
                if (_reasonCodeIdFilter != null)
                {
                    lock (_reasonCodeIdFilterSync)
                    {
                        ret = new int[_reasonCodeIdFilter.Count];
                        _reasonCodeIdFilter.CopyTo(ret);
                    }
                }
                return ret;
            }
            set
            {
                lock (_reasonCodeIdFilterSync)
                {
                    _reasonCodeIdFilter = new List<int>();
                    foreach (int val in value)
                    {
                        if (!_reasonCodeIdFilter.Contains(val))
                            _reasonCodeIdFilter.Add(val);
                    }
                }
            }
        }
        public int[] ReasonCodeIdIgnoreFilter
        {
            get
            {
                int[] ret = new int[0];
                if (_reasonCodeIdIgnoreFilter != null)
                {
                    lock (_reasonCodeIdIgnoreFilterSync)
                    {
                        ret = new int[_reasonCodeIdIgnoreFilter.Count];
                        _reasonCodeIdIgnoreFilter.CopyTo(ret);
                    }
                }
                return ret;
            }
            set
            {
                lock (_reasonCodeIdIgnoreFilterSync)
                {
                    _reasonCodeIdIgnoreFilter = new List<int>();
                    foreach (int val in value)
                    {
                        if (!_reasonCodeIdIgnoreFilter.Contains(val))
                            _reasonCodeIdIgnoreFilter.Add(val);
                    }
                }
            }
        }
        public Dictionary<int, List<int>> SpnFilter
        {
            get
            {
                Dictionary<int, List<int>> ret = new Dictionary<int, List<int>>();
                if (_spnFilter != null)
                {
                    lock (_spnFilterSync)
                    {
                        int[] keys = new int[_spnFilter.Keys.Count];
                        _spnFilter.Keys.CopyTo(keys, 0);
                        foreach (int key in keys)
                        {
                            List<int> listCopy = new List<int>();
                            foreach (int spnCode in _spnFilter[key])
                            {
                                listCopy.Add(spnCode);
                            }
                            ret.Add(key, listCopy);
                        }
                    }
                }
                return ret;
            }
            set
            {
                _spnFilter = new Dictionary<int, List<int>>();
                if (value != null)
                {
                    int[] keys = new int[value.Keys.Count];
                    value.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        List<int> listCopy = new List<int>();
                        foreach (int spnCode in value[key])
                        {
                            listCopy.Add(spnCode);
                        }
                        _spnFilter.Add(key, listCopy);
                    }
                }
            }
        }
        #region Stats
        public ulong PeekCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _peeks;
                    _peeks = 0;
                }
                return ret;
            }
        }
        public ulong PeekTimeoutCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _peeksTimeouts;
                    _peeksTimeouts = 0;
                }
                return ret;
            }
        }
        public ulong EnqueueCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _writes;
                    _writes = 0;
                }
                return ret;
            }
        }
        public ulong DequeueCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _reads;
                    _reads = 0;
                }
                return ret;
            }
        }
        public ulong WriteTimeoutCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _writeTimeouts;
                    _writeTimeouts = 0;
                }
                return ret;
            }
        }
        public ulong ReadTimeoutCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _readTimeouts;
                    _readTimeouts = 0;
                }
                return ret;
            }
        }
        public ulong QueueOverflowCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _queueOverflows;
                    _queueOverflows = 0;
                }
                return ret;
            }
        }
        public ulong OverflowMessagesDitchedCount
        {
            get
            {
                ulong ret = 0;
                lock (_statsSync)
                {
                    ret = _overflowMessagesDitched;
                    _overflowMessagesDitched = 0;
                }
                return ret;
            }
        }
        #endregion
        #endregion
        #region Constructors
        public MsmqBase()
        {
            _dequeueNumOfMsgsOnOverflow = 0;
            _isRecoverable = false;
        }
        public MsmqBase(string name, int maxQueueSize, bool isRecoverableQueue, int dequeueNumOfMsgsOnOverflow)
        {
            try
            {
                _isRecoverable = isRecoverableQueue;
                _dequeueNumOfMsgsOnOverflow = dequeueNumOfMsgsOnOverflow;
                CreateQueue(name, maxQueueSize, new System.Messaging.BinaryMessageFormatter());
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".MsmqBase(string name, int maxQueueSize, bool isRecoverableQueue)", ex);
            }
        }
        public void Dispose()
        {
            lock (queueCountTaskCreationLock)
            {
                stopQueueCountTask = true;
                queueCountTask?.Wait(1000);
            }

            lock (_syncRoot)
            {
                if (_queue != null)
                {
                    _queue.Close();
                }
                _queue = null;
                if (_fleetIdFilter != null)
                {
                    lock (_spnFilterSync)
                    {
                        _fleetIdFilter.Clear();
                        _fleetIdFilter = null;
                    }
                }

                if (_vehicleIdFilter != null)
                {
                    lock (_vehicleIdFilterSync)
                    {
                        _vehicleIdFilter.Clear();
                        _vehicleIdFilter = null;
                    }
                }

                if (_driverIdFilter != null)
                {
                    lock (_driverIdFilterSync)
                    {
                        _driverIdFilter.Clear();
                        _driverIdFilter = null;
                    }
                }

                if (_spnFilter != null)
                {
                    lock (_spnFilterSync)
                    {
                        _spnFilter.Clear();
                        _spnFilter = null;
                    }
                }
            }
        }
        #endregion
        #region Public Methods
        public ulong GetKey(string sfleetId, string svehicleIdOrdriverId)
        {
            ulong key = 0;
            if (sfleetId != "" && svehicleIdOrdriverId != "")
            {
                try
                {
                    int fleetId = Convert.ToInt32(sfleetId);
                    int vehicleId = Convert.ToInt32(svehicleIdOrdriverId);
                    if (fleetId > 0 && vehicleId > 0)
                    {
                        key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(svehicleIdOrdriverId);
                    }
                }
                catch (Exception)
                {
                    key = 0;
                }
            }
            return key;
        }
        public ulong GetKey(int fleetId, int vehicleIdOrdriverId)
        {
            ulong key = 0;
            try
            {
                if (fleetId > 0 && vehicleIdOrdriverId > 0)
                {
                    key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(vehicleIdOrdriverId);
                }
            }
            catch (Exception)
            {
                key = 0;
            }
            return key;
        }
        #region Check Filters
        public bool HasFleetIdInFilter(int fleetId)
        {
            try
            {
                if (_fleetIdFilter != null)
                {
                    bool bRet = false;
                    lock (_fleetIdFilterSync)
                        bRet = _fleetIdFilter.Contains(fleetId);
                    return bRet;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".HasFleetIdInFilter(int fleetId)", ex);
            }
            return true;
        }
        public bool HasVehicleIdInFilter(int fleetId, int vehicleId)
        {
            try
            {
                ulong key = GetKey(fleetId, vehicleId);
                if (_vehicleIdFilter != null)
                {
                    bool bRet = false;
                    lock (_vehicleIdFilterSync)
                        bRet = _vehicleIdFilter.Contains(key);
                    return bRet;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".HasVehicleIdInFilter(int fleetId, int vehicleId)", ex);
            }
            return true;
        }
        public bool HasDriverIdInFilter(int fleetId, int driverId)
        {
            try
            {
                ulong key = GetKey(fleetId, driverId);
                if (_driverIdFilter != null)
                {
                    bool bRet = false;
                    lock (_driverIdFilterSync)
                        bRet = _driverIdFilter.Contains(key);
                    return bRet;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".HasDriverIdInFilter(int driverId)", ex);
            }
            return true;
        }
        public bool HasReasonCodeIdInFilter(int reasonCodeId)
        {
            try
            {
                if (_reasonCodeIdFilter != null)
                {
                    bool bRet = false;
                    lock (_reasonCodeIdFilterSync)
                        bRet = _reasonCodeIdFilter.Contains(reasonCodeId);
                    return bRet;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".HasReasonCodeIdInFilter(int reasonCodeId)", ex);
            }
            return true;
        }
        public bool HasReasonCodeIdIgnoreInFilter(int reasonCodeId)
        {
            try
            {
                if (_reasonCodeIdIgnoreFilter != null)
                {
                    bool bRet = false;
                    lock (_reasonCodeIdIgnoreFilterSync)
                        bRet = _reasonCodeIdIgnoreFilter.Contains(reasonCodeId);
                    return bRet;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".HasReasonCodeIdIgnoreInFilter(int reasonCodeId)", ex);
            }
            return true;
        }
        public bool HasSpnInFilter(int sourceType, int spnCode)
        {
            try
            {
                return (SpnFilter.ContainsKey(sourceType) && SpnFilter[sourceType].Contains(spnCode));
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".HasSpnInFilter(int sourceType, int spnCode)", ex);
            }
            return false;
        }
        #endregion
        public bool InsertMessageToQueue(object message, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                return InsertMessageToQueue(message, fleetId, vehicleId, driverId, reasonId, null, System.Messaging.MessagePriority.Normal, _isRecoverable);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertMessageToQueue(object message, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
            return false;
        }
        public bool InsertMessageToQueue(LiveUpdate lupdate)
        {
            try
            {
                string err = "";
                return InsertMessageToQueue(lupdate.Encode(ref err), lupdate.iFleetID, lupdate.iVehicleID, -1, lupdate.iReasonID, null, System.Messaging.MessagePriority.Normal, _isRecoverable);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertMessageToQueue(LiveUpdate lupdate)", ex);
            }
            return false;
        }
        public bool InsertMessageToQueue(ListenerLogItem lupdate)
        {
            try
            {
                string err = "";
                return InsertMessageToQueue(lupdate.Encode(ref err), lupdate.FleetID, lupdate.VehicleID, -1, -1, null, System.Messaging.MessagePriority.Normal, _isRecoverable);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertMessageToQueue(LiveUpdate lupdate)", ex);
            }
            return false;
        }
        public bool InsertMessageToQueue(ECMUpdate lupdate)
        {
            try
            {
                string err = "";
                return InsertMessageToQueue(lupdate.Encode(ref err), lupdate.iFleetID, lupdate.iVehicleID, -1, -1, null, System.Messaging.MessagePriority.Normal, _isRecoverable);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertMessageToQueue(ECMUpdate lupdate)", ex);
            }
            return false;
        }
        public bool InsertMessageToQueue(object lupdate)
        {
            try
            {
                MemoryStream mstream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(mstream, lupdate);
                byte[] bData = mstream.ToArray();

                return InsertMessageToQueue(bData, -1, -1, -1, -1, null, System.Messaging.MessagePriority.Normal, _isRecoverable);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertMessageToQueue(ECMUpdate lupdate)", ex);
            }
            return false;
        }
        public bool InsertMessageToQueue(object message, int fleetId, int vehicleId, int driverId, int reasonId, ECMUpdate spnValues, System.Messaging.MessagePriority priority, bool setMessageAsRecoverable)
        {
            bool fRet = false;
            try
            {
                #region Apply the filters
                if (fleetId > 0 && _fleetIdFilter != null)
                    lock (_fleetIdFilterSync)
                        if (!_fleetIdFilter.Contains(fleetId))
                            return true;

                if (vehicleId > 0 && _vehicleIdFilter != null)
                    lock (_vehicleIdFilterSync)
                    {
                        ulong key = this.GetKey(fleetId, vehicleId);
                        if (!_vehicleIdFilter.Contains(key))
                            return true;
                    }

                if (driverId > 0 && _driverIdFilter != null)
                    lock (_driverIdFilterSync)
                    {
                        ulong key = this.GetKey(fleetId, driverId);
                        if (!_driverIdFilter.Contains(key))
                            return true;
                    }

                if (reasonId > 0 && _reasonCodeIdFilter != null)
                    lock (_reasonCodeIdFilterSync)
                        if (!_reasonCodeIdFilter.Contains(reasonId))
                            return true;

                if (reasonId > 0 && _reasonCodeIdIgnoreFilter != null)
                    lock (_reasonCodeIdIgnoreFilterSync)
                        if (_reasonCodeIdIgnoreFilter.Contains(reasonId))
                            return true;

                if (spnValues != null && _spnFilter != null)
                {
                    // Add only the values in the filter to the update that goes on the queue
                    ECMUpdate oUpdate = new ECMUpdate(spnValues.iFleetID, spnValues.iVehicleID, spnValues.dtDeviceTime, spnValues.Location, spnValues.Latitude, spnValues.Longitude);
                    foreach (ECMUpdateItemData ecmData in spnValues._ECMValues)
                    {
                        lock (_spnFilterSync)
                        {
                            if (_spnFilter.ContainsKey(ecmData.SourceType) && _spnFilter[ecmData.SourceType].Contains(ecmData.SPN_ID))
                                oUpdate.AddECMValue(ecmData.SPN_ID, ecmData.SourceType, ecmData.Flags, ecmData.SPN_Data);
                        }
                    }
                    if (oUpdate._ECMValues.Count > 0)
                    {
                        string err = "";
                        message = oUpdate.Encode(ref err);
                        if (message == null)
                            return true;
                    }
                    else
                        return true;

                }

                if (message == null)
                    return true;
                #endregion
                System.Messaging.Message msg = new System.Messaging.Message(message, this._queue.Formatter);
                msg.Priority = priority;
                msg.Recoverable = setMessageAsRecoverable;
                bool queueMessage = false;
                if (Count > 0 && _maxQueueSize > 0)
                {
                    if (Count < _maxQueueSize)
                        queueMessage = true;
                    else
                    {
                        if (_logStats)
                        {
                            lock (_statsSync)
                            {
                                if (_queueOverflows == ulong.MaxValue)
                                    _queueOverflows = 0;
                                _queueOverflows++;
                            }
                        }
                        object ditch = null;
                        if (_dequeueNumOfMsgsOnOverflow > 0)
                        {
                            int dequeueCount = _dequeueNumOfMsgsOnOverflow;
                            while (dequeueCount > 0)
                            {
                                ditch = this.ReadBody();
                                if (_logStats)
                                {
                                    lock (_statsSync)
                                    {
                                        if (_overflowMessagesDitched == ulong.MaxValue)
                                            _overflowMessagesDitched = 0;
                                        _overflowMessagesDitched++;
                                    }
                                }
                                dequeueCount--;
                            }
                        }
                        queueMessage = true;
                    }
                }

                if (_maxQueueSize <= 0 || queueMessage || Count < _maxQueueSize)
                {
                    try
                    {
                        lock (_syncRoot)
                        {
                            this._queue.Send(msg);
                        }
                        if (_logWrites)
                            _log.Info("MSMQ " + _queueName + " Enqueued " + BitConverter.ToString((byte[])msg.Body));
                        if (_logStats)
                        {
                            lock (_statsSync)
                            {
                                if (_writes == ulong.MaxValue)
                                    _writes = 0;
                                _writes++;
                            }
                        }
                        IncreaseQueueCount();
                    }
                    catch (Exception exEnqueue)
                    {
                        _log.Error(this.GetType().FullName + ".InsertMessageToQueue Enqueue Error - Pausing for " + _pauseOnFailedMsmqInsert + " milliseconds.", exEnqueue);
                        if (_logStats)
                        {
                            lock (_statsSync)
                            {
                                if (_writeTimeouts == ulong.MaxValue)
                                    _writeTimeouts = 0;
                                _writeTimeouts++;
                            }
                        }
                        if (_pauseOnFailedMsmqInsert > 0)
                        {
                            int timer = _pauseOnFailedMsmqInsert;
                            while (timer > 0)
                            {
                                Thread.Sleep(100);
                                timer = timer - 100;
                            }
                        }
                    }
                    fRet = true;
                }
                else
                {
                    if (_logWrites)
                    {
                        _log.Info("MSMQ " + _queueName + " has more than the limit of " + _maxQueueSize + " messages.  Data NOT enqueued : " + BitConverter.ToString((byte[])msg.Body));
                    }
                    if (_logStats)
                    {
                        lock (_statsSync)
                        {
                            if (_queueOverflows == ulong.MaxValue)
                                _queueOverflows = 0;
                            _queueOverflows++;
                        }
                    }
                    fRet = false;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertMessageToQueue(object message, int fleetId, int vehicleId, int driverId, int reasonId, List<ECMUpdateItemData> spnValues, System.Messaging.MessagePriority priority, bool setMessageAsRecoverable)", ex);
            }
            return fRet;
        }
        public object PeekBody()
        {
            object oRet = null;
            System.Messaging.Message message = null;
            try
            {
                message = _queue.Peek(new TimeSpan(0, 0, 0, 0, 100));
                if (inPeekErrorState)
                {
                    _log.Info("MSMQ " + _queueName + " is live again after Peek error state.");
                    inPeekErrorState = false;
                }

                if (_logStats)
                {
                    if (_logReads && message != null)
                        _log.Info("MSMQ " + _queueName + " Peek " + BitConverter.ToString((byte[]) message.Body));
                    lock (_statsSync)
                    {
                        if (_peeks == ulong.MaxValue)
                            _peeks = 0;
                        _peeks++;
                    }
                }
            }
            catch (Exception)
            {
                if (inPeekErrorState == false)
                {
                    _log.Error("MSMQ " + _queueName + " in Peek error state.");
                    _queue.Close();
                    inPeekErrorState = true;
                }

                if (_logStats)
                {
                    lock (_statsSync)
                    {
                        if (_peeksTimeouts == ulong.MaxValue)
                            _peeksTimeouts = 0;
                        _peeksTimeouts++;
                    }
                }

                message = null;
            }

            if (message != null)
                oRet = message.Body;
            return oRet;
        }
        public object ReadBody()
        {
            object oRet = null;
            System.Messaging.Message message = null;

            try
            {
                message = _queue.Receive(new TimeSpan(0, 0, 0, 0, 100));
                DecreaseQueueCount(); 
                if (_logStats)
                {
                    lock (_statsSync)
                    {
                        if (_logReads && message != null)
                            _log.Info("MSMQ " + _queueName + " Read " + BitConverter.ToString((byte[])message.Body));
                        if (_reads == ulong.MaxValue)
                            _reads = 0;
                        _reads++;
                    }
                }
            }
            catch (System.Exception)
            {
                if (_logStats)
                {
                    lock (_statsSync)
                    {
                        if (_readTimeouts == ulong.MaxValue)
                            _readTimeouts = 0;
                        _readTimeouts++;
                    }
                }
                message = null;
            }
            if (message != null)
                oRet = message.Body;

            return oRet;
        }
        public void Delete()
        {
            if (this._queueName.Length <= 0)
                throw new ApplicationException("Must specify a MSMQ Queue name to delete.");
            else
            {
                try
                {
                    lock (_syncRoot)
                    {
                        System.Messaging.MessageQueue.Delete(this._queueName);
                    }
                    lock (_statsSync)
                    {
                        _peeks = 0;
                        _peeksTimeouts = 0;
                        _writes = 0;
                        _reads = 0;
                        _writeTimeouts = 0;
                        _readTimeouts = 0;
                        _queueOverflows = 0;
                        _overflowMessagesDitched = 0;
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".Delete()", ex);
                }
            }
        }
        public void ResetStats()
        {
            try
            {
                lock (_statsSync)
                {
                    _peeks = 0;
                    _peeksTimeouts = 0;
                    _writes = 0;
                    _reads = 0;
                    _writeTimeouts = 0;
                    _readTimeouts = 0;
                    _queueOverflows = 0;
                    _overflowMessagesDitched = 0;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ResetStats()", ex);
            }
        }
        #endregion
        #region Private Methods
        private void CreateQueue(string name, int maxQueueSize, IMessageFormatter formatter)
        {
            try
            {
                this._queueName = name;
                if (this._queueName.ToUpper().StartsWith("HTTP"))
                    _isHTTPConnection = true;
                if (_isHTTPConnection)
                    this._queueName = string.Concat("FormatName:direct=", this._queueName);
                else
                {
                    if (!this._queueName.StartsWith("FormatName:direct=OS:"))
                        this._queueName = string.Concat("FormatName:direct=OS:", this._queueName);
                }
                lock (_syncRoot)
                {
                    this._queue = new MessageQueue(this._queueName);
                    this._queue.Formatter = formatter;
                    this._queue.MessageReadPropertyFilter.SetAll();
                }
                _maxQueueSize = maxQueueSize;
                try
                {
                    DoCount();
                }
                catch { }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".CreateQueue(string name, int maxQueueSize, IMessageFormatter formatter)", ex);
            }
        }

        /// <summary>
        /// Count the number of message in the queue and set the Count property
        /// </summary>
        private void DoCount()
        {
            var enumerator = _queue.GetMessageEnumerator2();
            long counter = 0;
            while (enumerator.MoveNext())
            {
                if (stopQueueCountTask)
                {
                    return;
                }

                counter++;
            }

            lock (queueCountSyncRoot)
            {
                _queueCount = (int)counter;
            }
        }

        /// <summary>
        /// Reset the queue count task.
        /// </summary>
        private void ResetQueueCountTask()
        {
            try
            {
                DoCount();
            }
            catch
            {
                //can throw an exception in MoveNext() when we are doing a count
                //try one more time and if it still fails just ignore it
                try
                {
                    DoCount();
                }
                catch { }
            }
        }


        /// <summary>
        /// Resets the queue count.
        /// </summary>
        private void ResetQueueCount()
        {
            // should not be counting when in error state.
            if (inPeekErrorState)
            {
                return;
            }

            //if HTTP message Queue - can't read so reset queue count to be zero
            if (_isHTTPConnection)
            {
                lock (queueCountSyncRoot)
                {
                    _queueCount = 0;
                    return;
                }
            }

            lock (queueCountTaskCreationLock)
            {
                if (stopQueueCountTask)
                {
                    // if disposing do not run the task
                    return;
                }

                if (queueCountTask != null)
                {
                    if (queueCountTask.Status == TaskStatus.Created ||
                        queueCountTask.Status == TaskStatus.WaitingForActivation ||
                        queueCountTask.Status == TaskStatus.WaitingToRun ||
                        queueCountTask.Status == TaskStatus.Running)
                    {
                        // we are already processing the count stop counting as the count is changed 
                        // by 1000 since then, so set the task stop flag
                        stopQueueCountTask = true;

                        // wait for the task to stop up to maximum of 1000 milliseconds 
                        // if it is not stopped then just ignore.
                        queueCountTask.Wait(1000);
                    }
                }

                stopQueueCountTask = false;
                queueCountTask = Task.Factory.StartNew(ResetQueueCountTask);
            }
        }

        private void IncreaseQueueCount()
        {
            lock (queueCountSyncRoot)
            {
                _queueCount++;
            }

            // reset queue count every 1000 records
            if (Count % 1000 == 0)
            {
                ResetQueueCount();
            }
        }
        private void DecreaseQueueCount()
        {
            lock (queueCountSyncRoot)
            {
                _queueCount--;
                if (_queueCount < 0)
                {
                    _queueCount = 0;
                }

            }

            //reset queue count every 1000 records
            if (Count > 0 && Count % 1000 == 0)
            {
                ResetQueueCount();
            }
        }
        #endregion

    }
}

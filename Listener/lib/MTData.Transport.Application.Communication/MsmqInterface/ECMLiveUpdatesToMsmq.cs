using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using log4net;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class ECMLiveUpdatesToMsmq : MsmqListManager, System.Configuration.IConfigurationSectionHandler
    {
        #region Private Vars
        private static ILog _log = LogManager.GetLogger(typeof(ECMLiveUpdatesToMsmq));
        #endregion
        #region Public Properties
        public new Dictionary<int, ECMUpdate> PeekAsyncData
        {
            get
            {
                Dictionary<int, ECMUpdate> result = new Dictionary<int, ECMUpdate>();
                Dictionary<int, byte[]> queueData;
                queueData = base.PeekAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if(!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToECMUpdate(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, ECMUpdate> ReadAsyncData
        {
            get
            {
                Dictionary<int, ECMUpdate> result = new Dictionary<int, ECMUpdate>();
                Dictionary<int, byte[]> queueData;
                queueData = base.ReadAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToECMUpdate(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, ECMUpdate> ReadFromQueues
        {
            get
            {
                Dictionary<int, ECMUpdate> ret = null;
                try
                {
                    ret = new Dictionary<int, ECMUpdate>();
                    MsmqAsyncItem[] items = base.ReadFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToECMUpdate((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ReadFromQueues", ex);
                }
                return ret;
            }
        }
        public Dictionary<int, ECMUpdate> PeekQueues
        {
            get
            {
                Dictionary<int, ECMUpdate> ret = null;
                try
                {
                    ret = new Dictionary<int, ECMUpdate>();
                    MsmqAsyncItem[] items = base.PeekFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToECMUpdate((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".PeekQueues", ex);
                }
                return ret;
            }
        }
        #endregion
        public ECMLiveUpdatesToMsmq()
        {

        }
        private ECMUpdate ConvertByteArrayToECMUpdate(byte[] bData)
        {
            ECMUpdate lupdate = null;
            try
            {
                lupdate = new ECMUpdate();
                string sErr = "";
                bool bRet = lupdate.Decode(bData, ref sErr);
                if (sErr != "")
                    throw (new Exception(sErr));
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ConvertByteArrayToECMUpdate(byte[] bData)", ex);
            }
            return lupdate;
        }
        #region Insert Methods
        public new void InsertIntoQueuesAsync(ECMUpdate spnValues)
        {
            try
            {
                base.InsertIntoQueuesAsync(spnValues);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(ECMUpdate spnValues)", ex);
            }
        }
        public new void InsertIntoQueueAsync(int index, ECMUpdate spnValues)
        {
            try
            {
                base.InsertIntoQueueAsync(index, spnValues);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int index, ECMUpdate spnValues)", ex);
            }
        }
        public new void InsertIntoQueues(ECMUpdate spnValues)
        {
            try
            {
                base.InsertIntoQueues(spnValues);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate spnValues)", ex);
            }
        }
        public new void InsertIntoQueue(int index, ECMUpdate spnValues)
        {
            try
            {
                base.InsertIntoQueues(spnValues);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int index, ECMUpdate spnValues)", ex);
            }
        }
        #endregion
        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<ECMLiveUpdatesToMsmq>
        /// 		<MSMQ name="localhost\private$\MTData.Listener.LiveUpdates"
        /// 			queueIsRecoverable="false"
        /// 			maxQueueSize="10000"
        /// 			pauseOnFailedMsmqInsert="2000"
        /// 			logWrites="false"
        /// 			logStats="false">
        /// 		</MSMQ>
        /// 	</ECMLiveUpdatesToMsmq>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("MSMQ");
                base.CreateQueueInterfaces(list, typeof(ECMUpdate));
                bool startReadThread = false;
                bool startWriteThread = false;
                for(int X = 0; X < base.Count; X++)
                {
                    if(base[X].IsReadQueue)
                        startReadThread = true;
                    if (base[X].IsWriteQueue)
                        startWriteThread = true;
                }
                if(startReadThread)
                    StartAsyncRecvThread();
                if(startWriteThread)
                    StartAsyncSendThread();
            }
            catch (Exception)
            {
            }
            return this;
        }
        #endregion
    }
}

﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using log4net;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class ListenerLogToMsmq : MsmqListManager, IConfigurationSectionHandler
    {
        private static ILog _log = LogManager.GetLogger(typeof(ListenerLogToMsmq));

        #region Public Properties
        public new Dictionary<int, ListenerLogItem> PeekAsyncData
        {
            get
            {
                Dictionary<int, ListenerLogItem> result = new Dictionary<int, ListenerLogItem>();
                Dictionary<int, byte[]> queueData;
                queueData = base.PeekAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if(!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToListenerLogItem(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, ListenerLogItem> ReadAsyncData
        {
            get
            {
                Dictionary<int, ListenerLogItem> result = new Dictionary<int, ListenerLogItem>();
                Dictionary<int, byte[]> queueData;
                queueData = base.ReadAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToListenerLogItem(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, ListenerLogItem> ReadFromQueues
        {
            get
            {
                Dictionary<int, ListenerLogItem> ret = null;
                try
                {
                    ret = new Dictionary<int, ListenerLogItem>();
                    MsmqAsyncItem[] items = base.ReadFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToListenerLogItem((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ReadFromQueues", ex);
                }
                return ret;
            }
        }
        public Dictionary<int, ListenerLogItem> PeekQueues
        {
            get
            {
                Dictionary<int, ListenerLogItem> ret = null;
                try
                {
                    ret = new Dictionary<int, ListenerLogItem>();
                    MsmqAsyncItem[] items = base.PeekFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToListenerLogItem((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".PeekQueues", ex);
                }
                return ret;
            }
        }
        #endregion

        private ListenerLogItem ConvertByteArrayToListenerLogItem(byte[] bData)
        {
            ListenerLogItem lupdate = null;
            try
            {
                lupdate = new ListenerLogItem();
                string sErr = "";
                bool bRet = lupdate.Decode(bData, ref sErr);
                if (sErr != "")
                    throw (new Exception(sErr));
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
            return lupdate;
        }
        #region Insert Methods
        public new void InsertIntoQueuesAsync(ListenerLogItem logItem)
        {
            try
            {
                base.InsertIntoQueuesAsync(logItem);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
        public new void InsertIntoQueueAsync(int index, ListenerLogItem logItem)
        {
            try
            {
                base.InsertIntoQueueAsync(index, logItem);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
        public new void InsertIntoQueues(ListenerLogItem logItem)
        {
            try
            {
                base.InsertIntoQueues(logItem);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
        public new void InsertIntoQueue(int index, ListenerLogItem logItem)
        {
            try
            {
                base.InsertIntoQueues(logItem);
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
        #endregion

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<ECMLiveUpdatesToMsmq>
        /// 		<MSMQ name="localhost\private$\MTData.Listener.LiveUpdates"
        /// 			queueIsRecoverable="false"
        /// 			maxQueueSize="10000"
        /// 			pauseOnFailedMsmqInsert="2000"
        /// 			logWrites="false"
        /// 			logStats="false">
        /// 		</MSMQ>
        /// 	</ECMLiveUpdatesToMsmq>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("MSMQ");
                base.CreateQueueInterfaces(list, typeof(ListenerLogItem));
                bool startReadThread = false;
                bool startWriteThread = false;
                for (int X = 0; X < base.Count; X++)
                {
                    if (base[X].IsReadQueue)
                        startReadThread = true;
                    if (base[X].IsWriteQueue)
                        startWriteThread = true;
                }
                if (startReadThread)
                    StartAsyncRecvThread();
                if (startWriteThread)
                    StartAsyncSendThread();
            }
            catch (Exception)
            {
            }
            return this;
        }
        #endregion
    }
}

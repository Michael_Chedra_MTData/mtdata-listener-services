using System;
using System.Collections.Generic;
using System.IO;
using System.Threading;
using System.Xml;
using log4net;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class LiveUpdatesToMsmq : MsmqListManager, System.Configuration.IConfigurationSectionHandler
    {        
        #region Private Vars
        private static ILog _log = LogManager.GetLogger(typeof(LiveUpdatesToMsmq));
        #endregion
        #region Public Properties
        public new Dictionary<int, LiveUpdate> PeekAsyncData
        {
            get
            {
                Dictionary<int, LiveUpdate> result = new Dictionary<int, LiveUpdate>();
                Dictionary<int, byte[]> queueData;
                queueData = base.PeekAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToLiveUpdate(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, LiveUpdate> ReadAsyncData
        {
            get
            {
                Dictionary<int, LiveUpdate> result = new Dictionary<int, LiveUpdate>();
                Dictionary<int, byte[]> queueData;
                queueData = base.ReadAsyncData;
                if (queueData != null)
                {
                    int[] keys = new int[queueData.Keys.Count];
                    queueData.Keys.CopyTo(keys, 0);
                    foreach (int key in keys)
                    {
                        if (!result.ContainsKey(key))
                            result.Add(key, ConvertByteArrayToLiveUpdate(queueData[key]));
                    }
                }
                return result;
            }
        }
        public new Dictionary<int, LiveUpdate> ReadFromQueues
        {
            get
            {
                Dictionary<int, LiveUpdate> ret = null;
                try
                {
                    ret = new Dictionary<int, LiveUpdate>();
                    MsmqAsyncItem[] items = base.ReadFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToLiveUpdate((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ReadFromQueues", ex);
                }
                return ret;
            }
        }
        public Dictionary<int, LiveUpdate> PeekQueues
        {
            get
            {
                Dictionary<int, LiveUpdate> ret = null;
                try
                {
                    ret = new Dictionary<int, LiveUpdate>();
                    MsmqAsyncItem[] items = base.PeekFromQueues();
                    foreach (MsmqAsyncItem item in items)
                    {
                        if (!ret.ContainsKey(item.Index))
                            ret.Add(item.Index, ConvertByteArrayToLiveUpdate((byte[])item.Payload));
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".PeekQueues", ex);
                }
                return ret;
            }
        }
        #endregion
        public LiveUpdatesToMsmq()
        {
            
        }
        #region Insert Methods
        public new void InsertIntoQueuesAsync(LiveUpdate lupdate)
        {
            try
            {
                base.InsertIntoQueuesAsync(lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(LiveUpdate lupdate)", ex);
            }
        }
        public new void InsertIntoQueueAsync(int index, LiveUpdate lupdate)
        {
            try
            {
                base.InsertIntoQueueAsync(index, lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int index, LiveUpdate lupdate)", ex);
            }
        }
        public new void InsertIntoQueues(LiveUpdate lupdate)
        {
            try
            {
                base.InsertIntoQueues(lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueues(LiveUpdate lupdate)", ex);
            }
        }
        public new void InsertIntoQueue(int index, LiveUpdate lupdate)
        {
            try
            {
                base.InsertIntoQueues(lupdate);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int index, LiveUpdate lupdate)", ex);
            }
        }
        #endregion
        private LiveUpdate ConvertByteArrayToLiveUpdate(byte[] bData)
        {
            LiveUpdate lupdate = null;
            try
            {
                lupdate = new LiveUpdate();
                string sErr = "";
                bool bRet = lupdate.Decode(bData, ref sErr);
                if (sErr != "")
                    throw (new Exception(sErr));
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ConvertByteArrayToLiveUpdate(byte[] bData)", ex);
            }
            return lupdate;
        }
        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// 	<LiveUpdatesToMsmq>
        /// 		<MSMQ name="localhost\private$\MTData.Listener.LiveUpdates"
        /// 			queueIsRecoverable="false"
        /// 			maxQueueSize="10000"
        /// 			pauseOnFailedMsmqInsert="2000"
        /// 			logWrites="false"
        /// 			logStats="false">
        /// 		</MSMQ>
        /// 	</LiveUpdatesToMsmq>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("MSMQ");
                base.CreateQueueInterfaces(list, typeof(LiveUpdate));
                bool startReadThread = false;
                bool startWriteThread = false;
                for (int X = 0; X < base.Count; X++)
                {
                    if (base[X].IsReadQueue)
                        startReadThread = true;
                    if (base[X].IsWriteQueue)
                        startWriteThread = true;
                }
                if (startReadThread)
                    StartAsyncRecvThread();
                if (startWriteThread)
                    StartAsyncSendThread();
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }
        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using System.Threading;
using System.Xml;
using log4net;
using MTData.Common.Network;

namespace MTData.Transport.Application.Communication.MsmqInterface
{
    public class MsmqListManager
    {
        #region Private Vars
        private static ILog _log = LogManager.GetLogger(typeof(MsmqListManager));
        private object _msgInsertSync = new object();
        private object _msgReadSync = new object();
        private List<MsmqBase> _msgQueues;
        private bool _threadSendRunning;
        private bool _threadSendOperational;
        private bool _threadRecvRunning;
        private bool _threadRecvOperational;
        private Thread _threadProcessAsyncEnqueue;
        private Thread _threadProcessAsyncDequeue;
        private object _asyncSendQueueSync = new object();
        private object _asyncRecvQueueSync = new object();
        private object _currentReadSync = new object();
        private Queue<MsmqAsyncItem> _asyncSendQueue;
        private Queue<MsmqAsyncItem> _asyncRecvQueue;
        Dictionary<int, MsmqAsyncItem> _currentRead;
        private ulong _asyncCount;
        private ulong _asyncDeQueueCount;
        #endregion
        #region Public Properties
        public bool HasData
        {
            get
            {
                bool bRet = false;
                lock (_currentReadSync)
                {
                    if (_currentRead != null)
                    {
                        if(_currentRead.Count > 0)
                            bRet = true;
                    }
                }
                return bRet;
            }
        }
        public Dictionary<int, byte[]> PeekAsyncData
        {
            get
            {
                Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
                Queue<int> removeKeys = new Queue<int>();
                lock (_currentReadSync)
                {
                    if (_currentRead != null)
                    {
                        int[] keys = new int[_currentRead.Keys.Count];
                        _currentRead.Keys.CopyTo(keys, 0);
                        foreach (int key in keys)
                        {
                            MsmqAsyncItem item = _currentRead[key];
                            if(item.Payload != null)
                                result.Add(key, (byte[]) item.Payload);
                            else
                            {
                                removeKeys.Enqueue(key);
                            }
                        }
                        while(removeKeys.Count > 0)
                        {
                            _currentRead.Remove(removeKeys.Dequeue());
                        }
                    }
                }
                return result;
            }
        }
        public Dictionary<int, byte[]> ReadAsyncData
        {
            get
            {
                Dictionary<int, byte[]> result = new Dictionary<int, byte[]>();
                Queue<int> removeKeys = new Queue<int>();
                lock (_currentReadSync)
                {
                    if (_currentRead != null)
                    {
                        int[] keys = new int[_currentRead.Keys.Count];
                        _currentRead.Keys.CopyTo(keys, 0);
                        foreach (int key in keys)
                        {
                            MsmqAsyncItem item = _currentRead[key];
                            if (item.Payload != null)
                                result.Add(key, (byte[])item.Payload);
                        }
                        _currentRead = new Dictionary<int, MsmqAsyncItem>();
                    }
                    
                }
                return result;
            }
        }
        public List<ulong> EnqueueCount
        {
            get
            {
                List<ulong> result = new List<ulong>();
                lock (_asyncSendQueueSync)
                {
                    result.Add(_asyncCount);
                    result.Add(_asyncDeQueueCount);
                    _asyncCount = 0;
                    _asyncDeQueueCount = 0;
                }
                foreach (MsmqBase queue in _msgQueues)
                {
                    result.Add(queue.EnqueueCount);
                }
                return result;
            }
        }
        #endregion

        protected MsmqBase this[int index] 
        {
            get 
            {
                if (_msgQueues == null)
                {
                    lock (_msgInsertSync)
                    {
                        lock (_msgReadSync)
                        {
                            _msgQueues = new List<MsmqBase>();
                        }
                    }
                }
                if(_msgQueues.Count > index)
                    return _msgQueues[index];
                return null;
            }
            set 
            {
                if (_msgQueues == null)
                {
                    lock (_msgInsertSync)
                    {
                        lock (_msgReadSync)
                        {
                            _msgQueues = new List<MsmqBase>();
                        }
                    }
                } 
                if (_msgQueues.Count > index)
                    _msgQueues[index] = value;
            }
        }
        public void Dispose()
        {
            lock (_msgInsertSync)
            {
                lock (_msgReadSync)
                {
                    for (int X = _msgQueues.Count -1; X >= 0; X--)
                    {
                        if(_msgQueues[X] != null)
                            _msgQueues[X].Dispose();
                        _msgQueues.RemoveAt(X);
                    }
                }
            }
            StopAsyncSendThread();
            StopAsyncRecvThread();
        }
        protected int Count
        {
            get
            {
                if (_msgQueues == null)
                {
                    lock (_msgInsertSync)
                    {
                        lock (_msgReadSync)
                        {
                            _msgQueues = new List<MsmqBase>();
                        }
                    }
                }
                return _msgQueues.Count;
            }
        }

        #region Thread Start Stop Processing
        public void StartAsyncSendThread()
        {
            try
            {
                _threadSendRunning = false;
                int iCount = 20;
                while (_threadSendOperational && iCount > 0)
                {
                    Thread.Sleep(100);
                    iCount--;
                }

                lock (_asyncSendQueueSync)
                    _asyncSendQueue = new Queue<MsmqAsyncItem>();

                _threadProcessAsyncEnqueue = null;
                _threadProcessAsyncEnqueue = new Thread(new ThreadStart(ProcessEnqueueThread));
                _threadProcessAsyncEnqueue.Name = "Processing Enqueue to Msmq";
                _threadSendRunning = true;
                _threadProcessAsyncEnqueue.Start();
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".StartAsyncSendThread()", ex);
            }
        }
        public void StopAsyncSendThread()
        {
            try
            {
                _threadSendRunning = false;
                int iCount = 20;
                while (_threadSendOperational && iCount > 0)
                {
                    Thread.Sleep(100);
                    iCount--;
                }
                lock (_asyncSendQueueSync)
                {
                    if (_asyncSendQueue != null)
                    {
                        _asyncSendQueue.Clear();
                        _asyncSendQueue = null;
                    }
                }
                _threadProcessAsyncEnqueue = null;
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".StopAsyncSendThread()", ex);
            }
        }
        public void StartAsyncRecvThread()
        {
            try
            {
                _threadRecvRunning = false;
                int iCount = 20;
                while (_threadRecvOperational && iCount > 0)
                {
                    Thread.Sleep(100);
                    iCount--;
                }
                lock (_asyncRecvQueueSync)
                    _asyncRecvQueue = new Queue<MsmqAsyncItem>();
                _threadProcessAsyncDequeue = null;
                _threadProcessAsyncDequeue = new Thread(new ThreadStart(ProcessDequeueThread));
                _threadProcessAsyncDequeue.Name = "Processing Dequeues from Msmq";
                _threadRecvRunning = true;
                _threadProcessAsyncDequeue.Start();
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".StartAsyncRecvThread()", ex);
            }
        }
        public void StopAsyncRecvThread()
        {
            try
            {
                _threadRecvRunning = false;
                int iCount = 20;
                while (_threadRecvOperational && iCount > 0)
                {
                    Thread.Sleep(100);
                    iCount--;
                }
                lock (_asyncRecvQueueSync)
                {
                    if (_asyncRecvQueue != null)
                    {
                        _asyncRecvQueue.Clear();
                        _asyncRecvQueue = null;
                    }
                }
                _threadProcessAsyncDequeue = null;
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".StartAsyncRecvThread()", ex);
            }
        }
        #endregion
        #region Thread Handling Code
        private void ProcessDequeueThread()
        {
            try
            {
                _threadRecvOperational = true;
                while (_threadRecvRunning)
                {
                    try
                    {
                        int queueCount = 0;
                        lock (_msgReadSync)
                        {
                            queueCount = _msgQueues.Count;
                        }
                        if (queueCount == 0)
                        {
                            // Slow down the thread loop if there are no queues to process.
                            Thread.Sleep(500);
                        }
                        else
                        {
                            lock (_currentReadSync)
                            {
                                if (_currentRead == null)
                                {
                                    _currentRead = new Dictionary<int, MsmqAsyncItem>();
                                }

                                for (int X = 0; X < queueCount; X++)
                                {
                                    if (!_currentRead.ContainsKey(X))
                                    {
                                        #region If we don't have a value for this queue, read it and add it to _currentRead
                                        object body = null;
                                        lock (_msgReadSync)
                                        {
                                            if (_msgQueues.Count > X)
                                            {
                                                MsmqBase queue = _msgQueues[X];
                                                if(queue.IsReadQueue)
                                                    body = queue.ReadBody();
                                            }
                                        }
                                        if (body != null)
                                        {
                                            MsmqAsyncItem item = new MsmqAsyncItem(X, body);
                                            _currentRead.Add(X, item);
                                        }
                                        #endregion
                                    }
                                }
                            }
                        }
                    }
                    catch (Exception exMainLoop)
                    {
                        _log.Error(this.GetType().FullName + ".ProcessDequeueThread() - Main Loop", exMainLoop);
                    }
                    finally
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessDequeueThread()", ex);
            }
            finally
            {
                _threadRecvOperational = false;
            }
        }
        private void ProcessEnqueueThread()
        {
            try
            {
                _threadSendOperational = true;
                while (_threadSendRunning)
                {
                    try
                    {
                        MsmqAsyncItem item = null;
                        lock (_asyncSendQueueSync)
                        {
                            if (_asyncSendQueue.Count > 0)
                            {
                                _asyncDeQueueCount++;
                                item = _asyncSendQueue.Dequeue();
                            }
                        }
                        if (item != null)
                        {
                            if (item.Index < 0)
                            {
                                if(item.Payload is LiveUpdate)
                                    InsertIntoQueues((LiveUpdate)item.Payload);
                                else if(item.Payload is ECMUpdate)
                                    InsertIntoQueues((ECMUpdate)item.Payload);
                                else if (item.Payload is ListenerLogItem)
                                    InsertIntoQueues((ListenerLogItem)item.Payload);
                                else if (item.Payload is byte[])
                                {
                                    InsertIntoQueues((byte[]) item.Payload, item.FleetId, item.VehicleId, item.DriverId, item.ReasonId);
                                }
                                else
                                {
                                    MemoryStream mstream = new MemoryStream();
                                    BinaryFormatter formatter = new BinaryFormatter();

                                    formatter.Serialize(mstream, item.Payload);
                                    byte[] serialzedBytes = mstream.ToArray();
                                    InsertIntoQueues(serialzedBytes, -1, -1, -1, -1);
                                }
                            }
                            else
                            {
                                if (item.Payload is LiveUpdate)
                                    InsertIntoQueue(item.Index, (LiveUpdate)item.Payload);
                                else if (item.Payload is ECMUpdate)
                                    InsertIntoQueue(item.Index, (ECMUpdate)item.Payload);
                                else if (item.Payload is ListenerLogItem)
                                    InsertIntoQueue(item.Index, (ListenerLogItem)item.Payload);
                                else
                                {
                                    InsertIntoQueue(item.Index, (byte[])item.Payload, item.FleetId, item.VehicleId, item.DriverId, item.ReasonId);
                                }
                            }
                        }
                        else if (_threadSendRunning)
                        {
                            Thread.Sleep(100);
                        }
                    }
                    catch (Exception exMainLoop)
                    {
                        _log.Error(this.GetType().FullName + ".ProcessEnqueueThread() - Main Loop", exMainLoop);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessEnqueueThread()", ex);
            }
            finally
            {
                _threadSendOperational = false;
            }
        }
        #endregion

        protected void InsertIntoQueues(byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            if (data != null)
            {
                lock (_msgInsertSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    foreach (MsmqBase queue in _msgQueues)
                    {
                        try
                        {
                            if(queue.IsWriteQueue)
                                queue.InsertMessageToQueue(data, fleetId, vehicleId, driverId, reasonId);
                        }
                        catch (Exception ex)
                        {
                            if(queue == null)
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(byte[] data, int fleetId, int vehicleId, int driverId, int reasonId) Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(byte[] data, int fleetId, int vehicleId, int driverId, int reasonId) Queue '" + queue.Name + "'", ex);
                        }
                    }
                }
            }
        }
        protected void InsertIntoQueues(ECMUpdate spnValues)
        {
            if (spnValues != null)
            {
                lock (_msgInsertSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    foreach (MsmqBase queue in _msgQueues)
                    {
                        try
                        {
                            if (queue.IsWriteQueue)
                                queue.InsertMessageToQueue(spnValues);
                        }
                        catch (Exception ex)
                        {
                            if (queue == null)
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate spnValues) Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate spnValues) Queue '" + queue.Name + "'", ex);
                        }
                    }
                }
            }
        }
        protected void InsertIntoQueues(LiveUpdate lUpdate)
        {
            if (lUpdate != null)
            {
                lock (_msgInsertSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    foreach (MsmqBase queue in _msgQueues)
                    {
                        try
                        {
	                        queue.InsertMessageToQueue(lUpdate);
                        }
                        catch (Exception ex)
                        {
                            if (queue == null)
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate lUpdate) Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate lUpdate) Queue '" + queue.Name + "'", ex);
                        }
                    }
                }
            }
        }
        protected void InsertIntoQueues(ListenerLogItem lUpdate)
        {
            if (lUpdate != null)
            {
                lock (_msgInsertSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    foreach (MsmqBase queue in _msgQueues)
                    {
                        try
                        {
                            queue.InsertMessageToQueue(lUpdate);
                        }
                        catch (Exception ex)
                        {
                            if (queue == null)
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ListenerLogItem lUpdate) Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ListenerLogItem lUpdate) Queue '" + queue.Name + "'", ex);
                        }
                    }
                }
            }
        }
        protected void InsertIntoQueues(object serializableObject)
        {
            if (serializableObject != null)
            {
                lock (_msgInsertSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    foreach (MsmqBase queue in _msgQueues)
                    {
                        bool bResult = false;
                        try
                        {
                            bResult = queue.InsertMessageToQueue(serializableObject);
                        }
                        catch (Exception ex)
                        {
                            if (queue == null)
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate lUpdate) Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(ECMUpdate lUpdate) Queue '" + queue.Name + "'", ex);
                        }
                        if (!bResult)
                            throw new Exception("Failed to enqueue object on MSMQ");

                    }
                }
            }
        }
        protected void InsertIntoQueues(TCPAsyncData tcpAsyncData)
        {
            if (tcpAsyncData != null)
            {
                lock (_msgInsertSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    foreach (MsmqBase queue in _msgQueues)
                    {
                        try
                        {
                            queue.InsertMessageToQueue(tcpAsyncData);
                        }
                        catch (Exception ex)
                        {
                            if (queue == null)
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(TCPAsyncData tcpAsyncData) Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".InsertIntoQueues(TCPAsyncData tcpAsyncData) Queue '" + queue.Name + "'", ex);
                        }
                    }
                }
            }
        }

        protected void InsertIntoQueue(int Index, byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                if (data != null)
                {
                    lock (_msgInsertSync)
                    {
                        if (_msgQueues == null)
                            _msgQueues = new List<MsmqBase>();

                        if (this.Count > Index && this[Index].IsWriteQueue)
                            this[Index].InsertMessageToQueue(data, fleetId, vehicleId, driverId, reasonId);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int Index, byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }
        protected void InsertIntoQueue(int Index, ECMUpdate spnValues)
        {
            try
            {
                if (spnValues != null)
                {
                    lock (_msgInsertSync)
                    {
                        if (_msgQueues == null)
                            _msgQueues = new List<MsmqBase>();

                        if (this.Count > Index && this[Index].IsWriteQueue)
                            this[Index].InsertMessageToQueue(spnValues);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int Index, ECMUpdate spnValues)", ex);
            }
        }
        protected void InsertIntoQueue(int Index, LiveUpdate lUpdate)
        {
            try
            {
                if (lUpdate != null)
                {
                    lock (_msgInsertSync)
                    {
                        if (_msgQueues == null)
                            _msgQueues = new List<MsmqBase>();

                        if (this.Count > Index && this[Index].IsWriteQueue)
                            this[Index].InsertMessageToQueue(lUpdate);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int Index, ECMUpdate lUpdate)", ex);
            }
        }
        protected void InsertIntoQueue(int Index, ListenerLogItem lUpdate)
        {
            try
            {
                if (lUpdate != null)
                {
                    lock (_msgInsertSync)
                    {
                        if (_msgQueues == null)
                            _msgQueues = new List<MsmqBase>();

                        if (this.Count > Index && this[Index].IsWriteQueue)
                            this[Index].InsertMessageToQueue(lUpdate);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int Index, ListenerLogItem lUpdate)", ex);
            }
        }

        protected void InsertIntoQueue(int Index, TCPAsyncData tcpAsyncData)
        {
            try
            {
                if (tcpAsyncData != null)
                {
                    lock (_msgInsertSync)
                    {
                        if (_msgQueues == null)
                            _msgQueues = new List<MsmqBase>();

                        if (this.Count > Index && this[Index].IsWriteQueue)
                            this[Index].InsertMessageToQueue(tcpAsyncData);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueue(int Index, TCPAsyncData tcpAsyncData)", ex);
            }
        }

        protected void InsertIntoQueuesAsync(byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(-1, data, fleetId, vehicleId, driverId, reasonId);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueuesAsync(byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }
        protected void InsertIntoQueuesAsync(ECMUpdate spnValues)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(-1, spnValues);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueuesAsync(ECMUpdate spnValues)", ex);
            }
        }
        protected void InsertIntoQueuesAsync(LiveUpdate lUpdate)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(-1, lUpdate);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueuesAsync(LiveUpdate lUpdate)", ex);
            }
        }
        protected void InsertIntoQueuesAsync(ListenerLogItem lUpdate)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(-1, lUpdate);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueuesAsync(ListenerLogItem lUpdate)", ex);
            }
        }

        protected void InsertIntoQueuesAsync(TCPAsyncData tcpAsyncData)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(-1, tcpAsyncData);
                _asyncCount++;
                lock (_asyncSendQueueSync)
                    _asyncSendQueue.Enqueue(item);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueuesAsync(TCPAsyncData tcpAsyncData)", ex);
            }
        }

        protected void InsertIntoQueuesAsync(object seralisedObject)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(-1, seralisedObject);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueuesAsync(LiveUpdate lUpdate)", ex);
            }
        }

        protected void InsertIntoQueueAsync(int Index, byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(Index, data, fleetId, vehicleId, driverId, reasonId);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int Index, byte[] data, int fleetId, int vehicleId, int driverId, int reasonId)", ex);
            }
        }
        protected void InsertIntoQueueAsync(int Index, ECMUpdate spnValues)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(Index, spnValues);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int Index, ECMUpdate spnValues)", ex);
            }
        }
        protected void InsertIntoQueueAsync(int Index, LiveUpdate lUpdate)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(Index, lUpdate);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int Index, LiveUpdate lUpdate)", ex);
            }
        }
        protected void InsertIntoQueueAsync(int Index, ListenerLogItem lUpdate)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(Index, lUpdate);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int Index, LiveUpdate lUpdate)", ex);
            }
        }
        protected void InsertIntoQueueAsync(int Index, object serializableObject)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(Index, serializableObject);
                lock (_asyncSendQueueSync)
                {
                    _asyncSendQueue.Enqueue(item);
                    _asyncCount++;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int Index, object serializableObject)", ex);
            }
        }

        protected void InsertIntoQueueAsync(int Index, TCPAsyncData tcpAsyncData)
        {
            try
            {
                MsmqAsyncItem item = new MsmqAsyncItem(Index, tcpAsyncData);
                _asyncCount++;
                lock (_asyncSendQueueSync)
                    _asyncSendQueue.Enqueue(item);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".InsertIntoQueueAsync(int Index, TCPAsyncData tcpAsyncData)", ex);
            }
        }

        protected MsmqAsyncItem[] ReadFromQueues()
        {
            MsmqAsyncItem[] oRet = new MsmqAsyncItem[0];
            List<MsmqAsyncItem> temp = new List<MsmqAsyncItem>();
            object result = null;
            try
            {
                lock (_msgReadSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    for(int X = 0; X < _msgQueues.Count; X++) 
                    {
                        result = null;
                        try
                        {
                            result = _msgQueues[X].ReadBody();
                        }
                        catch (Exception ex)
                        {
                            if (_msgQueues[X] == null)
                                _log.Error(this.GetType().FullName + ".ReadFromQueues() Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".ReadFromQueues() Queue '" + _msgQueues[X].Name + "'", ex);
                        }
                        if (result != null)
                        {
                            MsmqAsyncItem item = new MsmqAsyncItem(X, result);
                            temp.Add(item);
                        }
                    }
                }
                if (temp.Count > 0)
                {
                    oRet = new MsmqAsyncItem[temp.Count];
                    temp.CopyTo(oRet);
                }
            }
            catch (Exception ex)
            {
                oRet = new MsmqAsyncItem[0];
                _log.Error(this.GetType().FullName + ".ReadFromQueues()", ex);
            }
            return oRet;
        }
        protected MsmqAsyncItem ReadFromQueue(int index)
        {
            MsmqAsyncItem oRet = null;
            object result = null;
            try
            {
                lock (_msgReadSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();

                    if (this.Count > index && this[index].IsReadQueue)
                    {
                        result = this[index].ReadBody();
                    }
                }
                if(result != null)
                {
                    oRet = new MsmqAsyncItem(index, result);
                }
            }
            catch (Exception ex)
            {
                oRet = null;
                _log.Error(this.GetType().FullName + ".ReadFromQueue(int index)", ex);
            }
            return oRet;
        }

        protected MsmqAsyncItem[] PeekFromQueues()
        {
            MsmqAsyncItem[] oRet = new MsmqAsyncItem[0];
            List<MsmqAsyncItem> temp = new List<MsmqAsyncItem>();
            object result = null;
            try
            {
                lock (_msgReadSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();
                    for (int X = 0; X < _msgQueues.Count; X++)
                    {
                        try
                        {
                            result = _msgQueues[X].PeekBody();
                        }
                        catch (Exception ex)
                        {
                            if (_msgQueues[X] == null)
                                _log.Error(this.GetType().FullName + ".PeekFromQueues() Queue = null", ex);
                            else
                                _log.Error(this.GetType().FullName + ".PeekFromQueues() Queue '" + _msgQueues[X].Name + "'", ex);
                        }
                        if (result != null)
                        {
                            MsmqAsyncItem item = new MsmqAsyncItem(X, result);
                            temp.Add(item);
                        }
                    }
                }
                if (temp.Count > 0)
                {
                    oRet = new MsmqAsyncItem[temp.Count];
                    temp.CopyTo(oRet);
                }
            }
            catch (Exception ex)
            {
                oRet = new MsmqAsyncItem[0];
                _log.Error(this.GetType().FullName + ".PeekFromQueues()", ex);
            }
            return oRet;
        }
        protected MsmqAsyncItem PeekFromQueue(int index)
        {
            MsmqAsyncItem oRet = null;
            object result = null;
            try
            {
                lock (_msgReadSync)
                {
                    if (_msgQueues == null)
                        _msgQueues = new List<MsmqBase>();

                    if (this.Count > index)
                    {
                        result = this[index].PeekBody();
                    }
                }
                if (result != null)
                {
                    oRet = new MsmqAsyncItem(index, result);
                }
            }
            catch (Exception ex)
            {
                oRet = null;
                _log.Error(this.GetType().FullName + ".PeekFromQueue(int index)", ex);
            }
            return oRet;
        }

        protected void CreateQueueInterfaces(XmlNodeList list, Type queueItemType)
        {
            try
            {
                if ((list != null) && (list.Count > 0))
                {
                    lock (_msgInsertSync)
                    {
                        lock (_msgReadSync)
                        {
                            _msgQueues = new List<MsmqBase>();
                        }
                    }
                    foreach (XmlNode node in list)
                    {
                        string name = Convert.ToString(node.Attributes.GetNamedItem("name").Value);
                        bool queueIsRecoverable = Convert.ToBoolean(node.Attributes.GetNamedItem("queueIsRecoverable").Value);
			            int maxQueueSize=Convert.ToInt32(node.Attributes.GetNamedItem("maxQueueSize").Value);
			            int pauseOnFailedMsmqInsert=Convert.ToInt32(node.Attributes.GetNamedItem("pauseOnFailedMsmqInsert").Value);
                        int dequeueNumOfMsgsOnOverflow = Convert.ToInt32(node.Attributes.GetNamedItem("dequeueNumOfMsgsOnOverflow").Value);

                        MsmqBase queue = new MsmqBase(name, maxQueueSize, queueIsRecoverable, dequeueNumOfMsgsOnOverflow);
                        queue.QueueItemType = queueItemType;
                        queue.IsWriteQueue = Convert.ToBoolean(node.Attributes.GetNamedItem("IsAsyncWritter").Value);
                        queue.IsReadQueue = Convert.ToBoolean(node.Attributes.GetNamedItem("IsAsyncReader").Value);
                        if (queue.IsWriteQueue && queue.IsReadQueue)
                        {
                            queue.IsWriteQueue = true;
                            queue.IsReadQueue = false;
                        } 
                        queue.LogStats = Convert.ToBoolean(node.Attributes.GetNamedItem("logStats").Value);
                        queue.LogReads = Convert.ToBoolean(node.Attributes.GetNamedItem("logReads").Value);
                        queue.LogPeeks = Convert.ToBoolean(node.Attributes.GetNamedItem("logPeeks").Value);
                        queue.LogWrites = Convert.ToBoolean(node.Attributes.GetNamedItem("logWrites").Value);
                        #region Read in the fleet filter list
                        XmlNodeList fleetFilterList = node.SelectNodes("fleetFilter");
                        if (fleetFilterList != null && fleetFilterList.Count > 0)
                        {
                            int[] fleetFilter = new int[fleetFilterList.Count];
                            int X = 0;
                            foreach (XmlNode fleetNode in fleetFilterList)
                            {
                                fleetFilter[X++] = Convert.ToInt32(fleetNode.Attributes.GetNamedItem("fleetId").Value);
                            }
                            queue.FleetIdFilter = fleetFilter;
                        }
                        #endregion
                        #region Read in the vehicle filter list
                        XmlNodeList vehicleFilterList = node.SelectNodes("vehicleFilter");
                        if (vehicleFilterList != null && vehicleFilterList.Count > 0)
                        {
                            List<ulong> lstFilter = new List<ulong>();
                            foreach (XmlNode vehicleNode in vehicleFilterList)
                            {
                                string sfleetId = vehicleNode.Attributes.GetNamedItem("fleetId").Value;
                                string svehicleId = vehicleNode.Attributes.GetNamedItem("vehicleId").Value;
                                ulong key = queue.GetKey(sfleetId, svehicleId);
                                if(key > 0 && !lstFilter.Contains(key))
                                {
                                    lstFilter.Add(key);
                                }
                            }
                            ulong[] arrFilter = new ulong[lstFilter.Count];
                            lstFilter.CopyTo(arrFilter);
                            queue.VehicleIdFilter = arrFilter;
                        }
                        #endregion
                        #region Read in the driver filter list
                        XmlNodeList driverFilterList = node.SelectNodes("driverFilter");
                        if (driverFilterList != null && driverFilterList.Count > 0)
                        {
                            List<ulong> lstFilter = new List<ulong>();
                            foreach (XmlNode driverNode in driverFilterList)
                            {
                                string sfleetId = driverNode.Attributes.GetNamedItem("fleetId").Value;
                                string sdriverId = driverNode.Attributes.GetNamedItem("driverId").Value;
                                ulong key = queue.GetKey(sfleetId, sdriverId);
                                if (key > 0 && !lstFilter.Contains(key))
                                {
                                    lstFilter.Add(key);
                                }
                            }
                            ulong[] arrFilter = new ulong[lstFilter.Count];
                            lstFilter.CopyTo(arrFilter);
                            queue.DriverIdFilter = arrFilter;
                        }
                        #endregion
                        #region Read in the reasonCode filter list
                        XmlNodeList reasonCodeFilterList = node.SelectNodes("reasonCodeFilter");
                        if (reasonCodeFilterList != null && reasonCodeFilterList.Count > 0)
                        {
                            List<int> lstFilter = new List<int>();
                            foreach (XmlNode reasonCodeNode in reasonCodeFilterList)
                            {
                                int reasonId = 0;
                                try
                                {
                                    reasonId = Convert.ToInt32(reasonCodeNode.Attributes.GetNamedItem("reasonId").Value);
                                }
                                catch (Exception)
                                {
                                    reasonId = 0;
                                }
                                if (reasonId > 0 && !lstFilter.Contains(reasonId))
                                {
                                    lstFilter.Add(reasonId);
                                }
                            }
                            int[] arrFilter = new int[lstFilter.Count];
                            lstFilter.CopyTo(arrFilter);
                            queue.ReasonCodeIdFilter = arrFilter;
                        }
                       #endregion
                        #region Read in the reasonCode ignore filter list
                        XmlNodeList reasonCodeIgnoreFilterList = node.SelectNodes("reasonCodeIgnoreFilter");
                        if (reasonCodeIgnoreFilterList != null && reasonCodeIgnoreFilterList.Count > 0)
                        {
                            List<int> lstFilter = new List<int>();
                            foreach (XmlNode reasonCodeNode in reasonCodeIgnoreFilterList)
                            {
                                int reasonId = 0;
                                try
                                {
                                    reasonId = Convert.ToInt32(reasonCodeNode.Attributes.GetNamedItem("reasonId").Value);
                                }
                                catch (Exception)
                                {
                                    reasonId = 0;
                                }
                                if (reasonId > 0 && !lstFilter.Contains(reasonId))
                                {
                                    lstFilter.Add(reasonId);
                                }
                            }
                            int[] arrFilter = new int[lstFilter.Count];
                            lstFilter.CopyTo(arrFilter);
                            queue.ReasonCodeIdIgnoreFilter = arrFilter;
                        }
                        #endregion
                        #region Read in the SPN code filter list
                        XmlNodeList spnList = node.SelectNodes("spnFilter");
                        if(spnList != null && spnList.Count > 0)
                        {
                            Dictionary<int, List<int>> _spnFilter = new Dictionary<int, List<int>>();
                            foreach (XmlNode spnNode in spnList)
                            {
        			            int sourceType = Convert.ToInt32(spnNode.Attributes.GetNamedItem("sourceType").Value);
                                int spnCode = Convert.ToInt32(spnNode.Attributes.GetNamedItem("spnCode").Value);
                                if(_spnFilter.ContainsKey(sourceType))
                                {
                                    if(!_spnFilter[sourceType].Contains(spnCode))
                                    {
                                        _spnFilter[sourceType].Add(spnCode);
                                    }
                                }
                                else
                                {
                                    List<int> items = new List<int>();
                                    items.Add(spnCode);
                                    _spnFilter.Add(sourceType, items);
                                }
                            }
                            queue.SpnFilter = _spnFilter;
                        }
                        #endregion

                        lock (_msgInsertSync)
                        {
                            lock (_msgReadSync)
                            {
                                _msgQueues.Add(queue);
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".CreateQueueInterfaces(XmlNodeList list)", ex);
            }
        }
    }
}

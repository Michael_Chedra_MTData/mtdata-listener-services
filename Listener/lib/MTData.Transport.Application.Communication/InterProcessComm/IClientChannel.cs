using System;
using System.IO;

namespace MTData.Transport.Application.Communication.InterProcessComm
{
    public interface IClientChannel : IDisposable
    {
        string HandleRequest(string request);
        string HandleRequest(Stream request);
        object HandleRequest(object request);
        IClientChannel Create();
    }
}

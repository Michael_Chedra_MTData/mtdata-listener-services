using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Application.Communication.InterProcessComm
{
    public class InterProcessIOException : Exception
    {
        public bool IsServerAvailable = true;
        public uint ErrorCode = 0;
        public InterProcessIOException(String text)
            : base(text)
        {
        }
        protected InterProcessIOException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

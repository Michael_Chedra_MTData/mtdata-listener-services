using System;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
	public class TimezoneChangeUpdate
	{
		private char cCmd = 'T';
		public char cSubCmd = 'I';
		public short iPacketVersion = 0;
		public int iTimeZoneID = 0;
		public short iFleetID = 0;
		public int iVehicleID = 0;
		public int iDriverID = 0;
		public int iUserID = 0;
		public int iTrailerID = 0;

		public TimezoneChangeUpdate()
		{
		}

		public byte[] Encode(ref string sErrMsg)
		{
			byte[] bResult = null;
			byte bSOP = (byte) 0x01;
			byte bEOP = (byte) 0x07;
			MemoryStream oStream = null;

			// A time zone update packet	: [SOP (0x01)][Length (2 Bytes)][Cmd (1 Char)][SubCmd (1 Char)][Packet Ver (2 Bytes)][Time Zone ID (4 bytes)][FleetID (2 bytes)][Data][EOP (0x07)]
			// Sub Cmd Defs
			//					Info Update		: [Data] = null
			//					Driver Update	: [Data] = [Driver ID (4 Bytes)]
			//					Fleet Update		: [Data] = null;
			//					Vehicle Update	: [Data] = [Vehicle ID (4 Bytes)]
			//					User Update		: [Data] = [User ID (4 Bytes)]
			//					Trailer Update	: [Data] = [Trailer ID (4 Bytes)]

			try
			{
				oStream = new MemoryStream();
				PacketUtilities.WriteToStream(oStream, bSOP);								// SOP
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Length 1
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Length 2
				PacketUtilities.WriteToStream(oStream, this.cCmd);						// Packet Type (1 Char)
				PacketUtilities.WriteToStream(oStream, this.cSubCmd);				// Sub Command (1 Char)
				PacketUtilities.WriteToStream(oStream, this.iPacketVersion);		// Packet Version (2 byte int)
				PacketUtilities.WriteToStream(oStream, this.iTimeZoneID);			// Time Zone ID (4 byte int)
				PacketUtilities.WriteToStream(oStream, this.iFleetID);					// Fleet ID (2 byte int)
				if(cSubCmd == 'D')
					PacketUtilities.WriteToStream(oStream, this.iDriverID);			// Driver ID(4 byte int)
				else if(cSubCmd == 'V')
					PacketUtilities.WriteToStream(oStream, this.iVehicleID);			// Vehicle ID(4 byte int)
				else if(cSubCmd == 'T')
					PacketUtilities.WriteToStream(oStream, this.iTrailerID);			// Trailer ID(4 byte int)
				else if(cSubCmd == 'U')
					PacketUtilities.WriteToStream(oStream, this.iUserID);				// User ID(4 byte int)
				PacketUtilities.WriteToStream(oStream, bEOP);								// EOP

				bResult = oStream.ToArray();
				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding time zone update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}

		public bool Decode(byte[] bData, ref string sErrMsg)
		{
			// A time zone update packet	: [SOP (0x01)][Length (2 Bytes)][Cmd (1 Char)][SubCmd (1 Char)][Packet Ver (2 Bytes)][Time Zone ID (4 bytes)][Data][EOP (0x07)]
			// Sub Cmd Defs
			//					Info Update		: [Data] = null
			//					Driver Update	: [Data] = [Driver ID (4 Bytes)]
			//					Fleet Update		: [Data] = null
			//					Vehicle Update	: [Data] = [Vehicle ID (4 Bytes)]
			//					Trailer Update	: [Data] = [Trailer ID (4 Bytes)]

			bool bRet = false;
			byte bValue = (byte) 0x00;
			byte bEOP = (byte) 0x07;
			char cType = (char) 0x00;
			int iLength = 0;
			MemoryStream oStream = null;

			try
			{
				oStream = new MemoryStream(bData, 0, bData.Length, false, false);
				PacketUtilities.ReadFromStream(oStream, ref bValue);													
				if (bValue != (byte) 0x01)																										// SOP
				{
					sErrMsg = "Error encoding live update : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref iLength);													// Length
				if (bData.Length != iLength)
				{
					sErrMsg = "Error encoding live update : Live update packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
					sErrMsg = "Error encoding live update : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref cType);													// Command Type (1 byte)
				if (cType != 'T')
				{
					sErrMsg = "Error encoding live update : Time zone update packet type is incorrect.";
					return bRet;
				}
				else
					cCmd = cType;

				PacketUtilities.ReadFromStream(oStream, ref cType);													// Sub Command (1 byte)
				if (cType != 'I' && cType != 'D' && cType != 'F' && cType != 'V' && cType != 'U')
				{
					sErrMsg = "Error encoding live update : Time zone update packet sub command is invalid.";
					return bRet;
				}
				else
					cSubCmd = cType;
				PacketUtilities.ReadFromStream(oStream, ref this.iPacketVersion);								// Packet Version (2 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.iTimeZoneID);								// Time Zone ID (4 byte int)
				if(cSubCmd == 'U')
					this.iFleetID = 0;
				PacketUtilities.ReadFromStream(oStream, ref this.iFleetID);											// Fleet ID (2 byte int)
				if(cSubCmd == 'D')
					PacketUtilities.ReadFromStream(oStream, ref this.iDriverID);									// Driver ID (4 byte int)
				else if(cSubCmd == 'V')
					PacketUtilities.ReadFromStream(oStream, ref this.iVehicleID);								// Vehicle ID (4 byte int)
				else if(cSubCmd == 'T')
					PacketUtilities.ReadFromStream(oStream, ref this.iTrailerID);									// Trailer ID (4 byte int)
				else if(cSubCmd == 'U')
					PacketUtilities.ReadFromStream(oStream, ref this.iUserID);										// User ID (4 byte int)
				oStream.Close();
				oStream = null;
				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error decoding time zone update : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}

		public string ToDisplayString()
		{
			return	this.ToString();
		}

		public override string ToString()
		{
			StringBuilder builder = null;
			string sRet = "";

			try
			{
				builder = new StringBuilder();
				builder.Append("\r\nTime Zone Update Data : Version "); builder.Append(iPacketVersion);
				builder.Append("\r\n    Cmd : "); builder.Append(this.cCmd);
				builder.Append("\r\n    Sub Cmd : "); builder.Append(this.cSubCmd);
				builder.Append("\r\n    Time Zone ID : "); builder.Append(this.iTimeZoneID);
				builder.Append("\r\n    Fleet ID : "); builder.Append(this.iFleetID);
				if(cSubCmd == 'D')
				{
					builder.Append("\r\n    Driver ID : "); builder.Append(this.iDriverID);
				}
				else if(cSubCmd == 'V')
				{
					builder.Append("\r\n    Vehicle ID : "); builder.Append(this.iVehicleID);
				}
				else if(cSubCmd == 'T')
				{
					builder.Append("\r\n    Trailer ID : "); builder.Append(this.iTrailerID);
				}
				else if(cSubCmd == 'U')
				{
					builder.Append("\r\n    User ID : "); builder.Append(this.iUserID);
				}
				sRet = builder.ToString();
			}
			catch(System.Exception ex)
			{
				sRet = "Error extracting time zone update data.  Error : " + ex.Message;
			}
			return sRet;
		}
	}
}

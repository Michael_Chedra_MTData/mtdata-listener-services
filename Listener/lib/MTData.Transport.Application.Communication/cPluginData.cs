using System;

namespace MTData.Transport.Application.Communication
{
    public enum PluginIds
    {
        FlashInfo = 1,
        SatelliteInfo = 2,
        GForceInfo = 3,
        RouteInfo = 4,
        CanVehicleSpeed = 5,
        AccurateSpeed = 6,
        TaskState = 7,
        OverSpeedBreachInfo = 8,
        DssEventInfo = 9,
        DvrEventInfo = 10,
        DvrStatusInfo = 11
    }

    /// <summary>
    /// Summary description for cPluginData.
    /// </summary>
    public class cPluginData
    {
        #region Private Vars
        private PluginIds _PluginID = 0;
        private byte[] _bData = null;
        #endregion
        #region Public Properties
        public PluginIds PluginID
        {
            get { return _PluginID; }
            set { _PluginID = value; }
        }


        public byte[] PluginData
        {
            get { return _bData; }
            set { _bData = value; }
        }
        #endregion
        #region Constructors
        public cPluginData()
        {
        }

        public cPluginData(PluginIds iPluginID, byte[] bPluginData)
        {
            _PluginID = iPluginID;
            _bData = bPluginData;
        }
        #endregion
    }
}

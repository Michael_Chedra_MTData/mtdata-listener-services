using System;
using System.Data;
using System.IO;
using System.Text;
using System.Xml;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
    public class XMLUpdate
    {
        // This packet is used to transmit XML data via a live update.
        private const byte bSOP = (byte)0x01;
        private const byte bStartXML = (byte)0x02;
        private const byte bEndXML = (byte)0x03;
        private const byte bEOP = (byte)0x07;

        private char cCmd = 'X';
        public char cSubCmd = 'U';
        public short iPacketVersion = 0;
        public int iItemID = 0;
        public string sXMLData = "";
        public byte[] bPayload;

        public XMLUpdate()
        {
        }

        public void ConvertDataSetToXML(DataSet dsData)
        {
            MemoryStream oXMLStream = null;
            XmlTextWriter oXMLWriter = null;
            UnicodeEncoding utf = null;
            byte[] bData = null;
            int iStreamLength = 0;

            try
            {
                oXMLStream = new MemoryStream();
                oXMLWriter = new XmlTextWriter(oXMLStream, System.Text.Encoding.Unicode);
                dsData.WriteXml(oXMLWriter);
                iStreamLength = Convert.ToInt32(oXMLStream.Length);
                bData = new byte[iStreamLength];
                oXMLStream.Seek(0, System.IO.SeekOrigin.Begin);
                oXMLStream.Read(bData, 0, iStreamLength);
                utf = new UnicodeEncoding();
                sXMLData = utf.GetString(bData).Trim();
                oXMLWriter.Close();
                oXMLWriter = null;
                oXMLStream = null;
            }
            catch (System.Exception ex)
            {
                if (oXMLWriter != null)
                {
                    oXMLWriter.Close();
                }
                oXMLWriter = null;
                oXMLStream = null;
                sXMLData = "";
                throw (ex);
            }
        }
        public DataSet ConvertXMLToDataSet()
        {
            StringReader oXMLStream = null;
            XmlTextReader oXMLReader = null;
            DataSet dsRet = null;
            try
            {
                dsRet = new DataSet();
                oXMLStream = new StringReader(sXMLData);
                oXMLReader = new XmlTextReader(oXMLStream);
                dsRet.ReadXml(oXMLReader);
                if (oXMLStream != null)
                    oXMLStream.Close();
                oXMLStream = null;
                if (oXMLReader != null)
                    oXMLReader.Close();
                oXMLReader = null;
            }
            catch (System.Exception ex)
            {
                if (oXMLStream != null)
                    oXMLStream.Close();
                oXMLStream = null;
                if (oXMLReader != null)
                    oXMLReader.Close();
                oXMLReader = null;
                dsRet = null;
                throw (ex);
            }
            return dsRet;
        }
        public byte[] Encode(ref string sErrMsg)
        {
            byte[] bResult = null;
            MemoryStream oStream = null;

            // An XML update packet	: [SOP (0x01)][Length (2 Bytes)][Cmd (1 Char)][SubCmd (1 Char)][Packet Ver (2 Bytes)][Item ID (4 bytes)][XML Start (0x02)][XML Len (2 Bytes)][Data][XML End (0x02)][EOP (0x07)]
            try
            {
                oStream = new MemoryStream();
                PacketUtilities.WriteToStream(oStream, bSOP);										// SOP
                PacketUtilities.WriteToStream(oStream, (byte)0x00);									// Length 1
                PacketUtilities.WriteToStream(oStream, (byte)0x00);									// Length 2
                PacketUtilities.WriteToStream(oStream, cCmd);									// Packet Type (1 Char)
                PacketUtilities.WriteToStream(oStream, cSubCmd);								// Sub Command (1 Char)
                PacketUtilities.WriteToStream(oStream, iPacketVersion);						// Packet Version (2 byte int)
                PacketUtilities.WriteToStream(oStream, iItemID);								// Item ID (4 byte int)
                PacketUtilities.WriteToStream(oStream, bStartXML);								    // XML Start (1 byte)
                if (sXMLData == "" && bPayload != null)
                {
                    PacketUtilities.WriteToStream(oStream, Convert.ToInt16(bPayload.Length)); // XML Length (2 byte int)
                    PacketUtilities.WriteToStream(oStream, bPayload); // XML Data (Variable)
                }
                else
                {
                    PacketUtilities.WriteToStream(oStream, Convert.ToInt16(sXMLData.Length)); // XML Length (2 byte int)
                    PacketUtilities.WriteToStream(oStream, sXMLData);
                }
                PacketUtilities.WriteToStream(oStream, bEndXML);								    // XML End (1 byte)
                PacketUtilities.WriteToStream(oStream, bEOP);										// EOP
                bResult = oStream.ToArray();
                // Write in the length bytes
                bResult[1] = ((byte[])BitConverter.GetBytes(bResult.Length))[0];
                bResult[2] = ((byte[])BitConverter.GetBytes(bResult.Length))[1];
            }
            catch (System.Exception ex)
            {
                sErrMsg = "Error encoding XML update : " + ex.Message;
                bResult = null;
            }
            return bResult;
        }

        public bool Decode(byte[] bData, ref string sErrMsg)
        {
            // An XML update packet	: [SOP (0x01)][Length (2 Bytes)][Cmd (1 Char)][SubCmd (1 Char)][Packet Ver (2 Bytes)][Item ID (4 bytes)][XML Start (0x02)][XML Len (2 Bytes)][Data][XML End (0x02)][EOP (0x07)]

            bool bRet = false;
            byte bValue = (byte)0x00;
            short iLength = 0;
            MemoryStream oStream = null;

            try
            {
                oStream = new MemoryStream(bData, 0, bData.Length, false, false);
                PacketUtilities.ReadFromStream(oStream, ref bValue);
                if (bValue != bSOP)																					// SOP
                {
                    sErrMsg = "Error encoding XML update : SOP not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromStream(oStream, ref iLength);												// Length
                if (bData.Length != iLength)
                {
                    sErrMsg = "Error encoding XML update : XML update packet length is incorrect.";
                    return bRet;
                }
                if (bData[iLength - 1] != bEOP)
                {
                    sErrMsg = "Error encoding XML update : EOP was not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromStream(oStream, ref cCmd);												// Command Type (1 byte)
                PacketUtilities.ReadFromStream(oStream, ref cSubCmd);											// Sub Command (1 byte)
                PacketUtilities.ReadFromStream(oStream, ref iPacketVersion);								    // Packet Version (2 byte int)
                PacketUtilities.ReadFromStream(oStream, ref iItemID);											// Item ID (4 byte int)
                PacketUtilities.ReadFromStream(oStream, ref bValue);												// XML Start (1 byte)
                if (bValue != bStartXML)
                {
                    sErrMsg = "Error encoding XML update : Start of XML byte not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromStream(oStream, ref iLength);												// XML Length (2 byte int)
                bPayload = new byte[iLength];
                PacketUtilities.ReadFromStream(oStream, iLength, ref bPayload);								        // XML Data
                sXMLData = System.Text.ASCIIEncoding.ASCII.GetString(bPayload);
                PacketUtilities.ReadFromStream(oStream, ref bValue);												// XML End (1 byte)
                if (bValue != bEndXML)
                {
                    sErrMsg = "Error encoding XML update : End of XML byte not found.";
                    return bRet;
                }
                oStream.Close();
                oStream = null;
                bRet = true;
            }
            catch (System.Exception ex)
            {
                sErrMsg = "Error decoding XML update : " + ex.Message;
                bRet = false;
            }
            return bRet;
        }

        public string ToDisplayString()
        {
            return ToString();
        }

        public override string ToString()
        {
            StringBuilder builder = null;
            string sRet = "";

            try
            {
                builder = new StringBuilder();
                builder.Append("\r\nXML Update Data : Version "); builder.Append(iPacketVersion);
                builder.Append("\r\n    Cmd : "); builder.Append(cCmd);
                builder.Append("\r\n    Sub Cmd : "); builder.Append(cSubCmd);
                builder.Append("\r\n    Item ID : "); builder.Append(iItemID);
                builder.Append("\r\n    XML Data : \r\n"); builder.Append(sXMLData);
                sRet = builder.ToString();
            }
            catch (System.Exception ex)
            {
                sRet = "Error extracting XML update data.  Error : " + ex.Message;
            }
            return sRet;
        }
    }
}

using System;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
    public class PendentAlarmAck
    {

        public char bCmd = 'A';
        public char bSubCmd = 'A';
        public short iPacketVersion = 1;
        public int iFleetID = 0;
        public int iVehicleID = 0;
        public decimal Latitude = 0;
        public decimal Longitude = 0;
        public string Suburb = string.Empty;
        public DateTime? Timestamp;
        public bool IsTest;

        // reasonId == 69 || reasonId == 4165  test alarm
        // reasonId == 70 || reasonId == 4166  real alarm

        public PendentAlarmAck(){}

        public PendentAlarmAck(int fleetID, int vehicleID)
        {
            iFleetID = fleetID;
            iVehicleID = vehicleID;
        }

        public byte[] EncodeByStream(ref string sErrMsg)
        {
            const byte eop = 0x07;
            const byte sop = 0x01;
            byte[] result;

            try
            {
                var stream = new MemoryStream();
                PacketUtilities.WriteToStream(stream, sop);						// SOP
                PacketUtilities.WriteToStream(stream, (byte)0x00);				    // Length 1
                PacketUtilities.WriteToStream(stream, (byte)0x00);				    // Length 2
                PacketUtilities.WriteToStream(stream, bCmd);						// Packet Type (1 byte)
                PacketUtilities.WriteToStream(stream, bSubCmd);					// SubCmd (1 byte)
                PacketUtilities.WriteToStream(stream, iPacketVersion);             // Packet Version (2 byte int)
                PacketUtilities.WriteToStream(stream, iFleetID);				    // Fleet ID (4 byte int)
                PacketUtilities.WriteToStream(stream, iVehicleID);				    // Vehicle ID (4 byte int)
                PacketUtilities.WriteToStream(stream, eop);						// End of Packet (1 byte)
                result = stream.ToArray();
                // Write in the length bytes
                result[1] = BitConverter.GetBytes(result.Length)[0];
                result[2] = BitConverter.GetBytes(result.Length)[1];
            }
            catch (Exception ex)
            {
                sErrMsg = "Error encoding update : " + ex.Message;
                result = null;
            }
            return result;
        }

        public byte[] Encode(ref string sErrMsg)
        {
            return EncodeByStream(ref sErrMsg);
        }

        public bool DecodeFromStream(byte[] bData, ref string sErrMsg)
        {
            const byte eop = 0x07;
            byte value = 0x00;
            var type = (char)0x00;
            short length = 0;

            try
            {
                var stream = new MemoryStream(bData, 0, bData.Length, false, false);
                PacketUtilities.ReadFromStream(stream, ref value);
                if (value != 0x01) // SOP
                {
                    sErrMsg = "Error decoding : SOP not found.";
                    return false;
                }
                PacketUtilities.ReadFromStream(stream, ref length);
                if (bData.Length != length)
                {
                    sErrMsg = "Error decoding : Packet length is incorrect.";
                    return false;
                }
                if (bData[length - 1] != eop)
                {
                    sErrMsg = "Error decoding : EOP was not found.";
                    return false;
                }
                PacketUtilities.ReadFromStream(stream, ref type); // Packet Type (1 byte)
                if (type != bCmd)
                {
                    sErrMsg = "Error decoding : Packet type is incorrect.";
                    return false;
                }
                PacketUtilities.ReadFromStream(stream, ref type); // Packet Type (1 byte)
                if (type != bSubCmd)
                {
                    sErrMsg = "Error decoding : Sub Command Type is incorrect.";
                    return false;
                }
                PacketUtilities.ReadFromStream(stream, ref iPacketVersion); // Packet Version (2 byte int)
                PacketUtilities.ReadFromStream(stream, ref iFleetID); // Fleet ID (4 byte int)
                PacketUtilities.ReadFromStream(stream, ref iVehicleID); // Vehicle ID (4 byte int)
                return true;
            }
            catch (Exception ex)
            {
                sErrMsg = "Error decoding : " + ex.Message;
                return false;
            }
        }

        public bool Decode(byte[] bData, ref string sErrMsg)
        {
            return DecodeFromStream(bData, ref sErrMsg);
        }

        public string ToDisplayString()
        {
            return ToString();
        }

        public override string ToString()
        {
            var builder = new StringBuilder();
            try
            {
                builder.Append("\r\nPendentAlarmAck Packet :");
                builder.Append("\r\n    Version : "); builder.Append(iPacketVersion);
                builder.Append("\r\n    Fleet ID : "); builder.Append(iFleetID);
                builder.Append("\r\n    Vehicle ID : "); builder.Append(iVehicleID);
            }
            catch (Exception ex)
            {
                builder.Append("\r\nError Decoding PendentAlarmAck Packet : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

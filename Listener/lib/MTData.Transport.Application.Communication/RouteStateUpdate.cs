using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
    public class RouteStateUpdate
    {
        public enum RouteStateUpdateSubCmd : byte
        {
            AddToUpdateList = (byte) 'A',
            RemoveFromUpdateList = (byte) 'R',
            ClientUpdate = (byte) 'U'
        };

        public char Cmd = 'R';
        public RouteStateUpdateSubCmd SubCmd = RouteStateUpdateSubCmd.ClientUpdate;
        public short PacketVersion = 1;
    	public int FleetID = 0;
		public int VehicleID = 0;
        public int DriverID = 0;
        public int VehicleScheduleID = 0;
        public int CheckPointIndex = 0;
        public double ActualDepartTime = 0;
        public double ScheduledDepartTime = 0;
        public double EstimatedCPArrivalTime = 0;
        public double ScheduledCPArrivalTime = 0;
        public double EstimatedCustomerArrivalTime = 0;
        public double ScheduledCustomerArrivalTime = 0;
        public double EstimatedRouteEndTime = 0;
        public double ScheduledRouteEndTime = 0;
        public List<FatigueBreakItem> ScheduledBreaks = new List<FatigueBreakItem>();
        public RouteStateUpdate()
		{

		}

        public RouteStateUpdate(RouteStateUpdateSubCmd subCmd, int fleetID)
		{
            if(subCmd != RouteStateUpdateSubCmd.AddToUpdateList && subCmd != RouteStateUpdateSubCmd.RemoveFromUpdateList)
                throw (new Exception("The sub command type supplied does not match the parameter list"));
            SubCmd = subCmd;
            FleetID = fleetID;
		}

        public RouteStateUpdate(int fleetID, int vehicleID, int driverID, int vehicleScheduleID, int checkPointIndex, double actualDepartTime, double scheduledDepartTime, double estimatedCPArrivalTime, double scheduledCPArrivalTime, double estimatedCustomerArrivalTime, double scheduledCustomerArrivalTime, double estimatedRouteEndTime, double scheduledRouteEndTime)
		{
            SubCmd = RouteStateUpdateSubCmd.ClientUpdate;
            FleetID = fleetID;
            VehicleID = vehicleID;
            DriverID = driverID;
            VehicleScheduleID = vehicleScheduleID;
            CheckPointIndex = checkPointIndex;
            ActualDepartTime = actualDepartTime;
            ScheduledDepartTime = scheduledDepartTime;
            EstimatedCPArrivalTime = estimatedCPArrivalTime;
            ScheduledCPArrivalTime = scheduledCPArrivalTime;
            EstimatedCustomerArrivalTime = estimatedCustomerArrivalTime;
            ScheduledCustomerArrivalTime = scheduledCustomerArrivalTime;
            EstimatedRouteEndTime = estimatedRouteEndTime;
            ScheduledRouteEndTime = scheduledRouteEndTime;
		}

		public byte[] EncodeByStream(ref string sErrMsg)
		{
			byte bEOP = (byte) 0x07;
			byte bSOP = (byte) 0x01;
			byte[] bResult = null;
			MemoryStream oStream = null;

			try
			{
				oStream = new MemoryStream();
				PacketUtilities.WriteToStream(oStream, bSOP);						        // SOP
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);				        // Length 1
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);				        // Length 2
				PacketUtilities.WriteToStream(oStream, Cmd);						        // Packet Type (1 byte)
                PacketUtilities.WriteToStream(oStream, (byte)SubCmd);				        // Sub Command (1 byte)
                PacketUtilities.WriteToStream(oStream, PacketVersion);                      // Packet Version (2 byte int)
                PacketUtilities.WriteToStream(oStream, FleetID);					        // Fleet ID (4 byte int)
                if (SubCmd == RouteStateUpdateSubCmd.ClientUpdate)                          // If this is an update to send to the clients
                {
                    PacketUtilities.WriteToStream(oStream, VehicleID);				        // Vehicle ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, DriverID);				        // Driver ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, VehicleScheduleID);              // The time the route started (4 byte int)
                    PacketUtilities.WriteToStream(oStream, CheckPointIndex);                // The index of the checkpoint that the vehicle is currently departing (4 byte int)
                    PacketUtilities.WriteToStream(oStream, ActualDepartTime);               // The time the unit actually departed the check point (8 byte double)
                    PacketUtilities.WriteToStream(oStream, ScheduledDepartTime);            // The scheduled depart time for this check point (8 byte double)
                    PacketUtilities.WriteToStream(oStream, EstimatedCPArrivalTime);         // The estimated time the unit will arrive at the next checkpoint (8 byte double)
                    PacketUtilities.WriteToStream(oStream, ScheduledCPArrivalTime);         // The time the vehicle is scheduled to have arrived at the next check point (8 byte double)
                    PacketUtilities.WriteToStream(oStream, EstimatedCustomerArrivalTime);   // The estimated time the unit will arrive at the next customer (8 byte double)
                    PacketUtilities.WriteToStream(oStream, ScheduledCustomerArrivalTime);   // The time the vehicle is scheduled to arrive at the next customer (8 byte double)
                    PacketUtilities.WriteToStream(oStream, EstimatedRouteEndTime);          // The estimated time the unit will end the route (8 byte double)
                    PacketUtilities.WriteToStream(oStream, ScheduledRouteEndTime);          // The time the vehicle is scheduled to end the route (8 byte double)

                    if(ScheduledBreaks.Count > 0)
                    {
                        PacketUtilities.WriteToStream(oStream, (byte) 0x01);                // An indicator that there are driver break times on this packet.
                        PacketUtilities.WriteToStream(oStream, ScheduledBreaks.Count);      // The number of scheduled break (4 byte int)
                        foreach(FatigueBreakItem oBreak in ScheduledBreaks)
                        {
                            PacketUtilities.WriteToStream(oStream, oBreak.NextBreakTime);   // The scheduled break time (8 byte double)
                            PacketUtilities.WriteToStream(oStream, oBreak.BreakMinLength);  // The number of minutes required in this break (4 byte int)
                            PacketUtilities.WriteToStream(oStream, oBreak.BreakType);       // The break rule that has scheduled this break (4 byte int)
                        }
                    }
                    else
                        PacketUtilities.WriteToStream(oStream, (byte)0x00);                 // An indicator that there are NO driver break times on this packet.

                }
			    PacketUtilities.WriteToStream(oStream, bEOP);						        // End of Packet (1 byte)
				bResult = oStream.ToArray();
				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}
		public byte[] Encode(ref string sErrMsg)
		{
			int iPos = 0;
			byte bEOP = (byte) 0x07;
			byte bSOP = (byte) 0x01;
			byte[] bResult = null;
			MemoryStream oMS = new MemoryStream();
			
			try
			{
				PacketUtilities.WriteToStream(oMS, bSOP);						        // SOP
				PacketUtilities.WriteToStream(oMS, (byte) 0x00);				        // Length 1
				PacketUtilities.WriteToStream(oMS, (byte) 0x00);				        // Length 2
				PacketUtilities.WriteToStream(oMS, Cmd);						        // Packet Type (1 byte)
                PacketUtilities.WriteToStream(oMS, (byte) SubCmd);				        // Sub Command (1 byte)
                PacketUtilities.WriteToStream(oMS, PacketVersion);                      // Packet Version (2 byte int)

                PacketUtilities.WriteToStream(oMS, FleetID);                            // Fleet ID (4 byte int)
                if (SubCmd == RouteStateUpdateSubCmd.ClientUpdate)                      // If this is an update to send to the clients
                {
                    PacketUtilities.WriteToStream(oMS, VehicleID);				        // Vehicle ID (4 byte int)
                    PacketUtilities.WriteToStream(oMS, DriverID);				        // Driver ID (4 byte int)
                    PacketUtilities.WriteToStream(oMS, VehicleScheduleID);              // The time the route started (4 byte int)
                    PacketUtilities.WriteToStream(oMS, CheckPointIndex);                // The index of the checkpoint that the vehicle is currently departing (4 byte int)
                    PacketUtilities.WriteToStream(oMS, ActualDepartTime);               // The time the unit actually departed the check point (8 byte double)
                    PacketUtilities.WriteToStream(oMS, ScheduledDepartTime);            // The scheduled depart time for this check point (8 byte double)
                    PacketUtilities.WriteToStream(oMS, EstimatedCPArrivalTime);         // The estimated time the unit will arrive at the next checkpoint (8 byte double)
                    PacketUtilities.WriteToStream(oMS, ScheduledCPArrivalTime);         // The time the vehicle is scheduled to have arrived at the next check point (8 byte double)
                    PacketUtilities.WriteToStream(oMS, EstimatedCustomerArrivalTime);   // The estimated time the unit will arrive at the next customer (8 byte double)
                    PacketUtilities.WriteToStream(oMS, ScheduledCustomerArrivalTime);   // The time the vehicle is scheduled to arrive at the next customer (8 byte double)
                    PacketUtilities.WriteToStream(oMS, EstimatedRouteEndTime);          // The estimated time the unit will end the route (8 byte double)
                    PacketUtilities.WriteToStream(oMS, ScheduledRouteEndTime);          // The time the vehicle is scheduled to end the route (8 byte double)
                    if (ScheduledBreaks.Count > 0)
                    {
                        PacketUtilities.WriteToStream(oMS, (byte)0x01);                 // An indicator that there are driver break times on this packet.
                        PacketUtilities.WriteToStream(oMS, ScheduledBreaks.Count);      // The number of scheduled break (4 byte int)
                        foreach (FatigueBreakItem oBreak in ScheduledBreaks)
                        {
                            PacketUtilities.WriteToStream(oMS, oBreak.NextBreakTime);   // The scheduled break time (8 byte double)
                            PacketUtilities.WriteToStream(oMS, oBreak.BreakMinLength);  // The number of minutes required in this break (4 byte int)
                            PacketUtilities.WriteToStream(oMS, oBreak.BreakType);       // The break rule that has scheduled this break (4 byte int)
                        }
                    }
                    else
                        PacketUtilities.WriteToStream(oMS, (byte)0x00);                 // An indicator that there are NO driver break times on this packet.
                }
			    PacketUtilities.WriteToStream(oMS, bEOP);                               // End of Packet (1 byte)

				bResult = oMS.ToArray();
				iPos = bResult.Length;

				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(iPos))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(iPos))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}
		public bool DecodeFromStream(byte[] bData, ref string sErrMsg)
		{
			bool bRet = false;
		    byte bTemp = (byte) 0x00;
			byte bEOP = (byte) 0x07;
			byte bValue = (byte) 0x00;
			char cType = (char) 0x00;
			short iLength = 0;
			MemoryStream oStream = null;
            int iBreakCount = 0;

			try
			{
				oStream = new MemoryStream(bData, 0, bData.Length, false, false);
				PacketUtilities.ReadFromStream(oStream, ref bValue);													
				if (bValue != (byte) 0x01)                                                      // SOP
				{
					sErrMsg = "Error decoding : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref iLength);                           // Length
				if (bData.Length != iLength)
				{
					sErrMsg = "Error decoding : Packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
					sErrMsg = "Error decoding : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref cType);                             // Packet Type (1 byte)
				if (cType != Cmd)
				{
					sErrMsg = "Error decoding : Packet type is incorrect.";
					return bRet;
				}
                PacketUtilities.ReadFromStream(oStream, ref bTemp);                             // Sub Command (1 byte)
                if (bTemp != (byte)RouteStateUpdateSubCmd.AddToUpdateList && bTemp != (byte)RouteStateUpdateSubCmd.ClientUpdate && bTemp != (byte)RouteStateUpdateSubCmd.RemoveFromUpdateList)
                {
                    sErrMsg = "Error decoding : Packet sub command is not valid.";
                    return bRet;
                }
                SubCmd = (RouteStateUpdateSubCmd)bTemp;
				PacketUtilities.ReadFromStream(oStream, ref this.PacketVersion);                // Packet Version (2 byte int)

                PacketUtilities.ReadFromStream(oStream, ref this.FleetID);                      // Fleet ID (4 byte int)
                if (SubCmd == RouteStateUpdateSubCmd.ClientUpdate)
                {
                    PacketUtilities.ReadFromStream(oStream, ref VehicleID);                     // Vehicle ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref DriverID);				        // Driver ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref VehicleScheduleID);             // The time the route started (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref CheckPointIndex);               // The index of the checkpoint that the vehicle is currently departing (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref ActualDepartTime);              // The time the unit actually departed the check point (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref ScheduledDepartTime);           // The scheduled depart time for this check point (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref EstimatedCPArrivalTime);        // The estimated time the unit will arrive at the next checkpoint (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref ScheduledCPArrivalTime);        // The time the vehicle is scheduled to have arrived at the next check point (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref EstimatedCustomerArrivalTime);  // The estimated time the unit will arrive at the next customer (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref ScheduledCustomerArrivalTime);  // The time the vehicle is scheduled to arrive at the next customer (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref EstimatedRouteEndTime);         // The estimated time the unit will end the route (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref ScheduledRouteEndTime);         // The time the vehicle is scheduled to end the route (8 byte double)
                    PacketUtilities.ReadFromStream(oStream, ref bTemp);                         // An indicator that there are driver break times on this packet.
                    if(bTemp == (byte) 0x01)
                    {
                        ScheduledBreaks = new List<FatigueBreakItem>();
                        PacketUtilities.ReadFromStream(oStream, ref iBreakCount);               // The number of scheduled breaks (4 byte int)
                        for(int X = 0; X < iBreakCount; X++)
                        {
                            FatigueBreakItem oBreak = new FatigueBreakItem();
                            PacketUtilities.ReadFromStream(oStream, ref oBreak.NextBreakTime);  // The scheduled break time (8 byte double)
                            PacketUtilities.ReadFromStream(oStream, ref oBreak.BreakMinLength); // The number of minutes required in this break (4 byte int)
                            PacketUtilities.ReadFromStream(oStream, ref oBreak.BreakType);      // The break rule that has scheduled this break (4 byte int)
                            ScheduledBreaks.Add(oBreak);
                        }
                    }
                }
				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error decoding : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}
		public bool Decode(byte[] bData, ref string sErrMsg)
		{
			bool bRet = false;
            byte bTemp = (byte)0x00;
			byte bEOP = (byte) 0x07;
			char cType = (char) 0x00;
			int iPos = 0;
			short iLength = 0;
            int iBreakCount = 0;

			try
			{
				if (bData[iPos++] != (byte) 0x01)														    // SOP
				{
					sErrMsg = "Error decoding : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iLength);					        // Length
				if (bData.Length != iLength)
				{
                    sErrMsg = "Error decoding : Packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
                    sErrMsg = "Error decoding : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref cType);					        // Packet Type (1 byte)
                if (cType != Cmd)
				{
                    sErrMsg = "Error decoding : Packet type is incorrect.";
					return bRet;
				}
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bTemp);					        // Sub Command (1 byte)
                if (bTemp != (byte)RouteStateUpdateSubCmd.AddToUpdateList && bTemp != (byte)RouteStateUpdateSubCmd.ClientUpdate && bTemp != (byte)RouteStateUpdateSubCmd.RemoveFromUpdateList)
				{
                    sErrMsg = "Error decoding : Packet sub command is not valid.";
					return bRet;
				}
                SubCmd = (RouteStateUpdateSubCmd)bTemp;

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.PacketVersion);	            // Packet Version (2 byte int)
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.FleetID);					    // Fleet ID (4 byte int)
                if (SubCmd == RouteStateUpdateSubCmd.ClientUpdate)
                {
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref VehicleID);                     // Vehicle ID (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref DriverID);				         // Driver ID (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref VehicleScheduleID);             // The time the route started (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref CheckPointIndex);               // The index of the checkpoint that the vehicle is currently departing (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref ActualDepartTime);              // The time the unit actually departed the check point (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref ScheduledDepartTime);           // The scheduled depart time for this check point (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref EstimatedCPArrivalTime);        // The estimated time the unit will arrive at the next checkpoint (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref ScheduledCPArrivalTime);        // The time the vehicle is scheduled to have arrived at the next check point (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref EstimatedCustomerArrivalTime);  // The estimated time the unit will arrive at the next customer (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref ScheduledCustomerArrivalTime);  // The time the vehicle is scheduled to arrive at the next customer (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref EstimatedRouteEndTime);         // The estimated time the unit will end the route (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref ScheduledRouteEndTime);         // The time the vehicle is scheduled to end the route (8 byte double)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bTemp);                         // An indicator that there are driver break times on this packet.
                    if (bTemp == (byte)0x01)
                    {
                        ScheduledBreaks = new List<FatigueBreakItem>();
                        PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iBreakCount);               // The number of scheduled breaks (4 byte int)
                        for (int X = 0; X < iBreakCount; X++)
                        {
                            FatigueBreakItem oBreak = new FatigueBreakItem();
                            PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref oBreak.NextBreakTime);  // The scheduled break time (8 byte double)
                            PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref oBreak.BreakMinLength); // The number of minutes required in this break (4 byte int)
                            PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref oBreak.BreakType);      // The break rule that has scheduled this break (4 byte int)
                            ScheduledBreaks.Add(oBreak);
                        }
                    }
                }
				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding live update : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}
		public string ToDisplayString()
		{
			return	this.ToString();
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
                if (SubCmd == RouteStateUpdateSubCmd.AddToUpdateList || SubCmd == RouteStateUpdateSubCmd.RemoveFromUpdateList)
                {
                    if (SubCmd == RouteStateUpdateSubCmd.AddToUpdateList)
                        builder.Append("\r\nRoute State AddToUpdateList Packet :");
                    else
                        builder.Append("\r\nRoute State RemoveFromUpdateList Packet :");
                    builder.Append("\r\n    Version : "); builder.Append(PacketVersion);
                    builder.Append("\r\n    Fleet ID : "); builder.Append(FleetID);
                }
                else if (SubCmd == RouteStateUpdateSubCmd.ClientUpdate)
                {
                    builder.Append("\r\nRoute State ClientUpdate Packet :");
                    builder.Append("\r\n    Version : "); builder.Append(PacketVersion);
                    builder.Append("\r\n    Fleet ID : "); builder.Append(FleetID);
                    builder.Append("\r\n    Vehicle ID : "); builder.Append(VehicleID);
                    builder.Append("\r\n    Driver ID : "); builder.Append(DriverID);
                    builder.Append("\r\n    VehicleScheduleID : "); builder.Append(VehicleScheduleID);
                    builder.Append("\r\n    CheckPointIndex : "); builder.Append(CheckPointIndex);
                    builder.Append("\r\n    ActualDepartTime : ");
                    if(ActualDepartTime > 0)
                         builder.Append(DateTime.FromOADate(ActualDepartTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    ScheduledDepartTime : ");
                    if (ScheduledDepartTime > 0)
                        builder.Append(DateTime.FromOADate(ScheduledDepartTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    EstimatedCPArrivalTime : ");
                    if (EstimatedCPArrivalTime > 0)
                        builder.Append(DateTime.FromOADate(EstimatedCPArrivalTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    ScheduledCPArrivalTime : ");
                    if (ScheduledCPArrivalTime > 0)
                        builder.Append(DateTime.FromOADate(ScheduledCPArrivalTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    EstimatedCustomerArrivalTime : ");
                    if (EstimatedCustomerArrivalTime > 0)
                        builder.Append(DateTime.FromOADate(EstimatedCustomerArrivalTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    ScheduledCustomerArrivalTime : ");
                    if (ScheduledCustomerArrivalTime > 0)
                        builder.Append(DateTime.FromOADate(ScheduledCustomerArrivalTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    EstimatedRouteEndTime : ");
                    if (EstimatedRouteEndTime > 0)
                        builder.Append(DateTime.FromOADate(EstimatedRouteEndTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    ScheduledRouteEndTime : ");
                    if (ScheduledRouteEndTime > 0)
                        builder.Append(DateTime.FromOADate(ScheduledRouteEndTime).ToString("dd/MM/yyyy HH:mm:ss"));
                    if (ScheduledBreaks.Count > 0)
                    {
                        builder.Append("\r\n    Driver Breaks Required : " + ScheduledBreaks.Count.ToString());
                        foreach (FatigueBreakItem oBreak in ScheduledBreaks)
                        {
                            builder.Append("\r\n        Next Break Time : "); builder.Append(DateTime.FromOADate(oBreak.NextBreakTime).ToString("dd/MM/yyyy HH:mm:ss"));
                            builder.Append("\r\n        Break Length : "); builder.Append(oBreak.BreakMinLength);
                            builder.Append("\r\n        Break Type : "); builder.Append(oBreak.BreakType);
                            builder.Append("\r\n");
                        }
                    }
                    else
                        builder.Append("\r\n    Driver Breaks Required : None");
                }
			}
			catch(System.Exception ex)
			{
                builder.Append("\r\nError Decoding Route State Update Packet : " + ex.Message);
			}
			return builder.ToString();
		}
    }
}

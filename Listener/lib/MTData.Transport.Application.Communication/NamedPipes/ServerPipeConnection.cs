using System;
using System.Collections.Generic;
using System.Text;
using log4net;

namespace MTData.Transport.Application.Communication.NamedPipes
{
    public sealed class ServerPipeConnection : APipeConnection
    {
        private static ILog _log = LogManager.GetLogger(typeof(ServerPipeConnection));
        public void Disconnect()
        {
            CheckIfDisposed();
            NamedPipeWrapper.Disconnect(this.Handle);
        }
        public override void Close()
        {
            CheckIfDisposed();
            NamedPipeWrapper.Close(this.Handle);
        }
        public override void Connect()
        {
            CheckIfDisposed();
            NamedPipeWrapper.Connect(this.Handle);
        }
        public ServerPipeConnection(string name, uint outBuffer, uint inBuffer, int maxReadBytes)
        {
            this.Name = name;
            this.Handle = NamedPipeWrapper.Create(name, outBuffer, inBuffer, true);
            this.maxReadBytes = maxReadBytes;
        }
        public ServerPipeConnection(string name, uint outBuffer, uint inBuffer, int maxReadBytes, bool secure)
        {
            this.Name = name;
            this.Handle = NamedPipeWrapper.Create(name, outBuffer, inBuffer, secure);
            this.maxReadBytes = maxReadBytes;
        }
        ~ServerPipeConnection()
        {
            Dispose(false);
        }
    }
}

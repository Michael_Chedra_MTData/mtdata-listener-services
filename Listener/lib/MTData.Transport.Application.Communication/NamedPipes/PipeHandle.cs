using System;
using System.Collections.Generic;
using System.Text;
using log4net;
using MTData.Transport.Application.Communication.InterProcessComm;

namespace MTData.Transport.Application.Communication.NamedPipes
{
    public sealed class PipeHandle
    {
        private static ILog _log = LogManager.GetLogger(typeof(PipeHandle));
        public IntPtr Handle;
        public InterProcessConnectionState State;
        public PipeHandle(int hnd)
        {
            this.Handle = new IntPtr(hnd);
            this.State = InterProcessConnectionState.NotSet;
        }
        public PipeHandle(int hnd, InterProcessConnectionState state)
        {
            this.Handle = new IntPtr(hnd);
            this.State = state;
        }
        public PipeHandle()
        {
            this.Handle = new IntPtr(NamedPipeNative.INVALID_HANDLE_VALUE);
            this.State = InterProcessConnectionState.NotSet;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using log4net;

namespace MTData.Transport.Application.Communication.NamedPipes
{
    public sealed class ServerNamedPipe : IDisposable
    {
        private static ILog _log = LogManager.GetLogger(typeof(ServerNamedPipe));
        internal Thread PipeThread;
        internal ServerPipeConnection PipeConnection;
        internal bool Listen = true;
        internal DateTime LastAction;
        private bool disposed = false;
        private PipeManager _pipeManager;

        public PipeManager Manager
        {
            get { return _pipeManager; }
        }

        private void PipeListener()
        {
            CheckIfDisposed();
            try
            {
                Listen = _pipeManager.Listen;
                _log.Info("Pipe " + this.PipeConnection.NativeHandle.ToString() + ": new pipe started");
                while (Listen)
                {
                    LastAction = DateTime.Now;
                    string request = PipeConnection.Read();
                    LastAction = DateTime.Now;
                    if (request.Trim() != "")
                    {
                        PipeConnection.Write(_pipeManager.HandleRequest(request));
                        _log.Info("Pipe " + this.PipeConnection.NativeHandle.ToString() + ": request handled");
                    }
                    else
                    {
                        PipeConnection.Write("Error: bad request");
                    }
                    LastAction = DateTime.Now;
                    PipeConnection.Disconnect();
                    if (Listen)
                    {
                        _log.Info("Pipe " + this.PipeConnection.NativeHandle.ToString() + ": listening");
                        Connect();
                    }
                    _pipeManager.WakeUp();
                }
            }
            catch (System.Threading.ThreadAbortException) { }
            catch (System.Threading.ThreadStateException) { }
            catch (Exception ex)
            {
                _log.Error(this.GetType() + ".PipeListener(PipeManager pipeManager)", ex);
            }
            finally
            {
                this.Close();
            }
        }
        internal void Connect()
        {
            CheckIfDisposed();
            PipeConnection.Connect();
        }
        
        internal void Close()
        {
            CheckIfDisposed();
            this.Listen = false;
            _pipeManager.RemoveServerChannel(this.PipeConnection.NativeHandle);
            this.Dispose();
        }
        internal void Start()
        {
            CheckIfDisposed();
            PipeThread.Start();
        }
        private void CheckIfDisposed()
        {
            if (this.disposed)
            {
                throw new ObjectDisposedException("ServerNamedPipe");
            }
        }
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        private void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                PipeConnection.Dispose();
                if (PipeThread != null)
                {
                    try
                    {
                        PipeThread.Abort();
                    }
                    catch (System.Threading.ThreadAbortException) { }
                    catch (System.Threading.ThreadStateException) { }
                    catch (Exception ex)
                    {
                        _log.Error(this.GetType() + ".Dispose(bool disposing)", ex);
                    }
                }
            }
            _pipeManager = null;
            disposed = true;
        }
        ~ServerNamedPipe()
        {
            Dispose(false);
        }
        internal ServerNamedPipe(string name, uint outBuffer, uint inBuffer, int maxReadBytes, bool secure)
        {
            PipeConnection = new ServerPipeConnection(name, outBuffer, inBuffer, maxReadBytes, secure);
            _pipeManager = new PipeManager();
            PipeThread = new Thread(new ThreadStart(PipeListener));
            PipeThread.IsBackground = true;
            PipeThread.Name = "Pipe Thread " + this.PipeConnection.NativeHandle.ToString();
            LastAction = DateTime.Now;
        }
    }
}

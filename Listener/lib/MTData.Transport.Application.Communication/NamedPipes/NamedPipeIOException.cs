using System;
using System.Runtime.Serialization;
using log4net;
using MTData.Transport.Application.Communication.InterProcessComm;

namespace MTData.Transport.Application.Communication.NamedPipes
{
    public class NamedPipeIOException : InterProcessIOException
    {
        private static ILog _log = LogManager.GetLogger(typeof(NamedPipeIOException));
        public NamedPipeIOException(String text)
            : base(text)
        {
        }
        public NamedPipeIOException(String text, uint errorCode)
            : base(text)
        {
            this.ErrorCode = errorCode;
            if (errorCode == NamedPipeNative.ERROR_CANNOT_CONNECT_TO_PIPE)
            {
                this.IsServerAvailable = false;
            }
        }
        protected NamedPipeIOException(SerializationInfo info, StreamingContext context)
            : base(info, context)
        {
        }
    }
}

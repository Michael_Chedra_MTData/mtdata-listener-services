using System;
using System.Security;
using System.Runtime.InteropServices;
using log4net;

namespace MTData.Transport.Application.Communication.NamedPipes
{
    // This class exposes kernel32.dll methods for named pipes communication.
    [SuppressUnmanagedCodeSecurity]
    public sealed class NamedPipeNative
    {
        private static ILog _log = LogManager.GetLogger(typeof(NamedPipeNative));
        public const uint PIPE_ACCESS_OUTBOUND = 0x00000002;
        public const uint PIPE_ACCESS_DUPLEX = 0x00000003;
        public const uint PIPE_ACCESS_INBOUND = 0x00000001;
        public const uint PIPE_WAIT = 0x00000000;
        public const uint PIPE_NOWAIT = 0x00000001;
        public const uint PIPE_READMODE_BYTE = 0x00000000;
        public const uint PIPE_READMODE_MESSAGE = 0x00000002;
        public const uint PIPE_TYPE_BYTE = 0x00000000;
        public const uint PIPE_TYPE_MESSAGE = 0x00000004;
        public const uint PIPE_CLIENT_END = 0x00000000;
        public const uint PIPE_SERVER_END = 0x00000001;
        public const uint PIPE_UNLIMITED_INSTANCES = 255;
        public const uint NMPWAIT_WAIT_FOREVER = 0xffffffff;
        public const uint NMPWAIT_NOWAIT = 0x00000001;
        public const uint NMPWAIT_USE_DEFAULT_WAIT = 0x00000000;
        public const uint GENERIC_READ = (0x80000000);
        public const uint GENERIC_WRITE = (0x40000000);
        public const uint GENERIC_EXECUTE = (0x20000000);
        public const uint GENERIC_ALL = (0x10000000);
        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;
        public const uint OPEN_ALWAYS = 4;
        public const uint TRUNCATE_EXISTING = 5;
        public const int INVALID_HANDLE_VALUE = -1;
        public const ulong ERROR_SUCCESS = 0;
        public const ulong ERROR_CANNOT_CONNECT_TO_PIPE = 2;
        public const ulong ERROR_PIPE_BUSY = 231;
        public const ulong ERROR_NO_DATA = 232;
        public const ulong ERROR_PIPE_NOT_CONNECTED = 233;
        public const ulong ERROR_MORE_DATA = 234;
        public const ulong ERROR_PIPE_CONNECTED = 535;
        public const ulong ERROR_PIPE_LISTENING = 536;
        // Creates an instance of a named pipe and returns a handle for subsequent pipe operations.

        // lpName => Pointer to the null-terminated string that uniquely identifies the pipe.
        // dwOpenMode => Pipe access mode, the overlapped mode, the write-through mode, and the security access mode of the pipe handle.
        // dwPipeMode => Type, read, and wait modes of the pipe handle.
        // nMaxInstances => Maximum number of instances that can be created for this pipe.
        // nOutBufferSize => Number of bytes to reserve for the output buffer.
        // nInBufferSize => Number of bytes to reserve for the input buffer.
        // nDefaultTimeOut => Default time-out value, in milliseconds.
        // pipeSecurityDescriptor => Pointer to a SECURITY_ATTRIBUTES object that specifies a security descriptor for the new named pipe.
        
        // If the function succeeds, the return value is a handle to the server end of a named pipe instance.
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr CreateNamedPipe(
            String lpName,									// pipe name
            uint dwOpenMode,								// pipe open mode
            uint dwPipeMode,								// pipe-specific modes
            uint nMaxInstances,							// maximum number of instances
            uint nOutBufferSize,						// output buffer size
            uint nInBufferSize,							// input buffer size
            uint nDefaultTimeOut,						// time-out interval
            IntPtr pipeSecurityDescriptor		// SD
            );
        

        // Enables a named pipe server process to wait for a client 
        // process to connect to an instance of a named pipe.

        // hHandle => Handle to the server end of a named pipe instance.
        // lpOverlapped => Pointer to an Overlapped object.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ConnectNamedPipe(
            IntPtr hHandle,									// handle to named pipe
            Overlapped lpOverlapped					// overlapped structure
            );
        

        // Connects to a message-type pipe (and waits if an instance of the 
        // pipe is not available), writes to and reads from the pipe, and then closes the pipe.

        // lpNamedPipeName => Pointer to a null-terminated string specifying the pipe name.
        // lpInBuffer => Pointer to the buffer containing the data written to the pipe.
        // nInBufferSize => Size of the write buffer, in bytes.
        // lpOutBuffer => Pointer to the buffer that receives the data read from the pipe.
        // nOutBufferSize => Size of the read buffer, in bytes.
        // lpBytesRead => Pointer to a variable that receives the number of bytes read from the pipe.
        // nTimeOut => Number of milliseconds to wait for the named pipe to be available.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CallNamedPipe(
            string lpNamedPipeName,
            byte[] lpInBuffer,
            uint nInBufferSize,
            byte[] lpOutBuffer,
            uint nOutBufferSize,
            byte[] lpBytesRead,
            int nTimeOut
            );
        

        // Creates or opens a file, directory, physical disk, volume, console buffer, 
        // tape drive, communications resource, mailslot, or named pipe.

        // lpFileName => Pointer to a null-terminated string that 
        // specifies the name of the object to create or open.
        // dwDesiredAccess => Access to the object (reading, writing, or both).
        // dwShareMode => Sharing mode of the object (reading, writing, both, or neither).
        // attr => Pointer to a SecurityAttributes object that determines whether the returned handle can be inherited by child processes.
        // dwCreationDisposition => Action to take on files that exist and what action to take if the files do not exist.
        // dwFlagsAndAttributes => File attributes and flags.
        // hTemplateFile => Handle to a template file, with the GENERIC_READ access right.
        // If the function succeeds, the return value is an open handle to the specified file.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern IntPtr CreateFile(
            String lpFileName,						  // file name
            uint dwDesiredAccess,					  // access mode
            uint dwShareMode,								// share mode
            SecurityAttributes attr,				// SD
            uint dwCreationDisposition,			// how to create
            uint dwFlagsAndAttributes,			// file attributes
            uint hTemplateFile);					  // handle to template file
        

        // Reads data from a file, starting at the position indicated by the file pointer.

        // hHandle => Handle to the file to be read.
        // lpBuffer => Pointer to the buffer that receives the data read from the file.
        // nNumberOfBytesToRead => Number of bytes to be read from the file.
        // lpNumberOfBytesRead => Pointer to the variable that receives the number of bytes read.
        // lpOverlapped => Pointer to an Overlapped object.
        // The ReadFile function returns when one of the following 
        // conditions is met: a write operation completes on the write end of 
        // the pipe, the number of bytes requested has been read, or an error occurs.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool ReadFile(
            IntPtr hHandle,											// handle to file
            byte[] lpBuffer,								// data buffer
            uint nNumberOfBytesToRead,			// number of bytes to read
            byte[] lpNumberOfBytesRead,			// number of bytes read
            uint lpOverlapped								// overlapped buffer
            );
        

        // Writes data to a file at the position specified by the file pointer.

        // hHandle => Handle to the file.
        // lpBuffer => Pointer to the buffer containing the data to be written to the file.
        // nNumberOfBytesToWrite => 
        // lpNumberOfBytesWritten => Pointer to the variable that receives the number of bytes written.
        // lpOverlapped => Pointer to an Overlapped object.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WriteFile(
            IntPtr hHandle,											// handle to file
            byte[] lpBuffer,							  // data buffer
            uint nNumberOfBytesToWrite,			// number of bytes to write
            byte[] lpNumberOfBytesWritten,	// number of bytes written
            uint lpOverlapped								// overlapped buffer
            );

        // Retrieves information about a specified named pipe.

        // hHandle => Handle to the named pipe for which information is wanted.
        // lpState => Pointer to a variable that indicates the current state of the handle.
        // lpCurInstances => Pointer to a variable that receives the number of current pipe instances.
        // lpMaxCollectionCount => Pointer to a variable that receives the maximum number of bytes to be collected on the client's 
        //                          computer before transmission to the server.
        // lpCollectDataTimeout => Pointer to a variable that receives the maximum time, in milliseconds, that can pass before a remote 
        //                          named pipe transfers information over the network.
        // lpUserName => Pointer to a buffer that receives the null-terminated string containing the user name string associated 
        //                  with the client application. 
        // nMaxUserNameSize => Size of the buffer specified by the lpUserName parameter.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool GetNamedPipeHandleState(
            IntPtr hHandle,
            IntPtr lpState,
            ref uint lpCurInstances,
            IntPtr lpMaxCollectionCount,
            IntPtr lpCollectDataTimeout,
            IntPtr lpUserName,
            IntPtr nMaxUserNameSize
            );
        

        // Cancels all pending input and output (I/O) operations that were 
        // issued by the calling thread for the specified file handle.

        // hHandle => Handle to a file.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CancelIo(
            IntPtr hHandle
            );
        

        // Waits until either a time-out interval elapses or an instance of the specified named pipe is available for connection.

        // name => Pointer to a null-terminated string that specifies the name of the named pipe.
        // timeout => Number of milliseconds that the function will wait for an instance of the named pipe to be available.
        // If an instance of the pipe is available before the time-out interval elapses, the return value is nonzero.

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool WaitNamedPipe(
            String name,
            int timeout);
        
        // Retrieves the calling thread's last-error code value.
        // The return value is the calling thread's last-error code value.

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern uint GetLastError();
        
        // Flushes the buffers of the specified file and causes all buffered data to be written to the file.
        // hHandle => Handle to an open file.
        // If the function succeeds, the return value is nonzero.

        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool FlushFileBuffers(
            IntPtr hHandle);
        
        // Disconnects the server end of a named pipe instance from a client process.
        // hHandle => Handle to an instance of a named pipe.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool DisconnectNamedPipe(
            IntPtr hHandle);

        // Sets the read mode and the blocking mode of the specified named pipe.
        
        // If the specified handle is to the client end of a named pipe and if 
        // the named pipe server process is on a remote computer, the function 
        // can also be used to control local buffering.
        
        // hHandle => Handle to the named pipe instance.
        // mode => Pointer to a variable that supplies the new mode.
        // cc => Pointer to a variable that specifies the maximum number of bytes collected on the client computer before transmission to the server.
        // cd => Pointer to a variable that specifies the maximum time, in milliseconds, that can pass before a remote named pipe transfers information over the network.
        // If the function succeeds, the return value is nonzero.
        
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool SetNamedPipeHandleState(
            IntPtr hHandle,
            ref uint mode,
            IntPtr cc,
            IntPtr cd);
        
        // Closes an open object handle.
        [DllImport("kernel32.dll", SetLastError = true)]
        public static extern bool CloseHandle(
            IntPtr hHandle);

        // Sets the security descriptor attributes        
        [DllImport("Advapi32.dll", SetLastError = true)]
        public static extern bool SetSecurityDescriptorDacl(ref SECURITY_DESCRIPTOR sd, bool bDaclPresent, IntPtr Dacl, bool bDaclDefaulted);

        // Initializes a SECURITY_DESCRIPTOR structure.
        [DllImport("Advapi32.dll", SetLastError = true)]
        public static extern bool InitializeSecurityDescriptor(out SECURITY_DESCRIPTOR sd, int dwRevision);
        // Private constructor.
        private NamedPipeNative() { }
    }
    
    // Security Descriptor structure
    [StructLayout(LayoutKind.Sequential)]
    public struct SECURITY_DESCRIPTOR
    {
        private byte Revision;
        private byte Sbz1;
        private ushort Control;
        private IntPtr Owner;
        private IntPtr Group;
        private IntPtr Sacl;
        private IntPtr Dacl;
    }
    // Security Attributes structure.
    [StructLayout(LayoutKind.Sequential)]
    public struct SECURITY_ATTRIBUTES
    {
        public int nLength;
        public IntPtr lpSecurityDescriptor;
        public bool bInheritHandle;
    }
    // This class is used as a dummy parameter only.
    [StructLayout(LayoutKind.Sequential)]
    public class Overlapped
    {
    }
    // This class is used as a dummy parameter only.
    [StructLayout(LayoutKind.Sequential)]
    public class SecurityAttributes
    {
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Application.Communication
{
    public class FatigueBreakItem
    {
        public double NextBreakTime = 0;
        public int BreakMinLength = 0;
        public int BreakType = 0;
    }
}

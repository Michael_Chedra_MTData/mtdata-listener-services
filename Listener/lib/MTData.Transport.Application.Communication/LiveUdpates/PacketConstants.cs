using System;
using System.Text;

namespace MTData.Transport.Application.Communication.LiveUdpates
{
    /// <summary>
    /// This class will contain the constants for the command definitions common to both the Interpeeter and creater
    /// </summary>
    public class PacketConstants
    {
        public const string COMMAND_STD_GROUP_UPDATE = "GROUPUPDATE";
        public const string COMMAND_STD_KEEPALIVE_TEST = "TEST";
        public const string COMMAND_STD_KEEPALIVE_RESPONSE = "Alive";
        public const string COMMAND_STD_PING_REQUEST = "PING";
        public const string COMMAND_STD_PING_RESPONSE = "PONG";

        public const string COMMAND_STD_DISCONNECT = "Disconnect";
        public const string COMMAND_STD_REFRESH_SERVER_WAYPOINTS = "RefreshSWPList";

        public const string COMMAND_COMMA_CONNECT = "Connect";
        public const string COMMAND_COMMA_CONFIG_CHECK = "ConfigCheck";
        public const string COMMAND_COMMA_RESET_UNIT = "ResetUnit";
        public const string COMMAND_COMMA_DRIVE_OUTPUT = "Outputs";
        public const string COMMAND_COMMA_RAPID_REQUEST = "Update";
        public const string COMMAND_COMMA_GPS_HISTORY_REQUEST = "History";
        public const string COMMAND_COMMA_ACCIDENT_BUFFER_REQUEST = "AccidentBuffer";
        public const string COMMAND_COMMA_CLIENT_BROADCAST_REQUEST = "ClientBroadcast";
        public const string COMMAND_COMMA_BROADCAST_MESSAGE = "Broadcast";
        public const string COMMAND_COMMA_ADD_TO_WATCHLIST = "UpdateWatchList";
        public const string COMMAND_COMMA_EXTENDED_HISTORY_REQUEST = "ExtendedHistoryRequest";
        public const string COMMAND_COMMA_EXTENDED_HISTORY_REQUEST_COMPLETE = "ExtendedHistoryRequestComplete";
        public const string COMMAND_COMMA_DVR_ALARM = "DvrAlarm";
        public const string COMMAND_COMMA_DVR_ALARM_CONTENT = "DvrAlarmContent";
        public const string COMMAND_COMMA_DVR_ERROR = "DvrError";
        public const string COMMAND_COMMA_DVR_REQUEST = "DvrRequest";
        public const string COMMAND_COMMA_DVR_REQUEST_CONTENT = "DvrRequestContent";
        public const string COMMAND_COMMA_DVR_STATUS = "DvrStatus";

        public const string COMMAND_COMMA_MAIL = "Mail";

        public const string COMMAND_UNDERSCORE_VEHICLE = "VEHICLE";
        public const string COMMAND_UNDERSCORE_CONFIG = "CONFIG_CHANGED";

        public const string VEHICLE_DELETE = "DELETE";
        public const string VEHICLE_CREATE = "CREATE";
        public const string VEHICLE_DRIVERCHANGED = "DRIVERCHANGED";
        public const string VEHICLE_ACTIVESTATECHANGED = "ACTIVESTATECHANGED";
        public const string VEHICLE_MDTMESSAGE = "MDTMESSAGE";
        public const string VEHICLE_MESSAGERECIEVED = "MESSAGERECIEVED";
        public const string VEHICLE_MESSAGEQUEUED = "MESSAGEQUEUED";
        public const string VEHICLE_MESSAGENOTQUEUED = "MESSAGENOTQUEUED";
        public const string VEHICLE_SETATLUNCHSTATE = "SETATLUNCH";
        public const string VEHICLE_UNSETATLUNCHSTATE = "UNSETATLUNCH";
        public const string VEHICLE_CHANGED = "VEHICLECHANGED";


        public const char REASON_CODE_GROUP_UPDATE = 'R';
        public const char SETPOINT_GROUP_UPDATE = 'S';

        protected const string PENDENT_ALARM_ACK = "AA";
        protected const string BYTEARRAY_LIVEUPDATE = "LU";
        protected const string BYTEARRAY_TIMEZONE_INFO_CHANGED = "TI";
        protected const string BYTEARRAY_TIMEZONE_TO_DRIVER_CHANGED = "TD";
        protected const string BYTEARRAY_TIMEZONE_TO_FLEET_CHANGED = "TF";
        protected const string BYTEARRAY_TIMEZONE_TO_VEHICLE_CHANGED = "TV";
        protected const string BYTEARRAY_XML_UPDATE_GENERIC = "X";
        protected const string BYTEARRAY_XML_UPDATE_SETPOINT = "XS";
        protected const string BYTEARRAY_XML_UPDATE_REASON_CODE_OVERIDE = "XR";
        protected const string BYTEARRAY_PERSISTANCE_UPDATE_ADD = "PA";
        protected const string BYTEARRAY_PERSISTANCE_UPDATE_REMOVE = "PR";
        protected const string BYTEARRAY_PERSISTANCE_UPDATE_TOCLIENT = "PU";


        protected const string BYTEARRAY_ECM_LIVE_UPDATE_ADD = "EA";
        protected const string BYTEARRAY_ECM_LIVE_UPDATE_REMOVE = "ER";
        protected const string BYTEARRAY_ECM_LIVE_UPDATE_TOCLIENT = "EU";
        protected const string BYTEARRAY_ROUTE_STATE_UPDATE_ADD = "RA";
        protected const string BYTEARRAY_ROUTE_STATE_UPDATE_REMOVE = "RR";
        protected const string BYTEARRAY_ROUTE_STATE_UPDATE_TOCLIENT = "RU";

        protected const string BYTEARRAY_DRIVER_CREATE = "DC";
        protected const string BYTEARRAY_DRIVER_DELETE = "DD";
        protected const string BYTEARRAY_DRIVER_UPDATE = "DU";
        protected const string BYTEARRAY_DRIVER_FATIGUE = "DF";

        protected char[] UNESCAPED_CHARACTERS = new char[] { (char)'&', (char)',', (char)'_' };
        protected string[] ESCAPED_CHARACTERS = new string[] { "&amp;", "&com;", "&und;" };

        public PacketConstants()
        {

        }

        /// <summary>
        /// This is a list of the types of config check that can be performed.
        /// </summary>
        public enum ConfigCheckType
        {
            Vehicle,
            Config,
            Route,
            Schedule,
            Set,
            SetPoint,
            DriverPointsRules,
            DriverPointsGraphs,
            ReasonCodeOveride
        }

    }
}

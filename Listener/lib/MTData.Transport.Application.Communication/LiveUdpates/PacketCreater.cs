using System;
using System.Text;

namespace MTData.Transport.Application.Communication.LiveUdpates
{
	/// <summary>
	/// This class will create packets for the Live Update channel.
	/// NOTE : All messages are currently string based, but this will 
	/// return a byte array for future compatibility.
	/// NOTE 2: that the byte array is passed through raising an event, 
	/// to allow the centralisation of code. It will be up to the caller
	/// to determine the target and transport mechanism
	/// </summary>
	public class PacketCreater : PacketConstants
	{
		/// <summary>
		/// This delegate defines the structure for the outbound packet handler
		/// </summary>
		public event OutboundPacketDelegate OutboundPacket;
		/// <summary>
		/// This delegate defines the structure for the outbound packet handler
		/// </summary>
		public delegate void OutboundPacketDelegate(object context, byte[] packet);

		#region Private supporting methods
		
		/// <summary>
		/// Send the string out to the event, by converting it to a byte array and sending that.
		/// </summary>
		/// <param name="context">context information passed to the event</param>
		/// <param name="packet"></param>
		private void OnOutboundPacket(object context, string packet)
		{
			byte[] buffer = System.Text.Encoding.ASCII.GetBytes(packet);
			OnOutboundPacket(context, buffer);
		}

		/// <summary>
		/// Send the byte buffer out through the event.
		/// </summary>
		/// <param name="context">Context information passed to the event</param>
		/// <param name="packet"></param>
		private void OnOutboundPacket(object context, byte[] packet)
		{
			if (OutboundPacket != null)
				OutboundPacket(context, packet);
		}

		/// <summary>
		/// This method will escape any text value to ensure sdeparator characters are not 
		/// included in the stream unneccesarily.
		/// </summary>
		/// <param name="value"></param>
		/// <returns></returns>
		protected string EscapeStringValue(string value)
		{
			int startIndex = 0;
			string result = value;
			for(int loop = 0; loop < UNESCAPED_CHARACTERS.Length; loop++)
			{
				startIndex = 0;
				StringBuilder builder = new StringBuilder();
				int nextIndex = result.IndexOf(UNESCAPED_CHARACTERS[loop], startIndex);
				while(nextIndex >= 0)
				{
					builder.Append(result.Substring(startIndex, nextIndex - startIndex));
					builder.Append(ESCAPED_CHARACTERS[loop]);
					startIndex = nextIndex + 1;
					if (startIndex >= result.Length)
						break;

					nextIndex = result.IndexOf(UNESCAPED_CHARACTERS[loop], startIndex);
				}

				if (startIndex < result.Length)
					builder.Append(result.Substring(startIndex, result.Length - startIndex));

				result = builder.ToString();
			}
			return result;
		}

		#endregion

		#region Group Update

		/// <summary>
		/// Send a gorup update
		/// </summary>
		/// <param name="context">context information</param>
		public void GroupUpdate(object context)
		{
			OnOutboundPacket(context, COMMAND_STD_GROUP_UPDATE);
		}

		#endregion

        #region Ping Processing

        /// <summary>
        /// Send a test for a keep alive.
        /// </summary>
        /// <param name="context">Context information</param>
        public void PingRequest(object context)
        {
            OnOutboundPacket(context, COMMAND_STD_PING_REQUEST);
        }

        /// <summary>
        /// Send a response for the keep alive.
        /// </summary>
        /// <param name="context">Context information</param>
        public void PingResponse(object context)
        {
            OnOutboundPacket(context, COMMAND_STD_PING_RESPONSE);
        }

        #endregion

		#region Keep Alive Processing

		/// <summary>
		/// Send a test for a keep alive.
		/// </summary>
		/// <param name="context">Context information</param>
		public void KeepAliveRequest(object context)
		{
			OnOutboundPacket(context, COMMAND_STD_KEEPALIVE_TEST);
		}

		/// <summary>
		/// Send a response for the keep alive.
		/// </summary>
		/// <param name="context">Context information</param>
		public void KeepAliveResponse(object context)
		{
			OnOutboundPacket(context, COMMAND_STD_KEEPALIVE_RESPONSE);
		}

		#endregion
		
		#region LiveUpdate Connection Processing

		/// <summary>
		/// Connect to the server given the fleet information provided.
		/// </summary>
		/// <param name="context">Context information</param>
		/// <param name="fleetIDs"></param>
		public void Connect(object context, int[] fleetIDs)
		{
			//	concatenate all the fleetids, 
			//	and send the request.
			System.Text.StringBuilder builder = new StringBuilder();
			builder.Append(COMMAND_COMMA_CONNECT);
			for(int loop = 0; loop < fleetIDs.Length; loop++)
			{
				builder.Append(",");
				builder.Append(fleetIDs[loop]);
			}

			OnOutboundPacket(context, builder.ToString());
		}

		/// <summary>
		/// Connect to the server given the fleet information provided.
		/// </summary>
		/// <param name="context">Context information</param>
		/// <param name="fleetIDs"></param>
		public void ConnectWithVerNumber(object context, int[] fleetIDs, string sLiveUpdateVersionNumber)
		{
			//	concatenate all the fleetids, 
			//	and send the request.
			if(sLiveUpdateVersionNumber == "0")
			{
				Connect(context, fleetIDs);
			}
			else
			{
				System.Text.StringBuilder builder = new StringBuilder();
				builder.Append(COMMAND_COMMA_CONNECT + "V," + sLiveUpdateVersionNumber);
				for(int loop = 0; loop < fleetIDs.Length; loop++)
				{
					builder.Append(",");
					builder.Append(fleetIDs[loop]);
				}

				OnOutboundPacket(context, builder.ToString());
			}
		}

		/// <summary>
		/// Send a disconnect message.
		/// </summary>
		/// <param name="context"></param>
		public void Disconnect(object context)
		{
			OnOutboundPacket(context, COMMAND_STD_DISCONNECT);
        }
        #region Persist Live Update Connect Handlers
        /// <summary>
        /// Connect to a live update feed that indcates when data for a given fleet/vehicle has been persisted to the database.
        /// Plugins that need to load linked tables, should run off this feed.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void ConnectForPersistUpdateFeed(int fleetId, int vehicleId)
        {
            DataPersistUpdate oUpdateRequest = new DataPersistUpdate(DataPersistUpdate.DataPersistUpdateSubCmd.AddToUpdateList, fleetId, vehicleId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
        /// <summary>
        /// Disconnect to a live update feed that indcates when data for a given fleet/vehicle has been persisted to the database.
        /// Plugins that need to load linked tables, should run off this feed.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void DisconnectPersistUpdateFeed(int fleetId, int vehicleId)
        {
            DataPersistUpdate oUpdateRequest = new DataPersistUpdate(DataPersistUpdate.DataPersistUpdateSubCmd.RemoveFromUpdateList, fleetId, vehicleId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
        #endregion
        #region ECM Live Update Connect Handlers
        /// <summary>
        /// Connect to a live update feed of ECM engine data for a given fleet/vehicle.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void ConnectForECMLiveUpdateFeed(int fleetId, int vehicleId)
        {
            ECMUpdate oUpdateRequest = new ECMUpdate(ECMUpdate.ECMUpdateSubCmd.AddToUpdateList, fleetId, vehicleId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
        /// <summary>
        /// Disconnect from a live update feed of ECM engine data for a given fleet/vehicle.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void DisconnectECMLiveUpdateFeed(int fleetId, int vehicleId)
        {
            ECMUpdate oUpdateRequest = new ECMUpdate(ECMUpdate.ECMUpdateSubCmd.RemoveFromUpdateList, fleetId, vehicleId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
        #endregion
        #region Route State Update Connect Handlers
        /// <summary>
        /// Connect to a live update feed of Route State data for a given fleet/vehicle.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void ConnectForRouteStateUpdateFeed(int fleetId)
        {
            RouteStateUpdate oUpdateRequest = new RouteStateUpdate(RouteStateUpdate.RouteStateUpdateSubCmd.AddToUpdateList, fleetId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
        /// <summary>
        /// Disconnect to a live update feed of Route State data for a given fleet/vehicle.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void DisconnectRouteStateUpdateFeed(int fleetId)
        {
            RouteStateUpdate oUpdateRequest = new RouteStateUpdate(RouteStateUpdate.RouteStateUpdateSubCmd.RemoveFromUpdateList, fleetId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
        #endregion
        #endregion

        #region Live Update Processing

        /// <summary>
		/// Send a live update to the client form the listener.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fields"></param>
		public void LiveUpdate(object context, string[] fields)
		{
			StringBuilder builder = new StringBuilder();
			for(int loop = 0; loop < fields.Length; loop++)
			{
				if (loop > 0)
					builder.Append(",");
				
				builder.Append(fields[loop]);
			}
			OnOutboundPacket(context, builder.ToString());
		}

		#endregion

		#region Refresh Server Waypoints

		/// <summary>
		/// Tell the server ot refresh the server waypoints.
		/// </summary>
		/// <param name="context"></param>
		public void RefreshServerWaypoints(object context)
		{
			OnOutboundPacket(context, COMMAND_STD_REFRESH_SERVER_WAYPOINTS);
		}

		#endregion
		
		#region Config Check

		/// <summary>
		/// Send a config check for a single vehicle
		/// </summary>
        /// <param name="object context"></param>
        /// <param name="int fleetID"></param>
        /// <param name="ConfigCheckType type"></param>
        /// <param name="int affectedID"></param>
        /// <param name="int iSubID"></param>
        
        public void ConfigCheck(object context, int fleetID, ConfigCheckType type, int affectedID, int iSubID)
        {
            string command = string.Format("{0},{1},{2},{3},{4}", COMMAND_COMMA_CONFIG_CHECK, fleetID, type, affectedID, iSubID);

            OnOutboundPacket(context, command);
        }

		public void ConfigCheck(object context, int fleetID, ConfigCheckType type, int affectedID)
		{
			string command = string.Format("{0},{1},{2},{3}", COMMAND_COMMA_CONFIG_CHECK, fleetID, type, affectedID);

			OnOutboundPacket(context, command);
		}

		#endregion

		#region Vehicle Commands

		/// <summary>
		/// Send a Vehicle Deleted message
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void VehicleDelete(object context, int fleetID, uint vehicleID)
		{
			OnOutboundPacket(context, string.Format("{0}_{1}_{2}_{3}", COMMAND_UNDERSCORE_VEHICLE, VEHICLE_DELETE, fleetID, vehicleID));
		}

		/// <summary>
		/// Send a Vehicle Created message
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void VehicleCreate(object context, int fleetID, uint vehicleID)
		{
			OnOutboundPacket(context, string.Format("{0}_{1}_{2}_{3}", COMMAND_UNDERSCORE_VEHICLE, VEHICLE_CREATE, fleetID, vehicleID));
		}

		/// <summary>
		/// Send a Vehicle Driver Changed message
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="driverID">ID of new driver</param>
		/// <param name="driverName">Name of new driver</param>
		public void VehicleDriverChange(object context, int fleetID, uint vehicleID, int driverID, string driverName)
		{
			OnOutboundPacket(context, string.Format("{0}_{1}_{2}_{3}_{4}_{5}", COMMAND_UNDERSCORE_VEHICLE, VEHICLE_DRIVERCHANGED, fleetID, vehicleID, driverID, driverName));
		}

        /// <summary>
        /// Send a Vehicle Active State Changed message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        /// <param name="isActive">1 = Active, 0 = InActive</param>
        public void VehicleActiveStateChange(object context, int fleetID, int vehicleID, bool isActive)
        {
            OnOutboundPacket(context, string.Format("{0}_{1}_{2}_{3}_{4}", COMMAND_UNDERSCORE_VEHICLE, VEHICLE_ACTIVESTATECHANGED, fleetID, vehicleID, isActive ? "1" : "0"));
        }
		/// <summary>
		/// Send a message to a MDT
		/// </summary>
		/// <param name="iFleetID"></param>
		/// <param name="iVehicleID"></param>
		/// <param name="sMsg"></param>

		public void VehicleSendMDTMessage(object context, int iFleetID, int iVehicleID, string sMsg)
		{
			string sTempMsg = sMsg;
			// Replace any underscores with ||
			sTempMsg = sTempMsg.Replace("_", "||");
			string command = string.Format("{0}_{1}_{2}_{3}_{4}", COMMAND_UNDERSCORE_VEHICLE, VEHICLE_MDTMESSAGE, iFleetID, iVehicleID, sTempMsg);
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
		}

		/// <summary>
		/// Send a message to the listener to put a vehicle in the at lunch state.
		/// </summary>
		/// <param name="iFleetID"></param>
		/// <param name="iVehicleID"></param>
		/// <param name="sMsg"></param>

		public void VehicleSendSetAtLunchState(object context, int iFleetID, int iVehicleID)
		{
			string command = string.Format("{0},{1},{2}", "A", iFleetID, iVehicleID);
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
		}

		/// <summary>
		/// Send a message to the listener to take a vehicle out of the at lunch state.
		/// </summary>
		/// <param name="iFleetID"></param>
		/// <param name="iVehicleID"></param>
		/// <param name="sMsg"></param>

		public void VehicleSendUnsetAtLunchState(object context, int iFleetID, int iVehicleID)
		{
			string command = string.Format("{0},{1},{2}", "a", iFleetID, iVehicleID);
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
		}

        /// <summary>
        /// Acknowledge a pendent alarm for a given fleet/vehicle.
        /// </summary>
        /// <param name="fleetId"></param>
        /// <param name="vehicleId"></param>
        public void AcknowledgePendentAlarm(int fleetId, int vehicleId)
        {
            PendentAlarmAck oUpdateRequest = new PendentAlarmAck(fleetId, vehicleId);
            string sErr = "";
            byte[] bData = oUpdateRequest.EncodeByStream(ref sErr);
            if (sErr != "")
                throw (new Exception("Request Encode Error"));
            OnOutboundPacket(null, bData);
        }
		#endregion

		#region Unit Requests

		/// <summary>
		/// Send a message to reset a specific unit.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void VehicleReset(object context, int fleetID, uint vehicleID)
		{
			OnOutboundPacket(context, string.Format("{0},{1},{2}", COMMAND_COMMA_RESET_UNIT, fleetID, vehicleID));
		}

        /// <summary>
        /// Send a message to drive an output for a specific unit.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="iFleetID"></param>
        /// <param name="iVehicleID"></param>
        /// <param name="iOutput"></param>
        /// <param name="bOutputOn"></param>
        public void VehicleDriveOutput(object context, int iFleetID, uint iVehicleID, int iOutput, bool bOutputOn)
        {
            int iOutputOn = 0;
            if (bOutputOn)
                iOutputOn = 1;
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4}", COMMAND_COMMA_DRIVE_OUTPUT, iFleetID, iVehicleID, iOutput, iOutputOn));
        }

        /// <summary>
        /// Send a message to add a specific unit to the listener watch list.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="iFleetID"></param>
        /// <param name="iVehicleID"></param>
        public void VehicleAddToWatchList(object context, int iFleetID, uint iVehicleID)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3}", COMMAND_COMMA_ADD_TO_WATCHLIST, iFleetID, iVehicleID, "1"));
        }

        /// <summary>
        /// Send a message to remove a specific unit to the listener watch list.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="iFleetID"></param>
        /// <param name="iVehicleID"></param>
        public void VehicleRemoveFromWatchList(object context, int iFleetID, uint iVehicleID)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3}", COMMAND_COMMA_ADD_TO_WATCHLIST, iFleetID, iVehicleID, "0"));
        }

		/// <summary>
		/// This is a request for rapid updates form the specified vehicle.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="repetitions"></param>
		/// <param name="interval"></param>
		public void VehicleRapidUpdate(object context, int fleetID, uint vehicleID, int repetitions, int interval)
		{
			OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4}", COMMAND_COMMA_RAPID_REQUEST, fleetID, vehicleID, repetitions, interval));
		}

		/// <summary>
		/// This method will trigger a GPS History request for the vehicle specified.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void VehicleGPSHistory(object context, int fleetID, uint vehicleID)
		{
			OnOutboundPacket(context, string.Format("{0},{1},{2}", COMMAND_COMMA_GPS_HISTORY_REQUEST, fleetID, vehicleID));
		}

		/// <summary>
		/// Request for a unit to upload the accident buffer.
		/// The buffer index comes from the history record.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="bufferIndex"></param>
		public void VehicleAccidentBufferRequest(object context, int fleetID, int vehicleID, int bufferIndex)
		{																   
			OnOutboundPacket(context, string.Format("{0},{1},{2},{3}", COMMAND_COMMA_ACCIDENT_BUFFER_REQUEST, fleetID, vehicleID, bufferIndex));
		}
        /// <summary>
        /// Request for a unit to upload some extended history.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        public void ExtendedHistoryRequest(object context, int fleetID, int vehicleID)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2}", COMMAND_COMMA_EXTENDED_HISTORY_REQUEST, fleetID, vehicleID));
        }
        /// <summary>
        /// extended history request has been completed by the unit.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        public void ExtendedHistoryRequestComplete(object context, int fleetID, int vehicleID, DateTime start, DateTime end, int userId = -1)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4},{5}", COMMAND_COMMA_EXTENDED_HISTORY_REQUEST_COMPLETE, 
                fleetID, vehicleID, start.ToString("dd/MM/yyyy HH:mm:ss"), end.ToString("dd/MM/yyyy HH:mm:ss"), userId));
        }

        /// <summary>
        /// send vehicle changed request to refresh vehicles
        /// </summary>
        /// <param name="context"></param>
        /// <param name="fleetIds"></param>
        public void VehicleChanged(object context)
        {
            OnOutboundPacket(context, string.Format("{0}_{1}", COMMAND_UNDERSCORE_VEHICLE, VEHICLE_CHANGED));
        }

        /// <summary>
        /// DVR alarm raised by device
        /// </summary>
        public void DvrAlarm(object context, int fleetId, int vehicleId, int deviceId, int alarmId, DateTime date, double latitude, double longitude)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", COMMAND_COMMA_DVR_ALARM, fleetId, vehicleId, deviceId, alarmId, date.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude));
        }

        /// <summary>
        /// DVR alarm content is received
        /// </summary>
        public void DvrAlarmContent(object context, int fleetId, int vehicleId, int deviceId, int alarmId, DateTime date)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4},{5}", COMMAND_COMMA_DVR_ALARM_CONTENT, fleetId, vehicleId, deviceId, alarmId, date.ToString("dd/MM/yyyy HH:mm:ss")));
        }

        /// <summary>
        /// DVR error
        /// </summary>
        public void DvrError(object context, int fleetId, int vehicleId, string device, DateTime timestamp, double latitude, double longitude, short errorCode)
        {
            if (errorCode > 0)
            {
                OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", COMMAND_COMMA_DVR_ERROR, fleetId, vehicleId, device, timestamp.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude, errorCode));
            }
        }

        /// <summary>
        /// DVR requested
        /// </summary>
        public void DvrRequest(object context, int fleetId, int vehicleId, int deviceId, int requestId, DateTime date, double latitude, double longitude)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", COMMAND_COMMA_DVR_REQUEST, fleetId, vehicleId, deviceId, requestId, date.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude));
        }

        /// <summary>
        /// DVR requested content is received
        /// </summary>
        public void DvrRequestContent(object context, int fleetId, int vehicleId, int deviceId, int requestId)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4}", COMMAND_COMMA_DVR_REQUEST_CONTENT, fleetId, vehicleId, deviceId, requestId));
        }

        /// <summary>
        /// DVR status raised by device
        /// </summary>
        public void DvrStatus(object context, int fleetId, int vehicleId, string device, DateTime timestamp, double latitude, double longitude, short errorCode, bool connected, bool recordOn, bool hddOk,
            short hddSize, short hddFree, short videoConnectedChannel, short videoRecordChannel, int timeOffset, string firmwareVersion, string model, string videoFormat)
        {
            OnOutboundPacket(context, string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18}", COMMAND_COMMA_DVR_STATUS,
                fleetId, vehicleId, device, timestamp.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude, errorCode, connected ? 1 : 0, recordOn ? 1 : 0, hddOk ? 1 : 0, hddSize, hddFree, videoConnectedChannel, videoRecordChannel, timeOffset, firmwareVersion, model, videoFormat));
        }

        #endregion

		#region Mail Processing

		/// <summary>
		/// Send a mail packet to the server.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="unitIDs"></param>
		/// <param name="sender"></param>
		/// <param name="urgent"></param>
		/// <param name="time"></param>
		/// <param name="subject"></param>
		/// <param name="message"></param>
		public void MailPackage(object context, int fleetID, int[] unitIDs, string sender, bool urgent, DateTime time, string subject, string message)
		{
			char separator = (char) 0x02;
			
			// Tell theDATS Listener that we have mail for it:
			// Format for these messages: 
			//"Mail",<fleet>,<unit>;[<unit>];[<unit>],
			// <sender>,<urgent>,<time>,<subject>,<message>,
			//<hasAttachment>,[attachment]
			// where the comma char is actually character 0x02

			StringBuilder builder = new StringBuilder();
			builder.Append(COMMAND_COMMA_MAIL);
			builder.Append(",");
			builder.Append(fleetID);
			builder.Append(separator);

			for(int loop = 0; loop < unitIDs.Length; loop++)
			{
				builder.Append(unitIDs[loop]);
				if ((loop + 1) < unitIDs.Length)
					builder.Append(";");
			}
			builder.Append(separator);
			builder.Append(sender);
			builder.Append(separator);
			builder.Append(urgent);
			builder.Append(separator);
			builder.Append(time.ToString());
			builder.Append(separator);
			builder.Append(subject);
			builder.Append(separator);
			builder.Append(message);

			OnOutboundPacket(context, builder.ToString());

		}

		#endregion

		#region Driver Update
		/// <summary>
		/// Send a message to the server indicating that there has been a change in driver.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void DriverChanged(object context, int fleetID, int iVehicleID)
		{
			DriverChanged(context, fleetID, iVehicleID, 0, " ");
		}

		public void DriverChanged(object context, int fleetID, int iVehicleID, int iDriverID, string sDriverName)
		{
			string sTempName = sDriverName;
			sTempName = sTempName.Replace("_", "||");
			string command = "VEHICLE_DRIVERCHANGED_" + Convert.ToString(fleetID) + "_" +  Convert.ToString(iVehicleID) + "_" +  Convert.ToString(iDriverID) + "_" +  sTempName;
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
			// If the client message event handler is created, it will pass the message up as a string, with the fleet id to indicate which clients are to hear the message.
			//OnOutboundPacketToClient(fleetID, command);
		}
		#endregion

		#region MDT Messages
		public void MessageRecieved(object context, int fleetID, int iVehicleID)
		{
			string command = "VEHICLE_MESSAGERECIEVED_" + Convert.ToString(fleetID) + "_" +  Convert.ToString(iVehicleID);
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
		}

		public void MessageQueued(object context, int fleetID, int iVehicleID)
		{
			string command = "VEHICLE_MESSAGEQUEUED_" + Convert.ToString(fleetID) + "_" +  Convert.ToString(iVehicleID);
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
		}

		public void MessageNotQueued(object context, int fleetID, int iVehicleID)
		{
			string command = "VEHICLE_MESSAGENOTQUEUED_" + Convert.ToString(fleetID) + "_" +  Convert.ToString(iVehicleID);
			// If there byte event handler is created, it will pass up the new string as bytes.
			OnOutboundPacket(context, command);
		}
		#endregion

		#region Client DataModel Messages

		/// <summary>
		/// Send a config check for a single vehicle
		/// </summary>
		/// <param name="context"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void BroadcastMessage(object context, int[] fleetIDs, string message)
		{
			System.Text.StringBuilder builder = new StringBuilder();
			builder.Append(COMMAND_COMMA_CLIENT_BROADCAST_REQUEST);
			builder.Append(",");
			bool addColon = false;
			for(int loop = 0; loop < fleetIDs.Length; loop++)
			{
				if (fleetIDs[loop] != 0)
				{
					if (addColon)
						builder.Append(":");
					builder.Append(fleetIDs[loop]);
					addColon = true;
				}
			}
			builder.Append(",");
			builder.Append(message);

			OnOutboundPacket(context, builder.ToString());
		}

		public void SendByteData(object context, byte[] buffer)
		{
			OnOutboundPacket(context, buffer);
		}
		#endregion
	}
}

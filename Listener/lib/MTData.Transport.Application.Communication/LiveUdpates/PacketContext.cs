using System;

namespace MTData.Transport.Application.Communication.LiveUdpates
{
	/// <summary>
	/// PacketContext for pack reader and writer
	/// </summary>
	public class PacketContext
	{
		public PacketContext()
		{
			
		}

		/// <summary>
		/// Internal variable for holding handled status.
		/// </summary>
		private bool _handled = false;

		/// <summary>
		/// This property will indicate if the item has been processed or not.
		/// </summary>
		public bool Handled
		{
			get{ return _handled; }
			set{ _handled = value; }
		}
	}
}

using System;

namespace MTData.Transport.Application.Communication.LiveUdpates
{
	/// <summary>
	/// Summary description for PacketCreaterContext.
	/// </summary>
	public class PacketCreaterContext
	{
		public int[] FleetIDs = null;
		public string UpdateType = "";

		public PacketCreaterContext(int[] fleetIDs, string updateType)
		{
			FleetIDs = fleetIDs;
			UpdateType = updateType;
		}

		public PacketCreaterContext(int ifleetID, string updateType)
		{
			FleetIDs = new int[1];
			FleetIDs[0] = ifleetID;
			UpdateType = updateType;
		}
	}
}

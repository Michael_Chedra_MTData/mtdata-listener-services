using System;
using System.Data;
using System.Text;
using System.Collections;
using System.Globalization;


namespace MTData.Transport.Application.Communication.LiveUdpates
{
    /// <summary>
    /// This packet parser will take packets received over the LiveUpdate channel,
    /// and raise events with the information in them. 
    /// NOTE : All packets over this channel are currently string based.
    /// This may change in the future.
    /// </summary>
    public class PacketInterpreter : PacketConstants
    {
        /// <summary>
        /// This is the key that the default handlers will be attached to.
        /// </summary>
        private const string DEFAULT = "DEFAULT";
        private const string sClassName = "MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.";

        #region Event Definitions

        /// <summary>
        /// This delegate defines the parameters for a group update.
        /// </summary>
        public delegate void StandardPacketDelegate(object context);
        #region Pendant Alarm Events
        /// <summary>
        /// This delegate defines the parameters for a live update event.
        /// </summary>
        public delegate void PendentAlarmAckPacketDelegate(object context, MTData.Transport.Application.Communication.PendentAlarmAck pendentAlarmAck);
        /// <summary>
        /// This event is raised whenever a live update is received.
        /// </summary>
        public event PendentAlarmAckPacketDelegate PendentAlarmAckEvent;
        #endregion
        #region LiveUpdate
        /// <summary>
        /// This delegate defines the parameters for a live update event.
        /// </summary>
        public delegate void LiveUpdatePacketDelegate(object context, string[] liveUpdateData);
        /// <summary>
        /// This event is raised whenever a live update is received.
        /// </summary>
        public event LiveUpdatePacketDelegate LiveUpdate;
        /// <summary>
        /// This delegate defines the parameters for a live update event.
        /// </summary>
        public delegate void LiveUpdateBytePacketDelegate(object context, MTData.Transport.Application.Communication.LiveUpdate oLiveUpdate);
        /// <summary>
        /// This event is raised whenever a live update is received.
        /// </summary>
        public event LiveUpdateBytePacketDelegate LiveUpdateBytes;
        #endregion
        #region Timezone Update
        /// <summary>
        /// This delegate defines the parameters for a time zone change event.
        /// </summary>
        public delegate void TimeZoneChangedBytePacketDelegate(object context, MTData.Transport.Application.Communication.TimezoneChangeUpdate oTimeZoneChanged);
        /// <summary>
        /// This event is raised whenever a time zone change msg is received.
        /// </summary>
        public event TimeZoneChangedBytePacketDelegate TimeZoneChanged;
        /// <summary>
        /// This delegate defines the parameters for a time zone decode error
        /// </summary>
        public delegate void TimeZoneChangedDecodeErrorDelegate(object context, string sErrorMsg);
        /// <summary>
        /// This event is raised whenever a time zone decode error occurs
        /// </summary>
        public event TimeZoneChangedDecodeErrorDelegate TimeZoneChangedDecodeError;

        #endregion
        #region XML Data Update
        /// <summary>
        /// This delegate defines the config check event format
        /// </summary>
        public delegate void XMLUpdateDelegate(object context, MTData.Transport.Application.Communication.XMLUpdate oXMLUpdate);
        /// <summary>
        /// This is raised if there is a XML Update.
        /// </summary>
        public event XMLUpdateDelegate XMLUpdate;
        #endregion
        #region SetPoint Data Update
        /// <summary>
        /// This delegate defines the config check event format
        /// </summary>
        public delegate void SetPointUpdateDelegate(object context, int iSetPointID, DataSet dsUpdates);
        /// <summary>
        /// This is raised if there is a XML Update for set point data.
        /// </summary>
        public event SetPointUpdateDelegate SetPointUpdate;
        #endregion
        #region Persistance Data Update
        /// <summary>
        /// This delegate defines the persistance data update add/remove request
        /// </summary>
        public delegate void PersistanceUpdateRegisterDelegate(object context, int fleetId, int vehicleId);
        /// <summary>
        /// This delegate defines the persistance data update 
        /// </summary>
        public delegate void PersistanceUpdateDelegate(object context, long tableId, int fleetId, int vehicleId, int reasonId, DateTime deviceTime);
        /// <summary>
        /// This is raised if there is a client request to register and interest in a fleet/vehicle
        /// </summary>
        public event PersistanceUpdateRegisterDelegate PersistanceUpdateAdd;
        /// <summary>
        /// This is raised if there is a client request to un-register and interest in a fleet/vehicle
        /// </summary>
        public event PersistanceUpdateRegisterDelegate PersistanceUpdateRemove;
        /// <summary>
        /// This is raised if there is a data persistance update packet recieved
        /// </summary>
        public event PersistanceUpdateDelegate PersistanceUpdateClientUpdate;
        #endregion
        #region ECM Live Update
        /// <summary>
        /// This delegate defines the persistance data update add/remove request
        /// </summary>
        public delegate void ECMLiveUpdateRegisterDelegate(object context, int fleetId, int vehicleId);
        /// <summary>
        /// This delegate defines the persistance data update 
        /// </summary>
        public delegate void ECMLiveUpdateDelegate(object context, int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, ECMUpdateItemData[] oItems);
        /// <summary>
        /// This is raised if there is a client request to register and interest in a fleet/vehicle
        /// </summary>
        public event ECMLiveUpdateRegisterDelegate ECMLiveUpdateAdd;
        /// <summary>
        /// This is raised if there is a client request to un-register and interest in a fleet/vehicle
        /// </summary>
        public event ECMLiveUpdateRegisterDelegate ECMLiveUpdateRemove;
        /// <summary>
        /// This is raised if there is a data persistance update packet recieved
        /// </summary>
        public event ECMLiveUpdateDelegate ECMLiveUpdateClientUpdate;
        #endregion
        #region Route State Data Update
        /// <summary>
        /// This delegate defines the route state data update add/remove request
        /// </summary>
        public delegate void RouteStateUpdateRegisterDelegate(object context, int fleetId);
        /// <summary>
        /// This delegate defines the RouteState data update 
        /// </summary>
        public delegate void RouteStateUpdateDelegate(object context, int fleetID, int vehicleID, int vehicleScheduleID, int checkPointIndex, double actualDepartTime, double scheduledDepartTime, double scheduledCPArrivalTime, double scheduledCustomerArrivalTime, double scheduledRouteEndTime);
        /// <summary>
        /// This is raised if there is a client request to register and interest in a fleet
        /// </summary>
        public event RouteStateUpdateRegisterDelegate RouteStateUpdateAdd;
        /// <summary>
        /// This is raised if there is a client request to un-register and interest in a fleet
        /// </summary>
        public event RouteStateUpdateRegisterDelegate RouteStateUpdateRemove;
        /// <summary>
        /// This is raised if there is a RouteState update packet recieved
        /// </summary>
        public event RouteStateUpdateDelegate RouteStateUpdateClientUpdate;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="fleetID"></param>
        /// <param name="driverID"></param>
        public delegate void DriverUpdateDelegate(object context, DriverUpdate driver);
        /// <summary>
        /// 
        /// </summary>
        public event DriverUpdateDelegate DriverCreated;
        /// <summary>
        /// 
        /// </summary>
        public event DriverUpdateDelegate DriverDeleted;
        /// <summary>
        /// 
        /// </summary>
        public event DriverUpdateDelegate DriverUpdated;
        /// <summary>
        /// 
        /// </summary>
        public event DriverUpdateDelegate DriverFatigueUpdate;

        #endregion
        #region Group Update

        /// <summary>
        /// This event is raised whenever a gorup update is received.
        /// </summary>
        public event StandardPacketDelegate GroupUpdate;

        #endregion
        #region Connection handling

        /// <summary>
        /// This is the format of the connect handler
        /// </summary>
        public delegate void ConnectPacketDelegate(object context, int[] fleetIDs);

        /// <summary>
        /// This is raised when a connect message is received.
        /// </summary>
        public event ConnectPacketDelegate Connect;

        /// <summary>
        /// This event is raised when a disconnect is received.
        /// </summary>
        public event StandardPacketDelegate Disconnect;

        #endregion
        #region Server Process Waypoints

        /// <summary>
        /// This is raised when server processed waypoints list needs to be refreshed.
        /// </summary>
        public event StandardPacketDelegate RefreshServerWaypoints;

        #endregion
        #region Config Check

        /// <summary>
        /// This delegate defines the config check event format
        /// </summary>
        public delegate void ConfigCheckPacketDelegate(object context, int fleetID, ConfigCheckType type, int affectedID);

        /// <summary>
        /// This israised if there is a config check.
        /// </summary>
        public event ConfigCheckPacketDelegate ConfigCheck;

        /// <summary>
        /// This delegate defines the config check event format
        /// </summary>
        public delegate void ConfigChangedDelegate(object context);

        /// <summary>
        /// This israised if there is a config check.
        /// </summary>
        public event ConfigChangedDelegate ConfigChanged;
        #endregion
        #region Test - Keepalive

        /// <summary>
        /// This event is raised whenever the KeepAlive is received.
        /// </summary>
        public event StandardPacketDelegate KeepAliveRequest;

        /// <summary>
        /// This event is raised when a response to a keep alive is received.
        /// </summary>
        public event StandardPacketDelegate KeepAliveResponse;

        #endregion
        #region Vehicle Udpates

        /// <summary>
        /// This defines the parameters for a vehicle delete command
        /// </summary>
        public delegate void VehiclePacketDelegate(object context, int fleetID, int vehicleID);

        /// <summary>
        /// This event is raised whenever a vehicle delete is received.
        /// </summary>
        public event VehiclePacketDelegate VehicleDelete;

        /// <summary>
        /// This event is raised whenever a vehicle create is received.
        /// </summary>
        public event VehiclePacketDelegate VehicleCreate;

        /// <summary>
        /// A message has been received to reset a given unit
        /// </summary>
        public event VehiclePacketDelegate VehicleReset;

        /// <summary>
        /// This event is raised when a GPS history request is received.
        /// </summary>
        public event VehiclePacketDelegate VehicleGPSHistory;

        /// <summary>
        /// This defines the parameters for a vehicle driver change.
        /// </summary>
        public delegate void VehicleDriverChangePacketDelegate(object context, int fleetID, int vehicleID, int driverID, string driverName);

        /// <summary>
        /// This event is raised whenever a driver change is discovered.
        /// </summary>
        public event VehicleDriverChangePacketDelegate VehicleDriverChange;

        /// <summary>
        /// This defines the parameters for when a vehicle is made active/inactive
        /// </summary>
        public delegate void VehicleActiveStateChangedDelegate(object context, int fleetId, int vehicleId, bool isActive);

        /// <summary>
        /// This event is raised whenever the active state of a vehicle is changed.
        /// </summary>
        public event VehicleActiveStateChangedDelegate VehicleActiveStateChange;

        /// <summary>
        /// This defines the parameters for a vehicle driver change.
        /// </summary>
        public delegate void VehicleMDTMessageDelegate(object context, int fleetID, int vehicleID, string sMsg);

        /// <summary>
        /// This event is raised whenever a driver change is discovered.
        /// </summary>
        public event VehicleMDTMessageDelegate VehicleMDTMessage;

        /// <summary>
        /// This defines the parameters for a message recieved event.
        /// </summary>
        public delegate void VehicleMessageRecievedPacketDelegate(object context, int fleetID, int vehicleID);

        /// <summary>
        /// This event is raised when an MDT sends back a message recieved packet
        /// </summary>
        public event VehicleMessageRecievedPacketDelegate VehicleMessageRecieved;

        /// <summary>
        /// This defines the parameters for a message queued event.
        /// </summary>
        public delegate void VehicleMessageQueuedDelegate(object context, int fleetID, int vehicleID);

        /// <summary>
        /// This event is raised whenever a message is sent to the interface to be queued.
        /// </summary>
        public event VehicleMessageQueuedDelegate VehicleMessageQueued;

        /// <summary>
        /// This defines the parameters for a message queued event.
        /// </summary>
        public delegate void VehicleMessageNotQueuedDelegate(object context, int fleetID, int vehicleID);

        /// <summary>
        /// This event is raised whenever a message is sent to the interface to be queued.
        /// </summary>
        public event VehicleMessageNotQueuedDelegate VehicleMessageNotQueued;

        /// <summary>
        /// Defines parameters for a rapid Udpate event
        /// </summary>
        public delegate void VehicleRapidUpdatesPacketDelegate(object context, int fleetID, int vehicleID, int repetitions, int interval);

        /// <summary>
        /// Event raised if there is a rapid update inbound.
        /// </summary>
        public event VehicleRapidUpdatesPacketDelegate VehicleRapidUpdate;

        /// <summary>
        /// Defines the event to request an accident buffer fomr the specified vehicle 
        /// </summary>
        public delegate void VehicleAccidentBufferRequestDelegate(object context, int fleetID, int vehicleID, int bufferIndex);

        /// <summary>
        /// Event raised when an accident buffer request is processed.
        /// </summary>
        public event VehicleAccidentBufferRequestDelegate VehicleAccidentBufferRequest;

        /// <summary>
        /// Defines the event to request some extended history for the specified vehicle 
        /// </summary>
        public delegate void ExtendedHistoryRequestDelegate(object context, int fleetID, int vehicleID);
        /// <summary>
        /// Event raised when an extended history request is processed.
        /// </summary>
        public event ExtendedHistoryRequestDelegate ExtendedHistoryRequest;

        /// <summary>
        /// Defines the event to an extended history request has been completed by the specified vehicle 
        /// </summary>
        public delegate void ExtendedHistoryRequestCompleteDelegate(object context, int fleetID, int vehicleID, DateTime start, DateTime end, int userId);
        /// <summary>
        /// Event raised when an extended history request has been completed.
        /// </summary>
        public event ExtendedHistoryRequestCompleteDelegate ExtendedHistoryRequestComplete;

        /// <summary>
        /// Defines the event to an DVR alarm raised by a device
        /// </summary>
        public delegate void DvrAlarmDelegate(object context, int fleetId, int vehicleId, int deviceId, int alarmId, DateTime date, decimal latitude, decimal longitude);
        /// <summary>
        /// Event raised when an DVR alarm has been raised.
        /// </summary>
        public event DvrAlarmDelegate DvrAlarm;

        /// <summary>
        /// Defines the event when a DVR alarm content is received
        /// </summary>
        public delegate void DvrAlarmContentDelegate(object context, int fleetId, int vehicleId, int deviceId, int alarmId, DateTime date);
        /// <summary>
        /// Event raised when an DVR alarm content is received
        /// </summary>
        public event DvrAlarmContentDelegate DvrAlarmContent;

        /// <summary>
        /// Defines the event to an DVR error raised by a device
        /// </summary>
        public delegate void DvrErrorDelegate(object context, int fleetId, int vehicleId, string deviceId, DateTime date, decimal latitude, decimal longitude, short errorCode);
        /// <summary>
        /// Event raised when an DVR error has been raised.
        /// </summary>
        public event DvrErrorDelegate DvrError;

        /// <summary>
        /// Defines the event to an DVR status received by a device
        /// </summary>
        public delegate void DvrStatusDelegate(object context, int fleetId, int vehicleId, string device, DateTime timestamp, decimal latitude, decimal longitude, bool isHealthStatus, short errorCode, bool connected, bool recordOn, bool hddOk,
            short hddSize, short hddFree, short videoConnectedChannel, short videoRecordChannel, int timeOffset, string firmwareVersion, string model, string videoFormat);
        /// <summary>
        /// Event raised when an DVR status has been received.
        /// </summary>
        public event DvrStatusDelegate DvrStatus;

        /// <summary>
        /// Defines the event when a DVR requested content is received
        /// </summary>
        public delegate void DvrRequestContentDelegate(object context, int fleetId, int vehicleId, int deviceId, int requestId);
        /// <summary>
        /// Event raised when an DVR requested content is received
        /// </summary>
        public event DvrRequestContentDelegate DvrRequestContent;

        /// <summary>
        /// This delegate defines the update watch list event format
        /// </summary>
        public delegate void UpdateWatchListDelegate(object context, int fleetID, int vehicleID, bool addOrRemove);

        /// <summary>
        /// This is raised if a vehicle is add/removed from a watch list
        /// </summary>
        public event UpdateWatchListDelegate UpdateWatchList;
        #endregion
        #region Broadcast processing

        /// <summary>
        /// The format of the client broadcast message
        /// </summary>
        public delegate void ClientBroadcastRequestDelegate(object context, uint[] fleetIDs, string message);

        /// <summary>
        /// This event is raised if a client broadcast request is received.
        /// This is initiated by a client. 
        /// </summary>
        public event ClientBroadcastRequestDelegate ClientBroadcastRequest;

        /// <summary>
        /// This delegate dfines the broadcast message delegate, which is triggered
        /// whenever a broadcast is received by a client.
        /// </summary>
        public delegate void BroadcastMessageDelegate(object context, string message);

        /// <summary>
        /// This event is triggered whenever a client gets a broadcast message from the listener.
        /// A broadcast message is initiated by a ClientBroadcast Request from a client
        /// </summary>
        public event BroadcastMessageDelegate BroadcastMessage;

        #endregion
        #region Mail Message

        /// <summary>
        /// This defines the parameters for a mail message
        /// </summary>
        public delegate void MailMessagePacketDelegate(object context, int fleetID, int[] unitIDs, string sender, bool urgent, DateTime time, string subject, string message);

        /// <summary>
        /// This event will be raised on receipt of a mail message.
        /// </summary>
        public event MailMessagePacketDelegate MailMessage;

        #endregion
        #endregion

        #region Private instance data

        private Hashtable _standardHandlers = new Hashtable();
        private Hashtable _commaHandlers = new Hashtable();
        private Hashtable _byteArrayHandlers = new Hashtable();
        private Hashtable _underscoreHandlers = new Hashtable();
        private Hashtable _vehicleHandlers = new Hashtable();

        #endregion

        /// <summary>
        /// Prepare the class for use.
        /// </summary>
        public PacketInterpreter()
        {
            //	Prepare standard handlers
            _standardHandlers.Add(COMMAND_STD_KEEPALIVE_TEST.ToUpper(), new StandardCommandHandlerDelegate(KeepAliveRequestHandler));
            _standardHandlers.Add(COMMAND_STD_KEEPALIVE_RESPONSE.ToUpper(), new StandardCommandHandlerDelegate(KeepAliveResponseHandler));
            _standardHandlers.Add(COMMAND_STD_GROUP_UPDATE.ToUpper(), new StandardCommandHandlerDelegate(GroupUpdateHandler));
            _standardHandlers.Add(COMMAND_STD_DISCONNECT.ToUpper(), new StandardCommandHandlerDelegate(DisconnectHandler));
            _standardHandlers.Add(COMMAND_STD_REFRESH_SERVER_WAYPOINTS.ToUpper(), new StandardCommandHandlerDelegate(RefreshServerWaypointsHandler));

            //	Prepare the comma separated handlers
            _commaHandlers.Add(COMMAND_COMMA_CONNECT.ToUpper(), new SeparatedCommandHandlerDelegate(ConnectHandler));
            _commaHandlers.Add(COMMAND_COMMA_CONFIG_CHECK.ToUpper(), new SeparatedCommandHandlerDelegate(ConfigCheckHandler));
            _commaHandlers.Add(COMMAND_COMMA_RESET_UNIT.ToUpper(), new SeparatedCommandHandlerDelegate(VehicleResetHandler));
            _commaHandlers.Add(COMMAND_COMMA_GPS_HISTORY_REQUEST.ToUpper(), new SeparatedCommandHandlerDelegate(VehicleGPSHistoryHandler));
            _commaHandlers.Add(COMMAND_COMMA_RAPID_REQUEST.ToUpper(), new SeparatedCommandHandlerDelegate(VehicleRapidUpdateHandler));
            _commaHandlers.Add(COMMAND_COMMA_ACCIDENT_BUFFER_REQUEST.ToUpper(), new SeparatedCommandHandlerDelegate(VehicleAccidentBufferRequestHandler));
            _commaHandlers.Add(COMMAND_COMMA_EXTENDED_HISTORY_REQUEST.ToUpper(), new SeparatedCommandHandlerDelegate(ExtendedHistoryRequestHandler));
            _commaHandlers.Add(COMMAND_COMMA_EXTENDED_HISTORY_REQUEST_COMPLETE.ToUpper(), new SeparatedCommandHandlerDelegate(ExtendedHistoryRequestCompleteHandler));
            _commaHandlers.Add(COMMAND_COMMA_DVR_ALARM.ToUpper(), new SeparatedCommandHandlerDelegate(DvrAlarmHandler));
            _commaHandlers.Add(COMMAND_COMMA_DVR_ALARM_CONTENT.ToUpper(), new SeparatedCommandHandlerDelegate(DvrAlarmContentHandler));
            _commaHandlers.Add(COMMAND_COMMA_DVR_REQUEST_CONTENT.ToUpper(), new SeparatedCommandHandlerDelegate(DvrRequestContentHandler));
            _commaHandlers.Add(COMMAND_COMMA_CLIENT_BROADCAST_REQUEST.ToUpper(), new SeparatedCommandHandlerDelegate(ClientBroadcastRequestHandler));
            _commaHandlers.Add(COMMAND_COMMA_BROADCAST_MESSAGE.ToUpper(), new SeparatedCommandHandlerDelegate(BroadcastMessageHandler));
            _commaHandlers.Add(COMMAND_COMMA_MAIL.ToUpper(), new SeparatedCommandHandlerDelegate(MailMessageHandler));
            _commaHandlers.Add(COMMAND_COMMA_ADD_TO_WATCHLIST.ToUpper(), new SeparatedCommandHandlerDelegate(UpdateWatchListHandler));
            _commaHandlers.Add(DEFAULT, new SeparatedCommandHandlerDelegate(LiveUpdateHandler));

            //	Prepare the Underscore group handlers
            _underscoreHandlers.Add(COMMAND_UNDERSCORE_VEHICLE.ToUpper(), new SeparatedCommandHandlerDelegate(VehicleCommandHandler));
            _underscoreHandlers.Add(COMMAND_UNDERSCORE_CONFIG.ToUpper(), new SeparatedCommandHandlerDelegate(ConfigCommandHandler));

            //	Prepare the handlers for the vehicle group
            _vehicleHandlers.Add(VEHICLE_DELETE.ToUpper(), new VehicleCommandHandlerDelegate(VehicleDeleteHandler));
            _vehicleHandlers.Add(VEHICLE_CREATE.ToUpper(), new VehicleCommandHandlerDelegate(VehicleCreateHandler));
            _vehicleHandlers.Add(VEHICLE_DRIVERCHANGED.ToUpper(), new VehicleCommandHandlerDelegate(VehicleDriverChangeHandler));
            _vehicleHandlers.Add(VEHICLE_ACTIVESTATECHANGED.ToUpper(), new VehicleCommandHandlerDelegate(VehicleActiveStateChangeHandler));

            _vehicleHandlers.Add(VEHICLE_MDTMESSAGE.ToUpper(), new VehicleCommandHandlerDelegate(VehicleMDTMessageHandler));
            _vehicleHandlers.Add(VEHICLE_MESSAGERECIEVED.ToUpper(), new VehicleCommandHandlerDelegate(VehicleMessageRecivedHandler));
            _vehicleHandlers.Add(VEHICLE_MESSAGEQUEUED.ToUpper(), new VehicleCommandHandlerDelegate(VehicleMessageQueuedHandler));
            _vehicleHandlers.Add(VEHICLE_MESSAGENOTQUEUED.ToUpper(), new VehicleCommandHandlerDelegate(VehicleMessageNotQueuedHandler));

            // Prepare the byte array handlers
            _byteArrayHandlers.Add(PENDENT_ALARM_ACK, new ByteArrayCommandHandlerDelegate(ByteArrayPendentAlarmAckHandler));
            _byteArrayHandlers.Add(BYTEARRAY_LIVEUPDATE, new ByteArrayCommandHandlerDelegate(ByteArrayLiveUpdateHandler));
            _byteArrayHandlers.Add(BYTEARRAY_XML_UPDATE_GENERIC, new ByteArrayCommandHandlerDelegate(ByteArrayXMLUpdateHandler));
            _byteArrayHandlers.Add(BYTEARRAY_TIMEZONE_INFO_CHANGED, new ByteArrayCommandHandlerDelegate(ByteArrayTimeZoneInfoChangeHandler));
            _byteArrayHandlers.Add(BYTEARRAY_TIMEZONE_TO_DRIVER_CHANGED, new ByteArrayCommandHandlerDelegate(ByteArrayTimeZoneToDriverChangeHandler));
            _byteArrayHandlers.Add(BYTEARRAY_TIMEZONE_TO_FLEET_CHANGED, new ByteArrayCommandHandlerDelegate(ByteArrayTimeZoneToFleetChangeHandler));
            _byteArrayHandlers.Add(BYTEARRAY_TIMEZONE_TO_VEHICLE_CHANGED, new ByteArrayCommandHandlerDelegate(ByteArrayTimeZoneToVehicleChangeHandler));
            _byteArrayHandlers.Add(BYTEARRAY_XML_UPDATE_SETPOINT, new ByteArrayCommandHandlerDelegate(ByteArraySetPointUpdateHandler));
            _byteArrayHandlers.Add(BYTEARRAY_XML_UPDATE_REASON_CODE_OVERIDE, new ByteArrayCommandHandlerDelegate(ByteArrayReasonCodeGroupUpdateHandler));

            _byteArrayHandlers.Add(BYTEARRAY_PERSISTANCE_UPDATE_ADD, new ByteArrayCommandHandlerDelegate(ByteArrayPersistUpdateAddClientHandler));
            _byteArrayHandlers.Add(BYTEARRAY_PERSISTANCE_UPDATE_REMOVE, new ByteArrayCommandHandlerDelegate(ByteArrayPersistUpdateRemoveClientHandler));
            _byteArrayHandlers.Add(BYTEARRAY_PERSISTANCE_UPDATE_TOCLIENT, new ByteArrayCommandHandlerDelegate(ByteArrayPersistUpdateToClientHandler));

            _byteArrayHandlers.Add(BYTEARRAY_ECM_LIVE_UPDATE_ADD, new ByteArrayCommandHandlerDelegate(ByteArrayECMLiveUpdateAddClientHandler));
            _byteArrayHandlers.Add(BYTEARRAY_ECM_LIVE_UPDATE_REMOVE, new ByteArrayCommandHandlerDelegate(ByteArrayECMLiveUpdateRemoveClientHandler));
            _byteArrayHandlers.Add(BYTEARRAY_ECM_LIVE_UPDATE_TOCLIENT, new ByteArrayCommandHandlerDelegate(ByteArrayECMLiveUpdateToClientHandler));

            _byteArrayHandlers.Add(BYTEARRAY_ROUTE_STATE_UPDATE_ADD, new ByteArrayCommandHandlerDelegate(ByteArrayRouteStateUpdateAddClientHandler));
            _byteArrayHandlers.Add(BYTEARRAY_ROUTE_STATE_UPDATE_REMOVE, new ByteArrayCommandHandlerDelegate(ByteArrayRouteStateUpdateRemoveClientHandler));
            _byteArrayHandlers.Add(BYTEARRAY_ROUTE_STATE_UPDATE_TOCLIENT, new ByteArrayCommandHandlerDelegate(ByteArrayRouteStateUpdateToClientHandler));

            _byteArrayHandlers.Add(BYTEARRAY_DRIVER_CREATE, new ByteArrayCommandHandlerDelegate(ByteArrayDriverCreateHandler));
            _byteArrayHandlers.Add(BYTEARRAY_DRIVER_DELETE, new ByteArrayCommandHandlerDelegate(ByteArrayDriverDeleteHandler));
            _byteArrayHandlers.Add(BYTEARRAY_DRIVER_UPDATE, new ByteArrayCommandHandlerDelegate(ByteArrayDriverUpdateHandler));
            _byteArrayHandlers.Add(BYTEARRAY_DRIVER_FATIGUE, new ByteArrayCommandHandlerDelegate(ByteArrayDriverFatigueHandler));

        }
        #region Public Methods

        /// <summary>
        /// Take the packet buffer and process it.
        /// </summary>
        /// <param name="context">This is context information to be passed to the event handlers</param>
        /// <param name="packet"></param>
        public void ProcessPacket(object context, byte[] packet)
        {
            string command = "";
            if (packet.Length > 5)
            {
                if (packet[0] == (byte)0x01)
                {
                    // This is a version 4+ message which is sent as a byte array rather than a comma delimited string
                    ProcessByteArrayCommand(context, packet, _byteArrayHandlers);
                }
                else
                {
                    // This is an earlier version (< v4) message which is sent as a comma delimited string
                    command = System.Text.Encoding.ASCII.GetString(packet);
                    command = command.Trim();
                    ProcessPacket(context, command);
                }
            }
            else
            {
                // This is an earlier version (< v4) message which is sent as a comma delimited string
                command = System.Text.Encoding.ASCII.GetString(packet);
                command = command.Trim();
                ProcessPacket(context, command);
            }
        }

        /// <summary>
        /// Take the packet and process it.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="packet"></param>
        public void ProcessPacket(object context, string packet)
        {
            string commandUpper = packet.ToUpper().Trim();

            //	Live dpates arrive as a comma separated string.
            if (packet.IndexOf(',') >= 0)
                ProcessSeparatedCommand(context, packet, commandUpper, ',', _commaHandlers);
            else if (packet.IndexOf('_') >= 0)
                //	Vehicle and Other Udpates arrive as a _ separated list of details.
                ProcessSeparatedCommand(context, packet, commandUpper, '_', _underscoreHandlers);
            else
            {
                //	Group Updates arrive as a single string command
                //	Test Keepalives arrive as a single string command
                ProcessStandardCommand(context, packet, commandUpper);
            }
        }
        #endregion

        #region Private Supporting Methods

        /// <summary>
        /// This method will Unescape any text value to ensure sdeparator characters are not 
        /// included in the stream unneccesarily.
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        protected string UnEscapeStringValue(string value)
        {
            int startIndex = 0;
            string result = value;
            for (int loop = ESCAPED_CHARACTERS.Length - 1; loop >= 0; loop--)
            {
                startIndex = 0;
                StringBuilder builder = new StringBuilder();
                int nextIndex = result.IndexOf(ESCAPED_CHARACTERS[loop], startIndex);
                while (nextIndex >= 0)
                {
                    builder.Append(result.Substring(startIndex, nextIndex - startIndex));
                    builder.Append(UNESCAPED_CHARACTERS[loop]);
                    startIndex = nextIndex + ESCAPED_CHARACTERS[loop].Length;
                    if (startIndex >= result.Length)
                        break;

                    nextIndex = result.IndexOf(ESCAPED_CHARACTERS[loop], startIndex);
                }

                if (startIndex < result.Length)
                    builder.Append(result.Substring(startIndex, result.Length - startIndex));

                result = builder.ToString();
            }
            return result;
        }

        #region Standard Commands

        /// <summary>
        /// This allows the easy addition of command processors.
        /// </summary>
        protected delegate void StandardCommandHandlerDelegate(object context, string command, string commandUpper);

        /// <summary>
        /// This will take a standard command and trigger the appropriate handler method.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="commandUpper"></param>
        private void ProcessStandardCommand(object context, string command, string commandUpper)
        {
            StandardCommandHandlerDelegate handler = null;

            if (_standardHandlers.Contains(commandUpper))
                handler = (StandardCommandHandlerDelegate)_standardHandlers[commandUpper];
            else if (_standardHandlers.Contains(DEFAULT))
                handler = (StandardCommandHandlerDelegate)_standardHandlers[DEFAULT];

            if (handler != null)
                handler(context, command, commandUpper);

        }

        /// <summary>
        /// This will raise a group update event;
        /// </summary>
        /// <param name="command"></param>
        /// <param name="commandUpper"></param>
        private void GroupUpdateHandler(object context, string command, string commandUpper)
        {
            if (GroupUpdate != null)
                GroupUpdate(context);
        }

        /// <summary>
        /// This will raise a keep alive event.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="commandUpper"></param>
        private void KeepAliveRequestHandler(object context, string command, string commandUpper)
        {
            if (KeepAliveRequest != null)
                KeepAliveRequest(context);
        }

        /// <summary>
        /// This will raise a keep alive Response event.
        /// </summary>
        /// <param name="command"></param>
        /// <param name="commandUpper"></param>
        private void KeepAliveResponseHandler(object context, string command, string commandUpper)
        {
            if (KeepAliveResponse != null)
                KeepAliveResponse(context);
        }

        /// <summary>
        /// This handler will process dosconnect messages.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <param name="commandUpper"></param>
        protected virtual void DisconnectHandler(object context, string command, string commandUpper)
        {
            if (Disconnect != null)
                Disconnect(context);
        }

        /// <summary>
        /// Raise a refresh Server PRocessed Waypoints event.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        /// <param name="commandUpper"></param>
        protected virtual void RefreshServerWaypointsHandler(object context, string command, string commandUpper)
        {
            if (RefreshServerWaypoints != null)
                RefreshServerWaypoints(context);
        }

        #endregion

        #region Byte Array command handlers
        /// <summary>
        /// This allows the easy addition of command processors for byte array commands.
        /// </summary>
        protected delegate void ByteArrayCommandHandlerDelegate(object context, byte[] commandParts);

        protected virtual void ProcessByteArrayCommand(object context, byte[] command, Hashtable handlers)
        {
            // A byte array packet has the format [SOP (1 byte = 0x01)][Length (2 bytes)][Command Type (1 byte)][Sub Command Type (1 byte)][Data][EOP (1 byte = 0x07)]
            // The [Length] field is the total length of the byte array
            // EOP should be in position command[[Length] -1]

            string sCommandType = "";
            short iLength = 0;
            ByteArrayCommandHandlerDelegate handler = null;

            try
            {
                if (command[0] == (byte)0x01)
                {
                    iLength = BitConverter.ToInt16(command, 1);
                    if (command.Length == (int)iLength)
                    {
                        if (command[((int)iLength) - 1] == (byte)0x07)
                        {
                            sCommandType = Convert.ToString((char)command[3]);		// Command Type
                            sCommandType += (char)command[4];		// Sub Command
                            if (handlers.ContainsKey(sCommandType))
                            {
                                // Check to see if there is a specific handler for the command/sub command type
                                handler = (ByteArrayCommandHandlerDelegate)handlers[sCommandType];
                            }
                            else
                            {
                                // Check to see if there is a generic handler for just the command type
                                sCommandType = Convert.ToString((char)command[3]);
                                if (handlers.ContainsKey(sCommandType))
                                {
                                    handler = (ByteArrayCommandHandlerDelegate)handlers[sCommandType];
                                }
                            }
                        }
                    }
                }

                if (handler != null)
                    handler(context, command);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }


        /// <summary>
        /// This code will process the live update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayPendentAlarmAckHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            MTData.Transport.Application.Communication.PendentAlarmAck pendentAlarmAck = new MTData.Transport.Application.Communication.PendentAlarmAck();
            pendentAlarmAck.Decode(command, ref sErrMsg);
            PendentAlarmAckEvent(context, pendentAlarmAck);
        }

        /// <summary>
        /// This code will process the live update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayLiveUpdateHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            MTData.Transport.Application.Communication.LiveUpdate oLiveUpdate = new MTData.Transport.Application.Communication.LiveUpdate();
            oLiveUpdate.Decode(command, ref sErrMsg);
            LiveUpdateBytes?.Invoke(context, oLiveUpdate);
        }
        /// <summary>
        /// This code will process the XML update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayXMLUpdateHandler(object context, byte[] command)
        {
            string sErrMsg = "";
            try
            {
                MTData.Transport.Application.Communication.XMLUpdate oXMLUpdate = new MTData.Transport.Application.Communication.XMLUpdate();
                oXMLUpdate.Decode(command, ref sErrMsg);
                if (XMLUpdate != null)
                    XMLUpdate(context, oXMLUpdate);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
        /// <summary>
        /// This code will process the XML update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArraySetPointUpdateHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            MTData.Transport.Application.Communication.XMLUpdate oXMLUpdate = new MTData.Transport.Application.Communication.XMLUpdate();
            oXMLUpdate.Decode(command, ref sErrMsg);
            if (SetPointUpdate != null)
                SetPointUpdate(context, oXMLUpdate.iItemID, oXMLUpdate.ConvertXMLToDataSet());
        }

        /// <summary>
        /// This delegate defines the config check event format
        /// </summary>
        public delegate void ReasonCodeGroupUpdateDelegate(object context, int iFleetID, DataSet dsGroupDetails);
        /// <summary>
        /// This is raised if there is a XML Update for set point data.
        /// </summary>
        public event ReasonCodeGroupUpdateDelegate ReasonCodeGroupUpdate;

        /// <summary>
        /// This code will process the XML update from a reason code group change. 
        /// The XML is a dataset containing the results of the 
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayReasonCodeGroupUpdateHandler(object context, byte[] command)
        {
            string sErrMsg = "";
            // In this update, the oXMLUpdate.iItemID is the fleetID to distribute the message too.
            MTData.Transport.Application.Communication.XMLUpdate oXMLUpdate = new MTData.Transport.Application.Communication.XMLUpdate();
            oXMLUpdate.Decode(command, ref sErrMsg);
            if (ReasonCodeGroupUpdate != null)
                ReasonCodeGroupUpdate(context, oXMLUpdate.iItemID, oXMLUpdate.ConvertXMLToDataSet());
        }

        /// <summary>
        /// This code will process the Time Zone Info Changed update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayTimeZoneInfoChangeHandler(object context, byte[] command)
        {
            ProcessTimeZoneMessage(context, command);
        }
        /// <summary>
        /// This code will process the Time Zone To Driver Changed update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayTimeZoneToDriverChangeHandler(object context, byte[] command)
        {
            ProcessTimeZoneMessage(context, command);
        }
        /// <summary>
        /// This code will process the Time Zone to Fleet Assignment Changed update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayTimeZoneToFleetChangeHandler(object context, byte[] command)
        {
            ProcessTimeZoneMessage(context, command);
        }
        /// <summary>
        /// This code will process the Time Zone to Vehicle Assignment Changed update byte array message
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayTimeZoneToVehicleChangeHandler(object context, byte[] command)
        {
            ProcessTimeZoneMessage(context, command);
        }
        /// <summary>
        /// This code will process the Time Zone message and fire the TimeZoneChanged event.
        /// If a decode error occurs than the TimeZoneChangedDecodeError event will be fired.
        /// 	/// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ProcessTimeZoneMessage(object context, byte[] command)
        {
            string sErrMsg = "";
            MTData.Transport.Application.Communication.TimezoneChangeUpdate oTimeZoneUpdate = new MTData.Transport.Application.Communication.TimezoneChangeUpdate();
            oTimeZoneUpdate.Decode(command, ref sErrMsg);
            if (sErrMsg != "")
                if (TimeZoneChanged != null)
                    TimeZoneChanged(context, oTimeZoneUpdate);
                else
                    if (TimeZoneChangedDecodeError != null)
                        TimeZoneChangedDecodeError(context, sErrMsg);
        }
        #region Database Persistence Live Updates
        /// <summary>
        /// This will handle a request to register a clients interest in persistance for a given fleet vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayPersistUpdateAddClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DataPersistUpdate oUpdate = new DataPersistUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (PersistanceUpdateAdd != null)
                PersistanceUpdateAdd(context, oUpdate.iFleetID, oUpdate.iVehicleID);
        }
        /// <summary>
        /// This will handle a request to un-register a clients interest in persistance for a given fleet vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayPersistUpdateRemoveClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DataPersistUpdate oUpdate = new DataPersistUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (PersistanceUpdateRemove != null)
                PersistanceUpdateRemove(context, oUpdate.iFleetID, oUpdate.iVehicleID);
        }
        /// <summary>
        /// This will handle the decoding of a persistance update packet.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayPersistUpdateToClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DataPersistUpdate oUpdate = new DataPersistUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (PersistanceUpdateClientUpdate != null)
                PersistanceUpdateClientUpdate(context, oUpdate.iTableID, oUpdate.iFleetID, oUpdate.iVehicleID, oUpdate.iReasonID, oUpdate.dtDeviceTime);
        }
        #endregion
        #region ECM Live Updates
        /// <summary>
        /// This will handle a request to register a clients interest in ECM data for a given fleet vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayECMLiveUpdateAddClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            ECMUpdate oUpdate = new ECMUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (ECMLiveUpdateAdd != null)
                ECMLiveUpdateAdd(context, oUpdate.iFleetID, oUpdate.iVehicleID);
        }
        /// <summary>
        /// This will handle a request to un-register a clients interest in ECM data for a given fleet vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayECMLiveUpdateRemoveClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            ECMUpdate oUpdate = new ECMUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (ECMLiveUpdateRemove != null)
                ECMLiveUpdateRemove(context, oUpdate.iFleetID, oUpdate.iVehicleID);
        }
        /// <summary>
        /// This will handle the decoding of a ECM live update packet.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayECMLiveUpdateToClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            ECMUpdate oUpdate = new ECMUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (ECMLiveUpdateClientUpdate != null)
                ECMLiveUpdateClientUpdate(context, oUpdate.iFleetID, oUpdate.iVehicleID, oUpdate.dtDeviceTime, oUpdate.Location, oUpdate.Latitude, oUpdate.Longitude, oUpdate._ECMValues.ToArray());
        }
        #endregion
        #region Route State Live Updates
        /// <summary>
        /// This will handle a request to register a clients interest in route state for a given fleet vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayRouteStateUpdateAddClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            RouteStateUpdate oUpdate = new RouteStateUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (RouteStateUpdateAdd != null)
                RouteStateUpdateAdd(context, oUpdate.FleetID);
        }
        /// <summary>
        /// This will handle a request to un-register a clients interest in RouteState for a given fleet vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayRouteStateUpdateRemoveClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            RouteStateUpdate oUpdate = new RouteStateUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (RouteStateUpdateRemove != null)
                RouteStateUpdateRemove(context, oUpdate.FleetID);
        }
        /// <summary>
        /// This will handle the decoding of a RouteState update packet.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="command"></param>
        private void ByteArrayRouteStateUpdateToClientHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            RouteStateUpdate oUpdate = new RouteStateUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (RouteStateUpdateClientUpdate != null)
                RouteStateUpdateClientUpdate(context, oUpdate.FleetID, oUpdate.VehicleID, oUpdate.VehicleScheduleID, oUpdate.CheckPointIndex, oUpdate.ActualDepartTime, oUpdate.ScheduledDepartTime, oUpdate.ScheduledCPArrivalTime, oUpdate.ScheduledCustomerArrivalTime, oUpdate.ScheduledRouteEndTime);
        }
        #endregion
        #region Driver Live Update
        private void ByteArrayDriverCreateHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DriverUpdate oUpdate = new DriverUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (DriverCreated != null)
                DriverCreated(context, oUpdate);
        }

        private void ByteArrayDriverDeleteHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DriverUpdate oUpdate = new DriverUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (DriverDeleted != null)
                DriverDeleted(context, oUpdate);
        }

        private void ByteArrayDriverUpdateHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DriverUpdate oUpdate = new DriverUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (DriverUpdated != null)
                DriverUpdated(context, oUpdate);
        }

        private void ByteArrayDriverFatigueHandler(object context, byte[] command)
        {
            string sErrMsg = "";

            DriverUpdate oUpdate = new DriverUpdate();
            oUpdate.Decode(command, ref sErrMsg);
            if (DriverFatigueUpdate != null)
                DriverFatigueUpdate(context, oUpdate);
        }

        #endregion

        #endregion

        #region Separated command handlers

        /// <summary>
        /// This allows the easy addition of command processors for character separated commands.
        /// </summary>
        protected delegate void SeparatedCommandHandlerDelegate(object context, string[] commandParts, string command);

        /// <summary>
        /// Underscore commands are a new form of command that allows multiple levels of complexity.
        /// </summary>
        /// <param name="context">Context information passed with packet</param>
        /// <param name="command">Command string</param>
        /// <param name="commandUpper"></param>
        /// <param name="separator">Separator character to split with</param>
        /// <param name="handlers">Hashtable of registered handlers</param>
        protected virtual void ProcessSeparatedCommand(object context, string command, string commandUpper, char separator, Hashtable handlers)
        {
            string[] parts = command.Split(separator);
            SeparatedCommandHandlerDelegate handler = null;

            parts[0] = parts[0].ToUpper();

            if (parts.Length >= 1)
            {
                if (handlers.Contains(parts[0]))
                    handler = (SeparatedCommandHandlerDelegate)handlers[parts[0]];
                else if (handlers.Contains(DEFAULT))
                    handler = (SeparatedCommandHandlerDelegate)handlers[DEFAULT];
            }

            if (handler != null)
                handler(context, parts, command);
        }

        #region Comma separated commands

        #region Live Updates

        /// <summary>
        /// A live update is the simplest form of message.
        /// It is a comma separated list of values.
        /// </summary>
        /// <param name="context">Context information from caller</param>
        /// <param name="commandParts">This is an array of the split fields</param>
        /// <param name="command"></param>
        protected virtual void LiveUpdateHandler(object context, string[] commandParts, string command)
        {
            if (LiveUpdate != null)
                LiveUpdate(context, commandParts);
        }

        #endregion

        #region Connection Handling

        /// <summary>
        /// This handler will parse any information regarding conneciton attempts.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        protected virtual void ConnectHandler(object context, string[] commandParts, string command)
        {
            if (Connect != null)
            {
                //	the fleet ids are in the command parts..
                int[] fleetIDs = new int[commandParts.Length - 1];
                for (int loop = 0; loop < fleetIDs.Length; loop++)
                    fleetIDs[loop] = Int32.Parse(commandParts[loop + 1]);

                Connect(context, fleetIDs);
            }
        }

        #endregion

        #region Config Check command handling

        /// <summary>
        /// Handle a config check command.
        /// Format is as follows..
        /// ConfigCheck,
        /// FleetID,
        /// CheckType[Vehicle/Config],
        /// CheckID[VehicleID/ConfigID]
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        private void ConfigCheckHandler(object context, string[] commandParts, string command)
        {
            if ((ConfigCheck != null) && (commandParts.Length >= 4))
            {
                int fleetID = Int32.Parse(commandParts[1]);

                ConfigCheckType type = (ConfigCheckType)Enum.Parse(typeof(ConfigCheckType), commandParts[2], true);
                int affectedID = Int32.Parse(commandParts[3]);

                ConfigCheck(context, fleetID, type, affectedID);
            }
        }

        #endregion

        #region Vehicle Udpate Handling

        /// <summary>
        /// This will process a reset unit request.
        /// The reuest consists of ResetUnit,FleetID,VehicleID
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        private void VehicleResetHandler(object context, string[] commandParts, string command)
        {
            if ((VehicleReset != null) && (commandParts.Length >= 3))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);

                VehicleReset(context, fleetID, vehicleID);
            }
        }

        /// <summary>
        /// This method will parse and process a GPS History request.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        public void VehicleGPSHistoryHandler(object context, string[] commandParts, string command)
        {
            if ((VehicleGPSHistory != null) && (commandParts.Length >= 3))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);

                VehicleGPSHistory(context, fleetID, vehicleID);
            }
        }

        /// <summary>
        /// Request an accident buffer upload from a vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        public void VehicleAccidentBufferRequestHandler(object context, string[] commandParts, string command)
        {
            if ((VehicleAccidentBufferRequest != null) && (commandParts.Length >= 4))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);
                int bufferIndex = Int32.Parse(commandParts[3]);

                VehicleAccidentBufferRequest(context, fleetID, vehicleID, bufferIndex);
            }
        }


        /// <summary>
        /// Request an extended history upload from a vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        public void ExtendedHistoryRequestHandler(object context, string[] commandParts, string command)
        {
            if ((ExtendedHistoryRequest != null) && (commandParts.Length >= 3))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);

                ExtendedHistoryRequest(context, fleetID, vehicleID);
            }
        }

        /// <summary>
        /// extended history request completed by a vehicle
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        public void ExtendedHistoryRequestCompleteHandler(object context, string[] commandParts, string command)
        {
            if ((ExtendedHistoryRequestComplete != null) && (commandParts.Length >= 5))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);
                DateTime start = DateTime.ParseExact(commandParts[3], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                DateTime end = DateTime.ParseExact(commandParts[4], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                int userId = -1;
                if (commandParts.Length >= 6)
                {
                    userId = Int32.Parse(commandParts[5]);
                }

                ExtendedHistoryRequestComplete(context, fleetID, vehicleID, start, end, userId);
            }
        }

        /// <summary>
        /// Dvr alarm raised by device
        /// </summary>
        public void DvrAlarmHandler(object context, string[] commandParts, string command)
        {
            if ((DvrAlarm != null) && (commandParts.Length >= 8))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);
                int deviceID = Int32.Parse(commandParts[3]);
                int alarmID = Int32.Parse(commandParts[4]);
                DateTime date = DateTime.ParseExact(commandParts[5], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);
                decimal latitude = Decimal.Parse(commandParts[6]);
                decimal longitude = Decimal.Parse(commandParts[7]);

                DvrAlarm(context, fleetID, vehicleID, deviceID, alarmID, date, latitude, longitude);
            }
        }

        /// <summary>
        /// Dvr alarm content received
        /// </summary>
        public void DvrAlarmContentHandler(object context, string[] commandParts, string command)
        {
            if ((DvrAlarmContent != null) && (commandParts.Length >= 6))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);
                int deviceID = Int32.Parse(commandParts[3]);
                int alarmID = Int32.Parse(commandParts[4]);
                DateTime date = DateTime.ParseExact(commandParts[5], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                DvrAlarmContent(context, fleetID, vehicleID, deviceID, alarmID, date);
            }
        }

        /// <summary>
        /// Dvr requested content received
        /// </summary>
        public void DvrRequestContentHandler(object context, string[] commandParts, string command)
        {
            if ((DvrRequestContent != null) && (commandParts.Length >= 5))
            {
                int fleetId = Int32.Parse(commandParts[1]);
                int vehicleId = Int32.Parse(commandParts[2]);
                int deviceId = Int32.Parse(commandParts[3]);
                int requestId = Int32.Parse(commandParts[4]);

                DvrRequestContent(context, fleetId, vehicleId, deviceId, requestId);
            }
        }

        /// <summary>
        /// Request a client broadcast, to all appropriate clients for the fleets specified.
        /// ClientBroadcast,[fleets : separated],[client message]
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        public void ClientBroadcastRequestHandler(object context, string[] commandParts, string command)
        {
            if ((ClientBroadcastRequest != null) && (commandParts.Length >= 3))
            {
                string[] fleetIDList = commandParts[1].Split(':');
                uint[] fleetIDs = new uint[fleetIDList.Length];
                for (int loop = 0; loop < fleetIDList.Length; loop++)
                    fleetIDs[loop] = UInt32.Parse(fleetIDList[loop]);

                int length = 2 + commandParts[0].Length + commandParts[1].Length;
                ClientBroadcastRequest(context, fleetIDs, command.Substring(length, command.Length - length));
            }
        }

        /// <summary>
        /// This handler will parse the details from a broadcast message 
        /// and pass them on to all handlers listening for them.
        /// Format : "Broadcast,<Message>"
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        public void BroadcastMessageHandler(object context, string[] commandParts, string command)
        {
            if (BroadcastMessage != null)
            {
                int length = commandParts[0].Length + 1;
                BroadcastMessage(context, command.Substring(length, command.Length - length));
            }
        }

        /// <summary>
        /// This is the handler for processing rapid update requests.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        private void VehicleRapidUpdateHandler(object context, string[] commandParts, string command)
        {
            if ((VehicleRapidUpdate != null) && (commandParts.Length >= 4))
            {
                int fleetId = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);
                int repetitions = Int32.Parse(commandParts[3]);
                int interval = Int32.Parse(commandParts[4]);

                VehicleRapidUpdate(context, fleetId, vehicleID, repetitions, interval);
            }

        }

        #endregion

        #region Watch List Message Handling
        private void UpdateWatchListHandler(object context, string[] commandParts, string command)
        {
            if ((UpdateWatchList != null) && (commandParts.Length >= 4))
            {
                int fleetID = Int32.Parse(commandParts[1]);
                int vehicleID = Int32.Parse(commandParts[2]);
                int addOrRemove = Int32.Parse(commandParts[3]);
                UpdateWatchList(context, fleetID, vehicleID, (addOrRemove == 1));
            }
        }

        #endregion
        #region Mail Message Handling

        /// <summary>
        /// This method will handle the mail message inbound, and raise the event accordingly.
        /// Tell theDATS Listener that we have mail for it:
        /// Format for these messages: 
        /// "Mail",<fleet>,<unit>;[<unit>];[<unit>],
        /// <sender>,<urgent>,<time>,<subject>,<message>,
        /// <hasAttachment>,[attachment]
        /// where the comma char is actually character 0x02
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        private void MailMessageHandler(object context, string[] commandParts, string command)
        {
            if ((MailMessage != null) && (commandParts.Length >= 2))
            {
                char separator = (char)0x02;

                string[] messageElements = command.Substring(0, COMMAND_COMMA_MAIL.Length + 1).Split(separator);

                //	NOTE : At time of writing, attachments not supported.
                if (messageElements.Length >= 7)
                {
                    int fleetID = Int32.Parse(messageElements[0]);
                    string[] vehicles = messageElements[1].Split(';');
                    if ((vehicles != null) && (vehicles.Length > 0))
                    {
                        int[] vehicleIDs = new int[vehicles.Length];
                        for (int loop = 0; loop < vehicles.Length; loop++)
                            vehicleIDs[loop] = (vehicles[loop].Trim() == "") ? 0 : Int32.Parse(vehicles[loop].Trim());
                        string sender = messageElements[2];
                        bool urgent = (messageElements[3].ToLower() == "true");
                        DateTime time = DateTime.Parse(messageElements[4]);
                        string subject = messageElements[5];
                        string message = messageElements[6];

                        MailMessage(context, fleetID, vehicleIDs, sender, urgent, time, subject, message);
                    }
                }

            }

        }

        #endregion

        #endregion

        #region Underscore Commands

        #region Vehicle Commands

        /// <summary>
        /// This delegate defines what is passed to a vehicle handler
        /// </summary>
        protected delegate void VehicleCommandHandlerDelegate(object context, string[] commandParts, int fleetID, int vehicleID);

        /// <summary>
        /// Process Vehicle commands.
        /// A vehicle command consists of the following, separated by underscores..
        /// VEHICLE
        /// COMMANDTYPE - DELETE/DRIVERCHANGED etc..
        /// FleetID
        /// VehicleID
        /// Additional Data
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        private void VehicleCommandHandler(object context, string[] commandParts, string command)
        {
            if (commandParts.Length >= 4)
            {
                if (_vehicleHandlers.Contains(commandParts[1]))
                {
                    VehicleCommandHandlerDelegate handler = (VehicleCommandHandlerDelegate)_vehicleHandlers[commandParts[1]];
                    int fleetID = Int32.Parse(commandParts[2]);
                    int vehicleID = Int32.Parse(commandParts[3]);
                    handler(context, commandParts, fleetID, vehicleID);
                }
            }
        }

        /// <summary>
        /// Processes Config Commands
        /// When a user makes a change to a config, way point, route point or schedule the listener will send a CONFIG_CHANGED message out to
        /// all interested clients.
        /// CONFIG
        /// COMMANDTYPE - CHANGED
        /// FleetID
        /// VehicleID
        /// Additional Data
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="command"></param>
        private void ConfigCommandHandler(object context, string[] commandParts, string command)
        {
            if (commandParts.Length == 2)
            {
                if (ConfigChanged != null)
                    ConfigChanged(context);
            }
        }


        /// <summary>
        /// This handler will raise a vehicle deleted command.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleDeleteHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            if (VehicleDelete != null)
                VehicleDelete(context, fleetID, vehicleID);
        }

        /// <summary>
        /// This handler will raise a vehicle created command.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleCreateHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            if (VehicleCreate != null)
                VehicleCreate(context, fleetID, vehicleID);
        }

        /// <summary>
        /// This handler processes DriverChanged messages, which consist of an additional 2 fields,
        /// DriverID
        /// DriverName
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleDriverChangeHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            //	Driver ID should be the next field in the command parts.
            if ((VehicleDriverChange != null) && (commandParts.Length >= 6))
            {
                int driverID = Int32.Parse(commandParts[4]);
                string driverName = commandParts[5];
                VehicleDriverChange(context, fleetID, vehicleID, driverID, driverName);
            }
        }

        /// <summary>
        /// This handler processes ActiveStateChanged messages, which has 1 additional field,
        /// IsActive (1 = Active, 0 = Inactive)
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleActiveStateChangeHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            //	Driver ID should be the next field in the command parts.
            if ((VehicleActiveStateChange != null) && (commandParts.Length >= 5))
            {
                bool isActive = true;
                int currentState = Int32.Parse(commandParts[4]);
                if (currentState == 0)
                    isActive = false;
                VehicleActiveStateChange(context, fleetID, vehicleID, isActive);
            }
        }


        /// <summary>
        /// This handler processes 'Meesage Queued' messages.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleMessageQueuedHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            //	Driver ID should be the next field in the command parts.
            if ((VehicleMessageQueued != null) && (commandParts.Length >= 3))
            {
                VehicleMessageQueued(context, fleetID, vehicleID);
            }
        }

        /// <summary>
        /// This handler processes 'Meesage Queued' messages.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleMessageNotQueuedHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            //	Driver ID should be the next field in the command parts.
            if ((VehicleMessageNotQueued != null) && (commandParts.Length >= 3))
            {
                VehicleMessageNotQueued(context, fleetID, vehicleID);
            }
        }

        /// <summary>
        /// This handler processes 'MeesageRecieved' messages.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleMessageRecivedHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            //	Driver ID should be the next field in the command parts.
            if ((VehicleMessageRecieved != null) && (commandParts.Length >= 3))
            {
                VehicleMessageRecieved(context, fleetID, vehicleID);
            }
        }

        /// <summary>
        /// This handler processes messages to be sent out to the MDT
        /// DriverID
        /// DriverName
        /// </summary>
        /// <param name="context"></param>
        /// <param name="commandParts"></param>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void VehicleMDTMessageHandler(object context, string[] commandParts, int fleetID, int vehicleID)
        {
            //	Driver ID should be the next field in the command parts.
            if ((VehicleMDTMessage != null) && (commandParts.Length >= 5))
            {
                string sMsg = commandParts[4];
                VehicleMDTMessage(context, fleetID, vehicleID, sMsg);
            }
        }
        #endregion

        #endregion

        #endregion

        #endregion

    }
}

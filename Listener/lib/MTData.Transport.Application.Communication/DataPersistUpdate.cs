using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
    public class DataPersistUpdate
    {
        public enum DataPersistUpdateSubCmd : byte
        {
            AddToUpdateList = (byte) 'A',
            RemoveFromUpdateList = (byte) 'R',
            ClientUpdate = (byte) 'U'
        };

        public char bCmd = 'P';
        public DataPersistUpdateSubCmd bSubCmd = DataPersistUpdateSubCmd.ClientUpdate;
        public short iPacketVersion = 1;
        public long iTableID = 0;
    	public int iFleetID = 0;
		public int iVehicleID = 0;
        public int iReasonID = 0;
        public DateTime dtDeviceTime = System.DateTime.MinValue;
		public bool bIsTrackingUnit = true;
		public bool bIsRefrigerationUnit = false;

        public DataPersistUpdate()
		{

		}

        public DataPersistUpdate(DataPersistUpdateSubCmd subCmd, int fleetID, int vehicleID)
		{
            if(subCmd != DataPersistUpdateSubCmd.AddToUpdateList && subCmd != DataPersistUpdateSubCmd.RemoveFromUpdateList)
                throw (new Exception("The sub command type supplied does not match the parameter list"));
            bSubCmd = subCmd;
            iFleetID = fleetID;
            iVehicleID = vehicleID;
		}

        public DataPersistUpdate(long tableID, int fleetID, int vehicleID, int reasonID, DateTime deviceTime)
		{
            bSubCmd = DataPersistUpdateSubCmd.ClientUpdate;
            iTableID = tableID;
            iFleetID = fleetID;
            iVehicleID = vehicleID;
            iReasonID = reasonID;
            dtDeviceTime = deviceTime;
		}

		public byte[] EncodeByStream(ref string sErrMsg)
		{
			byte bEOP = (byte) 0x07;
			byte bSOP = (byte) 0x01;
			byte[] bResult = null;
			MemoryStream oStream = null;

			try
			{
				oStream = new MemoryStream();
				PacketUtilities.WriteToStream(oStream, bSOP);						// SOP
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);				// Length 1
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);				// Length 2
				PacketUtilities.WriteToStream(oStream, bCmd);						// Packet Type (1 byte)
                PacketUtilities.WriteToStream(oStream, (byte)bSubCmd);				// Sub Command (1 byte)
                PacketUtilities.WriteToStream(oStream, iPacketVersion);             // Packet Version (2 byte int)
                if (bSubCmd == DataPersistUpdateSubCmd.AddToUpdateList || bSubCmd == DataPersistUpdateSubCmd.RemoveFromUpdateList)
                {
                    PacketUtilities.WriteToStream(oStream, iFleetID);					// Fleet ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, iVehicleID);					// Vehicle ID (4 byte int)
                }
                else if (bSubCmd == DataPersistUpdateSubCmd.ClientUpdate) // If this is an update to send to the clients
                {
                    PacketUtilities.WriteToStream(oStream, iTableID);               // Table ID (8 byte int)
                    PacketUtilities.WriteToStream(oStream, iFleetID); // Fleet ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, iVehicleID); // Vehicle ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, iReasonID); // Reason ID (4 byte int)
                    PacketUtilities.WriteToStream(oStream, dtDeviceTime.ToOADate()); // Device Time (8 byte double)
                }
			    PacketUtilities.WriteToStream(oStream, bEOP);						// End of Packet (1 byte)
				bResult = oStream.ToArray();
				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}
		public byte[] Encode(ref string sErrMsg)
		{
			int iPos = 0;
			byte bEOP = (byte) 0x07;
			byte bSOP = (byte) 0x01;
			byte[] bResult = null;
			MemoryStream oMS = new MemoryStream();
			
			try
			{
				PacketUtilities.WriteToStream(oMS, bSOP);						// SOP
				PacketUtilities.WriteToStream(oMS, (byte) 0x00);				// Length 1
				PacketUtilities.WriteToStream(oMS, (byte) 0x00);				// Length 2
				PacketUtilities.WriteToStream(oMS, bCmd);						// Packet Type (1 byte)
                PacketUtilities.WriteToStream(oMS, (byte) bSubCmd);				// Sub Command (1 byte)
                PacketUtilities.WriteToStream(oMS, iPacketVersion);             // Packet Version (2 byte int)

                if (bSubCmd == DataPersistUpdateSubCmd.AddToUpdateList || bSubCmd == DataPersistUpdateSubCmd.RemoveFromUpdateList)
                {
                    PacketUtilities.WriteToStream(oMS, iFleetID);               // Fleet ID (4 byte int)
                    PacketUtilities.WriteToStream(oMS, iVehicleID);             // Vehicle ID (4 byte int)
                }
                else if (bSubCmd == DataPersistUpdateSubCmd.ClientUpdate) // If this is an update to send to the clients
                {
                    PacketUtilities.WriteToStream(oMS, iTableID);               // Table ID (8 byte int)
                    PacketUtilities.WriteToStream(oMS, iFleetID);               // Fleet ID (4 byte int)
                    PacketUtilities.WriteToStream(oMS, iVehicleID);             // Vehicle ID (4 byte int)
                    PacketUtilities.WriteToStream(oMS, iReasonID);              // Reason ID (4 byte int)
                    PacketUtilities.WriteToStream(oMS, dtDeviceTime.ToOADate()); // Device Time (8 byte double)
                }
			    PacketUtilities.WriteToStream(oMS, bEOP);						// End of Packet (1 byte)

				bResult = oMS.ToArray();
				iPos = bResult.Length;

				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(iPos))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(iPos))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}
		public bool DecodeFromStream(byte[] bData, ref string sErrMsg)
		{
			bool bRet = false;
		    byte bTemp = (byte) 0x00;
			byte bEOP = (byte) 0x07;
			byte bValue = (byte) 0x00;
			char cType = (char) 0x00;
			short iLength = 0;
			double dValue = 0;
			MemoryStream oStream = null;

			try
			{
				oStream = new MemoryStream(bData, 0, bData.Length, false, false);
				PacketUtilities.ReadFromStream(oStream, ref bValue);													
				if (bValue != (byte) 0x01)														    // SOP
				{
					sErrMsg = "Error decoding : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref iLength);							    // Length
				if (bData.Length != iLength)
				{
					sErrMsg = "Error decoding : Packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
					sErrMsg = "Error decoding : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref cType);								    // Packet Type (1 byte)
				if (cType != bCmd)
				{
					sErrMsg = "Error decoding : Packet type is incorrect.";
					return bRet;
				}
                PacketUtilities.ReadFromStream(oStream, ref bTemp);								    // Sub Command (1 byte)
                if (bTemp != (byte)DataPersistUpdateSubCmd.AddToUpdateList && bTemp != (byte)DataPersistUpdateSubCmd.ClientUpdate && bTemp != (byte)DataPersistUpdateSubCmd.RemoveFromUpdateList)
                {
                    sErrMsg = "Error decoding : Packet sub command is not valid.";
                    return bRet;
                }
                bSubCmd = (DataPersistUpdateSubCmd)bTemp;
				PacketUtilities.ReadFromStream(oStream, ref this.iPacketVersion);				    // Packet Version (2 byte int)

                if (bSubCmd == DataPersistUpdateSubCmd.AddToUpdateList || bSubCmd == DataPersistUpdateSubCmd.RemoveFromUpdateList)
                {
                    PacketUtilities.ReadFromStream(oStream, ref this.iFleetID);					    // Fleet ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref this.iVehicleID);				    // Vehicle ID (4 byte int)
                }
                else if (bSubCmd == DataPersistUpdateSubCmd.ClientUpdate)
                {
                    PacketUtilities.ReadFromStream(oStream, ref this.iTableID);					    // Table ID (8 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref this.iFleetID);					    // Fleet ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref this.iVehicleID);				    // Vehicle ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref this.iReasonID);				    // Reason ID (4 byte int)
                    PacketUtilities.ReadFromStream(oStream, ref dValue);
                    this.dtDeviceTime = DateTime.FromOADate(dValue);							    // Device Time (8 byte double)
                }

				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error decoding : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}
		public bool Decode(byte[] bData, ref string sErrMsg)
		{
			bool bRet = false;
            byte bTemp = (byte)0x00;
			byte bEOP = (byte) 0x07;
			char cType = (char) 0x00;
			int iPos = 0;
			short iLength = 0;
			double dValue = 0;

			try
			{
				if (bData[iPos++] != (byte) 0x01)																												// SOP
				{
					sErrMsg = "Error decoding : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iLength);					// Length
				if (bData.Length != iLength)
				{
                    sErrMsg = "Error decoding : Packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
                    sErrMsg = "Error decoding : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref cType);					// Packet Type (1 byte)
                if (cType != bCmd)
				{
                    sErrMsg = "Error decoding : Packet type is incorrect.";
					return bRet;
				}
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bTemp);					// Sub Command (1 byte)
                if (bTemp != (byte)DataPersistUpdateSubCmd.AddToUpdateList && bTemp != (byte)DataPersistUpdateSubCmd.ClientUpdate && bTemp != (byte)DataPersistUpdateSubCmd.RemoveFromUpdateList)
				{
                    sErrMsg = "Error decoding : Packet sub command is not valid.";
					return bRet;
				}
			    bSubCmd = (DataPersistUpdateSubCmd) bTemp;

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iPacketVersion);	    // Packet Version (2 byte int)
                if (bSubCmd == DataPersistUpdateSubCmd.AddToUpdateList || bSubCmd == DataPersistUpdateSubCmd.RemoveFromUpdateList)
                {
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iFleetID);		// Fleet ID (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iVehicleID);		// Vehicle ID (4 byte int)
                }
                else if (bSubCmd == DataPersistUpdateSubCmd.ClientUpdate)
                {
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTableID);		// Table ID (8 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iFleetID);		// Fleet ID (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iVehicleID);		// Vehicle ID (4 byte int)
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iReasonID);		// Reason ID (4 byte int)				
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref dValue);
                    this.dtDeviceTime = DateTime.FromOADate(dValue);								// GPS Time (8 byte double)                    
                }
				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding live update : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}
		public string ToDisplayString()
		{
			return	this.ToString();
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
                if (bSubCmd == DataPersistUpdateSubCmd.AddToUpdateList || bSubCmd == DataPersistUpdateSubCmd.RemoveFromUpdateList)
                {
                    if (bSubCmd == DataPersistUpdateSubCmd.AddToUpdateList)
                        builder.Append("\r\nData Persist AddToUpdateList Packet :");
                    else
                        builder.Append("\r\nData Persist RemoveFromUpdateList Packet :");
                    builder.Append("\r\n    Version : "); builder.Append(iPacketVersion);
                    builder.Append("\r\n    Fleet ID : "); builder.Append(iFleetID);
                    builder.Append("\r\n    Vehicle ID : "); builder.Append(iVehicleID);
                }
                else if (bSubCmd == DataPersistUpdateSubCmd.ClientUpdate)
                {
                    builder.Append("\r\nData Persist ClientUpdate Packet :");
                    builder.Append("\r\n    Version : "); builder.Append(iPacketVersion);
                    builder.Append("\r\n    Table ID : "); builder.Append(iTableID);
                    builder.Append("\r\n    Fleet ID : "); builder.Append(iFleetID);
                    builder.Append("\r\n    Vehicle ID : "); builder.Append(iVehicleID);
                    builder.Append("\r\n    Reason ID : "); builder.Append(iReasonID);
                    builder.Append("\r\n    Device Time : "); builder.Append(dtDeviceTime.ToString("dd/MM/yyyy HH:mm:ss"));
                }
			}
			catch(System.Exception ex)
			{
                builder.Append("\r\nError Decoding Data Persist Update Packet : " + ex.Message);
			}
			return builder.ToString();
		}
    }
}

﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Application.Communication
{
    public enum LogDirection
    {
        InBound = 0,
        Outbound = 1
    }

    /// <summary>
    /// Listener Log Item
    /// </summary>
    public class ListenerLogItem
    {
        #region private fields
        private int _logId;
        private int _fleetId;
        private string _fleetName;
        private int _vehicleId;
        private string _vehicleDisplayName;
        private string _listenerMessage;
        private string _messageType;
        private LogDirection _direction;
        private DateTime _logDate;
        private string _ipAddress;
        private string _serialNumber;
        #endregion 

        #region properties
        public int LogID
        {
            get { return _logId; }
            set { _logId = value; }
        }

        public int FleetID
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }

        public string FleetName
        {
            get { return _fleetName; }
            set { _fleetName = value; }
        }

        public int VehicleID
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public string VehicleDisplayName
        {
            get { return _vehicleDisplayName; }
            set { _vehicleDisplayName = value; }
        }

        public string ListenerMessage
        {
            get { return _listenerMessage; }
            set { _listenerMessage = value; }
        }

        public string MessageType
        {
            get { return _messageType; }
            set { _messageType = value; }
        }

        public LogDirection Direction
        {
            get { return _direction; }
            set { _direction = value; }
        }

        public DateTime LogDate
        {
            get { return _logDate; }
            set { _logDate = value; }
        }

        public string IPAddress
        {
            get { return _ipAddress; }
            set { _ipAddress = value; }
        }

        public string SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }
        #endregion

        #region Constructor
        public ListenerLogItem()
        { }

        public ListenerLogItem(SqlDataReader sdr)
        {
            _logId = sdr.GetInt32(sdr.GetOrdinal("logID"));
            _fleetId = sdr.GetInt32(sdr.GetOrdinal("fleetID"));
            _fleetName = sdr.GetString(sdr.GetOrdinal("fleetName"));
            _vehicleId = sdr.GetInt32(sdr.GetOrdinal("vehicleID"));
            _vehicleDisplayName = sdr.GetString(sdr.GetOrdinal("DisplayName"));
            _listenerMessage = sdr.GetString(sdr.GetOrdinal("listenerMessage"));
            _messageType = sdr.GetString(sdr.GetOrdinal("messageType"));
            _direction = (LogDirection)Enum.Parse(typeof(LogDirection), sdr.GetInt32(sdr.GetOrdinal("direction")).ToString());
            _logDate = sdr.GetDateTime(sdr.GetOrdinal("logDate"));
            _ipAddress = sdr.GetString(sdr.GetOrdinal("ipAddress"));

            try
            {
                _serialNumber = sdr.GetInt64(sdr.GetOrdinal("serialNumber")).ToString();
            }
            catch (Exception)
            {
                _serialNumber = "Unavailable";
            }
        }
        #endregion

        #region public methods
        public byte[] EncodeByStream(ref string errMsg)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    //add version number
                    PacketUtilities.WriteToStream(stream, (short)1);
                    PacketUtilities.WriteToStream(stream, _logId);
                    PacketUtilities.WriteToStream(stream, _fleetId);
                    PacketUtilities.WriteToStreamNullable(stream, _fleetName);
                    PacketUtilities.WriteToStream(stream, _vehicleId);
                    PacketUtilities.WriteToStreamNullable(stream, _vehicleDisplayName);
                    PacketUtilities.WriteToStreamNullable(stream, _listenerMessage);
                    PacketUtilities.WriteToStreamNullable(stream, _messageType);
                    PacketUtilities.WriteToStream(stream, (short)_direction);
                    PacketUtilities.WriteToStream(stream, _logDate.ToOADate());
                    PacketUtilities.WriteToStreamNullable(stream, _ipAddress);
                    PacketUtilities.WriteToStreamNullable(stream, _serialNumber);

                    return stream.ToArray();
                }
            }
            catch (System.Exception ex)
            {
                errMsg = "Error encoding listener Log Item : " + ex.Message;
            }
            return null;
        }
        public byte[] Encode(ref string errMsg)
        {
            return EncodeByStream(ref errMsg);
        }
        public bool DecodeFromStream(byte[] data, ref string errMsg)
        {
            try
            {
                using (MemoryStream stream = new MemoryStream(data, 0, data.Length, false, false))
                {
                    //read version number
                    short versionNumber = 0;
                    PacketUtilities.ReadFromStream(stream, ref versionNumber);
                    PacketUtilities.ReadFromStream(stream, ref _logId);
                    PacketUtilities.ReadFromStream(stream, ref _fleetId);
                    PacketUtilities.ReadFromStreamNullable(stream, ref _fleetName);
                    PacketUtilities.ReadFromStream(stream, ref _vehicleId);
                    PacketUtilities.ReadFromStreamNullable(stream, ref _vehicleDisplayName);
                    PacketUtilities.ReadFromStreamNullable(stream, ref _listenerMessage);
                    PacketUtilities.ReadFromStreamNullable(stream, ref _messageType);
                    short direction = 0;
                    PacketUtilities.ReadFromStream(stream, ref direction);
                    _direction = (LogDirection)direction;
                    double date = 0;
                    PacketUtilities.ReadFromStream(stream, ref date);
                    _logDate = DateTime.FromOADate(date);
                    PacketUtilities.ReadFromStreamNullable(stream, ref _ipAddress);
                    PacketUtilities.ReadFromStreamNullable(stream, ref _serialNumber);
                }
                return true;
            }
            catch (System.Exception ex)
            {
                errMsg = "Error decoding : " + ex.Message;
            }
            return false;
        }
        public bool Decode(byte[] bData, ref string sErrMsg)
        {
            return DecodeFromStream(bData, ref sErrMsg);
        }
        #endregion

    }
}

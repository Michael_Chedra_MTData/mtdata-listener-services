using System;
using System.Collections.Generic;
using System.Text;
using MTData.Common.Utilities;
using System.IO;
using System.Linq;

namespace MTData.Transport.Application.Communication
{
    public class DriverUpdate
    {

        public enum DriverUpdateSubCmd : byte
        {
            Create = (byte)'C',
            Delete = (byte)'D',
            Update = (byte)'U',
            FatigueUpdate = (byte)'F'
        };

        #region Private Variables
        private int _driverID;
        private int _fleetID;
        private short _packetVersion = 1;


        public char Cmd = 'D';
        public DriverUpdateSubCmd SubCmd = DriverUpdateSubCmd.Update;

        #endregion

        #region Properties

        public short PacketVersion
        {
            get { return _packetVersion; }
            set { _packetVersion = value; }
        }

        public int DriverID
        {
            get { return _driverID; }
            set { _driverID = value; }
        }

        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// The default Constructor.
        /// </summary>
        public DriverUpdate()
        {
        }

        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion


        public bool Decode(byte[] bData, ref string sErrMsg)
        {
            bool bRet = false;
            byte bTemp = (byte)0x00;
            byte bEOP = (byte)0x07;
            char cType = (char)0x00;
            int iPos = 0;
            short iLength = 0;

            try
            {
                if (bData[iPos++] != (byte)0x01)														    // SOP
                {
                    sErrMsg = "Error decoding : SOP not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iLength);					        // Length
                if (bData.Length != iLength)
                {
                    sErrMsg = "Error decoding : Packet length is incorrect.";
                    return bRet;
                }
                if (bData[iLength - 1] != bEOP)
                {
                    sErrMsg = "Error decoding : EOP was not found.";
                    return bRet;
                }
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref cType);					        // Packet Type (1 byte)
                if (cType != Cmd)
                {
                    sErrMsg = "Error decoding : Packet type is incorrect.";
                    return bRet;
                }
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bTemp);					        // Sub Command (1 byte)

                if (!Enum.GetValues(typeof(DriverUpdateSubCmd)).Cast<byte>().Contains(bTemp))
                {
                    sErrMsg = "Error decoding : Packet sub command is not valid.";
                    return bRet;
                }
                SubCmd = (DriverUpdateSubCmd)bTemp;

                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this._packetVersion);	            // Packet Version (2 byte int)
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this._fleetID);					    // Fleet ID (4 byte int)
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this._driverID);
                bRet = true;
            }
            catch (System.Exception ex)
            {
                sErrMsg = "Error encoding live update : " + ex.Message;
                bRet = false;
            }
            return bRet;
        }

        public byte[] Encode(ref string sErrMsg)
        {
            int iPos = 0;
            byte bEOP = (byte)0x07;
            byte bSOP = (byte)0x01;
            byte[] bResult = null;
            MemoryStream oMS = new MemoryStream();

            try
            {
                PacketUtilities.WriteToStream(oMS, bSOP);						        // SOP
                PacketUtilities.WriteToStream(oMS, (byte)0x00);				        // Length 1
                PacketUtilities.WriteToStream(oMS, (byte)0x00);				        // Length 2
                PacketUtilities.WriteToStream(oMS, Cmd);						        // Packet Type (1 byte)
                PacketUtilities.WriteToStream(oMS, (byte)SubCmd);				        // Sub Command (1 byte)
                PacketUtilities.WriteToStream(oMS, PacketVersion);                      // Packet Version (2 byte int)

                PacketUtilities.WriteToStream(oMS, FleetID);                            // Fleet ID (4 byte int)
                PacketUtilities.WriteToStream(oMS, DriverID);                            // Driver ID (4 byte int)  

                PacketUtilities.WriteToStream(oMS, bEOP);                               // End of Packet (1 byte)

                bResult = oMS.ToArray();
                iPos = bResult.Length;

                // Write in the length bytes
                bResult[1] = ((byte[])BitConverter.GetBytes(iPos))[0];
                bResult[2] = ((byte[])BitConverter.GetBytes(iPos))[1];
            }
            catch (System.Exception ex)
            {
                sErrMsg = "Error encoding update : " + ex.Message;
                bResult = null;
            }
            return bResult;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Timers;
using System.Xml;
using Amazon.SQS;
using Amazon.SQS.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using MTData.Common.Threading;
using log4net;

namespace MTData.Transport.Application.Communication.SQS
{
    public class SQSStream : IDisposable
    {
        #region private fields
        private ILog _log = LogManager.GetLogger(typeof(SQSStream));
        private AmazonSQSClient _sqsClient;
        private object _statsSync = new object();
        private long _insertCount;
        private long _readCount;
        #endregion

        #region properties
        public string QueueUrl { get; set; }

        public List<int> FleetIdFilter { get; set; }
        public List<Tuple<int,int>> VehicleIdFilter { get; set; }
        public List<Tuple<int, int>> DriverIdFilter { get; set; }
        public List<int> ReasonCodeIdFilter { get; set; }
        public List<int> ReasonCodeIdIgnoreFilter { get; set; }

        public bool LogStats { get; set; }
        public long InsertCount
        {
            get
            {
                long ret = 0;
                lock (_statsSync)
                {
                    ret = _insertCount;
                    _insertCount = 0;
                }
                return ret;
            }
        }
        public long ReadCount
        {
            get
            {
                long ret = 0;
                lock (_statsSync)
                {
                    ret = _readCount;
                    _readCount = 0;
                }
                return ret;
            }
        }
        #endregion

        #region Constructors
        public SQSStream() { }

        public SQSStream(AmazonSQSClient client)
        {
            _sqsClient = client;
        }
        #endregion

        public void InsertObject(string message)
        {
            SendMessageRequest req = new SendMessageRequest(QueueUrl, message);
            _sqsClient.SendMessage(req); 
            if (LogStats)
            {
                lock (_statsSync)
                {
                    _insertCount++;
                }
            }
        }

        public string PeekMessage(TimeSpan timeout)
        {
            return PeekMessageInternal(timeout, 1).Body;
        }


        public string ReceiveMessage(TimeSpan timeout)
        {
            Message message = PeekMessageInternal(timeout, 1);
            _sqsClient.DeleteMessage(QueueUrl, message.ReceiptHandle);
            return message.Body;
        }

        public bool CheckFilters(int? fleetId, int? vehicleId, int? driverId, int? reasonId)
        {
            if (fleetId.HasValue && FleetIdFilter != null && FleetIdFilter.Count > 0)
            {
                if (!FleetIdFilter.Contains(fleetId.Value))
                {
                    return false;
                }
            }

            if (fleetId.HasValue && vehicleId.HasValue && VehicleIdFilter != null && VehicleIdFilter.Count > 0)
            {
                if (!VehicleIdFilter.Contains(new Tuple<int, int>(fleetId.Value, vehicleId.Value)))
                {
                    return false;
                }
            }

            if (fleetId.HasValue && driverId.HasValue && DriverIdFilter != null && DriverIdFilter.Count > 0)
            {
                if (!DriverIdFilter.Contains(new Tuple<int, int>(fleetId.Value, driverId.Value)))
                {
                    return false;
                }
            }

            if (reasonId.HasValue && ReasonCodeIdFilter != null && ReasonCodeIdFilter.Count > 0)
            {
                if (!ReasonCodeIdFilter.Contains(reasonId.Value))
                {
                    return false;
                }
            }

            if (reasonId.HasValue && ReasonCodeIdIgnoreFilter != null && ReasonCodeIdIgnoreFilter.Count > 0)
            {
                if (ReasonCodeIdIgnoreFilter.Contains(reasonId.Value))
                {
                    return false;
                }
            }

            return true;

        }

        public void CreateStreamFromXml(XmlNode node)
        {
            var sqsConfig = new AmazonSQSConfig();
            sqsConfig.ServiceURL = node.Attributes.GetNamedItem("ServiceUrl").Value;
            XmlNode AccessKeyNode = node.Attributes.GetNamedItem("AccessKey");
            XmlNode SecretKeyNode = node.Attributes.GetNamedItem("SecretKey");
            if ((AccessKeyNode != null && !string.IsNullOrEmpty(AccessKeyNode.Value))
                || (SecretKeyNode != null && !string.IsNullOrEmpty(SecretKeyNode.Value)))
            {
                _sqsClient = new AmazonSQSClient(AccessKeyNode.Value, SecretKeyNode.Value, sqsConfig);
            }
            else
            {
                _sqsClient = new AmazonSQSClient(sqsConfig);
            }


            QueueUrl = node.Attributes.GetNamedItem("QueueUrl").Value;
            LogStats = Convert.ToBoolean(node.Attributes.GetNamedItem("LogStats").Value);

            FleetIdFilter = node.SelectNodes("fleetFilter").Cast<XmlNode>().
                Select(n => Convert.ToInt32(n.Attributes["fleetId"].Value)).ToList();

            VehicleIdFilter = node.SelectNodes("vehicleFilter").Cast<XmlNode>().
                Select(n => new Tuple<int, int>(
                    Convert.ToInt32(n.Attributes["fleetId"].Value), 
                    Convert.ToInt32(n.Attributes["vehicleId"].Value)
                    )).ToList();

            DriverIdFilter = node.SelectNodes("driverFilter").Cast<XmlNode>().
                Select(n => new Tuple<int, int>(
                    Convert.ToInt32(n.Attributes["fleetId"].Value),
                    Convert.ToInt32(n.Attributes["driverId"].Value)
                    )).ToList();

            ReasonCodeIdFilter = node.SelectNodes("reasonCodeFilter").Cast<XmlNode>().
                Select(n => Convert.ToInt32(n.Attributes["reasonId"].Value)).ToList();

            ReasonCodeIdIgnoreFilter = node.SelectNodes("reasonCodeIgnoreFilter").Cast<XmlNode>().
                Select(n => Convert.ToInt32(n.Attributes["reasonId"].Value)).ToList();

        }

        public void Dispose()
        {
            if (_sqsClient != null)
            {
                _sqsClient.Dispose();
            }
        }


        private Message PeekMessageInternal(TimeSpan timeout, int maxNumberOfMessages)
        {
            ReceiveMessageRequest rmr = new ReceiveMessageRequest(QueueUrl);
            rmr.WaitTimeSeconds = (int)timeout.TotalSeconds;
            rmr.MaxNumberOfMessages = maxNumberOfMessages;
            ReceiveMessageResponse receiveMessageResponse = _sqsClient.ReceiveMessage(rmr);
            if (receiveMessageResponse.Messages.Count > 0)
            {
                return receiveMessageResponse.Messages[0];
            }
            else
            {
                // For consistency with MSMQ throw exception
                // Could be no messages in the queue
                // Should throw more specific exception
                throw new Exception("Failed to retreive message from queue within specified timeout");
            }
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;

namespace MTData.Transport.Application.Communication.SQS
{
    public abstract class SQSStreamManager: IDisposable
    {
        private List<SQSStream> _streams = new List<SQSStream>();

        private object _statsSync = new object();
        private long _insertCount;
        private long _readCount;
        private TimeSpan _span = new TimeSpan(0, 0, 1);

        public List<long> InsertCount
        {
            get
            {
                List<long> result = new List<long>();
                lock (_statsSync)
                {
                    result.Add(_insertCount);
                    _insertCount = 0;
                }
                _streams.ForEach(s => result.Add(s.InsertCount));
                return result;
            }
        }


        public void InsertObject(string message)
        {
            lock (_statsSync)
            {
                _insertCount++;
            }
            _streams.ForEach(s => s.InsertObject(message));
        }

        public void InsertObjectWithFilters(string message, int? fleetId, int? vehicleId, int? driverId, int? reasonId)
        {
            lock (_statsSync)
            {
                _insertCount++;
            }
            _streams.ForEach(s =>
            {
                if (s.CheckFilters(fleetId, vehicleId, driverId, reasonId))
                {
                    s.InsertObject(message);
                }
            });
        }

        public List<string> PeekMessages()
        {
            List<string> messages = new List<string>();
            _streams.ForEach(s =>
            {
                string m = s.PeekMessage(_span);
                if (!string.IsNullOrEmpty(m))
                {
                    messages.Add(m);
                }
            });
            return messages;
        }
        public List<string> ReadMessages()
        {
            List<string> messages = new List<string>();
            _streams.ForEach(s =>
            {
                string m = s.ReceiveMessage(_span);
                if (!string.IsNullOrEmpty(m))
                {
                    messages.Add(m);
                }
            });
            return messages;
        }


        protected void CreateStreamsFromXml(XmlNodeList nodeList)
        {
            foreach (XmlNode n in nodeList)
            {
                SQSStream s = new SQSStream();
                s.CreateStreamFromXml(n);
                _streams.Add(s);
            }
        }


        public void Dispose()
        {
            _streams.ForEach(s => s.Dispose());
        }

        protected void AddStream(SQSStream stream)
        {
            _streams.Add(stream);
        }

    }
}

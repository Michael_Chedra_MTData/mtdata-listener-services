﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Xml;
using log4net;

namespace MTData.Transport.Application.Communication.SQS
{
    public class StringToSQS : SQSStreamManager, System.Configuration.IConfigurationSectionHandler
    {
        private ILog _log = LogManager.GetLogger(typeof(StringToSQS));

        public StringToSQS()
        {
        }
        public StringToSQS(SQSStream stream)
        {
            AddStream(stream);
        }

        #region IConfigurationSectionHandler Members
        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        ///     <UpdatesToSQS>
        ///         <SQSStream ServiceUrl = "https://sqs.ap-southeast-2.amazonaws.com"
        ///             QueueUrl="https://sqs.ap-southeast-2.amazonaws.com/036843008788/MTData_Transurban_Test"
        /// 			AccessKey="key" SecretKey="skey"
        ///             LogStats="true">
        ///         </SQSStream>
        ///     </UpdatesToSQS>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                XmlNodeList list = section.SelectNodes("SQSStream");
                base.CreateStreamsFromXml(list);
            }
            catch (Exception ex)
            {
                _log.Error("Error creating Kinesis streams", ex);
            }
            return this;
        }
        #endregion
    }
}

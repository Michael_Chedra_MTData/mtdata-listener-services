using System;
using System.Text;
using System.Collections;
using System.IO;
using MTData.Common.Utilities;


namespace MTData.Transport.Application.Communication
{
	/// <summary>
	/// Summary description for LiveUpdate.
	/// </summary>
	public class LiveUpdate
	{
		public short iPacketVersion = 1;

		public short iFleetID = 0;
		public int iVehicleID = 0;
		public bool bIsTrackingUnit = true;
		public bool bIsRefrigerationUnit = false;
		public int iReasonID = 0;
		public DateTime dtGPSTime = System.DateTime.MinValue;
		public DateTime dtDeviceTime = System.DateTime.MinValue;
		public DateTime dtServerTime = System.DateTime.MinValue;
		public decimal dLat = 0;
		public decimal dLong = 0;
		public short iDirection = 0;
		public byte bSpeed = (byte) 0x00;
		public string sRawGPS = "";
		public byte bFlags = (byte) 0x00;
		public string sUserDefined = "";
		public byte bInputStatus = (byte) 0x00;
		public byte bOutputStatus = (byte) 0x00;
		public short iLightStatus = 0;
		public short iButtonStatus = 0;
		public byte bMaxSpeed = (byte) 0x00;
		public long lSpeedAcc = 0;

		public short iCoolantTemperature = 0;
		public short iOilTemp = 0;
		public short iOilPressure = 0;
		public short iGear = 0;
		public short iMaxRPM = 0;
		public short iBrakeApplications = 0;
		public float fGForceFront = 0;
		public float fGForceBack = 0;
		public float fGForceLeftRight = 0;

		public int iTotalEngineHours = 0;
		public int iTripFuel = 0;

		public int iExtendedValue1 = 0;
		public int iExtendedValue2 = 0;
		public int iExtendedValue3 = 0;
		public int iExtendedValue4 = 0;

		public string sPosition = "";
		public string sPlaceName = "";
		public decimal dDistance = 0;
		public string sMapRef = "";
		public string sUserInfo = "";
		public string sVehicleTag = "";

		public short iBatteryVolts = 0;
		public short iBrakeUsageSeconds = 0;
		public uint iOdometer = 0;
		public short iRPMZone1 = 0;
		public short iRPMZone2 = 0;
		public short iRPMZone3 = 0;
		public short iRPMZone4 = 0;
		public short iSpeedZone1 = 0;
		public short iSpeedZone2 = 0;
		public short iSpeedZone3 = 0;
		public short iSpeedZone4 = 0;
		public int iTotalFuelUsed = 0;

		public int iTrailer1ID = 0;
		public string sTrailer1Name = "";
		public int iTrailer2ID = 0;
		public string sTrailer2Name = "";
		public int iTrailer3ID = 0;
		public string sTrailer3Name = "";
		public int iTrailer4ID = 0;
		public string sTrailer4Name = "";
		public int iTrailer5ID = 0;
		public string sTrailer5Name = "";

		public double dFuelEconomy = 0;
		public long lExtendedStatus = 0;
		public long lAuxilaryStatus = 0;
		public int iSetPointGroupID = 0;
		public int iSetPointID = 0;

		public bool bHasRefrigerationValues = false;
		public int iReferFlags = 0;
		public decimal dReferFuelPercentage = 0;
		public decimal dReferBatteryVolts = 0;
		public bool bReferInput1 = false;
		public bool bReferInput2 = false;
		public bool bReferInput3 = false;
		public bool bReferInput4 = false;
		public short iReferSensorsAvaliable = 0;
		public decimal dReferHumidity = 0;
		public float fReferSensor1 = 0;
		public float fReferSensor2 = 0;
		public float fReferSensor3 = 0;
		public float fReferSensor4 = 0;
		public float fReferSensor5 = 0;
		public float fReferSensor6 = 0;

		public int iReferZ1Zone = 0;
		public int iReferZ1OperatingMode = 0;
		public int iReferZ1Alarms = 0;
		public short iReferZ1SensorsAvaliable = 0;
		public float fReferZ1ReturnAir1 = 0;
		public float fReferZ1ReturnAir2 = 0;
		public float fReferZ1SupplyAir1 = 0;
		public float fReferZ1SupplyAir2 = 0;
		public float fReferZ1Setpoint = 0;
		public float fReferZ1Evap = 0;

		public int iReferZ2Zone = 0;
		public int iReferZ2OperatingMode = 0;
		public int iReferZ2Alarms = 0;
		public short iReferZ2SensorsAvaliable = 0;
		public float fReferZ2ReturnAir1 = 0;
		public float fReferZ2ReturnAir2 = 0;
		public float fReferZ2SupplyAir1 = 0;
		public float fReferZ2SupplyAir2 = 0;
		public float fReferZ2Setpoint = 0;
		public float fReferZ2Evap = 0;

		public int iReferZ3Zone = 0;
		public int iReferZ3OperatingMode = 0;
		public int iReferZ3Alarms = 0;
		public short iReferZ3SensorsAvaliable = 0;
		public float fReferZ3ReturnAir1 = 0;
		public float fReferZ3ReturnAir2 = 0;
		public float fReferZ3SupplyAir1 = 0;
		public float fReferZ3SupplyAir2 = 0;
		public float fReferZ3Setpoint = 0;
		public float fReferZ3Evap = 0;

        public float fTotalMass = 0;
        public float fWeightLimit = 0;

		public cPluginData[] oPluginData = null;

		public LiveUpdate()
		{

		}

		public void AddPluginDataField(cPluginData oPluginEntry)
		{
			cPluginData[] oTemp = null;

			if(oPluginData == null)
			{
				oPluginData = new cPluginData[1];
				oPluginData[0] = oPluginEntry;
			}
			else
			{
				oTemp = new cPluginData[oPluginData.Length + 1];
				for(int X = 0; X < oPluginData.Length; X++)
				{
					oTemp[X] = oPluginData[X];
				}
				oTemp[oTemp.Length -1] = oPluginEntry;
				oPluginData = oTemp;
			}
		}

		public byte[] EncodeByStream(ref string sErrMsg)
		{
			int iTrailerCount = 0;
			byte bEOS = (byte) 0x04;
			byte bEOP = (byte) 0x07;
			byte bSOP = (byte) 0x01;
			bool bZone1 = false;
			bool bZone2 = false;
			bool bZone3 = false;
			byte[] bResult = null;
			short isValue = 0;
			byte bUnitType = (byte) 0x00;
			int iPluginCount = 0;
			MemoryStream oStream = null;

			try
			{
				oStream = new MemoryStream();
				#region Determine the unit type byte
				if(bIsTrackingUnit)
					bUnitType = (byte) 0x01;
				if(bIsRefrigerationUnit)
					bUnitType += (byte) 0x02;
				#endregion

				if(oPluginData != null)
				{
					for (int X = 0; X < oPluginData.Length; X++)
					{
						if(oPluginData[X].PluginData != null)
						{
							iPluginCount++;
						}
					}
				}


				PacketUtilities.WriteToStream(oStream, bSOP);								// SOP
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Length 1
				PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Length 2
				PacketUtilities.WriteToStream(oStream, "L");									// Packet Type (1 byte)
				PacketUtilities.WriteToStream(oStream, "U");									// Sub Command (1 byte)
				PacketUtilities.WriteToStream(oStream, iPacketVersion);				// Packet Version (2 byte int)
				PacketUtilities.WriteToStream(oStream, iFleetID);							// Fleet ID (2 byte int)
				PacketUtilities.WriteToStream(oStream, iVehicleID);						// Vehicle ID (4 byte int)
				PacketUtilities.WriteToStream(oStream, bUnitType);						// Unit Type (1 byte)
				PacketUtilities.WriteToStream(oStream, iReasonID);						// Reason ID (4 byte int)
				PacketUtilities.WriteToStream(oStream, dtGPSTime.ToOADate());	// GPS Time (8 byte double)
				PacketUtilities.WriteToStream(oStream, dtDeviceTime.ToOADate());	// Device Time (8 byte double)
				PacketUtilities.WriteToStream(oStream, dtServerTime.ToOADate());	// Server Time (8 byte double)
				PacketUtilities.WriteToStream(oStream, Convert.ToInt32(dLat * 100000)); // Latitude (4 byte int)
				PacketUtilities.WriteToStream(oStream, Convert.ToInt32(dLong * 100000));	// Longitude (8 byte double)
				PacketUtilities.WriteToStream(oStream, bFlags);								// Flags (1 byte)
				PacketUtilities.WriteToStream(oStream, lSpeedAcc);						// Speed Accumalator(8 byte long)
				PacketUtilities.WriteToStream(oStream, iDirection);						// Direction (2 byte int)
				PacketUtilities.WriteToStream(oStream, bSpeed);							// Speed (1 byte int)
				PacketUtilities.WriteToStream(oStream, bMaxSpeed);					// Max Speed (1 byte int)
				PacketUtilities.WriteToStream(oStream, bInputStatus);					// Input Status (1 byte)
				PacketUtilities.WriteToStream(oStream, bOutputStatus);				// Output Status (1 byte)
				PacketUtilities.WriteToStream(oStream, iLightStatus);					// Light Status (2 byte short)
				PacketUtilities.WriteToStream(oStream, iButtonStatus);					// Button Status (2 byte short)

				if (iCoolantTemperature > 0 || iOilTemp > 0 || iOilPressure > 0 || iGear > 0 || iMaxRPM > 0 || iBrakeApplications > 0 || fGForceFront > 0 || fGForceBack > 0 || fGForceLeftRight > 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Engine Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, iCoolantTemperature);	// Coolant Temperature (2 byte int)
					PacketUtilities.WriteToStream(oStream, iOilTemp);						// Oil Temperature (2 byte int)
					PacketUtilities.WriteToStream(oStream, iOilPressure);					// Oil Pressure (2 byte int)
					PacketUtilities.WriteToStream(oStream, iGear);								// Gear Position (2 byte int)
					PacketUtilities.WriteToStream(oStream, iMaxRPM);						// Max RPM (2 byte int)
					PacketUtilities.WriteToStream(oStream, iBrakeApplications);		// Brake Applications (2 byte int)
					if(fGForceFront > 0 || fGForceBack > 0 || fGForceLeftRight > 0)
					{
						PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// G-Force Incuded = true (1 byte)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fGForceFront * (float) 100));		// Forward G-Force (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fGForceBack * (float) 100));		// Backwards G-Force (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fGForceLeftRight * (float) 100));// Lateral G-Force (2 byte int)
					}
					else
						PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// G-Force Incuded = false (1 byte)
				}
				else
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Engine Data Incuded = false (1 byte)
				}

				if(iTotalEngineHours > 0 || iTripFuel > 0 || dFuelEconomy > 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Fuel Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, iTotalEngineHours);		// Engine Hours (4 byte int)
					PacketUtilities.WriteToStream(oStream, iTripFuel);						// Trip Fuel (4 byte int)
					PacketUtilities.WriteToStream(oStream, Convert.ToInt32(dFuelEconomy * 100));	// Fuel Economy (4 byte int)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Fuel Data Incuded = false (1 byte)


				if(iExtendedValue1 != 0 || iExtendedValue2 != 0 || iExtendedValue3 != 0 || iExtendedValue4 != 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Extended Value Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, iExtendedValue1);			// Extended Value 1 (4 byte int)
					PacketUtilities.WriteToStream(oStream, iExtendedValue2);			// Extended Value 2 (4 byte int)
					PacketUtilities.WriteToStream(oStream, iExtendedValue3);			// Extended Value 3 (4 byte int)
					PacketUtilities.WriteToStream(oStream, iExtendedValue4);			// Extended Value 4 (4 byte int)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Extended Value Data Incuded = false (1 byte)


				if(iBatteryVolts != 0 || iBrakeUsageSeconds != 0 || iOdometer != 0 || iRPMZone1 != 0 || iRPMZone2 != 0 || iRPMZone3 != 0 || iRPMZone4 != 0 || iSpeedZone1 != 0 || iSpeedZone2 != 0 || iSpeedZone3 != 0 || iSpeedZone4 != 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Extended Engine Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, iBatteryVolts);					// Battery Volts (2 byte int)
					PacketUtilities.WriteToStream(oStream, iBrakeUsageSeconds);	// Brake Usage Seconds (2 byte int)
					PacketUtilities.WriteToStream(oStream, iOdometer);						// Odometer (4 byte int)
					PacketUtilities.WriteToStream(oStream, iTotalFuelUsed);				// Total Fuel Used(4 byte int)

					if(iRPMZone1 != 0 || iRPMZone2 != 0 || iRPMZone3 != 0 || iRPMZone4 != 0)
					{
						PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// RPM Zone Data Incuded = true (1 byte)
						PacketUtilities.WriteToStream(oStream, iRPMZone1);						// RPM Zone 1 (2 byte int)
						PacketUtilities.WriteToStream(oStream, iRPMZone2);					// RPM Zone 2 (2 byte int)
						PacketUtilities.WriteToStream(oStream, iRPMZone3);					// RPM Zone 3 (2 byte int)
						PacketUtilities.WriteToStream(oStream, iRPMZone4);					// RPM Zone 4 (2 byte int)
					}
					else
						PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// RPM Zone Data Incuded = false (1 byte)

					if(iSpeedZone1 != 0 || iSpeedZone2 != 0 || iSpeedZone3 != 0 || iSpeedZone4 != 0)
					{
						PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Speed Zone Data Incuded = true (1 byte)
						PacketUtilities.WriteToStream(oStream, iSpeedZone1);					// Speed Zone 1 (2 byte int)
						PacketUtilities.WriteToStream(oStream, iSpeedZone2);					// Speed Zone 2 (2 byte int)
						PacketUtilities.WriteToStream(oStream, iSpeedZone3);					// Speed Zone 3 (2 byte int)
						PacketUtilities.WriteToStream(oStream, iSpeedZone4);					// Speed Zone 4 (2 byte int)
					}
					else
						PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Speed Zone Data Incuded = false (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Extended Engine Data Incuded = false (1 byte)

				PacketUtilities.WriteToStream(oStream, lExtendedStatus);			// Extended Status (8 byte long)
				PacketUtilities.WriteToStream(oStream, lAuxilaryStatus);				// Auxilary Status (8 byte long)
				if(iSetPointGroupID != 0 || iSetPointID != 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Set Point Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, iSetPointGroupID);			// Setpoint Group ID (4 byte int)
					PacketUtilities.WriteToStream(oStream, iSetPointID);					// Setpoint ID (4 byte int)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Set Point Data Incuded = false (1 byte)

				if(iReferFlags != 0 || dReferFuelPercentage != 0 || dReferBatteryVolts != 0 || bReferInput1 || bReferInput2 || bReferInput3 || bReferInput4 || dReferHumidity != 0 || fReferSensor1 != 0 || fReferSensor2 != 0 || fReferSensor3 != 0 || fReferSensor4 != 0 || fReferSensor5 != 0 || fReferSensor6 != 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Refrigeration Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, iReferFlags);					// Refrigeration Flags (4 byte int)
					PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(Convert.ToInt16(((float) dReferFuelPercentage * (float) 2))))[0]);	// Refrigeration Fuel Percentage * 2 (1 byte int)
					PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(Convert.ToInt16(((float) dReferBatteryVolts * (float) 2))))[0]);		// Refrigeration Battery Volts * 2 (1 byte int)
					int iReferInputState = (bReferInput1?1:0) + (bReferInput2?2:0) + (bReferInput3?4:0) + (bReferInput3?8:0);
					PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferInputState))[0]);			// Refrigeration Input Status (1 byte)
					PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferSensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
					PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(Convert.ToInt16(((float) dReferHumidity * (float) 2))))[0]);	// Refrigeration Humidity (1 byte int)
					isValue = Convert.ToInt16(fReferSensor1 * (float) 10);
					PacketUtilities.WriteToStream(oStream, isValue);																	// Refrigeration Sensor 1 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor2 * (float) 10);
					PacketUtilities.WriteToStream(oStream, isValue);																	// Refrigeration Sensor 2 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor3 * (float) 10);
					PacketUtilities.WriteToStream(oStream, isValue);																	// Refrigeration Sensor 3 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor4 * (float) 10);
					PacketUtilities.WriteToStream(oStream, isValue);																	// Refrigeration Sensor 4 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor5 * (float) 10);
					PacketUtilities.WriteToStream(oStream, isValue);																	// Refrigeration Sensor 5 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor6 * (float) 10);
					PacketUtilities.WriteToStream(oStream, isValue);																	// Refrigeration Sensor 6 * 10 (2 byte int)

					if (iReferZ1Zone > 0 && iReferZ2Zone > 0 && iReferZ3Zone > 0)				// Number of Zones (1 byte int)
					{
						PacketUtilities.WriteToStream(oStream, (byte) 0x03);
						bZone1 = true;
						bZone2 = true;
						bZone3 = true;
					}
					else if (iReferZ1Zone > 0 && iReferZ2Zone > 0 && iReferZ3Zone == 0)
					{
						PacketUtilities.WriteToStream(oStream, (byte) 0x02);
						bZone1 = true;
						bZone2 = true;
					}
					else if (iReferZ1Zone > 0 && iReferZ2Zone == 0 && iReferZ3Zone == 0)
					{
						PacketUtilities.WriteToStream(oStream, (byte) 0x01);
						bZone1 = true;
					}
					else
						PacketUtilities.WriteToStream(oStream, (byte) 0x00);

					if (bZone1)																											// Refrigeration Zone 1 Data
					{
						PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferZ1Zone))[0]);		// Refrigeration Zone ID (1 byte int)
						PacketUtilities.WriteToStream(oStream, (short) iReferZ1OperatingMode);										// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.WriteToStream(oStream, (short) iReferZ1Alarms);														// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferZ1SensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ1ReturnAir1 * (float) 10));		// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ1ReturnAir2 * (float) 10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ1SupplyAir1 * (float) 10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ1SupplyAir2 * (float) 10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ1Setpoint * (float) 10));			// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ1Evap * (float) 10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (bZone2)																											// Refrigeration Zone 2 Data
					{
						PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferZ2Zone))[0]);		// Refrigeration Zone ID (1 byte int)
						PacketUtilities.WriteToStream(oStream, (short) iReferZ2OperatingMode);										// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.WriteToStream(oStream, (short) iReferZ2Alarms);													// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferZ2SensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ2ReturnAir1 * (float) 10));		// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ2ReturnAir2 * (float) 10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ2SupplyAir1 * (float) 10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ2SupplyAir2 * (float) 10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ2Setpoint * (float) 10));			// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ2Evap * (float) 10));				// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (bZone3)																											// Refrigeration Zone 3 Data
					{
						PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferZ3Zone))[0]);		// Refrigeration Zone ID (1 byte int)
						PacketUtilities.WriteToStream(oStream, (short) iReferZ3OperatingMode);										// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.WriteToStream(oStream, (short) iReferZ3Alarms);													// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iReferZ3SensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ3ReturnAir1 * (float) 10));		// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ3ReturnAir2 * (float) 10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ3SupplyAir1 * (float) 10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ3SupplyAir2 * (float) 10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ3Setpoint * (float) 10));			// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.WriteToStream(oStream, Convert.ToInt16(fReferZ3Evap * (float) 10));				// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Refrigeration Data Incuded = false (1 byte)

				if(sRawGPS.Length > 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Raw GPS Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, sRawGPS);						// Raw GPS (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Raw GPS Data Incuded = false (1 byte)

				if(sUserDefined.Length > 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// User Defined Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, sUserDefined);				// User Defined (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// User Defined Data Incuded = false (1 byte)

				if(sPosition.Length > 0 || sPlaceName.Length > 0 || sMapRef.Length > 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);						// Location Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, (float) dDistance);			// Distance From Nearest Place (4 byte float)
					PacketUtilities.WriteToStream(oStream, sPosition);							// Direction From Place (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
					PacketUtilities.WriteToStream(oStream, sPlaceName);					// Placename (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
					PacketUtilities.WriteToStream(oStream, sMapRef);							// Map Reference (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Location Data Incuded = false (1 byte)

				if(sUserInfo.Length > 0 || sVehicleTag.Length > 0)
				{
					PacketUtilities.WriteToStream(oStream, (byte) 0x01);					// Vehicle Label Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oStream, sUserInfo);						// User Info (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
					PacketUtilities.WriteToStream(oStream, sVehicleTag);					// Vehicle Tag (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oStream, (byte) 0x00);					// Vehicle Label Incuded = false (1 byte)

				if (iTrailer1ID > 0)
					iTrailerCount++;
				if (iTrailer2ID > 0)
					iTrailerCount++;
				if (iTrailer3ID > 0)
					iTrailerCount++;
				if (iTrailer4ID > 0)
					iTrailerCount++;
				if (iTrailer5ID > 0)
					iTrailerCount++;
				// Number of Trailers (1 byte int)
				PacketUtilities.WriteToStream(oStream, ((byte[]) BitConverter.GetBytes(iTrailerCount))[0]);

				if (iTrailer1ID > 0)
				{
					PacketUtilities.WriteToStream(oStream, iTrailer1ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oStream, sTrailer1Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer2ID > 0)
				{
					PacketUtilities.WriteToStream(oStream, iTrailer2ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oStream, sTrailer2Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer3ID > 0)
				{
					PacketUtilities.WriteToStream(oStream, iTrailer3ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oStream, sTrailer3Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer4ID > 0)
				{
					PacketUtilities.WriteToStream(oStream, iTrailer4ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oStream, sTrailer4Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer5ID > 0)
				{
					PacketUtilities.WriteToStream(oStream, iTrailer5ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oStream, sTrailer5Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oStream, bEOS);								// End of String Delimeter (1 byte)
				}

                if (iPacketVersion >= 5)
                {
                    if (fTotalMass > 0 || fWeightLimit > 0)
                    {
                        PacketUtilities.WriteToStream(oStream, (byte)0x01);					// Mass Incuded = true (1 byte)
                        PacketUtilities.WriteToStream(oStream, fTotalMass);                 // last total mass declaration
                        PacketUtilities.WriteToStream(oStream, fWeightLimit);               // vehicle's weight limit
                    }
                    else
                    {
                        PacketUtilities.WriteToStream(oStream, (byte)0x00);					// Mass Incuded = false (1 byte)
                    }
                }

				PacketUtilities.WriteToStream(oStream,  (byte) iPluginCount);			// Plugin Count (1 byte int)
				if(oPluginData != null)
				{
					for (int X = 0; X < oPluginData.Length; X++)
					{
						if(oPluginData[X].PluginData != null)
						{
							PacketUtilities.WriteToStream(oStream,  (int)oPluginData[X].PluginID);	// Plugin ID (4 byte int)
							PacketUtilities.WriteToStream(oStream,  Convert.ToInt16(oPluginData[X].PluginData.Length));	// Plugin Data Length (2 byte short)
							PacketUtilities.WriteToStream(oStream,  oPluginData[X].PluginData);	// Plugin Data  (Byte[])
						}
					}
				}

				PacketUtilities.WriteToStream(oStream, bEOP);								// End of Packet (1 byte)

				bResult = oStream.ToArray();
				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(bResult.Length))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding live update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}

		public byte[] Encode(ref string sErrMsg)
		{
			int iTrailerCount = 0;
			int iPos = 0;
			byte bEOS = (byte) 0x04;
			byte bEOP = (byte) 0x07;
			byte bSOP = (byte) 0x01;
			bool bZone1 = false;
			bool bZone2 = false;
			bool bZone3 = false;
			byte[] bResult = null;
			short isValue = 0;
			byte bUnitType = (byte) 0x00;
			int iPluginCount = 0;
			MemoryStream oMS = new MemoryStream();
			
			try
			{
				#region Determine the unit type byte
				if(bIsTrackingUnit)
					bUnitType = (byte) 0x01;
				if(bIsRefrigerationUnit)
					bUnitType += (byte) 0x02;
				#endregion

				if(oPluginData != null)
				{
					for (int X = 0; X < oPluginData.Length; X++)
					{
						if(oPluginData[X].PluginData != null)
						{
							iPluginCount++;
						}
					}
				}

				PacketUtilities.WriteToStream(oMS, bSOP);								// SOP
				PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Length 1
				PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Length 2
				PacketUtilities.WriteToStream(oMS, "L");									// Packet Type (1 byte)
				PacketUtilities.WriteToStream(oMS, "U");									// Sub Command (1 byte)
				PacketUtilities.WriteToStream(oMS, iPacketVersion);				// Packet Version (2 byte int)
				PacketUtilities.WriteToStream(oMS, iFleetID);							// Fleet ID (2 byte int)
				PacketUtilities.WriteToStream(oMS, iVehicleID);						// Vehicle ID (4 byte int)
				PacketUtilities.WriteToStream(oMS, bUnitType);						// Unit Type (1 byte)
				PacketUtilities.WriteToStream(oMS, iReasonID);						// Reason ID (4 byte int)
				PacketUtilities.WriteToStream(oMS, dtGPSTime.ToOADate());	// GPS Time (8 byte double)
				PacketUtilities.WriteToStream(oMS, dtDeviceTime.ToOADate());	// Device Time (8 byte double)
				PacketUtilities.WriteToStream(oMS, dtServerTime.ToOADate());	// Server Time (8 byte double)
				PacketUtilities.WriteToStream(oMS, Convert.ToInt32(dLat * 100000)); // Latitude (4 byte int)
				PacketUtilities.WriteToStream(oMS, Convert.ToInt32(dLong * 100000));	// Longitude (8 byte double)
				PacketUtilities.WriteToStream(oMS, bFlags);								// Flags (1 byte)
				PacketUtilities.WriteToStream(oMS, lSpeedAcc);						// Speed Accumalator(8 byte long)
				PacketUtilities.WriteToStream(oMS, iDirection);						// Direction (2 byte int)
				PacketUtilities.WriteToStream(oMS, bSpeed);							// Speed (1 byte int)
				PacketUtilities.WriteToStream(oMS, bMaxSpeed);					// Max Speed (1 byte int)
				PacketUtilities.WriteToStream(oMS, bInputStatus);					// Input Status (1 byte)
				PacketUtilities.WriteToStream(oMS, bOutputStatus);				// Output Status (1 byte)
				PacketUtilities.WriteToStream(oMS, iLightStatus);					// Light Status (2 byte short)
				PacketUtilities.WriteToStream(oMS, iButtonStatus);					// Button Status (2 byte short)

				if (iCoolantTemperature > 0 || iOilTemp > 0 || iOilPressure > 0 || iGear > 0 || iMaxRPM > 0 || iBrakeApplications > 0 || fGForceFront > 0 || fGForceBack > 0 || fGForceLeftRight > 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Engine Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, iCoolantTemperature);	// Coolant Temperature (2 byte int)
					PacketUtilities.WriteToStream(oMS, iOilTemp);						// Oil Temperature (2 byte int)
					PacketUtilities.WriteToStream(oMS, iOilPressure);					// Oil Pressure (2 byte int)
					PacketUtilities.WriteToStream(oMS, iGear);								// Gear Position (2 byte int)
					PacketUtilities.WriteToStream(oMS, iMaxRPM);						// Max RPM (2 byte int)
					PacketUtilities.WriteToStream(oMS, iBrakeApplications);		// Brake Applications (2 byte int)
					if(fGForceFront > 0 || fGForceBack > 0 || fGForceLeftRight > 0)
					{
						PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// G-Force Incuded = true (1 byte)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fGForceFront * (float) 100));		// Forward G-Force (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fGForceBack * (float) 100));		// Backwards G-Force (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fGForceLeftRight * (float) 100));// Lateral G-Force (2 byte int)
					}
					else
						PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// G-Force Incuded = false (1 byte)
				}
				else
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Engine Data Incuded = false (1 byte)
				}

				if(iTotalEngineHours > 0 || iTripFuel > 0 || dFuelEconomy > 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Fuel Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, iTotalEngineHours);		// Engine Hours (4 byte int)
					PacketUtilities.WriteToStream(oMS, iTripFuel);						// Trip Fuel (4 byte int)
					PacketUtilities.WriteToStream(oMS, Convert.ToInt32(dFuelEconomy * 100));	// Fuel Economy (4 byte int)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Fuel Data Incuded = false (1 byte)


				if(iExtendedValue1 != 0 || iExtendedValue2 != 0 || iExtendedValue3 != 0 || iExtendedValue4 != 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Extended Value Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, iExtendedValue1);			// Extended Value 1 (4 byte int)
					PacketUtilities.WriteToStream(oMS, iExtendedValue2);			// Extended Value 2 (4 byte int)
					PacketUtilities.WriteToStream(oMS, iExtendedValue3);			// Extended Value 3 (4 byte int)
					PacketUtilities.WriteToStream(oMS, iExtendedValue4);			// Extended Value 4 (4 byte int)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Extended Value Data Incuded = false (1 byte)


				if(iBatteryVolts != 0 || iBrakeUsageSeconds != 0 || iOdometer != 0 || iRPMZone1 != 0 || iRPMZone2 != 0 || iRPMZone3 != 0 || iRPMZone4 != 0 || iSpeedZone1 != 0 || iSpeedZone2 != 0 || iSpeedZone3 != 0 || iSpeedZone4 != 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Extended Engine Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, iBatteryVolts);					// Battery Volts (2 byte int)
					PacketUtilities.WriteToStream(oMS, iBrakeUsageSeconds);	// Brake Usage Seconds (2 byte int)
					PacketUtilities.WriteToStream(oMS, iOdometer);						// Odometer (4 byte int)
					PacketUtilities.WriteToStream(oMS, iTotalFuelUsed);				// Total Fuel Used(4 byte int)

					if(iRPMZone1 != 0 || iRPMZone2 != 0 || iRPMZone3 != 0 || iRPMZone4 != 0)
					{
						PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// RPM Zone Data Incuded = true (1 byte)
						PacketUtilities.WriteToStream(oMS, iRPMZone1);						// RPM Zone 1 (2 byte int)
						PacketUtilities.WriteToStream(oMS, iRPMZone2);					// RPM Zone 2 (2 byte int)
						PacketUtilities.WriteToStream(oMS, iRPMZone3);					// RPM Zone 3 (2 byte int)
						PacketUtilities.WriteToStream(oMS, iRPMZone4);					// RPM Zone 4 (2 byte int)
					}
					else
						PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// RPM Zone Data Incuded = false (1 byte)

					if(iSpeedZone1 != 0 || iSpeedZone2 != 0 || iSpeedZone3 != 0 || iSpeedZone4 != 0)
					{
						PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Speed Zone Data Incuded = true (1 byte)
						PacketUtilities.WriteToStream(oMS, iSpeedZone1);					// Speed Zone 1 (2 byte int)
						PacketUtilities.WriteToStream(oMS, iSpeedZone2);					// Speed Zone 2 (2 byte int)
						PacketUtilities.WriteToStream(oMS, iSpeedZone3);					// Speed Zone 3 (2 byte int)
						PacketUtilities.WriteToStream(oMS, iSpeedZone4);					// Speed Zone 4 (2 byte int)
					}
					else
						PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Speed Zone Data Incuded = false (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Extended Engine Data Incuded = false (1 byte)

				PacketUtilities.WriteToStream(oMS, lExtendedStatus);			// Extended Status (8 byte long)
				PacketUtilities.WriteToStream(oMS, lAuxilaryStatus);				// Auxilary Status (8 byte long)
				if(iSetPointGroupID != 0 || iSetPointID != 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Set Point Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, iSetPointGroupID);			// Setpoint Group ID (4 byte int)
					PacketUtilities.WriteToStream(oMS, iSetPointID);					// Setpoint ID (4 byte int)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Set Point Data Incuded = false (1 byte)

				if(iReferFlags != 0 || dReferFuelPercentage != 0 || dReferBatteryVolts != 0 || bReferInput1 || bReferInput2 || bReferInput3 || bReferInput4 || dReferHumidity != 0 || fReferSensor1 != 0 || fReferSensor2 != 0 || fReferSensor3 != 0 || fReferSensor4 != 0 || fReferSensor5 != 0 || fReferSensor6 != 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Refrigeration Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, iReferFlags);					// Refrigeration Flags (4 byte int)
					PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(Convert.ToInt16(((float) dReferFuelPercentage * (float) 2))))[0]);	// Refrigeration Fuel Percentage * 2 (1 byte int)
					PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(Convert.ToInt16(((float) dReferBatteryVolts * (float) 2))))[0]);		// Refrigeration Battery Volts * 2 (1 byte int)
					int iReferInputState = (bReferInput1?1:0) + (bReferInput2?2:0) + (bReferInput3?4:0) + (bReferInput3?8:0);
					PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferInputState))[0]);			// Refrigeration Input Status (1 byte)
					PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferSensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
					PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(Convert.ToInt16(((float) dReferHumidity * (float) 2))))[0]);	// Refrigeration Humidity (1 byte int)
					isValue = Convert.ToInt16(fReferSensor1 * (float) 10);
					PacketUtilities.WriteToStream(oMS, isValue);																	// Refrigeration Sensor 1 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor2 * (float) 10);
					PacketUtilities.WriteToStream(oMS, isValue);																	// Refrigeration Sensor 2 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor3 * (float) 10);
					PacketUtilities.WriteToStream(oMS, isValue);																	// Refrigeration Sensor 3 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor4 * (float) 10);
					PacketUtilities.WriteToStream(oMS, isValue);																	// Refrigeration Sensor 4 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor5 * (float) 10);
					PacketUtilities.WriteToStream(oMS, isValue);																	// Refrigeration Sensor 5 * 10 (2 byte int)
					isValue = Convert.ToInt16(fReferSensor6 * (float) 10);
					PacketUtilities.WriteToStream(oMS, isValue);																	// Refrigeration Sensor 6 * 10 (2 byte int)

					if (iReferZ1Zone > 0 && iReferZ2Zone > 0 && iReferZ3Zone > 0)				// Number of Zones (1 byte int)
					{
						PacketUtilities.WriteToStream(oMS, (byte) 0x03);
						bZone1 = true;
						bZone2 = true;
						bZone3 = true;
					}
					else if (iReferZ1Zone > 0 && iReferZ2Zone > 0 && iReferZ3Zone == 0)
					{
						PacketUtilities.WriteToStream(oMS, (byte) 0x02);
						bZone1 = true;
						bZone2 = true;
					}
					else if (iReferZ1Zone > 0 && iReferZ2Zone == 0 && iReferZ3Zone == 0)
					{
						PacketUtilities.WriteToStream(oMS, (byte) 0x01);
						bZone1 = true;
					}
					else
						PacketUtilities.WriteToStream(oMS, (byte) 0x00);

					if (bZone1)																											// Refrigeration Zone 1 Data
					{
						PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferZ1Zone))[0]);		// Refrigeration Zone ID (1 byte int)
						PacketUtilities.WriteToStream(oMS, (short) iReferZ1OperatingMode);										// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.WriteToStream(oMS, (short) iReferZ1Alarms);														// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferZ1SensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ1ReturnAir1 * (float) 10));		// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ1ReturnAir2 * (float) 10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ1SupplyAir1 * (float) 10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ1SupplyAir2 * (float) 10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ1Setpoint * (float) 10));			// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ1Evap * (float) 10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (bZone2)																											// Refrigeration Zone 2 Data
					{
						PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferZ2Zone))[0]);		// Refrigeration Zone ID (1 byte int)
						PacketUtilities.WriteToStream(oMS, (short) iReferZ2OperatingMode);										// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.WriteToStream(oMS, (short) iReferZ2Alarms);													// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferZ2SensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ2ReturnAir1 * (float) 10));		// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ2ReturnAir2 * (float) 10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ2SupplyAir1 * (float) 10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ2SupplyAir2 * (float) 10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ2Setpoint * (float) 10));			// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ2Evap * (float) 10));				// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (bZone3)																											// Refrigeration Zone 3 Data
					{
						PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferZ3Zone))[0]);		// Refrigeration Zone ID (1 byte int)
						PacketUtilities.WriteToStream(oMS, (short) iReferZ3OperatingMode);										// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.WriteToStream(oMS, (short) iReferZ3Alarms);													// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iReferZ3SensorsAvaliable))[0]);	// Refrigeration Sensors Avaliable (1 byte)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ3ReturnAir1 * (float) 10));		// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ3ReturnAir2 * (float) 10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ3SupplyAir1 * (float) 10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ3SupplyAir2 * (float) 10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ3Setpoint * (float) 10));			// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(fReferZ3Evap * (float) 10));				// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Refrigeration Data Incuded = false (1 byte)

				if(sRawGPS.Length > 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Raw GPS Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, sRawGPS);						// Raw GPS (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Raw GPS Data Incuded = false (1 byte)

				if(sUserDefined.Length > 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// User Defined Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, sUserDefined);				// User Defined (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// User Defined Data Incuded = false (1 byte)

				if(sPosition.Length > 0 || sPlaceName.Length > 0 || sMapRef.Length > 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);						// Location Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, (float) dDistance);			// Distance From Nearest Place (4 byte float)
					PacketUtilities.WriteToStream(oMS, sPosition);							// Direction From Place (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
					PacketUtilities.WriteToStream(oMS, sPlaceName);					// Placename (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
					PacketUtilities.WriteToStream(oMS, sMapRef);							// Map Reference (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Location Data Incuded = false (1 byte)

				if(sUserInfo.Length > 0 || sVehicleTag.Length > 0)
				{
					PacketUtilities.WriteToStream(oMS, (byte) 0x01);					// Vehicle Label Data Incuded = true (1 byte)
					PacketUtilities.WriteToStream(oMS, sUserInfo);						// User Info (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
					PacketUtilities.WriteToStream(oMS, sVehicleTag);					// Vehicle Tag (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				else
					PacketUtilities.WriteToStream(oMS, (byte) 0x00);					// Vehicle Label Incuded = false (1 byte)

				if (iTrailer1ID > 0)
					iTrailerCount++;
				if (iTrailer2ID > 0)
					iTrailerCount++;
				if (iTrailer3ID > 0)
					iTrailerCount++;
				if (iTrailer4ID > 0)
					iTrailerCount++;
				if (iTrailer5ID > 0)
					iTrailerCount++;
				// Number of Trailers (1 byte int)
				PacketUtilities.WriteToStream(oMS, ((byte[]) BitConverter.GetBytes(iTrailerCount))[0]);

				if (iTrailer1ID > 0)
				{
					PacketUtilities.WriteToStream(oMS, iTrailer1ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oMS, sTrailer1Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer2ID > 0)
				{
					PacketUtilities.WriteToStream(oMS, iTrailer2ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oMS, sTrailer2Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer3ID > 0)
				{
					PacketUtilities.WriteToStream(oMS, iTrailer3ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oMS, sTrailer3Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer4ID > 0)
				{
					PacketUtilities.WriteToStream(oMS, iTrailer4ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oMS, sTrailer4Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}
				if (iTrailer5ID > 0)
				{
					PacketUtilities.WriteToStream(oMS, iTrailer5ID);					// Trailer ID (4 byte int)
					PacketUtilities.WriteToStream(oMS, sTrailer5Name);				// Trailer Name (Delimited String)
					PacketUtilities.WriteToStream(oMS, bEOS);								// End of String Delimeter (1 byte)
				}

                if (iPacketVersion >= 5)
                {
                    if (fTotalMass > 0 || fWeightLimit > 0)
                    {
                        PacketUtilities.WriteToStream(oMS, (byte)0x01);					// Mass Incuded = true (1 byte)
                        PacketUtilities.WriteToStream(oMS, fTotalMass);                 // last total mass declaration
                        PacketUtilities.WriteToStream(oMS, fWeightLimit);               // vehicle's weight limit
                    }
                    else
                    {
                        PacketUtilities.WriteToStream(oMS, (byte)0x00);					// Mass Incuded = false (1 byte)
                    }
                }

                PacketUtilities.WriteToStream(oMS, (byte)iPluginCount);			// Plugin Count (1 byte int)
				if(oPluginData != null)
				{
					for (int X = 0; X < oPluginData.Length; X++)
					{
						if(oPluginData[X].PluginData != null)
						{
							PacketUtilities.WriteToStream(oMS,  (int)oPluginData[X].PluginID);	// Plugin ID (4 byte int)
							PacketUtilities.WriteToStream(oMS,  Convert.ToInt16(oPluginData[X].PluginData.Length));	// Plugin Data Length (2 byte short)
							PacketUtilities.WriteToStream(oMS,  oPluginData[X].PluginData);	// Plugin Data  (Byte[])
						}
					}
				}

				PacketUtilities.WriteToStream(oMS, bEOP);								// End of Packet (1 byte)

				bResult = oMS.ToArray();
				iPos = bResult.Length;

				// Write in the length bytes
				bResult[1] =  ((byte[]) BitConverter.GetBytes(iPos))[0];
				bResult[2] =  ((byte[]) BitConverter.GetBytes(iPos))[1];
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding live update : " + ex.Message;
				bResult = null;
			}
			return bResult;
		}

		public bool DecodeFromStream(byte[] bData, ref string sErrMsg)
		{
			bool bRet = false;
			byte bEOS = (byte) 0x04;
			byte bEOP = (byte) 0x07;
			byte bValue = (byte) 0x00;
			char cType = (char) 0x00;
			int iValue = 0;
			int iReferZoneCount = 0;
			int iTrailerCount = 0;
			short iLength = 0;
			short isValue = 0;
			double dValue = 0;
			float fValue = 0;
			int iPluginCount = 0;
			int iPluginID = 0;
			byte[] bPluginData = null;
			MemoryStream oStream = null;


			try
			{
				oStream = new MemoryStream(bData, 0, bData.Length, false, false);
				PacketUtilities.ReadFromStream(oStream, ref bValue);													
				if (bValue != (byte) 0x01)																												// SOP
				{
					sErrMsg = "Error encoding live update : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref iLength);													// Length
				if (bData.Length != iLength)
				{
					sErrMsg = "Error encoding live update : Live update packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
					sErrMsg = "Error encoding live update : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref cType);													// Packet Type (1 byte)
				if (cType != 'L')
				{
					sErrMsg = "Error encoding live update : Live update packet type is incorrect.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref cType);													// Sub Command (1 byte)
				if (cType != 'U')
				{
					sErrMsg = "Error encoding live update : Live update packet sub command is incorrect.";
					return bRet;
				}
				PacketUtilities.ReadFromStream(oStream, ref this.iPacketVersion);								// Packet Version (2 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.iFleetID);											// Fleet ID (2 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.iVehicleID);									// Vehicle ID (4 byte int)
				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Unit Type (1 byte int)
				if ((bValue & (byte) 0x01) == (byte) 0x01)
					bIsTrackingUnit = true;
				else
					bIsTrackingUnit = false;
				if ((bValue & (byte) 0x02) == (byte) 0x02)
					bIsRefrigerationUnit = true;
				else
					bIsRefrigerationUnit = false;

				PacketUtilities.ReadFromStream(oStream, ref this.iReasonID);										// Reason ID (4 byte int)				
				PacketUtilities.ReadFromStream(oStream, ref dValue);
				this.dtGPSTime = DateTime.FromOADate(dValue);																				// GPS Time (8 byte double)
				PacketUtilities.ReadFromStream(oStream, ref dValue);
				this.dtDeviceTime = DateTime.FromOADate(dValue);																			// Device Time (8 byte double)
				PacketUtilities.ReadFromStream(oStream, ref dValue);
				this.dtServerTime = DateTime.FromOADate(dValue);																			// Server Time (8 byte double)
				PacketUtilities.ReadFromStream(oStream, ref iValue);
				this.dLat = Convert.ToDecimal(iValue) / Convert.ToDecimal(100000);												 // Latitude (4 byte int)
				PacketUtilities.ReadFromStream(oStream, ref iValue);
				this.dLong = Convert.ToDecimal(iValue) / Convert.ToDecimal(100000);											 // Longitude (4 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.bFlags);											// Flags (1 byte)
				PacketUtilities.ReadFromStream(oStream, ref this.lSpeedAcc);										// Speed Accumalator(8 byte long)
				PacketUtilities.ReadFromStream(oStream, ref this.iDirection);										// Direction (2 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.bSpeed);											// Speed (1 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.bMaxSpeed);									// Max Speed (1 byte int)
				PacketUtilities.ReadFromStream(oStream, ref this.bInputStatus);									// Input Status (1 byte)
				PacketUtilities.ReadFromStream(oStream, ref this.bOutputStatus);								// Output Status (1 byte)
				PacketUtilities.ReadFromStream(oStream, ref this.iLightStatus);									// Light Status (2 byte short)
				PacketUtilities.ReadFromStream(oStream, ref this.iButtonStatus);								// Button Status (2 byte short)

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Engine Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iCoolantTemperature);				// Coolant Temperature (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iOilTemp);									// Oil Temperature (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iOilPressure);								// Oil Pressure (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iGear);											// Gear Position (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iMaxRPM);									// Max RPM (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iBrakeApplications);					// Brake Applications (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);												// G-Force Incuded (1 byte)
					if(bValue == (byte) 0x01)
					{
						PacketUtilities.ReadFromStream(oStream, ref isValue);											// Forward G-Force (2 byte int)
						this.fGForceFront = ((float) isValue / (float) 100);
						PacketUtilities.ReadFromStream(oStream, ref isValue);											// Backward G-Force (2 byte int)
						this.fGForceBack = ((float) isValue / (float) 100);
						PacketUtilities.ReadFromStream(oStream, ref isValue);											// Lateral G-Force (2 byte int)
						this.fGForceLeftRight = ((float) isValue / (float) 100);						
					}
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Fuel Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iTotalEngineHours);					// Engine Hours (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iTripFuel);									// Trip Fuel (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref iValue);
					dFuelEconomy = Convert.ToDouble(iValue) / (double) 100;																// Fuel Economy (4 byte int)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Extended Value Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iExtendedValue1);						// Extended Value 1 (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iExtendedValue2);						// Extended Value 2 (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iExtendedValue3);						// Extended Value 3 (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iExtendedValue4);						// Extended Value 4 (4 byte int)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Extended Engine Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iBatteryVolts);								// Battery Volts (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iBrakeUsageSeconds);				// Brake Usage Seconds (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iOdometer);									// Odometer (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iTotalFuelUsed);							// Total Fuel Used(4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);												// RPM Zone Data Incuded (1 byte)
					if(bValue == (byte) 0x01)
					{
						PacketUtilities.ReadFromStream(oStream, ref this.iRPMZone1);							// RPM Zone 1 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref this.iRPMZone2);							// RPM Zone 2 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref this.iRPMZone3);							// RPM Zone 3 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref this.iRPMZone4);							// RPM Zone 4 (2 byte int)
					}
					PacketUtilities.ReadFromStream(oStream, ref bValue);												// Speed Zone Data Incuded = true (1 byte)
					if(bValue == (byte) 0x01)
					{
						PacketUtilities.ReadFromStream(oStream, ref this.iSpeedZone1);							// Speed Zone 1 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref this.iSpeedZone2);						// Speed Zone 2 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref this.iSpeedZone3);						// Speed Zone 3 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref this.iSpeedZone4);						// Speed Zone 4 (2 byte int)
					}
				}

				PacketUtilities.ReadFromStream(oStream, ref this.lExtendedStatus);							// Extended Status (8 byte long)
				PacketUtilities.ReadFromStream(oStream, ref this.lAuxilaryStatus);							// Auxilary Status (8 byte long)
				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Set Point Data Incuded  (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iSetPointGroupID);					// Setpoint Group ID (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref this.iSetPointID);								// Setpoint ID (4 byte int)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Refrigeration Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					bHasRefrigerationValues = true;
					PacketUtilities.ReadFromStream(oStream, ref this.iReferFlags);								// Refrigeration Flags (4 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);
					this.dReferFuelPercentage = Convert.ToDecimal((int) bValue) / Convert.ToDecimal(2);				// Refrigeration Fuel Percentage * 2 (1 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);
					this.dReferBatteryVolts = Convert.ToDecimal((int) bValue) / Convert.ToDecimal(2);					// Refrigeration Battery Volts * 2 (1 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);												// Refrigeration Input Status (1 byte)
					iValue = (int) bValue;
					if ((iValue & 0x01) == 0x01)
						bReferInput1 = true;
					if ((iValue & 0x02) == 0x02)
						bReferInput2 = true;
					if ((iValue & 0x04) == 0x04)
						bReferInput3 = true;
					if ((iValue & 0x08) == 0x08)
						bReferInput4 = true;
					PacketUtilities.ReadFromStream(oStream, ref bValue);
					this.iReferSensorsAvaliable = (short) bValue;																					// Refrigeration Sensors Available (1 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);
					this.dReferHumidity = Convert.ToDecimal((int) bValue) / Convert.ToDecimal(2);							// Refrigeration Humidity (1 byte int)
					PacketUtilities.ReadFromStream(oStream, ref isValue);
					this.fReferSensor1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 1 * 10 (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref isValue);
					this.fReferSensor2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 2 * 10 (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref isValue);
					this.fReferSensor3 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 3 * 10 (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref isValue);
					this.fReferSensor4 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 4 * 10 (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref isValue);
					this.fReferSensor5 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 5 * 10 (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref isValue);
					this.fReferSensor6 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 6 * 10 (2 byte int)
					PacketUtilities.ReadFromStream(oStream, ref bValue);
					iReferZoneCount = (int) bValue;																											// Refrigeration Zone Count (1 byte int)
					if (iReferZoneCount >= 1)
					{
						PacketUtilities.ReadFromStream(oStream, ref bValue);
						this.iReferZ1Zone = (int) bValue;																									// Refrigeration Zone ID (1 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.iReferZ1OperatingMode = (int) isValue;																					// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.iReferZ1Alarms = (int) isValue;																								// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref bValue);
						this.iReferZ1SensorsAvaliable = (short) bValue;																			// Refrigeration Sensors Available (1 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ1ReturnAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ1ReturnAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ1SupplyAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ1SupplyAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ1Setpoint = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));				// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ1Evap = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (iReferZoneCount >= 2)
					{
						PacketUtilities.ReadFromStream(oStream, ref bValue);
						this.iReferZ2Zone = (int) bValue;																									// Refrigeration Zone ID (1 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.iReferZ2OperatingMode = (int) isValue;																				// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.iReferZ2Alarms = (int) isValue;																								// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref bValue);
						this.iReferZ2SensorsAvaliable = (short) bValue;																			// Refrigeration Sensors Available (1 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ2ReturnAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ2ReturnAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ2SupplyAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ2SupplyAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ2Setpoint = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));				// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ2Evap = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (iReferZoneCount >= 3)
					{
						PacketUtilities.ReadFromStream(oStream, ref bValue);
						this.iReferZ3Zone = (int) bValue;																									// Refrigeration Zone ID (1 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.iReferZ3OperatingMode = (int) isValue;																				// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.iReferZ3Alarms = (int) isValue;																								// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref bValue);
						this.iReferZ3SensorsAvaliable = (short) bValue;																			// Refrigeration Sensors Available (1 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ3ReturnAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ3ReturnAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ3SupplyAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ3SupplyAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ3Setpoint = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));				// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);
						this.fReferZ3Evap = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
				}
				else
					bHasRefrigerationValues = false;

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Raw GPS Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sRawGPS);						// Raw GPS Sting (Delimited String)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// User Defined Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sUserDefined);				// User Defined (Delimited String)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Location Data Incuded  (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, ref fValue);
					dDistance = Convert.ToDecimal(fValue);																							// Distance From Nearest Place (4 byte float)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sPosition);						// Direction From Place (Delimited String)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sPlaceName);					// Placename (Delimited String)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sMapRef);						// Map Reference (Delimited String)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);													// Vehicle Label Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sUserInfo);						// User Info (Delimited String)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sVehicleTag);					// Vehicle Tag (Delimited String)
				}

				PacketUtilities.ReadFromStream(oStream, ref bValue);
				iTrailerCount = (int) bValue;																												// Number of Trailers (1 byte int)
				if (iTrailerCount >= 1)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iTrailer1ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sTrailer1Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 2)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iTrailer2ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sTrailer2Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 3)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iTrailer3ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sTrailer3Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 4)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iTrailer4ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sTrailer4Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 5)
				{
					PacketUtilities.ReadFromStream(oStream, ref this.iTrailer5ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromStream(oStream, bEOS, ref this.sTrailer5Name);		// Trailer Name (Delimited String)
				}

                if (iPacketVersion >= 5)
                {
                    PacketUtilities.ReadFromStream(oStream, ref bValue);        // Mass Data Incuded (1 byte)
                    if (bValue == (byte)0x01)
                    {
                        PacketUtilities.ReadFromStream(oStream, ref this.fTotalMass);			// last total mass declaration
                        PacketUtilities.ReadFromStream(oStream, ref this.fWeightLimit);		// vehicle's weight limit
                    }
                }

				PacketUtilities.ReadFromStream(oStream, ref bValue);											// Plugin Count
				iPluginCount = (int) bValue;
				if(iPluginCount > 0)
				{
					oPluginData = new cPluginData[iPluginCount];
					for (int X = 0; X < oPluginData.Length; X++)
					{
						oPluginData[X] = new cPluginData();
						iPluginID = 0;
						bPluginData = null;

						PacketUtilities.ReadFromStream(oStream, ref iPluginID);								// Plugin ID (4 byte int)
						PacketUtilities.ReadFromStream(oStream, ref isValue);									// Plugin Data Length (2 byte short)
						PacketUtilities.ReadFromStream(oStream, (int) isValue, ref bPluginData);	// Plugin Data  (Byte[])

						oPluginData[X].PluginID = (PluginIds)iPluginID;
						oPluginData[X].PluginData = bPluginData;
					}
				}
				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding live update : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}
		public bool Decode(byte[] bData, ref string sErrMsg)
		{
			bool bRet = false;
			byte bEOS = (byte) 0x04;
			byte bEOP = (byte) 0x07;
			byte bValue = (byte) 0x00;
			char cType = (char) 0x00;
			int iPos = 0;
			int iValue = 0;
			int iReferZoneCount = 0;
			int iTrailerCount = 0;
			short iLength = 0;
			short isValue = 0;
			double dValue = 0;
			float fValue = 0;
			int iPluginCount = 0;
			int iPluginID = 0;
			byte[] bPluginData = null;

			try
			{
				if (bData[iPos++] != (byte) 0x01)																												// SOP
				{
					sErrMsg = "Error encoding live update : SOP not found.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iLength);													// Length
				if (bData.Length != iLength)
				{
					sErrMsg = "Error encoding live update : Live update packet length is incorrect.";
					return bRet;
				}
				if (bData[iLength -1] != bEOP)
				{
					sErrMsg = "Error encoding live update : EOP was not found.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref cType);													// Packet Type (1 byte)
				if (cType != 'L')
				{
					sErrMsg = "Error encoding live update : Live update packet type is incorrect.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref cType);													// Sub Command (1 byte)
				if (cType != 'U')
				{
					sErrMsg = "Error encoding live update : Live update packet sub command is incorrect.";
					return bRet;
				}
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iPacketVersion);								// Packet Version (2 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iFleetID);											// Fleet ID (2 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iVehicleID);									// Vehicle ID (4 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Unit Type (1 byte int)
				if ((bValue & (byte) 0x01) == (byte) 0x01)
					bIsTrackingUnit = true;
				else
					bIsTrackingUnit = false;
				if ((bValue & (byte) 0x02) == (byte) 0x02)
					bIsRefrigerationUnit = true;
				else
					bIsRefrigerationUnit = false;

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iReasonID);										// Reason ID (4 byte int)				
				if (iFleetID == 5 && iVehicleID == 1)
					Console.WriteLine("Test");
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref dValue);
				this.dtGPSTime = DateTime.FromOADate(dValue);																				// GPS Time (8 byte double)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref dValue);
				this.dtDeviceTime = DateTime.FromOADate(dValue);																			// Device Time (8 byte double)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref dValue);
				this.dtServerTime = DateTime.FromOADate(dValue);																			// Server Time (8 byte double)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iValue);
				this.dLat = Convert.ToDecimal(iValue) / Convert.ToDecimal(100000);												 // Latitude (4 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iValue);
				this.dLong = Convert.ToDecimal(iValue) / Convert.ToDecimal(100000);											 // Longitude (4 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.bFlags);											// Flags (1 byte)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.lSpeedAcc);										// Speed Accumalator(8 byte long)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iDirection);										// Direction (2 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.bSpeed);											// Speed (1 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.bMaxSpeed);									// Max Speed (1 byte int)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.bInputStatus);									// Input Status (1 byte)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.bOutputStatus);								// Output Status (1 byte)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iLightStatus);									// Light Status (2 byte short)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iButtonStatus);								// Button Status (2 byte short)

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Engine Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iCoolantTemperature);				// Coolant Temperature (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iOilTemp);									// Oil Temperature (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iOilPressure);								// Oil Pressure (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iGear);											// Gear Position (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iMaxRPM);									// Max RPM (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iBrakeApplications);					// Brake Applications (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);												// G-Force Incuded (1 byte)
					if(bValue == (byte) 0x01)
					{
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);											// Forward G-Force (2 byte int)
						this.fGForceFront = ((float) isValue / (float) 100);
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);											// Backward G-Force (2 byte int)
						this.fGForceBack = ((float) isValue / (float) 100);
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);											// Lateral G-Force (2 byte int)
						this.fGForceLeftRight = ((float) isValue / (float) 100);						
					}
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Fuel Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTotalEngineHours);					// Engine Hours (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTripFuel);									// Trip Fuel (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iValue);
					dFuelEconomy = Convert.ToDouble(iValue) / (double) 100;																// Fuel Economy (4 byte int)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Extended Value Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iExtendedValue1);						// Extended Value 1 (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iExtendedValue2);						// Extended Value 2 (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iExtendedValue3);						// Extended Value 3 (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iExtendedValue4);						// Extended Value 4 (4 byte int)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Extended Engine Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iBatteryVolts);								// Battery Volts (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iBrakeUsageSeconds);				// Brake Usage Seconds (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iOdometer);									// Odometer (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTotalFuelUsed);							// Total Fuel Used(4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);												// RPM Zone Data Incuded (1 byte)
					if(bValue == (byte) 0x01)
					{
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iRPMZone1);							// RPM Zone 1 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iRPMZone2);							// RPM Zone 2 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iRPMZone3);							// RPM Zone 3 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iRPMZone4);							// RPM Zone 4 (2 byte int)
					}
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);												// Speed Zone Data Incuded = true (1 byte)
					if(bValue == (byte) 0x01)
					{
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iSpeedZone1);							// Speed Zone 1 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iSpeedZone2);						// Speed Zone 2 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iSpeedZone3);						// Speed Zone 3 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iSpeedZone4);						// Speed Zone 4 (2 byte int)
					}
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.lExtendedStatus);							// Extended Status (8 byte long)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.lAuxilaryStatus);							// Auxilary Status (8 byte long)
				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Set Point Data Incuded  (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iSetPointGroupID);					// Setpoint Group ID (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iSetPointID);								// Setpoint ID (4 byte int)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Refrigeration Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					bHasRefrigerationValues = true;
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iReferFlags);								// Refrigeration Flags (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
					this.dReferFuelPercentage = Convert.ToDecimal((int) bValue) / Convert.ToDecimal(2);				// Refrigeration Fuel Percentage * 2 (1 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
					this.dReferBatteryVolts = Convert.ToDecimal((int) bValue) / Convert.ToDecimal(2);					// Refrigeration Battery Volts * 2 (1 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);												// Refrigeration Input Status (1 byte)
					iValue = (int) bValue;
					if ((iValue & 0x01) == 0x01)
						bReferInput1 = true;
					if ((iValue & 0x02) == 0x02)
						bReferInput2 = true;
					if ((iValue & 0x04) == 0x04)
						bReferInput3 = true;
					if ((iValue & 0x08) == 0x08)
						bReferInput4 = true;
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
					this.iReferSensorsAvaliable = (short) bValue;																					// Refrigeration Sensors Available (1 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
					this.dReferHumidity = Convert.ToDecimal((int) bValue) / Convert.ToDecimal(2);							// Refrigeration Humidity (1 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
					this.fReferSensor1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 1 * 10 (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
					this.fReferSensor2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 2 * 10 (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
					this.fReferSensor3 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 3 * 10 (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
					this.fReferSensor4 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 4 * 10 (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
					this.fReferSensor5 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 5 * 10 (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
					this.fReferSensor6 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));						// Refrigeration Sensor 6 * 10 (2 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
					iReferZoneCount = (int) bValue;																											// Refrigeration Zone Count (1 byte int)
					if (iReferZoneCount >= 1)
					{
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
						this.iReferZ1Zone = (int) bValue;																									// Refrigeration Zone ID (1 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.iReferZ1OperatingMode = (int) isValue;																					// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.iReferZ1Alarms = (int) isValue;																								// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
						this.iReferZ1SensorsAvaliable = (short) bValue;																			// Refrigeration Sensors Available (1 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ1ReturnAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ1ReturnAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ1SupplyAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ1SupplyAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ1Setpoint = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));				// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ1Evap = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (iReferZoneCount >= 2)
					{
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
						this.iReferZ2Zone = (int) bValue;																									// Refrigeration Zone ID (1 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.iReferZ2OperatingMode = (int) isValue;																				// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.iReferZ2Alarms = (int) isValue;																								// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
						this.iReferZ2SensorsAvaliable = (short) bValue;																			// Refrigeration Sensors Available (1 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ2ReturnAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ2ReturnAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ2SupplyAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ2SupplyAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ2Setpoint = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));				// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ2Evap = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
					if (iReferZoneCount >= 3)
					{
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
						this.iReferZ3Zone = (int) bValue;																									// Refrigeration Zone ID (1 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.iReferZ3OperatingMode = (int) isValue;																				// Refrigeration Zone Operating Mode (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.iReferZ3Alarms = (int) isValue;																								// Refrigeration Zone Alarms (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
						this.iReferZ3SensorsAvaliable = (short) bValue;																			// Refrigeration Sensors Available (1 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ3ReturnAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));			// Refrigeration Zone Return Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ3ReturnAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Return Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ3SupplyAir1 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 1 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ3SupplyAir2 = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));		// Refrigeration Zone Supply Air 2 * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ3Setpoint = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));				// Refrigeration Zone Setpoint * 10 (2 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);
						this.fReferZ3Evap = (float) (Convert.ToSingle(isValue) / Convert.ToSingle(10));					// Refrigeration Zone Evaporator Coil * 10 (2 byte int)
					}
				}
				else
					bHasRefrigerationValues = false;

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Raw GPS Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sRawGPS);						// Raw GPS Sting (Delimited String)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// User Defined Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sUserDefined);				// User Defined (Delimited String)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Location Data Incuded  (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref fValue);
					dDistance = Convert.ToDecimal(fValue);																							// Distance From Nearest Place (4 byte float)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sPosition);						// Direction From Place (Delimited String)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sPlaceName);					// Placename (Delimited String)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sMapRef);						// Map Reference (Delimited String)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);													// Vehicle Label Data Incuded (1 byte)
				if(bValue == (byte) 0x01)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sUserInfo);						// User Info (Delimited String)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sVehicleTag);					// Vehicle Tag (Delimited String)
				}

				PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);
				iTrailerCount = (int) bValue;																												// Number of Trailers (1 byte int)
				if (iTrailerCount >= 1)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTrailer1ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sTrailer1Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 2)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTrailer2ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sTrailer2Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 3)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTrailer3ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sTrailer3Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 4)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTrailer4ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sTrailer4Name);		// Trailer Name (Delimited String)
				}
				if (iTrailerCount >= 5)
				{
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.iTrailer5ID);						// Trailer ID (4 byte int)
					PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, bEOS, ref this.sTrailer5Name);		// Trailer Name (Delimited String)
				}

                if (iPacketVersion >= 5)
                {
                    PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);        // Mass Data Incuded (1 byte)
                    if (bValue == (byte)0x01)
                    {
                        PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.fTotalMass);			// last total mass declaration
                        PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref this.fWeightLimit);		// vehicle's weight limit
                    }
                }
                
                PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref bValue);											// Plugin Count
				iPluginCount = (int) bValue;
				if(iPluginCount > 0)
				{
					oPluginData = new cPluginData[iPluginCount];
					for (int X = 0; X < oPluginData.Length; X++)
					{
						oPluginData[X] = new cPluginData();
						iPluginID = 0;
						bPluginData = null;

						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref iPluginID);								// Plugin ID (4 byte int)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, ref isValue);									// Plugin Data Length (2 byte short)
						PacketUtilities.ReadFromPacketAtPos(bData, ref iPos, (int) isValue, ref bPluginData);	// Plugin Data  (Byte[])

						oPluginData[X].PluginID = (PluginIds)iPluginID;
						oPluginData[X].PluginData = bPluginData;
					}
				}
				bRet = true;
			}
			catch(System.Exception ex)
			{
				sErrMsg = "Error encoding live update : " + ex.Message;
				bRet = false;
			}
			return bRet;
		}

		public string ToDisplayString()
		{
			return	this.ToString();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nLive Update Data : Version "); builder.Append(iPacketVersion);
				builder.Append("\r\n    Fleet ID : "); builder.Append(iFleetID);
				builder.Append("\r\n    Vehicle ID : "); builder.Append(iVehicleID);
				builder.Append("\r\n    Reason ID : "); builder.Append(iReasonID);
				builder.Append("\r\n    GPS Time : "); builder.Append(dtGPSTime.ToString("dd/MM/yyyy HH:mm:ss"));
				builder.Append("\r\n    Device Time : "); builder.Append(dtDeviceTime.ToString("dd/MM/yyyy HH:mm:ss"));
				builder.Append("\r\n    Server Time : "); builder.Append(dtServerTime.ToString("dd/MM/yyyy HH:mm:ss"));
				builder.Append("\r\n    Latitude : "); builder.Append(dLat);
				builder.Append("\r\n    Longitude : "); builder.Append(dLong);
				builder.Append("\r\n    Lat/Long Flags : "); builder.Append((int) bFlags);
				builder.Append("\r\n    Speed Accumulator : "); builder.Append(lSpeedAcc);
				builder.Append("\r\n    Direction : "); builder.Append(iDirection);
				builder.Append("\r\n    Speed : "); builder.Append((int) bSpeed);
				builder.Append("\r\n    Max Speed : "); builder.Append((int) bMaxSpeed);
				builder.Append("\r\n    Input Status : "); builder.Append((int) bInputStatus);
				builder.Append("\r\n    Output Status : "); builder.Append((int) bOutputStatus);
				builder.Append("\r\n    Light Status : "); builder.Append(iLightStatus);
				builder.Append("\r\n    Button Status : "); builder.Append(iButtonStatus);
				builder.Append("\r\n    Engine Data :"); 
				builder.Append("\r\n        Coolant Temp : "); builder.Append(iCoolantTemperature);
				builder.Append("\r\n        Oil Temp : "); builder.Append(iOilTemp);
				builder.Append("\r\n        Oil Pressure : "); builder.Append(iOilPressure);
				builder.Append("\r\n        Gear : "); builder.Append(iGear);
				builder.Append("\r\n        Max RPM : "); builder.Append(iMaxRPM);
				builder.Append("\r\n        Brake Applications : "); builder.Append(iBrakeApplications);
				builder.Append("\r\n        G-Force Data :"); 
				builder.Append("\r\n            Forwards : "); builder.Append(fGForceFront);
				builder.Append("\r\n            Backwards : "); builder.Append(fGForceBack);
				builder.Append("\r\n            Lateral : "); builder.Append(fGForceLeftRight);
				builder.Append("\r\n    Fuel Data :"); 
				builder.Append("\r\n        Total Engine Hours : "); builder.Append(iTotalEngineHours);
				builder.Append("\r\n        Trip Fuel : "); builder.Append(iTripFuel);
				builder.Append("\r\n        Fuel Economy : "); builder.Append(dFuelEconomy);
				builder.Append("\r\n    Extended Value Data :"); 
				builder.Append("\r\n        Extended Value 1 : "); builder.Append(iExtendedValue1);
				builder.Append("\r\n        Extended Value 2 : "); builder.Append(iExtendedValue2);
				builder.Append("\r\n        Extended Value 3 : "); builder.Append(iExtendedValue3);
				builder.Append("\r\n        Extended Value 4 : "); builder.Append(iExtendedValue4);
				builder.Append("\r\n    Extended Engine Data :"); 
				builder.Append("\r\n        Battery Volts : "); builder.Append(iBatteryVolts);
				builder.Append("\r\n        Brake Usage Seconds : "); builder.Append(iBrakeUsageSeconds);
				builder.Append("\r\n        Odometer : "); builder.Append(iOdometer);
				builder.Append("\r\n        Total Fuel Used : "); builder.Append(iTotalFuelUsed);
				builder.Append("\r\n        RPM Zone Data :"); 
				builder.Append("\r\n            RPM Zone 1 : "); builder.Append(iRPMZone1);
				builder.Append("\r\n            RPM Zone 2 : "); builder.Append(iRPMZone2);
				builder.Append("\r\n            RPM Zone 3 : "); builder.Append(iRPMZone3);
				builder.Append("\r\n            RPM Zone 4 : "); builder.Append(iRPMZone4);
				builder.Append("\r\n        Speed Zone Data :"); 
				builder.Append("\r\n            Speed Zone 1 : "); builder.Append(iSpeedZone1);
				builder.Append("\r\n            Speed Zone 2 : "); builder.Append(iSpeedZone2);
				builder.Append("\r\n            Speed Zone 3 : "); builder.Append(iSpeedZone3);
				builder.Append("\r\n            Speed Zone 4 : "); builder.Append(iSpeedZone4);
				builder.Append("\r\n    Extended Status : "); builder.Append(lExtendedStatus);
				builder.Append("\r\n    Auxilary Status : "); builder.Append(lAuxilaryStatus);
				builder.Append("\r\n    Set Point Data :"); 
				builder.Append("\r\n        Set Point Group ID : "); builder.Append(iSetPointGroupID);
				builder.Append("\r\n        Set Point ID : "); builder.Append(iSetPointID);
				builder.Append("\r\n    Refrigeration Data :"); 
				builder.Append("\r\n        Refrigeration Flags : "); builder.Append(iReferFlags);
				builder.Append("\r\n        Fuel Percentage : "); builder.Append(dReferFuelPercentage);
				builder.Append("\r\n        Battery Volts : "); builder.Append(dReferBatteryVolts);
				builder.Append("\r\n        Refrigeration Inputs :"); 
				builder.Append("\r\n            Input 1 On : "); builder.Append((bReferInput1?"true":"false"));
				builder.Append("\r\n            Input 2 On : "); builder.Append((bReferInput2?"true":"false"));
				builder.Append("\r\n            Input 3 On : "); builder.Append((bReferInput3?"true":"false"));
				builder.Append("\r\n            Input 4 On : "); builder.Append((bReferInput4?"true":"false"));
				builder.Append("\r\n        Humidity : "); builder.Append(dReferHumidity);
				builder.Append("\r\n        Sensor 1 : "); builder.Append(fReferSensor1);
				builder.Append("\r\n        Sensor 2 : "); builder.Append(fReferSensor2);
				builder.Append("\r\n        Sensor 3 : "); builder.Append(fReferSensor3);
				builder.Append("\r\n        Sensor 4 : "); builder.Append(fReferSensor4);
				builder.Append("\r\n        Sensor 5 : "); builder.Append(fReferSensor5);
				builder.Append("\r\n        Sensor 6 : "); builder.Append(fReferSensor6);
				builder.Append("\r\n        Refrigeration Zone 1 :"); 
				builder.Append("\r\n            Zone ID : "); builder.Append(iReferZ1Zone);
				builder.Append("\r\n            Operating Mode : "); builder.Append(iReferZ1OperatingMode);
				builder.Append("\r\n            Alarms : "); builder.Append(iReferZ1Alarms);
				builder.Append("\r\n            Return Air 1 : "); builder.Append(fReferZ1ReturnAir1);
				builder.Append("\r\n            Return Air 2 : "); builder.Append(fReferZ1ReturnAir2);
				builder.Append("\r\n            Supply Air 1 : "); builder.Append(fReferZ1SupplyAir1);
				builder.Append("\r\n            Supply Air 2 : "); builder.Append(fReferZ1SupplyAir2);
				builder.Append("\r\n            Setpoint : "); builder.Append(fReferZ1Setpoint);
				builder.Append("\r\n            Evaporator : "); builder.Append(fReferZ1Evap);
				builder.Append("\r\n        Refrigeration Zone 2 :"); 
				builder.Append("\r\n            Zone ID : "); builder.Append(iReferZ2Zone);
				builder.Append("\r\n            Operating Mode : "); builder.Append(iReferZ2OperatingMode);
				builder.Append("\r\n            Alarms : "); builder.Append(iReferZ2Alarms);
				builder.Append("\r\n            Return Air 1 : "); builder.Append(fReferZ2ReturnAir1);
				builder.Append("\r\n            Return Air 2 : "); builder.Append(fReferZ2ReturnAir2);
				builder.Append("\r\n            Supply Air 1 : "); builder.Append(fReferZ2SupplyAir1);
				builder.Append("\r\n            Supply Air 2 : "); builder.Append(fReferZ2SupplyAir2);
				builder.Append("\r\n            Setpoint : "); builder.Append(fReferZ2Setpoint);
				builder.Append("\r\n            Evaporator : "); builder.Append(fReferZ2Evap);
				builder.Append("\r\n        Refrigeration Zone 3 :"); 
				builder.Append("\r\n            Zone ID : "); builder.Append(iReferZ3Zone);
				builder.Append("\r\n            Operating Mode : "); builder.Append(iReferZ3OperatingMode);
				builder.Append("\r\n            Alarms : "); builder.Append(iReferZ3Alarms);
				builder.Append("\r\n            Return Air 1 : "); builder.Append(fReferZ3ReturnAir1);
				builder.Append("\r\n            Return Air 2 : "); builder.Append(fReferZ3ReturnAir2);
				builder.Append("\r\n            Supply Air 1 : "); builder.Append(fReferZ3SupplyAir1);
				builder.Append("\r\n            Supply Air 2 : "); builder.Append(fReferZ3SupplyAir2);
				builder.Append("\r\n            Setpoint : "); builder.Append(fReferZ3Setpoint);
				builder.Append("\r\n            Evaporator : "); builder.Append(fReferZ3Evap);
				builder.Append("\r\n    Raw GPS : "); builder.Append(sRawGPS);
				builder.Append("\r\n    User Defined : "); builder.Append(sUserDefined);
				builder.Append("\r\n    Location Data :"); 
				builder.Append("\r\n        Distance From : "); builder.Append(dDistance);
				builder.Append("\r\n        Direction From  : "); builder.Append(sPosition);
				builder.Append("\r\n        Place Name : "); builder.Append(sPlaceName);
				builder.Append("\r\n        Map Ref : "); builder.Append(sMapRef);
				builder.Append("\r\n    Vehicle Label Data :"); 
				builder.Append("\r\n        User Info : "); builder.Append(sUserInfo);
				builder.Append("\r\n        Vehicle Tag : "); builder.Append(sVehicleTag);
				builder.Append("\r\n    Trailer Data :"); 
				builder.Append("\r\n        Trailer 1 :"); 
				builder.Append("\r\n            ID : "); builder.Append(iTrailer1ID);
				builder.Append("\r\n            Name  : "); builder.Append(sTrailer1Name);
				builder.Append("\r\n        Trailer 2 :"); 
				builder.Append("\r\n            ID : "); builder.Append(iTrailer2ID);
				builder.Append("\r\n            Name : "); builder.Append(sTrailer2Name);
				builder.Append("\r\n        Trailer 3 :"); 
				builder.Append("\r\n            ID : "); builder.Append(iTrailer3ID);
				builder.Append("\r\n            Name : "); builder.Append(sTrailer3Name);
				builder.Append("\r\n        Trailer 4 :"); 
				builder.Append("\r\n            ID : "); builder.Append(iTrailer4ID);
				builder.Append("\r\n            Name : "); builder.Append(sTrailer4Name);
				builder.Append("\r\n        Trailer 5 :"); 
				builder.Append("\r\n            ID : "); builder.Append(iTrailer5ID);
				builder.Append("\r\n            Name : "); builder.Append(sTrailer5Name);
                if (iPacketVersion >= 5)
                {
                    builder.Append("\r\n    Mass Data :");
                    builder.Append("\r\n        Total Mass : "); builder.Append(fTotalMass);
                    builder.Append("\r\n        Weight Limit : "); builder.Append(fWeightLimit);
                }
				if(oPluginData != null)
				{
					if(oPluginData.Length > 0)
					{
						builder.Append("\r\n    Plugin Data :"); 
						for (int X = 0; X < oPluginData.Length; X++)
						{
							builder.Append("\r\n        ID : "); builder.Append(oPluginData[X].PluginID);
							builder.Append("\r\n        Data : "); builder.Append(BitConverter.ToString(oPluginData[X].PluginData, 0));
						}
					}
				}
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
			}
			return builder.ToString();
		}

	}
}

using System;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Interfaces implemented by some packet types
	/// </summary>
	/// 

	// All packets which hold a position MUST implement this interface!
    public interface IPositionHolder
	{
		GPPositionFix GetPosition();
		GPClock		  GetFixClock();
	}
	
	// All packets which hold device status MUST implement this interface!
	public interface IStatusHolder
	{
		GPStatus GetStatus();
	}
}

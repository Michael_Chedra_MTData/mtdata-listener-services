using System;
using System.IO;
using System.Text;
using System.Collections;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// This packet will formulate a Route Update for the new routing system
	///
	///	typedef struct routePoints 
	///	{
	///		char			cPointNumber;
	///		char			zBinLat    [ 3 ];
	///		char			zBinLon    [ 3 ];
	///		unsigned char	cLatTol;				
	///		unsigned char	cLonTol;
	///		unsigned int	uArrive5mMinsStart;
	///		unsigned char	cArrive5mMinsEnd;
	///		unsigned char	cDepart5mMinsStart;
	///		unsigned char	cDepart5mMinsEnd;
	///		unsigned char	cMin5mStopTime;
	///		unsigned char	cMax5mStopTime;
	///		byte        	uFlags;
    ///		byte        	DependentOnPoint;
	///	} RoutePoints;
	///	
	/// typedef struct	routeData 
	///	{
	///		char				cStartofFile;
	///		unsigned int		iLength;
	///		unsigned long		ulStartMinsThisMillenium;
	///		unsigned int		uMaxDuration5Mins;
	///		unsigned long		ulRouteNumber;				// The Vehicle Schedule ID
	///		char				cSpare1		    [ 4 ];
	///		char				cProtocolByte;              // The current version = 1
	///		char				cNoRoutePoints;
	///		RoutePoints			tRoutePts		[ MaxRoutePoints ];	
	///		char				cCheckSum;
	///		char				cEndOfTransmission;
	///	} RouteData;
	///	
	/// </summary>
    [Serializable]
    public class ConfigRouteUpdateGPPacket : GatewayProtocolPacket
	{
		public const Byte CONFIG_ROUTE_UPDATE = 0xd9;
		public const Byte CONFIG_ROUTE_DELETE = 0xda;

		public const int CONFIG_ROUTE_MAXCHECKPOINTS = 10;
		#region Route Update Actions

		public enum RouteUpdateAction : byte
		{
			Add = (byte)0x01,
			Update = (byte)0x02,
			Delete = (byte)0x03
		}

		#endregion
		#region CheckPoint Structure

		/// <summary>
		/// This is an iindividual CheckPoint entry
		/// </summary>
        [Serializable]
        public class CheckPointEntry
		{
			public int				PointNumber;
			public double			Latitude;
			public double			Longitude;
			public int				LatitudeToleranceMetres;
			public int				LongitudeToleranceMetres;
			public int				ArriveMinsStart;		//	Offset from start of route
			public int				ArriveMinsEnd;			//	Offset from start of route
			public int				DepartMinsStart;		//	Offset from start of route
			public int				DepartMinsEnd;			//	Offset from start of route
			public int				MinMinsStopTime;		//	Minimum Stop time in 5 minutes 
			public int				MaxMinsStopTime;		//	Maximum Stop time in 5 minutes
			public byte				Flags;
            public byte             DependentOnPoint;       // This value indicates that this check point is dependent on another check
                                                            // point being reached first.  0 = No dependency.

			public CheckPointEntry(int pointNumber, 
				double latitude, double longitude, 
				int latitudeToleranceMetres, int longitudeToleranceMetres,
				int	arriveMinsStart, int arriveMinsEnd, 
				int	departMinsStart, int departMinsEnd,
				int	minMinsStopTime, int maxMinsStopTime,
				int	flags, int dependentOnPoint)
			{
				PointNumber = pointNumber;
				Latitude = latitude;
				Longitude = longitude;
				LatitudeToleranceMetres = latitudeToleranceMetres;
				LongitudeToleranceMetres = longitudeToleranceMetres;
				ArriveMinsStart = arriveMinsStart;
				ArriveMinsEnd = arriveMinsEnd;
				DepartMinsStart = departMinsStart;
				DepartMinsEnd = departMinsEnd;
				MinMinsStopTime = minMinsStopTime;
				MaxMinsStopTime = maxMinsStopTime;
				Flags = (byte) (flags & 0xFF);
                DependentOnPoint = (byte)(dependentOnPoint & 0xFF);
			}

			public CheckPointEntry()
			{
			}
		}

		#endregion

		#region Instance Data
		private ArrayList _entries = new ArrayList();
		private	DateTime	_startTimeUTC	= DateTime.MinValue;
		private uint		_vehicleRouteScheduleID = 0;
		private int			_maxDurationMins = 0;
		private RouteUpdateAction _action = RouteUpdateAction.Add;
		#endregion

		#region Public Accessors

		public void ClearCheckPoints()
		{
			_entries.Clear();
		}

		/// <summary>
		/// Add a check point entry.
		/// </summary>
		/// <param name="pointNumber"></param>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="latitudeTolerance"></param>
		/// <param name="longitudeTolerance"></param>
		/// <param name="arriveMinsStart"></param>
		/// <param name="arriveMinsEnd"></param>
		/// <param name="departMinsStart"></param>
		/// <param name="departMinsEnd"></param>
		/// <param name="minMinsStopTime"></param>
		/// <param name="maxMinsStopTime"></param>
		/// <param name="flags"></param>
		public void AddCheckPoint(int pointNumber, 
			double latitude, double longitude, 
			int latitudeToleranceMetres, int longitudeToleranceMetres,
			int	arriveMinsStart, int arriveMinsEnd, 
			int	departMinsStart, int departMinsEnd,
			int	minMinsStopTime, int maxMinsStopTime,
            int flags, int dependentOnPoint)
		{
			AddCheckPoint(new CheckPointEntry(pointNumber, latitude, longitude, latitudeToleranceMetres, longitudeToleranceMetres,
                arriveMinsStart, arriveMinsEnd, departMinsStart, departMinsEnd, minMinsStopTime, maxMinsStopTime, flags, dependentOnPoint));
		}

		/// <summary>
		/// Add a checkpoint entry
		/// </summary>
		/// <param name="entry"></param>
		public void AddCheckPoint(CheckPointEntry entry)
		{
			if (_maxDurationMins < entry.ArriveMinsEnd)
				_maxDurationMins = entry.ArriveMinsEnd;
			_entries.Add(entry);
		}

		public	DateTime	StartTimeUTC { get{ return _startTimeUTC; } set{ _startTimeUTC = value; }}
		public	uint		VehicleRouteScheduleID { get{ return _vehicleRouteScheduleID; } set{ _vehicleRouteScheduleID = value; }}
		public	int			MaxDurationMins { get{ return _maxDurationMins; } set{ _maxDurationMins = value; }}

		/// <summary>
		/// Action to be performed.
		/// </summary>
		public RouteUpdateAction Action 
		{ 
			get{ return _action; } 
			set
			{ 
				_action = value; 
				switch(_action)
				{
					case RouteUpdateAction.Delete:
						cMsgType = CONFIG_ROUTE_DELETE;
						break;
					default :
						cMsgType = CONFIG_ROUTE_UPDATE;
						break;

				}
			}
		}

		#endregion
		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\nVehicleRouteScheduleID : "); builder.Append(_vehicleRouteScheduleID);
			builder.Append("\r\nStartTimeUTC : "); builder.Append(_startTimeUTC);
			builder.Append("\r\nAction : "); builder.Append(Action);
			builder.Append("\r\nPoint Count : "); builder.Append(_entries.Count);
			if (_entries.Count > 0)
				for(int loop = 0; loop < _entries.Count; loop++)
				{
					CheckPointEntry entry = (CheckPointEntry)_entries[loop];
					builder.Append("\r\n  Point "); 
					builder.Append(loop); 
					builder.Append("\r\n\tPointNumber : "); builder.Append(entry.PointNumber);
					builder.Append("\r\n\tLatitude : "); builder.Append(entry.Latitude);
					builder.Append("\r\n\tLongitude : "); builder.Append(entry.Longitude);
					builder.Append("\r\n\tLatitudeToleranceMetres : "); builder.Append(entry.LatitudeToleranceMetres);
					builder.Append("\r\n\tLongitudeToleranceMetres : "); builder.Append(entry.LongitudeToleranceMetres);
					builder.Append("\r\n\tMinMinsStopTime : "); builder.Append(entry.MinMinsStopTime);
					builder.Append("\r\n\tMaxMinsStopTime : "); builder.Append(entry.MaxMinsStopTime);
					builder.Append("\r\n\tFlags : "); builder.Append(entry.Flags);
                    builder.Append("\r\n\tDependent On : "); builder.Append(entry.DependentOnPoint);
				}
			return builder.ToString();
		}

		public ConfigRouteUpdateGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_ROUTE_UPDATE;
			bIsFileType = false;
			Clear();
		}

		public ConfigRouteUpdateGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_ROUTE_UPDATE;
			bIsFileType = false;
			Clear();
		}

		///	typedef struct routePoints 
		///	{
		///		char			cPointNumber;
		///		char			zBinLat    [ 3 ];
		///		char			zBinLon    [ 3 ];
		///		unsigned char	cLatTol;				
		///		unsigned char	cLonTol;
		///		unsigned int	uArrive5mMinsStart;
		///		unsigned char	cArrive5mMinsEnd;
		///		unsigned char	cDepart5mMinsStart;
		///		unsigned char	cDepart5mMinsEnd;
		///		unsigned char	cMin5mStopTime;
		///		unsigned char	cMax5mStopTime;
		///		byte        	uFlags;
        ///		byte        	DependentOnPoint;
		///	} RoutePoints;
		private void EncodeCheckPoint(ByteStreamWriter writer, CheckPointEntry checkPoint)
		{
			writer.WriteByte((byte)checkPoint.PointNumber);

			// N.B. All multi-byte variables are big-endian
			// Positions and tolerances
			uint tempLatLong = (uint) (Math.Abs(checkPoint.Latitude) * 60000);
			writer.WriteByte((byte)(tempLatLong & 0xFF));
			writer.WriteByte((byte)((tempLatLong & 0xFF00) >> 8));
			writer.WriteByte((byte)((tempLatLong & 0xFF0000) >> 16));
			
			tempLatLong = (uint)(Math.Abs(checkPoint.Longitude) * 60000);
			writer.WriteByte((byte)(tempLatLong & 0xFF));
			writer.WriteByte((byte)((tempLatLong & 0xFF00) >> 8));
			writer.WriteByte((byte)((tempLatLong & 0xFF0000) >> 16));

			//	No need to multiply by 60000.. the value is already in metres..
			//	Send the value through in multiples of 50Nm or 500Nm (0 or 1 MSB)
			double latitudeTolerance = (((double)checkPoint.LatitudeToleranceMetres) / 1.852);
			double longitudeTolerance = ((((double)checkPoint.LongitudeToleranceMetres) / 1.852) / Math.Cos((checkPoint.Latitude * Math.PI) / (double)180));
			byte tolerance = 0;

			if ((latitudeTolerance / 50) > 0x7F)
			{
				if ((latitudeTolerance / 500) > 0x7F)
					latitudeTolerance = 500 * 0x7F;

				tolerance = (byte)(Convert.ToByte(Convert.ToInt32(latitudeTolerance / 500) & 0xFF) | (byte)0x80);
			}
			else
			{
				tolerance = Convert.ToByte(Convert.ToInt32(latitudeTolerance / 50) & 0x7F);
			}
			writer.WriteByte((byte)tolerance);

			if ((longitudeTolerance / 50) > 0x7F)
			{
				if ((longitudeTolerance / 500) > 0x7F)
					longitudeTolerance = 500 * 0x7F;

				tolerance = (byte)(Convert.ToByte(Convert.ToInt32(longitudeTolerance / 500) & 0xFF) | (byte)0x80);
			}
			else
			{
				tolerance = Convert.ToByte(Convert.ToInt32(longitudeTolerance / 50) & 0x7F);
			}
			writer.WriteByte((byte)tolerance);
			
			//	Start time is relative to start of route. IT is in Minutes, but we only send 5 minute increments to unit
			int minutes5Value = (checkPoint.ArriveMinsStart / 5);

			if (minutes5Value < 0)
				minutes5Value = 0;
			writer.WriteByte((byte)(minutes5Value & 0xFF));
			writer.WriteByte((byte)((minutes5Value >> 8) & 0xFF));

			//	All other times are relative to ArriveTimeStart.
			minutes5Value = ((checkPoint.ArriveMinsEnd - checkPoint.ArriveMinsStart) / 5);
			if (minutes5Value < 0)
				minutes5Value = 0;
			writer.WriteByte((byte)(minutes5Value & 0xFF));

			minutes5Value = ((checkPoint.DepartMinsStart - checkPoint.ArriveMinsStart) / 5);
			if (minutes5Value < 0)
				minutes5Value = 0;
			writer.WriteByte((byte)(minutes5Value & 0xFF));

			minutes5Value = ((checkPoint.DepartMinsEnd - checkPoint.ArriveMinsStart) / 5);
			if (minutes5Value < 0)
				minutes5Value = 0;
			writer.WriteByte((byte)(minutes5Value & 0xFF));

			minutes5Value = (checkPoint.MinMinsStopTime / 5);
			if (minutes5Value < 0)
				minutes5Value = 0;
			writer.WriteByte((byte)(minutes5Value & 0xFF));

			minutes5Value = (checkPoint.MaxMinsStopTime / 5);
			if (minutes5Value < 0)
				minutes5Value = 0;
			writer.WriteByte((byte)(minutes5Value & 0xFF));
            writer.WriteByte(checkPoint.Flags);
            // This value indicates that this check point is dependent on another check
            // point being reached first.  0 = No dependency.
            writer.WriteByte(checkPoint.DependentOnPoint);
			
		}

		private int EncodeCheckPointLength()
		{
			return 18;
		}

		/// <summary>
		/// This is performed in the same way as the mobile code to ensure they 
		/// both work identically
		/// </summary>
		/// <returns></returns>
		private uint CalculateMinutesThisYear()
		{
			uint result = 0;
			uint febCount = 28;
			
			result = (uint)this.StartTimeUTC.Minute;
			result += ((uint)this.StartTimeUTC.Hour * 60);
			result += ((uint)(this.StartTimeUTC.Day - 1) * 24 * 60);

			if ((this.StartTimeUTC.Year % 4) == 0)
				febCount = 29;
	
			switch ( this.StartTimeUTC.Month ) 
			{
				case 2 : result += (31 * 24 * 60); break;
				case 3 : result += (febCount + 31) * 24 * 60; break;  // days from jan
				case 4 : result += (febCount + 62) * 24 * 60; break;  // days from feb
				case 5 : result += (febCount + 92) * 24 * 60; break;  // days from april
				case 6 : result += (febCount + 123) * 24 * 60; break;// days from may
				case 7 : result += (febCount + 153) * 24 * 60; break;// days from jun
				case 8 : result += (febCount + 184) * 24 * 60; break;// days from jul
				case 9 : result += (febCount + 215) * 24 * 60; break;// days from aug
				case 10: result += (febCount + 245) * 24 * 60; break; // days from sep
				case 11: result += (febCount + 275) * 24 * 60; break; // days from oct
				case 12: result += (febCount + 305) * 24 * 60; break; // days from nov
			}

			return result;
		}

		/// <summary>
		/// This calcvulates the number of minutes between the two years, including the start year, 
		/// but excluding the end year
		/// </summary>
		/// <param name="startYear"></param>
		/// <param name="endYear"></param>
		/// <returns></returns>
		private uint CalculateYearMinutesForDifference(int startYear, int endYear)
		{
			uint result = 0;
			for( int year = startYear; year < endYear; year++ ) 
			{
				if (( year % 4 ) == 0) 
					result += (366 * 24 * 60);
				else 
					result += (365 * 24 * 60);
			}
			return result;
		}


		/// <summary>
		/// This is performed like this to ensure the mobile code and the listener calculate it exactly the same way.
		/// </summary>
		/// <returns></returns>
		private uint CalculateMinutesThisMillenium()
		{
			uint result = CalculateMinutesThisYear();
			result += CalculateYearMinutesForDifference(2000, this._startTimeUTC.Year);
			return result;
		}

		/// typedef struct	routeData 
		///	{
		///		char				cStartofFile;
		///		unsigned int		iLength;
		///		unsigned long		ulStartMinsThisMillenium;
		///		unsigned int		uMaxDuration5Mins;
		///		unsigned long		ulRouteNumber;				// The Vehicle Schedule ID
		///		char				cSpare1		    [ 4 ];
		///		char				cProtocolByte;
		///		char				cNoRoutePoints;
		///		RoutePoints			tRoutePts		[ MaxRoutePoints ];	
		///		char				cCheckSum;
		///		char				cEndOfTransmission;
		///	} RouteData;
		public override void Encode(ref Byte[] theData)
		{
			ByteStreamWriter writer = new ByteStreamWriter(1024);

			writer.WriteByte((byte)GSP_SOH);

			int length = 22 + ((_action != RouteUpdateAction.Delete)?(CONFIG_ROUTE_MAXCHECKPOINTS * EncodeCheckPointLength()):0);

			// Size of this data
			writer.WriteByte((byte)(length & 0xFF));
			writer.WriteByte((byte)((length & 0xFF00) >> 8));

			//	Date and Time
			uint ulStartMinsThisMillenium = CalculateMinutesThisMillenium();

			writer.WriteByte((byte)(ulStartMinsThisMillenium & 0xFF));
			writer.WriteByte((byte)((ulStartMinsThisMillenium >> 8) & 0xFF));
			writer.WriteByte((byte)((ulStartMinsThisMillenium >> 16) & 0xFF));
			writer.WriteByte((byte)((ulStartMinsThisMillenium >> 24) & 0xFF));

			writer.WriteByte((byte)((_maxDurationMins / 5) & 0xFF));
			writer.WriteByte((byte)(((_maxDurationMins / 5) >> 8) & 0xFF));

			//	Vehicle Route ScheduleID
			writer.WriteByte((byte)(_vehicleRouteScheduleID & 0xFF));
			writer.WriteByte((byte)((_vehicleRouteScheduleID >> 8) & 0xFF));
			writer.WriteByte((byte)((_vehicleRouteScheduleID >> 16) & 0xFF));
			writer.WriteByte((byte)((_vehicleRouteScheduleID >> 24) & 0xFF));

			//	Spares
			writer.WriteByte((byte)0);
			writer.WriteByte((byte)0);
			writer.WriteByte((byte)0);
			writer.WriteByte((byte)0);
			writer.WriteByte((byte)0);

			//	Protocol Byte
			writer.WriteByte((byte)1);

			int checkPointsWritten = 0;
			if (_action == RouteUpdateAction.Delete)
				writer.WriteByte((byte)0);
			else
			{
				checkPointsWritten = (_entries.Count > CONFIG_ROUTE_MAXCHECKPOINTS)?CONFIG_ROUTE_MAXCHECKPOINTS:_entries.Count;
				writer.WriteByte((byte)checkPointsWritten);

				CheckPointEntry blank = new CheckPointEntry(0,0,0,0,0,0,0,0,0,0,0,0,0);
				for(int loop = 0; loop < CONFIG_ROUTE_MAXCHECKPOINTS; loop++)
				{
					CheckPointEntry entry = null;
					if (loop < _entries.Count)
						entry = (CheckPointEntry)_entries[loop];
					else
						entry = blank;
					EncodeCheckPoint(writer, entry);
				}
			}
			
			// Write in the checksum placeholder and the end-of-message
			writer.WriteByte((byte)0);

			// Finish the packet
			writer.WriteByte((byte)GSP_EOT); 

			// And wrap it in the standard overhead bytes
			mDataField = writer.ToByteArray();

			int checkSum = 0;
			for(int loop = 0; loop < mDataField.Length - 2; loop++)
				checkSum += mDataField[loop];

			mDataField[mDataField.Length-2] = (byte)(checkSum & 0xFF);

			base.Encode(ref theData);
		}

		public void Clear()
		{
			ClearCheckPoints();
			_startTimeUTC	= DateTime.MinValue;
			_vehicleRouteScheduleID = 0;
			_action = RouteUpdateAction.Add;
		}
	}
}

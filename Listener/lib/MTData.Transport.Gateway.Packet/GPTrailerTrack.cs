using System;
using System.Collections;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// GPTransportExtraDetails Packet Structure
	/// 
	///  Type																						Size
	/// Trailer Seperator (0xAA)														1 byte
	///  Number of Trailers (0 -5)														1 bytes
	///  Trailer ID 1																			6 bytes
	///  Trailer ID 2																			6 bytes
	///  Trailer ID 3																			6 bytes
	///  Trailer ID 4																			6 bytes
	///  Trailer ID 5																			6 bytes
	/// </summary>

    [Serializable]
    public class GPTrailerTrack
	{
		public int Length = 2; // Minimum size is 2 bytes
		public ArrayList oTrailers = new ArrayList();
		public int iTrailerCount = 0;

		public GPTrailerTrack()
		{
		}

		public GPTrailerTrack CreateCopy()
		{
			GPTrailerTrack oTrailerTrack = new GPTrailerTrack();
			oTrailerTrack.Length = this.Length;
			oTrailerTrack.iTrailerCount = this.iTrailerCount;
			for (int X = 0; X < oTrailers.Count; X++)
			{
				oTrailerTrack.oTrailers.Add(this.oTrailers[X]);
			}
			return oTrailerTrack;
		}

		public string Decode(byte[] aData, int iPos)
		{
			string sMsg = "";
			int pos = iPos;
			byte bCompare = (byte) 0x00;
			uint uTemp = 0;
			try
			{
                this.Length = 0;
				if (pos + 1 < aData.Length)
				{
					bCompare = (byte) aData[pos++];
					this.Length++;
				}

				if (bCompare == (byte) 0xAA)
				{
					if (pos + 1 < aData.Length)
					{
						uTemp =	Convert.ToUInt32(aData[pos++]);
						iTrailerCount = Convert.ToInt32(uTemp);
						this.Length++;
						for (int X = 0; X < iTrailerCount; X++)
						{
							if (pos + 5 < aData.Length)
							{
								byte[] bTrailerNumber = new byte[6];
								bTrailerNumber[0] = aData[pos++];
								bTrailerNumber[1] = aData[pos++];
								bTrailerNumber[2] = aData[pos++];
								bTrailerNumber[3] = aData[pos++];
								bTrailerNumber[4] = aData[pos++];
								bTrailerNumber[5] = aData[pos++];
								oTrailers.Add(bTrailerNumber);
								this.Length += 6;
							}
						}
					}
				}
			}
			catch(System.Exception ex)
			{
				sMsg = "Error decoding trailer track values : " + ex.Message;
			}
			if (sMsg == "")
				return null;
			else
				return sMsg;
		}

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            this.Length = 0;
            // Ensure we've got a decent array:
            if (theData.Length >= (aStartPos + Length))
            {
                theData[aStartPos++] = (byte)0xAA;
                this.Length++;
                if (oTrailers != null)
                {
                    theData[aStartPos++] = (byte)oTrailers.Count;
                    this.Length++;
                    for (int X = 0; X < oTrailers.Count; X++)
                    {
                        bTemp = (byte[])oTrailers[X];
                        if (bTemp.Length == 6)
                        {
                            for (int Y = 0; Y < 6; Y++)
                            {
                                theData[aStartPos++] = bTemp[Y];
                                this.Length++;
                            }
                        }
                    }
                }
                else
                {
                    theData[aStartPos++] = (byte)0x00;
                    this.Length++;
                }
            }
        }

        public string ToDisplayString()
		{
			return	this.ToString();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				if (oTrailers.Count > 0)
				{
					builder.Append("\r\nTrailer Data :"); 
					for (int X = 0; X < oTrailers.Count; X ++)
					{
						try
						{
							builder.Append("\r\nTrailer " + Convert.ToString(X + 1) + " : "); builder.Append(BitConverter.ToString((byte[]) oTrailers[X], 0));
						}
						catch(System.Exception ex)
						{
							builder.Append("\r\nError Decoding Trailer ID : " + ex.Message);
						}
					}
				}
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding GPTrailerTrack : " + ex.Message);
			}
			return builder.ToString();
		}


	}
}

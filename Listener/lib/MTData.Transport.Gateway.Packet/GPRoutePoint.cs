using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Storage class for a Route Point in a New Route CONFIG.
	/// </summary>
    [Serializable]
    public class GPRoutePoint
	{
		public Byte cPointNumber;
		public Byte cNextPointNumber;
		public int iFlags;
		public float fBinLat;
		public float fBinLong;
		public int iLongTolerance;
		public int iLatTolerance;
		public Byte cArriveHour;
		public Byte cArriveMinute;
		public Byte cArriveTolerance;
		public Byte cDepartHour;
		public Byte cDepartMinute;
		public Byte cDepartTolerance;
		public Byte cDurationTime;
		public int iDistanceToNext;
		public Byte cTimeToNext;

		public static int Length = 32;

		public GPRoutePoint()
		{
		}

		/// <summary>
		/// Return a human readable representation of the data
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append("cPointNumber : "); builder.Append(cPointNumber);
			builder.Append("cNextPointNumber : "); builder.Append(cNextPointNumber);
			builder.Append("; iFlags : "); builder.Append(iFlags);
			builder.Append("; fBinLat : "); builder.Append(fBinLat);
			builder.Append("; fBinLong : "); builder.Append(fBinLong);
			builder.Append("; iLatTolerance : "); builder.Append(iLatTolerance);
			builder.Append("; iLongTolerance : "); builder.Append(iLongTolerance);
			builder.Append("; cArriveHour : "); builder.Append(cArriveHour);
			builder.Append("; cArriveMinute : "); builder.Append(cArriveMinute);
			builder.Append("; cArriveTolerance : "); builder.Append(cArriveTolerance);
			builder.Append("; cDepartHour : "); builder.Append(cDepartHour);
			builder.Append("; cDepartMinute : "); builder.Append(cDepartMinute);
			builder.Append("; cDepartTolerance : "); builder.Append(cDepartTolerance);
			builder.Append("; cDurationTime : "); builder.Append(cDurationTime);
			builder.Append("; iDistanceToNext : "); builder.Append(iDistanceToNext);
			builder.Append("; cTimeToNext : "); builder.Append(cTimeToNext);

			return builder.ToString();
		}


		// Encode this point into theData, at position startPos
		public void Encode(ref Byte[] theData, int startPos)
		{
			uint tempLatLong = 0;

			// Basic info
			theData[startPos++] = cPointNumber;
			theData[startPos++] = cNextPointNumber;

			// All multi-byte fields are Big-endian
			theData[startPos++] = (Byte) (iFlags & 0xFF);
			theData[startPos++] = (Byte) ((iFlags & 0xFF00) >> 8);
			
			// Positions and tolerances
			// N.B. All multi-byte variables are big-endian
			// Positions and tolerances
			tempLatLong = (uint) (Math.Abs(fBinLat) * 60000);
			theData[startPos++] = (Byte) (tempLatLong & 0xFF);
			theData[startPos++] = (Byte) ((tempLatLong & 0xFF00) >> 8);
			theData[startPos++] = (Byte) ((tempLatLong & 0xFF0000) >> 16);
			
			tempLatLong = (uint) (Math.Abs(fBinLong) * 60000);
			theData[startPos++] = (Byte) (tempLatLong & 0xFF);
			theData[startPos++] = (Byte) ((tempLatLong & 0xFF00) >> 8);
			theData[startPos++] = (Byte) ((tempLatLong & 0xFF0000) >> 16);
			
			//--------------------------------------------------------------------------
			//	This code is not ordered as would be expected, due to an innaccuracy 
			//	in the original 3021 code. The 3021 expects Lat, Long, LongTolerance, LatTolerance,
			//	and sp that is how we will present it.
			theData[startPos++] = (Byte) (iLongTolerance& 0xFF);
			theData[startPos++] = (Byte) ((iLongTolerance & 0xFF00) >> 8);

			theData[startPos++] = (Byte) (iLatTolerance& 0xFF);
			theData[startPos++] = (Byte) ((iLatTolerance & 0xFF00) >> 8);
			//--------------------------------------------------------------------------

			// Timings
			theData[startPos++] = cArriveHour;
			theData[startPos++] = cArriveMinute;
			theData[startPos++] = cArriveTolerance;
			theData[startPos++] = cDepartHour;
			theData[startPos++] = cDepartMinute;
			theData[startPos++] = cDepartTolerance;
			theData[startPos++] = cDurationTime;
			theData[startPos++] = (Byte) (iDistanceToNext& 0xFF);
			theData[startPos++] = (Byte) ((iDistanceToNext & 0xFF00) >> 8);
			theData[startPos++] = cTimeToNext;

			// 8 spare bytes:
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
			theData[startPos++] = 0x00;
		}

	}
}

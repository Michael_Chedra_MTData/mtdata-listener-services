using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class DriverPointsServerTrackingRuleBroken : GatewayProtocolPacket
    {
        #region private fields
        public const byte DRIVER_POINTS_SERVER_TRACKING_RULE_BROKEN = 0xE5;

        private string _serverTime_DateFormat;
        private string _constructorDecodeError;

        private byte _version;
        private int _ruleId;
        private byte _points;
        private short _output1;
        private short _output2;
        private int _id;
        private GPClock _gpsTime;
        private string _location;
        #endregion

        #region public properties
        public byte PacketVersion { get { return _version; } set { _version = value; } }
        public int RuleId { get { return _ruleId; } set { _ruleId = value; } }
        public int Points { get { return (int)_points; } set { try { _points = (byte)value; } catch { _points = 0xFF; } } }
        public short Output1 { get { return _output1; } set { _output1 = value; } }
        public short Output2 { get { return _output2; } set { _output2 = value; } }
        public int Id { get { return _id; } set { _id = value; } }
        public GPClock GpsTime { get { return _gpsTime; } set { _gpsTime = value; } }
        public string Location { get { return _location; } set { _location = value; } }
        #endregion

        #region constructor
        public DriverPointsServerTrackingRuleBroken(string serverTime_DateFormat) : base (serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }
        public DriverPointsServerTrackingRuleBroken(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
            Initialise(serverTime_DateFormat);
            _constructorDecodeError = DecodePayload();
            bIsPopulated = (_constructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = DRIVER_POINTS_SERVER_TRACKING_RULE_BROKEN;
            bIsFileType = true;

            bIsPopulated = false;
            _version = 1;
            _ruleId = 0;
            _points = 0x00;
            _output1 = 0;
            _output2 = 0;
            _id = 0;
            _gpsTime = new GPClock("GPS", serverTime_DateFormat);
            _location = string.Empty;
            _constructorDecodeError = null;
        }
        #endregion

        public new DriverPointsServerTrackingRuleBroken CreateCopy()
        {
            DriverPointsServerTrackingRuleBroken oData = new DriverPointsServerTrackingRuleBroken(this, _serverTime_DateFormat);
            oData.PacketVersion = _version;
            oData.RuleId = _ruleId;
            oData.Points = (int)_points;
            oData.Output1 = _output1;
            oData.Output2 = _output2;
            oData.Id = _id;
            if (_gpsTime != null)
            {
                oData.GpsTime = _gpsTime.CreateCopy();
            }
            else
            {
                oData.GpsTime = new GPClock("GPS", _serverTime_DateFormat);
            }
            oData.Location = _location;
            return oData;
        }
    
        public string Decode(Byte[] data)
        {
            string problem = null;
            if (!bIsPopulated) problem = base.Decode(data, 0);
            if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

            return problem;
        }
        public string DecodePayload()
        {
            try
            {
                MemoryStream oMS = new MemoryStream(mDataField, 0, mDataField.Length);
               PacketUtilities.ReadFromStream(oMS, ref _version);
               PacketUtilities.ReadFromStream(oMS, ref _ruleId);
               PacketUtilities.ReadFromStream(oMS, ref _points);
               PacketUtilities.ReadFromStream(oMS, ref _output1);
               PacketUtilities.ReadFromStream(oMS, ref _output2);
               PacketUtilities.ReadFromStream(oMS, ref _id);
                byte[] time = new byte[6];
               PacketUtilities.ReadFromStream(oMS, 6, ref time);
                _gpsTime.Decode(time);
               PacketUtilities.ReadFromStream(oMS, 0x12, ref _location);
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }

        public new void Encode(ref Byte[] theData)
        {
            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _version);
           PacketUtilities.WriteToStream(oMS, _ruleId);
           PacketUtilities.WriteToStream(oMS, _points);
           PacketUtilities.WriteToStream(oMS, _output1);
           PacketUtilities.WriteToStream(oMS, _output2);
           PacketUtilities.WriteToStream(oMS, _id);
            byte[] time = new byte[6];
            _gpsTime.Encode(ref time, 0);
           PacketUtilities.WriteToStream(oMS, time);
           PacketUtilities.WriteToStream(oMS, 0x12, _location);
            mDataField = oMS.ToArray();

            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
        }

        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nDriver Points Server Tracking Rule Broken :");
                builder.Append("\r\n    Packet Version : "); builder.Append(_version);
                builder.Append("\r\n    Rule Id : "); builder.Append(_ruleId);
                builder.Append("\r\n    Points : "); builder.Append(_points);
                builder.Append("\r\n    Output 1 : "); builder.Append(_output1);
                builder.Append("\r\n    Output 2 : "); builder.Append(_output2);
                builder.Append("\r\n    Id : "); builder.Append(_id);
                builder.Append("\r\n    GPS Time : "); builder.Append(_gpsTime.ToString());
                builder.Append("\r\n    Location : "); builder.Append(_location);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nDriver Points Server Tracking Rule Broken : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class UserAckForUnitAlarm : GatewayProtocolPacket
    {
        public const byte USER_ACK_FOR_UNIT_ALARM = 0x45;
        #region Private Member Vars
        private byte _PacketVer;
        private byte _AlarmAckFlags;
        private string _serverTime_DateFormat;
        #endregion
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public byte AlarmAckFlags
        {
            get { return _AlarmAckFlags; }
            set { _AlarmAckFlags = value; }
        }
        #endregion
        #region Constructor and CreateCopy functions
        public UserAckForUnitAlarm(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            base.cMsgType = USER_ACK_FOR_UNIT_ALARM;
            _serverTime_DateFormat = serverTime_DateFormat;
            bIsFileType = true;
            _PacketVer = 0;
            _AlarmAckFlags = 0x00;
        }
        public UserAckForUnitAlarm(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
            base.cMsgType = USER_ACK_FOR_UNIT_ALARM;
            _serverTime_DateFormat = serverTime_DateFormat;
            Decode(p.mDataField);
        }
        public new UserAckForUnitAlarm CreateCopy()
        {
            UserAckForUnitAlarm oData = new UserAckForUnitAlarm(_serverTime_DateFormat);
            oData.PacketVer = _PacketVer;
            oData.AlarmAckFlags = _AlarmAckFlags;
            return oData;
        }
        #endregion
        #region Encoding / Decoding
        public string Decode(Byte[] aData)
        {
            MemoryStream oMS = new MemoryStream(aData, 0, aData.Length);
            return Decode(ref oMS);
        }
        public string Decode(Byte[] aData, ref int index)
        {
            MemoryStream oMS = new MemoryStream(aData, index, aData.Length - index);
            string sRet = Decode(ref oMS);
            if (sRet != null) return sRet;
            index += (int)oMS.Position;
            return null;
        }
        public string Decode(ref MemoryStream oMS)
        {
            try
            {
               PacketUtilities.ReadFromStream(oMS, ref _PacketVer);
               PacketUtilities.ReadFromStream(oMS, ref _AlarmAckFlags);
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }

        public void Encode()
        {
            EncodeDataField();
            //base.Encode(ref theData);
        }

        public void EncodeDataField()
        {
            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _PacketVer);
           PacketUtilities.WriteToStream(oMS, _AlarmAckFlags);
            mDataField = oMS.ToArray();
        }
        #endregion
        #region ToString functions
        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nUser Ack For Unit Alarm Packet :");
                builder.Append("\r\n    Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n    Alarm Ack Flags : "); builder.Append(_AlarmAckFlags);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Config Watch List Packet : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigNewSetPtGPPacket.
	/// </summary>
    [Serializable]
    public class ConfigNewSetPtGPPacket : GatewayProtocolPacket
	{
		public const Byte CONFIG_NEW_SETPT = 0xd3;
		public const Byte CONFIG_NEW_SETPT_POLYGON_OLD = 0xdb;
        public const Byte CONFIG_NEW_SETPT_POLYGON = 0xdc;

		public byte cSetPointSetNumber;
		public byte cPageAdjust;
		public int iSetPointVersion;
		public int iNumPoints;
		public GPSetPoint[] mPointsList;

		public byte ProtocolVer;
		public byte cSetPointSetActive;
		public byte cSetPointSetActiveVersionMajor;
		public byte cSetPointSetActiveVersionMinor;
		public byte cSetPointSetSegmentRequest;


		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncSetPointSetNumber : "); builder.Append(cSetPointSetNumber);
			builder.Append("\r\ncPageAdjust : "); builder.Append(cPageAdjust);
			builder.Append("\r\niSetPointVersion : "); builder.Append(iSetPointVersion);
			builder.Append("\r\niNumPoints : "); builder.Append(iNumPoints);
			if ((mPointsList != null) && (iNumPoints > 0))
				for (int loop = 0; loop < iNumPoints; loop++)
				{
					if (mPointsList[loop] != null)
					{
						builder.Append("\r\n  Point ");
						builder.Append(loop);
						builder.Append(" - ");
						builder.Append(mPointsList[loop].ToString());
						builder.Append("\r\n");
					}
				}
			return builder.ToString();
		}


		public ConfigNewSetPtGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat, byte SegmentRequest, byte decodeRequest)
			: base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_NEW_SETPT_POLYGON;
            if(p.cMsgType == ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON_OLD)
                cMsgType = CONFIG_NEW_SETPT_POLYGON_OLD;
			Decode(p.mDataField);
		}


		public ConfigNewSetPtGPPacket(string serverTime_DateFormat)
			: base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_NEW_SETPT;
			bIsFileType = true;
			iNumPoints = 0;
			mPointsList = new GPSetPoint[300];
			ClearGroupData();
		}

		public ConfigNewSetPtGPPacket(string serverTime_DateFormat, uint SetPointGroupSpare1)
			: base(serverTime_DateFormat)
		{
			if ((SetPointGroupSpare1 & 0x01) == 1)
			{
				cMsgType = CONFIG_NEW_SETPT_POLYGON;
			}
			else
			{
				cMsgType = CONFIG_NEW_SETPT;
			}
			mPointsList = new GPSetPoint[300]; 

			bIsFileType = false;
			iNumPoints = 0;
			ClearGroupData();
		}


		public ConfigNewSetPtGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
			: base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_NEW_SETPT;
			bIsFileType = false;
			iNumPoints = 0;
			mPointsList = new GPSetPoint[300];

			ClearGroupData();
		}

		public ConfigNewSetPtGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat, uint SetPointGroupSpare1)
			: base(p, serverTime_DateFormat)
		{
			if ((SetPointGroupSpare1 & 0x01) == 1)
			{
				cMsgType = CONFIG_NEW_SETPT_POLYGON;
			}
			else
			{
				cMsgType = CONFIG_NEW_SETPT;
			}
			mPointsList = new GPSetPoint[300]; 
			bIsFileType = false;
			iNumPoints = 0;
			ClearGroupData();
		}


		public void AddPoint(GPSetPoint aPoint)
		{
			if (iNumPoints + 1 > mPointsList.Length)
				return;												//make sure we don't over populate the amount of waypoints we support
			mPointsList[iNumPoints++] = aPoint;
		}

		//x/y tolerance waypoint encode method
		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0, pointNum;
			int checksum = 0;

			cMsgType = CONFIG_NEW_SETPT;

			// Make sure we have sufficient space:
			iLength = (uint)21 + (uint)(iNumPoints * GPSetPoint.Length);
			if (mDataField.Length != iLength)
			{
				mDataField = new Byte[iLength];
			}

			mDataField[encPos++] = GSP_SOH;
			mDataField[encPos++] = cSetPointSetNumber;
			mDataField[encPos++] = (Byte)((iSetPointVersion & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte)(iSetPointVersion & 0xFF);

			// Size of this data
			mDataField[encPos++] = (Byte)((iLength & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte)(iLength & 0xFF);

			mDataField[encPos++] = cPageAdjust;
			encPos += 10;			// 10 bytes of Spare

			mDataField[encPos++] = (Byte)iNumPoints;

			// Encode each point
			for (pointNum = 0; pointNum < iNumPoints; pointNum++)
			{
				mPointsList[pointNum].Encode(ref mDataField, (byte) 0x00, encPos, 1);
				encPos += GPSetPoint.Length;
			}
			encPos++;	// Spare byte

			// Throw in a checksum:
			for (int pos = 0; pos < encPos; pos++)
			{
				checksum += mDataField[pos];
			}
			// Write in the checksum and the end-of-message
			mDataField[encPos++] = Convert.ToByte(checksum & 0xFF);

			// Finish the packet
			mDataField[encPos++] = GSP_EOT;

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}


		//Polygon Waypoint encode method
		public void Encode(ref Byte[] theData, byte cMsgType, byte Segment, byte bProtocolVer)
		{
			int encPos = 0, i;
			int checksum = 0;
            int iMaxPacketSize = 700;
			int SetPointsInSegment_Location = 0, SetPointsInSegment = 0, SetPointsInAllSegments = 0;
			byte[] SegmentData = new byte[950];
			byte[] SegmentData_Temp = new byte[950];
			int CurrentEncodePoint = 0;
			byte TotalSegments = 0;
			int PassingMechanism = 255;
			int SetPointDataLength = 0, SetPointDataLength_Location = 0;
			byte Segment_encoded = 0;

			//cMsgType = CONFIG_NEW_SETPT_POLYGON;
            base.cMsgType = cMsgType;
            if (bProtocolVer == (byte)0x02)
                iMaxPacketSize = 900;
			for (int Stage = 1; Stage <= 2; Stage++)
			{
				//stage 1 is the caluclation of the amoun to total segments
				//stage 2 is the encode of the desired packet

				Segment_encoded = 0;
				SetPointsInAllSegments = 0;
				CurrentEncodePoint = 0;
				if (Stage == 2)
					PassingMechanism = Segment;

				//go through and encode all of the segments and take the segment requested
				for (int j = 0; j < PassingMechanism + 1; j++)
				{
					encPos = 0;
					SetPointsInSegment = 0;

					SegmentData[encPos++] = 0xAA;
                    SegmentData[encPos++] = bProtocolVer;
					SegmentData[encPos++] = cSetPointSetNumber;
					
					SegmentData[encPos++] = (Byte)(iSetPointVersion & 0xFF);
					SegmentData[encPos++] = (Byte)((iSetPointVersion & 0xFF00) >> 8);

					//Setpoints in this segment (encoded after packet has been construcuted)
					SetPointsInSegment_Location = encPos;
					encPos += 2;

					//Total No of SetPoints
					SegmentData[encPos++] = (Byte)(iNumPoints & 0xFF);
					SegmentData[encPos++] = (Byte)((iNumPoints & 0xFF00) >> 8);

					//Segment number of this packet
					SegmentData[encPos++] = Segment_encoded;

					//Segment Total
					SegmentData[encPos++] = TotalSegments;

					//Length of setpoint data
					SetPointDataLength_Location = encPos;
					encPos += 2;

					SetPointDataLength = encPos;
					
					// Encode each point
					for (i = CurrentEncodePoint; i < iNumPoints; i++)
					{
                        if (encPos + (mPointsList[CurrentEncodePoint].Encode(ref SegmentData_Temp, cMsgType, 0, bProtocolVer)) >= iMaxPacketSize)
							break;
						encPos += mPointsList[CurrentEncodePoint++].Encode(ref SegmentData, cMsgType, encPos, bProtocolVer);
						SetPointsInSegment++;
						SetPointsInAllSegments++;
					}
					
					//Setpoint data length
					SetPointDataLength = encPos - SetPointDataLength;
					SegmentData[SetPointDataLength_Location++] = (Byte)(SetPointDataLength & 0xFF);
					SegmentData[SetPointDataLength_Location++] = (Byte)((SetPointDataLength & 0xFF00) >> 8);
					

					if (Stage == 1)		//stage 0 is the caluclation of the amoun to total segments
					{
						TotalSegments++;
						if (SetPointsInAllSegments == iNumPoints)		//if all of the points have been encoded, then break the caluation menthod
							break;
						continue;
					}

					//Stage 1

					//if this is not the segment we're after then continue encoding the next
					if (Segment-- != 0)
					{
						Segment_encoded++;
						continue;
					}
					//otherwise, finish off the encode on this segment and hand over encoding to be sent to the vehicle as this is the segment requested

					//Setpoints in this segment
					SegmentData[SetPointsInSegment_Location++] = (Byte)(SetPointsInSegment & 0xFF);
					SegmentData[SetPointsInSegment_Location++] = (Byte)((SetPointsInSegment & 0xFF00) >> 8);

					// calculate the a checksum:
					for (i = 0; i < encPos; i++)
					{
						checksum += SegmentData[i];
					}

					// Write in the checksum and the end-of-message
					SegmentData[encPos++] = Convert.ToByte(checksum & 0xFF);

					// Finish the packet
					SegmentData[encPos++] = 0xBB;

					break;
				}
			}

			mDataField = new Byte[encPos];

			//copy in the endcoded segment to the mDataField byte array.  Also create the length of th data array
			for (i = 0; i < encPos; i++)
			{
				mDataField[i] = SegmentData[i];
			}

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}

		public string Decode(byte[] theData)
		{
			int decPos = 0;
			string problem = null;
			if (!bIsPopulated) problem = base.Decode(theData, 0);
			if (problem != null) return problem;


            //first byte is 0xAA
            decPos++;
            ProtocolVer = mDataField[decPos++];

			cSetPointSetActive = mDataField[decPos++];
			cSetPointSetActiveVersionMinor = mDataField[decPos++];
            cSetPointSetActiveVersionMajor = mDataField[decPos++];

            cSetPointSetSegmentRequest = mDataField[decPos++];

			return problem;
		}

		private void ClearGroupData()
		{
			cSetPointSetNumber = 0;
			cPageAdjust = 0;
			iSetPointVersion = 0;
			iNumPoints = 0;
		}
	}
}

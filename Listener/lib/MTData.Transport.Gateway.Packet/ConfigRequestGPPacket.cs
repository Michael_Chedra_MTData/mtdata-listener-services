using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigRequestGPPacket.
	/// </summary>
    [Serializable]
    public class ConfigRequestGPPacket : GatewayProtocolPacket
	{	
		public const byte CONFIG_REQUEST = 0xd8;
		public const int CONFIG_REQUEST_LENGTH = 3;
	
		#region Variables
		public byte cConfigRequested;
		public byte cRouteRequested;
		public byte cSetRequested;
		#endregion

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncConfigRequested : "); builder.Append(cConfigRequested);
			builder.Append("\r\ncRouteRequested : "); builder.Append(cRouteRequested);
			builder.Append("\r\ncSetRequested : "); builder.Append(cSetRequested);
			return builder.ToString();
		}

		public ConfigRequestGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_REQUEST;
			cConfigRequested = 0;
			cRouteRequested = 0;
			cSetRequested = 0;
		}

		public ConfigRequestGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_REQUEST;
			Decode(p.mDataField);
		}

		public string Decode(Byte[] theData)
		{
			int decPos = 0;
			string problem = null;
			if (!bIsPopulated)	problem = base.Decode(theData, 0);
			if (problem != null) return problem;

			if (iLength != CONFIG_REQUEST_LENGTH) 
				return "Incorrect length to decode CONFIG_REQUEST";
			cConfigRequested = mDataField[decPos++];
			cRouteRequested = mDataField[decPos++];
			cSetRequested = mDataField[decPos++];

			return problem;
		}

	}
}

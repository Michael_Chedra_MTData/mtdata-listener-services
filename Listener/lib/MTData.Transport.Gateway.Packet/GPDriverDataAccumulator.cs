using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class GPDriverDataAccumulator : GatewayProtocolPacket, IStatusHolder, IPositionHolder
    {
        private string _serverTime_DateFormat = "";
        private byte _packetVer = (byte) 0x00;
        private byte _packetFlags = (byte)0x00;
        private byte _accumulatorType = (byte)0x00;
        private byte _typeOfEncode = (byte)0x00;
        private ushort _SPNType = 0;
        private ushort _SPNValue = 0;
        private byte _arraySize = (byte)0x00;
        private byte[] _metricData = null;

        public GPClock mCurrentClock;
        public GPClock mFixClock;
        public GPPositionFix mFix;
        public GPStatus mStatus;
        public GPDistance mDistance;

        public int PacketVersion { get { return (int)_packetVer; } set { _packetVer = (byte) value; } }
        public byte PacketFlags { get { return _packetFlags; } set { _packetFlags = value; }}
        public byte AccumulatorType { get { return _accumulatorType; } set { _accumulatorType = value; } }
        public byte TypeOfEncode { get { return _typeOfEncode; } set { _typeOfEncode = value; } }
        public ushort SPNType { get { return _SPNType; } set { _SPNType = value; } }
        public ushort SPNValue { get { return _SPNValue; } set { _SPNValue = value; } }
        public byte[] MetricData { get { return _metricData; } set { _metricData = value; } }
        public GPStatus GetStatus()
        {
            return mStatus;
        }
        public GPPositionFix GetPosition()
        {
            return mFix;
        }
        public GPClock GetFixClock()
        {
            return mFixClock;
        }

        public GPDriverDataAccumulator(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mCurrentClock = new GPClock("Device", _serverTime_DateFormat); 
			mFixClock = new GPClock("GPS", _serverTime_DateFormat); 
			mFix = new GPPositionFix(); 
			mStatus = new GPStatus(); 
			mDistance = new GPDistance();
			string sConstructorDecodeError = Decode(p.mDataField);
			bIsPopulated = (sConstructorDecodeError == null);
		}

        public string Decode(byte[] aData)
        {
            string retMsg = null;
            int pos = 0;
            try
            {
                if ((retMsg = mCurrentClock.Decode(aData, pos)) != null)
                {
                    // most likely this is an invalid clock (i.e. we haven't
                    // acquired GPS as yet). We will skip decoding the rest
                    // of the packet except for the Engine info which, as it
                    // is not derived from GPS, may be of some value
                    pos += GPClock.Length + GPClock.Length + GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(aData, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    pos += GPDistance.Length;
                }
                else
                {
                    pos += GPClock.Length;
                    if ((retMsg = mFixClock.Decode(aData, pos)) != null)
                    {
                        //return retMsg;
                    }
                    pos += GPClock.Length;
                    if ((retMsg = mFix.Decode(aData, pos)) != null) return retMsg;
                    pos += GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(aData, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    if ((retMsg = mDistance.Decode(aData, pos)) != null) return retMsg;
                    pos += GPDistance.Length;
                }

                _packetVer = aData[pos++];
                _packetFlags = aData[pos++];
                _typeOfEncode = aData[pos++];
                _accumulatorType = aData[pos++];
                byte[] bConvert = new byte[2];
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _SPNType = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _SPNValue = BitConverter.ToUInt16(bConvert, 0);
                _arraySize = aData[pos++];
                byte bNumOfBytes = aData[pos++];
                _metricData = new byte[(int)bNumOfBytes];
                for (int X = 0; X < (int)bNumOfBytes; X++)
                {
                    _metricData[X] = aData[pos++];
                }
            }
            catch (Exception ex)
            {
                retMsg = ex.Message;
            }
            return retMsg;
        }

        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append(base.ToDisplayString());
                if (mStatus != null)
                {
                    builder.Append(mStatus.ToString());
                }
                builder.Append("\r\nTimes : ");
                builder.Append("\r\n    Server Time : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                if (mCurrentClock != null)
                {
                    if (mCurrentClock.ToString() == " ")
                        builder.Append("\r\n    Device Time : No Value");
                    else
                    {
                        builder.Append("\r\n    Device Time : " + mCurrentClock.ToString());
                        try
                        {
                            builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                        }
                        catch (System.Exception)
                        {
                        }
                    }
                }
                else
                {
                    builder.Append("\r\n    Device Time : No Value");
                }
                if (mFixClock != null)
                {
                    if (mFixClock.ToString() == " ")
                        builder.Append("\r\n    GPS Time : No Value");
                    else
                    {
                        builder.Append("\r\n    GPS Time : " + mFixClock.ToString());
                        try
                        {
                            builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                        }
                        catch (System.Exception)
                        {
                        }
                    }
                }
                else
                {
                    builder.Append("\r\n    GPS Time : No Value");
                }
                if (mFix != null)
                {
                    builder.Append(mFix.ToString());
                }
                if (mDistance != null)
                {
                    builder.Append(mDistance.ToString());
                }
                builder.Append("\r\nData Accumulator Values :");
                builder.Append("\r\n    Packet Version : "); builder.Append(_packetVer);
                builder.Append("\r\n    Packet Flags : "); builder.Append(_packetFlags);
                builder.Append("\r\n    Type of Encode : "); builder.Append(_typeOfEncode);
                builder.Append("\r\n    Accumulator Type : "); builder.Append(_accumulatorType);
                builder.Append("\r\n    SPN Source : "); builder.Append(_SPNType);
                builder.Append("\r\n    SPN Value : "); builder.Append(_SPNValue);

                string sData = BitConverter.ToString(_metricData).Replace("-", "");
                string sResult = "";
                int iState = 0;
                int byteCount = 0;
                for (int X = 0; X < sData.Length; X++)
                {
                    if (iState == 0)
                    {
                        sResult += sData[X] + " ";
                        iState = 1;
                    }
                    else if (iState == 1)
                    {
                        sResult += sData[X] + " ";
                        iState = 2;
                        byteCount = 0;
                    }
                    else if (iState == 2)
                    {
                        byteCount++;
                        if (byteCount < 4)
                            sResult += sData[X];
                        else
                        {
                            if (X < sData.Length -1)
                                sResult += sData[X] + "  ";
                            else
                                sResult += sData[X];
                            iState = 0;
                        }
                    }
                }
                builder.Append("\r\n    Data Format [X (1 Byte)] [Y (1 Byte)] [Value (2 Bytes)]  : ");
                builder.Append("\r\n    Data : "); builder.Append(sResult);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError in Driver Data Accumulator Metrics : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

using System;
using System.IO;
using System.Text;
using System.Collections;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Holds engine-related data in a Status Report
	/// </summary>
	[Serializable]
	public class GPECMAdvanced
	{
		#region Enums
		public enum ECMValueType : byte
		{
			CurrentValue = 0x00,
			AlertValue = 0x01,
			MaxValue = 0x02,
			MinValue = 0x04,
			BinaryValue = 0x08,
			DataLength1 = 0x10,
			DataLength2 = 0x20,
			DataLength4 = 0x40,
			AverageValue = 0x80
		}
		public enum EMCAdvancePacketIncludes : ushort
		{
			GPSSATELLITEDATA = 0x0001,
			SPEEDZONES = 0x0002,
			RPMZONE = 0x0004,
			GFORCE = 0x0008,
			EXTERNALIO = 0x0010,
			EMCDATA = 0x0020,
			GFORCE_EXT = 0x0040,
			GPSSATELLITEDATA_EXT = 0x0080,
			GPSSATELLITEDATA_EXTODO = 0x0100,
			CARDREADERDATA = 0x0200,
			SPEEDDATA = 0x0400,
			SPEEDDATA_TWODECPLACES = 0x0800 // Passes speed with 2 decimal places
		}
		#endregion

		#region Constants
		private const byte STATUS_INCLUDE_SEP_GPSSATELLITES = (byte)0xf0;
		private const byte STATUS_INCLUDE_SEP_SPEEDZONES = (byte)0xf1;
		private const byte STATUS_INCLUDE_SEP_RPMZONES = (byte)0xf2;
		private const byte STATUS_INCLUDE_SEP_GFORCE = (byte)0xf3;
		private const byte STATUS_INCLUDE_SEP_EXTERNALIO = (byte)0xf4;
		private const byte STATUS_INCLUDE_SEP_ECM = (byte)0xf5;
		private const byte STATUS_INCLUDE_SEP_CARDREADER = (byte)0xf6;
		private const byte STATUS_INCLUDE_SEP_SPEED = (byte)0xf7;

		private const byte EOS = (byte)0xff;
		private const double dGPSOdoDiv = 3600;
		private const double dGPSOdoToKms = 1.852;
		#endregion

		#region Private Variables
		public bool bIsPopulated = false;
		private int _iLengthRead = 0;
		private object SyncRoot = new object();
		private bool _speedAccuracyValuesSet = false;
		private ushort _speedAccuracyDP = 0;
		private ushort iECMFlags = 0;
		private byte[] bSpeedZones = new byte[4];
		private byte[] bRPMZones = new byte[4];
		private byte[] bGForce = new byte[5];
		private ushort[] bExternalIO = new ushort[4];
		private byte bSatellites_NoInView = 0;
		private byte bSatellites_HDOP = 0;
		private short bSatellites_Altitude = 0;
		private uint iGPSOdometer = 0;
		private double[] bGForce2 = new double[5];
		private GPECMAdvancedItem[] oValues = null;
		private byte _cardSegmentVersion = (byte)0x00;
		private string _driverName = "";
		private uint _driverLogin = 0;
		private ushort _driverPin = 0;
		private bool _driverCardData = false;
		private byte[] _cardId;
		private byte _cardType;
		private byte _speedDecimalPlace;
		private byte _maxSpeedDecimalPlace;
        private GPEngineSummaryData _engineSummaryData;
        #endregion
        #region Constructors
        public GPECMAdvanced()
		{
			bIsPopulated = false;
		}
		#endregion
		#region Public Methods
		public void Add(GPECMAdvancedItem oItem)
		{
			GPECMAdvancedItem[] oTemp = null;
			if (oItem != null)
			{
				lock (SyncRoot)
				{
					if (oValues == null)
					{
						oValues = new GPECMAdvancedItem[1];
						oValues[0] = oItem;
					}
					else
					{
						oTemp = new GPECMAdvancedItem[oValues.Length + 1];
						for (int X = 0; X < oValues.Length; X++)
							oTemp[X] = oValues[X];
						oTemp[oTemp.Length - 1] = oItem;
						oValues = oTemp;
					}
				}
			}
		}
		public int Count
		{
			get
			{
				lock (SyncRoot)
				{
					if (oValues != null)
						return oValues.Length;
					else
						return 0;
				}
			}
		}
		public void RemoveAt(int iPos)
		{
			int iCount = 0;
			GPECMAdvancedItem[] oTemp = null;
			lock (SyncRoot)
			{
				if (oValues == null && oValues.Length > iPos)
				{
					oTemp = new GPECMAdvancedItem[oValues.Length - 1];
					for (int X = 0; X < oValues.Length; X++)
					{
						if (X != iPos)
							oTemp[iCount++] = oValues[X];
					}
					oValues = oTemp;
				}
			}
		}
		public GPECMAdvancedItem this[int pos]
		{
			get
			{
				GPECMAdvancedItem oRet = null;
				lock (SyncRoot)
				{
					if (oValues != null && oValues.Length > pos)
						oRet = oValues[pos];
				}
				return oRet;
			}
			set
			{
				lock (SyncRoot)
				{
					if (oValues != null && oValues.Length < pos)
						oValues[pos] = value;
				}
			}
		}
		public GPECMAdvancedItem[] ECMAdvancedItems
		{
			get
			{
				return oValues;
			}
			set
			{
				oValues = value;
			}
		}
		public GPECMAdvanced CreateCopy()
		{
			GPECMAdvanced oGPECMAdvanced = new GPECMAdvanced();
            oGPECMAdvanced.bIsPopulated = this.bIsPopulated;
            oGPECMAdvanced._speedAccuracyValuesSet = this._speedAccuracyValuesSet;
            oGPECMAdvanced._speedAccuracyDP = this._speedAccuracyDP;
            oGPECMAdvanced.iECMFlags = this.iECMFlags;
            System.Array.Copy(this.bSpeedZones, oGPECMAdvanced.bSpeedZones, 4);
            System.Array.Copy(this.bRPMZones, oGPECMAdvanced.bRPMZones, 4);
            if (bGForce != null)
            {
                oGPECMAdvanced.bGForce2 = null;
                oGPECMAdvanced.bGForce = new byte[bGForce.Length];
                System.Array.Copy(this.bGForce, oGPECMAdvanced.bGForce, bGForce.Length);
            }
            if (bGForce2 != null)
            {
                oGPECMAdvanced.bGForce = null;
                oGPECMAdvanced.bGForce2 = new double[bGForce2.Length];
                System.Array.Copy(this.bGForce2, oGPECMAdvanced.bGForce2, bGForce2.Length);
            }
            System.Array.Copy(this.bExternalIO, oGPECMAdvanced.bExternalIO, 4);
            oGPECMAdvanced.bSatellites_NoInView = this.bSatellites_NoInView;
            oGPECMAdvanced.bSatellites_HDOP = this.bSatellites_HDOP;
            oGPECMAdvanced.bSatellites_Altitude = this.bSatellites_Altitude;
            oGPECMAdvanced.iGPSOdometer = this.iGPSOdometer;
            oGPECMAdvanced._cardSegmentVersion = this._cardSegmentVersion;
            oGPECMAdvanced._driverName = this._driverName;
            oGPECMAdvanced._driverLogin = this._driverLogin;
            oGPECMAdvanced._driverPin = this._driverPin;
            oGPECMAdvanced._driverCardData = this._driverCardData;
            if (_cardId != null)
            {
                oGPECMAdvanced._cardId = new byte[_cardId.Length];
                System.Array.Copy(this._cardId, oGPECMAdvanced._cardId, this._cardId.Length);
            }
            oGPECMAdvanced._cardType = this._cardType;
            oGPECMAdvanced._speedDecimalPlace = this._speedDecimalPlace;
            oGPECMAdvanced._maxSpeedDecimalPlace = this._maxSpeedDecimalPlace;
            if (this._engineSummaryData != null)
            {
                oGPECMAdvanced._engineSummaryData = _engineSummaryData.CreateCopy();
            }
			if (oValues != null)
			{
				for (int X = 0; X < oValues.Length; X++)
				{
					oGPECMAdvanced.Add(oValues[X].CreateCopy());
				}
			}
			return oGPECMAdvanced;
		}
		public string Decode(Byte[] aData)
		{
			int iPos = 0;
			return Decode(aData, ref iPos);
		}
		public string Decode(Byte[] aData, ref int aStartPos)
		{
			int iNumOfEntries = 0;
			byte bSectionHeaderCheck = (byte)0x00;
			_driverCardData = false;
			// Read the ECM Flags
			iECMFlags = BitConverter.ToUInt16(aData, aStartPos);
			aStartPos += 2;
            HasGForce = false;

			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA) == (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA)
			{
				#region Satellite Data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_GPSSATELLITES)
					return "Failed to find GPS satellite header bytes";

				bSatellites_NoInView = aData[aStartPos++];
				bSatellites_HDOP = aData[aStartPos++];
				bSatellites_Altitude = 0;
				iGPSOdometer = 0;
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXT) == (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXT)
			{
				#region Extended Satellite Data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_GPSSATELLITES)
					return "Failed to find GPS satellite header bytes";

				bSatellites_NoInView = aData[aStartPos++];
				bSatellites_HDOP = aData[aStartPos++];
				bSatellites_Altitude = BitConverter.ToInt16(aData, aStartPos);
				aStartPos += 2;
				iGPSOdometer = 0;
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXTODO) == (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXTODO)
			{
				#region Extended Satellite Data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_GPSSATELLITES)
					return "Failed to find GPS satellite header bytes";

				bSatellites_NoInView = aData[aStartPos++];
				bSatellites_HDOP = aData[aStartPos++];
				bSatellites_Altitude = BitConverter.ToInt16(aData, aStartPos);
				aStartPos += 2;
				iGPSOdometer = BitConverter.ToUInt32(aData, aStartPos);
				if (iGPSOdometer > 0)
					iGPSOdometer = Convert.ToUInt32((Convert.ToDouble(iGPSOdometer) / dGPSOdoDiv) * dGPSOdoToKms);
				aStartPos += 4;
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.SPEEDZONES) == (ushort)EMCAdvancePacketIncludes.SPEEDZONES)
			{
				#region Read Speed Zones
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_SPEEDZONES)
					return "Failed to find speed zone header byte";
				bSpeedZones[0] = aData[aStartPos++];
				bSpeedZones[1] = aData[aStartPos++];
				bSpeedZones[2] = aData[aStartPos++];
				bSpeedZones[3] = aData[aStartPos++];
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.RPMZONE) == (ushort)EMCAdvancePacketIncludes.RPMZONE)
			{
				#region Read RPM Zones
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_RPMZONES)
					return "Failed to find rpm zone header byte";
				bRPMZones[0] = aData[aStartPos++];
				bRPMZones[1] = aData[aStartPos++];
				bRPMZones[2] = aData[aStartPos++];
				bRPMZones[3] = aData[aStartPos++];
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GFORCE) == (ushort)EMCAdvancePacketIncludes.GFORCE)
			{
				bGForce2 = null;
                HasGForce = true;
                #region Read G-Force Values
                bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_GFORCE)
					return "Failed to find G-Force header byte";
				bGForce[0] = aData[aStartPos++];
				bGForce[1] = aData[aStartPos++];
				bGForce[2] = aData[aStartPos++];
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GFORCE_EXT) == (ushort)EMCAdvancePacketIncludes.GFORCE_EXT)
			{
				bGForce = null;
                HasGForce = true;
                bGForce2 = new double[4];
				#region Read G-Force Values
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_GFORCE)
					return "Failed to find G-Force header byte";

				bGForce2[0] = Convert.ToDouble(BitConverter.ToInt16(aData, aStartPos)) / (double)1000;     // X
				aStartPos += 2;
				bGForce2[1] = Convert.ToDouble(BitConverter.ToInt16(aData, aStartPos)) / (double)1000;     // Y
				aStartPos += 2;
				bGForce2[2] = Convert.ToDouble(BitConverter.ToInt16(aData, aStartPos)) / (double)1000;     // Z
				aStartPos += 2;
				bGForce2[3] = Convert.ToDouble(BitConverter.ToInt16(aData, aStartPos));     // Temperature
                if (bGForce2[3] != 0)
                {
                    //if units sending in temp, then it is the raw value (x /2 -30)
                    bGForce2[3] = (bGForce2[3] / 2.0) - 30;
                }
				aStartPos += 2;
				#endregion
			}

			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.EXTERNALIO) == (ushort)EMCAdvancePacketIncludes.EXTERNALIO)
			{
				#region Read Extended IO Values
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_EXTERNALIO)
					return "Failed to find G-Force header byte";
				bExternalIO[0] = BitConverter.ToUInt16(aData, aStartPos);
				aStartPos += 2;
				bExternalIO[1] = BitConverter.ToUInt16(aData, aStartPos);
				aStartPos += 2;
				bExternalIO[2] = BitConverter.ToUInt16(aData, aStartPos);
				aStartPos += 2;
				bExternalIO[3] = BitConverter.ToUInt16(aData, aStartPos);
				aStartPos += 2;
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.EMCDATA) == (ushort)EMCAdvancePacketIncludes.EMCDATA)
			{
				#region Read ECM data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_ECM)
					return "Failed to find ECM Value header byte";
				iNumOfEntries = (int)aData[aStartPos++];
				lock (SyncRoot)
				{
					// If we have already added some ECM entries prior to the parsing of the data (ENGINE_ERROR_CODE)
					int iStartLoopAt = 0;
					if (oValues != null)
					{
						iNumOfEntries += oValues.Length;
						iStartLoopAt = oValues.Length;
						GPECMAdvancedItem[] oTemp = new GPECMAdvancedItem[oValues.Length];
						for (int X = 0; X < oValues.Length; X++)
						{
							oTemp[X] = oValues[X];
						}
						oValues = new GPECMAdvancedItem[iNumOfEntries];
						for (int X = 0; X < oTemp.Length; X++)
						{
							oValues[X] = oTemp[X];
						}
					}
					else
						oValues = new GPECMAdvancedItem[iNumOfEntries];
					for (int X = iStartLoopAt; X < iNumOfEntries; X++)
					{
						oValues[X] = new GPECMAdvancedItem(aData, ref aStartPos);
					}
				}
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.CARDREADERDATA) == (ushort)EMCAdvancePacketIncludes.CARDREADERDATA)
			{
				#region Read Card Reader data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_CARDREADER)
					return "Failed to find Card Reader Value header byte";
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _cardSegmentVersion);
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, EOS, ref _driverName);
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _driverLogin);
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _driverPin);
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _cardType);
				byte cardIdLength = 0;
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref cardIdLength);
				_cardId = new byte[cardIdLength];
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, cardIdLength, ref _cardId);
				_driverCardData = true;
				#endregion
			}
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.SPEEDDATA) == (ushort)EMCAdvancePacketIncludes.SPEEDDATA)
			{
				#region Read speed accuracy data with one decimal place data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_SPEED)
					return "Failed to find Speed Accuracy Value header byte";
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _speedDecimalPlace);
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _maxSpeedDecimalPlace);
				_speedAccuracyValuesSet = true;
				_speedAccuracyDP = 1;
				#endregion
			}
			else if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.SPEEDDATA_TWODECPLACES) == (ushort)EMCAdvancePacketIncludes.SPEEDDATA_TWODECPLACES)
			{
				#region Read speed accuracy data with two decimal places of data
				bSectionHeaderCheck = aData[aStartPos++];
				if (bSectionHeaderCheck != STATUS_INCLUDE_SEP_SPEED)
					return "Failed to find Speed Accuracy Value header byte";
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _speedDecimalPlace);
				PacketUtilities.ReadFromPacketAtPos(aData, ref aStartPos, ref _maxSpeedDecimalPlace);
				_speedAccuracyValuesSet = true;
				_speedAccuracyDP = 2;
				#endregion
			}
			else
			{
				_speedDecimalPlace = 0;
				_maxSpeedDecimalPlace = 0;
				_speedAccuracyValuesSet = false;
			}
			return null;
		}
		public void Encode(ref byte[] theData, int aStartPos)
		{
			MemoryStream oMS = new MemoryStream();

			PacketUtilities.WriteToStream(oMS, iECMFlags);
			#region GPS Data
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA) == (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_GPSSATELLITES);
				PacketUtilities.WriteToStream(oMS, bSatellites_NoInView);
				PacketUtilities.WriteToStream(oMS, bSatellites_HDOP);
			}
			#endregion
			#region GPS Data Extended
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXT) == (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXT)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_GPSSATELLITES);
				PacketUtilities.WriteToStream(oMS, bSatellites_NoInView);
				PacketUtilities.WriteToStream(oMS, bSatellites_HDOP);
				PacketUtilities.WriteToStream(oMS, bSatellites_Altitude);
			}
			#endregion
			#region GPS Data Extended
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXTODO) == (ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXTODO)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_GPSSATELLITES);
				PacketUtilities.WriteToStream(oMS, bSatellites_NoInView);
				PacketUtilities.WriteToStream(oMS, bSatellites_HDOP);
				PacketUtilities.WriteToStream(oMS, bSatellites_Altitude);
                if (iGPSOdometer > 0)
                    PacketUtilities.WriteToStream(oMS, Convert.ToUInt32((Convert.ToDouble(iGPSOdometer) / dGPSOdoToKms) * dGPSOdoDiv));
                else
                    PacketUtilities.WriteToStream(oMS, iGPSOdometer);
			}
			#endregion
			#region Speed Zones
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.SPEEDZONES) == (ushort)EMCAdvancePacketIncludes.SPEEDZONES)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_SPEEDZONES);
				if (bSpeedZones != null)
				{
					if (bSpeedZones.Length > 0)
						PacketUtilities.WriteToStream(oMS, bSpeedZones[0]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bSpeedZones.Length > 1)
						PacketUtilities.WriteToStream(oMS, bSpeedZones[1]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bSpeedZones.Length > 2)
						PacketUtilities.WriteToStream(oMS, bSpeedZones[2]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bSpeedZones.Length > 3)
						PacketUtilities.WriteToStream(oMS, bSpeedZones[3]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);

				}
				else
				{
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
				}
			}
			#endregion
			#region RPM Zones
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.RPMZONE) == (ushort)EMCAdvancePacketIncludes.RPMZONE)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_RPMZONES);
				if (bRPMZones != null)
				{
					if (bRPMZones.Length > 0)
						PacketUtilities.WriteToStream(oMS, bRPMZones[0]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bRPMZones.Length > 1)
						PacketUtilities.WriteToStream(oMS, bRPMZones[1]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bRPMZones.Length > 2)
						PacketUtilities.WriteToStream(oMS, bRPMZones[2]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bRPMZones.Length > 3)
						PacketUtilities.WriteToStream(oMS, bRPMZones[3]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);

				}
				else
				{
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
				}
			}
			#endregion
			#region G-Force
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GFORCE) == (ushort)EMCAdvancePacketIncludes.GFORCE)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_GFORCE);
				if (bGForce != null)
				{
					if (bGForce.Length > 0)
						PacketUtilities.WriteToStream(oMS, bGForce[0]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bGForce.Length > 1)
						PacketUtilities.WriteToStream(oMS, bGForce[1]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
					if (bGForce.Length > 2)
						PacketUtilities.WriteToStream(oMS, bGForce[2]);
					else
						PacketUtilities.WriteToStream(oMS, (byte)0x00);
				}
				else
				{
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
				}
			}
			#endregion
			#region G-Force Extended
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.GFORCE_EXT) == (ushort)EMCAdvancePacketIncludes.GFORCE_EXT)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_GFORCE);
				if (bGForce2 != null)
				{
					if (bGForce2.Length > 0)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(bGForce2[0] * (double)1000));
					else
                        PacketUtilities.WriteToStream(oMS, (short)0);
					if (bGForce2.Length > 1)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(bGForce2[1] * (double)1000));
					else
                        PacketUtilities.WriteToStream(oMS, (short)0);
					if (bGForce2.Length > 2)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16(bGForce2[2] * (double)1000));
					else
                        PacketUtilities.WriteToStream(oMS, (short) 0);
					if (bGForce2.Length > 3)
						PacketUtilities.WriteToStream(oMS, Convert.ToInt16((bGForce2[3] + 30) * 2.0));
					else
						PacketUtilities.WriteToStream(oMS, (short) 60);

				}
				else
				{
                    PacketUtilities.WriteToStream(oMS, (short)0);
                    PacketUtilities.WriteToStream(oMS, (short)0);
                    PacketUtilities.WriteToStream(oMS, (short)0);
                    PacketUtilities.WriteToStream(oMS, (short)60);
				}
			}
			#endregion
			#region External IO
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.EXTERNALIO) == (ushort)EMCAdvancePacketIncludes.EXTERNALIO)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_EXTERNALIO);
				ushort iExternalValue = 0;
				if (bExternalIO != null)
				{
					if (bExternalIO.Length > 0)
						PacketUtilities.WriteToStream(oMS, bExternalIO[0]);
					else
						PacketUtilities.WriteToStream(oMS, iExternalValue);
					if (bExternalIO.Length > 1)
						PacketUtilities.WriteToStream(oMS, bExternalIO[1]);
					else
						PacketUtilities.WriteToStream(oMS, iExternalValue);
					if (bExternalIO.Length > 2)
						PacketUtilities.WriteToStream(oMS, bExternalIO[2]);
					else
						PacketUtilities.WriteToStream(oMS, iExternalValue);
					if (bExternalIO.Length > 3)
						PacketUtilities.WriteToStream(oMS, bExternalIO[3]);
					else
						PacketUtilities.WriteToStream(oMS, iExternalValue);

				}
				else
				{
					PacketUtilities.WriteToStream(oMS, iExternalValue);
					PacketUtilities.WriteToStream(oMS, iExternalValue);
					PacketUtilities.WriteToStream(oMS, iExternalValue);
					PacketUtilities.WriteToStream(oMS, iExternalValue);
				}
			}
			#endregion
			#region ECM Data
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.EMCDATA) == (ushort)EMCAdvancePacketIncludes.EMCDATA)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_ECM);
				if (oValues != null)
				{
					PacketUtilities.WriteToStream(oMS, (byte)oValues.Length);
					for (int X = 0; X < oValues.Length; X++)
						oValues[X].Encode(oMS);
				}
				else
				{
					PacketUtilities.WriteToStream(oMS, (byte)0x00);
				}
			}
			#endregion
			#region CardReader
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.CARDREADERDATA) == (ushort)EMCAdvancePacketIncludes.CARDREADERDATA)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_CARDREADER);
				PacketUtilities.WriteToStream(oMS, _cardSegmentVersion);
				PacketUtilities.WriteToStream(oMS, _driverName);
				PacketUtilities.WriteToStream(oMS, EOS);
				PacketUtilities.WriteToStream(oMS, _driverLogin);
				PacketUtilities.WriteToStream(oMS, _driverPin);
				PacketUtilities.WriteToStream(oMS, _cardType);
				int cardIdLength = _cardId.Length;
				PacketUtilities.WriteToStream(oMS, (byte)cardIdLength);
				PacketUtilities.WriteToStream(oMS, _cardId);
				_driverCardData = true;
			}
			#endregion
			#region Speed Data
			if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.SPEEDDATA) == (ushort)EMCAdvancePacketIncludes.SPEEDDATA)
			{
				PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_SPEED);
				PacketUtilities.WriteToStream(oMS, _speedDecimalPlace);
				PacketUtilities.WriteToStream(oMS, _maxSpeedDecimalPlace);
			}
            else if ((iECMFlags & (ushort)EMCAdvancePacketIncludes.SPEEDDATA_TWODECPLACES) == (ushort)EMCAdvancePacketIncludes.SPEEDDATA_TWODECPLACES)
            {
                PacketUtilities.WriteToStream(oMS, STATUS_INCLUDE_SEP_SPEED);
                PacketUtilities.WriteToStream(oMS, _speedDecimalPlace);
                PacketUtilities.WriteToStream(oMS, _maxSpeedDecimalPlace);
            }
            #endregion
			byte[] bRet = oMS.ToArray();
			_iLengthRead = bRet.Length;
			System.Array.Copy(bRet, 0, theData, aStartPos, _iLengthRead);
		}
		public string ToDisplayString()
		{
			return this.ToString();
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nECM Advanced :");
				builder.Append("\r\n    GPS Data : ");
				builder.Append("\r\n        Sats In View : "); builder.Append(this.bSatellites_NoInView);
				builder.Append("\r\n        HDOP : "); builder.Append(this.bSatellites_HDOP);
				builder.Append("\r\n        Altitude : "); builder.Append(this.bSatellites_Altitude);
				builder.Append("\r\n        GPS Odo : "); builder.Append(this.iGPSOdometer);
				builder.Append("\r\n    Speed Zones : ");
				if (bSpeedZones != null)
				{
					builder.Append("\r\n        Zone 1 : ");
					if (bSpeedZones.Length > 0)
						builder.Append(this.bSpeedZones[0]);
					else
						builder.Append(0);
					builder.Append("\r\n        Zone 2 : ");
					if (bSpeedZones.Length > 0)
						builder.Append(this.bSpeedZones[1]);
					else
						builder.Append(0);
					builder.Append("\r\n        Zone 3 : ");
					if (bSpeedZones.Length > 0)
						builder.Append(this.bSpeedZones[2]);
					else
						builder.Append(0);
					builder.Append("\r\n        Zone 4 : ");
					if (bSpeedZones.Length > 0)
						builder.Append(this.bSpeedZones[3]);
					else
						builder.Append(0);
				}
				else
				{
					builder.Append("\r\n        Zone 1 : 0");
					builder.Append("\r\n        Zone 2 : 0");
					builder.Append("\r\n        Zone 3 : 0");
					builder.Append("\r\n        Zone 4 : 0");
				}
				builder.Append("\r\n    RPM Zones : ");
				if (bRPMZones != null)
				{
					builder.Append("\r\n        Zone 1 : ");
					if (bRPMZones.Length > 0)
						builder.Append(this.bRPMZones[0]);
					else
						builder.Append(0);
					builder.Append("\r\n        Zone 2 : ");
					if (bRPMZones.Length > 0)
						builder.Append(this.bRPMZones[1]);
					else
						builder.Append(0);
					builder.Append("\r\n        Zone 3 : ");
					if (bRPMZones.Length > 0)
						builder.Append(this.bRPMZones[2]);
					else
						builder.Append(0);
					builder.Append("\r\n        Zone 4 : ");
					if (bRPMZones.Length > 0)
						builder.Append(this.bRPMZones[3]);
					else
						builder.Append(0);
				}
				else
				{
					builder.Append("\r\n        Zone 1 : 0");
					builder.Append("\r\n        Zone 2 : 0");
					builder.Append("\r\n        Zone 3 : 0");
					builder.Append("\r\n        Zone 4 : 0");
				}
				builder.Append("\r\n    G-Force : ");
				if (bGForce != null)
				{
					builder.Append("\r\n        Forward : "); if (bGForce.Length > 0) { builder.Append(this.bGForce[0]); } else { builder.Append(0); }
					builder.Append("\r\n        Backward : "); if (bGForce.Length > 1) { builder.Append(this.bGForce[1]); } else { builder.Append(0); }
					builder.Append("\r\n        Left/Right : "); if (bGForce.Length > 2) { builder.Append(this.bGForce[2]); } else { builder.Append(0); }
				}
				else
				{
					builder.Append("\r\n        X : "); if (bGForce2.Length > 0) { builder.Append(this.bGForce2[0]); } else { builder.Append(0); }
					builder.Append("\r\n        Y : "); if (bGForce2.Length > 1) { builder.Append(this.bGForce2[1]); } else { builder.Append(0); }
					builder.Append("\r\n        Z : "); if (bGForce2.Length > 2) { builder.Append(this.bGForce2[2]); } else { builder.Append(0); }
					builder.Append("\r\n        Temp : "); if (bGForce2.Length > 3) { builder.Append(this.bGForce2[3]); } else { builder.Append(0); }
				}

				builder.Append("\r\n    External IO : ");
				builder.Append("\r\n        1 : "); if (bExternalIO.Length > 0) { builder.Append(this.bExternalIO[0]); } else { builder.Append(0); }
				builder.Append("\r\n        2 : "); if (bExternalIO.Length > 1) { builder.Append(this.bExternalIO[1]); } else { builder.Append(0); }
				builder.Append("\r\n        3 : "); if (bExternalIO.Length > 2) { builder.Append(this.bExternalIO[2]); } else { builder.Append(0); }
				builder.Append("\r\n        4 : "); if (bExternalIO.Length > 3) { builder.Append(this.bExternalIO[3]); } else { builder.Append(0); }
				builder.Append("\r\n    SPN Codes : "); builder.Append(this.Count);
				builder.Append("\r\n    Swipe Card : ");
				builder.Append("\r\n        Seg Ver : "); builder.Append(_cardSegmentVersion);
				builder.Append("\r\n        Driver Name : "); builder.Append(_driverName);
				builder.Append("\r\n        Login ID : "); builder.Append(_driverLogin);
				builder.Append("\r\n        Pin Number : "); builder.Append(_driverPin);
				builder.Append("\r\n        Card Type : "); builder.Append(_cardType);
				builder.Append("\r\n        Card ID : ");
				if (_cardId == null)
					builder.Append("null");
				else
					builder.Append(BitConverter.ToString(_cardId));

				if (oValues != null)
				{
					for (int X = 0; X < oValues.Length; X++)
					{
						builder.Append(oValues[X].ToString());
					}
				}
				builder.Append("\r\n    Speed Decimal Places : ");
				builder.Append("\r\n        Speed : "); builder.Append(_speedDecimalPlace);
				builder.Append("\r\n        Max Speed : "); builder.Append(_maxSpeedDecimalPlace);
			}
			catch (System.Exception ex)
			{
				builder.Append("\r\nError Decoding GPECMAdvanced Packet : " + ex.Message);
			}
			return builder.ToString();
		}

		public float SpeedAccuracyDecimalPlaces
		{
			get
			{
				float ret = 0;
				if (_speedAccuracyDP == 2)
				{
					ret = (((float)((int)_speedDecimalPlace)) / ((float)100.0));
				}
				else
				{
					ret = (((float)((int)_speedDecimalPlace)) / ((float)10.0));
				}
				return ret;
			}
		}
		public float MaxSpeedAccuracyDecimalPlaces
		{
			get
			{
				float ret = 0;
				if (_speedAccuracyDP == 2)
				{
					ret = (((float)((int)_maxSpeedDecimalPlace)) / ((float)100.0));
				}
				else
				{
					ret = (((float)((int)_maxSpeedDecimalPlace)) / ((float)10.0));
				}
				return ret;
			}
		}
		public int Length { get { return _iLengthRead; } }

		public void SetSpeedData(float speed, float maxSpeed)
		{
			//set flag
			iECMFlags |= (ushort)EMCAdvancePacketIncludes.SPEEDDATA;
			//set the first decimal place
			_speedDecimalPlace = (byte)(Math.Floor(speed * 10) % 10);
			_maxSpeedDecimalPlace = (byte)(Math.Floor(maxSpeed * 10) % 10);
		}
		#endregion
		#region Get SpeedZone, RPM Zone, G-Force, Extended IO values and GPS signal accuracy information
        /// <summary>
        /// The flags to indicate the segements contained in this packet.
        /// </summary>
        public ushort ECMFlags
        {
            get
            {
                return iECMFlags;
            }
            set
            {
                iECMFlags = value;
            }
        }
        /// <summary>
        /// This method allows the user to specify a certain content flag be set
        /// </summary>
        /// <param name="contentFlag"></param>
        public void SetContentFlag(EMCAdvancePacketIncludes contentFlag)
        {
            try
            {
                iECMFlags |= ((ushort)contentFlag);
            }
            catch (System.Exception ex)
            {
            }
        }

        /// <summary>
        /// method used for 3026 that have engine hours and fuel in the summary data
        /// </summary>
        /// <param name="engineSummaryData"></param>
        public void SetEngineSummaryData(GPEngineSummaryData engineSummaryData)
        {
            _engineSummaryData = engineSummaryData;
        }

		#region Speed Zones
		public int SpeedZone1()
		{
			return ((int)bSpeedZones[0]) * 10;
		}
		public int SpeedZone2()
		{
			return ((int)bSpeedZones[1]) * 10;
		}
		public int SpeedZone3()
		{
			return ((int)bSpeedZones[2]) * 10;
		}
		public int SpeedZone4()
		{
			return ((int)bSpeedZones[3]) * 10;
		}
        /// <summary>
        /// Allows user to set speed zone values
        /// </summary>
        /// <param name="zone1"></param>
        /// <param name="zone2"></param>
        /// <param name="zone3"></param>
        /// <param name="zone4"></param>
        public void SetSpeedZones(int zone1, int zone2, int zone3, int zone4)
        {
            try
            {
                bSpeedZones = new byte[4];
                bSpeedZones[0] = (byte)(zone1 / 10);
                bSpeedZones[1] = (byte)(zone2 / 10);
                bSpeedZones[2] = (byte)(zone3 / 10);
                bSpeedZones[3] = (byte)(zone4 / 10);
                // Set the flag to indicate this data is included
                iECMFlags |= ((ushort)EMCAdvancePacketIncludes.SPEEDZONES);
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + string.Format(".SetSpeedZones(int zone1 = {0}, int zone2 = {1}, int zone3 = {2}, int zone4 = {3})", zone1, zone2, zone3, zone4), ex);
            }
        }
		#endregion
		#region RPM Zones
		public int RPMZone1()
		{
			return ((int)bRPMZones[0]) * 10;
		}
		public int RPMZone2()
		{
			return ((int)bRPMZones[1]) * 10;
		}
		public int RPMZone3()
		{
			return ((int)bRPMZones[2]) * 10;
		}
		public int RPMZone4()
		{
			return ((int)bRPMZones[3]) * 10;
		}
        /// <summary>
        /// Allows user to set RPM zone values
        /// </summary>
        /// <param name="zone1"></param>
        /// <param name="zone2"></param>
        /// <param name="zone3"></param>
        /// <param name="zone4"></param>
        public void SetRPMZones(int zone1, int zone2, int zone3, int zone4)
        {
            try
            {
                bRPMZones = new byte[4];
                bRPMZones[0] = (byte) (zone1 / 10);
                bRPMZones[1] = (byte)(zone2 / 10);
                bRPMZones[2] = (byte)(zone3 / 10);
                bRPMZones[3] = (byte)(zone4 / 10);
                // Set the flag to indicate this data is included
                iECMFlags |= ((ushort)EMCAdvancePacketIncludes.RPMZONE);

            }
            catch (System.Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + string.Format(".SetRPMZones(int zone1 = {0}, int zone2 = {1}, int zone3 = {2}, int zone4 = {3})", zone1, zone2, zone3, zone4), ex);
            }
        }
		#endregion
		#region G-Force Readings
		public double GForceFront()
		{
			if (bGForce2 != null)
			{
				if (bGForce2[0] < 0)
					return bGForce2[0] * -1;
				else
					return 0;
			}
			else
				return Convert.ToDouble((int)bGForce[0]) * Convert.ToDouble(0.02);
		}
		public double GForceBack()
		{
			if (bGForce2 != null)
			{
				if (bGForce2[0] > 0)
					return bGForce2[0];
				else
					return 0;
			}
			else
				return Convert.ToDouble((int)bGForce[1]) * Convert.ToDouble(0.02);
		}
		public double GForceLeftRight()
		{
			if (bGForce2 != null)
			{
				if (bGForce2[1] < 0)
					return bGForce2[1] * -1;
				else
					return bGForce2[1];
			}
			else
			{
				return Convert.ToDouble((int)bGForce[2]) * Convert.ToDouble(0.02);
			}
		}
		public double GForceZAxis()
		{
			if (bGForce2 != null)
				return bGForce2[2];
			else
				return 0;
		}
		public double GForceTemperature()
		{
			if (bGForce2 != null)
				return bGForce2[3];
			else
				return 0;
		}
        /// <summary>
        /// This allows the user to specify GForce values
        /// The gforceFront and gforceBack are mutually exclusive, if gforceFront is > 0, gforceBack = 0
        /// </summary>
        /// <param name="gforceFront"></param>
        /// <param name="gforceBack"></param>
        /// <param name="gforceLeftRight"></param>
        /// <param name="gforceZAxis"></param>
        /// <param name="Temperature"></param>
        public void SetGForceValues(double gforceFront, double gforceBack, double gforceLeftRight, double gforceZAxis, double temperature)
        {
            try
            {
                bGForce2 = new double[4];
                if (gforceFront > 0)
                    bGForce2[0] = gforceFront;
                else
                    bGForce2[0] = gforceBack * -1.0;
                bGForce2[1] = gforceLeftRight;
                bGForce2[2] = gforceZAxis;
                bGForce2[3] = temperature;
                // Set the flag to indicate this data is included
                iECMFlags |= ((ushort)EMCAdvancePacketIncludes.GFORCE_EXT);
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + string.Format(".SetGForceValues(double gforceFront = {0}, double gforceBack = {1}, double gforceLeftRight = {2}, double gforceZAxis = {3}, double temperature = {4})", gforceFront, gforceBack, gforceLeftRight, gforceZAxis, temperature), ex);
            }
        }

        public bool HasGForce { get; private set; }
        #endregion
		#region Extended IO
		public ushort ExtendedIO1()
		{
			return bExternalIO[0];
		}
		public int ExtendedIO2(bool isConcreteFleet)
		{
            if (isConcreteFleet && bExternalIO[1] > 32768)
            {
                return bExternalIO[1] - 65535;
            }
            else
            {
                return bExternalIO[1];
            }
		}
		public ushort ExtendedIO3()
		{
			return bExternalIO[2];
		}
		public ushort ExtendedIO4()
		{
			return bExternalIO[3];
		}
        /// <summary>
        /// This allows the user to specify extended IO values
        /// </summary>
        /// <param name="io1"></param>
        /// <param name="io2"></param>
        /// <param name="io3"></param>
        /// <param name="io4"></param>
        public void SetExtendedIOValues(ushort io1, ushort io2, ushort io3, ushort io4)
        {
            try
            {
                bExternalIO = new ushort[4];
                bExternalIO[0] = io1;
                bExternalIO[1] = io2;
                bExternalIO[2] = io3;
                bExternalIO[3] = io4;
                // Set the flag to indicate this data is included
                iECMFlags |= ((ushort)EMCAdvancePacketIncludes.EXTERNALIO);
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + string.Format(".SetExtendedIOValues(ushort io1 = {0}, ushort io2 = {1}, ushort io3 = {2}, ushort io4 = {3})", io1, io2, io3, io4), ex);
            }
        }

        #endregion
        #region Satellites In View and HDOP
        public byte SatellitesInView()
		{
			return bSatellites_NoInView;
		}
		public float SatellitesHDOP()
		{
			return (float)((float)bSatellites_HDOP / (float)10.0);
		}
		public short Altitude()
		{
			return bSatellites_Altitude;
		}
		public uint GPSOdometer()
		{
			return iGPSOdometer;
		}
        /// <summary>
        /// This method allows the user to set sat values
        /// </summary>
        /// <param name="satsInView"></param>
        /// <param name="satHDOP"></param>
        /// <param name="altitude"></param>
        /// <param name="gpsOdometer"></param>
        public void SetSatValues(byte satsInView, float satHDOP, short altitude, uint gpsOdometer)
        {
            try
            {
                bSatellites_NoInView = satsInView;
                bSatellites_HDOP = (byte)Convert.ToInt16(satHDOP * (float)10.0);
                bSatellites_Altitude = altitude;
                iGPSOdometer = gpsOdometer;
                iECMFlags |= ((ushort)EMCAdvancePacketIncludes.GPSSATELLITEDATA_EXTODO);
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + string.Format(".SetSatValues(byte satsInView = {0}, float satHDOP = {1}, short altitude = {2}, uint gpsOdometer = {3})", ((int)satsInView), satHDOP, altitude, gpsOdometer), ex);
            }
        }
		#endregion
		#endregion
		#region Card Reader Data
		public byte CardSegmentVersion
		{
			get
			{
				return _cardSegmentVersion;
			}
			set
			{
				_cardSegmentVersion = value;
			}
		}
		public bool DriverCardData
		{
			get
			{
				return _driverCardData;
			}
			set
			{
				_driverCardData = value;
			}
		}
		public ushort DriverPin
		{
			get
			{
				return _driverPin;
			}
			set
			{
				_driverPin = value;
			}
		}
		public uint DriverLogin
		{
			get
			{
				return _driverLogin;
			}
			set
			{
				_driverLogin = value;
			}
		}
		public string DriverName
		{
			get
			{
				return _driverName;
			}
			set
			{
				_driverName = value;
			}
		}
		public byte CardType
		{
			get
			{
				return _cardType;
			}
			set
			{
				_cardType = value;
			}
		}
		public byte[] CardID
		{
			get
			{
				return _cardId;
			}
			set
			{
				_cardId = value;
			}
		}
		public ulong CardIDasLong
		{
			get
			{
				byte[] data = new byte[8];
				int pos = 0;
				foreach (byte b in _cardId)
				{
					data[pos++] = b;
				}
				return BitConverter.ToUInt64(data, 0);
			}
			set
			{
				byte[] data = BitConverter.GetBytes(value);
				int maxByte = -1;
				for (int X = data.Length - 1; X >= 0; X++)
				{
					if (data[X] != (byte)0x00)
					{
						maxByte = X;
						break;
					}
				}
				if (maxByte > 0)
				{
					_cardId = new byte[maxByte];
					for (int X = 0; X < maxByte; X++)
					{
						_cardId[X] = data[X];
					}
				}
			}
		}
		#endregion
		#region Get Derived Values
		private double GetValue(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, double dNumerator, double dDenominator, double dMin, double dMax, double dDefaultReturnValue, ECMValueType oValueType)
		{
			return GetValue(oSourceType, iSPN_ID, dNumerator, dDenominator, dMin, dMax, dDefaultReturnValue, oValueType, 0);
		}
		private double GetValue(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, double dNumerator, double dDenominator, double dMin, double dMax, double dDefaultReturnValue, ECMValueType oValueType, double dOffsetValue)
		{
			return GetValue(oSourceType, iSPN_ID, dNumerator, dDenominator, dMin, dMax, dDefaultReturnValue, oValueType, dOffsetValue, dDefaultReturnValue);
		}
		private double GetValue(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, double dNumerator, double dDenominator, double dMin, double dMax, double dDefaultReturnValue, ECMValueType oValueType, double dOffsetValue, double dNotValidReturnValue)
		{
			bool bMatched = false;
			return GetValue(oSourceType, iSPN_ID, dNumerator, dDenominator, dMin, dMax, dDefaultReturnValue, oValueType, dOffsetValue, dDefaultReturnValue, ref bMatched);
		}
		private double GetValue(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, double dNumerator, double dDenominator, double dMin, double dMax, double dDefaultReturnValue, ECMValueType oValueType, double dOffsetValue, double dNotValidReturnValue, ref bool bMatched)
		{
			try
			{
				bMatched = false;
				if (oValues != null)
				{
					for (int X = 0; X < oValues.Length; X++)
					{
						if (oValues[X].SPN_ID == iSPN_ID && oValues[X].SourceType == oSourceType)
						{
							bMatched = true;
							if (oValueType == 0x00)
							{
								if ((oValues[X].Flags & (int)0x01) != (int)0x01 && (oValues[X].Flags & (int)0x02) != (int)0x02 && (oValues[X].Flags & (int)0x04) != (int)0x04 && (oValues[X].Flags & (int)0x80) != (int)0x80)
								{
									// No other flags are set, so this is a current value.
									if (!oValues[X].ValueCalculated)
										return oValues[X].CalculateValue(dNumerator, dDenominator, dMin, dMax, dDefaultReturnValue, dOffsetValue, dNotValidReturnValue);
									else
										return oValues[X].DerivedValue;
								}
							}
							else
							{
								if ((oValues[X].Flags & (byte)oValueType) == (int)oValueType)
								{
									if (!oValues[X].ValueCalculated)
										return oValues[X].CalculateValue(dNumerator, dDenominator, dMin, dMax, dDefaultReturnValue, dOffsetValue, dNotValidReturnValue);
									else
										return oValues[X].DerivedValue;
								}
							}
						}
					}
				}
			}
			catch (System.Exception)
			{
			}
			return dDefaultReturnValue;
		}

		public bool SpeedAccuracyValuesSet
		{
			get
			{
				return _speedAccuracyValuesSet;
			}
		}
		public byte CreateSpeedSPNItem(byte cSpeed, bool isMaxSpeed)
		{
			byte ret = 0;
			int speed = (int)cSpeed;
			double gpsSpeed = Convert.ToDouble(cSpeed);
			double divisor = 10;
			double oneHundred = 100;
			double convertToKMH = 1.852;
			try
			{
				ret = (byte)(speed);
				if(speed > 0)
				{
					if (_speedAccuracyDP == 2)
					{
						if (isMaxSpeed)
							gpsSpeed += Convert.ToDouble((int)_maxSpeedDecimalPlace) / oneHundred;
						else
							gpsSpeed += Convert.ToDouble((int)_speedDecimalPlace) / oneHundred;
					}
					else
					{
						if (isMaxSpeed)
							gpsSpeed += Convert.ToDouble((int)_maxSpeedDecimalPlace) / divisor;
						else
							gpsSpeed += Convert.ToDouble((int)_speedDecimalPlace) / divisor;
					}
				}

				if (gpsSpeed > 0)
				{
					gpsSpeed = (gpsSpeed * convertToKMH);
					// Get the raw bytes as a 4 byte int (km/h * 100)
					byte[] bytes = BitConverter.GetBytes(Convert.ToInt32(Math.Round(gpsSpeed * oneHundred)));
					GPECMAdvancedItem item = new GPECMAdvancedItem();
					if (isMaxSpeed)
						item.SPN_ID = 6;
					else
						item.SPN_ID = 5;
					item.SourceType = GPECMAdvancedItem.SourceTypes.Common;
					item.DerivedValue = gpsSpeed;
					if (isMaxSpeed)
						item.Flags = 0x02;
					item.RawData = bytes;
					if (oValues == null)
					{
						oValues = new GPECMAdvancedItem[1];
					}
					else
					{
						int len = oValues.Length + 1;
						GPECMAdvancedItem[] oTemp = new GPECMAdvancedItem[len];
						for (int X = 0; X < oValues.Length; X++)
							oTemp[X] = oValues[X];
						oValues = new GPECMAdvancedItem[len];
						len--;
						for (int X = 0; X < len; X++)
							oValues[X] = oTemp[X];
					}
					oValues[oValues.Length - 1] = item;
				}
			}
			catch (System.Exception)
			{
				ret = cSpeed;
			}
			return ret;
		}


		private short GetStateSecondsActive(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, short iDefaultReturnValue, bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			return GetStateSecondsActive(oSourceType, iSPN_ID, iDefaultReturnValue, bReturnDefaultIfNotValid, ref bMatched);
		}
		private short GetStateSecondsActive(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, short iDefaultReturnValue, bool bReturnDefaultIfNotValid, ref bool bMatched)
		{
			try
			{
				if (oValues != null)
				{
					for (int X = 0; X < oValues.Length; X++)
					{
						if (oValues[X].SPN_ID == iSPN_ID && oValues[X].SourceType == oSourceType)
						{
							bMatched = true;
							if (oValues[X].StateAvailable)
								return oValues[X].StateSecondsActive;
							else
								return bReturnDefaultIfNotValid ? iDefaultReturnValue : short.MaxValue;
						}
					}
				}
			}
			catch (System.Exception)
			{
			}
			return iDefaultReturnValue;
		}

		private short GetStateChangeCount(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, short iDefaultReturnValue, bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			return GetStateChangeCount(oSourceType, iSPN_ID, iDefaultReturnValue, bReturnDefaultIfNotValid, ref bMatched);
		}
		private short GetStateChangeCount(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, short iDefaultReturnValue, bool bReturnDefaultIfNotValid, ref bool bMatched)
		{
			try
			{
				if (oValues != null)
				{
					for (int X = 0; X < oValues.Length; X++)
					{
						if (oValues[X].SPN_ID == iSPN_ID && oValues[X].SourceType == oSourceType)
						{
							bMatched = true;
							if (oValues[X].StateAvailable)
								return oValues[X].StateChangeCount;
							else
								return bReturnDefaultIfNotValid ? iDefaultReturnValue : short.MaxValue;
						}
					}
				}
			}
			catch (System.Exception)
			{
			}
			return iDefaultReturnValue;
		}

		private byte GetCurrentStateFlag(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, byte bDefaultReturnValue, bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			return GetCurrentStateFlag(oSourceType, iSPN_ID, bDefaultReturnValue, bReturnDefaultIfNotValid, ref bMatched);
		}
		private byte GetCurrentStateFlag(GPECMAdvancedItem.SourceTypes oSourceType, int iSPN_ID, byte bDefaultReturnValue, bool bReturnDefaultIfNotValid, ref bool bMatched)
		{
			try
			{
				if (oValues != null)
				{
					for (int X = 0; X < oValues.Length; X++)
					{
						if (oValues[X].SPN_ID == iSPN_ID && oValues[X].SourceType == oSourceType)
						{
							bMatched = true;
							if (oValues[X].StateAvailable)
								return oValues[X].CurrentState;
							else
								return bReturnDefaultIfNotValid ? bDefaultReturnValue : byte.MaxValue;
						}
					}
				}
			}
			catch (System.Exception)
			{
			}
			return bDefaultReturnValue;
		}

		public byte AcceleratorPedalPosition() { return AcceleratorPedalPosition(true); }
		public byte AcceleratorPedalPosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 51, 2, 5, 0, 100, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue)));
		}

		public byte AcceleratorPedalAvgPosition() { return AcceleratorPedalAvgPosition(true); }
		public byte AcceleratorPedalAvgPosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 51, 2, 5, 0, 100, 0, ECMValueType.AverageValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue)));
		}

		public byte AcceleratorPedalMaxPosition() { return AcceleratorPedalMaxPosition(true); }
		public byte AcceleratorPedalMaxPosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 51, 2, 5, 0, 100, 0, ECMValueType.MaxValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue)));
		}

		public int ClutchSeconds() { return ClutchSeconds(true); }
		public int ClutchSeconds(bool bReturnDefaultIfNotValid)
		{
			return GetStateSecondsActive(GPECMAdvancedItem.SourceTypes.J1939, 598, 0, bReturnDefaultIfNotValid);
		}

		public int ClutchPresses() { return ClutchPresses(true); }
		public int ClutchPresses(bool bReturnDefaultIfNotValid)
		{
			return GetStateChangeCount(GPECMAdvancedItem.SourceTypes.J1939, 598, 0, bReturnDefaultIfNotValid);
		}

		public byte ClutchState() { return ClutchState(true); }
		public byte ClutchState(bool bReturnDefaultIfNotValid)
		{
			return GetCurrentStateFlag(GPECMAdvancedItem.SourceTypes.J1939, 598, 0, bReturnDefaultIfNotValid);
		}

		public short CruiseSeconds() { return CruiseSeconds(true); }
		public short CruiseSeconds(bool bReturnDefaultIfNotValid)
		{
			return GetStateSecondsActive(GPECMAdvancedItem.SourceTypes.J1939, 595, 0, bReturnDefaultIfNotValid);
		}

		public byte CruiseState() { return CruiseState(true); }
		public byte CruiseState(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetCurrentStateFlag(GPECMAdvancedItem.SourceTypes.J1939, 595, 0, bReturnDefaultIfNotValid));
		}

		public byte EngineThrottlePosition() { return EngineThrottlePosition(true); }
		public byte EngineThrottlePosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 91, 2, 5, 0, 100, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue)));
		}

		public byte EngineThrottleAvgPosition() { return EngineThrottleAvgPosition(true); }
		public byte EngineThrottleAvgPosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 91, 2, 5, 0, 100, 0, ECMValueType.AverageValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue)));
		}

		public byte EngineThrottleMaxPosition() { return EngineThrottleMaxPosition(true); }
		public byte EngineThrottleMaxPosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 91, 2, 5, 0, 100, 0, ECMValueType.MaxValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue)));
		}

		public byte FuelLevel() { return FuelLevel(true); }
		public byte FuelLevel(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			byte bRet = Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 96, 2, 5, 0, 100, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue), ref bMatched));
			if (!bMatched)
				bRet = Convert.ToByte(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 96, 1, 2, 0, 100, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(byte.MaxValue), ref bMatched));
			return bRet;
		}

		public uint Odometer() { return Odometer(true); }
		public uint Odometer(bool bReturnDefaultIfNotValid)
		{
			uint iOdo = 0;
			bool bMatched = false;
			//odo is divided by an extra 1000, so that the value is stored in km instead of m
            iOdo = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 917, 5, 1000, 0, 21055400000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            if (!bMatched)
            {
                iOdo = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 245, 161, 1000, 0, 691208000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            }
            if (!bMatched && _engineSummaryData != null)
            {
                iOdo = (uint)_engineSummaryData.iOdometer;
            }
            return iOdo;
		}

		public uint TotalEngHours() { return TotalEngHours(true); }
		public uint TotalEngHours(bool bReturnDefaultIfNotValid)
		{
			uint iRet = 0;
			bool bMatched = false;
			iRet = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 247, 1, 20, 0, 210554000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            if (!bMatched)
            {
                iRet = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 247, 0.05, 1, 0, 210554000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            }
            if (!bMatched && _engineSummaryData != null)
            {
                iRet = (uint)_engineSummaryData.iTotalEngineHours;
            }
                return iRet;
		}

		public uint TotalFuelUsed() { return TotalFuelUsed(true); }
		public uint TotalFuelUsed(bool bReturnDefaultIfNotValid)
		{
			uint iRet = 0;
			bool bMatched = false;
			iRet = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 250, 1, 2, 0, 2105540000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            if (!bMatched)
            {
                iRet = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 250, 0.473, 1, 0, 2032280000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            }
            if (!bMatched && _engineSummaryData != null)
            {
                iRet = (uint)_engineSummaryData.iTotalFuel;
            }
            return iRet;
		}

		public uint TripFuel() { return TripFuel(true); }
		public uint TripFuel(bool bReturnDefaultIfNotValid)
		{
            uint iRet = 0;
            bool bMatched = false;
            iRet = Convert.ToUInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 182, 1, 2, 0, 2105540000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            if (!bMatched && _engineSummaryData != null)
            {
                iRet = (uint)_engineSummaryData.iTripFuel;
            }
            return iRet;
        }

        public double FuelEconomy() { return FuelEconomy(true); }
		public double FuelEconomy(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
            double dRet = GetValue(GPECMAdvancedItem.SourceTypes.J1939, 185, 0.001953, 1, 0, 125.5, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : double.MaxValue, ref bMatched);
            if (!bMatched)
            {
                dRet = GetValue(GPECMAdvancedItem.SourceTypes.J1708, 185, 0.00166072, 1, 0, 108.835, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched);
            }
            if (!bMatched && _engineSummaryData != null)
            {
                dRet = _engineSummaryData.fFuelEconomy;
            }
            return dRet;
		}

		public int MaxEngCoolTemp() { return MaxEngCoolTemp(true); }
		public int MaxEngCoolTemp(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			int iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 110, 1, 1, -40, 210, 0, ECMValueType.CurrentValue, -40, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue), ref bMatched));
			if (!bMatched)
			{
                iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 110, 1, 1, 0, 250, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
			}
            if (!bMatched)
            {
                iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1979Mode1, 5, 1, 1, -40, 210, 0, ECMValueType.CurrentValue, -40, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue)));
            }
            return iRet;
		}

		public int MaxEngOilTemp() { return MaxEngOilTemp(true); }
		public int MaxEngOilTemp(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
            int iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 175, 0.03125, 1, -273, 1735.97, 0, ECMValueType.CurrentValue, -273, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue), ref bMatched));
			if (!bMatched)
				iRet = Convert.ToInt32(ConvertFahrenheitToCelcius(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 175, 1, 4, -8192, 8191.75, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue)), true));
			return iRet;
		}

		public int MinOilPressure() { return MinOilPressure(true); }
		public int MinOilPressure(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			int iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 100, 4, 1, 0, 1000, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue), ref bMatched));
			if (!bMatched)
				iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 100, 3.45, 1, 0, 879, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue)));
			return iRet;
		}

		public int GearPosition() { return GearPosition(true); }
		public int GearPosition(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 523, 1, 1, -125, 125, 0, ECMValueType.CurrentValue, -125, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue)));
		}

		public int MaxEngSpeed() { return MaxEngSpeed(true); }
		public int MaxEngSpeed(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
            int iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 190, 1, 8, 0, 8031.88, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue), ref bMatched));
			if (!bMatched)
                iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 190, 1, 4, 0, 16383.8, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue), ref bMatched));
            if (!bMatched)
                iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1979Mode1, 12, 1, 8, 0, 8031.88, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue)));
            return iRet;
		}

		public int BrakeHits() { return BrakeHits(true); }
		public int BrakeHits(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			int iRet = GetStateChangeCount(GPECMAdvancedItem.SourceTypes.Common, 4, 0, bReturnDefaultIfNotValid, ref bMatched);
			if (!bMatched)
				iRet = GetStateChangeCount(GPECMAdvancedItem.SourceTypes.J1939, 597, 0, bReturnDefaultIfNotValid, ref bMatched);
			if (!bMatched)
				iRet = GetStateChangeCount(GPECMAdvancedItem.SourceTypes.J1708, 65, 0, bReturnDefaultIfNotValid, ref bMatched);
			return iRet;
		}

		public int BatteryVolts() { return BatteryVolts(true); }
		public int BatteryVolts(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			int iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 168, 5, 100, 0, 3212.75, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue), ref bMatched));
			if (!bMatched)
                iRet = Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 168, 5, 100, 0, 3212.75, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(uint.MaxValue)));
			return iRet;
		}

		public int BrakeUsageSeconds() { return BrakeUsageSeconds(true); }
		public int BrakeUsageSeconds(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
			int iRet = GetStateSecondsActive(GPECMAdvancedItem.SourceTypes.Common, 4, 0, bReturnDefaultIfNotValid, ref bMatched);
			if (!bMatched)
				iRet = GetStateSecondsActive(GPECMAdvancedItem.SourceTypes.J1939, 597, 0, bReturnDefaultIfNotValid, ref bMatched);
			if (!bMatched)
				iRet = GetStateSecondsActive(GPECMAdvancedItem.SourceTypes.J1708, 65, 0, bReturnDefaultIfNotValid);
			return iRet;
		}

		public short DriverDemandTorqueMax() { return DriverDemandTorqueMax(true); }
		public short DriverDemandTorqueMax(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 512, 1, 1, -125, 125, 0, ECMValueType.MaxValue, -125, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue)));
		}

		public short DriverDemandTorqueAvg() { return DriverDemandTorqueAvg(true); }
		public short DriverDemandTorqueAvg(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 512, 1, 1, -125, 125, 0, ECMValueType.AverageValue, -125, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue)));
		}

		public short EngineTorqueMax() { return EngineTorqueMax(true); }
		public short EngineTorqueMax(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 513, 1, 1, -125, 125, 0, ECMValueType.MaxValue, -125, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue)));
		}

		public short EngineTorqueAvg() { return EngineTorqueAvg(true); }
		public short EngineTorqueAvg(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 513, 1, 1, -125, 125, 0, ECMValueType.AverageValue, -125, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue)));
		}

		public short ECMVehicleSpeed() { return ECMVehicleSpeed(true); }
		public short ECMVehicleSpeed(bool bReturnDefaultIfNotValid)
		{
			bool bMatched = false;
            short iRet = Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 84, 0.003906, 1, 0, 250.996, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue), ref bMatched));
			if (!bMatched)
                iRet = Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1708, 84, 0.805, 1, 0, 205.2, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue), ref bMatched));
            if (!bMatched)
                iRet = Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1979Mode1, 13, 0.003906, 1, 0, 250.996, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue)));
            return iRet;
		}

		public short CoolantLevel() { return CoolantLevel(true); }
		public short CoolantLevel(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt16(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 111, 4, 10, 0, 100, 0, ECMValueType.CurrentValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(short.MaxValue)));
		}

		public double TurboBoostMax() { return TurboBoostMax(true); }
		public double TurboBoostMax(bool bReturnDefaultIfNotValid)
		{
			return GetValue(GPECMAdvancedItem.SourceTypes.J1939, 102, 2, 1, 0, 500, 0, ECMValueType.MaxValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(double.MaxValue));
		}

		public double TurboBoostAvg() { return TurboBoostAvg(true); }
		public double TurboBoostAvg(bool bReturnDefaultIfNotValid)
		{
			return GetValue(GPECMAdvancedItem.SourceTypes.J1939, 102, 2, 1, 0, 500, 0, ECMValueType.AverageValue, 0, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(double.MaxValue));
		}

		public double ExhaustGasTempAvg() { return ExhaustGasTempAvg(true); }
		public double ExhaustGasTempAvg(bool bReturnDefaultIfNotValid)
		{
            return GetValue(GPECMAdvancedItem.SourceTypes.J1939, 173, 0.03125, 1, -273, 273, 0, ECMValueType.AverageValue, -273, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(double.MaxValue));
		}

		public int ServiceDistance() { return ServiceDistance(true); }
		public int ServiceDistance(bool bReturnDefaultIfNotValid)
		{
			return Convert.ToInt32(GetValue(GPECMAdvancedItem.SourceTypes.J1939, 914, 5, 1, -160635, 160640, 0, ECMValueType.CurrentValue, -160635, bReturnDefaultIfNotValid ? 0 : Convert.ToDouble(int.MaxValue)));
		}

		private double ConvertFahrenheitToCelcius(double dFahrenheit, bool bRescale)
		{
			double dCelcius = 0;
			double dF1 = 32;
			double dF2 = (double)1.8;

			try
			{
				if (bRescale)
					dCelcius = (dFahrenheit - dF1) / dF2;
				else
					dCelcius = dFahrenheit / dF2;
			}
			catch (System.Exception)
			{
				dCelcius = 0;
			}
			return dCelcius;
		}
		#endregion
	}
}

using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// GPTransportExtraDetails Packet Structure
	/// 
	///  Type																						Size
	///  Can Bus Odometer																	4 bytes
	///  Total Fuel of vehicle																4 bytes
	///  Fuel Economy																			2 bytes
	///  Brake usage in seconds															2 bytes
	///  Speed Zone 1 Time in 10s intervals										1 byte
	///  Speed Zone 2 Time in 10s intervals									1 byte
	///  Speed Zone 3 Time in 10s intervals									1 byte
	///  Speed Zone 4 Time in 10s intervals									1 byte
	///  Engine RPM Speed Zone 1 Time in 10s intervals				1 byte
	///  Engine RPM Speed Zone 2 Time in 10s intervals				1 byte
	///  Engine RPM Speed Zone 3 Time in 10s intervals				1 byte
	///  Engine RPM Speed Zone 4 Time in 10s intervals				1 byte
	///  Battery Volts																			2 bytes
	/// </summary>

    [Serializable]
    public class GPTransportExtraDetails
	{
		public const int Length = 22;
		public uint iOdometer = 0;
		public int iTotalFuelUsed = 0;
		public int iCANVehicleSpeed = 0;
		public float fFuelEconomy = 0;
		public int iBrakeUsageSeconds = 0;
		public int iSpeedZone1 = 0;
		public int iSpeedZone2 = 0;
		public int iSpeedZone3 = 0;
		public int iSpeedZone4 = 0;
		public int iRPMZone1 = 0;
		public int iRPMZone2 = 0;
		public int iRPMZone3 = 0;
		public int iRPMZone4 = 0;
		public int iEngineHours = 0;
		public int iBatteryVolts = 0;

		public GPTransportExtraDetails()
		{
		}

		public GPTransportExtraDetails CreateCopy()
		{
			GPTransportExtraDetails oGPTransportExtraDetails = new GPTransportExtraDetails();
			oGPTransportExtraDetails.iOdometer = this.iOdometer;
			oGPTransportExtraDetails.iTotalFuelUsed = this.iTotalFuelUsed;
			oGPTransportExtraDetails.iCANVehicleSpeed = this.iCANVehicleSpeed;
			oGPTransportExtraDetails.fFuelEconomy = this.fFuelEconomy;
			oGPTransportExtraDetails.iBrakeUsageSeconds = this.iBrakeUsageSeconds;
			oGPTransportExtraDetails.iSpeedZone1 = this.iSpeedZone1;
			oGPTransportExtraDetails.iSpeedZone2 = this.iSpeedZone2;
			oGPTransportExtraDetails.iSpeedZone3 = this.iSpeedZone3;
			oGPTransportExtraDetails.iSpeedZone4 = this.iSpeedZone4;
			oGPTransportExtraDetails.iRPMZone1 = this.iRPMZone1;
			oGPTransportExtraDetails.iRPMZone2 = this.iRPMZone2;
			oGPTransportExtraDetails.iRPMZone3 = this.iRPMZone3;
			oGPTransportExtraDetails.iRPMZone4 = this.iRPMZone4;
			oGPTransportExtraDetails.iEngineHours = this.iEngineHours;
            oGPTransportExtraDetails.iBatteryVolts = this.iBatteryVolts;
            return oGPTransportExtraDetails;
		}

		public string Decode(byte[] aData, int iPos)
		{
			string sMsg = "";
			int pos = iPos;
			uint uTemp = 0;
			try
			{
				#region Odometer
				if (pos + 4 < aData.Length)
				{
                    uTemp = (uint) (aData[pos++]);
                    uTemp += (uint) (aData[pos++] << 8);
                    uTemp += (uint) (aData[pos++] << 16);
                    uTemp += (uint) (aData[pos++] << 24);
                    if(uTemp < 20000000)
					    iOdometer = uTemp;
					uTemp = 0;
				}
				#endregion
				#region Total Fuel Used
				if (pos + 4 < aData.Length)
				{
                    uTemp = (uint)(aData[pos++]);
                    uTemp += (uint)(aData[pos++] << 8);
                    uTemp += (uint)(aData[pos++] << 16);
                    uTemp += (uint)(aData[pos++] << 24);
                    try
                    {
                        if (uTemp < 20000000)
                            iTotalFuelUsed = Convert.ToInt32(uTemp);
                    }
                    catch (System.Exception)
                    {
                        iTotalFuelUsed = 0;
                    }
					uTemp = 0;
				}
				#endregion
				#region Fuel Economy
				if (pos + 2 < aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					uTemp += Convert.ToUInt32(aData[pos++] << 8);
					fFuelEconomy = Convert.ToSingle(uTemp);
					if (fFuelEconomy > 0)
					{
						if (fFuelEconomy > 100)
						{
							fFuelEconomy = fFuelEconomy / 100;
						}
						else
						{
							fFuelEconomy = fFuelEconomy / 10;
						}
					}
					uTemp = 0;
				}
				#endregion
				#region Brake Usage Seconds
				if (pos + 2 < aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					uTemp += Convert.ToUInt32(aData[pos++] << 8);
					iBrakeUsageSeconds = Convert.ToInt32(uTemp);
					uTemp = 0;
				}
				#endregion
				#region Speed Zones
				#region Seconds in Speed Zone 1
				if (pos + 1 < aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iSpeedZone1 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#region Seconds in Speed Zone 2
				if (pos + 1 < aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iSpeedZone2 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#region Seconds in Speed Zone 3
				if (pos + 1 < aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iSpeedZone3 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#region Seconds in Speed Zone 4
				if (pos + 1 < aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iSpeedZone4 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#endregion
				#region RPM Zones - If this is an ignition off, then the 4 RPM Zone bytes are the total engine hours
				byte[] bConvert = new byte[4];
				#region Seconds in RPM Zone 1
				if (pos + 1 < aData.Length)
				{
					bConvert[0] = aData[pos];
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iRPMZone1 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#region Seconds in RPM Zone 2
				if (pos + 1 < aData.Length)
				{
					bConvert[1] = aData[pos];
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iRPMZone2 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#region Seconds in RPM Zone 3
				if (pos + 1 < aData.Length)
				{
					bConvert[2] = aData[pos];
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iRPMZone3 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				#region Seconds in RPM Zone 4
				if (pos + 1 < aData.Length)
				{
					bConvert[3] = aData[pos];
					uTemp =	Convert.ToUInt32(aData[pos++]);
					iRPMZone4 = Convert.ToInt32(uTemp) * 10;
					uTemp = 0;
				}
				#endregion
				iEngineHours = BitConverter.ToInt32(bConvert, 0);
				#endregion
				#region Battery Volts
				if (pos + 2 <= aData.Length)
				{
					uTemp =	Convert.ToUInt32(aData[pos++]);
					uTemp += Convert.ToUInt32(aData[pos++] << 8);
					iBatteryVolts = Convert.ToInt32(uTemp) / 20;
					uTemp = 0;
				}
				#endregion
			}
			catch(System.Exception ex)
			{
				sMsg = "Error decoding extra transport values : " + ex.Message;
			}
			if (sMsg == "")
				return null;
			else
				return sMsg;
		}

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            // Ensure we've got a decent array:
            if (theData.Length >= (aStartPos + Length))
            {
                bTemp = BitConverter.GetBytes(iOdometer);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                theData[aStartPos++] = bTemp[2];
                theData[aStartPos++] = bTemp[3];
                bTemp = BitConverter.GetBytes(iTotalFuelUsed);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                theData[aStartPos++] = bTemp[2];
                theData[aStartPos++] = bTemp[3];
                bTemp = BitConverter.GetBytes(Convert.ToInt32(fFuelEconomy * 100));
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(iBrakeUsageSeconds);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(Convert.ToInt32(iSpeedZone1 / 10));
                theData[aStartPos++] = bTemp[0];
                bTemp = BitConverter.GetBytes(Convert.ToInt32(iSpeedZone2 / 10));
                theData[aStartPos++] = bTemp[0];
                bTemp = BitConverter.GetBytes(Convert.ToInt32(iSpeedZone3 / 10));
                theData[aStartPos++] = bTemp[0];
                bTemp = BitConverter.GetBytes(Convert.ToInt32(iSpeedZone4 / 10));
                theData[aStartPos++] = bTemp[0];
                if (iEngineHours > 0)
                {
                    bTemp = BitConverter.GetBytes(iEngineHours);
                    theData[aStartPos++] = bTemp[0];
                    theData[aStartPos++] = bTemp[1];
                    theData[aStartPos++] = bTemp[2];
                    theData[aStartPos++] = bTemp[3];
                }
                else
                {
                    bTemp = BitConverter.GetBytes(Convert.ToInt32(iRPMZone1 / 10));
                    theData[aStartPos++] = bTemp[0];
                    bTemp = BitConverter.GetBytes(Convert.ToInt32(iRPMZone2 / 10));
                    theData[aStartPos++] = bTemp[0];
                    bTemp = BitConverter.GetBytes(Convert.ToInt32(iRPMZone3 / 10));
                    theData[aStartPos++] = bTemp[0];
                    bTemp = BitConverter.GetBytes(Convert.ToInt32(iRPMZone4 / 10));
                    theData[aStartPos++] = bTemp[0];
                }
                bTemp = BitConverter.GetBytes(Convert.ToInt32(iBatteryVolts * 20));
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
            }
        }

		public string ToDisplayString()
		{
			return	this.ToString();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nTransport Extra Details :"); 
				builder.Append("\r\n    Odometer : "); builder.Append(iOdometer);
				builder.Append("\r\n    Total Fuel Used : "); builder.Append(iTotalFuelUsed);
				builder.Append("\r\n    Fuel Economy : "); builder.Append(fFuelEconomy);
				builder.Append("\r\n    Brake Usage Seconds : "); builder.Append(iBrakeUsageSeconds);
				builder.Append("\r\n    Speed Zone 1 : "); builder.Append(iSpeedZone1);
				builder.Append("\r\n    Speed Zone 2 : "); builder.Append(iSpeedZone2);
				builder.Append("\r\n    Speed Zone 3 : "); builder.Append(iSpeedZone3);
				builder.Append("\r\n    Speed Zone 4 : "); builder.Append(iSpeedZone4);
				builder.Append("\r\n    RPM Zone 1 : "); builder.Append(iRPMZone1);
				builder.Append("\r\n    RPM Zone 2 : "); builder.Append(iRPMZone2);
				builder.Append("\r\n    RPM Zone 3 : "); builder.Append(iRPMZone3);
				builder.Append("\r\n    RPM Zone 4 : "); builder.Append(iRPMZone4);
				builder.Append("\r\n    Battery Volts : "); builder.Append(iBatteryVolts);
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding GPTrailerTrack : " + ex.Message);
			}
			return builder.ToString();
		}
	}
}

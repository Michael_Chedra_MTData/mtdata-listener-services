using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class DriverPointsRuleRequest : GatewayProtocolPacket
    {
        public const byte DRIVER_POINTS_RULE_REQUEST = 0xde;

        //[PacketVersion][RuleGroupId][VersionMajor][VersionMinor]
        #region private fields
        private byte _packetVersion;
        private int _ruleGroupId;
        private byte _versionMajor;
        private byte _versionMinor;
        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        private string _constructorDecodeError;
        private string _serverTime_DateFormat;
        #endregion
        #region public fields
        public byte PacketVersion
        {
            get { return _packetVersion; }
            set { _packetVersion = value; }
        }
        public int RuleGroupId
        {
            get { return _ruleGroupId; }
            set { _ruleGroupId = value; }
        }
        public byte VersionMajor
        {
            get { return _versionMajor; }
            set { _versionMajor = value; }
        }
        public byte VersionMinor
        {
            get { return _versionMinor; }
            set { _versionMinor = value; }
        }
        public string ConstructorDecodeError { get { return _constructorDecodeError; } }
        #endregion

        public DriverPointsRuleRequest(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }

        public DriverPointsRuleRequest(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);

            _constructorDecodeError = DecodePayload();
            bIsPopulated = (ConstructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = DRIVER_POINTS_RULE_REQUEST;
            _packetVersion = 1;
            _ruleGroupId = 0;
            _versionMajor = 0;
            _versionMinor = 0;
            _constructorDecodeError = null;
        }

        public new DriverPointsRuleRequest CreateCopy()
        {
            DriverPointsRuleRequest data = new DriverPointsRuleRequest(this, _serverTime_DateFormat);
            data.RuleGroupId = _ruleGroupId;
            data.VersionMajor = _versionMajor;
            data.VersionMinor = _versionMinor;
            return data;
        }
        
        public new void Encode(ref byte[] theData)
        {
            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _packetVersion);
           PacketUtilities.WriteToStream(oMS, _ruleGroupId);
           PacketUtilities.WriteToStream(oMS, _versionMajor);
           PacketUtilities.WriteToStream(oMS, _versionMinor);
            mDataField = oMS.ToArray();
            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
        }

        public string Decode(Byte[] data)
        {
            string problem = null;
            if (!bIsPopulated) problem = base.Decode(data, 0);
            if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

            return problem;
        }
        public string DecodePayload()
        {
            try
            {
                MemoryStream oMS = new MemoryStream(mDataField, 0, mDataField.Length);
               PacketUtilities.ReadFromStream(oMS, ref _packetVersion);
               PacketUtilities.ReadFromStream(oMS, ref _ruleGroupId);
               PacketUtilities.ReadFromStream(oMS, ref _versionMajor);
               PacketUtilities.ReadFromStream(oMS, ref _versionMinor);
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }


        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nDriver Points Rule Request :");
                builder.Append("\r\n    Packet Version : "); builder.Append(_packetVersion);
                builder.Append("\r\n    Rule Group ID : "); builder.Append(_ruleGroupId);
                builder.Append("\r\n    Verison Major : "); builder.Append(_versionMajor);
                builder.Append("\r\n    Verison Minor : "); builder.Append(_versionMinor);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Driver Points Rule Request : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

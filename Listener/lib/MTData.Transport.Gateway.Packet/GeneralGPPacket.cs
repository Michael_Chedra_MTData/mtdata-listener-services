using System;
using System.Text;
using System.IO;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// General Packet type decodes
	/// </summary>
    [Serializable]
    public class GeneralGPPacket : GatewayProtocolPacket, IStatusHolder, IPositionHolder
	{	
		#region Packet Types
		/// <summary>
		/// This is the identifier of the GEN_IM_ALIVE_IMEI_ESN message for GPRS or CDMA logon
		/// </summary>

		public const byte DSS_START = 0x20;

        // Unit Tamper Codes:
        public const byte GEN_G_FORCES_SUSTAINED_START      = 0x22;
        public const byte GEN_G_FORCES_SUSTAINED_END        = 0x23;
        public const byte ALARM_LOW_BATTERY = 0x26;
        public const byte ECM_HYSTERESIS_ERROR = 0x27;
        public const byte GFORCE_CALIBRATION = 0x28;
        public const byte GPS_SETTINGS = 0x29;
        public const byte DIAGNOSTIC_LEVEL = 0x2A;
        public const byte DIAGNOSTIC = 0x2B;
        public const byte POSSIBLE_ACCIDENT = 0x2C;
        public const byte TAMPER = 0x2D;
        public const byte GEN_CONFIG_DOWNLOADED = 0x2F;

        public const byte GEN_NO_GPS_PACKET_MASK            = 0x30;
        public const byte GEN_NO_GPS_ANT_OPEN               = 0x30;
        public const byte GEN_NO_GPS_ANT_SHORT              = 0x31;        
        public const byte ALRM_SUSPECT_GPS                  = 0x32;
        public const byte ALRM_IGNITION_DISCONNECT          = 0x33;
        public const byte ALRM_ENGINE_DATA_MISSING          = 0x34;
        public const byte ALRM_SUSPEC_ENGINE_DATA           = 0x35;
        public const byte ALRM_G_FORCE_MISSING              = 0x36;
        public const byte ALRM_G_FORCE_OUT_OF_CALIBRATION   = 0x37;
        public const byte ALRM_CONCETE_SENSOR_ALERT         = 0x38;
        public const byte ALRM_MDT_NOT_LOGGED_IN	        = 0x39;
        public const byte ALRM_MDT_NOT_BEING_USED           = 0x3a;
        public const byte ALRM_UNIT_POWERUP                 = 0x3b;
        public const byte ALRM_VEHICLE_GPS_SPEED_DIFF       = 0x3c;
        public const byte ALRM_ENGINE_DATA_RE_CONNECT       = 0x3d;
        public const byte GEN_IF_CONFIG_RESTART             = 0x3e;
        public const byte GEN_ZERO_BYTES_RECEIVED_RESTART   = 0x3f;

		// Extended IO messages:
		public const byte	EXTENDED_IO_PACKET_MASK			= 0x50;
		public const byte 	EXTENDED_IO_BELOW_THRESHOLD		= 0x51;
		public const byte 	EXTENDED_IO_ABOVE_THRESHOLD		= 0x52;

		// Driver Statuses status
        public const byte   STATUS_VEHICLE_PACKET_MASK      = 0x40;
		public const byte	STATUS_VEHICLE_USAGE_START		= 0x40; 
		public const byte	STATUS_VEHICLE_USAGE_END		= 0x41; 
		public const byte	STATUS_VEHICLE_BREAK_START		= 0x42; 
		public const byte	STATUS_VEHICLE_BREAK_END		= 0x43;
        public const byte   GEN_MDT_STATUS_REPORT           = 0x44;
        
        public const byte   PENDENT_ALARM_TEST              = 0x45;
        public const byte   PENDENT_ALARM                   = 0x46;
        public const byte   PENDENT_ALARM_CLEARED           = 0x47;
        public const byte   PENDENT_ALARM_ACKD_BY_USER      = 0x48;
        public const byte   PENDENT_ALARM_NOT_ACKD_BY_USER  = 0x49;

        public const byte SHELL_REQUEST_REBOOT = 0x4E;
        public const byte SAT_PING = 0x4F;
        public const byte PENDENT_ALARM_CHECK = 0x50;

		public const byte	STATUS_LOGIN					= 0x53; 
		public const byte	STATUS_LOGOUT					= 0x54; 
		public const byte	STATUS_ATLUNCH					= 0x55; 
		public const byte	STATUS_TIPPER_UP				= 0x56; 
		public const byte	STATUS_TIPPER_DOWN				= 0x57; 
		public const byte	STATUS_PRESSURE_SENSOR_LOADED	= 0x58; 
		public const byte	STATUS_PRESSURE_SENSOR_UNLOADED	= 0x59; 
		public const byte	STATUS_JOB_PACKET				= 0x5A;
		public const byte	STATUS_PHONECALL_PACKET			= 0x5B;

		// Global Star Report Types
		public const byte	STATUS_GLOBAL_STAR = 0x5C;

		// Barrel messages for concrete applications:
		public const byte	BARREL_PACKET_MASK				= 0x60;
		public const byte 	BARREL_RPM_UP_TO_SPEED			= 0x61;
		public const byte 	BARREL_RPM_STOPPED				= 0x62;
		public const byte 	BARREL_RPM_REVERSED				= 0x63;
		public const byte 	BARREL_RPM_FORWARD				= 0x64;
		public const byte	BARREL_MIX_STARTED				= 0x65;
		public const byte	BARREL_NOT_MIXED				= 0x66;
		public const byte	BARREL_LEFT_LOADER				= 0x67;
		public const byte	BARREL_ABOVE_ONSITE_SPEED		= 0x68;
		public const byte	BARREL_LOADING_STARTED			= 0x69;
		public const byte	BARREL_CONCRETE_AGE_MINS		= 0x6A;
		public const byte	BARREL_CONCRETE_AGE_ROTATIONS   = 0x6B;
		public const byte	BARREL_DRIVER_ERROR_1			= 0x6E;
		public const byte	BARREL_DRIVER_ERROR_2			= 0x6F;

		// Engine-related packets for the Transport Solution
		public const byte	ENGINE_PACKET_MASK				= 0x70;
		public const byte	ENGINE_OVER_RPM					= 0x71;
		public const byte	ENGINE_OVER_COOLANT_TEMP		= 0x72;
		public const byte	ENGINE_OVER_OIL_TEMP			= 0x73;
		public const byte	ENGINE_UNDER_OIL_PRESSURE_LOW	= 0x74;
		public const byte	ENGINE_UNDER_OIL_PRESSURE_HIGH	= 0x75;
		public const byte	ENGINE_G_FORCES_HIGH			= 0x76;
		// Note 0x78 is APP PING - do not use! :-)
		public const byte	ENGINE_COOLANT_LEVEL_LOW	    = 0x78;
		public const byte	ENGINE_FUEL_ECONOMY_ALERT       = 0x79;
		public const byte	ENGINE_G_FORCE_CALIBRATED_RPT   = 0x7A;
		public const byte	ENGINE_ERROR_CODE				= 0x7F;

		// Extended Transport Messages
		public const byte	ZONE_ALERT_MESSAGES				= 0x80;
		public const byte	OVERSPEED_ZONE_1_START			= 0x80;
		public const byte	OVERSPEED_ZONE_2_START			= 0x81;
		public const byte	OVERSPEED_ZONE_3_START			= 0x82;
		public const byte	OVERSPEED_ZONE_1_END			= 0x83;
		public const byte	OVERSPEED_ZONE_2_END			= 0x84;
		public const byte	OVERSPEED_ZONE_3_END			= 0x85;
		public const byte	ANGEL_GEAR_ALERT_START			= 0x86;
		public const byte	ANGEL_GEAR_ALERT_END			= 0x89;
		public const byte	FATIGUE_REPORT_IGNON			= 0x8a;		//	MB/POD 01.08.07 Fatigue Reporting - K&S
		public const byte	FATIGUE_REPORT_24				= 0x8b;		//	MB/POD 01.08.07 Fatigue Reporting - K&S
        
		// Types of message handled by this class
		public const byte	GEN_PACKET_MASK					= 0x90;
		public const byte	GEN_IM_ALIVE_FULL               = 0x90;
		public const byte	GEN_IM_ALIVE					= 0x91; 
		public const byte	GEN_NOGPS						= 0x92;
		public const byte 	GEN_OVERSPEED					= 0x93;
		public const byte   GEN_EXCESSIVE_IDLE				= 0x94;
		public const byte	GEN_IM_ALIVE_IMEI_ESN			= 0x95;
		public const byte 	GEN_EXCESSIVE_IDLE_END			= 0x96;
		public const byte 	GEN_ALARM						= 0x97;
		public const byte 	GEN_IOONOFF						= 0x98;
		public const byte 	GEN_OUTPUT						= 0x99;
		public const byte 	GEN_NAK							= 0x9A;
		public const byte 	GEN_CLEAR_SENDBUF				= 0x9B;
		public const byte 	GEN_SLEEPING					= 0x9C;
		public const byte 	GEN_STARTUP						= 0x9D;
		public const byte 	GEN_ACK							= 0x9E;	// Simple ack packet - no data
		public const byte 	GEN_RESET						= 0x9F;
		public const byte 	GEN_GFORCEAUTOCALIBRATE         = 0xFE;

		// Set Point Related Message Types
		public const byte SETPT_ARRIVE                      = 0xa1;
		public const byte SETPT_DEPART                      = 0xa2;
		public const byte SETPT_DURATION_OVER               = 0xa3;
		public const byte SETPT_DURATION_UNDER              = 0xa4;
		public const byte SETPT_OUTSIDE_HRS                 = 0xa5;

		// New SPECIAL set points:
		public const byte SETPT_SPECIAL_1_ARRIVE            = 0xa6;
		public const byte SETPT_SPECIAL_1_DEPART            = 0xa7;
		public const byte WAYPT_ARRIVE_DOCK                 = 0xa8;
		public const byte WAYPT_DEPART_DOCK                 = 0xa9;

		public const byte WAYPT_OVER_SPEED                  = 0xaa;
		public const byte WAYPT_ARRIVE_WORKSHOP             = 0xab;
		public const byte WAYPT_DEPART_WORKSHOP             = 0xac;
		public const byte WAYPT_ARRIVE_NOGO                 = 0xad;
		public const byte WAYPT_DEPART_NOGO                 = 0xae;
		public const byte WAYPT_HOLDING_EXPIRY              = 0xaf;		


		public const byte 	STATUS_PACKET_MASK				= 0xC0;
		public const byte 	STATUS_REPORT					= 0xc1;
		public const byte 	STATUS_RAW_GPS					= 0xc2;
		public const byte 	STATUS_POS_REPORT				= 0xc3;
		public const byte 	STATUS_IGNITION_ON				= 0xc4;
		public const byte 	STATUS_IGNITION_OFF				= 0xc5;
		public const byte 	STATUS_IO_PULSES				= 0xc6;
		public const byte	STATUS_RX_TX					= 0xc7; 
		public const byte	STATUS_TRAILER_HITCH			= 0xc9; 
		public const byte	STATUS_TRAILER_DEHITCH		    = 0xca; 
		public const byte	STATUS_ACCIDENT_NEW			    = 0xcb; 
		public const byte	STATUS_DATA_USAGE_ALERT	        = 0xcc;
        public const byte   STATUS_RX_TX_SATTELITE          = 0xcd; 
        public const byte   STATUS_DATA_USAGE_ALERT_SATELLITE = 0xce;

		public const byte	GEN_IM_ALIVE_IMIE				= 0xe0; 
		public const byte	GPS_HISTORY						= 0xe1;
		public const byte 	GEN_TAMPER						= 0xe3; // POD This was 0x95, but MB reused it for IM_ALIVE_IMEI_ESN.
		public const byte	REFRIG_REPORT                   = 0xe4;
        // DriverPointsServerTrackingRuleBroken.DRIVER_POINTS_SERVER_TRACKING_RULE_BROKEN = 0xe5
        public const byte   GEN_ECM_OVERSPEED               = 0xe6;
        public const byte LOW_POWER_MODE = 0xe9;
        public const byte   DRIVER_PERFORMANCE_METRICS      = 0xEA;
        public const byte   DRIVER_DATAACCUMLATOR_METRICS   = 0xEB;
        public const byte   DRIVER_PERSONAL_KMS             = 0xEC;
        public const byte   DRIVER_OVERSPEED_REPORT         = 0xED;

		public const byte 	GEN_NEW_NETWORK_FOUND			= 0xFD;	
		public const byte 	GEN_NEW_IOBOX_SERIALNUMBER		= 0xFD;

        // IAP Messages
	    public const byte   IAP_UPDATE                      = 0xFE;
        #endregion

        public enum DRIVER_OVERSPEED_FLAGS
        {
            None = (byte) 0x00,
            SourceIsGPS = (byte)0x01,
            StartSpeedZone = (byte)0x02,
            EndSpeedZone = (byte)0x04,
            ToleranceBreach = (byte)0x08
        };

		#region Packet Lengths
		public const uint GEN_IM_ALIVE_LENGTH = 13;
		public const uint GEN_IM_ALIVE_TRANSPORT_LENGTH = GEN_IM_ALIVE_LENGTH +2;

		/// <summary>
		/// This indicates the length of the combined GEN_IM_ALIVE_IMEI_ESN for IMEI or CDMA Logon
		/// </summary>
		public const uint GEN_IM_ALIVE_IMEI_ESN_LENGTH = 55;
		public const uint GEN_IM_ALIVE_IMEI_ESN_LENGTH_INERROR = GEN_IM_ALIVE_IMEI_ESN_LENGTH - 19;

		#endregion
		#region Variables
		// Additional members
		// For an IM_ALIVE:
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
		public int iProtocolVer = 0;
		public long lSerialNumber = 0;
		public string sSimNumber = "";
		public int iSoftwareVersion = 0;
		public byte cProgramMajor = (byte) 0x00;
		public int iProgramMinor = 0;
		public int _iProgramMinor = 0;
		public byte _cProgramMajor = (byte) 0x00;
		public int iHardwareVersionNumber = 0;
		public int iSlotNumber = 0;
		public int iPreferedSlotNumber = 0;
		public string sNetworkName = "";
		public int iLoginFlags = 0;
		public long lIOBoxSerialNumber = 0;
		public int iIOBoxSoftwareNumber = 0;
		public byte cIOBoxProgramMajor = (byte) 0x00;
		public byte cIOBoxProgramMinor = (byte) 0x00;
		public int iIOBoxHardwareVersionNumber = 0;
		public long lMDTSerialNumber = 0;
		public int iMDTProgramMajor = 0;
		public int iMDTProgramMinor = 0;
		public int iMDTHardwareVer = 0;
        public sbyte OutputState;
		//	USed to indicate if there was a decode error during construction only.
		public string sConstructorDecodeError = null;
		public DateTime dtGPSTime = DateTime.Now.ToUniversalTime();
		// For a STATUS:
		public GPClock mCurrentClock;
		public GPClock mFixClock;
		public GPPositionFix mFix;
		public GPStatus mStatus;
		public GPDistance mDistance;
		public GPEngineData mEngineData;
		public GPExtendedValues mExtendedValues;
		public GPTransportExtraDetails mTransportExtraDetails;
		public GPTrailerTrack mTrailerTrack;
		public GBVariablePacketExtended59 mExtendedVariableDetails;
        public GPSatteliteDataUsage mSatteliteDataUsage;
        public ConfigDownloaded mConfigDownloaded;
		// For counting the bytes sent and recieved from the units.
		public int iUnitRXBytes = 0;
		public int iUnitTXBytes = 0;
		// For an ENGINE (alert type) message:
		public uint iEngineValue;
		// For a CANNED Message:
		public byte cCannedMessageNumber;
		// For an IO ON/OFF Message:
		public byte cButtonNumber;
		public GPEngineSummaryData mEngineSummaryData;
        public GPECMAdvanced mECMAdvanced;
        public GPUnitStartUpReport mUnitStartupReport;
        // For the driver overspeed report
	    public byte bSpeedZoneID;
        public byte bSpeedZoneFlags;
        public byte bSpeedZoneLimit;

		// For an Input ON/OFF Message:
		public byte cInputNumber;
		// For refrigeration zone
		public byte cRefrigerationZone;
		/// <summary>
		/// This will hold the flash index of the latest accident.
		/// </summary>
		public byte cAccidentIndex;
		// For an IO Pulse count message:
		public uint iPulseCount;
		// For a Barrel Status message:
		public uint uBarrelRevolutionCount;	//Unsigned - a simple count since the last one
		public sbyte scBarrelMaxSpeed;	// Signed to indicate fwd/reverse
		// For a Raw GPS Message:
		public string sRawGPS;
		public cIMIEData mImieData;
        public EcmHysteresisError EcmError;
		#region IMEI and ESN Logon
		public string sIMEIInbound = "";
		public string sCDMAESNInbound = "";
		public Alive_IMEIESN_HardwareType iHardwareTypeInbound = Alive_IMEIESN_HardwareType.Unknown_SerialNumber;	
		public int iHWTypeInbound = 0;

        public short OldReportingFrequency;
        public short NewReportingFrequency;
        public short GFCalibrationReason;
        public float OldGFCalibration1;
        public float OldGFCalibration2;
        public float NewGFCalibration1;
        public float NewGFCalibration2;
        public byte[] GPSSettings;
        public string GPSSettingsString
        {
            get
            {
                if (GPSSettings == null)
                {
                    return null;
                }
                else if (GPSSettings.Length != 7)
                {
                    return "Error Decoding";
                }
                else
                {
                    return string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}", GPSSettings[0], GPSSettings[1], GPSSettings[2], GPSSettings[3], GPSSettings[4]
                        , GPSSettings[5], GPSSettings[6]);
                }
            }
        }
        public short DiagnosticsHardwareType;
        public short DiagnosticsLevel;
        public int Diagnostics;

        public int ServerLoadLevel;
        public int ServerLoadStatusReport;

		/// <summary>
		/// Indicates the hardware type passed in in the GEN_IM_ALIVE_EMEI_ESN packet
		/// </summary>
		public enum Alive_IMEIESN_HardwareType
		{
			Unknown_SerialNumber = 0,
			Platform_3020_IMEI = 1,
			Platform_3021_IMEI = 2,
			Platform_3022_CDMAESN = 3,
			Platform_5070_IMIE = 4,
			Platform_3025_IMEI = 5,
            Platform_3026_IMEI = 6,
            Platform_GlobalStar = 7,
            Platform_3028_IMIE = 8,
            Platform_5080_Serial = 9,
            Platform_5010_Serial = 10,
            Platform_5040_Serial = 11,
            Platform_4000_Serial = 12,
            Android = 13,
            Platform_3029_NextG_Serial = 14,
            Platform_3027_Serial = 15,
            Platform_1035_Serial = 16,
            Platform_1039_Serial = 17,
            Platform_5081_Serial = 18,
            OBD = 19,
            ATrack = 20,
            Platform_5050_Serial = 21,
            Platform_3029_Serial = 22,
            Platform_3033_Serial = 23,
            Platform_2027_Serial = 24,
            Platform_3035_Serial = 25,
            CalAmp = 26,
        }

        /// <summary>
        /// This is the maximum value that hardware type can be.
        /// </summary>
        Alive_IMEIESN_HardwareType iHardwareTypeMax = Alive_IMEIESN_HardwareType.Platform_3035_Serial;

		#endregion

		#region Debug Dump Structures

		/// <summary>
		/// This class identifies that is going on in the debug dump.
		/// </summary>
        [Serializable]
        public class DebugDump_Struct
		{
			public byte		Reason;
			public int		Count;
			public int		Index;
			public ushort	WatchDog;
			public ushort	LogonWatchDog;
			public uint		ProgramCounter;
		}

		public DebugDump_Struct DebugDump = null;

        #endregion

        public int SoftwareBuild;
        public int SoftwareBranch;
        public int SoftwareBuildOptions;        
        #endregion

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append(base.ToDisplayString());
				builder.Append("\r\nGeneral Packet Data : ");
                if (cMsgType == GEN_OUTPUT && OutputState != 0)
                    builder.Append("\r\n    Output State : Output " + Math.Abs(OutputState) + " " + (OutputState < 0?"On":"Off"));
				builder.Append("\r\n    ButtonNumber : "); builder.Append(cButtonNumber);
				builder.Append("\r\n    InputNumber : "); builder.Append(cInputNumber);
				builder.Append("\r\n    AccidentIndex : "); builder.Append(cAccidentIndex);
				builder.Append("\r\n    PulseCount : "); builder.Append(iPulseCount);
				builder.Append("\r\n    BarrelRevolutionCount : "); builder.Append(uBarrelRevolutionCount);
				builder.Append("\r\n    BarrelMaxSpeed : "); builder.Append(scBarrelMaxSpeed);
				builder.Append("\r\n    RawGPS : "); builder.Append(sRawGPS);
				builder.Append("\r\n    HardwareTypeInbound : "); builder.Append(iHardwareTypeInbound);
				builder.Append("\r\n    302x : "); 
				builder.Append("\r\n        Serial Number : "); builder.Append(lSerialNumber);
				builder.Append("\r\n        IMEI : "); builder.Append(sIMEIInbound);
				builder.Append("\r\n        CDMA ESN : "); builder.Append(sCDMAESNInbound);
                builder.Append("\r\n        Slot Number : "); builder.Append((int)iSlotNumber);
                builder.Append("\r\n        Prefered Slot Number : "); builder.Append((int)iPreferedSlotNumber);
                if (mImieData != null)
                {
                    builder.Append("\r\n        HW Type : "); builder.Append(mImieData.i_HardwareType);
                    builder.Append("\r\n        HW Version Number : "); builder.Append(iHardwareVersionNumber);
                    builder.Append("\r\n        SW Version Number : "); builder.Append(mImieData.i_SoftwareVer);
                    builder.Append("\r\n        Program Major : "); builder.Append(mImieData.i_SoftwareVerMajor);
                    builder.Append("\r\n        Program Minor : "); builder.Append(mImieData.i_SoftwareVerMinor);
                    builder.Append("\r\n        Network Name : "); builder.Append(sNetworkName);
                    builder.Append("\r\n        Sim Number : "); builder.Append(sSimNumber);
                    builder.Append("\r\n        Login Flags : "); builder.Append((int)iLoginFlags);
                    builder.Append("\r\n    103x (IO Box) : ");
                    builder.Append("\r\n        Serial Number : "); builder.Append((int)lIOBoxSerialNumber);
                    builder.Append("\r\n        HW Version Number : "); builder.Append(iIOBoxHardwareVersionNumber);
                    builder.Append("\r\n        SW Version Number : "); builder.Append(iIOBoxSoftwareNumber);
                    builder.Append("\r\n        Program Major : "); builder.Append(mImieData.i_IOBoxVerMajor);
                    builder.Append("\r\n        Program Minor : "); builder.Append(mImieData.i_IOBoxVerMinor);
                    builder.Append("\r\n    MDT : ");
                    builder.Append("\r\n        Serial Number : "); builder.Append(lMDTSerialNumber);
                    builder.Append("\r\n        Program Major : "); builder.Append(iMDTProgramMajor);
                    builder.Append("\r\n        Program Minor : "); builder.Append(iMDTProgramMinor);
                }
                else
                {
                    builder.Append("\r\n        HW Version Number : ");
                    builder.Append("\r\n        SW Version Number : ");
                    builder.Append("\r\n        Program Major : ");
                    builder.Append("\r\n        Program Minor : ");
                    builder.Append("\r\n        Network Name : "); builder.Append(sNetworkName);
                    builder.Append("\r\n        Login Flags : "); builder.Append((int)iLoginFlags);
                    builder.Append("\r\n    103x (IO Box) : ");
                    builder.Append("\r\n        Serial Number : "); builder.Append((int)lIOBoxSerialNumber);
                    builder.Append("\r\n        HW Version Number : "); builder.Append(iIOBoxHardwareVersionNumber);
                    builder.Append("\r\n        SW Version Number : "); builder.Append(iIOBoxSoftwareNumber);
                    builder.Append("\r\n        Program Major : "); 
                    builder.Append("\r\n        Program Minor : "); 
                    builder.Append("\r\n    MDT : ");
                    builder.Append("\r\n        Serial Number : "); builder.Append(lMDTSerialNumber);
                    builder.Append("\r\n        Program Major : "); builder.Append(iMDTProgramMajor);
                    builder.Append("\r\n        Program Minor : "); builder.Append(iMDTProgramMinor);
                }

                builder.Append("\r\n    Software Build : "); builder.Append(SoftwareBuild);
                builder.Append("\r\n    Software Branch : "); builder.Append(SoftwareBranch);
                builder.Append("\r\n    Software Build Options : "); builder.Append(SoftwareBuildOptions);

                if (mStatus != null)
				{
					builder.Append(mStatus.ToString());
				}
				builder.Append("\r\nTimes : ");
				builder.Append("\r\n    Server Time : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
				if (mCurrentClock != null)
				{
					if (mCurrentClock.ToString() == " ")
						builder.Append("\r\n    Device Time : No Value");
					else
					{
						builder.Append("\r\n    Device Time : " + mCurrentClock.ToString());
						try
						{
							builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
						}
						catch(System.Exception)
						{
						}
					}
				}
				else
				{
					builder.Append("\r\n    Device Time : No Value");
				}
				if (mFixClock != null)
				{
					if (mFixClock.ToString() == " ")
						builder.Append("\r\n    GPS Time : No Value");
					else
					{
						builder.Append("\r\n    GPS Time : " + mFixClock.ToString());
						try
						{
							builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
						}
						catch(System.Exception)
						{
						}
					}
				}
				else
				{
					builder.Append("\r\n    GPS Time : No Value");
				}
                if (mECMAdvanced != null && (mFix != null || mDistance != null))
                {
                    builder.Append("\r\nAccurate Speed Adjustments :");
                    if(mFix != null)
                    {
                        builder.Append("\r\n    Speed : "); builder.Append(this.mECMAdvanced.CreateSpeedSPNItem(mFix.cSpeed, false));
                    }
                    else
                        builder.Append("\r\n    Speed : 0");
                    if (mDistance != null)
                    {
                        builder.Append("\r\n    Max Speed : "); builder.Append(this.mECMAdvanced.CreateSpeedSPNItem(mDistance.cMaxSpeed, false));
                    }
                    else
                        builder.Append("\r\n    Max Speed : 0");
                }
				if (mFix != null)
				{
					builder.Append(mFix.ToString());
				}
				if (mDistance != null)
				{
					builder.Append(mDistance.ToString());
				}
				if (mExtendedValues != null)
				{
					builder.Append(mExtendedValues.ToString());
				}
				if (mTrailerTrack != null)
				{
					builder.Append(mTrailerTrack.ToString());
				}
				if (mTransportExtraDetails != null)
				{
					builder.Append(mTransportExtraDetails.ToString());
				}
				if (mEngineData != null)
				{
					builder.Append(mEngineData.ToString());
				}
				if (mEngineSummaryData != null)
				{
					builder.Append(mEngineSummaryData.ToString());
				}
                if (mECMAdvanced != null)
                {
                    builder.Append(mECMAdvanced.ToString());
                }
				if (mExtendedVariableDetails != null)
				{
					builder.Append(mExtendedVariableDetails.ToString());
				}
                if (mSatteliteDataUsage != null)
                {
                    builder.Append(mSatteliteDataUsage.ToString());
                }
                if (mUnitStartupReport != null)
                {
                    builder.Append(mUnitStartupReport.ToString());
                }

                if (EcmError != null)
                {
                    builder.Append(EcmError.ToString());
                }

                if (RoadTypeData != null)
                {
                    builder.Append(RoadTypeData.ToString());
                }
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding Transport Extra Details : " + ex.Message);
			}

			return builder.ToString();
		}

		public string[] ToStringArray()
		{
			string[] retString =  {this.ToDisplayString()};
			return retString;
		}

		public GPStatus GetStatus()
		{
			return mStatus;
		}

		public GPPositionFix GetPosition()
		{
			return mFix;
		}

		public GPClock GetFixClock()
		{
			return mFixClock;
		}

		public GeneralGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mImieData = new cIMIEData();
			mCurrentClock = new GPClock("Device", _serverTime_DateFormat); 
			mFixClock = new GPClock("GPS", _serverTime_DateFormat); 
			mFix = new GPPositionFix(); 
			mStatus = new GPStatus(); 
			mDistance = new GPDistance();
			mEngineData = new GPEngineData();
			mExtendedValues = new GPExtendedValues();
			mEngineSummaryData = new GPEngineSummaryData();
			mTransportExtraDetails = new GPTransportExtraDetails();
			mTrailerTrack = new GPTrailerTrack();
            mECMAdvanced = new GPECMAdvanced();
			mExtendedVariableDetails = new GBVariablePacketExtended59();
            mSatteliteDataUsage = new GPSatteliteDataUsage();
            mUnitStartupReport = new GPUnitStartUpReport();
			sRawGPS = "";
			sConstructorDecodeError = null;            
		}

		public GeneralGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mImieData = new cIMIEData();
			mCurrentClock = new GPClock("Device", _serverTime_DateFormat); 
			mFixClock = new GPClock("GPS", _serverTime_DateFormat); 
			mFix = new GPPositionFix(); 
			mStatus = new GPStatus(); 
			mDistance = new GPDistance();
			mEngineData = new GPEngineData();
			mEngineSummaryData = new GPEngineSummaryData();
			mExtendedValues = new GPExtendedValues();
			mTransportExtraDetails = new GPTransportExtraDetails();
			mTrailerTrack = new GPTrailerTrack();
            mECMAdvanced = new GPECMAdvanced();
			mExtendedVariableDetails = new GBVariablePacketExtended59();
            mSatteliteDataUsage = new GPSatteliteDataUsage();
            mUnitStartupReport = new GPUnitStartUpReport();            

			sRawGPS = "";
			sConstructorDecodeError = Decode(p.mDataField);
			bIsPopulated = (sConstructorDecodeError == null);
		}
		
		public new GeneralGPPacket CreateCopy()
		{
			GeneralGPPacket oGP = new GeneralGPPacket(_serverTime_DateFormat);

            oGP.StreetName = this.StreetName;
            oGP.SpeedLimit = this.SpeedLimit;
            oGP.RoadType = this.RoadType;
			oGP.bAckImmediately = this.bAckImmediately;
			oGP.bAckRegardless = this.bAckRegardless;
			oGP.bArchivalData = this.bArchivalData;
			oGP.bIsFileType = this.bIsFileType;
			oGP.bIsPopulated = this.bIsPopulated;
			oGP.bPacketFormat = this.bPacketFormat;
			oGP.bRevertToPrimaryServer = this.bRevertToPrimaryServer;
			oGP.bSendFlash = this.bSendFlash;
			oGP.bUsingSecondaryServer = this.bUsingSecondaryServer;
			oGP.cAckSequence = this.cAckSequence;
			oGP.cButtonNumber = this.cButtonNumber;
			oGP.cCannedMessageNumber = this.cCannedMessageNumber;
			oGP.cFleetId = this.cFleetId;
			oGP.cInputNumber = this.cInputNumber;
			oGP.cAccidentIndex = this.cAccidentIndex;
			oGP.cIOBoxProgramMajor = this.cIOBoxProgramMajor;
			oGP.cIOBoxProgramMinor = this.cIOBoxProgramMinor;
			oGP.cMsgType = this.cMsgType;
			oGP.cOurSequence = this.cOurSequence;
			oGP.cPacketNum = this.cPacketNum;
			oGP.cPacketTotal= this.cPacketTotal;
			oGP.cProgramMajor = this.cProgramMajor;
			oGP.iChecksum = this.iChecksum;
			oGP.iEngineValue = this.iEngineValue;
			oGP.iHardwareTypeInbound = this.iHardwareTypeInbound;
			oGP.iHardwareTypeMax = this.iHardwareTypeMax;
			oGP.iLength = this.iLength;
			oGP.iProgramMinor= this.iProgramMinor;
			oGP.iPulseCount= this.iPulseCount;
			oGP.iTotalLength = this.iTotalLength;
			oGP.iUnitRXBytes = this.iUnitRXBytes;
			oGP.iUnitTXBytes = this.iUnitTXBytes;
			oGP.iVehicleId = this.iVehicleId;
			oGP.lSerialNumber = this.lSerialNumber;

			if(this.mCurrentClock != null) {oGP.mCurrentClock = this.mCurrentClock.CreateCopy();} 
			else {oGP.mCurrentClock = new GPClock("", _serverTime_DateFormat);}
			oGP.mDataField = this.mDataField;
			if(this.mDistance != null) {oGP.mDistance = this.mDistance.CreateCopy();}
			else {oGP.mDistance = new GPDistance();}
			if(this.mEngineData != null) {oGP.mEngineData = this.mEngineData.CreateCopy();}
			else {oGP.mEngineData = new GPEngineData();}
			if(this.mEngineSummaryData != null) {oGP.mEngineSummaryData= this.mEngineSummaryData.CreateCopy();}
			else {oGP.mEngineSummaryData = new GPEngineSummaryData();}
            if (this.mECMAdvanced != null) { oGP.mECMAdvanced = this.mECMAdvanced.CreateCopy(); }
            else { oGP.mECMAdvanced = new GPECMAdvanced(); }
			if(this.mExtendedValues != null) {oGP.mExtendedValues= this.mExtendedValues.CreateCopy();}
			else {oGP.mExtendedValues = new GPExtendedValues();}
			if(this.mFix != null) {oGP.mFix= this.mFix.CreateCopy();}
			else {oGP.mFix = new GPPositionFix();}
			if(this.mFixClock != null) {oGP.mFixClock= this.mFixClock.CreateCopy();}
			else {oGP.mFixClock = new GPClock("", _serverTime_DateFormat);}
			if(this.mImieData != null) {oGP.mImieData= this.mImieData.CreateCopy();}
			else {oGP.mImieData = new cIMIEData();}
			oGP.mRawBytes = this.mRawBytes;
			if(this.mSenderIP != null) {oGP.mSenderIP = new System.Net.IPEndPoint(this.mSenderIP.Address, this.mSenderIP.Port);}
			else {oGP.mSenderIP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse("0.0.0.0"), 0);}
			if(this.mStatus != null) {oGP.mStatus= this.mStatus.CreateCopy();}
			else {oGP.mStatus = new GPStatus();}
			if(this.mTrailerTrack != null) {oGP.mTrailerTrack= this.mTrailerTrack.CreateCopy();}
			else {oGP.mTrailerTrack = new GPTrailerTrack();}

			if(this.mTransportExtraDetails != null) {oGP.mTransportExtraDetails= this.mTransportExtraDetails.CreateCopy();}
			else{oGP.mTransportExtraDetails = new GPTransportExtraDetails();}

			if(this.mExtendedVariableDetails != null) {oGP.mExtendedVariableDetails= this.mExtendedVariableDetails.CreateCopy();}
			else {oGP.mExtendedVariableDetails = new GBVariablePacketExtended59();}

            if (mSatteliteDataUsage != null) { oGP.mSatteliteDataUsage = this.mSatteliteDataUsage.CreateCopy(); }
            else { oGP.mSatteliteDataUsage = new GPSatteliteDataUsage(); }

            if (this.mUnitStartupReport != null) { oGP.mUnitStartupReport = this.mUnitStartupReport.CreateCopy(); }
            else { oGP.mUnitStartupReport = new GPUnitStartUpReport(); }

			oGP.scBarrelMaxSpeed= this.scBarrelMaxSpeed;
			oGP.sCDMAESNInbound= this.sCDMAESNInbound;
			oGP.sConstructorDecodeError= this.sConstructorDecodeError;
			oGP.sIMEIInbound= this.sIMEIInbound;
			oGP.sRawGPS= this.sRawGPS;
			oGP.sSimNumber= this.sSimNumber;
			oGP.uBarrelRevolutionCount= this.uBarrelRevolutionCount;
            oGP.OldReportingFrequency = OldReportingFrequency;
            oGP.NewReportingFrequency = NewReportingFrequency;
            oGP.GFCalibrationReason = GFCalibrationReason;
            oGP.OldGFCalibration1 = OldGFCalibration1;
            oGP.OldGFCalibration2 = OldGFCalibration2;
            oGP.NewGFCalibration1 = NewGFCalibration1;
            oGP.NewGFCalibration2 = NewGFCalibration2;
            oGP.GPSSettings = GPSSettings;
            oGP.DiagnosticsHardwareType = DiagnosticsHardwareType;
            oGP.DiagnosticsLevel = DiagnosticsLevel;
            oGP.Diagnostics = Diagnostics;

            if (RoadTypeData != null)
            {
                oGP.RoadTypeData = this.RoadTypeData.CreateCopy();
            }

			return oGP;
		}

		
		private string DecodeIMIEAlivePacket(byte[] aData)
		{
			/*
			[Type][Hardware][SSC][IMIE String][SSC][Sim Number][SSC][CDMA ESN][SSC][FleetId][VehicleID][Software Ver][SoftwareVersionMajor][SoftwareVersionMinor][IOBoxVersionMajor][IOBoxVersionMinor]
			
			[Type] = 0xE0
			[Hardware] = 1 = CDMA Only, 2 = GPRS Only, 3 = Both
			[SSC] = 0x0A
			[IMIE String] = Variable length string
			[SSC] = 0x0A
			[Sim Number] = Variable length string
			[SSC] = 0x0A
			[CDMA ESN] = Variable length string
			[SSC] = 0x0A
			[FleetId] = 1 byte fleet ID
			[VehicleID] = 2 bytes unsigned int
			[Software Ver] = 1 byte
			[SoftwareVersionMajor] = 1 byte
			[SoftwareVersionMinor] = 1 byte
			[IOBoxVersionMajor] = 1 byte
			[IOBoxVersionMinor] = 1 byte
						
			*/

			byte[] bConvert = new byte[4];
			byte[] bSerialNumber = new byte[6];
			int iHardwareType = 0;
			int X = 0;
			int iStartPos = 0;
			int iEndPos = 0;
			int iNextStart = 0;
			int iFleetID = 0;
			int iVehicleID = 0;
			int iSoftwareVer = 0;
			int iSoftwareVerMajor = 0;
			int iSoftwareVerMinor = 0;
			int iIOBoxVerMajor = 0;
			int iIOBoxVerMinor = 0;

			string sIMIE = "";
			string sSimNumber = "";
			string sCDMAESN = "";

			bConvert[0] = (byte) 0x00;
			bConvert[1] = (byte) 0x00;
			bConvert[2] = (byte) 0x00;
			bConvert[3] = (byte) 0x00;

			bSerialNumber[0] = (byte) 0x00;
			bSerialNumber[1] = (byte) 0x00;
			bSerialNumber[2] = (byte) 0x00;
			bSerialNumber[3] = (byte) 0x00;
			bSerialNumber[4] = (byte) 0x00;
			bSerialNumber[5] = (byte) 0x00;

			try
			{
				bConvert[0] = aData[0];
				iHardwareType = BitConverter.ToInt32(bConvert, 0);
			}
			catch(System.Exception ex1)
			{
				return ex1.Message;	
			}

			if(aData[1] != (byte) 0x0A)
			{
				return "SSC 1 not found in GEN_IM_ALIVE_IMIE packet.";
			}

			try
			{
				iStartPos = 2;
				iEndPos = 2;
				for (X = 2; X< aData.Length; X++)
				{
					if (aData[X] == (byte) 0x0A)
					{
						iNextStart = X + 1;
						break;
					}
					else
					{
						iEndPos = X;
					}
				}
				sIMIE = System.Text.ASCIIEncoding.ASCII.GetString(aData, iStartPos, iEndPos - iStartPos + 1);

				//	Trim off the version for inclusion in the database.
				//	IMEI is 15 characters, followed by a 2 character version number.. but we want to ignore the version
				//	number as it is not available on the modem itsefl to read off.
				if (sIMIE.Length > 15)
					sIMIE = sIMIE.Substring(0, 15);
			}
			catch(System.Exception ex2)
			{
				return ex2.Message;	
			}

			try
			{
				iStartPos = iNextStart;
				iEndPos = iNextStart;
				for (X = iStartPos; X< aData.Length; X++)
				{
					if (aData[X] == (byte) 0x0A)
					{
						iNextStart = X + 1;
						break;
					}
					else
					{
						iEndPos = X;
					}
				}
				sSimNumber = System.Text.ASCIIEncoding.ASCII.GetString(aData, iStartPos, iEndPos - iStartPos + 1);
			}
			catch(System.Exception ex3)
			{
				return ex3.Message;	
			}

			
			try
			{
				iStartPos = iNextStart;
				iEndPos = iNextStart;
				for (X = iStartPos; X< aData.Length; X++)
				{
					if (aData[X] == (byte) 0x0A)
					{
						iNextStart = X + 1;
						break;
					}
					else
					{
						iEndPos = X;
					}
				}
				sCDMAESN = System.Text.ASCIIEncoding.ASCII.GetString(aData, iStartPos, iEndPos - iStartPos + 1);
			}
			catch(System.Exception ex4)
			{
				return ex4.Message;	
			}

			try
			{				
				iNextStart = iEndPos + 1;
				bConvert[0] = aData[iNextStart++];
				iFleetID = BitConverter.ToInt32(bConvert, 0);
				
				bConvert[0] = aData[iNextStart++];
				bConvert[1] = aData[iNextStart++];
				iVehicleID = BitConverter.ToInt32(bConvert, 0);

				bConvert[1] = (byte) 0x00;

				bConvert[0] = aData[iNextStart++];
				iSoftwareVer = BitConverter.ToInt32(bConvert, 0);

				bConvert[0] = aData[iNextStart++];
				iSoftwareVerMajor = BitConverter.ToInt32(bConvert, 0);

				bConvert[0] = aData[iNextStart++];
				iSoftwareVerMinor = BitConverter.ToInt32(bConvert, 0);

				bConvert[0] = aData[iNextStart++];
				iIOBoxVerMajor = BitConverter.ToInt32(bConvert, 0);

				bConvert[0] = aData[iNextStart++];
				iIOBoxVerMinor = BitConverter.ToInt32(bConvert, 0);

			}
			catch(System.Exception ex5)
			{
				return ex5.Message;	
			}

			if (mImieData != null)
				mImieData.SetValues(iHardwareType, sIMIE, sSimNumber, sCDMAESN, iFleetID, iVehicleID, iSoftwareVer, iSoftwareVerMajor, iSoftwareVerMinor, iIOBoxVerMajor, iIOBoxVerMinor);

			cProgramMajor = Convert.ToByte(iSoftwareVer);
			iProgramMinor = (iSoftwareVerMajor * 10) + iSoftwareVerMinor;

			cIOBoxProgramMajor = Convert.ToByte(iIOBoxVerMajor);
			cIOBoxProgramMinor = Convert.ToByte(iIOBoxVerMinor);

			return null;
		}

		private string DecodeAlivePacket(byte[] aData)
		{
			int readPos, writePos;
			char[] bcdString;
	
			// Check we've got enough data to decode:
			if	((aData.Length != GEN_IM_ALIVE_LENGTH) &&
				 (aData.Length != GEN_IM_ALIVE_TRANSPORT_LENGTH))
				return "Incorrect GEN_IM_ALIVE packet length";

			// bytes 8 - 11 (1-based) are serial number
			// Translates to bytes 0-3 in this payload
			lSerialNumber = (uint) (aData[3] << 24) +
							(uint) (aData[2] << 16) +
							(uint) (aData[1] << 8) +
							(uint) (aData[0]);

			// bytes 12 - 17 (1-based)are Sim card number,
			// stored in BCD format. We'll convert to a string.
			// It can be up to 12 digits long
			bcdString = new char[12];
			writePos = 0;
			for (readPos = 4; readPos < 10; readPos++)
			{
				bcdString[writePos++] = System.Convert.ToChar(((aData[readPos] & 0xF0) >> 4) + 0x30);
				bcdString[writePos++] = System.Convert.ToChar((aData[readPos] & 0x0F) + 0x30);
			}
			sSimNumber = new string(bcdString);

			// Program Version Numbers:
			cProgramMajor = aData[readPos++];
			iProgramMinor = Convert.ToInt16(aData[readPos++]) << 8;
			iProgramMinor += aData[readPos++];

			if (aData.Length == GEN_IM_ALIVE_TRANSPORT_LENGTH)
			{
				cIOBoxProgramMajor = aData[readPos++];
				cIOBoxProgramMinor = aData[readPos++];
			}
			return null;
		}

		/// <summary>
		/// This method will decode the IMEI and ESN Alive packet.
		/// This replaces the GEN_IM_ALIVE packet and includes either
		/// the ESN for CDMA logon, or the IMEI for GPRS logon. 
		/// It also includes a hardware type byte for use in deciding whether 
		/// we are dealing with CDMA or GPRS.
		/// Format of packet
		///			Standard Header - not included in this byte array
		///	0		Hardware Type
		///	1-4		Serial Number
		///	5-10	SIMCard Number
		///	11		Software Number
		///	12		Software Version Major
		///	13		Software Version Minor
		///	14		IO Box Version Major
		///	15		IO Box Version Minor
		///	16-35	IMEI (Hardware Type of 1, or 2) or ESN (HardwareType of 3)
		/// </summary>
		/// <param name="aData"></param>
		/// <returns></returns>
		private string DecodeAliveIMEIESNPacket(byte[] aData)
		{
			int readPos = 0;
			
			// Check we've got enough data to decode:
			bool simcardError = false;

			if	(aData.Length != GEN_IM_ALIVE_IMEI_ESN_LENGTH)
				if (aData.Length != GEN_IM_ALIVE_IMEI_ESN_LENGTH_INERROR)
				{
					return "Incorrect GEN_IM_ALIVE_IMEI_ESN packet length";
				}
				else
				{
					simcardError = true;
				}

			int tempHardwareType = Convert.ToInt32(aData[0]);
			if ((tempHardwareType >= 0) && (tempHardwareType <= (int)iHardwareTypeMax))
			{
				iHWTypeInbound  = tempHardwareType;
				iHardwareTypeInbound = (Alive_IMEIESN_HardwareType)tempHardwareType;
			}
			else
			{
				iHWTypeInbound  = 0;
				iHardwareTypeInbound = Alive_IMEIESN_HardwareType.Unknown_SerialNumber;
			}

			// bytes 12 - 14 (1-based) are serial number
			// Translates to bytes 1-4 in this payload
			lSerialNumber = (uint) (aData[4] << 24) +
				(uint) (aData[3] << 16) +
				(uint) (aData[2] << 8) +
				(uint) (aData[1]);

			// next 6 bytes (1-based)are Sim card number,
			// stored in BCD format. We'll convert to a string.
			// It can be up to 12 digits long
			int Alive_IMEIESN_SIMCARDNUMBER_LENGTH = 25;
			if (simcardError)
				Alive_IMEIESN_SIMCARDNUMBER_LENGTH = 6;

			//	POD Following code relies on BCD encoded string... NOTE : The SIMCardNumber is no longer BCD encoded.
//			char[] bcdString = new char[Alive_IMEIESN_SIMCARDNUMBER_LENGTH * 2];
//			for (readPos = 5; readPos < 5 + Alive_IMEIESN_SIMCARDNUMBER_LENGTH; readPos++)
//			{
//				bcdString[writePos++] = System.Convert.ToChar(((aData[readPos] & 0xF0) >> 4) + 0x30);
//				bcdString[writePos++] = System.Convert.ToChar((aData[readPos] & 0x0F) + 0x30);
//			}
//			sSimNumber = new string(bcdString);


			//POD This code handles non-BCD encoded string
			readPos = 5;
			sSimNumber = GetStringFrom0PaddedArray(aData, readPos, Alive_IMEIESN_SIMCARDNUMBER_LENGTH);
			readPos += Alive_IMEIESN_SIMCARDNUMBER_LENGTH;

			// Program Version Numbers:
			cProgramMajor = aData[readPos++];
			iProgramMinor = Convert.ToInt16(aData[readPos++]) << 8;
			iProgramMinor += aData[readPos++];

			cIOBoxProgramMajor = aData[readPos++];
			cIOBoxProgramMinor = aData[readPos++];

			string sIMEI_ESN_Number = GetStringFrom0PaddedArray(aData, readPos, 20);
			if (sIMEI_ESN_Number != "" && iHardwareTypeInbound == Alive_IMEIESN_HardwareType.Unknown_SerialNumber)
				iHardwareTypeInbound = Alive_IMEIESN_HardwareType.Platform_3020_IMEI;
			
			switch(iHardwareTypeInbound)
			{				
				case Alive_IMEIESN_HardwareType.Platform_3020_IMEI :
				case Alive_IMEIESN_HardwareType.Platform_3021_IMEI :
				case Alive_IMEIESN_HardwareType.Platform_5070_IMIE :
				case Alive_IMEIESN_HardwareType.Platform_3025_IMEI :
				case Alive_IMEIESN_HardwareType.Platform_3026_IMEI :
                case Alive_IMEIESN_HardwareType.Platform_3028_IMIE :
                case Alive_IMEIESN_HardwareType.Platform_3029_NextG_Serial:
                case Alive_IMEIESN_HardwareType.Platform_3029_Serial:
                case Alive_IMEIESN_HardwareType.Platform_3033_Serial:
                case Alive_IMEIESN_HardwareType.Platform_4000_Serial:
                case Alive_IMEIESN_HardwareType.Platform_5080_Serial:
                case Alive_IMEIESN_HardwareType.Platform_5081_Serial:
                    //POD 09-09-2005 
                    //	NOTE : Last two bytes of IMEI are version number.. 
                    //		drop these off.. 
                    //		We may use them again in the future, but for now, we drop them
                    if (sIMEI_ESN_Number.Length <= 2)
						sIMEIInbound = "";
					else
						if (sIMEI_ESN_Number.Length > 15)
							sIMEIInbound = sIMEI_ESN_Number.Substring(0, 15);
						else
							sIMEIInbound = sIMEI_ESN_Number;
					sCDMAESNInbound = "";
					break;
				case Alive_IMEIESN_HardwareType.Platform_3022_CDMAESN :
					sIMEIInbound = "";
					sCDMAESNInbound = sIMEI_ESN_Number;
					break;
				default :
				{
					sIMEIInbound = "";
					sCDMAESNInbound = "";
					break;
				}
			}

			return null;
		}

		/// <summary>
		/// This method will decode the IMEI and ESN Alive packet.
		/// This replaces the GEN_IM_ALIVE and GEN_IM_ALIVE_IMEI_ESN packets and includes 
		/// the current network and the sim card slot that is in use.
		/// Format of packet
		///			Standard Header - not included in this byte array
		///	0				Hardware Type
		///	1 - 25		GSM Sim Number
		///	26			Slot Number
		///	27			Prefered Slot Number
		///	28 - 47	Network Name
		///	48 - 67	IMIE Number
		///	68 - 71	Unit Serial Number
		///	72			302x Software Number
		///	73			302x Software Version Major
		///	74			302x Software Version Minor
		///	75			302x Hardware Version
		///	76 - 79	1035-8 Serial Number
		///	80			1035-8 Software Version Major
		///	81			1035-8 Software Version Minor
		///	82			1035-8 Software Hardware Version
		///	83 - 86	5040 Serial Number
		///	87			5040 Software Version Major
		///	88			5040 Software Version Minor
		///	89			Binary Flags
		/// </summary>
		/// <param name="aData"></param>
		/// <returns></returns>
		private string DecodeAliveFullPacket(byte[] aData)
		{
			int readPos = 0;
			string sIMEI_ESN_Number = "";
			string sErrMsg = "";
			byte bTemp = (byte) 0x00;
			byte bExtendFlags = (byte) 0x80;

			try
			{
				#region Hardware Type
				int tempHardwareType = Convert.ToInt32(aData[readPos++]);
				if ((tempHardwareType >= 0) && (tempHardwareType <= (int)iHardwareTypeMax))
					iHardwareTypeInbound = (Alive_IMEIESN_HardwareType)tempHardwareType;
				else
					iHardwareTypeInbound = Alive_IMEIESN_HardwareType.Unknown_SerialNumber;
				iHWTypeInbound = tempHardwareType;
				#endregion
				#region Read the Simcard Number, slot selection and prefered slot
				sSimNumber = GetStringFrom0PaddedArray(aData, readPos, 25);
				readPos += 25;
				iSlotNumber = Convert.ToInt32(aData[readPos++]);
				iPreferedSlotNumber = Convert.ToInt32(aData[readPos++]);
				#endregion
				#region Read the network name
				for (int X = 0; X < 20; X++)
				{
					sNetworkName += (char) aData[readPos++];
				}
				sNetworkName = sNetworkName.Replace("\0", "");
				#endregion
				#region Read the IMEI/ESN number
				sIMEI_ESN_Number = GetStringFrom0PaddedArray(aData, readPos, 20);
				readPos += 20;

				if (sIMEI_ESN_Number != "" && iHardwareTypeInbound == Alive_IMEIESN_HardwareType.Unknown_SerialNumber)
					iHardwareTypeInbound = Alive_IMEIESN_HardwareType.Platform_3020_IMEI;

				switch(iHardwareTypeInbound)
				{				
					case Alive_IMEIESN_HardwareType.Platform_3020_IMEI :
					case Alive_IMEIESN_HardwareType.Platform_3021_IMEI :
					case Alive_IMEIESN_HardwareType.Platform_5070_IMIE :
					case Alive_IMEIESN_HardwareType.Platform_3025_IMEI :
					case Alive_IMEIESN_HardwareType.Platform_3026_IMEI :
                    case Alive_IMEIESN_HardwareType.Platform_3028_IMIE :
                    case Alive_IMEIESN_HardwareType.Platform_3029_NextG_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_3029_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_3033_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_4000_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_5080_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_5081_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_2027_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_3027_Serial:
                    case Alive_IMEIESN_HardwareType.Platform_3035_Serial:
                        //POD 09-09-2005 
                        //	NOTE : Last two bytes of IMEI are version number.. 
                        //		drop these off.. 
                        //		We may use them again in the future, but for now, we drop them
                        if (sIMEI_ESN_Number.Length <= 2)
							sIMEIInbound = "";
						else
							if (sIMEI_ESN_Number.Length > 15)
							sIMEIInbound = sIMEI_ESN_Number.Substring(0, 15);
						else
							sIMEIInbound = sIMEI_ESN_Number;
						sCDMAESNInbound = "";
						break;
					case Alive_IMEIESN_HardwareType.Platform_3022_CDMAESN :
						sIMEIInbound = "";
						sCDMAESNInbound = sIMEI_ESN_Number;
						break;
					default :
					{
						sIMEIInbound = "";
						sCDMAESNInbound = "";
						break;
					}
				}
				#endregion
				#region Read the Serial number
				lSerialNumber = (uint) (aData[readPos++]);
				lSerialNumber += (uint) (aData[readPos++] << 8);
				lSerialNumber += (uint) (aData[readPos++] << 16);
				lSerialNumber += (uint) (aData[readPos++] << 24); 
				#endregion
				#region Read the 302x Program and Hardware Version Numbers:
				iSoftwareVersion = Convert.ToInt32(aData[readPos++]);
				cProgramMajor = aData[readPos++];
				iProgramMinor = aData[readPos++];
				iHardwareVersionNumber = Convert.ToInt32(aData[readPos++]);
				#endregion
				#region Read the 1035-8 Serial number and program and hardware version numbers:
				lIOBoxSerialNumber = (uint) (aData[readPos++]);
				lIOBoxSerialNumber += (uint) (aData[readPos++] << 8);
				lIOBoxSerialNumber += (uint) (aData[readPos++] << 16);
				lIOBoxSerialNumber += (uint) (aData[readPos++] << 24); 
			
				cIOBoxProgramMajor = aData[readPos++];
				cIOBoxProgramMinor = aData[readPos++];
				iIOBoxHardwareVersionNumber = Convert.ToInt32(aData[readPos++]);
				#endregion
				#region Read the MDT serial number and program version numbers
				lMDTSerialNumber = (uint) (aData[readPos++]);
				lMDTSerialNumber += (uint) (aData[readPos++] << 8);
				lMDTSerialNumber += (uint) (aData[readPos++] << 16);
				lMDTSerialNumber += (uint) (aData[readPos++] << 24); 
				iMDTProgramMajor = Convert.ToInt32(aData[readPos++]);
				iMDTProgramMinor = Convert.ToInt32(aData[readPos++]);
				iMDTHardwareVer = Convert.ToInt32(aData[readPos++]);
				#endregion
				#region Read the login flags
				bTemp = aData[readPos++];
				iLoginFlags = (int) bTemp;
				if ((bTemp & bExtendFlags) == bExtendFlags)
				{
					#region Extended to a second byte
					bTemp = aData[readPos++];
					iLoginFlags += (int) (bTemp << 8);
					if ((bTemp & bExtendFlags) == bExtendFlags)
					{
						#region Extended to a third byte
						bTemp = aData[readPos++];
						iLoginFlags += (int) (bTemp << 16);
						if ((bTemp & bExtendFlags) == bExtendFlags)
						{
							#region Extended to a forth byte
							bTemp = aData[readPos++];
							iLoginFlags += (int) (bTemp << 24);
							#endregion
						}
						#endregion
					}
					#endregion
				}
				#endregion
				bIsPopulated = true;

				if (mImieData != null)
					mImieData.SetValues(tempHardwareType, sIMEIInbound, sSimNumber, sCDMAESNInbound, (int) this.cFleetId, (int)this.iVehicleId, iSoftwareVersion, (int) cProgramMajor, (int) iProgramMinor, (int) cIOBoxProgramMajor, (int) cIOBoxProgramMinor);

				_iProgramMinor = iProgramMinor;
				_cProgramMajor = cProgramMajor;

				iProgramMinor = ((cProgramMajor * 10) << 8) + iProgramMinor;
				cProgramMajor = Convert.ToByte(iSoftwareVersion);

                try
                {
                    //check to see if there are any extended info provided
                    if (aData.Length > readPos)
                    {
                        bTemp = aData[readPos++];
                        int extendedLength = aData[readPos++];
                        switch (bTemp)
                        {
                            case 1: //extendedFirmwareVersion
                                SoftwareBuild = PacketUtilities.Read2ByteNumberFromBuffer(aData, readPos);
                                readPos += 2;
                                SoftwareBranch = PacketUtilities.Read2ByteNumberFromBuffer(aData, readPos);
                                readPos += 2;
                                SoftwareBuildOptions = PacketUtilities.Read4ByteNumberFromBuffer(aData, readPos);
                                readPos += 4;
                                break;
                        }
                    }
                }
                catch
                {

                }
			}
			catch(System.Exception ex)
			{
				sErrMsg = "DecodeAliveFullPacket : " + ex.Message;
				return sErrMsg;
			}
			return null;
		}

		/// <summary>
		/// This method will take the sectionof a byte array, and read the string out of it,
		/// up until the first 0 byte, which is the string ender..
		/// </summary>
		/// <param name="data"></param>
		/// <param name="startPos"></param>
		/// <param name="maxLength"></param>
		/// <returns></returns>
		private string GetStringFrom0PaddedArray(byte[] data, int startPos, int maxLength)
		{
			int length = 0;
			while((length < maxLength) && (data[startPos + length] != 0))
				length++;

			return System.Text.ASCIIEncoding.ASCII.GetString(data, startPos, length);
		}

		private string DecodeGeneralPacket(byte[] aData)
		{
			int pos =0;
			string retMsg;

			cRefrigerationZone = (byte) 0x00;

			switch (cMsgType)
			{
					// Check we've got reasonable length data to decode:
				case STATUS_REPORT:
				case GPS_HISTORY:
				case STATUS_POS_REPORT:	
				case GEN_OVERSPEED:
				//case GEN_UNDERSPEED:	//POD removed 09092005
				case GEN_EXCESSIVE_IDLE: 
				case BARREL_RPM_FORWARD:
				case BARREL_RPM_REVERSED:
				case BARREL_RPM_STOPPED:
				case BARREL_RPM_UP_TO_SPEED:
				case BARREL_MIX_STARTED:
				case BARREL_LEFT_LOADER:
				case BARREL_NOT_MIXED:
				case BARREL_ABOVE_ONSITE_SPEED:
				case BARREL_LOADING_STARTED:
				case STATUS_TRAILER_HITCH:
				case STATUS_TRAILER_DEHITCH:
				case OVERSPEED_ZONE_1_START:
				case OVERSPEED_ZONE_2_START	:
				case OVERSPEED_ZONE_3_START:
				case OVERSPEED_ZONE_1_END:
				case OVERSPEED_ZONE_2_END:
				case OVERSPEED_ZONE_3_END:
				case ANGEL_GEAR_ALERT_START:
				case ANGEL_GEAR_ALERT_END:
				case FATIGUE_REPORT_IGNON:
				case FATIGUE_REPORT_24:
				case BARREL_CONCRETE_AGE_MINS:
				case BARREL_CONCRETE_AGE_ROTATIONS:
                case GEN_NO_GPS_ANT_OPEN:
                case GEN_NO_GPS_ANT_SHORT:
                case GEN_NOGPS:
                case ALRM_SUSPECT_GPS:
                case ALRM_IGNITION_DISCONNECT:
                case ALRM_ENGINE_DATA_MISSING:
                case ALRM_ENGINE_DATA_RE_CONNECT:                
                case ALRM_SUSPEC_ENGINE_DATA:
                case ALRM_G_FORCE_MISSING:
                case ALRM_G_FORCE_OUT_OF_CALIBRATION:
                case ALRM_CONCETE_SENSOR_ALERT:
                case ALRM_MDT_NOT_LOGGED_IN:
                case ALRM_MDT_NOT_BEING_USED:
                case ALRM_UNIT_POWERUP:
				case ALRM_VEHICLE_GPS_SPEED_DIFF:
                case GEN_G_FORCES_SUSTAINED_START:
                case GEN_G_FORCES_SUSTAINED_END:
                case STATUS_DATA_USAGE_ALERT:
                case STATUS_DATA_USAGE_ALERT_SATELLITE:
                case GEN_EXCESSIVE_IDLE_END:
                case GEN_ECM_OVERSPEED:
                case PENDENT_ALARM_TEST:
                case PENDENT_ALARM:
                case PENDENT_ALARM_CLEARED:
                case PENDENT_ALARM_ACKD_BY_USER:
                case PENDENT_ALARM_NOT_ACKD_BY_USER:
                case GEN_ZERO_BYTES_RECEIVED_RESTART:
                case ECM_HYSTERESIS_ERROR:
                case SAT_PING:
                case LOW_POWER_MODE:
                    break;
				case GEN_IOONOFF:
					// Copy in the extra byte:
					cButtonNumber = aData[pos++];
					break;
                case GEN_OUTPUT:
                    OutputState = (sbyte)aData[pos++];
                    break;
				case EXTENDED_IO_BELOW_THRESHOLD:
				case EXTENDED_IO_ABOVE_THRESHOLD:
				case STATUS_IGNITION_ON:
				case STATUS_IGNITION_OFF:
				case REFRIG_REPORT:
                case STATUS_VEHICLE_BREAK_START:
                case STATUS_VEHICLE_BREAK_END:
                case DRIVER_PERSONAL_KMS:
                case GEN_MDT_STATUS_REPORT:
                case STATUS_LOGIN:
                case STATUS_LOGOUT:
                case GEN_IF_CONFIG_RESTART:
                case SHELL_REQUEST_REBOOT:
                    // Copy in the extra byte:
					cInputNumber = aData[pos++];
					if (cMsgType == REFRIG_REPORT)
						cRefrigerationZone = aData[pos++];
                    break;
                case ALARM_LOW_BATTERY:
					cInputNumber = aData[pos++];
                    if (cInputNumber >= 5 && cInputNumber <= 8)
                    {
                        OldReportingFrequency = (short)PacketUtilities.Read2ByteNumberFromBuffer(aData, pos);
                        pos += 2;
                        NewReportingFrequency = (short)PacketUtilities.Read2ByteNumberFromBuffer(aData, pos);
                        pos += 2;
                    }
					break;
                case GFORCE_CALIBRATION:
                    cInputNumber = aData[pos++];
                    GFCalibrationReason = (short)aData[pos++];
                    if (cInputNumber == 1)
                    {
                        PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref OldGFCalibration1);
                        PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref OldGFCalibration2);
                        PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref NewGFCalibration1);
                        PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref NewGFCalibration2);
                    }
                    else if (cInputNumber == 2)
                    {
                        PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref OldGFCalibration1);
                        PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref NewGFCalibration1);
                    }
                    break;
                case GPS_SETTINGS:
                    PacketUtilities.ReadFromPacketAtPos(aData, ref pos, 7, ref GPSSettings);
                    break;
                case DIAGNOSTIC_LEVEL:
                    DiagnosticsLevel = (short)aData[pos++];
                    break;
                case DIAGNOSTIC:
                    DiagnosticsHardwareType = (short)aData[pos++];
                    DiagnosticsLevel = (short)aData[pos++];
                    PacketUtilities.ReadFromPacketAtPos(aData, ref pos, ref Diagnostics);
                    break;
                case TAMPER:
                    cInputNumber = aData[pos++];
                    break;
                case DRIVER_OVERSPEED_REPORT:
                    bSpeedZoneID = aData[pos++];                        // Read the byte
			        bSpeedZoneFlags = (byte) (bSpeedZoneID & 0x0F);     // Bottom nibble is speed zone flags
                    bSpeedZoneID = (byte) ((bSpeedZoneID & 0xF0) >> 4); // Top nibble is the speed zone ID
                    bSpeedZoneLimit = aData[pos++];                     // Get the speed zone limit
                    //bSpeedZoneID
                    //bSpeedZoneFlags
                    //bSpeedZoneLimit
                    break;
				case STATUS_IO_PULSES:
					// Copy in the extra bytes:
					cInputNumber = aData[pos++];
					iPulseCount = Convert.ToUInt16(aData[pos++] << 8);
					iPulseCount += Convert.ToUInt16(aData[pos++]);
					break;
				case STATUS_ACCIDENT_NEW:
                case POSSIBLE_ACCIDENT:
                    cAccidentIndex = aData[pos++];
					break;

				case GEN_STARTUP:
					//	if there is a payload, then parse it and pull it in.
					if (aData.Length > 1)
					{
						DebugDump = new DebugDump_Struct();
						DebugDump.Reason = aData[pos++];

						if (aData.Length >= 11)
						{
							DebugDump.Count = aData[pos++];
							DebugDump.Index = aData[pos++];
							DebugDump.WatchDog = aData[pos++];
							DebugDump.WatchDog |= (ushort)(((ushort)aData[pos++]) << 8);

							DebugDump.LogonWatchDog = aData[pos++];
							DebugDump.LogonWatchDog |= (ushort)(((ushort)aData[pos++]) << 8);

							DebugDump.ProgramCounter = aData[pos++];
							DebugDump.ProgramCounter |= ((uint)aData[pos++]) << 8;
							DebugDump.ProgramCounter |= ((uint)aData[pos++]) << 16;
							DebugDump.ProgramCounter |= ((uint)aData[pos++]) << 24;
						}
					}

					return null;
				case GEN_ACK:
				case GEN_NAK:
                    return null;
                case ENGINE_OVER_RPM:
				case ENGINE_OVER_COOLANT_TEMP:
				case ENGINE_OVER_OIL_TEMP:
				case ENGINE_UNDER_OIL_PRESSURE_LOW:
				case ENGINE_UNDER_OIL_PRESSURE_HIGH:
				case ENGINE_G_FORCES_HIGH:
				case ENGINE_COOLANT_LEVEL_LOW:
				case ENGINE_G_FORCE_CALIBRATED_RPT:
				case ENGINE_FUEL_ECONOMY_ALERT:
					// Rip out the 4 interesting bytes and continue:
                    iEngineValue = (uint) (aData[pos++]);
                    iEngineValue += (uint) (aData[pos++] << 8);
                    iEngineValue += (uint) (aData[pos++] << 16);
					iEngineValue += (uint) (aData[pos++] << 24);
                    break;
                case ENGINE_ERROR_CODE:
                    byte[] errorCode = new byte[4];
                    errorCode[0] = aData[pos++];
                    errorCode[1] = aData[pos++];
                    errorCode[2] = aData[pos++];
                    errorCode[3] = aData[pos++];
                    bool bValidCode = true;
                    foreach (byte b in errorCode)
                    {
                        if (b == 0xFF)
                        {
                            bValidCode = false;
                            break;
                        }
                    }
                    if(bValidCode)
                    {
                        if (mECMAdvanced == null)
                            mECMAdvanced = new GPECMAdvanced();
                        mECMAdvanced.ECMAdvancedItems = new GPECMAdvancedItem[2];

                        int parameter = 0;
                        parameter = errorCode[1];
                        parameter = parameter << 8;
                        parameter += errorCode[0];

                        GPECMAdvancedItem oItem = new GPECMAdvancedItem();
                        oItem.SourceType = (GPECMAdvancedItem.SourceTypes)6;
                        oItem.SPN_ID = 2;
                        oItem.RawData = new byte[2];
                        oItem.RawData[0] = errorCode[0];
                        oItem.RawData[1] = errorCode[1];
                        mECMAdvanced.ECMAdvancedItems[0] = oItem;

                        oItem = new GPECMAdvancedItem();
                        oItem.SourceType = (GPECMAdvancedItem.SourceTypes)6;
                        oItem.SPN_ID = 3;
                        oItem.RawData = new byte[1];
                        oItem.RawData[0] = (byte) (errorCode[3] & 0x1F);
                        mECMAdvanced.ECMAdvancedItems[1] = oItem;
                    }
					break;
                case GeneralGPPacket.GEN_CONFIG_DOWNLOADED:
                    mConfigDownloaded = new ConfigDownloaded();
                    return mConfigDownloaded.Decode(aData);
                case GeneralGPPacket.STATUS_RX_TX_SATTELITE:
                    return mSatteliteDataUsage.Decode(aData, pos);
				case GeneralGPPacket.STATUS_RX_TX:		
					uint uTemp = 0;
                    uTemp = (uint)(aData[pos++]);
                    uTemp += (uint)(aData[pos++] << 8);
                    uTemp += (uint)(aData[pos++] << 16);
                    uTemp += (uint) (aData[pos++] << 24);
					iUnitTXBytes = Convert.ToInt32(uTemp);
					uTemp = 0;
                    uTemp = (uint)(aData[pos++]);
                    uTemp += (uint)(aData[pos++] << 8);
                    uTemp += (uint)(aData[pos++] << 16);
                    uTemp += (uint)(aData[pos++] << 24);
					iUnitRXBytes = Convert.ToInt32(uTemp);
					sNetworkName = "";
					byte bTemp = (byte) 0x00;
					if (pos < aData.Length)
					{
						iProtocolVer =	(int) aData[pos++];
						int iDay =	(int) aData[pos++];
						int iMonth =	(int) aData[pos++];
						int iYear =	(int) aData[pos++];
						int iHour =	(int) aData[pos++];
						int iMin =	(int) aData[pos++];
						int iSec =	(int) aData[pos++];
						dtGPSTime = DateTime.Now.ToUniversalTime();
						try
						{
							if(iYear > 0 && (iMonth > 0 && iMonth < 13) && (iDay > 0 && iDay < 32)  && (iHour >= 0 && iHour < 24)  && (iHour >= 0 && iHour < 60)  && (iSec >= 0 && iSec < 60))
								dtGPSTime = new DateTime(iYear + 2000, iMonth, iDay, iHour, iMin, iSec);
						}
						catch(System.Exception)
						{
							dtGPSTime = DateTime.Now.ToUniversalTime();
						}
						iSlotNumber =	(int) aData[pos++];
						while (pos < aData.Length)
						{
							bTemp = aData[pos++];
							if (bTemp != (byte) 0x0A)
								sSimNumber += Convert.ToString((char) bTemp);
							else
								break;
						}
						sSimNumber = sSimNumber.Replace("\0", "");
						while (pos < aData.Length)
						{
							bTemp = aData[pos++];
							if (bTemp != (byte) 0x0A)
								sNetworkName += Convert.ToString((char) bTemp);
							else
								break;
						}
						sNetworkName = sNetworkName.Replace("\0", "");
					}
					return null;
				case GeneralGPPacket.GEN_NEW_NETWORK_FOUND:	
					return null;
                case MassDeclarationPacket.MASS_DECLARATION:
                    pos++;
                    break;
				default:
					return "Unknown General Packet type: " + cMsgType;
			}

			// Work through the packet
			if ((retMsg = mCurrentClock.Decode(aData, pos))!= null)
			{
				// most likely this is an invalid clock (i.e. we haven't
				// acquired GPS as yet). We will skip decoding the rest
				// of the packet except for the Engine info which, as it
				// is not derived from GPS, may be of some value
				pos +=	GPClock.Length + GPClock.Length + GPPositionFix.Length;
				if ((retMsg = mStatus.Decode(aData, pos))!= null) return retMsg;
				pos += mStatus.Length;
				pos +=	GPDistance.Length;
			}
			else
			{
				pos += GPClock.Length;
				if ((retMsg = mFixClock.Decode(aData, pos))!= null) 
				{
						//return retMsg;
				}
				pos += GPClock.Length;
				if ((retMsg = mFix.Decode(aData, pos))!= null) return retMsg;
				pos += GPPositionFix.Length;
				if ((retMsg = mStatus.Decode(aData,pos))!= null) return retMsg;
				pos += mStatus.Length;		
				if ((retMsg = mDistance.Decode(aData,pos))!= null) return retMsg;	
				pos += GPDistance.Length;
			}

			mExtendedValues.iValue1 = 0;
			mExtendedValues.iValue2 = 0;
			mExtendedValues.iValue3 = 0;
			mExtendedValues.iValue4 = 0;

//cPacketTotal  cPacketNum  Mobile Structure	Transport Standard	Extra Transport	Trailer Track	ECM Advanced	Extended 5.9 States
// 0x01           0x00	            X					
// 0x01           0x01	            X	                    X				
// 0x01           0x03	            X	                    X	                X			
// 0x01           0x07	            X	                    X	                X	        X		
// 0x01           0x04	            X                   		                	        X		
// 0x01           0x05	            X	                    X	                	        X		
// 0x01           0x08	            X                   		                	        	            	                X
// 0x01           0x09	            X	                    X	                	        	            	                X
// 0x01           0x0B	            X	                    X	                X	        	            	                X
// 0x01           0x0F	            X	                    X	                X	        X	            	                X
// 0x01           0x0C	            X	                    	                	        X	            	                X
// 0x01           0x0D	            X	                    X	                	        X	            	                X
// 0x03           0x08	            X	                    X	                X	        	            X	                X
// 0x03           0x0C	            X	                    X	                X	        X	            X	                X

			bool bEngineData = false;
			bool bExtraDetails = false;
			bool bTrailerTrack = false;
			bool bExtendedVariableDetails = false;
            bool bECMAdvanced = false;
            bool hasRoadTypeData = false;

			if (this.cPacketTotal == (byte)0x01)
			{
				bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
				bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
				bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
				bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                bECMAdvanced = false;
			}
            else if (this.cPacketTotal == (byte)0x03)
            {
                bEngineData = false;
                bExtraDetails = false;
                bTrailerTrack = ((this.cPacketNum & (byte)0x0C) == (byte)0x0C);
                bExtendedVariableDetails = true;
                bECMAdvanced = true;
            }
            else if (this.cPacketTotal == (byte)0x04 || this.cPacketTotal == (byte)0x05 || this.cPacketTotal == (byte)0x06 || this.cPacketTotal == (byte)0x07)
            {
                bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                bECMAdvanced = true;

                if (cPacketTotal == 0x07)
                {
                    hasRoadTypeData = true;
                }
            }

            if (this.cPacketTotal == (byte)0x06)
            {
                //need to skip 31 bytes of additional advanced ECM data that is not used at present
                //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                pos += 31;
            }

			if ((pos+1) < aData.Length)
			{
                if (cMsgType == GeneralGPPacket.ALRM_UNIT_POWERUP)
                {
                    mUnitStartupReport = new GPUnitStartUpReport(mCurrentClock.ToDateTime());
                    if ((retMsg = mUnitStartupReport.Decode(aData, ref pos)) != null) return retMsg;
                }
                else
                {
                    if (bEngineData)
                    {
                        #region Decode Engine Summary Data and Extended Values
                        if ((retMsg = mEngineData.Decode(aData, pos)) != null) return retMsg;
                        pos += GPEngineData.Length;

                        mExtendedValues.iValue1 = 0;
                        mExtendedValues.iValue2 = 0;
                        mExtendedValues.iValue3 = 0;
                        mExtendedValues.iValue4 = 0;
                        if ((pos + 2) < aData.Length)
                        {
                            mExtendedValues.iValue1 = (aData[pos++]);
                            mExtendedValues.iValue1 += (aData[pos++] << 8);
                            if (mExtendedValues.iValue1 > 32768)
                                mExtendedValues.iValue1 = mExtendedValues.iValue1 - 65535;
                        }
                        if ((pos + 2) < aData.Length)
                        {
                            mExtendedValues.iValue2 = (aData[pos++]);
                            mExtendedValues.iValue2 += (aData[pos++] << 8);
                            if (mExtendedValues.iValue2 > 32768)
                                mExtendedValues.iValue2 = mExtendedValues.iValue2 - 65535;
                        }
                        if ((pos + 2) < aData.Length)
                        {
                            mExtendedValues.iValue3 = (aData[pos++]);
                            mExtendedValues.iValue3 += (aData[pos++] << 8);
                            if (mExtendedValues.iValue3 > 32768)
                                mExtendedValues.iValue3 = mExtendedValues.iValue3 - 65535;
                        }
                        if ((pos + 2) < aData.Length)
                        {
                            mExtendedValues.iValue4 = (aData[pos++]);
                            mExtendedValues.iValue4 += (aData[pos++] << 8);
                            if (mExtendedValues.iValue4 > 32768)
                                mExtendedValues.iValue4 = mExtendedValues.iValue4 - 65535;
                        }
                        #endregion
                    }
                    if (bExtraDetails)
                    {
                        #region Decode the Transport Extra Details
                        if ((retMsg = mTransportExtraDetails.Decode(aData, pos)) != null) return retMsg;
                        if (mEngineSummaryData != null)
                        {
                            if (Convert.ToInt32(mTransportExtraDetails.iOdometer) > mEngineSummaryData.iOdometer)
                                mEngineSummaryData.iOdometer = Convert.ToInt32(mTransportExtraDetails.iOdometer);
                            if (mTransportExtraDetails.iTotalFuelUsed > mEngineSummaryData.iTotalFuel)
                                mEngineSummaryData.iTotalFuel = mTransportExtraDetails.iTotalFuelUsed;
                            if (mTransportExtraDetails.iBatteryVolts > mEngineSummaryData.iBatteryVoltage)
                                mEngineSummaryData.iBatteryVoltage = mTransportExtraDetails.iBatteryVolts;
                            if (mTransportExtraDetails.fFuelEconomy > mEngineSummaryData.fFuelEconomy)
                                mEngineSummaryData.fFuelEconomy = mTransportExtraDetails.fFuelEconomy;
                        }
                        pos += GPTransportExtraDetails.Length;
                        #endregion
                    }
                    if (bECMAdvanced)
                    {
                        if ((retMsg = mECMAdvanced.Decode(aData, ref pos)) != null) return retMsg;
                        if(mECMAdvanced.SpeedAccuracyValuesSet)
                        {
                            mFix.cSpeed = mECMAdvanced.CreateSpeedSPNItem(mFix.cSpeed, false);
                            mDistance.cMaxSpeed = mECMAdvanced.CreateSpeedSPNItem(mDistance.cMaxSpeed, true);
                        }
                    }
                    if (bTrailerTrack)
                    {
                        #region Decode the Trailer Track data
                        if ((retMsg = mTrailerTrack.Decode(aData, pos)) != null) return retMsg;
                        pos += mTrailerTrack.Length;
                        #endregion
                    }
                    if (bExtendedVariableDetails)
                    {
                        if ((retMsg = mExtendedVariableDetails.Decode(aData, ref pos, cInputNumber, cRefrigerationZone)) != null) return retMsg;
                    }
                    if (cMsgType == STATUS_IGNITION_OFF && (int)cInputNumber == 0 || this.cPacketTotal == (byte)0x05)
                    {
                        if (this.cPacketNum == 9 || this.cPacketTotal == (byte)0x05)
                        {
                            if ((retMsg = mEngineSummaryData.Decode(aData, pos)) != null) return retMsg;
                            pos += GPEngineSummaryData.Length;
                            //set the engine summary data onto the advanced section so that it is saved to database
                            if (mECMAdvanced != null)
                            {
                                mECMAdvanced.SetEngineSummaryData(mEngineSummaryData);
                            }

                        }
                        try
                        {
                            // RPM Zones are being used to send total engine hours on an ignition off report.
                            mTransportExtraDetails.iRPMZone1 = 0;
                            mTransportExtraDetails.iRPMZone2 = 0;
                            mTransportExtraDetails.iRPMZone3 = 0;
                            mTransportExtraDetails.iRPMZone4 = 0;
                            // If the mEngineSummaryData section was not read for some reason, get the total engine hours from mTransportExtraDetails
                            if (mTransportExtraDetails.iEngineHours > 0 && mEngineSummaryData.iTotalEngineHours == 0)
                                mEngineSummaryData.iTotalEngineHours = mTransportExtraDetails.iEngineHours;
                        }
                        catch (Exception)
                        {
                        }
                    }

                    if (hasRoadTypeData)
                    {
                        if (RoadTypeData == null)
                        {
                            RoadTypeData = new RoadTypeData();
                        }

                        retMsg = RoadTypeData.Decode(aData, ref pos);
                        if (retMsg != null)
                        {
                            return retMsg;
                        }
                    }

                }
			}

            if (cMsgType == ECM_HYSTERESIS_ERROR)
            {
                EcmError = new EcmHysteresisError();
                EcmError.Decode(aData, ref pos);
            }

			return null;
		}

		public string DecodeRawGPS(byte[] aData)
		{
			if (aData.Length > 0)
			{
				sRawGPS = System.Text.Encoding.ASCII.GetString(aData);
				return null;
			}
			else return "No Raw GPS Payload found";
		}
        public string DecodeAck(byte[] aData)
        {
            if (aData.Length >= 6)
            {
                int versionNumber = (int)aData[0];
                ServerLoadLevel = (int)aData[1];
                ServerLoadStatusReport = PacketUtilities.Read4ByteNumberFromBuffer(aData, 2);
            }
            return null;
        }

		public string Decode (byte[] theData)
		{
			string problem = null;
			if (!bIsPopulated)	problem = base.Decode(theData, 0);
		
			if (problem != null) return problem;

			if (cMsgType == GEN_IM_ALIVE)
				problem = DecodeAlivePacket(mDataField);
            else if (cMsgType == GEN_ACK)
                problem = DecodeAck(mDataField);
            else if (cMsgType == STATUS_RAW_GPS)
				problem = DecodeRawGPS(mDataField);
			else if (cMsgType == GEN_IM_ALIVE_IMIE)
				problem = DecodeIMIEAlivePacket(mDataField);
			else if (cMsgType == GEN_IM_ALIVE_IMEI_ESN)
				problem = DecodeAliveIMEIESNPacket(mDataField);	//	POD 02092005 - New Logon Packet
			else if (cMsgType == GEN_IM_ALIVE_FULL)
				problem = DecodeAliveFullPacket(mDataField);
			else
				problem = DecodeGeneralPacket(mDataField);

			return problem;
		}

		private byte[] EncodeStatusReport()
		{
			int pos = 0;
			byte[] bTemp = null;
			bool bEngineData = false;
			bool bExtraDetails = false;
			bool bTrailerTrack = false;
			bool bExtendedVariableDetails = false;
            bool bECMAdvanced = false;
			DateTime dtDateTime = DateTime.Now.ToUniversalTime();
			byte[] payload = new byte[1500];
			byte[] payload_returned = null;
            bool hasRoadTypeData = false;

			#region Encode any special bytes for the message type
			switch (cMsgType)
			{
				case ANGEL_GEAR_ALERT_END:
				case ANGEL_GEAR_ALERT_START:
				case BARREL_ABOVE_ONSITE_SPEED:
				case BARREL_CONCRETE_AGE_MINS:
				case BARREL_CONCRETE_AGE_ROTATIONS:
				case BARREL_LEFT_LOADER:
				case BARREL_LOADING_STARTED:
				case BARREL_MIX_STARTED:
				case BARREL_NOT_MIXED:
				case BARREL_RPM_FORWARD:
				case BARREL_RPM_REVERSED:
				case BARREL_RPM_STOPPED:
				case BARREL_RPM_UP_TO_SPEED:
				case ENGINE_FUEL_ECONOMY_ALERT:
				case FATIGUE_REPORT_24:
				case FATIGUE_REPORT_IGNON:
				case GEN_EXCESSIVE_IDLE:
				case GEN_EXCESSIVE_IDLE_END:
				case GEN_OVERSPEED:
				case GPS_HISTORY:
				case OVERSPEED_ZONE_1_START:
				case OVERSPEED_ZONE_2_START:
				case OVERSPEED_ZONE_3_START:
				case OVERSPEED_ZONE_1_END:
				case OVERSPEED_ZONE_2_END:
				case OVERSPEED_ZONE_3_END:
				case STATUS_DATA_USAGE_ALERT:
				case STATUS_POS_REPORT:
				case STATUS_REPORT:
				case STATUS_TRAILER_DEHITCH:
				case STATUS_TRAILER_HITCH:
				case GeneralGPPacket.GEN_NEW_NETWORK_FOUND:
				case GeneralGPPacket.GEN_NO_GPS_ANT_OPEN:
				case GeneralGPPacket.GEN_NO_GPS_ANT_SHORT:
                case GeneralGPPacket.SAT_PING:
                case LOW_POWER_MODE:
                    break;
				case GEN_IOONOFF:
					payload[pos++] = cButtonNumber;
					break;
                case GEN_OUTPUT:
                    payload[pos++] = (byte) OutputState;
                    break;
				case EXTENDED_IO_BELOW_THRESHOLD:
				case EXTENDED_IO_ABOVE_THRESHOLD:
				case STATUS_IGNITION_ON:
				case STATUS_IGNITION_OFF:
                case STATUS_VEHICLE_BREAK_START:
                case STATUS_VEHICLE_BREAK_END:
                case DRIVER_PERSONAL_KMS:
                case GEN_MDT_STATUS_REPORT:
                case STATUS_LOGIN:
                case STATUS_LOGOUT:
                    payload[pos++] = cInputNumber;
                    break;
                case ALARM_LOW_BATTERY:
                    payload[pos++] = cInputNumber;
                    if (cInputNumber >= 5 && cInputNumber <= 8)
                    {
                        PacketUtilities.Write2ByteNumberToBuffer(payload, OldReportingFrequency, pos);
                        pos += 2;
                        PacketUtilities.Write2ByteNumberToBuffer(payload, NewReportingFrequency, pos);
                        pos += 2;
                    }
					break;
                case GFORCE_CALIBRATION:
                    payload[pos++] = cInputNumber;
                    payload[pos++] = (byte)GFCalibrationReason;
                    if (cInputNumber == 1)
                    {
                        PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, OldGFCalibration1);
                        PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, OldGFCalibration2);
                        PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, NewGFCalibration1);
                        PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, NewGFCalibration2);
                    }
                    else if (cInputNumber == 2)
                    {
                        PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, OldGFCalibration1);
                        PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, NewGFCalibration1);
                    }
                    break;
                case GPS_SETTINGS:
                    PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, GPSSettings);
                    break;
                case DIAGNOSTIC_LEVEL:
                    payload[pos++] = (byte)DiagnosticsLevel;
                    break;
                case DIAGNOSTIC:
                    payload[pos++] = (byte)DiagnosticsHardwareType;
                    payload[pos++] = (byte)DiagnosticsLevel;
                    PacketUtilities.WriteToPacketAtPos(ref payload, ref pos, Diagnostics);
                    break;
                case TAMPER:
                    payload[pos++] = cInputNumber;
                    break;
                case REFRIG_REPORT:
					payload[pos++] = cInputNumber;
					payload[pos++] = cRefrigerationZone;
					break;
                case DRIVER_OVERSPEED_REPORT:
                    // Top nibble is the speed zone ID and Bottom nibble is speed zone flags
                    byte bSpeedZoneData = (byte)((bSpeedZoneID << 4) + (bSpeedZoneFlags & 0x0F));
                    payload[pos++] = bSpeedZoneData;      // Speed zone ID and speed zone flags
                    payload[pos++] = bSpeedZoneLimit;     // Speed zone speed limit
                    break;
				case STATUS_IO_PULSES:
					payload[pos++] = cInputNumber;
					bTemp = BitConverter.GetBytes(iPulseCount);
					payload[pos++] = bTemp[0];
					payload[pos++] = bTemp[1];
					break;
				case STATUS_ACCIDENT_NEW:
                case POSSIBLE_ACCIDENT:
					payload[pos++] = cAccidentIndex;
					break;
				case GEN_STARTUP:
				case GEN_ACK:
				case GEN_NAK:
					return payload_returned;
				case ENGINE_OVER_RPM:
				case ENGINE_OVER_COOLANT_TEMP:
				case ENGINE_OVER_OIL_TEMP:
				case ENGINE_UNDER_OIL_PRESSURE_LOW:
				case ENGINE_UNDER_OIL_PRESSURE_HIGH:
				case ENGINE_G_FORCES_HIGH:
				case ENGINE_COOLANT_LEVEL_LOW:
				case ENGINE_G_FORCE_CALIBRATED_RPT:
				case ENGINE_ERROR_CODE:
					bTemp = BitConverter.GetBytes(iEngineValue);
					payload[pos++] = bTemp[0];
					payload[pos++] = bTemp[1];
					payload[pos++] = bTemp[2];
					payload[pos++] = bTemp[3];
					break;
				case GeneralGPPacket.STATUS_RX_TX:
					bTemp = BitConverter.GetBytes(iUnitTXBytes);
					payload[pos++] = bTemp[0];
					payload[pos++] = bTemp[1];
					payload[pos++] = bTemp[2];
					payload[pos++] = bTemp[3];
					bTemp = BitConverter.GetBytes(iUnitRXBytes);
					payload[pos++] = bTemp[0];
					payload[pos++] = bTemp[1];
					payload[pos++] = bTemp[2];
					payload[pos++] = bTemp[3];
					bTemp = BitConverter.GetBytes(iProtocolVer);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(dtDateTime.Day);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(dtDateTime.Month);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(dtDateTime.Year - 2000);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(dtDateTime.Hour);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(dtDateTime.Minute);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(dtDateTime.Second);
					payload[pos++] = bTemp[0];
					bTemp = BitConverter.GetBytes(iSlotNumber);
					payload[pos++] = bTemp[0];
					if (sSimNumber.Length > 0)
					{
						bTemp = System.Text.ASCIIEncoding.ASCII.GetBytes(sSimNumber);
						for (int X = 0; X < bTemp.Length; X++)
						{
							payload[pos++] = bTemp[X];
						}
					}
					payload[pos++] = (byte)0x0A;
					if (sNetworkName.Length > 0)
					{
						bTemp = System.Text.ASCIIEncoding.ASCII.GetBytes(sNetworkName);
						for (int X = 0; X < bTemp.Length; X++)
						{
							payload[pos++] = bTemp[X];
						}
					}
					payload[pos++] = (byte)0x0A;
					break;
				case SETPT_ARRIVE:
				case SETPT_DEPART:
				case SETPT_DURATION_OVER:
				case SETPT_DURATION_UNDER:
				case SETPT_SPECIAL_1_ARRIVE:
				case SETPT_SPECIAL_1_DEPART:
				case WAYPT_ARRIVE_WORKSHOP:
				case WAYPT_DEPART_WORKSHOP:
				case WAYPT_OVER_SPEED:
				case WAYPT_ARRIVE_DOCK:
				case WAYPT_DEPART_DOCK:
				case WAYPT_ARRIVE_NOGO:
				case WAYPT_DEPART_NOGO:
				case WAYPT_HOLDING_EXPIRY:
					payload[pos++] = (byte)mExtendedVariableDetails.CurrentSetPointGroup;
					payload[pos++] = (byte)(mExtendedVariableDetails.CurrentSetPointNumber & 0x00FF);
					break;
                case ECM_HYSTERESIS_ERROR:
                    EcmError.Encode(ref payload, pos);
                    pos += EcmHysteresisError.LENGTH;
                    break;
                default:
					break;
			}
			#endregion

			mCurrentClock.Encode(ref payload, pos);
			pos += GPClock.Length;

			mFixClock.Encode(ref payload, pos);
			pos += GPClock.Length;

			mFix.Encode(ref payload, pos);
			pos += GPPositionFix.Length;

			mStatus.Encode(ref payload, pos);
			pos += mStatus.Length;

			mDistance.Encode(ref payload, pos);
			pos += GPDistance.Length;

			if (this.cPacketTotal == (byte)0x01)
			{
				bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
				bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
				bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
				bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                bECMAdvanced = false;
            }
            else if (this.cPacketTotal == (byte)0x03)
            {
                bEngineData = false;
                bExtraDetails = false;
                bTrailerTrack = ((this.cPacketNum & (byte)0x0C) == (byte)0x0C);
                bExtendedVariableDetails = true;
                bECMAdvanced = true;
            }
            else if (this.cPacketTotal == (byte)0x04 || this.cPacketTotal == (byte)0x05 || this.cPacketTotal == (byte)0x06 || cPacketTotal == (byte)0x07)
            {
                bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                bECMAdvanced = true;

                if (cPacketTotal == 0x07)
                {
                    hasRoadTypeData = true;
                }
            }
			
            if (this.cPacketTotal == (byte)0x06)
            {
                //need to add 31 bytes of additional advanced ECM data that is not used at present
                //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                pos += 31;
            }
            if (cMsgType == GeneralGPPacket.ALRM_UNIT_POWERUP)
            {
                mUnitStartupReport.Encode(ref payload, pos);
            }
            else
            {
                if (bEngineData && mEngineData != null)
                {
                    mEngineData.Encode(ref payload, pos);
                    pos += GPEngineData.Length;
                    if (mExtendedValues != null)
                    {
                        mExtendedValues.Encode(ref payload, pos);
                        pos += GPExtendedValues.Length;
                    }
                }
                if (bExtraDetails && mTransportExtraDetails != null)
                {
                    mTransportExtraDetails.Encode(ref payload, pos);
                    pos += GPTransportExtraDetails.Length;
                }
                if (bTrailerTrack && mTrailerTrack != null)
                {
                    mTrailerTrack.Encode(ref payload, pos);
                    pos += mTrailerTrack.Length;
                }
                if (bECMAdvanced && mECMAdvanced != null)
                {
                    mECMAdvanced.Encode(ref payload, pos);
                    pos += mECMAdvanced.Length;
                }
                if (bExtendedVariableDetails && mExtendedVariableDetails != null)
                {
                    mExtendedVariableDetails.Encode(ref payload, pos);
                    pos += mExtendedVariableDetails.Length;
                }
                if (cMsgType == STATUS_IGNITION_OFF || this.cPacketTotal == (byte)0x05)
                {
                    if (this.cPacketNum == 9 || this.cPacketTotal == (byte)0x05)
                    {
                        mEngineSummaryData.Encode(ref payload, pos);
                        pos += GPEngineSummaryData.Length;
                    }
                }

                if (hasRoadTypeData && RoadTypeData != null)
                {
                    RoadTypeData.Encode(ref payload, pos);
                }
            }
			payload_returned = new byte[pos];
			for (int i = 0; i < pos; i++)
				payload_returned[i] = payload[i];
			return payload_returned;
		}

		public new void Encode(ref byte[] theData)
		{
            int writePos = 0;
            MemoryStream oMS = new MemoryStream();

			switch (cMsgType)
			{
				case GEN_STARTUP:
                case GEN_NO_GPS_ANT_OPEN:
                case GEN_NO_GPS_ANT_SHORT:
					iLength = 0;
					mDataField = null;
					break;
                case GEN_ACK:
                    PacketUtilities.WriteToStream(oMS, (byte)1);
                    PacketUtilities.WriteToStream(oMS, (byte)ServerLoadLevel);
                    PacketUtilities.WriteToStream(oMS, ServerLoadStatusReport);
                    mDataField = oMS.ToArray();
                    iLength = (uint) mDataField.Length;
                    break;
                case GEN_IM_ALIVE_FULL:
                    #region encoding an GEN_IM_ALIVE_FULL
                    PacketUtilities.WriteToStream(oMS, (byte)iHardwareTypeInbound);
                    PacketUtilities.WriteToStream(oMS, sSimNumber.PadLeft(25, '0').Substring(0, 25));
                    PacketUtilities.WriteToStream(oMS, (byte)iSlotNumber);
                    PacketUtilities.WriteToStream(oMS, (byte)iPreferedSlotNumber);
                    PacketUtilities.WriteToStream(oMS, sNetworkName.PadRight(20, '\0').Substring(0, 20));
                    PacketUtilities.WriteToStream(oMS, sIMEIInbound.PadRight(20, ' ').Substring(0, 20));
                    PacketUtilities.WriteToStream(oMS, Convert.ToUInt32(lSerialNumber));
                    PacketUtilities.WriteToStream(oMS, (byte)iSoftwareVersion);
                    PacketUtilities.WriteToStream(oMS, cProgramMajor);
                    PacketUtilities.WriteToStream(oMS, (byte)iProgramMinor);
                    PacketUtilities.WriteToStream(oMS, (byte)iHardwareVersionNumber);
                    PacketUtilities.WriteToStream(oMS, Convert.ToUInt32(lIOBoxSerialNumber));
                    PacketUtilities.WriteToStream(oMS, cIOBoxProgramMajor);
                    PacketUtilities.WriteToStream(oMS, cIOBoxProgramMinor);
                    PacketUtilities.WriteToStream(oMS, (byte)iIOBoxHardwareVersionNumber);
                    PacketUtilities.WriteToStream(oMS, Convert.ToUInt32(lMDTSerialNumber));
                    PacketUtilities.WriteToStream(oMS, (byte)iMDTProgramMajor);
                    PacketUtilities.WriteToStream(oMS, (byte)iMDTProgramMinor);
                    PacketUtilities.WriteToStream(oMS, (byte)iMDTHardwareVer);
                    PacketUtilities.WriteToStream(oMS, (byte)iLoginFlags);
                    if (SoftwareBuild > 0 || SoftwareBranch > 0 || SoftwareBuildOptions > 0)
                    {
                        PacketUtilities.WriteToStream(oMS, (byte)1);
                        PacketUtilities.WriteToStream(oMS, (byte)8);
                        PacketUtilities.WriteToStream(oMS, (short)SoftwareBuild);
                        PacketUtilities.WriteToStream(oMS, (short)SoftwareBranch);
                        PacketUtilities.WriteToStream(oMS, SoftwareBuildOptions);
                    }
                    mDataField = oMS.ToArray();
                    iLength = (uint) mDataField.Length;
                    break;
                    #endregion
                case GEN_IM_ALIVE:
					#region encoding an IM_ALIVE
					iLength = GEN_IM_ALIVE_LENGTH;
					mDataField = new byte[GEN_IM_ALIVE_LENGTH];
					// Serial:
					mDataField[0] = (byte) (lSerialNumber & 0xFF);
					mDataField[1] = Convert.ToByte((lSerialNumber >> 8) & 0xFF);
					mDataField[2] = Convert.ToByte((lSerialNumber >> 16) & 0xFF);
					mDataField[3] = Convert.ToByte((lSerialNumber >> 24) & 0xFF);
					// SIM Number:
					mDataField[4] = 0x11;
					mDataField[5] = 0x22;
					mDataField[6] = 0x33;
					mDataField[7] = 0x44;
					mDataField[8] = 0x55;
					mDataField[9] = 0x66;
					// Program Numbers:
					mDataField[10] = 0x01;
					mDataField[11] = 0x02;
					mDataField[12] = 0x03;
					break;
					#endregion
				case GEN_IM_ALIVE_IMEI_ESN :
				{
					#region Encoding IM_ALIVE_IMEI_ESN
					iLength = GEN_IM_ALIVE_IMEI_ESN_LENGTH;
					mDataField = new byte[GEN_IM_ALIVE_IMEI_ESN_LENGTH];

					//	Hardware Type
					mDataField[0] = Convert.ToByte((int)iHardwareTypeInbound);
                    
					// bytes 12 - 14 (1-based) are serial number
					// Translates to bytes 1-4 in this payload
					mDataField[1] = (byte) (lSerialNumber & 0xFF);
					mDataField[2] = Convert.ToByte((lSerialNumber >> 8) & 0xFF);
					mDataField[3] = Convert.ToByte((lSerialNumber >> 16) & 0xFF);
					mDataField[4] = Convert.ToByte((lSerialNumber >> 24) & 0xFF);
					
					//POD This code handles non-BCD encoded string
					byte[] ASCIIbytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sSimNumber);
					writePos = 5;
					for(int loop = 0; loop < 25;loop++)
					{
						if (loop < ASCIIbytes.Length)
							mDataField[writePos++] = ASCIIbytes[loop];
						else
							mDataField[writePos++] = 0;
					}

					// Program Version Numbers:
					mDataField[writePos++] = cProgramMajor;
					mDataField[writePos++] = (byte) (iProgramMinor & 0xFF);
					mDataField[writePos++] = Convert.ToByte((iProgramMinor >> 8) & 0xFF);

					mDataField[writePos++] = cIOBoxProgramMajor;
					mDataField[writePos++] = cIOBoxProgramMinor;

					string sIMEI_ESN_Number = "";
					switch(iHardwareTypeInbound)
					{
						case Alive_IMEIESN_HardwareType.Platform_3020_IMEI :
						case Alive_IMEIESN_HardwareType.Platform_3021_IMEI :
						case Alive_IMEIESN_HardwareType.Platform_3025_IMEI :
						{
							sIMEI_ESN_Number = sIMEIInbound + "05";
							break;
						}
						case Alive_IMEIESN_HardwareType.Platform_3022_CDMAESN :
						{
							sIMEI_ESN_Number = sCDMAESNInbound;
							break;
						}
						default :
						{
							break;
						}
					}
					ASCIIbytes = System.Text.ASCIIEncoding.ASCII.GetBytes(sIMEI_ESN_Number);
					for(int loop = 0; loop < 20;loop++)
					{
						if (loop < ASCIIbytes.Length)
							mDataField[writePos++] = ASCIIbytes[loop];
						else
							mDataField[writePos++] = 0;
					}


					#endregion
					break;
				}
				case STATUS_REPORT:
				case STATUS_TRAILER_HITCH:
				case STATUS_TRAILER_DEHITCH:
				case OVERSPEED_ZONE_1_START:
				case OVERSPEED_ZONE_2_START	:
				case OVERSPEED_ZONE_3_START:
				case OVERSPEED_ZONE_1_END:
				case OVERSPEED_ZONE_2_END:
				case OVERSPEED_ZONE_3_END:
				case ANGEL_GEAR_ALERT_START:
				case ANGEL_GEAR_ALERT_END:
				case FATIGUE_REPORT_IGNON:
				case FATIGUE_REPORT_24:
				case GeneralGPPacket.ENGINE_FUEL_ECONOMY_ALERT:
				case GeneralGPPacket.STATUS_DATA_USAGE_ALERT:
				case GeneralGPPacket.BARREL_CONCRETE_AGE_MINS:
				case GeneralGPPacket.BARREL_CONCRETE_AGE_ROTATIONS:
                case ALARM_LOW_BATTERY:
                case ECM_HYSTERESIS_ERROR:
                case LOW_POWER_MODE:
                default:
					mDataField = EncodeStatusReport();
					break;
			}
			// and encode this down:
			base.Encode(ref theData);
		}


	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class ConfigDownloaded
    {
        #region Private Member Vars
        private bool bIsPopulated;
        private int _PacketVer;
        private DateTime _dtGPSTime;
        private Dictionary<string, string> _content;
        #endregion
        #region Public Properties
        public int PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public DateTime GPSTime
        {
            get { return _dtGPSTime; }
            set { _dtGPSTime = value; }
        }
        public int ItemCount
        {
            get
            {
                int ret = 0;
                if (_content != null)
                    ret = _content.Count;
                return ret;
            }
        }
        public List<string> Keys
        {
            get {
                List<String> ret = null;
                if(_content != null)
                {
                    string[] keys = new string[_content.Keys.Count];
                    _content.Keys.CopyTo(keys, 0);
                    ret = new List<string>(keys);
                }
                return ret;
            }
        }
        public List<string> Values
        {
            get {
                List<String> ret = null;
                if(_content != null)
                {
                    ret = new List<string>();
                    string[] keys = new string[_content.Keys.Count];
                    _content.Keys.CopyTo(keys, 0);
                    foreach(string itemKey in keys)
                    {
                        ret.Add(_content[itemKey]);
                    }
                }
                return ret;
            }
        }
        public List<Tuple<string, string>> ListValues
        {
            get
            {
                List<Tuple<string, string>> ret = null;
                if(_content != null)
                {
                    ret = new List<Tuple<string, string>>();
                    string[] keys = new string[_content.Keys.Count];
                    _content.Keys.CopyTo(keys, 0);
                    foreach(string itemKey in keys)
                    {
                        Tuple<string, string> item = new Tuple<string,string>(itemKey, _content[itemKey]);
                        ret.Add(item);
                    }
                }
                return ret;
            }
        }

        public void AddValue(string itemKey, string itemValue)
        {
            try
            {
                if(_content == null)
                    _content = new Dictionary<string,string>();
                _content.Add(itemKey, itemValue);
            }
            catch(System.Exception ex)
            {
            }
        }

        public bool IsPopulated
        {
            get { return bIsPopulated; }
            set { bIsPopulated = value; }
        }
        #endregion
        public ConfigDownloaded()
        {
            _PacketVer = 0;
            _dtGPSTime = DateTime.Now.ToUniversalTime();
            _content = new Dictionary<string,string>();
            bIsPopulated = false;
        }
        public ConfigDownloaded CreateCopy()
        {
            ConfigDownloaded oData = new ConfigDownloaded();
            oData.PacketVer = _PacketVer;
            oData.GPSTime = _dtGPSTime;
            if(_content != null)
            {
                foreach(string itemKey in _content.Keys)
                {
                    oData.AddValue(itemKey, _content[itemKey]);
                }
            }
            return oData;
        }
        public string Decode(Byte[] aData)
        {
            return Decode(aData, 0);
        }
        public string Decode(Byte[] aData, int aStartPos)
        {
            byte bTemp = (byte)0x00;
            int iDay;
            int iMonth;
            int iYear;
            int iHour;
            int iMinute;
            int iSecond;
            int index = aStartPos;

            try
            {
                bTemp = aData[index++];
                _PacketVer = (int)bTemp;

                iDay = (int)aData[index++];
                iMonth = (int)aData[index++];
                iYear = (int)aData[index++];
                iYear += 2000;
                iHour = (int)aData[index++];
                iMinute = (int)aData[index++];
                iSecond = (int)aData[index++];

                try
                {
                    _dtGPSTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond);
                }
                catch (System.Exception)
                {
                    _dtGPSTime = DateTime.Now.ToUniversalTime();
                }
                int entries = (int) aData[index++];
                int state = 1;
                int counter = 0;
                _content = new Dictionary<string,string>();
                string itemKey = "";
                string itemValue = "";
                StringBuilder sb = new StringBuilder();
                while (counter < entries)
                {
                    bTemp = aData[index++];
                    if(state == 1)
                    {
                        #region Get the Item Key string, up until a : is encountered, raise an exception if ; is encountered first
                        if((char) bTemp == ';')
                        {
                            throw new System.Exception("Error parsing ConfigDownloadPacket did not found ; before : in items list. Index = " + (index -1).ToString() + " Bytes : " + BitConverter.ToString(aData, 0));
                        }
                        if((char) bTemp == ':')
                        {
                            // Get the item key value
                            itemKey = sb.ToString();
                            // Reset the string builder to collect the item value.
                            sb = new StringBuilder();
                            // Set the state to collect the item value.
                            state = 2;
                        }
                        else
                            // Keep collecing the item key.
                            sb.Append((char) bTemp);
                        #endregion
                    }
                    else if(state == 2)
                    {
                        #region Get the Item value string, up until a ; is encountered, raise an exception if : is encountered first
                        if((char) bTemp == ':')
                        {
                            throw new System.Exception("Error parsing ConfigDownloadPacket did not found : before ; in items list. Index = " + (index - 1).ToString() + " Bytes : " + BitConverter.ToString(aData, 0));
                        }
                        if((char) bTemp == ';')
                        {
                            // Add the new values to the collection
                            try
                            {
                                _content.Add(itemKey, sb.ToString());
                            }
                            catch(System.Exception ex)
                            {
                                // Duplicate Keys...
                            }
                            // Create a new string builder
                            sb = new StringBuilder();
                            // Switch back to looking for item key.
                            state = 1;
                            // Add one to the entry count
                            counter++;
                        }
                        else
                            // Keep collecing the item value.
                            sb.Append((char) bTemp);
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }
        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;

            using(MemoryStream ms = new MemoryStream())
            {
                ms.WriteByte((byte)_PacketVer);
                ms.WriteByte((byte)_dtGPSTime.Day);
                ms.WriteByte((byte)_dtGPSTime.Month);
                ms.WriteByte((byte)(_dtGPSTime.Year - 2000));
                ms.WriteByte((byte)_dtGPSTime.Hour);
                ms.WriteByte((byte)_dtGPSTime.Minute);
                ms.WriteByte((byte)_dtGPSTime.Second);
                if(_content == null || _content.Count == 0)
                {
                    ms.WriteByte((byte)0);
                }
                else
                {
                    ms.WriteByte((byte)_content.Count);
                    foreach(string itemKey in _content.Keys)
                    {
                        byte[] itemKeyAndValue = System.Text.ASCIIEncoding.ASCII.GetBytes(itemKey + ":" + _content[itemKey] + ";");
                        ms.Write(itemKeyAndValue, 0, itemKeyAndValue.Length);
                    }
                    ms.WriteByte((byte) '\0');
                }
                bTemp = ms.ToArray();
            }
            if(bTemp != null && bTemp.Length > 0)
            {
                for (int X = 0; X < bTemp.Length; X++)
                    theData[aStartPos++] = bTemp[X];
            }
        }
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nSattelite Data Usage :");
                builder.Append("\r\n    Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n    Time : "); builder.Append(_dtGPSTime.ToString("dd/MM/yyyy HH:mm:ss"));
                builder.Append("\r\n    List : "); 
                foreach(string itemKey in _content.Keys)
                {
                    builder.Append("\r\n        " + itemKey + " : " + _content[itemKey]);
                }
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Config Downloaded : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for MailMessagePacket.
	/// </summary>
    [Serializable]
    public class MailMessagePacket : GatewayProtocolPacket
	{
		public const byte MAIL_MESSAGE = 0x80;
		public const int MAX_MAIL_MESSAGE_LENGTH = 500;
		public const int MAX_MAIL_ATTACHMENT_SIZE = (MAX_MAIL_MESSAGE_LENGTH *
													Byte.MaxValue);

		#region Variables

		public static int MinimumPacketLength = 9;	// An empty message is 9 bytes
		public string	sTargetUser; // the sender/receiver of this message
		public bool	bUrgent;	// Urgency flag
		public bool		bHasAttachment;	// Has an attachment
		private bool	bIsAttachmentPart; // This message IS part of an attachment
		private byte	cAttachmentPartNumber;	// The total number of attachment parts (If 
												// this is the message packet, OR,
												// the number of THIS attachment part
		public string	sSentTime;	// Time of original message creation
		public string  sSubject;	// Subject line
		public string	sMessage;	// Message text

		private byte[] mAttachmentBytes;
		#endregion

		public string sConstructorDecodeError = null;

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\nbUrgent : "); builder.Append(bUrgent);
			builder.Append("\r\nbHasAttachment : "); builder.Append(bHasAttachment);
			builder.Append("\r\nbIsAttachmentPart : "); builder.Append(bIsAttachmentPart);
			builder.Append("\r\ncAttachmentPartNumber : "); builder.Append(cAttachmentPartNumber);
			builder.Append("\r\nsSentTime : "); builder.Append(sSentTime);
			builder.Append("\r\nsSubject : "); builder.Append(sSubject);
			builder.Append("\r\nsMessage : "); builder.Append(sMessage);

			return builder.ToString();
		}

		public MailMessagePacket(string serverTime_DateFormat) : base (serverTime_DateFormat)
		{
			cMsgType = MAIL_MESSAGE;
			sConstructorDecodeError = null;
		}

		public MailMessagePacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			cMsgType = MAIL_MESSAGE;
			sConstructorDecodeError = Decode(p.mDataField);
			bIsPopulated = (sConstructorDecodeError == null);
		}


		/// <summary>
		/// Attempt to add an attachment to the message.
		/// If the attachment is too large (currently the limit
		/// is 255*500 bytes = 124Kb) the call will fail - also
		/// if there is already an attachment present
		/// </summary>
		/// <param name="sAttachmentName"></param>
		/// <param name="attachmentBytes"></param>
		/// <returns></returns>
		public bool SetAttachment(	string sAttachmentName, 
									byte[] attachmentBytes)
		{
			// Already have an attachment:
			if (bHasAttachment) return false;

			if (attachmentBytes.Length > MAX_MAIL_ATTACHMENT_SIZE) return false;

			bHasAttachment = true;

			// work out the total:
			cAttachmentPartNumber = Convert.ToByte(
				(attachmentBytes.Length / MAX_MAIL_MESSAGE_LENGTH));
			if ((attachmentBytes.Length % MAX_MAIL_MESSAGE_LENGTH) != 0)
				cAttachmentPartNumber++;

			mAttachmentBytes = attachmentBytes;
			return true;
		}
		public string Decode (byte[] rawPacket)
		{

			int decPos = 0;
			bIsPopulated = false;
			#region Length Error Checking
			int iTotalLength;
			if (rawPacket.Length < MinimumPacketLength) return "Mail Packet too short";
			iTotalLength = Convert.ToInt32(rawPacket[decPos++]) << 8;
			iTotalLength += Convert.ToInt32(rawPacket[decPos++]);
			if (rawPacket.Length != iTotalLength) 
				return  "Mail Packet length mismatch - length: " + rawPacket.Length +
						" length field: " + iTotalLength;
			#endregion
			#region Flags and attachment data
			bUrgent = false;
			if ((rawPacket[decPos] & 0x01) == 0x01) bUrgent = true;
			if ((rawPacket[decPos] & 0x02) == 0x02) bHasAttachment = true;
			if ((rawPacket[decPos++] & 0x04) == 0x04) bIsAttachmentPart = true;
			cAttachmentPartNumber = rawPacket[decPos++];
			#endregion
			#region Length fields
			int cTargetNameLength, cSubjectLength, cSentTimeLength, iMessageLength;
			cTargetNameLength = rawPacket[decPos++];
			cSubjectLength =	rawPacket[decPos++];
			cSentTimeLength =	rawPacket[decPos++];
			iMessageLength = Convert.ToInt32(rawPacket[decPos++]) << 8;
			iMessageLength += Convert.ToInt32(rawPacket[decPos++]);
			// Check the sum of the fields is the same as packet length - overhead:
			if (	(cTargetNameLength + cSubjectLength + 
				cSentTimeLength + iMessageLength) 
				!= (iTotalLength - MinimumPacketLength)) return "Mail Packet field length mismatch";
			#endregion			
			#region populate fields
			// target user begins at a fixed location:
			sTargetUser = Encoding.ASCII.GetString(	rawPacket, decPos, cTargetNameLength);
			decPos += cTargetNameLength;
			// Subject:
			sSubject = Encoding.ASCII.GetString(rawPacket, decPos, cSubjectLength);
			decPos += cSubjectLength;
			// Sent time:
			sSentTime = Encoding.ASCII.GetString(rawPacket, decPos,	cSentTimeLength);
			decPos += cSentTimeLength;
			// Message Text:
			sMessage = Encoding.ASCII.GetString(rawPacket, decPos, iMessageLength);

			#endregion
			// All done.
			bIsPopulated = true;
			return null;
		}


		public new void Encode(ref byte[] theData)
		{
			int encPos = 0;
			#region Set up

			sSentTime = System.DateTime.Now.ToString();

			int iTotalLength = MinimumPacketLength +
				sTargetUser.Length +
				sSubject.Length +
				sSentTime.Length +
				sMessage.Length;

			if (mDataField == null)
			{
				mDataField = new byte[iTotalLength];
			}
			else if (mDataField.Length != iTotalLength)
			{
				mDataField = new byte[iTotalLength];
			}
			#endregion
			#region Header
			mDataField[encPos++] = Convert.ToByte((iTotalLength & 0xFF00) >> 8);
			mDataField[encPos++] = Convert.ToByte(iTotalLength & 0xFF);
			#region flags field
			mDataField[encPos] = 0x00;
			if (bUrgent) mDataField[encPos] |= 0x01;
			if (bHasAttachment) mDataField[encPos] |= 0x02;
			if (bIsAttachmentPart) mDataField[encPos] |= 0x04;
			encPos++;
			#endregion
			mDataField[encPos++] = cAttachmentPartNumber;
			mDataField[encPos++] = Convert.ToByte(sTargetUser.Length & 0xff);
			mDataField[encPos++] = Convert.ToByte(sSubject.Length & 0xff);
			mDataField[encPos++] = Convert.ToByte(sSentTime.Length & 0xff);
			mDataField[encPos++] = Convert.ToByte((sMessage.Length & 0xFF00) >> 8);
			mDataField[encPos++] = Convert.ToByte(sMessage.Length & 0xFF);
			#endregion
			#region Encode the text fields
			System.Text.Encoding.ASCII.GetBytes(sTargetUser,0, sTargetUser.Length, 
				mDataField, encPos);
			encPos += sTargetUser.Length;
			System.Text.Encoding.ASCII.GetBytes(sSubject,0, sSubject.Length, 
				mDataField, encPos);
			encPos += sSubject.Length;
			System.Text.Encoding.ASCII.GetBytes(sSentTime,0, sSentTime.Length, 
				mDataField, encPos);
			encPos += sSentTime.Length;
			System.Text.Encoding.ASCII.GetBytes(sMessage,0, sMessage.Length, 
				mDataField, encPos);
			#endregion
			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
	}
}


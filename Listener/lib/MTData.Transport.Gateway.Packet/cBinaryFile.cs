using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;


namespace MTData.Transport.Gateway.Packet
{
    /// <summary>
    /// Summary description for cBinaryFile.
    /// </summary>
    [Serializable]
    public class cBinaryFile
    {
        public string sError = "";
       
        private readonly List<byte[]> oFileArray = new List<byte[]>();
      
        private int iNextItem = 0;
     
        private uint iProgramCheckSum = 0;
      
        private uint iProgramByteCount = 0;

        public cBinaryFile(string sFileName, int iSplitLength)
        {
            try
            {
                using (FileStream fs = new FileStream(sFileName, FileMode.Open))
                {
                    InitialiseFromStream(fs, iSplitLength);
                }
            }
            catch (System.Exception ex)
            {
                //oFileArray = new ArrayList();
                oFileArray.Clear();
                sError = "File Read Error : " + ex.Message;
            }
        }

        public cBinaryFile(byte[] fileBytes, int iSplitLength)
        {
            try
            {
                using (MemoryStream ms = new MemoryStream(fileBytes))
                {
                    InitialiseFromStream(ms, iSplitLength);
                }
            }
            catch (System.Exception ex)
            {
                //oFileArray = new ArrayList();
                oFileArray.Clear();
                sError = "File Read Error : " + ex.Message;
            }
        }

        private void InitialiseFromStream(Stream ms, int iSplitLength)
        {
            int iData = 0;
            int iPos = 0;
            byte[] bData = null;
            byte[] bTemp = null;
            byte bByte = (byte)0x00;

            bData = new byte[iSplitLength];
            iData = ms.ReadByte();
            while (iData >= 0)
            {
                iProgramByteCount++;
                bByte = ((byte[])BitConverter.GetBytes(iData))[0];
                iProgramCheckSum += (uint)bByte;
                bData[iPos++] = bByte;
                if (iPos == iSplitLength)
                {
                    oFileArray.Add(bData);
                    bData = new byte[iSplitLength];
                    iPos = 0;
                }
                iData = ms.ReadByte();
            }
            if (iPos > 0)
            {
                bTemp = new byte[iPos];
                for (int X = 0; X < iPos; X++)
                {
                    bTemp[X] = bData[X];
                }
                oFileArray.Add(bTemp);
            }
            ms.Close();
        }

        public byte[] NextBlock
        {
            get
            {
                byte[] bRet = null;
                if (iNextItem < oFileArray.Count)
                    bRet = oFileArray[iNextItem++];
                return bRet;
            }
        }

        public byte[] GetBlock(int iBlock)
        {
            byte[] bRet = null;
            if (iBlock < oFileArray.Count)
                bRet = oFileArray[iBlock];
            return bRet;
        }

        public int TotalBlockCount
        {
            get
            {
                return oFileArray.Count;
            }
        }

        public uint ProgramCheckSum
        {
            get
            {
                return iProgramCheckSum;
            }
        }
     
        public uint ProgramByteCount
        {
            get
            {
                return iProgramByteCount;
            }
        }
    }
}

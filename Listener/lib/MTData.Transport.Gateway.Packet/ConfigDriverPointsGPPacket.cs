using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class ConfigDriverPointsGPPacket : GatewayProtocolPacket
    {
        #region Private Member Vars
//        [Packet Version][GroupID] [GroupMajorVersion] [GroupMinorVersion]
//[NumTrackingRules] { [RuleId] [Points] [Input1] [Input2] [ReasonCode] }
//[NumEcmRules] { [RuleId] [Points] [Input1] [Input2] [SpnCode] [EcmValue] [Condition] [Duration] [Reset] }
//[NumIORules] { [RuleId] [Points] [Input1] [Input2] [Input] [State] [VehicleSpeed] [Duration] [Reset]}
//[NumMdtRules] { [RuleId] [Points] [Input1] [Input2] [MdtType] [VehicleSpeed] [Duration] [Reset]}

        public const byte CONFIG_DRIVER_POINTS_RULE = 0xD7;

        private byte _PacketVer;
        private int _GroupID;
        private byte _GroupVerMajor;
        private byte _GroupVerMinor;
        private List<ConfigDriverPointsTrkRules> _TrackingRules;
        private List<ConfigDriverPointsECMRules> _ECMRules;
        private List<ConfigDriverPointsIORules> _IORules;
        private List<ConfigDriverPointsMDTRules> _MDTRules;
        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        private string _constructorDecodeError;
        private string _serverTime_DateFormat;
        #endregion
       
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public int GroupID
        {
            get { return _GroupID; }
            set { _GroupID = value; }
        }
        public byte GroupVerMajor
        {
            get { return _GroupVerMajor; }
            set { _GroupVerMajor = value; }
        }
        public byte GroupVerMinor
        {
            get { return _GroupVerMinor; }
            set { _GroupVerMinor = value; }
        }
        public List<ConfigDriverPointsTrkRules> TrackingRules
        {
            get { return _TrackingRules; }
            set { _TrackingRules = value; }
        }
        public List<ConfigDriverPointsECMRules> ECMRules
        {
            get { return _ECMRules; }
            set { _ECMRules = value; }
        }
        public List<ConfigDriverPointsIORules> IORules
        {
            get { return _IORules; }
            set { _IORules = value; }
        }
        public List<ConfigDriverPointsMDTRules> MDTRules
        {
            get { return _MDTRules; }
            set { _MDTRules = value; }
        }
        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        public string ConstructorDecodeError { get { return _constructorDecodeError; } }
        #endregion

        #region constructor
        public ConfigDriverPointsGPPacket(string serverTime_DateFormat) : base (serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }
        public ConfigDriverPointsGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
            Initialise(serverTime_DateFormat);
            _constructorDecodeError = DecodePayload();
            bIsPopulated = (_constructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = CONFIG_DRIVER_POINTS_RULE;
            bIsFileType = true;

            _PacketVer = 0;
            bIsPopulated = false;
            _GroupID = 0;
            _GroupVerMajor = 0;
            _GroupVerMinor = 0;
            _TrackingRules = new List<ConfigDriverPointsTrkRules>();
            _ECMRules = new List<ConfigDriverPointsECMRules>();
            _IORules = new List<ConfigDriverPointsIORules>();
            _MDTRules = new List<ConfigDriverPointsMDTRules>();
            _constructorDecodeError = null;
        }
        #endregion

        public new ConfigDriverPointsGPPacket CreateCopy()
        {
            ConfigDriverPointsGPPacket oData = new ConfigDriverPointsGPPacket(this, _serverTime_DateFormat);
            oData.PacketVer = _PacketVer;
            oData.GroupID = _GroupID;
            oData._GroupVerMajor = _GroupVerMajor;
            oData.GroupVerMinor = _GroupVerMinor;
            foreach (ConfigDriverPointsTrkRules t in _TrackingRules)
            {
                oData.TrackingRules.Add(t.CreateCopy());
            }
            foreach (ConfigDriverPointsECMRules e in _ECMRules)
            {
                oData.ECMRules.Add(e.CreateCopy());
            }
            foreach (ConfigDriverPointsIORules i in _IORules)
            {
                oData.IORules.Add(i.CreateCopy());
            }
            foreach (ConfigDriverPointsMDTRules m in _MDTRules)
            {
                oData.MDTRules.Add(m.CreateCopy());
            }
            return oData;
        }
    
        public string Decode(Byte[] data)
        {
            string problem = null;
            if (!bIsPopulated) problem = base.Decode(data, 0);
            if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

            return problem;
        }
        public string DecodePayload()
        {
            try
            {
                MemoryStream oMS = new MemoryStream(mDataField, 0, mDataField.Length);
                byte bTemp = (byte)0x00;
               PacketUtilities.ReadFromStream(oMS, ref _PacketVer);
               PacketUtilities.ReadFromStream(oMS, ref _GroupID);
               PacketUtilities.ReadFromStream(oMS, ref _GroupVerMajor);
               PacketUtilities.ReadFromStream(oMS, ref _GroupVerMinor);
               PacketUtilities.ReadFromStream(oMS, ref bTemp);
                for (int X = 0; X < (int)bTemp; X++)
                {
                    ConfigDriverPointsTrkRules oRule = new ConfigDriverPointsTrkRules();
                    string sRet = oRule.Decode(ref oMS, _PacketVer);
                    if (sRet != null) return sRet;
                    _TrackingRules.Add(oRule);
                }
               PacketUtilities.ReadFromStream(oMS, ref bTemp);
                for (int X = 0; X < (int)bTemp; X++)
                {
                    ConfigDriverPointsECMRules oRule = new ConfigDriverPointsECMRules();
                    string sRet = oRule.Decode(ref oMS, _PacketVer);
                    if (sRet != null) return sRet;
                    _ECMRules.Add(oRule);
                }
               PacketUtilities.ReadFromStream(oMS, ref bTemp);
                for (int X = 0; X < (int)bTemp; X++)
                {
                    ConfigDriverPointsIORules oRule = new ConfigDriverPointsIORules();
                    string sRet = oRule.Decode(ref oMS, _PacketVer);
                    if (sRet != null) return sRet;
                    _IORules.Add(oRule);
                }
               PacketUtilities.ReadFromStream(oMS, ref bTemp);
                for (int X = 0; X < (int)bTemp; X++)
                {
                    ConfigDriverPointsMDTRules oRule = new ConfigDriverPointsMDTRules();
                    string sRet = oRule.Decode(ref oMS, _PacketVer);
                    if (sRet != null) return sRet;
                    _MDTRules.Add(oRule);
                }
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }

        public new void Encode(ref Byte[] theData)
        {
            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _PacketVer);
           PacketUtilities.WriteToStream(oMS, _GroupID);
           PacketUtilities.WriteToStream(oMS, _GroupVerMajor);
           PacketUtilities.WriteToStream(oMS, _GroupVerMinor);
           PacketUtilities.WriteToStream(oMS, (byte)_TrackingRules.Count);
            foreach (ConfigDriverPointsTrkRules oRule in _TrackingRules)
                oRule.Encode(ref oMS);
           PacketUtilities.WriteToStream(oMS, (byte)_ECMRules.Count);
            foreach (ConfigDriverPointsECMRules oRule in _ECMRules)
                oRule.Encode(ref oMS);
           PacketUtilities.WriteToStream(oMS, (byte)_IORules.Count);
            foreach (ConfigDriverPointsIORules oRule in _IORules)
                oRule.Encode(ref oMS);
           PacketUtilities.WriteToStream(oMS, (byte)_MDTRules.Count);
            foreach (ConfigDriverPointsMDTRules oRule in _MDTRules)
                oRule.Encode(ref oMS);
            mDataField = oMS.ToArray();

            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
        }

        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nDriver Points Configuration :");
                builder.Append("\r\n    Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n    Group ID : "); builder.Append(_GroupID);
                builder.Append("\r\n    Group Ver Major : "); builder.Append(_GroupVerMajor);
                builder.Append("\r\n    Group Ver Minor : "); builder.Append(_GroupVerMinor);
                builder.Append("\r\n    Tracking Rules : "); builder.Append(_TrackingRules.Count + " items.");
                foreach (ConfigDriverPointsTrkRules oRule in _TrackingRules)
                    builder.Append(oRule.ToString());
                builder.Append("\r\n    ECM Rules : "); builder.Append(_ECMRules.Count + " items.");
                foreach (ConfigDriverPointsECMRules oRule in _ECMRules)
                    builder.Append(oRule.ToString());
                builder.Append("\r\n    IO Rules : "); builder.Append(_IORules.Count + " items.");
                foreach (ConfigDriverPointsIORules oRule in _IORules)
                    builder.Append(oRule.ToString());
                builder.Append("\r\n    MDT Rules : "); builder.Append(_MDTRules.Count + " items.");
                foreach (ConfigDriverPointsMDTRules oRule in _MDTRules)
                    builder.Append(oRule.ToString());
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Sattelite Data Usage : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

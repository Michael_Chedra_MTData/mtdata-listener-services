using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class GPSendScriptFile : GatewayProtocolPacket
    {
        public const byte FIRMWARE_SEND_SHELL_SCRIPT = 0xE8;
        public const byte FIRMWARE_SHELL_SCRIPT_EOS = 0xFF;
        #region Private Member Vars
        private ushort _maxSegmentLength = 250;
        private byte _PacketVer;
        private byte _subCmd;
        private string _filename;
        private uint _filelen;
        private ushort _fileId;
        private ushort _checksum;
        List<byte[]> _segments;
        List<byte[]> _segmentChecksums;
        private byte _errorCode;
        private uint _totalSegments;
        private int _segmentNumber;
        private ushort _segmentLength;
        private byte[] _segmentData;
        private ushort _segmentChecksum;
        private string _serverTime_DateFormat;

        #endregion
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public byte SubCommand
        {
            get { return _subCmd; }
            set { _subCmd = value; }
        }
        public byte[] FileBytes
        {
            get
            {
                return GetFileBytes();
            }
            set
            {
                SetFileBytes(value);
            }
        }
        public int SegmentNumber
        {
            get
            {
                return _segmentNumber;
            }
            set
            {
                _segmentNumber = value;
            }
        }
        private byte[] GetFileBytes()
        {
            byte[] ret = null;
            try
            {

            }
            catch (Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + ".GetFileBytes() : " + ex.Message);
            }
            return ret;
        }
        private void SetFileBytes(byte[] value)
        {
            uint check = 0;
            uint segmentCheck = 0;
            ushort tempChecksum = 0;
            ushort counter = 0;
            List<byte> data;

            _segments = new List<byte[]>();                         // Create a new segments list
            _segmentChecksums = new List<byte[]>();                 // Create a new segement checksum list

            if (value == null)                                      // If the file has no data
            {
                _filelen = 0;                                       // Set the file length to 0
                _checksum = 0;                                      // Set the file checksum to 0
            }
            else
            {
                _filelen = (uint)value.Length;                      // Set the file length value
                data = new List<byte>();                            // Create a new list of bytes to collect the bytes for each segment.
                for (int X = 0; X < value.Length; X++)              // For each byte in the file data
                {
                    // Get a two byte check sum
                    check += (uint)value[X];                        // Add the value of the current byte to the total
                    segmentCheck += (uint)value[X];
                    check = check & 0xFFFF;                         // Keep the bottom two bytes (i.e. roll off the top two bytes)
                    segmentCheck = segmentCheck & 0xFFFF;

                    data.Add(value[X]);                             // Add to this segments bytes
                    counter++;                                      // Increment the counter of bytes in this segment
                    if(counter >= _maxSegmentLength)                // If we have reached a segment limit
                    {
                        byte[] segmentData = new byte[data.Count];  // Create a new byte array
                        data.CopyTo(segmentData, 0);                // Copy the bytes to the new array
                        _segments.Add(segmentData);                 // Add the byte array to the list of segments
                        tempChecksum = (ushort)(segmentCheck & 0xFFFF); // Get the segment chech sum as an ushort
                        _segmentChecksums.Add(BitConverter.GetBytes(tempChecksum));    // Add the segment checksum to the list.
                        data = new List<byte>();                    // Create a new list for the next segment
                        segmentCheck = 0;                           // Reset the segment checksum
                        counter = 0;                                // Reset the segment byte counter
                    }
                }
                _checksum = (ushort)(segmentCheck & 0xFFFF);        // Get the files overall checksum
            }
        }
        public string FileName
        {
            get
            {
                return _filename;
            }
            set
            {
                _filename = value;
            }
        }
        public uint FileLength
        {
            get
            {
                return _filelen;
            }
        }
        public ushort FileId
        {
            get
            {
                return _fileId;
            }
            set
            {
                _fileId = value;
            }
        }
        public int TotalSegments
        {
            get
            {
                if(_segments != null)
                    return _segments.Count;
                return 0;
            }
        }
        public ushort SegmentSizeInBytes
        {
            get
            {
                return _maxSegmentLength;
            }
            set
            {
                _maxSegmentLength = value;
                SetFileBytes(GetFileBytes());
            }            
        }
        public List<byte[]> Segments
        {
            get
            {
                return _segments;
            }
            set
            {
                _segments = value;
            }
        }
        public List<byte[]> SegmentChecksums
        {
            get
            {
                return _segmentChecksums;
            }
            set
            {
                _segmentChecksums = value;
            }
        }
        public byte ErrorCode
        {
            get
            {
                return _errorCode;
            }
            set
            {
                _errorCode = value;
            }
        }
        
        #endregion
        #region Constructor and CreateCopy functions
        public GPSendScriptFile(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = FIRMWARE_SEND_SHELL_SCRIPT;
            _PacketVer = 0;
            SetFileBytes(new byte[0]);
        }
        public GPSendScriptFile(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            Decode(p.mDataField);
            cMsgType = FIRMWARE_SEND_SHELL_SCRIPT;
        }
        public new GPSendScriptFile CreateCopy()
        {
            GPSendScriptFile oData = new GPSendScriptFile(_serverTime_DateFormat);
            oData.PacketVer = _PacketVer;
            oData.SubCommand = _subCmd;
            oData.FileName = _filename;
            oData.SegmentSizeInBytes = _maxSegmentLength;
            oData.FileBytes = GetFileBytes();
            oData.FileId = _fileId;
            return oData;
        }
        #endregion
        #region Encoding / Decoding
        public string Decode(Byte[] aData)
        {
            MemoryStream oMS = new MemoryStream(aData, 0, aData.Length);
            return Decode(ref oMS);
        }
        public string Decode(Byte[] aData, ref int index)
        {
            MemoryStream oMS = new MemoryStream(aData, index, aData.Length - index);
            string sRet = Decode(ref oMS);
            if (sRet != null) return sRet;
            index += (int)oMS.Position;
            return null;
        }
        public string Decode(ref MemoryStream oMS)
        {
            //
            // Header Packet (SubCmd = 0x00)
            //          [PacketVer][SubCmd][FileNameLen][FileName][EOS   ][FileID  ][DataLen   ][Checksum][TotalSegments][EOS]
            // Bytes :  [1 Byte]   [1 Byte][1 Byte]     [X Bytes] [1 Byte][2 ushort][4 byte int][2 UShort][4 byte int]   [1 Byte]   
            //
            // Request Segment Data Packet (SubCmd = 0x01)
            //          [PacketVer][SubCmd][FileID  ][Segment Number]
            // Bytes :  [1 byte]   [1 byte][2 ushort][4 byte int]
            //
            // Segment Data Packet (SubCmd = 0x02)
            //          [PacketVer][SubCmd][FileID  ][Segment Number][Segment Len][Segment Data][Segment CheckSum]
            // Bytes :  [1 byte]   [1 byte][2 ushort][4 byte int]    [2 ushort]   [X bytes]     [2 ushort]
            //
            // Cancel Download Packet (SubCmd = 0x03)
            //          [PacketVer][SubCmd][FileID  ]
            // Bytes :  [1 byte]   [1 byte][2 ushort]
            //
            //
            // Download Complete Packet (SubCmd = 0x04)
            //          [PacketVer][SubCmd][FileID  ][Error Code]
            // Bytes :  [1 byte]   [1 byte][2 ushort][1 byte]
            //
            try
            {                
                byte bData = (byte)0x00;
               PacketUtilities.ReadFromStream(oMS, ref _PacketVer);                  // [PacketVer]
               PacketUtilities.ReadFromStream(oMS, ref _subCmd);                     // [SubCmd]
                if (_subCmd == (byte) 0x00)
                {
                    // [PacketVer][SubCmd][FileNameLen][FileName][EOS][FileID][DataLen][Checksum][TotalSegments]
                   PacketUtilities.ReadFromStream(oMS, ref bData);                   // [FileNameLen]
                   PacketUtilities.ReadFromStream(oMS, (int)bData, ref _filename);   // [FileName]
                   PacketUtilities.ReadFromStream(oMS, ref bData);                   // [EOS]
                    if (bData != FIRMWARE_SHELL_SCRIPT_EOS)
                        throw (new Exception("Did not find FIRMWARE_SHELL_SCRIPT_EOS at end of filename string."));
                   PacketUtilities.ReadFromStream(oMS, ref _fileId);                 // [FileID]
                   PacketUtilities.ReadFromStream(oMS, ref _filelen);                // [DataLen]
                   PacketUtilities.ReadFromStream(oMS, ref _checksum);               // [Checksum]
                   PacketUtilities.ReadFromStream(oMS, ref _totalSegments);          // [TotalSegments]
                    for(int X = 0; X < _totalSegments; X++)
                    {
                        _segments.Add(new byte[0]);
                        _segmentChecksums.Add(new byte[0]);
                    }
                }
                else if (_subCmd == (byte)0x01)
                {
                    // [PacketVer][SubCmd][FileID][Segment Number]
                   PacketUtilities.ReadFromStream(oMS, ref _fileId);                 // [FileID]
                   PacketUtilities.ReadFromStream(oMS, ref _segmentNumber);          // [Segment Number]
                }
                else if (_subCmd == (byte)0x02)
                {
                    // [PacketVer][SubCmd][FileID][Segment Number][Segment Len][Segment Data][CheckSum]
                   PacketUtilities.ReadFromStream(oMS, ref _fileId);                 // [FileID]
                   PacketUtilities.ReadFromStream(oMS, ref _segmentNumber);          // [Segment Number]
                   PacketUtilities.ReadFromStream(oMS, ref _segmentLength);          // [Segment Len]
                    _segmentData = new byte[_segmentLength];                                    // [Segment Data]
                   PacketUtilities.ReadFromStream(oMS, _segmentLength, ref _segmentData);
                   PacketUtilities.ReadFromStream(oMS, ref _segmentChecksum);        // [Segment CheckSum]
                    while(_segments.Count < _segmentNumber)
                    {
                        _segments.Add(new byte[0]);
                        _segmentChecksums.Add(new byte[0]);
                    }
                    _segments[_segmentNumber] = _segmentData;
                    _segmentChecksums[_segmentNumber] = _segmentData;
                }
                else if (_subCmd == (byte)0x03)
                {
                    // [PacketVer][SubCmd][FileID]
                   PacketUtilities.ReadFromStream(oMS, ref _fileId);                 // [FileID]
                }
                else if (_subCmd == (byte)0x04)
                {
                    // [PacketVer][SubCmd][FileID][ErrorCode]
                   PacketUtilities.ReadFromStream(oMS, ref _fileId);                 // [FileID]
                   PacketUtilities.ReadFromStream(oMS, ref _errorCode);              // [ErrorCode]
                }
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }

        public void Encode()
        {
            EncodeDataField();
        }

        public void EncodeDataField()
        {
            //
            // Header Packet (SubCmd = 0x00)
            //          [PacketVer][SubCmd][FileNameLen][FileName][EOS   ][FileID  ][DataLen   ][Checksum][TotalSegments][EOS]
            // Bytes :  [1 Byte]   [1 Byte][1 Byte]     [X Bytes] [1 Byte][2 ushort][4 byte int][2 UShort][4 byte int]   [1 Byte]   
            //
            // Request Segment Data Packet (SubCmd = 0x01)
            //          [PacketVer][SubCmd][FileID  ][Segment Number]
            // Bytes :  [1 byte]   [1 byte][2 ushort][4 byte int]
            //
            // Segment Data Packet (SubCmd = 0x02)
            //          [PacketVer][SubCmd][FileID  ][Segment Number][Segment Len][Segment Data][Segment CheckSum]
            // Bytes :  [1 byte]   [1 byte][2 ushort][4 byte int]    [2 ushort]   [X bytes]     [2 ushort]
            //
            // Cancel Download Packet (SubCmd = 0x03)
            //          [PacketVer][SubCmd][FileID  ]
            // Bytes :  [1 byte]   [1 byte][2 ushort]
            //
            //
            // Download Complete Packet (SubCmd = 0x04)
            //          [PacketVer][SubCmd][FileID  ][Error Code]
            // Bytes :  [1 byte]   [1 byte][2 ushort][1 byte]
            //

            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _PacketVer);

            byte bData = (byte)0x00;
           PacketUtilities.WriteToStream(oMS, _PacketVer);                  // [PacketVer]
           PacketUtilities.WriteToStream(oMS, _subCmd);                     // [SubCmd]
            if (_subCmd == (byte)0x00)
            {
                // [PacketVer][SubCmd][FileNameLen][FileName][EOS][FileID][DataLen][Checksum][TotalSegments]
                bData = (byte)_filename.Length;
               PacketUtilities.WriteToStream(oMS, bData);                   // [FileNameLen]
               PacketUtilities.WriteToStream(oMS, _filename);               // [FileName]
               PacketUtilities.WriteToStream(oMS, FIRMWARE_SHELL_SCRIPT_EOS); // [EOS]
               PacketUtilities.WriteToStream(oMS, _fileId);                 // [FileID]
               PacketUtilities.WriteToStream(oMS, _filelen);                // [DataLen]
               PacketUtilities.WriteToStream(oMS, _checksum);               // [Checksum]
               PacketUtilities.WriteToStream(oMS, _segments.Count);         // [TotalSegments]
            }
            else if (_subCmd == (byte)0x01)
            {
                // [PacketVer][SubCmd][FileID][Segment Number]
               PacketUtilities.WriteToStream(oMS, _fileId);                 // [FileID]
               PacketUtilities.WriteToStream(oMS, _segmentNumber);          // [Segment Number]
            }
            else if (_subCmd == (byte)0x02)
            {
                // [PacketVer][SubCmd][FileID][Segment Number][Segment Len][Segment Data][CheckSum]
               PacketUtilities.WriteToStream(oMS, _fileId);                 // [FileID]
               PacketUtilities.WriteToStream(oMS, _segmentNumber);          // [Segment Number]
                ushort segmentLength = 0;
                if (_segments.Count > _segmentNumber)
                {
                    segmentLength = (ushort)_segments[_segmentNumber].Length;
                   PacketUtilities.WriteToStream(oMS, segmentLength);            // [Segment Len]
                   PacketUtilities.WriteToStream(oMS, _segments[_segmentNumber]); // [Segment Data]
                   PacketUtilities.WriteToStream(oMS, _segmentChecksums[_segmentNumber]); // [Segment CheckSum]
                }
                else
                {
                   PacketUtilities.WriteToStream(oMS, segmentLength);         // [Segment Len]
                   PacketUtilities.WriteToStream(oMS, (ushort) 0);            // [Segment CheckSum]                    
                }
            }
            else if (_subCmd == (byte)0x03)
            {
                // [PacketVer][SubCmd][FileID]
               PacketUtilities.WriteToStream(oMS, _fileId);                 // [FileID]
            }
            else if (_subCmd == (byte)0x04)
            {
                // [PacketVer][SubCmd][FileID][ErrorCode]
               PacketUtilities.WriteToStream(oMS, _fileId);                 // [FileID]
               PacketUtilities.WriteToStream(oMS, _errorCode);              // [ErrorCode]
            }
            mDataField = oMS.ToArray();
        }
        #endregion
        #region ToString functions
        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nSend Script File Packet :");
                builder.Append("\r\n\tPacket Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n\tSub Command : "); builder.Append((int) _subCmd);
                if (_subCmd == (byte)0x00)
                {
                    builder.Append("Header Packet");
                    // [PacketVer][SubCmd][FileNameLen][FileName][EOS][FileID][DataLen][Checksum][TotalSegments]
                    builder.Append("\r\n\tFilename : "); builder.Append(_filename);
                    builder.Append("\r\n\tFile ID : "); builder.Append(_fileId);
                    builder.Append("\r\n\tFile Length : "); builder.Append(_filelen);
                    builder.Append("\r\n\tChecksum : "); builder.Append(_checksum);
                    builder.Append("\r\n\tTotal Segments : "); builder.Append(_segments.Count);
                }
                else if (_subCmd == (byte)0x01)
                {
                    builder.Append("Request Segment Data Packet");
                    // [PacketVer][SubCmd][FileID][Segment Number]
                    builder.Append("\r\n\tFile ID : "); builder.Append(_fileId);
                    builder.Append("\r\n\tSegment Number : "); builder.Append(_segmentNumber);
                }
                else if (_subCmd == (byte)0x02)
                {
                    builder.Append("Segment Data Packet");
                    // [PacketVer][SubCmd][FileID][Segment Number][Segment Len][Segment Data][CheckSum]
                    builder.Append("\r\n\tFile ID : "); builder.Append(_fileId);
                    builder.Append("\r\n\tSegment Number : "); builder.Append(_segmentNumber);
                    ushort segmentLength = 0;
                    if (_segments.Count > _segmentNumber)
                    {
                        segmentLength = (ushort)_segments[_segmentNumber].Length;
                        builder.Append("\r\n\tSegment Length : "); builder.Append(segmentLength);
                        builder.Append("\r\n\tSegment Data : "); builder.Append(BitConverter.ToString(_segments[_segmentNumber]));
                        builder.Append("\r\n\tSegment CheckSum : "); builder.Append(BitConverter.ToString(_segmentChecksums[_segmentNumber]));
                    }
                    else
                    {
                        builder.Append("\r\n\tSegment Length : 0");
                        builder.Append("\r\n\tSegment Data : null");
                        builder.Append("\r\n\tSegment CheckSum : 0");
                    }
                }
                else if (_subCmd == (byte)0x03)
                {
                    builder.Append("Cancel Download Packet");
                    // [PacketVer][SubCmd][FileID]
                    builder.Append("\r\n\tFile ID : "); builder.Append(_fileId);
                }
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nSend Script File Packet : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

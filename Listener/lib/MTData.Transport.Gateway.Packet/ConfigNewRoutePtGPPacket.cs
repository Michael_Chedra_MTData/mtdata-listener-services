using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigNewRoutePtGPPacket.
	/// </summary>
    [Serializable]
    public class ConfigNewRoutePtGPPacket : GatewayProtocolPacket
	{
		public const Byte CONFIG_NEW_ROUTE = 0xd2;
		
		#region Variables
		public byte cRouteSetNumber;
		public byte cPageAdjust;
		public int iRouteVersion;
		public byte cNumPoints;
		public GPRoutePoint[] mPointsList;
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
		#endregion
		
		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncRouteSetNumber : "); builder.Append(cRouteSetNumber);
			builder.Append("\r\ncPageAdjust : "); builder.Append(cPageAdjust);
			builder.Append("\r\niRouteVersion : "); builder.Append(iRouteVersion);
			builder.Append("\r\ncNumPoints : "); builder.Append(cNumPoints);
			if ((mPointsList != null) && (mPointsList.Length > 0))
				for(int loop = 0; loop < mPointsList.Length; loop++)
				{
					builder.Append("\r\n  Point "); 
					builder.Append(loop); 
					builder.Append(" - "); 
					builder.Append(mPointsList[loop].ToString());
				}
			return builder.ToString();
		}

		public ConfigNewRoutePtGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			cMsgType = CONFIG_NEW_ROUTE;
			bIsFileType = true;
			mPointsList = new GPRoutePoint[100];
			ClearRouteData();
		}

		public ConfigNewRoutePtGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			cMsgType = CONFIG_NEW_ROUTE;
			bIsFileType = true;
			mPointsList = new GPRoutePoint[100];
			ClearRouteData();
		}

		public void AddPoint(GPRoutePoint aPoint)
		{
			mPointsList[cNumPoints++] = aPoint;
		}

		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0, pointNum;
			int checksum = 0;
			
			// Make sure we have sufficient space:
			iLength = (uint) 21 + (uint) (cNumPoints * GPRoutePoint.Length);
			if (mDataField.Length != iLength)
			{
				mDataField = new Byte[iLength];
			}

			mDataField[encPos++] = GSP_SOH;
			mDataField[encPos++] =	cRouteSetNumber;
			mDataField[encPos++] =	(Byte) ((iRouteVersion & 0xFF00) >> 8);
			mDataField[encPos++] =	(Byte) (iRouteVersion & 0xFF);
			
			// Size of this data
			mDataField[encPos++] = (Byte) ((iLength & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (iLength & 0xFF);

			mDataField[encPos++] =	cPageAdjust;
			encPos += 10;			// 10 bytes of Spare

			mDataField [encPos++] = (Byte) cNumPoints;

			// Encode each point
			for (pointNum = 0; pointNum < cNumPoints; pointNum++)
			{
				mPointsList[pointNum].Encode(ref mDataField, encPos);
				encPos += GPRoutePoint.Length;
			}
			encPos++;	// Spare byte

			// Throw in a checksum:
			for (int pos = 0; pos < encPos; pos++)
			{
				checksum += mDataField[pos];
			}
			// Write in the checksum and the end-of-message
			mDataField[encPos++] = Convert.ToByte(checksum & 0xFF);

			// Finish the packet
			mDataField[encPos++] = GSP_EOT; 

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}

		private void ClearRouteData()
		{
		cRouteSetNumber = 0;
		cPageAdjust = 0;
		iRouteVersion = 0;
		cNumPoints = 0;
		}
	}
}

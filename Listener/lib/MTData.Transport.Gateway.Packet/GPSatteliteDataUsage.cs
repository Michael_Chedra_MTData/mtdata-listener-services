using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class GPSatteliteDataUsage
    {
        #region Private Member Vars
        private bool bIsPopulated;
        private int _PacketVer;
        private DateTime _dtGPSTime;
        private int _SatVersion;
        private int _TXPackets;
        private int _TXBytes;
        private int _RXPackets;
        private int _RXBytes;
        private int _MailBoxChecks;
        private ushort _MOMSN;
        private ushort _MTMSN;
        private string _IMIE;
        #endregion
        #region Public Properties
        public int PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public DateTime GPSTime
        {
            get { return _dtGPSTime; }
            set { _dtGPSTime = value; }
        }
        public int SatVersion
        {
            get { return _SatVersion; }
            set { _SatVersion = value; }
        }
        public int TXPackets
        {
            get { return _TXPackets; }
            set { _TXPackets = value; }
        }
        public int TXBytes
        {
            get { return _TXBytes; }
            set { _TXBytes = value; }
        }
        public int RXPackets
        {
            get { return _RXPackets; }
            set { _RXPackets = value; }
        }
        public int RXBytes
        {
            get { return _RXBytes; }
            set { _RXBytes = value; }
        }
        public int MailBoxChecks
        {
            get { return _MailBoxChecks; }
            set { _MailBoxChecks = value; }
        }
        public ushort MOMSN
        {
            get { return _MOMSN; }
            set { _MOMSN = value; }
        }
        public ushort MTMSN
        {
            get { return _MTMSN; }
            set { _MTMSN = value; }
        }
        public string IMIE
        {
            get { return _IMIE; }
            set { _IMIE = value; }
        }
        public bool IsPopulated
        {
            get { return bIsPopulated; }
            set { bIsPopulated = value; }
        }
        #endregion
        public GPSatteliteDataUsage()
        {
            _PacketVer = 0;
            _dtGPSTime = DateTime.Now.ToUniversalTime();
            _TXPackets = 0;
            _TXBytes = 0;
            _RXPackets = 0;
            _RXBytes = 0;
            _MailBoxChecks = 0;
            _MOMSN = 0;
            _MTMSN = 0;
            _IMIE = "";
            bIsPopulated = false;
        }
        public GPSatteliteDataUsage CreateCopy()
        {
            GPSatteliteDataUsage oData = new GPSatteliteDataUsage();
            oData.PacketVer = _PacketVer;
            oData.GPSTime = _dtGPSTime;
            oData.SatVersion = _SatVersion;
            oData.TXPackets = _TXPackets;
            oData.TXBytes = _TXBytes;
            oData.RXPackets = _RXPackets;
            oData.RXBytes = _RXBytes;
            oData.MailBoxChecks = _MailBoxChecks;
            oData.MOMSN = _MOMSN;
            oData.MTMSN = _MTMSN;
            oData.IMIE = _IMIE;
            oData.IsPopulated = bIsPopulated;
            return oData;
        }
        public string Decode(Byte[] aData)
        {
            return Decode(aData, 0);
        }
        public string Decode(Byte[] aData, int aStartPos)
        {
            byte bTemp = (byte)0x00;
            int iDay;
            int iMonth;
            int iYear;
            int iHour;
            int iMinute;
            int iSecond;
            int index = aStartPos;

            try
            {
                bTemp = aData[index++];
                _PacketVer = (int)bTemp;

                iDay = (int)aData[index++];
                iMonth = (int)aData[index++];
                iYear = (int)aData[index++];
                iYear += 2000;
                iHour = (int)aData[index++];
                iMinute = (int)aData[index++];
                iSecond = (int)aData[index++];

                try
                {
                    _dtGPSTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond);
                }
                catch (System.Exception)
                {
                    _dtGPSTime = DateTime.Now.ToUniversalTime();
                }
				bTemp = aData[index++];
                _SatVersion = (int)bTemp;
                _TXPackets = BitConverter.ToInt32(aData, index);
                index += 4;
                _TXBytes = BitConverter.ToInt32(aData, index);
                index += 4;
                _RXPackets = BitConverter.ToInt32(aData, index);
                index += 4;
                _RXBytes = BitConverter.ToInt32(aData, index);
                index += 4;
                _MailBoxChecks = BitConverter.ToInt32(aData, index);
                index += 4;
                _MOMSN = BitConverter.ToUInt16(aData, index);
                index += 2;
                _MTMSN = BitConverter.ToUInt16(aData, index);
                index += 2;

				_IMIE = "";
				bTemp = bTemp = aData[index++];
				while (bTemp != 0x0A)
				{
					_IMIE += ((char)bTemp).ToString();
					bTemp = aData[index++];
				}
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }
        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp;

            theData[aStartPos++] = (byte)_PacketVer;
            theData[aStartPos++] = (byte)_dtGPSTime.Day;
            theData[aStartPos++] = (byte)_dtGPSTime.Month;
            theData[aStartPos++] = (byte)(_dtGPSTime.Year - 2000);
            theData[aStartPos++] = (byte)_dtGPSTime.Hour;
            theData[aStartPos++] = (byte)_dtGPSTime.Minute;
            theData[aStartPos++] = (byte)_dtGPSTime.Second;
            theData[aStartPos++] = (byte)_SatVersion;
            bTemp = BitConverter.GetBytes(_TXPackets);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(_TXBytes);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(_RXPackets);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(_RXBytes);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(_MailBoxChecks);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(_MOMSN);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            bTemp = BitConverter.GetBytes(_MTMSN);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            bTemp = ASCIIEncoding.ASCII.GetBytes(_IMIE);
            for(int X = 0; X < bTemp.Length; X++)
                theData[aStartPos++] = bTemp[X];
            theData[aStartPos++] = (byte) 0x0A;
        }
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nSattelite Data Usage :");
                builder.Append("\r\n    Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n    Time : "); builder.Append(_dtGPSTime.ToString("dd/MM/yyyy HH:mm:ss"));
                builder.Append("\r\n    Sat Version : "); builder.Append(_SatVersion);
                builder.Append("\r\n    TX Packets : "); builder.Append(_TXPackets);
                builder.Append("\r\n    TX Bytes : "); builder.Append(_TXBytes);
                builder.Append("\r\n    RX Packets : "); builder.Append(_RXPackets);
                builder.Append("\r\n    RX Bytes : "); builder.Append(_RXBytes);
                builder.Append("\r\n    MailBox Checks : "); builder.Append(_MailBoxChecks);
                builder.Append("\r\n    MOMSN : "); builder.Append(_MOMSN);
                builder.Append("\r\n    MTMSN : "); builder.Append(_MTMSN);
                builder.Append("\r\n    IMIE : "); builder.Append(_IMIE);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Sattelite Data Usage : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

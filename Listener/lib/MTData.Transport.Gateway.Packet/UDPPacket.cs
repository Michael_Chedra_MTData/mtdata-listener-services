using System;
using System.Net;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Holds one or more GatewayProtocol packets.
	/// </summary>
    [Serializable]
    public class UDPPacket
	{
		public uint mNumPackets;
		public GatewayProtocolPacket[] mPackets;
		public Boolean mIsPopulated;
		public IPEndPoint mSenderIP;
		private string _serverTime_DateFormat = "";

		public UDPPacket(string serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mNumPackets = 0;
			mIsPopulated = false;
			// The absolute MOST GP packets we could have in a 1500 byte UDP packet:
			mPackets = new GatewayProtocolPacket[120];
		}


		public string Decode( Byte[] aData)
		{
			int length = aData.Length;
			uint decodePosition = 0;
			string errString;
			mNumPackets = 0;

			while ( decodePosition < length )
			{	
				mPackets[mNumPackets] = new GatewayProtocolPacket(_serverTime_DateFormat);
				if ((errString = mPackets[mNumPackets].Decode(aData, decodePosition)) == null)
				{
					// Worked. Now copy in the IP information from this UDP packet:
					mPackets[mNumPackets].mSenderIP = mSenderIP;
					decodePosition += (mPackets[mNumPackets]).iTotalLength;
					mNumPackets++;
				}
				else 
				{	// this GSP packet is a wreck. Forget about it, but all previous
					// packets within this UDP packet were OK. Cease decoding before
					// anything gets any worse.
					return "Length: " + length + ". Error in GSP packet " + mNumPackets + ":" + errString;
				}
			}
			return null;
		}
	}
}

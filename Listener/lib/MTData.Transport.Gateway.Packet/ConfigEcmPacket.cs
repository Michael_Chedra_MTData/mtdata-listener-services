﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    /// <summary>
    /// configuration packet sent to unit with the ECM hysteresis info
    /// </summary>
    [Serializable]
    public class ConfigEcmPacket : GatewayProtocolPacket
    {
        public const Byte CONFIG_ECM_HYSTERESIS = 0xDF;
        private string _serverTime_DateFormat = "";

        public int ConfigId;
        public int ConfigMajor;
        public int ConfigMinor;
        public List<EcmRule> Rules;

        /// <summary>
        /// object describing ECM rule
        /// </summary>
        public struct EcmRule
        {
            public int RuleId;
            public int SourceType;
            public int SpnCode;
            public int RuleType;
            public float LowValue;
            public float HighValue;
            public int ReportDuring;
            public int NumberOfReportsToBreakOut;
            public int WellKnownType;
            public int PgnCode;
            public int TransmissionRate;
            public int StartByte;
            public int StartBit;
            public int LengthBits;
            public float Multipler;
            public float Offset;
            public int ValueType;
        }

		public ConfigEcmPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
            Rules = new List<EcmRule>();
			cMsgType = CONFIG_ECM_HYSTERESIS;
			bIsFileType = true;
		}

        public ConfigEcmPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
            Rules = new List<EcmRule>();
            cMsgType = CONFIG_ECM_HYSTERESIS;
            bIsFileType = true;
		}

        public new void Encode(ref Byte[] theData)
        {
            MemoryStream stream = new MemoryStream();
            stream.WriteByte((byte)ConfigId);
            stream.WriteByte((byte)ConfigMajor);
            stream.WriteByte((byte)ConfigMinor);
            stream.WriteByte((byte)Rules.Count);
            foreach (EcmRule r in Rules)
            {
                PacketUtilities.WriteToStream(stream, r.RuleId);
                PacketUtilities.WriteToStream(stream, r.SourceType);
                PacketUtilities.WriteToStream(stream, r.SpnCode);
                stream.WriteByte((byte)r.RuleType);
                PacketUtilities.WriteToStream(stream, r.LowValue);
                PacketUtilities.WriteToStream(stream, r.HighValue);
                stream.WriteByte((byte)r.ReportDuring);
                stream.WriteByte((byte)r.NumberOfReportsToBreakOut);
                PacketUtilities.WriteToStream(stream, r.WellKnownType);
                PacketUtilities.WriteToStream(stream, r.PgnCode);
                PacketUtilities.WriteToStream(stream, r.TransmissionRate);
                stream.WriteByte((byte)r.StartByte);
                stream.WriteByte((byte)r.StartBit);
                stream.WriteByte((byte)r.LengthBits);
                PacketUtilities.WriteToStream(stream, r.Multipler);
                PacketUtilities.WriteToStream(stream, r.Offset);
                stream.WriteByte((byte)r.ValueType);
            }

            mDataField = stream.ToArray();
            base.Encode(ref theData);

        }

    }
}

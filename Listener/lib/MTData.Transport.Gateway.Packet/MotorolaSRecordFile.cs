using System;
using System.IO;
using System.Drawing;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for MotorolaSRecordFile.
	/// </summary>
    [Serializable]
    public class MotorolaSRecordFile
	{
		private MotorolaSRecordRow[] rows;
		private int iRowCount; 
		private int iTotalByteCount;
		public bool FileIsGood;
		public string FileName;
		public string sFileFailureReason;
		public byte PadCharacter = 0xFF; // char to insert if padding blocks

		public MotorolaSRecordFile(string sFileName)
		{
			FileIsGood = false;
			FileIsGood = ReadFile(sFileName, null);
			FileName = sFileName;
		}

        public MotorolaSRecordFile(byte[] data)
        {
            FileIsGood = false;
            FileIsGood = ReadFromBytes(data);
            FileName = new Guid().ToString();
        }

		public MotorolaSRecordFile(MotorolaSRecordFile src)
		{
			this.FileIsGood =			src.FileIsGood;
			this.sFileFailureReason =	src.sFileFailureReason;
			this.iRowCount =			src.iRowCount;
			this.iTotalByteCount =		src.iTotalByteCount;
			this.FileName		=		src.FileName;

			this.rows = new MotorolaSRecordRow[this.iRowCount];
			System.Array.Copy(src.rows, 0, this.rows, 0, iRowCount);
		}

		/// <summary>
		/// 
		/// </summary>
		/// <param name="iRow">1-based row number</param>
		/// <returns></returns>
		public MotorolaSRecordRow GetRow(int iRow)
		{
			if (iRow <= iRowCount)
			{
				return rows[(iRow-1)];
			}
			else return null;
		}


		public bool SetByte(Point byteLocation, long byteValue)
		{
			MotorolaSRecordRow sRow = GetRow(byteLocation.Y);
			if (sRow == null) return false;
			return sRow.SetDataByte(byteLocation.X, byteValue);
		}

		public bool GetByte(Point byteLocation, ref byte theByte)
		{
			MotorolaSRecordRow sRow = GetRow(byteLocation.Y);
			if (sRow == null) return false;
			byte[] theBytes = sRow.GetByteArray(byteLocation.X, 1);
			if (theBytes == null) return false;
			if (theBytes.Length == 1)
			{
				theByte = theBytes[0];
				return true;
			}
			return false;
		}

		
		/// <summary>
		/// Gets the next bytes from the MOT "stream"
		/// </summary>
		/// <param name="lStartAddress">Where to begin reading</param>
		/// <param name="iBytesToGet">How many bytes to get</param>
		/// <param name="array">the byte array which will be populated.</param>
		/// <returns></returns>
		public bool GetContiguousByteArray(long lStartAddress,
			int iBytesToGet,
			ref byte[] array,
			ref long lNextAddress,
			bool bPadGaps,
			bool bPadToPageFront)
		{
			int iActualDataLength = 0;
			#region Sanity checks
			// Can't read an address past the last one:
			if (lStartAddress > GetAddressOfRow(rows.Length -1)) return false;
			// Can't read off the end of our file:
			if (iBytesToGet > iTotalByteCount) return false;
			#endregion
			#region Locate to the given address
			int iCurrentRow = 0;
			int iCurrentColumn = 0;
			for (int rowNumber = 0; rowNumber < rows.Length; rowNumber++)
			{
				if (rows[rowNumber].lAddress == lStartAddress)
				{
					// Perfect start point - start of this row!
					iCurrentRow = rowNumber;
					break;
				}

				if ((rows[rowNumber].lAddress < lStartAddress) &&
					(lStartAddress < 
					(rows[rowNumber].lAddress +
					(rows[rowNumber].GetDataBytes().Length))))
				{
					// The start is somewhere in THIS row.
					iCurrentRow = rowNumber;

					// Find the difference - that's our start column
					iCurrentColumn = Convert.ToInt32(lStartAddress - rows[rowNumber].lAddress);
					break;
				}
			}
			if (iCurrentRow == 0) return false; // didn't find the start address
			if (rows[iCurrentRow].mRecordType 
				== MotorolaSRecordRow.MotorolaRecordType.Data24BitEnd)
				return false; // We've hit the last record in the file
			#endregion
			
			int iBytesRead = 0;
			int iBytesWritten = 0;
			bool bContinue = true;
			// Iff the iBytesToGet bytes can be found in
			// a contiguous block from lStartAddress onwards,
			// get them!
			while (bContinue)
			{
				// Get what is left on this line
				byte[] thisRowDataBytes = 
					rows[iCurrentRow].GetByteArray(	iCurrentColumn,
						rows[iCurrentRow].GetDataBytes().Length -
						iCurrentColumn);		
													
											
				// If we are about to read too much, we must reduce the
				// number of bytes we will read from this row:
				int iBytesToFillUserArray = iBytesToGet - iBytesRead;
				if ((iBytesRead + thisRowDataBytes.Length) > iBytesToGet) 
				{
					// Start from the same place, but end early.
					thisRowDataBytes = 
						rows[iCurrentRow].GetByteArray(	iCurrentColumn,
														iBytesToFillUserArray);
					bContinue = false;
				}

				iBytesRead += thisRowDataBytes.Length;

				// Expand the returned array 
				if ((array == null) || (array.Length < iBytesRead))
				{
					byte[] newArray = new byte[iBytesRead];
					if (array != null)
					{
						Array.Copy(array, newArray, array.Length);
					}
					array = newArray;	
				}
				// and populate it:
				Array.Copy(thisRowDataBytes, 0, array, iBytesWritten, thisRowDataBytes.Length);
				iBytesWritten += thisRowDataBytes.Length;	

				long thisRowAddress = rows[iCurrentRow].lAddress;
				lNextAddress = thisRowAddress + iCurrentColumn + thisRowDataBytes.Length;
				// See if the next row follows contiguously:
				if (lNextAddress == rows[iCurrentRow + 1].lAddress)
				{
					// Excellent - it's contiguous
					iCurrentRow++;
					// We can read the entire row the front on the next pass
					iCurrentColumn = 0;
				}
				else
				{
					if (bContinue)
					{
						// The data on the next line
						// starts at a non-contiguous location.
						// We must PAD the intervening gap...
						if (bPadGaps)
						{
							// Fudge the next address to be that on the next row:
							long lNextRowAddress = rows[iCurrentRow + 1].lAddress;

							// find the amount of pad required:
							int iBytesToPad = Convert.ToInt32(lNextRowAddress - 
								lNextAddress);

							// If it's going to be too much, we'll just
							// round off this block with padding
							if ((iBytesToPad > (iBytesToGet - iBytesWritten)) ||
								(rows[iCurrentRow+1].mRecordType == 
								MotorolaSRecordRow.MotorolaRecordType.Data24BitEnd))
							{
								iBytesToPad = (iBytesToGet - iBytesWritten);
								bContinue = false;
							}

							iBytesRead += iBytesToPad;
							// insert the pad characters: 
							if (array.Length < iBytesRead)
							{
								byte[] newArray = new byte[iBytesRead];
								int oldLength = array.Length;
								Array.Copy(array, newArray, oldLength);
								array = newArray;	
							}
							// and populate:
							for (int pos = iBytesWritten; pos < iBytesRead; pos++)
							{
								array[pos] = PadCharacter;
							}
							iActualDataLength = iBytesWritten;
							iBytesWritten += iBytesToPad;
							// Start the next row from scratch:
							lNextAddress = lNextRowAddress;
							iCurrentRow++;
							iCurrentColumn = 0;
						}
						else
						{	// .. or stop if the user prefers
							bContinue = false;
						}
					}
				}
			}
			// Get to here when we have a collection of bytes to
			// give back

			if (bPadToPageFront)
			{	// Rearrange the data so that it starts at an exact page offset
				// Extract the bytes of interest:
				byte[] dataBytes = new byte[iActualDataLength];
				Array.Copy(array,0,dataBytes,0, iActualDataLength);
				
				for (int pos = 0; pos < array.Length; pos++)
				{
					array[pos] = PadCharacter; // Clean out the array
				}
					
				
				// And put the data in the BACK of the array:
				Array.Copy(dataBytes, 0, array,(array.Length - iActualDataLength), iActualDataLength);

				// And indicate to the caller the address they SHOULD be using:
				// This is the given start address, plus the amount of bytes we actually read,
				// minus the page size.
				lNextAddress = (lStartAddress + iActualDataLength) - iBytesToGet;
			}



			return true;
		}

		
		public long GetAddressOfRow(int iRow)
		{
			if (iRow <= iRowCount)
			{
				return rows[(iRow-1)].lAddress;
			}
			else return 0;
		}
				
		public bool CompareByte(Point byteLocation, long byteValue)
		{
			MotorolaSRecordRow sRow = GetRow(byteLocation.Y);
			if (sRow == null) return false;
			byte[] theBytes = sRow.GetByteArray(byteLocation.X, 1);
			if (theBytes == null) return false;
			if (theBytes.Length == 1)
			{
				if (theBytes[0] == Convert.ToByte(byteValue & 0xFF))
				{
					return true;
				}
			}

			return false;
		}

        private bool ReadFromBytes(byte[] data)
        {// We read the file in two passes, the first to obtain the
            // number of rows, the second to copy them into our array
            // of SRecordRows.
            iRowCount = 0;
            iTotalByteCount = 0;
            try
            {
                // Create an instance of StreamReader to read from a file.
                // The using statement also closes the StreamReader.
                using(MemoryStream ms = new MemoryStream(data))
                using (StreamReader srCounter = new StreamReader(ms))
                {
                    // Read lines from the file until the end of 
                    // the file is reached.
                    while (srCounter.ReadLine() != null) iRowCount++;
                }

                // Now size the array:
                rows = new MotorolaSRecordRow[iRowCount];

                int iCount = 0;

                using (MemoryStream ms = new MemoryStream(data))
                using (StreamReader sr = new StreamReader(ms))
                {
                    String line;
                    // Read lines from the file until the end of 
                    // the file is reached.
                    while ((line = sr.ReadLine()) != null)
                    {
                        rows[iCount] = new MotorolaSRecordRow(line);
                        if (!rows[iCount].bIsValid)
                        {
                            // A problem within this row
                            sFileFailureReason = "Problem decoding row " + iCount;
                            return false;
                        }
                        // Keep a tally of how many bytes are in this file
                        iTotalByteCount += rows[iCount].cLineLengthBytes;
                        iCount++;
                    }
                }
            }
            catch (Exception e)
            {
                sFileFailureReason = "Exception while reading file - " + e.Message;
                return false;
            }
            return true;
        }

		private bool ReadFile(string sFileName, byte[] data)
		{
			// We read the file in two passes, the first to obtain the
			// number of rows, the second to copy them into our array
			// of SRecordRows.
			iRowCount = 0;
			iTotalByteCount = 0;
			try 
			{
				// Create an instance of StreamReader to read from a file.
				// The using statement also closes the StreamReader.
				using (StreamReader srCounter = new StreamReader(sFileName)) 
				{
					// Read lines from the file until the end of 
					// the file is reached.
					while (srCounter.ReadLine() != null) iRowCount++;
				}
				
				// Now size the array:
				rows = new MotorolaSRecordRow[iRowCount];

				int iCount = 0;
				using (StreamReader sr = new StreamReader(sFileName)) 
				{
					String line;
					// Read lines from the file until the end of 
					// the file is reached.
					while ((line = sr.ReadLine()) != null) 
					{
						rows[iCount] = new MotorolaSRecordRow(line);
						if (!rows[iCount].bIsValid)
						{
							// A problem within this row
							sFileFailureReason = "Problem decoding row " + iCount;
							return false;
						}
						// Keep a tally of how many bytes are in this file
						iTotalByteCount += rows[iCount].cLineLengthBytes;
						iCount++;
					}
				}
			}
			catch (Exception e) 
			{
				sFileFailureReason = "Exception while reading file - " + e.Message;
				return false;	
			}
			return true;
		}
		public bool WriteFile(string sFileName)
		{
			// Create an instance of StreamWriter to write text to a file.
			// The using statement also closes the StreamWriter.
			using (StreamWriter sw = new StreamWriter(sFileName)) 
			{
				// Add the text to the file.
				foreach (MotorolaSRecordRow row in rows)
				{
					sw.WriteLine(row.ToString());
				}
			}

			return true;
		}
	}
}

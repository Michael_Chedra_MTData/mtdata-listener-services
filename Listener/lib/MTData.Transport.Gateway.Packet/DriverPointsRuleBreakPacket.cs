using System;
using System.IO;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class DriverPointsRuleBreakPacket : GatewayProtocolPacket, IStatusHolder, IPositionHolder
    {
        public const byte DRIVER_POINTS_RULE_BROKEN = 0x24;

        #region varaibles
        private string _serverTime_DateFormat;
        public GPClock mCurrentClock;
        public GPClock mFixClock;
        public GPPositionFix mFix;
        public GPStatus mStatus;
        public GPDistance mDistance;
        public GPEngineData mEngineData;
        public GPExtendedValues mExtendedValues;
        public GPTransportExtraDetails mTransportExtraDetails;
        public GPEngineSummaryData mEngineSummaryData;
        public GPECMAdvanced mECMAdvanced;
        public GPTrailerTrack mTrailerTrack;
        public GBVariablePacketExtended59 mExtendedVariableDetails;
        public byte cInputNumber;
        public byte cRefrigerationZone;

        /// <summary>
        /// the protocol number for this packet
        /// </summary>
        public int ProtocolNumber;
        /// <summary>
        /// the advanced status - 4 bytes of status flags
        /// </summary>
        public int ExtraStatusFlags;
        /// <summary>
        /// the Id of the rule which is broken
        /// </summary>
        public int RuleId;
        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        public string ConstructorDecodeError;
        
        #endregion

        #region constructors
        public DriverPointsRuleBreakPacket(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }

        public DriverPointsRuleBreakPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);

            ConstructorDecodeError = DecodePayload();
            bIsPopulated = (ConstructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = DRIVER_POINTS_RULE_BROKEN;
            mCurrentClock = new GPClock("Device", serverTime_DateFormat);
            mFixClock = new GPClock("GPS", serverTime_DateFormat);
            mFix = new GPPositionFix();
            mStatus = new GPStatus();
            mDistance = new GPDistance();
            mEngineData = new GPEngineData();
            mExtendedValues = new GPExtendedValues();
            mEngineSummaryData = new GPEngineSummaryData();
            mTransportExtraDetails = new GPTransportExtraDetails();
            mTrailerTrack = new GPTrailerTrack();
            mECMAdvanced = new GPECMAdvanced();
            mExtendedVariableDetails = new GBVariablePacketExtended59();

        ConstructorDecodeError = null;
        }
        #endregion

        #region public methods
        /// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\nDriver Points Rule Break : ");
            builder.Append("\r\n    Protocol Number : "); builder.Append(ProtocolNumber);
            builder.Append("\r\n    RuleId : "); builder.Append(RuleId);
            builder.Append("\r\n    ExtraStatusFlags : "); builder.Append(ExtraStatusFlags);
            if (mStatus != null)
            {
                builder.Append(mStatus.ToString());
            }
            builder.Append("\r\nTimes : ");
            if (mCurrentClock != null)
            {
                if (mCurrentClock.ToString() == " ")
                    builder.Append("\r\n    Device Time : No Value");
                else
                {
                    builder.Append("\r\n    Device Time : " + mCurrentClock.ToString());
                    try
                    {
                        builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                builder.Append("\r\n    Device Time : No Value");
            }
            if (mFixClock != null)
            {
                if (mFixClock.ToString() == " ")
                    builder.Append("\r\n    GPS Time : No Value");
                else
                {
                    builder.Append("\r\n    GPS Time : " + mFixClock.ToString());
                    try
                    {
                        builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                builder.Append("\r\n    GPS Time : No Value");
            }
            if (mFix != null)
            {
                builder.Append(mFix.ToString());
            }
            if (mDistance != null)
            {
                builder.Append(mDistance.ToString());
            }
            if (mECMAdvanced != null && (mFix != null || mDistance != null))
            {
                builder.Append("\r\nAccurate Speed Adjustments :");
                if (mFix != null)
                {
                    builder.Append("\r\n    Speed : "); builder.Append(this.mECMAdvanced.CreateSpeedSPNItem(mFix.cSpeed, false));
                }
                else
                    builder.Append("\r\n    Speed : 0");
                if (mDistance != null)
                {
                    builder.Append("\r\n    Max Speed : "); builder.Append(this.mECMAdvanced.CreateSpeedSPNItem(mDistance.cMaxSpeed, false));
                }
                else
                    builder.Append("\r\n    Max Speed : 0");
            }
            if (mExtendedValues != null)
            {
                builder.Append(mExtendedValues.ToString());
            }
            if (mTrailerTrack != null)
            {
                builder.Append(mTrailerTrack.ToString());
            }
            if (mTransportExtraDetails != null)
            {
                builder.Append(mTransportExtraDetails.ToString());
            }
            if (mEngineData != null)
            {
                builder.Append(mEngineData.ToString());
            }
            if (mEngineSummaryData != null)
            {
                builder.Append(mEngineSummaryData.ToString());
            }
            if (mECMAdvanced != null)
            {
                builder.Append(mECMAdvanced.ToString());
            }
            if (mExtendedVariableDetails != null)
            {
                builder.Append(mExtendedVariableDetails.ToString());
            }

		    if (RoadTypeData != null)
		    {
		        builder.Append(RoadTypeData);
		    }

            return builder.ToString();
        }

        public new DriverPointsRuleBreakPacket CreateCopy()
        {
            DriverPointsRuleBreakPacket oGP = new DriverPointsRuleBreakPacket(this, _serverTime_DateFormat);

            if (mCurrentClock != null) 
            { 
                oGP.mCurrentClock = mCurrentClock.CreateCopy(); 
            }
            else 
            { 
                oGP.mCurrentClock = new GPClock("", _serverTime_DateFormat); 
            }
            if (mFixClock != null)
            {
                oGP.mFixClock = mFixClock.CreateCopy(); 
            }
            else 
            {
                oGP.mFixClock = new GPClock("", _serverTime_DateFormat); 
            }
            if (mFix != null)
            { 
                oGP.mFix = mFix.CreateCopy(); 
            }
            else 
            { 
                oGP.mFix = new GPPositionFix(); 
            }
            if (mStatus != null) 
            {
                oGP.mStatus = mStatus.CreateCopy(); 
            }
            else 
            { 
                oGP.mStatus = new GPStatus();
            }
            if (mDistance != null) 
            {
                oGP.mDistance = mDistance.CreateCopy(); 
            }
            else 
            { 
                oGP.mDistance = new GPDistance(); 
            }
            if (this.mEngineData != null) { oGP.mEngineData = this.mEngineData.CreateCopy(); }
            else { oGP.mEngineData = new GPEngineData(); }
            if (this.mEngineSummaryData != null) { oGP.mEngineSummaryData = this.mEngineSummaryData.CreateCopy(); }
            else { oGP.mEngineSummaryData = new GPEngineSummaryData(); }
            if (this.mECMAdvanced != null) { oGP.mECMAdvanced = this.mECMAdvanced.CreateCopy(); }
            else { oGP.mECMAdvanced = new GPECMAdvanced(); }
            if (this.mExtendedValues != null) { oGP.mExtendedValues = this.mExtendedValues.CreateCopy(); }
            else { oGP.mExtendedValues = new GPExtendedValues(); }
            if (this.mTrailerTrack != null) { oGP.mTrailerTrack = this.mTrailerTrack.CreateCopy(); }
            else { oGP.mTrailerTrack = new GPTrailerTrack(); }
            if (this.mTransportExtraDetails != null) { oGP.mTransportExtraDetails = this.mTransportExtraDetails.CreateCopy(); }
            else { oGP.mTransportExtraDetails = new GPTransportExtraDetails(); }
            if (this.mExtendedVariableDetails != null) { oGP.mExtendedVariableDetails = this.mExtendedVariableDetails.CreateCopy(); }
            else { oGP.mExtendedVariableDetails = new GBVariablePacketExtended59(); }

            if (this.RoadTypeData != null)
            {
                oGP.RoadTypeData = this.RoadTypeData.CreateCopy();            
            }

            oGP.ProtocolNumber = ProtocolNumber;
            oGP.RuleId = RuleId;
            return oGP;
        }

		public new void Encode(ref Byte[] theData)
		{
            MemoryStream oMS = new MemoryStream();

            byte[] payload = new byte[1500];
            int pos = 0;

            //protocol number
            payload[pos++] = (byte)ProtocolNumber;

            mCurrentClock.Encode(ref payload, pos);
            pos += GPClock.Length;

            mFixClock.Encode(ref payload, pos);
            pos += GPClock.Length;

            mFix.Encode(ref payload, pos);
            pos += GPPositionFix.Length;

            mStatus.Encode(ref payload, pos);
            pos += mStatus.Length;

            mDistance.Encode(ref payload, pos);
            pos += GPDistance.Length;

            if (ProtocolNumber >=3 )
            {
                bool bEngineData = false;
                bool bExtraDetails = false;
                bool bTrailerTrack = false;
                bool bExtendedVariableDetails = false;
                bool bECMAdvanced = false;
                bool hasRoadTypeData = false;

                if (this.cPacketTotal == (byte)0x01)
                {
                    bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                    bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                    bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                    bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                    bECMAdvanced = false;
                }
                else if (this.cPacketTotal == (byte)0x03)
                {
                    bEngineData = false;
                    bExtraDetails = false;
                    bTrailerTrack = ((this.cPacketNum & (byte)0x0C) == (byte)0x0C);
                    bExtendedVariableDetails = true;
                    bECMAdvanced = true;
                }
                else if (this.cPacketTotal == (byte)0x04 || this.cPacketTotal == (byte)0x05 || this.cPacketTotal == (byte)0x06 || this.cPacketTotal == 0x07)
                {
                    bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                    bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                    bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                    bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                    bECMAdvanced = true;

                    if (cPacketTotal == 0x07)
                    {
                        hasRoadTypeData = true;
                    }                    
                }

                if (this.cPacketTotal == (byte)0x06)
                {
                    //need to add 31 bytes of additional advanced ECM data that is not used at present
                    //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                    pos += 31;
                }

                if (bEngineData && mEngineData != null)
                {
                    mEngineData.Encode(ref payload, pos);
                    pos += GPEngineData.Length;
                    if (mExtendedValues != null)
                    {
                        mExtendedValues.Encode(ref payload, pos);
                        pos += GPExtendedValues.Length;
                    }
                }
                if (bExtraDetails && mTransportExtraDetails != null)
                {
                    mTransportExtraDetails.Encode(ref payload, pos);
                    pos += GPTransportExtraDetails.Length;
                }
                if (bTrailerTrack && mTrailerTrack != null)
                {
                    mTrailerTrack.Encode(ref payload, pos);
                    pos += mTrailerTrack.Length;
                }
                if (bECMAdvanced && mECMAdvanced != null)
                {
                    mECMAdvanced.Encode(ref payload, pos);
                    pos += mECMAdvanced.Length;
                }
                if (bExtendedVariableDetails && mExtendedVariableDetails != null)
                {
                    mExtendedVariableDetails.Encode(ref payload, pos);
                    pos += mExtendedVariableDetails.Length;
                }

                if (RoadTypeData != null && hasRoadTypeData)
                {
                    RoadTypeData.Encode(ref payload, pos);
                    pos += RoadTypeData.Length;
                }
            }
            
            WriteInt(pos, RuleId, ref payload);
            pos += 4;

            mDataField = new Byte[pos];
            System.Array.Copy(payload, 0, mDataField, 0, pos);
            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
		}
		
        public string Decode(Byte[] theData)
		{
			string problem = null;
			if (!bIsPopulated)	problem = base.Decode(theData, 0);
			if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

			return problem;
        }

        private string DecodePayload()
        {
            // Work through the packet
            int pos = 0;

            ProtocolNumber = (int)mDataField[pos++];

            string retMsg = null;
            if ((retMsg = mCurrentClock.Decode(mDataField, pos)) != null)
            {
                // most likely this is an invalid clock (i.e. we haven't
                // acquired GPS as yet). We will skip decoding the rest
                // of the packet except for the Mass Declaration which, as it
                // is not derived from GPS, may be of some value
                pos += GPClock.Length + GPClock.Length + GPPositionFix.Length;
                if ((retMsg = mStatus.Decode(mDataField, pos)) != null) return retMsg;
                pos += mStatus.Length;
                pos += GPDistance.Length;
            }
            else
            {
                pos += GPClock.Length;
                mFixClock.Decode(mDataField, pos);
                pos += GPClock.Length;
                if ((retMsg = mFix.Decode(mDataField, pos)) != null) return retMsg;
                pos += GPPositionFix.Length;
                if ((retMsg = mStatus.Decode(mDataField, pos)) != null) return retMsg;
                pos += mStatus.Length;
                if ((retMsg = mDistance.Decode(mDataField, pos)) != null) return retMsg;
                pos += GPDistance.Length;
            }

            //version 3 is a long status packet
            if (ProtocolNumber >= 3)
            {
                mExtendedValues.iValue1 = 0;
                mExtendedValues.iValue2 = 0;
                mExtendedValues.iValue3 = 0;
                mExtendedValues.iValue4 = 0;

                bool bEngineData = false;
                bool bExtraDetails = false;
                bool bTrailerTrack = false;
                bool bExtendedVariableDetails = false;
                bool bECMAdvanced = false;
                bool hasRoadTypeData = false;

                if (this.cPacketTotal == (byte)0x01)
                {
                    bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                    bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                    bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                    bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                    bECMAdvanced = false;
                }
                else if (this.cPacketTotal == (byte)0x03)
                {
                    bEngineData = false;
                    bExtraDetails = false;
                    bTrailerTrack = ((this.cPacketNum & (byte)0x0C) == (byte)0x0C);
                    bExtendedVariableDetails = true;
                    bECMAdvanced = true;
                }
                else if (this.cPacketTotal == (byte)0x04 || this.cPacketTotal == (byte)0x05 || this.cPacketTotal == (byte)0x06 || cPacketTotal == 0x07)
                {
                    bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                    bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                    bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                    bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                    bECMAdvanced = true;

                    if (cPacketTotal == 0x07)
                    {
                        hasRoadTypeData = true;
                    }
                }

                if (this.cPacketTotal == (byte)0x06)
                {
                    //need to skip 31 bytes of additional advanced ECM data that is not used at present
                    //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                    pos += 31;
                }

                if ((pos + 1) < mDataField.Length)
                {
                    if (bEngineData)
                    {
                        #region Decode Engine Summary Data and Extended Values
                        if ((retMsg = mEngineData.Decode(mDataField, pos)) != null) return retMsg;
                        pos += GPEngineData.Length;

                        mExtendedValues.iValue1 = 0;
                        mExtendedValues.iValue2 = 0;
                        mExtendedValues.iValue3 = 0;
                        mExtendedValues.iValue4 = 0;
                        if ((pos + 2) < mDataField.Length)
                        {
                            mExtendedValues.iValue1 = (mDataField[pos++]);
                            mExtendedValues.iValue1 += (mDataField[pos++] << 8);
                            if (mExtendedValues.iValue1 > 32768)
                                mExtendedValues.iValue1 = mExtendedValues.iValue1 - 65535;
                        }
                        if ((pos + 2) < mDataField.Length)
                        {
                            mExtendedValues.iValue2 = (mDataField[pos++]);
                            mExtendedValues.iValue2 += (mDataField[pos++] << 8);
                            if (mExtendedValues.iValue2 > 32768)
                                mExtendedValues.iValue2 = mExtendedValues.iValue2 - 65535;
                        }
                        if ((pos + 2) < mDataField.Length)
                        {
                            mExtendedValues.iValue3 = (mDataField[pos++]);
                            mExtendedValues.iValue3 += (mDataField[pos++] << 8);
                            if (mExtendedValues.iValue3 > 32768)
                                mExtendedValues.iValue3 = mExtendedValues.iValue3 - 65535;
                        }
                        if ((pos + 2) < mDataField.Length)
                        {
                            mExtendedValues.iValue4 = (mDataField[pos++]);
                            mExtendedValues.iValue4 += (mDataField[pos++] << 8);
                            if (mExtendedValues.iValue4 > 32768)
                                mExtendedValues.iValue4 = mExtendedValues.iValue4 - 65535;
                        }
                        #endregion
                    }
                    if (bExtraDetails)
                    {
                        #region Decode the Transport Extra Details
                        if ((retMsg = mTransportExtraDetails.Decode(mDataField, pos)) != null) return retMsg;
                        if (mEngineSummaryData != null)
                        {
                            if (Convert.ToInt32(mTransportExtraDetails.iOdometer) > mEngineSummaryData.iOdometer)
                                mEngineSummaryData.iOdometer = Convert.ToInt32(mTransportExtraDetails.iOdometer);
                            if (mTransportExtraDetails.iTotalFuelUsed > mEngineSummaryData.iTotalFuel)
                                mEngineSummaryData.iTotalFuel = mTransportExtraDetails.iTotalFuelUsed;
                            if (mTransportExtraDetails.iBatteryVolts > mEngineSummaryData.iBatteryVoltage)
                                mEngineSummaryData.iBatteryVoltage = mTransportExtraDetails.iBatteryVolts;
                            if (mTransportExtraDetails.fFuelEconomy > mEngineSummaryData.fFuelEconomy)
                                mEngineSummaryData.fFuelEconomy = mTransportExtraDetails.fFuelEconomy;
                        }
                        pos += GPTransportExtraDetails.Length;
                        #endregion
                    }
                    if (bECMAdvanced)
                    {
                        if ((retMsg = mECMAdvanced.Decode(mDataField, ref pos)) != null) return retMsg;
                        if (mECMAdvanced.SpeedAccuracyValuesSet)
                        {
                            mFix.cSpeed = mECMAdvanced.CreateSpeedSPNItem(mFix.cSpeed, false);
                            mDistance.cMaxSpeed = mECMAdvanced.CreateSpeedSPNItem(mDistance.cMaxSpeed, true);
                        }
                    }
                    if (bTrailerTrack)
                    {
                        #region Decode the Trailer Track data
                        if ((retMsg = mTrailerTrack.Decode(mDataField, pos)) != null) return retMsg;
                        pos += mTrailerTrack.Length;
                        #endregion
                    }
                    if (bExtendedVariableDetails)
                    {
                        if ((retMsg = mExtendedVariableDetails.Decode(mDataField, ref pos, cInputNumber, cRefrigerationZone)) != null) return retMsg;
                    }

                    if (hasRoadTypeData)
                    {
                        if (RoadTypeData == null)
                        {
                            RoadTypeData = new RoadTypeData();
                        }
                        
                        retMsg = RoadTypeData.Decode(mDataField, ref pos);

                        if (retMsg != null)
                        {
                            return retMsg;
                        }
                    }
                }
            }

            ExtraStatusFlags = ReadInt(pos, mDataField);
            pos += 4;
            RuleId = ReadInt(pos, mDataField);
            pos += 4;
            return null;
        }

        private int ReadInt(int pos, byte[] payload)
        {
            byte[] bTemp = new byte[4];

            bTemp[0] = payload[pos];
            bTemp[1] = payload[pos + 1];
            bTemp[2] = payload[pos + 2];
            bTemp[3] = payload[pos + 3];

            return BitConverter.ToInt32(bTemp, 0);
        }

        private void WriteInt(int pos, int value, ref byte[] payload)
        {

            byte[] temp = BitConverter.GetBytes(value);
            payload[pos++] = (temp.Length > 0) ? temp[0] : (byte)0;
            payload[pos++] = (temp.Length > 1) ? temp[1] : (byte)0;
            payload[pos++] = (temp.Length > 2) ? temp[2] : (byte)0;
            payload[pos++] = (temp.Length > 3) ? temp[3] : (byte)0;
        }

        public GeneralGPPacket CreateGeneralGPPacket()
        {
            GeneralGPPacket p = new GeneralGPPacket(_serverTime_DateFormat);
            //p.mRawBytes = this.mRawBytes;
            p.Decode(this.mRawBytes);
		    p.cMsgType = cMsgType;
		    p.bPacketFormat = bPacketFormat;
		    p.cFleetId = cFleetId;
		    p.iVehicleId = iVehicleId;
            p.StreetName = this.StreetName;
            p.SpeedLimit = this.SpeedLimit;
            p.RoadType = this.RoadType;
            if (mCurrentClock != null)
            {
                p.mCurrentClock = mCurrentClock.CreateCopy();
            }
            else
            {
                p.mCurrentClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFixClock != null)
            {
                p.mFixClock = mFixClock.CreateCopy();
            }
            else
            {
                p.mFixClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFix != null)
            {
                p.mFix = mFix.CreateCopy();
            }
            else
            {
                p.mFix = new GPPositionFix();
            }
            if (mStatus != null)
            {
                p.mStatus = mStatus.CreateCopy();
            }
            else
            {
                p.mStatus = new GPStatus();
            }
            if (mDistance != null)
            {
                p.mDistance = mDistance.CreateCopy();
            }
            else
            {
                p.mDistance = new GPDistance();
            }
            if (this.mEngineData != null) { p.mEngineData = this.mEngineData.CreateCopy(); }
            else { p.mEngineData = new GPEngineData(); }
            if (this.mEngineSummaryData != null) { p.mEngineSummaryData = this.mEngineSummaryData.CreateCopy(); }
            else { p.mEngineSummaryData = new GPEngineSummaryData(); }
            if (this.mECMAdvanced != null) { p.mECMAdvanced = this.mECMAdvanced.CreateCopy(); }
            else { p.mECMAdvanced = new GPECMAdvanced(); }
            if (this.mExtendedValues != null) { p.mExtendedValues = this.mExtendedValues.CreateCopy(); }
            else { p.mExtendedValues = new GPExtendedValues(); }
            if (this.mTrailerTrack != null) { p.mTrailerTrack = this.mTrailerTrack.CreateCopy(); }
            else { p.mTrailerTrack = new GPTrailerTrack(); }
            if (this.mTransportExtraDetails != null) { p.mTransportExtraDetails = this.mTransportExtraDetails.CreateCopy(); }
            else { p.mTransportExtraDetails = new GPTransportExtraDetails(); }
            if (this.mExtendedVariableDetails != null) { p.mExtendedVariableDetails = this.mExtendedVariableDetails.CreateCopy(); }
            else { p.mExtendedVariableDetails = new GBVariablePacketExtended59(); }

            if (this.RoadTypeData != null)
            {
                p.RoadTypeData = this.RoadTypeData = this.RoadTypeData.CreateCopy();
            }

            return p;
        }
        #endregion

        #region IStatusHolder Members

        public GPStatus GetStatus()
        {
            return mStatus;
        }

        #endregion

        #region IPositionHolder Members

        public GPPositionFix GetPosition()
        {
            return mFix;
        }

        public GPClock GetFixClock()
        {
            return mFixClock;
        }

        #endregion 
    }
}

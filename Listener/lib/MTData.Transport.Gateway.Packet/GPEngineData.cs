using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Holds engine-related data in a Status Report
	/// </summary>
    [Serializable]
    public class GPEngineData
	{
		public int iCoolantTemperature;
		public int iOilTemperature;
		public int iOilPressure;
		public int iGear;
		public int iMaxRPM;
		public int iBrakeApplications;
		public float fGForceFront;
		public float fGForceBack;
		public float fGForceLeftRight;
		public bool bIsPopulated;
		public const int Length = 13;
		public GPEngineData()
		{
			iCoolantTemperature = 0;
			iOilTemperature = 0;
			iOilPressure = 0;
			iGear = 0;
			iMaxRPM = 0;
			iBrakeApplications = 0;

			fGForceFront = 0;
			fGForceBack = 0;
			fGForceLeftRight = 0;

			bIsPopulated = false;
		}

		public GPEngineData CreateCopy()
		{
			GPEngineData oEngineData = new GPEngineData();
			oEngineData.iCoolantTemperature = this.iCoolantTemperature;
			oEngineData.iOilTemperature = this.iOilTemperature;
			oEngineData.iOilPressure = this.iOilPressure;
			oEngineData.iGear = this.iGear;
			oEngineData.iMaxRPM = this.iMaxRPM;
			oEngineData.iBrakeApplications = this.iBrakeApplications;
			oEngineData.fGForceFront = this.fGForceFront;
			oEngineData.fGForceBack = this.fGForceBack;
			oEngineData.fGForceLeftRight = this.fGForceLeftRight;
			oEngineData.bIsPopulated = this.bIsPopulated;
			return oEngineData;
		}

		public string Decode(Byte[] aData)
		{
			return Decode(aData, 0);
		}

		public string Decode(Byte[] aData, int aStartPos)
		{
			// Ensure we've got a decent array:
			if (aData.Length < (aStartPos + Length)) return "Engine Data field too short";
			
			iCoolantTemperature = aData[aStartPos++];
			iCoolantTemperature += (aData[aStartPos++] << 8);

			// Protect against a probable zero-value:
			//iCoolantTemperature -= 40;
			//if (iCoolantTemperature == -40) iCoolantTemperature = 0;

			// Oil temp is in Kelvin and multiplied by 32:
			iOilTemperature = aData[aStartPos++];
			iOilTemperature += (aData[aStartPos++] << 8);
			iOilTemperature = (iOilTemperature / 32) - 273;
			if (iOilTemperature == -273) iOilTemperature = 0;

			// Oil Pressure at Max RPM moment:
			iOilPressure = aData[aStartPos++] * 4; // Multiply by 4 for actual value
			// Gear at Max RPM moment:
			// 125 = Neutral
			// 251 = Park
			// Numbers < 125 = reverse,
			// Numbers > 125 fwd gears
			iGear = aData[aStartPos++];
			if (iGear == 251) iGear = 0;
			else if (iGear != 0) iGear -= 125;

			iMaxRPM = aData[aStartPos++];
			iMaxRPM += (aData[aStartPos++] << 8);
			iBrakeApplications = aData[aStartPos++];
			iBrakeApplications += (aData[aStartPos++] << 8);

			// G-Forces must be divided by 50, giving 0.2G accuracy up to
			// a ceiling of 5.1G
			byte[] bConvert = new byte[4];

			bConvert[0] = (byte) 0x00;
			bConvert[1] = (byte) 0x00;
			bConvert[2] = (byte) 0x00;
			bConvert[3] = (byte) 0x00;

			fGForceFront =	0;
			fGForceBack = 0;
			fGForceLeftRight = 0;

			int iRes = 0;

			bConvert[0] = aData[aStartPos++];
			iRes = BitConverter.ToInt32(bConvert, 0);
			fGForceFront =	((float) iRes / 50);

			bConvert[0] = aData[aStartPos++];
			iRes = BitConverter.ToInt32(bConvert, 0);
			fGForceBack = ((float) iRes / 50);

			bConvert[0] = aData[aStartPos++];
			iRes = BitConverter.ToInt32(bConvert, 0);
			fGForceLeftRight =	((float) iRes / 50);

			bIsPopulated = true;
			return null;
		}

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            double dTemp = 0;

            if (theData.Length >= (aStartPos + Length))
            {
                bTemp = BitConverter.GetBytes(iCoolantTemperature);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                dTemp = Convert.ToDouble(iOilTemperature) + Convert.ToDouble(273);
                dTemp = dTemp / Convert.ToDouble(32);
                bTemp = BitConverter.GetBytes(Convert.ToInt32(dTemp));
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(Convert.ToInt32(Convert.ToDouble(iOilPressure) / Convert.ToDouble(4)));
                theData[aStartPos++] = bTemp[0];
                bTemp = BitConverter.GetBytes(iGear);
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(iMaxRPM);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(iBrakeApplications);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                dTemp = Convert.ToDouble(fGForceFront) * Convert.ToDouble(50);
                bTemp = BitConverter.GetBytes(Convert.ToInt32(dTemp));
                theData[aStartPos++] = bTemp[0];
                dTemp = Convert.ToDouble(fGForceBack) * Convert.ToDouble(50);
                bTemp = BitConverter.GetBytes(Convert.ToInt32(dTemp));
                theData[aStartPos++] = bTemp[0];
                dTemp = Convert.ToDouble(fGForceLeftRight) * Convert.ToDouble(50);
                bTemp = BitConverter.GetBytes(Convert.ToInt32(dTemp));
                theData[aStartPos++] = bTemp[0];
            }
        }
		public string ToDisplayString()
		{
			return	this.ToString();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nEngine Data :"); 
				builder.Append("\r\n    Oil Temp : "); builder.Append(iOilTemperature);
				builder.Append("\r\n    Oil Pressure : "); builder.Append(iOilPressure);
				builder.Append("\r\n    Gear : "); builder.Append(iGear);
				builder.Append("\r\n    Max RPM : "); builder.Append(iMaxRPM);
				builder.Append("\r\n    Coolant Temp : "); builder.Append(iCoolantTemperature);
				builder.Append("\r\n    Gear : "); builder.Append(iGear);
				builder.Append("\r\n    Brake Apps : "); builder.Append(iBrakeApplications);
				builder.Append("\r\n    G-Force Data :"); 
				builder.Append("\r\n        Front : "); builder.Append(fGForceFront);
				builder.Append("\r\n        Back : "); builder.Append(fGForceBack);
				builder.Append("\r\n        Lateral : "); builder.Append(fGForceLeftRight);
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
			}
			return builder.ToString();
		}

	}
    [Serializable]
    public class GPEngineSummaryData
	{
		public int iTotalEngineHours;
		public int iTotalFuel;
		public int iTripFuel;
		public double fFuelEconomy;
		public int iOdometer;
		public int iBatteryVoltage;
		public bool bIsPopulated;

		public const int Length = 22;

		public GPEngineSummaryData()
		{
			iTotalEngineHours = 0;
			iTotalFuel = 0;
			iTripFuel = 0;
			fFuelEconomy = 0;
			iOdometer = 0;
			iBatteryVoltage = 0;
			bIsPopulated = false;
		}

		public GPEngineSummaryData CreateCopy()
		{
			GPEngineSummaryData oEngineSummaryData = new GPEngineSummaryData();
			oEngineSummaryData.iTotalEngineHours = this.iTotalEngineHours;
			oEngineSummaryData.iTotalFuel = this.iTotalFuel;
			oEngineSummaryData.iTripFuel = this.iTripFuel;
			oEngineSummaryData.fFuelEconomy = this.fFuelEconomy;
			oEngineSummaryData.iOdometer = this.iOdometer;
			oEngineSummaryData.iBatteryVoltage = this.iBatteryVoltage;
			oEngineSummaryData.bIsPopulated = this.bIsPopulated;
			return oEngineSummaryData;
		}

		public string Decode(Byte[] aData)
		{
			return Decode(aData, 0);
		}

		public string Decode(Byte[] aData, int aStartPos)
		{
			int iFuelEconomy = 0;
			iTotalEngineHours = 0;
			iTotalFuel = 0;
			iFuelEconomy = 0;
			iOdometer = 0;
			iBatteryVoltage = 0;

			// Ensure we've got a decent array:
			if (aData.Length < (aStartPos + Length)) return "Engine Summary Data field too short";
			
			// Total Hours
			iTotalEngineHours = (aData[aStartPos++]);
			iTotalEngineHours += (aData[aStartPos++] << 8);
			iTotalEngineHours += (aData[aStartPos++] << 16);
			iTotalEngineHours += (aData[aStartPos++] << 24);

			//	 Total Fuel
			iTotalFuel = (aData[aStartPos++]);
			iTotalFuel += (aData[aStartPos++] << 8);
			iTotalFuel += (aData[aStartPos++] << 16);
			iTotalFuel += (aData[aStartPos++] << 24);
		
			//	 Trip Fuel
			iTripFuel = (aData[aStartPos++]);
			iTripFuel += (aData[aStartPos++] << 8);
			iTripFuel += (aData[aStartPos++] << 16);
			iTripFuel += (aData[aStartPos++] << 24);
	
			//	 Avg Fuel Economy
			iFuelEconomy = (aData[aStartPos++]);
			iFuelEconomy += (aData[aStartPos++] << 8);
			iFuelEconomy += (aData[aStartPos++] << 16);
			iFuelEconomy += (aData[aStartPos++] << 24);
	
			fFuelEconomy = Convert.ToDouble(iFuelEconomy);
			if (fFuelEconomy > 100)
			{
				fFuelEconomy = fFuelEconomy / 100;
			}
			else
			{
				fFuelEconomy = fFuelEconomy / 10;
			}

			//	 Odometer
			iOdometer = (aData[aStartPos++]);
			iOdometer += (aData[aStartPos++] << 8);
			iOdometer += (aData[aStartPos++] << 16);
			iOdometer += (aData[aStartPos++] << 24);
			
			iBatteryVoltage = aData[aStartPos++];
			iBatteryVoltage += (aData[aStartPos++] << 8);
			iBatteryVoltage = iBatteryVoltage / 20; // adjust from CAN values

			bIsPopulated = true;
			return null;
		}

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            int iFuelEconomy = 0;

            bTemp = BitConverter.GetBytes(iTotalEngineHours);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(iTotalFuel);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(iTripFuel);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            iFuelEconomy = Convert.ToInt32(fFuelEconomy * Convert.ToDouble(100));
            bTemp = BitConverter.GetBytes(iFuelEconomy);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(iOdometer);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(iBatteryVoltage * 20);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
        }
        public string ToDisplayString()
		{
			return	this.ToString();
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nEngine Summary Data :"); 
				builder.Append("\r\n    Total Hrs : "); builder.Append(iTotalEngineHours);
				builder.Append("\r\n    Total Fuel : "); builder.Append(iTotalFuel);
				builder.Append("\r\n    Trip Fuel : "); builder.Append(iTripFuel);
				builder.Append("\r\n    Fuel Econ : "); builder.Append(fFuelEconomy);
				builder.Append("\r\n    Odometer : "); builder.Append(iOdometer);
				builder.Append("\r\n    Battery Volts : "); builder.Append(iBatteryVoltage);
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
			}
			return builder.ToString();
		}


	}
}

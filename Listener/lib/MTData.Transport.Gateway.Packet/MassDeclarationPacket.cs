using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class MassDeclarationPacket : GatewayProtocolPacket, IStatusHolder, IPositionHolder
    {
        public const byte MASS_DECLARATION = 0x21;
        public const byte MASS_PACKET_MASK = 0x20;


        #region enum declarations
        /// <summary>
        /// the scales type
        /// </summary>
        public enum ScaleType
        {
            Manual = 0, // Manual declaration (driver input)
            LoadSMART   // declaration via LoadSMART scales
        }

        /// <summary>
        /// the state the scales are in for this declaration
        /// </summary>
        public enum ScaleState
        {
            OK = 0,     //the scales are stable, a perfect read
            UNSTABLE_WEIGHT //the scales are reporting the weight is unstable 
        }

        /// <summary>
        /// the vehicle section types for an axle group
        /// </summary>
        public enum SectionType
        {
            NO_SECTION = 0,
            PRIME_1_1,
            PRIME_1_2,
            RIGID_1_1,
            RIGID_1_2,
            RIGID_1_3,
            RIGID_2_2,
            RIGID_2_3,
            PIG_1,
            PIG_2,
            PIG_3,
            DOG_1_1,
            DOG_1_2,
            DOG_2_2,
            SEMI_1,
            SEMI_2,
            SEMI_3,
            SEMI_2_5TH_WHEEL,
            SEMI_3_5TH_WHEEL,
            DOLLY_2,
            DOLLY_3,
            Unknown,
            SEMI_4_5TH_WHEEL,
            SEMI_4,
			PRIME_2_2,
            DOG_2_3,
            DOG_3_3,
            DOG_3_4,
            DOLLY_1,
            PRIME_1_1_COMBINED,
            PRIME_1_2_COMBINED,
            PRIME_2_2_COMBINED
        }
        #endregion

        #region varaibles
        private string _serverTime_DateFormat;
        public GPClock mCurrentClock;
        public GPClock mFixClock;
        public GPPositionFix mFix;
        public GPStatus mStatus;
        public GPDistance mDistance;
        /// <summary>
        /// the protocol number for this packet
        /// </summary>
        public int ProtocolNumber;
        /// <summary>
        /// the type of scales used in this declaration
        /// </summary>
        public ScaleType ScalesType;
        /// <summary>
        /// the state of the scales for this declaration
        /// </summary>
        public ScaleState ScalesState;
        /// <summary>
        /// the total mass of the vehicle
        /// </summary>
        public float TotalMass;
        /// <summary>
        /// the mass of axle group 1
        /// </summary>
        public float MassAxleGroup1;
        /// <summary>
        /// the mass of axle group 2
        /// </summary>
        public float MassAxleGroup2;
        /// <summary>
        /// the mass of axle group 3
        /// </summary>
        public float MassAxleGroup3;
        /// <summary>
        /// the mass of axle group 4
        /// </summary>
        public float MassAxleGroup4;
        /// <summary>
        /// the mass of axle group 5
        /// </summary>
        public float MassAxleGroup5;
        /// <summary>
        /// the mass of axle group 6
        /// </summary>
        public float MassAxleGroup6;
        /// <summary>
        /// the mass of axle group 7
        /// </summary>
        public float MassAxleGroup7;
        /// <summary>
        /// the vehcile section which contains axle group 1
        /// </summary>
        public SectionType AxleGroupType1;
        /// <summary>
        /// the vehcile section which contains axle group 2
        /// </summary>
        public SectionType AxleGroupType2;
        /// <summary>
        /// the vehcile section which contains axle group 3
        /// </summary>
        public SectionType AxleGroupType3;
        /// <summary>
        /// the vehcile section which contains axle group 4
        /// </summary>
        public SectionType AxleGroupType4;
        /// <summary>
        /// the vehcile section which contains axle group 5
        /// </summary>
        public SectionType AxleGroupType5;
        /// <summary>
        /// the vehcile section which contains axle group 6
        /// </summary>
        public SectionType AxleGroupType6;
        /// <summary>
        /// the vehcile section which contains axle group 7
        /// </summary>
        public SectionType AxleGroupType7;
        /// <summary>
        /// the weighbridge ticket id
        /// </summary>
        public string TicketId;
        /// <summary>
        /// If the mass declaration is releated to a job, this is the leg ID from the job.
        /// </summary>
        public int LegID;
        /// <summary>
        /// Mass scheme List Item ID
        /// </summary>
        public int MassSchemeItemID;
        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        public string ConstructorDecodeError;
        #endregion

        #region constructors
        public MassDeclarationPacket(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }

        public MassDeclarationPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);

            ConstructorDecodeError = DecodePayload();
            bIsPopulated = (ConstructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = MASS_DECLARATION;
            mCurrentClock = new GPClock("Device", serverTime_DateFormat);
            mFixClock = new GPClock("GPS", serverTime_DateFormat);
            mFix = new GPPositionFix();
            mStatus = new GPStatus();
            mDistance = new GPDistance();

            ScalesType = ScaleType.Manual;
            ScalesState = ScaleState.OK;
            AxleGroupType1 = SectionType.NO_SECTION;
            AxleGroupType2 = SectionType.NO_SECTION;
            AxleGroupType3 = SectionType.NO_SECTION;
            AxleGroupType4 = SectionType.NO_SECTION;
            AxleGroupType5 = SectionType.NO_SECTION;
            AxleGroupType6 = SectionType.NO_SECTION;
            AxleGroupType7 = SectionType.NO_SECTION;
            TicketId = string.Empty;
            LegID = -1;
            MassSchemeItemID = -1;
            ConstructorDecodeError = null;
        }
        #endregion

        #region public properties
        /// <summary>
        /// get the packet length for this packet
        /// //works out the length depending on the length of the variable fields (strings)
        /// </summary>
        public int MassDeclarationPacketLength
        {
            get
            {
                int length = 8; //protocol number + totalmass + scales type + scales status + axle group count
                //add the gps and status
                length += GPClock.Length + GPClock.Length + GPPositionFix.Length + mStatus.Length + GPDistance.Length;
                //get the number of axle groups
                int axleGroupCount = NumberOfAxleGroups();
                length += 5 * axleGroupCount;
                //add the ticket id length and delimiter
                length += TicketId.Length + 1;
                if (ProtocolNumber > 1)
                {
                    //add the leg ID length
                    length += 4;
                }
                return length;
            }
        }
        #endregion

        #region public methods
        /// <summary>
        /// Produce a user-friendly description of the details parsed
        /// </summary>
        /// <returns></returns>
        public override string ToDisplayString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToDisplayString());
            builder.Append("\r\nMass Declaration : ");
            builder.Append("\r\n    Protocol Number : "); builder.Append(ProtocolNumber);
            builder.Append("\r\n    ScalesType : "); builder.Append(ScalesType.ToString());
            builder.Append("\r\n    ScalesState : "); builder.Append(ScalesState.ToString());
            builder.Append("\r\n    TotalMass : "); builder.Append(TotalMass);
            builder.Append("\r\n    MassAxleGroup1 : "); builder.Append(MassAxleGroup1);
            builder.Append("\r\n    MassAxleGroup2 : "); builder.Append(MassAxleGroup2);
            builder.Append("\r\n    MassAxleGroup3 : "); builder.Append(MassAxleGroup3);
            builder.Append("\r\n    MassAxleGroup4 : "); builder.Append(MassAxleGroup4);
            builder.Append("\r\n    MassAxleGroup5 : "); builder.Append(MassAxleGroup5);
            builder.Append("\r\n    MassAxleGroup6 : "); builder.Append(MassAxleGroup6);
            builder.Append("\r\n    MassAxleGroup7 : "); builder.Append(MassAxleGroup7);
            builder.Append("\r\n    AxleGroupType1 : "); builder.Append(AxleGroupType1.ToString());
            builder.Append("\r\n    AxleGroupType2 : "); builder.Append(AxleGroupType2.ToString());
            builder.Append("\r\n    AxleGroupType3 : "); builder.Append(AxleGroupType3.ToString());
            builder.Append("\r\n    AxleGroupType4 : "); builder.Append(AxleGroupType4.ToString());
            builder.Append("\r\n    AxleGroupType5 : "); builder.Append(AxleGroupType5.ToString());
            builder.Append("\r\n    AxleGroupType6 : "); builder.Append(AxleGroupType6.ToString());
            builder.Append("\r\n    AxleGroupType7 : "); builder.Append(AxleGroupType7.ToString());
            builder.Append("\r\n    TicketId : "); builder.Append(TicketId);
            builder.Append("\r\n    LegId : "); builder.Append(LegID);
            builder.Append("\r\n    MassSchemeItemId : "); builder.Append(MassSchemeItemID);
            if (mStatus != null)
            {
                builder.Append(mStatus.ToString());
            }
            builder.Append("\r\nTimes : ");
            if (mCurrentClock != null)
            {
                if (mCurrentClock.ToString() == " ")
                    builder.Append("\r\n    Device Time : No Value");
                else
                {
                    builder.Append("\r\n    Device Time : " + mCurrentClock.ToString());
                    try
                    {
                        builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                builder.Append("\r\n    Device Time : No Value");
            }
            if (mFixClock != null)
            {
                if (mFixClock.ToString() == " ")
                    builder.Append("\r\n    GPS Time : No Value");
                else
                {
                    builder.Append("\r\n    GPS Time : " + mFixClock.ToString());
                    try
                    {
                        builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                builder.Append("\r\n    GPS Time : No Value");
            }
            if (mFix != null)
            {
                builder.Append(mFix.ToString());
            }
            if (mDistance != null)
            {
                builder.Append(mDistance.ToString());
            }
            return builder.ToString();
        }

        public new MassDeclarationPacket CreateCopy()
        {
            MassDeclarationPacket oGP = new MassDeclarationPacket(this, _serverTime_DateFormat);

            if (mCurrentClock != null)
            {
                oGP.mCurrentClock = mCurrentClock.CreateCopy();
            }
            else
            {
                oGP.mCurrentClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFixClock != null)
            {
                oGP.mFixClock = mFixClock.CreateCopy();
            }
            else
            {
                oGP.mFixClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFix != null)
            {
                oGP.mFix = mFix.CreateCopy();
            }
            else
            {
                oGP.mFix = new GPPositionFix();
            }
            if (mStatus != null)
            {
                oGP.mStatus = mStatus.CreateCopy();
            }
            else
            {
                oGP.mStatus = new GPStatus();
            }
            if (mDistance != null)
            {
                oGP.mDistance = mDistance.CreateCopy();
            }
            else
            {
                oGP.mDistance = new GPDistance();
            }

            oGP.ProtocolNumber = ProtocolNumber;
            oGP.ScalesType = ScalesType;
            oGP.ScalesState = ScalesState;
            oGP.TotalMass = TotalMass;
            oGP.MassAxleGroup1 = MassAxleGroup1;
            oGP.MassAxleGroup2 = MassAxleGroup2;
            oGP.MassAxleGroup3 = MassAxleGroup3;
            oGP.MassAxleGroup4 = MassAxleGroup4;
            oGP.MassAxleGroup5 = MassAxleGroup5;
            oGP.MassAxleGroup6 = MassAxleGroup6;
            oGP.MassAxleGroup7 = MassAxleGroup7;
            oGP.AxleGroupType1 = AxleGroupType1;
            oGP.AxleGroupType2 = AxleGroupType2;
            oGP.AxleGroupType3 = AxleGroupType3;
            oGP.AxleGroupType4 = AxleGroupType4;
            oGP.AxleGroupType5 = AxleGroupType5;
            oGP.AxleGroupType6 = AxleGroupType6;
            oGP.AxleGroupType7 = AxleGroupType7;
            oGP.TicketId = TicketId;
            oGP.LegID = LegID;
            oGP.MassSchemeItemID = MassSchemeItemID;
            return oGP;
        }

        public new void Encode(ref Byte[] theData)
        {
            int pos = 0;

            //workout the length of the packet
            mDataField = new Byte[MassDeclarationPacketLength];

            //protocol number
            mDataField[pos++] = (byte)ProtocolNumber;

            mCurrentClock.Encode(ref mDataField, pos);
            pos += GPClock.Length;

            mFixClock.Encode(ref mDataField, pos);
            pos += GPClock.Length;

            mFix.Encode(ref mDataField, pos);
            pos += GPPositionFix.Length;

            mStatus.Encode(ref mDataField, pos);
            pos += mStatus.Length;

            mDistance.Encode(ref mDataField, pos);
            pos += GPDistance.Length;

            mDataField[pos++] = (byte)ScalesType;
            mDataField[pos++] = (byte)ScalesState;
            WriteFloat(pos, TotalMass);
            pos += 4;

            int numberOfSections = NumberOfAxleGroups();
            mDataField[pos++] = (byte)numberOfSections;
            if (numberOfSections >= 1)
            {
                WriteFloat(pos, MassAxleGroup1);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType1;
            }
            if (numberOfSections >= 2)
            {
                WriteFloat(pos, MassAxleGroup2);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType2;
            }
            if (numberOfSections >= 3)
            {
                WriteFloat(pos, MassAxleGroup3);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType3;
            }
            if (numberOfSections >= 4)
            {
                WriteFloat(pos, MassAxleGroup4);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType4;
            }
            if (numberOfSections >= 5)
            {
                WriteFloat(pos, MassAxleGroup5);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType5;
            }
            if (numberOfSections >= 6)
            {
                WriteFloat(pos, MassAxleGroup6);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType6;
            }
            if (numberOfSections >= 7)
            {
                WriteFloat(pos, MassAxleGroup7);
                pos += 4;
                mDataField[pos++] = (byte)AxleGroupType7;
            }

            if (TicketId.Length > 0)
            {
                byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(TicketId);

                for (int loop = 0; loop < bytes.Length; loop++)
                {
                    mDataField[pos++] = bytes[loop];
                }
            }
            mDataField[pos++] = (byte)0x12;
            if (ProtocolNumber > 1)
            {
                WriteInt(pos, LegID);
            }
            if (ProtocolNumber > 2)
            {
                WriteInt(pos, MassSchemeItemID);
            }
            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
        }

        public string Decode(Byte[] theData)
        {
            string problem = null;
            if (!bIsPopulated) problem = base.Decode(theData, 0);
            if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

            return problem;
        }

        private string DecodePayload()
        {
            try
            {
                // Work through the packet
                int pos = 0;

                ProtocolNumber = (int)mDataField[pos++];

                string retMsg = null;
                if ((retMsg = mCurrentClock.Decode(mDataField, pos)) != null)
                {
                    // most likely this is an invalid clock (i.e. we haven't
                    // acquired GPS as yet). We will skip decoding the rest
                    // of the packet except for the Mass Declaration which, as it
                    // is not derived from GPS, may be of some value
                    pos += GPClock.Length + GPClock.Length + GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(mDataField, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    pos += GPDistance.Length;
                }
                else
                {
                    pos += GPClock.Length;
                    mFixClock.Decode(mDataField, pos);
                    pos += GPClock.Length;
                    if ((retMsg = mFix.Decode(mDataField, pos)) != null) return retMsg;
                    pos += GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(mDataField, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    if ((retMsg = mDistance.Decode(mDataField, pos)) != null) return retMsg;
                    pos += GPDistance.Length;
                }

                if (this.cPacketTotal == (byte)0x06)
                {
                    //need to skip 31 bytes of additional advanced ECM data that is not used at present
                    //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                    pos += 31;
                }

                ScalesType = (ScaleType)((int)mDataField[pos++]);
                ScalesState = (ScaleState)((int)mDataField[pos++]);
                TotalMass = ReadFloat(pos);
                pos += 4;
                int numberOfSections = (int)mDataField[pos++];
                if (numberOfSections >= 1)
                {
                    MassAxleGroup1 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType1 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections >= 2)
                {
                    MassAxleGroup2 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType2 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections >= 3)
                {
                    MassAxleGroup3 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType3 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections >= 4)
                {
                    MassAxleGroup4 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType4 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections >= 5)
                {
                    MassAxleGroup5 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType5 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections >= 6)
                {
                    MassAxleGroup6 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType6 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections >= 7)
                {
                    MassAxleGroup7 = ReadFloat(pos);
                    pos += 4;
                    AxleGroupType7 = (SectionType)((int)mDataField[pos++]);
                }
                if (numberOfSections > 7)
                {
                    return string.Format("Too many axle group sections {0}, can't be more than 7", numberOfSections);
                }

                int length = 0;
                while (true)
                {
                    if ((pos + length) > mDataField.Length)
                        return "TicketID: Attempt to read past end of buffer";

                    if (mDataField[pos + length] == 0x12)
                        break;
                    length++;
                }
                TicketId = System.Text.ASCIIEncoding.ASCII.GetString(mDataField, pos, length);
                if (TicketId.StartsWith("\0"))
                    TicketId = "";

                pos += length + 1; //plus one for the delimiter        

                if (ProtocolNumber > 1)
                {
                    LegID = ReadInt(pos);
                    pos += 4;
                }
                else
                {
                    LegID = -1;
                }

                if (ProtocolNumber > 2)
                {
                    MassSchemeItemID = ReadInt(pos);
                    pos += 4;
                }
                else
                {
                    MassSchemeItemID = -1;
                }
            }
            catch
            {
                
            }
            return null;
        }

        private float ReadFloat(int pos)
        {
            int iTemp = 0;
            if (mDataField.Length >= pos + 4)
            {
                byte[] bTemp = new byte[4];
                bTemp[0] = mDataField[pos];
                bTemp[1] = mDataField[pos + 1];
                bTemp[2] = mDataField[pos + 2];
                bTemp[3] = mDataField[pos + 3];
                iTemp = BitConverter.ToInt32(bTemp, 0);
            }

            float result = (float)iTemp / (float)100;
            return result;
        }

        private int ReadInt(int pos)
        {
            int iTemp = -1;
            if (mDataField.Length >= pos + 4)
            {
                byte[] bTemp = new byte[4];
                bTemp[0] = mDataField[pos];
                bTemp[1] = mDataField[pos + 1];
                bTemp[2] = mDataField[pos + 2];
                bTemp[3] = mDataField[pos + 3];
                iTemp = BitConverter.ToInt32(bTemp, 0);
            }
            return iTemp;
        }
        /// <summary>
        /// Write a float to mDataField
        /// </summary>
        /// <param name="pos">the position in the mDataField to write the float</param>
        /// <param name="value">the value to write</param>
        private void WriteFloat(int pos, float value)
        {

            int valueInt = Convert.ToInt32(value * 100);
            byte[] temp = BitConverter.GetBytes(valueInt);
            mDataField[pos++] = (temp.Length > 0) ? temp[0] : (byte)0;
            mDataField[pos++] = (temp.Length > 1) ? temp[1] : (byte)0;
            mDataField[pos++] = (temp.Length > 2) ? temp[2] : (byte)0;
            mDataField[pos++] = (temp.Length > 3) ? temp[3] : (byte)0;
        }

        private void WriteInt(int pos, int value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            mDataField[pos++] = (temp.Length > 0) ? temp[0] : (byte)0;
            mDataField[pos++] = (temp.Length > 1) ? temp[1] : (byte)0;
            mDataField[pos++] = (temp.Length > 2) ? temp[2] : (byte)0;
            mDataField[pos++] = (temp.Length > 3) ? temp[3] : (byte)0;
        }

        private int NumberOfAxleGroups()
        {
            if (AxleGroupType1 == SectionType.NO_SECTION)
            {
                return 0;
            }
            else if (AxleGroupType2 == SectionType.NO_SECTION)
            {
                return 1;
            }
            else if (AxleGroupType3 == SectionType.NO_SECTION)
            {
                return 2;
            }
            else if (AxleGroupType4 == SectionType.NO_SECTION)
            {
                return 3;
            }
            else if (AxleGroupType5 == SectionType.NO_SECTION)
            {
                return 4;
            }
            else if (AxleGroupType6 == SectionType.NO_SECTION)
            {
                return 5;
            }
            else if (AxleGroupType7 == SectionType.NO_SECTION)
            {
                return 6;
            }
            return 7;
        }
        #endregion

        #region IStatusHolder Members

        public GPStatus GetStatus()
        {
            return mStatus;
        }

        #endregion

        #region IPositionHolder Members

        public GPPositionFix GetPosition()
        {
            return mFix;
        }

        public GPClock GetFixClock()
        {
            return mFixClock;
        }

        #endregion
    }
}

using System;
using System.Net.Sockets;
using System.Text;
using System.Net;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigNetworkInfoGPPacket.
	/// </summary>
    [Serializable]
    public class ConfigNetworkInfoGPPacket : GatewayProtocolPacket
	{
		#region Variables
		public byte cVersion;
		public byte cNewFleetId;
		public uint iNewUnitId;
		public byte cNetwork;
		public IPAddress mPrimaryServerIPAddress;
		public int iPrimaryServerRxPort = 0;
		public IPAddress mSecondaryServerIPAddress;
		public int iSecondaryServerRxPort = 0;
		public IPAddress mPingableIPAddress;
		public byte cMaxPingRetries = 5;
		public string sNetworkAPN;
		public byte cPort1BaudRate;
        public string ServerHostName = "";
		private string _serverTime_DateFormat = "";
		#endregion
		public const Byte CONFIG_NETWORK_INFO = 0xd5;

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncVersion : "); builder.Append(cVersion);
			builder.Append("\r\ncNewFleetId : "); builder.Append(cNewFleetId);
			builder.Append("\r\niNewUnitId : "); builder.Append(iNewUnitId);
			builder.Append("\r\ncNetwork : "); builder.Append(cNetwork);
			builder.Append("\r\nmPrimaryServerIPAddress : "); builder.Append(mPrimaryServerIPAddress);
			builder.Append("\r\niPrimaryServerRxPort : "); builder.Append(iPrimaryServerRxPort);
			builder.Append("\r\nmSecondaryServerIPAddress : "); builder.Append(mSecondaryServerIPAddress);
			builder.Append("\r\niSecondaryServerRxPort : "); builder.Append(iSecondaryServerRxPort);
			builder.Append("\r\nmPingableIPAddress : "); builder.Append(mPingableIPAddress);
			builder.Append("\r\ncMaxPingRetries : "); builder.Append(cMaxPingRetries);
			builder.Append("\r\nsNetworkAPN : "); builder.Append(sNetworkAPN);
			builder.Append("\r\ncPort1BaudRate : "); builder.Append(cPort1BaudRate);

			return builder.ToString();
		}

		public ConfigNetworkInfoGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			cMsgType = CONFIG_NETWORK_INFO;
			Setup();
		}

		public ConfigNetworkInfoGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			cMsgType = CONFIG_NETWORK_INFO;
			Setup();
		}

		private void Setup()
		{
			mPingableIPAddress = new IPAddress(0);
			mPrimaryServerIPAddress = new IPAddress(0);
			mSecondaryServerIPAddress = new IPAddress(0);
		}

		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0, checksum = 0;

			// Make sure we have sufficient space:
			iLength = 159;
			if (mDataField.Length != iLength)
			{
				mDataField = new Byte[iLength];
			}

			mDataField[encPos++] = GSP_SOH;
			mDataField[encPos++] = cVersion;
			mDataField[encPos++] = cNewFleetId;
			mDataField[encPos++] = Convert.ToByte((iNewUnitId & 0xFF00) >> 8);
			mDataField[encPos++] = Convert.ToByte(iNewUnitId & 0xFF);
			mDataField[encPos++] = cNetwork;
			// Customer's Primary IP address:
            //byte[] bAddress = mPrimaryServerIPAddress.GetAddressBytes();
            //mDataField[encPos++] = bAddress[0];
            //mDataField[encPos++] = bAddress[1];
            //mDataField[encPos++] = bAddress[2];
            //mDataField[encPos++] = bAddress[3];
            byte[] bAssress = null;
            long ipAddress = 0;

            if (mPrimaryServerIPAddress != null)
            {
                bAssress = mPrimaryServerIPAddress.GetAddressBytes();
                ipAddress = mPrimaryServerIPAddress.Address;
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF000000) >> 24);
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF0000) >> 16);
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF00) >> 8);
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF));
            }
            else
            {
                mDataField[encPos++] = (Byte) 0x00;
                mDataField[encPos++] = (Byte) 0x00;
                mDataField[encPos++] = (Byte) 0x00;
                mDataField[encPos++] = (Byte) 0x00;
            }
            // Primary UDP Port:
            mDataField[encPos++] = (Byte)((iPrimaryServerRxPort & 0xFF));
			mDataField[encPos++] = (Byte) ((iPrimaryServerRxPort & 0xFF00) >> 8);
			encPos += 2;	// 2 spare bytes

			if (sNetworkAPN != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sNetworkAPN, 
					0, 
					sNetworkAPN.Length,
					mDataField,
					encPos);
			}
			encPos += 80;	// The APN can be up to 80 chars in length

			mDataField[encPos++] = cPort1BaudRate;

			// Customer's Secondary IP address:
            //bAddress = mSecondaryServerIPAddress.GetAddressBytes();
            //mDataField[encPos++] = bAddress[0];
            //mDataField[encPos++] = bAddress[1];
            //mDataField[encPos++] = bAddress[2];
            //mDataField[encPos++] = bAddress[3];
            if (mSecondaryServerIPAddress != null)
            {
                ipAddress = mSecondaryServerIPAddress.Address;
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF000000) >> 24);
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF0000) >> 16);
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF00) >> 8);
                mDataField[encPos++] = (Byte)((ipAddress & 0xFF));
            }
            else
            {
                mDataField[encPos++] = (Byte) 0x00;
                mDataField[encPos++] = (Byte) 0x00;
                mDataField[encPos++] = (Byte) 0x00;
                mDataField[encPos++] = (Byte) 0x00;
            }
			// Secondary UDP Port:
			mDataField[encPos++] = (Byte) ((iSecondaryServerRxPort & 0xFF));
			mDataField[encPos++] = (Byte) ((iSecondaryServerRxPort & 0xFF00) >> 8);
			encPos += 2;	// 2 spare bytes

			// Ping IP address:
            //bAddress = mPingableIPAddress.GetAddressBytes();
            //mDataField[encPos++] = bAddress[0];
            //mDataField[encPos++] = bAddress[1];
            //mDataField[encPos++] = bAddress[2];
            //mDataField[encPos++] = bAddress[3];
            ipAddress = mPingableIPAddress.Address;
            mDataField[encPos++] = (Byte)((ipAddress & 0xFF000000) >> 24);
            mDataField[encPos++] = (Byte)((ipAddress & 0xFF0000) >> 16);
            mDataField[encPos++] = (Byte)((ipAddress & 0xFF00) >> 8);
            mDataField[encPos++] = (Byte)((ipAddress & 0xFF));
			// Ping retry maximum:
			mDataField[encPos++] = cMaxPingRetries;

            int hostNamelen = 0;
            if (!string.IsNullOrEmpty(ServerHostName))
            {
                hostNamelen = ServerHostName.Length;
                if (hostNamelen > 46)
                    ServerHostName = ServerHostName.Substring(0, 46);
                ServerHostName += "\0";
                hostNamelen = ServerHostName.Length;
                byte[] serverHostNameBytes = System.Text.ASCIIEncoding.ASCII.GetBytes(ServerHostName);
                foreach (byte b in serverHostNameBytes)
                    mDataField[encPos++] = b;
            }

            encPos += (47 - hostNamelen);	// 47 Spare bytes - host name string length

            // Config Server
            mDataField[encPos++] = (Byte)((iPrimaryServerRxPort & 0xFF));
            mDataField[encPos++] = (Byte)((iPrimaryServerRxPort & 0xFF00) >> 8);

			// Perform an additional checksum on everything between the internal SOH
			// field, and this point.
			for (int pos = 0; pos < encPos; pos++)
			{
				checksum += mDataField[pos];
//				byte[] bConvert = BitConverter.GetBytes(checksum);
//				bConvert[1] = (byte) 0x00;
//				bConvert[2] = (byte) 0x00;
//				bConvert[3] = (byte) 0x00;
//				checksum = BitConverter.ToInt32(bConvert, 0);
			}
			// Write in the checksum and the end-of-message
			mDataField[encPos++] = Convert.ToByte(checksum & 0xFF);
			mDataField[encPos++] = GSP_EOT; 

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    /// <summary>
    /// an ECM hysteresis error
    /// </summary>
    [Serializable]
    public class EcmHysteresisError
    {
        public static readonly int LENGTH = 21;
        public bool Cancelled;
        public int RuleId;
        public uint RawValue;
        public float ProcessedValue;
        public uint PreviousRaw;
        public float PreviousProcessed;

        public void Decode(byte[] aData, ref int pos)
        {
            Cancelled = false;
            if (aData[pos++] == 0x02)
            {
                Cancelled = true;
            }
            RuleId = BitConverter.ToInt32(aData, pos);
            pos += 4;
            RawValue = BitConverter.ToUInt32(aData, pos);
            pos += 4;
            ProcessedValue = BitConverter.ToSingle(aData, pos);
            pos += 4;
            PreviousRaw = BitConverter.ToUInt32(aData, pos);
            pos += 4;
            PreviousProcessed = BitConverter.ToSingle(aData, pos);
            pos += 4;
        }

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp;

            if (Cancelled)
            {
                theData[aStartPos++] = 0x02;
            }
            else
            {
                theData[aStartPos++] = 0x01;
            }
            bTemp = BitConverter.GetBytes(RuleId);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(RawValue);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(ProcessedValue);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(PreviousRaw);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
            bTemp = BitConverter.GetBytes(PreviousProcessed);
            theData[aStartPos++] = bTemp[0];
            theData[aStartPos++] = bTemp[1];
            theData[aStartPos++] = bTemp[2];
            theData[aStartPos++] = bTemp[3];
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nECM Hysteresis Error :");

                builder.Append("\r\n    Cancelled : "); builder.Append(Cancelled);
                builder.Append("\r\n    RuleID : "); builder.Append(RuleId);
                builder.Append("\r\n    RawValue : "); builder.Append(RawValue);
                builder.Append("\r\n    Processed Value : "); builder.Append(ProcessedValue);
                builder.Append("\r\n    Previous Raw : "); builder.Append(PreviousRaw);
                builder.Append("\r\n    Previous Processed : "); builder.Append(PreviousProcessed);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding ECM Hysteresis Packet : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

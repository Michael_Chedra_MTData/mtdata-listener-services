using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for RefrigerationZoneInfo.
	/// </summary>
    [Serializable]
    public class RefrigerationZoneInfo
	{
		#region Enumerations
		public enum RefrigerationOperatingModes : int
		{
			PowerOff = 0,
			Cooling = 1,
			Heating = 2,
			Defrost = 3,
			Null = 4,
			Pretrip = 5,
			Extended1 = 6,
			Extended2 = 7
		}

		public enum RefrigerationZoneAlarms : int
		{
			Null = 0,
			MaintenanceRequired = 1,
			LowFuel = 2,
			MaintenanceOverdue = 3,
			Reserved1 = 4,
			Reserved2 = 5,
			Reserved3 = 6,
			Reserved4 = 7,
			ImmediateAttentionRequired = 8,
			Reserved5 = 9,
			Reserved6 = 10,
			Reserved7 = 11,
			Reserved8 = 12,
			Reserved9 = 13,
			Reserved10 = 14,
			SystemFailure = 15,
		}
		#endregion
		#region Private Vars
		private int _iZoneNumber = 0;
        private int iLength = 0;
		private RefrigerationOperatingModes _eOperatingMode = RefrigerationOperatingModes.Null;
		private RefrigerationZoneAlarms _eZoneAlarms = RefrigerationZoneAlarms.Null;
		private short _iReturnAir1 = 0;
		private short _iReturnAir2 = 0;
		private short _iSupplyAir1 = 0;
		private short _iSupplyAir2 = 0;
		private short _iSetPoint = 0;
		private short _iEvap = 0;
		private int _iAvailableSensors = 0;		
		// Available data flags
		private bool bOperatingAvailable = false;
		private bool bSupply1Available = false;
		private bool bSupply2Available = false;
		private bool bReturn1Available = false;
		private bool bReturn2Available = false;
		private bool bEvapAvailable = false;
		private bool bSetPointAvailable = false;
		#endregion
		#region Public Properties
		public int ZoneNumber { get{ return _iZoneNumber;}}
        public int Length { get { return iLength; } }
		public RefrigerationOperatingModes OperatingMode { get{ return _eOperatingMode;}}
		public RefrigerationZoneAlarms ZoneAlarms { get{ return _eZoneAlarms;}}
		public int SensorsAvailable { get{ return _iAvailableSensors;}}
		public float ReturnAir1 { get{ return ((float) _iReturnAir1 / (float) 10);}}
		public float ReturnAir2 { get{ return ((float)_iReturnAir2 / (float) 10);}}
		public float SupplyAir1 { get{ return ((float)_iSupplyAir1 / (float) 10);}}
		public float SupplyAir2 { get{ return ((float)_iSupplyAir2 / (float) 10);}}
		public float Setpoint{ get{ return ((float)_iSetPoint / (float) 10);}}
		public float EvaporatorCoil{ get{ return ((float)_iEvap / (float) 10);}}
		#endregion
		public RefrigerationZoneInfo()
		{
		}

		public RefrigerationZoneInfo (int iZoneNumber,  RefrigerationOperatingModes eOperatingMode, RefrigerationZoneAlarms eZoneAlarms, short iReturnAir1, short iReturnAir2, short iSupplyAir1, short iSupplyAir2, short iSetPoint, short iEvap, int iAvailableSensors)
		{
			try
			{
				_iZoneNumber = iZoneNumber;
				_eOperatingMode = eOperatingMode;
				_eZoneAlarms = eZoneAlarms;
				_iReturnAir1 = iReturnAir1;
				_iReturnAir2 = iReturnAir2;
				_iSupplyAir1 = iSupplyAir1;
				_iSupplyAir2 = iSupplyAir2;
				_iSetPoint = iSetPoint;
				_iEvap = iEvap;
				_iAvailableSensors = iAvailableSensors;
				SetAvailableFlags(((byte[])BitConverter.GetBytes(_iAvailableSensors))[0]);
			}
			catch(System.Exception)
			{
			}
		}

		public string Decode(ref byte[] data, ref int pos)
		{
			string sMsg = null;
			try
			{
				#region Get the Zone Number
				_iZoneNumber = data[pos++];
				#endregion
				#region Get the operating mode
				_eOperatingMode = (RefrigerationOperatingModes) data[pos++];
				#endregion
				#region Get the zone alarm code
				_eZoneAlarms = (RefrigerationZoneAlarms) data[pos++];
				#endregion
				#region Get the available flags and values
				SetAvailableFlags(data[pos++]);
				if(bOperatingAvailable || bSupply1Available || bSupply2Available || bReturn1Available || bReturn2Available || bEvapAvailable || bSetPointAvailable)
				{
					_iReturnAir1 = ReadSigned2ByteIntFromArray(ref data, ref pos);
					_iSupplyAir1 = ReadSigned2ByteIntFromArray(ref data, ref pos);
					_iReturnAir2 = ReadSigned2ByteIntFromArray(ref data, ref pos);
					_iSupplyAir2 = ReadSigned2ByteIntFromArray(ref data, ref pos);
					_iSetPoint = ReadSigned2ByteIntFromArray(ref data, ref pos);
					_iEvap = ReadSigned2ByteIntFromArray(ref data, ref pos);
				}
				#endregion
			}
			catch(System.Exception ex)
			{
				sMsg = "Error decoding RefrigerationZoneInfo  values : " + ex.Message;
			}
			return sMsg;
		}
        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            iLength = 0;
            byte bSensorFlags = (byte)0x00;

            bTemp = BitConverter.GetBytes(_iZoneNumber);
            theData[aStartPos++] = bTemp[0];
            iLength++;
            bTemp = BitConverter.GetBytes((int)_eOperatingMode);
            theData[aStartPos++] = bTemp[0];
            iLength++;
            bTemp = BitConverter.GetBytes((int)_eZoneAlarms);
            theData[aStartPos++] = bTemp[0];
            iLength++;

            if ((int)_eOperatingMode > 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 2);
                bOperatingAvailable = true;
            }
            if (_iSupplyAir2 != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 4);
                bSupply2Available = true;
            }
            if (_iReturnAir2 != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 8);
                bReturn2Available = true;
            }
            if (_iEvap != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 16);
                bEvapAvailable = true;
            }
            if (_iSetPoint != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 32);
                bSetPointAvailable = true;
            }
            if (_iSupplyAir1 != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 64);
                bSupply1Available = true;
            }
            if (_iReturnAir1 != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 128);
                bReturn1Available = true;
            }

            theData[aStartPos++] = bSensorFlags;
            iLength++;

            if (bOperatingAvailable || bSupply1Available || bSupply2Available || bReturn1Available || bReturn2Available || bEvapAvailable || bSetPointAvailable)
            {
                bTemp = BitConverter.GetBytes(_iReturnAir1);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
                bTemp = BitConverter.GetBytes(_iSupplyAir1);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
                bTemp = BitConverter.GetBytes(_iReturnAir2);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
                bTemp = BitConverter.GetBytes(_iSupplyAir2);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
                bTemp = BitConverter.GetBytes(_iSetPoint);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
                bTemp = BitConverter.GetBytes(_iEvap);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
        }
        
		public RefrigerationZoneInfo CreateCopy()
		{
			RefrigerationZoneInfo oCopy = new RefrigerationZoneInfo(_iZoneNumber, _eOperatingMode, _eZoneAlarms, _iReturnAir1, _iReturnAir2, _iSupplyAir1, _iSupplyAir2, _iSetPoint, _iEvap, _iAvailableSensors);
			return oCopy;
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\n        Zone "); builder.Append(_iZoneNumber); builder.Append(":");
				builder.Append("\r\n            Available Sensors : "); builder.Append(_iAvailableSensors);
				builder.Append("\r\n            Operating Mode : "); builder.Append(GetOperatingModesAsString((int) _eOperatingMode));
				builder.Append("\r\n            Zone Alarm : "); builder.Append(GetZoneAlarmAsString((int) _eZoneAlarms));
				builder.Append("\r\n            Return Air 1 : "); builder.Append(_iReturnAir1);
				builder.Append("\r\n            Return Air 2 : "); builder.Append(_iReturnAir2);
				builder.Append("\r\n            Supply Air 1 : "); builder.Append(_iSupplyAir1);
				builder.Append("\r\n            Supply Air 2 : "); builder.Append(_iSupplyAir2);
				builder.Append("\r\n            Setpoint : "); builder.Append(_iSetPoint);
				builder.Append("\r\n            Evaporator Coil : "); builder.Append(_iEvap);
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
			}
			return builder.ToString();
		}

		#region Support Functions
		private void SetAvailableFlags(byte bAvailable)
		{
			try
			{
				_iAvailableSensors = (int) bAvailable;
				if ((bAvailable & 0x02) == 0x02)
					bOperatingAvailable = true;
				if ((bAvailable & 0x04) == 0x04)
					bSupply2Available = true;
				if ((bAvailable & 0x08) == 0x08)
					bReturn2Available = true;
				if ((bAvailable & 0x10) == 0x10)
					bEvapAvailable = true;
				if ((bAvailable & 0x20) == 0x20)
					bSetPointAvailable = true;
				if ((bAvailable & 0x40) == 0x40)
					bSupply1Available = true;
				if ((bAvailable & 0x80) == 0x80)
					bReturn1Available = true;
			}
			catch(System.Exception)
			{
				bOperatingAvailable = false;
				bSupply1Available = false;
				bSupply2Available = false;
				bReturn1Available = false;
				bReturn2Available = false;
				bEvapAvailable = false;
				bSetPointAvailable = false;
			}
		}

		private short ReadSigned2ByteIntFromArray(ref byte[] data, ref int pos)
		{
			short iRet = 0;
			byte[] bConvert = null;
			try
			{
				if (data.Length > pos + 2)
				{
					bConvert = new byte[2];
					bConvert[0] = data[pos++];
					bConvert[1] = data[pos++];
					iRet = BitConverter.ToInt16(bConvert, 0);
				}
			}
			catch(System.Exception)
			{
				iRet = 0;
			}
			return iRet;
		}
		public string GetOperatingModesAsString(int iOperatingMode)
		{
			string sRet = "";
			switch(iOperatingMode)
			{
				case 0:
					sRet = "Power Off";
					break;
				case 1:
					sRet = "Cooling";
					break;
				case 2:
					sRet = "Heating";
					break;
				case 3:
					sRet = "Defrost";
					break;
				case 4:
					sRet = "Null";
					break;
				case 5:
					sRet = "Pretrip";
					break;
				case 6:
					sRet = "Extended 1";
					break;
				case 7:
					sRet = "Extended 2";
					break;
				default:
					sRet = "Unknown";
					break;
			}
			return sRet;
		}

		public string GetZoneAlarmAsString(int iZoneAlarm)
		{
			string sRet = "";
			switch(iZoneAlarm)
			{
				case 0:
					sRet = "No Alarms";
					break;
				case 1:
					sRet = "Maintenance Required";
					break;
				case 2:
					sRet = "Low Fuel";
					break;
				case 3:
					sRet = "Maintenance Overdue";
					break;
				case 4:
					sRet = "Reserved 1";
					break;
				case 5:
					sRet = "Reserved 2";
					break;
				case 6:
					sRet = "Reserved 3";
					break;
				case 7:
					sRet = "Reserved 4";
					break;
				case 8:
					sRet = "Immediate Attention Required";
					break;
				case 9:
					sRet = "Reserved 5";
					break;
				case 10:
					sRet = "Reserved 6";
					break;
				case 11:
					sRet = "Reserved 7";
					break;
				case 12:
					sRet = "Reserved 8";
					break;
				case 13:
					sRet = "Reserved 9";
					break;
				case 14:
					sRet = "Reserved 10";
					break;
				case 15:
					sRet = "System Failure";
					break;
				default:
					sRet = "Unknown";
					break;
			}
			return sRet;
		}
		#endregion
	}
}

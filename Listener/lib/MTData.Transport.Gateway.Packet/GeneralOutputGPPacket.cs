using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for GeneralOutputGPPacket.
	/// </summary>
    [Serializable]
    public class GeneralOutputGPPacket : GatewayProtocolPacket
	{
		public const Byte GEN_OUTPUT = 0x99;
		#region Variables
		public Byte cOutputNumber;
		public Byte cSequence;
		#endregion


		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncOutputNumber : "); builder.Append(cOutputNumber);
			builder.Append("\r\ncSequence : "); builder.Append(cSequence);

			return builder.ToString();
		}

		public GeneralOutputGPPacket(string serverTime_DateFormat) : base (serverTime_DateFormat)
		{
			cMsgType = GEN_OUTPUT;
		}

		public GeneralOutputGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			cMsgType = GEN_OUTPUT;
		}

		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0;
			if(mDataField == null)
				mDataField = new Byte[2];
			if (mDataField.Length != 2)
			{
				mDataField = new Byte[2];
			}
			mDataField[encPos++] = cOutputNumber;
			mDataField[encPos++] = cSequence;

		// And wrap it in the standard overhead bytes
		base.Encode(ref theData);
		}

	}
}

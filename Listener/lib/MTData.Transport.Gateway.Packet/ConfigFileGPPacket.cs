using System;
using System.Collections.Generic;
using System.Text;


namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Encapsulates a NEW_FILE config packet.
	/// </summary>
    [Serializable]
    public class ConfigFileGPPacket : GatewayProtocolPacket
	{
		public const Byte CONFIG_NEW_FILE = 0xD1;
		private const int CONFIG_NEW_FILE_DATA_LENGTH = 591;  

		public const int CONFIG_DEFAULT_RESEND_TIME = 300;

        /// <summary>
        /// object describing a channel
        /// </summary>
        public struct Channel
        {
            public int ChannelNumber;
            public int ChannelType;

            public Channel(int channelNumber, int channelType)
            {
                ChannelNumber = channelNumber;
                ChannelType = channelType;
            }
        }

		#region Variables
		public byte cConfigFileNo;
		public byte cVersionMajor;
		public byte cVersionMinor;

		public byte cListenerVersionMajor;
		public byte cListenerVersionMinor;
		public byte cListenerVersionRevison;

		public byte cAckTime;
		public byte cMDTType;
		public byte cIOType;
		public int iResendTime;
		public byte cRetryCount;
		public int iMaxSendLength;
		public byte cPingRetriesTime;
        public byte GpsTuningActive;
        public byte Low_Speed_Delta_Threshold_In_Knots;
        public byte High_Speed_Delta_Threshold_In_Knots;
        public byte Hdop_Hysteresis_Threshold;
        public byte Seconds_For_Averaging_Speed;
        public byte Hysterisis_recovery_good_values;
        public byte NumberOfSatellitesThreshold;
        public byte GPS_CancelHysteresisTime;

		public int iUnitFreqReportSecs;
		public int iUnitFreqReportMins;
		public int iUnitFreqReportExcpDist;
		public int iUnitReportDistance;
		public int iGPSNotValidReport;
		public byte cLastSecGPSStores;
		public byte cLastMinGPSStores;
		public byte cLastHourGPSStores;
		public byte cLastXMinGPSStores;
		public byte cLastXMinPeriod;

		public int iSendGPSHistTime;

		public byte cMaxSpeed;
		public byte cMinSpeed;

		public int iGPSRules;
		public int iGPSRules2;
		public int iGPSRules3;

		public int iSpeedRules;
		public int iSpeedRules2;
		public int iSpeedRules3;
        public int iSpeedRules4;

        public byte cSpeedZone1Tolerace = (byte)0x00;
        public byte cSpeedZone2Tolerace = (byte)0x00;
        public byte cSpeedZone3Tolerace = (byte)0x00;
        public byte cSpeedZone4Tolerace = (byte)0x00;

		public int iSendingRules;
		public int iSendingRules2;
		public int iSendingRules3;

		public int iSetPointRules;
		public int iSetPointRules2;
		public int iSetPointRules3;

		public int iRoutePointRules;
		public int iRoutePointRules2;
		public int iRoutePointRules3;

		public int iStatusRules;
		public int iStatusRules2;
		public int iStatusRules3;

		public int iSMSRules;
		public int iSMSRules2;
		public int iSMSRules3;

		public byte cOverspeedZone1 = (byte) 0x00;
		public byte cOverspeedZone2 = (byte) 0x00;
		public byte cOverspeedZone3 = (byte) 0x00;
        public byte cOverspeedZone4 = (byte)0x00;

        public byte cOverspeedZone1DecimalPlaces = (byte)0x00;
        public byte cOverspeedZone2DecimalPlaces = (byte)0x00;
        public byte cOverspeedZone3DecimalPlaces = (byte)0x00;
        public byte cOverspeedZone4DecimalPlaces = (byte)0x00;
		public byte cVehicleSpeedZone1 = (byte) 0x00;
		public byte cVehicleSpeedZone2 = (byte) 0x00;
		public byte cVehicleSpeedZone3 = (byte) 0x00;

		public byte cRPMSpeedZone1 = (byte) 0x00;
		public byte cRPMSpeedZone2 = (byte) 0x00;
		public byte cRPMSpeedZone3 = (byte) 0x00;

		public byte cAngelGearSpeed = (byte) 0x00;
		public byte[] cAngelGearRPM = new byte[2];
		public byte cAngelGearTime = (byte) 0x00;

		public byte cInput1RPMCounterDivideFactor = (byte) 0x00;
		public byte cInput2VehicleSpeedCounterDivideFactor = (byte) 0x00;

		//	Fatigue reporting
		public int iFatigueThresholdMinutesContinuousIgnOn = 0;
		public int iFatigueThresholdMinutes24Hrs = 0;
		// Refrigeration Reporting
		public int iRefrigerationFlags = 0;
		public int iRefrigerationMinVolts = 0;
		public int iRefrigerationFuelAlert = 0;
		public int iRefrigerationDwellSetPoint = 0;
		public int iRefrigerationDwellSupply = 0;
		public int iRefrigerationDwellReturn = 0;
		public int iRefrigerationDwellEvap = 0;
		public int iRefrigerationZone1SetPoint = 0;
		public int iRefrigerationZone1SetPointTol = 0;
		public int iRefrigerationZone1Supply = 0;
		public int iRefrigerationZone1SupplyTol = 0;
		public int iRefrigerationZone1Return = 0;
		public int iRefrigerationZone1ReturnTol = 0;
		public int iRefrigerationZone1Evap = 0;
		public int iRefrigerationZone1EvapTol = 0;
		public int iRefrigerationZone2SetPoint = 0;
		public int iRefrigerationZone2SetPointTol = 0;
		public int iRefrigerationZone2Supply = 0;
		public int iRefrigerationZone2SupplyTol = 0;
		public int iRefrigerationZone2Return = 0;
		public int iRefrigerationZone2ReturnTol = 0;
		public int iRefrigerationZone2Evap = 0;
		public int iRefrigerationZone2EvapTol = 0;
		public int iRefrigerationZone3SetPoint = 0;
		public int iRefrigerationZone3SetPointTol = 0;
		public int iRefrigerationZone3Supply = 0;
		public int iRefrigerationZone3SupplyTol = 0;
		public int iRefrigerationZone3Return = 0;
		public int iRefrigerationZone3ReturnTol = 0;
		public int iRefrigerationZone3Evap = 0;
		public int iRefrigerationZone3EvapTol = 0;

		public int iIgnitionOnRules;
		public int iIgnitionOffRules;

		public int iInputRules;
		public int iInput1Rules;
		public int iInput2Rules;
		public int iInput3Rules;
		public int iInput4Rules;
		public int iInput5Rules;
		public int iInput6Rules;
		public int iInput7Rules;
		public ulong iInput8Rules;
		public ulong iInput9Rules;
		public ulong iInput10Rules;

		public int iInput1OffRules;
		public int iInput2OffRules;
		public int iInput3OffRules;
		public int iInput4OffRules;
		//public int iInput5OffRules;
		//public int iInput6OffRules;
		//public int iInput7OffRules;
        public CardReaderConfiguration CardReaderConfiguration;        
		public ulong iInput8OffRules;
		public ulong iInput9OffRules;
		public ulong iInput10OffRules;

		// For MobileApp:
		public int iOutputRules;
		public int iOutput1Rules;
		public int iOutput2Rules;
		public int iOutput3Rules;
		public int iOutput4Rules;
		public int iOutput5Rules;
		public int iOutput6Rules;
		public int iOutput7Rules;
		public int iOutput8Rules;
		public int iOutput9Rules;
		public int iOutput10Rules;
		// For TransportApp: - engine limits
		public int iMaxCoolantTempLimit;
		public int iMaxOilTempLimit;
		public int iMinOilPressLow;
		public int iMinOilPressHigh;
		public int iMinOilPressLowRPM;
		public int iMinOilPressHighRPM;
		public int iMaxRPM;
		public float fGForceAccelerationLimit;
		public float fGForceBrakingLimit;
		public float fGForceLRLimit;
		public float fGForceAccident;

		public int iOutput1Data;
		public int iOutput2Data;
		public int iOutput3Data;
		public int iOutput4Data;
		public int iOutput5Data;
		public int iOutput6Data;
		public int iOutput7Data;
		public int iOutput8Data;
		public long iOutput9Data;
		public long iOutput10Data;
		public int iConcreteStationaryMinutes = 0;

		public long lSMSNetworkNo;

		public long lSMSNotifyNo1;
		public long lSMSNotifyNo2;
		public long lSMSNotifyNo3;
		public long lSMSNotifyNo4;
		public long lSMSNotifyNo5;
		public long lSMSNotifyNo6;

		public long lPhoneNumber1;
		public long lPhoneNumber2;
		public long lPhoneNumber3;
		public long lPhoneNumber4;
		public long lPhoneNumber5;
		public long lPhoneNumber6;

		public string sMDTIPAddress = "";
		public int iMDTPortAddress = 0;
		public int iMDTKeepAliveFreq = 0;
	    public List<ConfigFileGPPacketECMAlert> _customECMAlerts;

		private string _serverTime_DateFormat = "";

        /// <summary>
        /// the channels in this config - max size 10
        /// </summary>
        public List<Channel> Channels;
        public int MAXIMUM_NUMBER_OF_CHANNELS = 10;

        public short TrailerReportFreqPower;
        public short TrailerReportFreqBattery;
        public short TrailerReportFreqLowBattery;
        public short TrailerReportFreqStationary;
        public short TrailerBatteryLow;
        public short TrailerBatteryCritical;
        public short TrailerBatteryShutdown;
        #endregion


		/// <summary>
		/// //////////////////////////////////////////////////////////////
		/// </summary>

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncConfigFileNo : "); builder.Append(cConfigFileNo);
			builder.Append("\r\ncVersionMajor : "); builder.Append(cVersionMajor);
			builder.Append("\r\ncVersionMinor : "); builder.Append(cVersionMinor);
			builder.Append("\r\ncAckTime : "); builder.Append(cAckTime);
			builder.Append("\r\ncMDTType : "); builder.Append(cMDTType);
			builder.Append("\r\ncIOType : "); builder.Append(cIOType);
			builder.Append("\r\niResendTime : "); builder.Append(iResendTime);
			builder.Append("\r\ncRetryCount : "); builder.Append(cRetryCount);
			builder.Append("\r\niMaxSendLength : "); builder.Append(iMaxSendLength);
			builder.Append("\r\ncPingRetriesTime : "); builder.Append(cPingRetriesTime);
			builder.Append("\r\niUnitFreqReportSecs : "); builder.Append(iUnitFreqReportSecs);
			builder.Append("\r\niUnitFreqReportMins : "); builder.Append(iUnitFreqReportMins);
			builder.Append("\r\niUnitFreqReportExcpDist : "); builder.Append(iUnitFreqReportExcpDist);
			builder.Append("\r\niUnitReportDistance : "); builder.Append(iUnitReportDistance);
			builder.Append("\r\niGPSNotValidReport : "); builder.Append(iGPSNotValidReport);
			builder.Append("\r\ncLastSecGPSStores : "); builder.Append(cLastSecGPSStores);
			builder.Append("\r\ncLastMinGPSStores : "); builder.Append(cLastMinGPSStores);
			builder.Append("\r\ncLastHourGPSStores : "); builder.Append(cLastHourGPSStores);
			builder.Append("\r\ncLastXMinGPSStores : "); builder.Append(cLastXMinGPSStores);
			builder.Append("\r\ncLastXMinPeriod : "); builder.Append(cLastXMinPeriod);
			builder.Append("\r\niSendGPSHistTime : "); builder.Append(iSendGPSHistTime);
			builder.Append("\r\ncMaxSpeed : "); builder.Append(cMaxSpeed);
			builder.Append("\r\ncMinSpeed : "); builder.Append(cMinSpeed);
			builder.Append("\r\niGPSRules : "); builder.Append(iGPSRules);
			builder.Append("\r\niGPSRules2 : "); builder.Append(iGPSRules2);
			builder.Append("\r\niGPSRules3 : "); builder.Append(iGPSRules3);

			builder.Append("\r\nOverspeed Zone 1 Flags : "); builder.Append(iSpeedRules);
            builder.Append("\r\nOverspeed Zone 2 Flags : "); builder.Append(iSpeedRules2);
            builder.Append("\r\nOverspeed Zone 3 Flags : "); builder.Append(iSpeedRules3);

			builder.Append("\r\niSendingRules : "); builder.Append(iSendingRules);
			builder.Append("\r\niSendingRules2 : "); builder.Append(iSendingRules2);
			builder.Append("\r\niSendingRules3 : "); builder.Append(iSendingRules3);

			builder.Append("\r\niSetPointRules : "); builder.Append(iSetPointRules);
			builder.Append("\r\niSetPointRules2 : "); builder.Append(iSetPointRules2);
			builder.Append("\r\niSetPointRules3 : "); builder.Append(iSetPointRules3);

			builder.Append("\r\niRoutePointRules : "); builder.Append(iRoutePointRules);
			builder.Append("\r\niRoutePointRules2 : "); builder.Append(iRoutePointRules2);
			builder.Append("\r\niRoutePointRules3 : "); builder.Append(iRoutePointRules3);

			builder.Append("\r\niStatusRules : "); builder.Append(iStatusRules);
			builder.Append("\r\niStatusRules2 : "); builder.Append(iStatusRules2);
			builder.Append("\r\niStatusRules3 : "); builder.Append(iStatusRules3);

			builder.Append("\r\niSMSRules : "); builder.Append(iSMSRules);
			builder.Append("\r\niSMSRules2 : "); builder.Append(iSMSRules2);
			builder.Append("\r\niSMSRules3 : "); builder.Append(iSMSRules3);

			builder.Append("\r\niIgnitionOnRules : "); builder.Append(iIgnitionOnRules);
			builder.Append("\r\niIgnitionOffRules : "); builder.Append(iIgnitionOffRules);

			builder.Append("\r\niInputRules : "); builder.Append(iInputRules);
			builder.Append("\r\niInput1Rules : "); builder.Append(iInput1Rules);
			builder.Append("\r\niInput2Rules : "); builder.Append(iInput2Rules);
			builder.Append("\r\niInput3Rules : "); builder.Append(iInput3Rules);
			builder.Append("\r\niInput4Rules : "); builder.Append(iInput4Rules);
			builder.Append("\r\niInput5Rules : "); builder.Append(iInput5Rules);
			builder.Append("\r\niInput6Rules : "); builder.Append(iInput6Rules);
			builder.Append("\r\niInput7Rules : "); builder.Append(iInput7Rules);
			builder.Append("\r\niInput8Rules : "); builder.Append(iInput8Rules);
			builder.Append("\r\niInput9Rules : "); builder.Append(iInput9Rules);
			builder.Append("\r\niInput10Rules : "); builder.Append(iInput10Rules);

			builder.Append("\r\niInput1OffRules : "); builder.Append(iInput1OffRules);
			builder.Append("\r\niInput2OffRules : "); builder.Append(iInput2OffRules);
			builder.Append("\r\niInput3OffRules : "); builder.Append(iInput3OffRules);
			builder.Append("\r\niInput4OffRules : "); builder.Append(iInput4OffRules);
            // Bytes are now being used by the Card Read Configuration
            //builder.Append("\r\niInput5OffRules : "); builder.Append(iInput5OffRules);
            //builder.Append("\r\niInput6OffRules : "); builder.Append(iInput6OffRules);
            //builder.Append("\r\niInput7OffRules : "); builder.Append(iInput7OffRules);
			builder.Append("\r\niInput8OffRules : "); builder.Append(iInput8OffRules);
			builder.Append("\r\niInput9OffRules : "); builder.Append(iInput9OffRules);
			builder.Append("\r\niInput10OffRules : "); builder.Append(iInput10OffRules);

			builder.Append("\r\niOutputRules : "); builder.Append(iOutputRules);
			builder.Append("\r\niOutput1Rules : "); builder.Append(iOutput1Rules);
			builder.Append("\r\niOutput2Rules : "); builder.Append(iOutput2Rules);
			builder.Append("\r\niOutput3Rules : "); builder.Append(iOutput3Rules);
			builder.Append("\r\niOutput4Rules : "); builder.Append(iOutput4Rules);
			builder.Append("\r\niOutput5Rules : "); builder.Append(iOutput5Rules);
			builder.Append("\r\niOutput6Rules : "); builder.Append(iOutput6Rules);
			builder.Append("\r\niOutput7Rules : "); builder.Append(iOutput7Rules);
			builder.Append("\r\niOutput8Rules : "); builder.Append(iOutput8Rules);
			builder.Append("\r\niOutput9Rules : "); builder.Append(iOutput9Rules);
			builder.Append("\r\niOutput10Rules : "); builder.Append(iOutput10Rules);

			builder.Append("\r\niMaxCoolantTempLimit : "); builder.Append(iMaxCoolantTempLimit);
			builder.Append("\r\niMaxOilTempLimit : "); builder.Append(iMaxOilTempLimit);
			builder.Append("\r\niMinOilPressLow : "); builder.Append(iMinOilPressLow);
			builder.Append("\r\niMinOilPressHigh : "); builder.Append(iMinOilPressHigh);
			builder.Append("\r\niMinOilPressLowRPM : "); builder.Append(iMinOilPressLowRPM);
			builder.Append("\r\niMinOilPressHighRPM : "); builder.Append(iMinOilPressHighRPM);
			builder.Append("\r\niMaxRPM : "); builder.Append(iMaxRPM);
			builder.Append("\r\nfGForceAccelerationLimit : "); builder.Append(fGForceAccelerationLimit);
			builder.Append("\r\nfGForceBrakingLimit : "); builder.Append(fGForceBrakingLimit);
			builder.Append("\r\nfGForceLRLimit : "); builder.Append(fGForceLRLimit);
			builder.Append("\r\nfGForceAccident : "); builder.Append(fGForceAccident);

			builder.Append("\r\niOutput1Data : "); builder.Append(iOutput1Data);
			builder.Append("\r\niOutput2Data : "); builder.Append(iOutput2Data);
			builder.Append("\r\niOutput3Data : "); builder.Append(iOutput3Data);
			builder.Append("\r\niOutput4Data : "); builder.Append(iOutput4Data);
			builder.Append("\r\niOutput5Data : "); builder.Append(iOutput5Data);
			builder.Append("\r\niOutput6Data : "); builder.Append(iOutput6Data);
			builder.Append("\r\niOutput7Data : "); builder.Append(iOutput7Data);
			builder.Append("\r\niOutput8Data : "); builder.Append(iOutput8Data);
			builder.Append("\r\niOutput9Data : "); builder.Append(iOutput9Data);
			builder.Append("\r\niOutput10Data : "); builder.Append(iOutput10Data);

			builder.Append("\r\nlSMSNetworkNo : "); builder.Append(lSMSNetworkNo);

			builder.Append("\r\nlSMSNotifyNo1 : "); builder.Append(lSMSNotifyNo1);
			builder.Append("\r\nlSMSNotifyNo2 : "); builder.Append(lSMSNotifyNo2);
			builder.Append("\r\nlSMSNotifyNo3 : "); builder.Append(lSMSNotifyNo3);
			builder.Append("\r\nlSMSNotifyNo4 : "); builder.Append(lSMSNotifyNo4);
			builder.Append("\r\nlSMSNotifyNo5 : "); builder.Append(lSMSNotifyNo5);
			builder.Append("\r\nlSMSNotifyNo6 : "); builder.Append(lSMSNotifyNo6);

			builder.Append("\r\nlPhoneNumber1 : "); builder.Append(lPhoneNumber1);
			builder.Append("\r\nlPhoneNumber2 : "); builder.Append(lPhoneNumber2);
			builder.Append("\r\nlPhoneNumber3 : "); builder.Append(lPhoneNumber3);
			builder.Append("\r\nlPhoneNumber4 : "); builder.Append(lPhoneNumber4);
			builder.Append("\r\nlPhoneNumber5 : "); builder.Append(lPhoneNumber5);
			builder.Append("\r\nlPhoneNumber6 : "); builder.Append(lPhoneNumber6);

			builder.Append("\r\niFatigueThresholdMinutesContinuousIgnOn : "); builder.Append(iFatigueThresholdMinutesContinuousIgnOn);
			builder.Append("\r\niFatigueThresholdMinutes24Hrs : "); builder.Append(iFatigueThresholdMinutes24Hrs);
			builder.Append("\r\niRefrigerationFlags : "); builder.Append(iRefrigerationFlags);
			builder.Append("\r\niRefrigerationMinVolts : "); builder.Append(iRefrigerationMinVolts);
			builder.Append("\r\niRefrigerationFuelAlert : "); builder.Append(iRefrigerationFuelAlert);
			builder.Append("\r\niRefrigerationDwellSetPoint : "); builder.Append(iRefrigerationDwellSetPoint);
			builder.Append("\r\niRefrigerationDwellSupply : "); builder.Append(iRefrigerationDwellSupply);
			builder.Append("\r\niRefrigerationDwellReturn : "); builder.Append(iRefrigerationDwellReturn);
			builder.Append("\r\niRefrigerationDwellEvap : "); builder.Append(iRefrigerationDwellEvap);
			builder.Append("\r\niRefrigerationZone1SetPoint : "); builder.Append(iRefrigerationZone1SetPoint);
			builder.Append("\r\niRefrigerationZone1SetPointTol : "); builder.Append(iRefrigerationZone1SetPointTol);
			builder.Append("\r\niRefrigerationZone1Supply : "); builder.Append(iRefrigerationZone1Supply);
			builder.Append("\r\niRefrigerationZone1SupplyTol : "); builder.Append(iRefrigerationZone1SupplyTol);
			builder.Append("\r\niRefrigerationZone1Return : "); builder.Append(iRefrigerationZone1Return);
			builder.Append("\r\niRefrigerationZone1ReturnTol : "); builder.Append(iRefrigerationZone1ReturnTol);
			builder.Append("\r\niRefrigerationZone1Evap : "); builder.Append(iRefrigerationZone1Evap);
			builder.Append("\r\niRefrigerationZone1EvapTol : "); builder.Append(iRefrigerationZone1EvapTol);
			builder.Append("\r\niRefrigerationZone2SetPoint : "); builder.Append(iRefrigerationZone2SetPoint);
			builder.Append("\r\niRefrigerationZone2SetPointTol : "); builder.Append(iRefrigerationZone2SetPointTol);
			builder.Append("\r\niRefrigerationZone2Supply : "); builder.Append(iRefrigerationZone2Supply);
			builder.Append("\r\niRefrigerationZone2SupplyTol : "); builder.Append(iRefrigerationZone2SupplyTol);
			builder.Append("\r\niRefrigerationZone2Return : "); builder.Append(iRefrigerationZone2Return);
			builder.Append("\r\niRefrigerationZone2ReturnTol : "); builder.Append(iRefrigerationZone2ReturnTol);
			builder.Append("\r\niRefrigerationZone2Evap : "); builder.Append(iRefrigerationZone2Evap);
			builder.Append("\r\niRefrigerationZone2EvapTol : "); builder.Append(iRefrigerationZone2EvapTol);
			builder.Append("\r\niRefrigerationZone3SetPoint : "); builder.Append(iRefrigerationZone3SetPoint);
			builder.Append("\r\niRefrigerationZone3SetPointTol : "); builder.Append(iRefrigerationZone3SetPointTol);
			builder.Append("\r\niRefrigerationZone3Supply : "); builder.Append(iRefrigerationZone3Supply);
			builder.Append("\r\niRefrigerationZone3SupplyTol : "); builder.Append(iRefrigerationZone3SupplyTol);
			builder.Append("\r\niRefrigerationZone3Return : "); builder.Append(iRefrigerationZone3Return);
			builder.Append("\r\niRefrigerationZone3ReturnTol : "); builder.Append(iRefrigerationZone3ReturnTol);
			builder.Append("\r\niRefrigerationZone3Evap : "); builder.Append(iRefrigerationZone3Evap);
			builder.Append("\r\niRefrigerationZone3EvapTol : "); builder.Append(iRefrigerationZone3EvapTol);
            int numberOfChannels = NumberOfChannels;
            builder.Append("\r\nChannel Count : "); builder.Append(numberOfChannels);
            for (int i=0; i<numberOfChannels; i++)
            {
                builder.Append(string.Format("\r\nChannel {0} : Number {1}, Type {2}", i, Channels[i].ChannelNumber, Channels[i].ChannelType));
            }
            builder.Append("\r\nCard Reader Configuration : "); builder.Append((CardReaderConfiguration != null?"null":this.CardReaderConfiguration.ToString()));
			return builder.ToString();
		}

		public ConfigFileGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
            Channels = new List<Channel>();
			cMsgType = CONFIG_NEW_FILE;
			bIsFileType = true;
			iResendTime = CONFIG_DEFAULT_RESEND_TIME;
            _customECMAlerts = new List<ConfigFileGPPacketECMAlert>();
			ClearConfigFields();
		}

		public ConfigFileGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
            Channels = new List<Channel>();
			cMsgType = CONFIG_NEW_FILE;
			bIsFileType = true;
			iResendTime = CONFIG_DEFAULT_RESEND_TIME;
            _customECMAlerts = new List<ConfigFileGPPacketECMAlert>();
			ClearConfigFields();
		}

		/// <summary>
		///  Convert a string representation of a phone number to a BCD-encoded long
		/// with a maximum length of 6 bytes (12 digits in the string)</summary>
		/// <param name="s">The string to encode</param>
		/// <returns>The BCD-encoded string, filled MSB -> LSB</returns>
		public static long StringToBCDLong(string s)
		{
			// Hitting an 0xF tells the unit to stop decoding
			long returnValue = 0;
			int encodeNibble = 11;

			if (s == null) return 0;

			// Remove any whitespace from the ends:
			s = s.Trim();
			
			// Get characters:
			char[] charArray = s.ToCharArray();
			
			foreach (char c in charArray)
			{
				if (c != ' ') // skip spaces
				{
					char bcdEnc = System.Convert.ToChar(c - 0x30); // a single nibble, values 0-9
					// Place the nibble in the right spot:
					returnValue |= ((long) bcdEnc << (4 * encodeNibble--));
					if (encodeNibble < 0) break;
				}
			}
				
			while (encodeNibble >= 0)
			{	// Pad with with 0xF
				returnValue |= ((long) 0xf << (4 * encodeNibble--));
			}
			
			return returnValue;
		}
		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0;
			int checksum = 0;
			byte[] bConvert = null;

			if (mDataField.Length != CONFIG_NEW_FILE_DATA_LENGTH)
			{
				mDataField = new Byte[CONFIG_NEW_FILE_DATA_LENGTH];
			}

			mDataField[encPos++] = GSP_SOH;
			mDataField[encPos++] = GSP_SM; // FIXME: STX??
			mDataField[encPos++] = cConfigFileNo;
			mDataField[encPos++] = cVersionMajor;
			mDataField[encPos++] = cVersionMinor;
			encPos += 2; // File size - we'll work that out later

			mDataField[encPos++] = cListenerVersionMajor;
			mDataField[encPos++] = cListenerVersionMinor;
			mDataField[encPos++] = cListenerVersionRevison;
			encPos += 2;	// 2 Spare bytes here

			mDataField[encPos++] = cAckTime;
			// Hardware Type:
			mDataField[encPos++] = cMDTType;
			mDataField[encPos++] = cIOType;

			// Resend time if no ack:
			mDataField[encPos++] = (Byte) ((iResendTime & 0xFF));
			mDataField[encPos++] = (Byte) ((iResendTime & 0xFF00) >> 8);
			
			// Retries:
			mDataField[encPos++] = cRetryCount;
			// Maximum Send length before requiring an ACK:
			mDataField[encPos++] = (Byte) ((iMaxSendLength & 0xFF));
			mDataField[encPos++] = (Byte) ((iMaxSendLength & 0xFF00) >> 8);
			// PPP Ping details:
			mDataField[encPos++] = cPingRetriesTime;

			encPos += 4;		// 4 SPARE 1 bytes here
			
			// Reporting intervals:
			// All multi-byte values must be written LSB first!
			mDataField[encPos++] = (Byte) ((iUnitFreqReportSecs & 0xFF));
			mDataField[encPos++] = (Byte) ((iUnitFreqReportSecs & 0xFF00) >> 8);
			
			mDataField[encPos++] = (Byte) ((iUnitFreqReportMins & 0xFF));
			mDataField[encPos++] = (Byte) ((iUnitFreqReportMins & 0xFF00) >> 8);			
			
			mDataField[encPos++] = (Byte) ((iUnitFreqReportExcpDist & 0xFF));
			mDataField[encPos++] = (Byte) ((iUnitFreqReportExcpDist & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iUnitFreqReportExcpDist & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iUnitFreqReportExcpDist & 0xFF000000) >> 24);


			mDataField[encPos++] = (Byte) ((iUnitReportDistance & 0xFF));
			mDataField[encPos++] = (Byte) ((iUnitReportDistance & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iUnitReportDistance & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iUnitReportDistance & 0xFF000000) >> 24);

            bConvert = BitConverter.GetBytes(TrailerReportFreqPower);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
            bConvert = BitConverter.GetBytes(TrailerReportFreqBattery);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
            bConvert = BitConverter.GetBytes(TrailerReportFreqLowBattery);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
            bConvert = BitConverter.GetBytes(TrailerReportFreqStationary);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
            bConvert = BitConverter.GetBytes(TrailerBatteryLow);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
			
			// GPS Not Valid:
			mDataField[encPos++] = (Byte) (iGPSNotValidReport & 0xFF);
			mDataField[encPos++] = (Byte) ((iGPSNotValidReport & 0xFF00) >> 8);

            bConvert = BitConverter.GetBytes(TrailerBatteryCritical);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
            bConvert = BitConverter.GetBytes(TrailerBatteryShutdown);
            mDataField[encPos++] = bConvert[0];
            mDataField[encPos++] = bConvert[1];
            encPos += 6;		// 10 SPARE3 bytes here

			// GPS Stores:
			mDataField[encPos++] = cLastSecGPSStores;
			mDataField[encPos++] = cLastMinGPSStores;
			mDataField[encPos++] = cLastHourGPSStores;
			mDataField[encPos++] = cLastXMinGPSStores;
			mDataField[encPos++] = cLastXMinPeriod;
			mDataField[encPos++] = (Byte) (iSendGPSHistTime & 0xFF);
			mDataField[encPos++] = (Byte) ((iSendGPSHistTime & 0xFF00) >> 8);

			encPos += 10;		// 10 SPARE4 bytes here	
				
			// Speed thresholds:
			mDataField[encPos++] = cMaxSpeed;
			mDataField[encPos++] = cMinSpeed;
			encPos += 5;		// 5 Spare5 bytes here

		// GPS Rules:
			mDataField[encPos++] = (Byte) ((iGPSRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iGPSRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iGPSRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iGPSRules & 0xFF000000) >> 24);
		
			mDataField[encPos++] = (Byte) ((iGPSRules2 & 0xFF));
			mDataField[encPos++] = (Byte) ((iGPSRules2 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iGPSRules2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iGPSRules2 & 0xFF000000) >> 24);
		
			mDataField[encPos++] = (Byte) ((iGPSRules3 & 0xFF));
			mDataField[encPos++] = (Byte) ((iGPSRules3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iGPSRules3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iGPSRules3 & 0xFF000000) >> 24);
			
			// These bytes are no longer in use
            mDataField[encPos++] = GpsTuningActive;
            mDataField[encPos++] = NumberOfSatellitesThreshold;
            mDataField[encPos++] = Low_Speed_Delta_Threshold_In_Knots;
            mDataField[encPos++] = High_Speed_Delta_Threshold_In_Knots;

            mDataField[encPos++] = Hdop_Hysteresis_Threshold;
            mDataField[encPos++] = GPS_CancelHysteresisTime;
            mDataField[encPos++] = 0x00;
            mDataField[encPos++] = Seconds_For_Averaging_Speed;

            mDataField[encPos++] = Hysterisis_recovery_good_values;
            mDataField[encPos++] = (byte)0x00;
            mDataField[encPos++] = (byte)0x00;
            mDataField[encPos++] = (byte)0x00;
			
			// Sending Rules:
			mDataField[encPos++] = (Byte) ((iSendingRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iSendingRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSendingRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSendingRules & 0xFF000000) >> 24);
			
			bConvert = BitConverter.GetBytes(iSendingRules2);
			mDataField[encPos++] = bConvert[0];											// G-Force Calibrate when <
			mDataField[encPos++] = bConvert[1];
			mDataField[encPos++] = bConvert[2];											// G-Force Calibrate when >
			mDataField[encPos++] = bConvert[3];
			
			bConvert = BitConverter.GetBytes(iSendingRules3);
			mDataField[encPos++] = bConvert[0];											// G-Force Calibrate report when calibration >
			mDataField[encPos++] = bConvert[1];
			mDataField[encPos++] = bConvert[2];											// G-Force Calibrate Spare Bytes
			mDataField[encPos++] = bConvert[3];
			
			// SetPoint Rules:
			mDataField[encPos++] = (Byte) ((iSetPointRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iSetPointRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSetPointRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSetPointRules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iSetPointRules2 & 0xFF));
			mDataField[encPos++] = (Byte) ((iSetPointRules2 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSetPointRules2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSetPointRules2 & 0xFF000000) >> 24);

            // MinSatsForValidSpeed - 2bytes, MinSatsForValidSpeed 2 bytes
			mDataField[encPos++] = (Byte) ((iSetPointRules3 & 0xFF));
			mDataField[encPos++] = (Byte) ((iSetPointRules3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSetPointRules3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSetPointRules3 & 0xFF000000) >> 24);

            
			mDataField[encPos++] = (Byte) ((iRoutePointRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iRoutePointRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iRoutePointRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iRoutePointRules & 0xFF000000) >> 24);
            // Status Rules: 4 bytes for sattelite mail box checking frequency when ignition is on
			mDataField[encPos++] = (Byte) ((iRoutePointRules2 & 0xFF));
			mDataField[encPos++] = (Byte) ((iRoutePointRules2 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iRoutePointRules2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iRoutePointRules2 & 0xFF000000) >> 24);
            // Status Rules: 4 bytes for sattelite mail box checking frequency when ignition is off
			mDataField[encPos++] = (Byte) ((iRoutePointRules3 & 0xFF));
			mDataField[encPos++] = (Byte) ((iRoutePointRules3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iRoutePointRules3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iRoutePointRules3 & 0xFF000000) >> 24);
			
			// Status Rules: 4 bytes for sattelite reporting frequency
			mDataField[encPos++] = (Byte) ((iStatusRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iStatusRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iStatusRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iStatusRules & 0xFF000000) >> 24);

            mDataField[encPos++] = (Byte)((iStatusRules2 & 0xFF));             // Sattelite KB limit, number of hours to measure over Hours

			mDataField[encPos++] = (Byte) ((iStatusRules2 & 0xFF00) >> 8);     // Sattelite KB limit
			mDataField[encPos++] = (Byte) ((iStatusRules2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iStatusRules2 & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iStatusRules3 & 0xFF));
			mDataField[encPos++] = (Byte) ((iStatusRules3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iStatusRules3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iStatusRules3 & 0xFF000000) >> 24);
			
			// SMS Rules:
			mDataField[encPos++] = (Byte) ((iSMSRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iSMSRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSMSRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSMSRules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iSMSRules2 & 0xFF));
			mDataField[encPos++] = (Byte) ((iSMSRules2 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSMSRules2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSMSRules2 & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iSMSRules3 & 0xFF));
			mDataField[encPos++] = (Byte) ((iSMSRules3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iSMSRules3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iSMSRules3 & 0xFF000000) >> 24);

			if (sMDTIPAddress.Length > 0)
			{
				string[] sSplitIP = sMDTIPAddress.Split(".".ToCharArray());
				bConvert = new byte[4];
				if (sSplitIP.Length == 4)
				{
					// Send the IP Address
					bConvert = BitConverter.GetBytes(Convert.ToInt32(sSplitIP[0]));
					mDataField[encPos++] = bConvert[0];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(sSplitIP[1]));
					mDataField[encPos++] = bConvert[0];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(sSplitIP[2]));
					mDataField[encPos++] = bConvert[0];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(sSplitIP[3]));
					mDataField[encPos++] = bConvert[0];
					// Send the port number
					bConvert = BitConverter.GetBytes(iMDTPortAddress);
					mDataField[encPos++] = bConvert[0];
					mDataField[encPos++] = bConvert[1];
					// Send the Keep alive frequencey
					bConvert = BitConverter.GetBytes(iMDTKeepAliveFreq);
					mDataField[encPos++] = bConvert[0];
					mDataField[encPos++] = bConvert[1];
				}
				else
					encPos += 8;	// Invalid IP, send 8 blank bytes
			}
			else
				encPos += 8;		// No IP Sepcified, send 8 blank bytes

			mDataField[encPos++] = cOverspeedZone1;												// Overspeed Zone 1
			mDataField[encPos++] = cOverspeedZone2;												// Overspeed Zone 2
			mDataField[encPos++] = cOverspeedZone3;												// Overspeed Zone 3
			mDataField[encPos++] = cVehicleSpeedZone1;											// Vehicle Speed Zone 1
			mDataField[encPos++] = cVehicleSpeedZone2;											// Vehicle Speed Zone 2
			mDataField[encPos++] = cVehicleSpeedZone3;											// Vehicle Speed Zone 3
			mDataField[encPos++] = cRPMSpeedZone1;												// RPM Speed Zone 1
			mDataField[encPos++] = cRPMSpeedZone2;												// RPM Speed Zone 2
			mDataField[encPos++] = cRPMSpeedZone3;												// RPM Speed Zone 3
			mDataField[encPos++] = cAngelGearSpeed;												// Angel Gear Speed
			mDataField[encPos++] = cAngelGearRPM[0];													// Angel Gear RPM
			mDataField[encPos++] = cAngelGearRPM[1];													// Angel Gear RPM
			mDataField[encPos++] = cAngelGearTime;													// Angel Gear Time
			mDataField[encPos++] = cInput1RPMCounterDivideFactor;						// Input 1 RPM Counter Divide Factor
			mDataField[encPos++] = cInput2VehicleSpeedCounterDivideFactor;		// Input 2 Vehicle Speed Counter Divide Factor

			encPos += 1;		// Spare 6	//	Accident Report Minutes??
			mDataField[encPos++] = (Byte) (iFatigueThresholdMinutesContinuousIgnOn & 0xFF);
			mDataField[encPos++] = (Byte) ((iFatigueThresholdMinutesContinuousIgnOn & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (iFatigueThresholdMinutes24Hrs & 0xFF);
			mDataField[encPos++] = (Byte) ((iFatigueThresholdMinutes24Hrs & 0xFF00) >> 8);
			encPos += 2;		// Spare 75
			encPos += 4;		// Spare 8

			// Ignition Rules:
			mDataField[encPos++] = (Byte) ((iIgnitionOnRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iIgnitionOnRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iIgnitionOnRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iIgnitionOnRules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iIgnitionOffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iIgnitionOffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iIgnitionOffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iIgnitionOffRules & 0xFF000000) >> 24);
			
			// Button Rules now replaced by Channels size = 32 bytes
            int numberOfChannels = NumberOfChannels;
            mDataField[encPos++] = (Byte)numberOfChannels;
            for (int i = 0; i < numberOfChannels; i++)
            {
                mDataField[encPos++] = (Byte)Channels[i].ChannelNumber;
                mDataField[encPos++] = (Byte)Channels[i].ChannelType;
            }
            //set spare bytes to be zero (32 - 1 byte for channel size - 2 bytes per channel)
            int spare = 31 - (2 * numberOfChannels);
            for (int i = 0; i < spare; i++)
            {
                mDataField[encPos++] = 0x00;
            }

            // Input Rules:
			mDataField[encPos++] = (Byte) ((iInputRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInputRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInputRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInputRules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput1Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput1Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput1Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput1Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput2Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput2Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput2Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput2Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput3Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput3Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput3Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput3Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput4Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput4Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput4Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput4Rules & 0xFF000000) >> 24);

			// The input 5 value is being used to send the concrete age values.
			mDataField[encPos++] = (byte) 0x00;
			mDataField[encPos++] = (byte) 0x00;
			mDataField[encPos++] = (byte) 0x00;
			mDataField[encPos++] = (byte) 0x00;

			mDataField[encPos++] = (Byte) ((iInput6Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput6Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput6Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput6Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput7Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput7Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput7Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput7Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput8Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput8Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput8Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput8Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput9Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput9Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput9Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput9Rules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput10Rules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput10Rules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput10Rules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput10Rules & 0xFF000000) >> 24);			
			
			// Input OFF Rules
			mDataField[encPos++] = (Byte) ((iInput1OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput1OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput1OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput1OffRules & 0xFF000000) >> 24);

			mDataField[encPos++] = (Byte) ((iInput2OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput2OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput2OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput2OffRules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput3OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput3OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput3OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput3OffRules & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) ((iInput4OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput4OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput4OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput4OffRules & 0xFF000000) >> 24);

            byte[] cardReaderData = new byte[16];
            if(this.CardReaderConfiguration != null)
                cardReaderData = this.CardReaderConfiguration.ByteArray;
            // The card reader data is variable length with a max of 16 bytes.
            int crPos = 0;
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++]:(byte) 0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);

            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);

            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
            mDataField[encPos++] = (cardReaderData.Length > crPos ? cardReaderData[crPos++] : (byte)0x00);
			
			mDataField[encPos++] = (Byte) ((iInput8OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput8OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput8OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput8OffRules & 0xFF000000) >> 24);
			
			// Fuel Economy Warning
			// iInput9OffRules = 
			//	[Byte 0] = 0x00
			//	[Byte 1] = 0x00
			//	[Byte 2] = Fuel Economy Min Value / 100 (Byte 1)
			//	[Byte 3] = Fuel Economy Min Value / 100 (Byte 0)

			mDataField[encPos++] = (Byte) ((iInput9OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput9OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput9OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput9OffRules & 0xFF000000) >> 24);
			
			// Excessive Data Usage
			// iInput10OffRules = 
			//	[Byte 0] = Kb Value (Byte 1)
			//	[Byte 1] = Kb Value (Byte 0)
			//	[Byte 2] = Hours Value (Byte 1)
			//	[Byte 3] = Hours Value (Byte 0)
			mDataField[encPos++] = (Byte) ((iInput10OffRules & 0xFF));
			mDataField[encPos++] = (Byte) ((iInput10OffRules & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iInput10OffRules & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iInput10OffRules & 0xFF000000) >> 24);
		
			// Output Rules, OR Engine limits, depending on Application:
			mDataField[encPos++] = (Byte) (((iOutputRules | iMaxCoolantTempLimit) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutputRules | iMaxCoolantTempLimit) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutputRules | iMaxCoolantTempLimit) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutputRules | iMaxCoolantTempLimit) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput1Rules | iMaxOilTempLimit) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput1Rules | iMaxOilTempLimit) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput1Rules | iMaxOilTempLimit) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput1Rules | iMaxOilTempLimit) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput2Rules | iMinOilPressLow) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput2Rules | iMinOilPressLow) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput2Rules | iMinOilPressLow) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput2Rules | iMinOilPressLow) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput3Rules | iMinOilPressHigh) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput3Rules | iMinOilPressHigh) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput3Rules | iMinOilPressHigh) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput3Rules | iMinOilPressHigh) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput4Rules | iMinOilPressLowRPM) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput4Rules | iMinOilPressLowRPM) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput4Rules | iMinOilPressLowRPM) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput4Rules | iMinOilPressLowRPM) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput5Rules | iMinOilPressHighRPM) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput5Rules | iMinOilPressHighRPM) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput5Rules | iMinOilPressHighRPM) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput5Rules | iMinOilPressHighRPM) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput6Rules | iMaxRPM) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput6Rules | iMaxRPM) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput6Rules | iMaxRPM) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput6Rules | iMaxRPM) & 0xFF000000) >> 24);
			
			int iGForceAccelerationLimit = Convert.ToInt32(fGForceAccelerationLimit * 10);
			int iGForceBrakingLimit = Convert.ToInt32(fGForceBrakingLimit * 10);
			int iGForceLRLimit = Convert.ToInt32(fGForceLRLimit * 10);
			int iGForceAccident = Convert.ToInt32(fGForceAccident * 10);

			mDataField[encPos++] = (Byte) (((iOutput7Rules | iGForceAccelerationLimit) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput7Rules | iGForceAccelerationLimit) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput7Rules | iGForceAccelerationLimit) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput7Rules | iGForceAccelerationLimit) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput8Rules | iGForceBrakingLimit) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput8Rules | iGForceBrakingLimit) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput8Rules | iGForceBrakingLimit) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput8Rules | iGForceBrakingLimit) & 0xFF000000) >> 24);
			
			mDataField[encPos++] = (Byte) (((iOutput9Rules | iGForceLRLimit) & 0xFF));
			mDataField[encPos++] = (Byte) (((iOutput9Rules | iGForceLRLimit) & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) (((iOutput9Rules | iGForceLRLimit) & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) (((iOutput9Rules | iGForceLRLimit) & 0xFF000000) >> 24);

			mDataField[encPos++] = (Byte) ((iGForceAccident & 0xFF));
			mDataField[encPos++] = (Byte) ((iGForceAccident & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iGForceAccident & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iGForceAccident & 0xFF000000) >> 24);
			
			// Output Data:
			
			mDataField[encPos++] = (Byte) ((iOutput1Data & 0xFF0000) >> 16);		// IO 1 Check Lower
			mDataField[encPos++] = (Byte) ((iOutput1Data & 0xFF00) >> 8);				// IO 1 Lower value alert
			mDataField[encPos++] = (Byte) ((iOutput1Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput2Data & 0xFF0000) >> 16);		// IO 2 Check Lower
			mDataField[encPos++] = (Byte) ((iOutput2Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput2Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput3Data & 0xFF0000) >> 16);		// IO 3 Check Lower
			mDataField[encPos++] = (Byte) ((iOutput3Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput3Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput4Data & 0xFF0000) >> 16);		// IO 4 Check Lower
			mDataField[encPos++] = (Byte) ((iOutput4Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput4Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput5Data & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iOutput5Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput5Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput6Data & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iOutput6Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput6Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput7Data & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iOutput7Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput7Data & 0xFF));
			
			mDataField[encPos++] = (Byte) ((iOutput8Data & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iOutput8Data & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iOutput8Data & 0xFF));
			
			// Concrete Values
			mDataField[encPos++] = (Byte) ((iOutput9Data & 0xFF0000) >> 16);	// Concrete mix minutes
			mDataField[encPos++] = (Byte) ((iOutput9Data & 0xFF00) >> 8);			// Concrete mix minimum revs
			mDataField[encPos++] = (Byte) ((iOutput9Data & 0xFF));						// Concrete on site speed
			
			mDataField[encPos++] = (Byte) ((iOutput10Data & 0xFF0000) >> 16);	// Concrete barrel forward
			mDataField[encPos++] = (Byte) ((iOutput10Data & 0xFF00) >> 8);		// Barrel RPM upto speed - Byte 1
			mDataField[encPos++] = (Byte) ((iOutput10Data & 0xFF));					// Barrel RPM upto speed - Byte 2
						
			mDataField[encPos++] = (Byte) ((iOutput10Data & 0xFF000000) >> 24);		// Age of concrete in Mins
			mDataField[encPos++] = (Byte) ((iOutput9Data & 0xFF000000) >> 24);		// Age of concrete in Rotations

			mDataField[encPos++] = (byte) iConcreteStationaryMinutes;		// Number of stationary minutes for On Site
			encPos += 7;			// 7 bytes of SPARE9

			// SMSNotifyNos
			// These are now big-endian as they are read out char by char
			mDataField[encPos++] = (Byte) ((lSMSNetworkNo & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNetworkNo & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNetworkNo & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNetworkNo & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNetworkNo & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNetworkNo & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo1 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo1 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo1 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo1 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo1 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo1 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo2 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo2 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo2 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo2 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo2 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo3 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo3 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo3 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo3 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo4 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo4 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo4 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo4 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo4 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo4 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo5 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo5 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo5 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo5 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo5 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo5 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo6 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo6 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo6 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo6 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo6 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSMSNotifyNo6 & 0xFF));
			

			// Phone numbers
			mDataField[encPos++] = (Byte) ((lPhoneNumber1 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lPhoneNumber1 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lPhoneNumber1 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lPhoneNumber1 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lPhoneNumber1 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lPhoneNumber1 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lPhoneNumber2 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lPhoneNumber2 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lPhoneNumber2 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lPhoneNumber2 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lPhoneNumber2 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lPhoneNumber2 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lPhoneNumber3 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lPhoneNumber3 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lPhoneNumber3 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lPhoneNumber3 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lPhoneNumber3 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lPhoneNumber3 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lPhoneNumber4 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lPhoneNumber4 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lPhoneNumber4 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lPhoneNumber4 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lPhoneNumber4 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lPhoneNumber4 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lPhoneNumber5 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lPhoneNumber5 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lPhoneNumber5 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lPhoneNumber5 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lPhoneNumber5 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lPhoneNumber5 & 0xFF));
			
			mDataField[encPos++] = (Byte) ((lPhoneNumber6 & 0xFF0000000000) >> 40);
			mDataField[encPos++] = (Byte) ((lPhoneNumber6 & 0xFF00000000) >> 32);
			mDataField[encPos++] = (Byte) ((lPhoneNumber6 & 0xFF000000) >> 24);
			mDataField[encPos++] = (Byte) ((lPhoneNumber6 & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lPhoneNumber6 & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lPhoneNumber6 & 0xFF));
			
			// Spare bytes
			mDataField[encPos++] = 0x00;	//cksum
			mDataField[encPos++] = 0x00;    //not EM any more

			#region Refrigeration Values
			#region Zone 1 Values
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1SetPoint);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1SetPointTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1Supply);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1SupplyTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1Return);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1ReturnTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1Evap);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone1EvapTol);
			#endregion
			#region Zone 2 Values
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2SetPoint);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2SetPointTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2Supply);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2SupplyTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2Return);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2ReturnTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2Evap);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone2EvapTol);
			#endregion
			#region Zone 3 Values
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3SetPoint);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3SetPointTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3Supply);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3SupplyTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3Return);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3ReturnTol);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3Evap);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationZone3EvapTol);
			#endregion
			#region Common Values
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationMinVolts);
			Add1ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationFuelAlert);
			Add4ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationFlags);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationDwellSetPoint);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationDwellSupply);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationDwellReturn);
			Add2ByteIntToDataField(ref mDataField, ref encPos, iRefrigerationDwellEvap);
			#endregion
			#endregion

            // 37 bytes to EOP

            // This section has been removed at Tims request.  He is going to setup 1708 and 1939 manually in the firmware.
            //// Add a maximum of two custom alert fields of 9 bytes each.
            //if (_customECMAlerts != null && _customECMAlerts.Count > 0)
            //{
            //    // First entry
            //    ConfigFileGPPacketECMAlert oAlert = _customECMAlerts[0];
            //    byte[] bData = oAlert.Encode();
            //    for (int iData = 0; iData < bData.Length; iData++)
            //    {
            //        mDataField[encPos++] = bData[iData];
            //    }

            //    if (_customECMAlerts.Count > 1)
            //    {
            //        // Second entry
            //        oAlert = _customECMAlerts[1];
            //        bData = oAlert.Encode();
            //        for (int iData = 0; iData < bData.Length; iData++)
            //        {
            //            mDataField[encPos++] = bData[iData];
            //        }
            //    }
            //    else
            //    {
            //        // If there is no second entry, skip over the bytes.
            //        encPos += 9;
            //    }
            //}
            //else
            //{
            //    // Two blank entries
            //    encPos += 18;
            //}

            // 37 bytes to EOP
		    // Overspeed zone 1 flags
            mDataField[encPos++] = (Byte)((iSpeedRules & 0xFF));
            mDataField[encPos++] = (Byte)((iSpeedRules & 0xFF00) >> 8);
            mDataField[encPos++] = (Byte)((iSpeedRules & 0xFF0000) >> 16);
            mDataField[encPos++] = (Byte)((iSpeedRules & 0xFF000000) >> 24);
            // Overspeed zone 2 flags
            mDataField[encPos++] = (Byte)((iSpeedRules2 & 0xFF));
            mDataField[encPos++] = (Byte)((iSpeedRules2 & 0xFF00) >> 8);
            mDataField[encPos++] = (Byte)((iSpeedRules2 & 0xFF0000) >> 16);
            mDataField[encPos++] = (Byte)((iSpeedRules2 & 0xFF000000) >> 24);
            // Overspeed zone 3 flags
            mDataField[encPos++] = (Byte)((iSpeedRules3 & 0xFF));
            mDataField[encPos++] = (Byte)((iSpeedRules3 & 0xFF00) >> 8);
            mDataField[encPos++] = (Byte)((iSpeedRules3 & 0xFF0000) >> 16);
            mDataField[encPos++] = (Byte)((iSpeedRules3 & 0xFF000000) >> 24);
            // Overspeed zone 4 flags
            mDataField[encPos++] = (Byte)((iSpeedRules4 & 0xFF));
            mDataField[encPos++] = (Byte)((iSpeedRules4 & 0xFF00) >> 8);
            mDataField[encPos++] = (Byte)((iSpeedRules4 & 0xFF0000) >> 16);
            mDataField[encPos++] = (Byte)((iSpeedRules4 & 0xFF000000) >> 24);

            // 21 bytes to EOP
            mDataField[encPos++] = cOverspeedZone4;												// Overspeed Zone 4 - Speed Limit
            mDataField[encPos++] = cSpeedZone1Tolerace;											// Overspeed Zone 1 Tolerance Seconds
            mDataField[encPos++] = cSpeedZone2Tolerace;											// Overspeed Zone 2 Tolerance Seconds
            mDataField[encPos++] = cSpeedZone3Tolerace;											// Overspeed Zone 3 Tolerance Seconds
            mDataField[encPos++] = cSpeedZone4Tolerace;											// Overspeed Zone 4 Tolerance Seconds

            // Decimal places for the overspeed zone values.
            mDataField[encPos++] = cOverspeedZone1DecimalPlaces;								// Overspeed Zone 1 Tolerance decimal places
            mDataField[encPos++] = cOverspeedZone2DecimalPlaces;								// Overspeed Zone 2 Tolerance decimal places
            mDataField[encPos++] = cOverspeedZone3DecimalPlaces;								// Overspeed Zone 3 Tolerance decimal places
            mDataField[encPos++] = cOverspeedZone4DecimalPlaces;								// Overspeed Zone 4 Tolerance decimal places

            encPos += 12; // 12 bytes left for expansion.

		    // Last but not least, encode out final length:
			mDataField[5] = (Byte) (((encPos +2) & 0xFF00) >> 8);
			mDataField[6] = (Byte) ((encPos +2) & 0xFF);

			// Perform an additional checksum on everything between the internal SOH
			// field, and this point.
			for (int pos = 0; pos < encPos; pos++)
			{
				checksum += mDataField[pos];
			}
			// Write in the checksum and the end-of-message
			mDataField[encPos++] = Convert.ToByte(checksum & 0xFF);
			mDataField[encPos++] = GSP_EOT; 

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}

		private void Add1ByteIntToDataField(ref byte[] mDataField, ref int encPos, int iValue)
		{
			short siValue = Convert.ToInt16(iValue);
			byte[] bConvert = BitConverter.GetBytes(siValue);
			mDataField[encPos++] = bConvert[0];
		}

		private void Add2ByteIntToDataField(ref byte[] mDataField, ref int encPos, int iValue)
		{
			short siValue = Convert.ToInt16(iValue);
			byte[] bConvert = BitConverter.GetBytes(siValue);
			mDataField[encPos++] = bConvert[0];
			mDataField[encPos++] = bConvert[1];
		}

		private void Add4ByteIntToDataField(ref byte[] mDataField, ref int encPos, int iValue)
		{
			byte[] bConvert = BitConverter.GetBytes(iValue);
			mDataField[encPos++] = bConvert[0];
			mDataField[encPos++] = bConvert[1];
			mDataField[encPos++] = bConvert[2];
			mDataField[encPos++] = bConvert[3];
		}

		private void ClearConfigFields()
		{
			cConfigFileNo = 0;
			cVersionMajor = 0;
			cVersionMinor = 0;
		
			cAckTime = 0;
			cMDTType = 0;
			cIOType = 0;
			iResendTime = 0;
			cRetryCount = 0;
			iMaxSendLength = 0;
			cPingRetriesTime = 1;

			iUnitFreqReportSecs = 0;
			iUnitFreqReportMins = 0;
			iUnitFreqReportExcpDist = 0;
			iUnitReportDistance = 0;
			iGPSNotValidReport = 0;
			cLastSecGPSStores = 0;
			cLastMinGPSStores = 0;
			cLastHourGPSStores = 0;
			cLastXMinGPSStores = 0;
			cLastXMinPeriod = 0;

			iSendGPSHistTime = 0;

			cMaxSpeed = 0;
			cMinSpeed = 0;

			iGPSRules = 0;
			iGPSRules2 = 0;
			iGPSRules3 = 0;

			iSpeedRules = 0;
			iSpeedRules2 = 0;
			iSpeedRules3 = 0;

			iSendingRules = 0;
			iSendingRules2 = 0;
			iSendingRules3 = 0;

			iSetPointRules = 0;
			iSetPointRules2 = 0;
			iSetPointRules3 = 0;

			iRoutePointRules = 0;
			iRoutePointRules2 = 0;
			iRoutePointRules3 = 0;

			iStatusRules = 0;
			iStatusRules2 = 0;
			iStatusRules3 = 0;

			iSMSRules = 0;
			iSMSRules2 = 0;
			iSMSRules3 = 0;

			iIgnitionOnRules = 0;
			iIgnitionOffRules = 0;

			iInputRules = 0;
			iInput1Rules = 0;
			iInput2Rules = 0;
			iInput3Rules = 0;
			iInput4Rules = 0;
			iInput5Rules = 0;
			iInput6Rules = 0;
			iInput7Rules = 0;
			iInput8Rules = 0;
			iInput9Rules = 0;
			iInput10Rules = 0;

			iInput1OffRules = 0;
			iInput2OffRules = 0;
			iInput3OffRules = 0;
			iInput4OffRules = 0;
            // Bytes are now being used by the Card Read Configuration
            //iInput5OffRules = 0;
            //iInput6OffRules = 0;
            //iInput7OffRules = 0;
			iInput8OffRules = 0;
			iInput9OffRules = 0;
			iInput10OffRules = 0;

			iOutputRules = 0;
			iOutput1Rules = 0;
			iOutput2Rules = 0;
			iOutput3Rules = 0;
			iOutput4Rules = 0;
			iOutput5Rules = 0;
			iOutput6Rules = 0;
			iOutput7Rules = 0;
			iOutput8Rules = 0;
			iOutput9Rules = 0;
			iOutput10Rules = 0;

			iMaxCoolantTempLimit = 0;
			iMaxOilTempLimit = 0;
			iMinOilPressLow = 0;
			iMinOilPressHigh = 0;
			iMinOilPressLowRPM = 0;
			iMinOilPressHighRPM = 0;
			iMaxRPM = 0;
			fGForceAccelerationLimit = 0F;
			fGForceBrakingLimit = 0F;
			fGForceLRLimit = 0F;
			fGForceAccident = 0F;


			iOutput1Data = 0;
			iOutput2Data = 0;
			iOutput3Data = 0;
			iOutput4Data = 0;
			iOutput5Data = 0;
			iOutput6Data = 0;
			iOutput7Data = 0;
			iOutput8Data = 0;
			iOutput9Data = 0;
			iOutput10Data = 0;

			lSMSNetworkNo = 0;

			lSMSNotifyNo1 = 0;
			lSMSNotifyNo2 = 0;
			lSMSNotifyNo3 = 0;
			lSMSNotifyNo4 = 0;
			lSMSNotifyNo5 = 0;
			lSMSNotifyNo6 = 0;

			lPhoneNumber1 = 0;
			lPhoneNumber2 = 0;
			lPhoneNumber3 = 0;
			lPhoneNumber4 = 0;
			lPhoneNumber5 = 0;
			lPhoneNumber6 = 0;

			iFatigueThresholdMinutes24Hrs = 0;
			iFatigueThresholdMinutesContinuousIgnOn = 0;

            Channels.Clear();
		}

        private int NumberOfChannels
        {
            get
            {
                if (Channels.Count > MAXIMUM_NUMBER_OF_CHANNELS)
                {
                    return MAXIMUM_NUMBER_OF_CHANNELS;
                }
                return Channels.Count;
            }
        }

        public void AddCustomECMAlert(byte ReasonID, ushort SourceType, ushort SPNCode, uint CompareToValue)
        {
            try
            {
                _customECMAlerts.Add(new ConfigFileGPPacketECMAlert(ReasonID, SourceType, SPNCode, CompareToValue));
            }
            catch (Exception)
            {
                
            }
           
        }
	}
}

using System;
using System.IO;
using System.Text;
using System.Collections;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Holds engine-related data in a Status Report
	/// </summary>
    [Serializable]
    public class GPECMAdvancedItem
	{
		#region enums
		enum SPNCodeTypes : byte
		{
			SPN_unknown,
			SPN_alert = 0x01,
			SPN_Max = 0x02,
			SPN_Min = 0x04,
			SPN_Binary = 0x08,
			SPN_Data_1 = 0x10,
			SPN_Data_2 = 0x20,
			SPN_Data_4 = 0x40,
			SPN_Data_AVG = 0x80
		};

        public enum SourceTypes : byte
        {
            Unknown = 0x00,
            J1939 = 0x01,
            J1708 = 0x02,
            iMev = 0x03,
            Prius = 0x04,
            PersonalTracker = 0x05,
            Common = 0x06,
            VehicleData = 0x07,
            Holden_OBD = 0x08,
            Leaf = 0x09,
            TrailerTrack = 0x0A,
            OBDSmart = 0x0B,
            J1979Mode1 = 0x0C,
            J1979Mode9 = 0x0D,
            J1979Colorado = 0x0E,
            J1979Ranger = 0x0D,
            J1979Prado = 0x10,
            J1979Pajero = 0x11,
            J1979Triton = 0x12,
            J1979Hilux = 0x13
        };
		#endregion

		#region Private Variables
		private ushort iSPN_ID = 0;
        private byte bSPNFlags = (byte)0x00;
        private byte[] bSPNData = null;
        private bool bValueCalculated = false;
        private double dDerivedValue = 0;
        private byte bCurrentStateOn = 0x00;
        private short iStateChangeCount = 0;
        private short iStateSecondsActive = 0;
        private byte bValueAvailable = 0x00;
        private byte bSourceType = 0x00;
        #endregion
        #region Constructors
        public GPECMAdvancedItem()
        {
        }
        public GPECMAdvancedItem(Byte[] aData, ref int aStartPos)
        {
            Decode(aData, ref aStartPos);
        }
        #endregion
        #region Public Properties
        public SourceTypes SourceType
        {
            get
            {
                return (SourceTypes) bSourceType;
            }
            set
            {
                bSourceType = (byte) value;
            }
        }

        public ushort SPN_ID
        {
            get
            {
                return iSPN_ID;
            }
            set
            {
                iSPN_ID = value;
            }
        }
        public double DerivedValue
        {
            get
            {
                if (bValueCalculated)
                    return dDerivedValue;
                else
                    return 0;
            }
            set
            {
                dDerivedValue = value;
            }
        }
        public byte Flags
        {
            get
            {
                if (iSPN_ID > 0)
                    return bSPNFlags;
                else
                    return 0x00;
            }
            set
            {
                bSPNFlags = value;
            }
        }
        public byte[] RawData
        {
            get
            {
                if (iSPN_ID > 0)
                    return bSPNData;
                else
                    return null;
            }
            set
            {
                bSPNData = value;
            }
        }
        public double SignedRawValue(int iDataLength)
        {
            double dRawValue = 0;
            byte[] bConvert = new byte[8];
            int iLen = 0;

            if (iDataLength > 8)
                iDataLength = 8;
            iLen = bSPNData.Length;
            if (iLen > iDataLength)
                iLen = iDataLength;

            for (int X = 0; X < iLen; X++)
            {
                // If all of the data bytes are 0xFF, then the value is not available.
                if (bSPNData[X] != (byte) 0xFF)
                    bValueAvailable = 0x01;

                bConvert[X] = bSPNData[X];
                if (X == 7)
                    break;
            }

            if ((bSPNFlags & (byte) SPNCodeTypes.SPN_Binary) == (byte) SPNCodeTypes.SPN_Binary)
            {
                if (iDataLength == 1)
                    dRawValue = Convert.ToDouble((sbyte)bConvert[0]);
                else if (iDataLength == 2)
                    dRawValue = Convert.ToDouble(BitConverter.ToInt16(bConvert, 0));
                else if (iDataLength == 4)
                    dRawValue = Convert.ToDouble(BitConverter.ToInt32(bConvert, 0));
                else if (iDataLength == 8)
                    dRawValue = Convert.ToDouble(BitConverter.ToInt64(bConvert, 0));
            }
            if (bValueAvailable == 0x01)
            {
                if ((bSPNFlags & (byte) SPNCodeTypes.SPN_Data_AVG) == (byte) SPNCodeTypes.SPN_Data_AVG)
                {
                    uint iAvgValue = BitConverter.ToUInt32(bSPNData, 0);
                    uint iTotal = iAvgValue & 0x7FFFFF;
                    uint iCount = (iAvgValue >> 23);

                    if (iCount == 0)
                        dRawValue = 0;
                    else
                    {
                        dRawValue = (Convert.ToDouble(iTotal)/Convert.ToDouble(iCount));
                    }
                }
                else
                {
                    if (iDataLength == 1)
                        dRawValue = Convert.ToDouble((sbyte)bConvert[0]);
                    else if (iDataLength == 2)
                        dRawValue = Convert.ToDouble(BitConverter.ToInt16(bConvert, 0));
                    else if (iDataLength == 4)
                        dRawValue = Convert.ToDouble(BitConverter.ToInt32(bConvert, 0));
                    else if (iDataLength == 8)
                        dRawValue = Convert.ToDouble(BitConverter.ToInt64(bConvert, 0));
                }
            }
            return dRawValue;
        }

	    public double RawValue
        {
            get
            {
                double dRawValue = 0;
                byte[] bConvert = new byte[8];
                for (int X = 0; X < bSPNData.Length; X++)
                {
                    // If all of the data bytes are 0xFF, then the value is not available.
                    if (bSPNData[X] != (byte)0xFF)
                        bValueAvailable = 0x01;

                    bConvert[X] = bSPNData[X];
                    if (X == 7)
                        break;
                }
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Binary) == (byte)SPNCodeTypes.SPN_Binary)
                {
                    dRawValue = Convert.ToDouble(BitConverter.ToUInt64(bConvert, 0));
                }
                if (bValueAvailable == 0x01)
                {
                    
                    if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Data_AVG) == (byte)SPNCodeTypes.SPN_Data_AVG)
                    {
                        uint iAvgValue = BitConverter.ToUInt32(bSPNData, 0);
                        uint iTotal = iAvgValue & 0x7FFFFF;
                        uint iCount = (iAvgValue >> 23);
                        if (iCount == 0)
                            dRawValue = 0;
                        else
                        {
                            dRawValue = (Convert.ToDouble(iTotal) / Convert.ToDouble(iCount));
                        }
                    }
                    else
                    {
                        dRawValue = Convert.ToDouble(BitConverter.ToUInt64(bConvert, 0));
                    }
                }
                return dRawValue;
            }
        }
        public bool ValueCalculated
        {
            get
            {
                return bValueCalculated;
            }
            set
            {
                bValueCalculated = value;
            }
        }
        public bool StateAvailable
        {
            get
            {
                if(bValueAvailable == (byte)0x01)
                    return true;
                else
                    return false;
            }
            set
            {
                if (value)
                    bValueAvailable = (byte)0x01;
                else
                    bValueAvailable = (byte)0x00;
            }
        }
        public byte CurrentState
        {
            get
            {
                return bCurrentStateOn;
            }
            set
            {
                bCurrentStateOn = value;
            }
        }
        public short StateChangeCount
        {
            get
            {
                return iStateChangeCount;
            }
            set
            {
                iStateChangeCount = value;
            }
        }
        public short StateSecondsActive
        {
            get
            {
                return iStateSecondsActive;
            }
            set
            {
                iStateSecondsActive = value;
            }
        }
        #endregion
        #region Public Methods
        public GPECMAdvancedItem CreateCopy()
        {
            GPECMAdvancedItem oRet = new GPECMAdvancedItem();
            oRet.SPN_ID = iSPN_ID;
            oRet.Flags = bSPNFlags;
            oRet.RawData = bSPNData;
            oRet.ValueCalculated = bValueCalculated;
            if (bValueCalculated)
            {
                oRet.DerivedValue = dDerivedValue;
            }
            oRet.bCurrentStateOn = bCurrentStateOn;
            oRet.iStateChangeCount = iStateChangeCount;
            oRet.iStateSecondsActive = iStateSecondsActive;
            oRet.bValueAvailable = bValueAvailable;
            oRet.bSourceType = bSourceType;
            return oRet;
    }
    public string Decode(Byte[] aData, ref int aStartPos)
        {
            int iDataLen = 0;

            bSourceType = aData[aStartPos++];

            iSPN_ID = BitConverter.ToUInt16(aData, aStartPos);
            aStartPos += 2;
            bSPNFlags = aData[aStartPos++];

			SPNCodeTypes TypeLength = (SPNCodeTypes)(bSPNFlags & 0xF8);
			switch (TypeLength)
			{
				case SPNCodeTypes.SPN_Data_1:	iDataLen = 1;	break;
				case SPNCodeTypes.SPN_Data_2:	iDataLen = 2;	break;
				case SPNCodeTypes.SPN_Data_4:	iDataLen = 4;	break;
				case SPNCodeTypes.SPN_Binary:	iDataLen = 3;	break;
				default:						iDataLen = 4;	break;
			}

            bValueAvailable = 0x00;
            bCurrentStateOn = 0x00;
            iStateChangeCount = 0;
            iStateSecondsActive = 0;
            #region Read the byte field
            bSPNData = new byte[iDataLen];
            for (int X = 0; X < iDataLen; X++)
            {
                bSPNData[X] = aData[aStartPos++];
            }
            #endregion
            
			if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Binary) == (byte)SPNCodeTypes.SPN_Binary)
            {
                #region If this is a binary value
                // This is a binary value, so bSPNData should be 3 bytes long
                // The values are encoded as Current State (1 bit), Count (10 bits), Time on in seconds (13 bits)
                // 1 + 10 + 13 bits = 24 bits = 3 bytes
                if(bSPNData.Length == 3)
                {
                    byte[] bConvert = new byte[4];
                    bConvert[0] = bSPNData[0];
                    bConvert[1] = bSPNData[1];
                    bConvert[2] = bSPNData[2];
                    int iValue = BitConverter.ToInt32(bConvert, 0);

                    
                    if((iValue & 0x800000) == 0x800000)
                        bValueAvailable = 0x01;
                    else
                        bValueAvailable = 0x00;
                    // Clear the state bit
                    iValue &= ~0x800000;

                    if((iValue & 0x400000) == 0x400000)
                        bCurrentStateOn = 0x01;
                    else
                        bCurrentStateOn = 0x00;
                    // Clear the state bit
                    iValue &= ~0x400000;
                    iStateChangeCount = Convert.ToInt16(iValue >> 12);
                    iStateSecondsActive = Convert.ToInt16(iValue - ((iValue >> 12) << 12));
                }
                #endregion
            }
            return null;
        }
        public void Encode(MemoryStream oMS)
        {
            PacketUtilities.WriteToStream(oMS, bSourceType);
            PacketUtilities.WriteToStream(oMS, iSPN_ID);
            PacketUtilities.WriteToStream(oMS, bSPNFlags);
            if (bSPNData != null)
                PacketUtilities.WriteToStream(oMS, bSPNData);
        }

        public byte[] Encode()
        {
            byte[] bData = new byte[6 + bSPNData.Length];
            int iPos = 0;
            
            bData[iPos++] = bSourceType;                            // Source Type
            byte[] bTemp = BitConverter.GetBytes(iSPN_ID);          // SPN ID
            for (int X = 0; X < bTemp.Length; X++)
                bData[iPos++] = bTemp[X];
            bData[iPos++] = bSPNFlags;                              // SPN Flags
            if (bSPNData != null)
                for (int X = 0; X < bSPNData.Length; X++)
                    bData[iPos++] = bSPNData[X];                    // SPN Data
            return bData;
        }

        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\n        SPN ID : "); builder.Append(iSPN_ID);
				builder.Append("\r\n        Flags  : ");

                if ((bSPNFlags & (byte) SPNCodeTypes.SPN_alert) == (int) SPNCodeTypes.SPN_alert)
                    builder.Append("Alert ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Max) == (int)SPNCodeTypes.SPN_Max)
                    builder.Append("Max Value ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Min) == (int)SPNCodeTypes.SPN_Min)
                    builder.Append("Min Value ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Binary) == (int)SPNCodeTypes.SPN_Binary)
                    builder.Append("Binary Value ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Data_AVG) == (int)SPNCodeTypes.SPN_Data_AVG)
                    builder.Append("Average Value ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Data_1) == (int)SPNCodeTypes.SPN_Data_1)
                    builder.Append("Data Length 1 Byte ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Data_2) == (int)SPNCodeTypes.SPN_Data_2)
                    builder.Append("Data Length 2 Byte ");
                if ((bSPNFlags & (byte)SPNCodeTypes.SPN_Data_4) == (int)SPNCodeTypes.SPN_Data_4)
                    builder.Append("Data Length 4 Byte ");

                builder.Append("\r\n        Source  : ");
                switch((SourceTypes) bSourceType)
                {
                    case SourceTypes.J1939:
                        builder.Append("J1939");
                        break;
                    case SourceTypes.J1708:
                        builder.Append("J1708");
                        break;
                    case SourceTypes.iMev:
                        builder.Append("iMev");
                        break;
                    case SourceTypes.Leaf:
                        builder.Append("Leaf");
                        break;
                    case SourceTypes.Prius:
                        builder.Append("Prius");
                        break;
                    case SourceTypes.PersonalTracker:
                        builder.Append("Personal Tracker");
                        break;
                    case SourceTypes.Common:
                        builder.Append("Common ECM Values");
                        break;
                    case SourceTypes.Holden_OBD:
                        builder.Append("Holden OBD");
                        break;
                    case SourceTypes.VehicleData:
                        builder.Append("Firmware Generated");
                        break;
                    case SourceTypes.Unknown:
                    default:
                        builder.Append("Unknown");
                        break;
                }

				if (bSPNData != null)
                {
                    builder.Append("\r\n        Data   : "); builder.Append(BitConverter.ToString(bSPNData));
                }
                else
                    builder.Append("\r\n        Data   : null");
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding GPECMAdvancedItem Packet : " + ex.Message);
            }
            return builder.ToString();
        }
        public double CalculateValue(double dNumerator, double dDenominator, double dMin, double dMax, double dDefaultReturnValue, double dOffsetValue, double dNotValidReturnValue)
        {
            byte[] bConvert = new byte[8];
            UInt64 iValue = 0;
            dDerivedValue = dDefaultReturnValue;
            bValueAvailable = 0x00;

            for (int X = 0; X < bSPNData.Length; X++)
            {
                // If all of the data bytes are 0xFF, then the value is not available.
                if (bSPNData[X] != (byte)0xFF)
                    bValueAvailable = 0x01;

                bConvert[X] = bSPNData[X];
                if (X == 7)
                    break;
            }

            if (bValueAvailable == 0x01)
            {
                if ((bSPNFlags & (byte) SPNCodeTypes.SPN_Data_AVG) == (byte) SPNCodeTypes.SPN_Data_AVG)
                {
                    uint iAvgValue = BitConverter.ToUInt32(bSPNData, 0);
                    uint iTotal = iAvgValue & 0x7FFFFF;
                    uint iCount = (iAvgValue >> 23);
                    if (iCount == 0)
                        dDerivedValue = dNotValidReturnValue;
                    else
                    {
                        if (dNumerator > 0 && dDenominator > 0)
                            dDerivedValue = (Convert.ToDouble(iTotal)/Convert.ToDouble(iCount))*(dNumerator/dDenominator);
                        else
                            dDerivedValue = (Convert.ToDouble(iTotal)/Convert.ToDouble(iCount));

                        dDerivedValue = dDerivedValue + dOffsetValue;
                    }
                }
                else
                {
                    iValue = BitConverter.ToUInt64(bConvert, 0);
                    if (dDenominator > 0)
                        dDerivedValue = Convert.ToDouble(iValue)*(dNumerator/dDenominator);
                    else
                        dDerivedValue = Convert.ToDouble(iValue);

                    dDerivedValue = dDerivedValue + dOffsetValue;

                    if (dDerivedValue > dMax)
                        dDerivedValue = dMax;
                    if (dDerivedValue < dMin)
                        dDerivedValue = dMin;
                }
            }
            else
                dDerivedValue = dNotValidReturnValue;


            bValueCalculated = true;
            return dDerivedValue;
        }
        #endregion
    }
}

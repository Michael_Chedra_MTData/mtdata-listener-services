using System;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class FirmwareDriverLoginReply : GatewayProtocolPacket
    {
        public const byte FIRMWARE_LOGIN_REPLY = 0xE7;
        public enum FirmwareLoginResult
        {
            LoginSuccess = 0,
            CredentailFail = 1,
            DriverInactive = 2,
            CardInactive = 3,
            PinIncorrect = 4,
            LoggedIntoAnotherVehicle = 5
        }
        #region Private Member Vars
        private byte _PacketVer;
        private FirmwareLoginResult _loginResult;
        private int _driverId;
        private int _driverPin;
        private byte[] _cardId;
        private string _driverName;
        private string _serverTime_DateFormat;
        #endregion
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public FirmwareLoginResult LoginResult
        {
            get { return _loginResult; }
            set { _loginResult = value; }
        }
        public int DriverID
        {
            get
            {
                return _driverId;
            }
            set
            {
                _driverId = value;
            }
        }
        public int DriverPin
        {
            get
            {
                return _driverPin;
            }
            set
            {
                _driverPin = value;
            }
        }
        public byte[] CardID
        {
            get
            {
                return _cardId;
            }
            set
            {
                _cardId = value;
            }
        }
        public ulong CardIDasLong
        {
            get
            {
                byte[] data = new byte[8];
                int pos = 0;
                foreach (byte b in _cardId)
                {
                    data[pos++] = b;
                }
                return BitConverter.ToUInt64(data, 0);
            }
            set
            {
                byte[] data = BitConverter.GetBytes(value);
                int maxByte = -1;
                for (int X = data.Length - 1; X >= 0; X++)
                {
                    if (data[X] != (byte)0x00)
                    {
                        maxByte = X;
                        break;
                    }
                }
                if (maxByte > 0)
                {
                    _cardId = new byte[maxByte];
                    for (int X = 0; X < maxByte; X++)
                    {
                        _cardId[X] = data[X];
                    }
                }
            }
        }
        public string DriverName
        {
            get { return _driverName; }
            set { _driverName = value; }
        }
        #endregion
        #region Constructor and CreateCopy functions
        public FirmwareDriverLoginReply(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = FIRMWARE_LOGIN_REPLY;
            _PacketVer = 0;
            _driverId = 0;
            _driverPin = 0;
            _loginResult = FirmwareLoginResult.CredentailFail;
            _cardId = null;
            _driverName = "";
        }
        public FirmwareDriverLoginReply(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            Decode(p.mDataField);
        }
        public new FirmwareDriverLoginReply CreateCopy()
        {
            FirmwareDriverLoginReply oData = new FirmwareDriverLoginReply(_serverTime_DateFormat);
            oData.PacketVer = _PacketVer;
            oData.LoginResult = LoginResult;
            oData.DriverID = DriverID;
            oData.DriverPin = DriverPin;
            oData.CardID = CardID;
            oData.DriverName = DriverName;
            return oData;
        }
        #endregion
        #region Encoding / Decoding
        public string Decode(Byte[] aData)
        {
            MemoryStream oMS = new MemoryStream(aData, 0, aData.Length);
            return Decode(ref oMS);
        }
        public string Decode(Byte[] aData, ref int index)
        {
            MemoryStream oMS = new MemoryStream(aData, index, aData.Length - index);
            string sRet = Decode(ref oMS);
            if (sRet != null) return sRet;
            index += (int)oMS.Position;
            return null;
        }
        public string Decode(ref MemoryStream oMS)
        {
            // [PacketVer][LoginResult][DriverID][Len of CardID][CardID][Len of Name][Name]
            try
            {
                byte bData = (byte)0x00;
               PacketUtilities.ReadFromStream(oMS, ref _PacketVer);
               PacketUtilities.ReadFromStream(oMS, ref bData);
                _loginResult = (FirmwareLoginResult) Convert.ToInt32(bData);
               PacketUtilities.ReadFromStream(oMS, ref _driverId);
               PacketUtilities.ReadFromStream(oMS, ref bData);
                int length = (int) bData;
                if (length > 8)
                    length = 8;
                if (length > 0)
                {
                    byte[] data = new byte[length];
                   PacketUtilities.ReadFromStream(oMS, length, ref data);
                }
               PacketUtilities.ReadFromStream(oMS, ref bData);
                length = (int)bData;
                if (length > 16)
                    length = 16;
                if (length > 0)
                {
                   PacketUtilities.ReadFromStream(oMS, length, ref _driverName);
                }
               PacketUtilities.ReadFromStream(oMS, ref _driverPin);
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }

        public void Encode()
        {
            EncodeDataField();
        }

        public void EncodeDataField()
        {
            // [PacketVer][LoginResult][DriverID][Len of CardID][CardID][Len of Name][Name]
            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _PacketVer);
            byte bData = (byte)_loginResult;
           PacketUtilities.WriteToStream(oMS, bData);
           PacketUtilities.WriteToStream(oMS, _driverId);
            if (CardID != null)
            {
                bData = (byte)CardID.Length;
               PacketUtilities.WriteToStream(oMS, bData);
               PacketUtilities.WriteToStream(oMS, CardID);
            }
            else
            {
                bData = (byte)0x00;
               PacketUtilities.WriteToStream(oMS, bData);
            }
            if (DriverName.Length > 0)
            {
                bData = (byte)DriverName.Length;
               PacketUtilities.WriteToStream(oMS, bData);
               PacketUtilities.WriteToStream(oMS, DriverName);
            }
            else
            {
                bData = (byte)0x00;
               PacketUtilities.WriteToStream(oMS, bData);
            }
           PacketUtilities.WriteToStream(oMS, _driverPin);
            mDataField = oMS.ToArray();
        }
        #endregion
        #region ToString functions
        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nFirmware Login Reply Packet :");
                builder.Append("\r\n    Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n    Login Result : "); builder.Append(Enum.GetName(typeof(FirmwareLoginResult), _loginResult));
                builder.Append("\r\n    Driver ID : "); builder.Append(_driverId);
                builder.Append("\r\n    Driver Pin : "); builder.Append(_driverPin);
                builder.Append("\r\n    Card ID : "); builder.Append(_cardId);
                builder.Append("\r\n    DriverName : "); builder.Append(_driverName);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nFirmware Login Reply Packet : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

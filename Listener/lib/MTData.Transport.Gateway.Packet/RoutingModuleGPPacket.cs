using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// this is the inbound packet for the new routing module.
	/// </summary>
    [Serializable]
    public class RoutingModuleGPPacket : GatewayProtocolPacket, IStatusHolder, IPositionHolder
	{
		public const byte ROUTES_PACKET_MASK			= 0xB0;
		public const byte ROUTES_ARRIVE_EARLY			= 0xb1;
		public const byte ROUTES_ARRIVE_ONTIME			= 0xb2;
		public const byte ROUTES_ARRIVE_LATE			= 0xb3;
		public const byte ROUTES_DEPART_EARLY			= 0xb4;
		public const byte ROUTES_DEPART_ONTIME			= 0xb5;
		public const byte ROUTES_DEPART_LATE			= 0xb6;
		public const byte ROUTES_DURATION_OVER			= 0xb7;
		public const byte ROUTES_DURATION_UNDER			= 0xb8;
		public const byte ROUTES_LATE					= 0xb9;
		public const byte ROUTES_UPDATE_ACCEPTED		= 0xba;		//	POD : Accepted the route update
		public const byte ROUTES_STATUS_CANCELLED		= 0xbb;		//	POD : Inform Listener of Route Activation/DeActivation/Completion
		public const byte ROUTES_STATUS_ACTIVATED		= 0xbc;		//	POD : Inform Listener of Route Activation/DeActivation/Completion
		public const byte ROUTES_STATUS_COMPLETED		= 0xbd;		//	POD : Inform Listener of Route Activation/DeActivation/Completion
		public const byte ROUTES_DEPART_MISSED			= 0xbe;

		public const byte ROUTES_OFF_ROUTE				= 0xbf;
		public const byte ROUTES_OFF_ROUTE_END			= 0xc0;

		private uint ROUTES_LENGTH 
        { 
            get
            {
                return 5 +	// The info bytes
					(2 * GPClock.Length) +
					GPPositionFix.Length +
					(uint)mStatus.Length +
					GPDistance.Length;
            }
        }

		private uint ROUTES_ENGINE_LENGTH { get { return ROUTES_LENGTH + GPEngineData.Length; } }

        private uint ROUTES_ENGINE_EXT_LENGTH { get { return ROUTES_ENGINE_LENGTH + GPExtendedValues.Length; } }

		// Additional members above the base packet
		public string sConstructorDecodeError = null;

		public uint RouteNumber;
		public byte CheckPointIndex;

		public GPClock mCurrentClock;
		public GPClock mFixClock;
		public GPPositionFix mFix;
		public GPStatus mStatus;
		public GPDistance mDistance;
		public GPEngineData mEngineData;
		public GPExtendedValues mExtendedValues;
        public GPEngineSummaryData mEngineSummaryData;
		public GPTransportExtraDetails mTransportExtraDetails;
		public GPTrailerTrack mTrailerTrack;
		public GBVariablePacketExtended59 mExtendedVariableDetails;
        public GPECMAdvanced mECMAdvanced;	    

		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";

		public GPStatus GetStatus()
		{
			return mStatus;
		}

		public GPPositionFix GetPosition()
		{
			return mFix;
		}

		public GPClock GetFixClock()
		{
			return mFixClock;
		}

		public new RoutingModuleGPPacket CreateCopy()
		{
			RoutingModuleGPPacket oGP = new RoutingModuleGPPacket(_serverTime_DateFormat);
			oGP.RouteNumber = this.RouteNumber;
			oGP.CheckPointIndex = this.CheckPointIndex;
			oGP.bAckImmediately = this.bAckImmediately;
			oGP.bAckRegardless = this.bAckRegardless;
			oGP.bArchivalData = this.bArchivalData;
			oGP.bIsFileType = this.bIsFileType;
			oGP.bIsPopulated = this.bIsPopulated;
			oGP.bPacketFormat = this.bPacketFormat;
			oGP.bRevertToPrimaryServer = this.bRevertToPrimaryServer;
			oGP.bSendFlash = this.bSendFlash;
			oGP.bUsingSecondaryServer = this.bUsingSecondaryServer;
			oGP.cAckSequence = this.cAckSequence;
			oGP.cFleetId = this.cFleetId;
			oGP.cMsgType = this.cMsgType;
			oGP.cOurSequence = this.cOurSequence;
			oGP.cPacketNum = this.cPacketNum;
			oGP.cPacketTotal= this.cPacketTotal;
			oGP.iChecksum = this.iChecksum;
			oGP.iLength = this.iLength;
			oGP.iTotalLength = this.iTotalLength;
			oGP.iVehicleId = this.iVehicleId;

			if(this.mCurrentClock != null) {oGP.mCurrentClock = this.mCurrentClock.CreateCopy();} 
			else {oGP.mCurrentClock = new GPClock("", _serverTime_DateFormat);}
			oGP.mDataField = this.mDataField;
			if(this.mDistance != null) {oGP.mDistance = this.mDistance.CreateCopy();}
			else {oGP.mDistance = new GPDistance();}
			if(this.mEngineData != null) {oGP.mEngineData = this.mEngineData.CreateCopy();}
			else {oGP.mEngineData = new GPEngineData();}
            if (this.mEngineSummaryData != null) { oGP.mEngineSummaryData = this.mEngineSummaryData.CreateCopy(); }
            else { oGP.mEngineSummaryData = new GPEngineSummaryData(); }
			if(this.mExtendedValues != null) {oGP.mExtendedValues= this.mExtendedValues.CreateCopy();}
			else {oGP.mExtendedValues = new GPExtendedValues();}
			if(this.mFix != null) {oGP.mFix= this.mFix.CreateCopy();}
			else {oGP.mFix = new GPPositionFix();}
			if(this.mFixClock != null) {oGP.mFixClock= this.mFixClock.CreateCopy();}
			else {oGP.mFixClock = new GPClock("", _serverTime_DateFormat);}
			oGP.mRawBytes = this.mRawBytes;
			if(this.mSenderIP != null) {oGP.mSenderIP = new System.Net.IPEndPoint(this.mSenderIP.Address, this.mSenderIP.Port);}
			else {oGP.mSenderIP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse("0.0.0.0"), 0);}
			if(this.mStatus != null) {oGP.mStatus= this.mStatus.CreateCopy();}
			else {oGP.mStatus = new GPStatus();}
			if(this.mTrailerTrack != null) {oGP.mTrailerTrack= this.mTrailerTrack.CreateCopy();}
			else {oGP.mTrailerTrack = new GPTrailerTrack();}

			if(this.mTransportExtraDetails != null) {oGP.mTransportExtraDetails= this.mTransportExtraDetails.CreateCopy();}
			else{oGP.mTransportExtraDetails = new GPTransportExtraDetails();}

			if(this.mExtendedVariableDetails != null) {oGP.mExtendedVariableDetails= this.mExtendedVariableDetails.CreateCopy();}
			else {oGP.mExtendedVariableDetails = new GBVariablePacketExtended59();}

            if (this.mECMAdvanced != null) { oGP.mECMAdvanced = this.mECMAdvanced.CreateCopy(); }
            else {oGP.mECMAdvanced = new GPECMAdvanced(); }

			oGP.sConstructorDecodeError= this.sConstructorDecodeError;

		    if (this.RoadTypeData != null)
		    {
		        oGP.RoadTypeData = this.RoadTypeData.CreateCopy();
		    }

			return oGP;
		}

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\nRouteNumber : "); builder.Append(RouteNumber);
			builder.Append("\r\nCheckPointIndex : "); builder.Append(CheckPointIndex);
			builder.Append("\r\nmCurrentClock : "); builder.Append(mCurrentClock);
			builder.Append("\r\nmFixClock : "); builder.Append(mFixClock);
			builder.Append("\r\nmFix : "); builder.Append(mFix);
			builder.Append("\r\nmStatus : "); builder.Append(mStatus);
			builder.Append("\r\nmDistance : "); builder.Append(mDistance);
			builder.Append("\r\nmEngineData : "); builder.Append(mEngineData);
			builder.Append("\r\nmExtendedValues : "); builder.Append(mExtendedValues);
            if (mEngineSummaryData != null)
            {
                builder.Append(mEngineSummaryData.ToString());
            }
            if (mECMAdvanced != null)
            {
                builder.Append(mECMAdvanced.ToString());
            }
			return builder.ToString();
		}

		public RoutingModuleGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mCurrentClock = new GPClock("Device", _serverTime_DateFormat); 
			mFixClock = new GPClock("GPS", _serverTime_DateFormat); 
			mFix = new GPPositionFix(); 
			mStatus = new GPStatus(); 
			mDistance = new GPDistance();
			mEngineData = new GPEngineData();
			mExtendedValues = new GPExtendedValues();
			mTransportExtraDetails = new GPTransportExtraDetails();
			mTrailerTrack = new GPTrailerTrack();
			mExtendedVariableDetails = new GBVariablePacketExtended59();
            mEngineSummaryData = new GPEngineSummaryData();
            mECMAdvanced = new GPECMAdvanced();
			bIsPopulated = false;
			sConstructorDecodeError = null;            
		}

		public RoutingModuleGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mCurrentClock = new GPClock("Device", _serverTime_DateFormat); 
			mFixClock = new GPClock("GPS", _serverTime_DateFormat); 
			mFix = new GPPositionFix(); 
			mStatus = new GPStatus(); 
			mDistance = new GPDistance();
			mEngineData = new GPEngineData();
			mExtendedValues = new GPExtendedValues();
			mTransportExtraDetails = new GPTransportExtraDetails();
			mTrailerTrack = new GPTrailerTrack();
			mExtendedVariableDetails = new GBVariablePacketExtended59();
            mEngineSummaryData = new GPEngineSummaryData();
            mECMAdvanced = new GPECMAdvanced();
			sConstructorDecodeError = Decode(p.mDataField);
			bIsPopulated = (sConstructorDecodeError == null);            
		}

		public string Decode (Byte[] aData)
		{
			int pos = 0;
			string retMsg = null;
			if (!bIsPopulated) retMsg = base.Decode(aData, 0);
			if (retMsg != null) return retMsg;

			// Work through the packet
			RouteNumber = aData[pos++];
			RouteNumber += ((uint)aData[pos++]) << 8;
			RouteNumber += ((uint)aData[pos++]) << 16;
			RouteNumber += ((uint)aData[pos++]) << 24;

			CheckPointIndex = aData[ pos++ ];

			if ((retMsg = mCurrentClock.Decode(aData, pos))!= null) return retMsg;
			pos += GPClock.Length;

			if ((retMsg = mFixClock.Decode(aData, pos))!= null) return retMsg;
			pos += GPClock.Length;
		
			if ((retMsg = mFix.Decode(aData, pos))!= null) return retMsg;
			pos += GPPositionFix.Length;

			if ((retMsg = mStatus.Decode(aData,pos))!= null) return retMsg;
			pos += mStatus.Length;
		
			if ((retMsg = mDistance.Decode(aData,pos))!= null) return retMsg;
			pos += GPDistance.Length;

			mExtendedValues.iValue1 = 0;
			mExtendedValues.iValue2 = 0;
			mExtendedValues.iValue3 = 0;
			mExtendedValues.iValue4 = 0;

            //cPacketTotal  cPacketNum  Mobile Structure	Transport Standard	Extra Transport	Trailer Track	ECM Advanced	Extended 5.9 States
            // 0x01           0x00	            X					
            // 0x01           0x01	            X	                    X				
            // 0x01           0x03	            X	                    X	                X			
            // 0x01           0x07	            X	                    X	                X	        X		
            // 0x01           0x04	            X                   		                	        X		
            // 0x01           0x05	            X	                    X	                	        X		
            // 0x01           0x08	            X                   		                	        	            	                X
            // 0x01           0x09	            X	                    X	                	        	            	                X
            // 0x01           0x0B	            X	                    X	                X	        	            	                X
            // 0x01           0x0F	            X	                    X	                X	        X	            	                X
            // 0x01           0x0C	            X	                    	                	        X	            	                X
            // 0x01           0x0D	            X	                    X	                	        X	            	                X
            // 0x03           0x08	            X	                    X	                X	        	            X	                X
            // 0x03           0x0C	            X	                    X	                X	        X	            X	                X

			bool bEngineData = false;
			bool bExtraDetails = false;
			bool bTrailerTrack = false;
			bool bExtendedVariableDetails = false;
            bool bECMAdvanced = false;
		    bool hasRouteTypeData = false;

			if (this.cPacketTotal == (byte)0x01)
			{
				bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
				bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
				bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
				bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                bECMAdvanced = false;
            }
            else if (this.cPacketTotal == (byte)0x03)
            {
                bEngineData = false;
                bExtraDetails = false;
                bTrailerTrack = ((this.cPacketNum & (byte)0x0C) == (byte)0x0C);
                bExtendedVariableDetails = true;
                bECMAdvanced = true;
            }
            else if (this.cPacketTotal == (byte)0x04 || this.cPacketTotal == (byte)0x05 || this.cPacketTotal == (byte)0x06 || cPacketTotal == 0x07)
            {
                bEngineData = ((this.cPacketNum & (byte)0x01) == (byte)0x01);
                bExtraDetails = ((this.cPacketNum & (byte)0x02) == (byte)0x02);
                bTrailerTrack = ((this.cPacketNum & (byte)0x04) == (byte)0x04);
                bExtendedVariableDetails = ((this.cPacketNum & (byte)0x08) == (byte)0x08);
                bECMAdvanced = true;
				
		        if (cPacketTotal == 0x07)
		        {
		            hasRouteTypeData = true;
		        }				
            }

            if (this.cPacketTotal == (byte)0x06)
            {
                //need to add 31 bytes of additional advanced ECM data that is not used at present
                //odo, fuel, total idle time, total fuel used in idle, total PTO time, total fuel in PTO
                pos += 31;
            }

            if ((pos + 1) < aData.Length)
            {
                if (bEngineData)
                {
                    #region Decode Engine Summary Data and Extended Values
                    if ((retMsg = mEngineData.Decode(aData, pos)) != null) return retMsg;
                    pos += GPEngineData.Length;
                    mExtendedValues.iValue1 = 0;
                    mExtendedValues.iValue2 = 0;
                    mExtendedValues.iValue3 = 0;
                    mExtendedValues.iValue4 = 0;
                    if ((pos + 2) < aData.Length)
                    {
                        mExtendedValues.iValue1 = (aData[pos++]);
                        mExtendedValues.iValue1 += (aData[pos++] << 8);
                        if (mExtendedValues.iValue1 > 32768)
                            mExtendedValues.iValue1 = mExtendedValues.iValue1 - 65535;
                    }
                    if ((pos + 2) < aData.Length)
                    {
                        mExtendedValues.iValue2 = (aData[pos++]);
                        mExtendedValues.iValue2 += (aData[pos++] << 8);
                        if (mExtendedValues.iValue2 > 32768)
                            mExtendedValues.iValue2 = mExtendedValues.iValue2 - 65535;
                    }
                    if ((pos + 2) < aData.Length)
                    {
                        mExtendedValues.iValue3 = (aData[pos++]);
                        mExtendedValues.iValue3 += (aData[pos++] << 8);
                        if (mExtendedValues.iValue3 > 32768)
                            mExtendedValues.iValue3 = mExtendedValues.iValue3 - 65535;
                    }
                    if ((pos + 2) < aData.Length)
                    {
                        mExtendedValues.iValue4 = (aData[pos++]);
                        mExtendedValues.iValue4 += (aData[pos++] << 8);
                        if (mExtendedValues.iValue4 > 32768)
                            mExtendedValues.iValue4 = mExtendedValues.iValue4 - 65535;
                    }
                    #endregion
                }
                if (bExtraDetails)
                {
                    #region Decode the Transport Extra Details
                    if ((retMsg = mTransportExtraDetails.Decode(aData, pos)) != null) return retMsg;
                    if (mEngineSummaryData != null)
                    {
                        if (Convert.ToInt32(mTransportExtraDetails.iOdometer) > mEngineSummaryData.iOdometer)
                            mEngineSummaryData.iOdometer = Convert.ToInt32(mTransportExtraDetails.iOdometer);
                        if (mTransportExtraDetails.iTotalFuelUsed > mEngineSummaryData.iTotalFuel)
                            mEngineSummaryData.iTotalFuel = mTransportExtraDetails.iTotalFuelUsed;
                        if (mTransportExtraDetails.iBatteryVolts > mEngineSummaryData.iBatteryVoltage)
                            mEngineSummaryData.iBatteryVoltage = mTransportExtraDetails.iBatteryVolts;
                        if (mTransportExtraDetails.fFuelEconomy > mEngineSummaryData.fFuelEconomy)
                            mEngineSummaryData.fFuelEconomy = mTransportExtraDetails.fFuelEconomy;
                    }
                    pos += GPTransportExtraDetails.Length;
                    #endregion
                }
                if (bECMAdvanced)
                {
                    if ((retMsg = mECMAdvanced.Decode(aData, ref pos)) != null) return retMsg;
                    if (mECMAdvanced.SpeedAccuracyValuesSet)
                    {
                        mFix.cSpeed = mECMAdvanced.CreateSpeedSPNItem(mFix.cSpeed, false);
                        mDistance.cMaxSpeed = mECMAdvanced.CreateSpeedSPNItem(mDistance.cMaxSpeed, true);
                    }
                }
                if (bTrailerTrack)
				{
					#region Decode the Trailer Track data
					if ((retMsg = mTrailerTrack.Decode(aData, pos)) != null) return retMsg;
					pos += mTrailerTrack.Length;
					#endregion
				} 
				if (bExtendedVariableDetails)
                {
                    if ((retMsg = mExtendedVariableDetails.Decode(aData, ref pos, 0x00, 0x00)) != null) return retMsg;
                }

                if (hasRouteTypeData)
                {
                    if (RoadTypeData == null)
                    {
                        RoadTypeData = new RoadTypeData();
                    }

                    retMsg = RoadTypeData.Decode(aData, ref pos);
                    if (retMsg != null)
                    {
                        return retMsg;
                    }
                }
            }
			return null;
		}

		public new void Encode(ref byte[] theData)
		{	
			if (mEngineData == null)
			{
				iLength = ROUTES_LENGTH;
				mDataField = new byte[ROUTES_LENGTH];
			}
			else
			{
				if (mExtendedValues == null)
				{
					iLength = ROUTES_ENGINE_LENGTH;
					mDataField = new byte[ROUTES_ENGINE_LENGTH];
				}
				else
				{
					iLength = ROUTES_ENGINE_EXT_LENGTH;
					mDataField = new byte[ROUTES_ENGINE_EXT_LENGTH];
				}
			}
		
			int pos = 0;

			mDataField[pos++] = Convert.ToByte(RouteNumber & 0xFF);
			mDataField[pos++] = Convert.ToByte((RouteNumber >> 8) & 0xFF);
			mDataField[pos++] = Convert.ToByte((RouteNumber >> 16) & 0xFF);
			mDataField[pos++] = Convert.ToByte((RouteNumber >> 24) & 0xFF);

			mDataField[pos++] = CheckPointIndex;

			// Work through the packet
			mCurrentClock.Encode(ref mDataField, pos);
			pos += GPClock.Length;

			mFixClock.Encode(ref mDataField, pos);
			pos += GPClock.Length;
		
			mFix.Encode(ref mDataField, pos);
			pos += GPPositionFix.Length;

			mStatus.Encode(ref mDataField,pos);
			pos += mStatus.Length;
		
			mDistance.Encode(ref mDataField,pos);	
			pos += GPDistance.Length;

			// Insert the engine info if it's there:
			if (mEngineData != null)
			{
				mEngineData.Encode(ref mDataField, pos);
				pos += GPEngineData.Length;
			}

			// Insert the extended info if it's there:
			if (mExtendedValues != null)
			{
				mExtendedValues.Encode(ref mDataField, pos);
				pos += GPExtendedValues.Length;
			}

			// and encode this down:
			base.Encode(ref theData);
		}

	}
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class DriverPointsConfigError : GatewayProtocolPacket
    {
        public const byte DRIVER_POINTS_CONFIG_ERROR = 0xfc;

        public enum ErrorCode
        {
            UnknownError = 0,
            TooManyTrackingRules,
            TooManyIoRules,
            TooManyEcmRules,
            TooManyMdtRules,
            TooManyConfigRequestsInHour,
        }

        //[Error]
        private ErrorCode _error;
        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        private string _constructorDecodeError;
        private string _serverTime_DateFormat;

        public ErrorCode Error
        {
            get { return _error; }
            set { _error = value; }
        }
        public string ConstructorDecodeError { get { return _constructorDecodeError; } }

        public DriverPointsConfigError(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }

        public DriverPointsConfigError(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);

            _constructorDecodeError = DecodePayload();
            bIsPopulated = (ConstructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = DRIVER_POINTS_CONFIG_ERROR;
            _error = ErrorCode.UnknownError;
            _constructorDecodeError = null;
        }

        public new DriverPointsConfigError CreateCopy()
        {
            DriverPointsConfigError data = new DriverPointsConfigError(this, _serverTime_DateFormat);
            data.Error = _error;
            return data;
        }
        
        public new void Encode(ref byte[] theData)
        {
            MemoryStream oMS = new MemoryStream();
            byte b = (byte)_error;
            PacketUtilities.WriteToStream(oMS, b);
            mDataField = oMS.ToArray();
            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
        }

        public string Decode(Byte[] data)
        {
            string problem = null;
            if (!bIsPopulated) problem = base.Decode(data, 0);
            if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

            return problem;
        }
        public string DecodePayload()
        {
            try
            {
                MemoryStream oMS = new MemoryStream(mDataField, 0, mDataField.Length);
                byte b = 0x00;
               PacketUtilities.ReadFromStream(oMS, ref b);
                _error = (ErrorCode)b;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }


        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nDriver Points Config Error :");
                builder.Append("\r\n    Error : "); builder.Append(_error.ToString());
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Driver Points Config File : " + ex.Message);
            }
            return builder.ToString();
        }
}
}

using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class ConfigDriverPointsECMRules
    {
        #region Private Member Vars
        private bool bIsPopulated;
        private byte _PacketVer;
        private int _RuleID;
        private byte _Points;
        private short _Output1;
        private short _Output2;
        private ushort _sourceType;
        private GPECMAdvanced.ECMValueType _ecmValyeType;
        private ushort _PGN;
        private ushort _SPN;
        private byte _ByteLocation;
        private byte _BitLocation;
        private uint _EcmValue;
        private byte _Condition;
        private uint _Duration;
        private uint _ResetDuration;
        #endregion
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public int RuleID
        {
            get { return _RuleID; }
            set { _RuleID = value; }
        }
        public byte Points
        {
            get { return _Points; }
            set { _Points = value; }
        }
        public short Output1
        {
            get { return _Output1; }
            set { _Output1 = value; }
        }
        public short Output2
        {
            get { return _Output2; }
            set { _Output2 = value; }
        }
        public ushort SourceType
        {
            get { return _sourceType; }
            set { _sourceType = value; }
        }
        public GPECMAdvanced.ECMValueType EcmValyeType
        {
            get { return _ecmValyeType; }
            set { _ecmValyeType = value; }
        }
        public ushort PGN
        {
            get { return _PGN; }
            set { _PGN = value; }
        }
        public ushort SPN
        {
            get { return _SPN; }
            set { _SPN = value; }
        }
        public byte BitLocation
        {
            get { return _BitLocation; }
            set { _BitLocation = value; }
        }
        public byte ByteLocation
        {
            get { return _ByteLocation; }
            set { _ByteLocation = value; }
        }
        public uint EcmValue
        {
            get { return _EcmValue; }
            set { _EcmValue = value; }
        }
        public bool IsPopulated
        {
            get { return bIsPopulated; }
            set { bIsPopulated = value; }
        }
        public byte Condition
        {
            get { return _Condition; }
            set { _Condition = value; }
        }
        public uint Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }
        public uint ResetDuration
        {
            get { return _ResetDuration; }
            set { _ResetDuration = value; }
        }
        
        
        #endregion
        #region Constructor and CreateCopy functions
        public ConfigDriverPointsECMRules()
        {
            _PacketVer = 0;
            bIsPopulated = false;
            _RuleID = 0;
            _Points = 0;
            _Output1 = -2;
            _Output2 = -2;
            _sourceType = 0;
            _ecmValyeType = GPECMAdvanced.ECMValueType.DataLength1;
            _PGN = 0;
            _SPN = 0;
            _BitLocation = 0;
            _ByteLocation = 0;
            _EcmValue = 0;
            _Condition = 0;
            _Duration = 0;
            _ResetDuration = 0;
        }
        public ConfigDriverPointsECMRules CreateCopy()
        {
            ConfigDriverPointsECMRules oData = new ConfigDriverPointsECMRules();
            oData.PacketVer = _PacketVer;
            oData.RuleID = _RuleID;
            oData.Points = _Points;
            oData.Output1 = _Output1;
            oData.Output2 = _Output2;
            oData.SourceType = _sourceType;
            oData.EcmValyeType = _ecmValyeType;
            oData.PGN = _PGN;
            oData.SPN = _SPN;
            oData.BitLocation = _BitLocation;
            oData.ByteLocation = _ByteLocation;
            oData.IsPopulated = bIsPopulated;
            oData.EcmValue = _EcmValue;
            oData.Condition = _Condition;
            oData.Duration = _Duration;
            oData.ResetDuration = _ResetDuration;
            return oData;
        }
        #endregion
        #region Encoding / Decoding
        public string Decode(Byte[] aData, byte bPacketVer)
        {
            MemoryStream oMS = new MemoryStream(aData, 0, aData.Length);
            return Decode(ref oMS, bPacketVer);
        }
        public string Decode(Byte[] aData, byte bPacketVer, ref int index)
        {
            MemoryStream oMS = new MemoryStream(aData, index, aData.Length - index);
            string sRet = Decode(ref oMS, bPacketVer);
            if (sRet != null) return sRet;
            index += (int)oMS.Position;
            return null;
        }
        public string Decode(ref MemoryStream oMS, byte bPacketVer)
        {
            try
            {
                _PacketVer = bPacketVer;
               PacketUtilities.ReadFromStream(oMS, ref _RuleID);
               PacketUtilities.ReadFromStream(oMS, ref _Points);
               PacketUtilities.ReadFromStream(oMS, ref _Output1);
               PacketUtilities.ReadFromStream(oMS, ref _Output2);
                if (_PacketVer > 1)
                {
                   PacketUtilities.ReadFromStream(oMS, ref _sourceType);
                    byte b = 0x00;
                   PacketUtilities.ReadFromStream(oMS, ref b);
                    _ecmValyeType = (GPECMAdvanced.ECMValueType)b;
                }
               PacketUtilities.ReadFromStream(oMS, ref _PGN);
               PacketUtilities.ReadFromStream(oMS, ref _SPN);
               PacketUtilities.ReadFromStream(oMS, ref _ByteLocation);
               PacketUtilities.ReadFromStream(oMS, ref _BitLocation);
               PacketUtilities.ReadFromStream(oMS, ref _EcmValue);
               PacketUtilities.ReadFromStream(oMS, ref _Condition);
               PacketUtilities.ReadFromStream(oMS, ref _Duration);
               PacketUtilities.ReadFromStream(oMS, ref _ResetDuration);
                bIsPopulated = true;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }
        public void Encode(ref MemoryStream oMS)
        {
           PacketUtilities.WriteToStream(oMS, _RuleID);
           PacketUtilities.WriteToStream(oMS, _Points);
           PacketUtilities.WriteToStream(oMS, _Output1);
           PacketUtilities.WriteToStream(oMS, _Output2);
            if (_PacketVer > 1)
            {
               PacketUtilities.WriteToStream(oMS, _sourceType);
               PacketUtilities.WriteToStream(oMS, (byte)_ecmValyeType);
            }
           PacketUtilities.WriteToStream(oMS, _PGN);
           PacketUtilities.WriteToStream(oMS, _SPN);
           PacketUtilities.WriteToStream(oMS, _ByteLocation);
           PacketUtilities.WriteToStream(oMS, _BitLocation);
           PacketUtilities.WriteToStream(oMS, _EcmValue);
           PacketUtilities.WriteToStream(oMS, _Condition);
           PacketUtilities.WriteToStream(oMS, _Duration);
           PacketUtilities.WriteToStream(oMS, _ResetDuration);
        }
        #endregion
        #region ToString functions
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\n        Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n        Rule ID : "); builder.Append(_RuleID);
                builder.Append("\r\n        Points : "); builder.Append(_Points);
                builder.Append("\r\n        Output 1 : "); builder.Append(_Output1);
                builder.Append("\r\n        Output 2 : "); builder.Append(_Output2);
                builder.Append("\r\n        SourceType : "); builder.Append(_sourceType);
                builder.Append("\r\n        EcmValyeType : "); builder.Append(_ecmValyeType);
                builder.Append("\r\n        PGN : "); builder.Append(_PGN);
                builder.Append("\r\n        SPN : "); builder.Append(_SPN);
                builder.Append("\r\n        Byte Location : "); builder.Append(_ByteLocation);
                builder.Append("\r\n        Bit Location : "); builder.Append(_BitLocation);
                builder.Append("\r\n        ECM Value : "); builder.Append(_EcmValue);
                builder.Append("\r\n        Condition : "); builder.Append(_Condition);
                builder.Append("\r\n        Duration : "); builder.Append(_Duration);
                builder.Append("\r\n        Reset Duration : "); builder.Append(_ResetDuration);
                builder.Append("\r\n        -----------------------------");
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Config Driver Points ECM Rule : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

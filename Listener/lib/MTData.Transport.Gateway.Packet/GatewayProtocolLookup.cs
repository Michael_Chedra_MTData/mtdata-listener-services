using System;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for GatewayProtocolLookup.
	/// </summary>
    [Serializable]
    public class GatewayProtocolLookup
	{
		#region Error Codes
		// SYSTEM_SETUP
		public const byte SENDBUFFER_TOO_SMALL			=	1;
		public const byte MAXPACKETSIZE_TOO_SMALL		=	2;
		public const byte PACKETSPERMIN_TOO_SMALL		=	3;
		public const byte HISTORY_INFO_SETUP			=	4;
		public const byte NETWORK_STRUCT_CHECKSUM		=	5;
		public const byte NETWORK_STRUCT_CHECKSUM_2		=	6;
		public const byte NETWORK_STRUCT_CHECKSUM_3		=	7;

		public const byte NETWORK_INFO_NO_SOH			=	11;
		public const byte NETWORK_INFO_NO_EOT			=	12;
		public const byte NETWORK_INFO_WRONG_CHECKSUM	=	13;

		public const byte SCHEDULE_NO_SOH				=	21;
		public const byte SCHEDULE_NO_EOT				=	22;
		public const byte SCHEDULE_NO_WRONG_CHECKSUM	=	23;
		public const byte SCHEDULE_LOAD_NO				=	24;
		public const byte SCHEDULE_RANGE_2				=	25;

		public const byte RECEIVEBUFFER_TOO_SMALL		=	26;
		public const byte SCHEDULE_CHECKSUM_LOAD		=	27;

		public const byte CUSTOMER_STRUCT_CHECKSUM		=	28;
		public const byte CUSTOMER_STRUCT_CHECKSUM_2	=	29;
		public const byte CUSTOMER_STRUCT_CHECKSUM_3	=	30;
		public const byte CUSTOMER_INFO_NO_SOH			=	31;
		public const byte CUSTOMER_INFO_NO_EOT			=	32;
		public const byte CUSTOMER_INFO_WRONG_CHECKSUM	=	33;


		// SYSTEM_SETPOINT
		public const byte SETPT_FILE_NO_SOH				=	1;
		public const byte SETPT_FILE_NO_EOT				=	2;
		public const byte SETPT_FILE_WRONG_CHECKSUM		=	3;
		public const byte SETPT_FILE_NO_OUT_RANGE		=	4;
		public const byte SETPT_FILE_LOAD_NO_RANGE		=	5;
		public const byte SETPT_RANGE					=	6;
		public const byte SETPT_STRUCT_CHECKSUM_2		=	7;
		public const byte SETPT_STRUCT_CHECKSUM_3		=	8;
		public const byte SETPT_RANGE_2					=	9;
		public const byte SCHEDULE_SETPT_CHECKSUM		=	10;
		public const byte SCHEDULE_SETPT_RANGE			=	11;
		public const byte SETPT_STRUCT_CHECKSUM_4		=	12;
		public const byte SETPT_STRUCT_ERROR			=	13;

		// SYSTEM ROUTE POINT
		public const byte ROUTE_FILE_NO_SOH			=	1;
		public const byte ROUTE_FILE_NO_EOT			=	2;
		public const byte ROUTE_FILE_WRONG_CHECKSUM	=	3;
		public const byte	ROUTE_FILE_NO_OUT_RANGE	=	4;
		public const byte ROUTE_FILE_LOAD_NO_RANGE	=	5;
		public const byte ROUTE_RANGE				=	6;
		public const byte ROUTE_STRUCT_CHECKSUM_2	=	7;
		public const byte ROUTE_STRUCT_CHECKSUM_3	=	8;
		public const byte ROUTE_RANGE_2				=	9;
		public const byte ROUTE_STRUCT_CHECKSUM_4	=	10;
		public const byte	ROUTE_ERROR_STRUCT		=	11;
		public const byte SCHEDULE_ROUTE_CHECKSUM	=	12;
		public const byte SCHEDULE_ROUTE_RANGE		=	13;

		// SYSTEM CONFIG
		public const byte CONFIG_FILE_NO_SOH			=	1;
		public const byte CONFIG_FILE_NO_EOT			=	2;
		public const byte CONFIG_FILE_WRONG_CHECKSUM	=	3;
		public const byte CONFIG_FILE_RANGE				=	4;
		public const byte CONFIG_STRUCT_CHECKSUM		=	5;
		public const byte CONFIG_STRUCT_CHECKSUM_2		=	6;
		public const byte CONFIG_STRUCT_CHECKSUM_3		=	7;
		public const byte CONFIG_STRUCT_CHECKSUM_4		=	8;
		public const byte CONFIG_STRUCT_CHECKSUM_5		=	9;
		public const byte CONFIG_STRUCT_CHECKSUM_6		=	10;
		public const byte CONFIG_RANGE					=	11;
		public const byte SCHEDULE_CHECKSUM				=	12;
		public const byte SCHEDULE_RANGE				=	13;
		public const byte SCHEDULE_DETAILS				=	14;

		// SYSTEM GPS FAIL
		public const byte NO_GPS_FOR_TOO_LONG			=	1;

		
		// SYSTEM GPRS FAIL
		public const byte GPRS_FAILED					=	1;
		public const byte GPRS_SEQUENCE_FAIL			=	2;
		public const byte GPRS_ESTABLISH_FAIL			=	3;
		public const byte GPRS_PPP_FAIL					=	4;
		public const byte GPRS_ERROR_API_DISCONNECT		=	10;
		public const byte GPRS_ERROR_NET_DISCONNECT_1	=	11;
		public const byte GPRS_ERROR_NET_DISCONNECT_2	=	12;
		public const byte GPRS_ERROR_NET_DISCONNECT_3	=	13;



		// SYSTEM_SEND BUFFER
		public const byte TOO_MANY_BYTES_MIN		=	1;
		public const byte SEND_LOAD_ACK_1			=	2;
		public const byte SEND_LOAD_ACK_2			=	3;
		public const byte SEND_LOAD_SEND_1			=	4;
		public const byte SEND_LOAD_SEND_2			=	5;
		public const byte SEND_PAST_SIZE			=	6;
		public const byte SEND_NO_SOH				=	7;
		public const byte SEND_NO_LENGTH			=	8;
		public const byte SEND_FLASH_BACKUP			=	9;
		public const byte SEND_NO_GPRS_COMMS		=	10;
		public const byte SEND_NO_GPRS_COMMS_2		=	11;
		public const byte SEND_RETRIES_FAIL			=	12;
		// SYSTEM RECEIVE BUFFER
		public const byte RX_NO_SOH					=	1;
		public const byte RX_NO_EOT					=	2;
		public const byte RX_WRONG_CHECKSUM			=	3;
		public const byte RX_DUPLICATE_PACKET		=	4;

		
		// SYSTEM_RESET
		public const byte SYSTEM_WAS_RESET			=	1;

		// DOWNLOAD NAK codes:
		public const byte DWNLD_ERROR_1				=	1;
		public const byte DWNLD_ERROR_2				=	2;
		public const byte DWNLD_ERROR_3				=	3;
		public const byte DWNLD_ERROR_4				=	4;
		public const byte DWNLD_ERROR_5				=	5;
		public const byte DWNLD_ERROR_6				=	6;
		public const byte DWNLD_ERROR_7				=	7;
		public const byte DWNLD_ERROR_8				=	8;
		public const byte DWNLD_ERROR_9				=	9;
		#endregion

		public GatewayProtocolLookup()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static string GetMessageType(uint messageVal)
		{
			switch (messageVal)
			{
                #region APP Ping:
				case GatewayProtocolPacket.GSP_APP_PING:					return "Application Ping";
					#endregion
					#region General message types:
                case GeneralGPPacket.GEN_NO_GPS_ANT_OPEN:           return "GEN_NO_GPS_ANT_OPEN";
                case GeneralGPPacket.GEN_NO_GPS_ANT_SHORT:          return "GEN_NO_GPS_ANT_SHORT";
				case GeneralGPPacket.GEN_IM_ALIVE:					return "GEN_IM_ALIVE";
				case GeneralGPPacket.GEN_IM_ALIVE_IMIE:				return "GEN_IM_ALIVE_IMIE";
				case GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN:			return "GEN_IM_ALIVE_IMIE_ESN";
				case GeneralGPPacket.GEN_IM_ALIVE_FULL:				return "GEN_IM_ALIVE_FULL";
				case GeneralGPPacket.GEN_NOGPS:						return "GEN_NOGPS";
				case GeneralGPPacket.GEN_OVERSPEED:					return "GEN_OVERSPEED";
				case GeneralGPPacket.GEN_EXCESSIVE_IDLE:			return "GEN_EXCESSIVE_IDLE";
				case GeneralGPPacket.GEN_TAMPER:					return "GEN_TAMPER";
				case GeneralGPPacket.GEN_EXCESSIVE_IDLE_END:		return "GEN_EXCESSIVE_IDLE_END";
				case GeneralGPPacket.GEN_ALARM:						return "GEN_ALARM";
				case GeneralGPPacket.GEN_IOONOFF:					return "GEN_IOONOFF";
				case GeneralGPPacket.GEN_OUTPUT:					return "GEN_OUTPUT";
				case GeneralGPPacket.GEN_SLEEPING:					return "GEN_SLEEPING";
				case GeneralGPPacket.GEN_STARTUP:					return "GEN_STARTUP";
				case GeneralGPPacket.GEN_ACK:						return "GEN_ACK";
				case GeneralGPPacket.GEN_NAK:						return "GEN_NAK";
				case GeneralGPPacket.GEN_RESET:						return "GEN_RESET";
				case GeneralGPPacket.GEN_CLEAR_SENDBUF:				return "GEN_CLEAR_SENDBUF";
				case GeneralGPPacket.STATUS_RX_TX:					return "STATUS_RX_TX";
				case GeneralGPPacket.GEN_NEW_NETWORK_FOUND:			return "GEN_NEW_NETWORK_FOUND";
                case GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START:  return "GEN_G_FORCES_SUSTAINED_START";
                case GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END:    return "GEN_G_FORCES_SUSTAINED_END";
                case GeneralGPPacket.GEN_ECM_OVERSPEED:             return "GEN_ECM_OVERSPEED";
                case GeneralGPPacket.GEN_CONFIG_DOWNLOADED:         return "GEN_CONFIG_DOWNLOADED";
                    
                #endregion
                #region Alarm message types:
                case GeneralGPPacket.ALRM_SUSPECT_GPS:              return "ALRM_SUSPECT_GPS";
                case GeneralGPPacket.ALRM_IGNITION_DISCONNECT:      return "ALRM_IGNITION_DISCONNECT";
                case GeneralGPPacket.ALRM_ENGINE_DATA_MISSING:      return "ALRM_ENGINE_DATA_MISSING";
                case GeneralGPPacket.ALRM_ENGINE_DATA_RE_CONNECT:   return "ALRM_ENGINE_DATA_RE_CONNECT";
                case GeneralGPPacket.ALRM_SUSPEC_ENGINE_DATA:       return "ALRM_SUSPEC_ENGINE_DATA";
                case GeneralGPPacket.ALRM_G_FORCE_MISSING:          return "ALRM_G_FORCE_MISSING";
                case GeneralGPPacket.ALRM_G_FORCE_OUT_OF_CALIBRATION: return "ALRM_G_FORCE_OUT_OF_CALIBRATION";
                case GeneralGPPacket.ALRM_CONCETE_SENSOR_ALERT:     return "ALRM_CONCETE_SENSOR_ALERT";
                case GeneralGPPacket.ALRM_MDT_NOT_LOGGED_IN:        return "ALRM_MDT_NOT_LOGGED_IN";
                case GeneralGPPacket.ALRM_MDT_NOT_BEING_USED:       return "ALRM_MDT_NOT_BEING_USED";
                case GeneralGPPacket.ALRM_UNIT_POWERUP:             return "ALRM_UNIT_POWERUP";
				case GeneralGPPacket.ALRM_VEHICLE_GPS_SPEED_DIFF: return "ALRM_VEHICLE_GPS_SPEED_DIFF";
                case GeneralGPPacket.ALARM_LOW_BATTERY: return "ALARM LOW BATTERY";
					#endregion
					#region SET PoinT message types:
				case RoutePtSetPtGPPacket.SETPT_ARRIVE:				return "SETPT_ARRIVE";
				case RoutePtSetPtGPPacket.SETPT_DEPART:				return "SETPT_DEPART";
				case RoutePtSetPtGPPacket.SETPT_DURATION_OVER:		return "SETPT_DURATION_OVER";
				case RoutePtSetPtGPPacket.SETPT_DURATION_UNDER:		return "SETPT_DURATION_UNDER";
				case RoutePtSetPtGPPacket.SETPT_OUTSIDE_HRS:		return "SETPT_OUTSIDE_HRS";
				case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_ARRIVE:	return "SETPT_SPECIAL_1_ARRIVE";
				case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_DEPART:	return "SETPT_SPECIAL_1_DEPART";
				case RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK:		return "SETPT_ARRIVE_DOCK";
				case RoutePtSetPtGPPacket.SETPT_DEPART_DOCK:		return "SETPT_DEPART_DOCK";
				case RoutePtSetPtGPPacket.SETPT_SPEEDING_AT_WP:		return "SETPT_SPEEDING_AT_WP";
				case RoutePtSetPtGPPacket.SETPT_ARRIVE_WORKSHOP:	return "SETPT_ARRIVE_WORKSHOP";
				case RoutePtSetPtGPPacket.SETPT_DEPART_WORKSHOP:	return "SETPT_DEPART_WORKSHOP";
				case RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO:		return "SETPT_ARRIVE_NOGO";
				case RoutePtSetPtGPPacket.SETPT_DEPART_NOGO:		return "SETPT_DEPART_NOGO";
				case RoutePtSetPtGPPacket.SETPT_HOLDING_EXPIRY:		return "SETPT_HOLDING_EXPIRY";
					#endregion
					#region ROUTING message types:
				case RoutingModuleGPPacket.ROUTES_ARRIVE_EARLY:		return "ROUTES_ARRIVE_EARLY";
				case RoutingModuleGPPacket.ROUTES_ARRIVE_ONTIME:	return "ROUTES_ARRIVE_ONTIME";
				case RoutingModuleGPPacket.ROUTES_ARRIVE_LATE:		return "ROUTES_ARRIVE_LATE";
				case RoutingModuleGPPacket.ROUTES_DEPART_EARLY:		return "ROUTES_DEPART_EARLY";
				case RoutingModuleGPPacket.ROUTES_DEPART_ONTIME:	return "ROUTES_DEPART_ONTIME";
				case RoutingModuleGPPacket.ROUTES_DEPART_LATE:		return "ROUTES_DEPART_LATE";
				case RoutingModuleGPPacket.ROUTES_DURATION_OVER:	return "ROUTES_DURATION_OVER";
				case RoutingModuleGPPacket.ROUTES_DURATION_UNDER:	return "ROUTES_DURATION_UNDER";
				case RoutingModuleGPPacket.ROUTES_LATE:				return "ROUTES_LATE";
				case RoutingModuleGPPacket.ROUTES_UPDATE_ACCEPTED:	return "ROUTES_UPDATE_ACCEPTED";
				case RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED:	return "ROUTES_STATUS_CANCELLED";
				case RoutingModuleGPPacket.ROUTES_STATUS_ACTIVATED:	return "ROUTES_STATUS_ACTIVATED";
				case RoutingModuleGPPacket.ROUTES_STATUS_COMPLETED:	return "ROUTES_STATUS_COMPLETED";
				case RoutingModuleGPPacket.ROUTES_DEPART_MISSED:	return "ROUTES_DEPART_MISSED";
					#endregion
					#region STATUS message types:
				case GeneralGPPacket.STATUS_REPORT:					return "STATUS_REPORT";
				case GeneralGPPacket.STATUS_TRAILER_HITCH:			return "STATUS_TRAILER_HITCH";
				case GeneralGPPacket.STATUS_TRAILER_DEHITCH:		return "STATUS_TRAILER_DEHITCH";
				case GeneralGPPacket.STATUS_ACCIDENT_NEW:			return "STATUS_ACCIDENT_NEW";
				case GeneralGPPacket.OVERSPEED_ZONE_1_START:		return "OVERSPEED_ZONE_1_START";
				case GeneralGPPacket.OVERSPEED_ZONE_2_START	:		return "OVERSPEED_ZONE_2_START";
				case GeneralGPPacket.OVERSPEED_ZONE_3_START:		return "OVERSPEED_ZONE_3_START";
				case GeneralGPPacket.OVERSPEED_ZONE_1_END:			return "OVERSPEED_ZONE_1_END";
				case GeneralGPPacket.OVERSPEED_ZONE_2_END:			return "OVERSPEED_ZONE_2_END";
				case GeneralGPPacket.OVERSPEED_ZONE_3_END:			return "OVERSPEED_ZONE_3_END";
				case GeneralGPPacket.ANGEL_GEAR_ALERT_START:		return "ANGEL_GEAR_ALERT_START";
				case GeneralGPPacket.ANGEL_GEAR_ALERT_END:			return "ANGEL_GEAR_ALERT_END";
				case GeneralGPPacket.FATIGUE_REPORT_IGNON :			return "FATIGUE_REPORT_IGNON";
				case GeneralGPPacket.FATIGUE_REPORT_24 :			return "FATIGUE_REPORT_24";
				case GeneralGPPacket.STATUS_RAW_GPS:				return "STATUS_RAW_GPS";
				case GeneralGPPacket.STATUS_POS_REPORT:				return "STATUS_POSITION_REPORT";
				case GeneralGPPacket.STATUS_IGNITION_ON:			return "STATUS_IGNITION_ON";
				case GeneralGPPacket.STATUS_IGNITION_OFF:			return "STATUS_IGNITION_OFF";
				case GeneralGPPacket.STATUS_IO_PULSES:				return "STATUS_IO_PULSES";
				case GeneralGPPacket.BARREL_CONCRETE_AGE_MINS:		return "BARREL_CONCRETE_AGE_MINS";
				case GeneralGPPacket.BARREL_CONCRETE_AGE_ROTATIONS:	return "BARREL_CONCRETE_AGE_ROTATIONS";
				case GeneralGPPacket.ENGINE_FUEL_ECONOMY_ALERT:		return "ENGINE_FUEL_ECONOMY_ALERT";
				case GeneralGPPacket.STATUS_DATA_USAGE_ALERT:		return "STATUS_DATA_USAGE_ALERT";
                case GeneralGPPacket.STATUS_RX_TX_SATTELITE:        return "STATUS_RX_TX_SATTELITE";
                case GeneralGPPacket.STATUS_DATA_USAGE_ALERT_SATELLITE: return "STATUS_DATA_USAGE_ALERT_SATELLITE";
                case GeneralGPPacket.GEN_GFORCEAUTOCALIBRATE:       return "GEN_GFORCEAUTOCALIBRATE";
					#endregion
					#region BARREL messages:
				case GeneralGPPacket.BARREL_RPM_UP_TO_SPEED:		return "BARREL_RPM_UP_TO_SPEED";
				case GeneralGPPacket.BARREL_RPM_STOPPED:			return "BARREL_RPM_STOPPED";
				case GeneralGPPacket.BARREL_RPM_REVERSED:			return "BARREL_RPM_REVERSED";
				case GeneralGPPacket.BARREL_RPM_FORWARD:			return "BARREL_RPM_FORWARD";
				case GeneralGPPacket.BARREL_MIX_STARTED:			return "BARREL_MIX_STARTED";
				case GeneralGPPacket.BARREL_NOT_MIXED:				return "BARREL_NOT_MIXED";
				case GeneralGPPacket.BARREL_LEFT_LOADER:			return "BARREL_LEFT_LOADER";
				case GeneralGPPacket.BARREL_ABOVE_ONSITE_SPEED:		return "BARREL_ABOVE_ONSITE_SPEED";
				case GeneralGPPacket.BARREL_LOADING_STARTED:		return "BARREL_LOADING_STARTED";
					#endregion
					#region EXTENDED_IO alerts:
				case GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD:	return "EXT_IO_BELOW_THRESHOLD";			
				case GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD:	return "EXT_IO_ABOVE_THRESHOLD";	
					#endregion
					#region ENGINE alerts
				case GeneralGPPacket.ENGINE_OVER_RPM:				return "ENGINE_OVER_RPM";
				case GeneralGPPacket.ENGINE_OVER_COOLANT_TEMP:		return "ENGINE_OVER_COOLANT_TEMP";
				case GeneralGPPacket.ENGINE_OVER_OIL_TEMP:			return "ENGINE_OVER_OIL_TEMP";
				case GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_LOW:	return "ENGINE_UNDER_OIL_PRESSURE_LOW";
				case GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_HIGH:return "ENGINE_UNDER_OIL_PRESSURE_HIGH";
				case GeneralGPPacket.ENGINE_G_FORCES_HIGH:			return "ENGINE_G_FORCES_HIGH";
				case GeneralGPPacket.ENGINE_G_FORCE_CALIBRATED_RPT:	return "ENGINE_G_FORCE_CALIBRATED_RPT";					
				case GeneralGPPacket.ENGINE_COOLANT_LEVEL_LOW:		return "ENGINE_COOLANT_LEVEL_LOW";
				case GeneralGPPacket.ENGINE_ERROR_CODE:				return "ENGINE_ERROR_CODE";
					#endregion
					#region CONFIGuration message types:
				case ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE:			return "CONFIG_TOTAL_ACTIVE";
				case ConfigFileGPPacket.CONFIG_NEW_FILE:				return "CONFIG_NEW_FILE";
				case ConfigNewRoutePtGPPacket.CONFIG_NEW_ROUTE:			return "CONFIG_NEW_ROUTE";
				case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT:			return "CONFIG_NEW_SETPT";
				case 0xd4:												return "CONFIG_NEW_PROGRAM (not supported)";
				case ConfigNetworkInfoGPPacket.CONFIG_NETWORK_INFO:		return "CONFIG_NETWORK_INFO";
				case ConfigScheduleInfoGPPacket.CONFIG_SCHEDULE_INFO:	return "CONFIG_SCHEDULE_INFO";
				case ConfigScheduleClearGPPacket.CONFIG_SCHEDULE_CLEAR:	return "CONFIG_SCHEDULE_CLEAR";
				case ConfigCustomerInfoGPPacket.CONFIG_CUSTOMER_INFO:	return "CONFIG_CUSTOMER_INFO";
				case ConfigRouteUpdateGPPacket.CONFIG_ROUTE_UPDATE:		return "CONFIG_ROUTE_UPDATE";
				case ConfigRouteUpdateGPPacket.CONFIG_ROUTE_DELETE:		return "CONFIG_ROUTE_DELETE";
				case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON:	return "CONFIG_NEW_SETPT_POLYGON";
                case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON_OLD: return "CONFIG_OLD_SETPT_POLYGON";
                case ConfigDriverPointsGPPacket.CONFIG_DRIVER_POINTS_RULE: return "CONFIG_DRIVER_POINTS_RULE";
                case DriverPointsRuleRequest.DRIVER_POINTS_RULE_REQUEST: return "DRIVER_POINTS_RULE_REQUEST";
                case ConfigEcmPacket.CONFIG_ECM_HYSTERESIS: return "CONFIG_ECM_HYSTERESIS";
                case GeneralGPPacket.ECM_HYSTERESIS_ERROR: return "ECM_HYSTERESIS_ERROR";
                    #endregion
					#region GPS message types:
				case GPSHistoryGPPacket.GPS_HISTORY:				return "GPS_HISTORY";
                case ConfigWatchList.CONFIG_WATCH_LIST:             return "CONFIG_WATCH_LIST";
                case FirmwareDriverLoginReply.FIRMWARE_LOGIN_REPLY: return "FIRMWARE_LOGIN_REPLY";
					#endregion
					#region SYSTEM message types:
				case GatewayProtocolPacket.SYSTEM_SETUP:			return "SYSTEM_SETUP";
				case GatewayProtocolPacket.SYSTEM_SETPOINT:			return "SYSTEM_SETPOINT";
				case GatewayProtocolPacket.SYSTEM_ROUTEPOINT:		return "SYSTEM_ROUTEPOINT";
				case GatewayProtocolPacket.SYSTEM_CONFIG:			return "SYSTEM_CONFIG";
				case GatewayProtocolPacket.SYSTEM_GPS_FAILURE:		return "SYSTEM_GPS_FAILURE";
				case GatewayProtocolPacket.SYSTEM_GPRS_FAILURE:		return "SYSTEM_GPRS_FAILURE";
				case GatewayProtocolPacket.SYSTEM_SENDBUFFER:		return "SYSTEM_SENDBUFFER";
				case GatewayProtocolPacket.SYSTEM_RECEIVEBUFFER:	return "SYSTEM_RECEIVEBUFFER";
				case GatewayProtocolPacket.SYSTEM_SMS_FAILURE:		return "SYSTEM_SMS_FAILURE";
				case GatewayProtocolPacket.SYSTEM_HARDWARE_FAILURE:	return "SYSTEM_HARDWARE_FAILURE";
				case GatewayProtocolPacket.SYSTEM_UNIT_RESET:		return "SYSTEM_UNIT_RESET";
					#endregion			
					#region DOWNLOAD message types:
				case ProgramDownloadPacket.DOWNLOAD_PROGRAM:		return "DOWNLOAD_PROGRAM";
				case ProgramDownloadPacket.DOWNLOAD_IOBOX:			return "DOWNLOAD_IOBOX";
				case ProgramDownloadPacket.DOWNLOAD_MDT:			return "DOWNLOAD_MDT";
				case ProgramVerifyPacket.DOWNLOAD_VERIFY:			return "DOWNLOAD_VERIFY";
				case ProgramVerifyPacket.DOWNLOAD_IOBOX_VERIFY:		return "DOWNLOAD_IOBOX_VERIFY";
				case ProgramVerifyPacket.DOWNLOAD_MDT_VERIFY:		return "DOWNLOAD_MDT_VERIFY";
                case ProgramDownloadPacket.DOWNLOAD_FILE: return "DOWNLOAD_FILE";
					#endregion
					#region Refrigeration message types
				case GeneralGPPacket.REFRIG_REPORT:					return "REFRIG_REPORT";
					#endregion
                    #region Mass Declaration message types
                case MassDeclarationPacket.MASS_DECLARATION: return "MASS_DECLARATION";
                case DriverPointsRuleBreakPacket.DRIVER_POINTS_RULE_BROKEN: return "DRIVER_POINTS_RULE_BROKEN";
                case GenericPacket.GENERIC_PACKET: return "GENERIC_PACKET";
                    #endregion
                case DriverPointsConfigError.DRIVER_POINTS_CONFIG_ERROR: return "DRIVER_POINTS_CONFIG_ERROR";
                case DriverPointsServerTrackingRuleBroken.DRIVER_POINTS_SERVER_TRACKING_RULE_BROKEN: return "DRIVER_POINTS_SERVER_TRACKING_RULE_BROKEN";
                case GeneralGPPacket.DRIVER_PERFORMANCE_METRICS: return "DRIVER_PERFORMANCE_METRICS";
                case GeneralGPPacket.DRIVER_DATAACCUMLATOR_METRICS: return "DRIVER_DATAACCUMLATOR_METRICS";
                case GeneralGPPacket.STATUS_VEHICLE_BREAK_START: return "STATUS_VEHICLE_BREAK_START";
                case GeneralGPPacket.STATUS_VEHICLE_BREAK_END: return "STATUS_VEHICLE_BREAK_END";
                case GeneralGPPacket.DRIVER_PERSONAL_KMS: return "DRIVER_PERSONAL_KMS";
                case GeneralGPPacket.DRIVER_OVERSPEED_REPORT: return "DRIVER_OVERSPEED_REPORT";
                case GeneralGPPacket.GEN_MDT_STATUS_REPORT: return "GEN_MDT_STATUS_REPORT";
                case GeneralGPPacket.PENDENT_ALARM_TEST: return "PENDENT_ALARM_TEST";
                case GeneralGPPacket.PENDENT_ALARM: return "PENDENT_ALARM";
                case GeneralGPPacket.PENDENT_ALARM_CLEARED: return "PENDENT_ALARM_CLEARED";
                case GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER: return "PENDENT_ALARM_ACKD_BY_USER";
                case GeneralGPPacket.PENDENT_ALARM_NOT_ACKD_BY_USER: return "PENDENT_ALARM_NOT_ACKD_BY_USER";
                case GeneralGPPacket.STATUS_LOGIN: return "FIRMWARE_LOGIN";
                case GeneralGPPacket.STATUS_LOGOUT: return "FIRMWARE_LOGOUT";
                case GeneralGPPacket.GEN_ZERO_BYTES_RECEIVED_RESTART: return "GEN_ZERO_BYTES_RECEIVED_RESTART";
                case GeneralGPPacket.GEN_IF_CONFIG_RESTART: return "GEN_IF_CONFIG_RESTART";
                case GeneralGPPacket.SHELL_REQUEST_REBOOT: return "SHELL_REQUEST_REBOOT";
                case GeneralGPPacket.GFORCE_CALIBRATION: return "GFORCE_CALIBRATION";
                case GeneralGPPacket.GPS_SETTINGS: return "GPS_SETTINGS";
                case GeneralGPPacket.DIAGNOSTIC_LEVEL: return "DIAGNOSTIC_LEVEL";
                case GeneralGPPacket.DIAGNOSTIC: return "DIAGNOSTIC";
                case GeneralGPPacket.POSSIBLE_ACCIDENT: return "POSSIBLE_ACCIDENT";
                case GeneralGPPacket.SAT_PING: return "SAT_PING";
                case GeneralGPPacket.TAMPER: return "TAMPER";
                case GeneralGPPacket.LOW_POWER_MODE: return "LOW_POWER_MODE";

                default: return "Unknown Message Type";
			}
		}
		public static string GetErrorCode(uint messageVal, byte reasonCode)
		{
			string retVal;
			switch (messageVal)
			{
				#region SYSTEM_SETUP
				case GatewayProtocolPacket.SYSTEM_SETUP:
					switch (reasonCode)
					{
						case SENDBUFFER_TOO_SMALL:			retVal = "SENDBUFFER_TOO_SMALL";			break;
						case MAXPACKETSIZE_TOO_SMALL:		retVal = "MAXPACKETSIZE_TOO_SMALL";			break;
						case PACKETSPERMIN_TOO_SMALL:		retVal = "PACKETSPERMIN_TOO_SMALL";			break;
						case HISTORY_INFO_SETUP:			retVal = "HISTORY_INFO_SETUP";				break;
						case NETWORK_STRUCT_CHECKSUM:		retVal = "NETWORK_STRUCT_CHECKSUM";			break;
						case NETWORK_STRUCT_CHECKSUM_2:		retVal = "NETWORK_STRUCT_CHECKSUM_2";		break;
						case NETWORK_STRUCT_CHECKSUM_3:		retVal = "NETWORK_STRUCT_CHECKSUM_3";		break;
						case NETWORK_INFO_NO_SOH:			retVal = "NETWORK_INFO_NO_SOH";				break;
						case NETWORK_INFO_NO_EOT:			retVal = "NETWORK_INFO_NO_EOT";				break;		
						case NETWORK_INFO_WRONG_CHECKSUM:	retVal = "NETWORK_INFO_WRONG_CHECKSUM";		break;
						case SCHEDULE_NO_SOH:				retVal = "SCHEDULE_NO_SOH";					break;
						case SCHEDULE_NO_EOT:				retVal = "SCHEDULE_NO_EOT";					break;
						case SCHEDULE_NO_WRONG_CHECKSUM:	retVal = "SCHEDULE_NO_WRONG_CHECKSUM";		break;
						case SCHEDULE_LOAD_NO:				retVal = "SCHEDULE_LOAD_NO";				break;
						case SCHEDULE_RANGE_2:				retVal = "SCHEDULE_RANGE_2";				break;
						case RECEIVEBUFFER_TOO_SMALL:		retVal = "RECEIVEBUFFER_TOO_SMALL";			break;
						case SCHEDULE_CHECKSUM_LOAD:		retVal = "SCHEDULE_CHECKSUM_LOAD";			break;
						case CUSTOMER_STRUCT_CHECKSUM:		retVal = "CUSTOMER_STRUCT_CHECKSUM";		break;
						case CUSTOMER_STRUCT_CHECKSUM_2:	retVal = "CUSTOMER_STRUCT_CHECKSUM_2";		break;
						case CUSTOMER_STRUCT_CHECKSUM_3:	retVal = "CUSTOMER_STRUCT_CHECKSUM_3";		break;
						case CUSTOMER_INFO_NO_SOH:			retVal = "CUSTOMER_INFO_NO_SOH";			break;
						case CUSTOMER_INFO_NO_EOT:			retVal = "CUSTOMER_INFO_NO_EOT";			break;
						case CUSTOMER_INFO_WRONG_CHECKSUM:	retVal = "CUSTOMER_INFO_WRONG_CHECKSUM";	break;

						default:							retVal = "Unknown SYSTEM_SETUP Error Code";	break;
					}
					break;
					#endregion
				#region SYSTEM_SETPOINT
				case GatewayProtocolPacket.SYSTEM_SETPOINT:
					switch(reasonCode)
					{
						case SETPT_FILE_NO_SOH:				retVal = "SETPT_FILE_NO_SOH"; break;
						case SETPT_FILE_NO_EOT:				retVal = "SETPT_FILE_NO_EOT"; break;
						case SETPT_FILE_WRONG_CHECKSUM:		retVal = "SETPT_FILE_WRONG_CHECKSUM"; break;
						case SETPT_FILE_NO_OUT_RANGE:		retVal = "SETPT_FILE_NO_OUT_RANGE"; break;
						case SETPT_FILE_LOAD_NO_RANGE:		retVal = "SETPT_FILE_LOAD_NO_RANGE"; break;
						case SETPT_RANGE:					retVal = "SETPT_RANGE"; break;
						case SETPT_STRUCT_CHECKSUM_2:		retVal = "SETPT_STRUCT_CHECKSUM_2"; break;
						case SETPT_STRUCT_CHECKSUM_3:		retVal = "SETPT_STRUCT_CHECKSUM_3"; break;
						case SETPT_RANGE_2:					retVal = "SETPT_RANGE_2"; break;
						case SCHEDULE_SETPT_CHECKSUM:		retVal = "SCHEDULE_SETPT_CHECKSUM"; break;
						case SCHEDULE_SETPT_RANGE:			retVal = "SCHEDULE_SETPT_RANGE"; break;
						case SETPT_STRUCT_CHECKSUM_4:		retVal = "SETPT_STRUCT_CHECKSUM_4"; break;
						case SETPT_STRUCT_ERROR:			retVal = "SETPT_STRUCT_ERROR"; break;
						default:	retVal = "Unknown SYSTEM_SETPOINT Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_ROUTEPOINT
				case GatewayProtocolPacket.SYSTEM_ROUTEPOINT:
					switch(reasonCode)
					{
						case ROUTE_FILE_NO_SOH:				retVal = "ROUTE_FILE_NO_SOH"; break;
						case ROUTE_FILE_NO_EOT:				retVal = "ROUTE_FILE_NO_EOT"; break;
						case ROUTE_FILE_WRONG_CHECKSUM:		retVal = "ROUTE_FILE_WRONG_CHECKSUM"; break;
						case ROUTE_FILE_NO_OUT_RANGE:		retVal = "ROUTE_FILE_NO_OUT_RANGE"; break;
						case ROUTE_FILE_LOAD_NO_RANGE:		retVal = "ROUTE_FILE_LOAD_NO_RANGE"; break;
						case ROUTE_RANGE:					retVal = "ROUTE_RANGE"; break;
						case ROUTE_STRUCT_CHECKSUM_2:		retVal = "ROUTE_STRUCT_CHECKSUM_2"; break;
						case ROUTE_STRUCT_CHECKSUM_3:		retVal = "ROUTE_STRUCT_CHECKSUM_3"; break;
						case ROUTE_RANGE_2:					retVal = "ROUTE_RANGE_2"; break;
						case ROUTE_STRUCT_CHECKSUM_4:		retVal = "ROUTE_STRUCT_CHECKSUM_4"; break;
						case ROUTE_ERROR_STRUCT:			retVal = "ROUTE_ERROR_STRUCT"; break;
						case SCHEDULE_ROUTE_CHECKSUM:		retVal = "SCHEDULE_ROUTE_CHECKSUM"; break;
						case SCHEDULE_ROUTE_RANGE:			retVal = "SCHEDULE_ROUTE_RANGE"; break;
						default:	retVal = "Unknown SYSTEM_ROUTEPOINT Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_CONFIG
				case GatewayProtocolPacket.SYSTEM_CONFIG:
					switch(reasonCode)
					{
						case CONFIG_FILE_NO_SOH:			retVal = "CONFIG_FILE_NO_SOH"; break;
						case CONFIG_FILE_NO_EOT:			retVal = "CONFIG_FILE_NO_EOT"; break;
						case CONFIG_FILE_WRONG_CHECKSUM:			retVal = "CONFIG_FILE_WRONG_CHECKSUM"; break;
						case CONFIG_FILE_RANGE:			retVal = "CONFIG_FILE_RANGE"; break;
						case CONFIG_STRUCT_CHECKSUM:			retVal = "CONFIG_STRUCT_CHECKSUM"; break;
						case CONFIG_STRUCT_CHECKSUM_2:			retVal = "CONFIG_STRUCT_CHECKSUM_2"; break;
						case CONFIG_STRUCT_CHECKSUM_3:			retVal = "CONFIG_STRUCT_CHECKSUM_3"; break;
						case CONFIG_STRUCT_CHECKSUM_4:			retVal = "CONFIG_STRUCT_CHECKSUM_4"; break;
						case CONFIG_STRUCT_CHECKSUM_5:			retVal = "CONFIG_STRUCT_CHECKSUM_5"; break;
						case CONFIG_STRUCT_CHECKSUM_6:			retVal = "CONFIG_STRUCT_CHECKSUM_6"; break;
						case CONFIG_RANGE:			retVal = "CONFIG_RANGE"; break;
						case SCHEDULE_CHECKSUM:			retVal = "SCHEDULE_CHECKSUM"; break;
						case SCHEDULE_RANGE:			retVal = "SCHEDULE_RANGE"; break;
						case SCHEDULE_DETAILS:			retVal = "SCHEDULE_DETAILS"; break;
						default:	retVal = "Unknown SYSTEM_CONFIG Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_GPS_FAILURE
				case GatewayProtocolPacket.SYSTEM_GPS_FAILURE:
					switch(reasonCode)
					{
						case NO_GPS_FOR_TOO_LONG:	retVal = "NO_GPS_FOR_TOO_LONG"; break;
						default:	retVal = "Unknown SYSTEM_GPS_FAILURE Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_GPRS_FAILURE
				case GatewayProtocolPacket.SYSTEM_GPRS_FAILURE:
					switch(reasonCode)
					{
						case GPRS_FAILED:					retVal = "GPRS_FAILED"; break;
						case GPRS_SEQUENCE_FAIL:			retVal = "GPRS_SEQUENCE_FAIL"; break;
						case GPRS_ESTABLISH_FAIL:			retVal = "GPRS_ESTABLISH_FAIL"; break;
						case GPRS_PPP_FAIL:					retVal = "GPRS_PPP_FAIL"; break;
						case GPRS_ERROR_API_DISCONNECT:		retVal = "GPRS_ERROR_API_DISCONNECT"; break;
						case GPRS_ERROR_NET_DISCONNECT_1:	retVal = "GPRS_ERROR_NET_DISCONNECT_1"; break;
						case GPRS_ERROR_NET_DISCONNECT_2:	retVal = "FAR END DISCONNECTED (DISC2)"; break;
						case GPRS_ERROR_NET_DISCONNECT_3:	retVal = "FAR END DISCONNECTED (DISC3)"; break;

						default:	retVal = "Unknown SYSTEM_GPRS_FAILURE Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_SENDBUFFER
				case GatewayProtocolPacket.SYSTEM_SENDBUFFER:
					switch(reasonCode)
					{
						case TOO_MANY_BYTES_MIN:	retVal = "TOO_MANY_BYTES_MIN"; break;
						case SEND_LOAD_ACK_1:		retVal = "SEND_LOAD_ACK_1"; break;
						case SEND_LOAD_ACK_2:		retVal = "SEND_LOAD_ACK_2"; break;
						case SEND_LOAD_SEND_1:		retVal = "SEND_LOAD_SEND_1"; break;
						case SEND_LOAD_SEND_2:		retVal = "SEND_LOAD_SEND_2"; break;
						case SEND_PAST_SIZE:		retVal = "SEND_PAST_SIZE"; break;
						case SEND_NO_SOH:			retVal = "SEND_NO_SOH"; break;
						case SEND_NO_LENGTH:		retVal = "SEND_NO_LENGTH"; break;
						case SEND_FLASH_BACKUP:		retVal = "SEND_FLASH_BACKUP"; break;
						case SEND_NO_GPRS_COMMS:	retVal = "SEND_NO_GPRS_COMMS"; break;
						case SEND_NO_GPRS_COMMS_2:	retVal = "SEND_NO_GPRS_COMMS_2"; break;
						case SEND_RETRIES_FAIL:		retVal = "SEND_RETRIES_FAIL"; break;
						default:	retVal = "Unknown SYSTEM_SENDBUFFER Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_RECEIVEBUFFER
				case GatewayProtocolPacket.SYSTEM_RECEIVEBUFFER:
					switch(reasonCode)
					{
						case RX_NO_SOH:				retVal = "RX_NO_SOH"; break;
						case RX_NO_EOT:				retVal = "RX_NO_EOT"; break;
						case RX_WRONG_CHECKSUM:		retVal = "RX_WRONG_CHECKSUM"; break;
						case RX_DUPLICATE_PACKET:	retVal = "RX_DUPLICATE_PACKET"; break;
						default:	retVal = "Unknown SYSTEM_RECEIVEBUFFER Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_SMS_FAILURE
				case GatewayProtocolPacket.SYSTEM_SMS_FAILURE:
					switch(reasonCode)
					{
						default:	retVal = "Unknown SYSTEM_SMS_FAILURE Error Code";	break;
					}
					break;
				#endregion
				#region SYSTEM_HARDWARE_FAILURE
				case GatewayProtocolPacket.SYSTEM_HARDWARE_FAILURE:
					switch(reasonCode)
					{

						default:	retVal = "Unknown SYSTEM_HARDWARE_FAILURE Error Code";	break;
					}
					break;
				#endregion

				#region SYSTEM_RESET
				case GatewayProtocolPacket.SYSTEM_RESET:
					switch(reasonCode)
					{
						case SYSTEM_WAS_RESET:		retVal = "SYSTEM_WAS_RESET"; break;
						default:	retVal = "Unknown SYSTEM_RESET Error Code";	break;
					}
					break;
				#endregion
				#region DOWNLOAD NAK codes
				case GeneralGPPacket.GEN_NAK:
				switch (reasonCode)
				{
					case DWNLD_ERROR_1:	retVal = "DOWNLOAD_ERROR_1"; break;
					case DWNLD_ERROR_2:	retVal = "DOWNLOAD_ERROR_2"; break;
					case DWNLD_ERROR_3:	retVal = "DOWNLOAD_ERROR_3"; break;
					case DWNLD_ERROR_4:	retVal = "DOWNLOAD_ERROR_4"; break;
					case DWNLD_ERROR_5:	retVal = "DOWNLOAD_ERROR_5"; break;
					case DWNLD_ERROR_6:	retVal = "DOWNLOAD_ERROR_6"; break;
					case DWNLD_ERROR_7:	retVal = "DOWNLOAD_ERROR_7"; break;
					case DWNLD_ERROR_8:	retVal = "DOWNLOAD_ERROR_8"; break;
					case DWNLD_ERROR_9:	retVal = "DOWNLOAD_ERROR_9"; break;
					default:			retVal = "Unknown GEN_NAK Error Code";	break;
				}
					break;
					#endregion
				default:
					retVal = "";
					break;
			}
			return retVal;
		}
	}
}

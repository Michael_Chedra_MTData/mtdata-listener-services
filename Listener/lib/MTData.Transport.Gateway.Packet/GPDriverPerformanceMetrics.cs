using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class GPDriverPerformanceMetrics : GatewayProtocolPacket, IStatusHolder, IPositionHolder
    {
        #region Private Vars
        private string _serverTime_DateFormat = "";
        private byte _packetVer = (byte) 0x00;
        private ushort _drivingTime = 0;
        private ushort _idleTime = 0;
        private ushort _excessiveIdleTime = 0;
        private ushort _ignitionOnTime = 0;
        private ushort _warmUpTime = 0;
        private ushort _brakeCount = 0;
        private ushort _brakeRoll = 0;
        private ushort _hardBrakingCount = 0;
        private ushort _hardCorneringCount = 0;
        private ushort _hardAccelerationCount = 0;
        private ushort _sustainedGForceEvents = 0;
        private ushort _accidentBufferEvents = 0;
        private ushort _brakeOnTime = 0;
        #endregion
        #region Public Vars
        public GPClock mCurrentClock;
        public GPClock mFixClock;
        public GPPositionFix mFix;
        public GPStatus mStatus;
        public GPDistance mDistance;
        #endregion
        #region Public Properties
        public int PacketVersion { get { return (int)_packetVer; } set { _packetVer = (byte) value; } }
        public ushort DrivingTime { get { return _drivingTime; } set { _drivingTime = value; } }
        public ushort IdleTime { get { return _idleTime; } set { _idleTime = value; } }
        public ushort ExcessiveIdleTime { get { return _excessiveIdleTime; } set { _excessiveIdleTime = value; } }
        public ushort IgnitionOnTime { get { return _ignitionOnTime; } set { _ignitionOnTime = value; } }
        public ushort WarmUpTime { get { return _warmUpTime; } set { _warmUpTime = value; } }
        public ushort BrakeCount { get { return _brakeCount; } set { _brakeCount = value; } }
        public ushort BrakeRoll { get { return _brakeRoll; } set { _brakeRoll = value; } }
        public ushort HardBrakingCount { get { return _hardBrakingCount; } set { _hardBrakingCount = value; } }
        public ushort HardCorneringCount { get { return _hardCorneringCount; } set { _hardCorneringCount = value; } }
        public ushort HardAccelerationCount { get { return _hardAccelerationCount; } set { _hardAccelerationCount = value; } }
        public ushort SustainedGForceEvents { get { return _sustainedGForceEvents; } set { _sustainedGForceEvents = value; } }
        public ushort AccidentBufferEvents { get { return _accidentBufferEvents; } set { _accidentBufferEvents = value; } }
        public ushort BrakeOnTime { get { return _brakeOnTime; } set { _brakeOnTime = value; } }
        #endregion
        #region Interface Support
        public GPStatus GetStatus()
        {
            return mStatus;
        }
        public GPPositionFix GetPosition()
        {
            return mFix;
        }
        public GPClock GetFixClock()
        {
            return mFixClock;
        }
        #endregion
        #region Constructor
        public GPDriverPerformanceMetrics(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			mCurrentClock = new GPClock("Device", _serverTime_DateFormat); 
			mFixClock = new GPClock("GPS", _serverTime_DateFormat); 
			mFix = new GPPositionFix(); 
			mStatus = new GPStatus(); 
			mDistance = new GPDistance();
			string sConstructorDecodeError = Decode(p.mDataField);
			bIsPopulated = (sConstructorDecodeError == null);
        }
        #endregion
        #region Decode
        public string Decode(byte[] aData)
        {
            string retMsg = null;
            byte[] bConvert = new byte[2];
            int pos = 0;
            try
            {
                #region Process Packet header
                if ((retMsg = mCurrentClock.Decode(aData, pos)) != null)
                {
                    // most likely this is an invalid clock (i.e. we haven't
                    // acquired GPS as yet). We will skip decoding the rest
                    // of the packet except for the Engine info which, as it
                    // is not derived from GPS, may be of some value
                    pos += GPClock.Length + GPClock.Length + GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(aData, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    pos += GPDistance.Length;
                }
                else
                {
                    pos += GPClock.Length;
                    if ((retMsg = mFixClock.Decode(aData, pos)) != null)
                    {
                        //return retMsg;
                    }
                    pos += GPClock.Length;
                    if ((retMsg = mFix.Decode(aData, pos)) != null) return retMsg;
                    pos += GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(aData, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    if ((retMsg = mDistance.Decode(aData, pos)) != null) return retMsg;
                    pos += GPDistance.Length;
                }
                #endregion
                _packetVer = aData[pos++];
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _drivingTime = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _idleTime = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _excessiveIdleTime = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _ignitionOnTime = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _warmUpTime = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _brakeCount = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _brakeRoll = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _hardBrakingCount = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _hardCorneringCount = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _hardAccelerationCount = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _sustainedGForceEvents = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _accidentBufferEvents = BitConverter.ToUInt16(bConvert, 0);
                bConvert[0] = aData[pos++];
                bConvert[1] = aData[pos++];
                _brakeOnTime = BitConverter.ToUInt16(bConvert, 0);
            }
            catch (Exception ex)
            {
                retMsg = ex.Message;
            }
            return retMsg;
        }
        #endregion

        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                #region Base Packet Data
                builder.Append(base.ToDisplayString());
                if (mStatus != null)
                {
                    builder.Append(mStatus.ToString());
                }
                builder.Append("\r\nTimes : ");
                builder.Append("\r\n    Server Time : " + DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
                if (mCurrentClock != null)
                {
                    if (mCurrentClock.ToString() == " ")
                        builder.Append("\r\n    Device Time : No Value");
                    else
                    {
                        builder.Append("\r\n    Device Time : " + mCurrentClock.ToString());
                        try
                        {
                            builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                        }
                        catch (System.Exception)
                        {
                        }
                    }
                }
                else
                {
                    builder.Append("\r\n    Device Time : No Value");
                }
                if (mFixClock != null)
                {
                    if (mFixClock.ToString() == " ")
                        builder.Append("\r\n    GPS Time : No Value");
                    else
                    {
                        builder.Append("\r\n    GPS Time : " + mFixClock.ToString());
                        try
                        {
                            builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                        }
                        catch (System.Exception)
                        {
                        }
                    }
                }
                else
                {
                    builder.Append("\r\n    GPS Time : No Value");
                }
                if (mFix != null)
                {
                    builder.Append(mFix.ToString());
                }
                if (mDistance != null)
                {
                    builder.Append(mDistance.ToString());
                }
                #endregion
                builder.Append("\r\nDriver Performance Metrics :");
                builder.Append("\r\n    Packet Version : "); builder.Append(_packetVer);
                builder.Append("\r\n    Driving Time: "); builder.Append(_drivingTime);
                builder.Append("\r\n    Idle Time : "); builder.Append(_idleTime);
                builder.Append("\r\n    Excessive Idle Time : "); builder.Append(_excessiveIdleTime);
                builder.Append("\r\n    Ignition On Time : "); builder.Append(_ignitionOnTime);
                builder.Append("\r\n    Warm Up Time : "); builder.Append(_warmUpTime);
                builder.Append("\r\n    Brake Hit Count : "); builder.Append(_brakeCount);
                builder.Append("\r\n    Brake Roll : "); builder.Append(_brakeRoll);
                builder.Append("\r\n    Hard Braking Count : "); builder.Append(_hardBrakingCount);
                builder.Append("\r\n    Hard Cornering Count : "); builder.Append(_hardCorneringCount);
                builder.Append("\r\n    Hard Acceleration Count : "); builder.Append(_hardAccelerationCount);
                builder.Append("\r\n    Sustained G-Force Events : "); builder.Append(_sustainedGForceEvents);
                builder.Append("\r\n    Accident Buffer Events : "); builder.Append(_accidentBufferEvents);
                builder.Append("\r\n    Brake On Time : "); builder.Append(_brakeOnTime);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError in Driver Performance Metrics : " + ex.Message);
            }
            return builder.ToString();
        }
    }
}

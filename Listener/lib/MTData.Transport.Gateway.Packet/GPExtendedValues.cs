using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// holds a set of generic 16-bit values
	/// </summary>
    [Serializable]
    public class GPExtendedValues
	{
		public int iValue1;
		public int iValue2;
		public int iValue3;
		public int iValue4;

		public const int Length = 8;

		public static readonly string BlankDatabaseString = "0%0%0%0";

		public GPExtendedValues()
		{
			iValue1 = 0;
			iValue2 = 0;
			iValue3 = 0;
			iValue4 = 0;
		}

		public GPExtendedValues CreateCopy()
		{
			GPExtendedValues oExtendedValue = new GPExtendedValues();
			oExtendedValue.iValue1 = this.iValue1;
			oExtendedValue.iValue2 = this.iValue2;
			oExtendedValue.iValue3 = this.iValue3;
			oExtendedValue.iValue4 = this.iValue4;
			return oExtendedValue;
		}

		public string Decode(byte[] aData)
		{
			return Decode(aData, 0);
		}

		public string Decode(byte[] aData, int aStartPos)
		{
			// Ensure we've got a decent array:
			if (aData.Length < (aStartPos + Length)) return "Extended Data field too short";

			
			iValue1 = aData[aStartPos++];
			iValue1 += (aData[aStartPos++] << 8);
			iValue1 = (short) iValue1;

			iValue2 = aData[aStartPos++];
			iValue2 += (aData[aStartPos++] << 8);
			iValue2 = (short) iValue2;

			iValue3 = aData[aStartPos++];
			iValue3 += (aData[aStartPos++] << 8);
			iValue3 = (short) iValue3;

			iValue4 = aData[aStartPos++];
			iValue4 += (aData[aStartPos++] << 8);
			iValue4 = (short) iValue4;

			return null;
		}

		public void Encode(ref byte[] theData, int aStartPos)
		{
            byte[] bTemp = null;
			// Ensure we've got a decent array:
			if (theData.Length >= (aStartPos + Length))
			{
                bTemp = BitConverter.GetBytes(iValue1);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(iValue2);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(iValue3);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
                bTemp = BitConverter.GetBytes(iValue4);
                theData[aStartPos++] = bTemp[0];
                theData[aStartPos++] = bTemp[1];
			}
		}

		public string ToDatabaseFormatString()
		{		
			return iValue1 + "%" + iValue2 + "%" + iValue3 + "%" + iValue4;
		}

		public string ToDisplayString()
		{
			return	this.ToString();
		}
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nExtended Values :"); 
				builder.Append("\r\n    Value 1 : "); builder.Append(iValue1);
				builder.Append("\r\n    Value 2 : "); builder.Append(iValue2);
				builder.Append("\r\n    Value 3 : "); builder.Append(iValue3);
				builder.Append("\r\n    Value 4 : "); builder.Append(iValue4);
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding RefrigerationZoneInfo : " + ex.Message);
			}
			return builder.ToString();
		}


	}
}

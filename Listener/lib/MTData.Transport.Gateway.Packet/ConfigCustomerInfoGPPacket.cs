using System;
using System.Text;
using System.Net;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigNetworkInfoGPPacket.
	/// </summary>
    [Serializable]
    public class ConfigCustomerInfoGPPacket : GatewayProtocolPacket
	{
		#region Variables
		public uint uModelNo;
		public ulong lSerialNumber;
		public ulong lMDTSerialNumber;
		public string sSimCardNumber;
		public byte cMTDataInfoSent;
		public string sDistributorsName;
		public string sDistributorsCity;
		public string sDistributorsCountry;

		public string sCustomersName;
		public string sCustomersAddress;
		public string sCustomersCity;
		public string sCustomersCountry;

		public string sWarrantyStart;
		public string sWarrantyEnd;
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
		#endregion
		public const Byte CONFIG_CUSTOMER_INFO = 0xd8;

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\nuModelNo : "); builder.Append(uModelNo);
			builder.Append("\r\nlSerialNumber : "); builder.Append(lSerialNumber);
			builder.Append("\r\nlMDTSerialNumber : "); builder.Append(lMDTSerialNumber);
			builder.Append("\r\nsSimCardNumber : "); builder.Append(sSimCardNumber);
			builder.Append("\r\ncMTDataInfoSent : "); builder.Append(cMTDataInfoSent);
			builder.Append("\r\nsDistributorsName : "); builder.Append(sDistributorsName);
			builder.Append("\r\nsDistributorsCity : "); builder.Append(sDistributorsCity);
			builder.Append("\r\nsDistributorsCountry : "); builder.Append(sDistributorsCountry);
			builder.Append("\r\nsCustomersName : "); builder.Append(sCustomersName);
			builder.Append("\r\nsCustomersAddress : "); builder.Append(sCustomersAddress);
			builder.Append("\r\nsCustomersCity : "); builder.Append(sCustomersCity);
			builder.Append("\r\nsCustomersCountry : "); builder.Append(sCustomersCountry);
			builder.Append("\r\nsWarrantyStart : "); builder.Append(sWarrantyStart);
			builder.Append("\r\nsWarrantyEnd : "); builder.Append(sWarrantyEnd);

			return builder.ToString();
		}

		public ConfigCustomerInfoGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			cMsgType = CONFIG_CUSTOMER_INFO;
		}

		public ConfigCustomerInfoGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			cMsgType = CONFIG_CUSTOMER_INFO;
		}

		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0, checksum = 0;

			// Make sure we have sufficient space:
			iLength = 220;
			if (mDataField.Length != iLength)
			{
				mDataField = new Byte[iLength];
			}

			mDataField[encPos++] = GSP_SOH;
			mDataField[encPos++] = (Byte) ((uModelNo & 0xFF));
			mDataField[encPos++] = (Byte) ((uModelNo & 0xFF00) >> 8);
			
			#region Serial Numbers:
			mDataField[encPos++] = (Byte) ((lSerialNumber & 0xFF));
			mDataField[encPos++] = (Byte) ((lSerialNumber & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lSerialNumber & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lSerialNumber & 0xFF000000) >> 24);

			mDataField[encPos++] = (Byte) ((lMDTSerialNumber & 0xFF));
			mDataField[encPos++] = (Byte) ((lMDTSerialNumber & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((lMDTSerialNumber & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((lMDTSerialNumber & 0xFF000000) >> 24);

			if (sSimCardNumber != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sSimCardNumber, 
					0, 
					sSimCardNumber.Length,
					mDataField,
					encPos);
			}
			encPos += 6;	// The SimCardNumber can be up to 6 chars in length
			#endregion

			mDataField[encPos++] = cMTDataInfoSent;

			#region Dist Name
			if (sDistributorsName != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sDistributorsName, 
					0, 
					sDistributorsName.Length,
					mDataField,
					encPos);
			}
			encPos += 20;	// The Dist name can be up to 20 chars in length
			#endregion
			#region Dist City
			if (sDistributorsCity != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sDistributorsCity, 
					0, 
					sDistributorsCity.Length,
					mDataField,
					encPos);
			}
			encPos += 15;	// The Dist city can be up to 20 chars in length
			#endregion
			#region Dist Country
			if (sDistributorsCountry != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sDistributorsCountry, 
					0, 
					sDistributorsCountry.Length,
					mDataField,
					encPos);
			}
			encPos += 15;	// The Dist country can be up to 20 chars in length
			#endregion

			encPos += 20;	// 20 Spare bytes

			#region Cust Name
			if (sCustomersName != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sCustomersName, 
					0, 
					sCustomersName.Length,
					mDataField,
					encPos);
			}
			encPos += 20;	// The cust name can be up to 20 chars in length
			#endregion
			#region Cust Address
			if (sCustomersAddress != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sCustomersAddress, 
					0, 
					sCustomersAddress.Length,
					mDataField,
					encPos);
			}
			encPos += 30;	// The cust addr can be up to 30 chars in length
			#endregion
			#region Cust City
			if (sCustomersCity != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sCustomersCity, 
					0, 
					sCustomersCity.Length,
					mDataField,
					encPos);
			}
			encPos += 15;	// The cust city can be up to 15 chars in length
			#endregion
			#region Cust Country
			if (sCustomersCountry != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sCustomersCountry, 
					0, 
					sCustomersCountry.Length,
					mDataField,
					encPos);
			}
			encPos += 15;	// The cust country can be up to 15 chars in length
			#endregion

			encPos += 20;	// 20 Spare bytes

			#region Warranty Start
			if (sWarrantyStart != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sWarrantyStart, 
					0, 
					sWarrantyStart.Length,
					mDataField,
					encPos);
			}
			encPos += 10;	// The warranty can be up to 10 chars in length
			#endregion
			#region Warranty End
			if (sWarrantyEnd != null)
			{
				System.Text.Encoding.ASCII.GetBytes(sWarrantyEnd, 
					0, 
					sWarrantyEnd.Length,
					mDataField,
					encPos);
			}
			encPos += 10;	// The warranty can be up to 15 chars in length
			#endregion

			encPos += 10;	// 10 Spare bytes

			// Perform an additional checksum on everything between the internal SOH
			// field, and this point.
			for (int pos = 0; pos < encPos; pos++)
			{
				checksum += mDataField[pos];
			}
			// Write in the checksum and the end-of-message
			mDataField[encPos++] = Convert.ToByte(checksum & 0xFF);
			mDataField[encPos++] = GSP_EOT; 

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
	}
}

using System;
using System.Text;
using System.Collections;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for RefrigerationInfo.
	/// </summary>
    [Serializable]
    public class RefrigerationInfo
	{
		#region Private Vars
        private int iLength = 0;
		private byte _bSubCmd = (byte) 0x00;
		private byte _bRefrigerationZone =  (byte) 0x00;
		private int _iRefrigerationFlags = 0;
		private decimal _dFuelPercentage = 0;
		private decimal _dBatteryVolts = 0;
		private int _iDigitalInputFlags = 0;
		private int _iSensorFlags = 0;
		private decimal _dHumidity = 0;
		private int _iSensor1Temperature = 0;
		private int _iSensor2Temperature = 0;
		private int _iSensor3Temperature = 0;
		private int _iSensor4Temperature = 0;
		private int _iSensor5Temperature = 0;
		private int _iSensor6Temperature = 0;
		private int _iNumberOfZones = 0;
		private RefrigerationZoneInfo[] _oRefrigerationZone = null;
		// Available data flags
		private bool bDASHumiditySensor = false;
		private bool bSensor1Available = false;
		private bool bSensor2Available = false;
		private bool bSensor3Available = false;
		private bool bSensor4Available = false;
		private bool bSensor5Available = false;
		private bool bSensor6Available = false;
		#endregion
		#region Public Properties
        public int Length { get { return iLength; } }
		public byte SubCmd{ get{ return _bSubCmd;}}
		public byte RefrigerationZone{ get{ return _bRefrigerationZone;}}
		public int RefrigerationFlags { get{ return _iRefrigerationFlags;} set { _iRefrigerationFlags = value;}}
		public decimal FuelPercentage { get{ return _dFuelPercentage;}}
		public decimal BatteryVolts { get{ return _dBatteryVolts;}}
		public bool Input1On
		{
			get
			{
				if((_iDigitalInputFlags & 0x08) == 0x08)
					return true;
				else
					return false;
			}
		}
		public bool Input2On
		{
			get
			{
				if((_iDigitalInputFlags & 0x04) == 0x04)
					return true;
				else
					return false;
			}
		}
		public bool Input3On
		{
			get
			{
				if((_iDigitalInputFlags & 0x02) == 0x02)
					return true;
				else
					return false;
			}
		}
		public bool Input4On
		{
			get
			{
				if((_iDigitalInputFlags & 0x01) == 0x01)
					return true;
				else
					return false;
			}
		}
		public int SensorsAvailable { get{ return _iSensorFlags;}}
		public decimal Humidity { get{ return _dHumidity;}}
		public float Sensor1Temperature { get{ return ((float) _iSensor1Temperature / (float) 10);}}
		public float Sensor2Temperature { get{ return ((float) _iSensor2Temperature / (float) 10);}}
		public float Sensor3Temperature { get{ return ((float) _iSensor3Temperature / (float) 10);}}
		public float Sensor4Temperature { get{ return ((float) _iSensor4Temperature / (float) 10);}}
		public float Sensor5Temperature { get{ return ((float) _iSensor5Temperature / (float) 10);}}
		public float Sensor6Temperature { get{ return ((float) _iSensor6Temperature / (float) 10);}}
		public int NumberOfZones { get{ return _iNumberOfZones;}}
		public RefrigerationZoneInfo[] RefrigerationZones { get{ return _oRefrigerationZone;}}
		#endregion

		public RefrigerationInfo(byte bSubCmd, byte bRefrigerationZone)
		{
			_bSubCmd = bSubCmd;
			_bRefrigerationZone = bRefrigerationZone;
		}

		public RefrigerationInfo(int iRefrigerationFlags, decimal dFuelPercentage, decimal dBatteryVolts, int iDigitalInputFlags, int iSensorFlags, decimal dHumidity, int iSensor1Temperature, int iSensor2Temperature, int iSensor3Temperature, int iSensor4Temperature, int iSensor5Temperature, int iSensor6Temperature, int iNumberOfZones, RefrigerationZoneInfo[] oRefrigerationZone)
		{
			_iRefrigerationFlags = iRefrigerationFlags;
			_dFuelPercentage = dFuelPercentage;
			_dBatteryVolts = dBatteryVolts;
			_iDigitalInputFlags = iDigitalInputFlags;
			_iSensorFlags = iSensorFlags;
			_dHumidity = dHumidity;
			_iSensor1Temperature = iSensor1Temperature;
			_iSensor2Temperature = iSensor2Temperature;
			_iSensor3Temperature = iSensor3Temperature;
			_iSensor4Temperature = iSensor4Temperature;
			_iSensor5Temperature = iSensor5Temperature;
			_iSensor6Temperature = iSensor5Temperature;
			_iNumberOfZones = iNumberOfZones;
			if (oRefrigerationZone != null)
			{
				_oRefrigerationZone = new RefrigerationZoneInfo[oRefrigerationZone.Length];
				for (int X = 0; X < oRefrigerationZone.Length; X++)
				{
					RefrigerationZoneInfo oZoneInfo = oRefrigerationZone[X].CreateCopy();
					_oRefrigerationZone[X] = oZoneInfo;
				}
			}
			else
				_oRefrigerationZone = null;
		}

		public RefrigerationInfo CreateCopy()
		{
			RefrigerationInfo oRet = null;
			try
			{
				oRet = new RefrigerationInfo(_iRefrigerationFlags, _dFuelPercentage, _dBatteryVolts, _iDigitalInputFlags, _iSensorFlags, _dHumidity, _iSensor1Temperature, _iSensor2Temperature, _iSensor3Temperature, _iSensor4Temperature, _iSensor5Temperature, _iSensor6Temperature, _iNumberOfZones, _oRefrigerationZone);
			}
			catch(System.Exception)
			{
			}
			return oRet;
		}

		public string Decode(ref byte[] data, ref int pos)
		{
			string sMsg = null;
			RefrigerationZoneInfo oZoneInfo = null;
			try
			{
				#region Get the refrigeration flags
				_iRefrigerationFlags = data[pos++];
				#endregion
				#region Get the fuel percentage
				_dFuelPercentage = Convert.ToDecimal((int) data[pos++]) / 2;
				#endregion
				#region Get the battery volts
				_dBatteryVolts = Convert.ToDecimal((int) data[pos++]) / 2;
				// Set a limit of 30 on the battery voltage value.
				if (_dBatteryVolts > 30)
					_dBatteryVolts = 0;
				#endregion
				#region Get the Input status
				_iDigitalInputFlags = data[pos++];
				#endregion
				#region Get the Sensor flags
				SetAvailableFlags(data[pos++]);
				if (bDASHumiditySensor)
					_dHumidity = Convert.ToDecimal((int) data[pos++]) / 2;
				if (bSensor1Available)
					_iSensor1Temperature = ReadSigned2ByteIntFromArray(ref data, ref pos);
				if (bSensor2Available)
					_iSensor2Temperature = ReadSigned2ByteIntFromArray(ref data, ref pos);
				if (bSensor3Available)
					_iSensor3Temperature = ReadSigned2ByteIntFromArray(ref data, ref pos);
				if (bSensor4Available)
					_iSensor4Temperature = ReadSigned2ByteIntFromArray(ref data, ref pos);
				if (bSensor5Available)
					_iSensor5Temperature = ReadSigned2ByteIntFromArray(ref data, ref pos);
				if (bSensor6Available)
					_iSensor6Temperature = ReadSigned2ByteIntFromArray(ref data, ref pos);
				#endregion
				#region Read the zone information
				_iNumberOfZones = data[pos++];
				if (_iNumberOfZones > 0 && _iNumberOfZones <= 3)
				{
					_oRefrigerationZone = new RefrigerationZoneInfo[_iNumberOfZones];
					for(int X = 0; X < _iNumberOfZones; X++)
					{
						oZoneInfo = new RefrigerationZoneInfo();
						sMsg = oZoneInfo.Decode(ref data, ref pos);
						if (sMsg == null)
						{
							_oRefrigerationZone[X] = oZoneInfo;
						}
						else
							break;
					}
				}
				#endregion
			}
			catch(System.Exception ex)
			{
				sMsg = "Error decoding RefrigerationInfo  values : " + ex.Message;
			}
			return sMsg;
		}

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            byte bSensorFlags = (byte)0x00;
            iLength = 0;
            RefrigerationZoneInfo oZoneInfo = null;

            bTemp = BitConverter.GetBytes(_iRefrigerationFlags);
            theData[aStartPos++] = bTemp[0];
            iLength++;
            bTemp = BitConverter.GetBytes(Convert.ToInt32(_dFuelPercentage * 2));
            theData[aStartPos++] = bTemp[0];
            iLength++;
            bTemp = BitConverter.GetBytes(Convert.ToInt32(_dBatteryVolts * 2));
            theData[aStartPos++] = bTemp[0];
            iLength++;
            bTemp = BitConverter.GetBytes(_iDigitalInputFlags);
            theData[aStartPos++] = bTemp[0];
            iLength++;

            if (_dHumidity != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 2);
                bDASHumiditySensor = true;
            }
            if (_iSensor6Temperature != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 4);
                bSensor6Available = true;
            }
            if (_iSensor5Temperature != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 8);
                bSensor5Available = true;
            }
            if (_iSensor4Temperature != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 16);
                bSensor4Available = true;
            }
            if (_iSensor3Temperature != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 32);
                bSensor3Available = true;
            }
            if (_iSensor2Temperature != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 64);
                bSensor2Available = true;
            }
            if (_iSensor1Temperature != 0)
            {
                bSensorFlags = (byte)((int)bSensorFlags + 128);
                bSensor1Available = true;
            }
            theData[aStartPos++] = bSensorFlags;
            iLength++;
            if (bDASHumiditySensor)
            {
                bTemp = BitConverter.GetBytes(Convert.ToInt32(_dHumidity * 2));
                theData[aStartPos++] = bTemp[0];
                iLength++;
            }
            if (bSensor1Available)
            {
                bTemp = BitConverter.GetBytes(_iSensor1Temperature);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
            if (bSensor2Available)
            {
                bTemp = BitConverter.GetBytes(_iSensor2Temperature);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
            if (bSensor3Available)
            {
                bTemp = BitConverter.GetBytes(_iSensor3Temperature);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
            if (bSensor4Available)
            {
                bTemp = BitConverter.GetBytes(_iSensor4Temperature);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
            if (bSensor5Available)
            {
                bTemp = BitConverter.GetBytes(_iSensor5Temperature);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
            if (bSensor6Available)
            {
                bTemp = BitConverter.GetBytes(_iSensor6Temperature);
                theData[aStartPos++] = bTemp[0];
                iLength++;
                theData[aStartPos++] = bTemp[1];
                iLength++;
            }
            if (_oRefrigerationZone != null)
            {
                if (_iNumberOfZones > 0 && _iNumberOfZones <= 3)
                {
                    theData[aStartPos++] = (byte)_oRefrigerationZone.Length;
                    iLength++;
                    for (int X = 0; X < _oRefrigerationZone.Length; X++)
                    {
                        oZoneInfo = (RefrigerationZoneInfo) _oRefrigerationZone[X];
                        oZoneInfo.Encode(ref theData, aStartPos);
                        iLength += oZoneInfo.Length;
                    }
                }
                else
                {
                    theData[aStartPos++] = (byte)0x00;
                    iLength++;
                }
            }
            else
            {
                theData[aStartPos++] = (byte) 0x00;
                iLength++;
            }
        }

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nRefrigeration Values : ");
				builder.Append("\r\n    Sub Command : "); builder.Append((int) _bSubCmd);
				builder.Append("\r\n    Refrigeration Zone : "); builder.Append((int) _bRefrigerationZone);
				builder.Append("\r\n    Refrigeration Flags : "); builder.Append(_iRefrigerationFlags);
				builder.Append("\r\n    Fuel Percentage : "); builder.Append(_dFuelPercentage);
				builder.Append("\r\n    Battery Volts : "); builder.Append(_dBatteryVolts);
				builder.Append("\r\n    Digital Inputs : ");
				builder.Append("\r\n        Input 1 : "); 
				if (this.Input1On) {builder.Append("On");} 
				else {builder.Append("Off");}
				builder.Append("\r\n        Input 2 : "); 
				if (this.Input2On) {builder.Append("On");} 
				else {builder.Append("Off");}
				builder.Append("\r\n        Input 3 : "); 
				if (this.Input3On) {builder.Append("On");} 
				else {builder.Append("Off");}
				builder.Append("\r\n        Input 4 : "); 
				if (this.Input4On) {builder.Append("On");} 
				else {builder.Append("Off");}
				builder.Append("\r\n    Available Sensors : "); builder.Append(_iSensorFlags);
				builder.Append("\r\n    Humidity : "); builder.Append(_dHumidity);
				builder.Append("\r\n    Sensor Temperature : "); 
				builder.Append("\r\n        Sensor 1 : "); builder.Append(_iSensor1Temperature);
				builder.Append("\r\n        Sensor 2 : "); builder.Append(_iSensor2Temperature);
				builder.Append("\r\n        Sensor 3 : "); builder.Append(_iSensor3Temperature);
				builder.Append("\r\n        Sensor 4 : "); builder.Append(_iSensor4Temperature);
				builder.Append("\r\n        Sensor 5 : "); builder.Append(_iSensor5Temperature);
				builder.Append("\r\n        Sensor 6 : "); builder.Append(_iSensor6Temperature);
				builder.Append("\r\n    Zone Info : "); 
				builder.Append("\r\n        Number of Zones : "); builder.Append(_iNumberOfZones);
				if (_oRefrigerationZone != null)
				{
					for (int X = 0; X < _oRefrigerationZone.Length; X++)
					{
						builder.Append("\r\n"); builder.Append(_oRefrigerationZone[X].ToString());
					}
				}
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding RefrigerationInfo : " + ex.Message);
			}
			return builder.ToString();
		}

		#region Support Functions
		private void SetAvailableFlags(byte bAvailable)
		{
			try
			{
				_iSensorFlags = (int) bAvailable;
				if ((bAvailable & 0x02) == 0x02)
					bDASHumiditySensor = true;
				if ((bAvailable & 0x04) == 0x04)
					bSensor6Available = true;
				if ((bAvailable & 0x08) == 0x08)
					bSensor5Available = true;
				if ((bAvailable & 0x10) == 0x10)
					bSensor4Available = true;
				if ((bAvailable & 0x20) == 0x20)
					bSensor3Available = true;
				if ((bAvailable & 0x40) == 0x40)
					bSensor2Available = true;
				if ((bAvailable & 0x80) == 0x80)
					bSensor1Available = true;
			}
			catch(System.Exception)
			{
				bDASHumiditySensor = false;
				bSensor1Available = false;
				bSensor2Available = false;
				bSensor3Available = false;
				bSensor4Available = false;
				bSensor5Available = false;
				bSensor6Available = false;
			}
		}

		private short ReadSigned2ByteIntFromArray(ref byte[] data, ref int pos)
		{
			short iRet = 0;
			byte[] bConvert = null;
			try
			{
				if (data.Length > pos + 2)
				{
					bConvert = new byte[2];
					bConvert[0] = data[pos++];
					bConvert[1] = data[pos++];
					iRet = BitConverter.ToInt16(bConvert, 0);
				}
			}
			catch(System.Exception)
			{
				iRet = 0;
			}
			return iRet;
		}
		#endregion
	}
}

using System;
using System.Text;
using System.Net;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Provides a convenient interface for working
	/// with the MTData Gateway System Protocol PDUs
	/// </summary>
    [Serializable]
    public class GatewayProtocolPacket
	{
		#region Message Types and constants
		// GSP = Gateway System Protocol
		public const byte GSP_APP_PING = 0x77;
		public const byte GSP_SOH = 0x01; // Start Of Header
		protected const byte GSP_SM = 0x02;
		protected const byte GSP_EM = 0x03;
		public const byte GSP_EOT = 0x04;
		protected const byte GSP_EOT2 = 0x04;
		public const byte GSP_PROTOCOL_VER = 0x01; // Start Of Header

		// Message types with trivial payloads:
		public const byte SYSTEM_PACKET_MASK	= 0xF0;
		public const byte SYSTEM_SETUP			= 0xF1;
		public const byte SYSTEM_SETPOINT		= 0xF2;
		public const byte SYSTEM_ROUTEPOINT		= 0xF3;
		public const byte SYSTEM_CONFIG			= 0xF4;
		public const byte SYSTEM_GPS_FAILURE	= 0xF5;
		public const byte SYSTEM_GPRS_FAILURE	= 0xF6;
		public const byte SYSTEM_SENDBUFFER		= 0xF7;
		public const byte SYSTEM_RECEIVEBUFFER	= 0xF8;
		public const byte SYSTEM_SMS_FAILURE	= 0xF9;
		public const byte SYSTEM_HARDWARE_FAILURE	= 0xFA;
		public const byte SYSTEM_UNIT_RESET		= 0xFB;
		public const byte SYSTEM_RESET			= 0xFE;

		// GSP imposes 12 bytes of overhead due to headers and trailers
		private const int GSP_HEADER_LENGTH = 10;
		private const int GSP_TRAILER_LENGTH = 2;
		private const int GSP_OVERHEAD = GSP_HEADER_LENGTH + GSP_TRAILER_LENGTH;

		// GPS Flags
		public const byte GPS_FLG_NTH				=	0x01;
		public const byte GPS_FLG_EAST				=	0x02;
		public const byte GPS_FLG_VALID				=	0x80;

		public const byte PACKET_FLAG_MASK				= 0xF0;
        public const byte PACKET_FLAG_ACKI = 0x80;
        public const byte PACKET_FLAG_ACK_REGARDLESS = 0x40;
        public const byte PACKET_FLAG_SEND_FLASH = 0x20;
        public const byte PACKET_FLAG_ARCHIVAL_DATA = 0x10;

		#endregion
		#region Variables
		// PDU fields - i.e. data members 
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
		public byte cMsgType;
		public byte bPacketFormat;	//
		public bool bAckImmediately;	// whether this packet needs an immediate ACK
		public bool bAckRegardless;		// if this is set, ignore any prior missing sequence numbers
		public bool bSendFlash;			// Request / Allow / Disallow Flash unloading
		public bool bReceivedSendFlash {get{ return _receivedFlashAvailable; }}			// Request / Allow / Disallow Flash unloading
		public bool bArchivalData;		// Data is not recent - don't update viewing clients
		public bool bUsingSecondaryServer;	// Data is being sent TO the device's second-choice server
		public bool bRevertToPrimaryServer;	// if set when sent from server, device should revert to first-choice server. 
		public uint iLength; // the length of mDataField
		public byte cFleetId;
		public uint iVehicleId;
		public byte cPacketNum;
		public byte cPacketTotal; 
		public byte cOurSequence;
		public byte cAckSequence;
		public byte[] mDataField;
		public uint iChecksum;
		public bool bIsPopulated;
		public IPEndPoint mSenderIP;
		public byte[] mRawBytes;
		public uint iTotalLength;
		public bool bIsFileType;	// a PDU which is a "file" and should not be downloaded repeatedly

        public string StreetName;
        public int SpeedLimit;
        public string RoadType;
        
        private RoadTypeData roadTypeData;
        #endregion

        public bool _receivedFlashAvailable = false;

	    #region Public Properties

	    public RoadTypeData RoadTypeData
	    {
	        get { return roadTypeData; }
            set { roadTypeData = value; }
	    }

	    #endregion

        #region Constructors
        public void Dispose()
		{
			cPacketNum = 1;
			cPacketTotal = 1;
			bIsPopulated = false;
			bAckImmediately = false;
			bAckRegardless = false;
			bSendFlash = false;
			_receivedFlashAvailable = false;
			bArchivalData = false;
			bUsingSecondaryServer = false;
			bRevertToPrimaryServer = false;
			mDataField = null;
			bIsFileType = false;
		}

		public GatewayProtocolPacket(string serverTime_DateFormat)
		{
			//
			// TODO: Add constructor logic here
			//
			_serverTime_DateFormat = serverTime_DateFormat;
			cPacketNum = 1;
			cPacketTotal = 1;
			bIsPopulated = false;
			bAckImmediately = false;
			bAckRegardless = false;
			bSendFlash = false;
			_receivedFlashAvailable = false;
			bArchivalData = false;
			bUsingSecondaryServer = false;
			bRevertToPrimaryServer = false;
			mDataField = null;
			bIsFileType = false;
		}
		
		public GatewayProtocolPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
		{
			int pos;

			_serverTime_DateFormat = serverTime_DateFormat;

			if (p.mRawBytes != null)
			{
				this.mRawBytes = new byte[p.mRawBytes.Length];
				for (pos = 0; pos < p.mRawBytes.Length; pos++)
				{
					this.mRawBytes[pos] = p.mRawBytes[pos];
				}
			}
			if (p.mDataField == null) this.mDataField = null;
			else
			{
				this.mDataField = new byte[p.mDataField.Length];
				for (pos = 0; pos < p.iLength; pos++)
				{
					this.mDataField[pos] = p.mDataField[pos];
				}
			}
			this.cMsgType		= p.cMsgType;
			this.iLength		= p.iLength;
			this.bAckImmediately= p.bAckImmediately;
			this.bAckRegardless = p.bAckRegardless;
			this.bSendFlash		= p.bSendFlash;
			this._receivedFlashAvailable = p._receivedFlashAvailable;
			this.bArchivalData  = p.bArchivalData;
			this.bUsingSecondaryServer = p.bUsingSecondaryServer;
			this.bRevertToPrimaryServer = p.bRevertToPrimaryServer;
			this.cFleetId		= p.cFleetId;
			this.iVehicleId		= p.iVehicleId;
			this.cPacketNum		= p.cPacketNum;
			this.cPacketTotal	= p.cPacketTotal;
			this.cOurSequence	= p.cOurSequence;
			this.cAckSequence	= p.cAckSequence;
			this.iChecksum		= p.iChecksum;
			this.bIsPopulated	= p.bIsPopulated;
			this.bIsFileType	= p.bIsFileType;
			this.mSenderIP		= p.mSenderIP;
	
		}

		#endregion
		// Just return the type - in English
		public override string ToString()
		{
			if (bIsPopulated)
				return GatewayProtocolLookup.GetMessageType(cMsgType);
			else 
				return "Empty";
		}

		public virtual GatewayProtocolPacket CreateCopy()
		{
			GatewayProtocolPacket oGPP = new GatewayProtocolPacket(_serverTime_DateFormat);
			oGPP.cMsgType = this.cMsgType;
			oGPP.bPacketFormat = this.bPacketFormat;
			oGPP.bAckImmediately = this.bAckImmediately;
			oGPP.bAckRegardless = this.bAckRegardless;
			oGPP.bSendFlash = this.bSendFlash;
			oGPP._receivedFlashAvailable = this._receivedFlashAvailable;
			oGPP.bArchivalData = this.bArchivalData;
			oGPP.bUsingSecondaryServer = this.bUsingSecondaryServer;
			oGPP.bRevertToPrimaryServer = this.bRevertToPrimaryServer;
			oGPP.iLength = this.iLength;
			oGPP.cFleetId = this.cFleetId;
			oGPP.iVehicleId = this.iVehicleId;
			oGPP.cPacketNum = this.cPacketNum;
			oGPP.cPacketTotal = this.cPacketTotal;
			oGPP.cOurSequence = this.cOurSequence;
			oGPP.cAckSequence = this.cAckSequence;
			oGPP.mDataField = this.mDataField;
			oGPP.iChecksum = this.iChecksum;
			oGPP.bIsPopulated = this.bIsPopulated;
			if(this.mSenderIP != null) {oGPP.mSenderIP = new System.Net.IPEndPoint(this.mSenderIP.Address, this.mSenderIP.Port);}
			else {oGPP.mSenderIP = new System.Net.IPEndPoint(System.Net.IPAddress.Parse("0.0.0.0"), 0);}
			oGPP.mRawBytes = this.mRawBytes;
			oGPP.iTotalLength = this.iTotalLength;
			oGPP.bIsFileType = this.bIsFileType;
			return oGPP;
		}

		/// <summary>
		/// This method will be used for display purposes, as the ToString method was already overridden for other purposes,
		/// and there is too much relying on it to modify it now.
		/// </summary>
		/// <returns></returns>
		public virtual string ToDisplayString()
		{
			if (bIsPopulated)
			{
				StringBuilder builder = new StringBuilder();
				builder.Append("Base Packet Data : ");
				builder.Append("\r\n    MsgType : ");	builder.Append(GatewayProtocolLookup.GetMessageType(cMsgType));
				builder.Append("\r\n    FleetID : "); builder.Append(cFleetId);
				builder.Append("\r\n    VehicleID : "); builder.Append(iVehicleId);
				builder.Append("\r\n    AckImmediately : "); builder.Append(bAckImmediately);
				builder.Append("\r\n    AckRegardless : "); builder.Append(bAckRegardless);
				builder.Append("\r\n    SendFlash : "); builder.Append(bSendFlash);
				builder.Append("\r\n    ReceivedFlashAvailable : "); builder.Append(_receivedFlashAvailable);
				builder.Append("\r\n    ArchivalData : "); builder.Append(bArchivalData);
				builder.Append("\r\n    Using Secondary Server : "); builder.Append(bUsingSecondaryServer);
				builder.Append("\r\n    Revert To Primary Server : "); builder.Append(bRevertToPrimaryServer);
				builder.Append("\r\n    Packet Length : "); builder.Append(iLength);
				builder.Append("\r\n    Packet Flags : "); builder.Append(cPacketNum); builder.Append(" and "); builder.Append(cPacketTotal);
				builder.Append("\r\n    OurSequence : "); builder.Append(cOurSequence);
				builder.Append("\r\n    AckSequence : "); builder.Append(cAckSequence);
				builder.Append("\r\n    Checksum : "); builder.Append(iChecksum);
				builder.Append("\r\n    Sender IP : "); builder.Append(mSenderIP);
				builder.Append("\r\n    Total Length : "); builder.Append(iTotalLength);
				builder.Append("\r\n    Is File Type : "); builder.Append(bIsFileType);
				return builder.ToString();
			}
			else 
				return "MsgType : PARSING ERROR";
		}

		// Compare everything except Sequence and Ack #s,
		// unless it's an ACK or a POSITION REPORT 
		public bool Equals(GatewayProtocolPacket p)
		{
			bool isSame = true;
			isSame &= (this.cMsgType		== p.cMsgType);
			isSame &= (this.iLength			== p.iLength);
			isSame &= (this.cFleetId		== p.cFleetId);
			isSame &= (this.iVehicleId		== p.iVehicleId);
			isSame &= (this.cPacketNum		== p.cPacketNum);
			isSame &= (this.cPacketTotal	==  p.cPacketTotal);

			if ((this.cMsgType == GeneralGPPacket.GEN_ACK) ||
				(this.cMsgType == GeneralGPPacket.STATUS_POS_REPORT))												
			{
				isSame &= (this.cAckSequence == p.cAckSequence);
				isSame &= (this.iChecksum == p.iChecksum);
			}
			return isSame;
		}
		// Returns whether we know about a valid packet,
		// and hence our data members are OK to access
		public bool IsPopulated()
		{
			return bIsPopulated;
		}

		#region Encode and Decode functions
		/// <summary>
		///  Return the encoded PDU in the internally allocated
		///  memory, with the given length
		/// </summary>
		public virtual void Encode(ref byte[] theData)
		{
			int checksum = 0, pos = 0;
			uint dataLength;
			
			if (cMsgType == GSP_APP_PING)
			{
				theData = new byte[iTotalLength + 20];
				theData[0] = GSP_APP_PING;
				theData[1] = cFleetId;
				theData[2] = Convert.ToByte((iVehicleId & 0xFF00) >> 8);
				theData[3] = Convert.ToByte(iVehicleId & 0xFF);
				mRawBytes = theData;
				return;
			}
			if (mDataField != null) iLength = (uint) mDataField.Length;
			else iLength = 0;

			// We'll need 12 extra bytes to encode this:
			dataLength = iLength + GSP_OVERHEAD;
			theData = new byte[dataLength];
			mRawBytes = theData;

			// Copy the field values which we know:
			theData[0] = GSP_SOH;
			theData[1] = cMsgType;
			theData[2] = Convert.ToByte((iLength & 0xFF00) >> 8);
			theData[3] = Convert.ToByte(iLength & 0xFF); 
			theData[4] = cFleetId;
			theData[5] = Convert.ToByte((iVehicleId & 0xFF00) >> 8);
			theData[6] = Convert.ToByte(iVehicleId & 0xFF);
			theData[7] = Convert.ToByte(((cPacketNum << 4) & 0xF0) + (cPacketTotal & 0x0F)); 
			theData[8] = cOurSequence;
			theData[9] = cAckSequence; 
			// And loop through the data, which starts at offset 10
			for (pos = 0; pos < iLength; pos++)
			{
				theData[pos+GSP_HEADER_LENGTH] = mDataField[pos];
			}
			
			// Create the checksum. Sum all bytes except
			// the final 2:
			for (pos = 0; pos < (dataLength-GSP_TRAILER_LENGTH); pos++)
			{
				checksum += theData[pos];				
			}
			// Write the checksum into the penultimate position:
			theData[dataLength - GSP_TRAILER_LENGTH] = Convert.ToByte(checksum & 0xFF);
			
			if (bAckImmediately) theData[2] |= PACKET_FLAG_ACKI;
			if (bAckRegardless)	theData[2] |= PACKET_FLAG_ACK_REGARDLESS;
			if (bSendFlash)		theData[2] |= PACKET_FLAG_SEND_FLASH;
			if (bArchivalData)	theData[2] |= PACKET_FLAG_ARCHIVAL_DATA;
			
			// Sending the ARCHIVE flag from server to device means "revert to primary server"
			if (bRevertToPrimaryServer) theData[2] |= PACKET_FLAG_ARCHIVAL_DATA; 

			// And finish the packet:
			theData[dataLength-1] = GSP_EOT;
		}

		// Returns null if successful, else an error reason
		// Decodes the FIRST packet it finds in theData
		public string Decode (byte[] theData, uint aStartPos)
		{
			// [SOP][Cmd Type][Flags][Length][FleetID][VehicleID][Packet Num][Packet Total][Our Seq][Unit Ack Seq][Data][Checksum][EOT]
			// [SOP] = 0x01
			// [Cmd Type] - 1 byte
			// [Flags] - 4 bits
			// [Length] - 12 bits
			// [FleetID] - 1 byte
			// [VehicleID] - 2 bytes
			// [Packet Num] - 4 bits
			// [Packet Total] - 4 bits
			// [Our Seq] - 1 byte
			// [Unit Ack Seq] - 1 byte
			// [Data] - Variable lenght payload
			// [Checksum] - 1 byte
			// [EOT] = 0x04

			uint checksum = 0, pos = 0;

			// Application-level PING
			if (((theData.Length == 1) || 
				(theData.Length == 4) ||
				(theData.Length == 20)) && (theData[0] == GSP_APP_PING))
			{
				cMsgType = GSP_APP_PING;

				if (theData.Length > 1)
				{
					cFleetId = theData[1];
					iVehicleId = (uint) theData[3] + (uint) (theData[2] << 8);
					//bPacketFormat = (byte) theData[4];
				}
				else
				{
					cFleetId = 0;
					iVehicleId = 0;
				}
				iLength = 0;
				iTotalLength = Convert.ToUInt32(theData.Length);
				mRawBytes = theData;
				bIsPopulated = true;
				return null;
			}

			// Check the array is a reasonable length:
			if (theData.Length < GSP_OVERHEAD) return "Packet too short";

			// Check we start and end properly:
			if (theData[aStartPos] != GSP_SOH) return "No SOH byte";

			// length is actually a 12-bit field, and we have a 4-bit flag area
			// in the MS nibble of the MS byte.
			// decode these bits now:
			bAckImmediately = ((theData[aStartPos + 2] & PACKET_FLAG_ACKI) == PACKET_FLAG_ACKI); 
			// and the accept this sequence number, regardless, bir:
			bAckRegardless = ((theData[aStartPos + 2] & PACKET_FLAG_ACK_REGARDLESS) == PACKET_FLAG_ACK_REGARDLESS);
			// And check if the other end wants to unload their flash:
			bSendFlash	= ((theData[aStartPos + 2] & PACKET_FLAG_SEND_FLASH) == PACKET_FLAG_SEND_FLASH);
			_receivedFlashAvailable = bSendFlash;
			// See if this is historical data which should be quietly inserted into the database
			// without telling viewers:
			bArchivalData = ((theData[aStartPos + 2] & PACKET_FLAG_ARCHIVAL_DATA) == PACKET_FLAG_ARCHIVAL_DATA);

			// The Secondary Server flag is actually not a bit in the packet:
			// If a device ever sends
			// a packet with ARCHIVE set but no FLASH flag (which is impossible
			// if it's unloading from Flash) it actually means the device is
			// using the Secondary Server. The Listener can then take action to
			// move the device back using the same flag. jam 4/8/04
			bUsingSecondaryServer = false;
			if (!bSendFlash  && bArchivalData)
			{	// This is the secondary server for this device
				bUsingSecondaryServer = true;
				bArchivalData = false; // the data is not actually archival :-)
			}


			// Read in the Length because it's very useful to check:
			iLength = (uint) ((theData[aStartPos + 2] & (~PACKET_FLAG_MASK)) << 8)
							+ (theData[aStartPos + 3]);
			iTotalLength = iLength + GSP_OVERHEAD;

			if (iTotalLength > (theData.Length - aStartPos))
			{
				// There is no way this packet is gonna fit
				return "Packet size mismatch";
			}

			if ((aStartPos + (iTotalLength -1)) > theData.Length)
				return "Indicated packet length is too long";

			if (theData[aStartPos + iTotalLength -1] != GSP_EOT) return "No EOT byte";

			// Read in the checksum now
			iChecksum = theData[aStartPos + iTotalLength - GSP_TRAILER_LENGTH];

			// Verify the checksum. This is time-consuming, but better to do
			// it now rather than copy over the fields and then discover a problem.
			for (pos = aStartPos; pos < (aStartPos + iTotalLength - GSP_TRAILER_LENGTH); pos++)
			{
				if ( pos == (aStartPos + 2)) 
				{	// Special case for the upper nibble of length-				
					// we ignore the packet flags therein:
					checksum += Convert.ToUInt16(theData[pos] & 0x0F);
				}
				else 
					checksum +=	theData[pos];				
			}
			// Trim off bits in excess of 255 and test:
			if (iChecksum != (checksum & 0xFF)) 
				return "Checksum error: Rxed " + iChecksum + ", calculated " + (checksum & 0xFF);


			// By now we have handled the most likely errors. Start copying over:
			cMsgType = theData[aStartPos + 1];
			cFleetId = theData[aStartPos + 4];
			iVehicleId = (uint) theData[aStartPos + 6] + (uint) (theData[aStartPos + 5] << 8);
			cPacketNum = Convert.ToByte((theData[aStartPos + 7] & 0xF0) >> 4); // Upper nibble
			cPacketTotal = Convert.ToByte(theData[aStartPos + 7] & 0x0F); // Lower nibble
			cOurSequence = theData[aStartPos + 8]; 
			cAckSequence = theData[aStartPos + 9];

			// And loop through the data, which starts at offset 10
			mDataField = new byte[iLength];
			for (pos = GSP_HEADER_LENGTH; pos < (iTotalLength - GSP_TRAILER_LENGTH); pos++)
			{
				mDataField[(pos-GSP_HEADER_LENGTH)] = theData[pos + aStartPos];
			}
	
			// Grab a copy of the raw bytes too:
			mRawBytes = new byte[iTotalLength];
			for (pos = 0; pos < iTotalLength; pos++)
			{
				mRawBytes[pos] = theData[pos + aStartPos]; 
			}
			// If we get to here, all is well.
			bIsPopulated = true;
			return null;
		}
		#endregion
	}
}

﻿using System;
using System.Text;
using System.IO;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    public enum GenericSubCommands
    {
        SpeedCat0Start = 0x00,
        SpeedCat0End = 0x01,
        SpeedCat1Start = 0x02,
        SpeedCat1End = 0x03,
        SpeedCat2Start = 0x04,
        SpeedCat2End = 0x05,
        SpeedCat3Start = 0x06,
        SpeedCat3End = 0x07,
        SeatBeltDisengagedStart = 0x08,
        SeatBeltDisengagedEnd = 0x09,
        CruiseControlEngagedStart = 0x0A,
        CruiseControlEngagedEnd = 0x0B,
        FourWDDisengagedStart = 0x0C,
        FourWDDisengagedEnd = 0x0D,
        HarshDecelerationStart = 0x0E,
        HarshDecelerationEnd = 0x0f,
        DecelerationStart = 0x10,
        DecelerationEnd = 0x11,
        HarshAccelerationStart = 0x12,
        HarshAccelerationEnd = 0x13,
        FatigueCat1Start = 0x14,
        FatigueCat1End = 0x15,
        FatigueCat2Start = 0x16,
        FatigueCat2End = 0x17,
        Tamper = 0x18,
        Generic = 0x19,
        BrakeOn = 0x1A,
        BrakeOff = 0x1B,
        PtoOn = 0x1C,
        PtoOff = 0x1D,
        SeatBeltWarning = 0x1E,
        FatigueWarning = 0x1F,
        MaxSpeedWarning = 0x20,
        MaxSpeedCat1Start = 0x21,
        MaxSpeedCat1End = 0x22,
        MaxSpeedCat2Start = 0x23,
        MaxSpeedCat2End = 0x24,
        MaxSpeedCat3Start = 0x25,
        MaxSpeedCat4End = 0x26,
        FourWDEngagedStart = 0x27,
        FourWDEngagedEnd = 0x28,
        StationaryAlertStart = 0x29,
        StationaryAlertEnd = 0x2A,
        OutOfHoursUsageStart = 0x2B,
        OutOfHoursUsageEnd = 0x2C,
        EngineOn = 0x2D,
        EngineOff = 0x2E,
        WheelsInMotionStart = 0x2F,
        WheelsInMotionStop = 0x30,
        IdleStart = 0x31,
        IdleStop = 0x32, 
        TripStart = 0x33,
        TripEnd = 0x34, 
        SeatBeltOn = 0x35,
        SeatBeltOff = 0x36,
        EBSOn = 0x37, 
        EBSOff = 0x38,
        ABSOn = 0x39,
        ABSOff = 0x3A,
        OffRoadOn = 0x3B,
        OffRoadOff = 0x3C,
        GearChange = 0x3D,
        PassengerSeatBeltOn = 0x3E,
        PassengerSeatBeltOff = 0x3F,
        ServiceOverDueOn = 0x40,
        ServiceOverDueOff = 0x41,
        GearChange1 = 0x42,
        GearChange2 = 0x43,
        GearChange3 = 0x44,
        GearChange4 = 0x45,
        GearChange5 = 0x46,
        GearChange6 = 0x47,
        GearChange7 = 0x48,
        FourWheelDriveOn = 0x49,
        FourWheelDriveOff = 0x4A,
        DoorsOpen = 0x4B,
        DoorsClosed = 0x4C,
        HandBrakeOn = 0x4D,
        HandBrakeOff = 0x4E,
        GearChange8 = 0x4F,
        GearChange9 = 0x50,
        GearChange10 = 0x51,
        GearChange11 = 0x52,
        GearChange12 = 0x53,
        GearChange13 = 0x54,
        GearChange14 = 0x55,
        GearChange15 = 0x56,
        GearChangeNeutral = 0x57,
        CruiseControlOn = 0x58,
        CruiseControlOff = 0x59,
        WipersOn = 0x5A,
        WipersOff = 0x5B,
        HeadlightsOn = 0x5C,
        HeadlightsOff = 0x5D,
        HighBeamOn = 0x5E,
        HighBeamOff = 0x5F,
        LeftIndicatorOn = 0x60,
        LeftIndicatorOff = 0x61,
        LowFuelIndicatorOn = 0x62,
        LowFuelIndicatorOff = 0x63,
        RightIndicatorOn = 0x64,
        RightIndicatorOff = 0x65,
        DvrAlarm = 0x66,
        AlarmSeatBelt = 0x67,
        DvrStatus = 0x68,
        DvrError = 0x69,
        DvrErrorDiskStatus = 0x6A,
        DvrErrorDiskRecording = 0x6B,
        DvrErrorCamerasConnected = 0x6C,
        DvrErrorComms = 0x6D,
        DvrErrorDataUsageLimit = 0x6E,
        DvrErrorUploadAttemptLimit = 0x6F,
        DvrErrorIncompatibleFirmware = 0x70,
        DvrRequest = 0x71
    }

    [Serializable]
    public class GenericPacket : GatewayProtocolPacket, IStatusHolder, IPositionHolder
    {
        public const byte GENERIC_PACKET = 0x25;
        
        #region varaibles
        private string _serverTime_DateFormat;
        public GPClock mCurrentClock;
        public GPClock mFixClock;
        public GPPositionFix mFix;
        public GPStatus mStatus;
        public GPDistance mDistance;

        public GPECMAdvancedItem[] SpnData;

        public GenericSubCommands SubCommand;

        public byte[] Payload;

        public float Speed;
        public int RuleSetId;
        public int RuleSetVersion;
        public int RuleId;
        public int LocationFileId;
        public int LocationFileVersion;
        public int LocationId;
        public string Generic;

        public int DvrDeviceId;
        public int DvrEventType;
        public int DvrEventId;

        public int DvrStatusVersion;
        public int DvrErrorCode;
        public bool DvrStatusHddOk;
        public bool DvrStatusRecording;
        public int DvrStatusVideoConnectedChannel;
        public int DvrStatusVideoRecordChannel;
        public int DvrStatusHddSize;
        public int DvrStatusHddFree;
        public bool DvrStatusConnected;
        public int DvrStatusTimeOffset;
        public string DvrStatusDeviceName;
        public string DvrStatusFirmwareVersion;
        public string DvrStatusModel;
        public string DvrStatusVideoFormat;
        public bool DvrStatusIsHealthStatus;

        /// <summary>
        /// if there is a error decoding a packet in the constructor
        /// </summary>
        public string ConstructorDecodeError;
        #endregion

        #region constructors
        public GenericPacket(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);
        }

        public GenericPacket(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
        {
            Initialise(serverTime_DateFormat);

            ConstructorDecodeError = DecodePayload();
            bIsPopulated = (ConstructorDecodeError == null);
        }

        private void Initialise(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            cMsgType = GENERIC_PACKET;
            mCurrentClock = new GPClock("Device", serverTime_DateFormat);
            mFixClock = new GPClock("GPS", serverTime_DateFormat);
            mFix = new GPPositionFix();
            mStatus = new GPStatus();
            mDistance = new GPDistance();
            Payload = null;

            ConstructorDecodeError = null;
        }
        #endregion

        #region public properties
        /// <summary>
        /// get the packet length for this packet
        /// //works out the length depending on the length of the variable fields (strings)
        /// </summary>
        public int GenericPacketLength
        {
            get
            {
                int length = Payload.Length + 7; //sub command, payload count and payload, spnCount
                //add the gps and status
                length += GPClock.Length + GPClock.Length + GPPositionFix.Length + mStatus.Length + GPDistance.Length;
                return length;
            }
        }
        public bool IsRuleBreachPacket
        {
            get 
            {
                switch (SubCommand)
                {
                    case GenericSubCommands.SpeedCat0Start:
                    case GenericSubCommands.SpeedCat0End:
                    case GenericSubCommands.SpeedCat1Start:
                    case GenericSubCommands.SpeedCat1End:
                    case GenericSubCommands.SpeedCat2Start:
                    case GenericSubCommands.SpeedCat2End:
                    case GenericSubCommands.SpeedCat3Start:
                    case GenericSubCommands.SpeedCat3End:
                    case GenericSubCommands.SeatBeltDisengagedStart:
                    case GenericSubCommands.SeatBeltDisengagedEnd:
                    case GenericSubCommands.CruiseControlEngagedStart:
                    case GenericSubCommands.CruiseControlEngagedEnd:
                    case GenericSubCommands.FourWDDisengagedStart:
                    case GenericSubCommands.FourWDDisengagedEnd:
                    case GenericSubCommands.HarshDecelerationStart:
                    case GenericSubCommands.HarshDecelerationEnd:
                    case GenericSubCommands.DecelerationStart:
                    case GenericSubCommands.DecelerationEnd:
                    case GenericSubCommands.HarshAccelerationStart:
                    case GenericSubCommands.HarshAccelerationEnd:
                    case GenericSubCommands.FatigueCat1Start:
                    case GenericSubCommands.FatigueCat1End:
                    case GenericSubCommands.FatigueCat2Start:
                    case GenericSubCommands.FatigueCat2End:
                    case GenericSubCommands.Tamper:
                    case GenericSubCommands.Generic:
                    case GenericSubCommands.SeatBeltWarning:
                    case GenericSubCommands.FatigueWarning:
                    case GenericSubCommands.MaxSpeedWarning:
                    case GenericSubCommands.MaxSpeedCat1Start:
                    case GenericSubCommands.MaxSpeedCat1End:
                    case GenericSubCommands.MaxSpeedCat2Start:
                    case GenericSubCommands.MaxSpeedCat2End:
                    case GenericSubCommands.MaxSpeedCat3Start:
                    case GenericSubCommands.MaxSpeedCat4End:
                    case GenericSubCommands.FourWDEngagedStart:
                    case GenericSubCommands.FourWDEngagedEnd:
                        return true;
                    default:
                        return false;
                }
            }
        }
        #endregion

        #region public methods
        /// <summary>
        /// Produce a user-friendly description of the details parsed
        /// </summary>
        /// <returns></returns>
        public override string ToDisplayString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(base.ToDisplayString());
            builder.Append("\r\nGeneric Packet : ");
            builder.Append("\r\n    Sub Command : "); builder.Append(SubCommand);
            builder.Append("\r\n    PayloadLength : "); builder.Append(Payload.Length);
            builder.Append("\r\n    Is Rule Breach packet: "); builder.Append(IsRuleBreachPacket);
            if (IsRuleBreachPacket)
            {
                builder.Append("\r\n    Speed : "); builder.Append(Speed);
                builder.Append("\r\n    RuleSetId : "); builder.Append(RuleSetId);
                builder.Append("\r\n    RuleSetVersion : "); builder.Append(RuleSetVersion);
                builder.Append("\r\n    RuleId : "); builder.Append(RuleId);
                builder.Append("\r\n    LocationFileId : "); builder.Append(LocationFileId);
                builder.Append("\r\n    LocationFileVersion : "); builder.Append(LocationFileVersion);
                builder.Append("\r\n    LocationId : "); builder.Append(LocationId);
                if (Generic != null)
                {
                    builder.Append("\r\n    Generic : "); builder.Append(Generic);
                }
            }

            if (SubCommand == GenericSubCommands.DvrStatus ||
                SubCommand == GenericSubCommands.DvrError ||
                SubCommand == GenericSubCommands.DvrErrorDiskStatus ||
                SubCommand == GenericSubCommands.DvrErrorDiskRecording ||
                SubCommand == GenericSubCommands.DvrErrorCamerasConnected ||
                SubCommand == GenericSubCommands.DvrErrorComms ||
                SubCommand == GenericSubCommands.DvrErrorDataUsageLimit ||
                SubCommand == GenericSubCommands.DvrErrorUploadAttemptLimit ||
                SubCommand == GenericSubCommands.DvrErrorIncompatibleFirmware ||
                SubCommand == GenericSubCommands.DvrAlarm ||
                SubCommand == GenericSubCommands.DvrRequest)
            {
                builder.Append("\r\n    DvrDeviceId : "); builder.Append(DvrDeviceId);
                builder.Append("\r\n    DvrEventType : "); builder.Append(DvrEventType);
                builder.Append("\r\n    DvrEventId : "); builder.Append(DvrEventId);
                builder.Append("\r\n    DvrStatusVersion : "); builder.Append(DvrStatusVersion);
                builder.Append("\r\n    DvrErrorCode : "); builder.Append(DvrErrorCode);
                builder.Append("\r\n    DvrStatusHddOk : "); builder.Append(DvrStatusHddOk);
                builder.Append("\r\n    DvrStatusRecording : "); builder.Append(DvrStatusRecording);
                builder.Append("\r\n    DvrStatusVideoConnectedChannel : "); builder.Append(DvrStatusVideoConnectedChannel);
                builder.Append("\r\n    DvrStatusVideoRecordChannel : "); builder.Append(DvrStatusVideoRecordChannel);
                builder.Append("\r\n    DvrStatusHddSize : "); builder.Append(DvrStatusHddSize);
                builder.Append("\r\n    DvrStatusHddFree : "); builder.Append(DvrStatusHddFree);
                builder.Append("\r\n    DvrStatusConnected : "); builder.Append(DvrStatusConnected);
                builder.Append("\r\n    DvrStatusTimeOffset : "); builder.Append(DvrStatusTimeOffset);
                builder.Append("\r\n    DvrStatusDeviceName : "); builder.Append(DvrStatusDeviceName);
                builder.Append("\r\n    DvrStatusFirmwareVersion : "); builder.Append(DvrStatusFirmwareVersion);
                builder.Append("\r\n    DvrStatusModel : "); builder.Append(DvrStatusModel);
                builder.Append("\r\n    DvrStatusVideoFormat : "); builder.Append(DvrStatusVideoFormat);
                builder.Append("\r\n    DvrStatusIsHealthStatus : "); builder.Append(DvrStatusIsHealthStatus);
            }

            if (mStatus != null)
            {
                builder.Append(mStatus.ToString());
            }
            builder.Append("\r\nTimes : ");
            if (mCurrentClock != null)
            {
                if (mCurrentClock.ToString() == " ")
                    builder.Append("\r\n    Device Time : No Value");
                else
                {
                    builder.Append("\r\n    Device Time : " + mCurrentClock.ToString());
                    try
                    {
                        builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                builder.Append("\r\n    Device Time : No Value");
            }
            if (mFixClock != null)
            {
                if (mFixClock.ToString() == " ")
                    builder.Append("\r\n    GPS Time : No Value");
                else
                {
                    builder.Append("\r\n    GPS Time : " + mFixClock.ToString());
                    try
                    {
                        builder.Append(" (Local Time : " + mFixClock.ToDateTime().ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss") + ")");
                    }
                    catch (System.Exception)
                    {
                    }
                }
            }
            else
            {
                builder.Append("\r\n    GPS Time : No Value");
            }
            if (mFix != null)
            {
                builder.Append(mFix.ToString());
            }
            if (mDistance != null)
            {
                builder.Append(mDistance.ToString());
            }
            return builder.ToString();
        }

        public new GenericPacket CreateCopy()
        {
            GenericPacket oGP = new GenericPacket(this, _serverTime_DateFormat);

            if (mCurrentClock != null)
            {
                oGP.mCurrentClock = mCurrentClock.CreateCopy();
            }
            else
            {
                oGP.mCurrentClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFixClock != null)
            {
                oGP.mFixClock = mFixClock.CreateCopy();
            }
            else
            {
                oGP.mFixClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFix != null)
            {
                oGP.mFix = mFix.CreateCopy();
            }
            else
            {
                oGP.mFix = new GPPositionFix();
            }
            if (mStatus != null)
            {
                oGP.mStatus = mStatus.CreateCopy();
            }
            else
            {
                oGP.mStatus = new GPStatus();
            }
            if (mDistance != null)
            {
                oGP.mDistance = mDistance.CreateCopy();
            }
            else
            {
                oGP.mDistance = new GPDistance();
            }

            oGP.Payload = Payload;
            oGP.Speed = Speed;
            oGP.RuleId = RuleId;
            oGP.RuleSetId = RuleSetId;
            oGP.RuleSetVersion = RuleSetVersion;
            oGP.LocationFileId = LocationFileId;
            oGP.LocationFileVersion = LocationFileVersion;
            oGP.LocationId = LocationId;
            oGP.Generic = Generic;

            oGP.DvrDeviceId = DvrDeviceId;
            oGP.DvrEventType = DvrEventType;
            oGP.DvrEventId = DvrEventId;
            oGP.DvrStatusVersion = DvrStatusVersion;
            oGP.DvrErrorCode = DvrErrorCode;
            oGP.DvrStatusHddOk = DvrStatusHddOk;
            oGP.DvrStatusRecording = DvrStatusRecording;
            oGP.DvrStatusVideoConnectedChannel = DvrStatusVideoConnectedChannel;
            oGP.DvrStatusVideoRecordChannel = DvrStatusVideoRecordChannel;
            oGP.DvrStatusHddSize = DvrStatusHddSize;
            oGP.DvrStatusHddFree = DvrStatusHddFree;
            oGP.DvrStatusConnected = DvrStatusConnected;
            oGP.DvrStatusTimeOffset = DvrStatusTimeOffset;
            oGP.DvrStatusDeviceName = DvrStatusDeviceName;
            oGP.DvrStatusFirmwareVersion = DvrStatusFirmwareVersion;
            oGP.DvrStatusModel = DvrStatusModel;
            oGP.DvrStatusVideoFormat = DvrStatusVideoFormat;
            oGP.DvrStatusIsHealthStatus = DvrStatusIsHealthStatus;

            return oGP;
        }
        public GeneralGPPacket CreateGeneralGPPacket()
        {
            GeneralGPPacket p = new GeneralGPPacket(_serverTime_DateFormat);
            p.Decode(this.mRawBytes);
            p.cMsgType = cMsgType;
            p.bPacketFormat = bPacketFormat;
            p.cFleetId = cFleetId;
            p.iVehicleId = iVehicleId;
            p.StreetName = this.StreetName;
            p.SpeedLimit = this.SpeedLimit;
            p.RoadType = this.RoadType;
            if (mCurrentClock != null)
            {
                p.mCurrentClock = mCurrentClock.CreateCopy();
            }
            else
            {
                p.mCurrentClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFixClock != null)
            {
                p.mFixClock = mFixClock.CreateCopy();
            }
            else
            {
                p.mFixClock = new GPClock("", _serverTime_DateFormat);
            }
            if (mFix != null)
            {
                p.mFix = mFix.CreateCopy();
            }
            else
            {
                p.mFix = new GPPositionFix();
            }
            if (mStatus != null)
            {
                p.mStatus = mStatus.CreateCopy();
            }
            else
            {
                p.mStatus = new GPStatus();
            }
            if (mDistance != null)
            {
                p.mDistance = mDistance.CreateCopy();
            }
            else
            {
                p.mDistance = new GPDistance();
            }

            //if speed is set add ECM advanced 
			if (Speed > 0)
			{
	            double gpsSpeed = (Speed * 1.852);
	            // Get the raw bytes as a 4 byte int (km/h * 100)
	            byte[] bytes = BitConverter.GetBytes(Convert.ToInt32(Math.Round(gpsSpeed * 100)));
	            GPECMAdvancedItem item = new GPECMAdvancedItem();
	            item.SPN_ID = 5;
	            item.SourceType = GPECMAdvancedItem.SourceTypes.Common;
	            item.DerivedValue = gpsSpeed;
	            item.RawData = bytes;
	            p.mECMAdvanced.Add(item);
			}
            if (SpnData != null)
            {
                foreach (GPECMAdvancedItem spn in SpnData)
                {
                    p.mECMAdvanced.Add(spn);
                }
            }
            return p;
        }
        public void UpdateFromGeneralGPPacket(GeneralGPPacket gp)
        {
            cFleetId = gp.cFleetId;
            iVehicleId = gp.iVehicleId;
            mCurrentClock = gp.mCurrentClock.CreateCopy();
            mFixClock = gp.mFixClock.CreateCopy();
            mFix = gp.mFix.CreateCopy();
            mStatus = gp.mStatus.CreateCopy();
            mDistance = gp.mDistance.CreateCopy();
            Payload = new byte[0];
        }
        public void AddSeconds(int seconds)
        {
            DateTime dtTemp = mFixClock.ToDateTime();
            dtTemp = dtTemp.AddSeconds(seconds);
            mFixClock.FromDateTime(dtTemp);
            dtTemp = mCurrentClock.ToDateTime();
            dtTemp = dtTemp.AddSeconds(seconds);
            mCurrentClock.FromDateTime(dtTemp);
        }

        public new void Encode(ref Byte[] theData)
        {
            int pos = 0;

            // Get the Payload contents (must occur before setting the mDataField length)
            Payload = EncodePayload();

            // Workout the length of the packet
            mDataField = new Byte[GenericPacketLength];

            mCurrentClock.Encode(ref mDataField, pos);
            pos += GPClock.Length;

            mFixClock.Encode(ref mDataField, pos);
            pos += GPClock.Length;

            mFix.Encode(ref mDataField, pos);
            pos += GPPositionFix.Length;

            mStatus.Encode(ref mDataField, pos);
            pos += mStatus.Length;

            mDistance.Encode(ref mDataField, pos);
            pos += GPDistance.Length;

            WriteInt(pos, (int)SubCommand);
            pos += 4;
            WriteShort(pos, Payload.Length);
            pos += 2;
            Array.Copy(Payload, 0, mDataField, pos, Payload.Length);
            pos += Payload.Length;

            if (SpnData != null)
            {
                MemoryStream local = new MemoryStream();
                local.WriteByte((byte)SpnData.Length);
                foreach (GPECMAdvancedItem spn in SpnData)
                {
                    spn.Encode(local);
                }
                byte[] b = local.ToArray();
                //create a new datafield and copy everything into it
                byte[] temp = new byte[pos + b.Length];
                Array.Copy(mDataField, 0, temp, 0, pos);
                Array.Copy(b, 0, temp, pos, b.Length);
                mDataField = temp;
                pos += b.Length;
            }
            else
            {
                //write zero spn data
                mDataField[pos++] = 0x00;
            }

            // And wrap it in the standard overhead bytes
            base.Encode(ref theData);
        }

        public string Decode(Byte[] theData)
        {
            string problem = null;
            if (!bIsPopulated) problem = base.Decode(theData, 0);
            if (problem != null) return problem;

            //decode payload
            problem = DecodePayload();

            return problem;
        }

        /// <summary>
        /// Set the sub command based on the DVR error code
        /// </summary>
        /// <param name="errorCode">DVR error code</param>
        public void ParseDvrErrorCode(int errorCode)
        {
            // As the error code is a bit flag and we can only assign to a single sub command the sub command will be assigned by highest priority first
            if ((errorCode & 8) == 8)
            {
                SubCommand = GenericSubCommands.DvrErrorComms;
            }
            else if ((errorCode & 1) == 1)
            {
                SubCommand = GenericSubCommands.DvrErrorDiskStatus;
            }
            else if ((errorCode & 2) == 2)
            {
                SubCommand = GenericSubCommands.DvrErrorDiskRecording;
            }
            else if ((errorCode & 4) == 4)
            {
                SubCommand = GenericSubCommands.DvrErrorCamerasConnected;
            }
            else if ((errorCode & 16) == 16)
            {
                SubCommand = GenericSubCommands.DvrErrorIncompatibleFirmware;
            }
            else if ((errorCode & 8192) == 8192)
            {
                SubCommand = GenericSubCommands.DvrErrorDataUsageLimit;
            }
            else if ((errorCode & 16384) == 16384)
            {
                SubCommand = GenericSubCommands.DvrErrorUploadAttemptLimit;
            }
            else
            {
                // Default to DvrError for an unknown error code
                SubCommand = GenericSubCommands.DvrError;
            }
        }

        private string DecodePayload()
        {
            try
            {
                // Work through the packet
                int pos = 0;

                string retMsg = null;
                if ((retMsg = mCurrentClock.Decode(mDataField, pos)) != null)
                {
                    // most likely this is an invalid clock (i.e. we haven't
                    // acquired GPS as yet). We will skip decoding the rest
                    // of the packet except for the Mass Declaration which, as it
                    // is not derived from GPS, may be of some value
                    pos += GPClock.Length + GPClock.Length + GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(mDataField, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    pos += GPDistance.Length;
                }
                else
                {
                    pos += GPClock.Length;
                    mFixClock.Decode(mDataField, pos);
                    pos += GPClock.Length;
                    if ((retMsg = mFix.Decode(mDataField, pos)) != null) return retMsg;
                    pos += GPPositionFix.Length;
                    if ((retMsg = mStatus.Decode(mDataField, pos)) != null) return retMsg;
                    pos += mStatus.Length;
                    if ((retMsg = mDistance.Decode(mDataField, pos)) != null) return retMsg;
                    pos += GPDistance.Length;
                }

                SubCommand = (GenericSubCommands)(ReadInt(mDataField, pos));
                pos += 4;
                int len = ReadShort(mDataField, pos);
                pos += 2;
                Payload = new byte[len];
                Array.Copy(mDataField, pos, Payload, 0, len);
                pos += len;

                //speed is always sent on every packet
                Speed = ReadFloat(Payload, 0);
                if (IsRuleBreachPacket)
                {
                    //decode rule breach data
                    RuleSetId = ReadInt(Payload, 4);
                    RuleSetVersion = ReadInt(Payload, 8);
                    RuleId = ReadInt(Payload, 12);
                    LocationFileId = ReadInt(Payload, 16);
                    LocationFileVersion = ReadInt(Payload, 20);
                    LocationId = ReadInt(Payload, 24);
                    if (SubCommand == GenericSubCommands.Generic)
                    {
                        int payloadPosition = 28;
                        Generic = ReadString(Payload, ref payloadPosition, 0x12);

                        if (Generic.StartsWith("\0"))
                        {
                            Generic = string.Empty;
                        }
                    }
                    else
                    {
                        Generic = null;
                    }
                }
                else if (SubCommand == GenericSubCommands.DvrAlarm || SubCommand == GenericSubCommands.DvrRequest)
                {
                    DvrDeviceId = ReadInt(Payload, 4);
                    DvrEventType = ReadInt(Payload, 8);
                    DvrEventId = ReadInt(Payload, 12);
                }
                else if (SubCommand == GenericSubCommands.DvrStatus ||
                         SubCommand == GenericSubCommands.DvrError ||
                         SubCommand == GenericSubCommands.DvrErrorDiskStatus ||
                         SubCommand == GenericSubCommands.DvrErrorDiskRecording ||
                         SubCommand == GenericSubCommands.DvrErrorCamerasConnected ||
                         SubCommand == GenericSubCommands.DvrErrorComms ||
                         SubCommand == GenericSubCommands.DvrErrorDataUsageLimit ||
                         SubCommand == GenericSubCommands.DvrErrorUploadAttemptLimit ||
                         SubCommand == GenericSubCommands.DvrErrorIncompatibleFirmware)
                {
                    DvrStatusVersion = ReadShort(Payload, 4);

                    if (DvrStatusVersion == -1)
                    {
                        // Indicates we are dealing with a error entry
                        DvrErrorCode = ReadShort(Payload, 6);
                    }
                    else if (DvrStatusVersion == 1)
                    {
                        // Indicates whether the status is from tracking or a health status with associated still images
                        DvrStatusIsHealthStatus = false;

                        DvrErrorCode = ReadShort(Payload, 6);
                        DvrStatusHddOk = !ReadBool(Payload, 8);
                        DvrStatusRecording = ReadBool(Payload, 9);
                        DvrStatusVideoConnectedChannel = ReadShort(Payload, 10);
                        DvrStatusVideoRecordChannel = ReadShort(Payload, 12);
                        DvrStatusHddSize = ReadShort(Payload, 14);
                        DvrStatusHddFree = ReadShort(Payload, 16);
                        DvrStatusConnected = !ReadBool(Payload, 18);
                        DvrStatusTimeOffset = ReadInt(Payload, 19);

                        // Decode the 0 terminated strings
                        int dvrPos = 23;
                        DvrStatusDeviceName = ReadString(Payload, ref dvrPos, 0);
                        DvrStatusFirmwareVersion = ReadString(Payload, ref dvrPos, 0);
                        DvrStatusModel = ReadString(Payload, ref dvrPos, 0);
                        DvrStatusVideoFormat = ReadString(Payload, ref dvrPos, 0);
                    }
                }

                // Decode the SPN Data
                try
                {
                    if (mDataField.Length > pos)
                    {
                        int count = (int)mDataField[pos];
                        SpnData = new GPECMAdvancedItem[count];
                        for (int i = 0; i < count; i++)
                        {
                            SpnData[i] = new GPECMAdvancedItem(mDataField, ref pos);
                        }
                    }
                }
                catch { }
            }
            catch
            {
                if (Payload == null)
                {
                    Payload = new byte[0];
                }
            }
            return null;
        }

        /// <summary>
        /// Encode the payload into a byte array
        /// </summary>
        private byte[] EncodePayload()
        {
            try
            {
                using (MemoryStream stream = new MemoryStream())
                {
                    // Speed is always sent
                    WriteFloat(stream, Speed);

                    if (IsRuleBreachPacket)
                    {
                        PacketUtilities.WriteToStream(stream, RuleSetId);
                        PacketUtilities.WriteToStream(stream, RuleSetVersion);
                        PacketUtilities.WriteToStream(stream, RuleId);
                        PacketUtilities.WriteToStream(stream, LocationFileId);
                        PacketUtilities.WriteToStream(stream, LocationFileVersion);
                        PacketUtilities.WriteToStream(stream, LocationId);

                        if (SubCommand == GenericSubCommands.Generic)
                        {
                            PacketUtilities.WriteToStream(stream, 0x12, Generic);
                        }
                        else
                        {
                            PacketUtilities.WriteToStream(stream, 0x12, null);
                        }
                    }
                    else if (SubCommand == GenericSubCommands.DvrAlarm || SubCommand == GenericSubCommands.DvrRequest)
                    {
                        PacketUtilities.WriteToStream(stream, DvrDeviceId);
                        PacketUtilities.WriteToStream(stream, DvrEventType);
                        PacketUtilities.WriteToStream(stream, DvrEventId);
                    }
                    else if (SubCommand == GenericSubCommands.DvrStatus ||
                             SubCommand == GenericSubCommands.DvrError ||
                             SubCommand == GenericSubCommands.DvrErrorDiskStatus ||
                             SubCommand == GenericSubCommands.DvrErrorDiskRecording ||
                             SubCommand == GenericSubCommands.DvrErrorCamerasConnected ||
                             SubCommand == GenericSubCommands.DvrErrorComms ||
                             SubCommand == GenericSubCommands.DvrErrorDataUsageLimit ||
                             SubCommand == GenericSubCommands.DvrErrorUploadAttemptLimit ||
                             SubCommand == GenericSubCommands.DvrErrorIncompatibleFirmware)
                    {
                        PacketUtilities.WriteToStream(stream, DvrStatusVersion);

                        if (DvrStatusVersion == -1)
                        {
                            PacketUtilities.WriteToStream(stream, DvrErrorCode);
                        }
                        else if (DvrStatusVersion == 1)
                        {
                            PacketUtilities.WriteToStream(stream, DvrErrorCode);
                            PacketUtilities.WriteToStream(stream, !DvrStatusHddOk);

                            PacketUtilities.WriteToStream(stream, DvrStatusRecording);
                            PacketUtilities.WriteToStream(stream, DvrStatusVideoConnectedChannel);
                            PacketUtilities.WriteToStream(stream, DvrStatusVideoRecordChannel);

                            PacketUtilities.WriteToStream(stream, DvrStatusHddSize);
                            PacketUtilities.WriteToStream(stream, DvrStatusHddFree);
                            PacketUtilities.WriteToStream(stream, !DvrStatusConnected);

                            PacketUtilities.WriteToStream(stream, DvrStatusTimeOffset);

                            PacketUtilities.WriteToStream(stream, 0, DvrStatusDeviceName);
                            PacketUtilities.WriteToStream(stream, 0, DvrStatusFirmwareVersion);
                            PacketUtilities.WriteToStream(stream, 0, DvrStatusModel);
                            PacketUtilities.WriteToStream(stream, 0, DvrStatusVideoFormat);
                        }
                    }

                    return stream.ToArray();
                }
            }
            catch (Exception)
            {
                return new byte[0];
            }
        }

        private float ReadFloat(byte[] data, int pos)
        {
            int iTemp = 0;
            if (data.Length >= pos + 4)
            {
                byte[] bTemp = new byte[4];
                bTemp[0] = data[pos];
                bTemp[1] = data[pos + 1];
                bTemp[2] = data[pos + 2];
                bTemp[3] = data[pos + 3];
                iTemp = BitConverter.ToInt32(bTemp, 0);
            }

            float result = (float)iTemp / (float)100;
            return result;
        }

        private int ReadInt(byte[] data, int pos)
        {
            int iTemp = -1;
            if (data.Length >= pos + 4)
            {
                byte[] bTemp = new byte[4];
                bTemp[0] = data[pos];
                bTemp[1] = data[pos + 1];
                bTemp[2] = data[pos + 2];
                bTemp[3] = data[pos + 3];
                iTemp = BitConverter.ToInt32(bTemp, 0);
            }
            return iTemp;
        }

        private int ReadShort(byte[] data, int pos)
        {
            int iTemp = -1;
            if (data.Length >= pos + 2)
            {
                byte[] bTemp = new byte[2];
                bTemp[0] = data[pos];
                bTemp[1] = data[pos + 1];
                iTemp = BitConverter.ToInt16(bTemp, 0);
            }
            return iTemp;
        }

        private bool ReadBool(byte[] data, int pos)
        {
            return data[pos] == 0x01;
        }

        private static string ReadString(byte[] data, ref int byteIndex, byte delimiter)
        {
            int loop = 0;
            while (true)
            {
                if (data[byteIndex + loop] == delimiter)
                    break;
                loop++;
            }

            string value = System.Text.ASCIIEncoding.ASCII.GetString(data, byteIndex, loop);
            byteIndex += loop + 1; //Loop + delimiter
            return value;
        }

        /// <summary>
        /// Write a float to mDataField
        /// </summary>
        /// <param name="pos">the position in the mDataField to write the float</param>
        /// <param name="value">the value to write</param>
        private void WriteFloat(int pos, float value)
        {
            int valueInt = Convert.ToInt32(value * 100);
            byte[] temp = BitConverter.GetBytes(valueInt);
            mDataField[pos++] = (temp.Length > 0) ? temp[0] : (byte)0;
            mDataField[pos++] = (temp.Length > 1) ? temp[1] : (byte)0;
            mDataField[pos++] = (temp.Length > 2) ? temp[2] : (byte)0;
            mDataField[pos++] = (temp.Length > 3) ? temp[3] : (byte)0;
        }

        private void WriteInt(int pos, int value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            mDataField[pos++] = (temp.Length > 0) ? temp[0] : (byte)0;
            mDataField[pos++] = (temp.Length > 1) ? temp[1] : (byte)0;
            mDataField[pos++] = (temp.Length > 2) ? temp[2] : (byte)0;
            mDataField[pos++] = (temp.Length > 3) ? temp[3] : (byte)0;
        }

        private void WriteShort(int pos, int value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            mDataField[pos++] = (temp.Length > 0) ? temp[0] : (byte)0;
            mDataField[pos++] = (temp.Length > 1) ? temp[1] : (byte)0;
        }

        private void WriteFloat(Stream data, float value)
        {
            int valueInt = Convert.ToInt32(value * 100);

            byte[] temp = BitConverter.GetBytes(valueInt);
            data.Write(temp, 0, 4);
        }

        #endregion

        #region IStatusHolder Members

        public GPStatus GetStatus()
        {
            return mStatus;
        }

        #endregion

        #region IPositionHolder Members

        public GPPositionFix GetPosition()
        {
            return mFix;
        }

        public GPClock GetFixClock()
        {
            return mFixClock;
        }

        #endregion        
    }
}

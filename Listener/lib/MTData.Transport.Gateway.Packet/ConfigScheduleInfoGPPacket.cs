using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigScheduleInfoGPPacket.
	/// </summary>

    [Serializable]
    public class ConfigScheduleInfoGPPacket : GatewayProtocolPacket
	{
		#region Variables
		public byte cScheduleNumber;
		public byte cScheduleVersionMajor;
		public byte cScheduleVersionMinor;

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncScheduleNumber : "); builder.Append(cScheduleNumber);
			builder.Append("\r\ncScheduleVersionMajor : "); builder.Append(cScheduleVersionMajor);
			builder.Append("\r\ncScheduleVersionMinor : "); builder.Append(cScheduleVersionMinor);
			return builder.ToString();
		}

        [Serializable]
        public class ConfigScheduleHour
		{
			public byte cConfigNumber;
			public byte cRouteNumber;
			public byte cSetPtNumber;

			public ConfigScheduleHour()
			{
				cConfigNumber = 0;
				cRouteNumber = 0;
				cSetPtNumber = 0;
			}
			public ConfigScheduleHour(ConfigScheduleHour other)
			{
				cConfigNumber = other.cConfigNumber;
				cRouteNumber = other.cRouteNumber;
				cSetPtNumber = other.cSetPtNumber;
			}
			public ConfigScheduleHour(byte conf, byte route, byte setpt)
			{
				cConfigNumber = conf;
				cRouteNumber = route;
				cSetPtNumber = setpt;
			}
		}
		public ConfigScheduleHour[] mHourList;
		#endregion
		public const byte CONFIG_SCHEDULE_INFO = 0xd6;

		public ConfigScheduleInfoGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_SCHEDULE_INFO;
			bIsFileType = true;
			mHourList = new ConfigScheduleHour[168];
		}

		public ConfigScheduleInfoGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_SCHEDULE_INFO;
			bIsFileType = true;
			mHourList = new ConfigScheduleHour[168];
		}

		public void SetHourConfiguration(ConfigScheduleHour config, int hour)
		{
			if ((hour >= 0) && (hour <= 168))
			mHourList[hour] = new ConfigScheduleHour(config);
		}

		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0;
			int checksum = 0;

			// Make sure we have sufficient space:
			iLength = 510; // 168 hrs per week * 3 bytes, plus 4 to begin and 2 on the end
			if (mDataField.Length != iLength)
			{
				mDataField = new Byte[iLength];
			}

			mDataField[encPos++] = GSP_SOH;
			mDataField[encPos++] = cScheduleNumber;
			mDataField[encPos++] = cScheduleVersionMajor;
			mDataField[encPos++] = cScheduleVersionMinor;

			

			foreach (ConfigScheduleHour hr in mHourList)
			{
				if (hr == null)
				{
					// no entry for this hour - skip it
					encPos += 3;
				}
				else
				{
					mDataField[encPos++] = hr.cConfigNumber;
					mDataField[encPos++] = hr.cRouteNumber;
					mDataField[encPos++] = hr.cSetPtNumber;
				}
			}

			// Perform an additional checksum on everything between the internal SOH
			// field, and this point.
			for (int pos = 0; pos < encPos; pos++)
			{
				checksum += mDataField[pos];
			}
			// Write in the checksum and the end-of-message
			mDataField[encPos++] = Convert.ToByte(checksum & 0xFF);
			mDataField[encPos++] = GSP_EOT;

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
	}
}

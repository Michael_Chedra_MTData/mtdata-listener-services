using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class ConfigWatchList : GatewayProtocolPacket
    {
        public const byte CONFIG_WATCH_LIST = 0xE2;
        #region Private Member Vars
        private byte _PacketVer;
        private bool _AddToWatchList;
        private int _WatchListFlags;
        private string _serverTime_DateFormat;
        #endregion
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public int WatchListFlags
        {
            get { return _WatchListFlags; }
            set { _WatchListFlags = value; }
        }
        public bool AddToWatchList
        {
            get { return _AddToWatchList; }
            set { _AddToWatchList = value; }
        }
        #endregion
        #region Constructor and CreateCopy functions
        public ConfigWatchList(string serverTime_DateFormat)
            : base(serverTime_DateFormat)
        {
            cMsgType = CONFIG_WATCH_LIST;
            _serverTime_DateFormat = serverTime_DateFormat;
            bIsFileType = true;
            _PacketVer = 0;
            _AddToWatchList = false;
            _WatchListFlags = 0;
        }
        public ConfigWatchList(GatewayProtocolPacket p, string serverTime_DateFormat)
            : base(p, serverTime_DateFormat)
		{
            _serverTime_DateFormat = serverTime_DateFormat;
            Decode(p.mDataField);
        }
        public new ConfigWatchList CreateCopy()
        {
            ConfigWatchList oData = new ConfigWatchList(_serverTime_DateFormat);
            oData.PacketVer = _PacketVer;
            oData.AddToWatchList = _AddToWatchList;
            oData.WatchListFlags = _WatchListFlags;
            return oData;
        }
        #endregion
        #region Encoding / Decoding
        public string Decode(Byte[] aData)
        {
            MemoryStream oMS = new MemoryStream(aData, 0, aData.Length);
            return Decode(ref oMS);
        }
        public string Decode(Byte[] aData, ref int index)
        {
            MemoryStream oMS = new MemoryStream(aData, index, aData.Length - index);
            string sRet = Decode(ref oMS);
            if (sRet != null) return sRet;
            index += (int)oMS.Position;
            return null;
        }
        public string Decode(ref MemoryStream oMS)
        {
            try
            {
                byte bData = (byte) 0x00;
               PacketUtilities.ReadFromStream(oMS, ref _PacketVer);
               PacketUtilities.ReadFromStream(oMS, ref bData);
                if (bData == (byte) 0x00)
                    _AddToWatchList = false;
                else
                    _AddToWatchList = true;
               PacketUtilities.ReadFromStream(oMS, ref _WatchListFlags);
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }

        public void Encode()
        {
            EncodeDataField();
            //base.Encode(ref theData);
        }

        public void EncodeDataField()
        {
            MemoryStream oMS = new MemoryStream();
           PacketUtilities.WriteToStream(oMS, _PacketVer);
            byte bData = (byte)0x00;
            if (_AddToWatchList)
                bData = (byte) 0x01;
           PacketUtilities.WriteToStream(oMS, bData);
           PacketUtilities.WriteToStream(oMS, _WatchListFlags);
            mDataField = oMS.ToArray();
        }
        #endregion
        #region ToString functions
        public override string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nConfig Watch List Packet :");
                builder.Append("\r\n    Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n    Add To Watch List : "); builder.Append(_AddToWatchList);
                builder.Append("\r\n    Watch List Flags : "); builder.Append(_WatchListFlags);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Config Watch List Packet : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

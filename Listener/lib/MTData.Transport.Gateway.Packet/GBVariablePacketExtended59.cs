using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// This class will read the extended bytes from the end of the packet.
	/// these status bytes
	/// </summary>
    [Serializable]
    public class GBVariablePacketExtended59
	{
		#region Private Vars
		private long _iUnitStatus = 0;
		private int _iCurrentSetPointGroup = 0;
		private int _iCurrentSetPointNumber = 0;
		private int _iCurrentSetPointID = 0;				//if set override _iCurrentSetPointGroup and _iCurrentSetPointNumber, as _iCurrentSetPointID is the id of that setpoint in the t_setpoint table
		private uint _iCurrentVehicleRouteScheduleID = 0;
		private int  _iCurrentRouteCheckPointIndex = -1;
		private int _iLengthRead = 0;
		private bool _bPopulated = false;
		private byte _ExtendedType = 0x0C;

        private int _NumOfSattelites = 0x00;
        private double _HDOP = 0;
	
		//Refrigeration Values
		private RefrigerationInfo _oRefInfo = null;
		#endregion
		#region Public Properties
		public long UnitStatus { get{ return _iUnitStatus; } set{ _iUnitStatus = value; }}
        public int CurrentSetPointID { 	get { 	return _iCurrentSetPointID; } 	set {	_ExtendedType = 0x0D;	_iCurrentSetPointID = value; 	} 	}
        public int CurrentSetPointGroup { get { return _iCurrentSetPointGroup; } set { _iCurrentSetPointGroup = value; } }
        public int CurrentSetPointNumber { get { return _iCurrentSetPointNumber; } set { _iCurrentSetPointNumber = value; } }
        public uint CurrentVehicleRouteScheduleID { get { return _iCurrentVehicleRouteScheduleID; } set { _iCurrentVehicleRouteScheduleID = value; } }
        public int CurrentRouteCheckPointIndex { get { return _iCurrentRouteCheckPointIndex; } set { _iCurrentRouteCheckPointIndex = value; } }
        public int NumOfSattelites { get { return _NumOfSattelites; } set { _NumOfSattelites = value; } }
        public double HDOP { get { return _HDOP; } set { _HDOP = value; } }
		public int Length {get{ return _iLengthRead; }}
		public bool Populated {get{ return _bPopulated; } set { _bPopulated = value;}}
        public RefrigerationInfo Refrigeration { get { return _oRefInfo; } set { _oRefInfo = value; } }
		#endregion
		public GBVariablePacketExtended59()
		{
			
		}
		public GBVariablePacketExtended59 CreateCopy()
		{
			GBVariablePacketExtended59 result = new GBVariablePacketExtended59();
			result._iUnitStatus = this._iUnitStatus;
			result._iCurrentSetPointGroup = this._iCurrentSetPointGroup;
			result._iCurrentSetPointNumber = this._iCurrentSetPointNumber;
            result._iCurrentSetPointID = this._iCurrentSetPointID;
            result._iCurrentVehicleRouteScheduleID = this._iCurrentVehicleRouteScheduleID;
			result._iCurrentRouteCheckPointIndex = this._iCurrentRouteCheckPointIndex;
			result._iLengthRead = this._iLengthRead;
			result._bPopulated = this._bPopulated;
            result._ExtendedType = this._ExtendedType;
            result._NumOfSattelites = this._NumOfSattelites;
            result._HDOP = this._HDOP;
            return result;
		}

		/// <summary>
		/// Format of additional packet data
		/// 
		/// [Standard] = [Extended Field Length][Extended State Flags][Setpoint Group][Setpoint Number][RouteScheduleID][CheckPointIndex]
		/// 
		/// If [Extended Field Length] = 0x0C then
		///		[Packet Def] = [Standard]
		///	If [Extended Field Length] = 0x1C then
		///		[Packet Def] = [Standard][Refrigeration]
		/// </summary>
		/// <param name="data"></param>
		/// <param name="pos"></param>
		/// <returns></returns>
		public string Decode(byte[] data, ref int pos, byte bSubCmd, byte bRefrigerationZone)
		{
			string sMsg = null;
			byte bExtendedFieldLength = (byte) 0x00;

            // Flag definition.
            // If (bExtendedFieldLength & 0x0C) == (byte)0x0C - Setpoint group ID and setpoint number are encoded.
            // If (bExtendedFieldLength & 0x0D) == (byte)0x0D - Setpoint ID is encoded.
            // If (bExtendedFieldLength & 0x10) == (byte)0x10 - Refrigeration data is encoded.

			try
			{
				_iUnitStatus = 0;
				_iCurrentSetPointGroup = 0;
				_iCurrentSetPointNumber = 0;
                _iCurrentSetPointID = 0;
				_iCurrentVehicleRouteScheduleID = 0;
				_iCurrentRouteCheckPointIndex = 0;

				bExtendedFieldLength = data[pos++];
                if ((bExtendedFieldLength & 0x0C) == (byte)0x0C || (bExtendedFieldLength & 0x0D) == (byte)0x0D)
				{
					#region Read the extended status
					_iUnitStatus = (data[pos++]);
					_iUnitStatus += (data[pos++] << 8);
					_iUnitStatus += (data[pos++] << 16);
					_iUnitStatus += (data[pos++] << 24);
					#endregion
					#region Read the Setpoint info
                    if ((bExtendedFieldLength & 0x0D) == (byte)0x0D)
                    {
                        _iCurrentSetPointID = (data[pos++]);
                        _iCurrentSetPointID += (data[pos++] << 8);
                        _iCurrentSetPointID += (data[pos++] << 16);
                        _iCurrentSetPointID += (data[pos++] << 32);
                        _ExtendedType = 0x0D;
                    }
                    else
                    {
                        _iCurrentSetPointGroup = data[pos++];
                        _iCurrentSetPointNumber = (data[pos++]);
                        _iCurrentSetPointNumber += (data[pos++] << 8);
                    }
					#endregion
					#region Read the Routing info
					_iCurrentVehicleRouteScheduleID = ((uint)data[pos++]);
					_iCurrentVehicleRouteScheduleID += (((uint)data[pos++]) << 8);
					_iCurrentVehicleRouteScheduleID += (((uint)data[pos++]) << 16);
					_iCurrentVehicleRouteScheduleID += (((uint)data[pos++]) << 24);
					_iCurrentRouteCheckPointIndex = data[pos++];
					#endregion
                    if ((bExtendedFieldLength & 0x10) == (byte)0x10)
					{
						#region Read the refrigeration info
						_oRefInfo = new RefrigerationInfo(bSubCmd, bRefrigerationZone);
						_oRefInfo.Decode(ref data, ref pos);
						#endregion
					}
                    else
                        _oRefInfo = null;

					_bPopulated = true;
				}
			}
			catch(System.Exception ex)
			{
				sMsg = "Error decoding Extended Status values : " + ex.Message;
			}
			return sMsg;
		}

        public void Encode(ref byte[] theData, int aStartPos)
        {
            byte[] bTemp = null;
            _iLengthRead = 0;
            byte bExtendedFieldLength = (byte)0x00;


            if (_oRefInfo != null)
				bExtendedFieldLength = (byte)(_ExtendedType | 0x10);
            else
				bExtendedFieldLength = (byte)_ExtendedType;

            theData[aStartPos++] = bExtendedFieldLength;
            _iLengthRead++;
            bTemp = BitConverter.GetBytes(_iUnitStatus);
            theData[aStartPos++] = bTemp[0];
            _iLengthRead++;
            theData[aStartPos++] = bTemp[1];
            _iLengthRead++;
            theData[aStartPos++] = bTemp[2];
            _iLengthRead++;
            theData[aStartPos++] = bTemp[3];
            _iLengthRead++;
			if ((_ExtendedType & 0x0F) == 0x0C)			//setpoint group and setpoint number extended type
			{
				bTemp = BitConverter.GetBytes(_iCurrentSetPointGroup);
				theData[aStartPos++] = bTemp[0];
				_iLengthRead++;
				bTemp = BitConverter.GetBytes(_iCurrentSetPointNumber);
				theData[aStartPos++] = bTemp[0];
				_iLengthRead++;
				theData[aStartPos++] = bTemp[1];
				_iLengthRead++;
			}
			else if ((_ExtendedType & 0x0F) == 0x0D)			//waypoint id extended type
			{
				bTemp = BitConverter.GetBytes(_iCurrentSetPointID);
				theData[aStartPos++] = bTemp[0];
				_iLengthRead++;
				theData[aStartPos++] = bTemp[1];
				_iLengthRead++;
				theData[aStartPos++] = bTemp[2];
				_iLengthRead++;
				theData[aStartPos++] = bTemp[3];
				_iLengthRead++;
			}
            bTemp = BitConverter.GetBytes(_iCurrentVehicleRouteScheduleID);
            theData[aStartPos++] = bTemp[0];
            _iLengthRead++;
            theData[aStartPos++] = bTemp[1];
            _iLengthRead++;
            theData[aStartPos++] = bTemp[2];
            _iLengthRead++;
            theData[aStartPos++] = bTemp[3];
            _iLengthRead++;
            bTemp = BitConverter.GetBytes(_iCurrentRouteCheckPointIndex);
            theData[aStartPos++] = bTemp[0];
            _iLengthRead++;
            if (_oRefInfo != null && (_ExtendedType & 0x10) == 0x10)
            {
                _oRefInfo.Encode(ref theData, aStartPos);
                _iLengthRead += _oRefInfo.Length;
            }
            if ((_ExtendedType & 0x20) == 0x20)
            {
                bTemp = BitConverter.GetBytes(_NumOfSattelites);
                theData[aStartPos++] = bTemp[0];
                int iTemp = Convert.ToInt32(_HDOP * Convert.ToDouble(10));
                bTemp = BitConverter.GetBytes(iTemp);
                theData[aStartPos++] = bTemp[0];
                _iLengthRead += 2;
            }
        }

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			try
			{
				builder.Append("\r\nExtended Values : ");
				builder.Append("\r\n    Extended State : "); builder.Append(_iUnitStatus);
				builder.Append("\r\n    Setpoint Group : "); builder.Append(_iCurrentSetPointGroup);
				builder.Append("\r\n    Setpoint Number : "); builder.Append(_iCurrentSetPointNumber);
				builder.Append("\r\n    Route Schedule ID : "); builder.Append(_iCurrentVehicleRouteScheduleID);
				builder.Append("\r\n    Route Check Point Index : "); builder.Append(_iCurrentRouteCheckPointIndex);
                builder.Append("\r\n    Number of Sattelites : "); builder.Append(_NumOfSattelites);
                builder.Append("\r\n    HDOP : "); builder.Append(_HDOP);
				if(_oRefInfo != null)
					builder.Append(_oRefInfo.ToString());
			}
			catch(System.Exception ex)
			{
				builder.Append("\r\nError Decoding GBVariablePacketExtended59 : " + ex.Message);
			}
			return builder.ToString();
		}
	}
}

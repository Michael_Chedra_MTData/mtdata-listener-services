using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Schedule version advisory packet
	/// </summary>

    [Serializable]
    public class ConfigScheduleGPPacket : GatewayProtocolPacket
	{
		#region Variables
		public byte cScheduleNumber;
		public byte cScheduleVersionMajor;
		public byte cScheduleVersionMinor;

		#endregion
		public const byte CONFIG_SCHEDULE = 0xd0;

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncScheduleNumber : "); builder.Append(cScheduleNumber);
			builder.Append("\r\ncScheduleVersionMajor : "); builder.Append(cScheduleVersionMajor);
			builder.Append("\r\ncScheduleVersionMinor : "); builder.Append(cScheduleVersionMinor);
			return builder.ToString();
		}

		public ConfigScheduleGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_SCHEDULE;
			cScheduleNumber			= 0;
			cScheduleVersionMajor	= 0;
			cScheduleVersionMinor	= 0;
		}

		public ConfigScheduleGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_SCHEDULE;
			cScheduleNumber			= 0;
			cScheduleVersionMajor	= 0;
			cScheduleVersionMinor	= 0;
		}

		public new void Encode(ref Byte[] theData)
		{
			int encPos = 0;

			// Make sure we have sufficient space:
			iLength = 3; 
			if (mDataField.Length != iLength)
			{
				mDataField = new Byte[iLength];
			}
			mDataField[encPos++] = cScheduleNumber;
			mDataField[encPos++] = cScheduleVersionMajor;
			mDataField[encPos++] = cScheduleVersionMinor;

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
		public string Decode(Byte[] theData)
		{
			int decPos = 0;
			string problem = null;
			if (!bIsPopulated)	problem = base.Decode(theData, 0);
			if (problem != null) return problem;

			if (iLength != 3) 
				return "Incorrect length to decode CONFIG_SCHEDULE";
			cScheduleNumber = mDataField[decPos++];
			cScheduleVersionMajor = mDataField[decPos++];
			cScheduleVersionMinor = mDataField[decPos++];

			return problem;
		}

	}
}

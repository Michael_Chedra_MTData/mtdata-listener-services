using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class CardReaderConfiguration
    {
        public byte SlotLoc_Driver_ID;          // Max value 255
        public byte SlotLoc_Driver_Pin;         // Max value 255
        public byte SlotLoc_Driver_Name;        // Max value 255
        public byte Authentication_key_size;    // Max value of 8
        public byte[] Authentication_key;       // Max of 8 bytes

        public CardReaderConfiguration()
        {
            SlotLoc_Driver_ID = (byte)0x00;
            SlotLoc_Driver_Pin = (byte)0x00;
            SlotLoc_Driver_Name = (byte)0x00;
            Authentication_key_size = (byte)0x00;
            Authentication_key = new byte[0];
        }

        public CardReaderConfiguration(byte slotLoc_Driver_ID, byte slotLoc_Driver_Pin, byte slotLoc_Driver_Name, byte authentication_key_size, byte[] authentication_key)
        {
            try
            {
                SlotLoc_Driver_ID = slotLoc_Driver_ID;
                SlotLoc_Driver_Pin = slotLoc_Driver_Pin;
                SlotLoc_Driver_Name = slotLoc_Driver_Name;
                Authentication_key_size = authentication_key_size;
                Authentication_key = authentication_key;
            }
            catch (Exception ex)
            {
                SlotLoc_Driver_ID = (byte)0x00;
                SlotLoc_Driver_Pin = (byte)0x00;
                SlotLoc_Driver_Name = (byte)0x00;
                Authentication_key_size = (byte)0x00;
                Authentication_key = new byte[0];
                throw new Exception(this.GetType().FullName + ".CardReaderConfiguration(byte slotLoc_Driver_ID, byte slotLoc_Driver_Pin, byte slotLoc_Driver_Name, byte authentication_key_size, byte[] authentication_key)", ex);
            }
        }

        public CardReaderConfiguration(DataRow dr)
        {
            try
            {
                SlotLoc_Driver_ID = Convert.ToByte(dr["SlotLoc_Driver_ID"]);
                SlotLoc_Driver_Pin = Convert.ToByte(dr["SlotLoc_Driver_Pin"]);
                SlotLoc_Driver_Name = Convert.ToByte(dr["SlotLoc_Driver_Name"]);
                Authentication_key_size = Convert.ToByte(dr["Authentication_key_size"]);
                Authentication_key = (byte[])dr["Authentication_key"];
            }
            catch (Exception ex)
            {
                SlotLoc_Driver_ID = (byte)0x00;
                SlotLoc_Driver_Pin = (byte)0x00;
                SlotLoc_Driver_Name = (byte)0x00;
                Authentication_key_size = (byte)0x00;
                Authentication_key = new byte[0];
                throw new Exception(this.GetType().FullName + ".CardReaderConfiguration(DataRow dr)", ex);
            }
        }

        public byte[] ByteArray
        {
            get
            {
                byte[] data = new byte[12];
                try
                {
                    data[0] = SlotLoc_Driver_ID;
                    data[1] = SlotLoc_Driver_Pin;
                    data[2] = SlotLoc_Driver_Name;
                    if (Authentication_key_size > 8)
                        Authentication_key_size = 8;
                    data[3] = Authentication_key_size;
                    int iOffset = 4;
                    for (int X = 0; X < Authentication_key_size; X++)
                    {
                        if (Authentication_key.Length > X)
                            data[X + iOffset] = Authentication_key[X];
                        else
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(this.GetType().FullName + ".ByteArray (Get byte[])", ex);
                }
                return data;
            }
            set
            {
                try
                {
                    SlotLoc_Driver_ID = value[0];
                    SlotLoc_Driver_Pin = value[1];
                    SlotLoc_Driver_Name = value[2];
                    Authentication_key_size = value[3];
                    if (((int)Authentication_key_size) > 8)
                        Authentication_key_size = (int)8;
                    Authentication_key = new byte[(int)Authentication_key_size];
                    int iOffset = 4;
                    for (int X = 0; X < Authentication_key.Length; X++)
                    {
                        if (X + iOffset < value.Length)
                        {
                            Authentication_key[X] = value[X + iOffset];
                        }
                        else
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(this.GetType().FullName + ".ByteArray (Set byte[])", ex);
                }
            }
        }
        public override string ToString()
        {
            string ret = "";
            try
            {
                ret = "SlotLoc_Driver_ID = " + (int)SlotLoc_Driver_ID + ", SlotLoc_Driver_Pin = " + (int)SlotLoc_Driver_Pin + ", SlotLoc_Driver_Name = " + (int)SlotLoc_Driver_Name + ", Authentication_key_size = " + (int)Authentication_key_size + ", Authentication_key = " + (Authentication_key == null ? "null" : BitConverter.ToString(Authentication_key));
            }
            catch (Exception ex)
            {
                throw new Exception(this.GetType().FullName + ".ToString()", ex);
            }
            return ret;
        }
    }
}

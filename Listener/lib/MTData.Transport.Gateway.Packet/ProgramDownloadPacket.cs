using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ProgramDownloadPacket.
	/// </summary>
    [Serializable]
    public class ProgramDownloadPacket : GatewayProtocolPacket
	{
		public const byte DOWNLOAD_PROGRAM	= 0x8f;
		public const byte DOWNLOAD_IOBOX	= 0x8d;
		public const byte DOWNLOAD_MDT		= 0x88;
        public const byte DOWNLOAD_FILE = 0xe8;
		public const int DOWNLOAD_PROGRAM_DATA_LENGTH = 250;
		public const int DOWNLOAD_IOBOX_DATA_LENGTH = 256;
		public const int DOWNLOAD_PROGRAM_PAYLOAD_LENGTH = 
							DOWNLOAD_PROGRAM_DATA_LENGTH + 11;

		public const int DOWNLOAD_MDT_PAYLOAD_LENGTH = 
			DOWNLOAD_PROGRAM_DATA_LENGTH + 9;

		public const int DOWNLOAD_IOBOX_PAYLOAD_LENGTH = 
							DOWNLOAD_IOBOX_DATA_LENGTH + 10;

		public const int DOWNLOAD_FILE_DATA_LENGTH = 250;
		public const int DOWNLOAD_FILE_PAYLOAD_LENGTH = DOWNLOAD_FILE_DATA_LENGTH + 11;
		#region Variables
		public byte cDownloadVersion;
		public int  iDownloadPacketNumber;
		public int	iDownloadAddress;
		public int	iDownloadChecksum;
		public byte[] mDownloadData;
		#endregion

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncDownloadVersion : "); builder.Append(cDownloadVersion);
			builder.Append("\r\niDownloadPacketNumber : "); builder.Append(iDownloadPacketNumber);
			builder.Append("\r\niDownloadAddress : "); builder.Append(iDownloadAddress);
			builder.Append("\r\niDownloadChecksum : "); builder.Append(iDownloadChecksum);
			builder.Append("\r\nmDownloadData : <Too Big For Display>"); 

			return builder.ToString();
		}

		public ProgramDownloadPacket(byte cMessageType, string serverTime_DateFormat) : base (serverTime_DateFormat)
		{
			cMsgType = cMessageType;
		}

		public ProgramDownloadPacket(byte cMessageType, GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			cMsgType = cMessageType;
		}

		/// <summary>
		/// This method allows outside access to the raw datafield produced for a download
		/// </summary>
		public void PrepareDataField()
		{
			#region set up
			int encPos = 0;
			int theDataLength = 0;
			int thePayloadLength = 0;
			int startPos = 0;

			switch (cMsgType)
			{
				case DOWNLOAD_PROGRAM:
					theDataLength = DOWNLOAD_PROGRAM_DATA_LENGTH;
					thePayloadLength = DOWNLOAD_PROGRAM_PAYLOAD_LENGTH;
					if ((mDataField == null) || (mDataField.Length != DOWNLOAD_PROGRAM_PAYLOAD_LENGTH))
					{
						mDataField = new byte[thePayloadLength];	
					}
					mDataField[encPos++] = cMsgType; // spare - filled with type
					break;
				case DOWNLOAD_MDT:
					theDataLength = DOWNLOAD_PROGRAM_DATA_LENGTH;
					thePayloadLength = DOWNLOAD_MDT_PAYLOAD_LENGTH;
					if ((mDataField == null) || (mDataField.Length != DOWNLOAD_MDT_PAYLOAD_LENGTH))
					{
						mDataField = new byte[thePayloadLength];	
					}
					mDataField[encPos++] = cMsgType; // spare - filled with type
					break;
					
				case DOWNLOAD_IOBOX:
					theDataLength = DOWNLOAD_IOBOX_DATA_LENGTH;
					thePayloadLength = DOWNLOAD_IOBOX_PAYLOAD_LENGTH;
					if (mDataField.Length != DOWNLOAD_IOBOX_PAYLOAD_LENGTH)
					{
						mDataField = new byte[thePayloadLength];	
					}
					mDataField[encPos++] = (byte) ((thePayloadLength & 0xFF00) >> 8);	// 2 spare bytes - filled with length for the benefit of
					mDataField[encPos++] = (byte) ((thePayloadLength & 0xFF));			// any IO box that needs it!
					startPos = 2;
					break;
				case DOWNLOAD_FILE:
					theDataLength = DOWNLOAD_FILE_DATA_LENGTH;
					thePayloadLength = DOWNLOAD_FILE_PAYLOAD_LENGTH;
					mDataField = new byte[thePayloadLength];
                    mDataField[encPos++] = cMsgType; // spare - filled with type
					break;

				default:
					return;
			}
			
			#endregion
			
			mDataField[encPos++] = cDownloadVersion;

			#region iDownloadPacketNumber:
			mDataField[encPos++] = (byte) ((iDownloadPacketNumber & 0xFF));
			mDataField[encPos++] = (byte) ((iDownloadPacketNumber & 0xFF00) >> 8);
			#endregion
			#region iDownloadAddress (MSB -> LSB):
			mDataField[encPos++] = (byte) ((iDownloadAddress & 0xFF0000) >> 16);
			mDataField[encPos++] = (byte) ((iDownloadAddress & 0xFF00) >> 8);
			mDataField[encPos++] = (byte) ((iDownloadAddress & 0xFF));
			#endregion
			if (cMsgType == DOWNLOAD_PROGRAM || cMsgType == DOWNLOAD_FILE)
			{
				mDataField[encPos++] = 0;	
				mDataField[encPos++] = 0;
			}

			#region Copy in the program chunk
            Array.Copy(mDownloadData, 0, mDataField, encPos, mDownloadData.Length);
			encPos += theDataLength;
			#endregion
			#region do 16-bit checksum of payload:
			iDownloadChecksum = 0;
			for (int pos = startPos; pos < encPos; pos++)
			{
				iDownloadChecksum += mDataField[pos];
			}
			mDataField[encPos++] = (byte) ((iDownloadChecksum & 0xFF));
			mDataField[encPos++] = (byte) ((iDownloadChecksum & 0xFF00) >> 8);
			#endregion
		}

		public new void Encode(ref Byte[] theData)
		{
			PrepareDataField();

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}

	}
}


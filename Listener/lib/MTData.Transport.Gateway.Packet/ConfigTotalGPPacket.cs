using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Encapsulates the CONFIG_TOTAL Gateway Protocol Packet
	/// </summary>
    [Serializable]
    public class ConfigTotalGPPacket : GatewayProtocolPacket
	{
		private const uint CONFIG_TOTAL_DRIVER_PHONE_LENGTH = 6;
		private const uint CONFIG_TOTAL_EXTRA_SEP_FLAGS_LENGTH = 25;
        

		public const uint CONFIG_TOTAL_ACTIVE_LENGTH = 13 +
							CONFIG_TOTAL_EXTRA_SEP_FLAGS_LENGTH;
		public const Byte CONFIG_TOTAL_ACTIVE= 0xd0; 
		public const Byte CONFIG_PACKET_MASK = 0xd0;
		private const Byte CONFIG_TOTAL_ADDED_FLAGS = 0xAA;

		#region Variables
		public byte cConfigFileActive;
		public byte cConfigFileActiveVersionMajor;
		public byte cConfigFileActiveVersionMinor;

        public byte cEcmConfigFileActive;
        public byte cEcmConfigFileActiveVersionMajor;
        public byte cEcmConfigFileActiveVersionMinor;
        
        public byte cRouteSetActive;
		public byte cRouteSetActiveVersionMajor;
		public byte cRouteSetActiveVersionMinor;
		public byte cRoutePointNumber;

		public byte cSetPointSetActive;
		public byte cSetPointSetActiveVersionMajor;
		public byte cSetPointSetActiveVersionMinor;

		public byte cScheduleActive;
		public byte cScheduleActiveVersionMajor;
		public byte cScheduleActiveVersionMinor;

		public uint uSetPointGroup_Flags;

        public uint uVehicleMass = 0;

		// Optional fields:
		public string sDriverName;
		public string sVehicleDescription;
		public string sDriverPhoneNumber;

        public byte cListenerVersionMajor;
        public byte cListenerVersionMinor;
        public byte cListenerVersionRevison;

        public byte cSetPointAll;

		#endregion

		public string sConstructorDecodeError = null;

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\niLength : "); builder.Append(iLength);
			builder.Append("\r\ncConfigFileActive : "); builder.Append(cConfigFileActive);
			builder.Append("\r\ncConfigFileActiveVersionMajor : "); builder.Append(cConfigFileActiveVersionMajor);
			builder.Append("\r\ncConfigFileActiveVersionMinor : "); builder.Append(cConfigFileActiveVersionMinor);
            builder.Append("\r\ncEcmConfigFileActive : "); builder.Append(cEcmConfigFileActive);
            builder.Append("\r\ncEcmConfigFileActiveVersionMajor : "); builder.Append(cEcmConfigFileActiveVersionMajor);
            builder.Append("\r\ncEcmConfigFileActiveVersionMinor : "); builder.Append(cEcmConfigFileActiveVersionMinor);
            builder.Append("\r\ncRouteSetActive : "); builder.Append(cRouteSetActive);
			builder.Append("\r\ncRouteSetActiveVersionMajor : "); builder.Append(cRouteSetActiveVersionMajor);
			builder.Append("\r\ncRouteSetActiveVersionMinor : "); builder.Append(cRouteSetActiveVersionMinor);
			builder.Append("\r\ncRoutePointNumber : "); builder.Append(cRoutePointNumber);
			builder.Append("\r\ncSetPointSetActive : "); builder.Append(cSetPointSetActive);
			builder.Append("\r\ncSetPointSetActiveVersionMajor : "); builder.Append(cSetPointSetActiveVersionMajor);
			builder.Append("\r\ncSetPointSetActiveVersionMinor : "); builder.Append(cSetPointSetActiveVersionMinor);
			builder.Append("\r\ncScheduleActive : "); builder.Append(cScheduleActive);
			builder.Append("\r\ncScheduleActiveVersionMajor : "); builder.Append(cScheduleActiveVersionMajor);
			builder.Append("\r\ncScheduleActiveVersionMinor : "); builder.Append(cScheduleActiveVersionMinor);
			builder.Append("\r\nsDriverName : "); builder.Append(sDriverName);
			builder.Append("\r\nsVehicleDescription : "); builder.Append(sVehicleDescription);
			builder.Append("\r\nsDriverPhoneNumber : "); builder.Append(sDriverPhoneNumber);
			builder.Append("\r\nSetPoint Group flags: "); builder.Append(uSetPointGroup_Flags);
            builder.Append("\r\ncListenerVersionMajor : "); builder.Append(cListenerVersionMajor);
            builder.Append("\r\ncListenerVersionMinor : "); builder.Append(cListenerVersionMinor);
            builder.Append("\r\ncListenerVersionRevison : "); builder.Append(cListenerVersionRevison);

			return builder.ToString();
		}

		public ConfigTotalGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_TOTAL_ACTIVE;
			cConfigFileActive = 0;
			cConfigFileActiveVersionMajor = 0;
			cConfigFileActiveVersionMinor = 0;
            cEcmConfigFileActive = 0;
            cEcmConfigFileActiveVersionMajor = 0;
            cEcmConfigFileActiveVersionMinor = 0;
            cRouteSetActive = 0;
			cRouteSetActiveVersionMajor = 0;
			cRouteSetActiveVersionMinor = 0;
			cRoutePointNumber = 0;
			cSetPointSetActive = 0;
			cSetPointSetActiveVersionMajor = 0;
			cSetPointSetActiveVersionMinor = 0;
			cScheduleActive = 0;
			cScheduleActiveVersionMajor = 0;
			cScheduleActiveVersionMinor = 0;
            cSetPointAll = 0;
			sConstructorDecodeError = null;
		}

		public ConfigTotalGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_TOTAL_ACTIVE;
			sConstructorDecodeError = Decode(p.mDataField);
			bIsPopulated = (sConstructorDecodeError == null);
		}

        public ConfigTotalGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat, bool bDecodeBaseOnly)
            : base(p, serverTime_DateFormat)
        {
            cMsgType = CONFIG_TOTAL_ACTIVE;
            if (bDecodeBaseOnly)
            {
                cConfigFileActive = 0;
                cConfigFileActiveVersionMajor = 0;
                cConfigFileActiveVersionMinor = 0;
                cEcmConfigFileActive = 0;
                cEcmConfigFileActiveVersionMajor = 0;
                cEcmConfigFileActiveVersionMinor = 0;
                cRouteSetActive = 0;
                cRouteSetActiveVersionMajor = 0;
                cRouteSetActiveVersionMinor = 0;
                cRoutePointNumber = 0;
                cSetPointSetActive = 0;
                cSetPointSetActiveVersionMajor = 0;
                cSetPointSetActiveVersionMinor = 0;
                cScheduleActive = 0;
                cScheduleActiveVersionMajor = 0;
                cScheduleActiveVersionMinor = 0;
                cSetPointAll = 0;
                bIsPopulated = true;
            }
            else
            {
                sConstructorDecodeError = Decode(p.mDataField);
                bIsPopulated = (sConstructorDecodeError == null);
            }
        }
		// Returns True if the configuration fields are the same.
		// Other protocol fields are not checked for equality
		public bool SchedulesAreIdentical(ConfigTotalGPPacket p)
		{
			bool isSame = true;
			isSame = isSame && (cScheduleActive == p.cScheduleActive);
			isSame = isSame && (cScheduleActiveVersionMajor == p.cScheduleActiveVersionMajor);
			isSame = isSame && (cScheduleActiveVersionMinor == p.cScheduleActiveVersionMinor);
			return isSame;
		}

		// Returns True if the configuration fields are the same.
		// Other protocol fields are not checked for equality
		public bool ConfigurationsAreIdentical(ConfigTotalGPPacket p)
		{
			bool isSame = true;
			isSame = isSame && (cConfigFileActive == p.cConfigFileActive);
			isSame = isSame && (cConfigFileActiveVersionMajor == p.cConfigFileActiveVersionMajor);
			isSame = isSame && (cConfigFileActiveVersionMinor == p.cConfigFileActiveVersionMinor);
			return isSame;
		}

        // Returns True if the configuration fields are the same.
        // Other protocol fields are not checked for equality
        public bool EcmConfigurationsAreIdentical(ConfigTotalGPPacket p)
        {
            //check if ECM config was sent in
            if (p.cEcmConfigFileActive == 0xFF && p.cEcmConfigFileActiveVersionMajor == 0xFF && p.cEcmConfigFileActiveVersionMinor == 0xFF)
            {
                //unit is not requesting ECM config, return true so now config is sent
                return true;
            }

            bool isSame = true;
            isSame = isSame && (cEcmConfigFileActive == p.cEcmConfigFileActive);
            isSame = isSame && (cEcmConfigFileActiveVersionMajor == p.cEcmConfigFileActiveVersionMajor);
            isSame = isSame && (cEcmConfigFileActiveVersionMinor == p.cEcmConfigFileActiveVersionMinor);
            return isSame;
        }

        // Returns True if the route group fields are the same.
		// Other protocol fields are not checked for equality
		// Note we don't test the initial point any more, as it takes too much work to
		// determine this. If the user changed the initial point, their version number
		// would increment automatically anyway and they'd get their new route group
		// downloaded because of that
		public bool RoutesAreIdentical(ConfigTotalGPPacket p)
		{
			bool isSame = true;
			isSame = isSame && (cRouteSetActive == p.cRouteSetActive);
			isSame = isSame && (cRouteSetActiveVersionMajor == p.cRouteSetActiveVersionMajor);
			isSame = isSame && (cRouteSetActiveVersionMinor == p.cRouteSetActiveVersionMinor);
		//	isSame = isSame && (cRoutePointNumber == p.cRoutePointNumber);
			return isSame;
		}

		// Returns True if the setpt group fields are the same.
		// Other protocol fields are not checked for equality
		public bool SetPointsAreIdentical(ConfigTotalGPPacket p)
		{
            if(p.cSetPointAll == 0x01)
            {
                cSetPointAll = 1;
                return true;
            }

			bool isSame = true;
			isSame = isSame && (cSetPointSetActive == p.cSetPointSetActive);
			isSame = isSame && (cSetPointSetActiveVersionMajor == p.cSetPointSetActiveVersionMajor);
			isSame = isSame && (cSetPointSetActiveVersionMinor == p.cSetPointSetActiveVersionMinor);
			return isSame;
		}

		public new void Encode(ref byte[] theData)
		{	
			int encPos = 0;

			mDataField = new Byte[CONFIG_TOTAL_ACTIVE_LENGTH];

			mDataField[encPos++] = cConfigFileActive;
			mDataField[encPos++] = cConfigFileActiveVersionMajor;
			mDataField[encPos++] = cConfigFileActiveVersionMinor;

			mDataField[encPos++] = cRouteSetActive;
			mDataField[encPos++] = cRouteSetActiveVersionMajor;
			mDataField[encPos++] = cRouteSetActiveVersionMinor;
			mDataField[encPos++] = cRoutePointNumber;

			mDataField[encPos++] = cSetPointSetActive;
			mDataField[encPos++] = cSetPointSetActiveVersionMajor;
			mDataField[encPos++] = cSetPointSetActiveVersionMinor;

			mDataField[encPos++] = cScheduleActive;
			mDataField[encPos++] = cScheduleActiveVersionMajor;
			mDataField[encPos++] = cScheduleActiveVersionMinor;

			//updated config_total-active to support pollygon waypoints
			mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator
			mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator

			//config total active flags
			mDataField[encPos++] = (byte)(uSetPointGroup_Flags & 0xFF);
			mDataField[encPos++] = (byte)((uSetPointGroup_Flags & 0xFF00) >> 8);
			mDataField[encPos++] = (byte)((uSetPointGroup_Flags & 0xFF0000) >> 16);
			mDataField[encPos++] = (byte)((uSetPointGroup_Flags & 0xFF0000) >> 24);

            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator
            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator

            //Vehicle Mass
			mDataField[encPos++] = (byte)(uVehicleMass & 0xFF);
			mDataField[encPos++] = (byte)((uVehicleMass & 0xFF00) >> 8);
			mDataField[encPos++] = (byte)((uVehicleMass & 0xFF0000) >> 16);
			mDataField[encPos++] = (byte)((uVehicleMass & 0xFF0000) >> 24);

            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator
            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator
            mDataField[encPos++] = cEcmConfigFileActive;
            mDataField[encPos++] = cEcmConfigFileActiveVersionMajor;
            mDataField[encPos++] = cEcmConfigFileActiveVersionMinor;

            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator
            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;			//seperator
            mDataField[encPos++] = cListenerVersionMajor;
            mDataField[encPos++] = cListenerVersionMinor;
            mDataField[encPos++] = cListenerVersionRevison;

            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;            //seperator
            mDataField[encPos++] = CONFIG_TOTAL_ADDED_FLAGS;            //seperator
            mDataField[encPos++] = cSetPointAll;            

            // and encode this down:
            base.Encode(ref theData);
		}

		public string Decode(byte[] theData)
		{
			int decPos = 0;
			string problem = null;
			if (!bIsPopulated)	problem = base.Decode(theData, 0);
			if (problem != null) return problem;

			cConfigFileActive = mDataField[decPos++];
			cConfigFileActiveVersionMajor = mDataField[decPos++];
			cConfigFileActiveVersionMinor = mDataField[decPos++];

			cRouteSetActive = mDataField[decPos++];
			cRouteSetActiveVersionMajor = mDataField[decPos++];
			cRouteSetActiveVersionMinor = mDataField[decPos++];
			cRoutePointNumber = mDataField[decPos++];

			cSetPointSetActive = mDataField[decPos++];
			cSetPointSetActiveVersionMajor = mDataField[decPos++];
			cSetPointSetActiveVersionMinor = mDataField[decPos++];

			cScheduleActive = mDataField[decPos++];
			cScheduleActiveVersionMajor = mDataField[decPos++];
			cScheduleActiveVersionMinor = mDataField[decPos++];

            if(mDataField.Length >= decPos + 6)
            {
                if ((mDataField[decPos] == (byte)0xAA) && (mDataField[decPos + 1] == (byte)0xAA))
                {
                    decPos += 2;
                    uSetPointGroup_Flags = BitConverter.ToUInt32(mDataField, decPos);
                    decPos += 4;
                    if (mDataField.Length >= decPos + 6)
                    {
                        if ((mDataField[decPos] == (byte)0xAA) && (mDataField[decPos + 1] == (byte)0xAA))
                        {
                            decPos += 2;
                            uVehicleMass = BitConverter.ToUInt32(mDataField, decPos);
                            decPos += 4;
                        }
                    }
                }
            }

            cEcmConfigFileActive = 0xFF;
            cEcmConfigFileActiveVersionMajor = 0xFF;
            cEcmConfigFileActiveVersionMinor = 0xFF;
            if (mDataField.Length >= decPos + 5)
            {
                if ((mDataField[decPos] == (byte)0xAA) && (mDataField[decPos + 1] == (byte)0xAA))
                {
                    decPos += 2;
                    cEcmConfigFileActive = mDataField[decPos++];
                    cEcmConfigFileActiveVersionMajor = mDataField[decPos++];
                    cEcmConfigFileActiveVersionMinor = mDataField[decPos++];
                }
            }

            if (mDataField.Length >= decPos + 5)
            {
                if ((mDataField[decPos] == (byte)0xAA) && (mDataField[decPos + 1] == (byte)0xAA))
                {
                    decPos += 2;
                    cListenerVersionMajor = mDataField[decPos++];
                    cListenerVersionMinor = mDataField[decPos++];
                    cListenerVersionRevison = mDataField[decPos++];
                }
            }

            if(mDataField.Length >= decPos + 3)
            {
                if((mDataField[decPos] == (byte)0xAA) && (mDataField[decPos +1] == (byte)0xAA))
                {
                    cSetPointAll = mDataField[decPos++];
                }
            }
            return problem;
		}

	}
}

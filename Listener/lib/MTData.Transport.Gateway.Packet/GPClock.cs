using System;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Wraps up a clock value in the Gateway Protocol.
	/// Unfortunately this can have two possible lengths.
	/// When used in status reports etc, it's 6 bytes.
	/// When used in GPS History, it's only 5 (year byte is omitted)
	/// </summary>
    [Serializable]
    public class GPClock
	{
		public uint iDay;
		public uint iMonth;
		public uint iYear;
		public uint iHour;
		public uint iMinute;
		public uint iSecond;
		public bool bIsPopulated;
		public bool bUseShortVersion;
		private string sClockName = "GPS";
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
		public const int Length = 6;
		public const int ShortLength = 5;

		public GPClock(string clockName, string serverTime_DateFormat)
		{
			sClockName = clockName;
			_serverTime_DateFormat = serverTime_DateFormat;
			iDay = 0;
			iMonth = 0;
			iYear = 0;
			iHour = 0;
			iMinute = 0;
			iSecond = 0;
			bIsPopulated = false;
			bUseShortVersion = false;
		}

		public GPClock(string clockName, System.DateTime t, string serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			sClockName = clockName;
			iDay		= (uint) t.Day;
			iMonth		= (uint) t.Month;
			iYear		= (uint) t.Year;
			iHour		= (uint) t.Hour;
			iMinute		= (uint) t.Minute;
			iSecond		= (uint) t.Second;
			bIsPopulated = true;
			bUseShortVersion = false;
		}
		
		public GPClock CreateCopy()
		{
			GPClock oClock = new GPClock(this.sClockName, _serverTime_DateFormat);
			oClock.iDay = this.iDay;
			oClock.iMonth = this.iMonth;
			oClock.iYear = this.iYear;
			oClock.iHour = this.iHour;
			oClock.iMinute = this.iMinute;
			oClock.iSecond = this.iSecond;
			oClock.bIsPopulated = this.bIsPopulated;
			oClock.bUseShortVersion = this.bUseShortVersion;
			return oClock;
		}

		public string ToTimeString()
		{
			if (bIsPopulated)
			{
                if (iYear < 2000)
                    iYear += 2000;

				if (IsDate(iMonth.ToString("00") + "/" + 
					iDay.ToString("00") + "/" + 
					iYear.ToString("0000") + " " +	
					iHour.ToString("00") + ":" + 
					iMinute.ToString("00") + ":" + 
					iSecond.ToString("00")))				
					return iHour.ToString("00") + ":" + 
						iMinute.ToString("00") + ":" + 
						iSecond.ToString("00");
				else
					return " ";				
			}
			else return " ";
		}

		public void FromDateTime(DateTime dtDateTime)
		{
			try
			{
				iYear = (uint) dtDateTime.Year;
				iMonth = (uint) dtDateTime.Month;
				iDay = (uint) dtDateTime.Day;
				iHour = (uint) dtDateTime.Hour;
				iMinute = (uint) dtDateTime.Minute;
				iSecond = (uint) dtDateTime.Second;
			}
			catch(System.Exception)
			{
				iYear = 0;
				iMonth = 0;
				iDay = 0;
				iHour = 0;
				iMinute = 0;
				iSecond = 0;
			}
		}

		public DateTime ToDateTime()
		{
			if (bIsPopulated)
			{
				if(iYear < 2000)
				{
					return new DateTime(
						(int) (iYear + 2000),
						(int) iMonth,
						(int) iDay,
						(int) iHour,
						(int) iMinute,
						(int) iSecond);
				}
				else
				{
					return new DateTime(
						(int) iYear,
						(int) iMonth,
						(int) iDay,
						(int) iHour,
						(int) iMinute,
						(int) iSecond);
				}
			}
			else
			{
				// Better than nothing:
				return DateTime.UtcNow;
			}
		}
		public string Decode(byte[] aData)
		{
			return Decode(aData, 0);
		}

		public string Decode(byte[] aData, int aStartPos)
		{
			int index = aStartPos;

			if (bUseShortVersion)
			{
				// Ensure we've got a decent array:
				if (aData.Length < (index + ShortLength)) return sClockName + " Clock field too short";

				iMonth	= aData[index++];
				iDay	= aData[index++];
				
				// Year is omitted
				iHour	= aData[index++];
				iMinute = aData[index++];
				iSecond = aData[index++];

				// Manufacture the year ourselves from the current date:
				iYear = (uint) (DateTime.Now.Year % 100); // Keep it as 2 digits for consistency
			}
			else
			{
				// Ensure we've got a decent array:
				if (aData.Length < (index + Length)) return sClockName + " Clock field too short";

				iDay	= aData[index++];
				iMonth	= aData[index++];
				iYear	= aData[index++];
				iHour	= aData[index++];
				iMinute = aData[index++];
				iSecond = aData[index++];
			}
			
			if (iYear < 100)
				iYear = iYear + 2000;

			if ((iDay > 0) && (iDay < 32)) 
			{// Get to here if values are reasonably sane :-)
				bIsPopulated = true;
				return null;
			}
			else
			{
				bIsPopulated = false;
				StringBuilder builder = new StringBuilder();
				builder.Append(sClockName);
				builder.Append(" Clock invalid - ");
				builder.Append(Util.EncodeByteArrayAsHex(aData, aStartPos, (bUseShortVersion?ShortLength:Length)));

				return builder.ToString();
			}
		}

		public bool IsDate(object inValue)
		{
			bool bValid = false;
			try 
			{
                if (iYear > 0 && iMonth > 0 && iDay > 0 && iDay > 0 && iDay < 32 && iHour < 25 && iMinute < 61 && iSecond < 61)
                {
                    DateTime dtDate = new DateTime((int) iYear, (int) iMonth, (int) iDay, (int) iHour, (int) iMinute, (int) iSecond);
                    bValid = true;
                }
			}
			catch (System.Exception) 
			{
				bValid = false;
			}			
			return bValid;
		}

		public void Encode(ref byte[] theData, int aStartPos)
		{
			// Ensure we've got a decent array:
			if (theData.Length >= (aStartPos + Length))
			{
				theData[aStartPos++] = (byte) iDay;
				theData[aStartPos++] = (byte) iMonth;
				theData[aStartPos++] = (byte) (iYear % 100);
				theData[aStartPos++] = (byte) iHour;
				theData[aStartPos++] = (byte) iMinute;
				theData[aStartPos++] = (byte) iSecond;
			}
		}
		public override string ToString()
		{			
			if (bIsPopulated)
			{
                if (iYear < 2000)
                    iYear += 2000;

				string s = iMonth.ToString("00") + "/" +  iDay.ToString("00") + "/" + iYear.ToString("0000") + " " +	 iHour.ToString("00") + ":" +  iMinute.ToString("00") + ":" + iSecond.ToString("00");
				if (IsDate(s)) return s;			
				else return " ";				
			}
			else return " ";
		}

		public string ToDisplayString()
		{
			return	this.ToString();
		}

        public DateTime? ToNullableDateTime()
        {
            if (bIsPopulated)
            {
                if (iYear < 2000)
                {
                    return new DateTime(
                        (int)(iYear + 2000),
                        (int)iMonth,
                        (int)iDay,
                        (int)iHour,
                        (int)iMinute,
                        (int)iSecond);
                }
                else
                {
                    return new DateTime(
                        (int)iYear,
                        (int)iMonth,
                        (int)iDay,
                        (int)iHour,
                        (int)iMinute,
                        (int)iSecond);
                }
            }
            else
            {
                return null;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class GPUnitStartUpReport
    {
        #region Private Variables
        public bool bIsPopulated = false;
        private DateTime dtReportDeviceTime = DateTime.MinValue;
        private DateTime dtGPSTime = DateTime.MinValue;
        private DateTime dtDeviceTime = DateTime.MinValue;
        private byte bGPSFlags = (byte)0x00;
        private double dLastKnowLat = 0;
        private double dLastKnowLong = 0;
        private ushort iOffsetSeconds = 0;
        private ushort iSecondsSincePowerup = 0;
        private ushort iSecondsSinceFirstIgnition = 0;
        private ushort iSecondsSinceGPRSActive = 0;
        private ushort iSecondsSinceGPSAquired = 0;
        private ushort iSecondsSinceFirstCTA = 0;
        private uint iStatusFlags = 0;
        private uint iWPID = 0;
        private uint iRefStatusFlags = 0;
        #endregion
        #region Public Properties
        public const int DataLength = 25;
        public DateTime ReportDeviceTime
        {
            get { return dtReportDeviceTime; }
            set { dtReportDeviceTime = value; }
        }
        public DateTime GPSTime
        {
            get { return dtGPSTime; }
            set {dtGPSTime = value;}
        }
        public DateTime DeviceTime
        {
            get { return dtDeviceTime; }
            set { dtDeviceTime = value; }
        }

        public DateTime IgnitionOnGPSTime
        {
            get { return dtDeviceTime; }
        }
        public DateTime IgnitionOnDeviceTime
        {
            get { return dtReportDeviceTime.AddSeconds(-1 * iSecondsSinceFirstIgnition); }
        }

        public DateTime PowerOnGPSTime
        {
            get { return dtReportDeviceTime.AddSeconds(-1 * iSecondsSinceGPSAquired); }
        }
        public DateTime PowerOnDeviceTime
        {
            get { return dtReportDeviceTime.AddSeconds(-1 * iSecondsSincePowerup); }
        }

        public byte GPSFlags
        {
            get { return bGPSFlags; }
            set { bGPSFlags = value; }
        }
        public double Latitude
        {
            get
            {
                if (dLastKnowLat == 1 && dLastKnowLong == 1)
                    return 0;
                else
                    return dLastKnowLat;
            }
            set { dLastKnowLat = value; }
        }
        public double Longitude
        {
            get
            {
                if (dLastKnowLat == 1 && dLastKnowLong == 1)
                    return 0;
                else
                    return dLastKnowLong;
            }
            set { dLastKnowLong = value; }
        }
        public ushort SecondsSincePowerup
        {
            get { return iSecondsSincePowerup; }
            set { iSecondsSincePowerup = value; }
        }
        public ushort SecondsSinceFirstIgnition
        {
            get { return iSecondsSinceFirstIgnition; }
            set { iSecondsSinceFirstIgnition = value; }
        }
        public ushort SecondsSinceGPRSActive
        {
            get { return iSecondsSinceGPRSActive; }
            set { iSecondsSinceGPRSActive = value; }
        }
        public ushort SecondsSinceGPSAquired
        {
            get { return iSecondsSinceGPSAquired; }
            set { iSecondsSinceGPSAquired = value; }
        }
        public ushort SecondsSinceFirstCTA
        {
            get { return iSecondsSinceFirstCTA; }
            set { iSecondsSinceFirstCTA = value; }
        }
        public uint StatusFlags
        {
            get { return iStatusFlags; }
            set { iStatusFlags = value; }
        }
        public uint CurrentSetPointID
        {
            get { return iWPID; }
            set { iWPID = value; }
        }
        public uint RefStatusFlags
        {
            get { return iRefStatusFlags; }
            set { iRefStatusFlags = value; }
        }
        #endregion
        #region Constructors
        public GPUnitStartUpReport()
        {
            bIsPopulated = false;
        }
        public GPUnitStartUpReport(DateTime reportDeviceTime)
        {
            bIsPopulated = false;
            dtReportDeviceTime = reportDeviceTime;
        }
        #endregion
        #region Public Methods
        public GPUnitStartUpReport CreateCopy()
        {
            GPUnitStartUpReport oGPUnitStartUpReport = new GPUnitStartUpReport();
            oGPUnitStartUpReport.ReportDeviceTime = dtReportDeviceTime;
            oGPUnitStartUpReport.GPSTime = dtGPSTime;
            oGPUnitStartUpReport.DeviceTime = dtDeviceTime;
            oGPUnitStartUpReport.Latitude = dLastKnowLat;
            oGPUnitStartUpReport.Longitude = dLastKnowLong;
            oGPUnitStartUpReport.SecondsSincePowerup = iSecondsSincePowerup;
            oGPUnitStartUpReport.SecondsSinceFirstIgnition = iSecondsSinceFirstIgnition;
            oGPUnitStartUpReport.SecondsSinceGPRSActive = iSecondsSinceGPRSActive;
            oGPUnitStartUpReport.SecondsSinceFirstCTA = iSecondsSinceFirstCTA;
            return oGPUnitStartUpReport;
        }
        public string Decode(Byte[] aData)
        {
            int iPos = 0;
            return Decode(aData, ref iPos);
        }
        public string Decode(Byte[] aData, ref int index)
        {
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            double dTemp = 0;
            double dDivisor = 60000;
            byte bMarker = (byte) 0x00;
            try
            {
                #region Last Known GPS Time and Device Time
                iDay = aData[index++];
                iMonth = aData[index++];
                iYear = aData[index++];
                iHour = aData[index++];
                iMinute = aData[index++];
                iSecond = aData[index++];
                iYear = iYear + 2000;
                iOffsetSeconds = BitConverter.ToUInt16(aData, index);
                index += 2;
                try
                {
                    dtGPSTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond);
                    dtDeviceTime = dtGPSTime.AddSeconds(iOffsetSeconds);
                }
                catch (System.Exception)
                {
                    dtGPSTime = dtReportDeviceTime;
                    dtDeviceTime = dtReportDeviceTime;
                }
                #endregion
                #region Last Known Lat/Long
                bGPSFlags = aData[index++];
                // Latitude
                dTemp = Convert.ToDouble(aData[index] + (aData[index + 1] << 8) + (aData[index + 2] << 16));
                index += 3;
                dLastKnowLat = Math.Round((dTemp / dDivisor), 5);
                if((bGPSFlags & 0x01) == 0)
                    dLastKnowLat = (0 - dLastKnowLat);
                // Longitude
                dTemp = Convert.ToDouble(aData[index] + (aData[index + 1] << 8) + (aData[index + 2] << 16));
                index += 3;
                dLastKnowLong = Math.Round((dTemp / dDivisor), 5);
                if((bGPSFlags & 0x02) == 0)
                    dLastKnowLong = (0 - dLastKnowLong);
                #endregion
                #region Event Timings
                iSecondsSincePowerup = BitConverter.ToUInt16(aData, index);
                index += 2;
                iSecondsSinceFirstIgnition = BitConverter.ToUInt16(aData, index);
                index += 2;
                iSecondsSinceGPRSActive = BitConverter.ToUInt16(aData, index);
                index += 2;
                iSecondsSinceGPSAquired = BitConverter.ToUInt16(aData, index);
                index += 2;
                iSecondsSinceFirstCTA = BitConverter.ToUInt16(aData, index);
                index += 2;
                #endregion

                // Extended States
                if (aData.Length >= index + 11)
                {
                    bMarker = aData[index++];
                    if(bMarker == (byte) 0xAA)
                    {
                        bMarker = aData[index++];
                        if (bMarker == (byte)0xAA)
                        {
                            iStatusFlags = BitConverter.ToUInt32(aData, index);
                            index += 4;
                            iWPID = BitConverter.ToUInt32(aData, index);
                            index += 4;
                            iRefStatusFlags = (uint) aData[index++];
                        }
                    }
                }


            }
            catch (System.Exception ex)
            {
                return "GPUnitStartUpReport Decode Fail : " + ex.Message;
            }
            return null;
        }
        public void Encode(ref byte[] theData, int aStartPos)
        {
            int iYear = 0;
            byte[] bData = null;
            byte bGPSFlags = (byte)0x00;
            double dLat = 0;
            double dLong = 0;
            double dMultiplier = 60000;
            if (theData.Length > aStartPos + DataLength)
            {
                dLat = dLastKnowLat;
                dLong = dLastKnowLong;
                if (dLat < 0)
                {
                    dLat = dLat * -1;
                    bGPSFlags = (byte) (bGPSFlags & 0x01);
                }
                if (dLong < 0)
                {
                    dLong = dLong * -1;
                    bGPSFlags = (byte)(bGPSFlags & 0x02);
                }
                dLat = dLat * dMultiplier;
                dLong = dLong * dMultiplier;

                theData[aStartPos++] = (byte)dtGPSTime.Day;
                theData[aStartPos++] = (byte)dtGPSTime.Month;
                iYear = dtGPSTime.Year;
                iYear -= 2000;
                theData[aStartPos++] = (byte)iYear;
                theData[aStartPos++] = (byte)dtGPSTime.Hour;
                theData[aStartPos++] = (byte)dtGPSTime.Minute;
                theData[aStartPos++] = (byte)dtGPSTime.Second;
                bData = BitConverter.GetBytes(Convert.ToUInt16(((TimeSpan)dtDeviceTime.Subtract(dtGPSTime)).TotalSeconds));
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                theData[aStartPos++] = bGPSFlags;
                bData = BitConverter.GetBytes(Convert.ToInt32(dLat));
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                theData[aStartPos++] = bData[2];
                bData = BitConverter.GetBytes(Convert.ToInt32(dLong));
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                theData[aStartPos++] = bData[2];
                bData = BitConverter.GetBytes(iSecondsSincePowerup);
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                bData = BitConverter.GetBytes(iSecondsSinceFirstIgnition);
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                bData = BitConverter.GetBytes(iSecondsSinceGPRSActive);
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                bData = BitConverter.GetBytes(iSecondsSinceGPSAquired);
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
                bData = BitConverter.GetBytes(iSecondsSinceFirstCTA);
                theData[aStartPos++] = bData[0];
                theData[aStartPos++] = bData[1];
            }
        }
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nUnit Startup Report :");
                builder.Append("\r\n    Last Known GPS Time : "); builder.Append(dtGPSTime.ToString("dd/MM/yyyy HH:mm:ss"));
                builder.Append("\r\n    Last Known Device Time : "); builder.Append(dtDeviceTime.ToString("dd/MM/yyyy HH:mm:ss"));
                builder.Append("\r\n    Last Known Latitude : "); builder.Append(dLastKnowLat);
                builder.Append("\r\n    Last Known Longitude : "); builder.Append(dLastKnowLong);
                if (DateTime.MinValue.CompareTo(dtReportDeviceTime) == 0)
                {
                    builder.Append("\r\n    Powerup Report Time : None"); 
                    builder.Append("\r\n    Seconds Since Powerup : "); builder.Append(iSecondsSincePowerup);
                    builder.Append("\r\n    Seconds Since First Ignition : "); builder.Append(iSecondsSinceFirstIgnition);
                    builder.Append("\r\n    Seconds Since GPRS Active : "); builder.Append(iSecondsSinceGPRSActive);
                    builder.Append("\r\n    Seconds Since GPS Aquired : "); builder.Append(iSecondsSinceGPSAquired);
                    builder.Append("\r\n    Seconds Since First CTA : "); builder.Append(iSecondsSinceFirstCTA);
                }
                else
                {
                    builder.Append("\r\n    Powerup Report Time : "); builder.Append(dtReportDeviceTime.ToString("dd/MM/yyyy HH:mm:ss"));
                    builder.Append("\r\n    Powerup : "); builder.Append(dtReportDeviceTime.AddSeconds(-1 * iSecondsSincePowerup).ToString("dd/MM/yyyy HH:mm:ss"));
                    if (iSecondsSinceFirstIgnition > 0)
                    {
                        builder.Append("\r\n    First Ignition : "); builder.Append(dtReportDeviceTime.AddSeconds(-1 * iSecondsSinceFirstIgnition).ToString("dd/MM/yyyy HH:mm:ss"));
                    }
                    else
                        builder.Append("\r\n    First Ignition : None");

                    if (iSecondsSinceGPRSActive > 0)
                    {
                        builder.Append("\r\n    GPRS Active : "); builder.Append(dtReportDeviceTime.AddSeconds(-1 * iSecondsSinceGPRSActive).ToString("dd/MM/yyyy HH:mm:ss"));
                    }
                    else
                        builder.Append("\r\n    GPRS Active : None");

                    if (iSecondsSinceGPSAquired > 0)
                    {
                        builder.Append("\r\n    GPS Aquired : "); builder.Append(dtReportDeviceTime.AddSeconds(-1 * iSecondsSinceGPSAquired).ToString("dd/MM/yyyy HH:mm:ss"));
                    }
                    else
                        builder.Append("\r\n    GPS Aquired : None");

                    if (iSecondsSinceFirstCTA > 0)
                    {
                        builder.Append("\r\n    First CTA : "); builder.Append(dtReportDeviceTime.AddSeconds(-1 * iSecondsSinceFirstCTA).ToString("dd/MM/yyyy HH:mm:ss"));
                    }
                    else
                        builder.Append("\r\n    First CTA : None");
                }
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding GPUnitStartUpReport Packet : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

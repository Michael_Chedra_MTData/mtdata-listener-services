using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Encapsulates a Set point in a New SETPT config message.
	/// </summary>
	/// 
    [Serializable]
    public class PolygonNode
	{
		public float Latitude;
		public float Longitude;
        public int Radius = 0;
	}

    [Serializable]
    public class GPSetPoint
    {
        public byte cPointNumberOld;
        public byte cFlagsOld;
        public float fBinLat;
        public float fBinLong;
        public int iLatTolerance;
        public int iLongTolerance;
        public byte cDurationTime;
        public byte cMaxSpeedNmh = (byte)0x00;
        public byte cMaxSpeedNmhDecimalPlaces = (byte)0x00;
        public ushort uPointNumber = 0;
        public int iSetPointID = 0;
        public ushort uFlags = 0;

        //Polygon Waypoint support
        public byte PolySideNodes;
        PolygonNode[] Nodes;


        public static int Length = 21;			//only applicable with x/y waypoints, not polygon waypoints

        /// <summary>
        /// Return a human readable representation of the data
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("cPointNumber : "); builder.Append(cPointNumberOld);
            builder.Append("; cFlags : "); builder.Append(cFlagsOld);
            builder.Append("; fBinLat : "); builder.Append(fBinLat);
            builder.Append("; fBinLong : "); builder.Append(fBinLong);
            builder.Append("; iLatTolerance : "); builder.Append(iLatTolerance);
            builder.Append("; iLongTolerance : "); builder.Append(iLongTolerance);
            builder.Append("; cDurationTime : "); builder.Append(cDurationTime);
            builder.Append("; cMaxSpeedNmh : "); builder.Append(cMaxSpeedNmh);
            builder.Append("; cMaxSpeedNmhDecimalPlaces : "); builder.Append(cMaxSpeedNmhDecimalPlaces);
            builder.Append("; uSetPointNumber : "); builder.Append(uPointNumber);
            builder.Append("; uFlags : "); builder.Append(uFlags);
            if (PolySideNodes > 0)
            {
                builder.Append("; PolySideNodes : "); builder.Append(PolySideNodes);
                builder.Append("; Lat/Long Nodes: ");
                for (int i = 0; i < PolySideNodes; i++)
                {
                    builder.Append(i);
                    builder.Append(":(");
                    builder.Append(Nodes[i].Latitude);
                    builder.Append(", ");
                    builder.Append(Nodes[i].Longitude);
                    builder.Append(") - ");
                }
            }
            return builder.ToString();
        }

        public GPSetPoint()
        {

        }

        public GPSetPoint(int NodesNeeded)
        {
            Nodes = new PolygonNode[NodesNeeded];
            PolySideNodes = 0;
        }

        public void AddPointNode(PolygonNode Node)
        {
            Nodes[PolySideNodes++] = Node;
        }


        public GPSetPoint CreateCopy()
        {
            GPSetPoint oSetPoint = new GPSetPoint();

            oSetPoint.cPointNumberOld = this.cPointNumberOld;
            oSetPoint.cFlagsOld = this.cFlagsOld;
            this.Nodes.CopyTo(oSetPoint.Nodes, Nodes.Length);
            oSetPoint.cDurationTime = this.cDurationTime;
            oSetPoint.cMaxSpeedNmh = this.cMaxSpeedNmh;
            oSetPoint.uPointNumber = this.uPointNumber;
            oSetPoint.uFlags = this.uFlags;

            return oSetPoint;
        }



        // Encode this point into theData, at position startPos
        public int Encode(ref byte[] theData, byte cMsgType, int startPos, int versionNumber)
        {
            uint tempLatLong = 0;
            int i = 0;
            int EncodedLength = startPos;

            if (Nodes != null && Nodes.Length > 0)
            {
                if (cMsgType == ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON)
                {
                    //Start of setpoint - New Protocol
                    theData[startPos++] = (byte)0xCD;
                    //	New 4 byte Set Point Number
                    byte[] bData = BitConverter.GetBytes(iSetPointID);
                    for (int X = 0; X < bData.Length; X++)
                    {
                        theData[startPos++] = bData[X];
                    }
                }
                else
                {
                    //Start of setpoint - Old Protocol
                    theData[startPos++] = (byte)0xCC;
                    //	New 2 byte Set Point Number
                    theData[startPos++] = (byte)(uPointNumber & 0xFF);
                    theData[startPos++] = (byte)((uPointNumber & 0xFF00) >> 8);
                }
                //	New extended flags variable
                theData[startPos++] = (byte)(uFlags & 0xFF);
                theData[startPos++] = (byte)((uFlags & 0xFF00) >> 8);

                // Timings
                theData[startPos++] = cDurationTime;

                //	MAx Speed allowed at this waypoint
                theData[startPos++] = cMaxSpeedNmh;
                if (versionNumber >= 3)
                {
                    theData[startPos++] = cMaxSpeedNmhDecimalPlaces;
                }


                // N.B. All multi-byte variables are big-endian
                // Positions and tolerances
                tempLatLong = (uint)(Math.Abs(fBinLat) * 60000);
                theData[startPos++] = (byte)(tempLatLong & 0xFF);
                theData[startPos++] = (byte)((tempLatLong & 0xFF00) >> 8);
                theData[startPos++] = (byte)((tempLatLong & 0xFF0000) >> 16);

                tempLatLong = (uint)(Math.Abs(fBinLong) * 60000);
                theData[startPos++] = (byte)(tempLatLong & 0xFF);
                theData[startPos++] = (byte)((tempLatLong & 0xFF00) >> 8);
                theData[startPos++] = (byte)((tempLatLong & 0xFF0000) >> 16);

                //--------------------------------------------------------------------------
                // Defect 7082 - Firmware is now coded to have lat tolerance before long tolerance

                theData[startPos++] = (byte)(iLatTolerance & 0xFF);
                theData[startPos++] = (byte)((iLatTolerance & 0xFF00) >> 8);
                theData[startPos++] = (byte)((iLatTolerance & 0xFF0000) >> 16);

                theData[startPos++] = (byte)(iLongTolerance & 0xFF);
                theData[startPos++] = (byte)((iLongTolerance & 0xFF00) >> 8);
                theData[startPos++] = (byte)((iLongTolerance & 0xFF0000) >> 16);
                //--------------------------------------------------------------------------


                //No of polysides
                if (PolySideNodes > 20)				//limit the polysides to 20
                    PolySideNodes = 20;
                //Encode all of the latitudes,
                byte[] temparray = new byte[4];
                if (PolySideNodes == 1)
                {
                    theData[startPos++] = (byte)((int)PolySideNodes + 1);
                    temparray = System.BitConverter.GetBytes(Convert.ToInt32(Math.Round(Nodes[0].Latitude * 60000.0, 0)));
                    for (int j = 0; j < 4; j++)
                        theData[startPos++] = temparray[j];
                    temparray = System.BitConverter.GetBytes(Convert.ToInt32(Math.Round(Nodes[0].Longitude * 60000.0, 0)));
                    for (int j = 0; j < 4; j++)
                        theData[startPos++] = temparray[j];

                    temparray = System.BitConverter.GetBytes(Nodes[0].Radius);
                    for (int j = 0; j < 4; j++)
                        theData[startPos++] = temparray[j];
                    for (int j = 0; j < 4; j++)
                        theData[startPos++] = (byte)0x00;

                }
                else
                {
                    theData[startPos++] = PolySideNodes;
                    for (i = 0; i < PolySideNodes; i++)
                    {
                        temparray = System.BitConverter.GetBytes(Nodes[i].Latitude);
                        for (int j = 0; j < 4; j++)
                            theData[startPos++] = temparray[j];
                    }
                    for (i = 0; i < PolySideNodes; i++)
                    {
                        temparray = System.BitConverter.GetBytes(Nodes[i].Longitude);
                        for (int j = 0; j < 4; j++)
                            theData[startPos++] = temparray[j];
                    }
                }
            }
            else
            {

                // Basic info
                theData[startPos++] = cPointNumberOld;

                theData[startPos++] = cFlagsOld;

                // N.B. All multi-byte variables are big-endian
                // Positions and tolerances
                tempLatLong = (uint)(Math.Abs(fBinLat) * 60000);
                theData[startPos++] = (byte)(tempLatLong & 0xFF);
                theData[startPos++] = (byte)((tempLatLong & 0xFF00) >> 8);
                theData[startPos++] = (byte)((tempLatLong & 0xFF0000) >> 16);

                tempLatLong = (uint)(Math.Abs(fBinLong) * 60000);
                theData[startPos++] = (byte)(tempLatLong & 0xFF);
                theData[startPos++] = (byte)((tempLatLong & 0xFF00) >> 8);
                theData[startPos++] = (byte)((tempLatLong & 0xFF0000) >> 16);

                //--------------------------------------------------------------------------
                //	Firmware is now coded to have lat tolerance before long tolerance
                theData[startPos++] = (byte)(iLatTolerance & 0xFF);
                theData[startPos++] = (byte)((iLatTolerance & 0xFF00) >> 8);

                theData[startPos++] = (byte)(iLongTolerance & 0xFF);
                theData[startPos++] = (byte)((iLongTolerance & 0xFF00) >> 8);
                //--------------------------------------------------------------------------

                // Timings
                theData[startPos++] = cDurationTime;

                //	MAx Speed allowed at this waypoint
                theData[startPos++] = cMaxSpeedNmh;
                if (versionNumber >= 3)
                {
                    theData[startPos++] = cMaxSpeedNmhDecimalPlaces;
                }

                //	New 2 byte Set Point Number
                theData[startPos++] = (byte)(uPointNumber & 0xFF);
                theData[startPos++] = (byte)((uPointNumber & 0xFF00) >> 8);

                //	New extended flags variable
                theData[startPos++] = (byte)(uFlags & 0xFF);
                theData[startPos++] = (byte)((uFlags & 0xFF00) >> 8);

                // 3 Spare bytes
                theData[startPos++] = 0x00;
                theData[startPos++] = 0x00;
                theData[startPos++] = 0x00;
            }
            EncodedLength = startPos - EncodedLength;

            return EncodedLength;
        }

    }

}

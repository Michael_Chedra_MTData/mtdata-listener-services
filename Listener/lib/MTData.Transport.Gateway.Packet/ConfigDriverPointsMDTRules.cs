using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class ConfigDriverPointsMDTRules
    {
        #region Private Member Vars
        // [RuleId] [Points] [Output1] [Output2] [MdtType] [VehicleSpeed] [Duration] [Reset]
        private bool bIsPopulated;
        private byte _PacketVer;
        private int _RuleID;
        private byte _Points;
        private short _Output1;
        private short _Output2;
        private byte _MdtType;
        private byte _VehicleSpeed;
        private uint _Duration;
        private uint _ResetDuration;
        #endregion
        #region Public Properties
        public byte PacketVer
        {
            get { return _PacketVer; }
            set { _PacketVer = value; }
        }
        public int RuleID
        {
            get { return _RuleID; }
            set { _RuleID = value; }
        }
        public byte Points
        {
            get { return _Points; }
            set { _Points = value; }
        }
        public short Output1
        {
            get { return _Output1; }
            set { _Output1 = value; }
        }
        public short Output2
        {
            get { return _Output2; }
            set { _Output2 = value; }
        }
        public byte MdtType
        {
            get { return _MdtType; }
            set { _MdtType = value; }
        }
        public byte VehicleSpeed
        {
            get { return _VehicleSpeed; }
            set { _VehicleSpeed = value; }
        }
        public uint Duration
        {
            get { return _Duration; }
            set { _Duration = value; }
        }
        public uint ResetDuration
        {
            get { return _ResetDuration; }
            set { _ResetDuration = value; }
        }
        public bool IsPopulated
        {
            get { return bIsPopulated; }
            set { bIsPopulated = value; }
        }
        
        #endregion
        #region Constructor and CreateCopy functions
        public ConfigDriverPointsMDTRules()
        {
            _PacketVer = 0;
            bIsPopulated = false;
            _RuleID = 0;
            _Points = 0;
            _Output1 = -2;
            _Output2 = -2;
            _MdtType = 0;
            _VehicleSpeed = 0;
            _Duration = 0;
            _ResetDuration = 0;
        }
        public ConfigDriverPointsMDTRules CreateCopy()
        {
            ConfigDriverPointsMDTRules oData = new ConfigDriverPointsMDTRules();
            oData.PacketVer = _PacketVer;
            oData.RuleID = _RuleID;
            oData.Points = _Points;
            oData.Output1 = _Output1;
            oData.Output2 = _Output2;
            oData.MdtType = _MdtType;
            oData.VehicleSpeed = _VehicleSpeed;
            oData.Duration = _Duration;
            oData.ResetDuration = _ResetDuration;
            oData.IsPopulated = bIsPopulated;
            return oData;
        }
        #endregion
        #region Encoding / Decoding
        public string Decode(Byte[] aData, byte bPacketVer)
        {
            MemoryStream oMS = new MemoryStream(aData, 0, aData.Length);
            return Decode(ref oMS, bPacketVer);
        }
        public string Decode(Byte[] aData, byte bPacketVer, ref int index)
        {
            MemoryStream oMS = new MemoryStream(aData, index, aData.Length - index);
            string sRet = Decode(ref oMS, bPacketVer);
            if (sRet != null) return sRet;
            index += (int)oMS.Position;
            return null;
        }
        public string Decode(ref MemoryStream oMS, byte bPacketVer)
        {
            try
            {
                _PacketVer = bPacketVer;
               PacketUtilities.ReadFromStream(oMS, ref _RuleID);
               PacketUtilities.ReadFromStream(oMS, ref _Points);
               PacketUtilities.ReadFromStream(oMS, ref _Output1);
               PacketUtilities.ReadFromStream(oMS, ref _Output2);
               PacketUtilities.ReadFromStream(oMS, ref _MdtType);
               PacketUtilities.ReadFromStream(oMS, ref _VehicleSpeed);
               PacketUtilities.ReadFromStream(oMS, ref _Duration);
               PacketUtilities.ReadFromStream(oMS, ref _ResetDuration);
                bIsPopulated = true;
            }
            catch (System.Exception ex)
            {
                return ex.Message;
            }
            return null;
        }
        public void Encode(ref MemoryStream oMS)
        {
           PacketUtilities.WriteToStream(oMS, _RuleID);
           PacketUtilities.WriteToStream(oMS, _Points);
           PacketUtilities.WriteToStream(oMS, _Output1);
           PacketUtilities.WriteToStream(oMS, _Output2);
           PacketUtilities.WriteToStream(oMS, _MdtType);
           PacketUtilities.WriteToStream(oMS, _VehicleSpeed);
           PacketUtilities.WriteToStream(oMS, _Duration);
           PacketUtilities.WriteToStream(oMS, _ResetDuration);
        }
        #endregion
        #region ToString functions
        public string ToDisplayString()
        {
            return this.ToString();
        }
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\n        Packet Ver : "); builder.Append(_PacketVer);
                builder.Append("\r\n        Rule ID : "); builder.Append(_RuleID);
                builder.Append("\r\n        Points : "); builder.Append(_Points);
                builder.Append("\r\n        Input 1 : "); builder.Append(_Output1);
                builder.Append("\r\n        Input 2 : "); builder.Append(_Output2);
                builder.Append("\r\n        MDT Type : "); builder.Append(_MdtType);
                builder.Append("\r\n        Vehicle Speed : "); builder.Append(_VehicleSpeed);
                builder.Append("\r\n        Duration : "); builder.Append(_Duration);
                builder.Append("\r\n        Reset Duration : "); builder.Append(_ResetDuration);
                builder.Append("\r\n        -----------------------------");
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding Config Driver Points ECM Rule : " + ex.Message);
            }
            return builder.ToString();
        }
        #endregion
    }
}

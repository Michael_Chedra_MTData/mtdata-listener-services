﻿using System;
using System.IO;
using System.Text;
using MTData.Common.Utilities;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class RoadTypeData
    {
        #region Private Variables

        private byte versionNumber;

        private long segmentId;

        private double speedLimit;

        private double overrideSpeedLimit;

        private byte schoolZone;

        private byte isPrivate;

        private byte roadType;

        private int length;

        private string streetNumber;

        private string street;

        private string suburb;

        private string city;

        private string state;

        private string country;

        private string postCode;

        #endregion

        #region Constructor

        public RoadTypeData()
        {
            versionNumber = 1;
        }

        #endregion

        #region Public Properties

        public byte VersionNumber
        {
            get { return versionNumber; }
            set { versionNumber = value; }
        }

        public long SegmentId
        {
            get { return segmentId; }
            set { segmentId = value; }
        }

        public double SpeedLimit
        {
            get { return speedLimit; }
            set { speedLimit = value; }
        }

        public double OverrideSpeedLimit
        {
            get { return overrideSpeedLimit; }
            set { overrideSpeedLimit = value; }
        }

        public byte SchoolZone
        {
            get { return schoolZone; }
            set { schoolZone = value; }
        }

        public byte IsPrivate
        {
            get { return isPrivate; }
            set { isPrivate = value; }
        }

        public byte RoadType
        {
            get { return roadType; }
            set { roadType = value; }
        }

        public string StreetNumber
        {
            get { return streetNumber; }
            set { streetNumber = value; }
        }

        public string Street
        {
            get { return street; }
            set { street = value; }
        }

        public string Suburb
        {
            get { return suburb; }
            set { suburb = value; }
        }

        public string City
        {
            get { return city; }
            set { city = value; }
        }

        public string State
        {
            get { return state; }
            set { state = value; }
        }

        public string Country
        {
            get { return country; }
            set { country = value; }
        }

        public string PostCode
        {
            get { return postCode; }
            set { postCode = value; }
        }

        public int Length => length;

        #endregion

        #region Public Methods

        public string Decode(byte[] data, ref int index)
        {
            using (MemoryStream memoryStream = new MemoryStream(data, index, data.Length - index))
            {
                PacketUtilities.ReadFromStream(memoryStream, ref versionNumber);
                PacketUtilities.ReadFromStream(memoryStream, ref segmentId);
                PacketUtilities.ReadFromStream(memoryStream, ref speedLimit);
                PacketUtilities.ReadFromStream(memoryStream, ref overrideSpeedLimit);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref streetNumber);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref street);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref suburb);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref city);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref state);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref country);
                PacketUtilities.ReadFromStream(memoryStream, (byte)0x12, ref postCode);
                PacketUtilities.ReadFromStream(memoryStream, ref schoolZone);
                PacketUtilities.ReadFromStream(memoryStream, ref isPrivate);
                PacketUtilities.ReadFromStream(memoryStream, ref roadType);

                index += (int)memoryStream.Position;
            }

            return null;
        }

        public void Encode(ref byte[] data, int startPosition)
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                PacketUtilities.WriteToStream(memoryStream, versionNumber);
                PacketUtilities.WriteToStream(memoryStream, segmentId);
                PacketUtilities.WriteToStream(memoryStream, speedLimit);
                PacketUtilities.WriteToStream(memoryStream, overrideSpeedLimit);
                PacketUtilities.WriteToStream(memoryStream, 0x12, streetNumber);
                PacketUtilities.WriteToStream(memoryStream, 0x12, street);
                PacketUtilities.WriteToStream(memoryStream, 0x12, suburb);
                PacketUtilities.WriteToStream(memoryStream, 0x12, city);
                PacketUtilities.WriteToStream(memoryStream, 0x12, state);
                PacketUtilities.WriteToStream(memoryStream, 0x12, country);
                PacketUtilities.WriteToStream(memoryStream, 0x12, postCode);
                PacketUtilities.WriteToStream(memoryStream, schoolZone);
                PacketUtilities.WriteToStream(memoryStream, isPrivate);
                PacketUtilities.WriteToStream(memoryStream, roadType);

                byte[] returnData = memoryStream.ToArray();
                length = returnData.Length;
                Array.Copy(returnData, 0, data, startPosition, length);
            }
        }

        public RoadTypeData CreateCopy()
        {
            return new RoadTypeData
            {
                VersionNumber = VersionNumber,
                SegmentId = SegmentId,
                SpeedLimit = SpeedLimit,
                OverrideSpeedLimit = OverrideSpeedLimit,                
                SchoolZone = SchoolZone,
                IsPrivate = IsPrivate,
                RoadType = RoadType,
                StreetNumber = StreetNumber,
                Street = Street,
                Suburb = Suburb,
                City = City,
                State = State,
                Country = Country,
                PostCode = PostCode
            };
        }

        #endregion

        #region Override Methods

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            try
            {
                builder.Append("\r\nRoad Type Values : ");
                builder.Append("\r\nVersion : "); builder.Append(versionNumber);
                builder.Append("\r\nSegment Id: "); builder.Append(segmentId);
                builder.Append("\r\nSpeed Limit: "); builder.Append(speedLimit);
                builder.Append("\r\nOverride Speed Limit: "); builder.Append(overrideSpeedLimit);
                builder.Append("\r\nSchool zone: "); builder.Append(schoolZone);
                builder.Append("\r\nIs Private: "); builder.Append(isPrivate);
                builder.Append("\r\nRoad Type: "); builder.Append(roadType);
                builder.Append("\r\nStreet Number: "); builder.Append(streetNumber);
                builder.Append("\r\nStreet: "); builder.Append(street);
                builder.Append("\r\nSuburb: "); builder.Append(suburb);
                builder.Append("\r\nCity: "); builder.Append(city);
                builder.Append("\r\nState: "); builder.Append(state);
                builder.Append("\r\nCountry: "); builder.Append(country);
                builder.Append("\r\nPostal Code: "); builder.Append(postCode);
            }
            catch (System.Exception ex)
            {
                builder.Append("\r\nError Decoding RoadTypeData : " + ex.Message);
            }
            return builder.ToString();
        }

        #endregion
    }
}

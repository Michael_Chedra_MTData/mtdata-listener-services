using System;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for MotorolaSRecordRow.
	/// </summary>
    [Serializable]
    public class MotorolaSRecordRow
	{
		public enum MotorolaRecordType 
		{
			StartRecord, // 0
			Data16Bit, // 1
			Data24Bit, // 2
			Data32Bit, // 3
			Data32BitEnd = 7,
			Data24BitEnd = 8,
			Data16BitEnd = 9};

		public MotorolaRecordType mRecordType;
		public byte cLineLengthBytes;
		public byte cTotalLineLength;
		public long lAddress;
		private byte[] mDataBytes;
		public byte cChecksum;
		public bool bIsValid;

		public MotorolaSRecordRow(string rowFromFile)
		{
			bIsValid = Decode(rowFromFile);
		}

		private bool Decode(string s)
		{
			int iDecodePosition = 0;
			int iAddressLengthBytes = 0;
			byte checksum = 0;

			// An S-Record MUST start with an S!
			if (s[iDecodePosition++] != 'S') return false;	

			// Next comes type - one character
			mRecordType = (MotorolaRecordType) Convert.ToByte(s[iDecodePosition++]) - 0x30;						
	
			// Then length, two chars, which should be interpreted as a byte
			cLineLengthBytes = Convert.ToByte(s.Substring(iDecodePosition,2), 16);	
			iDecodePosition += 2;	

			cTotalLineLength = Convert.ToByte((iDecodePosition + (cLineLengthBytes * 2)) & 0xFF);
			// Check the length makes sense: 2 chars per byte for the rest of the line:
			if (s.Length != cTotalLineLength) return false;

			#region Decode the address depending on how big it is:
			switch (mRecordType)
			{
				case MotorolaRecordType.Data16Bit:
				case MotorolaRecordType.Data16BitEnd:
				case MotorolaRecordType.StartRecord:
					lAddress = Convert.ToUInt32(s.Substring(iDecodePosition,4), 16);
					iDecodePosition += 4;	
					iAddressLengthBytes = 2;

					break;
				case MotorolaRecordType.Data24Bit:
				case MotorolaRecordType.Data24BitEnd:
					lAddress = Convert.ToUInt32(s.Substring(iDecodePosition,6), 16);
					iDecodePosition += 6;	
					iAddressLengthBytes = 3;
					break;
				case MotorolaRecordType.Data32Bit:
				case MotorolaRecordType.Data32BitEnd:
					lAddress = Convert.ToUInt32(s.Substring(iDecodePosition,8), 16);
					iDecodePosition += 8;	
					iAddressLengthBytes = 4;
					break;
				default:
					return false;
					
			}	
			#endregion
	
			// We are now poised at the start of the data
			// Create an appropriately-sized byte array:
			mDataBytes = new byte[cLineLengthBytes - (iAddressLengthBytes + 1)];
			// Read it into the array:
			int byteCount = 0;
			for (; iDecodePosition < cTotalLineLength -2; iDecodePosition += 2)
			{
				mDataBytes[byteCount++] = Convert.ToByte(s.Substring(iDecodePosition,2), 16);	
			}
			
			// Read the checksum - last position:
			cChecksum = Convert.ToByte(s.Substring(iDecodePosition,2), 16);

			// Now verify the checksum:
			// One's complement of the length, address and data fields modulo 256 minus 1. 
			for (int i = 2; i < cTotalLineLength - 2; i += 2)
			{
				checksum += Convert.ToByte(s.Substring(i,2), 16);
			}
			checksum = Convert.ToByte((0xFF - checksum) & 0xFF);

			if (cChecksum != checksum) return false;

			return true;

		}
		public override string ToString()
		{
			string retString;
			byte checksum = 0;
			// Write this row back out to a string:
			retString = "S" + (int) mRecordType;
			retString += cLineLengthBytes.ToString("X2");

			#region spool out address
			switch (mRecordType)
			{
				case MotorolaRecordType.Data16Bit:
				case MotorolaRecordType.Data16BitEnd:
				case MotorolaRecordType.StartRecord:
					retString += lAddress.ToString("X4");
					break;
				case MotorolaRecordType.Data24Bit:
				case MotorolaRecordType.Data24BitEnd:
					retString += lAddress.ToString("X6");	
					break;
				case MotorolaRecordType.Data32Bit:
				case MotorolaRecordType.Data32BitEnd:
					retString += lAddress.ToString("X8");	
					break;
				default:
					retString += "0000";
					break;
			}	
			#endregion

			foreach (byte b in mDataBytes)
			{
				retString += b.ToString("X2");
				checksum += b; // Start doing the checksum while we're here
			}

			// Create the checksum:
			// One's complement of the length, address and data fields modulo 256 minus 1. 
			// The data field has already been done, just add the length and address:
			checksum += cLineLengthBytes;
			checksum += Convert.ToByte((lAddress >> 56) & 0xFF);
			checksum += Convert.ToByte((lAddress >> 48) & 0xFF);
			checksum += Convert.ToByte((lAddress >> 40) & 0xFF);
			checksum += Convert.ToByte((lAddress >> 32) & 0xFF);
			checksum += Convert.ToByte((lAddress >> 24) & 0xFF);
			checksum += Convert.ToByte((lAddress >> 16) & 0xFF);
			checksum += Convert.ToByte((lAddress >> 8) & 0xFF);
			checksum += Convert.ToByte(lAddress & 0xFF);

			// and the one's complement stuff:
			checksum = Convert.ToByte((0xFF - checksum) & 0xFF);
			retString += checksum.ToString("X2");

			return retString;
		}

		/// <summary>
		/// Get a number of contiguous bytes from the given offset in the Data area
		/// </summary>
		/// <param name="iOffset">The byte offset to start reading from</param>
		/// <param name="iLength">The number of bytes to return in the array</param>
		/// <returns>null if couldn't read that many bytes or offset too big</returns>
		public byte[] GetByteArray(int iOffset, int iLength)
		{
			// Check offset sanity:
			if (iOffset > mDataBytes.Length) return null;

			// Check (offset + length) sanity:
			if ((iOffset + iLength) > mDataBytes.Length) return null;

			byte[] retArray = new byte[iLength];
			Array.Copy(mDataBytes, iOffset, retArray, 0, iLength);
			return retArray;
		}
		
		public byte[] GetDataBytes()
		{
			if (mDataBytes != null)
			{
				return mDataBytes;
			}
			return null;
		}

		public bool SetDataByte(int iOffset, long value)
		{
			// Check offset sanity:
			if (iOffset > mDataBytes.Length) return false;

			mDataBytes[iOffset] = Convert.ToByte(value & 0xFF);
			return true;
		}
	}
}






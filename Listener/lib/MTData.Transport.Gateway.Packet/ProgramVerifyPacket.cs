using System;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for GeneralOutputGPPacket.
	/// </summary>
    [Serializable]
    public class ProgramVerifyPacket : GatewayProtocolPacket
	{
		public const byte DOWNLOAD_VERIFY		= 0x8e;
		public const byte DOWNLOAD_IOBOX_VERIFY = 0x8c;
		public const byte DOWNLOAD_MDT_VERIFY	= 0x87;
		public const int  DOWNLOAD_VERIFY_PAYLOAD_LENGTH = 16;
		#region Variables
		public byte cDownloadVersion;
		public int iProgramChecksumTotal;
		public int iPacketChecksumTotal;
		public int iPacketCountTotal;
		public int iProgramBytesCountTotal;
		#endregion

		/// <summary>
		/// Produce a user-friendly description of the details parsed
		/// </summary>
		/// <returns></returns>
		public override string ToDisplayString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(base.ToDisplayString());
			builder.Append("\r\ncDownloadVersion : "); builder.Append(cDownloadVersion);
			builder.Append("\r\niProgramChecksumTotal : "); builder.Append(iProgramChecksumTotal);
			builder.Append("\r\niPacketChecksumTotal : "); builder.Append(iPacketChecksumTotal);
			builder.Append("\r\niPacketCountTotal : "); builder.Append(iPacketCountTotal);
			builder.Append("\r\niProgramBytesCountTotal : "); builder.Append(iProgramBytesCountTotal);

			return builder.ToString();
		}

		public ProgramVerifyPacket(string serverTime_DateFormat) : this (DOWNLOAD_VERIFY, serverTime_DateFormat)
		{
		
		}
		public ProgramVerifyPacket(byte msgType, string serverTime_DateFormat) : base (serverTime_DateFormat)
		{
			cMsgType = msgType;
		}

		public ProgramVerifyPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			cMsgType = DOWNLOAD_VERIFY;
		}

		public void PrepareDataField()
		{
			int encPos = 0;
			if ((mDataField == null) || ((mDataField != null) && (mDataField.Length != DOWNLOAD_VERIFY_PAYLOAD_LENGTH)))
			{
				mDataField = new Byte[DOWNLOAD_VERIFY_PAYLOAD_LENGTH];
			}
			// Spare byte up front - duplicates the msg type byte
			mDataField[encPos++] = cMsgType;
			mDataField[encPos++] = cDownloadVersion;

			#region encode iProgramChecksumTotal:
			mDataField[encPos++] = (Byte) ((iProgramChecksumTotal & 0xFF));
			mDataField[encPos++] = (Byte) ((iProgramChecksumTotal & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iProgramChecksumTotal & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iProgramChecksumTotal & 0xFF000000) >> 24);
			#endregion
			#region iPacketChecksumTotal:
			mDataField[encPos++] = (Byte) ((iPacketChecksumTotal & 0xFF));
			mDataField[encPos++] = (Byte) ((iPacketChecksumTotal & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iPacketChecksumTotal & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iPacketChecksumTotal & 0xFF000000) >> 24);
			#endregion
			#region iPacketCountTotal:
			mDataField[encPos++] = (Byte) ((iPacketCountTotal & 0xFF));
			mDataField[encPos++] = (Byte) ((iPacketCountTotal & 0xFF00) >> 8);
			#endregion
			#region iProgramBytesCountTotal:
			mDataField[encPos++] = (Byte) ((iProgramBytesCountTotal & 0xFF));
			mDataField[encPos++] = (Byte) ((iProgramBytesCountTotal & 0xFF00) >> 8);
			mDataField[encPos++] = (Byte) ((iProgramBytesCountTotal & 0xFF0000) >> 16);
			mDataField[encPos++] = (Byte) ((iProgramBytesCountTotal & 0xFF000000) >> 24);
			#endregion
		}

		public new void Encode(ref Byte[] theData)
		{
			PrepareDataField();
			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
	}
}


using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.Gateway.Packet
{
    [Serializable]
    public class ConfigFileGPPacketECMAlert
    {
        private byte _reasonID;
        private ushort _sourceType;
        private ushort _SPNCode;
        private uint _compareToValue;

        public ConfigFileGPPacketECMAlert(byte ReasonID, ushort SourceType, ushort SPNCode, uint CompareToValue)
        {
            _reasonID = ReasonID;
            _sourceType = SourceType;
            _SPNCode = SPNCode;
            _compareToValue = CompareToValue;
        }

        public byte[] Encode()
        {
            MemoryStream oMS = new MemoryStream();
            oMS.WriteByte(_reasonID);
            byte[] bData = BitConverter.GetBytes(_sourceType);
            oMS.Write(bData, 0, 2);
            bData = BitConverter.GetBytes(_SPNCode);
            oMS.Write(bData, 0, 2);
            bData = BitConverter.GetBytes(_compareToValue);
            oMS.Write(bData, 0, 4);
            return oMS.ToArray();
        }
    }
}

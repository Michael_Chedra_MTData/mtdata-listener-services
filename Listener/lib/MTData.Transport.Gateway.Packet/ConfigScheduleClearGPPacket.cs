using System;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for ConfigScheduleClearGPPacket.
	/// </summary>
    [Serializable]
    public class ConfigScheduleClearGPPacket : GatewayProtocolPacket
	{
		public const byte CONFIG_SCHEDULE_CLEAR = 0xdd;

		public ConfigScheduleClearGPPacket(string serverTime_DateFormat) : base(serverTime_DateFormat)
		{
			cMsgType = CONFIG_SCHEDULE_CLEAR;
		}

		public ConfigScheduleClearGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base(p, serverTime_DateFormat)
		{
			cMsgType = CONFIG_SCHEDULE_CLEAR;
		}

		public new void Encode(ref Byte[] theData)
		{
			// Has no contents
			iLength = 0;
			mDataField = null;

			// And wrap it in the standard overhead bytes
			base.Encode(ref theData);
		}
	}	
}

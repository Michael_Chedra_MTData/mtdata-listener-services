using System;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Summary description for cIMIEData.
	/// </summary>
    [Serializable]
	public class cIMIEData
	{
		public bool bIsPopulated = false;
		public int i_HardwareType = 0;
		public string s_IMIE = "";
		public string s_SimNumber = "";
		public string s_CDMAESN = "";
		public int i_FleetID = 0; 
		public int i_VehicleID = 0; 
		public int i_SoftwareVer = 0; 
		public int i_SoftwareVerMajor = 0; 
		public int i_SoftwareVerMinor = 0; 
		public int i_IOBoxVerMajor = 0; 
		public int i_IOBoxVerMinor = 0;

		public cIMIEData()
		{
			bIsPopulated = false;
		}

		public cIMIEData CreateCopy()
		{
			cIMIEData oIMIEData = new cIMIEData();
			oIMIEData.bIsPopulated = this.bIsPopulated;
			oIMIEData.i_HardwareType = this.i_HardwareType;
			oIMIEData.s_IMIE = this.s_IMIE;
			oIMIEData.s_SimNumber = this.s_SimNumber;
			oIMIEData.s_CDMAESN = this.s_CDMAESN;
			oIMIEData.i_FleetID = this.i_FleetID;
			oIMIEData.i_VehicleID = this.i_VehicleID;
			oIMIEData.i_SoftwareVer = this.i_SoftwareVer;
			oIMIEData.i_SoftwareVerMajor = this.i_SoftwareVerMajor;
			oIMIEData.i_SoftwareVerMinor = this.i_SoftwareVerMinor;
			oIMIEData.i_IOBoxVerMajor = this.i_IOBoxVerMajor;
			oIMIEData.i_IOBoxVerMinor = this.i_IOBoxVerMinor;
			return oIMIEData;
		}

		public bool IsPopulated
		{
			get
			{
				return bIsPopulated;
			}
		}

		public string UpdateParameters()
		{
			string sRet = "UPDATE [T_GatewayUnit] SET [ListenPort] = '@Vers',  [SIMCardNumber]='@SIMCardNumber', [HardwareType]=@HardwareType, [IMIE]='@IMIE', [CDMAESN]='@CDMAESN' WHERE FleetID = @FleetID and VehicleID = @VehicleID";

			sRet = sRet.Replace("@HardwareType", Convert.ToString(i_HardwareType));
			sRet = sRet.Replace("@SIMCardNumber", s_SimNumber);
			sRet = sRet.Replace("@IMIE", s_IMIE);
			sRet = sRet.Replace("@CDMAESN", s_CDMAESN);
			sRet = sRet.Replace("@FleetID", Convert.ToString(i_FleetID));
			sRet = sRet.Replace("@VehicleID", Convert.ToString(i_VehicleID));
			sRet = sRet.Replace("@SerialNumber",Convert.ToString(i_VehicleID));
			sRet = sRet.Replace("@Vers", Convert.ToString(i_SoftwareVer) + "," +  Convert.ToString(i_SoftwareVerMajor) +  Convert.ToString(i_SoftwareVerMinor) + ":" + Convert.ToString(i_IOBoxVerMajor) + Convert.ToString(i_IOBoxVerMinor));
			return sRet;
		}

		public void SetValues(int iHardwareType, string sIMIE, string sSimNumber, string sCDMAESN, int iFleetID, int iVehicleID, int iSoftwareVer, int iSoftwareVerMajor, int iSoftwareVerMinor, int iIOBoxVerMajor, int iIOBoxVerMinor)
		{
			i_HardwareType = iHardwareType;
			s_IMIE = sIMIE;
			s_SimNumber = sSimNumber;
			s_CDMAESN = sCDMAESN;
			i_FleetID = iFleetID; 
			i_VehicleID = iVehicleID; 
			i_SoftwareVer = iSoftwareVer; 
			i_SoftwareVerMajor = iSoftwareVerMajor; 
			i_SoftwareVerMinor = iSoftwareVerMinor; 
			i_IOBoxVerMajor = iIOBoxVerMajor; 
			i_IOBoxVerMinor = iIOBoxVerMinor;
			bIsPopulated = true;
		}
	}
}

using System;

namespace MTData.Transport.Gateway.Packet
{
	/// <summary>
	/// Encapsulates a GPS_HISTORY packet.
	/// </summary>
    [Serializable]
    public class GPSHistoryGPPacket : GatewayProtocolPacket
	{
		// Types of message handled by this class
		public const	byte GPS_HISTORY_PACKET_MASK	= 0xe0;
		public const	byte GPS_HISTORY				= 0xe1; 

		// Members:
		public GPClock mClockList;
		public GPPositionFix mFixList;
        public GPStatus mStatusList;
        public GPDistance mDistance;
        public GPEngineData mEngineData;
        public GPExtendedValues mExtendedValues;
        public bool IsExtendedHistoryComplete = false;
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";

		public GPSHistoryGPPacket(string serverTime_DateFormat) : base (serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
            mClockList = new GPClock("History", _serverTime_DateFormat);
            mFixList = new GPPositionFix();
            mStatusList = new GPStatus();
            mDistance = new GPDistance();
            mEngineData = new GPEngineData();
            mExtendedValues = new GPExtendedValues();
		}

		public GPSHistoryGPPacket(GatewayProtocolPacket p, string serverTime_DateFormat) : base (p, serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
            mClockList = new GPClock("History", _serverTime_DateFormat);
            mFixList = new GPPositionFix();
            mStatusList = new GPStatus();
            mDistance = new GPDistance();
            mEngineData = new GPEngineData();
            mExtendedValues = new GPExtendedValues();
			if (Decode(p.mDataField) != null) 
			{	
				bIsPopulated = false;
			}
		}

        public string Decode(Byte[] theData)
        {
            string retMsg = null;
            int pos = 0;
            if (!bIsPopulated) retMsg = base.Decode(theData, 0);
            if (retMsg != null) return retMsg;

            #region Read the first clock

            //if the date is FF FF FF FF FF FF then this is a Extendend History complete packet
            if (theData.Length < 6 && theData[0] == 0xFF && theData[1] == 0xFF && theData[2] == 0xFF && theData[3] == 0xFF && theData[4] == 0xFF && theData[5] == 0xFF)
            {
                IsExtendedHistoryComplete = true;
                return null;
            }
            retMsg = mClockList.Decode(theData, pos);
            pos += GPClock.Length;
            #endregion
            #region Read the second clock
            if (retMsg == null) // No probs decoding clock field
            {
                mClockList = new GPClock("History", _serverTime_DateFormat);
                retMsg = mClockList.Decode(theData, pos);
                pos += GPClock.Length;
            }
            #endregion
            #region Read the GPS Position
            if (retMsg == null) // No probs decoding clock field
            {
                retMsg = mFixList.Decode(theData, pos);
            }
            pos += GPPositionFix.Length;
            #endregion
            #region Read the input status, spare and speed accumulator values
            if (retMsg == null) // No probs decoding clock field
            {
                retMsg = mStatusList.Decode(theData, pos);
                pos += mStatusList.Length;
            }
            #endregion
            #region Read the Max Speed, Speed acc and sample values
            if (retMsg == null) // No probs decoding clock field
            {
                retMsg = mDistance.Decode(theData, pos);
                pos += GPDistance.Length;
            }
            #endregion
            #region Read the engine values
            if (retMsg == null) // No probs decoding clock field
            {
                retMsg = mEngineData.Decode(theData, pos);
                pos += GPEngineData.Length;
            }
            #endregion
            #region Read the extended data values
            if (retMsg == null) // No probs decoding clock field
            {
                retMsg = mExtendedValues.Decode(theData, pos);
                pos += GPExtendedValues.Length;
            }
            #endregion
            return null;
        }
	}
}

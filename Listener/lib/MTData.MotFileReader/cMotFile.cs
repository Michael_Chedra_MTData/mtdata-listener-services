using System;
using System.Collections;
using MTData.Transport.Gateway.Packet;
using log4net;

namespace MTData.MotFileReader
{
    /// <summary>
    /// Summary description for Class1.
    /// </summary>
    public class cMotFile
    {
        #region Private Vars
        private MotorolaSRecordFile _oMotFile = null;
        private ILog _oLogging = LogManager.GetLogger(typeof(cMotFile));
        #endregion
        #region Public Vars
        /// <summary>
        /// DownloadVersion = The version number of this file your downloading (1 byte)
        ///	ProgramTotalCheckSum = This is the total checksum for the entire program, segments only.  (4 bytes) 
        ///	PacketCheckSum = This is the entire checksum of all the packets sent, which includes all header except for the checksum�s itself. (4 bytes) (DownloadCheckSum)
        ///	PacketCountTotal = The number of packets downloaded (2 Bytes)
        ///	ProgramByteTotal = This is the amount of total bytes downloaded. (4 bytes)
        ///	CheckSum = The checksum of the data bytes (1 byte)
        /// </summary>
        public ArrayList ParcelArray = null;
        public byte DownloadVersion = 0;
        public int ProgramTotalCheckSum = 0;
        public int PacketTotalCheckSum = 0;
        public int ProgramByteTotal = 0;
        public int PacketCountTotal = 0;
        public byte CheckSum = 0;
        #endregion
        public cMotFile(string sFileName)
        {
            //Default Block Size is 250
            CreateObject(sFileName, 250);
        }

        public cMotFile(string sFileName, int iBlockSize)
        {
            CreateObject(sFileName, iBlockSize);
        }

        private void CreateObject(string sFileName, int iBlockSize)
        {
            int iPos = 0;
            long lThisAddress = 0;
            long lNextAddress = 0;
            int iBlockCount = 1;
            int iPreferredDataLength = 0;
            int iCheckSum = 0;
            int iDownloadChecksum = 0;
            byte[] bData = null;
            byte[] bSendData = null;
            byte[] bConvert = null;

            try
            {
                iPreferredDataLength = iBlockSize;
                ParcelArray = new ArrayList();
                ProgramTotalCheckSum = 0;
                ProgramByteTotal = 0;

                _oMotFile = new MotorolaSRecordFile(sFileName);
                if (_oMotFile.FileIsGood)
                {
                    // A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
                    lThisAddress = _oMotFile.GetAddressOfRow(2);
                    bData = new byte[0];
                    while (_oMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref bData, ref lNextAddress, true, false) == true)
                    {
                        // Parcel.Data = [Msg Type][Download Version][Block Count][Address][Data][CheckSum]
                        bSendData = new byte[bData.Length + 9];
                        iPos = 0;
                        // Msg Type
                        bSendData[iPos++] = (byte)0x88;
                        // Download Version
                        bSendData[iPos++] = (byte)0x06;
                        // Block Count
                        bConvert = BitConverter.GetBytes(iBlockCount);
                        bSendData[iPos++] = (byte)((iBlockCount & 0xFF));
                        bSendData[iPos++] = (byte)((iBlockCount & 0xFF00) >> 8);
                        iBlockCount++;
                        // Address
                        bSendData[iPos++] = (byte)((lThisAddress & 0xFF0000) >> 16);
                        bSendData[iPos++] = (byte)((lThisAddress & 0xFF00) >> 8);
                        bSendData[iPos++] = (byte)((lThisAddress & 0xFF));
                        // MOT File data
                        for (int X = 0; X < bData.Length; X++)
                            bSendData[iPos++] = bData[X];

                        // Check Sum
                        iDownloadChecksum = 0;
                        for (int pos = 0; pos < iPos; pos++)
                        {
                            iDownloadChecksum += bSendData[pos];
                        }
                        bSendData[iPos++] = (byte)((iDownloadChecksum & 0xFF));
                        bSendData[iPos++] = (byte)((iDownloadChecksum & 0xFF00) >> 8);

                        // Calculate the checksum from the bytes of the mot file of the 250 bytes
                        iCheckSum = SumOfBytes(bData);

                        // Calculate the overall program checksum, just the mot file data, those 250 bytes
                        ProgramTotalCheckSum += iCheckSum;

                        // Create a new object to hold the data and checksum value.
                        cParcel oParcel = new cParcel(bSendData, iDownloadChecksum);

                        //checksum of all the check sums
                        PacketTotalCheckSum += iDownloadChecksum;

                        // Add the Parcel (Mot file fragment) to the ParcelArray list.
                        ParcelArray.Add(oParcel);

                        // And count the program bytes:
                        ProgramByteTotal += (short)bData.Length;

                        // keep looping while there is data
                        lThisAddress = lNextAddress;
                        // Clear the old byte data.
                        bData = new byte[0];

                        PacketCountTotal++;
                    }
                }
                else
                {
                    if (_oLogging != null)
                        _oLogging.Error("Error reading Mot File '" + sFileName + "'", null);
                }
            }
            catch (System.Exception ex)
            {
                if (_oLogging != null) _oLogging.Error("Error reading Mot File '" + sFileName + "'", ex);
            }
        }

        public bool FileIsGood
        {
            get
            {
                try
                {
                    if (_oMotFile != null)
                        return _oMotFile.FileIsGood;
                    else
                        return false;
                }
                catch (System.Exception ex)
                {
                    if (_oLogging != null) _oLogging.Error("Error checking Mot File.", ex);
                }
                return false;
            }
        }

        private int SumOfBytes(byte[] bData)
        {
            int iTotal = 0;
            try
            {
                foreach (byte bByte in bData)
                {
                    iTotal += Convert.ToInt32(bByte);
                }
            }
            catch (System.Exception ex)
            {
                if (_oLogging != null) _oLogging.Error("Error calculating checksum.", ex);
            }
            return iTotal;
        }
    }
}

using System;

namespace MTData.MotFileReader
{
	/// <summary>
	/// Summary description for cParcel.
	/// </summary>
	public class cParcel
	{
		// Parcel.Data = [Msg Type][Download Version][Block Count][Address][Data][CheckSum]
		public byte[] Data = null;
		public int CheckSum = 0;

		public cParcel(byte[] bData, int iCheckSum)
		{
			Data = bData;
			CheckSum = iCheckSum;
		}
	}
}

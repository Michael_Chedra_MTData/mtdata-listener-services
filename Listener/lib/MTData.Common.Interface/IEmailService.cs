﻿namespace MTData.Common.Interface
{
    using System.Collections.Generic;
    using System.Runtime.Serialization;
    using System.ServiceModel;

    /// <summary>
    /// Email service interface
    /// </summary>
    [ServiceContract]
    public interface IEmailService
    {
        /// <summary>
        /// The GetBlackList
        /// </summary>
        /// <returns>The <see cref="List{string}"/></returns>
        [OperationContract]
        [FaultContract(typeof(ConfigInvalidFault))]
        List<string> GetBlackList();

        /// <summary>
        /// The IsBlackListed
        /// </summary>
        /// <param name="address">The <see cref="string"/></param>
        /// <returns>The <see cref="bool"/></returns>
        [OperationContract]
        [FaultContract(typeof(ConfigInvalidFault))]
        bool IsBlackListed(string address);

        /// <summary>
        /// The SendEmail
        /// </summary>
        /// <param name="message">The <see cref="EmailMessage"/></param>
        [OperationContract]
        [FaultContract(typeof(InvalidOperationFault))]
        [FaultContract(typeof(ArgumentNullFault))]
        [FaultContract(typeof(SmtpFault))]
        [FaultContract(typeof(SmtpFailedRecipientsFault))]
        [FaultContract(typeof(ConfigInvalidFault))]
        void SendEmail(EmailMessage message);
    }

    /// <summary>
    /// Defines the <see cref="EmailMessage" />
    /// </summary>
    [DataContract]
    public class EmailMessage
    {
        /// <summary>
        /// Gets or sets the Attachments
        /// </summary>
        [DataMember]
        public EmailAttachment[] Attachments
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the BodyHtml
        /// </summary>
        [DataMember]
        public string BodyHtml
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the BodyPlain
        /// </summary>
        [DataMember]
        public string BodyPlain
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the CC
        /// </summary>
        [DataMember]
        public string[] CC
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the From
        /// </summary>
        [DataMember]
        public string From
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the FromDisplayName
        /// TODO consider creating composite class that contains email address + displayName.
        /// </summary>
        [DataMember]
        public string FromDisplayName
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Subject
        /// </summary>
        [DataMember]
        public string Subject
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the To
        /// </summary>
        [DataMember]
        public string[] To
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Defines the <see cref="EmailAttachment" />
    /// </summary>
    [DataContract]
    public class EmailAttachment
    {
        /// <summary>
        /// Gets or sets the ContentBase64
        /// </summary>
        [DataMember]
        public string ContentBase64
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Filename
        /// </summary>
        [DataMember]
        public string Filename
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Defines the <see cref="ArgumentNullFault" />
    /// </summary>
    [DataContract]
    public class ArgumentNullFault
    {
    }

    /// <summary>
    /// Defines the <see cref="InvalidOperationFault" />
    /// </summary>
    [DataContract]
    public class InvalidOperationFault
    {
    }

    /// <summary>
    /// Defines the <see cref="SmtpFault" />
    /// </summary>
    [DataContract]
    public class SmtpFault
    {
    }

    /// <summary>
    /// Defines the <see cref="ConfigInvalidFault" />
    /// </summary>
    [DataContract]
    public class ConfigInvalidFault
    {
    }

    /// <summary>
    /// Defines the <see cref="SmtpFailedRecipientsFault" />
    /// </summary>
    [DataContract]
    public class SmtpFailedRecipientsFault : SmtpFault
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="SmtpFailedRecipientsFault"/> class.
        /// </summary>
        /// <param name="failedRecipients">The <see cref="SmtpFailedRecipient[]"/></param>
        public SmtpFailedRecipientsFault(SmtpFailedRecipient[] failedRecipients)
        {
            this.FailedRecipients = failedRecipients;
        }

        /// <summary>
        /// Gets or sets the FailedRecipients
        /// </summary>
        [DataMember]
        public SmtpFailedRecipient[] FailedRecipients
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets the Testing
        /// </summary>
        [DataMember]
        public string Testing
        {
            get;
            set;
        }
    }

    /// <summary>
    /// Defines the <see cref="SmtpFailedRecipient" />
    /// </summary>
    [DataContract]
    public class SmtpFailedRecipient
    {
        /// <summary>
        /// Gets or sets the Address
        /// </summary>
        [DataMember]
        public string Address
        {
            get;
            set;
        }

        /// <summary>
        /// Gets or sets a value indicating whether IsBlackListed
        /// </summary>
        [DataMember]
        public bool IsBlackListed
        {
            get;
            set;
        }
    }
}

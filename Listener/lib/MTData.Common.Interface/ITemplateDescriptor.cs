using System;

namespace MTData.Common.Interface
{
	/// <summary>
	/// This interface will describe the template to be processed.
	/// </summary>
	public interface ITemplateDescriptor
	{
		string FormattingString { get; }
		int FieldCount { get; }
		string this[int fieldIndex] { get; }
	}
}

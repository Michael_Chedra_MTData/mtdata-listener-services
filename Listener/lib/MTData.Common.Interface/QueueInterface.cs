using System;
using System.Threading;

namespace MTData.Common.Interface
{
	/// <summary>
	/// This interface identifies the basic methods that will be supported by any queue implementation.
	/// This allows multiple queue implementations to be used, including GUI update queues, and branching
	/// or routing queues.
	/// </summary>
	public interface QueueInterface
	{
		object SyncRoot {get;}
		void Enqueue(object entry);
		object Dequeue();
		object DequeueEx(object entry);
		object Peek();
		int Count {get;}
        int StatsCountAndReset {get;}
        int StatsDeQueueCountAndReset { get; }
    }

    /// <summary>
    /// This interface will allow a queue to be thread synchronised
    /// to avoid unneccessary thread activity
    /// </summary>
    public interface SignaledQueueInterface
	{
		void WaitOne();
		void Set();
	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Common.Interface
{
    public interface ILoader
    {
        /// <summary>
        /// Load and instantiate a class instance based on an xml node supplied.
        /// There are two methods used to configure the type to be loaded.
        ///		type Attribute, specifying the full typename and assembly reference
        ///		assembly and class Attributes, which can be combined to provide the typename
        /// </summary>
        /// <param name="configNode"></param>
        /// <returns></returns>
        object Create(object context, System.Xml.XmlNode configNode);

        /// <summary>
        /// Create an instance of the typename supplied.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        object Create(object context, string typeName);

        /// <summary>
        /// Create an instance of the typename supplied, and configure it from the configNode
        /// </summary>
        /// <param name="context"></param>
        /// <param name="typeName"></param>
        /// <param name="configNode"></param>
        /// <returns></returns>
        object Create(object context, string typeName, System.Xml.XmlNode configNode);

        /// <summary>
        /// This method will create an instance of a givne type, and configure it.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="configNode"></param>
        /// <returns></returns>
        object Create(object context, Type type, System.Xml.XmlNode configNode);

        /// <summary>
        /// This method creates an instance of the specified type and configures it with the 
        /// subnode with the name specified in the configNodeName parameter
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="configNode"></param>
        /// <param name="configNodeName"></param>
        /// <returns></returns>
        object Create(object context, Type type, System.Xml.XmlNode configNode, string configNodeName);

        /// <summary>
        /// This is the base path from which an assembly should be loaded if it is not found through standard mechanisms.
        /// </summary>
        string CodeBase { get; set; }
    }
}

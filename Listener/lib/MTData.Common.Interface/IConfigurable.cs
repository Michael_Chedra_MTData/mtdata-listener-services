using System;

namespace MTData.Common.Interface
{
	/// <summary>
	/// This interface will be implemented by any Dynamically loaded object if it 
	/// supports being configured by a series of Xml Nodes
	/// When configuring such a class instance, the name of the node in the 
	/// configuration will be that of the class being configured.
	/// </summary>
	public interface IConfigurable
	{
		/// <summary>
		/// This method will be called by the loader an instance of the object 
		/// has been created and the loader understands that it supports this 
		/// interface.
		/// </summary>
		/// <param name="context">This is an application supplied context object</param>
		/// <param name="configNode">This is the parent node fo the configuration. Generally the name of this node will be that of the class being configured.</param>
		void Configure(object context, System.Xml.XmlNode configNode);
	}
}

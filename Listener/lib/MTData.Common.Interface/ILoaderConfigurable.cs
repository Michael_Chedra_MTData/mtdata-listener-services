using System;

namespace MTData.Common.Interface
{
	/// <summary>
	/// This interface allows the same functionality as the IConfigurable interface,
	/// but takes the parent loader as a parameter in the configure method.
	/// It allows child instantiations to use the same instance of the loader for efficiency
	/// </summary>
	public interface ILoaderConfigurable
	{
		/// <summary>
		/// This method will be called by the loader an instance of the object 
		/// has been created and the loader understands that it supports this 
		/// interface.
		/// </summary>
		/// <param name="loader">The loader class used to load it into memory</param>
		/// <param name="context">This is an application supplied context object</param>
		/// <param name="configNode">This is the parent node fo the configuration. Generally the name of this node will be that of the class being configured.</param>
		void Configure(ILoader loader, object context, System.Xml.XmlNode configNode);
	}
}

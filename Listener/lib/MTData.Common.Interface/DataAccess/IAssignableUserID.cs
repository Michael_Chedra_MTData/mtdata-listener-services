using System;

namespace MTData.Common.Interface.DataAccess
{
	/// <summary>
	/// This interface allows the provider instantiation to specify a userid
	/// or not.
	/// </summary>
	public interface IAssignableUserID
	{
		int UserID {get; set;}
	}
}

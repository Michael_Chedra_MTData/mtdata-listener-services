using System;

namespace MTData.Common.Interface.DataAccess
{
	/// <summary>
	/// This class identifies object that support having the DSN assigned to them.
	/// This is the default behaviour for all tracking providers.
	/// </summary>
	public interface IAssignableDSN
	{
		string DSN {get; set;}
	}
}

using System;

namespace MTData.Common.Interface.DataAccess
{
	/// <summary>
	/// This interface identifies standard methods that all DAL objects must support to 
	/// be overridable.
	/// </summary>
	public interface IContextConfigurableProvider
	{
		/// <summary>
		/// Configure the instance
		/// </summary>
		/// <param name="contextProvider"></param>
		void Configure(IContextProvider contextProvider);
	}
}

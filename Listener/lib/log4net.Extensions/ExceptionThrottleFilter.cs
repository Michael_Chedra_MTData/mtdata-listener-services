﻿using System;
using log4net.Core;
using log4net.Filter;

namespace log4net.Extensions
{
    public class ExceptionThrottleFilter : FilterSkeleton
    {
        private DateTime _lastExceptionTime = DateTime.MinValue;
        private Type _lastExceptionType;
        private string _lastExceptionMessage;

        public int ThresholdSeconds { get; set; }

        public override FilterDecision Decide(LoggingEvent loggingEvent)
        {
            var currentException = loggingEvent.ExceptionObject;
            if (currentException == null)
            {
                return FilterDecision.Neutral;
            }

            if (currentException.GetType() == _lastExceptionType)
            {
                var timeSinceLastException = loggingEvent.TimeStamp.Subtract(_lastExceptionTime).TotalSeconds;
                if (timeSinceLastException > ThresholdSeconds ||
                    loggingEvent.MessageObject.ToString() != _lastExceptionMessage)
                {
                    SetLastExceptionDetails(loggingEvent);
                    return FilterDecision.Neutral;
                }
                return FilterDecision.Deny;
            }

            SetLastExceptionDetails(loggingEvent);
            return FilterDecision.Neutral;
        }

        private void SetLastExceptionDetails(LoggingEvent e)
        {
            _lastExceptionType = e.ExceptionObject.GetType();
            _lastExceptionMessage = e.MessageObject.ToString();
            _lastExceptionTime = e.TimeStamp;
        }
    }
}

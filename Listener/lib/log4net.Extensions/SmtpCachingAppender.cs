using System;
using System.Collections.Generic;
using log4net.Appender;
using log4net.Core;

namespace log4net.Extensions
{
      /// <summary>
      ///         Caching version of the standard log4net SmtpAppender. This appender will
      ///         cache log events that are to be sent out via Smtp and send them in block.
      /// 
      ///         Configuration options:
      /// 
      ///         <FlushInterval value="hh:mm:ss" />
      ///               Indicates the periodic interval for log events flushing (e.g. sending and e-mail), specified
      ///               as a time span.  If the value of FlushInterval is 0:0:0 (zero), periodic interval flushing
      ///               is surpressed.  Default value:  0:5:0 (5 minutes).
      /// 
      ///         <FlushCount value="x" />
      ///               Indicates the number of log events received by the appender which will trigger flushing
      ///               (e.g. sending and e-mail).  If the value FlushCount is 0, buffer flush triggering based
      ///               the number of log events received is surpressed. Default value:  20 events.
      /// 
      ///         <IncludeContextLogEvents value="[true/false]"/>"
      ///               True if context log events are included in the buffered log event cache.  Cache events are
      ///               those that are of a lower level than that specified for triggering the SmtpCachingAppender.
      ///               False if no context log events are included. Default value:  true.
      /// 
      ///         <MaxBufferSize value="x"/>
      ///               Maximum number of log events, both SmtpCachingAppender events and context events to save
      ///               in the cache buffer.  If more than MaxBufferSize events are received before the flushing
      ///               criteria is met, only the newest MaxBufferSize events are saved in the buffer.  A value
      ///               of 0 indicates no limit in the size of the cache.  Default value:  0 (no limit)
      /// 
      ///         Sample SmtpCachingAppender configuration (in addition to all standard SmtpAppender options).
      ///               Note the namespace and assembly name for the appender.
      /// 
      ///         <appender name="SmtpCachingAppender" type="log4net.Extensions.SmtpCachingAppender, Utilities">
      ///               . . .
      ///               <FlushInterval value="00:05:00" />
      ///               <FlushCount value="20" />
      ///               <IncludeContextLogEvents value="false"/>"
      ///               <MaxBufferSize value="3"/>
      ///         </appender>
      ///
      /// </summary>
      public class SmtpCachingAppender : SmtpAppender
      {
            // Configuration options
            int _flushCount = 20;
            TimeSpan _flushInterval = new TimeSpan(0, 5, 0);
            bool _includeContextLogEvents = true;
            int _maxBufferSize = 0;

            // Appender state data
            System.Threading.Timer _timer;
            bool _timedFlush = false;
            int _numCachedMessages = 0;
            List<LoggingEvent> _loggingEvents = new List<LoggingEvent>();
            /// <summary>
            ///         TimeSpan indicating the maximum period of time elapsed before sending
            ///         any cached SMTP log events.
            /// </summary>
            public TimeSpan FlushInterval
            {
                  get { return this._flushInterval; }
                  set { this._flushInterval = value; }
            }

          /// <summary>
            ///         Maximium number of SMTP events to cache before sending via SMTP.
            /// </summary>
            public int FlushCount
            {
                  get { return this._flushCount; }
                  set { this._flushCount = value; }
            }

          public int MaxBufferSize
            {
                  get { return this._maxBufferSize; }
                  set { this._maxBufferSize = value; }
            }

          public bool IncludeContextLogEvents
            {
                  get { return this._includeContextLogEvents; }
                  set { this._includeContextLogEvents = value; }
            }

            /// <summary>
            ///         Create a timer that fires to force flushing cached log events
            ///         via SMTP at a specified interval.
            /// </summary>
            public override void ActivateOptions()
            {
                  if (this._flushInterval > TimeSpan.Zero)
                  {
                        this._timer = new System.Threading.Timer(
                              new System.Threading.TimerCallback(OnTimer),
                              null,
                              this._flushInterval,
                              this._flushInterval);
                  }
                  base.ActivateOptions();
            }
 
            void OnTimer(Object stateInfo)
            {
                  this._timedFlush = true;
                  Flush(true);
            }
            protected override void SendBuffer(LoggingEvent[] events)
            {
                  if (this._includeContextLogEvents)
                  {
                        this._loggingEvents.AddRange(events);
                  }
                  else
                  {
                        this._loggingEvents.Add(events[events.Length - 1]);
                  }
                  if (this._maxBufferSize != 0)
                  {
                        int numRemoved = this._loggingEvents.Count - this._maxBufferSize;
                        if ((numRemoved > 0) && (numRemoved <= this._loggingEvents.Count))
                        {
                              this._loggingEvents.RemoveRange(0,numRemoved);
                        }
                  }
                  this._numCachedMessages++;
                  if (((this._flushCount != 0) && (this._numCachedMessages >= this._flushCount)) || this._timedFlush)
                  {
                        if (this._loggingEvents.Count > 0)
                        {
                              LoggingEvent[] bufferedEvents = this._loggingEvents.ToArray();
                              base.SendBuffer(bufferedEvents);
                              this._loggingEvents.Clear();
                        }
                        // Reset cache buffer conditions.
                        this._numCachedMessages = 0;
                        this._timedFlush = false;
                  }
            }
      }
}
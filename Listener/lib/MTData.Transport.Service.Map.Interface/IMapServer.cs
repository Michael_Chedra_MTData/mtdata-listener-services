﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Transport.Service.Map.Interface
{
    public interface IMapServer : IDisposable
    {

        bool Available { get; }

        bool ProcessingRequest { get; set; }

        ulong Key { get; }

        bool TryReconnect();

        TcpClient OpenConnection();

        void CloseConnection(ref TcpClient client);
    }
}

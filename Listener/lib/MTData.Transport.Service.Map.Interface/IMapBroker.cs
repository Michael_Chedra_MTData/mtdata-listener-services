using System;
using System.Collections.Generic;
using System.Configuration;

namespace MTData.Transport.Service.Map.Interface
{
    public interface IMapBroker : IDisposable
    {
        int ProcessingTimeoutMs { get; set; }

        // May not be supported in all forms for MapBroker
        void ProcessRequest(IMapDataRequest request, int threadId);

        Dictionary<ulong, IMapServer> ServiceList { set; }
        
    }
}

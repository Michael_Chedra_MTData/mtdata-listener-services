namespace MTData.Transport.Service.Map.Interface
{
    public interface IRequestHandler
    {
        object Execute(object request, object context);
    }
}
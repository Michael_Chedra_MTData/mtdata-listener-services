using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// interface used by a device layer to encryt/unencrypt data
    /// </summary>
    public interface IDeviceLayerEncrypter
    {
        /// <summary>
        /// Encrypt data
        /// </summary>
        /// <param name="unitId">the unit id of the vehicle which sends/recevies packet</param>
        /// <param name="data">the unencrypted data</param>
        /// <returns>the encrypted data</returns>
        byte[] EncryptData(int unitId, byte[] data);

        /// <summary>
        /// Unencrypt data
        /// </summary>
        /// <param name="unitId">the unit id of the vehicle which sends/recevies packet</param>
        /// <param name="encrytedData">the encrypted data</param>
        /// <returns>the unencrypted data</returns>
        byte[] UnencryptData(int unitId, byte[] encrytedData);
    }
}

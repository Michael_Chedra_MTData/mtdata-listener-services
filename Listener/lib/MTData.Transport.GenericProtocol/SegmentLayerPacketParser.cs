using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// this class will parse a Segment Layer packet when given a byte at a time
    /// </summary>
    public class SegmentLayerPacketParser
    {
        /// <summary>
        /// enum of packet states
        /// </summary>
        private enum PacketState
        {
            SOP,
            Type,
            Version,
            Length,
            Data,
            CheckSum,
        }

        /// <summary>
        /// enum of add byte result
        /// </summary>
        public enum AddByteResult
        {
            ReadingPacket,
            PacketReady,
            Error
        }

        #region private fields
        /// <summary>
        /// the buffer of the bytes read
        /// </summary>
        private MemoryStream _buffer;
        /// <summary>
        /// teh current packet state (indicates what is being read)
        /// </summary>
        private PacketState _packetState;
        /// <summary>
        /// the last packet successfully read
        /// </summary>
        private SegmentLayer _lastPacket;
        /// <summary>
        /// the last error message
        /// </summary>
        private string _lastError;
        /// <summary>
        /// the current checksum of packet data
        /// </summary>
        private byte _checksum;
        /// <summary>
        /// the length of the data
        /// </summary>
        private int _dataLength;
        /// <summary>
        /// the number of bytes to shift when reading the length
        /// </summary>
        private int _shift;
        /// <summary>
        /// the number of bytes still to be read in an array (used by Data)
        /// </summary>
        private int _length;
        #endregion

        #region properties
        public SegmentLayer LastPacket { get { return _lastPacket; } }
        public string LastError { get { return _lastError; } }
        #endregion

        #region constructor
        public SegmentLayerPacketParser()
        {
            _buffer = new MemoryStream();
            _packetState = PacketState.SOP;
            _lastError = string.Empty;
            _lastPacket = new SegmentLayer();
        }
        #endregion

        #region public methods
        /// <summary>
        /// reset the packet reader
        /// </summary>
        public void Reset()
        {
            _packetState = PacketState.SOP;
            _length = 0;
            _dataLength = 0;
            _shift = 0;
            if (_buffer != null)
            {
                _buffer.Close();
                _buffer = null;
            }
            _buffer = new MemoryStream();
        }

        /// <summary>
        /// add a byte to the reader
        /// </summary>
        /// <param name="b">the byte to add</param>
        /// <returns>the result of reading that byte</returns>
        public AddByteResult AddByte(byte b)
        {
            switch (_packetState)
            {
                case PacketState.SOP:
                    if (b == _lastPacket.StartOfPacket)
                    {
                        _buffer.WriteByte(b);
                        _packetState = PacketState.Type;
                        _checksum = b;
                    }
                    else
                    {
                        _lastError = string.Format("Wrong Start of Packet byte, expecting {0}, got {1}", _lastPacket.StartOfPacket, b);
                        return AddByteResult.Error;
                    }
                    break;
                case PacketState.Type:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    if ((b & 0x80) == 0)
                    {
                        _packetState = PacketState.Version;
                    }
                    break;
                case PacketState.Version:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _packetState = PacketState.Length;
                    break;
                case PacketState.Length:
                    _buffer.WriteByte(b);
                    _checksum += b;

                    _dataLength |= (b & 0x7F) << _shift;
                    _shift += 7;

                    if ((b & 0x80) == 0)
                    {
                        if (_dataLength > 0)
                        {
                            _packetState = PacketState.Data;
                            _length = _dataLength;
                        }
                        else
                        {
                            _packetState = PacketState.CheckSum;
                        }
                    }
                    break;
                case PacketState.Data:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _length--;
                    if (_length == 0)
                    {
                        _packetState = PacketState.CheckSum;
                    }
                    break;
                case PacketState.CheckSum:
                    _buffer.WriteByte(b);
                    _packetState = PacketState.SOP;
                    if (_checksum == b)
                    {
                        try
                        {
                            _lastPacket = new SegmentLayer();
                            byte[] data = _buffer.ToArray();
                            int read = _lastPacket.Decode(data);
                            if (read == data.Length)
                            {
                                return AddByteResult.PacketReady;
                            }
                            else
                            {
                                _lastError = string.Format("Decoding packet only read {0} bytes instaed of {1}", read, data.Length);
                                return AddByteResult.Error;
                            }
                        }
                        catch (Exception exp)
                        {
                            _lastError = exp.Message;
                            return AddByteResult.Error;
                        }
                    }
                    else
                    {
                        _lastError = string.Format("Checksum invalid, expecting {0}, got {1}", _checksum, b);
                        return AddByteResult.Error;
                    }
            }
            return AddByteResult.ReadingPacket;
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// Class that describes a unit 
    /// Either its unitId or combination of SerialNumber and HardwareType
    /// </summary>
    public class UnitKey
    {
        private int _unitId;

        private long _serialNumber;

        private HardwareType _hardware;

        public int UnitId { get { return _unitId; } }

        public long SerialNumber { get { return _serialNumber; } }

        public HardwareType Hardware { get { return _hardware; } }

        public UnitKey(IMccPacket packet)
        {
            _unitId = packet.UnitId;
            _hardware = packet.Hardware;
            _serialNumber = packet.SerialNumber;
        }

        public UnitKey(DeviceLayer layer)
        {
            _unitId = layer.UnitId;
            _hardware = layer.Hardware;
            _serialNumber = layer.SerialNumber;
        }

        public UnitKey(int unitId)
        {
            _unitId = unitId;
            _hardware = HardwareType.Unknown;
            _serialNumber = 0;
        }

        public UnitKey(long serialNumber, HardwareType hardware)
        {
            _unitId = 0;
            _hardware = hardware;
            _serialNumber = serialNumber;
        }

        public static bool operator ==(UnitKey x, UnitKey y)
        {
            if (object.ReferenceEquals(x, null))
            {
                return object.ReferenceEquals(y, null);
            }

            return x.Equals(y);
        }

        public static bool operator !=(UnitKey x, UnitKey y)
        {
            if (object.ReferenceEquals(x, null))
            {
                return !object.ReferenceEquals(y, null);
            }

            return !x.Equals(y);
        }

        public override string ToString()
        {
            if (_hardware == HardwareType.Unknown)
            {
                return _unitId.ToString();
            }
            else
            {
                return _serialNumber.ToString();
            }
        }

        public override bool Equals(object obj)
        {
            UnitKey key = obj as UnitKey;
            if (key != null)
            {
                return _unitId == key._unitId && _serialNumber == key._serialNumber && _hardware == key._hardware;
            }

            return false;
        }

        public override int GetHashCode()
        {
            if (_hardware == HardwareType.Unknown)
            {
                return _unitId.GetHashCode();
            }
            else
            {
                return _serialNumber.GetHashCode();
            }
        }

        public class EqualityComparer : IEqualityComparer<UnitKey>
        {
            public bool Equals(UnitKey x, UnitKey y)
            {
                return x._unitId == y._unitId && x._serialNumber == y._serialNumber && x._hardware == y._hardware;
            }

            public int GetHashCode(UnitKey obj)
            {
                return obj.GetHashCode();
            }
        }
    }
}

using System;
using System.Collections;
#if !WindowsCE && !Android && !NETSTANDARD
using System.Configuration;
#endif
using System.Text;
using MTData.Transport.GenericProtocol;
#if !Android
//using log4net;
#endif

namespace MTData.Transport.GenericProtocol.OldProtocol
{
	/// <summary>
	/// Summary description for MCCPacket.
	/// </summary>
	public sealed class MCCPacket : IMccPacket
	{
		#region Variables
		public static readonly int MaxPayloadLength = 1400;
		private static readonly int HeaderLength = 10;
		private static readonly int TrailerLength = 3;
		private static readonly int AckNakLength = 5;
		private static readonly int OverheadLength = HeaderLength + TrailerLength;
		public static readonly int MaxTotalLength = MaxPayloadLength + OverheadLength;
        private static bool bCheckCRC = true;
        private static bool bCheckedCRCSetting = false;
		public const byte SOH = 0x01;
		private const byte EOT = 0x04;

		public  const byte ACK = 0xAC;
		public const byte NAK = 0xFF;
		public const byte DATA_PKT_TYPE = (byte) 'D';
        public const byte SIGN_OFF_TYPE = (byte)'X';
        public const byte KEEPALIVE_TYPE = (byte)'K';
		
		public byte[] CRCBytes
		{
			get
			{
				return bRaw;
			}
			set
			{
				if (value.Length > 3)
				{
					int iLen = value.Length - 3;
					bRaw = new byte[iLen];
					for(int X = 0; X < iLen; X++)
					{
						bRaw[X] = value[X];
					}
				}
			}
		}

		public bool IsInvalid
		{
			get
			{ 
				return (sFailureReason.Length > 0);
			}
		}
        public static bool CheckCRC
        {
            get
            {
                try
                {
                    if (!bCheckedCRCSetting)
                    {
#if !WindowsCE && !Android && !NETSTANDARD
                        if (ConfigurationManager.AppSettings["CheckCRC"] != null)
                        {
                            if (ConfigurationManager.AppSettings["CheckCRC"].ToUpper() == "FALSE")
                            {
                                bCheckCRC = false;
                            }
                        }
#else
                        bCheckCRC = true;
#endif
                        
                        bCheckedCRCSetting = true;
                    }
                }
                catch (System.Exception)
                {
                    bCheckCRC = true;
                }
                return bCheckCRC;
            }
        }
		private string sFailureReason;
		internal string FailureReason
		{
			get
			{
				return sFailureReason;
			}
		}

		public byte cMessageType;
		private byte[] aPayload;
		public byte[] Payload
		{
			get
			{
				return aPayload;
			}
		}
		private int iPayloadLength;

		public int PacketLength
		{
			get
			{
				return iPayloadLength + OverheadLength;
			}
		}

		public uint iUnitId;
		public int iPacketNumber;
		public int iPacketTotal;
		public int iSequenceNumber;
		public byte cSpare;
		public byte[] bRaw;
		#endregion

		public MCCPacket()
		{
			Initialise(null, 1,1, DATA_PKT_TYPE);
		}

		private MCCPacket(string sFailReason)
		{
			sFailureReason = sFailReason;
		}

		public MCCPacket(byte[] payload)
		{
			Initialise(payload, 1, 1, DATA_PKT_TYPE);
		}

		public MCCPacket(byte[] payload, int pktNumber, int pktTotal)
		{
			Initialise(payload, pktNumber, pktTotal, DATA_PKT_TYPE); // Data);
		}

		public MCCPacket(byte[] payload, int pktNumber, int pktTotal, byte cPktType)
		{
			Initialise(payload, pktNumber, pktTotal, cPktType);
		}

		private void Initialise(byte[] payload, int pktNumber, int pktTotal, byte cPktType)
		{
			this.cMessageType = cPktType;
			aPayload = payload;
			if (payload != null) iPayloadLength = payload.Length;
			else iPayloadLength = 0;
			iUnitId = 0;
			iPacketNumber = pktNumber;
			iPacketTotal = pktTotal;
			iSequenceNumber = 0;
			cSpare = 0;
			sFailureReason = "";
		}

		public byte[] ToByteArray()
		{
			int encPos = 0;
			byte[] aData = new byte[iPayloadLength + OverheadLength];

			aData[encPos++] = SOH;
			aData[encPos++] = cMessageType;
			aData[encPos++] = (byte) ((iUnitId & 0xFF00) >> 8);
			aData[encPos++] = (byte) (iUnitId & 0xFF);
			
			//if (iSequenceNumber == 0) iSequenceNumber = 1;
			//if (iSequenceNumber >= 255) iSequenceNumber = 1;

			aData[encPos++] = (byte) (iSequenceNumber & 0xFF);
			if ((cMessageType == ACK) || (cMessageType == NAK)) 
			{
				byte[] ackNak = new byte[AckNakLength];
				Array.Copy(aData, 0, ackNak, 0, AckNakLength);
				return ackNak;
			}
			aData[encPos++] = (byte) ((iPayloadLength & 0xFF00) >> 8);
			aData[encPos++] = (byte) (iPayloadLength & 0xFF);
			aData[encPos++] = (byte) (iPacketNumber & 0xFF);
			aData[encPos++] = (byte) (iPacketTotal & 0xFF);
			
			aData[encPos++] = cSpare;

			if ((aPayload != null) && (aPayload.Length > 0))
			{
				aPayload.CopyTo(aData,encPos);
				encPos += aPayload.Length;
			}

			int calcCrc = CalculateCheckSum(aData, 0, encPos);
			aData[encPos++] = (byte) ((calcCrc & 0x0000FF00) >> 8);
			aData[encPos++] = (byte) (calcCrc & 0x000000FF);
			aData[encPos++] = EOT;

			return aData;
		}

		private static int CalculateCheckSum(byte[] data, int iStartPos, int iEndPos)
		{
			byte[] bConvert = new byte[4];
			ulong iChecksum = 0;
			int iRet = 0;

			for (int X = iStartPos; X < iEndPos; X++)
			{
				iChecksum += (ulong) data[X];
			}
			bConvert = BitConverter.GetBytes(iChecksum);
			bConvert[2] = (byte) 0x00;
			bConvert[3] = (byte) 0x00;
			iRet = BitConverter.ToInt32(bConvert, 0);
			return iRet;
		}
		private static int CalculateCRC(byte[] data, int iStartPos, int iEndPos)
		{

			// Update the CRC for transmitted and received data using
			// the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1).
			int crc = 0;

			for (int pos = iStartPos; pos < (iEndPos - iStartPos); pos++)
			{
				crc  = (byte)(crc >> 8) | (crc << 8);
				crc ^= data[pos];
				crc ^= (byte)(crc & 0xff) >> 4;
				crc ^= (crc << 8) << 4;
				crc ^= ((crc & 0xff) << 4) << 1;
			}
			return crc & 0xFFFF;

			/*
			// Update the CRC for transmitted and received data using
			// the CCITT 16bit algorithm (X^16 + X^12 + X^5 + 1).
			byte[] crc = new byte[2];
			byte[] bConvert = new byte[4];
			byte bTemp = (byte) 0x00;

			for (int pos = iStartPos; pos < (iEndPos - iStartPos); pos++)
			{
				bTemp = crc[1];
				crc[1] = crc[0];
				crc[0] = bTemp;
				// crc  = (byte)(crc >> 8) | (crc << 8);

				crc[0] ^= data[pos];
				// crc  ^= data[pos];

				bTemp = crc[0];
				bTemp >>= 4;
				crc[0] = bTemp & 0xF0;
				bTemp = crc[1];				
				bTemp <<= 4;
				bTemp &= 0x0F;
				crc[0] = bTemp;
				
				//crc ^= (byte)(crc & 0xff) >> 4;

				
				//crc ^= (crc << 8) << 4;
				crc ^= ((crc & 0xff) << 4) << 1;
			}

			bConvert[0] = crc[0];
			bConvert[1] = crc[1];
			bConvert[2] = (byte) 0x00;
			bConvert[3] = (byte) 0x00;
			return BitConverter.ToInt32(bConvert);
			*/
		}

		private static int ClearTopTwoBytesOfInt(int iValue)
		{
			// This function clears the top two bytes of an int.
			byte[] bConvert = new byte[4];

			bConvert = BitConverter.GetBytes(iValue);
			bConvert[2] = (byte) 0x0;
			bConvert[3] = (byte) 0x0;
			return BitConverter.ToInt32(bConvert, 0);
		}


		public string Decode(byte[] sourceBytes)
		{
			int decPos = 1;

			#region Sanity-check our data:
			if (sourceBytes == null) 
				return "Source byte array was null";
			if (sourceBytes.Length < 1) 
				return "Byte array was Zero-length";
			if (sourceBytes[0] != SOH)
				return "Packet does not begin with SOH";
			if (sourceBytes.Length > MaxTotalLength) 
				return "Packet too long";

			if (sourceBytes.Length != AckNakLength)
			{
				if (sourceBytes.Length < HeaderLength) 
					return "Packet too short";
			}
			#endregion

			cMessageType = sourceBytes[decPos++];
			iUnitId = (uint)(((int)sourceBytes[decPos++]) << 8);
			iUnitId += (uint)sourceBytes[decPos++];
			iSequenceNumber = (int)sourceBytes[decPos++];
			if ((cMessageType == ACK) || (cMessageType == NAK))
				return "";		// That's it for an ACK/NAK

			iPayloadLength = (((int)sourceBytes[decPos++]) << 8);
			iPayloadLength += (int)sourceBytes[decPos++];
			iPacketNumber = (int)sourceBytes[decPos++];
			iPacketTotal = (int)sourceBytes[decPos++];

			cSpare = sourceBytes[decPos++];

			// Check here to verify packet length:
			if (sourceBytes[(HeaderLength + iPayloadLength + TrailerLength) - 1] != EOT) 
				return "Packet does not end with EOT";


			// Create payload:
			if (iPayloadLength > 0)
			{
				aPayload = new byte[iPayloadLength];
				Array.Copy(sourceBytes, decPos, aPayload, 0, iPayloadLength);
				decPos += iPayloadLength;
			}
			else
			{
				aPayload = null;
			}

			byte[] bTemp = new byte[(HeaderLength + iPayloadLength + TrailerLength)];

			for (int X = 0; X < (HeaderLength + iPayloadLength + TrailerLength); X++)
			{
				bTemp[X] = sourceBytes[X];
			}

			CRCBytes = bTemp;

			// Check trailer:
			int rxCrc = (((int)sourceBytes[decPos++]) << 8);
			rxCrc += (int)sourceBytes[decPos++];

			int calcCrc = CalculateCheckSum(CRCBytes, 0, CRCBytes.Length);

			// A 0 in the received CRC means "ignore it - we didn't bother doing a CRC"
			if ((rxCrc != 0) && (calcCrc != rxCrc)) 
				return "CRC Check failed: Rx: " + rxCrc.ToString() + " Calculated: " + calcCrc.ToString();


			return "";
		}





		public static MCCPacket Parse(byte[] sourceBytes)
		{
			int decPos = 1;
			#region Sanity-check our data:
			if (sourceBytes == null)					return new MCCPacket("Source byte array was null");
			if (sourceBytes.Length < 1)					return new MCCPacket("Byte array was Zero-length");
			if (sourceBytes[0] != SOH)					return new MCCPacket("Packet does not begin with SOH");
			if (sourceBytes.Length > MaxTotalLength)	return new MCCPacket("Packet too long");
			
			if (sourceBytes.Length != AckNakLength)
			{
				if (sourceBytes.Length < HeaderLength)			return new MCCPacket("Packet too short");
			} 
			#endregion

			// Create packet and header fields:
			MCCPacket pkt = new MCCPacket();
			
			pkt.cMessageType = sourceBytes[decPos++];
			pkt.iUnitId = (uint) (((int) sourceBytes[decPos++]) << 8);
			pkt.iUnitId += (uint) sourceBytes[decPos++];
			pkt.iSequenceNumber = (int) sourceBytes[decPos++];
			if ((pkt.cMessageType == ACK) || (pkt.cMessageType == NAK)) return pkt; // That's it for an ACK/NAK

			pkt.iPayloadLength = (((int) sourceBytes[decPos++]) << 8);
			pkt.iPayloadLength += (int) sourceBytes[decPos++];
			pkt.iPacketNumber = (int) sourceBytes[decPos++];
			pkt.iPacketTotal = (int) sourceBytes[decPos++];
			
			pkt.cSpare = sourceBytes[decPos++];

			// Check here to verify packet length:
			if (sourceBytes[(HeaderLength + pkt.iPayloadLength +  TrailerLength) - 1] != EOT)	return new MCCPacket("Packet does not end with EOT");			


			// Create payload:
			if (pkt.iPayloadLength > 0)
			{
				pkt.aPayload = new byte[pkt.iPayloadLength];
				Array.Copy(sourceBytes, decPos, pkt.aPayload, 0, pkt.iPayloadLength);
				decPos += pkt.iPayloadLength;
			}
			else
			{
				pkt.aPayload = null;
			}

			byte[] bTemp = new byte[(HeaderLength + pkt.iPayloadLength +  TrailerLength)];

			for(int X = 0; X < (HeaderLength + pkt.iPayloadLength +  TrailerLength); X++)
			{
				bTemp[X] = sourceBytes[X];
			}

			pkt.CRCBytes = bTemp;

            if (MCCPacket.CheckCRC)
            {
                // Check trailer:
                int rxCrc = (((int)sourceBytes[decPos++]) << 8);
                rxCrc += (int)sourceBytes[decPos++];

                int calcCrc = CalculateCheckSum(pkt.CRCBytes, 0, pkt.CRCBytes.Length);

                // A 0 in the received CRC means "ignore it - we didn't bother doing a CRC"
                if ((rxCrc != 0) && (calcCrc != rxCrc)) return new MCCPacket("CRC Check failed: Rx: " + rxCrc + " Calculated: " + calcCrc);
            }
			return pkt;
		}

        #region IMccPacket Members
        /// <summary>
        /// old protocol does not have a channel, return -1
        /// </summary>
        public int Channel
        {
            get { return -1; }
        }

        public int PacketNumber
        {
            get { return iPacketNumber; }
        }

        public int PacketTotal
        {
            get { return iPacketTotal; }
        }

        public MccPacketType PacketType
        {
            get 
            {
                switch (cMessageType)
                {
                    case ACK:
                        return MccPacketType.Ack;
                    case NAK:
                        return MccPacketType.Nack;
                    case KEEPALIVE_TYPE:
                        return MccPacketType.KeepAlive;
                    case SIGN_OFF_TYPE:
                        return MccPacketType.SignOff;
                    case DATA_PKT_TYPE:
                        return MccPacketType.Data;
                    default:
                        return MccPacketType.Unknown;
                }
            }
        }

        public int SequenceNumber
        {
            get { return iSequenceNumber; }
            set { iSequenceNumber = value; }
        }

        byte[] IMccPacket.ToByteArray
        {
            get { return ToByteArray(); }
        }

        public int UnitId
        {
            get { return (int)iUnitId; }
            set { iUnitId = (uint)value; }
        }

        /// <summary>
        /// serial number not used in old protocol
        /// </summary>
        public long SerialNumber
        {
            get { return 0; }
            set { }
        }

        /// <summary>
        /// serial number not used in old protocol
        /// </summary>
        public HardwareType Hardware
        {
            get { return HardwareType.Unknown; }
            set { }
        }

        public UnitKey CreateUnitKey()
        {
            return new UnitKey(this);
        }
        #endregion
    }

}

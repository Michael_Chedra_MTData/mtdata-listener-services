using System;
using System.Collections.Generic;
using System.IO;
#if !Android
using log4net;
#endif

namespace MTData.Transport.GenericProtocol
{
    public class MccLayer : BaseLayer, IMccPacket
    {
        public const int ACK_TYPE = 1;
        public const int NACK_TYPE = 2;
        public const int KEEP_ALIVE_TYPE = 3;
        public const int DATA_TYPE = 4;

        #region private fields
#if !Android
        private static ILog _log = LogManager.GetLogger(typeof(MccLayer));
#endif
        private int _unitId;
        private int _ackSequence;
        private int _packetId;
        private int _packetNo;
        private int _packetTotal;
        private int _channel;
        private long _serialNumber;
        private HardwareType _hardwareType;
        #endregion

        #region properties
        /// <summary>
        /// UnitId is only used in version 1, 2 and 3 of the MCC packet - it has been replaced by SerialNumber and Hardware type from version 3 onwards
        /// </summary>
        public int UnitId
        {
            get { return _unitId; }
            set { _unitId = value; }
        }
        public int SequenceNumber
        {
            get { return _ackSequence; }
            set { _ackSequence = value; }
        }
        public int PacketId
        {
            get { return _packetId; }
            set { _packetId = value; }
        }
        public int PacketNumber
        {
            get { return _packetNo; }
            set { _packetNo = value; }
        }
        public int PacketTotal
        {
            get { return _packetTotal; }
            set { _packetTotal = value; }
        }

        public int Channel
        {
            get { return _channel; }
            set { _channel = value; }
        }

        /// <summary>
        /// set the device layer within the packet (sets packet total and number to 1)
        /// get the device layer (if packet total is not 1 then will return null as not a complete packet)
        /// </summary>
        public DeviceLayer DeviceLayer
        {
            get
            {
                DeviceLayer device = null;
                if (_packetTotal == 1)
                {
                    device = new DeviceLayer();
                    device.Decode(Data);
                }
                return device;
            }
            set
            {
                Data = value.GetBytes();
                _packetTotal = 1;
                _packetNo = 1;
            }
        }

        /// <summary>
        /// SerialNumber is only used from version 3 onwards
        /// </summary>
        public long SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }
        /// <summary>
        /// HardwareType is only used from version 3 onwards
        /// </summary>
        public HardwareType Hardware
        {
            get { return _hardwareType; }
            set { _hardwareType = value; }
        }
        #endregion

        #region constructor
        public MccLayer()
            : base(0x02)
        {
            Type = 1;
            Version = 4;
            _packetId = 1;
            _packetNo = 1;
            _packetTotal = 1;
            _hardwareType = HardwareType.Unknown;
        }
        #endregion

        #region implement abstract methods
        public override byte[] GetBytes()
        {
            //work out the version number, depending on if hardware type is set
            if (_hardwareType == HardwareType.Unknown && Version > 3)
            {
                Version = 3;
            }
            else if (_hardwareType != HardwareType.Unknown && Version <= 3)
            {
                Version = 4;
            }


            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte(StartOfPacket);
                WriteMoreFlag(stream, Type);
                stream.WriteByte((byte)Version);
                WriteMoreFlag(stream, Length);
                if (Version <= 3)
                {
                    Write4ByteInteger(stream, _unitId);
                }
                else
                {
                    WriteLong(stream, _serialNumber);
                    stream.WriteByte((byte)_hardwareType);
                }
                stream.WriteByte((byte)_ackSequence);
                stream.WriteByte((byte)_packetId);
                if (Version == 1)
                {
                    stream.WriteByte(WritePacketNumbers());
                }
                else if (Version == 2)
                {
                    stream.WriteByte((byte)_packetNo);
                    stream.WriteByte((byte)_packetTotal);
                }
                else
                {
                    Write2ByteInteger(stream, _packetNo);
                    Write2ByteInteger(stream, _packetTotal);
                }
                stream.WriteByte((byte)_channel);
                if (Data != null)
                {
                    stream.Write(Data, 0, Length);
                }
                byte[] data = stream.ToArray();
                byte checkSum = CalculateCheckSum(data, 0, data.Length);
                stream.WriteByte(checkSum);

                return stream.ToArray();
            }
        }

        public override int Decode(byte[] payload)
        {
            try
            {
                //check start of packet
                int index = 0;
                if (payload[index++] != StartOfPacket)
                {
                    throw new Exception("MccLayer start of packet is incorrect");
                }

                //read type, version and data length
                Type = ReadMoreFlag(payload, ref index);
                Version = (int)payload[index++];
                int length = ReadMoreFlag(payload, ref index);

                //read unit id, acq sequence, packet id, packet total and channel
                if (Version <= 3)
                {
                    _unitId = Read4ByteInteger(payload, ref index);
                    _serialNumber = 0;
                    _hardwareType = HardwareType.Unknown;
                }
                else
                {
                    _unitId = 0;
                    _serialNumber = ReadLong(payload, ref index);
                    _hardwareType = (HardwareType)payload[index++];
                }
                _ackSequence = (int)payload[index++];
                _packetId = (int)payload[index++];
                if (Version == 1)
                {
                    ReadPacketTotals(payload[index++]);
                }
                else if (Version == 2)
                {
                    _packetNo = (int)payload[index++];
                    _packetTotal = (int)payload[index++];
                }
                else
                {
                    _packetNo = Read2ByteInteger(payload, ref index);
                    _packetTotal = Read2ByteInteger(payload, ref index);
                }
                _channel = (int)payload[index++];

                //check that packet is long enough
                if (payload.Length < index + length)
                {
                    throw new Exception(string.Format("packet is not long enough. Packet Length = {0}, start of data {1}, data length {2}", payload.Length, index, length));
                }

                //read data
                byte[] data = new byte[length];
                Array.Copy(payload, index, data, 0, length);
                Data = data;
                index += length;

                //check the check sum
                byte checkSum = CalculateCheckSum(payload, 0, index);
                if (payload[index] != checkSum)
                {
                    throw new Exception(string.Format("Checksum failed. expected {0}, read {1}", checkSum, payload[index]));
                }
                index++;

                return index;
            }
            catch (Exception exp)
            {
#if !Android
                _log.ErrorFormat("Unable to read mcc layer: {0}", exp.Message);
#endif
                throw new Exception("Unable to read mcc layer", exp);
            }
        }

        #endregion

        #region private methods
        private byte WritePacketNumbers()
        {
            return (byte)((_packetNo << 4) | _packetTotal);
        }

        private void ReadPacketTotals(byte b)
        {
            _packetNo = (int)(b >> 4);
            _packetTotal = (int)(b & 0xF);
        }
        #endregion

        #region IMccPacket Members

        public MccPacketType PacketType
        {
            get
            {
                switch (Type)
                {
                    case (ACK_TYPE):
                        return MccPacketType.Ack;
                    case (NACK_TYPE):
                        return MccPacketType.Nack;
                    case (KEEP_ALIVE_TYPE):
                        return MccPacketType.KeepAlive;
                    case (DATA_TYPE):
                        return MccPacketType.Data;
                    default:
                        return MccPacketType.Unknown;
                }
            }
        }

        public byte[] Payload
        {
            get { return Data; }
        }

        public byte[] ToByteArray
        {
            get { return GetBytes(); }
        }

        public UnitKey CreateUnitKey()
        {
            return new UnitKey(this);
        }
        #endregion

        #region public static methods
        public static MccLayer[] SplitMccPacket(MccLayer packet, int maxPayloadSize)
        {
            List<MccLayer> mccPacketSegments = new List<MccLayer>();
            int packets = (int)(packet.Data.Length / maxPayloadSize) + 1;
#if !Android
            _log.DebugFormat("{0} packets for message of length {1}", packets, packet.Data.Length);
#endif

            byte[] data;
            int index = 0;
            MccLayer splitPacket;
            for (int i = 1; i <= packets; i++)
            {
                if (i == packets)
                {
                    data = new byte[packet.Data.Length - index];
                }
                else
                {
                    data = new byte[maxPayloadSize];
                }
                Array.Copy(packet.Data, index, data, 0, data.Length);
                index += maxPayloadSize;

                splitPacket = new MccLayer();
                splitPacket.Type = packet.Type;
                splitPacket.Version = packet.Version;
                splitPacket.UnitId = packet.UnitId;
                splitPacket.SerialNumber = packet.SerialNumber;
                splitPacket.Hardware = packet.Hardware;
                splitPacket.SequenceNumber = packet.SequenceNumber;
                splitPacket.PacketId = packet.PacketId;
                splitPacket.PacketNumber = i;
                splitPacket.PacketTotal = packets;
                splitPacket.Channel = packet.Channel;
                splitPacket.Data = data;
                mccPacketSegments.Add(splitPacket);

            }
            return mccPacketSegments.ToArray();
        }

        public static MccLayer ReformMccPacket(MccLayer[] packets)
        {
            int packetTotal = packets[0].PacketTotal;
            int id = packets[0].PacketId;

            MemoryStream data = new MemoryStream();
            for (int i = 1; i <= packetTotal; i++)
            {
                //find the correct packet
                MccLayer match = null;
                foreach (var packet in packets)
                {
                    if (packet.PacketNumber == i && packet.PacketId == id)
                    {
                        match = packet;
                        break;
                    }
                }
                if (match == null)
                {
                    throw new Exception(string.Format("Can't find packet number {0} in the array", i));
                }
                //add the payload
                data.Write(match.Data, 0, match.Data.Length);
            }

            MccLayer mergedPacket = new MccLayer();
            mergedPacket.Type = packets[0].Type;
            mergedPacket.Version = packets[0].Version;
            mergedPacket.UnitId = packets[0].UnitId;
            mergedPacket.SerialNumber = packets[0].SerialNumber;
            mergedPacket.Hardware = packets[0].Hardware;
            mergedPacket.SequenceNumber = packets[0].SequenceNumber;
            mergedPacket.PacketId = packets[0].PacketId;
            mergedPacket.PacketNumber = 1;
            mergedPacket.PacketTotal = 1;
            mergedPacket.Channel = packets[0].Channel;
            mergedPacket.Data = data.ToArray();

            //check merged data can be read has a device Layer
            try
            {
                DeviceLayer l = mergedPacket.DeviceLayer;
            }
            catch (Exception exp)
            {
                throw new Exception("Merged packet, data can't be read as a DeviceLayer", exp);
            }
            return mergedPacket;
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Pings
{
    public class PingSegment : SegmentLayer
    {
        #region constructor
        public PingSegment()
        {
            Version = 1;
            Type = (int)PingSegmentsTypes.Ping;
        }
        public PingSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)PingSegmentsTypes.Ping)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", PingSegmentsTypes.Ping, segment.Type));
            }
            Version = segment.Version;
            Type = (int)PingSegmentsTypes.Ping;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }

        #endregion
    }
}

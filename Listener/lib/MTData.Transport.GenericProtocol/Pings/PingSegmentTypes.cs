using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Pings
{
    public enum PingSegmentsTypes
    {
        Ping = SegmentTypes.PingSegments + 1,
        Pong
    }
}

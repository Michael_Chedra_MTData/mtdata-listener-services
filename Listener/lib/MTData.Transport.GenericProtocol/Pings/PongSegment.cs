using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Pings
{
    public class PongSegment : SegmentLayer
    {
        #region constructor
        public PongSegment()
        {
            Version = 1;
            Type = (int)PingSegmentsTypes.Pong;
        }
        public PongSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)PingSegmentsTypes.Pong)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", PingSegmentsTypes.Pong, segment.Type));
            }
            Version = segment.Version;
            Type = (int)PingSegmentsTypes.Pong;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }
        #endregion
    }
}

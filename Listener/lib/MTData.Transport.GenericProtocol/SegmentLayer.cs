using System;
using System.Collections.Generic;
using System.IO;
#if !Android
using log4net;
#endif
namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// Segement types
    /// </summary>
    public enum SegmentTypes
    {
        DriverPointsSegments = 0,
        FatigueSegments = 50,
        DownloadsSegments = 100,
        LogisticsSegments = 150,
        LogisticsLiveUpdateSegments = 300,
        ReleaseManagerSegments = 400,
        ReleaseManagerLiveUpdatesSegments = 450,
        APISegmentTypes = 500,
        PingSegments = 550,
        ListenerSegments = 600,
        DashboardSegments = 650,
        ManagementCentralSegments = 700
    }


    /// <summary>
    /// this class represents a segment
    /// </summary>
    public class SegmentLayer : BaseLayer
    {
        protected const string SqlDateFormat = "yyyy-MM-dd HH:mm:ss";

#if !Android
        private static ILog _log = LogManager.GetLogger(typeof(SegmentLayer));
#endif
        #region constructor
        public SegmentLayer()
            : base(0x10)
        {
        }
        #endregion

        #region implement abstract methods
        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte(StartOfPacket);
                WriteMoreFlag(stream, Type);
                stream.WriteByte((byte)Version);
                WriteMoreFlag(stream, Length);
                if (Data != null)
                {
                    stream.Write(Data, 0, Length);
                }
                byte[] data = stream.ToArray();
                byte checkSum = CalculateCheckSum(data, 0, data.Length);
                stream.WriteByte(checkSum);

                return stream.ToArray();
            }
        }

        public override int Decode(byte[] payload)
        {
            try
            {
                //check start of packet
                int index = 0;
                if (payload[index++] != StartOfPacket)
                {
                    throw new Exception("Segment start of packet is incorrect");
                }

                //read type, version and data length
                Type = ReadMoreFlag(payload, ref index);
                Version = (int)payload[index++];
                int length = ReadMoreFlag(payload, ref index);

                //check that packet is long enough
                if (payload.Length < index + length)
                {
                    throw new Exception(string.Format("packet is not long enough. Packet Length = {0}, start of data {1}, data length {2}", payload.Length, index, length));
                }

                //read data
                byte[] data = new byte[length];
                Array.Copy(payload, index, data, 0, length);
                Data = data;
                index += length;

                //check the check sum
                byte checkSum = CalculateCheckSum(payload, 0, index);
                if (payload[index] != checkSum)
                {
                    throw new Exception(string.Format("Checksum failed. expected {0}, read {1}", checkSum, payload[index]));
                }
                index++;

                return index;
            }
            catch (Exception exp)
            {
#if !Android
                _log.ErrorFormat("Unable to read segment: {0}", exp.Message);
#endif
                throw new Exception("Unable to read segment", exp);
            }
        }
        #endregion
    }
}

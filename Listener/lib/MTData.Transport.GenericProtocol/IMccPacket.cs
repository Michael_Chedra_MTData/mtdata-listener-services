using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// the different types of MCC packet
    /// </summary>
    public enum MccPacketType
    {
        Ack,
        Nack,
        SignOff,
        KeepAlive,
        Data,
        Unknown,
    }

    /// <summary>
    /// Hardware Type enum
    /// </summary>
    public enum HardwareType
    {
        Unknown = 0,
        ECMInterface1035,
        TrackingUnit3020,
        TrackingUnit3021,
        TrackingUnit3022,
        TrackingUnit3023,
        TrackingUnit3024,
        TrackingUnit3025,
        TrackingUnit3026,
        TrackingUnit3027,
        PersonalTrackerUnit3028,
        TrackingUnit3029,
        TrackingUnit4000,
        TrackingUnit5070,
        TrackingUnit5080,
        TrackingUnit5081,
        Terminal5010,
        Terminal5040,
        Terminal5050,
        Terminal5060,
        Terminal5070,
        Terminal5080,
        Terminal6041,
        Swift,
        ShellAPI,
        PC,
        Talon
    }
    /// <summary>
    /// interface that is implemented by all MCC packets
    /// </summary>
    public interface IMccPacket
    {
        /// <summary>
        /// get the MCC packet type
        /// </summary>
        MccPacketType PacketType { get; }

        /// <summary>
        /// get the unit id, the unit this packet is for is identifued by either the UnitId or Serial Number and Hardware Type
        /// </summary>
        int UnitId { get; set; }
        /// <summary>
        /// get the SerialNumber, the unit this packet is for is identifued by either the UnitId or Serial Number and Hardware Type
        /// </summary>
        long SerialNumber { get; set; }
        /// <summary>
        /// get the HardwareType, the unit this packet is for is identifued by either the UnitId or Serial Number and Hardware Type
        /// </summary>
        HardwareType Hardware { get; set; }

        /// <summary>
        /// Create a unit key for this packet
        /// </summary>
        /// <returns>A Unit key</returns>
        UnitKey CreateUnitKey();

        /// <summary>
        /// get the packet number 
        /// </summary>
        int PacketNumber { get; }

        /// <summary>
        /// get the packet total
        /// </summary>
        int PacketTotal { get; }

        /// <summary>
        /// get the sequence number
        /// </summary>
        int SequenceNumber { get; set;}

        /// <summary>
        /// get the channel number
        /// </summary>
        int Channel { get; }

        /// <summary>
        /// get the payload
        /// </summary>
        byte[] Payload { get; }

        /// <summary>
        /// get the Mcc packet as bytes
        /// </summary>
        byte[] ToByteArray { get; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// interface that allows an object to encode and decode itself to a byte[]
    /// </summary>
    public interface IPacketEncode
    {
        /// <summary>
        /// decode the object from a byte[]
        /// </summary>
        /// <param name="Data">byte[] to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        void Decode(byte[] data, ref int index);

        /// <summary>
        /// encode the object into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        void Encode(System.IO.MemoryStream stream);
    }
}

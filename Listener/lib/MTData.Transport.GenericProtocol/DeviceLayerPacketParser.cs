using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// this class will parse a Device Layer packet when given a byte at a time
    /// </summary>
    public class DeviceLayerPacketParser
    {
        /// <summary>
        /// enum of packet states
        /// </summary>
        private enum PacketState
        {
            SOD,
            Type,
            Version,
            Length,
            UnitId,
            SerialNumber,
            HardwareType,
            DeviceFlags,
            SegmentData,
            Md5Hash,
            CheckSum,
        }

        /// <summary>
        /// enum of add byte result
        /// </summary>
        public enum AddByteResult
        {
            ReadingPacket,
            PacketReady,
            Error
        }

        #region private fields
        /// <summary>
        /// the buffer of the bytes read
        /// </summary>
        private MemoryStream _buffer;
        /// <summary>
        /// teh current packet state (indicates what is being read)
        /// </summary>
        private PacketState _packetState;
        /// <summary>
        /// the last packet successfully read
        /// </summary>
        private DeviceLayerExtended _lastPacket;
        /// <summary>
        /// the last error message
        /// </summary>
        private string _lastError;
        /// <summary>
        /// the current checksum of packet data
        /// </summary>
        private byte _checksum;
        /// <summary>
        /// the length of the segment data
        /// </summary>
        private int _segmentDataLength;
        /// <summary>
        /// the number of bytes to shift when reading the length
        /// </summary>
        private int _shift;
        /// <summary>
        /// the number of bytes still to be read in an array (used by UnitId, SegemntData and Md5Hash)
        /// </summary>
        private int _length;
        /// <summary>
        /// is MD5 hash in the packet
        /// </summary>
        private bool _md5Check;
        /// <summary>
        /// need to know the version of the packet currently being decoded as the packet structure is different
        /// </summary>
        private int _version;
        #endregion

        #region properties
        public DeviceLayerExtended LastPacket { get { return _lastPacket; } }
        public string LastError { get { return _lastError; } }
        #endregion

        #region constructor
        public DeviceLayerPacketParser()
        {
            _buffer = new MemoryStream();
            _packetState = PacketState.SOD;
            _lastError = string.Empty;
            _lastPacket = new DeviceLayerExtended();
        }
        #endregion

        #region public methods
        /// <summary>
        /// reset the packet reader
        /// </summary>
        public void Reset()
        {
            _packetState = PacketState.SOD;
            _length = 0;
            _md5Check = false;
            _segmentDataLength = 0;
            _shift = 0;
            _version = 0;
            if (_buffer != null)
            {
                _buffer.Close();
                _buffer = null;
            }
            _buffer = new MemoryStream();
        }

        /// <summary>
        /// add a byte to the reader
        /// </summary>
        /// <param name="b">the byte to add</param>
        /// <returns>the result of reading that byte</returns>
        public AddByteResult AddByte(byte b)
        {
            switch (_packetState)
            {
                case PacketState.SOD:
                    if (b == _lastPacket.StartOfPacket)
                    {
                        _buffer.WriteByte(b);
                        _packetState = PacketState.Type;
                        _checksum = b;
                    }
                    else
                    {
                        _lastError = string.Format("Wrong Start of Packet byte, expecting {0}, got {1}", _lastPacket.StartOfPacket, b);
                        return AddByteResult.Error;
                    }
                    break;
                case PacketState.Type:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    if ((b & 0x80) == 0)
                    {
                        _packetState = PacketState.Version;
                    }
                    break;
                case PacketState.Version:
                    _version = (int)b;
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _packetState = PacketState.Length;
                    break;
                case PacketState.Length:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    
                    _segmentDataLength |= (b & 0x7F) << _shift;
                    _shift += 7;

                    if ((b & 0x80) == 0)
                    {
                        if (_version == 1)
                        {
                            _packetState = PacketState.UnitId;
                            _length = 4;
                        }
                        else
                        {
                            _packetState = PacketState.SerialNumber;
                            _length = 8;
                        }
                    }
                    break;
                case PacketState.UnitId:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _length--;
                    if (_length == 0)
                    {
                        _packetState = PacketState.DeviceFlags;
                    }
                    break;
                case PacketState.SerialNumber:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _length--;
                    if (_length == 0)
                    {
                        _packetState = PacketState.HardwareType;
                    }
                    break;
                case PacketState.HardwareType:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _packetState = PacketState.DeviceFlags;
                    break;
                case PacketState.DeviceFlags:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _md5Check = IsMd5HashIncluded(b);
                    if (_segmentDataLength > 0)
                    {
                        _packetState = PacketState.SegmentData;
                        _length = _segmentDataLength;
                    }
                    else if (_md5Check)
                    {
                        _packetState = PacketState.Md5Hash;
                        _length = 16;
                    }
                    else
                    {
                        _packetState = PacketState.CheckSum;
                    }
                    break;
                case PacketState.SegmentData:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _length--;
                    if (_length == 0)
                    {
                        if (_md5Check)
                        {
                            _packetState = PacketState.Md5Hash;
                            _length = 16;
                        }
                        else
                        {
                            _packetState = PacketState.CheckSum;
                        }
                    }
                    break;
                case PacketState.Md5Hash:
                    _buffer.WriteByte(b);
                    _checksum += b;
                    _length--;
                    if (_length == 0)
                    {
                        _packetState = PacketState.CheckSum;
                    }
                    break;
                case PacketState.CheckSum:
                    _buffer.WriteByte(b);
                    _packetState = PacketState.SOD;
                    if (_checksum == b)
                    {
                        try
                        {
                            _lastPacket = new DeviceLayerExtended();
                            byte[] data = _buffer.ToArray();
                            int read = _lastPacket.Decode(data);
                            if (read == data.Length)
                            {
                                return AddByteResult.PacketReady;
                            }
                            else
                            {
                                _lastError = string.Format("Decoding packet only read {0} bytes instaed of {1}", read, data.Length);
                                return AddByteResult.Error;
                            }
                        }
                        catch (Exception exp)
                        {
                            _lastError = exp.Message;
                            return AddByteResult.Error;
                        }
                    }
                    else
                    {
                        _lastError = string.Format("Checksum invalid, expecting {0}, got {1}", _checksum, b);
                        return AddByteResult.Error;
                    }
            }
            return AddByteResult.ReadingPacket;
        }
        #endregion

        #region private fields
        private bool IsMd5HashIncluded(byte b)
        {
            if ((b & (byte)DeviceFlags.Md5Hash) != 0)
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using MTData.Transport.GenericProtocol.OldProtocol;

namespace MTData.Transport.GenericProtocol
{
    public class MccPackets
    {
        public static IMccPacket CreateMccAckPacket(int channel, UnitKey unit, int sequenceNumber)
        {
            IMccPacket mccAck;
            if (channel == -1)
            {
                MCCPacket ackPacket = new MCCPacket();
                ackPacket.cMessageType = MCCPacket.ACK;
                mccAck = ackPacket;
            }
            else
            {
                MccLayer ackLayer = new MccLayer();
                ackLayer.Type = MccLayer.ACK_TYPE;
                ackLayer.Channel = channel;
                mccAck = ackLayer;
            }
            mccAck.UnitId = unit.UnitId;
            mccAck.SerialNumber = unit.SerialNumber;
            mccAck.Hardware = unit.Hardware;
            mccAck.SequenceNumber = sequenceNumber;
            return mccAck;
        }

        public static IMccPacket CreateMccNackPacket(int channel, UnitKey unit, int sequenceNumber)
        {
            IMccPacket mccNack;
            if (channel == -1)
            {
                MCCPacket nackPacket = new MCCPacket();
                nackPacket.cMessageType = MCCPacket.NAK;
                mccNack = nackPacket;
            }
            else
            {
                MccLayer nackLayer = new MccLayer();
                nackLayer.Type = MccLayer.NACK_TYPE;
                nackLayer.Channel = channel;
                mccNack = nackLayer;
            }
            mccNack.UnitId = unit.UnitId;
            mccNack.SerialNumber = unit.SerialNumber;
            mccNack.Hardware = unit.Hardware;
            mccNack.SequenceNumber = sequenceNumber;
            return mccNack;
        }

        public static IMccPacket CreateMccDataPacket(int channel, UnitKey unit, int sequenceNumber, byte[] payload, int packetNumber, int packetTotal)
        {
            IMccPacket mccData;
            if (channel == -1)
            {
                MCCPacket dataPacket = new MCCPacket(payload, packetNumber, packetTotal, MCCPacket.DATA_PKT_TYPE);
                mccData = dataPacket;
            }
            else
            {
                MccLayer dataLayer = new MccLayer();
                dataLayer.Type = MccLayer.DATA_TYPE;
                dataLayer.Channel = channel;
                dataLayer.Data = payload;
                dataLayer.PacketNumber = packetNumber;
                dataLayer.PacketTotal = packetTotal;
                mccData = dataLayer;
            }
            mccData.UnitId = unit.UnitId;
            mccData.SerialNumber = unit.SerialNumber;
            mccData.Hardware = unit.Hardware;
            mccData.SequenceNumber = sequenceNumber;
            return mccData;
        }

        public static IMccPacket DecodeMccPacket(byte[] data)
        {
            IMccPacket mccPacket;
            if (data[0] == MCCPacket.SOH)
            {
                //old protocol
                mccPacket = MCCPacket.Parse(data);
            }
            else
            {
                //new protocol
                MccLayer l = new MccLayer();
                l.Decode(data);
                mccPacket = l;
            }
            return mccPacket;
        }
    }
}

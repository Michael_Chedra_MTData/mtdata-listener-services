﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// Structure that uniquely identifies a vehicle.
    /// </summary>
    public struct FleetVehicle
    {
        public bool Equals(FleetVehicle other)
        {
            return fleetId == other.fleetId && vehicleId == other.vehicleId;
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            return obj is FleetVehicle && Equals((FleetVehicle) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                return (fleetId * 397) ^ vehicleId;
            }
        }

        public readonly int fleetId, vehicleId;

        public FleetVehicle(int fleetId, int vehicleId)
        {
            this.fleetId = fleetId;
            this.vehicleId = vehicleId;
        }

        public static bool operator ==(FleetVehicle v1, FleetVehicle v2)
        {
            return v1.Equals(v2);
        }

        public static bool operator !=(FleetVehicle v1, FleetVehicle v2)
        {
            return !v1.Equals(v2);
        }
    }
}

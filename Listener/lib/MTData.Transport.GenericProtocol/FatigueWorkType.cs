﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    public enum FatigueWorkType : byte
    {
        Driving = 1,
        TwoUp = 2,
        ShortRest = 3,
        Rest = 4,
        NonDrivingWork = 5
    }
}

﻿using System;

namespace MTData.Transport.GenericProtocol
{
    public enum BreakType : byte
    {
        OrdinaryBreak = 0,
        NonDrivingWork = 1,
        Driving = 2
    }

    public enum BreakEventType : byte
    {
        MissedStartBreak = 0,
        BreakFinishedEarly = 1
    }
}

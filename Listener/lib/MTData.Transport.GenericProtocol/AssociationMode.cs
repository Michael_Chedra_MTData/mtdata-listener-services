﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol
{
    public enum AssociationMode
    {
        Disabled = 0,
        Master,
        Slave
    }
}

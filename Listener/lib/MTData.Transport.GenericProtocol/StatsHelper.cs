﻿using System;
using System.Collections.Generic;
using System.Text;
using log4net;

namespace MTData.Transport.GenericProtocol
{
    public static class StatsHelper
    {
        private static ILog _logger = LogManager.GetLogger(typeof (StatsHelper));
        private static int _mccPacketsReceived = 0;
        private static int _maxQueueSize = 0;
        private static object _syncObject = new object();

        public static void IncrementPacketsReceived()
        {
            try
            {
                lock (_syncObject)
                {
                    _mccPacketsReceived++;
                }
            }
            catch (Exception ex)
            {                
                _logger.Error("Error incrementing number of mcc packets received", ex);
            }
        }

        public static void SetMaxQueueSize(int currentCount)
        {
            try
            {
                lock (_syncObject)
                {
                    if (currentCount > _maxQueueSize)
                    {
                        _maxQueueSize = currentCount;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error incrementing number of mcc packets received", ex);
            }
        }

        public static int PacketsReceivedCountAndReset
        {
            get
            {
                int count = 0;
                lock (_syncObject)
                {
                    count = _mccPacketsReceived;
                    _mccPacketsReceived = 0;
                }
                return count;
            }
        }

        public static int MaxQueueSizeCountAndReset
        {
            get
            {
                int count = 0;
                lock (_syncObject)
                {
                    count = _maxQueueSize;
                    _maxQueueSize = 0;
                }
                return count;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Security.Cryptography;
#if !Android
using log4net;
#endif

namespace MTData.Transport.GenericProtocol
{
    /// <summary>
    /// enum for device flags
    /// </summary>
    public enum DeviceFlags
    {
        Standard = 0x01,
        Md5Hash = 0x02,
        EncryptedDataWithImeiKey = 0x04
    }

    /// <summary>
    /// the device layer
    /// </summary>
    public class DeviceLayer : BaseLayer
    {
        public static int KEEP_ALIVE_TYPE = 1;
        public static int KEEP_ALIVE_START = 0;
        public static int KEEP_ALIVE_ACK = 1;

        #region private fields
#if !Android
        private static ILog _log = LogManager.GetLogger(typeof(DeviceLayer));
#endif
        private byte _deviceFlags;
        private int _unitId;
        private long _serialNumber;
        private HardwareType _hardwareType;

        private IDeviceLayerEncrypter _encrypter;
        #endregion

        #region properties
        /// <summary>
        /// UnitId is only used in version 1 of the Device Layer - it has been replaced by SerialNumber and Hardware type from version 2 onwards
        /// </summary>
        public int UnitId
        {
            get { return _unitId; }
            set { _unitId = value; }
        }
        /// <summary>
        /// SerialNumber is only used from version 2 onwards
        /// </summary>
        public long SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }
        /// <summary>
        /// HardwareType is only used from version 2 onwards
        /// </summary>
        public HardwareType Hardware
        {
            get { return _hardwareType; }
            set { _hardwareType = value; }
        }
        public bool StandardDeviceFlag
        {
            get { return IsDeviceFlagOn(DeviceFlags.Standard); }
            set { SetDeviceFlag(DeviceFlags.Standard, value); }
        }
        public bool Md5HashDeviceFlag
        {
            get { return IsDeviceFlagOn(DeviceFlags.Md5Hash); }
            set { SetDeviceFlag(DeviceFlags.Md5Hash, value); }
        }
        public bool EncryptedDataWithImeiKeyDeviceFlag
        {
            get { return IsDeviceFlagOn(DeviceFlags.EncryptedDataWithImeiKey); }
            set { SetDeviceFlag(DeviceFlags.EncryptedDataWithImeiKey, value); }
        }
        public IDeviceLayerEncrypter Encrypter
        {
            get { return _encrypter; }
            set { _encrypter = value; }
        }

        /// <summary>
        /// get a list of segments from the data
        /// </summary>
        public virtual List<SegmentLayer> Segments
        {
            get
            {
                List<SegmentLayer> segments = new List<SegmentLayer>();
                byte[] uncryptedData = Data;
                if (uncryptedData == null)
                {
                    return segments;
                }

                //unencrypt the data
                if (EncryptedDataWithImeiKeyDeviceFlag)
                {
                    if (_encrypter == null)
                    {
                        throw new Exception("Data is encrypted, no IDeviceLayerEncrypter provided");
                    }
                    else
                    {
                        uncryptedData = _encrypter.UnencryptData(_unitId, Data);
                    }
                }

                int index = 0;
                byte[] segementData = uncryptedData;
                while (index < uncryptedData.Length)
                {
                    SegmentLayer segment = new SegmentLayer();
                    index += segment.Decode(segementData);
                    segementData = new byte[uncryptedData.Length - index];
                    Array.Copy(uncryptedData, index, segementData, 0, segementData.Length);
                    segments.Add(segment);
                }
                return segments;
            }
            set
            {
                //if data to be encrypted, check that a encrypter is provided
                if (EncryptedDataWithImeiKeyDeviceFlag && _encrypter == null)
                {
                    throw new Exception("Data is encrypted, no IDeviceLayerEncrypter provided");
                }

                using (MemoryStream stream = new MemoryStream()) {
                foreach (SegmentLayer segment in value)
                {
                    byte[] data = segment.GetBytes();
                    stream.Write(data, 0, data.Length);
                }

                //encrypt data
                if (EncryptedDataWithImeiKeyDeviceFlag)
                {
                    Data = _encrypter.EncryptData(_unitId, stream.ToArray());
                }
                else
                {
                    Data = stream.ToArray();
                }
                stream.Close();
                }
            }
        }
        #endregion

        #region constructor
        public DeviceLayer()
            : base(0x02)
        {
            Version = 2;
            _hardwareType = HardwareType.Unknown;
        }
        #endregion

        #region implement abstract methods
        public override byte[] GetBytes()
        {
            //work out the version number, if keep alive packet then must be sent using v1 as it uses the unit parameter to indicate start or ack packet
            //otherwise check if hardware type is set
            if (Type == KEEP_ALIVE_TYPE)
            {
                Version = 1;
            }
            else if (_hardwareType == HardwareType.Unknown && Version > 1)
            {
                Version = 1;
            }
            else if (_hardwareType != HardwareType.Unknown && Version == 1)
            {
                Version = 2;
            }

            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte(StartOfPacket);
                WriteMoreFlag(stream, Type);
                stream.WriteByte((byte)Version);
                WriteMoreFlag(stream, Length);
                if (Version == 1)
                {
                    Write4ByteInteger(stream, _unitId);
                }
                else
                {
                    WriteLong(stream, _serialNumber);
                    stream.WriteByte((byte)_hardwareType);
                }
                stream.WriteByte(_deviceFlags);

                if (Data != null)
                {
                    stream.Write(Data, 0, Length);
                }
                if (Md5HashDeviceFlag)
                {
                    byte[] md5HashKey = CreateMd5Hash();
                    stream.Write(md5HashKey, 0, md5HashKey.Length);
                }
                byte[] data = stream.ToArray();
                byte checkSum = CalculateCheckSum(data, 0, data.Length);
                stream.WriteByte(checkSum);

                return stream.ToArray();
            }
        }

        public override int Decode(byte[] payload)
        {
            try
            {
                //check start of packet
                int index = 0;
                if (payload[index++] != StartOfPacket)
                {
                    throw new Exception("Segment start of packet is incorrect");
                }

                //read type, version and data length
                Type = ReadMoreFlag(payload, ref index);
                Version = (int)payload[index++];
                int length = ReadMoreFlag(payload, ref index);

                //read unit id and device flags
                if (Version <= 1)
                {
                    _unitId = Read4ByteInteger(payload, ref index);
                    _serialNumber = 0;
                    _hardwareType = HardwareType.Unknown;
                }
                else
                {
                    _unitId = 0;
                    _serialNumber = ReadLong(payload, ref index);
                    _hardwareType = (HardwareType)payload[index++];
                }
                _deviceFlags = payload[index++];

                //check that packet is long enough
                if (payload.Length < index + length)
                {
                    throw new Exception(string.Format("packet is not long enough. Packet Length = {0}, start of data {1}, data length {2}", payload.Length, index, length));
                }

                //read data
                byte[] data = new byte[length];
                Array.Copy(payload, index, data, 0, length);
                Data = data;
                index += length;

                if (Md5HashDeviceFlag)
                {
                    //check that packet is long enough
                    length = 16;
                    if (payload.Length < index + length)
                    {
                        throw new Exception(string.Format("packet is not long enough. Packet Length = {0}, start of data {1}, data length {2}", payload.Length, index, length));
                    }
                    byte[] hashKey = new byte[length];
                    Array.Copy(payload, index, hashKey, 0, length);
                    //check hash key is valid
                    byte[] newHashKey = CreateMd5Hash();
                    if (hashKey.Length != newHashKey.Length)
                    {
                        throw new Exception("hash key's length different");
                    }
                    for (int i = 0; i < hashKey.Length; i++)
                    {
                        if (hashKey[i] != newHashKey[i])
                        {
                            throw new Exception("hash key is invalid");
                        }
                    }
                    index += length;
                }
                //check the check sum
                byte checkSum = CalculateCheckSum(payload, 0, index);
                if (payload[index] != checkSum)
                {
                    throw new Exception(string.Format("Checksum failed. expected {0}, read {1}", checkSum, payload[index]));
                }
                index++;

                return index;
            }
            catch (Exception exp)
            {
#if !Android
                _log.ErrorFormat("Unable to read device layer: {0}", exp.Message);
#endif
                throw new Exception("Unable to read device layer", exp);
            }
        }
        #endregion

        #region public methods
        public UnitKey CreateUnitKey()
        {
            return new UnitKey(this);
        }
        #endregion

        #region private methods
        private bool IsDeviceFlagOn(DeviceFlags flag)
        {
            if ((_deviceFlags & (byte)flag) != 0)
            {
                return true;
            }
            return false;
        }
        private void SetDeviceFlag(DeviceFlags flag, bool on)
        {
            if (on)
            {
                _deviceFlags = (byte)(_deviceFlags | (byte)flag);
            }
            else
            {
                _deviceFlags = (byte)(_deviceFlags & ~(byte)flag);
            }
        }

        private byte[] CreateMd5Hash()
        {
            MD5CryptoServiceProvider md5 = new MD5CryptoServiceProvider();
            return md5.ComputeHash(Data);
        }

        #endregion

        #region public static methods
        public static DeviceLayer CreateKeepAlivePacket()
        {
            DeviceLayer packet = new DeviceLayer();
            packet.Type = KEEP_ALIVE_TYPE;
            packet.UnitId = KEEP_ALIVE_START;
            return packet;
        }

        public static DeviceLayer CreateKeepAliveAckPacket()
        {
            DeviceLayer packet = new DeviceLayer();
            packet.Type = KEEP_ALIVE_TYPE;
            packet.UnitId = KEEP_ALIVE_ACK;
            return packet;
        }
        #endregion
    }

    public class DeviceLayerExtended : DeviceLayer
    {
        private long _enqueueTimeTicks;

        public DeviceLayerExtended()
        {
        }

        public long EnqueueTimeTicks
        {
            get { return _enqueueTimeTicks; }
            set { _enqueueTimeTicks = value; }
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using log4net;

namespace MTData.Transport.GenericProtocol
{
    public abstract class BaseLayer
    {
        #region private fields
        private static readonly ILog _log = LogManager.GetLogger(typeof(BaseLayer));
        /// <summary>
        /// the start of packet byte
        /// </summary>
        private byte _startOfPacket;
        /// <summary>
        /// the type of the packet
        /// </summary>
        private int _type;
        /// <summary>
        /// the version number of this packet
        /// </summary>
        private int _version;
        /// <summary>
        /// the data of the packet
        /// </summary>
        private byte[] _data;
        #endregion

        #region properties
        /// <summary>
        /// the start of packet byte
        /// </summary>
        public byte StartOfPacket
        {
            get { return _startOfPacket; }
        }
        /// <summary>
        /// the type of the packet
        /// </summary>
        public int Type
        {
            get { return _type; }
            set { _type = value; }
        }
        /// <summary>
        /// the version number of this packet
        /// </summary>
        public int Version
        {
            get { return _version; }
            set { _version = value; }
        }
        /// <summary>
        /// the data of the packet
        /// </summary>
        public byte[] Data
        {
            get { return _data; }
            set { _data = value; }
        }

        /// <summary>
        /// the length of the data
        /// </summary>
        public int Length
        {
            get
            {
                if (_data != null)
                {
                    return _data.Length;
                }
                return 0;
            }
        }
        #endregion

        #region constrcutor
        public BaseLayer(byte startOfPacket)
        {
            _startOfPacket = startOfPacket;
        }
        #endregion

        #region abstract methods
        /// <summary>
        /// convert this packet into a byte[]
        /// </summary>
        /// <returns>the packet as a byte[]</returns>
        public abstract byte[] GetBytes();

        /// <summary>
        /// decode a byte[] as this packet
        /// </summary>
        /// <param name="payload">the payload</param>
        /// <returns>the number of bytes read</returns>
        /// <exception cref="Exception">throws an exception if unable to read packet correctly</exception>
        public abstract int Decode(byte[] payload);
        #endregion

        #region protected methods
        /// <summary>
        /// calculate the checksum of binary array
        /// </summary>
        /// <param name="data">the array of data to checksum</param>
        /// <param name="startIndex">the index to start at</param>
        /// <param name="endIndex">the index to end at</param>
        /// <returns></returns>
        protected internal byte CalculateCheckSum(byte[] data, int startIndex, int length)
        {
            byte checksum = 0x00;
            int index = startIndex;
            int end = startIndex + length;
            if (data.Length < end)
            {
                end = data.Length;
            }

            while (index < end)
            {
                checksum += data[index++];
            }
            return checksum;
        }

        #endregion

        #region static methods
        /// <summary>
        /// write a more flag 
        /// 7bits of type, if MSB is set then next byte is added to the value of the type
        /// </summary>
        /// <param name="stream">the stream to write the flag to</param>
        /// <param name="value">the value of the flag</param>
        /// <returns>teh number of bits writtern</returns>
        public static int WriteMoreFlag(Stream stream, int value)
        {
            int bytesWritten = 0;
            while (value > 0x7F)
            {
                stream.WriteByte((byte)((value & 0x7F) | 0x80));
                bytesWritten++;
                value >>= 7;
            }

            stream.WriteByte((byte)(value & 0x7F));
            bytesWritten++;
            return bytesWritten;
        }

        /// <summary>
        /// Read a more flag
        /// </summary>
        /// <param name="data">the byte[] to read from</param>
        /// <param name="byteIndex">the index in the array to read from. on return contains the current index</param>
        /// <returns>the value of the flag</returns>
        public static int ReadMoreFlag(byte[] data, ref int byteIndex)
        {
            byte b;
            int shift = 0;
            int value = 0;

            do
            {
                if (byteIndex < data.Length)
                    b = data[byteIndex++];
                else
                    b = 0;

                value |= (b & 0x7F) << shift;
                shift += 7;

            } while ((b & 0x80) != 0);

            return value;
        }

        /// <summary>
        /// Write a 4 byte integer lsb, to msb
        /// </summary>
        /// <param name="value"></param>
        public static void Write4ByteInteger(Stream stream, int value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            stream.Write(temp, 0, 4);
        }

        /// <summary>
        /// This method will read 4 bytes form the buffer and convert them into an integer.
        /// </summary>
        /// <returns></returns>
        public static int Read4ByteInteger(byte[] data, ref int byteIndex)
        {
            int result = BitConverter.ToInt32(data, byteIndex);
            byteIndex += 4;
            return result;
        }

        /// <summary>
        /// Write a 2 byte integer lsb, to msb
        /// </summary>
        /// <param name="value"></param>
        public static void Write2ByteInteger(Stream stream, int value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            stream.Write(temp, 0, 2);
        }

        /// <summary>
        /// This method will read 2 bytes form the buffer and convert them into an integer.
        /// </summary>
        /// <returns></returns>
        public static int Read2ByteInteger(byte[] data, ref int byteIndex)
        {
            int result = BitConverter.ToInt16(data, byteIndex);
            byteIndex += 2;
            return result;
        }

        public static void WriteDateTime(Stream stream, DateTime date)
        {
            if (date.Equals(DateTime.MinValue))
            {
                stream.WriteByte(0x00);
                stream.WriteByte(0x00);
                stream.WriteByte(0x00);
                stream.WriteByte(0x00);
                stream.WriteByte(0x00);
                stream.WriteByte(0x00);
            }
            else if (date.Equals(DateTime.MaxValue))
            {
                stream.WriteByte(0xFF);
                stream.WriteByte(0xFF);
                stream.WriteByte(0xFF);
                stream.WriteByte(0xFF);
                stream.WriteByte(0xFF);
                stream.WriteByte(0xFF);
            }
            else
            {
                stream.WriteByte((byte)date.Day);
                stream.WriteByte((byte)date.Month);
                stream.WriteByte((byte)(date.Year - 2000));
                stream.WriteByte((byte)date.Hour);
                stream.WriteByte((byte)date.Minute);
                stream.WriteByte((byte)date.Second);
            }
        }
        public static DateTime ReadDateTime(byte[] data, ref int byteIndex)
        {
            int day = (int)data[byteIndex++];
            int month = (int)data[byteIndex++];
            int year = (int)data[byteIndex++] + 2000;
            int hour = (int)data[byteIndex++];
            int minute = (int)data[byteIndex++];
            int second = (int)data[byteIndex++];

            if (day == 0 && month == 0 && year == 2000 && hour == 0 && minute == 0 && second == 0)
            {
                return DateTime.MinValue;
            }
            else if (day == 255 && month == 255 && year == 2255 && hour == 255 && minute == 255 && second == 255)
            {
                return DateTime.MaxValue;
            }
            else
            {
                return new DateTime(year, month, day, hour, minute, second);
            }
        }

        public static int WriteString(Stream stream, string value)
        {
            return WriteString(stream, value, 0x12);
        }

        public static int WriteString(Stream stream, string value, byte delimiter)
        {
            if (value == null)
                value = "";

            byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(value);
            stream.Write(bytes, 0, bytes.Length);

            stream.WriteByte(delimiter);
            return bytes.Length + 1;
        }

        public static string ReadString(byte[] data, ref int byteIndex)
        {
            return ReadString(data, ref byteIndex, 0x12);
        }

        public static string ReadString(byte[] data, ref int byteIndex, byte delimiter)
        {
            int loop = 0;
            while (true)
            {
                if (data[byteIndex + loop] == delimiter)
                    break;
                loop++;
            }

            string value = System.Text.ASCIIEncoding.ASCII.GetString(data, byteIndex, loop);
            byteIndex += loop + 1; //Loop + delimiter
            return value;
        }

        public static void Write4ByteUInteger(Stream stream, uint value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            stream.Write(temp, 0, 4);
        }
        public static uint Read4ByteUInteger(byte[] data, ref int byteIndex)
        {
            uint result = BitConverter.ToUInt32(data, byteIndex);
            byteIndex += 4;
            return result;
        }

        public static void WriteCoordinate(Stream stream, double coordinate)
        {
            uint coord = BaseLayer.ConvertCoordinate(coordinate);
            Write4ByteUInteger(stream, coord);
        }
        public static double ReadCoordinate(byte[] data, ref int byteIndex)
        {
            uint coord = BaseLayer.Read4ByteUInteger(data, ref byteIndex);
            return BaseLayer.ConvertCoordinate(coord);
        }
        private static double ConvertCoordinate(uint coordinate)
        {
            uint temp = 0;
            bool isNegative = false;

            if ((coordinate & 0x10000000) == 0x10000000)
            {
                temp = coordinate - (int)0x10000000;
                isNegative = true;
            }
            else
            {
                temp = coordinate;
            }

            double coordinateAsDouble = Convert.ToDouble(temp) / 600000;
            if (isNegative)
            {
                coordinateAsDouble = coordinateAsDouble * -1;
            }
            return coordinateAsDouble;
        }
        private static uint ConvertCoordinate(double coordinate)
        {
            uint coordinateAsUInt = 0;
            try
            {
                if (coordinate < 0)
                {
                    coordinateAsUInt = Convert.ToUInt32((coordinate * -1) * 600000);
                    coordinateAsUInt = coordinateAsUInt + (int)0x10000000;
                }
                else
                {
                    coordinateAsUInt = Convert.ToUInt32(coordinate * 600000);
                }
            }
            catch (Exception exp)
            {
                //we get overflow exceptions if the coordinate is wrong
                _log.ErrorFormat("Unable to convert coordinate {0}, setting it to 0. Error - {1}", coordinate, exp.Message);
                coordinateAsUInt = 0;
            }

            return coordinateAsUInt;
        }

        public static void WriteBool(Stream stream, bool value)
        {
            if (value)
            {
                stream.WriteByte(0x01);
            }
            else
            {
                stream.WriteByte(0x00);
            }
        }
        public static bool ReadBool(byte[] data, ref int byteIndex)
        {
            if (data[byteIndex++] == 0x01)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void WriteNullableBool(Stream stream, bool? value)
        {
            if (!value.HasValue)
            {
                stream.WriteByte(0x02);
            }
            else if (value.Value)
            {
                stream.WriteByte(0x01);
            }
            else
            {
                stream.WriteByte(0x00);
            }
        }

        public static bool? ReadNullableBool(byte[] data, ref int byteIndex)
        {
            byte value = data[byteIndex++]; 
            if (value == 0x02)
            {
                return null; 
            }
            else if (value == 0x01)
            {
                return true; 
            }
            else
            {
                return false; 
            }
        }

        /// <summary>
        /// Write a float
        /// </summary>
        /// <param name="value"></param>
        public static void WriteFloat(Stream stream, float value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            stream.Write(temp, 0, 4);
        }

        /// <summary>
        /// This method will read 4 bytes form the buffer and convert them into an float.
        /// </summary>
        /// <returns></returns>
        public static float ReadFloat(byte[] data, ref int byteIndex)
        {
            float result = BitConverter.ToSingle(data, byteIndex);
            byteIndex += 4;
            return result;
        }

        /// <summary>
        /// Write a long lsb, to msb
        /// </summary>
        /// <param name="value"></param>
        public static void WriteLong(Stream stream, long value)
        {
            byte[] temp = BitConverter.GetBytes(value);
            stream.Write(temp, 0, 8);
        }

        /// <summary>
        /// This method will read 8 bytes form the buffer and convert them into a long.
        /// </summary>
        /// <returns></returns>
        public static long ReadLong(byte[] data, ref int byteIndex)
        {
            long result = BitConverter.ToInt64(data, byteIndex);
            byteIndex += 8;
            return result;
        }

        public static void CopyStream(Stream from, Stream to, int bufferSize)
        {
            from.Position = 0;
            byte[] buffer = new byte[bufferSize];
            int read;
            while ((read = from.Read(buffer, 0, bufferSize)) > 0)
            {
                to.Write(buffer, 0, read);
            }
            to.Flush();
        }

        public static void CopyStream(Stream from, Stream to)
        {

#if NETCF || NETCF2
            CopyStream(from, to, 512);
#else
            CopyStream(from, to, 8192);
#endif
        }


        /// <summary>
        /// Write a 4 byte nullable integer lsb, to msb
        /// </summary>
        /// <param name="value"></param>
        public static void Write4ByteNullableInteger(Stream stream, int? value)
        {
            WriteBool(stream, value.HasValue);
            if (value.HasValue)
            {
                Write4ByteInteger(stream, value.Value);
            }
        }

        /// <summary>
        /// This method will read a 4 byte nullable integer from the stream.
        /// </summary>
        /// <returns></returns>
        public static int? Read4ByteNullableInteger(byte[] data, ref int byteIndex)
        {
            if (ReadBool(data, ref byteIndex))
            {
                return Read4ByteInteger(data, ref byteIndex);
            }
            else
            {
                return null;
            }
        }
        #endregion

    }
}

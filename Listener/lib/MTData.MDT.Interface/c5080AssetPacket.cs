using System;
using System.Collections.Generic;
using System.Text;
using System.IO;

using MTData.Common.Utilities;
using log4net;

namespace MTData.MDT.Interface
{
    public class c5080AssetPacket
    {
        /*
        [CustomerBuildMajor][CustomerBuildMinor][CustomerName][TSC]
        [MapDllMajor][MapDllMinor][MapDataMajor][MapDataMinor][MapDataName][TSC]
        [ShellMajor][ShellMinor][ApiMajor][ApiMinor][CoreMajor][CoreMinor][CoreUiMajor][CoreUiMinor]
        [GuiMajor][GuiMinor][IapMajor][IapMinor][JobsMajor][JobsMinor][MapMajor][MapMinor]
         * 
        [PreTripMajor][PreTripMinor][ResourcesMajor][ResourcesMinor][ScreenMajor][ScreenMinor]
        [RouteMajor][RouteMinor] [WebServiceMajor][WebServiceMinor][SysMajor][SysMinor]
        [PhoneMajor][PhoneMinor][DownloadUpdatesMajor][DownloadUpdatesMinor]
        [PaltformMajor][PlatformMinor][WinCEMajor][WinCEMinor][CFCardTotal][CFCardFree]
        [MCCQueueCount][Battery]
         */
        #region Private Vars
        private const string sClassName = "c5080AssetPacket.";
        private ILog oLogging = LogManager.GetLogger(typeof(c5080AssetPacket));
        private byte _customerBuildMajor = (byte)0x00;
        private byte _customerBuildMinor = (byte)0x00;
        private byte MapDllBuildMajor = (byte)0x00;
        private byte MapDllBuildMinor = (byte)0x00;
        private byte MapDataMajor = (byte)0x00;
        private byte MapDataMinor = (byte)0x00;
        private byte ShellMajor = (byte)0x00;
        private byte ShellMinor = (byte)0x00;
        private byte ApiMajor = (byte)0x00;
        private byte ApiMinor = (byte)0x00;
        private byte CoreMajor = (byte)0x00;
        private byte CoreMinor = (byte)0x00;
        private byte CoreUiMajor = (byte)0x00;
        private byte CoreUiMinor = (byte)0x00;
        private byte GuiMajor = (byte)0x00;
        private byte GuiMinor = (byte)0x00;
        private byte IapMajor = (byte)0x00;
        private byte IapMinor = (byte)0x00;
        private byte JobsMajor = (byte)0x00;
        private byte JobsMinor = (byte)0x00;
        private byte MapMajor = (byte)0x00;
        private byte MapMinor = (byte)0x00;
        private byte PreTripMajor = (byte)0x00;
        private byte PreTripMinor = (byte)0x00;
        private byte ResourcesMajor = (byte)0x00;
        private byte ResourcesMinor = (byte)0x00;
        private byte ScreenMajor = (byte)0x00;
        private byte ScreenMinor = (byte)0x00;
        private byte RouteMajor = (byte)0x00;
        private byte RouteMinor = (byte)0x00;
        private byte WebServiceMajor = (byte)0x00;
        private byte WebServiceMinor = (byte)0x00;
        private byte SysMajor = (byte)0x00;
        private byte SysMinor = (byte)0x00;
        private byte PhoneMajor = (byte)0x00;
        private byte PhoneMinor = (byte)0x00;
        private byte DownloadUpdatesMajor = (byte)0x00;
        private byte DownloadUpdatesMinor = (byte)0x00;
        private byte PlatformMajor = (byte)0x00;
        private byte PlatformMinor = (byte)0x00;
        private byte WinCEMajor = (byte)0x00;
        private byte WinCEMinor = (byte)0x00;
        #endregion
        #region Public Vars
        public int CFCardTotal = 0;
        public int CFCardFree = 0;
        public int Battery = 0;
        public short MCCQueueCount = 0;
        public string CustomerName = "";
        public string MapDataName = "";
        #endregion
        #region Constructor
        public c5080AssetPacket()
        {
            
        }
        #endregion
        #region Public Functions
        public bool Decode(MemoryStream oMS)
        {
            bool bRet = false;
            try
            {
                PacketUtilities.ReadFromStream(oMS, ref _customerBuildMajor);
                PacketUtilities.ReadFromStream(oMS, ref _customerBuildMinor);
                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref CustomerName);
                PacketUtilities.ReadFromStream(oMS, ref MapDllBuildMajor);
                PacketUtilities.ReadFromStream(oMS, ref MapDllBuildMinor);
                PacketUtilities.ReadFromStream(oMS, ref MapDataMajor);
                PacketUtilities.ReadFromStream(oMS, ref MapDataMinor);
                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref MapDataName);
                PacketUtilities.ReadFromStream(oMS, ref ShellMajor);
                PacketUtilities.ReadFromStream(oMS, ref ShellMinor);
                PacketUtilities.ReadFromStream(oMS, ref ApiMajor);
                PacketUtilities.ReadFromStream(oMS, ref ApiMinor);
                PacketUtilities.ReadFromStream(oMS, ref CoreMajor);
                PacketUtilities.ReadFromStream(oMS, ref CoreMinor);
                PacketUtilities.ReadFromStream(oMS, ref CoreUiMajor);
                PacketUtilities.ReadFromStream(oMS, ref CoreUiMinor);
                PacketUtilities.ReadFromStream(oMS, ref GuiMajor);
                PacketUtilities.ReadFromStream(oMS, ref GuiMinor);
                PacketUtilities.ReadFromStream(oMS, ref IapMajor);
                PacketUtilities.ReadFromStream(oMS, ref IapMinor);
                PacketUtilities.ReadFromStream(oMS, ref JobsMajor);
                PacketUtilities.ReadFromStream(oMS, ref JobsMinor);
                PacketUtilities.ReadFromStream(oMS, ref MapMajor);
                PacketUtilities.ReadFromStream(oMS, ref MapMinor);
                PacketUtilities.ReadFromStream(oMS, ref PreTripMajor);
                PacketUtilities.ReadFromStream(oMS, ref PreTripMinor);
                PacketUtilities.ReadFromStream(oMS, ref ResourcesMajor);
                PacketUtilities.ReadFromStream(oMS, ref ResourcesMinor);
                PacketUtilities.ReadFromStream(oMS, ref ScreenMajor);
                PacketUtilities.ReadFromStream(oMS, ref ScreenMinor);
                PacketUtilities.ReadFromStream(oMS, ref RouteMajor);
                PacketUtilities.ReadFromStream(oMS, ref RouteMinor);
                PacketUtilities.ReadFromStream(oMS, ref WebServiceMajor);
                PacketUtilities.ReadFromStream(oMS, ref WebServiceMinor);
                PacketUtilities.ReadFromStream(oMS, ref SysMajor);
                PacketUtilities.ReadFromStream(oMS, ref SysMinor);
                PacketUtilities.ReadFromStream(oMS, ref PhoneMajor);
                PacketUtilities.ReadFromStream(oMS, ref PhoneMinor);
                PacketUtilities.ReadFromStream(oMS, ref DownloadUpdatesMajor);
                PacketUtilities.ReadFromStream(oMS, ref DownloadUpdatesMinor);
                PacketUtilities.ReadFromStream(oMS, ref PlatformMajor);
                PacketUtilities.ReadFromStream(oMS, ref PlatformMinor);
                PacketUtilities.ReadFromStream(oMS, ref WinCEMajor);
                PacketUtilities.ReadFromStream(oMS, ref WinCEMinor);
                PacketUtilities.ReadFromStream(oMS, ref CFCardTotal);
                PacketUtilities.ReadFromStream(oMS, ref CFCardFree);
                PacketUtilities.ReadFromStream(oMS, ref MCCQueueCount);
                PacketUtilities.ReadFromStream(oMS, ref Battery);
                bRet = true;
            }
            catch (System.Exception ex)
            {
                bRet = false;
                if (oLogging != null) oLogging.Error(sClassName + "Decode(MemoryStream oMS)", ex);
            }
            return bRet;
        }
        public bool Encode(MemoryStream oMS)
        {
            bool bRet = false;
            try
            {
                PacketUtilities.WriteToStream(oMS, _customerBuildMajor);
                PacketUtilities.WriteToStream(oMS, _customerBuildMinor);
                PacketUtilities.WriteToStream(oMS, CustomerName);
                PacketUtilities.WriteToStream(oMS, (byte)0x12);
                PacketUtilities.WriteToStream(oMS, MapDllBuildMajor);
                PacketUtilities.WriteToStream(oMS, MapDllBuildMinor);
                PacketUtilities.WriteToStream(oMS, MapDataMajor);
                PacketUtilities.WriteToStream(oMS, MapDataMinor);
                PacketUtilities.WriteToStream(oMS, MapDataName);
                PacketUtilities.WriteToStream(oMS, (byte)0x12);
                PacketUtilities.WriteToStream(oMS, ShellMajor);
                PacketUtilities.WriteToStream(oMS, ShellMinor);
                PacketUtilities.WriteToStream(oMS, ApiMajor);
                PacketUtilities.WriteToStream(oMS, ApiMinor);
                PacketUtilities.WriteToStream(oMS, CoreMajor);
                PacketUtilities.WriteToStream(oMS, CoreMinor);
                PacketUtilities.WriteToStream(oMS, CoreUiMajor);
                PacketUtilities.WriteToStream(oMS, CoreUiMinor);
                PacketUtilities.WriteToStream(oMS, GuiMajor);
                PacketUtilities.WriteToStream(oMS, GuiMinor);
                PacketUtilities.WriteToStream(oMS, IapMajor);
                PacketUtilities.WriteToStream(oMS, IapMinor);
                PacketUtilities.WriteToStream(oMS, JobsMajor);
                PacketUtilities.WriteToStream(oMS, JobsMinor);
                PacketUtilities.WriteToStream(oMS, MapMajor);
                PacketUtilities.WriteToStream(oMS, MapMinor);
                PacketUtilities.WriteToStream(oMS, PreTripMajor);
                PacketUtilities.WriteToStream(oMS, PreTripMinor);
                PacketUtilities.WriteToStream(oMS, ResourcesMajor);
                PacketUtilities.WriteToStream(oMS, ResourcesMinor);
                PacketUtilities.WriteToStream(oMS, ScreenMajor);
                PacketUtilities.WriteToStream(oMS, ScreenMinor);
                PacketUtilities.WriteToStream(oMS, RouteMajor);
                PacketUtilities.WriteToStream(oMS, RouteMinor);
                PacketUtilities.WriteToStream(oMS, WebServiceMajor);
                PacketUtilities.WriteToStream(oMS, WebServiceMinor);
                PacketUtilities.WriteToStream(oMS, SysMajor);
                PacketUtilities.WriteToStream(oMS, SysMinor);
                PacketUtilities.WriteToStream(oMS, PhoneMajor);
                PacketUtilities.WriteToStream(oMS, PhoneMinor);
                PacketUtilities.WriteToStream(oMS, DownloadUpdatesMajor);
                PacketUtilities.WriteToStream(oMS, DownloadUpdatesMinor);
                PacketUtilities.WriteToStream(oMS, PlatformMajor);
                PacketUtilities.WriteToStream(oMS, PlatformMinor);
                PacketUtilities.WriteToStream(oMS, WinCEMajor);
                PacketUtilities.WriteToStream(oMS, WinCEMinor);
                PacketUtilities.WriteToStream(oMS, CFCardTotal);
                PacketUtilities.WriteToStream(oMS, CFCardFree);
                PacketUtilities.WriteToStream(oMS, MCCQueueCount);
                PacketUtilities.WriteToStream(oMS, Battery);
                bRet = true;
            }
            catch (System.Exception ex)
            {
                bRet = false;
                if (oLogging != null) oLogging.Error(sClassName + "Encode(MemoryStream oMS)", ex);
            }
            return bRet;
        }
        #endregion
        #region Public Properties
        public int CustomerBuildMajor
        {
            get { return _customerBuildMajor; }
            set { _customerBuildMajor = (byte)value; }
        }
        public int CustomerBuildMinor
        {
            get { return _customerBuildMinor; }
            set { _customerBuildMinor = (byte)value; }
        }
        [System.Obsolete("use CustomerBuildMajor and CustomerBuildMinor")]
        public double CustomerBuild
        {
            get { return GetDouble(_customerBuildMajor, _customerBuildMinor); }
            set { SetDouble(value, ref _customerBuildMajor, ref _customerBuildMinor); }
        }

        public double MapDllBuild
        {
            get { return GetDouble(MapDllBuildMajor, MapDllBuildMinor); }
            set { SetDouble(value, ref MapDllBuildMajor, ref MapDllBuildMinor); }
        }
        public double MapData
        {
            get { return GetDouble(MapDataMajor, MapDataMinor); }
            set { SetDouble(value, ref MapDataMajor, ref MapDataMinor); }
        }
        public double Shell
        {
            get { return GetDouble(ShellMajor, ShellMinor); }
            set { SetDouble(value, ref ShellMajor, ref ShellMinor); }
        }
        public double Api
        {
            get { return GetDouble(ApiMajor, ApiMinor); }
            set { SetDouble(value, ref ApiMajor, ref ApiMinor); }
        }
        public double Core
        {
            get { return GetDouble(CoreMajor, CoreMinor); }
            set { SetDouble(value, ref CoreMajor, ref CoreMinor); }
        }
        public double CoreUI
        {
            get { return GetDouble(CoreUiMajor, CoreUiMinor); }
            set { SetDouble(value, ref CoreUiMajor, ref CoreUiMinor); }
        }
        public double Gui
        {
            get { return GetDouble(GuiMajor, GuiMinor); }
            set { SetDouble(value, ref GuiMajor, ref GuiMinor); }
        }
        public double IAP
        {
            get { return GetDouble(IapMajor, IapMinor); }
            set { SetDouble(value, ref IapMajor, ref IapMinor); }
        }
        public double Jobs
        {
            get { return GetDouble(JobsMajor, JobsMinor); }
            set { SetDouble(value, ref JobsMajor, ref JobsMinor); }
        }
        public double Map
        {
            get { return GetDouble(MapMajor, MapMinor); }
            set { SetDouble(value, ref MapMajor, ref MapMinor); }
        }
        public double PreTrip
        {
            get { return GetDouble(PreTripMajor, PreTripMinor); }
            set { SetDouble(value, ref PreTripMajor, ref PreTripMinor); }
        }
        public double Resources
        {
            get { return GetDouble(ResourcesMajor, ResourcesMinor); }
            set { SetDouble(value, ref ResourcesMajor, ref ResourcesMinor); }
        }
        public double Screen
        {
            get { return GetDouble(ScreenMajor, ScreenMinor); }
            set { SetDouble(value, ref ScreenMajor, ref ScreenMinor); }
        }
        public double Route
        {
            get { return GetDouble(RouteMajor, RouteMinor); }
            set { SetDouble(value, ref RouteMajor, ref RouteMinor); }
        }
        public double WebService
        {
            get { return GetDouble(WebServiceMajor, WebServiceMinor); }
            set { SetDouble(value, ref WebServiceMajor, ref WebServiceMinor); }
        }
        public double Sys
        {
            get { return GetDouble(SysMajor, SysMinor); }
            set { SetDouble(value, ref SysMajor, ref SysMinor); }
        }
        public double Phone
        {
            get { return GetDouble(PhoneMajor, PhoneMinor); }
            set { SetDouble(value, ref PhoneMajor, ref PhoneMinor); }
        }
        public double DownloadUpdates
        {
            get { return GetDouble(DownloadUpdatesMajor, DownloadUpdatesMinor); }
            set { SetDouble(value, ref DownloadUpdatesMajor, ref DownloadUpdatesMinor); }
        }
        public double Platform
        {
            get { return GetDouble(PlatformMajor, PlatformMinor); }
            set { SetDouble(value, ref PlatformMajor, ref PlatformMinor); }
        }
        public double WinCE
        {
            get { return GetDouble(WinCEMajor, WinCEMinor); }
            set { SetDouble(value, ref WinCEMajor, ref WinCEMinor); }
        }
        #endregion
        #region Private Functions
        private double GetDouble(byte bMajor, byte bMinor)
        {
            double dRet = 0;
            try
            {
                dRet = Convert.ToDouble(Convert.ToString((int)bMajor) + "." + Convert.ToString((int)bMinor));
            }
            catch (System.Exception)
            {
                dRet = 0;
            }
            return dRet;
        }
        private void SetDouble(double dValue, ref byte bMajor, ref byte bMinor)
        {
            string sValue = "";
            string[] sSplitValue = null;
            try
            {
                bMajor = (byte)0x00;
                bMinor = (byte)0x00;
                sValue = Convert.ToString(dValue);
                if (sValue.IndexOf(".") >= 0)
                {
                    sSplitValue = sValue.Split(".".ToCharArray());
                    if (sSplitValue.Length > 0) bMajor = (byte)Convert.ToInt32(sSplitValue[0]);
                    if (sSplitValue.Length > 1) bMinor = (byte)Convert.ToInt32(sSplitValue[1]);
                }
                else
                {
                    bMajor = (byte)Convert.ToInt32(sValue);
                    bMinor = (byte)0x00;

                }
            }
            catch (System.Exception)
            {
                bMajor = (byte)0x00;
                bMinor = (byte)0x00;
            }
        }
        #endregion
    }
}

using System;
using System.Collections;

namespace MTData.MDT.Interface
{
	public class cCollection
	{
		#region Private Variables
		private Hashtable objCol;
		private int iCount;
		private int iFirstIndex;
		private int iLastIndex;
		#endregion

		#region Public Properties
		public int Count
		{
			get
			{
				return iCount;
			}
		}

		public int FirstIndex
		{
			get
			{
				return iFirstIndex;
			}
		}

		public int LastIndex
		{
			get
			{
				return iLastIndex;
			}
		}
		#endregion

		#region Class Code
		public cCollection()
		{
			objCol = new Hashtable();
			iCount = 0;
			iFirstIndex = 0;
			iLastIndex = 0;
		}

		public void Dispose()
		{
			for (int iX = iFirstIndex; iX <= iLastIndex; iX++)
			{
				try
				{
					objCol.Remove(iX);
				}
				catch (System.Exception ex)
				{
					Console.Write(ex.Message + "\n");
				}					
			}
			objCol = null;
		}
		#endregion

		#region Public Functions
		public bool Add(Object objData)
		{
			int iTempCount = iCount;

			try
			{
				iTempCount++;
				objCol.Add(Convert.ToString(iTempCount), objData);
				iCount = iTempCount;
				iLastIndex = iFirstIndex + iCount;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return false;
			}
			return true;
		}

		public bool Remove(int iIndex)
		{
			try
			{
				objCol.Remove(Convert.ToString(iIndex));
				iCount--;
				iLastIndex = iFirstIndex + iCount;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return false;
			}
			return true;
		}

		public byte[] Item(int iIndex)
		{
			byte[] bRet = null;
			
			try
			{
				bRet =  ((byte []) objCol[Convert.ToString(iIndex)]);
				return  bRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return bRet;
			}
		}

		public int IntItem(int iIndex)
		{
			int bRet = 0;
			
			try
			{
				bRet =  Convert.ToInt32(objCol[Convert.ToString(iIndex)]);
				return  bRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return bRet;
			}
		}

		public cCollection colItem(int iIndex)
		{
			cCollection bRet = new cCollection();
			
			try
			{
				bRet =  ((cCollection) objCol[Convert.ToString(iIndex)]);
				return  bRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return bRet;
			}
		}

		public string StrItem(int iIndex)
		{
			string bRet = "";
			
			try
			{
				bRet =  ((string) objCol[Convert.ToString(iIndex)]);
				return  bRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return bRet;
			}
		}

		
		public double DoubleItem(int iIndex)
		{
			double dRet = 0;			

			try
			{
				dRet =  ((double) objCol[Convert.ToString(iIndex)]);
				return  dRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return dRet;
			}
		}

		public System.DateTime DateTimeItem(int iIndex)
		{
			System.DateTime dtRet = new System.DateTime(1,1,1);			

			try
			{
				dtRet =  ((System.DateTime) objCol[Convert.ToString(iIndex)]);
				return  dtRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return dtRet;
			}
		}

		public object ObjectItem(int iIndex)
		{
			object oRet = null;			

			try
			{
				oRet =  ((object) objCol[Convert.ToString(iIndex)]);
				return  oRet;
			}
			catch (System.Exception ex)
			{
				Console.Write(ex.Message + "\n");
				return oRet;
			}
		}
		#endregion
	}
}

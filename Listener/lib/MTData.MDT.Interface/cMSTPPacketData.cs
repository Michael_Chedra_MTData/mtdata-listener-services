using System;
using System.IO;
using System.Collections;
using MTData.MotFileReader;
using MTData.MDT.Interface;
using MTData.Common.Utilities;
using System.Collections.Specialized;
using log4net;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// Summary description for cMSTPPacketData.
	/// </summary>
	public class cMSTPPacketData
	{
		#region Incomming Packet Defs
		// [GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
		// [Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
		// N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Job ID][Stop #][SBS][Spare]
		// I - Logon to System - [CmdType] [Header] [Password] [PhoneBookID] [Personal PhoneBook ID] [SBS] [Spare]
		// O - Log off the System -[CmdType] [Header] [SBS] [Spare]
		// R - Request Packets
		//		J - Request Jobs - [CmdType] [SubCmd] [Header] [SBS] [Spare]
		// U - Alert Packet - [CmdType] [SubCmd] [Header] [SBS] [Spare]
		// M - Messages
		//	R - Driver Read Meassage - [CmdType] [SubCmd] [Header] [MessageID] [SBS][Spare]
		//	P - Predefined Responce - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
		//	F - Freeform Message - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
		// K - Phone call report
		//	I - Incomming Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]
		//	O - Outgoing Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]
		#endregion

		#region Outgoing Packet Defs
		// N - Ack -	[CmdType] [Result Code] [SBS] [Spare]
		// M - Message - [CmdType] ([Description] [EOL]) [SBS] [Spare]
		// K - Phonebook Entries
		//	P - Global Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
		//	D - Drivers Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
		// L - Login Ack
		//	U - Incorrect Username - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		//	P - Incorrect Password - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		//	C - Correct Login  - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		//	O - Logout unit - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		#endregion

		#region Private Vars
        private ILog _log = LogManager.GetLogger(typeof(cMSTPPacketData));
		private bool bTranslateRJs = true;
        private bool _bLogOutbound = false;
		#endregion

		#region Public Vars
		// Shareed Public variables
		public int iMobileID = 0;
		public string CommandType = "";				// [CmdType]
		public int JobID = 0;								// [Job ID]
		public int StopID = 0;								// [Stop #]
		public int CodeNumber = 0;						// [FailCode], [SucessCode], [Function ID]
		public string Message = "";						// [Message Data]
	
		// Incomming Packet Specific
		public string SubCommandType = "";		// [SubCmd]
		public bool StartFunction = false;			// [Start / Stop]
		public int ReferenceNumber = 0;
		public int TimeLoading = 0;
		public int DivisionID = 0;
		public int CallTime = 0;
		public string CallType = "";
		public string SimCardNumber = "";
		public byte AllowFreeDial = (byte) 0x00; // [AllowFreeDial] 0 = No, 1 = Yes
        public ushort AutoLogoffXMinsAfterIgnOff = 0;   // [AutoLogoffXMinsAfterIgnOff] 0 = Feature disabled
		public int ResultCode = 0;							// [Result Code]
		public bool YesNo = false;						// [Yes No] -> Yes = true
		public int ResponceTimeHours = 0;			// [ResponceTimeHours]
		public int ResponceTimeMinutes = 0;			// [ResponceTimeMinutes]
		public int SuburbID = 0;							// [Suburb ID]
		public int MessageID = 0;							// [Message ID]
		public int VehicleID = 0;							// [VehicleID]
		public int FleetID = 0;								// [FleetID]
		public int DriverID = 0;							// [DriverID]
		public int ProtocolVersion = 0;					// [Protocol Version]
		public int HardwareType = 0;					// [Hardware Type]
		public int TimeOffset = 0;						// [TimeOffset]
		public byte DLSByte = (byte) 0x00;		// [Day Light Savings Byte]
		public int MessagebookID = 0;					// [MessageBook ID]
		public int QuestionBookID = 0;					// Question book list id
		public int DivisionReasonID = 0;				// [DivisionReasonID]
		public int DelayReasonID = 0;					// [DelayReasonID]
		public int RejectedReasonID = 0;			// [RejectedReasonID]
		public int Distance = 0;								// [Distance]
		public int Odometer = 0;
		public int DeviceTimeSeconds = 0;					// Time since last GPS lock
		public int FirmwareRevMajor = 0;
		public int FirmwareRevMinor = 0;
		public int HardwareRevMajor = 0;
		public int HardwareRevMinor = 0;
		public int BatteryLevel = 0;
		public int Options = 0;		
		public double CurTemp = 0;
		public string Spare1 = "";
		public string Spare2 = "";
		public string Spare3 = "";
		public string Spare4 = "";
		public string Spare5 = "";
		public string FirmwareVariant = "";
		public string DelayDescription = "";
		public string NoOfPallets = "";
		public string EmailAddress = "";
		public string PhoneNumber = "";
		public string Username = "";						// [Username]
		public string Password = "";						// [Password]
		public string PODName = "";					// [POD Name]
		public string JobTime = "";						// [Time], [Time in Mins]
		public string BusinessName = "";				// [Business Name]
		public string InvoiceNumber = "";			// [Invoice Number]
		public StringCollection BarCodes = null	;				// [Bar Code x]
		public StringCollection ReturnBarCodes = null	;	// [Return Bar Code x]
		public StringCollection PalletTypeCodes = null;
		public StringCollection PalletNumbers = null;
		public StringCollection Trailers = null;
		public StringCollection TrailerOdometers = null;
		public byte[] bGenericData = null;			// [200 bytes]
		public byte[] SignatureData = null;				// [Signature]



		// Outgoing Packet Specific
        public byte FunctionID = (byte)0x00;
        public bool FunctionStatus = false;
        public byte[] FunctionParamers = null;
        public byte[] FunctionResult = null;
        public int TotalNumberOfStops = 0;
		public int PhoneBookID = 0;
		public int PersonalPhoneBookID = 0;
		public int DivisionListID = 0;
		public int DelayReasonListID = 0;
		public int RejectionReasonListID = 0;
		public string JobTitle = "";
		public string ContactName = "";
		public string ContactPh = "";
		public string JobDescription = "";
		public string LoginReply = "";
		public StringCollection PhoneBookLocations = new StringCollection();
		public StringCollection PhoneBookNumbers = new StringCollection();
		public StringCollection PhoneBookNames = new StringCollection();
		public StringCollection MessageNames = new StringCollection();
		public StringCollection DivisionIDs = new StringCollection();
		public StringCollection DivisionNames = new StringCollection();
		public StringCollection DelayReasonNames = new StringCollection();
		public StringCollection DelayReasonIDs = new StringCollection();
		public StringCollection RejectionReasonNames = new StringCollection();
		public StringCollection RejectionReasonIDs = new StringCollection();

        // Firmware download vars
        public byte FirmwareVersionMajor = (byte)0x00;
        public byte FirmwareVersionMinor = (byte)0x00;
        public bool ForceFirmwareUpdate = false;

        public byte Firmware5010VersionMajor = (byte)0x00;
        public byte Firmware5010VersionMinor = (byte)0x00;
        public bool ForceFirmware5010Update = false;

        public byte CPLDVersionMajor = (byte)0x00;
        public byte CPLDVersionMinor = (byte)0x00;
        public bool ForceCPLDUpdate = false;

        public byte WinCEVersionMajor = (byte)0x00;
        public byte WinCEVersionMinor = (byte)0x00;
        public bool ForceWinCEUpdate = false;

        public ushort UnitTypeID = 0;
        public ushort SegmentNumber = 0;
        public ushort NumberOfDataSegments = 0;
        public byte DownloadVersion = 0;
        public byte NumberOfDataSegmentsInThisPacket = 0;
        public byte[] bDataSegment = null;
        public byte[] DownloadCEParcel = null;
        public byte[] DownloadCPLDParcel = null;
        public ArrayList DownloadMotParcel = null;
        public uint ProgramTotalCheckSum = 0;
        public uint PacketTotalCheckSum = 0;
        public ushort ProgramPacketCountTotal = 0;
        public uint ProgramByteTotal = 0;
        public byte ProgramDataBytesCheckSum = (byte)0x00;

		// GPS Data
		public cGPSData GPSPosition = null;
        public c5080AssetPacket o5080AssetPacket = null;
		#endregion

		public cMSTPPacketData(  bool TranslateRJs)
		{
			
			bTranslateRJs = TranslateRJs;
            _bLogOutbound = false;
		}

        public cMSTPPacketData( bool TranslateRJs, bool bLogOutboundPackets)
        {
            
            bTranslateRJs = TranslateRJs;
            _bLogOutbound = bLogOutboundPackets;
        }
		#region Build Packet Data
		public byte[] BuildOutgoingPacket()
		{
			byte[] bRet = null;
            byte[] bConvert = null;
			int iEntries = 0;
            int iCheckSum = 0;
			MemoryStream oMS = new MemoryStream();
			
			PacketUtilities.WriteToStream(oMS, CommandType);			
			switch(CommandType)
			{
                case "c":
                    #region Download CPLD Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                    PacketUtilities.WriteToStream(oMS, SubCommandType);                                     // Command Type
                    if (SubCommandType == "H")
                    {
                        #region Download Header
                        if (_log != null && _bLogOutbound) _log.Info("Building CPLD Download Header Packet");
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMajor);                               // CPLD Version Major
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMinor);                               // CPLD Version Minor
                        PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);                           // Segment Count
                        PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                        #endregion
                    }
                    else if (SubCommandType == "D")
                    {
                        #region Download Packet Data
                        if (_log != null && _bLogOutbound) _log.Info("Building CPLD Download Data Packet");
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMajor);			                    // CPLD Ver Major (1 Byte)
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMinor);			                    // CPLD Ver Minor (1 Byte)
                        PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);	                        // Number of Segements (2 Bytes)
                        PacketUtilities.WriteToStream(oMS, SegmentNumber);	                                // Segment Number
                        PacketUtilities.WriteToStream(oMS, (ushort)DownloadCPLDParcel.Length);	            // Segment Length
                        PacketUtilities.WriteToStream(oMS, DownloadCPLDParcel);	                            // Segment
                        for (int X = 0; X < DownloadCPLDParcel.Length; X++)
                        {
                            iCheckSum += (int)DownloadCPLDParcel[X];
                        }
                        bConvert = BitConverter.GetBytes(iCheckSum);
                        PacketUtilities.WriteToStream(oMS, bConvert[0]);	                                // Segment Checksum 1
                        PacketUtilities.WriteToStream(oMS, bConvert[1]);	                                // Segment Checksum 2
                        #endregion
                    }
                    else if (SubCommandType == "V")
                    {
                        #region Download Verify
                        if (_log != null && _bLogOutbound) _log.Info("Building CPLD Download Verify Packet");
                        PacketUtilities.WriteToStream(oMS, DownloadVersion);                                // Download Version
                        PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                        PacketUtilities.WriteToStream(oMS, ProgramPacketCountTotal);                        // Segment Count
                        PacketUtilities.WriteToStream(oMS, ProgramByteTotal);                               // Program Byte Total
                        #endregion
                    }
                    #endregion
                    break;
                case "d":
                    #region Download Win CE Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                    PacketUtilities.WriteToStream(oMS, SubCommandType);                                     // Command Type
                    if (SubCommandType == "H")
                    {
                        #region Download Header
                        if (_log != null && _bLogOutbound) _log.Info("Building Download Header Packet");
                        PacketUtilities.WriteToStream(oMS, WinCEVersionMajor);                              // WinCE Version Major
                        PacketUtilities.WriteToStream(oMS, WinCEVersionMinor);                              // WinCE Version Minor
                        PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);                           // Segment Count
                        PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                        #endregion
                    }
                    else if (SubCommandType == "D")
                    {
                        #region Download Packet Data
                        if (_log != null && _bLogOutbound) _log.Info("Building Download Data Packet");
                        PacketUtilities.WriteToStream(oMS, WinCEVersionMajor);			                    // WinCE Ver Major (1 Byte)
                        PacketUtilities.WriteToStream(oMS, WinCEVersionMinor);			                    // WinCE Ver Minor (1 Byte)
                        PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);	                        // Number of Segements (2 Bytes)
                        PacketUtilities.WriteToStream(oMS, SegmentNumber);	                                // Segment Number
                        PacketUtilities.WriteToStream(oMS, (ushort)DownloadCEParcel.Length);	            // Segment Length
                        PacketUtilities.WriteToStream(oMS, DownloadCEParcel);	                            // Segment
                        for (int X = 0; X < DownloadCEParcel.Length; X++)
                        {
                            iCheckSum += (int)DownloadCEParcel[X];
                        }
                        bConvert = BitConverter.GetBytes(iCheckSum);
                        PacketUtilities.WriteToStream(oMS, bConvert[0]);	                                // Segment Checksum 1
                        PacketUtilities.WriteToStream(oMS, bConvert[1]);	                                // Segment Checksum 2
                        #endregion
                    }
                    else if (SubCommandType == "V")
                    {
                        #region Download Verify
                        if (_log != null && _bLogOutbound) _log.Info("Building WinCE Download Verify Packet");
                        PacketUtilities.WriteToStream(oMS, DownloadVersion);                                // Download Version
                        PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                        PacketUtilities.WriteToStream(oMS, ProgramPacketCountTotal);                        // Segment Count
                        PacketUtilities.WriteToStream(oMS, ProgramByteTotal);                               // Program Byte Total
                        #endregion
                    }
                    #endregion
                    break;
                case "D":
                    #region Download MOT Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                    PacketUtilities.WriteToStream(oMS, SubCommandType);                                     // Command Type
                    if (SubCommandType == "H")
                    {
                        #region Download Header
                        if (_log != null && _bLogOutbound) _log.Info("Building Download Header Packet");
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);                           // Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);                           // Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);                           // Segment Count
                        PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                        #endregion
                    }
                    else if (SubCommandType == "D")
                    {
                        #region Download Packet Data
                        if (_log != null && _bLogOutbound) _log.Info("Building Download Data Packet");
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);			                // Firmware Ver Major (1 Byte)
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);			                // Firmware Ver Minor (1 Byte)
                        PacketUtilities.WriteToStream(oMS, (ushort)DownloadMotParcel.Count);	            // Number of Segements (2 Bytes)
                        ushort iMaxDownloadMotParcelNumber = Convert.ToUInt16(DownloadMotParcel.Count - SegmentNumber);
                        if (iMaxDownloadMotParcelNumber > 2)
                            iMaxDownloadMotParcelNumber = 2;
                        if (iMaxDownloadMotParcelNumber >= 0)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)iMaxDownloadMotParcelNumber);	        // Number of segments in this packet (1 Byte)
                            for (ushort i = SegmentNumber; i < iMaxDownloadMotParcelNumber + SegmentNumber; i++)
                            {
                                cParcel oParcel = (cParcel)DownloadMotParcel[(int)i];
                                PacketUtilities.WriteToStream(oMS, i);										// PacketNumber (2 Bytes)
                                PacketUtilities.WriteToStream(oMS, oParcel.Data);					        // Segment Data (250 Bytes)
                                PacketUtilities.WriteToStream(oMS, (ushort)oParcel.CheckSum);		        // Segment Checksum (2 Bytes)
                                PacketUtilities.WriteToStream(oMS, (byte)0xBB);						        // End of Segment (1 Byte)
                            }
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0xCC);						                // End of Downlaod Data (1 Byte)
                        #endregion
                    }
                    else if (SubCommandType == "V")
                    {
                        #region Download Verify
                        if (_log != null && _bLogOutbound) _log.Info("Building MOT File Download Verify Packet");
                        PacketUtilities.WriteToStream(oMS, DownloadVersion);                                // Download Version
                        PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                        PacketUtilities.WriteToStream(oMS, PacketTotalCheckSum);                            // Total Checksum
                        PacketUtilities.WriteToStream(oMS, ProgramPacketCountTotal);                        // Program Packet Count
                        PacketUtilities.WriteToStream(oMS, ProgramByteTotal);                               // Program Byte Total
                        PacketUtilities.WriteToStream(oMS, ProgramDataBytesCheckSum);                       // Program Data Bytes checksum
                        #endregion
                    }
                    #endregion
                    break;
				case "N": 
					#region Ack Packet
					// [CmdType] [Result Code] [SBS] [Spare]
					PacketUtilities.WriteToStream(oMS, (byte) this.CodeNumber);					// Result Code
					#endregion
					break;
				case "L":
					#region Login Reply packet
					// L - Login Ack
					//	U - Incorrect Username - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					//	P - Incorrect Password - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					//	C - Correct Login  - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					//	O - Logout unit - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					PacketUtilities.WriteToStream(oMS, this.SubCommandType);					// Login Responce
					PacketUtilities.WriteToStream(oMS, (byte) this.TimeOffset);					// Time Offset
					PacketUtilities.WriteToStream(oMS, this.DLSByte);							// DLS Bit
					PacketUtilities.WriteToStream(oMS, this.AllowFreeDial);						// Allow Free dial.
                    PacketUtilities.WriteToStream(oMS, this.AutoLogoffXMinsAfterIgnOff);		// Auto Logoff X Mins After Ign Off
					#endregion
					break;
				case "M":
					#region Send a driver message
					// M - Message - [CmdType] [Message ID] ([Description] [EOL]) [SBS] [Spare]
					PacketUtilities.WriteToStream(oMS, (byte) this.MessageID);					// Message ID
					PacketUtilities.WriteToStream(oMS, this.Message);									// Message
					PacketUtilities.WriteToStream(oMS, (byte) 0x0F);										// Message ID
					#endregion
					break;
                case "Q":
                    #region Start Pre-defined Function Packet - [Function ID][Start / Stop][Parameter Length][Function Parameters][EOP]
                    if (_log != null && _bLogOutbound) _log.Info("Building Predefined Function Packet for mobile ID " + Convert.ToString(iMobileID) + ", Function ID = " + Convert.ToString(FunctionID));

                    PacketUtilities.WriteToStream(oMS, FunctionID);											// Function ID
                    PacketUtilities.WriteToStream(oMS, (StartFunction ? (byte)0x01 : (byte)0x00));			// Start / Stop
                    if (FunctionParamers != null)
                    {
                        PacketUtilities.WriteToStream(oMS, (ushort)FunctionParamers.Length);				// Parameter Length
                        PacketUtilities.WriteToStream(oMS, FunctionParamers);								// Function Parameters
                    }
                    else
                    {
                        PacketUtilities.WriteToStream(oMS, (ushort)0);										// Parameter Length
                    }
                    PacketUtilities.WriteToStream(oMS, (byte)0x12);										    // EOP
                    #endregion
                    break;
                case "K":
					#region Send a phone book Entry
					// K - Phonebook Entries
					//	P - Global Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
					//	D - Drivers Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
					iEntries = 0;
					if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > iEntries) iEntries = PhoneBookNumbers.Count;
					if (PhoneBookNames != null) if (PhoneBookNames.Count > iEntries) iEntries = PhoneBookNames.Count;
					// according to specs, max number for global phb is 50
					if (iEntries > 50) iEntries = 50;

					PacketUtilities.WriteToStream(oMS, this.SubCommandType);					// Sub Command
					PacketUtilities.WriteToStream(oMS, (byte) this.PhoneBookID);				// Phonebook ID
					if (iEntries > 0)
					{
						// global phonebook starts from 0 and personal from 50
						int iStart = 0;
						if (this.SubCommandType == "D")
							iStart = 50;

						for (int X = iStart; X < iStart + iEntries; X++)
						{
							PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// PSS - Phone book separator
							PacketUtilities.WriteToStream(oMS, (byte)X);										// Entry Index
							if (PhoneBookNumbers != null)
								PacketUtilities.WriteToStream(oMS, (string) PhoneBookNumbers[X - iStart]);
							else
								PacketUtilities.WriteToStream(oMS, "0");										// Phone Number
							PacketUtilities.WriteToStream(oMS, (byte)0x0B);								// NSC - Phone book separator
							if (PhoneBookNames != null)
								PacketUtilities.WriteToStream(oMS, (string) PhoneBookNames[X - iStart]);
							else
								PacketUtilities.WriteToStream(oMS, "N/A");									// Entry Name
						}
					}
					
					#endregion
					break;
                case "V":
                    #region Firmware Version packet
                    // V - [CmdType][MOT Firmware Major][MOT Firmware Minor][Force MOT Firmware Download][WinCE Firmware Major][WinCE Firmware Minor][Force WinCE Firmware Download][CPLD Firmware Major][CPLD Firmware Minor][Force CPLD Firmware Download][SBS] [Spare]
                    PacketUtilities.WriteToStream(oMS, this.FirmwareVersionMajor);
                    PacketUtilities.WriteToStream(oMS, this.FirmwareVersionMinor);
                    PacketUtilities.WriteToStream(oMS, this.ForceFirmwareUpdate);
                    PacketUtilities.WriteToStream(oMS, this.Firmware5010VersionMajor);
                    PacketUtilities.WriteToStream(oMS, this.Firmware5010VersionMinor);
                    PacketUtilities.WriteToStream(oMS, this.ForceFirmware5010Update);
                    PacketUtilities.WriteToStream(oMS, this.WinCEVersionMajor);
                    PacketUtilities.WriteToStream(oMS, this.WinCEVersionMinor);
                    PacketUtilities.WriteToStream(oMS, this.ForceWinCEUpdate);
                    PacketUtilities.WriteToStream(oMS, this.CPLDVersionMajor);
                    PacketUtilities.WriteToStream(oMS, this.CPLDVersionMinor);
                    PacketUtilities.WriteToStream(oMS, this.ForceCPLDUpdate);
                    #endregion
                    break;
				default:
					bRet = null;
					break;
			}

			PacketUtilities.WriteToStream(oMS, (byte)0x07);											// PSS - Phone book separator
			bRet = oMS.ToArray();
			string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
			if(_log != null) _log.Info(sData);
			return bRet;
		}
		#endregion

		#region Parse Incomming Packet
		public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
		{
			string sRet = "";
			try
			{
				this.iMobileID = Convert.ToInt32(sUnitID);
				sRet = TranslateMsgToHost(bMsg, sUnitID);
			}
			catch(System.Exception ex)
			{
				if(_log != null) 
				{
					if(bMsg == null)
						_log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = null, sUnitID = '" + sUnitID + "') " + ex.InnerException.Message + " " + ex.Message, ex);
					else
						_log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = " + BitConverter.ToString(bMsg) + ", sUnitID = '" + sUnitID + "') " + ex.InnerException.Message + ex.Message, ex);
				}
			}
			return sRet;
		}

		private string TranslateMsgToHost(byte[] bMsg, string sUnitID)
		{
			// Special Field Defs
			// [GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
			// [Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]

			// Packet Types
			// N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Job ID][Stop #][SBS][Spare]
			// I - Logon to System - [CmdType] [Header] [Password] [PhoneBookID] [Personal PhoneBook ID] [SBS] [Spare]
			// O - Log off the System -[CmdType] [Header] [SBS] [Spare]
			// R - Request Packets
			//		J - Request Jobs - [CmdType] [SubCmd] [Header] [SBS] [Spare]
			// U - Alert Packet - [CmdType] [SubCmd] [Header] [SBS] [Spare]
			// M - Messages
			//	R - Driver Read Meassage - [CmdType] [SubCmd] [Header] [MessageID] [SBS][Spare]
			//	P - Predefined Responce - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
			//	F - Freeform Message - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
			// K - Phone call report
			//	I - Incomming Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]
			//	O - Outgoing Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]

            //4B-50-06-0A-00-30-33-39-35-37-34-35-37-30-30-0B-4D-54-44-61-74-61-0A-01-30-34-33-39-36-32-38-38-32-32-0B-4D-61-74-74-68-65-77-0A-02-30-33-39-35-37-34-35-37-38-39-0B-4D-74-64-61-74-61-20-68-65-6C-70-64-65-73-6B-0A-03-30-33-39-35-37-34-35-37-36-32-0B-54-69-6D-20-57-6F-72-6B-07

			#region Local Variables
			byte[] bFailCode = new byte[4];
			byte[] bMBPPacket = new byte[1];
			byte[] bStopID = new byte[4];
			byte[] bSignatureLen = new byte[4];
			byte[] bFunctionID = new byte[1];
			byte[] bStartStop = new byte[1];
			ushort isValue = 0;
			byte bValue = (byte) 0x00;
            byte bTemp = (byte)0x00;
			byte[] bConvert = null;
			MemoryStream oMS = new MemoryStream(bMsg, 0, bMsg.Length);
			#endregion

			#region Get the CommandType and SubCommandType for the packet
			PacketUtilities.ReadFromStream(oMS, (int) 1, ref this.CommandType);
			if (this.CommandType == "R" || this.CommandType == "M" || this.CommandType == "K" || this.CommandType == "U")
				PacketUtilities.ReadFromStream(oMS, (int) 1, ref this.SubCommandType);
			#endregion
            if (CommandType != "d" && CommandType != "D" && CommandType != "c" && this.CommandType != "N")
			{
				#region Read the header record.
				// [Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
				// Get the vehicle
				PacketUtilities.ReadFromStream(oMS, ref isValue);
				this.iMobileID = (int) isValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				this.FleetID= (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref isValue);
				this.VehicleID = isValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				this.ProtocolVersion= (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				this.HardwareType= (int) bValue;
				#region [GPS]
				this.GPSPosition = new cGPSData();
				// LSC Byte
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				// Lat
				PacketUtilities.ReadFromStream(oMS, (int) 4, ref bConvert);
				this.GPSPosition.dLatitude = LatLonConversion.ConvLatLon(bConvert);
				// Long
				PacketUtilities.ReadFromStream(oMS, (int) 4, ref bConvert);
				this.GPSPosition.dLongitude = LatLonConversion.ConvLatLon(bConvert);
				#region Get GPS Time string
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				int iDay = (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				int iMonth = (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				int iYear = 2000 + (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				int iHour = (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				int iMinute = (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				int iSecond = (int) bValue;
				try
				{
					this.GPSPosition.dtGPSTime= new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
				}
				catch(System.Exception)
				{
				}
				#endregion
				PacketUtilities.ReadFromStream(oMS, ref bValue);
				this.GPSPosition.iSpeed = (int) bValue;
				PacketUtilities.ReadFromStream(oMS, ref isValue);
				this.GPSPosition.iHeading = isValue;
				PacketUtilities.ReadFromStream(oMS, ref this.Distance);
				PacketUtilities.ReadFromStream(oMS, ref isValue);
				this.DeviceTimeSeconds = isValue;
				#endregion
				PacketUtilities.ReadFromStream(oMS, (byte) 0x0A, ref this.Username);
				#endregion
			}

			switch(CommandType)
			{
                case "c":
                    #region Download CPLD Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                    SubCommandType = Convert.ToString((char)bTemp);
                    if (SubCommandType == "d")
                    {
                        if (_log != null && _bLogOutbound) _log.Info("Reading CPLD File Download Header Request Packet");
                    }
                    if (SubCommandType == "r")
                    {
                        if (_log != null && _bLogOutbound) _log.Info("Reading CPLD File Download Request Segment Packet");
                        PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);				                // Segment Number
                    }
                    else if (SubCommandType == "v")
                    {
                        if (_log != null && _bLogOutbound) _log.Info("Reading CPLD File Download Verify Packet");
                    }
                    #endregion
                    break;
                case "d":
                    #region Download Win CE Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                    SubCommandType = Convert.ToString((char)bTemp);
                    if (SubCommandType == "d")
                    {
                        if (_log != null && _bLogOutbound) _log.Info("Reading CE File Download Header Request Packet");
                    }
                    if (SubCommandType == "r")
                    {
                        if (_log != null && _bLogOutbound) _log.Info("Reading CE File Download Request Segment Packet");
                        PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);				                // Segment Number
                    }
                    else if (SubCommandType == "v")
                    {
                        if (_log != null && _bLogOutbound) _log.Info("Reading CE File Download Verify Packet");
                    }
                    #endregion
                    break;
                case "D":
                    #region Download MOT Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                    SubCommandType = Convert.ToString((char)bTemp);
                    PacketUtilities.ReadFromStream(oMS, ref UnitTypeID);	                                // Unit Type ID
					switch (SubCommandType)
					{
						case "d":
							if (_log != null && _bLogOutbound)
								_log.Info("Reading MOT File Download Header Request Packet");
							break;
						case "r":
							if (_log != null && _bLogOutbound)
								_log.Info("Reading MOT File Download Request Segment Packet:" + SegmentNumber.ToString());
							PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);				                // Segment Number
							break;
						case "v":
							if (_log != null && _bLogOutbound)
								_log.Info("Reading MOT File Download Verify Packet");
							break;
						default:
							if (_log != null && _bLogOutbound)
								_log.Info("Download packet with uknown subcommand:" + SubCommandType);
							break;
					}
                    #endregion
                    break;
				case "N":
					#region Ack an NAck Responce
					// N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Job ID][Stop #][SBS][Spare]
					PacketUtilities.ReadFromStream(oMS, ref isValue);
					this.iMobileID = (int) isValue;
					PacketUtilities.ReadFromStream(oMS, ref bValue);
					this.ResultCode= (int) bValue;
					PacketUtilities.ReadFromStream(oMS, ref this.JobID);
					PacketUtilities.ReadFromStream(oMS, ref bValue);
					this.StopID= (int) bValue;
					#endregion
					break;
				case "I":
					#region Logon to System
					// I - Logon to System - [CmdType] [Header] [Password] [PhoneBookID] [MessageBook ID] [SBS] [QuestionBookID]
					PacketUtilities.ReadFromStream(oMS,(int) 4, ref this.Password);
					PacketUtilities.ReadFromStream(oMS, ref bValue);
					this.PhoneBookID= (int) bValue;
					PacketUtilities.ReadFromStream(oMS, ref bValue);
					this.MessagebookID = (int) bValue;
					PacketUtilities.ReadFromStream(oMS, ref bValue);
					//updated on the 30th of April 2009 to support driver questions
					try
					{
						PacketUtilities.ReadFromStream(oMS, ref bValue);
						this.QuestionBookID = bValue;
					}
					catch(Exception)
					{
						this.QuestionBookID = 0;
					}
					#endregion
					break;
				case "O":
					#region Logoff System
					// O - Log off the System -[CmdType] [Header] [SBS] [Spare]
					#endregion
					break;
				case "R":
					#region Request Packet
					// R - Request Packets
					//		J - Request Jobs - [CmdType] [SubCmd] [Header] [SBS] [Spare]
					#endregion
					break;
				case "U":
					#region Alert Packet
					// U - Alert Packet - [CmdType] [SubCmd] [Header] [SBS] [Spare]
					#endregion
					break;
				case "M":
					#region Message System
					// M - Messages
					//	R - Driver Read Meassage - [CmdType] [SubCmd] [Header] [MessageID] [SBS][Spare]
					//	P - Predefined Responce - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
					//	F - Freeform Message - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
					if (this.SubCommandType == "R")
					{
						PacketUtilities.ReadFromStream(oMS, ref this.MessageID);
					}
					else
					{
						if (this.SubCommandType == "P")
						{
							PacketUtilities.ReadFromStream(oMS, ref this.MessageID);
							PacketUtilities.ReadFromStream(oMS, ref bValue);
							if (bValue == (byte) 0x01)
								this.YesNo = true;
							else
								this.YesNo = false;
							PacketUtilities.ReadFromStream(oMS, ref bValue);
							this.ResponceTimeHours = (int) bValue;
							PacketUtilities.ReadFromStream(oMS, ref bValue);
							this.ResponceTimeMinutes = (int) bValue;
						}
						else
						{
							if (this.SubCommandType == "F")
							{
								PacketUtilities.ReadFromStream(oMS, (byte) 0x07, ref this.Message);
							}
						}
					}
					#endregion
					break;
                case "Q":
                    #region Predefined Function Result Packet - [Function ID][Status][Result Length][Result]
                    if (_log != null && _bLogOutbound) _log.Info("Reading Logout Packet for mobile ID " + Convert.ToString(iMobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Function ID = " + Convert.ToString(FunctionID));
                    PacketUtilities.ReadFromStream(oMS, ref FunctionID);							        // Function ID
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Status
                    if (bTemp == (byte)0x00)
                        FunctionStatus = false;
                    else
                        FunctionStatus = true;
                    PacketUtilities.ReadFromStream(oMS, ref isValue);								        // Result Length
                    PacketUtilities.ReadFromStream(oMS, (int)isValue, ref FunctionResult);	                // Result
                    #endregion
                    break;
				case "K":
					#region Phone call report
					// K - Phone call report
					//	I - Incomming Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SimCardNumber] [SBS] [Spare]
					//	O - Outgoing Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SimCardNumber] [SBS] [Spare]
					PacketUtilities.ReadFromStream(oMS, (byte) 0x0A, ref this.PhoneNumber);
					PacketUtilities.ReadFromStream(oMS, ref isValue);
					this.CallTime = (int) isValue;
					PacketUtilities.ReadFromStream(oMS, (byte) 0x07, ref this.SimCardNumber);
					#endregion
					break;
                case "Z":
                    #region 5080 Asset Packet - [CustomerBuildMajor][CustomerBuildMinor][CustomerName][TSC][MapDllMajor][MapDllMinor][MapDataMajor][MapDataMinor][MapDataName][TSC][ShellMajor][ShellMinor][ApiMajor][ApiMinor][CoreMajor][CoreMinor][CoreUiMajor][CoreUiMinor][GuiMajor][GuiMinor][IapMajor][IapMinor][JobsMajor][JobsMinor][MapMajor][MapMinor][PreTripMajor][PreTripMinor][ResourcesMajor][ResourcesMinor][ScreenMajor][ScreenMinor][RouteMajor][RouteMinor] [WebServiceMajor][WebServiceMinor][SysMajor][SysMinor][PhoneMajor][PhoneMinor][DownloadUpdatesMajor][DownloadUpdatesMinor][PaltformMajor][PlatformMinor][WinCEMajor][WinCEMinor][CFCardTotal][CFCardFree][MCCQueueCount][Battery]
                    o5080AssetPacket = new c5080AssetPacket();
                    o5080AssetPacket.Decode(oMS);
                    if (_log != null) _log.Info("Reading 5080 Asset Packet for mobile ID " + Convert.ToString(iMobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID));
                    #endregion
                    break;
				default :
					break;
			}
			return "";
		}
		#endregion	

		#region GetPacketTypeName
		public string GetPacketTypeName()
		{
			string sRet = "";

			switch(CommandType)
			{
				case "N":
					sRet = "Ack/Nack Packet";
					break;
				case "I":
					sRet = "Login Packet";
					break;
				case "O":
					sRet = "Logout Packet";
					break;
				case "R":
					sRet = "Request Packet";
					break;
				case "U":
					sRet = "Alert Packet";
					break;
				case "M":
					sRet = "Message Packet";
					break;
				case "K":
					sRet = "Phonecall Packet";
					break;
				case "L":
					sRet = "Login Reply Packet";
					break;
                case "c":
                    if (SubCommandType == "d")
                        sRet = "Download NEC Header Packet";
                    if (SubCommandType == "r")
                        sRet = "Download NEC Data Segment Packet";
                    else if (SubCommandType == "v")
                        sRet = "Download NEC Verify Packet";
                    else
                        sRet = "Unknown NEC Download Packet";
                    break;
                case "d":
                    if (SubCommandType == "d")
                        sRet = "Download WinCE Header Packet";
                    if (SubCommandType == "r")
                        sRet = "Download WinCE Data Segment Packet";
                    else if (SubCommandType == "v")
                        sRet = "Download WinCE Verify Packet";
                    else
                        sRet = "Unknown WinCE Download Packet";
                    break;
                case "D":
                    if (SubCommandType == "d")
                        sRet = "Download MOT File Header Packet";
                    if (SubCommandType == "r")
                        sRet = "Download MOT File Data Segment Packet";
                    else if (SubCommandType == "v")
                        sRet = "Download MOT File Verify Packet";
                    else
                        sRet = "Unknown MOT Download Packet";
                    break;
                case "Q":
                    sRet = "Pre-defined Function Packet";
                    break;
                case "V":
                    sRet = "Firmware Version Packet";
                    break;
                case "Z":
                    sRet = "5080 Asset Packet";
                    break;
                default:
					sRet = "Unknown Packet Type";
					break;
            }
			return sRet;
		}
		#endregion

		#region Private Functions
		private static byte[] CreateBadPacketNak()
		{
			byte[] bPacketData = new byte[1];
			byte[] bPacket = new byte[1];
			byte[] bPacketLen = null;
			byte[] bCmdType = null;

			bCmdType = System.Text.ASCIIEncoding.ASCII.GetBytes("N");

			bPacketData[0] = bCmdType[0];
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, Convert.ToInt32((byte) 0x00));
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, "4");
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x07);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x03);

			bPacketLen = BitConverter.GetBytes( Convert.ToUInt32(bPacketData.Length));
			bPacket = PacketUtilities.AppendToPacket(bPacket, (byte) bPacketLen[0]);
			bPacket = PacketUtilities.AppendToPacket(bPacket, (byte) bPacketLen[1]);
			bPacket = PacketUtilities.AppendToPacket(bPacket, bPacketData);
			return bPacket;
		}

		private static byte[] GetSubBytes(byte[] bMsg, int iStartAt, int iNumOfBytes)
		{
			byte[] bResult = null;
			int iResultCount = 0;

			if (bMsg == null) return null;

			if (bMsg.Length >= (iStartAt + iNumOfBytes))
			{	

				bResult = new byte[iNumOfBytes];

				for (int X = iStartAt; X < iStartAt + iNumOfBytes; X++)
				{
					bResult[iResultCount] = bMsg[X];
					iResultCount++;
				}
				return bResult;
			}
			else
			{
				iNumOfBytes = bMsg.Length;
				bResult = new byte[iNumOfBytes - iStartAt];
				for (int X = iStartAt; X <iNumOfBytes; X++)


				{
					bResult[iResultCount] = bMsg[X];
					iResultCount++;
				}
				return bResult;
			}
		}

		private static string RemoveLeadingReturnChars(string strData)
		{
			string strTemp = strData.Trim();

			if (strTemp.Length > 0)
			{
				while (strTemp[0] == '\r' || strTemp[0] == '\n') 
				{
					if (strTemp.Length == 1)
					{
						strTemp = "";
						break;
					}
					else
					{
						strTemp = strTemp.Substring(1, strTemp.Length - 1);
					}
				}
			}
			return strTemp;
		}

		private static string WordWrapString(string strDescLines, int iLineLength)
		{
			char[] cDesc = strDescLines.ToCharArray(0, strDescLines.Length);
			string sResult = "";
			string sLine = "";
			int iCurrentPos = 1;

			if (cDesc.Length > 0)
			{
				for(int X = 0; X < cDesc.Length; X++)
				{
					// For each char in the array
					if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
					{
						if (iCurrentPos > 0)
						{
							sResult += sLine + "\n";
							sLine = "";
							iCurrentPos = 0;
						}
						else
						{
							sResult += "\n";
							sLine = "";
							iCurrentPos = 0;
						}
					}
					else
					{
						if (iCurrentPos > iLineLength)
						{
							int iLastSpace = sLine.LastIndexOf(' ');
							if (iLastSpace > 0)
							{
								sResult += sLine.Substring(0, iLastSpace) + "\n";
								sLine = sLine.Substring(iLastSpace + 1).Trim();
								iCurrentPos = sLine.Length;
							}
							else
							{
								sResult += sLine + "\n";
								sLine = "";
								iCurrentPos = 0;
							}
							if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
							{
								if (iCurrentPos > 0)
								{
									sResult += sLine + "\n";
									sLine = "";
									iCurrentPos = 0;
								}
								else
								{
									sResult += "\n";
									sLine = "";
									iCurrentPos = 0;
								}
							}
							else
							{
								if (iCurrentPos == 0 && cDesc[X] == ' ')
								{
									// Don't add white space to the front of the line.
								}
								else
								{
									sLine += cDesc[X];
									iCurrentPos++;
								}
							}
						}
						else
						{
							if (iCurrentPos == 0 && cDesc[X] == ' ')
							{
								// Don't add white space to the front of the line.

							}
							else
							{
								sLine += cDesc[X];
								iCurrentPos++;
							}
						}
					}
				}
			}

			if (sLine.Length > 0)
				sResult += sLine;

			return sResult;
		}

		public static string ConvertToAscii(byte[] bPacket)
		{
			return ConvertToAscii(bPacket, true);
		}

		public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
		{
			string sRet = "";
			int X = 0;
			//			byte bLower = 0x21;	// 33
			//			byte bUpper = 0x7E;  // 126
			byte bTest = 0x00;  // null
			byte[] bTestArray = new byte[1];

			if (bPacket.Length > 0)
			{
				try
				{
					for (X = 0; X<bPacket.Length; X++)
					{
						bTest = bPacket[X];	
						bTestArray = new byte[1];
						bTestArray[0] = bTest;
						if (bTest >= 33 && bTest <= 126 )  // If the character is in printable range
							if (bAddSpacing)
								if (sRet.Length > 0)
									sRet += " - " + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
								else
									sRet = System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
							else
								sRet += System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
						else  // Show it as a number
							if (bAddSpacing)
							if (sRet.Length > 0)
								sRet += " - [" +  bTest + "]";
							else
								sRet = "[" + Convert.ToString(bTest) + "]";
						else
							sRet += "[" + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray) + "]";
					}
					
				}
				catch(System.Exception ex)
				{
					Console.Write(ex.Message + "\n");
				}
			}
			return sRet;
		}

		private static int FindFirstIndexOfByte(string sData, byte bFind)
		{
			return FindFirstIndexOfByte(sData, bFind, 0, sData.Length);
		}

		private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt)
		{
			return FindFirstIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
		}

		private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
		{
			int iRet = -1;

			try
			{
				iRet = sData.IndexOf((char) bFind + "", iStartAt, iLength);
			}
			catch(System.Exception)
			{
				return -1;
			}
			return iRet;
		}

		private static int FindLastIndexOfByte(string sData, byte bFind)
		{
			return FindLastIndexOfByte(sData, bFind, sData.Length, sData.Length);
		}

		private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt)
		{
			return FindLastIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
		}

		private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
		{
			int iRet = -1;

			try
			{
				iRet = sData.LastIndexOf((char) bFind + "", iStartAt, iLength);
			}
			catch(System.Exception)
			{
				return -1;
			}
			return iRet;
		}


		private static string TruncateString(string sData, int iPos)
		{
			string sRet = "";
			
			try
			{
				if (sData.Length < iPos + 1)
					return "";
				else
					sRet = sData.Substring((iPos + 1), sData.Length - (iPos + 1));
			}
			catch(System.Exception)
			{
				return "";
			}
			return sRet;
		}
		#endregion	
	}
}

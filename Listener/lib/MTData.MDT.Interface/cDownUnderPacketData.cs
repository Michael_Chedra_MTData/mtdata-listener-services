using System;
using System.Collections;
using System.Collections.Specialized;
using log4net;
using MTData.Common.Config;
using MTData.Common.Network;
using MTData.Common.Utilities;


namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cCameronsPacketData.
    /// </summary>

    public class cDownUnderPacketData
    {
        #region Incomming Packet Defs
        // N - Ack an NAck Response to MCC - [CmdType][Mobile ID][Result Code][ACK_Message][EventID][StopID]
        // I - Logon to System - [CmdType][Mobile ID][FleetID][VehicleID][GPS][Username][USB][Password][PSB][PhoneBookID][MessagesID][SBS][Spare]
        // O - Log off the System - [CmdType][Mobile ID][FleetID][VehicleID][GPS][Username][USB][SBS][Spare]
        // A - Accept a Request (Job) - [CmdType][Mobile ID] [FleetID][VehicleID] [SpareByte][GPS][Username][USB][EventID[BookingID][SBS][Spare]
        // S - Stop Packet
        //		P - Complete a Stop (pickup) - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][EventID][BookingID][StopNo][ProfitCenter][PAXNo][SBS][Spare]
        //		D - Complete a Stop (deliver) - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][EventID][BookingID][StopNo][ProfitCenter][PAXNo][SBS][Spare]
        //		T - Job Stop Exceeded Time Requirements - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][EventID][BookingID][StopNo][ProfitCenter][SBS][Spare]
        // C - Complete a Job - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][EventID][BookingID][ProfitCenter][SBS][Spare]
        // R - Request Packets (a keep alive for mobile communications)
        //		J - Request Jobs -  [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]
        // M - Messages
        //		F - Driver initiated message (Freeform). - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Message Data][MSDC][SBS][Spare]
        //		P - Driver initiated message (Predefined) - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Message ID][SBS][Spare]
        //		f - Driver initiated email message (Freeform). - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Email][MSS][Message Data][MSDC][SBS][Spare]
        //		p - Driver initiated email message (Predefined) - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Message ID][SBS][Spare]
        // F - Fuel Packets
        //		P - Fuel Pumped into Coach Packet - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Location][FuelLiters][LiterCost][TotalCost][SBS][Spare]
        // C - Driver Usage Control Packet - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]

        #endregion

        #region Outgoing Packet Defs
        // J - New Job Packet - [CmdType][MobileID][EventID][BookingID][NumOfStops][StartTime][EndTime][JobType][Description][DSC][GroupName][EOL][AgentName][EOL][ProductCode][ProfitCenter][JobBrief][EOL] [SBS][Spare]
        // A - Add a stop to the job - [CmdType][MobileID][EventID][BookingID][StopNo][TimeToStart][TimeToAlert][JobStopType][ProductCode][ProfitCenter][Passengers][GroupName][GNS][AgentsName][ANS][Description][EOL][SBS][Spare]
        // U - Update a stop on the job - [CmdType][MobileID][EventID][BookingID][StopNo][TimeToStart][TimeToAlert][JobStopType][ProductCode][ProfitCenter][Passengers][GroupName][GNS][AgentsName][ANS][Description][EOL][SBS][Spare]
        // R - Remove a stop from a job - [CmdType][Mobile ID][EventID][BookingID][StopNo][SBS][Spare]
        // D - Delete a job - [CmdType][Mobile ID][EventID][BookingID][SBS][Spare]
        // M - Send a message to the driver - [CmdType][Mobile ID][Description] [EOL][SBS][Spare]
        // m - Send Predefined Messages to for unit�s memory (Predefined).- [CmdType][Mobile ID][SubCmd][MessageID][MSS][Location][MessageName][SBS][Spare]
        // L -  Send Log ON Attempt ACK - [CmdType][Mobile ID][SubCmd][SBS][Spare]
        #endregion

        #region Private Vars
        private ConvertSignatureConfig _sigConfig = null;
        private ILog _log = LogManager.GetLogger(typeof(cDownUnderPacketData));
        private bool bTranslateRJs = true;
        #endregion

        #region Public Vars
        // Shared Public variables
        public int iMobileID = 0;
        public string CommandType = "";				// [CmdType]
        public int JobID = 0;								// [Job ID]
        public int StopID = 0;								// [Stop #]
        public int CodeNumber = 0;						// [FailCode], [SucessCode], [Function ID]
        public string Message = "";						// [Message Data]
        public byte[] bRawBytes = null;

        // Client Incomming Packet Specific
        // New Job Packet
        public long EventID = 0;
        public long BookingID = 0;
        public int NoOfStops = 0;
        public string StartTime = "00:00";
        public string EndTime = "00:00";
        public int JobType = 0;
        public string GroupName = "";
        public string AgentName = "";
        public long ProductCode = 0;
        public int ProfitCentreID = 0;
        public int iJobDay = 0;
        public int JobStopLat = 0;
        public int JobStopLong = 0;
        public string JobBrief = "";
        // Add stop packet
        public string TimeToStart = "00:00";
        public int TimeToAlert = 0;
        public string JobStopType = "";
        public int NoOfPassengers = 0;
        // Send a predefined message
        public int FlashMemoryLocation = 0;

        public int FuelStationID = 0;
        public int FuelLitres = 0;
        public int FuelCost = 0;
        public string UsageType = "";
        public int PAXNo = 0;

        // Incomming Packet Specific
        public string SubCommandType = "";		// [SubCmd]
        public bool StartFunction = false;			// [Start / Stop]
        public int ReferenceNumber = 0;
        public int TimeLoading = 0;
        public int DivisionID = 0;
        public int DeviceTimeSeconds = 0;
        public int CallTime = 0;
        public int SuburbID = 0;							// [Suburb ID]
        public int MessageID = 0;							// [Message ID]
        public int VehicleID = 0;							// [VehicleID]
        public int FleetID = 0;								// [FleetID]
        public int PhonebookID = 0;						// [PhoneBook ID]
        public int MessagebookID = 0;					// [MessageBook ID]
        public int DivisionReasonID = 0;				// [DivisionReasonID]
        public int DelayReasonID = 0;					// [DelayReasonID]
        public int RejectedReasonID = 0;			// [RejectedReasonID]
        public int Distance = 0;								// [Distance]
        public string DelayDescription = "";
        public string PalletTypeCode = "";
        public string NoOfPallets = "";
        public string EmailAddress = "";
        public string PhoneNumber = "";
        public string Username = "";						// [Username]
        public string Password = "";						// [Password]
        public string PODName = "";					// [POD Name]
        public string JobTime = "";						// [Time], [Time in Mins]
        public string BusinessName = "";				// [Business Name]
        public string InvoiceNumber = "";			// [Invoice Number]
        public StringCollection BarCodes = null;				// [Bar Code x]
        public StringCollection ReturnBarCodes = null;	// [Return Bar Code x]
        public byte[] bGenericData = null;			// [200 bytes]
        public byte[] SignatureData = null;				// [Signature]
        public int WelcomeMessageID = 0;

        // Outgoing Packet Specific
        public int TotalNumberOfStops = 0;
        public int PhoneBookID = 0;
        public int DivisionListID = 0;
        public int DelayReasonListID = 0;
        public int RejectionReasonListID = 0;
        public string JobTitle = "";
        public string ContactName = "";
        public string ContactPh = "";
        public string JobDescription = "";
        public string LoginReply = "";
        public string DriverName = "";
        public int MessageLocation = 0;
        public short iFuelStationListID = 0;
        public ushort iFuelStationRadius = 200;
        public ArrayList oFuelStationList = new ArrayList();
        public StringCollection PhoneBookLocations = new StringCollection();
        public StringCollection PhoneBookNumbers = new StringCollection();
        public StringCollection PhoneBookNames = new StringCollection();
        public StringCollection MessageNames = new StringCollection();
        public StringCollection DivisionNames = new StringCollection();
        public StringCollection DelayReasonNames = new StringCollection();
        public StringCollection RejectionReasonNames = new StringCollection();
        // GPS Data
        public cGPSData GPSPosition = null;
        public double DesiredLatitude = 0;
        public double DesiredLongitude = 0;
        #endregion

        public cDownUnderPacketData(ConvertSignatureConfig sigConfig, bool TranslateRJs)
        {
            _sigConfig = sigConfig;

            bTranslateRJs = TranslateRJs;
        }

        #region Build Packet Data
        public byte[] ParseIncommingData(byte[] bMsg)
        {
            byte[] bConvert = null;
            int iPos = 0;
            int iPacketLength = 0;

            if (bMsg.Length < 6)
            {
                _log.Info("Short packet recieved from client - " + BitConverter.ToString(bMsg, 0));
                return null;
            }

            bConvert = new byte[4];
            bConvert[0] = bMsg[1];
            bConvert[1] = bMsg[2];

            iPacketLength = BitConverter.ToInt32(bConvert, 0);

            Console.Write("Packet Length " + Convert.ToString(iPacketLength) + "\n");
            // Strip off the header and footer bytes.
            bMsg = GetSubBytes(bMsg, 3, bMsg.Length - 4);

            this.CommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPos++, 1);
            switch (CommandType)
            {
                case "J":
                    #region J - New Job Packet
                    // Mobile ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Event ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.EventID = BitConverter.ToUInt32(bConvert, 0);

                    // Booking ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.BookingID = BitConverter.ToUInt32(bConvert, 0);

                    // No Of Stops
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.NoOfStops = BitConverter.ToInt32(bConvert, 0);

                    // Start Time
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.StartTime = Convert.ToString(BitConverter.ToInt32(bConvert, 0)).PadLeft(2, '0');
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.StartTime += ":" + Convert.ToString(BitConverter.ToInt32(bConvert, 0)).PadLeft(2, '0');

                    // EndTime
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.EndTime = Convert.ToString(BitConverter.ToInt32(bConvert, 0)).PadLeft(2, '0');
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.EndTime += ":" + Convert.ToString(BitConverter.ToInt32(bConvert, 0)).PadLeft(2, '0');

                    // Job Type
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    //this.JobType = System.Text.ASCIIEncoding.ASCII.GetString(bConvert, 0, 1);
                    this.JobType = BitConverter.ToInt32(bConvert, 0);

                    // Description
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0E) // If this byte is the DSC byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.JobDescription = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }

                    // Group Name
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0F) // If this byte is the DSC byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.GroupName = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }

                    // Agent Name
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0F) // If this byte is the DSC byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.AgentName = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }

                    // Product Code
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.ProductCode = BitConverter.ToUInt32(bConvert, 0);

                    // Profit Centre ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.ProfitCentreID = BitConverter.ToInt32(bConvert, 0);

                    // Job Brief
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x07) // If this byte is the DSC byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.JobBrief = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }
                    #endregion
                    break;
                case "A": // Add a Job Stop 
                    #region A - Add a Job Stop
                    // Mobile ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Event ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.EventID = BitConverter.ToUInt32(bConvert, 0);

                    // Booking ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.BookingID = BitConverter.ToUInt32(bConvert, 0);

                    // Stop ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.StopID = BitConverter.ToInt32(bConvert, 0);

                    // Time To Start
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    this.TimeToStart = Convert.ToString(BitConverter.ToInt32(bConvert, 0)).PadLeft(2, '0');
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    this.TimeToStart += ":" + Convert.ToString(BitConverter.ToInt32(bConvert, 0)).PadLeft(2, '0');

                    // EndTime
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.TimeToAlert = BitConverter.ToInt32(bConvert, 0);

                    // Job Stop Type
                    this.JobStopType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPos++, 1);

                    // Product Code
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.ProductCode = BitConverter.ToUInt32(bConvert, 0);

                    // Profit Centre
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.ProfitCentreID = BitConverter.ToInt32(bConvert, 0);

                    // Number of Passengers
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.NoOfPassengers = BitConverter.ToInt32(bConvert, 0);

                    // Group Name
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0A) // If this byte is the GNC byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.GroupName = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }

                    // Agent Name
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0A) // If this byte is the ANS byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.AgentName = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }

                    // Description
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0F) // If this byte is the ANS byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.JobDescription = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.JobStopLat = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.JobStopLong = BitConverter.ToInt32(bConvert, 0);

                    iPos++;		// Skip over the sbs

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.iJobDay = BitConverter.ToInt32(bConvert, 0);

                    #endregion
                    break;
                case "D": // Delete Job Packet
                    #region D - Delete Job Packet
                    // Mobile ID					
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Event ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.EventID = BitConverter.ToUInt32(bConvert, 0);

                    // Booking ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = bMsg[iPos++];
                    bConvert[3] = bMsg[iPos++];
                    this.BookingID = BitConverter.ToUInt32(bConvert, 0);
                    #endregion
                    break;
                case "M":
                    #region M - Messages
                    // Mobile ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Message ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.MessageID = BitConverter.ToInt32(bConvert, 0);

                    // Description
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0F) // If this byte is the ANS byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }
                    #endregion
                    break;
                case "m":
                    // Mobile ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Sub Command
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPos++, 1);

                    // Message ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.MessageID = BitConverter.ToInt32(bConvert, 0);

                    // Flash Memory Location
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.FlashMemoryLocation = BitConverter.ToInt32(bConvert, 0);

                    // Location
                    for (int X = iPos; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x07) // If this byte is the EOP byte
                        {
                            bConvert = new byte[X - iPos];
                            for (int Y = iPos; Y < X; Y++)
                            {
                                bConvert[Y - iPos] = bMsg[Y];
                            }
                            this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPos = X + 1;
                            break;
                        }
                    }
                    break;
                case "L":
                    // Mobile ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Sub Command
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPos++, 1);
                    this.LoginReply = this.SubCommandType;



                    break;
                case "X":
                    // Mobile ID
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPos++];
                    bConvert[1] = bMsg[iPos++];
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Sub Command
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPos++, 1);
                    break;
            }

            return bMsg;
        }

        public byte[] BuildOutgoingPacket()
        {
            byte[] bRet = new byte[1];
            byte[] bConvert = null;

            switch (CommandType)
            {
                // At this point the packet consists of [CmdType][Mobile ID]
                case "L":
                    #region Login Reply packet
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("L")[0];					// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);											// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, LoginReply);									// Login Response
                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.DriverName);				// Driver Name
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);								// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "X":
                    #region Send Jobs on Unit Request
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("X")[0];					// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);											// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, System.Text.ASCIIEncoding.ASCII.GetBytes("O")[0]);										// Login Responce
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);								// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "J":
                    #region New Job Packet:
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("J")[0];												// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);																	// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = BitConverter.GetBytes(this.EventID);																		// Event ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.BookingID);																	// Booking ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.NoOfStops);																// No of Stops
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    string[] sTime = this.StartTime.Split(":".ToCharArray());															// Start Time
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sTime[0]));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sTime[1]));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    string[] sEndTime = this.EndTime.Split(":".ToCharArray());														// End Time
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sEndTime[0]));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sEndTime[1]));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);																															// Job Type
                    // adds only first byte frm JobType
                    bConvert = BitConverter.GetBytes(this.JobType);																		// Event ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    if (this.JobDescription.Length > 20)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription.Substring(0, 20));		// Description
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription);		// Description
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0E);														// DSC
                    if (this.GroupName.Length > 20)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.GroupName.Substring(0, 20));			// Group Name
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.GroupName);			// Group Name
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);															// EOL

                    if (this.AgentName.Length > 20)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.AgentName.Substring(0, 20));				// Agent Name
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.AgentName);				// Agent Name
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);															// EOL
                    bConvert = BitConverter.GetBytes(this.ProductCode);																// Product Code
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.ProfitCentreID);															// Profit Centre
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);

                    if (this.JobBrief.Length > 620)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobBrief.Substring(0, 620));				// Job Breif
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobBrief);				// Job Breif
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);														// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "A":
                    #region Add a job stop
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("A")[0];												// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);																	// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = BitConverter.GetBytes(this.EventID);																		// Event ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.BookingID);																	// Booking ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.StopID);																		// Stop Number
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    string[] sTimeToStart = this.TimeToStart.Split(":".ToCharArray());										// Start Time
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sTimeToStart[0]));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sTimeToStart[1]));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bConvert = BitConverter.GetBytes(this.TimeToAlert);																// Time to Alert
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, this.JobStopType);
                    //					if (this.JobStopType.Length > 0)																									// Job Type
                    //						bRet = PacketUtilities.AppendToPacket(bRet, Convert.ToString(this.JobStopType).ToCharArray()[0]);
                    //					else
                    //						bRet = PacketUtilities.AppendToPacket(bRet, "P");
                    bConvert = BitConverter.GetBytes(this.ProductCode);																// Product Code
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.ProfitCentreID);															// Profit Centre
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = BitConverter.GetBytes(this.NoOfPassengers);														// No of Passengers
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    if (this.GroupName.Length > 20)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.GroupName.Substring(0, 20));			// Group Name
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.GroupName);			// Group Name
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);														// GNS
                    if (this.AgentName.Length > 20)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.AgentName.Substring(0, 20));				// Agent Name
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.AgentName);				// Agent Name
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);														// ANS
                    if (this.JobDescription.Length > 620)
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription.Substring(0, 620));				// Job Breif
                    else
                        bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription);				// Description
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);															// EOL

                    bConvert = BitConverter.GetBytes(this.JobStopLat);																// Job Lat
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);

                    bConvert = BitConverter.GetBytes(this.JobStopLong);																// Job Long
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);														// SBS - Spare Bytes Seperator
                    bConvert = BitConverter.GetBytes(this.iJobDay);																		// Job Day
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    #endregion
                    break;
                case "D":
                    #region Delete a job
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("D")[0];												// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);																	// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = BitConverter.GetBytes(this.EventID);																		// Event ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bConvert = BitConverter.GetBytes(this.BookingID);																	// Booking ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);														// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "M":
                    #region Send a driver message
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("M")[0];												// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);																	// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = BitConverter.GetBytes(this.MessageID);																// Message ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.Message);				// Message
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);															// EOL
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);															// EOL
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);														// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "m":
                    #region Send a pre-defined message to the 5040
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("m")[0];												// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);																	// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, "D");																		// Sub Command Type
                    bConvert = BitConverter.GetBytes(this.MessageID);																// Message ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);														// MSS
                    bConvert = BitConverter.GetBytes(this.MessageLocation);														// Location
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.Message.Substring(0, 200));				// Message
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);														// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "l":
                    #region Send a driver message
                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("l")[0];												// Command Type
                    bConvert = BitConverter.GetBytes(this.iMobileID);																	// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = BitConverter.GetBytes(this.WelcomeMessageID);																// Message ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    //bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.Message);				// Message
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);															// EOL
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);															// EOL
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);														// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                case "K":
                    #region Send a fuel station WP list to the unit
                    //Send Fuel Station WayPoint List
                    //[CmdType][MobileID][SubCmd][FuelStationListID][NoOfItems][LSS]{[SSS][ItemNo][Lattidue][Longitude][Radius][Name][NES]}[ESS]
                    //CmdType = �K� (1 char)
                    //Mobile ID = 2 bytes number
                    //SubCmd = �f� is predefined phone book list
                    //FuelStationListID = The ID code for this Container Name List (1 byte)
                    //NoOfItems = number of items (1 byte) (max 30)
                    //LSS = List start separator 0x01
                    //SSS = Item start separator 0x02
                    //ItemNo = 1 byte item number 0 base
                    //Lattitude = 4 byte binary latitude * 600,000
                    //Longitude = 4 byte binary longitude * 600,000
                    //Radius = in meters (2 bytes)
                    //Name = Fuel Station name (10 bytes max)
                    //NES = name end separator
                    //ESS = separator start 0x03

                    bRet[0] = System.Text.ASCIIEncoding.ASCII.GetBytes("K")[0];												// Command Type
                    bRet = PacketUtilities.AppendToPacket(bRet, (ushort)iMobileID);				// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, "f");											// Sub Cmd
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)iFuelStationListID);	// Fuel Station List ID
                    if (this.oFuelStationList.Count > 20)
                        bConvert = BitConverter.GetBytes(20);																						// Fuel Station List Count
                    else
                        bConvert = BitConverter.GetBytes(this.oFuelStationList.Count);
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x01);							// LSS
                    if (this.oFuelStationList.Count > 0)
                    {
                        for (int X = 0; X < this.oFuelStationList.Count; X++)
                        {
                            if (X == 20) break;
                            cWPData oWP = (cWPData)this.oFuelStationList[X];
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x02);					// SSS
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)X);							// Item Number
                            bRet = PacketUtilities.AppendToPacket(bRet, oWP.iLatitude);				// Latitude
                            bRet = PacketUtilities.AppendToPacket(bRet, oWP.iLongitude);			// Latitude
                            bRet = PacketUtilities.AppendToPacket(bRet, (short)oWP.iRadius);		// Radius
                            if (oWP.sPointName.Length > 30)
                                bRet = PacketUtilities.AppendToPacket(bRet, oWP.sPointName.Substring(0, 30));			// Station Name	
                            else
                                bRet = PacketUtilities.AppendToPacket(bRet, oWP.sPointName);
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);					// NES
                        }
                    }
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x03);							// ESS					
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07);							// SBS - Spare Bytes Seperator
                    #endregion
                    break;
                default:
                    bRet = null;
                    break;
            }
            return bRet;
        }
        #endregion

        #region Parse Incomming Packet
        public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
        {
            string sRet = "";
            try
            {
                sRet = TranslateMsgToHost(bMsg, sUnitID);
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                {
                    if (bMsg == null)
                        _log.Error("MTData.MDTInterface.cDownUnderPacketData ParseIncommingPacket(byte[] bMsg = null, sUnitID = '" + sUnitID + "')", ex);
                    else
                        _log.Error("MTData.MDTInterface.cDownUnderPacketData ParseIncommingPacket(byte[] bMsg = " + BitConverter.ToString(bMsg) + ", sUnitID = '" + sUnitID + "')", ex);
                }
            }
            return sRet;
        }

        private string TranslateMsgToHost(byte[] bMsg, string sUnitID)
        {
            // NOTES :
            //
            // All packets from the unit (except Nack) start with [Cmd Type][GPS]
            //
            // All msgs to the host start with bSOP (0x02), 2 bytes of length and end with bEOP (0x03);
            //		i.e. [SOP][Length of Data (2 Bytes)][Data][EOP]
            //
            // [GPS]
            // All packets from the unit to the host include the following GPS information;
            // [LSC][Lat][Long][GPS Time][Speed][Heading] = 1 + 4 + 4 + 6 + 1 + 2 = 18 bytes
            // 
            // These 18 bytes can just be copied to the host packet as a block, no interpretation is required.
            //
            // [SBS] - Spare bytes seperator
            // Most of the packets have a SBS before the EOP byte.  This has been added to allow for expansion
            // without changing the protocol.

            #region Local Variables
            byte[] bFailCode = new byte[4];
            byte[] bMBPPacket = new byte[1];
            byte[] bStopID = new byte[4];
            byte[] bConvert = new byte[4];
            byte[] bGPS = null;
            byte[] bSignatureLen = new byte[4];
            byte[] bFunctionID = new byte[1];
            byte[] bStartStop = new byte[1];
            int iPosition = 0;
            string strErrMsg = "";
            #endregion

            #region Validate Packet Length and Structure
            bConvert = BitConverter.GetBytes(0);

            // Make sure the unit ID is only 4 chars long.
            if (sUnitID.Length > 4)
                sUnitID = sUnitID.Substring(0, 4);

            sUnitID = sUnitID.PadLeft(4, '0');

            // Get the Cmd Type for the packet
            this.CommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
            // Get the unit ID 
            this.iMobileID = Convert.ToInt32(sUnitID);

            // Copy the data into a raw bytes array for transmission to clients.
            bRawBytes = new byte[bMsg.Length];
            for (int X = 0; X < bMsg.Length; X++)
            {
                bRawBytes[X] = bMsg[X];
            }

            // If the Cmd Type != N then extract the standard header fields [FleetID][VehicleID][GPS]
            if (this.CommandType != "N" && this.CommandType != "T") // RJ is now valid packet
            {
                if (bMsg.Length < 20)
                {
                    // If the packet is too small, just return.
                    return strErrMsg;
                }
                else
                {
                    #region A login packet has extra fields in the header...
                    // [CmdType][MobileID][FleetID][VehicleID][GPS][.....]
                    //					CmdType =  1 char
                    //					Mobile ID = 2 bytes number
                    //					FleetID = 3021 Fleet ID (1 byte number 0 - 255)
                    //					VehicleID = 3021 Unit ID (2 byte number 0 - 65000)
                    //					GPS = See above.

                    // Get the fleet
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[3];
                    this.FleetID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    // Get the vehicle
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[4];
                    bConvert[1] = bMsg[5];
                    this.VehicleID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    // Get the GPS Data
                    bGPS = GetSubBytes(bMsg, 6, 18);
                    this.GPSPosition = new cGPSData(bGPS);

                    // Strip off the [CmdType][MobileID][FleetID][VehicleID][GPS] fields from bMsg
                    // Get the elapsed seconds since the last lock.
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[24];
                    bConvert[1] = bMsg[25];
                    this.DeviceTimeSeconds = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[26];
                    bConvert[1] = bMsg[27];
                    bConvert[2] = bMsg[28];
                    bConvert[3] = bMsg[29];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);

                    // Strip off the [CmdType][MobileID][FleetID][VehicleID][GPS] fields from bMsg
                    bMsg = GetSubBytes(bMsg, 30, bMsg.Length);
                    #endregion

                    // If this packet type includes a username...
                    if (this.CommandType == "I" || this.CommandType == "O" || this.CommandType == "A" || this.CommandType == "S" || this.CommandType == "C" || this.CommandType == "R" || this.CommandType == "M" || this.CommandType == "F" || this.CommandType == "U")
                    {
                        // Get the username
                        for (int X = 0; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the USB byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                }
                                this.Username = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the USB byte.
                                iPosition = X + 1;
                                break;
                            }
                        }
                        bMsg = GetSubBytes(bMsg, iPosition, bMsg.Length);
                    }
                }
            }
            else
            {
                if (bMsg.Length < 9)
                {
                    // If the packet is too small, just return.
                    return strErrMsg;
                }
                else
                {
                    // Strip off the [CmdType][MobileID] fileds from bMsg
                    if (this.CommandType == "N")
                        bMsg = GetSubBytes(bMsg, 1, bMsg.Length);
                    else
                        bMsg = GetSubBytes(bMsg, 3, bMsg.Length);
                }
            }
            #endregion

            // bMsg is now the remaining bytes after the standard header ([CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB] has been stripped out)

            iPosition = 0;

            switch (CommandType)
            {
                case "N":
                    #region Ack an NAck Responce
                    bConvert = new byte[4];																							// Code Number
                    bConvert[0] = bMsg[0];
                    this.CodeNumber = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 1, 4);		// Message

                    if (this.Message.Substring(0, 2).ToUpper() == "OK")
                    {
                        // Get the stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[3];
                        bConvert[1] = bMsg[4];
                        bConvert[2] = bMsg[5];
                        bConvert[3] = bMsg[6];
                        this.EventID = BitConverter.ToUInt32(bConvert, 0);

                        bConvert[0] = bMsg[7];
                        bConvert[1] = (byte)0x00;
                        bConvert[2] = (byte)0x00;
                        bConvert[3] = (byte)0x00;
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    }
                    else
                    {
                        // Get the stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[5];
                        bConvert[1] = bMsg[6];
                        bConvert[2] = bMsg[7];
                        bConvert[3] = bMsg[8];
                        this.EventID = BitConverter.ToUInt32(bConvert, 0);
                        bConvert[0] = bMsg[9];
                        bConvert[1] = (byte)0x00;
                        bConvert[2] = (byte)0x00;
                        bConvert[3] = (byte)0x00;
                        this.StopID = BitConverter.ToInt32(bConvert, 0);
                    }

                    #endregion
                    break;
                case "I":
                    #region Logon to System
                    bConvert = new byte[4];																								// Password
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Password = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);

                    //iPosition++;																													// PSC

                    //bConvert = new byte[4];																							// Phone book ID
                    //bConvert[0] = bMsg[iPosition++];
                    //this.PhonebookID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];																							// Message book ID
                    bConvert[0] = bMsg[iPosition++];
                    this.MessagebookID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];																							// Message book ID
                    bConvert[0] = bMsg[iPosition++];
                    this.WelcomeMessageID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    if (iPosition < bMsg.Length)
                    {
                        bConvert = new byte[4];																							// Fuel Station List ID
                        bConvert[0] = bMsg[iPosition++];
                        this.iFuelStationListID = Convert.ToInt16(BitConverter.ToUInt32(bConvert, 0));
                    }
                    else
                        this.iFuelStationListID = 0;
                    #endregion
                    break;
                case "O":
                    #region Log off the System
                    // No additional data here.
                    #endregion
                    break;
                case "A":
                    #region Accept Job Packet
                    this.EventID = BitConverter.ToUInt32(bMsg, 0);				// Event ID
                    this.BookingID = BitConverter.ToUInt32(bMsg, 4);			// Booking ID
                    #endregion
                    break;
                case "S":
                    #region Job Stop Packets
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);		// Sub Command

                    if (this.SubCommandType == "P")
                    {
                        #region Stop Packet - Pickup
                        this.EventID = BitConverter.ToUInt32(bMsg, iPosition);										// Event ID
                        iPosition = iPosition + 4;
                        this.JobStopType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);				// Job Stop Type
                        this.BookingID = BitConverter.ToUInt32(bMsg, iPosition);									// Booking ID
                        iPosition = iPosition + 4;
                        bConvert = new byte[4];																															// Stop Number
                        bConvert[0] = bMsg[iPosition++];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        bConvert = new byte[4];																															// Profit Centre
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        this.ProfitCentreID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x07) // If this byte is the DDS  byte
                            {
                                bConvert = new byte[X - iPosition];
                                for (int Y = iPosition; Y < X; Y++)
                                {
                                    bConvert[Y - iPosition] = bMsg[Y];
                                }
                                string sPaxTmp = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                if (Util.IsIntegerPositive(sPaxTmp))
                                    this.PAXNo = Convert.ToInt32(sPaxTmp);
                                // Set iPosition to the next byte after the DDS byte.
                                break;
                            }
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "T")
                    {
                        #region Job Stop Exceeded Time Requirement
                        this.EventID = BitConverter.ToUInt32(bMsg, iPosition);										// Event ID
                        iPosition = iPosition + 4;
                        this.BookingID = BitConverter.ToUInt32(bMsg, iPosition);									// Booking ID
                        iPosition = iPosition + 4;
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = 0;
                        bConvert[2] = 0;
                        bConvert[3] = 0;
                        this.StopID = BitConverter.ToInt32(bConvert, 0);
                        //iPosition += 1;
                        bConvert = new byte[4];																															// Profit Centre
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        this.ProfitCentreID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        #endregion
                    }
                    #endregion
                    break;
                case "R":
                    #region Request Packets
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "J")
                    {
                        #region Request Jobs
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]

                        // Nothing more to decode.
                        #endregion
                    }
                    #endregion
                    break;
                case "C":
                    #region Complete a Job Packet
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "J")
                    {
                        #region Complete Job Packet
                        iPosition++;
                        this.EventID = BitConverter.ToUInt32(bMsg, iPosition);										// Event ID
                        iPosition = iPosition + 4;
                        this.BookingID = BitConverter.ToUInt32(bMsg, iPosition);									// Booking ID
                        iPosition = iPosition + 4;
                        bConvert = new byte[4];																															// Profit Centre
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        this.ProfitCentreID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        #endregion
                    }
                    #endregion
                    break;
                case "M":
                    #region Messages
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);		// Sub Command
                    if (this.SubCommandType == "F")
                    {
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x06) // If this byte is the MDSC byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                }
                                this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                break;
                            }
                        }
                    }
                    if (this.SubCommandType == "f")
                    {
                        for (int X = iPosition; X < bMsg.Length; X++)																					// Email Address
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the EMS byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                    iPosition++;
                                }
                                this.EmailAddress = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                break;
                            }
                        }
                        for (int X = iPosition; X < bMsg.Length; X++)																					// Message Data
                        {
                            if (bMsg[X] == (byte)0x06) // If this byte is the MSDC byte
                            {
                                bConvert = new byte[X];
                                for (int Y = iPosition; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                }
                                this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                break;
                            }
                        }

                    }
                    if (this.SubCommandType == "P")
                    {
                        bConvert = new byte[4];																														// Message ID
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.MessageID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    }
                    if (this.SubCommandType == "p")
                    {
                        for (int X = iPosition; X < bMsg.Length; X++)																					// Email Address
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the EMS byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                    iPosition++;
                                }
                                this.EmailAddress = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                break;
                            }
                        }
                        bConvert = new byte[4];																														// Message ID
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.MessageID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    }
                    if (this.SubCommandType == "R")
                    {
                        bConvert = new byte[4];																														// Message ID
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.MessageID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    }
                    #endregion
                    break;
                case "F":
                    #region Fuel Usage
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);		// Sub Command
                    if (this.SubCommandType == "p")
                    {
                        bConvert = new byte[4];																															// Location ID
                        bConvert[0] = bMsg[iPosition++];
                        this.FuelStationID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        bConvert = new byte[4];																															// Fuel Litres
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.FuelLitres = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        bConvert = new byte[4];																															// Fuel Cost
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        this.FuelCost = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    }
                    #endregion
                    break;
                case "U":
                    #region Vehicle Usage
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);		// Sub Command
                    #endregion
                    break;
                default:

                    break;
            }
            return "";
        }
        #endregion

        #region GetPacketTypeName
        public string GetPacketTypeName()
        {
            string sRet = "";

            switch (CommandType)
            {
                case "N":
                    sRet = "Interface Ack Packet";
                    break;
                case "O":
                    sRet = "Login Packet";
                    break;
                case "A":
                    sRet = "Accept Job Packet";
                    break;
                case "X":
                    sRet = "Reject Job Packet";
                    break;
                case "S":
                    sRet = GetAdhocPacketType();
                    break;
                case "C":
                    sRet = "Complete Job Packet";
                    break;
                case "R":
                    sRet = GetRequestPacketType();
                    break;
                case "P":
                    sRet = "Pre-defined Function Packet";
                    break;
                case "B":
                    sRet = GetNettrackPacketType();
                    break;
                case "M":
                    sRet = "Message Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetAdhocPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "P":
                    sRet = "Adhoc Pickup  Packet";
                    break;
                case "D":
                    sRet = "Adhoc Delivery Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetRequestPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "J":
                    sRet = "Request Job Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetNettrackPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "G":
                    sRet = "Nettrack Generic Data Packet";
                    break;
                case "S":
                    sRet = "Nettrack Start Job Packet";
                    break;
                case "P":
                    sRet = "Nettrack Picjup Packet";
                    break;
                case "D":
                    sRet = "Nettrack Pre-Delivery Packet";
                    break;
                case "C":
                    sRet = "Nettrack Complete Job Packet";
                    break;
                case "O":
                    sRet = "Nettrack Delivery Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }
        #endregion

        #region Private Functions
        private static byte[] CreateBadPacketNak()
        {
            byte[] bPacketData = new byte[1];
            byte[] bPacket = new byte[1];
            byte[] bPacketLen = null;
            byte[] bCmdType = null;

            bCmdType = System.Text.ASCIIEncoding.ASCII.GetBytes("N");

            bPacketData[0] = bCmdType[0];
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, Convert.ToInt32((byte)0x00));
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, "4");
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x07);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x03);

            bPacketLen = BitConverter.GetBytes(Convert.ToUInt32(bPacketData.Length));
            bPacket = PacketUtilities.AppendToPacket(bPacket, (byte)bPacketLen[0]);
            bPacket = PacketUtilities.AppendToPacket(bPacket, (byte)bPacketLen[1]);
            bPacket = PacketUtilities.AppendToPacket(bPacket, bPacketData);
            return bPacket;
        }

        private static byte[] GetSubBytes(byte[] bMsg, int iStartAt, int iNumOfBytes)
        {
            byte[] bResult = null;
            int iResultCount = 0;

            if (bMsg == null) return null;

            if (bMsg.Length >= (iStartAt + iNumOfBytes))
            {

                bResult = new byte[iNumOfBytes];

                for (int X = iStartAt; X < iStartAt + iNumOfBytes; X++)
                {
                    bResult[iResultCount] = bMsg[X];
                    iResultCount++;
                }
                return bResult;
            }
            else
            {
                iNumOfBytes = bMsg.Length;
                bResult = new byte[iNumOfBytes - iStartAt];
                for (int X = iStartAt; X < iNumOfBytes; X++)
                {
                    bResult[iResultCount] = bMsg[X];
                    iResultCount++;
                }
                return bResult;
            }
        }

        private static string RemoveLeadingReturnChars(string strData)
        {
            string strTemp = strData.Trim();

            if (strTemp.Length > 0)
            {
                while (strTemp[0] == '\r' || strTemp[0] == '\n')
                {
                    if (strTemp.Length == 1)
                    {
                        strTemp = "";
                        break;
                    }
                    else
                    {
                        strTemp = strTemp.Substring(1, strTemp.Length - 1);
                    }
                }
            }
            return strTemp;
        }

        private static string WordWrapString(string strDescLines, int iLineLength)
        {
            char[] cDesc = strDescLines.ToCharArray(0, strDescLines.Length);
            string sResult = "";
            string sLine = "";
            int iCurrentPos = 1;

            if (cDesc.Length > 0)
            {
                for (int X = 0; X < cDesc.Length; X++)
                {
                    // For each char in the array
                    if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                    {
                        if (iCurrentPos > 0)
                        {
                            sResult += sLine + "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                        else
                        {
                            sResult += "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                    }
                    else
                    {
                        if (iCurrentPos > iLineLength)
                        {
                            int iLastSpace = sLine.LastIndexOf(' ');
                            if (iLastSpace > 0)
                            {
                                sResult += sLine.Substring(0, iLastSpace) + "\n";
                                sLine = sLine.Substring(iLastSpace + 1).Trim();
                                iCurrentPos = sLine.Length;
                            }
                            else
                            {
                                sResult += sLine + "\n";
                                sLine = "";
                                iCurrentPos = 0;
                            }
                            if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                            {
                                if (iCurrentPos > 0)
                                {
                                    sResult += sLine + "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                                else
                                {
                                    sResult += "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                            }
                            else
                            {
                                if (iCurrentPos == 0 && cDesc[X] == ' ')
                                {
                                    // Don't add white space to the front of the line.
                                }
                                else
                                {
                                    sLine += cDesc[X];
                                    iCurrentPos++;
                                }
                            }
                        }
                        else
                        {
                            if (iCurrentPos == 0 && cDesc[X] == ' ')
                            {
                                // Don't add white space to the front of the line.

                            }
                            else
                            {
                                sLine += cDesc[X];
                                iCurrentPos++;
                            }
                        }
                    }
                }
            }

            if (sLine.Length > 0)
                sResult += sLine;

            return sResult;
        }

        public static string ConvertToAscii(byte[] bPacket)
        {
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
        {
            string sRet = "";
            int X = 0;
            //			byte bLower = 0x21;	// 33
            //			byte bUpper = 0x7E;  // 126
            byte bTest = 0x00;  // null
            byte[] bTestArray = new byte[1];

            if (bPacket.Length > 0)
            {
                try
                {
                    for (X = 0; X < bPacket.Length; X++)
                    {
                        bTest = bPacket[X];
                        bTestArray = new byte[1];
                        bTestArray[0] = bTest;
                        if (bTest >= 33 && bTest <= 126)  // If the character is in printable range
                            if (bAddSpacing)
                                if (sRet.Length > 0)
                                    sRet += " - " + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                                else
                                    sRet = System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                            else
                                sRet += System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                        else  // Show it as a number
                            if (bAddSpacing)
                                if (sRet.Length > 0)
                                    sRet += " - [" + bTest + "]";
                                else
                                    sRet = "[" + Convert.ToString(bTest) + "]";
                            else
                                sRet += "[" + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray) + "]";
                    }

                }
                catch (System.Exception ex)
                {
                    Console.Write(ex.Message + "\n");
                }
            }
            return sRet;
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind)
        {
            return FindFirstIndexOfByte(sData, bFind, 0, sData.Length);
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt)
        {
            return FindFirstIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
        {
            int iRet = -1;

            try
            {
                iRet = sData.IndexOf((char)bFind + "", iStartAt, iLength);
            }
            catch (System.Exception)
            {
                return -1;
            }
            return iRet;
        }

        private static int FindLastIndexOfByte(string sData, byte bFind)
        {
            return FindLastIndexOfByte(sData, bFind, sData.Length, sData.Length);
        }

        private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt)
        {
            return FindLastIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
        }

        private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
        {
            int iRet = -1;

            try
            {
                iRet = sData.LastIndexOf((char)bFind + "", iStartAt, iLength);
            }
            catch (System.Exception)
            {
                return -1;
            }
            return iRet;
        }


        private static string TruncateString(string sData, int iPos)
        {
            string sRet = "";

            try
            {
                if (sData.Length < iPos + 1)
                    return "";
                else
                    sRet = sData.Substring((iPos + 1), sData.Length - (iPos + 1));
            }
            catch (System.Exception)
            {
                return "";
            }
            return sRet;
        }
        #endregion
    }

}

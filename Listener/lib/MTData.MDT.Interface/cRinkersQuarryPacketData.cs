using System;
using System.Drawing;
using System.IO;
using System.Collections;
using System.Collections.Specialized;
using log4net;
using MTData.MDT.Interface;
using MTData.Common.Utilities;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// Summary description for cRinkersQuarryPacketData.
	/// </summary>
	public class cRinkersQuarryPacketData
	{

		#region Incomming Packet Defs
		// [GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
		// [Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
		// N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Job ID][Stop #][SBS][Spare]
		// I - Logon to System - [CmdType] [Header] [Password] [PhoneBookID] [Personal PhoneBook ID] [SBS] [Spare]
		// O - Log off the System -[CmdType] [Header] [SBS] [Spare]
		// R - Request Packets
		//		J - Request Jobs - [CmdType] [SubCmd] [Header] [SBS] [Spare]
		// U - Alert Packet - [CmdType] [SubCmd] [Header] [SBS] [Spare]
		// M - Messages
		//	R - Driver Read Meassage - [CmdType] [SubCmd] [Header] [MessageID] [SBS][Spare]
		//	P - Predefined Responce - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
		//	F - Freeform Message - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
		// K - Phone call report
		//	I - Incomming Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]
		//	O - Outgoing Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]
		#endregion

		#region Outgoing Packet Defs
		// N - Ack -	[CmdType] [Result Code] [SBS] [Spare]
		// M - Message - [CmdType] ([Description] [EOL]) [SBS] [Spare]
		// K - Phonebook Entries
		//	P - Global Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
		//	D - Drivers Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
		// L - Login Ack
		//	U - Incorrect Username - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		//	P - Incorrect Password - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		//	C - Correct Login  - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		//	O - Logout unit - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
		#endregion

		#region Private Vars
        private ILog _log = LogManager.GetLogger(typeof(cRinkersQuarryPacketData));
		private bool bTranslateRJs = true;		
		#endregion

		#region Public Vars
		// File Download vars
		public string sSigFilePath = "";
		public byte[] bDataSegment = null;
		public int NumberOfDataSegments = 0;
		public int SegmentNumber = 0;
		public int SendSegment = 0;
		public int iSoftwareVersion = 0;
		// Shareed Public variables
		public int iMobileID = 0;
		public string CommandType = "";				// [CmdType]
		public int JobID = 0;								// [Job ID]
		public int StopID = 0;								// [Stop #]
		public int CodeNumber = 0;						// [FailCode], [SucessCode], [Function ID]
		public string Message = "";						// [Message Data]
	
		// Incomming Packet Specific
		public string SubCommandType = "";		// [SubCmd]
		public int QuantityLeft = 0;
		public int StatusCode = 0;
		public bool StartFunction = false;			// [Start / Stop]
		public int ReferenceNumber = 0;
		public int TimeLoading = 0;
		public int DivisionID = 0;
		public int CallTime = 0;
		public string CallType = "";
		public string SimCardNumber = "";
		public int ResultCode = 0;							// [Result Code]
		public bool YesNo = false;						// [Yes No] -> Yes = true
		public int ResponceTimeHours = 0;			// [ResponceTimeHours]
		public int ResponceTimeMinutes = 0;			// [ResponceTimeMinutes]
		public int SuburbID = 0;							// [Suburb ID]
		public int MessageID = 0;							// [Message ID]
		public int VehicleID = 0;							// [VehicleID]
		public int FleetID = 0;								// [FleetID]
		public int DriverID = 0;							// [DriverID]
		public string DriverName = "";
		public int ProtocolVersion = 0;					// [Protocol Version]
		public int HardwareType = 0;					// [Hardware Type]
		public int TimeOffset = 0;						// [TimeOffset]
		public byte DLSByte = (byte) 0x00;		// [Day Light Savings Byte]
		public int MessagebookID = 0;					// [MessageBook ID]
		public int QuestionListID = 0;					// [Quesiton List ID]
		public int DivisionReasonID = 0;				// [DivisionReasonID]
		public int DelayReasonID = 0;					// [DelayReasonID]
		public int RejectedReasonID = 0;			// [RejectedReasonID]
		public int Distance = 0;								// [Distance]
		public int Odometer = 0;
		public int DeviceTimeSeconds = 0;					// Time since last GPS lock
		public int FirmwareRevMajor = 0;
		public int FirmwareRevMinor = 0;
		public int HardwareRevMajor = 0;
		public int HardwareRevMinor = 0;
		public int BatteryLevel = 0;
		public int Options = 0;		
		public int SensorState = 0;
		public double CurTemp = 0;
		public string Spare1 = "";
		public string Spare2 = "";
		public string Spare3 = "";
		public string Spare4 = "";
		public string Spare5 = "";
		public string FirmwareVariant = "";
		public string DelayDescription = "";
		public string NoOfPallets = "";
		public string EmailAddress = "";
		public string PhoneNumber = "";
		public string Username = "";						// [Username]
		public string Password = "";						// [Password]
		public string PODName = "";					// [POD Name]
		public string JobTime = "";						// [Time], [Time in Mins]
		public string BusinessName = "";				// [Business Name]
		public string InvoiceNumber = "";			// [Invoice Number]
		public StringCollection BarCodes = null	;				// [Bar Code x]
		public StringCollection ReturnBarCodes = null	;	// [Return Bar Code x]
		public StringCollection PalletTypeCodes = null;
		public StringCollection PalletNumbers = null;
		public StringCollection Trailers = null;
		public StringCollection TrailerOdometers = null;
		public ArrayList QuestionNumbers = null;
		public ArrayList QuestionAnswers = null;
		public byte[] bGenericData = null;			// [200 bytes]
		public byte[] SignatureData = null;				// [Signature]
		public byte bAllowFreeDial = (byte) 0x00;		// [Allow Free Dial]


		// Outgoing Packet Specific
		public int TotalNumberOfStops = 0;
		public int PhoneBookID = 0;
		public int PersonalPhoneBookID = 0;
		public int DivisionListID = 0;
		public int DelayReasonListID = 0;
		public int RejectionReasonListID = 0;
		public string JobTitle = "";
		public string ContactName = "";
		public string ContactPh = "";
		public string JobDescription = "";
		public string LoginReply = "";
		public string JobType = "";
		public string OrderNumber = "";
		public int LoadNumber = 0;
		public int PlantLatitude = 0;
		public int PlantLongitude = 0;
		public int PlantRadius = 0;
		public string PlantAddress = "";
		public string PlantName = "";
		public int JobLatitude = 0;
		public int JobLongitude = 0;
		public int JobRadius = 0;
		public string CustomerName = "";
		public string ItemCode = "";
		public string JobAddress = "";
		public string JobInstructions = "";
		public DateTime TimeRequiredAtJob = System.DateTime.Now;
		public DateTime TimeRequiredAtPlant = System.DateTime.Now;
		public int ReturnLatitude = 0;
		public int ReturnLongitude = 0;
		public int ReturnRadius = 0;
		public string ReturnAddress = "";
		public string ReturnName = "";
		public int JobFlags;
		public int LoadWeight_kilos;

		public StringCollection PhoneBookLocations = new StringCollection();
		public StringCollection PhoneBookNumbers = new StringCollection();
		public StringCollection PhoneBookNames = new StringCollection();
		public StringCollection MessageNames = new StringCollection();
		public StringCollection DivisionIDs = new StringCollection();
		public StringCollection DivisionNames = new StringCollection();
		public StringCollection DelayReasonNames = new StringCollection();
		public StringCollection DelayReasonIDs = new StringCollection();
		public StringCollection RejectionReasonNames = new StringCollection();
		public StringCollection RejectionReasonIDs = new StringCollection();
		
		// GPS Data
		public cGPSData GPSPosition = null;
		#endregion

		public cRinkersQuarryPacketData(  string SigFilePath, bool TranslateRJs)
		{
			
			sSigFilePath = SigFilePath;
			bTranslateRJs = TranslateRJs;
		}

		#region Build Packet Data
		public byte[] BuildOutgoingPacket()
		{
			byte[] bRet = null;
			byte[] bConvert = null;
			int iEntries = 0;
			
			bRet = new byte[1];
			bRet[0] = (byte) 0x02;
			int iStartLen = bRet.Length;
			bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x00);
			bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x00);			
			bRet = PacketUtilities.AppendToPacket(bRet, CommandType);
			
			switch(CommandType)
			{
				case "B":
					#region Put Unit in Standby
					if(_log != null) _log.Info("Building 'Put Unit in Standby' Packet");
					#endregion
					break;
				case "C":
					#region Change Return Address
					if(_log != null) _log.Info("Building Change Address Packet");
					bRet = PacketUtilities.AppendToPacket(bRet, SubCommandType);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnLatitude);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnLongitude);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.ReturnRadius);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnName);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnAddress);					
					#endregion
					break;
				case "D":
					#region Download Packets
					if (SubCommandType == "H")
					{
						#region Download Header
						if(_log != null) _log.Info("Building Download Header Packet");
						bRet = PacketUtilities.AppendToPacket(bRet, SubCommandType);
						bConvert = new byte[4];
						bConvert = BitConverter.GetBytes(this.iSoftwareVersion);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
						bConvert = new byte[4];
						bConvert = BitConverter.GetBytes(this.NumberOfDataSegments);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
						#endregion
					}
					else if (SubCommandType == "D")
					{
						#region Download Packet Data
						if(_log != null) _log.Info("Building Download Data Packet");
						bRet = PacketUtilities.AppendToPacket(bRet, SubCommandType);
						bConvert = new byte[4];
						bConvert = BitConverter.GetBytes(this.SegmentNumber);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
						bConvert = new byte[4];
						bConvert = BitConverter.GetBytes(this.bDataSegment.Length);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
						bRet = PacketUtilities.AppendToPacket(bRet, bDataSegment);
						int iCheckSum = 0;
						for (int X = 3; X < bRet.Length; X++)
						{
							iCheckSum += (int) bRet[X];
						}
						bConvert = BitConverter.GetBytes(iCheckSum);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
						#endregion
					}
					#endregion
					break;
				case "N": 
					#region Ack Packet
					// [CmdType] [Result Code] [SBS] [Spare]
					if(_log != null) _log.Info("Building Ack Packet");
					bConvert = new byte[4];																								// Result Code
					bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.CodeNumber));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					#endregion
					break;
				case "L":
					#region Login Reply packet
					// L - Login Ack
					//	U - Incorrect Username - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					//	P - Incorrect Password - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					//	C - Correct Login  - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					//	O - Logout unit - [CmdType] [SubCmd] [Time Offset] [DLS Bit] [SBS] [Spare]
					if(_log != null) _log.Info("Building Login Reply Packet");
					bRet = PacketUtilities.AppendToPacket(bRet, SubCommandType);				// Login Responce
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.TimeOffset);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);					// Time Offset
					bRet = PacketUtilities.AppendToPacket(bRet, this.DLSByte);					// DLS Bit
					bRet = PacketUtilities.AppendToPacket(bRet, this.DriverName);				// Driver Name
					#endregion
					break;
				case "G":
					#region GPS Request packet
					if(_log != null) _log.Info("Building GPS Request Packet");
					#endregion
					break;
				case "M":
					#region Send a driver message
					// M - Message - [CmdType] [Message ID] ([Description] [EOL]) [SBS] [Spare]
					if(_log != null) _log.Info("Building Driver Message Packet");
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.MessageID);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);												// Message ID
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);												// Message ID
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);												// Message ID
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);												// Message ID
					bRet = PacketUtilities.AppendToPacket(bRet, this.Message);	
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0F);	
					#endregion
					break;
				case "K":
					#region Send a phone book Entry
					// K - Phonebook Entries
					//	P - Global Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
					//	D - Drivers Phone book - [CmdType] [SubCmd] [PhonebookID] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
					//	M - Predefined Messages - [CmdType] [SubCmd] [ListVersion] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]
					//	Q - Driver Questions - [CmdType] [SubCmd] [ListVersion] ( [PSS] [Location] [Number] [NSC] [Name] ) [SBS] [Spare]

					if(_log != null) _log.Info("Building List Entries Packet");
					bRet = PacketUtilities.AppendToPacket(bRet, this.SubCommandType);								// SubCmd
					bConvert = new byte[4];																							// phonebookID / Version ID
					bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.PhoneBookID));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

					iEntries = 0;
					
					if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > iEntries) iEntries = PhoneBookNumbers.Count;
					if (PhoneBookNames != null) if (PhoneBookNames.Count > iEntries) iEntries = PhoneBookNames.Count;
					if (iEntries > 0)
					{
						// global phonebook starts from 0 and personal from 50
						int iStart = 0;
						if (this.SubCommandType == "D")
							iStart = 50;

						// according to specs, max number for global phb is 50
						if (iEntries > 50) iEntries = 50;
						for (int X = iStart; X < iStart + iEntries; X++)
						{
							bRet = PacketUtilities.AppendToPacket(bRet,   (byte)0x0A);									// PSS - Phone book separator

							bConvert = new byte[4];																						// Location
							bConvert = BitConverter.GetBytes(Convert.ToInt32(X));
							bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
						
							if (PhoneBookNumbers != null)
								if (PhoneBookNumbers[X - iStart] != "")
									bRet = PacketUtilities.AppendToPacket(bRet, PhoneBookNumbers[X - iStart]);			// Phone Number
								else
									bRet = PacketUtilities.AppendToPacket(bRet, "0");
						
							bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0B);									// NSC - Phone book separator

							if (PhoneBookNames != null)
								bRet = PacketUtilities.AppendToPacket(bRet, PhoneBookNames[X - iStart]);				// Name
							else
								bRet = PacketUtilities.AppendToPacket(bRet, "N/A");	
						}
					}
					#endregion
					break;
				case "J":
					#region Dispatch a Job
					// [CmdType] [Job Type] [Order Number] [Load Number] [Plant Lat] [Plant Long] [Plant Radius] [Plant Name] [EOS] [Plant Address] [EOS] [Job Lat] [Job Long] [Job Radius] [Customer Name] [EOS] [Product Name] [EOS] [Job Address] [EOS] [Job Instructions] [EOS] [Time Required at Job] [Time Required at Plant] [Return Lat] [Return Long] [Return Radius] [Return Name] [EOS] [Return Address][SBS]
					if(_log != null) _log.Info("Building Job Dispatch Packet");
					bRet = PacketUtilities.AppendToPacket(bRet, JobType);
					
					int orderNumber = 0;
					try
					{
						string TempOrderNo = this.OrderNumber.Trim();
						if (TempOrderNo.Length > 0)
							orderNumber = Int32.Parse(TempOrderNo);
					}
					catch(Exception ex)
					{
						if(_log != null) _log.Error("Error parsing OrderNumber", ex);
					}
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(orderNumber);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.LoadNumber);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, this.PlantLatitude);
					bRet = PacketUtilities.AppendToPacket(bRet, this.PlantLongitude);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.PlantRadius);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, this.PlantName);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.PlantAddress);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.JobLatitude);
					bRet = PacketUtilities.AppendToPacket(bRet, this.JobLongitude);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.JobRadius);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, this.CustomerName);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ItemCode);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.JobAddress);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.JobInstructions);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtJob.Day));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtJob.Month));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtJob.Year));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtJob.Hour));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtJob.Minute));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					//bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtPlant.Day));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtPlant.Month));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtPlant.Year));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtPlant.Hour));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(Convert.ToInt32(this.TimeRequiredAtPlant.Minute));
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);					
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnLatitude);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnLongitude);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.ReturnRadius);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnName);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bRet = PacketUtilities.AppendToPacket(bRet, this.ReturnAddress);
					bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.LoadWeight_kilos);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[2]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[3]);
					bConvert = new byte[4];
					bConvert = BitConverter.GetBytes(this.JobFlags);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
					bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);

					#endregion
					break;
				case "F":
					#region Delete Jobs
					if(_log != null) _log.Info("Sending Delete Job");
					#endregion
					break;
				default:
					break;
			}

			bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x07); // SBS - Spare Bytes Seperator
			bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x03); // EOP - End of Packet
			bConvert = new byte[4];
			bConvert = BitConverter.GetBytes(bRet.Length);
			bRet[iStartLen] = bConvert[0];
			bRet[iStartLen + 1] = bConvert[1];
			string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
			if(_log != null) _log.Info(sData);
			return bRet;
		}
		#endregion

		#region Parse Incomming Packet
		public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
		{
			string sRet = "";
			try
			{
				this.iMobileID = Convert.ToInt32(sUnitID);
				sRet = TranslateMsgToHost(bMsg, sUnitID);
			}
			catch(System.Exception ex)
			{
				if(_log != null) 
				{
					if(bMsg == null)
						_log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = null, sUnitID = '" + sUnitID + "') " + ex.InnerException.Message + " " + ex.Message, ex);
					else
						_log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg, sUnitID)", ex);
				}
			}
			return sRet;
		}

		private string TranslateMsgToHost(byte[] bMsg, string sUnitID)
		{
			// Special Field Defs
			// [GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
			// [Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]

			// Packet Types
			// N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Job ID][Stop #][SBS][Spare]
			// D - Request Data Segment - [CmdType][SubCmd][Send Segment][SBS][Spare]
			// I - Logon to System - [CmdType] [Header] [Password] [PhoneBookID] [MessageBook ID] [Question List ID] [SBS] [Spare]
			// O - Log off the System -[CmdType] [Header] [SBS] [Spare]
			// R - Request Packets
			//		J - Request Jobs - [CmdType] [SubCmd] [Header] [SBS] [Spare]
			// U - Alert Packet - [CmdType] [SubCmd] [Header] [SBS] [Spare]
			// M - Messages
			//	R - Driver Read Meassage - [CmdType] [SubCmd] [Header] [MessageID] [SBS][Spare]
			//	P - Predefined Responce - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
			//	F - Freeform Message - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
			// K - Phone call report
			//	I - Incomming Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]
			//	O - Outgoing Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SBS] [Spare]

			#region Local Variables
			byte bTemp = (byte) 0x00;
			byte[] bFailCode = new byte[4];
			byte[] bMBPPacket = new byte[1];
			byte[] bStopID = new byte[4];
			byte[] bConvert = new byte[4];
			//byte[] bGPS = null;
			byte[] bSignatureLen = new byte[4];
			byte[] bFunctionID = new byte[1];
			byte[] bStartStop = new byte[1];
			//UInt32 iJobID = 0;
			int iPosition = 0;
			//string strErrMsg = "";
			//int z = 0;
			//int y = 0;
			//string sDD = ""; // delay description helper
			//string sPN = ""; // pod name helper
			#endregion

			// Get the Cmd Type for the packet
			// Get the Cmd Type for the packet
			bConvert = new byte[4];
			bConvert[0] = bMsg[iPosition++]; // 0x02 Header
			if (bConvert[0] != (byte) 0x02)
			{
				if(_log != null) _log.Info("Found packet with no MSTP header : " + BitConverter.ToString(bMsg, 0));
				return "";
			}
			bConvert[0] = bMsg[iPosition++]; // Length 1
			bConvert[1] = bMsg[iPosition++]; // Length 2

			int iLength = BitConverter.ToInt32(bConvert, 0);
			if (bMsg.Length != iLength)
				return "";

			this.CommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);
			if (this.CommandType == "A" || this.CommandType == "B" || this.CommandType == "R" || this.CommandType == "M" || this.CommandType == "K" || this.CommandType == "D" || this.CommandType == "J" || this.CommandType == "V")
			{
				this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);
			}

			if (this.CommandType != "N" && this.CommandType != "D")
			{
				#region Read the header record.
				// [Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
				// Get the vehicle
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				this.iMobileID = BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				this.FleetID= BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				this.VehicleID= BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				this.ProtocolVersion = BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				this.HardwareType = BitConverter.ToInt32(bConvert, 0);

				#region [GPS]
				this.GPSPosition = new cGPSData();
				// LSC Byte
				iPosition++;
				// Lat
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				bConvert[2] = bMsg[iPosition++]; 
				bConvert[3] = bMsg[iPosition++]; 
				this.GPSPosition.dLatitude = LatLonConversion.ConvLatLon(bConvert);
				// Long
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				bConvert[2] = bMsg[iPosition++]; 
				bConvert[3] = bMsg[iPosition++]; 
				this.GPSPosition.dLongitude = LatLonConversion.ConvLatLon(bConvert);

				#region Get GPS Time string
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				int iDay = BitConverter.ToInt32(bConvert, 0);
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				int iMonth = BitConverter.ToInt32(bConvert, 0);
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				int iYear = 2000 + BitConverter.ToInt32(bConvert, 0);
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				int iHour = BitConverter.ToInt32(bConvert, 0);
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				int iMinute = BitConverter.ToInt32(bConvert, 0);
				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				int iSecond = BitConverter.ToInt32(bConvert, 0);
				try
				{
					this.GPSPosition.dtGPSTime= new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
				}
				catch(System.Exception)
				{
				}
				#endregion

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				this.GPSPosition.iSpeed = BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				this.GPSPosition.iHeading = BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				bConvert[2] = bMsg[iPosition++]; 
				bConvert[3] = bMsg[iPosition++]; 
				this.Distance= BitConverter.ToInt32(bConvert, 0);

				bConvert = new byte[4];
				bConvert[0] = bMsg[iPosition++]; 
				bConvert[1] = bMsg[iPosition++]; 
				this.DeviceTimeSeconds= BitConverter.ToInt32(bConvert, 0);
				#endregion

				this.Username = "";
				for (int X = 0; X < 7; X++)
				{
					if (bMsg[iPosition] == (byte) 0x0A)
					{
						iPosition++;
						break;
					}
					else
					{
						this.Username += System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);
					}
				}
				#endregion
			}

			switch(CommandType)
			{
				case "A":
					#region Change in Sensor state
					//A - Change in sensor state - [CmdType][SubCmd][Header][SensorState][SBS][Spare]
					if(_log != null) _log.Info("Decoding Vehicle State Change Packet (Tipper or Pressure Sensor)");
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					this.SensorState = BitConverter.ToInt32(bConvert, 0);
					#endregion
					break;
				case "B":
					#region Driver Break Packet
					#endregion
					break;
				case "C":
					#region Download Complete
					#endregion
					break;
				case "D":
					#region Request a Download Packet
					//D - Request Data Segment - [CmdType][SubCmd][Send Segment][SBS][Spare]
					if(_log != null) _log.Info("Decoding Download Request Packet");
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					bConvert[2] = bMsg[iPosition++]; 
					bConvert[3] = bMsg[iPosition++]; 
					this.SendSegment = BitConverter.ToInt32(bConvert, 0);
					#endregion
					break;
				case "N":
					#region Ack an NAck Responce
					if(_log != null) _log.Info("Decoding Ack/Nack Packet");
					// N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Job ID][Stop #][SBS][Spare]
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					this.iMobileID = BitConverter.ToInt32(bConvert, 0);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					this.ResultCode= BitConverter.ToInt32(bConvert, 0);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					bConvert[2] = bMsg[iPosition++]; 
					bConvert[3] = bMsg[iPosition++]; 
					this.JobID = BitConverter.ToInt32(bConvert, 0);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					this.StopID = BitConverter.ToInt32(bConvert, 0);
					#endregion
					break;
				case "I":
					#region Logon to System
					if(_log != null) _log.Info("Decoding Login Packet");
					// I - Logon to System - [CmdType] [Header] [Password] [PhoneBookID] [MessageBook ID] [Question List ID] [SBS] [Spare]
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					bConvert[2] = bMsg[iPosition++]; 
					bConvert[3] = bMsg[iPosition++]; 
					this.Password = System.Text.ASCIIEncoding.ASCII.GetString(bConvert, 0, 4);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					this.iSoftwareVersion = BitConverter.ToInt32(bConvert, 0);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					bConvert[2] = bMsg[iPosition++]; 
					bConvert[3] = bMsg[iPosition++]; 
					this.PhoneBookID = BitConverter.ToInt32(bConvert, 0);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					bConvert[2] = bMsg[iPosition++]; 
					bConvert[3] = bMsg[iPosition++]; 
					this.MessagebookID = BitConverter.ToInt32(bConvert, 0);
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					bConvert[2] = bMsg[iPosition++]; 
					bConvert[3] = bMsg[iPosition++]; 
					this.QuestionListID = BitConverter.ToInt32(bConvert, 0);
					#endregion
					break;
				case "O":
					#region Logoff System
					// O - Log off the System -[CmdType] [Header] [SBS] [Spare]
					if(_log != null) _log.Info("Decoding Logoff Packet");
					#endregion
					break;
				case "R":
					#region Request Packet
					// R - Request Packets
					//		J - Request Jobs - [CmdType] [SubCmd] [Header] [SBS] [Spare]
					
					#endregion
					break;
				case "L":
					#region Location Responce
					// Nothing to do here.
					#endregion
					break;
				case "M":
					#region Message Responce
					// M - Messages
					//	A - Answer Question - [CmdType] [SubCmd] [Header] ([Question Number] [Answer] [ASB]) [SOS] [Signature Data] [EOS] [SBS] [Spare]
					//	R - Driver Read Meassage - [CmdType] [SubCmd] [Header] [MessageID] [SBS][Spare]
					//	P - Predefined Responce - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
					//	F - Freeform Message - [CmdType] [SubCmd] [Header] [Message ID] [Yes No] [Time] [SBS] [Spare]
					if (this.SubCommandType == "A")
					{
						if(_log != null) _log.Info("Decoding Question Answer Packet");

						QuestionNumbers = new ArrayList();
						QuestionAnswers = new ArrayList();
						if (iPosition < bMsg.Length)
						{
							while (bMsg[iPosition] != (byte) 0xFF)
							{
								bConvert = new byte[4];
								bConvert[0] = bMsg[iPosition++]; 
								int iQuestionNumber = BitConverter.ToInt32(bConvert, 0);
								bConvert = new byte[4];
								bConvert[0] = bMsg[iPosition++]; 
								int iQuestionAnswer = BitConverter.ToInt32(bConvert, 0);
								if(bMsg[iPosition++] == (byte) 0x0A)
								{
									QuestionNumbers.Add(iQuestionNumber);
									if (iQuestionAnswer == 0)
										QuestionAnswers.Add(false);
									else
										QuestionAnswers.Add(true);
								}
								if (iPosition >= bMsg.Length)
									break;
							}
							iPosition++; // Skip over the 0xFF (Start of Signature)
							if (iPosition + 6 < bMsg.Length)
							{

								bConvert = new byte[4];
								bConvert[0] = bMsg[iPosition++]; 
								bConvert[1] = bMsg[iPosition++]; 
								bConvert[2] = bMsg[iPosition++]; 
								bConvert[3] = bMsg[iPosition++]; 
								int iSigLength = BitConverter.ToInt32(bConvert, 0);
								
								if(iSigLength > 0)
								{
									byte[] bInputData = new byte[1];
									bInputData[0] = bMsg[iPosition++];
									for (int iSig = 1; iSig < iSigLength; iSig++)
									{
										if (iPosition < bMsg.Length)
										{
											bInputData = PacketUtilities.AppendToPacket(bInputData, bMsg[iPosition++]);
										}
										else
											break;
									}

									if (iPosition < bMsg.Length)
									{
										if (!System.IO.Directory.Exists(sSigFilePath))
											System.IO.Directory.CreateDirectory(sSigFilePath);

										string sSigFileName = sSigFilePath + "Unit " + Convert.ToString(iMobileID) + " Signature - " +  System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
										SignatureData = DecodeSignature(bInputData, sSigFileName);
										bInputData = null;
									}
								}
							}
						}
					}
					else if (this.SubCommandType == "R")
					{
						bConvert = new byte[4];
						bConvert[0] = bMsg[iPosition++]; 
						bConvert[1] = bMsg[iPosition++]; 
						bConvert[2] = bMsg[iPosition++]; 
						bConvert[3] = bMsg[iPosition++]; 
						this.MessageID = BitConverter.ToInt32(bConvert, 0);
					}
					else if (this.SubCommandType == "P")
					{
						bConvert = new byte[4];
						bConvert[0] = bMsg[iPosition++]; 
						bConvert[1] = bMsg[iPosition++]; 
						bConvert[2] = bMsg[iPosition++]; 
						bConvert[3] = bMsg[iPosition++]; 
						this.MessageID = BitConverter.ToInt32(bConvert, 0);

						bConvert = new byte[1];
						bConvert[0] = bMsg[iPosition++]; 
						this.YesNo = BitConverter.ToBoolean(bConvert, 0);

						bConvert = new byte[4];
						bConvert[0] = bMsg[iPosition++]; 
						this.ResponceTimeHours = BitConverter.ToInt32(bConvert, 0);

						bConvert = new byte[4];
						bConvert[0] = bMsg[iPosition++]; 
						this.ResponceTimeMinutes = BitConverter.ToInt32(bConvert, 0);
					}
					else if (this.SubCommandType == "F")
					{
						for (int X = 0; X < bMsg.Length - iPosition; X++)
						{
							if (bMsg[iPosition] == (byte) 0x07)
							{
								iPosition++;
								break;
							}
							else
								this.Message += System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);
						}
					}
					else if (this.SubCommandType == "C")
					{
						bConvert = new byte[4];
						bConvert[0] = bMsg[iPosition++]; 
						bConvert[1] = bMsg[iPosition++]; 
						bConvert[2] = bMsg[iPosition++]; 
						bConvert[3] = bMsg[iPosition++]; 
						this.MessageID = BitConverter.ToInt32(bConvert, 0);
					}
					#endregion
					break;
				case "Q":
					#region Quantity Left On Message
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					this.QuantityLeft = BitConverter.ToInt32(bConvert, 0);
					#endregion
					break;
				case "K":
					#region Phone call report
					// K - Phone call report
					//	I - Incomming Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SimCardNumber] [SBS] [Spare]
					//	O - Outgoing Call - [CmdType] [SubCmd] [Header]  [Number] [PSP] [Time] [SimCardNumber] [SBS] [Spare]
					for (int X = 0; X < bMsg.Length; X++)
					{
						if (bMsg[iPosition] == (byte) 0x0A)
						{
							iPosition++;
							break;
						}
						else
							this.PhoneNumber += System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);
					}
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					bConvert[1] = bMsg[iPosition++]; 
					this.CallTime = BitConverter.ToInt32(bConvert, 0);
					for (int X = 0; X < 21; X++)
					{
						if (bMsg[iPosition] == (byte) 0x07)
						{
							iPosition++;
							break;
						}
						else
						{
							this.SimCardNumber += System.Text.ASCIIEncoding.ASCII.GetString(bMsg, iPosition++, 1);
						}
					}
					#endregion
					break;
				case "U":
					#region Status Change Packet
					bConvert = new byte[4];
					bConvert[0] = bMsg[iPosition++]; 
					this.StatusCode = BitConverter.ToInt32(bConvert, 0);
					#endregion
					break;
				case "J":
					#region Job Packet
					while(bMsg[iPosition] != (byte) 0x0A && iPosition < bMsg.Length)
					{
						bTemp = bMsg[iPosition++];
						if (bTemp != (byte) 0x0A)
							this.OrderNumber += (char) bTemp; 
					}
					if(bMsg[iPosition] == (byte) 0x0A)
					{
						iPosition++;
						while(bMsg[iPosition] != (byte) 0x07 && iPosition < bMsg.Length)
						{
							bTemp = bMsg[iPosition++];
							if (bTemp != (byte) 0x07)
								this.CustomerName += (char) bTemp; 
						}
					}
					Console.WriteLine("Job Packet Received");
					#endregion
					break;
				case "V":
					#region Vehicle Usage Packet
					#endregion
					break;
				default :
					break;
			}
			return "";
		}

		private byte[] DecodeSignature(byte[] bData, string sFileName)
		{
			Bitmap oBMP = null;
			byte[] bRet = null;
			byte[] bConvert = null;
			ArrayList oEncodedBytes = new ArrayList();
			ArrayList oDecodedBytes = new ArrayList();
			int iPos = 0;
			int iTop = 0;
			int iLeft = 0;
			int iWidth = 0;
			int iHeight = 0;
			int iWhiteBits = 0;
			int iBlackBits = 0;
			int iState = 0;
			int iMask = 0;
			int iShift = 0;
			int iTemp = 0;
			byte bTemp = (byte) 0x00;
			string sInstruction = "";
			string sColor = "";
			int iLength = 0;
			int iRow = 0;

			try
			{
				#region Read the header information (Top, Left, Height, Width, Black Bits, White Bits)
				// Get the top
				bConvert = new byte[4];
				for (int X = 0; X < 4; X++)
					bConvert[X] = bData[iPos++];
				iTop = BitConverter.ToInt32(bConvert, 0);
				// Get the left
				bConvert = new byte[4];
				for (int X = 0; X < 4; X++)
					bConvert[X] = bData[iPos++];
				iLeft = BitConverter.ToInt32(bConvert, 0);
				// Get the width
				bConvert = new byte[4];
				for (int X = 0; X < 4; X++)
					bConvert[X] = bData[iPos++];
				iWidth = BitConverter.ToInt32(bConvert, 0);
				// Get the height
				bConvert = new byte[4];
				for (int X = 0; X < 4; X++)
					bConvert[X] = bData[iPos++];
				iHeight = BitConverter.ToInt32(bConvert, 0);
				// Get the number of white bits in the encoding
				bConvert = new byte[4];
				bConvert[0] = bData[iPos++];
				bConvert[1] = bData[iPos++];
				iWhiteBits = BitConverter.ToInt32(bConvert, 0);
				// Get the number of black bits in the encoding
				bConvert = new byte[4];
				bConvert[0] = bData[iPos++];
				bConvert[1] = bData[iPos++];
				iBlackBits = BitConverter.ToInt32(bConvert, 0);
				#endregion
				#region Expand the byte array out to a list of bits
				while (iPos < bData.Length)
				{
					bTemp = bData[iPos++];
					for (int X = 7; X >= 0; X--)
					{
						iMask = 1 << X;
						if (((int) bTemp & iMask) == iMask)
							oEncodedBytes.Add(1);
						else
							oEncodedBytes.Add(0);
					}				 
				}
				#endregion
				#region Decode the data to create an array list (oDecodedBytes) of instructions ("B~X" for X black bytes or "W~X" for X white bytes)
				iState = 0;
				for (int X = 0; X < oEncodedBytes.Count; X++)
				{
					switch(iState)
					{
						case 0:
							#region Determine if we are decoding black or white
							if ((int) oEncodedBytes[X] == 0)
							{
								iState = 1;
								iShift = iBlackBits - 1;
							}
							else
							{
								iState = 2;
								iShift = iWhiteBits - 1;
							}
							iTemp = 0;
							#endregion
							break;
						case 1:
							#region Decode Black Bytes
							if (iShift == 0)
							{
								iTemp += (int) oEncodedBytes[X] << iShift;
								oDecodedBytes.Add("B~" + Convert.ToString(iTemp));
								iState = 0;
							}
							else
							{
								iTemp += (int) oEncodedBytes[X] << iShift;
								iShift--;
							}
							#endregion
							break;
						case 2:
							#region Decode White Bytes
							if (iShift == 0)
							{
								iTemp += (int) oEncodedBytes[X] << iShift;
								oDecodedBytes.Add("W~" + Convert.ToString(iTemp));
								iState = 0;
							}
							else
							{
								iTemp += (int) oEncodedBytes[X] << iShift;
								iShift--;
							}
							#endregion
							break;
					}
				}
				#endregion

				if (oDecodedBytes.Count > 0)
				{
                    string sDefaultBMP = System.Configuration.ConfigurationManager.AppSettings["UseBackgroundGIFforSignatures"];
                    if (!sDefaultBMP.Contains(":"))
                    {
                        string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                        path = System.IO.Path.GetDirectoryName(path);
                        sDefaultBMP = path + "\\" + sDefaultBMP;
                    }
					FileStream oFS = new FileStream(sDefaultBMP, FileMode.Open);
					oBMP = (Bitmap) Bitmap.FromStream(oFS);
					oFS.Close();
					oFS = null;
					iPos = iLeft;
					iRow = iTop;
					for (int Z = 0; Z< oDecodedBytes.Count; Z++)
					{
						sInstruction = (string) oDecodedBytes[Z];
						sColor = sInstruction.Split("~".ToCharArray())[0];
						iLength = Convert.ToInt32(sInstruction.Split("~".ToCharArray())[1]);
						if(sColor == "B")
						{
							for (int X = 0; X < iLength; X++)
							{
								if ((iPos - iLeft) > iWidth)
								{
									iRow++;
									iPos = iLeft;
								}
								oBMP.SetPixel(iPos, iRow, Color.Black);
								iPos++;
							}
						}
						else
						{
							for (int X = 0; X < iLength; X++)
							{
								if ((iPos - iLeft) > iWidth)
								{
									iRow++;
									iPos = iLeft;
								}
								iPos++;
							}
						}
					}
					oBMP.Save(sFileName, System.Drawing.Imaging.ImageFormat.Gif);
					oBMP.Dispose();
					oBMP = null;
					oFS = new FileStream(sFileName, FileMode.Open);
					bRet = new byte[oFS.Length];
					oFS.Read(bRet, 0, Convert.ToInt32(oFS.Length));
					oFS.Close();
					oFS = null;
				}
				else
				{
					_log.Info("Blank Signature on Packet.");
					bRet = new byte[1];
					bRet[0] = (byte) 0x00;
				}
			}
			catch(System.Exception ex)
			{
				if(_log != null)
					_log.Error("Error decoding signature", ex);
			}
			return bRet;
		}

		#endregion	

		#region GetPacketTypeName
		public string GetPacketTypeName()
		{
			string sRet = "";

			switch(CommandType)
			{
				case "N":
					sRet = "Interface Ack Packet";
					break;
				case "O":
					sRet = "Login Packet";
					break;
				case "A":
					sRet = "Accept Job Packet";
					break;
				case "X":
					sRet = "Reject Job Packet";
					break;
				case "S":
					sRet = GetAdhocPacketType();
					break;
				case "C":
					sRet = "Complete Job Packet";
					break;
				case "R":
					sRet = GetRequestPacketType();
					break;
				case "P":
					sRet = "Pre-defined Function Packet";
					break;
				case "B":
					sRet = GetNettrackPacketType();
					break;
				case "M":
					sRet = "Message Packet";
					break;
				default :
					sRet = "";
					break;
			}
			return sRet;
		}

		private string GetAdhocPacketType()
		{
			string sRet = "";
			switch(SubCommandType)
			{
				case "P" :
					sRet = "Adhoc Pickup  Packet";
					break;
				case "D" :
					sRet = "Adhoc Delivery Packet";
					break;
				default:
					sRet = "";
					break;
			}
			return sRet;
		}

		private string GetRequestPacketType()
		{
			string sRet = "";
			switch(SubCommandType)
			{
				case "J" :
					sRet = "Request Job Packet";
					break;
				default:
					sRet = "";
					break;
			}
			return sRet;
		}

		private string GetNettrackPacketType()
		{
			string sRet = "";
			switch(SubCommandType)
			{
				case "G" :
					sRet = "Nettrack Generic Data Packet";
					break;
				case "S" :
					sRet = "Nettrack Start Job Packet";
					break;
				case "P" :
					sRet = "Nettrack Picjup Packet";
					break;
				case "D" :
					sRet = "Nettrack Pre-Delivery Packet";
					break;
				case "C" :
					sRet = "Nettrack Complete Job Packet";
					break;
				case "O" :
					sRet = "Nettrack Delivery Packet";
					break;
				default:
					sRet = "";
					break;
			}
			return sRet;
		}
		#endregion

		#region Private Functions
		private static byte[] CreateBadPacketNak()
		{
			byte[] bPacketData = new byte[1];
			byte[] bPacket = new byte[1];
			byte[] bPacketLen = null;
			byte[] bCmdType = null;

			bCmdType = System.Text.ASCIIEncoding.ASCII.GetBytes("N");

			bPacketData[0] = bCmdType[0];
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, Convert.ToInt32((byte) 0x00));
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, "4");
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x00);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x07);
			bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte) 0x03);

			bPacketLen = BitConverter.GetBytes( Convert.ToUInt32(bPacketData.Length));
			bPacket = PacketUtilities.AppendToPacket(bPacket, (byte) bPacketLen[0]);
			bPacket = PacketUtilities.AppendToPacket(bPacket, (byte) bPacketLen[1]);
			bPacket = PacketUtilities.AppendToPacket(bPacket, bPacketData);
			return bPacket;
		}

		private static byte[] GetSubBytes(byte[] bMsg, int iStartAt, int iNumOfBytes)
		{
			byte[] bResult = null;
			int iResultCount = 0;

			if (bMsg == null) return null;

			if (bMsg.Length >= (iStartAt + iNumOfBytes))
			{	

				bResult = new byte[iNumOfBytes];

				for (int X = iStartAt; X < iStartAt + iNumOfBytes; X++)
				{
					bResult[iResultCount] = bMsg[X];
					iResultCount++;
				}
				return bResult;
			}
			else
			{
				iNumOfBytes = bMsg.Length;
				bResult = new byte[iNumOfBytes - iStartAt];
				for (int X = iStartAt; X <iNumOfBytes; X++)


				{
					bResult[iResultCount] = bMsg[X];
					iResultCount++;
				}
				return bResult;
			}
		}

		private static string RemoveLeadingReturnChars(string strData)
		{
			string strTemp = strData.Trim();

			if (strTemp.Length > 0)
			{
				while (strTemp[0] == '\r' || strTemp[0] == '\n') 
				{
					if (strTemp.Length == 1)
					{
						strTemp = "";
						break;
					}
					else
					{
						strTemp = strTemp.Substring(1, strTemp.Length - 1);
					}
				}
			}
			return strTemp;
		}

		private static string WordWrapString(string strDescLines, int iLineLength)
		{
			char[] cDesc = strDescLines.ToCharArray(0, strDescLines.Length);
			string sResult = "";
			string sLine = "";
			int iCurrentPos = 1;

			if (cDesc.Length > 0)
			{
				for(int X = 0; X < cDesc.Length; X++)
				{
					// For each char in the array
					if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
					{
						if (iCurrentPos > 0)
						{
							sResult += sLine + "\n";
							sLine = "";
							iCurrentPos = 0;
						}
						else
						{
							sResult += "\n";
							sLine = "";
							iCurrentPos = 0;
						}
					}
					else
					{
						if (iCurrentPos > iLineLength)
						{
							int iLastSpace = sLine.LastIndexOf(' ');
							if (iLastSpace > 0)
							{
								sResult += sLine.Substring(0, iLastSpace) + "\n";
								sLine = sLine.Substring(iLastSpace + 1).Trim();
								iCurrentPos = sLine.Length;
							}
							else
							{
								sResult += sLine + "\n";
								sLine = "";
								iCurrentPos = 0;
							}
							if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
							{
								if (iCurrentPos > 0)
								{
									sResult += sLine + "\n";
									sLine = "";
									iCurrentPos = 0;
								}
								else
								{
									sResult += "\n";
									sLine = "";
									iCurrentPos = 0;
								}
							}
							else
							{
								if (iCurrentPos == 0 && cDesc[X] == ' ')
								{
									// Don't add white space to the front of the line.
								}
								else
								{
									sLine += cDesc[X];
									iCurrentPos++;
								}
							}
						}
						else
						{
							if (iCurrentPos == 0 && cDesc[X] == ' ')
							{
								// Don't add white space to the front of the line.

							}
							else
							{
								sLine += cDesc[X];
								iCurrentPos++;
							}
						}
					}
				}
			}

			if (sLine.Length > 0)
				sResult += sLine;

			return sResult;
		}

		public static string ConvertToAscii(byte[] bPacket)
		{
			return ConvertToAscii(bPacket, true);
		}

		public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
		{
			string sRet = "";
			int X = 0;
			//			byte bLower = 0x21;	// 33
			//			byte bUpper = 0x7E;  // 126
			byte bTest = 0x00;  // null
			byte[] bTestArray = new byte[1];

			if (bPacket.Length > 0)
			{
				try
				{
					for (X = 0; X<bPacket.Length; X++)
					{
						bTest = bPacket[X];	
						bTestArray = new byte[1];
						bTestArray[0] = bTest;
						if (bTest >= 33 && bTest <= 126 )  // If the character is in printable range
							if (bAddSpacing)
								if (sRet.Length > 0)
									sRet += " - " + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
								else
									sRet = System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
							else
								sRet += System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
						else  // Show it as a number
							if (bAddSpacing)
							if (sRet.Length > 0)
								sRet += " - [" +  bTest + "]";
							else
								sRet = "[" + Convert.ToString(bTest) + "]";
						else
							sRet += "[" + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray) + "]";
					}
					
				}
				catch(System.Exception ex)
				{
					Console.Write(ex.Message + "\n");
				}
			}
			return sRet;
		}

		private static int FindFirstIndexOfByte(string sData, byte bFind)
		{
			return FindFirstIndexOfByte(sData, bFind, 0, sData.Length);
		}

		private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt)
		{
			return FindFirstIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
		}

		private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
		{
			int iRet = -1;

			try
			{
				iRet = sData.IndexOf((char) bFind + "", iStartAt, iLength);
			}
			catch(System.Exception)
			{
				return -1;
			}
			return iRet;
		}

		private static int FindLastIndexOfByte(string sData, byte bFind)
		{
			return FindLastIndexOfByte(sData, bFind, sData.Length, sData.Length);
		}

		private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt)
		{
			return FindLastIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
		}

		private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
		{
			int iRet = -1;

			try
			{
				iRet = sData.LastIndexOf((char) bFind + "", iStartAt, iLength);
			}
			catch(System.Exception)
			{
				return -1;
			}
			return iRet;
		}


		private static string TruncateString(string sData, int iPos)
		{
			string sRet = "";
			
			try
			{
				if (sData.Length < iPos + 1)
					return "";
				else
					sRet = sData.Substring((iPos + 1), sData.Length - (iPos + 1));
			}
			catch(System.Exception)
			{
				return "";
			}
			return sRet;
		}
		#endregion	
	}
}

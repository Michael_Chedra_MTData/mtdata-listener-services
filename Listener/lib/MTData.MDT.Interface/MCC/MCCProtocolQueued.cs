using System;
using System.Collections;
using System.Threading;
using MTData.Transport.Core;

namespace MTData.MDT.Interface.MCC
{
	/// <summary>
	/// Queued protocol handler will listen to queue inbound,
	/// parsing packets, and writing responses to outqueue
	/// NOTE : The PacketFound event will be raised from, and will run in the _fromMCCThread.
	/// If there is a requirement to dissassociate these, then it must be dealt with in the 
	/// listening class.
	/// </summary>
	public class MCCProtocolQueued
	{
		/// <summary>
		/// This is the parser that will take consecutive byte arrays and convert them into 
		/// packets of the correct length.
		/// </summary>
		private MCCProtocolParser _parser = null; 

		/// <summary>
		/// This will take packets of data and wrap them in the MCC Protocol.
		/// </summary>
		private MCCProtocolWrapper _wrapper = null;

		/// <summary>
		/// This is the queue of data inbound from the MCC.. it will 
		/// </summary>
		private QueueInterface _fromMCCQueue;

		/// <summary>
		/// This is the queue of data going to the MCC.
		/// </summary>
		private QueueInterface _toMCCQueue;

		/// <summary>
		/// This is the thread that will respond to inbound byte[] from MCC, and
		/// pass them on to the MCCProtocolParser
		/// </summary>
		private QueueReaderThread _fromMCCThread;

		/// <summary>
		/// This is the logger that errors and message will be passed to.
		/// </summary>
		private LoggingInterface _logger = null;

		/// <summary>
		/// The constructor will initialize the appliation for use. The only other requirement is to call Start;
		/// </summary>
		/// <param name="logger"></param>
		/// <param name="fromMCCQueue"></param>
		/// <param name="toMCCQueue"></param>
		public MCCProtocolQueued(LoggingInterface logger, QueueInterface fromMCCQueue, QueueInterface toMCCQueue)
		{
			_logger = logger;
			if (logger == null)
				throw new ApplicationException("MCCProtocolQueued requires a valid logger instance");
			_logger.LogInformation("MCC Protocol Parser : Initialising");

			if (fromMCCQueue == null)
				throw new ApplicationException("MCCProtocolQueued requires a valid fromMCCQueue instance");

			if (toMCCQueue == null)
				throw new ApplicationException("MCCProtocolQueued requires a valid toMCCQueue instance");

			_parser = new MCCProtocolParser();
			_parser.PacketFound += new MTData.MDT.Interface.MCC.MCCProtocolParser.PacketFoundDelegate(_parser_PacketFound);
			_parser.PacketError += new MTData.MDT.Interface.MCC.MCCProtocolParser.PacketErrorDelegate(_parser_PacketError);
			
			_wrapper = new MCCProtocolWrapper();
			
			_fromMCCQueue = fromMCCQueue;
			_toMCCQueue = toMCCQueue;

			_fromMCCThread = new QueueReaderThread("From MCC Queue Reader", _fromMCCQueue, 10, 100);
			_fromMCCThread.DataFound += new MTData.Transport.Core.QueueReaderThread.DataFoundDelegate(_fromMCCThread_DataFound);
			_fromMCCThread.ThreadException += new MTData.Transport.Core.QueueReaderThread.ThreadExceptionDelegate(_fromMCCThread_ThreadException);
			_fromMCCThread.ThreadStateChanged += new MTData.Transport.Core.QueueReaderThread.ThreadStateChangedDelegate(_fromMCCThread_ThreadStateChanged);
			
			_logger.LogInformation("MCC Protocol Parser : Initialised");

		}

		#region Private Methods and Event handlers

		/// <summary>
		/// Process the data passed. this should be a byte[].
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="data"></param>
		private void _fromMCCThread_DataFound(object sender, object data)
		{
			byte[] packet = (byte[])data;

			_logger.LogInformation("MCC Protocol Parser : Processing Bytes : " + MTData.Transport.Core.Utilities.EncodeByteArrayAsHex(packet, packet.Length));

			_parser.ProcessBytes(packet);
		}

		/// <summary>
		/// This indicates that an exception occurred within the reader.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="ex"></param>
		private void _fromMCCThread_ThreadException(object sender, Exception ex)
		{
			_logger.LogInformation(string.Format("MCC Protocol Parser : Reader Error : {0} : {1}",
				ex.Message, ex.StackTrace));
		}

		/// <summary>
		/// This indicates that the state of the thread has changed.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="newState"></param>
		private void _fromMCCThread_ThreadStateChanged(object sender, MTData.Transport.Core.QueueReaderThread.ReaderThreadState newState)
		{
			_logger.LogInformation(string.Format("MCC Protocol Parser : {0}", newState));
		}

		/// <summary>
		/// This indicates that a valid packet was found, and the packet is the payload of that.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="packet"></param>
		private void _parser_PacketFound(object sender, byte[] packet)
		{
			_logger.LogInformation("MCC Protocol Parser : Packet Found : " + MTData.Transport.Core.Utilities.EncodeByteArrayAsHex(packet, packet.Length));
			if (PacketFound != null)
				PacketFound(this, packet);
		}

		/// <summary>
		/// This handles that an error has occurred, and logs it, and passes it on.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="currentBuffer"></param>
		/// <param name="message"></param>
		private void _parser_PacketError(object sender, byte[] currentBuffer, string message)
		{
			_logger.LogInformation(string.Format("MCC Protocol Parser : Parser Error : {0} : {1}",
				message, 
				MTData.Transport.Core.Utilities.EncodeByteArrayAsHex(currentBuffer, currentBuffer.Length)));

			if (PacketError != null)
				PacketError(this, currentBuffer, message);
		}

		#endregion

		#region Event Definitions

		/// <summary>
		/// This event will be raised when a valid packet is found in the recombined inbound byte[] queue.
		/// </summary>
		public event MCCProtocolParser.PacketFoundDelegate PacketFound;

		/// <summary>
		/// This event will be raised whenever an error occurs in the packet parsing.
		/// </summary>
		public event MCCProtocolParser.PacketErrorDelegate PacketError;

		#endregion

		#region Public Methods

		/// <summary>
		/// Start the thread listening.
		/// </summary>
		public void Start()
		{
			_fromMCCThread.Start();
		}

		/// <summary>
		/// Stop the thread form listening.
		/// </summary>
		public void Stop()
		{
			_fromMCCThread.Stop();
		}

		/// <summary>
		/// This method will take a packet and wrap it in the MCC protocol. It will then place it on the 
		/// toMCCQueue to enable it to be transmitted.
		/// </summary>
		/// <param name="packet">PAcket to be sent as payload</param>
		public void WritePacket(byte[] packet)
		{
			byte[] wrappedPacket = _wrapper.Encode(packet);
			lock(_toMCCQueue.SyncRoot)
				_toMCCQueue.Enqueue(wrappedPacket);
		}

		/// <summary>
		/// Indicates if the thread is still active.
		/// </summary>
		public bool Running
		{
			get{ return _fromMCCThread.Running; }
		}

		/// <summary>
		/// Indicates if the thread is still active.
		/// </summary>
		public bool Stopping
		{
			get{ return _fromMCCThread.Stopping; }
		}

		#endregion
	}
}

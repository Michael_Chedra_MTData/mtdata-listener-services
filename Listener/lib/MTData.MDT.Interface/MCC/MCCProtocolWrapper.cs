using System;

namespace MTData.MDT.Interface.MCC
{
	/// <summary>
	/// This class will wrap any byte array data with the 
	/// details required for the MCC Protocol
	/// </summary>
	public class MCCProtocolWrapper
	{
		public MCCProtocolWrapper()
		{
			
		}

		/// <summary>
		/// Wrap the data with the relevant data from the 
		/// Header details specified.
		/// Packet format = [SOP][Length][Data][EOP]
		/// [SOP] = 0x0A
		/// [Length] = 4 Bytes
		/// [Data] = Var length data
		/// [EOP] = 0x0C
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public byte[] Encode(byte[] data)
		{
			int length = data.Length; 
			int bufferLength = length + 6; // SOP, EOP, Length
			
			byte[] buffer = new byte[bufferLength];

			int index = 0;
			buffer[index++] = MCCProtocolConstants.SOP;
			MTData.Utilities.PacketUtilities.Write4ByteNumberToBuffer(buffer, length, index);
			index+= 4;
			Array.Copy(data, 0, buffer, index, data.Length);
			index+= data.Length;
			buffer[index] = MCCProtocolConstants.EOP;

			return buffer;
		}

	}
}

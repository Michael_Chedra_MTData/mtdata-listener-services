using System;

namespace MTData.MDT.Interface.MCC
{
	/// <summary>
	/// This class will process byte arrays, parsing them to produce
	/// valid MCC packets.
	/// </summary>
	public class MCCProtocolParser
	{
		/// <summary>
		/// this is the state that identifies where the parser is in the current
		/// byte array.
		/// </summary>
		private enum MCCParserState
		{
			SeekingStart,
			ReadingPacketLength,
			ReadingData
		}

		/// <summary>
		/// This is the increment by which the buffer is expanded if more 
		/// space is required.
		/// </summary>
		private const int BUFFER_SIZE_INCREMENT = 1024;

		/// <summary>
		/// this is the state of the parser.
		/// </summary>
		private MCCParserState _parserState = MCCParserState.SeekingStart;

		/// <summary>
		/// This is the buffer that contains the parsed packet
		/// </summary>
		private byte[] _currentPacketBuffer = null;

		/// <summary>
		/// Position of next insert into currentPacketBuffer
		/// </summary>
		private int _nextbufferInsert = 0;

		/// <summary>
		/// Buffer for holding the length of the data.
		/// </summary>
		private byte[] _packetLengthBuffer = new byte[4];

		/// <summary>
		/// this is the index into the Packet Length buffer for the next 
		/// length byte
		/// </summary>
		private int _nextPacketLengthInsert = 0;

		/// <summary>
		/// This is the packet length after the packet has been parsed.
		/// </summary>
		private int _parsedPacketLength = 0;

		/// <summary>
		/// default constructor
		/// </summary>
		public MCCProtocolParser()
		{
			
		}

		/// <summary>
		/// Definition of the PacketFound event
		/// </summary>
		public delegate void PacketFoundDelegate(object sender, byte[] packet);

		/// <summary>
		/// This delegate identifies the format for an error delegate
		/// </summary>
		public delegate void PacketErrorDelegate(object sender, byte[] currentBuffer, string message);

		/// <summary>
		/// Event registration for the PacketFound event
		/// </summary>
		public event PacketFoundDelegate PacketFound;

		/// <summary>
		/// this event handler will notify listeners of errors in the packet data
		/// </summary>
		public event PacketErrorDelegate PacketError;

		/// <summary>
		/// Reset the parser to the start of the cycle.
		/// </summary>
		public void Reset()
		{
			_parserState = MCCParserState.SeekingStart;
		}

		/// <summary>
		/// Process the byte array data fragment. If the parser matches up a full
		/// packet, parse it and raise an event.
		/// Packet format = [SOP][Length][Unit ID][Sep][Job ID][Sep][Data][EOP]
		/// [SOP] = 0x0A
		/// [Length] = 4 Bytes
		/// [Unit ID] = 4 Bytes
		/// [Sep] = 0x0B
		/// [Job ID] = 4 Bytes
		/// [Sep] = 0x0B
		/// [Data] = Var length data
		/// [EOP] = 0x0C
		/// Length includes [Unit ID][Sep][Job ID][Sep][Data]
		/// </summary>
		/// <param name="buffer"></param>
		public void ProcessBytes(byte[] inBuffer)
		{
			for(int loop = 0; loop < inBuffer.Length; loop++)
			{
				switch(_parserState)
				{
					case MCCParserState.SeekingStart :
					{
						//	Keep looking for the start of the packet..
						if (inBuffer[loop] == MCCProtocolConstants.SOP)
						{
							_nextbufferInsert = 0;
							_parserState = MCCParserState.ReadingPacketLength;
							_nextPacketLengthInsert = 0;
						}
						break;
					}
					case MCCParserState.ReadingPacketLength :
					{
						_packetLengthBuffer[_nextPacketLengthInsert++] = inBuffer[loop];
						if (_nextPacketLengthInsert >= 4)
						{
							_parsedPacketLength = BitConverter.ToInt32(_packetLengthBuffer, 0);
							if (_parsedPacketLength > 3000)
							{
								OnPacketError(inBuffer, "Invalid Packet Received from Buffer");
								Reset();
							}
							else
							{
								if (_parsedPacketLength > 0)
								{
									EnsureBufferSize(_parsedPacketLength);
									_parserState = MCCParserState.ReadingData;
								}
								else
									Reset();
							}
						}
						break;
					}
					case MCCParserState.ReadingData :
					{
						_currentPacketBuffer[_nextbufferInsert++] = inBuffer[loop];

						//  if the end point is found.. convert the buffer..
						//	NOTE : We want to include the EOP flag, but it is not
						//	included in the packet length...
						if (_nextbufferInsert == (_parsedPacketLength + 1))
						{
							//	Notify of packet
							OnPacketFound();
							
							//  switch back to seeking start
							Reset();
						}
						break;
					}
				}
			}
		}

		/// <summary>
		/// This method will be called ot ensure the buffer is the correct length
		/// </summary>
		/// <param name="requiredSize"></param>
		private void EnsureBufferSize(int requiredSize)
		{
			int currentLength = (_currentPacketBuffer != null)?_currentPacketBuffer.Length:0;
			int newSize = currentLength;

			while (requiredSize > newSize)
				newSize += BUFFER_SIZE_INCREMENT;

			if (newSize > currentLength)
			{
				byte[] temp = new byte[newSize];
				if (_currentPacketBuffer != null)
					Array.Copy(_currentPacketBuffer, 0, temp, 0, _currentPacketBuffer.Length);
				_currentPacketBuffer = temp;
			}
		}

		/// <summary>
		/// if anyone is listening to the event, notify them now.
		/// </summary>
		/// <param name="currentBuffer"></param>
		/// <param name="message"></param>
		protected virtual void OnPacketError(byte[] currentBuffer, string message)
		{
			if (PacketError != null)
				PacketError(this, currentBuffer, message);
		}

		/// <summary>
		/// when a packet is found, cpopy it into an independent array, and
		/// notify any listeners of it.
		/// </summary>
		protected virtual void OnPacketFound()
		{
			if (PacketFound != null)
			{
				byte[] packet = new byte[_parsedPacketLength];
				Array.Copy(_currentPacketBuffer, 0, packet, 0, _parsedPacketLength);

				PacketFound(this, packet);
			}
		}
	}
}

using System;

namespace MTData.MDT.Interface.MCC
{
	/// <summary>
	/// this class groups constants together for use in the MCC protocol.
	/// </summary>
	public class MCCProtocolConstants
	{
		/// <summary>
		/// SOP constant
		/// </summary>
		public const byte SOP = (byte)0x0A;

		/// <summary>
		/// EOP constant
		/// </summary>
		public const byte EOP = (byte)0x0C;

		public MCCProtocolConstants()
		{
			
		}
	}
}

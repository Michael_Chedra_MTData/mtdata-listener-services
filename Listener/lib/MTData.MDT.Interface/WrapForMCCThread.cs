using System;
using System.Collections;
using System.Threading;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;
using MTData.Common.Network;
using MTData.Transport.GenericProtocol;
using MTData.Common.Config;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// This class 
    ///		1. Accepts inbound c5040PacketData objects
    ///		2. Creates the 5040 packet and wraps it in the MCC protocol layer.
    ///		3. Puts them on the _mccOutbound queue to be sent to the MCC layer.
    /// </summary>
    public class WrapForMCCThread
    {
        private int _bufferSize;
        private QueueInterface _mccOutbound;
        private QueueInterface _toMobileUnitsQ;
        private ILog _log = LogManager.GetLogger(typeof(WrapForMCCThread));
        private EnhancedThread _thread;
        private string _TranslatorType = "";															// The name of the translator object in use.

        public WrapForMCCThread(
            int bufferSize,
            string TranslatorType,
            QueueInterface toMobileUnitsQ,
            QueueInterface mccOutbound,
            ConvertSignatureConfig signatureConfig)
        {
            _bufferSize = bufferSize;
            _TranslatorType = TranslatorType;
            _toMobileUnitsQ = toMobileUnitsQ;
            _mccOutbound = mccOutbound;
            

            _thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
            _thread.Name = "MCC Packet Wrapping";
            _thread.Start();
        }

        private object ThreadHandler(EnhancedThread sender, object data)
        {
            _log.Info("MCC connection wrapper started");
            try
            {
                while (!sender.Stopping)
                {
                    object entry = null;
                    byte[] bData = null;
                    byte[] bMobileID = new byte[4];
                    byte[] bJobID = new byte[4];
                    object[] oMultiPackets = null;

                    lock (_toMobileUnitsQ.SyncRoot)
                        if (_toMobileUnitsQ.Count > 0)
                        {
                            entry = _toMobileUnitsQ.Dequeue();
                        }

                    //	Append piece of packet to 
                    if (entry == null)
                    {
                        Thread.Sleep(10);
                    }
                    else
                    {
                        if (entry != null)
                        {
                            _log.Info("Sending to MCC : Processing Data: ");

                            //check if entry is a device layer (new Protocol)
                            DeviceLayer deviceLayer = entry as DeviceLayer;
                            if (deviceLayer != null)
                            {
                                bData = deviceLayer.GetBytes();
                                if (bData != null && bData.Length > 0)
                                {
                                    lock (_mccOutbound.SyncRoot)
                                    {
                                        _mccOutbound.Enqueue(bData);
                                    }
                                }
                            }
                            else // old protocol
                            {
                                // Build the 5040 packet
                                //	Translate into a host Interface packet, and send on its way
                                #region Raw Translator
                                if (_TranslatorType == "Raw")
                                {
                                    try
                                    {
                                        int iMobileID = 0;
                                        byte[] bConvert = new byte[4];

                                        bData = (byte[])entry;
                                        bConvert[0] = bData[0];
                                        bConvert[1] = bData[1];
                                        bConvert[2] = bData[2];
                                        bConvert[3] = bData[3];
                                        iMobileID = BitConverter.ToInt32(bConvert, 0);

                                        bConvert = new byte[bData.Length - 4];
                                        for (int X = 4; X < bData.Length; X++)
                                        {
                                            bConvert[X - 4] = bData[X];
                                        }

                                        if (bData != null) bData = ThreadCreateMCCPacket(iMobileID, 0, bConvert);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping Raw Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region 5040 Translator
                                if (_TranslatorType == "5040")
                                {
                                    try
                                    {
                                        bData = ((c5040PacketData)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((c5040PacketData)entry).iMobileID, ((c5040PacketData)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping 5040PacketData Packet for MCC", ex);
                                    }
                                }
                                else if (_TranslatorType == "Camerons")
                                {
                                    try
                                    {
                                        bData = ((cCameronsPacketData)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((cCameronsPacketData)entry).iMobileID, ((cCameronsPacketData)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping CameronsPacketData Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region Downunder
                                if (_TranslatorType == "DownUnder")
                                {
                                    try
                                    {
                                        if (!entry.GetType().Equals(typeof(MTData.MDT.Interface.cDownUnderPacketData)))
                                        {
                                            cDownUnderPacketData oPData = new cDownUnderPacketData( null, true);
                                            bData = oPData.ParseIncommingData((byte[])entry);
                                            if (bData != null) bData = ThreadCreateMCCPacket(oPData.iMobileID, oPData.EventID, bData);
                                        }
                                        else
                                        {
                                            bData = ((cDownUnderPacketData)entry).BuildOutgoingPacket();
                                            if (bData != null) bData = ThreadCreateMCCPacket(((cDownUnderPacketData)entry).iMobileID, ((cDownUnderPacketData)entry).EventID, bData);
                                        }
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping DownunderPacketData Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region MSTP
                                if (_TranslatorType == "MSTP")
                                {
                                    try
                                    {
                                        bData = ((cMSTPPacketData)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((cMSTPPacketData)entry).iMobileID, ((cMSTPPacketData)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping MSTP Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region Bunkers
                                if (_TranslatorType == "Bunkers")
                                {
                                    try
                                    {
                                        bData = ((IExternalMessage)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(Convert.ToInt32(((IExternalMessage)entry).UnitID), ((IExternalMessage)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping BunkersPacketData Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region Rinkers Quarry
                                if (_TranslatorType == "Rinkers.Quarry")
                                {
                                    try
                                    {
                                        bData = ((cRinkersQuarryPacketData)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((cRinkersQuarryPacketData)entry).iMobileID, ((cRinkersQuarryPacketData)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping Rinkers Quarry Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region Yellow.Couriers
                                if (_TranslatorType == "Yellow.Couriers")
                                {
                                    try
                                    {
                                        bData = ((cYellowCouriersPacketData)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((cYellowCouriersPacketData)entry).iMobileID, ((cYellowCouriersPacketData)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("WrappingYellow Couriers Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region WinCE.Dialler
                                if (_TranslatorType == "WinCE.Dialler")
                                {
                                    try
                                    {
                                        bData = ((cWinCEDiallerPacketData)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((cWinCEDiallerPacketData)entry).iMobileID, ((cWinCEDiallerPacketData)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping WinCE Dialler Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region MTData.Logistics
                                if (_TranslatorType == "MTData.Logistics")
                                {
                                    try
                                    {
                                        oMultiPackets = ((cLogisticsPacketData)entry).BuildOutgoingPacket();
                                        for (int iPackets = 0; iPackets < oMultiPackets.Length; iPackets++)
                                        {
                                            bData = (byte[])oMultiPackets[iPackets];
                                            if (bData != null)
                                            {
                                                bData = ThreadCreateMCCPacket(((cLogisticsPacketData)entry).MobileID, ((cLogisticsPacketData)entry).JobID, bData);
                                                if (bData.Length > 0)
                                                    lock (_mccOutbound.SyncRoot)
                                                        _mccOutbound.Enqueue(bData);
                                            }
                                        }
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping MTData Logistics Packet for MCC", ex);
                                    }
                                }
                                #endregion
                                #region WebInterfaceTerminal
                                if (_TranslatorType == "K.S_Dispatcher")
                                {
                                    try
                                    {
                                        bData = ((TerminalInterfacePacketProcessor)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((TerminalInterfacePacketProcessor)entry).iMobileID, ((TerminalInterfacePacketProcessor)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping K&S Web interface terminal packet", ex);
                                    }
                                }
                                #endregion
                                #region Asset Forestry
                                if (_TranslatorType == "AssetForestry_Dispatcher")
                                {
                                    try
                                    {
                                        bData = ((AssetForestry_PacketProcessor)entry).BuildOutgoingPacket();
                                        if (bData != null) bData = ThreadCreateMCCPacket(((AssetForestry_PacketProcessor)entry).iMobileID, ((AssetForestry_PacketProcessor)entry).JobID, bData);
                                    }
                                    catch (System.Exception ex)
                                    {
                                        _log.Error("Wrapping Asset Forestry terminal packet", ex);
                                    }
                                }
                                #endregion
                                if (_TranslatorType != "MTData.Logistics")
                                {
                                    // If we have something to send, the queue it on the _mccOutbound queue.
                                    if (bData != null)
                                    {
                                        if (bData.Length > 0)
                                            lock (_mccOutbound.SyncRoot)
                                                _mccOutbound.Enqueue(bData);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                _log.Info("Interface to MCC converter Stopped");
            }
            return null;
        }

        /// <summary>
        /// Take a standard integer and convert it into a 4 byte array
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private byte[] ConvertIntto4ByteArray(int newValue)
        {
            byte[] temp = BitConverter.GetBytes(newValue);
            byte[] result = new byte[4];
            result[0] = (temp.Length > 0) ? temp[0] : (byte)0;
            result[1] = (temp.Length > 1) ? temp[1] : (byte)0;
            result[2] = (temp.Length > 2) ? temp[2] : (byte)0;
            result[3] = (temp.Length > 3) ? temp[3] : (byte)0;
            return result;
        }

        /// <summary>
        /// Take a standard long and convert it into a 4 byte array
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private byte[] ConvertLongto4ByteArray(long newValue)
        {
            byte[] temp = BitConverter.GetBytes(newValue);
            byte[] result = new byte[4];
            result[0] = (temp.Length > 0) ? temp[0] : (byte)0;
            result[1] = (temp.Length > 1) ? temp[1] : (byte)0;
            result[2] = (temp.Length > 2) ? temp[2] : (byte)0;
            result[3] = (temp.Length > 3) ? temp[3] : (byte)0;
            return result;
        }

        public void Stop()
        {
            _thread.Stop();
        }

        /// <summary>
        /// Create the MCC packet for queing.
        /// </summary>
        /// <param name="mobileId"></param>
        /// <param name="jobId"></param>
        /// <param name="translatedData"></param>
        /// <returns></returns>
        private byte[] ThreadCreateMCCPacket(int mobileId, long jobId, byte[] translatedData)
        {
            byte[] bSendPacket = null;
            byte[] bIntegerBuffer = null;
            byte[] bData = null;
            int iLen = 0;
            int X = 0;

            try
            {
                // If the TCP connection to the MCC server is established, then use it.  Otherwise use the DB interface.
                bData = translatedData;

                // Create the keep alive packet.
                // Packet format = [SOP][Length][Unit ID][Sep][Job ID][Sep][Data][EOP]
                // [SOP] = 0x0A
                // [Length] = 4 Bytes
                // [Unit ID] = 4 Bytes
                // [Sep] = 0x0B
                // [Job ID] = 4 Bytes
                // [Sep] = 0x0B
                // [Data] = Var length data
                // [EOP] = 0x0C
                // Length includes [Unit ID][Sep][Job ID][Sep][Data]

                if (bData == null)
                {
                    bData = new byte[1];
                    bData[0] = (byte)0x00;
                }

                bSendPacket = new byte[16 + bData.Length];
                iLen = 0;

                // Create the packet.
                X = 0;
                bSendPacket[X++] = (byte)0x0A;		 // SOP
                bSendPacket[X++] = (byte)0x00;		// Length 1
                bSendPacket[X++] = (byte)0x00;		// Length 2
                bSendPacket[X++] = (byte)0x00;		// Length 3
                bSendPacket[X++] = (byte)0x00;		// Length 4

                bIntegerBuffer = ConvertIntto4ByteArray(mobileId);
                bSendPacket[X++] = bIntegerBuffer[0];			// Unit ID 1
                bSendPacket[X++] = bIntegerBuffer[1];			// Unit ID 2
                bSendPacket[X++] = bIntegerBuffer[2];			// Unit ID 3
                bSendPacket[X++] = bIntegerBuffer[3];			// Unit ID 4
                bSendPacket[X++] = (byte)0x0B;		// Sep

                bIntegerBuffer = ConvertLongto4ByteArray(jobId);
                bSendPacket[X++] = bIntegerBuffer[0];			// Job ID 1
                bSendPacket[X++] = bIntegerBuffer[1];			// Job ID 2
                bSendPacket[X++] = bIntegerBuffer[2];			// Job ID 3
                bSendPacket[X++] = bIntegerBuffer[3];			// Job ID 4
                bSendPacket[X++] = (byte)0x0B;		// Sep

                for (int Y = 0; Y < bData.Length; Y++)
                    bSendPacket[X++] = bData[Y];			// Data

                bSendPacket[X++] = (byte)0x0C;		// EOP						

                iLen = X - 6;
                bIntegerBuffer = BitConverter.GetBytes(iLen);

                // Populate the length field.
                bSendPacket[1] = bIntegerBuffer[0];
                bSendPacket[2] = bIntegerBuffer[1];
                bSendPacket[3] = bIntegerBuffer[2];
                bSendPacket[4] = bIntegerBuffer[3];

            }
            catch (System.Exception ex)
            {
                _log.Error(String.Format("Interface Packet Error : Generate MCC Packet : {0}", X), ex);
                bSendPacket = null;
            }
            return bSendPacket;
        }
    }
}

using System;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// Summary description for cClientData.
	/// </summary>
	public class cClientData
	{
		public int Key = 0;
		public byte[] bData = null;

		public cClientData(int iKey, byte[] oData)
		{
			Key = iKey;
			bData = oData;
		}
	}
}

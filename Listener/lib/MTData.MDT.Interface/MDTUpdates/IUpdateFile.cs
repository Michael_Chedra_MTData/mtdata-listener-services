using System;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates
{
    /// <summary>
    /// interface for an update file
    /// </summary>
    public interface IUpdateFile
    {
        /// <summary>
        /// if this update is a firmware update for 5010 or 5040, the MOT file to update
        /// </summary>
        cMotFile MotFile
        {
            get;
        }
        /// <summary>
        /// if this update is a WinCE or NEC update for the 5080, the binary file to update
        /// </summary>
        cBinaryFile BinaryFile
        {
            get;
        }
        /// <summary>
        /// the major version number for this update
        /// </summary>
        int MajorVersion
        {
            get;
        }
        /// <summary>
        /// the minor version number for this update
        /// </summary>
        int MinorVersion
        {
            get;
        }

        /// <summary>
        /// does this file require a forced download
        /// </summary>
        bool ForceDownload
        {
            get;
        }

        /// <summary>
        /// Does this file require a force logout once the download is complete
        /// </summary>
        bool ForceLogout
        {
            get;
        }

    }
}

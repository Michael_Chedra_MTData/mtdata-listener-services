using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.MDT.Interface.MDTUpdates.XMLUpdate
{
    /// <summary>
    /// class containing the details of an update for a MDT
    /// </summary>
    public class XMLUpdateMDT: IUpdateMDT
    {
        #region private fields
        /// <summary>
        /// force download
        /// </summary>
        private bool _forceDownload;
        /// <summary>
        /// dictionary of fleets which are to run a different version
        /// key = fleet's id
        /// value = the UpdateFleet for this fleet
        /// </summary>
        private Dictionary<int, XMLUpdateFleet> _fleets;
        /// <summary>
        /// the defualt update file
        /// </summary>
        private IUpdateFile _defaultUpdate;
        #endregion

        #region IUpdateMDT Members
        public DownloadType DownloadType { get; private set; }
        /// <summary>
        /// force download
        /// </summary>
        public bool ForceDownload
        {
            get { return _forceDownload; }
        }
        /// <summary>
        /// get the update for a specfic fleet and vehcile
        /// </summary>
        /// <param name="mobileId">the mobile id of the unit</param>
        /// <returns>the Update file for this mobile id</returns>
        public IUpdateFile GetUpdateFile(int mobileId)
        {
            //convert the mobile id into fleet and vehicle
            int fleetId = (int)mobileId / 256;
            int vehicleId = mobileId % 256;
            return GetUpdateFile(fleetId, vehicleId);
        }

        public bool SupportsMultipleDownloads
        {
            get
            {
                return false;
            }
        }
        #endregion

        #region constructor
        public XMLUpdateMDT(IUpdateFile updateFile, bool forceDownload, DownloadType downloadType)
        {
            _defaultUpdate = updateFile;
            _forceDownload = forceDownload;
            DownloadType = downloadType;
            _fleets = new Dictionary<int, XMLUpdateFleet>();
        }
        #endregion

        #region public methods
        /// <summary>
        /// add an update for a specfic fleet
        /// </summary>
        /// <param name="updateFile">the update</param>
        /// <param name="fleetId">the fleet id</param>
        public void AddFleet(XMLUpdateFleet updateFleet, int fleetId)
        {
            _fleets.Add(fleetId, updateFleet);
        }

        /// <summary>
        /// get the update for a specfic fleet and vehcile
        /// </summary>
        /// <param name="fleetId">the fleet id</param>
        /// <param name="vehicleId">the vehicle's id</param>
        /// <returns>the Update file for this fleet and vehicle, if fleet is not defined return the defualt update file</returns>
        public IUpdateFile GetUpdateFile(int fleetId, int vehicleId)
        {
            if (_fleets.ContainsKey(fleetId))
            {
                return _fleets[fleetId].GetUpdateFileForVehicle(vehicleId);
            }
            //return the default update for this mdt
            return _defaultUpdate;
        }

        public List<IUpdateFile> GetUpdates(int fleetId, int vehicleId)
        {
            List<IUpdateFile> updates = new List<IUpdateFile>();
            updates.Add(GetUpdateFile(fleetId, vehicleId));
            return updates;
        }
        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.MDT.Interface.MDTUpdates.XMLUpdate
{
    /// <summary>
    /// contains the update file for a fleet and any different files for specfic vehicles
    /// </summary>
    public class XMLUpdateFleet
    {
        #region private fields
        /// <summary>
        /// the update file for this fleet
        /// </summary>
        private IUpdateFile _updateFile;
        /// <summary>
        /// the fleet's id
        /// </summary>
        private int _fleetId;
        /// <summary>
        /// dictionary of vehicles which are to run a different version
        /// key = vehicle's id
        /// value = the UpdateFile for this vehcile
        /// </summary>
        private Dictionary<int, IUpdateFile> _vehicles;
        #endregion

        #region constructor
        /// <summary>
        /// Create a new update fleet
        /// </summary>
        /// <param name="updateFile">the update file for this fleet</param>
        /// <param name="fleetId">the fleet id</param>
        public XMLUpdateFleet(IUpdateFile updateFile, int fleetId)
        {
            _updateFile = updateFile;
            _fleetId = fleetId;
            _vehicles = new Dictionary<int, IUpdateFile>();
        }
        #endregion

        #region public methods
        /// <summary>
        /// add an update for a specfic vehicle
        /// </summary>
        /// <param name="updateFile">the update</param>
        /// <param name="vehicleId">the vehicle id</param>
        public void AddVehicle(IUpdateFile updateFile, int vehicleId)
        {
            _vehicles.Add(vehicleId, updateFile); 
        }

        /// <summary>
        /// get the update for a specfic vehicle
        /// </summary>
        /// <param name="vehicleId">the vehicle's id</param>
        /// <returns>the Update file for this vehicle, if vehicle is not defined return the fleet's update file</returns>
        public IUpdateFile GetUpdateFileForVehicle(int vehicleId)
        {
            if (_vehicles.ContainsKey(vehicleId))
            {
                return _vehicles[vehicleId];
            }
            //return the default update for this fleet
            return _updateFile;
        }
        #endregion
    }
}

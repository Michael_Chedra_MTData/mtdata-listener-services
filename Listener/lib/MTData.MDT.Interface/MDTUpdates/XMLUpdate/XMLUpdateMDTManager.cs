using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using log4net;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates.XMLUpdate
{
    /// <summary>
    /// contains details for all the different updates
    /// </summary>
    public class XMLUpdateMDTManager : IUpdateMDTManager
    {
        #region private fields
        private ILog _log = LogManager.GetLogger(typeof(XMLUpdateMDTManager));
        /// <summary>
        /// the dictionary of binary files loaded
        /// </summary>
        private Dictionary<string, cBinaryFile> _binaryFiles;
        /// <summary>
        /// the dictionary of MOT files loaded
        /// </summary>
        private Dictionary<string, cMotFile> _motFiles;
        /// <summary>
        /// the updates for WinCE
        /// </summary>
        private IUpdateMDT _winCE;
        /// <summary>
        /// the updates for the NEC on the 5080
        /// </summary>
        private IUpdateMDT _nec;
        /// <summary>
        /// the updates for firmware on 5040
        /// </summary>
        private IUpdateMDT _firmware5040;
        /// <summary>
        /// the updates for firmware on 5010
        /// </summary>
        private IUpdateMDT _firmware5010;
        /// <summary>
        /// the updates for firmware on 4000
        /// </summary>
        private IUpdateMDT _firmware4000;
        /// <summary>
        /// the updates for shell configuration file
        /// </summary>
        private IUpdateMDT _shellConfiguration;
        /// <summary>
        /// the updates for talon configuration file
        /// </summary>
        private IUpdateMDT _talonConfiguration;
        private IUpdateMDT _talon;
        private IUpdateMDT _swift;
        #endregion

        #region IUpdateMDTManager Members
        /// <summary>
        /// the updates for WinCE
        /// </summary>
        public IUpdateMDT WinCE
        {
            get { return _winCE; }
        }
        /// <summary>
        /// the updates for the NEC on the 5080
        /// </summary>
        public IUpdateMDT Nec
        {
            get { return _nec; }
        }
        /// <summary>
        /// the updates for firmware on 5040
        /// </summary>
        public IUpdateMDT Firmware5040
        {
            get { return _firmware5040; }
        }
        /// <summary>
        /// the updates for firmware on 5010
        /// </summary>
        public IUpdateMDT Firmware5010
        {
            get { return _firmware5010; }
        }
        /// <summary>
        /// the updates for firmware on 4000
        /// </summary>
        public IUpdateMDT Firmware4000
        {
            get { return _firmware4000; }
        }
        /// <summary>
        /// the updates for shell configuration file
        /// </summary>
        public IUpdateMDT ShellConfiguration
        {
            get { return _shellConfiguration; }
        }
        /// <summary>
        /// the updates for talon configuration file
        /// </summary>
        public IUpdateMDT TalonConfiguration
        {
            get { return _talonConfiguration; }
        }
        /// <summary>
        /// the updates for talon APK file
        /// </summary>
        public IUpdateMDT Talon
        {
            get { return _talon; }
        }
        /// <summary>
        /// the updates for swift APK file
        /// </summary>
        public IUpdateMDT Swift
        {
            get { return _swift; }
        }
        /// <summary>
        /// the updates for Talon 3rd Party apk files
        /// </summary>
        public IUpdateMDT Talon3rdPartyApps
        {
            get { return null; }
        }
        #endregion

        #region constructor
        /// <summary>
        /// create a new Update manager
        /// </summary>
        /// <param name="updateUnitsFile">the path of the Update units xml file</param>
        public XMLUpdateMDTManager(string updateUnitsFile)
        {
            _binaryFiles = new Dictionary<string, cBinaryFile>();
            _motFiles = new Dictionary<string, cMotFile>();

            try
            {
                FileInfo f = new FileInfo(updateUnitsFile);
                XmlDocument document = new XmlDocument();
                using (XmlTextReader reader = new XmlTextReader(f.FullName))
                {
                    reader.WhitespaceHandling = WhitespaceHandling.None;
                    document.Load(reader);

                    //get the Updates for WinCE
                    XmlNode node = document.DocumentElement.SelectSingleNode("WinCE");
                    _winCE = ReadUpdateNode(node, true, DownloadType.WinCE);

                    //get the Updates for NEC
                    node = document.DocumentElement.SelectSingleNode("NEC");
                    _nec = ReadUpdateNode(node, true, DownloadType.NEC);

                    //get the Updates for 5040
                    node = document.DocumentElement.SelectSingleNode("Firmware5040");
                    _firmware5040 = ReadUpdateNode(node, false, DownloadType.Firmware5040);

                    //get the Updates for 5010
                    node = document.DocumentElement.SelectSingleNode("Firmware5010");
                    _firmware5010 = ReadUpdateNode(node, false, DownloadType.Firmware5010);

                    //get the Updates for 4000
                    node = document.DocumentElement.SelectSingleNode("Firmware4000");
                    _firmware4000 = ReadUpdateNode(node, true, DownloadType.Firmware4000);

                    //get the Updates for WinCE
                    node = document.DocumentElement.SelectSingleNode("ShellConfiguration");
                    _shellConfiguration = ReadUpdateNode(node, true, DownloadType.ShellConfiguration);

                    //get the Updates for Talon/Swift configs
                    node = document.DocumentElement.SelectSingleNode("TalonConfiguration");
                    _talonConfiguration = ReadUpdateNode(node, true, DownloadType.TalonConfig);
                    //get the Updates for Talon
                    node = document.DocumentElement.SelectSingleNode("Talon");
                    _talon = ReadUpdateNode(node, true, DownloadType.Talon);
                    //get the Updates for Swift
                    node = document.DocumentElement.SelectSingleNode("Swift");
                    _swift = ReadUpdateNode(node, true, DownloadType.Swift);
                    //get the Updates for Talon 3rdparty - not supported via xml
                }
            }
            catch (Exception exp)
            {
                _log.Error(string.Format("Error reading {0}. {1}", updateUnitsFile, exp.Message), exp);
            }
        }
        #endregion

        #region private methods
        private IUpdateMDT ReadUpdateNode(XmlNode mdtNode, bool binaryFile, DownloadType downloadType)
        {
            XMLUpdateMDT update = null;
            if (mdtNode == null)
            {
                update = new XMLUpdateMDT(null, false, downloadType);
            }
            else
            {
                //create the update MDT from the node
                bool forceDownload = bool.Parse(mdtNode.Attributes["forceDownload"].Value);
                int segmentSize = int.Parse(mdtNode.Attributes["segmentSize"].Value);
                XmlNode fileNode = mdtNode.SelectSingleNode("File");
                IUpdateFile updateFile = GetUpdateFile(fileNode, binaryFile, segmentSize);
                update = new XMLUpdateMDT(updateFile, forceDownload, downloadType);

                //create the fleets for this mdt type
                XmlNodeList nodeList = mdtNode.SelectNodes("Fleet");
                foreach (XmlNode fleetNode in nodeList)
                {
                    int fleetId = int.Parse(fleetNode.Attributes["id"].Value);
                    fileNode = fleetNode.SelectSingleNode("File");
                    updateFile = GetUpdateFile(fileNode, binaryFile, segmentSize);
                    XMLUpdateFleet fleet = new XMLUpdateFleet(updateFile, fleetId);

                    //create the vehciles for this fleet
                    XmlNodeList vehicleNodeList = fleetNode.SelectNodes("Vehicle");
                    foreach (XmlNode vehicleNode in vehicleNodeList)
                    {
                        int vehicleId = int.Parse(vehicleNode.Attributes["id"].Value);
                        fileNode = vehicleNode.SelectSingleNode("File");
                        updateFile = GetUpdateFile(fileNode, binaryFile, segmentSize);
                        fleet.AddVehicle(updateFile, vehicleId);
                    }

                    update.AddFleet(fleet, fleetId);
                }
            }
            return update;
        }

        /// <summary>
        /// check if the update file already exists, if it does return it, else create a new one and return it
        /// </summary>
        /// <param name="fileNode">the xml node which represents the file</param>
        /// <param name="binaryFile">true if it is a binary file</param>
        /// <param name="segmentSize">the segment size to split the file (binary only)</param>
        /// <returns>the Update file</returns>
        private IUpdateFile GetUpdateFile(XmlNode fileNode, bool binaryFile, int segmentSize)
        {
            if (fileNode == null)
            {
                return null;
            }

            //read the xml node
            int major = int.Parse(fileNode.Attributes["major"].Value);
            int minor = int.Parse(fileNode.Attributes["minor"].Value);
            XmlNode attribute = fileNode.Attributes["forceDownload"];
            bool forceDownload = false;
            if (attribute != null)
            {
                forceDownload = bool.Parse(attribute.Value);
            }
            attribute = fileNode.Attributes["forceLogout"];
            bool forceLogout = false;
            if (attribute != null)
            {
                forceLogout = bool.Parse(attribute.Value);
            }
            string path = fileNode.Attributes["path"].Value;

            UpdateFile updateFile;
            if (binaryFile)
            {
                //check if binary file has already been read
                cBinaryFile binFile;
                if (_binaryFiles.ContainsKey(path))
                {
                    binFile = _binaryFiles[path];
                }
                else
                {
                    binFile = new cBinaryFile(path, segmentSize);
                    _binaryFiles.Add(path, binFile);
                }
                updateFile = new UpdateFile(path, major, minor, forceDownload, forceLogout, binFile);
            }
            else
            {
                //check if mot file has already been read
                cMotFile motFile;
                if (_motFiles.ContainsKey(path))
                {
                    motFile = _motFiles[path];
                }
                else
                {
                    motFile = new cMotFile(path);
                    _motFiles.Add(path, motFile);
                }
                updateFile = new UpdateFile(path, major, minor, forceDownload, forceLogout, motFile);
            }
            return updateFile;
        }
        #endregion

            
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.MDT.Interface.MDTUpdates
{
    /// <summary>
    /// interface for the details of an update for a MDT
    /// </summary>
    public interface IUpdateMDT
    {
        /// <summary>
        /// the download type
        /// </summary>
        DownloadType DownloadType { get; }
        /// <summary>
        /// force download
        /// </summary>
        bool ForceDownload
        {
            get;
        }

        /// <summary>
        /// does this update type support multiple downloads
        /// </summary>
        bool SupportsMultipleDownloads
        {
            get;
        }

        /// <summary>
        /// get the update for a specfic mobile ID
        /// </summary>
        /// <param name="mobileId">the mobile id of the unit</param>
        /// <returns>the Update file for this mobile id</returns>
        IUpdateFile GetUpdateFile(int mobileId);

        /// <summary>
        /// get the update for a specfic fleet and vehcile
        /// </summary>
        /// <param name="fleetId">the fleet id of the unit</param>
        /// <param name="vehicleId">the vehicle id of the unit</param>
        /// <returns>the Update file for this fleet/vehicle id</returns>
        IUpdateFile GetUpdateFile(int fleetId, int vehicleId);

        /// <summary>
        /// if multiple lists are supported get all the updates for a specfic fleet and vehcile
        /// </summary>
        /// <param name="fleetId">the fleet id of the unit</param>
        /// <param name="vehicleId">the vehicle id of the unit</param>
        /// <returns>the Update file for this fleet/vehicle id</returns>
        List<IUpdateFile> GetUpdates(int fleetId, int vehicleId);
    }
}

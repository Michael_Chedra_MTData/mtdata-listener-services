using System;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates
{
    /// <summary>
    /// class that contains the inforamation for an update of Talon 3rd party app
    /// </summary>
    public class Talon3rdPartyUpdateFile : UpdateFile, ITalon3rdPartyUpdateFile
    {
        #region private fields
        private string _displayName;
        private string _packageName;
        private int _versionCode;
        private string _versionName;
        #endregion

        #region ITalon3rdPartyUpdateFile Members
        /// <summary>
        /// the dispaly name of the app
        /// </summary>
        public string DisplayName { get { return _displayName; } }
        /// <summary>
        /// the package name of the app
        /// </summary>
        public string PackageName { get { return _packageName; } }
        /// <summary>
        /// the version Code of the app
        /// </summary>
        public int VersionCode { get { return _versionCode; } }
        /// <summary>
        /// the version name of the app
        /// </summary>
        public string VersionName { get { return _versionName; } }
        #endregion

        /// <summary>
        /// Create a new Update for a binary file
        /// </summary>
        /// <param name="fileName">the full path of the file</param>
        /// <param name="majorVersion">the major version number for this update</param>
        /// <param name="minorVersion">the minor version number for this update</param>
        /// <param name="binaryFile">the binary file to update</param>
        /// <param name="forceDownload">is this updated forced onto the unit</param>
        /// <param name="forceLogout">is a logout of unit required as soon as the download has completed</param>
        public Talon3rdPartyUpdateFile(string fileName, int majorVersion, int minorVersion, bool forceDownload, bool forceLogout, cBinaryFile binaryFile,
            string displayName, string packageName, int versionCode, string versionName)
            : base(fileName, majorVersion, minorVersion, forceDownload, forceLogout, binaryFile)
        {
            _displayName = displayName;
            _packageName = packageName;
            _versionCode = versionCode;
            _versionName = versionName;
        }


    }
}

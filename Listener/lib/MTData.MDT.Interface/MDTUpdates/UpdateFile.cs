using System;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates
{
    /// <summary>
    /// class that contains the inforamation for an update of MOT, WinCE or NEC
    /// </summary>
    public class UpdateFile: IUpdateFile
    {
        #region private fields
        /// <summary>
        /// the MOT file to update
        /// </summary>
        protected cMotFile _motFile;
        /// <summary>
        /// the binary file to update
        /// </summary>
        protected cBinaryFile _binaryFile;
        /// <summary>
        /// the major version number for this update
        /// </summary>
        protected int _majorVersion;
        /// <summary>
        /// the minor version number for this update
        /// </summary>
        protected int _minorVersion;
        /// <summary>
        /// the file name of the update file
        /// </summary>
        protected string _fileName;
        /// <summary>
        /// does this file require a forced download
        /// </summary>
        protected bool _forceDownload;
        /// <summary>
        /// Does this file require a force logout once the download is complete
        /// </summary>
        protected bool _forceLogout;
        #endregion

        #region IUpdateFile Members
        /// <summary>
        /// the MOT file to update
        /// </summary>
        public cMotFile MotFile
        {
            get { return _motFile; }
        }
        /// <summary>
        /// the binary file to update
        /// </summary>
        public cBinaryFile BinaryFile
        {
            get { return _binaryFile; }
        }
        /// <summary>
        /// the major version number for this update
        /// </summary>
        public int MajorVersion
        {
            get { return _majorVersion; }
        }
        /// <summary>
        /// the minor version number for this update
        /// </summary>
        public int MinorVersion
        {
            get { return _minorVersion; }
        }

        /// <summary>
        /// does this file require a forced download
        /// </summary>
        public bool ForceDownload
        {
            get { return _forceDownload; }
        }

        /// <summary>
        /// Does this file require a force logout once the download is complete
        /// </summary>
        public bool ForceLogout
        {
            get { return _forceLogout; }
        }
        #endregion

        #region constructors
        /// <summary>
        /// Create a new Update for a MOT file
        /// </summary>
        /// <param name="fileName">the full path of the file</param>
        /// <param name="majorVersion">the major version number for this update</param>
        /// <param name="minorVersion">the minor version number for this update</param>
        /// <param name="forceDownload">is this updated forced onto the unit</param>
        /// <param name="forceLogout">is a logout of unit required as soon as the download has completed</param>
        /// <param name="motFile">the MOT file to update</param>
        public UpdateFile(string fileName, int majorVersion, int minorVersion, bool forceDownload, bool forceLogout, cMotFile motFile)
        {
            _fileName = fileName;
            _majorVersion = majorVersion;
            _minorVersion = minorVersion;
            _motFile = motFile;
            _forceDownload = forceDownload;
            _forceLogout = forceLogout;
        }
        /// <summary>
        /// Create a new Update for a binary file
        /// </summary>
        /// <param name="fileName">the full path of the file</param>
        /// <param name="majorVersion">the major version number for this update</param>
        /// <param name="minorVersion">the minor version number for this update</param>
        /// <param name="binaryFile">the binary file to update</param>
        /// <param name="forceDownload">is this updated forced onto the unit</param>
        /// <param name="forceLogout">is a logout of unit required as soon as the download has completed</param>
        public UpdateFile(string fileName, int majorVersion, int minorVersion, bool forceDownload, bool forceLogout, cBinaryFile binaryFile)
        {
            _fileName = fileName;
            _majorVersion = majorVersion;
            _minorVersion = minorVersion;
            _binaryFile = binaryFile;
            _forceDownload = forceDownload;
            _forceLogout = forceLogout;
        }
        #endregion


    }
}

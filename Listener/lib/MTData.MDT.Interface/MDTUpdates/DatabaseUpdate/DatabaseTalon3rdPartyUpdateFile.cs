﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates.DatabaseUpdate
{
    public class DatabaseTalon3rdPartyUpdateFile : DatabaseUpdateFile, ITalon3rdPartyUpdateFile
    {
        #region ITalon3rdPartyUpdateFile Members
        /// <summary>
        /// the dispaly name of the app
        /// </summary>
        public string DisplayName { get; private set; }
        /// <summary>
        /// the package name of the app
        /// </summary>
        public string PackageName { get; private set; }
        /// <summary>
        /// the version Code of the app
        /// </summary>
        public int VersionCode { get; private set; }
        /// <summary>
        /// the version name of the app
        /// </summary>
        public string VersionName { get; private set; }
        #endregion

        #region constructors
        /// <summary>
        /// Create a new Update for a binary file
        /// </summary>
        /// <param name="fileName">the full path of the file</param>
        /// <param name="majorVersion">the major version number for this update</param>
        /// <param name="minorVersion">the minor version number for this update</param>
        /// <param name="binaryFile">the binary file to update</param>
        /// <param name="forceDownload">is this updated forced onto the unit</param>
        /// <param name="forceLogout">is a logout of unit required as soon as the download has completed</param>
        public DatabaseTalon3rdPartyUpdateFile(string fileName, int majorVersion, int minorVersion, bool forceDownload, bool forceLogout, cBinaryFile binaryFile, 
            int dispatchID, string displayName, string packageName, int versionCode, string versionName)
            : base(fileName, majorVersion, minorVersion, forceDownload, forceLogout, binaryFile, dispatchID)
        {
            DispatchID = dispatchID;
            DisplayName = displayName;
            PackageName = packageName;
            VersionCode = versionCode;
            VersionName = versionName;
        }
        #endregion



    }
}

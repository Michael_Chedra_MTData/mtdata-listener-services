﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MTData.MDT.Interface.MDTUpdates;

namespace MTData.MDT.Interface.MDTUpdates.DatabaseUpdate
{
    public class DatabaseUpdateMDTManager : IUpdateMDTManager
    {
        private ILog _log = LogManager.GetLogger(typeof(DatabaseUpdateMDTManager));

        /// <summary>
        /// the updates for WinCE
        /// </summary>
        private IUpdateMDT _winCE;
        /// <summary>
        /// the updates for the NEC on the 5080
        /// </summary>
        private IUpdateMDT _nec;
        /// <summary>
        /// the updates for firmware on 5040
        /// </summary>
        private IUpdateMDT _firmware5040;
        /// <summary>
        /// the updates for firmware on 5010
        /// </summary>
        private IUpdateMDT _firmware5010;
        /// <summary>
        /// the updates for firmware on 4000
        /// </summary>
        private IUpdateMDT _firmware4000;
        /// <summary>
        /// the updates for shell configuration file
        /// </summary>
        private IUpdateMDT _shellConfiguration;
        /// <summary>
        /// the updates for talon configuration file
        /// </summary>
        private IUpdateMDT _talonConfiguration;
        private IUpdateMDT _talon;
        private IUpdateMDT _swift;
        private IUpdateMDT _talon3rdParty;

        #region IUpdateMDTManager Members
        /// <summary>
        /// the updates for WinCE
        /// </summary>
        public IUpdateMDT WinCE
        {
            get { return _winCE; }
        }
        /// <summary>
        /// the updates for the NEC on the 5080
        /// </summary>
        public IUpdateMDT Nec
        {
            get { return _nec; }
        }
        /// <summary>
        /// the updates for firmware on 5040
        /// </summary>
        public IUpdateMDT Firmware5040
        {
            get { return _firmware5040; }
        }
        /// <summary>
        /// the updates for firmware on 5010
        /// </summary>
        public IUpdateMDT Firmware5010
        {
            get { return _firmware5010; }
        }
        /// <summary>
        /// the updates for firmware on 4000
        /// </summary>
        public IUpdateMDT Firmware4000
        {
            get { return _firmware4000; }
        }
        /// <summary>
        /// the updates for shell configuration file
        /// </summary>
        public IUpdateMDT ShellConfiguration
        {
            get { return _shellConfiguration; }
        }
        /// <summary>
        /// the updates for talon configuration file
        /// </summary>
        public IUpdateMDT TalonConfiguration
        {
            get { return _talonConfiguration; }
        }
        /// <summary>
        /// the updates for talon APK file
        /// </summary>
        public IUpdateMDT Talon
        {
            get { return _talon; }
        }
        /// <summary>
        /// the updates for swift APK file
        /// </summary>
        public IUpdateMDT Swift
        {
            get { return _swift; }
        }
        /// <summary>
        /// the updates for Talon 3rd Party apk files
        /// </summary>
        public IUpdateMDT Talon3rdPartyApps
        {
            get { return _talon3rdParty; }
        }
        #endregion

        public DatabaseUpdateMDTManager(
         IUpdateMDT winCE,
         IUpdateMDT nec,
         IUpdateMDT firmware5040,
         IUpdateMDT firmware5010,
         IUpdateMDT firmware4000,
         IUpdateMDT shellConfiguration,
         IUpdateMDT talonConfiguration,
         IUpdateMDT talon,
         IUpdateMDT swift,
         IUpdateMDT talon3rdParty)
        {

            this._winCE = winCE;
            this._nec = nec;
            this._firmware5040 = firmware5040;
            this._firmware5010 = firmware5010;
            this._firmware4000 = firmware4000;
            this._shellConfiguration = shellConfiguration;
            _talonConfiguration = talonConfiguration;
            _talon = talon;
            _swift = swift;
            _talon3rdParty = talon3rdParty;
        }

        public DatabaseUpdateMDTManager(Func<DownloadType, IUpdateMDT> readUpdateTableDelegate)
        {
            if (readUpdateTableDelegate != null)
            {
                this._winCE = readUpdateTableDelegate(DownloadType.WinCE);
                this._nec = readUpdateTableDelegate(DownloadType.NEC);
                this._firmware5040 = readUpdateTableDelegate(DownloadType.Firmware5040);
                this._firmware5010 = readUpdateTableDelegate(DownloadType.Firmware5010);
                this._firmware4000 = readUpdateTableDelegate(DownloadType.Firmware4000);
                this._shellConfiguration = readUpdateTableDelegate(DownloadType.ShellConfiguration);
                this._talonConfiguration = readUpdateTableDelegate(DownloadType.TalonConfig);
                this._talon = readUpdateTableDelegate(DownloadType.Talon);
                this._swift = readUpdateTableDelegate(DownloadType.Swift);
                this._talon3rdParty = readUpdateTableDelegate(DownloadType.Talon3rdPartyApp);
            }

        }






    }
}

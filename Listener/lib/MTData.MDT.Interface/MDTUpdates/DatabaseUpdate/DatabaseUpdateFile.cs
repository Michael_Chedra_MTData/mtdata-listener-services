﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates.DatabaseUpdate
{
    public class DatabaseUpdateFile : UpdateFile, IUpdateFile
    {

        public int DispatchID { get; protected set; }

        #region constructors
        /// <summary>
        /// Create a new Update for a MOT file
        /// </summary>
        /// <param name="fileName">the full path of the file</param>
        /// <param name="majorVersion">the major version number for this update</param>
        /// <param name="minorVersion">the minor version number for this update</param>
        /// <param name="forceDownload">is this updated forced onto the unit</param>
        /// <param name="forceLogout">is a logout of unit required as soon as the download has completed</param>
        /// <param name="motFile">the MOT file to update</param>
        public DatabaseUpdateFile(string fileName, int majorVersion, int minorVersion, bool forceDownload, bool forceLogout, cMotFile motFile, int dispatchID)
            : base(fileName, majorVersion, minorVersion, forceDownload, forceLogout, motFile)
        {
            DispatchID = dispatchID;
        }
        /// <summary>
        /// Create a new Update for a binary file
        /// </summary>
        /// <param name="fileName">the full path of the file</param>
        /// <param name="majorVersion">the major version number for this update</param>
        /// <param name="minorVersion">the minor version number for this update</param>
        /// <param name="binaryFile">the binary file to update</param>
        /// <param name="forceDownload">is this updated forced onto the unit</param>
        /// <param name="forceLogout">is a logout of unit required as soon as the download has completed</param>
        public DatabaseUpdateFile(string fileName, int majorVersion, int minorVersion, bool forceDownload, bool forceLogout, cBinaryFile binaryFile, int dispatchID)
            : base(fileName, majorVersion, minorVersion, forceDownload, forceLogout, binaryFile)
        {
            DispatchID = dispatchID;
        }
        #endregion



    }
}

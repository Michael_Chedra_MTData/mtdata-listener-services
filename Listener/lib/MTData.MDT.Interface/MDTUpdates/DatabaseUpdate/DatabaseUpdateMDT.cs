﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MTData.MDT.Interface.MDTUpdates;

namespace MTData.MDT.Interface.MDTUpdates.DatabaseUpdate
{
    public class DatabaseUpdateMDT : IUpdateMDT
    {
        private readonly object _syncLock = new object();

        private readonly Dictionary<Tuple<int, int>, List<IUpdateFile>> _fleetVehicleUpdateFile = new Dictionary<Tuple<int, int>, List<IUpdateFile>>();


        public DatabaseUpdateMDT(bool forceDownload, DownloadType downloadType)
        {
            ForceDownload = forceDownload;
            DownloadType = downloadType;
        }

        #region IUpdateMDT Members

        public DownloadType DownloadType { get; private set; }
        public bool ForceDownload { get; private set; }

        public bool SupportsMultipleDownloads
        {
            get
            {
                if (DownloadType == DownloadType.Talon3rdPartyApp)
                {
                    return true;
                }
                return false;
            }
        }

        public IUpdateFile GetUpdateFile(int mobileId)
        {
            int fleetId = (int)mobileId / 256;
            int vehicleId = mobileId % 256;
            return GetUpdateFile(fleetId, vehicleId);
        }

        public IUpdateFile GetUpdateFile(int fleetId, int vehicleId)
        {
            var tuple = new Tuple<int, int>(fleetId, vehicleId);
            if (_fleetVehicleUpdateFile.ContainsKey(tuple))
            {
                return _fleetVehicleUpdateFile[tuple][0];
            }
            else
            {
                return GetUpdateFileForFleet(fleetId);

            }

        }

        public List<IUpdateFile> GetUpdates(int fleetId, int vehicleId)
        {
            var tuple = new Tuple<int, int>(fleetId, vehicleId);
            if (_fleetVehicleUpdateFile.ContainsKey(tuple))
            {
                return _fleetVehicleUpdateFile[tuple];
            }
            else
            {
                return GetUpdateFilesForFleet(fleetId);
            }
        }

        #endregion

        public void AddFleetVehicle(int fleetID, int vehicleID, IUpdateFile updateFile)
        {
            lock (this._syncLock)
            {
                var tuple = new Tuple<int, int>(fleetID, vehicleID);
                if (this._fleetVehicleUpdateFile.ContainsKey(tuple))
                {
                    if (!SupportsMultipleDownloads)
                    {
                        _fleetVehicleUpdateFile[tuple].Clear();
                    }
                    _fleetVehicleUpdateFile[tuple].Add(updateFile);
                }
                else
                {
                    var entry = new List<IUpdateFile>();
                    entry.Add(updateFile);
                    _fleetVehicleUpdateFile.Add(tuple, entry);
                }
            }

        }

        public void AddFleetVehicle(int fleetID, IUpdateFile updateFile)
        {
            AddFleetVehicle(fleetID, 0, updateFile);
        }


        public IUpdateFile GetUpdateFileForFleet(int fleetID)
        {
            var tuple = new Tuple<int, int>(fleetID, 0);
            if (this._fleetVehicleUpdateFile.ContainsKey(tuple))
            {
                return this._fleetVehicleUpdateFile[tuple][0];
            }
            else
            {
                //TODO Default Update???
                return null;
            }
        }
        public List<IUpdateFile> GetUpdateFilesForFleet(int fleetID)
        {
            var tuple = new Tuple<int, int>(fleetID, 0);
            if (this._fleetVehicleUpdateFile.ContainsKey(tuple))
            {
                return this._fleetVehicleUpdateFile[tuple];
            }
            else
            {
                return new List<IUpdateFile>();
            }
        }
    }
}

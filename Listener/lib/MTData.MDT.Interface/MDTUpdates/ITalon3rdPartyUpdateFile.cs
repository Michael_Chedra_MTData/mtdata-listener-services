using System;
using MTData.MotFileReader;
using MTData.Transport.Gateway.Packet;

namespace MTData.MDT.Interface.MDTUpdates
{
    /// <summary>
    /// interface for an update file
    /// </summary>
    public interface ITalon3rdPartyUpdateFile : IUpdateFile
    {
        /// <summary>
        /// the dispaly name of the app
        /// </summary>
        string DisplayName
        {
            get;
        }
        /// <summary>
        /// the package name of the app
        /// </summary>
        string PackageName
        {
            get;
        }
        /// <summary>
        /// the version Code of the app
        /// </summary>
        int VersionCode
        {
            get;
        }
        /// <summary>
        /// the version name of the app
        /// </summary>
        string VersionName
        {
            get;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.MDT.Interface.MDTUpdates
{
    public enum DownloadType
    {
        WinCE = 1,
        NEC = 2,
        Firmware5040 = 3,
        Firmware5010 = 4,
        ShellConfiguration = 5,
        MapDll = 6,
        Firmware4000 = 7,
        TalonConfig = 11,
        Swift = 12,
        Talon = 13,
        Talon3rdPartyApp = 14
    }
}

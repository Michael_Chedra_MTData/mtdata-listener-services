using System;

namespace MTData.MDT.Interface.MDTUpdates
{
    public interface IUpdateMDTManager
    {
        /// <summary>
        /// the updates for WinCE
        /// </summary>
        IUpdateMDT WinCE
        {
            get;
        }
        /// <summary>
        /// the updates for the NEC on the 5080
        /// </summary>
        IUpdateMDT Nec
        {
            get;
        }
        /// <summary>
        /// the updates for firmware on 5040
        /// </summary>
        IUpdateMDT Firmware5040
        {
            get;
        }
        /// <summary>
        /// the updates for firmware on 5010
        /// </summary>
        IUpdateMDT Firmware5010
        {
            get;
        }
        /// <summary>
        /// the updates for firmware on 4000
        /// </summary>
        IUpdateMDT Firmware4000
        {
            get;
        }
        /// <summary>
        /// the updates for shell configuration file
        /// </summary>
        IUpdateMDT ShellConfiguration
        {
            get;
        }

        /// <summary>
        /// the updates for Swift/talon configuration file
        /// </summary>
        IUpdateMDT TalonConfiguration
        {
            get;
        }

        /// <summary>
        /// the updates for talon apk file
        /// </summary>
        IUpdateMDT Talon
        {
            get;
        }

        /// <summary>
        /// the updates for Swift apk file
        /// </summary>
        IUpdateMDT Swift
        {
            get;
        }

        /// <summary>
        /// the updates for Talon 3rd Party apk files
        /// </summary>
        IUpdateMDT Talon3rdPartyApps
        {
            get;
        }
    }
}

using System;
using System.IO;
using System.Collections;
using log4net;
using MTData.Common.Utilities;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// Summary description for cWinCEDiallerPacketData.
	/// </summary>
	public class cWinCEDiallerPacketData
	{
		#region Private Vars
		private const string sClassName = "MTData.MDT.Interface.cWinCEDiallerPacketData.";
        private ILog _log = LogManager.GetLogger(typeof(cWinCEDiallerPacketData));
		#endregion
		#region Public Vars
		public int iMobileID = 0;
		public int JobID = 0;
		public string sCommandType = "";
		public string sSubCommandType = "";
		public int iFleetID = 0;
		public int iVehicleID = 0;
		public int iDriverID = 0;
		public int iPinNumber = 0;
		public short iSoftwareVersion = 0;
		public int NumberOfDataSegments = 0;
		public int SegmentNumber = 0;
		public int SendSegment = 0;
		public short iPhoneBookID = 0;
		public bool bAllowFreeDial = false;
		public string sMessage = "";
		public string sLoginResponce = "";
		public byte[] bDataSegment = null;
		public ArrayList PhoneBookNames = new ArrayList();
		public ArrayList PhoneBookNumbers = new ArrayList();
		#endregion

		public cWinCEDiallerPacketData()
		{
			
		}

		public byte[] BuildOutgoingPacket()
		{
			MemoryStream oMS = null;
			byte[] bData = null;
			byte[] bConvert = null;
			ushort iLength = 0;
			
			try
			{
				oMS = new MemoryStream();
				PacketUtilities.WriteToStream(oMS, (byte)0x02);																// SOP
				PacketUtilities.WriteToStream(oMS, iLength);																		// Length
				PacketUtilities.WriteToStream(oMS, ((char[]) sCommandType.ToCharArray(0, 1))[0]);	// Command Type
				switch(sCommandType)
				{
					case "D":
						#region Download Packets
						if (sSubCommandType == "H")
						{
							#region Download Header
							PacketUtilities.WriteToStream(oMS, this.sSubCommandType);								// Sub Command
							PacketUtilities.WriteToStream(oMS, this.iSoftwareVersion);									// Software Version
							PacketUtilities.WriteToStream(oMS, this.NumberOfDataSegments);						// Number of Segments
							#endregion
						}
						else if (sSubCommandType == "D")
						{
							#region Download Packet Data
							PacketUtilities.WriteToStream(oMS, this.sSubCommandType);								// Sub Command
							PacketUtilities.WriteToStream(oMS, this.SegmentNumber);									// Segment Number
							iLength = Convert.ToUInt16(this.bDataSegment.Length);
							PacketUtilities.WriteToStream(oMS, iLength);															// Segement Length
							PacketUtilities.WriteToStream(oMS, this.bDataSegment);										// Segement Data
							iLength = 0;
							bData = oMS.ToArray();
							int iCheckSum = 0;
							for (int X = 3; X < bData.Length; X++)
							{
								iCheckSum += (int) bData[X];
							}
							bConvert = BitConverter.GetBytes(iCheckSum);
							PacketUtilities.WriteToStream(oMS, bConvert[0]);													// Segement Checksum
							PacketUtilities.WriteToStream(oMS, bConvert[1]);
							#endregion
						}
						#endregion
						break;
					case "I":	// Login
						PacketUtilities.WriteToStream(oMS, sLoginResponce);		// Login Responce
						break;
					case "M":	// Message
						PacketUtilities.WriteToStream(oMS, (byte) 0x01, sMessage);	// Message
						break;
					case "K":	// Phonebook
						PacketUtilities.WriteToStream(oMS, ((char[]) sSubCommandType.ToCharArray(0, 1))[0]);	// Sub Command Type
						PacketUtilities.WriteToStream(oMS, bAllowFreeDial);	// Free Dialling Flag
						PacketUtilities.WriteToStream(oMS, iPhoneBookID);	// Phonebook ID
						iLength = Convert.ToUInt16(PhoneBookNames.Count);
						PacketUtilities.WriteToStream(oMS, iLength);	// Phonebook Entry Count
						for (int X = 0; X < PhoneBookNames.Count; X++)
						{
							PacketUtilities.WriteToStream(oMS, (byte) 0x0A, (string) PhoneBookNames[X]);	// Entry Name
							PacketUtilities.WriteToStream(oMS, (byte) 0x0B, (string) PhoneBookNumbers[X]); // Entry Number
						}
						break;
					default:
						break;
				}
				bData = oMS.ToArray();
				// Update the length bytes
				iLength = Convert.ToUInt16(bData.Length);
				bData[1] = ((byte[]) BitConverter.GetBytes(iLength))[0];
				bData[2] = ((byte[]) BitConverter.GetBytes(iLength))[1];
			}
			catch(System.Exception ex)
			{
				if(_log != null) _log.Error(sClassName + "BuildOutgoingPacket()", ex);
				bData = null;
			}

			return bData;
		}
		public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
		{
			#region Local Variables
			string sRet = "";
			byte bTemp = (byte) 0x00;
			string sTemp = "";
			int iPosition = 0;
			short iLength = 0;
			#endregion

			try
			{
				this.iMobileID = Convert.ToInt32(sUnitID);

				// Check for the Start of packet
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bTemp);
				if (bTemp != (byte) 0x02)
				{
					if(_log != null) _log.Info("Found packet with no SOP : " + BitConverter.ToString(bMsg, 0));
					return sRet;
				}
				// Check the packet length
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iLength);
				if (bMsg.Length != (int) iLength)
					return sRet;
				// Get the command type
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bTemp);				
				this.sCommandType = Convert.ToString((char) bTemp);

				switch(sCommandType)
				{
					case "D":
						#region Request a Download Packet
						//D - Request Data Segment - [CmdType][SubCmd][Send Segment][SBS][Spare]
						// Get the command type
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bTemp);				
						this.sSubCommandType = Convert.ToString((char) bTemp);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.SendSegment);
						#endregion
						break;
					case "I":
						#region Login Packet
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iFleetID);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iVehicleID);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iDriverID);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iPinNumber);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iPhoneBookID);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iSoftwareVersion);
						#endregion
						break;
					case "O":
						#region Logout Packet
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iFleetID);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iVehicleID);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iDriverID);
						#endregion
						break;
					case "M":
						#region Message Packet
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte) 0x01, ref sMessage);
						#endregion
						break;
					case "K":
						#region Phone Packet
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bTemp);
						sSubCommandType = Convert.ToString((char) bTemp);
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref iLength);
						for (int X = 0; X < (int) iLength; X++)
						{
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte) 0x0A, ref sTemp);
							PhoneBookNames.Add(sTemp);
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte) 0x0B, ref sTemp);
							PhoneBookNumbers.Add(sTemp);
						}
						#endregion
						break;
					default :
						break;
				}
			}
			catch(System.Exception ex)
			{
				if(_log != null) 
				{
					if(bMsg == null)
						_log.Error(sClassName + "ParseIncommingPacket(byte[] bMsg = null, string sUnitID = '" + sUnitID + "')", ex);
					else
						_log.Error(sClassName + "ParseIncommingPacket(byte[] bMsg, string sUnitID = '" + sUnitID + "')", ex);
				}
			}
			return sRet;
		}

		public string GetPacketTypeName()
		{
			string sRet = "";
			switch(sCommandType)
			{
				case "I":
					sRet = "Login Packet";
					break;
				case "O":
					sRet = "Logout Packet";
					break;
				case "M":
					sRet = "Message Packet";
					break;
				case "K":
					sRet = "Phonebook Packet";
					break;
				default:
					sRet = "WinCE Dialler Packet";
					break;
			}
			return sRet;
		}

	}
}

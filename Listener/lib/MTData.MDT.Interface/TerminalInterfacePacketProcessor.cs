using System;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;
using MTData.MDT.Interface;
using MTData.Common.Utilities;
using MTData.MotFileReader;

using System.Collections;
using System.IO;
using log4net;


namespace MTData.MDT.Interface
{
	public class TerminalInterfacePacketProcessor
	{
		#region Incomming Packet Defs
		// [Standard Packet] = [SOP] [Length (2 Bytes)] [CmdType (1 Byte)][PacketData][SBS (1 Byte)][EOP (1 Byte)]
		//	[GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
		//	[Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
		// [Packet Data] Definitions
		//	Download Request - [SOP] [Length (2 Bytes)] [CmdType "D" (1 Byte)][Send Segment][SBS (1 Byte)][EOP (1 Byte)]
		#endregion
		#region Outgoing Packet Defs
		// [Standard Packet] = [SOP] [Length (2 Bytes)] [CmdType (1 Byte)][PacketData][SBS (1 Byte)][EOP (1 Byte)]
		// [Packet Data] Definitions
		//	Download Packets - CmdType = "D"
		//		Download Header :		[SubCmd = H][Software Version (2 Bytes)][Number of Segments (4 Bytes)]
		//		Download Data :			[SubCmd = D][Segment Number (4 Bytes)][Segment Length (2 Bytes)][Data (Var Length][CheckSum (2 bytes)]
		#endregion
		#region Private Vars
		private const string sClassName = "MTData.Terminal.Interface";
        private ILog _log = LogManager.GetLogger(typeof(TerminalInterfacePacketProcessor));
		private bool bTranslateRJs = true;		
		#endregion
		#region Public Vars
		// File Download vars
		public ushort MotFileRequestedParcelNo = 0;
		public byte SoftwareVersionMajor = 0;
		public byte SoftwareVersionMinor = 0;
		public ushort TotalNumberOfDataParcels = 0;
		public ushort NumberOfDataParcels = 0;
		public ushort DownloadMotParcelNumber = 0;
		public ArrayList DownloadMotParcel = null;
		public ushort[] DownloadMotParcelCheckSum = null;
		public byte DownloadVersion = 0;
		public ushort PacketNumber = 0;
		public ushort RequestedParcelNo;
				
		//download verfity variables
		public int ProgramTotalCheckSum = 0;
		public int PacketTotalCheckSum = 0;
		public int ProgramPacketCountTotal = 0;
		public int ProgramByteTotal = 0;
		public int ProgramDataBytesCheckSum = 0;

		public string sSigFilePath = "";
		public byte [] bRawData = null;

		// Shared Public variables
		public string CommandType = "";						// [CmdType]
		public int iMobileID = 0;							// [Mobile ID]
		public string SubCommandType = "";					// [SubCmd]
		public int VehicleID = 0;							// [VehicleID]
		public int DriverID = 0;							// [DriverID]
		public int FleetID = 0;								// [FleetID]
		public int JobID = 0;								// [JobID]
		public int ProtocolVersion = 0;						// [Protocol Version]
		public int HardwareType = 0;						// [Hardware Type]
		public uint Distance = 0;							// [Distance]
		public int DeviceTimeSeconds = 0;					// Time since last GPS lock
		public cGPSData GPSPosition = null;					// [GPS] 

		//Login Response
		public string	Logon_Result			= "";		// Login result
															//	C: Login accepted
															//	U: Unknown username
															//	P: Incorrect password
															//	Q: Contact query channe
		public string	Logon_DriversName		= "";		// Login Drivers name
		public string	Logon_VehicleType		= "";		// Flag set if the vehicle is a truck, 1 for truck
		public string	Logon_DaylightSavings	= "";		// Login daylights savings active, "Y", or "N"
		public int		Logon_GMT_OffsetHrs		= 0;		// GMT Offset Hours
		public int		Logon_GMT_OffsetMins	= 0;		// GMT Offset Minutes
		public int		Logon_NetworkKeepAlive	= 0;		// network keep alive time in seconds
		public int		Logon_NewMobileID		= 0;		// change of mobile id
		public int		Logon_ServerMajor_FirmwareVersion	= 0 ;
		public int		Logon_ServerMinor_FirmwareVersion	= 0 ;
		public int		Logon_DriverFlags	= 0;			// bit 0: set for driver free dial option

        //job header variables
        public string	Job_JobNumber		= "";		// job number in ASCII
        public int		Job_LegCount		= 0;		// number of stops
        public string	Job_Title			= "";		// job text
		public int		LoadNumber			= 0;		// load number

        #region Job Leg Variables
        public int		JobLeg_Number			= 0;			// number of this leg
        public string	JobLeg_Type			    = "";			// leg type "P" or "D"
        public int		JobLeg_Flags			= 0;			//	Bit 1 SET = Signature Flag 
                                                                //	Bit 2 SET = Quantity Flag
                                                                //	Bit 3 SET = Barcode Flag
        public string	JobLeg_Status			= "";			// Job Stop leg state
                                                                //  A = active
                                                                //  A = Active
                                                                //  P = Present
                                                                //  V = Visited
                                                                //  C = Complete
                                                                //  0 = No change
        public int JobLeg_TimeStart_Year = 0;		            // job leg arrive time   
        public int JobLeg_TimeStart_Month = 0;		            // job leg arrive time   
        public int JobLeg_TimeStart_Day = 0;		            // job leg arrive time   
        public int JobLeg_TimeStart_Hour = 0;		            // job leg arrive time   
        public int JobLeg_TimeStart_Min = 0;		            // job leg arrive time   
        public int JobLeg_Latitude = 0;			                // job leg latitude
        public int JobLeg_Longitude = 0;			            // job leg longitude
	    public int JobLeg_LocationRadius = 0;                   // job leg location radius
        public string JobLeg_CustomerName = "";			        // job leg customer name	max 20 characters
        public string JobLeg_Address = "";			            // job leg address			max 100 characters
		public string JobLeg_ConNumber = "";					// con note number for the job leg
		public int JobLeg_TaksID = 0;							// job leg unique task id
		public int JobLeg_NoOfProducts = 0;						// number of products attached to this leg
		public ArrayList JobLeg_ProductList = new ArrayList();         // job product list types
		public string JobLeg_Remaks = "";			            // job leg remarks,		    max 600 characters


		public int JobLeg_Products_PacketNo = 0;				//products packet no
		public int JobLeg_Products_PacketNoTotal = 0;			//job leg products total number of packets

        #endregion

        //Message to Driver
        public int		Message_ID				= 0;			// message id of this message,
        public string	Message_Text			= "";			// message text 

        //PreDefined Message Download List
        public int		PreDefined_MessageListID = 0;			// predefined message list ID
        public  int		PreDefined_MessageAmount = 0;
        //public int		PreDefined_MessageID	 = 0;			// predefined message ID
        public ArrayList oPreDefinedMessageList = new ArrayList ();


        ////Delay Reason Download List
        //public int DelayReason_ListID = 0;				// delay reason message list ID
        //public int DelayReason_Amount = 0;
        //public ArrayList DelayReasonList = new ArrayList();

        ////Delay Reason Download List
        //public int NoSignReason_ListID = 0;				// no sign reason message list ID
        //public int NoSignReason_Amount = 0;
        //public ArrayList NoSignReason_List = new ArrayList();

        ////Phone book id
        //public int LogonQuestion_ListID = 0;				// Logon question list id
        //public int LogonQuestion_Amount = 0;
        //public ArrayList LogonQuestion_List = new ArrayList();

        ////Phone book id
        public int PhoneBook_ListID = 0;				// phone book list ID
        //public int PhoneBook_Amount = 0;
        public StringCollection PhoneBookNumbers = new StringCollection();
		public StringCollection PhoneBookNames = new StringCollection();

        //******Incoming packets from the MCC / MDT*******************/
        //Login request packet
        public int Login_Password = 0;			// password
        public string PDT_SerialNumber = "";			// Serail number of the PDT
        public int		FirwmareVerMajor		= 0;
        public int		FirmwareVerMinor		= 0;
        public int		HardwareVerMajor		= 0;
        public int		HardwareVerMinor		= 0;
        public int		BatteryPercentage		= 0;
		public int		LogonQuestion_ListID	= 0;
		public int		NoSignReason_ListID		= 0;
		public int		DelayReason_ListID		= 0;

        ////Leg Complete
		public int			LegComplete_StopID		= 0;
		public byte			LegComplete_Delay		= 0;
		public int			LegComplete_DelayReason = 0;
		public int			LegComplete_NoSignReson = 0;
		public string		LegComplete_POD			= "";		// pod name
		public byte []		LegComplete_Signature	= null;		// signature
		public string		LegComplete_SigAddress	= "";

        ////Arrive and Depart menssages,
        //public string		JobStop_ArriveDepartType = "";		// Method the Depart message was generated
        //                                                        //	A � Automatic
        //                                                        //	E � Driver Entry

        //reset 5050 request packet defines	
		
		public string		ResetRequest_String		= "";		// text must be PLEASE RESET THIS UNIT to reset the unit

        //update time in UTC to the Dats5050
        public int			iTimeUTC_Hour			= 0;
        public int			iTimeUTC_Min			= 0;		//

        ////driver Query Channel Selection Update
        //public string		zRadioChannelSelection = null;

		#endregion

		public TerminalInterfacePacketProcessor( string SigFilePath, bool TranslateRJs)
		{
			
			sSigFilePath = SigFilePath;
			bTranslateRJs = TranslateRJs;
		}


		public byte[] BuildOutgoingPacket() //to MDT
		{
			// [Packet] = [SOP (1 Byte) 0x02][Mobile ID (4 Ascii String)][Length (2 Bytes)][CmdType (1 Byte Ascii)][Data][SBS (1 Byte) 0x07][EOP (1 Byte) 0x03]
			// 
			// If [CmdType] then [Data] =
			// J - New Job 
			// A - Add Job Stop
			// I - Logon Response			
			// D - Delete Job Stop
			// M - Message To Driver
			// K - Predefined Message List
            // R - Remove all jobs
            // r - Reset 5040
            // T - Update time
			MemoryStream oMS = null;
			byte[] bRet = null;
			byte[] bConvert = null;

			string sUnitID = "";
			
			try
			{
				oMS = new MemoryStream();

				PacketUtilities.WriteToStream(oMS, (byte) 0x02);						// [SOP (1 Byte) 0x02]
				sUnitID = Convert.ToString(iMobileID).PadLeft(4, '0').Substring(0, 4);
				PacketUtilities.WriteToStream(oMS, sUnitID);							// [Unit ID (4 Byte Ascii)]
				PacketUtilities.WriteToStream(oMS, (short) 0);							// [Length (2 Bytes)]
				PacketUtilities.WriteToStream(oMS, CommandType);						// [Command Type (1 Byte Ascii)]
			
				switch(CommandType)
				{
					case "J":			
						//	J - New Job 
						#region New Job 
						if(_log != null) _log.Info("Building Job Header Packet");
						// job id
						PacketUtilities.WriteToStream(oMS,  JobID);							// [JobID (4 bytes)]
						//job leg count
                        PacketUtilities.WriteToStream(oMS, (byte)Job_LegCount);				// [Job_LegCount (1 byte)]
						//header text
						PacketUtilities.WriteToStream(oMS, Job_Title);					    // [Job Title (Var Ascii)]
						PacketUtilities.WriteToStream(oMS, (byte)0x0E);						// [Break 0x0E]
						PacketUtilities.WriteToStream(oMS, (byte)LoadNumber);				// [Load number, order in which the jobs are sorted]
						#endregion
						break;
					case "A":			
						// A - Add Job Leg
						#region Add Job Stop
						if(_log != null) _log.Info("Building Add Job Leg Packet");
						PacketUtilities.WriteToStream(oMS, JobID);									// [JobID (4 bytes)]
						PacketUtilities.WriteToStream(oMS, (byte)JobLeg_Number);					// [Stop Number (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, JobLeg_Type.PadLeft(1, ' ').Substring(0, 1));	// [Type (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (short)JobLeg_Flags);					// [Flags (2 Bytes)]
                        PacketUtilities.WriteToStream(oMS, JobLeg_Status.PadLeft(1, (char)0x00).Substring(0, 1)); // [Status (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)JobLeg_TimeStart_Year);		// [Time Start Day (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)JobLeg_TimeStart_Month);	// [Time Start Hour  (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)JobLeg_TimeStart_Day);		// [Time Start Day (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)JobLeg_TimeStart_Hour);	// [Time Start Hour  (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)JobLeg_TimeStart_Min);		// [Time Start Min (1 Byte)]
						PacketUtilities.WriteToStream(oMS, JobLeg_ConNumber);						// [Latitude (4 Bytes)]                        
						PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
						PacketUtilities.WriteToStream(oMS, JobLeg_Latitude);						// [Latitude (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, JobLeg_Longitude);						// [Longitude (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, (short)JobLeg_LocationRadius);					// [Location Radius (2 Bytes)]
                        PacketUtilities.WriteToStream(oMS, JobLeg_CustomerName);					// [Customer Name (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
                        PacketUtilities.WriteToStream(oMS, JobLeg_Address);						// [Address (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
						PacketUtilities.WriteToStream(oMS, JobLeg_TaksID);						// [JobLegTaskID (uint32)]
						PacketUtilities.WriteToStream(oMS, (ushort)JobLeg_NoOfProducts);
                        PacketUtilities.WriteToStream(oMS, JobLeg_Remaks);                                  // [Remarks (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);                                     // [Break (1 Byte) 0x0F]
						#endregion
						break;
					case "Q":
						#region Products added to the jobs legs
						PacketUtilities.WriteToStream(oMS, JobID);									// [JobID (4 bytes)]
						PacketUtilities.WriteToStream(oMS, (byte)JobLeg_Number);					// [Stop Number (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)JobLeg_Products_PacketNo);			// [Products Packet No (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)JobLeg_Products_PacketNoTotal);	// [Products Packets Total (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// [Product List Start]
						for (int i = 0; i < JobLeg_ProductList.Count; i++)
						{
							PacketUtilities.WriteToStream(oMS, ((JobLegProduct)JobLeg_ProductList[i]).ProductID);
							PacketUtilities.WriteToStream(oMS, (byte)0x0F);                                                                                                   // [Message Seperator (1 Byte) 0x0A]
							PacketUtilities.WriteToStream(oMS, ((JobLegProduct)JobLeg_ProductList[i]).Description);                                                                                                                // [Message List Number (1 Byte)]
							PacketUtilities.WriteToStream(oMS, (byte)0x0F);                                                                                                   // [Message Seperator (1 Byte) 0x0A]
						}
						PacketUtilities.WriteToStream(oMS, (byte)0x0A); 
						#endregion
						break;
					case "I":
						// I - Logon Response - [Result (1 Byte Ascii)][Drivers Name (var Ascii)][Break (1 Byte) 0x0A][DaylightSavings (1 Byte Ascii)]
						//								[GMT Offset Hours (1 Byte)][GMT Offset Mins (1 Byte)]
						//								[Network Keep Alive (2 Bytes)][New Mobile ID (2 Bytes)]
						//								[Firmware Ver Major (1 Byte)][Firmware Ver Minor (1 Byte)]
						#region Logon Response
						if(_log != null) _log.Info("Building Login Response Packet");
						PacketUtilities.WriteToStream(oMS, Logon_Result[0]);									// [Result (1 Byte Ascii)]
						PacketUtilities.WriteToStream(oMS, Logon_DriversName);																// [Drivers Name (var Ascii)]
						PacketUtilities.WriteToStream(oMS, (byte)0x0A);																			// [Break (1 Byte) 0x0A]
						PacketUtilities.WriteToStream(oMS, Logon_VehicleType[0]);
						if (Logon_DaylightSavings != "Y" && Logon_DaylightSavings != "N")
							Logon_DaylightSavings = "Y";
						PacketUtilities.WriteToStream(oMS, Logon_DaylightSavings.PadLeft(1, 'Y').Substring(0, 1));	// [DaylightSavings (1 Byte Ascii)]
						PacketUtilities.WriteToStream(oMS, (byte)Logon_GMT_OffsetHrs);												// [GMT Offset Hours (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)Logon_GMT_OffsetMins);											// [GMT Offset Mins (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)Logon_ServerMajor_FirmwareVersion);					// [Firmware Ver Major (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)Logon_ServerMinor_FirmwareVersion);					// [Firmware Ver Minor (1 Byte)]
						
						//added free dial option on the 25th of feb 2009
						PacketUtilities.WriteToStream(oMS, (byte)0xAA);													// [Extra Data included (1 Byte)]
						PacketUtilities.WriteToStream(oMS, (byte)Logon_DriverFlags);									// [Driver Free Dial Flag (1 Byte)]
						#endregion
						break;
					case "D":		// D - Delete Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][History Flag (1 Byte)]
						#region Delete job or job leg packet
						if(_log != null) _log.Info("Building Delete job or job leg packet");
                        PacketUtilities.WriteToStream(oMS, JobID);																		// [JobID (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, (byte)JobLeg_Number);														// [Stop Number (1 Byte)]                        
						#endregion
						break;
					case "M":	// M - Message To Driver - [Message ID (4 Bytes)][Job ID (4 Bytes)][Message (var Ascii)][Break (1 Byte) 0x0F]
						#region Message to the driver packet
						if(_log != null) _log.Info("Building A new message for the driver packet");
                        PacketUtilities.WriteToStream(oMS, Message_ID);																			// [Message ID (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, Message_Text);																		// [Message (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
						
                        #endregion
						break;
					case "K":		

                        if (SubCommandType == "P")
                        {
                            #region Driver Phonebook download
                            if (_log != null) _log.Info("Building the global phonebook packet");
                            PacketUtilities.WriteToStream(oMS, "P");																					// [Sub Command (1 Byte Ascii)]
                            PacketUtilities.WriteToStream(oMS, (byte)PhoneBook_ListID);																		// [Message ID (4 Bytes)]


                            int phonenumberscount = 0;
                            if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > phonenumberscount) phonenumberscount = PhoneBookNumbers.Count;
                            if (PhoneBookNames != null) if (PhoneBookNames.Count > phonenumberscount) phonenumberscount = PhoneBookNames.Count;
                            if (phonenumberscount > 50) phonenumberscount = 50;

                            for (int i = 0; i < phonenumberscount; i++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);
                                PacketUtilities.WriteToStream(oMS, (byte)i);
                                PacketUtilities.WriteToStream(oMS, PhoneBookNumbers[i]);
                                PacketUtilities.WriteToStream(oMS, (byte)0x0B);
                                PacketUtilities.WriteToStream(oMS, PhoneBookNames[i] != null ? PhoneBookNames[i] : PhoneBookNumbers[i]);
                            }
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Driver Phonebook download
                            if (_log != null) _log.Info("Building the driver phonebook packet");
                            PacketUtilities.WriteToStream(oMS, "D");																					// [Sub Command (1 Byte Ascii)]
                            PacketUtilities.WriteToStream(oMS, (byte)PhoneBook_ListID);																		// [Message ID (4 Bytes)]
                           
                            
    						int phonenumberscount = 0;
						    if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > phonenumberscount) phonenumberscount = PhoneBookNumbers.Count;
						    if (PhoneBookNames != null) if (PhoneBookNames.Count > phonenumberscount) phonenumberscount = PhoneBookNames.Count;
                            if (phonenumberscount > 50) phonenumberscount = 50;
                            
                            for (int i = 0; i<phonenumberscount; i++)  
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);
                                PacketUtilities.WriteToStream(oMS, (byte)i);
                                PacketUtilities.WriteToStream(oMS, PhoneBookNumbers[i].Substring(0, 10));
                                PacketUtilities.WriteToStream(oMS, (byte)0x0B);
                                PacketUtilities.WriteToStream(oMS, PhoneBookNames[i] != null? PhoneBookNames[i].Substring(0, 10): PhoneBookNumbers[i].Substring(0, 10));
                            }
						    #endregion

                        }
                        else if (SubCommandType == "M")
                        {
                            #region Predefined Message list download
                            if (_log != null) _log.Info("Building the predefined message list packet");
                            PacketUtilities.WriteToStream(oMS, "M");																						// [Sub Command (1 Byte Ascii)]
                            PacketUtilities.WriteToStream(oMS, (byte)PreDefined_MessageListID);																		// [Message ID (4 Bytes)]
                            PacketUtilities.WriteToStream(oMS, (byte)PreDefined_MessageAmount);
                            for (int i = 0; i<oPreDefinedMessageList.Count; i++)  
                            {
                                PacketUtilities.WriteToStream(oMS, ((JobLegProduct) oPreDefinedMessageList[i]).ProductID);
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);																	// [Message Seperator (1 Byte) 0x0A]
                                PacketUtilities.WriteToStream(oMS, (byte)i);																			// [Message List Number (1 Byte)]
                                string zTempString = (string)oPreDefinedMessageList[i];
                                PacketUtilities.WriteToStream(oMS, zTempString);																	// [Message (var Ascii)]
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
						    #endregion

                        }
						break;
					case "R":
						#region Delete all jobs and messages on the unit
						if(_log != null) _log.Info("Building Delete all jobs and messages packet");
						#endregion
						break;
					case "r":
						#region Reset the 5050 request
						if(_log != null) _log.Info("Building reset request pacekt");
                        PacketUtilities.WriteToStream(oMS, ResetRequest_String);												// [Reset string  (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);												// [RES 0x0F]
						#endregion
						break;
					case "T":
						#region Update Current Time on the 5050
						if(_log != null) _log.Info("Building UTC Time update packet");
                        PacketUtilities.WriteToStream(oMS, (byte)iTimeUTC_Hour);
                        PacketUtilities.WriteToStream(oMS, (byte)iTimeUTC_Min);
						#endregion
						break;
					case "d":
						#region Download Packets

						PacketUtilities.WriteToStream(oMS, SubCommandType);															// [SubCmd (1 Byte Ascii)]
						if (SubCommandType == "d")
						{
							//[CmdType]			= 'd'
							//[MobileID]		
							//[SubCmd]			= 'd'
							//[CurrentVersionMSB]
							//[CurrentVersionLSB]
							//[TotalNoOfParcels]
							//[NoOfParcels]
							//{
							//	[ParcelNo]
							//	[ProgramFragment]
							//	[DownloadcheckSum]
							//	[DSB]
							//}
							//[DESB]
							#region Download Parcel
							if (_log != null) _log.Info("Building Download Header Packet");

							PacketUtilities.WriteToStream(oMS, this.SoftwareVersionMajor);							// [Software Ver Major (1 Byte)
							PacketUtilities.WriteToStream(oMS, this.SoftwareVersionMinor);							// [Software Ver Minor (1 Byte)						
							PacketUtilities.WriteToStream(oMS, (ushort)DownloadMotParcel.Count);					// [NumberOfDataSegments (2 Bytes)
							ushort iMaxDownloadMotParcelNumber = (ushort)(DownloadMotParcel.Count - DownloadMotParcelNumber);
							if (iMaxDownloadMotParcelNumber > 3)
								iMaxDownloadMotParcelNumber = 3;
							PacketUtilities.WriteToStream(oMS, (byte)iMaxDownloadMotParcelNumber);						// [NoOfParcels] (1 Bytes)]						
							for (ushort i = DownloadMotParcelNumber; i < iMaxDownloadMotParcelNumber + DownloadMotParcelNumber; i++)
							{
								cParcel oParcel = (cParcel)DownloadMotParcel[i];
								PacketUtilities.WriteToStream(oMS, (ushort)i);					// [PacketNumber] (2 Byte)]																						
								PacketUtilities.WriteToStream(oMS, oParcel.Data);				// [ProgramFragment] = 250 bytes
								PacketUtilities.WriteToStream(oMS, (ushort)oParcel.CheckSum);			// [DownloadcheckSum] = 2 bytes
								PacketUtilities.WriteToStream(oMS, (byte)0xBB);				// [DSB] = 1 byte
							}
							PacketUtilities.WriteToStream(oMS, (byte)0xCC);					// [DESB]
							#endregion
						}
						else if (SubCommandType == "v")
						{
							//[CmdType]
							//[MobileID]
							//[SubCmd]
							//[DownloadVersion]
							//[ProgramTotalCheckSum]
							//[PacketCheckSum]
							//[PacketCountTotal]
							//[ProgramByteTotal]
							//[Checksum]
							#region Download Verify
							if (_log != null) _log.Info("Building Download Verify Packet");

							PacketUtilities.WriteToStream(oMS, (byte)this.DownloadVersion);
							PacketUtilities.WriteToStream(oMS, (int)ProgramTotalCheckSum);
							PacketUtilities.WriteToStream(oMS, (int)PacketTotalCheckSum);
							PacketUtilities.WriteToStream(oMS, (ushort)ProgramPacketCountTotal);
							PacketUtilities.WriteToStream(oMS, (int)ProgramByteTotal);
							PacketUtilities.WriteToStream(oMS, (byte)ProgramDataBytesCheckSum);
							#endregion
						}
						#endregion
						break;
					default:
						break;
				}

				PacketUtilities.WriteToStream(oMS, (byte) 0x07); // [SBS (1 Byte) 0x07]
				PacketUtilities.WriteToStream(oMS, (byte) 0x03); // [EOP (1 Byte) 0x03]
				bRet = oMS.ToArray();
				#region Update the length bytes
				bConvert = new byte[4];
				bConvert = BitConverter.GetBytes(bRet.Length);
				bRet[5] = bConvert[0];
				bRet[6] = bConvert[1];
				#endregion
				string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
				if(_log != null) _log.Info(sData);
			}
			catch(System.Exception ex)
			{
				if(_log != null) 
					_log.Error("MTData.MDTInterface.cYellowCouriersPacketData BuildOutgoingPacket()", ex);
			}
			return bRet;
		}

		public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
		{
			// Special Field Defs
			// [GPS] = [LSC (1 Byte)] [Lat (4 Bytes)] [Long (4 Bytes)] [GPS Time  (6 Bytes)] [Speed (1 Byte)] [Heading (2 Bytes)] [DeviceTimer (2 Bytes)][Distance (4 Bytes)] 
			// [Header] = [MobileID (4 Bytes)][VehicleID (2 Bytes)][FleetID (1 Bytes)][GPS (24 Bytes)][DriverID (4 Byte Ascii)]
			// [Packet] = [CmdType (1 Byte)][Data][SBS (1 Byte)][Spare (1 Byte)]

			// 
			// If [CmdType] then [Data] =
			//	N - ACK / NACK - No Payload
			//	I - Login Packet - [Login Password (4 Byte Ascii)][MessageListID (1 Byte)]
			//	O - Logout Packet - No Payload
			//	A - Job Accept Packet - [Job ID (4 Bytes)]
			//	S - Job Stop Complete Packet  - 
			//				[Sub Section] = [Sub Cmd Type (1 Byte)][Sub Cmd Data]
			//				If [Sub Cmd Type] then [Sub Cmd Data] =
			//					0x0A - Barcodes - ([Barcode (Var Ascii)][Break (1 Byte 0x0C)][Quantity (1 Byte)])[Break (1 Byte 0x0B) Or (1 Byte 0x0A)]
			//					0x0C - Job Leg Complete - [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]
			//					0x0B - Signature Data - [Length (2 bytes)][Signature Data ([Length] Bytes)]
			//		[Job Stop Complete Packet]  = [Job ID (4 Bytes)][JobStop_Number (1 Bytes)]([Sub Section X])[LegComplete_CompleteStatus (1 Byte)]
			//	C - Job Complete Packet - [Job ID (4 Bytes)]
			//	M - Message Packet  - [SubCmd][Sub Cmd Data]
			//		If [SubCmd] then [Sub Cmd Data] = 
			//			F - Incoming Freeform Message Packet - [Sub Cmd Data] = [Message (Var Ascii)][Break (1 Byte 0x06)]
			//			P - Incoming Predefined Message Packet - [Sub Cmd Data] = [MessageID (1 Byte)]
			//			R - Message Read by Driver Packet - [Sub Cmd Data] = [MessageID (4 Byte)]
			//	B - Arrive Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
			//	D - Depart Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
			//	d - Request Data Segment - [Send Segment (4 Bytes)]
			#region Local Variables
			byte bValue = (byte) 0x00;
			short isValue = 0;
			int iPosition = 0;
			int iDay = 0;
			int iMonth = 0;
			int iYear = 0;
			int iHour = 0;
			int iMinute = 0;
			int iSecond = 0;
			string sTemp = "";
			bool bsubavailable = false;
			byte [] bTempArray = new byte[1];
			#endregion

			try
			{
				#region Get the command type
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				
				this.CommandType = Convert.ToString((char) bValue);
				#endregion	
				#region Get the Fleet, Vehicle and Mobile IDs
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
				iMobileID = (int)isValue;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
				VehicleID = (int) isValue;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				FleetID = (int) bValue;
				#endregion
				#region [GPS]
				this.GPSPosition = new cGPSData();
				// LSC Byte
				iPosition++;
				// Lat
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref bTempArray);
				this.GPSPosition.dLatitude = LatLonConversion.ConvLatLon(bTempArray);
				// Long
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref bTempArray);
				this.GPSPosition.dLongitude = LatLonConversion.ConvLatLon(bTempArray);
				#region Get GPS Time string
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				iDay = (int) bValue;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				iMonth = (int) bValue;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				iYear = ((int) bValue) + 2000;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				iHour = (int) bValue;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				iMinute = (int) bValue;
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				iSecond = (int) bValue;
				try
				{
					this.GPSPosition.dtGPSTime= new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
				}
				catch(System.Exception)
				{
				}
				#endregion
				// Speed
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
				this.GPSPosition.iSpeed = (int) bValue;
				// Heading
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
				this.GPSPosition.iHeading = (int) isValue;
				// Device Time is Seconds
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
				this.DeviceTimeSeconds = (int) isValue;
				//distance odo
				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Distance);
				#endregion
				#region ready the subcommand if availabe
				switch (this.CommandType)
				{

					case "M":
					case "R":
					case "S":
					case "K":
					case "d":
						bsubavailable = true;
						break;

					default:
						bsubavailable = false;
						break;
				}
				if (bsubavailable == true)
				{
					PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
					this.SubCommandType = Convert.ToString((char)bValue);
				}
				#endregion
				#region Get the Driver ID
				try
				{
					int temposition = iPosition;
					PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0 , ref sTemp);
					if (sTemp.Length > 6)
					{					
						sTemp = sTemp.Substring(0, 6);
					}
					iPosition = temposition + 6;
					this.DriverID = Convert.ToInt32(sTemp);
				}
                catch (System.Exception)
				{
					this.DriverID = 0;
				}

				#endregion
				switch(CommandType)
				{
					case "N":	// N - ACK / NACK - No Payload
						#region	ACK / NACK
						if(_log != null) _log.Info("Decoding ACK/NAK Packet");
						#endregion
						break;
					case "R":
						#region Request Jobs Keep Alive
						if(_log != null) _log.Info("Decoding Request for Jobs Packet (KeepAlive)");
                        //PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);
                        //PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        //JobStop_Number = (int)bValue;
						#endregion
						break;
					case "I":	// I - Login Packet - [Login Password (4 Byte Ascii)][MessageListID (1 Byte)]
						#region Login Packet
						if(_log != null) _log.Info("Decoding Login Packet");
						
						//password
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int) 4, ref sTemp);
						this.Login_Password = Convert.ToInt32(sTemp);
						
						//predefined currently stored message list id
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.PreDefined_MessageListID = (int) bValue;

						//delay reason list currently stored on the mdt
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.DelayReason_ListID = (int) bValue;

						//No sign reason list currently stored on the mdt
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.NoSignReason_ListID = (int)bValue;

						//Loing question list id stored on the PDT
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.LogonQuestion_ListID = (int)bValue;	
					
						//phone book id stored on the PDT	
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.PhoneBook_ListID = (int)bValue;

						//PDT serial number
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, 9, ref this.PDT_SerialNumber);
						
						//firwmare version major,
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.FirwmareVerMajor = (int) bValue;
						
						//firmware version minor,
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.FirmwareVerMinor = (int) bValue;
						
						//hardware version major,
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.HardwareVerMajor = (int) bValue;
						
						//hardware version minor,
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.HardwareVerMinor = (int) bValue;

						//Battery percentage
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						this.BatteryPercentage = (int) bValue;
						
						#endregion
						break;
					case "L":
						#region Logon response
						if (_log != null) _log.Info("Decoding Logon Response Packet");

						#endregion
						break;
					case "O":	// O - Logout Packet - No Payload
						#region Logout Packet
						if(_log != null) _log.Info("Decoding Logout Packet");
						#endregion
						break;
					case "A":		// A - Job Accept Packet - [Job ID (4 Bytes)]
						#region Job Accept Packet
						if(_log != null) _log.Info("Decoding Job Leg Accept");
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);
						#endregion
						break;
					case "S":	
						#region Job Stop Complete Packet
						// S - Job Stop Complete Packet  - 
						//			[Sub Section] = [Sub Cmd Type (1 Byte)][Sub Cmd Data]
						//			If [Sub Cmd Type] = 
						//				0x0A - Barcodes - ([Barcode X (Var Ascii)][Break (1 Byte 0x0C)][Quantity X (1 Byte)])[Break (1 Byte 0x0B) Or (1 Byte 0x0A)]
						//				0x0C - Job Leg Complete - [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]
						//				0x0B - Signature Data - [Length (2 bytes)][Signature Data ([Length] Bytes)]
						//		[Job Stop Complete Packet]  = [Job ID (4 Bytes)][JobStop_Number (1 Bytes)]([Sub Section X])[LegComplete_CompleteStatus (1 Byte)]
						if(_log != null) _log.Info("Decoding Job Leg Complete Packet");
						//jop id
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);			// [Job ID (4 Bytes)]
						
						//Leg number
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_Number (1 Bytes)]
						this.LegComplete_StopID = (int) bValue;

						//job leg task id
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobLeg_TaksID);	// [job leg task id (4 Bytes)]			
					
						//Delayed "Y" / "N"
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [Delayed (1 Bytes)]
						this.LegComplete_Delay = bValue;

						//Delay Reason
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [Delay Reason (1 Bytes)]
						this.LegComplete_DelayReason = (int)bValue;

						//POD
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_POD);							// [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]

						//No sign reason
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);			// [No Sign Reason (1 Bytes)]
						this.LegComplete_NoSignReson = (int)bValue;

						//Signature
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
						if (bValue== 0x0B)
						{
							//encode the vector image into a gif
							
							//signature legnth
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);																	// [Length (2 Bytes)]
							//read vector signature into byte array
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)isValue, ref LegComplete_Signature);				// [Signature Data ([Length] Bytes)]
							//read end byte of 0x03
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);																	// [LegComplete_CompleteStatus (1 Byte)]
						
							//decode signature into a gif format
							if (LegComplete_Signature.Length > 0)
							{
								string path = System.Configuration.ConfigurationManager.AppSettings["PicturePath"];
                                if (path.EndsWith("/"))
                                    path += "/";

								try
								{


									LegComplete_SigAddress = string.Format("{0}-{1}-{2}-{3}.gif", DateTime.Now.ToString("yyMMddHHmmss"),
									                                       Convert.ToString(iMobileID), Convert.ToString(JobID),
									                                       Convert.ToString(LegComplete_StopID));

                                    string DefaultSignature = ConfigurationManager.AppSettings["UseBackgroundGIFforSignatures"];
                                    if (!DefaultSignature.Contains(":"))
                                    {
                                        string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                                        exePath = System.IO.Path.GetDirectoryName(path);
                                        DefaultSignature = exePath + "\\" + DefaultSignature;
                                    }

                                    cVectorSignature oSig = new cVectorSignature( false, 80, 300, 200, 600, path, DefaultSignature, 0, 0, 0.5, true,
									                                             100000, true, System.Drawing.Color.White, true, 10);
									oSig.FileName = LegComplete_SigAddress;
									LegComplete_Signature = oSig.TranslateSignature(LegComplete_Signature, (byte) 0x0B);
										//convert the signature into gif file
								}
								catch (Exception ex)
								{
									if (_log != null) _log.Info("Decode of signature:" + ex);
								}

							}
						}
						else if (bValue == 0x0C)
						{
							//copy the already compliled gif image to the byte array
							//read the length of the gif image
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);																	// [Length (2 Bytes)]
							//copy the gif image into the signature byte array
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)isValue, ref LegComplete_Signature);				// [Signature Data ([Length] Bytes)]
							PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);																	// [LegComplete_CompleteStatus (1 Byte)]
						}
						#endregion
						break;				
					case "C":	// C - Job Complete Packet - [Job ID (4 Bytes)]
						#region Job Complete Packet
						if(_log != null) _log.Info("Decoding Job Complete Packet");
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);
						#endregion
						break;				
					case "M":
						#region Incoming driver message from driver
						// M - Message Packet  - [SubCmd][Sub Cmd Data]
						//		If [SubCmd] = 
						//			F - Incoming Freeform Message Packet - [Sub Cmd Data] = [Message (Var Ascii)][Break (1 Byte 0x06)]
						//			P - Incoming Predefined Message Packet - [Sub Cmd Data] = [MessageID (1 Byte 0x06)]
						//			R - Message Read by Driver Packet - [Sub Cmd Data] = [MessageID (4 Byte)]
						if(_log != null) _log.Info("Decoding Message from unit Packet");
						switch (SubCommandType) 
						{
							case "F":			//free formed messages
								//related job id
								//PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_RelatedJobID);
								//free hand message text
								PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x06, ref Message_Text);	// [Message (Var Ascii)][Break (1 Byte 0x06)]
								break;
							case "P":			//predefined message
								//related job id
							//	PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_RelatedJobID);
								// predefined mesage List ID
								PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);										//  [MessageID (1 Byte)]
								this.PreDefined_MessageListID = (int) bValue;
								// predefined mesage ID
								PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);										//  [MessageID (1 Byte)]
							//	this.PreDefined_MessageID = (int) bValue;
								break;
							case "R":			//message read by driver
								PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Message_ID);						// [MessageID (4 Byte)]
								break;
						}
						#endregion
						break;				
					case "B":	// B - Arrive Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
					case "D":	// D - Depart Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
						#region Arrive or Depart Job Leg Packet
						if(_log != null) _log.Info("Decoding Arrive or Depart Job Leg Packet");
						//job id
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);		// [Job ID (4 Bytes)]
						//stop number
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_Number (1 Byte)]
			//			this.JobStop_Number = (int)bValue;
						//type - auto or entered
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_ArriveDepartType (1 Byte)]
			//			this.JobStop_ArriveDepartType = Convert.ToString((char) bValue);
						#endregion
						break;				
					case "d":	// d - Request Data Segment - [Send Segment (4 Bytes)]
						#region Request a Download Packet
						if(_log != null) _log.Info("Decoding Download Request Packet");
						switch (this.SubCommandType)  
						{
							case "r":			//request new download parcel
								this.SubCommandType = "d";
								PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.RequestedParcelNo);						
								break;
							case "v":			//verify							
								break;
							default:
								break;
						}
						#endregion
						break;
					case "H":
						#region Driver Channel Selection Update
		//				PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref zRadioChannelSelection);
						#endregion
						break;
					default :
						break;
				}
			}
			catch(System.Exception ex)
			{
				if(_log != null) 
				{
					if(bMsg == null)
						_log.Error(sClassName + "TranslateMsgToHost(byte[] bMsg = null, string sUnitID = '" + sUnitID + "')", ex);
					else
						_log.Error(sClassName + "TranslateMsgToHost(byte[] bMsg, string sUnitID = '" + sUnitID + "')", ex);
				}
			}
			bRawData = bMsg;
			return "";
		}

		public string GetPacketTypeName()
		{
			string sRet = "";
			switch(CommandType)
			{
				case "N":
					sRet = "ACK / NACK";
					break;
				case "I":
					sRet = "Login Packet";
					break;
				case "O":
					sRet = "Logout Packet";
					break;
				case "A":
					sRet = "Job Accept Packet";
					break;
				case "S":	
					sRet = "Job Stop Complete Packet";
					break;
				case "C":						
					sRet = "Job Complete Packet";
					break;
				case "M":
					if(SubCommandType == "F")
						sRet = "Incoming Freeform Message Packet";
					else if(SubCommandType == "P")
						sRet = "Incoming Predefined Message Packet";
					else if(SubCommandType == "R")
						sRet = "Message Read by Driver Packet";
					break;
				case "B":			//Arrive at job leg
				case "D":
					sRet = "Depart Job Leg Packet";
					break;
				case "K":
					sRet = "Pre-Defined Message List Packet";
					break;
				case "d":			
					sRet = "Request a Download Packet";
					break;
				case "R":
					sRet = "Request jobs packet";
					break;
				case "r":
					sRet = "Reset Request Packet";
					break;
				case "T":
					sRet = "Time UTC update packet for the 5040";
					break;
				default :
					sRet = "MTData Terminal Interfcae Packet";
					break;
			}
			return sRet;
		}

        public class JobLegProduct
        {
            #region  Private vars
			private string _productID;
            private string _description;
            private byte _quantity;

            #endregion

            #region Properties
			public string ProductID
            {
                get
                {
                    return _productID;
                }
                set
                {
                    _productID = value;

                }
            }
            public string Description
            {
                get
                {
                    return _description;
                }
                set
                {
                    _description = value;
                }
            }

            public byte Quantity
            {
                get
                {
                    return _quantity;
                }
                set
                {
                    _quantity = value;
                }
            }




            #endregion
        }


        public class PhoneBookEntry
        {
            #region  Private vars
            private string _name;
            private string _number;

            #endregion

            #region Properties
 
            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }

            public string Number
            {
                get
                {
                    return _number;
                }
                set
                {
                    _number = value;
                }
            }
            #endregion       
        }

     
	}
}

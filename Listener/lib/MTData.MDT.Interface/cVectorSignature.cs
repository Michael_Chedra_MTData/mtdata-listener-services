using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Collections;
using System.IO;
using System.Runtime.InteropServices;
using log4net;


namespace MTData.MDT.Interface
{
	public class cVectorSignature
	{
        ILog oLogging = LogManager.GetLogger(typeof(cVectorSignature));

        private const string sClassName = "cVectorSignature.";
		public class RGB
		{
			public const short R = 2;
			public const short G = 1;
			public const short B = 0;
		};

		internal class Win32
		{
			// memcpy - copy a block of memory
			[DllImport("ntdll.dll")]
			public static extern IntPtr memcpy(
				IntPtr dst,
				IntPtr src,
				int count);
			[DllImport("ntdll.dll")]
			public static extern unsafe byte * memcpy(
				byte * dst,
				byte * src,
				int count);

			// memset - fill memory with specified values
			[DllImport("ntdll.dll")]
			public static extern IntPtr memset(
				IntPtr dst,
				int filler,
				int count);
		}


		public class cVectorSignatureConfig
		{
			public bool LogSignatureData = false;
			public int MaxHeight = 0;
			public int MaxWidth = 0;
			public int MaxVectorHeight = 0;
			public int MaxVectorWidth = 0;
			public string PicturePath = "";
			public string DefaultImagePath = "";
			public int XCorrection = 0;
			public int YCorrection = 0;
			public double ResizeFactor = 1;
			public bool IncreaseContrast = true;
			public uint ContrastTolerance = 1;
			public bool MakeSignatureTransparent = false;
			public Color SignatureBackgroundColor = Color.White;
			public bool SaveImageFile = false;
			public int iPenSize = 2;
		}

		private cVectorSignatureConfig _config = null;

		/// <summary>
		/// Prepare te signature converter for use.
		/// </summary>
		/// <param name="config"></param>
        public cVectorSignature(cVectorSignatureConfig config)
		{
            
			_config = (config != null)?config:new cVectorSignatureConfig();
			
		}

        public cVectorSignature(bool LogSignatureData, int MaxHeight, int MaxWidth, int MaxVectorHeight, int MaxVectorWidth, string PicturePath, string DefaultImagePath, int XCorrection, int YCorrection, double ResizeFactor, bool IncreaseContrast, uint ContrastTolerance, bool MakeSignatureTransparent, Color SignatureBackgroundColor, bool SaveImageFile, int iPenSize)
		{
            
			_config = new cVectorSignatureConfig();
			_config.LogSignatureData = LogSignatureData;
			_config.MaxHeight = MaxHeight;
			_config.MaxWidth = MaxWidth;
			_config.MaxVectorHeight = MaxVectorHeight;
			_config.MaxVectorWidth = MaxVectorWidth;
			_config.PicturePath = PicturePath;
			_config.DefaultImagePath = DefaultImagePath;
			_config.XCorrection = XCorrection;
			_config.YCorrection = YCorrection;
			_config.ResizeFactor = ResizeFactor;
			_config.IncreaseContrast = IncreaseContrast;
			_config.ContrastTolerance = ContrastTolerance;
			_config.MakeSignatureTransparent = MakeSignatureTransparent;
			_config.SignatureBackgroundColor = SignatureBackgroundColor;
			_config.SaveImageFile = SaveImageFile;
			_config.iPenSize = iPenSize;


		}

		private string _fileName = "";
		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{			
				_fileName = value;
			}
		}
		
		public byte[] TranslateSignature(byte[] bData, string fileName )
		{
			_fileName = fileName;
			return TranslateSignature(bData, (byte) 0x0B);
		}

		public byte[] TranslateSignature(byte[] bData, byte bSignatureType)
		{
			#region Variables
			#region Drawing Variables
			System.Drawing.Bitmap oBMP = null;
			System.Drawing.Bitmap oNewBMP = null;
			System.Drawing.Image image = null;
			System.Drawing.Graphics g = null;
			System.Drawing.Pen myPen = new Pen(Color.Black, _config.iPenSize);
			System.Drawing.Color cBackGroundColor = _config.SignatureBackgroundColor;
			ArrayList oPoints = null;
			System.Drawing.Point bPoint = new Point(0,0);
			ArrayList oPostProcess = new ArrayList();
			#endregion
			FileStream oNewFS = null;

			byte[] bResult = new byte[1];
			byte[] bXStart = new byte[4];
			byte[] bYStart = new byte[4];
			byte[] bYChange = new byte[4];
			byte[] bXChange = new byte[4];
			byte[] bLoadData = null;

			int iX = 0;
			int iY = 0;
			int iXChange = 0;
			int iYChange = 0;
			int iNextX = 0;
			int iNextY = 0;
			int iMode = 0;
			int iMaxHeight = 2500;
			int iMaxWidth = 8000;
			int iBytes = 0;
			int iTop = 0;
			int iLeft = 0;
			int iRight = 0;
			int iBottom = 0;
			int iResizeHeight = 0;
			int iResizeWidth = 0;
			int iXOffset = 0;
			int iYOffset = 0;

			double dResizeX = 0;
			double dResizeY = 0;
			double dResizeFactor = 0;

			string sFilename = "";
			#endregion

			if (bSignatureType != (byte) 0x0B)
				return TranslateSignatureV1(bData);

            try
            {
                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 2) - Pre-processing data");

                iMode = 0;
                // Setup the var to find the top/left, bottom/right of the signature data
                iTop = iMaxHeight;
                iLeft = iMaxWidth;
                iRight = 0;
                iBottom = 0;
                for (int iPos = 1; iPos < bData.Length; iPos++)
                {
                    #region Process bytes
                    try
                    {
                        // Mode 0 = Read Starting X byte 1
                        // Mode 1 = Read Starting X byte 2
                        // Mode 2 = Read Starting Y byte 1
                        // Mode 3 = Read Starting Y byte 2
                        // Mode 4 = Read Y change byte
                        // Mode 5 = Read X change byte

                        switch (iMode)
                        {
                            case 0:
                                #region Read Starting X byte 1
                                bXStart[0] = bData[iPos];
                                iMode = 1;
                                break;
                                #endregion
                            case 1:
                                #region Read Starting X byte 2
                                bXStart[1] = bData[iPos];
                                iX = BitConverter.ToInt32(bXStart, 0);
                                if (iX > iMaxWidth)
                                {
                                    #region The signature data is not valid, clean up and return.
                                    return bResult;
                                    #endregion
                                }
                                if (iX == 0)
                                {
                                    // Double Pen up
                                    bXStart[0] = (byte)0x00;
                                    bXStart[1] = (byte)0x00;
                                    iMode = 0;
                                }
                                else
                                {
                                    if (iX < iLeft) iLeft = iX;
                                    if (iX > iRight) iRight = iX;

                                    bXStart[0] = (byte)0x00;
                                    bXStart[1] = (byte)0x00;
                                    iMode = 2;
                                }
                                break;
                                #endregion
                            case 2:
                                #region Read Starting Y byte 1
                                bYStart[0] = bData[iPos];
                                iMode = 3;
                                break;
                                #endregion
                            case 3:
                                #region Read Starting Y byte 2
                                bYStart[1] = bData[iPos];
                                iY = BitConverter.ToInt32(bYStart, 0);
                                if (iY > iMaxHeight)
                                {
                                    #region The signature data is not valid, clean up and return.
                                    return bResult;
                                    #endregion
                                }
                                bYStart[0] = (byte)0x00;
                                bYStart[1] = (byte)0x00;

                                if (iY < iTop) iTop = iY;
                                if (iY > iBottom) iBottom = iY;

                                bPoint = new Point(iX, iY);
                                oPostProcess.Add(bPoint);
                                iMode = 4;
                                break;
                                #endregion
                            case 4:
                                #region Read Y change byte
                                bYChange[0] = bData[iPos];
                                iYChange = BitConverter.ToInt32(bYChange, 0);
                                bYChange[0] = (byte)0x00;
                                iMode = 5;
                                break;
                                #endregion
                            case 5:
                                #region Read X change byte and draw the vector
                                bXChange[0] = bData[iPos];
                                iXChange = BitConverter.ToInt32(bXChange, 0);
                                bXChange[0] = (byte)0x00;

                                if (iYChange == 0 && iXChange == 0)
                                {
                                    #region Pen up
                                    oPostProcess.Add(new Point(0, 0));
                                    iMode = 0;
                                    #endregion
                                }
                                else
                                {
                                    if (iYChange == 1 && iXChange == 0)
                                    {
                                        #region Continue the same line, but the next values in the array are X,Y co-ordinates
                                        iMode = 0;
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Draw the next vector
                                        if (iXChange > 127)
                                            iNextX = iX + ((128 - (iXChange - 127)) * -1);
                                        else
                                            iNextX = iX + iXChange;

                                        if (iNextX < 0) iNextX = 0;
                                        if (iNextX >= iMaxWidth) iNextX = iMaxWidth - 1;

                                        if (iYChange > 127)
                                            iNextY = iY + ((128 - (iYChange - 127)) * -1);
                                        else
                                            iNextY = iY + iYChange;

                                        if (iNextY < 0) iNextY = 0;
                                        if (iNextY >= iMaxHeight) iNextY = iMaxHeight - 1;

                                        bPoint = new Point(iNextX, iNextY);
                                        oPostProcess.Add(bPoint);
                                        iX = iNextX;
                                        iY = iNextY;

                                        if (iX > iMaxWidth)
                                        {
                                            #region The signature data is not valid, clean up and return.
                                            return bResult;
                                            #endregion
                                        }

                                        if (iY > iMaxHeight)
                                        {
                                            #region The signature data is not valid, clean up and return.
                                            return bResult;
                                            #endregion
                                        }

                                        if (iX < iLeft) iLeft = iX;
                                        if (iX > iRight) iRight = iX;
                                        if (iY < iTop) iTop = iY;
                                        if (iY > iBottom) iBottom = iY;

                                        iNextX = 0;
                                        iNextY = 0;
                                        iYChange = 0;
                                        iXChange = 0;

                                        // Get the next Y change value
                                        iMode = 4;
                                        #endregion
                                    }
                                }
                                break;
                                #endregion
                        }
                    }
                    catch (System.Exception)
                    {
                        return bResult;
                    }
                    #endregion
                }

                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 2) - Post-processing data");
                #region Post Process Data
                Point bPrevPoint = new Point(0, 0);
                Point bCurPoint = new Point(0, 0);
                Point bNextPoint = new Point(0, 0);
                bool bXValid = false;
                bool bYValid = false;

                int iPrevX = 0;
                int iPrevY = 0;
                int iCurX = 0;
                int iCurY = 0;
                int iAvgX = 0;
                int iAvgY = 0;
                iNextX = 0;
                iNextY = 0;

                //			oLog.Info("X,Y");
                //			for(int X = 0; X < oPostProcess.Count; X++)
                //			{
                //				bCurPoint = (Point) oPostProcess[X];
                //				oLog.Info(Convert.ToString(bCurPoint.X) + "," + Convert.ToString(bCurPoint.Y));
                //			}

                int iStartXCorrection = _config.XCorrection * 2;
                int iStartYCorrection = _config.YCorrection * 2;

                oPoints = new ArrayList();

                for (int X = 0; X < oPostProcess.Count; X++)
                {
                    bCurPoint = (Point)oPostProcess[X];
                    if (oPostProcess.Count - 1 > X)
                        bNextPoint = (Point)oPostProcess[X + 1];
                    else
                        bNextPoint = new Point(0, 0);
                    if (X - 1 >= 0)
                        bPrevPoint = (Point)oPostProcess[X - 1];
                    else
                        bPrevPoint = new Point(0, 0);

                    iPrevX = bPrevPoint.X;
                    iPrevY = bPrevPoint.Y;
                    iCurX = bCurPoint.X;
                    iCurY = bCurPoint.Y;
                    iNextX = bNextPoint.X;
                    iNextY = bNextPoint.Y;

                    if (iCurX != 0 || iCurY != 0)
                    {
                        bXValid = true;
                        bYValid = true;
                        if (iPrevX == 0 && iPrevY == 0)
                        {
                            if (iNextX != 0 || iNextY != 0)
                            {
                                #region No Previous Point in this line
                                //							if (Math.Abs(Convert.ToDecimal(iCurX) - Convert.ToDecimal(iNextX)) > iStartXCorrection)
                                //							{
                                //								if (iCurX - iNextX < 0)
                                //								{
                                //									// If NextX is greater
                                //									iCurX = iNextX - iStartXCorrection;
                                //								}
                                //								else
                                //								{
                                //									iCurX = iNextX + iStartXCorrection;
                                //								}
                                //							}
                                //							if (Math.Abs(Convert.ToDecimal(iCurY) - Convert.ToDecimal(iNextY)) > iStartYCorrection)
                                //							{
                                //								if (iCurY - iNextY < 0)
                                //								{
                                //									// If NextY is greater
                                //									iCurY = iNextY - iStartYCorrection;
                                //								}
                                //								else
                                //								{
                                //									iCurY = iNextY + iStartYCorrection;
                                //								}
                                //							}
                                oPoints.Add(new Point(iCurX, iCurY));
                                oPostProcess[X] = new Point(iCurX, iCurY);
                                #endregion
                            }
                            else
                            {
                                oPoints.Add(new Point(iCurX, iCurY));
                                oPoints.Add(new Point(iCurX + 1, iCurY));
                                oPoints.Add(new Point(0, 0));
                            }
                        }
                        else
                        {
                            if (iNextX == 0 && iNextY == 0)
                            {
                                if (iPrevX != 0 || iPrevY != 0)
                                {
                                    #region No Next Point is this line
                                    //								if (Math.Abs(Convert.ToDecimal(iCurX) - Convert.ToDecimal(iPrevX)) > iStartXCorrection)
                                    //								{
                                    //									if (iCurX - iPrevX < 0)
                                    //									{
                                    //										// If NextX is greater
                                    //										iCurX = iPrevX - iStartXCorrection;
                                    //									}
                                    //									else
                                    //									{
                                    //										iCurX = iPrevX + iStartXCorrection;
                                    //									}
                                    //								}
                                    //								if (Math.Abs(Convert.ToDecimal(iCurY) - Convert.ToDecimal(iPrevY)) > iStartYCorrection)
                                    //								{
                                    //									if (iCurY - iPrevY < 0)
                                    //									{
                                    //										// If NextY is greater
                                    //										iCurY = iPrevY - iStartYCorrection;
                                    //									}
                                    //									else
                                    //									{
                                    //										iCurY = iPrevY + iStartYCorrection;
                                    //									}
                                    //								}
                                    oPoints.Add(new Point(iCurX, iCurY));
                                    oPostProcess[X] = new Point(iCurX, iCurY);
                                    #endregion
                                }
                            }
                            else
                            {
                                #region  This is a previous and a next point
                                if (Math.Abs(Convert.ToDecimal(iCurX) - Convert.ToDecimal(iPrevX)) > _config.XCorrection)
                                {
                                    if (Math.Abs(Convert.ToDecimal(iNextX) - Convert.ToDecimal(iCurX)) > _config.XCorrection)
                                    {
                                        bXValid = false;
                                    }
                                }
                                if (Math.Abs(Convert.ToDecimal(iCurY) - Convert.ToDecimal(iPrevY)) > _config.YCorrection)
                                {
                                    if (Math.Abs(Convert.ToDecimal(iNextY) - Convert.ToDecimal(iCurY)) > _config.YCorrection)
                                    {
                                        bYValid = false;
                                    }
                                }
                                if (!bXValid)
                                {
                                    iAvgX = (iPrevX + iNextX) / 2;
                                    iCurX = iAvgX;
                                }
                                if (!bYValid)
                                {
                                    iAvgY = (iPrevY + iNextY) / 2;
                                    iCurY = iAvgY;
                                }
                                #endregion
                                oPoints.Add(new Point(iCurX, iCurY));
                                oPostProcess[X] = new Point(iCurX, iCurY);
                            }
                        }
                    }
                    else
                    {
                        oPoints.Add(new Point(0, 0));
                    }
                }
                #endregion

                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 2) - Setting up default image - '" + _config.DefaultImagePath + "'");
                #region Setup default image
                try
                {
                    // Create a new bitmap in memory.
                    //				oBMP = new System.Drawing.Bitmap(iMaxWidth, iMaxHeight);
                    image = Image.FromFile(_config.DefaultImagePath);
                    //				image = Bitmap.FromHbitmap(oBMP.GetHbitmap());
                    g = Graphics.FromImage(image);
                    SolidBrush oBrush = new SolidBrush(cBackGroundColor);
                    g.FillRectangle(oBrush, 0, 0, iMaxWidth, iMaxHeight);

                    //				oBMP.Dispose();
                    //				oBMP = null;
                }
                catch (System.Exception exLoadDefaultImage)
                {
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignature(byte[] bData, byte bSignatureType)", exLoadDefaultImage);

                    if (g != null)
                    {
                        g.Dispose();
                        g = null;
                    }
                    if (image != null)
                    {
                        image.Dispose();
                        image = null;
                    }
                    GC.Collect();

                    return bResult;
                }
                #endregion

                ArrayList oCurLine = new ArrayList();
                for (int X = 0; X < oPoints.Count; X++)
                {
                    bCurPoint = (Point)oPoints[X];
                    if (bCurPoint.X == 0 && bCurPoint.Y == 0)
                    {
                        // Pen up.
                        if (oCurLine.Count == 4)
                        {
                            System.Drawing.Point[] bLinePoints = new Point[4];
                            bLinePoints[0] = (System.Drawing.Point)oCurLine[0];
                            bLinePoints[1] = (System.Drawing.Point)oCurLine[1];
                            bLinePoints[2] = (System.Drawing.Point)oCurLine[2];
                            bLinePoints[3] = (System.Drawing.Point)oCurLine[3];
                            g.DrawBeziers(myPen, bLinePoints);
                            oCurLine = new ArrayList();
                        }
                        else
                        {
                            if (oCurLine.Count == 3)
                            {
                                g.DrawLine(myPen, (System.Drawing.Point)oCurLine[oCurLine.Count - 3], (System.Drawing.Point)oCurLine[oCurLine.Count - 2]);
                                g.DrawLine(myPen, (System.Drawing.Point)oCurLine[oCurLine.Count - 2], (System.Drawing.Point)oCurLine[oCurLine.Count - 1]);
                            }
                            else
                            {
                                if (oCurLine.Count == 2)
                                {
                                    g.DrawLine(myPen, (System.Drawing.Point)oCurLine[oCurLine.Count - 2], (System.Drawing.Point)oCurLine[oCurLine.Count - 1]);
                                }
                            }
                        }
                        oCurLine = new ArrayList();
                    }
                    else
                    {
                        oCurLine.Add(bCurPoint);
                        if (oCurLine.Count == 4)
                        {
                            System.Drawing.Point[] bLinePoints = new Point[4];
                            bLinePoints[0] = (System.Drawing.Point)oCurLine[0];
                            bLinePoints[1] = (System.Drawing.Point)oCurLine[1];
                            bLinePoints[2] = (System.Drawing.Point)oCurLine[2];
                            bLinePoints[3] = (System.Drawing.Point)oCurLine[3];
                            g.DrawBeziers(myPen, bLinePoints);
                            oCurLine = new ArrayList();
                            oCurLine.Add(bCurPoint);
                        }
                    }
                }

                #region Convert the image to a standard size GIF
                try
                {
                    // Pad out the image size by 2 pixels in each direction
                    iTop = iTop - 2;
                    if (iTop < 0) iTop = 0;
                    iLeft = iLeft - 2;
                    if (iLeft < 0) iLeft = 0;
                    iBottom = iBottom + 2;
                    if (iBottom > iMaxHeight) iBottom = iMaxHeight;
                    iRight = iRight + 2;
                    if (iRight > iMaxWidth) iRight = iMaxWidth;

                    int iWidth = iRight - iLeft;
                    int iHeight = iBottom - iTop;

                    if (iHeight > iMaxHeight || iWidth > iMaxWidth)
                    {
                        #region The signature data is not valid, clean up and return.
                        if (g != null)
                        {
                            g.Dispose();
                            g = null;
                        }
                        if (image != null)
                        {
                            image.Dispose();
                            image = null;
                        }
                        if (myPen != null)
                        {
                            myPen.Dispose();
                            myPen = null;
                        }
                        if (oBMP != null)
                        {
                            oBMP.Dispose();
                            oBMP = null;
                        }
                        GC.Collect();
                        return bResult;
                        #endregion
                    }

                    #region Work out the resize factor
                    dResizeX = Convert.ToDouble(iWidth) / Convert.ToDouble(_config.MaxVectorWidth);
                    dResizeY = Convert.ToDouble(iHeight) / Convert.ToDouble(_config.MaxVectorHeight);

                    // Use the larger of the two resize factors
                    if (dResizeX > dResizeY)
                        dResizeFactor = dResizeX;
                    else
                        dResizeFactor = dResizeY;

                    iResizeHeight = Convert.ToInt32(Math.Round(Convert.ToDouble(iHeight) / dResizeFactor, 0));
                    iResizeWidth = Convert.ToInt32(Math.Round(Convert.ToDouble(iWidth) / dResizeFactor, 0));
                    #endregion

                    #region Copy the signature from the image to a new bitmap
                    oNewBMP = new Bitmap(iWidth, iHeight);
                    Graphics gr1 = Graphics.FromImage(oNewBMP);
                    System.Drawing.Rectangle oDestR = new Rectangle(0, 0, iWidth, iHeight);
                    System.Drawing.Rectangle oSrcR = new Rectangle(iLeft, iTop, iWidth, iHeight);
                    GraphicsUnit units = GraphicsUnit.Pixel;
                    gr1.DrawImage(image, oDestR, oSrcR, units);
                    gr1.Save();
                    #region Clean up objects
                    if (image != null)
                    {
                        image.Dispose();
                        image = null;
                    }
                    gr1.Dispose();
                    gr1 = null;
                    GC.Collect();
                    #endregion
                    #endregion

                    #region Resize the new bitmap using the new calculated size
                    oNewBMP = ResizeImage(oNewBMP, iResizeWidth, iResizeHeight);
                    for (int X = 0; X < oNewBMP.Width; X++)
                    {
                        oNewBMP.SetPixel(X, 0, cBackGroundColor);
                        //oNewBMP.SetPixel(X, oNewBMP.Height -1, cBackGroundColor);
                    }
                    for (int Y = 0; Y < oNewBMP.Height; Y++)
                    {
                        oNewBMP.SetPixel(0, Y, cBackGroundColor);
                        //oNewBMP.SetPixel(oNewBMP.Width -1, Y, cBackGroundColor);
                    }
                    GC.Collect();
                    #endregion

                    #region Invert and centre the image on a fixed size bitmap
                    oBMP = new Bitmap(_config.MaxVectorWidth + 1, _config.MaxVectorHeight + 1);
                    iXOffset = ((_config.MaxVectorWidth - iResizeWidth) / 2) - 1;
                    iYOffset = ((_config.MaxVectorHeight - iResizeHeight) / 2) - 1;
                    if (iXOffset < 0) iXOffset = 0;
                    if (iYOffset < 0) iYOffset = 0;

                    // Fill in the edges
                    for (int X = 0; X < oBMP.Width; X++)
                    {
                        for (int Y = 0; Y < oBMP.Height; Y++)
                        {
                            //if (Y <= iYOffset + 3 || Y >= oBMP.Height - (iYOffset + 3) || X <= (iXOffset + 3) || X >= oBMP.Width - (iXOffset + 3))
                            oBMP.SetPixel(X, Y, cBackGroundColor);
                        }
                    }

                    iY = _config.MaxVectorHeight - iYOffset;
                    iX = iXOffset;

                    for (int X = 0; X < iResizeWidth; X++)
                    {
                        iY = _config.MaxVectorHeight - iYOffset;
                        for (int Y = 0; Y < iResizeHeight; Y++)
                        {
                            try
                            {
                                oBMP.SetPixel(iX, iY, oNewBMP.GetPixel(X, Y));
                            }
                            catch (System.Exception ex)
                            {
                                Console.Write(ex.Message);
                            }
                            iY--;
                        }
                        iX++;
                    }
                    oNewBMP.Dispose();
                    oNewBMP = null;
                    GC.Collect();

                    #endregion

                }
                catch (System.Exception exFileCreate)
                {
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignature(byte[] bData, byte bSignatureType)", exFileCreate);
                }
                #endregion

                if (_config.MakeSignatureTransparent)
                {
                    try
                    {
                        MemoryStream memStrm = new MemoryStream();
                        oBMP.Save(memStrm, ImageFormat.Gif);
                        oBMP = new Bitmap(memStrm);
                        oBMP = MakeTransparent(oBMP, cBackGroundColor);
                    }
                    catch (System.Exception exTransparent)
                    {
                        if (oLogging != null) oLogging.Error(sClassName + "TranslateSignature(byte[] bData, byte bSignatureType)", exTransparent);
                    }
                }

                #region Write the data to GIF format and read back the bytes to pass to external interface.
                try
                {
                    if (_fileName == null)
                        _fileName = "";
                    sFilename = _fileName;
                    if (sFilename.Length > 0)
                        sFilename = _config.PicturePath + sFilename;
                    else
                        sFilename = _config.PicturePath + System.DateTime.Now.Year.ToString() + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + System.DateTime.Now.Millisecond.ToString().PadLeft(3, '0') + ".gif";


                    if (System.IO.File.Exists(sFilename))
                        oNewFS = new FileStream(sFilename, System.IO.FileMode.Truncate);
                    else
                        oNewFS = new FileStream(sFilename, System.IO.FileMode.CreateNew);
                    try
                    {
                        oBMP.Save(oNewFS, System.Drawing.Imaging.ImageFormat.Gif);
                    }
                    finally
                    {
                        oNewFS.Close();
                        oNewFS = null;
                    }
                }
                catch (System.Exception exFileCreate)
                {
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignature(byte[] bData, byte bSignatureType)", exFileCreate);
                }

                bLoadData = new byte[10000];
                iBytes = File.OpenRead(sFilename).Read(bLoadData, 0, 10000);

                bResult = new byte[iBytes];
                for (int iStrip = 0; iStrip < iBytes; iStrip++)
                    bResult[iStrip] = bLoadData[iStrip];

                #region Clean up
                bLoadData = null;
                if (oBMP != null)
                {
                    oBMP.Dispose();
                    oBMP = null;
                }
                GC.Collect();
                #endregion


                if (!_config.SaveImageFile)
                {
                    File.Delete(sFilename);
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "TranslateSignature(byte[] bData, byte bSignatureType)", ex);
            }
			return  bResult;
		}


		/// <summary>
		/// this code has been modified to enusre that the signature is transparent
		/// NO attempt to use any background GIF images will be entertained.
		/// Signature will be portrayed black on transparent background
		/// </summary>
		/// <param name="bData"></param>
		/// <returns></returns>
		public byte[] TranslateSignatureV1(byte[] bData)
		{		

			#region Local Vars
			byte bTemp = (byte) 0x00;
			byte bExamine = (byte) 0x00;
			byte[] bRet = new byte[1];
			byte[] bConvert = new byte[4];
			byte[] bWhiteSpace = new byte[1]; // (iWhiteBits bits)
			byte[] bBlackSpace = new byte[1]; // (iBlackBits bits)
			byte[] bBitCount = new byte[4];
			byte[] bDoIt = new byte[4];
			int iHeight = 0;
			int iWidth = 0;
			int iHeightOffset = 0;
			int iWidthOffset = 0;
			int iMaxHeight = _config.MaxHeight; 
			int iMaxWidth = _config.MaxWidth;
			int iBlackBits = 0;
			int iWhiteBits = 0;
			int iCount = 0;
			int iBitCount = 0;
			int iPixels = 0;
			int iBitMask = (int) 0x100;
			int iX = 0;
			int iY = 0;
			int iPaint = 0;
			double dResizeFactor = 0;
			System.Drawing.Bitmap oBMP = null;
			cCollection oCol = null;
			cCollection oConvertedCol = null;
			System.Drawing.Color cBackGroundColor = _config.SignatureBackgroundColor;
			byte[] bResult = null;
			bResult = new byte[1];
			bResult[0] = (byte)0x00;
			string sFilename = "";
			#endregion

			try
			{
                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Setting up Vars");
				try
				{
					#region Setup Vars
					oCol = new cCollection();
					oConvertedCol = new cCollection();
					if (_fileName == "")
						_fileName = Convert.ToString(System.DateTime.Now.Day) + Convert.ToString(System.DateTime.Now.Month) + Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Hour) + Convert.ToString(System.DateTime.Now.Minute) + Convert.ToString(System.DateTime.Now.Second) + Convert.ToString(System.DateTime.Now.Millisecond) + ".gif";
					sFilename = _config.PicturePath + _fileName;
					dResizeFactor = _config.ResizeFactor;

					iMaxHeight = Convert.ToInt32(Math.Round(Convert.ToDecimal(iMaxHeight * (dResizeFactor + 1)), 0));
					iMaxWidth = Convert.ToInt32(Math.Round(Convert.ToDecimal(iMaxWidth * (dResizeFactor + 1)), 0));

					#endregion
				}
				catch(System.Exception exSetupVars)
				{
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", exSetupVars);
				}

                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Measuring Signature Dimensions");
				try
				{
					#region Get the Image Size

					// Initialise the conversion byte array
					bConvert[0] = (byte) 0x00;
					bConvert[1] = (byte) 0x00;
					bConvert[2] = (byte) 0x00;
					bConvert[3] = (byte) 0x00;

					// Get the height and width.
					bConvert[0] = bData[1];
					iHeight = BitConverter.ToInt32(bConvert, 0);
					bConvert[0] = bData[2];
					iWidth = BitConverter.ToInt32(bConvert, 0);
					iWidth = iWidth * 8;  // Width is in bytes, so x8 to convert to pixels.

					// Get the encoding type
					bConvert[0] = bData[3];
					iWhiteBits = BitConverter.ToInt32(bConvert, 0);
					bConvert[0] = bData[4];
					iBlackBits = BitConverter.ToInt32(bConvert, 0);
			
					// Convert the bytes to a collection of bits, starting at byte 3
					for (iCount = 5; iCount < (bData.Length - 1); iCount++)
					{
						bTemp = bData[iCount];

						cCollection otempcol = new cCollection();

						for (iBitCount = 7; iBitCount >= 0; iBitCount--)
						{
							// Examine the iBitCount'th bit of the bTemp byte.
							bExamine = (byte) ((bTemp >> iBitCount) & 0x01);
							if (bExamine == (byte) 0x00)
							{
								//otempcol.Add(0);
								oCol.Add(0);
							}
							else
							{
								//otempcol.Add(1);
								oCol.Add(1);
							}
						}
					}

					// Create a new bitmap in memory.
					oBMP = new System.Drawing.Bitmap(iMaxWidth, iMaxHeight);
					//System.Drawing.Bitmap oBMP = new System.Drawing.Bitmap(iWidth, iHeight);
					#endregion
				}
				catch(System.Exception exReadImageDefault)
				{
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", exReadImageDefault);
				}

                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Setting up default image");
				try
				{
					#region Setup default image
					//				// Paint the bitmap white.
					for (iY = 0; iY < iMaxHeight; iY++)
					{
						for (iX = 0; iX < iMaxWidth; iX++)
						{
							//oBMP.SetPixel(iX, iY, System.Drawing.Color.White);
							oBMP.SetPixel(iX, iY, cBackGroundColor);
						}
					}
					// (iMaxHeight - iHeight) / 2 as a rounded integer.
					iHeightOffset = Convert.ToInt32(Math.Round(Convert.ToDecimal( (iMaxHeight - iHeight) / 2 ), 0));
					if (iHeightOffset > 1) iHeightOffset = iHeightOffset - 1;

					// (iMaxHeight - iHeight) / 2 as a rounded integer.
					iWidthOffset = Convert.ToInt32(Math.Round(Convert.ToDecimal( (iMaxWidth - iWidth) / 2 ), 0));
					if (iWidthOffset > 1) iWidthOffset = iWidthOffset - 1;
					#endregion 
				}
				catch(System.Exception exSetupImage)
				{
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", exSetupImage);
				}
			

				// Move to the start of the image.
				iX = 0;
				iY = 0;
				bool bError = false;
                if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Expand the bit collection to a collections of lengths");
				// Expand the bit collection to a collections of lengths
				for (iCount = 1; iCount <= oCol.Count; iCount++)
				{
					if (oCol.IntItem(iCount) == 0)
					{
						try
						{
							#region Processing Background

							// Set the bit mask for the iWhiteBitsth bit
							iBitMask = 1 << (iWhiteBits - 1);
							//iBitMask = (int) 0x100;
							// Reset the reult value.
							iPixels = 0;

							// Process for white space (iWhiteBits bits)
							for (iBitCount = 0; iBitCount < iWhiteBits; iBitCount++)
							{
								iCount++;  // bit array pointer
								// If this bit is set, then add the mask to the result.
								if (oCol.IntItem(iCount) == 1)
								{
									iPixels += iBitMask;
								}
								// Shift the mask right by 1.
								iBitMask = iBitMask >> 1;
							}

							if (iPixels > 0)
							{

								// Painting Background Pixels
								while ( (iX + iPixels) > iWidth )
								{
									// If there are more pixels to be painted than are left on the
									// current line, paint to the end of the line, and reduce
									// iPixels by the number of pixels drawn.
									try
									{
										for (iPaint = iX; iPaint < iWidth; iPaint++)
										{
											oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, cBackGroundColor);
											iPixels--;
										}
									}
									catch(System.Exception ex4)
									{
										Console.Write (ex4.Message + "\n");
										bError = true;
										break;
									}
									// Move to the start of the next line in the image
									iY++;
									iX = 0;

									if (iY >= iHeight) 
									{
										bError = true;
										break;
									}
								}

								if (bError) break;

								try
								{
									// If there are less pixels to be painted than are remaining
									// on the current line, paint the pixels and move iX to the
									// next pixel to be painted
									for (iPaint = iX; iPaint < (iX + iPixels); iPaint++)
									{
										oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, cBackGroundColor);
									}
									iX = iX + iPixels;
								}
								catch(System.Exception ex4)
								{
									Console.Write (ex4.Message + "\n");
								}

								if (bError) break;
							}
							#endregion
						}
						catch(System.Exception exProcessWhite)
						{
                            if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", exProcessWhite);
						}
					}
					else
					{
						try
						{
							#region Processing Black (Foreground)
							// We are processing blacks

							// Set the bit mask for the iBlackBitsrd bit
							iBitMask = 1 << (iBlackBits -1);
							//iBitMask = (int) 0x04;

							// Reset the reult value.
							iPixels = 0;

							// Process for black space (iBlackBits bits)
							for (iBitCount = 0; iBitCount < iBlackBits; iBitCount++)
							{
								iCount++;  // bit array pointer
								// If this bit is set, then add the mask to the result.
								if (oCol.IntItem(iCount) == 1)
								{
									iPixels += iBitMask;
								}
								// Shift the mask right by 1.
								iBitMask = iBitMask >> 1;
							}

							if (iPixels > 0)
							{
								// Painting Black Pixels
								while (iX + iPixels > iWidth)
								{
									// If there are more pixels to be painted than are left on the
									// current line, paint to the end of the line, and reduce
									// iPixels by the number of pixels drawn.
									try
									{
										for (iPaint = iX; iPaint < iWidth; iPaint++)
										{
											oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, System.Drawing.Color.Black);
											iPixels--;
										}
									}
									catch(System.Exception ex4)
									{
										Console.Write (ex4.Message + "\n");
										bError = true;
										break;
									}
									// Move to the start of the next line.
									iY++;
									iX = 0;
									if (iY >= iHeight) 
									{
										bError = true;
										break;
									}
								}

								if (bError) break;

								try
								{
									// If there are less pixels to be painted than are remaining
									// on the current line, paint the pixels and move iX to the
									// next pixel to be painted
									for (iPaint = iX; iPaint < (iX + iPixels); iPaint++)
									{
										oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, System.Drawing.Color.Black);
									}
									iX = iX + iPixels;
								}
								catch(System.Exception ex4)
								{
									Console.Write (ex4.Message + "\n");
									bError = true;
									break;
								}

								if (bError) break;
							}
							#endregion
						}
						catch(System.Exception exProcessBlack)
						{
                            if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", exProcessBlack);
						}

					}
				
				}

                try
                {
                    if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Resize the image");

                    oBMP = ResizeImage(oBMP, Convert.ToInt32(oBMP.PhysicalDimension.Width * dResizeFactor), Convert.ToInt32(oBMP.PhysicalDimension.Height * dResizeFactor));

                    if (_config.IncreaseContrast)
                    {
                        if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Increasing contrast");
                        uint uTolerance = (uint)_config.ContrastTolerance;
                        uint uBlack = (uint)Color.Black.ToArgb();
                        uint uBoundary = (uint)uBlack - uTolerance;

                        for (iY = 0; iY < oBMP.Height; iY++)
                        {
                            for (iX = 0; iX < oBMP.Width; iX++)
                            {
                                try
                                {
                                    if (iX == 0 || iY == 0)
                                    {
                                        oBMP.SetPixel(iX, iY, cBackGroundColor);
                                    }
                                    else
                                    {
                                        #region Paint Black
                                        if (oBMP.GetPixel(iX, iY).ToArgb() != cBackGroundColor.ToArgb())
                                        {
                                            uint iExamineValue = (uint)oBMP.GetPixel(iX, iY).ToArgb();
                                            if (iExamineValue > uBoundary)
                                            {
                                                oBMP.SetPixel(iX, iY, System.Drawing.Color.Black);
                                            }
                                            else
                                            {
                                                oBMP.SetPixel(iX, iY, cBackGroundColor);
                                            }
                                        }
                                        else
                                        {
                                            oBMP.SetPixel(iX, iY, cBackGroundColor);
                                        }
                                    }
                                        #endregion
                                }
                                catch (System.Exception exPaintBlack)
                                {
                                    Console.Write(exPaintBlack.Message + "\nSource : " + exPaintBlack.Source);
                                }
                            }
                        }
                    }
                    else
                    {
                        for (iY = 0; iY < oBMP.Height; iY++)
                        {
                            for (iX = 0; iX < oBMP.Width; iX++)
                            {
                                if (oBMP.GetPixel(iX, iY).ToArgb() == cBackGroundColor.ToArgb())
                                {
                                    oBMP.SetPixel(iX, iY, cBackGroundColor);
                                }
                            }
                        }
                    }

                    if (_config.MakeSignatureTransparent)
                    {
                        if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Making background transparent");
                        MemoryStream memStrm = new MemoryStream();
                        oBMP.Save(memStrm, ImageFormat.Gif);
                        oBMP = new Bitmap(memStrm);

                        //oBMP.Save(@"C:\MTDataLogs\SigTemp\Resized.GIF", ImageFormat.Gif);
                        //oBMP = new Bitmap(@"C:\MTDataLogs\SigTemp\Resized.GIF");

                        oBMP = MakeTransparent(oBMP, cBackGroundColor);
                    }
                }
                catch (System.Exception ex)
                {
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", ex);
                }
				FileStream oNewFS = null;

				try
				{
                    if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Writting image to file '" + sFilename + "'");

					if (System.IO.File.Exists(sFilename))
						oNewFS = new FileStream(sFilename, System.IO.FileMode.Truncate);
					else
						oNewFS = new FileStream(sFilename, System.IO.FileMode.CreateNew);
					try
					{
						oBMP.Save(oNewFS, System.Drawing.Imaging.ImageFormat.Gif);
					}
					finally
					{
						oNewFS.Close();
						oNewFS = null;
					}
				}
				catch(System.Exception exFileCreate)
				{
					Console.Write( exFileCreate.Message + "\nSource : " + exFileCreate.Source);
				}

                try
                {
                    if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Loading image bytes from '" + sFilename + "'");
                    byte[] bLoadData = new byte[4000];
                    int iBytes = File.OpenRead(sFilename).Read(bLoadData, 0, 4000);

                    bResult = new byte[iBytes];
                    for (int iStrip = 0; iStrip < iBytes; iStrip++)
                        bResult[iStrip] = bLoadData[iStrip];

                    if (!_config.SaveImageFile)
                    {
                        if (oLogging != null && _config.LogSignatureData) oLogging.Info("Vector (Ver 1) - Deleting image file at '" + sFilename + "'");
                        File.Delete(sFilename);
                    }
                }
                catch (System.Exception ex)
                {
                    if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", ex);
                }
            }
            catch(System.Exception exOverall)
            {
                if (oLogging != null) oLogging.Error(sClassName + "TranslateSignatureV1(byte[] bData)", exOverall);
			}
			finally
			{
				if (oBMP != null)
				{
					oBMP.Dispose();
					oBMP = null;
				}

			}


            if (oLogging != null && _config.LogSignatureData)
            {
                if (bResult != null)
                    oLogging.Info("Vector (Ver 1) - Returning '" + BitConverter.ToString(bResult) + "'");
                else
                    oLogging.Info("Vector (Ver 1) - Returning no data.");
            }

			return  bResult;
		}


		public Bitmap ResizeImage(Bitmap srcImg, int newWidth, int newHeight)
		{
			// get source image size
			int width = srcImg.Width;
			int height = srcImg.Height;

			if ((newWidth == width) && (newHeight == height))
			{
				// just clone the image
				return this.Clone(srcImg, PixelFormat.Format8bppIndexed);
			}

			PixelFormat fmt = (srcImg.PixelFormat == PixelFormat.Format8bppIndexed) ?
				PixelFormat.Format8bppIndexed : PixelFormat.Format24bppRgb;

			// create new image
			Bitmap dstImg = (fmt == PixelFormat.Format8bppIndexed) ?
				CreateGrayscaleImage(newWidth, newHeight) :
				new Bitmap(newWidth, newHeight, fmt);

			BitmapData srcData = null;
			BitmapData dstData = null;
			bool srcLocked = false;
			bool dstLocked = false;
			try
			{
				// lock source bitmap data
				srcData = srcImg.LockBits(
					new Rectangle(0, 0, width, height),
					ImageLockMode.ReadOnly, fmt);
				srcLocked = true;

				// lock destination bitmap data
				dstData = dstImg.LockBits(
					new Rectangle(0, 0, newWidth, newHeight),
					ImageLockMode.ReadWrite, fmt);
				dstLocked = true;

				int srcStride = srcData.Stride;
				int dstOffset = dstData.Stride - ((fmt == PixelFormat.Format8bppIndexed) ? newWidth : newWidth * 3);
				float xFactor = (float) width / newWidth;
				float yFactor = (float) height / newHeight;

				// do the job
				unsafe
				{
					byte * src = (byte *) srcData.Scan0.ToPointer();
					byte * dst = (byte *) dstData.Scan0.ToPointer();

					#region Bicubic Interpolation -Extracted from " Image Processing Library" by Andrew Kirillov from www.codeproject.com, 2005
					// ----------------------------------
					// resize using bicubic interpolation
					// ----------------------------------

					float	ox, oy, dx, dy, k1, k2;
					float	r, g, b;
					int		ox1, oy1, ox2, oy2;
					int		ymax = height - 1;
					int		xmax = width - 1;
					byte *	p;

					if (fmt == PixelFormat.Format8bppIndexed)
					{
						// grayscale
						for (int y = 0; y < newHeight; y++)
						{
							// Y coordinates
							oy	= (float) y * yFactor - 0.5f;
							oy1	= (int) oy;
							dy	= oy - (float) oy1;

							for (int x = 0; x < newWidth; x++, dst ++)
							{
								// X coordinates
								ox	= (float) x * xFactor - 0.5f;
								ox1	= (int) ox;
								dx	= ox - (float) ox1;

								g = 0;

								for (int n = -1; n < 3; n++) 
								{
									k1 = this.BiCubicKernel(dy - (float) n);

									oy2 = oy1 + n;
									if (oy2 < 0)
										oy2 = 0;
									if (oy2 > ymax)
										oy2 = ymax;

									for (int m = -1; m < 3; m++) 
									{
										k2 = k1 * this.BiCubicKernel((float) m - dx);

										ox2 = ox1 + m;
										if (ox2 < 0)
											ox2 = 0;
										if (ox2 > xmax)
											ox2 = xmax;

										g += k2 * src[oy2 * srcStride + ox2];
									}
								}
								*dst = (byte) g;
							}
							dst += dstOffset;
						}
					}
					else
					{
						// RGB
						for (int y = 0; y < newHeight; y++)
						{
							// Y coordinates
							oy	= (float) y * yFactor - 0.5f;
							oy1	= (int) oy;
							dy	= oy - (float) oy1;

							for (int x = 0; x < newWidth; x++, dst += 3)
							{
								// X coordinates
								ox	= (float) x * xFactor - 0.5f;
								ox1	= (int) ox;
								dx	= ox - (float) ox1;

								r = g = b = 0;

								for (int n = -1; n < 3; n++) 
								{
									k1 = this.BiCubicKernel(dy - (float) n);

									oy2 = oy1 + n;
									if (oy2 < 0)
										oy2 = 0;
									if (oy2 > ymax)
										oy2 = ymax;

									for (int m = -1; m < 3; m++) 
									{
										k2 = k1 * this.BiCubicKernel((float) m - dx);

										ox2 = ox1 + m;
										if (ox2 < 0)
											ox2 = 0;
										if (ox2 > xmax)
											ox2 = xmax;

										// get pixel of original image
										p = src + oy2 * srcStride + ox2 * 3;

										r += k2 * p[RGB.R];
										g += k2 * p[RGB.G];
										b += k2 * p[RGB.B];
									}
								}

								dst[RGB.R] = (byte) r;
								dst[RGB.G] = (byte) g;
								dst[RGB.B] = (byte) b;
							}
							dst += dstOffset;
						}
					}
					#endregion
				}
			}
			finally
			{
				if (dstLocked)
					dstImg.UnlockBits(dstData);
				if (srcLocked)
					srcImg.UnlockBits(srcData);
			}
			return dstImg;
		}

		public Bitmap MakeTransparent(Bitmap gifImage, Color targetBackground)
		{
			ColorPalette palette = gifImage.Palette;

			int backgroundOffset = -1;
			for(int loop = 0; loop < palette.Entries.Length; loop++)
			{
				Color color = (Color)palette.Entries[loop];
				if (color.ToArgb() == targetBackground.ToArgb())
					backgroundOffset = loop;
			}

			if (backgroundOffset >= 0)
			{
				Bitmap newImage =new Bitmap(gifImage.Width, gifImage.Height, PixelFormat.Format8bppIndexed); 

				ColorPalette newPalette = newImage.Palette; 

				//copy all the entries from the old palette removing any transparency 

				int loop=0; 
				foreach(Color color in palette.Entries) 
					newPalette.Entries[loop++]=Color.FromArgb(255,color); 

				//Set the newly selected transparency 
				newPalette.Entries[backgroundOffset] = Color.FromArgb(0, palette.Entries[backgroundOffset]); 

				//re-insert the palette 
				newImage.Palette=newPalette;
 
				//now to copy the actual bitmap data 
				//lock the source and destination bits 
				BitmapData src=((Bitmap)gifImage).LockBits(new Rectangle(0, 0, gifImage.Width, gifImage.Height), ImageLockMode.ReadOnly, gifImage.PixelFormat); 
				BitmapData dst= newImage.LockBits(new Rectangle(0, 0, newImage.Width, newImage.Height), ImageLockMode.WriteOnly, newImage.PixelFormat); 
				try
				{
					//uses pointers so we need unsafe code. 
					//the project is also compiled with /unsafe 
					unsafe 
					{ 

						//steps through each pixel 
						for(int y = 0; y < gifImage.Height; y++) 
							for(int x = 0; x < gifImage.Width; x++) 
							{ 
								//transferring the bytes 
								((byte *)dst.Scan0.ToPointer())[(dst.Stride*y)+x]=((byte *)src.Scan0.ToPointer())[(src.Stride*y)+x]; 
							} 
					} 
				}
				finally
				{
					//all done, unlock the bitmaps 
					gifImage.UnlockBits(src); 
					newImage.UnlockBits(dst); 
				}
				//newImage.Save(targetFile,ImageFormat.Gif);
				return newImage;
			}
			else
				return gifImage;
			
		}

		/// <summary>
		/// Create and initialize grayscale image
		/// </summary>
		public Bitmap CreateGrayscaleImage(int width, int height)
		{
			// create new image
			Bitmap bmp = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
			// set palette to grayscale
			SetGrayscalePalette(bmp);
			// return new image
			return bmp;
		}

		/// <summary>
		/// Set pallete of the image to grayscale
		/// </summary>
		public void SetGrayscalePalette(Bitmap srcImg)
		{
			// check pixel format
			if (srcImg.PixelFormat != PixelFormat.Format8bppIndexed)
				throw new ArgumentException();

			// get palette
			ColorPalette cp = srcImg.Palette;
			// init palette
			for (int i = 0; i < 256; i++)
			{
				cp.Entries[i] = Color.FromArgb(i, i, i);
			}
			// set palette back
			srcImg.Palette = cp;
		}

		private Bitmap Clone(Bitmap src, PixelFormat format)
		{
			// copy image if pixel format is the same
			if (src.PixelFormat == format)
				return Clone(src);

			int width	= src.Width;
			int height	= src.Height;

			// create new image with desired pixel format
			Bitmap bmp = new Bitmap(width, height, format);

			// draw source image on the new one using Graphics
			Graphics g = Graphics.FromImage(bmp);
			g.DrawImage(src, 0, 0, width, height);
			g.Dispose();

			return bmp;
		}

		private Bitmap Clone(Bitmap src)
		{
			// get source image size
			int width = src.Width;
			int height = src.Height;

			// lock source bitmap data
			BitmapData srcData = src.LockBits(
				new Rectangle(0, 0, width, height),
				ImageLockMode.ReadWrite, src.PixelFormat);

			// create new image
			Bitmap dst = new Bitmap(width, height, src.PixelFormat);

			// lock destination bitmap data
			BitmapData dstData = dst.LockBits(
				new Rectangle(0, 0, width, height),
				ImageLockMode.ReadWrite, dst.PixelFormat);

			Win32.memcpy(dstData.Scan0, srcData.Scan0, height * srcData.Stride);

			// unlock both images
			dst.UnlockBits(dstData);
			src.UnlockBits(srcData);

			//
			if ((src.PixelFormat == PixelFormat.Format1bppIndexed) ||
				(src.PixelFormat == PixelFormat.Format4bppIndexed) ||
				(src.PixelFormat == PixelFormat.Format8bppIndexed) ||
				(src.PixelFormat == PixelFormat.Indexed))
			{
				ColorPalette srcPalette = src.Palette;
				ColorPalette dstPalette = dst.Palette;

				int n = srcPalette.Entries.Length;

				// copy pallete
				for (int i = 0; i < n; i++)
				{
					dstPalette.Entries[i] = srcPalette.Entries[i];
				}

				dst.Palette = dstPalette;
			}
			
			return dst;
		}

		private float BiCubicKernel(float x)
		{
			if (x > 2.0f)
				return 0.0f;

			float	a, b, c, d;
			float	xm1 = x - 1.0f;
			float	xp1 = x + 1.0f;
			float	xp2 = x + 2.0f;

			a = (xp2 <= 0.0f) ? 0.0f : xp2 * xp2 * xp2;
			b = (xp1 <= 0.0f) ? 0.0f : xp1 * xp1 * xp1;
			c = (x   <= 0.0f) ? 0.0f : x * x * x;
			d = (xm1 <= 0.0f) ? 0.0f : xm1 * xm1 * xm1;

			return (0.16666666666666666667f * (a - (4.0f * b) + (6.0f * c) - (4.0f * d)));
		}
	}
}

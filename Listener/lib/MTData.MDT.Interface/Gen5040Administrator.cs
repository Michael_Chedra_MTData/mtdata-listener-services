using System;
using System.Threading;
using System.Net.Sockets;
using System.Configuration;
using System.Collections;
using MTData.MDT.Interface;
using System.Text;
using System.IO;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Network;
using MTData.Common.Queues;
using MTData.Common.Utilities;
using MTData.Common.Config;


namespace MTData.MDT.Interface
{
    /// <summary>
    /// This class is the heart of the MDT Interface.
    /// It coordinates the construciton and initialisation of all
    /// components.
    /// </summary>
    public class Gen5040Administrator
    {
        private GenConfigReader _config = null;												// Configuration options
        private object MCCSyncRoot = new object();												// To make this object thread safe.
        private ILog _log = LogManager.GetLogger(typeof(Gen5040Administrator));			// For logging messages to file.
        private QueueInterface _mccInbound = null;											// For inbound MCC Packet
        private QueueInterface _mccOutbound = null;											// For sending to MCC
        private QueueInterface _fromMobileUnitsQ = null;								// Updates to be sent to the interface
        private QueueInterface _toMobileUnitsQ = null;									// Updates to be sent to the interface
        private TCPServer _mccServer = null;														// The TCP listener for the MCC server
        private TCPConnection _mccConnection = null;											// The TCP connection to the MCC server
        private WrapForMCCThread _WrapForMCC = null;									// For passing 5040 packets to be wrapped by the MCC layer
        private UnwrapFromMCCThread _UnwrapFromMCC = null;						// For passing MCC packets to extract the 5040 data.
        private string _TranslatorType = "";															// The name of the translator object in use.
        private bool bLogMCCData = true;                                        // The var to indicate if the TCP connection should log data from MCC

        public Gen5040Administrator(string TranslatorType, QueueInterface fromMobileUnitsQ, QueueInterface toMobileUnitsQ, GenConfigReader configuration)
        {
            // Setup the logging and configuration
            _config = (configuration != null) ? configuration : new GenConfigReader();
            try
            {
                bLogMCCData = _config.LogFromMCCData;
            }
            catch (System.Exception)
            {
            }
            _log.Info("Initialisation : Preparing Interface for Use");
            _TranslatorType = TranslatorType;

            //	Prepare the queues
            _log.Info("Initialisation : Creating queues");
            _mccInbound = new StandardQueue(_config.MaxQueueSize);
            _mccOutbound = new StandardQueue(_config.MaxQueueSize);
            _fromMobileUnitsQ = fromMobileUnitsQ;
            _toMobileUnitsQ = toMobileUnitsQ;

            //	Prepare the Wrapping / Unwrapping threads
            _log.Info("Initialisation : Preparing MCC Wrapping Threads");
            // _WrapForMCC takes messages off the _toMobileUnitsQ, wraps them with the mcc layer protocol and queues the data on the _mccOutbound queue.
            _WrapForMCC = new WrapForMCCThread(_config.TranslationBufferSize, _TranslatorType, _toMobileUnitsQ, _mccOutbound,  _config.SignatureConfig);
            //_UnwrapFromMCC takes messages from the _mccInbound queue, decodes the packet to a c5040PacketData object and queues it on the _fromMobileUnitsQ queue.
            // if a message need to be sent back to MCC, it is placed on the _mccOutbound queue
            _UnwrapFromMCC = new UnwrapFromMCCThread(_config.TranslationBufferSize, _TranslatorType, _mccInbound, _mccOutbound, _fromMobileUnitsQ,  _config.SignatureConfig, _config.LogFromMCCData, _config.LogToMCCData);

            // Start listening for a connection from MCC
            Console.Write("Listening for MCC Connection on port " + Convert.ToString(_config.MCCPort));
            TCPServer.InboundConnectionHandler mccHandler = new TCPServer.InboundConnectionHandler(MCC_HandleInboundConnection);
            _mccServer = new TCPServer("0.0.0.0", _config.MCCPort, "MCC Comms", mccHandler);
            _mccServer.Start();
            _log.Info("Initialisation : Complete");

            /*
             * Here goes a part for manual inserts from the log file
             * */
            string sMCCLogFileRecovery = Convert.ToString(ConfigurationManager.AppSettings["MCCLogFileRecovery"]);
            if (sMCCLogFileRecovery != null)
            {
                if (sMCCLogFileRecovery.Length > 0)
                {
                    if (Util.FileExist(sMCCLogFileRecovery))
                    {
                        ParseLogFile(sMCCLogFileRecovery);
                        _log.Info("ParseLogFile : Completed");
                    }
                    else
                    {
                        _log.Info("ParseLogFile : Error ParseLogFile doesn't exist");
                    }
                }
            }

        }

        #region Parse Log File From MCC Manually

        private char[] seps = { '[', ']', ' ' };
        private string[] sParsed;
        private byte[] bTmpParsed = null;
        private byte[] bParsed = null;
        private string sTmp = "";

        private void ProcessCameronsPacket(byte[] bParsed)
        {
            try
            {
                _mccInbound.Enqueue(bParsed);
            }
            catch (Exception ex)
            {
                _log.Error("ParseLogFile : Error Enqueuing", ex);
            }
        }


        private void ParseLine(string sLine)
        {
            try
            {
                sTmp = "";
                sParsed = sLine.Split(seps);
                bTmpParsed = new byte[2000];
                int k = 0;
                for (int i = 0; i < sParsed.Length; i++)
                {
                    if (sParsed[i] != "")
                    {
                        bTmpParsed[k++] = Convert.ToByte(Convert.ToInt32(sParsed[i]));
                        sTmp += sParsed[i] + ",";
                    }
                }

                bParsed = new byte[k];
                for (int i = 0; i < k; i++)
                    bParsed[i] = bTmpParsed[i];

                //Console.WriteLine(sTmp);
                ProcessCameronsPacket(bParsed);
            }
            catch (Exception ex)
            {
                _log.Error("ParseLogFile : Error ParseLine", ex);
            }
        }

        private void ParseLogFile(string sLogPathFile)
        {
            FileStream fs = null;
            StreamReader sr = null;
            try
            {
                int i = 0;
                string sLine = "";
                fs = new FileStream(@sLogPathFile, FileMode.Open, FileAccess.Read, FileShare.None);
                sr = new StreamReader(fs);
                while (sr.Peek() > -1)
                {
                    sLine = sr.ReadLine();
                    if (sLine.IndexOf("InterfaceConnection : Writing", 0) > 0)
                    {
                        if (!((sLine.IndexOf("InterfaceConnection : Writing 37 bytes", 0) > 0) || (sLine.IndexOf("InterfaceConnection : Writing 17 bytes", 0) > 0)))
                        {

                            //Console.WriteLine(sLine);
                            i++;
                            //Console.WriteLine(sLine.Substring(sLine.IndexOf("bytes :")+7));
                            ParseLine(sLine.Substring(sLine.IndexOf("bytes :") + 7));
                        }
                    }
                }

                //Console.WriteLine(i);
            }
            catch (Exception ex)
            {
                _log.Error("ParseLogFile : Error ParseLogFile", ex);
            }
            finally
            {
                sr.Close();
                fs.Close();
            }
        }

        #endregion

        #region Properties
        /// <summary>
        /// Property allowing direct access to the MCC connection
        /// </summary>
        public TCPConnection MCCConnection
        {
            get
            {
                lock (this.MCCSyncRoot)
                    return _mccConnection;
            }
        }
        #endregion

        #region Connection handling
        /// <summary>
        /// Event handler to cater for the initiation of an MCC connection
        /// Note that any subsequent connections assume control of the outbound queues.
        /// </summary>
        /// <param name="listener"></param>
        public void MCC_HandleInboundConnection(TcpListener listener)
        {
            Console.Write("MCC Connected");
            // Accept the connection
            TcpClient client = listener.AcceptTcpClient();

            // Create a new TCP connection and bind the in and out queues for MCC
            // pass it the reference to the inbound and outbound queues. (_mccInbound and _mccOutbound)
            TCPConnection connection = new TCPConnection(client, "MCCConnection", _mccInbound, _mccOutbound, new TCPConnection.ConnectionClosedHandler(mcc_connectionclosed), _config.PacketSize, _config.PacketWaitPeriod, _config.SleepWaitPeriod);
            connection.LogData = bLogMCCData;
            lock (this.MCCSyncRoot)
            {
                if (_mccConnection != null)
                {
                    _log.Info("MCC Connection : Closing Previous Connection");
                    //	Ensure that the previous connection is detached from the queues
                    _mccConnection.Close();
                }
                _mccConnection = connection;
            }
        }

        /// <summary>
        /// Event hanlder to cater for the closure of the MCC connection
        /// </summary>
        /// <param name="sender"></param>
        public void mcc_connectionclosed(TCPConnection sender)
        {
            Console.Write("MCC Disconnected");

            lock (this.MCCSyncRoot)
            {
                if (_mccConnection == sender)
                    _mccConnection = null;
            }
        }
        #endregion

        /// <summary>
        /// Method to stop the listeners and threads.
        /// </summary>
        public void Stop(bool wait)
        {
            if (_mccServer != null)
                _mccServer.Stop();

            if (_mccConnection != null)
                _mccConnection.Close();

            if (_WrapForMCC != null)
                _WrapForMCC.Stop();

            if (_WrapForMCC != null)
                _UnwrapFromMCC.Stop();

            if (wait)
            {
                while (MCCConnection != null)
                    Thread.Sleep(10);
            }
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append("=====  system dump =====\r\n");
            builder.Append("\r\nMCC Inbound count : ");
            lock (_mccInbound.SyncRoot)
                builder.Append(_mccInbound.Count);
            builder.Append("\r\nMCC Outbound count : ");
            lock (_mccOutbound.SyncRoot)
                builder.Append(_mccOutbound.Count);

            builder.Append("\r\n============================");

            return builder.ToString();
        }

    }
}

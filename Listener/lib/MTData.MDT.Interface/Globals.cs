using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Runtime.InteropServices;
using System.Security;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// A class for supporting the passing of colors as parameters
	/// </summary>

	public class RGB
	{
		public const short R = 2;
		public const short G = 1;
		public const short B = 0;
	};

	internal class Win32
	{
		// memcpy - copy a block of memory
		[DllImport("ntdll.dll")]
		public static extern IntPtr memcpy(
			IntPtr dst,
			IntPtr src,
			int count);
		[DllImport("ntdll.dll")]
		public static extern unsafe byte * memcpy(
			byte * dst,
			byte * src,
			int count);

		// memset - fill memory with specified values
		[DllImport("ntdll.dll")]
		public static extern IntPtr memset(
			IntPtr dst,
			int filler,
			int count);
	}

	/// <summary>
	/// Enum type for translators
	/// </summary>
	public enum TranslatorType { MBP, MMP }


	/// <summary>
	/// Summary description for Globals.
	/// </summary>
	public class Globals
	{
		public Globals()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		// Global Variables

		//public static bool bLogSignatureData = false;

		// BiCubic kernel
		public static float BiCubicKernel(float x)
		{
			if (x > 2.0f)
				return 0.0f;

			float	a, b, c, d;
			float	xm1 = x - 1.0f;
			float	xp1 = x + 1.0f;
			float	xp2 = x + 2.0f;

			a = (xp2 <= 0.0f) ? 0.0f : xp2 * xp2 * xp2;
			b = (xp1 <= 0.0f) ? 0.0f : xp1 * xp1 * xp1;
			c = (x   <= 0.0f) ? 0.0f : x * x * x;
			d = (xm1 <= 0.0f) ? 0.0f : xm1 * xm1 * xm1;

			return (0.16666666666666666667f * (a - (4.0f * b) + (6.0f * c) - (4.0f * d)));
		}

		public static byte[] BuildKeepAlive(int iKeepAliveSeq)
		{
			byte[] bSendPacket = new byte[17];
			byte[] bTemp =  null;
			int iLen = 0;
			int X = 0;

			if (iKeepAliveSeq > -1)
			{

				try
				{
					bTemp = BitConverter.GetBytes(iKeepAliveSeq);

					// Create the keep alive packet.
					// Packet format = [SOP][Length][Unit ID][Sep][Job ID][Sep][Data][EOP]
					// [SOP] = 0x0A
					// [Length] = 4 Bytes
					// [Unit ID] = 4 Bytes
					// [Sep] = 0x0B
					// [Job ID] = 4 Bytes
					// [Sep] = 0x0B
					// [Data] = 0x00
					// [EOP] = 0x0C
					// Length includes [Unit ID][Sep][Job ID][Sep][Data]

					bSendPacket[X++] = (byte) 0x0A;		 // SOP
					bSendPacket[X++] = (byte) 0x00;		// Length 1
					bSendPacket[X++] = (byte) 0x00;		// Length 2
					bSendPacket[X++] = (byte) 0x00;		// Length 3
					bSendPacket[X++] = (byte) 0x00;		// Length 4

					bSendPacket[X++] = (byte) 0x00;			// Unit ID 1
					bSendPacket[X++] = (byte) 0x00;			// Unit ID 2
					bSendPacket[X++] = (byte) 0x00;			// Unit ID 3
					bSendPacket[X++] = (byte) 0x00;			// Unit ID 4
					bSendPacket[X++] = (byte) 0x0B;		// Sep
					bSendPacket[X++] = bTemp[0];					// Keep Alive Seq (0 - 255 = ack request.)
					bSendPacket[X++] = bTemp[1];					// Keep Alive Seq
					bSendPacket[X++] = bTemp[2];					// Keep Alive Seq (Bit is set only if this is an ack, i.e. 256 = ack responce)
					bSendPacket[X++] = (byte) 0x00;			// Keep Alive Seq
					bSendPacket[X++] = (byte) 0x0B;		// Sep
					bSendPacket[X++] = (byte) 0x00;			// Data

					bSendPacket[X++] = (byte) 0x0C;		// EOP						
				
					iLen = X - 6;

					bTemp = BitConverter.GetBytes(iLen);
					// Populate the length field.
					bSendPacket[1] = bTemp[0];
					bSendPacket[2] = bTemp[1];
					bSendPacket[3] = bTemp[2];
					bSendPacket[4] = bTemp[3];
				}
				catch(System.Exception ex)
				{
					Console.WriteLine(ex.Message);
				}
			}

			return bSendPacket;
		}

		public static Bitmap Clone(Bitmap src, PixelFormat format)
		{
			// copy image if pixel format is the same
			if (src.PixelFormat == format)
				return Clone(src);

			int width	= src.Width;
			int height	= src.Height;

			// create new image with desired pixel format
			Bitmap bmp = new Bitmap(width, height, format);

			// draw source image on the new one using Graphics
			Graphics g = Graphics.FromImage(bmp);
			g.DrawImage(src, 0, 0, width, height);
			g.Dispose();

			return bmp;
		}

		// and with unspecified PixelFormat it works strange too
		public static Bitmap Clone(Bitmap src)
		{
			// get source image size
			int width = src.Width;
			int height = src.Height;

			// lock source bitmap data
			BitmapData srcData = src.LockBits(
				new Rectangle(0, 0, width, height),
				ImageLockMode.ReadWrite, src.PixelFormat);

			// create new image
			Bitmap dst = new Bitmap(width, height, src.PixelFormat);

			// lock destination bitmap data
			BitmapData dstData = dst.LockBits(
				new Rectangle(0, 0, width, height),
				ImageLockMode.ReadWrite, dst.PixelFormat);

			Win32.memcpy(dstData.Scan0, srcData.Scan0, height * srcData.Stride);

			// unlock both images
			dst.UnlockBits(dstData);
			src.UnlockBits(srcData);

			//
			if ((src.PixelFormat == PixelFormat.Format1bppIndexed) ||
				(src.PixelFormat == PixelFormat.Format4bppIndexed) ||
				(src.PixelFormat == PixelFormat.Format8bppIndexed) ||
				(src.PixelFormat == PixelFormat.Indexed))
			{
				ColorPalette srcPalette = src.Palette;
				ColorPalette dstPalette = dst.Palette;

				int n = srcPalette.Entries.Length;

				// copy pallete
				for (int i = 0; i < n; i++)
				{
					dstPalette.Entries[i] = srcPalette.Entries[i];
				}

				dst.Palette = dstPalette;
			}
			
			return dst;
		}

		public static Bitmap CreateGrayscaleImage(int width, int height)
		{
			// create new image
			Bitmap bmp = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
			// set palette to grayscale
			SetGrayscalePalette(bmp);
			// return new image
			return bmp;
		}

		/// <summary>
		/// Set pallete of the image to grayscale
		/// </summary>
		public static void SetGrayscalePalette(Bitmap srcImg)
		{
			// check pixel format
			if (srcImg.PixelFormat != PixelFormat.Format8bppIndexed)
				throw new ArgumentException();

			// get palette
			ColorPalette cp = srcImg.Palette;
			// init palette
			for (int i = 0; i < 256; i++)
			{
				cp.Entries[i] = Color.FromArgb(i, i, i);
			}
			// set palette back
			srcImg.Palette = cp;
		}
	}
}

﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:2.0.50727.3053
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

#pragma warning disable 1591

namespace MTData.Terminal.Interface {
    
    
    /// <summary>
    ///Represents a strongly typed in-memory cache of data.
    ///</summary>
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
    [global::System.Serializable()]
    [global::System.ComponentModel.DesignerCategoryAttribute("code")]
    [global::System.ComponentModel.ToolboxItem(true)]
    [global::System.Xml.Serialization.XmlSchemaProviderAttribute("GetTypedDataSetSchema")]
    [global::System.Xml.Serialization.XmlRootAttribute("ToDriver_LoginResponse")]
    [global::System.ComponentModel.Design.HelpKeywordAttribute("vs.data.DataSet")]
    public partial class ToDriver_LoginResponse : global::System.Data.DataSet {
        
        private LoginResponseDataTable tableLoginResponse;
        
        private global::System.Data.SchemaSerializationMode _schemaSerializationMode = global::System.Data.SchemaSerializationMode.IncludeSchema;
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public ToDriver_LoginResponse() {
            this.BeginInit();
            this.InitClass();
            global::System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new global::System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            base.Tables.CollectionChanged += schemaChangedHandler;
            base.Relations.CollectionChanged += schemaChangedHandler;
            this.EndInit();
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected ToDriver_LoginResponse(global::System.Runtime.Serialization.SerializationInfo info, global::System.Runtime.Serialization.StreamingContext context) : 
                base(info, context, false) {
            if ((this.IsBinarySerialized(info, context) == true)) {
                this.InitVars(false);
                global::System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler1 = new global::System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
                this.Tables.CollectionChanged += schemaChangedHandler1;
                this.Relations.CollectionChanged += schemaChangedHandler1;
                return;
            }
            string strSchema = ((string)(info.GetValue("XmlSchema", typeof(string))));
            if ((this.DetermineSchemaSerializationMode(info, context) == global::System.Data.SchemaSerializationMode.IncludeSchema)) {
                global::System.Data.DataSet ds = new global::System.Data.DataSet();
                ds.ReadXmlSchema(new global::System.Xml.XmlTextReader(new global::System.IO.StringReader(strSchema)));
                if ((ds.Tables["LoginResponse"] != null)) {
                    base.Tables.Add(new LoginResponseDataTable(ds.Tables["LoginResponse"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, global::System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else {
                this.ReadXmlSchema(new global::System.Xml.XmlTextReader(new global::System.IO.StringReader(strSchema)));
            }
            this.GetSerializationData(info, context);
            global::System.ComponentModel.CollectionChangeEventHandler schemaChangedHandler = new global::System.ComponentModel.CollectionChangeEventHandler(this.SchemaChanged);
            base.Tables.CollectionChanged += schemaChangedHandler;
            this.Relations.CollectionChanged += schemaChangedHandler;
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.Browsable(false)]
        [global::System.ComponentModel.DesignerSerializationVisibility(global::System.ComponentModel.DesignerSerializationVisibility.Content)]
        public LoginResponseDataTable LoginResponse {
            get {
                return this.tableLoginResponse;
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.BrowsableAttribute(true)]
        [global::System.ComponentModel.DesignerSerializationVisibilityAttribute(global::System.ComponentModel.DesignerSerializationVisibility.Visible)]
        public override global::System.Data.SchemaSerializationMode SchemaSerializationMode {
            get {
                return this._schemaSerializationMode;
            }
            set {
                this._schemaSerializationMode = value;
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.DesignerSerializationVisibilityAttribute(global::System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public new global::System.Data.DataTableCollection Tables {
            get {
                return base.Tables;
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [global::System.ComponentModel.DesignerSerializationVisibilityAttribute(global::System.ComponentModel.DesignerSerializationVisibility.Hidden)]
        public new global::System.Data.DataRelationCollection Relations {
            get {
                return base.Relations;
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void InitializeDerivedDataSet() {
            this.BeginInit();
            this.InitClass();
            this.EndInit();
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public override global::System.Data.DataSet Clone() {
            ToDriver_LoginResponse cln = ((ToDriver_LoginResponse)(base.Clone()));
            cln.InitVars();
            cln.SchemaSerializationMode = this.SchemaSerializationMode;
            return cln;
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override bool ShouldSerializeTables() {
            return false;
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override bool ShouldSerializeRelations() {
            return false;
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override void ReadXmlSerializable(global::System.Xml.XmlReader reader) {
            if ((this.DetermineSchemaSerializationMode(reader) == global::System.Data.SchemaSerializationMode.IncludeSchema)) {
                this.Reset();
                global::System.Data.DataSet ds = new global::System.Data.DataSet();
                ds.ReadXml(reader);
                if ((ds.Tables["LoginResponse"] != null)) {
                    base.Tables.Add(new LoginResponseDataTable(ds.Tables["LoginResponse"]));
                }
                this.DataSetName = ds.DataSetName;
                this.Prefix = ds.Prefix;
                this.Namespace = ds.Namespace;
                this.Locale = ds.Locale;
                this.CaseSensitive = ds.CaseSensitive;
                this.EnforceConstraints = ds.EnforceConstraints;
                this.Merge(ds, false, global::System.Data.MissingSchemaAction.Add);
                this.InitVars();
            }
            else {
                this.ReadXml(reader);
                this.InitVars();
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        protected override global::System.Xml.Schema.XmlSchema GetSchemaSerializable() {
            global::System.IO.MemoryStream stream = new global::System.IO.MemoryStream();
            this.WriteXmlSchema(new global::System.Xml.XmlTextWriter(stream, null));
            stream.Position = 0;
            return global::System.Xml.Schema.XmlSchema.Read(new global::System.Xml.XmlTextReader(stream), null);
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        internal void InitVars() {
            this.InitVars(true);
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        internal void InitVars(bool initTable) {
            this.tableLoginResponse = ((LoginResponseDataTable)(base.Tables["LoginResponse"]));
            if ((initTable == true)) {
                if ((this.tableLoginResponse != null)) {
                    this.tableLoginResponse.InitVars();
                }
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void InitClass() {
            this.DataSetName = "ToDriver_LoginResponse";
            this.Prefix = "";
            this.Namespace = "http://tempuri.org/ToDriver_LoginResponse.xsd";
            this.EnforceConstraints = true;
            this.SchemaSerializationMode = global::System.Data.SchemaSerializationMode.IncludeSchema;
            this.tableLoginResponse = new LoginResponseDataTable();
            base.Tables.Add(this.tableLoginResponse);
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private bool ShouldSerializeLoginResponse() {
            return false;
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        private void SchemaChanged(object sender, global::System.ComponentModel.CollectionChangeEventArgs e) {
            if ((e.Action == global::System.ComponentModel.CollectionChangeAction.Remove)) {
                this.InitVars();
            }
        }
        
        [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
        public static global::System.Xml.Schema.XmlSchemaComplexType GetTypedDataSetSchema(global::System.Xml.Schema.XmlSchemaSet xs) {
            ToDriver_LoginResponse ds = new ToDriver_LoginResponse();
            global::System.Xml.Schema.XmlSchemaComplexType type = new global::System.Xml.Schema.XmlSchemaComplexType();
            global::System.Xml.Schema.XmlSchemaSequence sequence = new global::System.Xml.Schema.XmlSchemaSequence();
            global::System.Xml.Schema.XmlSchemaAny any = new global::System.Xml.Schema.XmlSchemaAny();
            any.Namespace = ds.Namespace;
            sequence.Items.Add(any);
            type.Particle = sequence;
            global::System.Xml.Schema.XmlSchema dsSchema = ds.GetSchemaSerializable();
            if (xs.Contains(dsSchema.TargetNamespace)) {
                global::System.IO.MemoryStream s1 = new global::System.IO.MemoryStream();
                global::System.IO.MemoryStream s2 = new global::System.IO.MemoryStream();
                try {
                    global::System.Xml.Schema.XmlSchema schema = null;
                    dsSchema.Write(s1);
                    for (global::System.Collections.IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator(); schemas.MoveNext(); ) {
                        schema = ((global::System.Xml.Schema.XmlSchema)(schemas.Current));
                        s2.SetLength(0);
                        schema.Write(s2);
                        if ((s1.Length == s2.Length)) {
                            s1.Position = 0;
                            s2.Position = 0;
                            for (; ((s1.Position != s1.Length) 
                                        && (s1.ReadByte() == s2.ReadByte())); ) {
                                ;
                            }
                            if ((s1.Position == s1.Length)) {
                                return type;
                            }
                        }
                    }
                }
                finally {
                    if ((s1 != null)) {
                        s1.Close();
                    }
                    if ((s2 != null)) {
                        s2.Close();
                    }
                }
            }
            xs.Add(dsSchema);
            return type;
        }
        
        public delegate void LoginResponseRowChangeEventHandler(object sender, LoginResponseRowChangeEvent e);
        
        /// <summary>
        ///Represents the strongly named DataTable class.
        ///</summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
        [global::System.Serializable()]
        [global::System.Xml.Serialization.XmlSchemaProviderAttribute("GetTypedTableSchema")]
        public partial class LoginResponseDataTable : global::System.Data.DataTable, global::System.Collections.IEnumerable {
            
            private global::System.Data.DataColumn columnMobileUnit_ID;
            
            private global::System.Data.DataColumn columnLoginResposne;
            
            private global::System.Data.DataColumn columnDriverName;
            
            private global::System.Data.DataColumn columnTruckType;
            
            private global::System.Data.DataColumn columnDayLigh_Savings;
            
            private global::System.Data.DataColumn columnGMT_Offset_Hours;
            
            private global::System.Data.DataColumn columnGMT_Offset_Mins;
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public LoginResponseDataTable() {
                this.TableName = "LoginResponse";
                this.BeginInit();
                this.InitClass();
                this.EndInit();
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            internal LoginResponseDataTable(global::System.Data.DataTable table) {
                this.TableName = table.TableName;
                if ((table.CaseSensitive != table.DataSet.CaseSensitive)) {
                    this.CaseSensitive = table.CaseSensitive;
                }
                if ((table.Locale.ToString() != table.DataSet.Locale.ToString())) {
                    this.Locale = table.Locale;
                }
                if ((table.Namespace != table.DataSet.Namespace)) {
                    this.Namespace = table.Namespace;
                }
                this.Prefix = table.Prefix;
                this.MinimumCapacity = table.MinimumCapacity;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected LoginResponseDataTable(global::System.Runtime.Serialization.SerializationInfo info, global::System.Runtime.Serialization.StreamingContext context) : 
                    base(info, context) {
                this.InitVars();
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn MobileUnit_IDColumn {
                get {
                    return this.columnMobileUnit_ID;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn LoginResposneColumn {
                get {
                    return this.columnLoginResposne;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn DriverNameColumn {
                get {
                    return this.columnDriverName;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn TruckTypeColumn {
                get {
                    return this.columnTruckType;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn DayLigh_SavingsColumn {
                get {
                    return this.columnDayLigh_Savings;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn GMT_Offset_HoursColumn {
                get {
                    return this.columnGMT_Offset_Hours;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataColumn GMT_Offset_MinsColumn {
                get {
                    return this.columnGMT_Offset_Mins;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            [global::System.ComponentModel.Browsable(false)]
            public int Count {
                get {
                    return this.Rows.Count;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public LoginResponseRow this[int index] {
                get {
                    return ((LoginResponseRow)(this.Rows[index]));
                }
            }
            
            public event LoginResponseRowChangeEventHandler LoginResponseRowChanging;
            
            public event LoginResponseRowChangeEventHandler LoginResponseRowChanged;
            
            public event LoginResponseRowChangeEventHandler LoginResponseRowDeleting;
            
            public event LoginResponseRowChangeEventHandler LoginResponseRowDeleted;
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void AddLoginResponseRow(LoginResponseRow row) {
                this.Rows.Add(row);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public LoginResponseRow AddLoginResponseRow(int MobileUnit_ID, string LoginResposne, string DriverName, string TruckType, bool DayLigh_Savings, byte GMT_Offset_Hours, byte GMT_Offset_Mins) {
                LoginResponseRow rowLoginResponseRow = ((LoginResponseRow)(this.NewRow()));
                object[] columnValuesArray = new object[] {
                        MobileUnit_ID,
                        LoginResposne,
                        DriverName,
                        TruckType,
                        DayLigh_Savings,
                        GMT_Offset_Hours,
                        GMT_Offset_Mins};
                rowLoginResponseRow.ItemArray = columnValuesArray;
                this.Rows.Add(rowLoginResponseRow);
                return rowLoginResponseRow;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public virtual global::System.Collections.IEnumerator GetEnumerator() {
                return this.Rows.GetEnumerator();
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public override global::System.Data.DataTable Clone() {
                LoginResponseDataTable cln = ((LoginResponseDataTable)(base.Clone()));
                cln.InitVars();
                return cln;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override global::System.Data.DataTable CreateInstance() {
                return new LoginResponseDataTable();
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            internal void InitVars() {
                this.columnMobileUnit_ID = base.Columns["MobileUnit_ID"];
                this.columnLoginResposne = base.Columns["LoginResposne"];
                this.columnDriverName = base.Columns["DriverName"];
                this.columnTruckType = base.Columns["TruckType"];
                this.columnDayLigh_Savings = base.Columns["DayLigh_Savings"];
                this.columnGMT_Offset_Hours = base.Columns["GMT_Offset_Hours"];
                this.columnGMT_Offset_Mins = base.Columns["GMT_Offset_Mins"];
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            private void InitClass() {
                this.columnMobileUnit_ID = new global::System.Data.DataColumn("MobileUnit_ID", typeof(int), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnMobileUnit_ID);
                this.columnLoginResposne = new global::System.Data.DataColumn("LoginResposne", typeof(string), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnLoginResposne);
                this.columnDriverName = new global::System.Data.DataColumn("DriverName", typeof(string), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnDriverName);
                this.columnTruckType = new global::System.Data.DataColumn("TruckType", typeof(string), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnTruckType);
                this.columnDayLigh_Savings = new global::System.Data.DataColumn("DayLigh_Savings", typeof(bool), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnDayLigh_Savings);
                this.columnGMT_Offset_Hours = new global::System.Data.DataColumn("GMT_Offset_Hours", typeof(byte), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnGMT_Offset_Hours);
                this.columnGMT_Offset_Mins = new global::System.Data.DataColumn("GMT_Offset_Mins", typeof(byte), null, global::System.Data.MappingType.Element);
                base.Columns.Add(this.columnGMT_Offset_Mins);
                this.columnMobileUnit_ID.DefaultValue = ((int)(0));
                this.columnLoginResposne.DefaultValue = ((string)("C"));
                this.columnLoginResposne.MaxLength = 1;
                this.columnDriverName.MaxLength = 20;
                this.columnTruckType.DefaultValue = ((string)("D"));
                this.columnTruckType.MaxLength = 1;
                this.columnDayLigh_Savings.DefaultValue = ((bool)(true));
                this.columnGMT_Offset_Hours.DefaultValue = ((byte)(10));
                this.columnGMT_Offset_Mins.DefaultValue = ((byte)(0));
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public LoginResponseRow NewLoginResponseRow() {
                return ((LoginResponseRow)(this.NewRow()));
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override global::System.Data.DataRow NewRowFromBuilder(global::System.Data.DataRowBuilder builder) {
                return new LoginResponseRow(builder);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override global::System.Type GetRowType() {
                return typeof(LoginResponseRow);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override void OnRowChanged(global::System.Data.DataRowChangeEventArgs e) {
                base.OnRowChanged(e);
                if ((this.LoginResponseRowChanged != null)) {
                    this.LoginResponseRowChanged(this, new LoginResponseRowChangeEvent(((LoginResponseRow)(e.Row)), e.Action));
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override void OnRowChanging(global::System.Data.DataRowChangeEventArgs e) {
                base.OnRowChanging(e);
                if ((this.LoginResponseRowChanging != null)) {
                    this.LoginResponseRowChanging(this, new LoginResponseRowChangeEvent(((LoginResponseRow)(e.Row)), e.Action));
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override void OnRowDeleted(global::System.Data.DataRowChangeEventArgs e) {
                base.OnRowDeleted(e);
                if ((this.LoginResponseRowDeleted != null)) {
                    this.LoginResponseRowDeleted(this, new LoginResponseRowChangeEvent(((LoginResponseRow)(e.Row)), e.Action));
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            protected override void OnRowDeleting(global::System.Data.DataRowChangeEventArgs e) {
                base.OnRowDeleting(e);
                if ((this.LoginResponseRowDeleting != null)) {
                    this.LoginResponseRowDeleting(this, new LoginResponseRowChangeEvent(((LoginResponseRow)(e.Row)), e.Action));
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void RemoveLoginResponseRow(LoginResponseRow row) {
                this.Rows.Remove(row);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public static global::System.Xml.Schema.XmlSchemaComplexType GetTypedTableSchema(global::System.Xml.Schema.XmlSchemaSet xs) {
                global::System.Xml.Schema.XmlSchemaComplexType type = new global::System.Xml.Schema.XmlSchemaComplexType();
                global::System.Xml.Schema.XmlSchemaSequence sequence = new global::System.Xml.Schema.XmlSchemaSequence();
                ToDriver_LoginResponse ds = new ToDriver_LoginResponse();
                global::System.Xml.Schema.XmlSchemaAny any1 = new global::System.Xml.Schema.XmlSchemaAny();
                any1.Namespace = "http://www.w3.org/2001/XMLSchema";
                any1.MinOccurs = new decimal(0);
                any1.MaxOccurs = decimal.MaxValue;
                any1.ProcessContents = global::System.Xml.Schema.XmlSchemaContentProcessing.Lax;
                sequence.Items.Add(any1);
                global::System.Xml.Schema.XmlSchemaAny any2 = new global::System.Xml.Schema.XmlSchemaAny();
                any2.Namespace = "urn:schemas-microsoft-com:xml-diffgram-v1";
                any2.MinOccurs = new decimal(1);
                any2.ProcessContents = global::System.Xml.Schema.XmlSchemaContentProcessing.Lax;
                sequence.Items.Add(any2);
                global::System.Xml.Schema.XmlSchemaAttribute attribute1 = new global::System.Xml.Schema.XmlSchemaAttribute();
                attribute1.Name = "namespace";
                attribute1.FixedValue = ds.Namespace;
                type.Attributes.Add(attribute1);
                global::System.Xml.Schema.XmlSchemaAttribute attribute2 = new global::System.Xml.Schema.XmlSchemaAttribute();
                attribute2.Name = "tableTypeName";
                attribute2.FixedValue = "LoginResponseDataTable";
                type.Attributes.Add(attribute2);
                type.Particle = sequence;
                global::System.Xml.Schema.XmlSchema dsSchema = ds.GetSchemaSerializable();
                if (xs.Contains(dsSchema.TargetNamespace)) {
                    global::System.IO.MemoryStream s1 = new global::System.IO.MemoryStream();
                    global::System.IO.MemoryStream s2 = new global::System.IO.MemoryStream();
                    try {
                        global::System.Xml.Schema.XmlSchema schema = null;
                        dsSchema.Write(s1);
                        for (global::System.Collections.IEnumerator schemas = xs.Schemas(dsSchema.TargetNamespace).GetEnumerator(); schemas.MoveNext(); ) {
                            schema = ((global::System.Xml.Schema.XmlSchema)(schemas.Current));
                            s2.SetLength(0);
                            schema.Write(s2);
                            if ((s1.Length == s2.Length)) {
                                s1.Position = 0;
                                s2.Position = 0;
                                for (; ((s1.Position != s1.Length) 
                                            && (s1.ReadByte() == s2.ReadByte())); ) {
                                    ;
                                }
                                if ((s1.Position == s1.Length)) {
                                    return type;
                                }
                            }
                        }
                    }
                    finally {
                        if ((s1 != null)) {
                            s1.Close();
                        }
                        if ((s2 != null)) {
                            s2.Close();
                        }
                    }
                }
                xs.Add(dsSchema);
                return type;
            }
        }
        
        /// <summary>
        ///Represents strongly named DataRow class.
        ///</summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
        public partial class LoginResponseRow : global::System.Data.DataRow {
            
            private LoginResponseDataTable tableLoginResponse;
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            internal LoginResponseRow(global::System.Data.DataRowBuilder rb) : 
                    base(rb) {
                this.tableLoginResponse = ((LoginResponseDataTable)(this.Table));
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public int MobileUnit_ID {
                get {
                    try {
                        return ((int)(this[this.tableLoginResponse.MobileUnit_IDColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'MobileUnit_ID\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.MobileUnit_IDColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public string LoginResposne {
                get {
                    try {
                        return ((string)(this[this.tableLoginResponse.LoginResposneColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'LoginResposne\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.LoginResposneColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public string DriverName {
                get {
                    try {
                        return ((string)(this[this.tableLoginResponse.DriverNameColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'DriverName\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.DriverNameColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public string TruckType {
                get {
                    try {
                        return ((string)(this[this.tableLoginResponse.TruckTypeColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'TruckType\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.TruckTypeColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool DayLigh_Savings {
                get {
                    try {
                        return ((bool)(this[this.tableLoginResponse.DayLigh_SavingsColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'DayLigh_Savings\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.DayLigh_SavingsColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public byte GMT_Offset_Hours {
                get {
                    try {
                        return ((byte)(this[this.tableLoginResponse.GMT_Offset_HoursColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'GMT_Offset_Hours\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.GMT_Offset_HoursColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public byte GMT_Offset_Mins {
                get {
                    try {
                        return ((byte)(this[this.tableLoginResponse.GMT_Offset_MinsColumn]));
                    }
                    catch (global::System.InvalidCastException e) {
                        throw new global::System.Data.StrongTypingException("The value for column \'GMT_Offset_Mins\' in table \'LoginResponse\' is DBNull.", e);
                    }
                }
                set {
                    this[this.tableLoginResponse.GMT_Offset_MinsColumn] = value;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsMobileUnit_IDNull() {
                return this.IsNull(this.tableLoginResponse.MobileUnit_IDColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetMobileUnit_IDNull() {
                this[this.tableLoginResponse.MobileUnit_IDColumn] = global::System.Convert.DBNull;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsLoginResposneNull() {
                return this.IsNull(this.tableLoginResponse.LoginResposneColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetLoginResposneNull() {
                this[this.tableLoginResponse.LoginResposneColumn] = global::System.Convert.DBNull;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsDriverNameNull() {
                return this.IsNull(this.tableLoginResponse.DriverNameColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetDriverNameNull() {
                this[this.tableLoginResponse.DriverNameColumn] = global::System.Convert.DBNull;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsTruckTypeNull() {
                return this.IsNull(this.tableLoginResponse.TruckTypeColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetTruckTypeNull() {
                this[this.tableLoginResponse.TruckTypeColumn] = global::System.Convert.DBNull;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsDayLigh_SavingsNull() {
                return this.IsNull(this.tableLoginResponse.DayLigh_SavingsColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetDayLigh_SavingsNull() {
                this[this.tableLoginResponse.DayLigh_SavingsColumn] = global::System.Convert.DBNull;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsGMT_Offset_HoursNull() {
                return this.IsNull(this.tableLoginResponse.GMT_Offset_HoursColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetGMT_Offset_HoursNull() {
                this[this.tableLoginResponse.GMT_Offset_HoursColumn] = global::System.Convert.DBNull;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public bool IsGMT_Offset_MinsNull() {
                return this.IsNull(this.tableLoginResponse.GMT_Offset_MinsColumn);
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public void SetGMT_Offset_MinsNull() {
                this[this.tableLoginResponse.GMT_Offset_MinsColumn] = global::System.Convert.DBNull;
            }
        }
        
        /// <summary>
        ///Row event argument class
        ///</summary>
        [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Data.Design.TypedDataSetGenerator", "2.0.0.0")]
        public class LoginResponseRowChangeEvent : global::System.EventArgs {
            
            private LoginResponseRow eventRow;
            
            private global::System.Data.DataRowAction eventAction;
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public LoginResponseRowChangeEvent(LoginResponseRow row, global::System.Data.DataRowAction action) {
                this.eventRow = row;
                this.eventAction = action;
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public LoginResponseRow Row {
                get {
                    return this.eventRow;
                }
            }
            
            [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
            public global::System.Data.DataRowAction Action {
                get {
                    return this.eventAction;
                }
            }
        }
    }
}

#pragma warning restore 1591
using System;
using System.Collections;
using MTData.Common.Utilities;


namespace MTData.MDT.Interface
{

    public interface IExternalMessage
    {
        byte[] Process();
        IExternalMessage GetInstance();
        string UnitID { get; set; }
        int JobID { get; set; }
        bool SendOnToOutsideClient { get; }
        void RemoveHeaderFooter();
        byte[] BuildOutgoingPacket();
        string ToString();
        string GetMessageType();
    }
    /// <summary>
    /// Summary description for cCommonPacketData.
    /// </summary>	
    public class cCommonPacketData : IExternalMessage
    {
        protected ArrayList data;
        public static byte StartOfPacketIndicator = 0x02;
        public static byte SpareBytesSeparator = 0x07;
        public static byte EndOfPacket = 0x03;
        public static byte EndOfLine = 0x0F;
        public static byte Delimiter = 0x0A;

        public int StartOfMsgPosition = 4;
        //public static int PACKET_ID_INDEX			= 1;		
        public static int PAYLOAD_LENGTH_INDEX = 1;
        public static int PAYLOAD_LENGTH_LENGTH = 2;
        public static int COMMAND_TYPE_INDEX = (PAYLOAD_LENGTH_INDEX + PAYLOAD_LENGTH_LENGTH);
        public static int COMMAND_TYPE_LENGTH = 1;
        public static int UNIT_ID_INDEX = (COMMAND_TYPE_INDEX + COMMAND_TYPE_LENGTH);
        public static int UNIT_ID_LENGTH = 4;
        public static int JOB_ID_INDEX = (UNIT_ID_INDEX + UNIT_ID_LENGTH);
        public static int JOB_ID_LENGTH = 4;

        public const int GPS_DATA_LENGTH = 18;
        protected const int MESSAGE_MAX_LENGTH = 127;
        protected bool UDP_SEND = false;

        private string unitID = "";
        private int jobID = -1;
        protected bool sendOnToOutsideClient = true;


        protected ByteStreamReader reader;
        protected ByteStreamWriter writer;

        public cCommonPacketData()
        {
            Build(null);
        }

        public cCommonPacketData(byte[] data)
        {
            Build(data);
        }

        public bool SendOnToOutsideClient
        {
            get
            {
                return this.sendOnToOutsideClient;
            }
        }

        public byte[] BuildOutgoingPacket()
        {
            return (byte[])data.ToArray(typeof(byte));
        }

        private void Build(byte[] dat)
        {
            this.data = new ArrayList();
            if (dat == null)
            {
                dat = new byte[4];
                dat[0] = StartOfPacketIndicator;
                //dat[1] = 0;  // PacketID
                dat[1] = 0;  // Payload length
                dat[2] = 0;  // Payload length
            }
            int cnt = 0;
            this.data.Add(dat[cnt++]);
            //this.data.Add (dat[cnt++]); // PacketID
            this.data.Add(dat[cnt++]); // Length 
            this.data.Add(dat[cnt++]); // Length
            reader = new ByteStreamReader((byte[])this.data.ToArray(typeof(byte)));
            writer = new ByteStreamWriter();
            writer.WriteByteArray((byte[])this.data.ToArray(typeof(byte)));
        }

        public string GetYN(bool v)
        {
            if (v)
                return "Y";
            return "N";
        }

        public bool GetBool(string v)
        {
            return (v.Trim().ToUpper().Equals("Y"));
        }

        public DateTime GetDDHHMM(byte[] b)
        {
            byte[] dayBytes = new byte[2];
            int startIndex = 0;
            dayBytes[0] = (byte)b[startIndex++];
            dayBytes[1] = (byte)b[startIndex++];
            int day = BitConverter.ToInt16(dayBytes, 0);

            byte[] hourBytes = new byte[2];
            hourBytes[0] = (byte)b[startIndex++];
            hourBytes[1] = (byte)b[startIndex++];
            int hour = BitConverter.ToInt16(hourBytes, 0);

            byte[] minuteBytes = new byte[2];
            minuteBytes[0] = (byte)b[startIndex++];
            minuteBytes[1] = (byte)b[startIndex++];
            int min = BitConverter.ToInt16(minuteBytes, 0);

            return new DateTime(DateTime.MinValue.Year, DateTime.MinValue.Month, day, hour, min, DateTime.MinValue.Second);

        }

        protected string GetUDPMessageToSend()
        {
            return "";
        }

        protected void SetType(string t)
        {
            SetType(System.Text.ASCIIEncoding.ASCII.GetBytes(t)[0]);
        }

        public void PadToLength(int len)
        {
            while (this.data.Count <= len)
                this.data.Add((byte)0x00);
        }

        protected void SetType(byte t)
        {
            PadToLength(COMMAND_TYPE_INDEX);
            this.data[COMMAND_TYPE_INDEX] = t;
        }

        public string GetMessageType()
        {
            try
            {
                byte[] b = new byte[1];
                b[0] = (byte)this.data[COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b);
            }
            catch { return ""; }
        }

        protected void AddFooter()
        {
            this.data.Add(SpareBytesSeparator);
            this.data.Add(EndOfPacket);
        }

        public void SetMessageLength()
        {
            ushort cnt = (ushort)this.data.Count;
            if (this.data[cnt - 1].Equals(EndOfPacket)) // subtract the end bytes from count
                cnt -= 1;
            cnt -= 3;
            byte[] count = new byte[4];
            count = BitConverter.GetBytes(cnt);
            this.data[PAYLOAD_LENGTH_INDEX] = count[0];
            this.data[PAYLOAD_LENGTH_INDEX + 1] = count[1];
        }

        public void Finalise()
        {
            AddFooter();
            SetMessageLength();
        }

        public byte[] Data
        {
            get
            {
                return (byte[])data.ToArray(typeof(byte));
            }
        }

        public byte PacketID
        {
            get
            {
                return (byte)data[1];
            }
            set
            {
                this.data[1] = value;
            }
        }

        public byte Length
        {
            get
            {
                return (byte)data[2];
            }
            set
            {
                this.data[2] = value;
            }
        }

        public bool ContainsHeaderAndFooter()
        {
            try
            {
                if (this.data.Count > 0)
                {
                    if (this.data[0].Equals(cCommonPacketData.StartOfPacketIndicator))
                    {
                        if (this.data[this.data.Count - 1].Equals(cCommonPacketData.EndOfPacket))
                        {
                            if (this.data[this.data.Count - 2].Equals(cCommonPacketData.SpareBytesSeparator))
                                return true;
                        }
                    }
                }
            }
            catch { }
            return false;
        }

        public int SetJobID()
        {
            // Job ID
            PadToLength(JOB_ID_INDEX + JOB_ID_LENGTH);
            byte[] byts = BitConverter.GetBytes(JobID);
            int pos = JOB_ID_INDEX;
            foreach (byte c in byts)
                this.data[pos++] = c;
            this.writer = new ByteStreamWriter();
            this.writer.WriteByteArray((byte[])this.data.ToArray(typeof(byte)));
            return pos;
        }

        public void Set()
        {
            SetUnitID();
            SetJobID();
        }

        protected void SetUnitID()
        {
            // Unit ID
            PadToLength(UNIT_ID_INDEX);
            byte[] b = new byte[4];
            while (this.UnitID.Length < 4)
                UnitID = UnitID.Insert(0, "0");
            b = System.Text.ASCIIEncoding.ASCII.GetBytes(UnitID.ToString());
            this.data[UNIT_ID_INDEX] = b[0];
            this.data.Add(b[1]);
            this.data.Add(b[2]);
            this.data.Add(b[3]);
            this.writer = new ByteStreamWriter();
            this.writer.WriteByteArray((byte[])this.data.ToArray(typeof(byte)));
        }

        public int GetUnitID()
        {
            // Unit ID
            byte[] unitIDBytes = new byte[4];
            unitIDBytes[0] = (byte)this.data[UNIT_ID_INDEX];
            unitIDBytes[1] = (byte)this.data[UNIT_ID_INDEX + 1];
            unitIDBytes[2] = (byte)this.data[UNIT_ID_INDEX + 2];
            unitIDBytes[3] = (byte)this.data[UNIT_ID_INDEX + 3];
            UnitID = System.Text.ASCIIEncoding.ASCII.GetString(unitIDBytes);
            reader = new ByteStreamReader(this.Data);
            int x = UNIT_ID_INDEX + 4;
            reader.MoveBytes(x);
            return x;
        }

        public int GetJobID()
        {
            // Job ID
            return GetJobID(JOB_ID_INDEX);
        }

        public int GetJobID(int index)
        {
            // Job ID
            ArrayList a = this.data.GetRange(index, JOB_ID_LENGTH);
            byte[] b = (byte[])a.ToArray(typeof(byte));
            this.JobID = BitConverter.ToInt32(b, 0);
            return (index + JOB_ID_LENGTH);
        }

        public int Get()
        {
            GetUnitID();
            return GetJobID();
        }

        public string UnitID
        {
            get
            {
                return this.unitID;
            }
            set
            {
                this.unitID = value;
            }
        }

        public int JobID
        {
            get
            {
                return this.jobID;
            }
            set
            {
                this.jobID = value;
            }
        }

        #region MDTMessage Members

        public virtual byte[] Process()
        {
            return (byte[])this.data.ToArray(typeof(byte));
            // TODO:  Add cCommonPacketData.Process implementation
        }

        public void RemoveHeaderFooter()
        {
            if (this.data.Count > 0)
            {
                if (this.data[0].Equals(StartOfPacketIndicator))
                {
                    this.data.RemoveAt(0); // header
                    this.data.RemoveAt(0); // len1
                    this.data.RemoveAt(0); // len2
                }
                if (this.data[this.data.Count].Equals(EndOfPacket))
                {
                    this.data.RemoveAt(this.data.Count); // EOP
                    this.data.RemoveAt(this.data.Count); // Spare byte separator
                }
            }
        }

        public override string ToString()
        {
            return "UnitID = " + UnitID + " JobID = " + JobID;
        }

        public static bool IsType(byte[] data, string type, int index)
        {
            byte[] b = new byte[1];
            // exclude header and length since we dont get them			

            b[0] = data[index];
            return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(type);
        }

        public static bool IsType(byte[] data, string v)
        {
            byte[] b = new byte[1];
            b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
            return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(v);
        }

        public IExternalMessage GetInstance()
        {
            return null;
        }
        #endregion
    }



    public class AckNack : cCommonPacketData
    {
        public const string Type = "N";
        public int ResultCode = -1;
        public int LegNumber = -1;

        public AckNack()
            : base()
        {
            SetType(Type);
        }

        public AckNack(byte[] data)
            : base(data)
        {
            this.data.Clear();
            this.data.AddRange(data);
        }

        public new void Get()
        {
            int index = base.GetUnitID();
            this.ResultCode = Convert.ToInt32(data[index++]);
            this.JobID = base.GetJobID(index++);
            this.LegNumber = Convert.ToInt32(this.data[index++]);
        }

        public new void Set()
        {
            this.SetUnitID();
            this.data.Add((byte)this.ResultCode);
            this.data.AddRange(BitConverter.GetBytes(this.JobID));
            this.data.Add((byte)this.LegNumber);
            this.Finalise();
        }
    }


}

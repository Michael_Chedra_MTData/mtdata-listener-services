using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Configuration;
using System.Collections;
using System.IO;
using MTData.Common.Config;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cConvertSignature.
    /// </summary>
    public class cConvertSignature
    {

        private ConvertSignatureConfig _config = null;

        /// <summary>
        /// Prepare te signature converter for use.
        /// </summary>
        /// <param name="config"></param>
        public cConvertSignature(ConvertSignatureConfig config)
        {
            _config = config ?? new ConvertSignatureConfig(); //(config != null) ? config : new ConvertSignatureConfig();

        }

        private string _fileName = "";
        public string FileName
        {
            get
            {
                return _fileName;
            }
            set
            {
                _fileName = value;
            }
        }

        public byte[] TranslateSignature(byte[] bData, string fileName)
        {
            _fileName = fileName + ".gif";
            return TranslateSignature(bData);
        }

        /// <summary>
        /// this code has been modified to enusre that the signature is transparent
        /// NO attempt to use any background GIF images will be entertained.
        /// Signature will be portrayed black on transparent background
        /// </summary>
        /// <param name="bData"></param>
        /// <returns></returns>
        public byte[] TranslateSignature(byte[] bData)
        {

            #region Local Vars
            byte bTemp = (byte)0x00;
            byte bExamine = (byte)0x00;
            byte[] bRet = new byte[1];
            byte[] bConvert = new byte[4];
            byte[] bWhiteSpace = new byte[1]; // (iWhiteBits bits)
            byte[] bBlackSpace = new byte[1]; // (iBlackBits bits)
            byte[] bBitCount = new byte[4];
            byte[] bDoIt = new byte[4];
            int iHeight = 0;
            int iWidth = 0;
            int iHeightOffset = 0;
            int iWidthOffset = 0;
            int iMaxHeight = _config.MaxHeight;
            int iMaxWidth = _config.MaxWidth;
            int iBlackBits = 0;
            int iWhiteBits = 0;
            int iCount = 0;
            int iBitCount = 0;
            int iPixels = 0;
            int iBitMask = (int)0x100;
            int iX = 0;
            int iY = 0;
            int iPaint = 0;
            double dResizeFactor = 0;
            System.Drawing.Bitmap oBMP = null;
            cCollection oCol = null;
            cCollection oConvertedCol = null;
            System.Drawing.Color cBackGroundColor = _config.SignatureBackgroundColor;
            byte[] bResult = null;
            bResult = new byte[1];
            bResult[0] = (byte)0x00;
            string sFilename = "";
            #endregion

            try
            {
                try
                {
                    #region Setup Vars
                    oCol = new cCollection();
                    oConvertedCol = new cCollection();
                    if (_fileName == "")
                        _fileName = Convert.ToString(System.DateTime.Now.Day) + Convert.ToString(System.DateTime.Now.Month) + Convert.ToString(System.DateTime.Now.Year) + Convert.ToString(System.DateTime.Now.Hour) + Convert.ToString(System.DateTime.Now.Minute) + Convert.ToString(System.DateTime.Now.Second) + Convert.ToString(System.DateTime.Now.Millisecond) + ".gif";
                    sFilename = _config.PicturePath + _fileName;
                    dResizeFactor = _config.ResizeFactor;

                    iMaxHeight = Convert.ToInt32(Math.Round(Convert.ToDecimal(iMaxHeight * (dResizeFactor + 1)), 0));
                    iMaxWidth = Convert.ToInt32(Math.Round(Convert.ToDecimal(iMaxWidth * (dResizeFactor + 1)), 0));

                    #endregion
                }
                catch (System.Exception exSetupVars)
                {
                    Console.Write(exSetupVars.Message + "\nSource : " + exSetupVars.Source);

                }

                try
                {
                    #region Get the Image Size

                    // Initialise the conversion byte array
                    bConvert[0] = (byte)0x00;
                    bConvert[1] = (byte)0x00;
                    bConvert[2] = (byte)0x00;
                    bConvert[3] = (byte)0x00;

                    // Get the height and width.
                    bConvert[0] = bData[1];
                    iHeight = BitConverter.ToInt32(bConvert, 0);
                    bConvert[0] = bData[2];
                    iWidth = BitConverter.ToInt32(bConvert, 0);
                    iWidth = iWidth * 8;  // Width is in bytes, so x8 to convert to pixels.

                    // Get the encoding type
                    bConvert[0] = bData[3];
                    iWhiteBits = BitConverter.ToInt32(bConvert, 0);
                    bConvert[0] = bData[4];
                    iBlackBits = BitConverter.ToInt32(bConvert, 0);

                    // Convert the bytes to a collection of bits, starting at byte 3
                    for (iCount = 5; iCount < (bData.Length - 1); iCount++)
                    {
                        bTemp = bData[iCount];

                        cCollection otempcol = new cCollection();

                        for (iBitCount = 7; iBitCount >= 0; iBitCount--)
                        {
                            // Examine the iBitCount'th bit of the bTemp byte.
                            bExamine = (byte)((bTemp >> iBitCount) & 0x01);
                            if (bExamine == (byte)0x00)
                            {
                                //otempcol.Add(0);
                                oCol.Add(0);
                            }
                            else
                            {
                                //otempcol.Add(1);
                                oCol.Add(1);
                            }
                        }
                    }

                    // Create a new bitmap in memory.
                    oBMP = new System.Drawing.Bitmap(iMaxWidth, iMaxHeight);
                    //System.Drawing.Bitmap oBMP = new System.Drawing.Bitmap(iWidth, iHeight);
                    #endregion
                }
                catch (System.Exception exReadImageDefault)
                {
                    Console.Write(exReadImageDefault.Message + "\nSource : " + exReadImageDefault.Source);
                }

                try
                {

                    #region Setup default image
                    //				// Paint the bitmap white.
                    for (iY = 0; iY < iMaxHeight; iY++)
                    {
                        for (iX = 0; iX < iMaxWidth; iX++)
                        {
                            //oBMP.SetPixel(iX, iY, System.Drawing.Color.White);
                            oBMP.SetPixel(iX, iY, cBackGroundColor);
                        }
                    }
                    // (iMaxHeight - iHeight) / 2 as a rounded integer.
                    iHeightOffset = Convert.ToInt32(Math.Round(Convert.ToDecimal((iMaxHeight - iHeight) / 2), 0));
                    if (iHeightOffset > 1) iHeightOffset = iHeightOffset - 1;

                    // (iMaxHeight - iHeight) / 2 as a rounded integer.
                    iWidthOffset = Convert.ToInt32(Math.Round(Convert.ToDecimal((iMaxWidth - iWidth) / 2), 0));
                    if (iWidthOffset > 1) iWidthOffset = iWidthOffset - 1;
                    #endregion
                }
                catch (System.Exception exSetupImage)
                {
                    Console.Write(exSetupImage.Message + "\nSource : " + exSetupImage.Source);
                }


                // Move to the start of the image.
                iX = 0;
                iY = 0;
                bool bError = false;
                // Expand the bit collection to a collections of lengths
                for (iCount = 1; iCount <= oCol.Count; iCount++)
                {
                    if (oCol.IntItem(iCount) == 0)
                    {
                        try
                        {
                            #region Processing Background

                            // Set the bit mask for the iWhiteBitsth bit
                            iBitMask = 1 << (iWhiteBits - 1);
                            //iBitMask = (int) 0x100;
                            // Reset the reult value.
                            iPixels = 0;

                            // Process for white space (iWhiteBits bits)
                            for (iBitCount = 0; iBitCount < iWhiteBits; iBitCount++)
                            {
                                iCount++;  // bit array pointer
                                // If this bit is set, then add the mask to the result.
                                if (oCol.IntItem(iCount) == 1)
                                {
                                    iPixels += iBitMask;
                                }
                                // Shift the mask right by 1.
                                iBitMask = iBitMask >> 1;
                            }

                            if (iPixels > 0)
                            {

                                // Painting Background Pixels
                                while ((iX + iPixels) > iWidth)
                                {
                                    // If there are more pixels to be painted than are left on the
                                    // current line, paint to the end of the line, and reduce
                                    // iPixels by the number of pixels drawn.
                                    try
                                    {
                                        for (iPaint = iX; iPaint < iWidth; iPaint++)
                                        {
                                            oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, cBackGroundColor);
                                            iPixels--;
                                        }
                                    }
                                    catch (System.Exception ex4)
                                    {
                                        Console.Write(ex4.Message + "\n");
                                        bError = true;
                                        break;
                                    }
                                    // Move to the start of the next line in the image
                                    iY++;
                                    iX = 0;

                                    if (iY >= iHeight)
                                    {
                                        bError = true;
                                        break;
                                    }
                                }

                                if (bError) break;

                                try
                                {
                                    // If there are less pixels to be painted than are remaining
                                    // on the current line, paint the pixels and move iX to the
                                    // next pixel to be painted
                                    for (iPaint = iX; iPaint < (iX + iPixels); iPaint++)
                                    {
                                        oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, cBackGroundColor);
                                    }
                                    iX = iX + iPixels;
                                }
                                catch (System.Exception ex4)
                                {
                                    Console.Write(ex4.Message + "\n");
                                }

                                if (bError) break;
                            }
                            #endregion
                        }
                        catch (System.Exception exProcessWhite)
                        {
                            Console.Write(exProcessWhite.Message + "\nSource : " + exProcessWhite.Source);
                        }
                    }
                    else
                    {
                        try
                        {
                            #region Processing Black (Foreground)
                            // We are processing blacks

                            // Set the bit mask for the iBlackBitsrd bit
                            iBitMask = 1 << (iBlackBits - 1);
                            //iBitMask = (int) 0x04;

                            // Reset the reult value.
                            iPixels = 0;

                            // Process for black space (iBlackBits bits)
                            for (iBitCount = 0; iBitCount < iBlackBits; iBitCount++)
                            {
                                iCount++;  // bit array pointer
                                // If this bit is set, then add the mask to the result.
                                if (oCol.IntItem(iCount) == 1)
                                {
                                    iPixels += iBitMask;
                                }
                                // Shift the mask right by 1.
                                iBitMask = iBitMask >> 1;
                            }

                            if (iPixels > 0)
                            {
                                // Painting Black Pixels
                                while (iX + iPixels > iWidth)
                                {
                                    // If there are more pixels to be painted than are left on the
                                    // current line, paint to the end of the line, and reduce
                                    // iPixels by the number of pixels drawn.
                                    try
                                    {
                                        for (iPaint = iX; iPaint < iWidth; iPaint++)
                                        {
                                            oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, System.Drawing.Color.Black);
                                            iPixels--;
                                        }
                                    }
                                    catch (System.Exception ex4)
                                    {
                                        Console.Write(ex4.Message + "\n");
                                        bError = true;
                                        break;
                                    }
                                    // Move to the start of the next line.
                                    iY++;
                                    iX = 0;
                                    if (iY >= iHeight)
                                    {
                                        bError = true;
                                        break;
                                    }
                                }

                                if (bError) break;

                                try
                                {
                                    // If there are less pixels to be painted than are remaining
                                    // on the current line, paint the pixels and move iX to the
                                    // next pixel to be painted
                                    for (iPaint = iX; iPaint < (iX + iPixels); iPaint++)
                                    {
                                        oBMP.SetPixel(iPaint + iWidthOffset, iY + iHeightOffset, System.Drawing.Color.Black);
                                    }
                                    iX = iX + iPixels;
                                }
                                catch (System.Exception ex4)
                                {
                                    Console.Write(ex4.Message + "\n");
                                    bError = true;
                                    break;
                                }

                                if (bError) break;
                            }
                            #endregion
                        }
                        catch (System.Exception exProcessBlack)
                        {
                            Console.Write(exProcessBlack.Message + "\nSource : " + exProcessBlack.Source);
                        }

                    }

                }

                oBMP = ResizeImage(oBMP, Convert.ToInt32(oBMP.PhysicalDimension.Width * dResizeFactor), Convert.ToInt32(oBMP.PhysicalDimension.Height * dResizeFactor));

                if (_config.IncreaseContrast)
                {
                    uint uTolerance = (uint)_config.ContrastTolerance;
                    uint uBlack = (uint)Color.Black.ToArgb();
                    uint uBoundary = (uint)uBlack - uTolerance;

                    for (iY = 0; iY < oBMP.Height; iY++)
                    {
                        for (iX = 0; iX < oBMP.Width; iX++)
                        {
                            try
                            {
                                if (iX == 0 || iY == 0)
                                {
                                    oBMP.SetPixel(iX, iY, cBackGroundColor);
                                }
                                else
                                {
                                    #region Paint Black
                                    if (oBMP.GetPixel(iX, iY).ToArgb() != cBackGroundColor.ToArgb())
                                    {
                                        uint iExamineValue = (uint)oBMP.GetPixel(iX, iY).ToArgb();
                                        if (iExamineValue > uBoundary)
                                        {
                                            oBMP.SetPixel(iX, iY, System.Drawing.Color.Black);
                                        }
                                        else
                                        {
                                            oBMP.SetPixel(iX, iY, cBackGroundColor);
                                        }
                                    }
                                    else
                                    {
                                        oBMP.SetPixel(iX, iY, cBackGroundColor);
                                    }
                                }
                                    #endregion
                            }
                            catch (System.Exception exPaintBlack)
                            {
                                Console.Write(exPaintBlack.Message + "\nSource : " + exPaintBlack.Source);
                            }
                        }
                    }
                }
                else
                {
                    for (iY = 0; iY < oBMP.Height; iY++)
                    {
                        for (iX = 0; iX < oBMP.Width; iX++)
                        {
                            if (oBMP.GetPixel(iX, iY).ToArgb() == cBackGroundColor.ToArgb())
                            {
                                oBMP.SetPixel(iX, iY, cBackGroundColor);
                            }
                        }
                    }
                }

                if (_config.MakeSignatureTransparent)
                {
                    MemoryStream memStrm = new MemoryStream();
                    oBMP.Save(memStrm, ImageFormat.Gif);
                    oBMP = new Bitmap(memStrm);

                    //oBMP.Save(@"C:\MTDataLogs\SigTemp\Resized.GIF", ImageFormat.Gif);
                    //oBMP = new Bitmap(@"C:\MTDataLogs\SigTemp\Resized.GIF");

                    oBMP = MakeTransparent(oBMP, cBackGroundColor);
                }

                FileStream oNewFS = null;

                try
                {
                    if (System.IO.File.Exists(sFilename))
                        oNewFS = new FileStream(sFilename, System.IO.FileMode.Truncate);
                    else
                        oNewFS = new FileStream(sFilename, System.IO.FileMode.CreateNew);
                    try
                    {
                        oBMP.Save(oNewFS, System.Drawing.Imaging.ImageFormat.Gif);
                    }
                    finally
                    {
                        oNewFS.Close();
                        oNewFS = null;
                    }
                }
                catch (System.Exception exFileCreate)
                {
                    Console.Write(exFileCreate.Message + "\nSource : " + exFileCreate.Source);
                }

                byte[] bLoadData = new byte[4000];
                int iBytes = File.OpenRead(sFilename).Read(bLoadData, 0, 4000);

                bResult = new byte[iBytes];
                for (int iStrip = 0; iStrip < iBytes; iStrip++)
                    bResult[iStrip] = bLoadData[iStrip];

                if (!_config.SaveImageFile)
                {
                    File.Delete(sFilename);
                }
            }
            finally
            {
                if (oBMP != null)
                {
                    oBMP.Dispose();
                    oBMP = null;
                }

            }
            return bResult;
        }


        public Bitmap ResizeImage(Bitmap srcImg, int newWidth, int newHeight)
        {
            // get source image size
            int width = srcImg.Width;
            int height = srcImg.Height;

            if ((newWidth == width) && (newHeight == height))
            {
                // just clone the image
                return Globals.Clone(srcImg, PixelFormat.Format8bppIndexed);
            }

            PixelFormat fmt = (srcImg.PixelFormat == PixelFormat.Format8bppIndexed) ?
                PixelFormat.Format8bppIndexed : PixelFormat.Format24bppRgb;

            // create new image
            Bitmap dstImg = (fmt == PixelFormat.Format8bppIndexed) ?
                CreateGrayscaleImage(newWidth, newHeight) :
                new Bitmap(newWidth, newHeight, fmt);

            BitmapData srcData = null;
            BitmapData dstData = null;
            bool srcLocked = false;
            bool dstLocked = false;
            try
            {
                // lock source bitmap data
                srcData = srcImg.LockBits(
                    new Rectangle(0, 0, width, height),
                    ImageLockMode.ReadOnly, fmt);
                srcLocked = true;

                // lock destination bitmap data
                dstData = dstImg.LockBits(
                    new Rectangle(0, 0, newWidth, newHeight),
                    ImageLockMode.ReadWrite, fmt);
                dstLocked = true;

                int srcStride = srcData.Stride;
                int dstOffset = dstData.Stride - ((fmt == PixelFormat.Format8bppIndexed) ? newWidth : newWidth * 3);
                float xFactor = (float)width / newWidth;
                float yFactor = (float)height / newHeight;

                // do the job
                unsafe
                {
                    byte* src = (byte*)srcData.Scan0.ToPointer();
                    byte* dst = (byte*)dstData.Scan0.ToPointer();

                    #region Bicubic Interpolation -Extracted from " Image Processing Library" by Andrew Kirillov from www.codeproject.com, 2005
                    // ----------------------------------
                    // resize using bicubic interpolation
                    // ----------------------------------

                    float ox, oy, dx, dy, k1, k2;
                    float r, g, b;
                    int ox1, oy1, ox2, oy2;
                    int ymax = height - 1;
                    int xmax = width - 1;
                    byte* p;

                    if (fmt == PixelFormat.Format8bppIndexed)
                    {
                        // grayscale
                        for (int y = 0; y < newHeight; y++)
                        {
                            // Y coordinates
                            oy = (float)y * yFactor - 0.5f;
                            oy1 = (int)oy;
                            dy = oy - (float)oy1;

                            for (int x = 0; x < newWidth; x++, dst++)
                            {
                                // X coordinates
                                ox = (float)x * xFactor - 0.5f;
                                ox1 = (int)ox;
                                dx = ox - (float)ox1;

                                g = 0;

                                for (int n = -1; n < 3; n++)
                                {
                                    k1 = Globals.BiCubicKernel(dy - (float)n);

                                    oy2 = oy1 + n;
                                    if (oy2 < 0)
                                        oy2 = 0;
                                    if (oy2 > ymax)
                                        oy2 = ymax;

                                    for (int m = -1; m < 3; m++)
                                    {
                                        k2 = k1 * Globals.BiCubicKernel((float)m - dx);

                                        ox2 = ox1 + m;
                                        if (ox2 < 0)
                                            ox2 = 0;
                                        if (ox2 > xmax)
                                            ox2 = xmax;

                                        g += k2 * src[oy2 * srcStride + ox2];
                                    }
                                }
                                *dst = (byte)g;
                            }
                            dst += dstOffset;
                        }
                    }
                    else
                    {
                        // RGB
                        for (int y = 0; y < newHeight; y++)
                        {
                            // Y coordinates
                            oy = (float)y * yFactor - 0.5f;
                            oy1 = (int)oy;
                            dy = oy - (float)oy1;

                            for (int x = 0; x < newWidth; x++, dst += 3)
                            {
                                // X coordinates
                                ox = (float)x * xFactor - 0.5f;
                                ox1 = (int)ox;
                                dx = ox - (float)ox1;

                                r = g = b = 0;

                                for (int n = -1; n < 3; n++)
                                {
                                    k1 = Globals.BiCubicKernel(dy - (float)n);

                                    oy2 = oy1 + n;
                                    if (oy2 < 0)
                                        oy2 = 0;
                                    if (oy2 > ymax)
                                        oy2 = ymax;

                                    for (int m = -1; m < 3; m++)
                                    {
                                        k2 = k1 * Globals.BiCubicKernel((float)m - dx);

                                        ox2 = ox1 + m;
                                        if (ox2 < 0)
                                            ox2 = 0;
                                        if (ox2 > xmax)
                                            ox2 = xmax;

                                        // get pixel of original image
                                        p = src + oy2 * srcStride + ox2 * 3;

                                        r += k2 * p[RGB.R];
                                        g += k2 * p[RGB.G];
                                        b += k2 * p[RGB.B];
                                    }
                                }

                                dst[RGB.R] = (byte)r;
                                dst[RGB.G] = (byte)g;
                                dst[RGB.B] = (byte)b;
                            }
                            dst += dstOffset;
                        }
                    }
                    #endregion
                }
            }
            finally
            {
                if (dstLocked)
                    dstImg.UnlockBits(dstData);
                if (srcLocked)
                    srcImg.UnlockBits(srcData);
            }
            return dstImg;
        }

        public Bitmap MakeTransparent(Bitmap gifImage, Color targetBackground)
        {
            ColorPalette palette = gifImage.Palette;

            int backgroundOffset = -1;
            for (int loop = 0; loop < palette.Entries.Length; loop++)
            {
                Color color = (Color)palette.Entries[loop];
                if (color.ToArgb() == targetBackground.ToArgb())
                    backgroundOffset = loop;
            }

            if (backgroundOffset >= 0)
            {
                Bitmap newImage = new Bitmap(gifImage.Width, gifImage.Height, PixelFormat.Format8bppIndexed);

                ColorPalette newPalette = newImage.Palette;

                //copy all the entries from the old palette removing any transparency 

                int loop = 0;
                foreach (Color color in palette.Entries)
                    newPalette.Entries[loop++] = Color.FromArgb(255, color);

                //Set the newly selected transparency 
                newPalette.Entries[backgroundOffset] = Color.FromArgb(0, palette.Entries[backgroundOffset]);

                //re-insert the palette 
                newImage.Palette = newPalette;

                //now to copy the actual bitmap data 
                //lock the source and destination bits 
                BitmapData src = ((Bitmap)gifImage).LockBits(new Rectangle(0, 0, gifImage.Width, gifImage.Height), ImageLockMode.ReadOnly, gifImage.PixelFormat);
                BitmapData dst = newImage.LockBits(new Rectangle(0, 0, newImage.Width, newImage.Height), ImageLockMode.WriteOnly, newImage.PixelFormat);
                try
                {
                    //uses pointers so we need unsafe code. 
                    //the project is also compiled with /unsafe 
                    unsafe
                    {

                        //steps through each pixel 
                        for (int y = 0; y < gifImage.Height; y++)
                            for (int x = 0; x < gifImage.Width; x++)
                            {
                                //transferring the bytes 
                                ((byte*)dst.Scan0.ToPointer())[(dst.Stride * y) + x] = ((byte*)src.Scan0.ToPointer())[(src.Stride * y) + x];
                            }
                    }
                }
                finally
                {
                    //all done, unlock the bitmaps 
                    gifImage.UnlockBits(src);
                    newImage.UnlockBits(dst);
                }
                //newImage.Save(targetFile,ImageFormat.Gif);
                return newImage;
            }
            else
                return gifImage;

        }

        /// <summary>
        /// Create and initialize grayscale image
        /// </summary>
        public Bitmap CreateGrayscaleImage(int width, int height)
        {
            // create new image
            Bitmap bmp = new Bitmap(width, height, PixelFormat.Format8bppIndexed);
            // set palette to grayscale
            SetGrayscalePalette(bmp);
            // return new image
            return bmp;
        }

        /// <summary>
        /// Set pallete of the image to grayscale
        /// </summary>
        public void SetGrayscalePalette(Bitmap srcImg)
        {
            // check pixel format
            if (srcImg.PixelFormat != PixelFormat.Format8bppIndexed)
                throw new ArgumentException();

            // get palette
            ColorPalette cp = srcImg.Palette;
            // init palette
            for (int i = 0; i < 256; i++)
            {
                cp.Entries[i] = Color.FromArgb(i, i, i);
            }
            // set palette back
            srcImg.Palette = cp;
        }
    }
}

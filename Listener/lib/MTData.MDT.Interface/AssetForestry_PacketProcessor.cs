using System;
using System.Collections.Specialized;
using System.Data;
using System.Configuration;
using MTData.MDT.Interface;
using MTData.Common.Utilities;
using MTData.MotFileReader;

using System.Collections;
using System.Collections.Generic;
using System.IO;
using log4net;


namespace MTData.MDT.Interface
{
    public class AssetForestry_PacketProcessor
    {
        #region Incomming Packet Defs
        // [Standard Packet] = [SOP] [Length (2 Bytes)] [CmdType (1 Byte)][PacketData][SBS (1 Byte)][EOP (1 Byte)]
        //	[GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
        //	[Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
        // [Packet Data] Definitions
        //	Download Request - [SOP] [Length (2 Bytes)] [CmdType "D" (1 Byte)][Send Segment][SBS (1 Byte)][EOP (1 Byte)]
        #endregion
        #region Outgoing Packet Defs
        // [Standard Packet] = [SOP] [Length (2 Bytes)] [CmdType (1 Byte)][PacketData][SBS (1 Byte)][EOP (1 Byte)]
        // [Packet Data] Definitions
        //	Download Packets - CmdType = "D"
        //		Download Header :		[SubCmd = H][Software Version (2 Bytes)][Number of Segments (4 Bytes)]
        //		Download Data :			[SubCmd = D][Segment Number (4 Bytes)][Segment Length (2 Bytes)][Data (Var Length][CheckSum (2 bytes)]
        #endregion
        #region Private Vars
        private const string sClassName = "MTData.Terminal.Interface";
        private ILog _log = LogManager.GetLogger(typeof(AssetForestry_PacketProcessor));
        private bool bTranslateRJs = true;
        #endregion
        #region Public Vars

        #region File download vars
        public ushort MotFileRequestedParcelNo = 0;
        public byte SoftwareVersionMajor = 0;
        public byte SoftwareVersionMinor = 0;
        public ushort TotalNumberOfDataParcels = 0;
        public ushort NumberOfDataParcels = 0;
        public ushort DownloadMotParcelNumber = 0;
        public ArrayList DownloadMotParcel = null;
        public ushort[] DownloadMotParcelCheckSum = null;
        public byte DownloadVersion = 0;
        public ushort PacketNumber = 0;
        public ushort RequestedParcelNo;

        //download verfity variables
        public int ProgramTotalCheckSum = 0;
        public int PacketTotalCheckSum = 0;
        public int ProgramPacketCountTotal = 0;
        public int ProgramByteTotal = 0;
        public int ProgramDataBytesCheckSum = 0;
        #endregion

        public string sSigFilePath = "";
        public byte[] bRawData = null;

        // Shared Public variables
        public string CommandType = "";						// [CmdType]
        public int iMobileID = 0;							// [Mobile ID]
        public string SubCommandType = "";					// [SubCmd]
        public int VehicleID = 0;							// [VehicleID]
        public int DriverID = 0;							// [DriverID]
        public int FleetID = 0;								// [FleetID]
        public int JobID = 0;								// [JobID]
        public string JobState = "";						//
        public string JobState_GeneratedType = "";
        public int ProtocolVersion = 0;						// [Protocol Version]
        public int HardwareType = 0;						// [Hardware Type]
        public uint Distance = 0;							// [Distance]
        public int DeviceTimeSeconds = 0;					// Time since last GPS lock
        public cGPSData GPSPosition = null;					// [GPS] 

        //Login Response
        public string Logon_Result = "";		// Login result
        //	C: Login accepted
        //	U: Unknown username
        //	P: Incorrect password
        //	Q: Contact query channe
        public string Logon_DriversName = "";		// Login Drivers name
        public string Logon_VehicleType = "";		// Flag set if the vehicle is a truck, 1 for truck
        public string Logon_VehicleLocation = "";
        public string Logon_DaylightSavings = "";		// Login daylights savings active, "Y", or "N"
        public int Logon_GMT_OffsetHrs = 0;		// GMT Offset Hours
        public int Logon_GMT_OffsetMins = 0;		// GMT Offset Minutes
        public int Logon_NetworkKeepAlive = 0;		// network keep alive time in seconds
        public int Logon_NewMobileID = 0;		// change of mobile id
        public int Logon_ServerMajor_FirmwareVersion = 0;
        public int Logon_ServerMinor_FirmwareVersion = 0;
        public int Logon_DriverFlags = 0;			// bit 0: set for driver free dial option
        public int Logon_AutoLogoffTimer = 0;		// timer in minutes to auto logoff the driver

        public string Logon_Report_FitForDuty = "";
        public int Logon_Report_QuestionListID_Used = 0;
        #region Logon driver answers
        public class DriverLoginAnswers
        {
            public int AnswerID = 0;
            public string AnswerType = "";
            public string Answer = "";
        }
        #endregion

        public ArrayList Logon_Report_QuestionAnswers = new ArrayList();
        public string Logon_Report_SignatureAddress = "";
        public byte[] Logon_Report_Signature = null;		// signature

        #region Job Releated Variables

        //Truck job variables
        public int Asset_UpliftNo = 0;
        public string Asset_Job_Status = "";
        public DateTime Asset_DateTime_of_Uplift;
        public DateTime Asset_Date_of_Delivery;
        public int Asset_Uplift_ID_Out = 0;
        public int Asset_Uplift_ID_In = 0;
        public string Asset_Docket_Number = "";
        public bool Asset_Uplift_LogGradeDiffer = false;
        public string Asset_Uplift_Indicator = "";
        public string Asset_BackLoad_Indicator = "";
        public string Asset_Crew_Code = "";
        public string Asset_Setting_Code = "";
        public string Asset_Loader_Code = "";
        public string Asset_Loader_Driver_Name = "";
        public string Asset_Location_Code = "";
        public string Asset_Compartment_Description = "";
        public string Asset_Compartment_Code = "";
        public string Asset_Crew_Location_Comment = "";
        public string Asset_Customer_Code = "";
        public string Asset_Customer_Name = "";

        public int Asset_Customer_Radius = 0;
        public string Asset_Uplift_WoodType = "";
        public string Asset_Delivery_WoodType = "";
        public decimal Asset_Tare_Weight = 0;
        public decimal Asset_Gross_Weight = 0;
        public decimal Asset_Net_Weight = 0;
        public DateTime Asset_Fell_Date;
        public string Asset_Number_of_Logs = "";
        public decimal Asset_Volume = 0;
        public string Asset_Reference_No = "";
        public string Asset_Directions = "";
        public int Asset_Uplift_Ack_Result = 0;
        public ushort Asset_CrewToCustomerTime_Minutes = 0;
		public bool Asset_AutoStatusOnly = false;							//Stage 2D, section 1.0 
		public int Asset_DocketDigits = -1;									//Number of Docket Digits required on entry

        public class Asset_WaypointData
        {
            public int Latitude = 0;
            public int Longitude = 0;
            public int Radius = 0;
            public int Radius2 = 0;
            public byte DwellTime = 0;
        }
        public Asset_WaypointData Asset_Waypoint_Info_Customer = new Asset_WaypointData();
        public Asset_WaypointData Asset_Waypoint_Info_Crew = new Asset_WaypointData();
        public Asset_WaypointData Asset_Waypoint_Info_Start = new Asset_WaypointData();

        public class Asset_Adhoc_DocketEntry
        {
            public string DocketNumber = "";			//Docket Number(1)
            public string DocketIssuer = "";		//Docket Issuer
            public string Truck_No = "";			//Truck Number
            public string Trailer_No = "";			//Trailer Number
            public string Crew = "";					//Crew Number or Name
            public string Forest = "";				//Forest Code or Name
            public string Compartment = "";			//Compartment Code or Name
            public string Setting = "";				//Setting Code or Name
            public string Customer = "";				//Customer Code or Name
            public string DeliveryPnt = "";			//Delivery Point
            public string UPL_Grade = "";			//Uplifted Log Grade Code or Name
            public string DELV_Grade = "";			//Delivered Log Grade Code or Name
            public string Gross = "";				//Gross Weight
            public string Tare = "";					//Tare Weight
            public string Net = "";					//Net Weight
            public string Fell_Date = "";			//Fell Date
            public string Pieces = "";				//Number of Logs
            public string Delv_Date = "";			//Delivery Date
            public string Weighbrdidge = "";			//Weighbridge/ Weighbridge Dkt No.
            public string Loader = "";				//Loader Code or Name
            public string Misc1 = "";				//Miscellaneous
            public string Misc2 = "";				//Miscellaneous
            public string Misc3 = "";				//Miscellaneous
            public string Comment = "";				//Comment
            public bool UsePounds = false;			//Use Pounds
        }
        public Asset_Adhoc_DocketEntry Asset_Adhoc_Docket = new Asset_Adhoc_DocketEntry();
        #endregion

        #region PhoneCall Related Variables
        public string PhoneCall_Report_CLI = "";
        public string PhoneCall_Report_SIMCardNo = "";
        public char PhoneCall_Report_CallDirection = ' ';
        public short PhoneCall_report_CallDuration = 0;
        #endregion

        #region Recipient Message List Variables
        public int MessageList_Recipient_List_ID = 0;
        public ArrayList MessageList_Recipients = new ArrayList();
        public class DriverMessageRecipientList
        {
            public int RecipientID = 0;
            public int DataBaseID = 0;
            public string RecipientName = "";
            public bool AllowDocketDataEntry = false;
        }
        #endregion

        #region Proximity Message
        public ArrayList Proximity_MessageLines = new ArrayList();

        #endregion

        //Message to Driver
        public int Message_ID = 0;			// message id of this message,
        public int Asset_RecipientID = 0;
        public string Message_Text = "";			// message text 
		public string Message_SendTimeStamp = "";
        public char Asset_Message_Status = ' ';

        //PreDefined Message Download List
        public int PreDefined_MessageListID = 0;			// predefined message list ID
        public int PreDefined_MessageAmount = 0;
        //public int		PreDefined_MessageID	 = 0;			// predefined message ID
        public ArrayList oPreDefinedMessageList = new ArrayList();

        ////Phone book id
        public int PhoneBook_ListID = 0;				// phone book list ID
        //public int PhoneBook_Amount = 0;
        public StringCollection PhoneBookNumbers = new StringCollection();
        public StringCollection PhoneBookNames = new StringCollection();

        //******Incoming packets from the MCC / MDT*******************/
        //Login request packet
        public int Login_Password = 0;			// password
        public string PDT_SerialNumber = "";			// Serail number of the PDT
        public int FirwmareVerMajor = 0;
        public int FirmwareVerMinor = 0;
        public int HardwareVerMajor = 0;
        public int HardwareVerMinor = 0;
        public int BatteryPercentage = 0;
        public int LogonQuestion_ListID = 0;
        public int LogoffQuestion_ListID = 0;

        ////Leg Complete
        public int LegComplete_StopID = 0;
        public string LegComplete_UpliftType = "";
        public string LegComplete_DocketNumber = "";
        public string LegComplete_GrossWeight = "";
        public string LegComplete_NetWeight = "";
        public string LegComplete_TareWeight = "";
        public string LegComplete_ScaledVolume = "";
        public DateTime LegComplete_FellDate;
        public string LegComplete_NoOfLogs = "";
        public string LegComplete_ReferenceNo = "";
        public bool LegComplete_UsePounds = false;


        public class Asset_LogOrg_DatabaseID
        {
            public string LogOrg = "";
            public int DataBaseID = 0;
        }
        public List<Asset_LogOrg_DatabaseID> Asset_LogOrg_DatabaseID_t = new List<Asset_LogOrg_DatabaseID>();

        public string Asset_LogOrg = "";
        public int Asset_DataBaseID = 0;

        ////Arrive and Depart messages,
        //public string		JobStop_ArriveDepartType = "";		// Method the Depart message was generated
        //                                                        //	A � Automatic
        //                                                        //	E � Driver Entry

        //reset 5050 request packet defines	
        public string ResetRequest_String = "";		// text must be PLEASE RESET THIS UNIT to reset the unit

        //update time in UTC to the Dats5050
        public int iTimeUTC_Hour = 0;
        public int iTimeUTC_Min = 0;		//

        #endregion


        public AssetForestry_PacketProcessor(string SigFilePath, bool TranslateRJs)
        {
            sSigFilePath = SigFilePath;
            bTranslateRJs = TranslateRJs;
        }


        public byte[] BuildOutgoingPacket() //to MDT
        {
            // [Packet] = [SOP (1 Byte) 0x02][Mobile ID (4 Ascii String)][Length (2 Bytes)][CmdType (1 Byte Ascii)][Data][SBS (1 Byte) 0x07][EOP (1 Byte) 0x03]
            // 
            // If [CmdType] then [Data] =
            // J - New Job 
            // A - Add Job Stop
            // I - Logon Response			
            // D - Delete Job Stop
            // M - Message To Driver
            // K - Predefined Message List
            // R - Remove all jobs
            // r - Reset 5040
            // T - Update time
            MemoryStream oMS = null;
            byte[] bRet = null;
            byte[] bConvert = null;
            string sUnitID = "";

            try
            {
                oMS = new MemoryStream();

                PacketUtilities.WriteToStream(oMS, (byte)0x02);						// [SOP (1 Byte) 0x02]
                sUnitID = Convert.ToString(iMobileID).PadLeft(4, '0').Substring(0, 4);
                PacketUtilities.WriteToStream(oMS, sUnitID);							// [Unit ID (4 Byte Ascii)]
                PacketUtilities.WriteToStream(oMS, (short)0);							// [Length (2 Bytes)]
                PacketUtilities.WriteToStream(oMS, CommandType);						// [Command Type (1 Byte Ascii)]

                switch (CommandType)
                {
                    case "J":
                        #region New Job
                        //	J - New Job 
                        if (_log != null) _log.Info("Building Job Header Packet");
                        // job id
                        PacketUtilities.WriteToStream(oMS, JobID);
                        PacketUtilities.WriteToStream(oMS, (byte)1);

                        //DataBaseID
                        PacketUtilities.WriteToStream(oMS, Asset_DataBaseID);

                        //Asset LogOrg
                        PacketUtilities.WriteToStream(oMS, (string)Asset_LogOrg);
                        PacketUtilities.WriteToStream(oMS, (byte)0x05);

                        //job type
                        PacketUtilities.WriteToStream(oMS, (char)SubCommandType[0]);

                        //Asset job Status
                        PacketUtilities.WriteToStream(oMS, (char)Asset_Job_Status[0]);

                        //Crew Location
                        PacketUtilities.WriteToStream(oMS, Asset_Crew_Location_Comment);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Compartment Description
                        PacketUtilities.WriteToStream(oMS, Asset_Compartment_Description);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Customer Name
                        PacketUtilities.WriteToStream(oMS, Asset_Customer_Name);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Day of week
                        PacketUtilities.WriteToStream(oMS, Asset_DateTime_of_Uplift.ToString("HH").ToString());
                        PacketUtilities.WriteToStream(oMS, Asset_DateTime_of_Uplift.ToString("mm").ToString());
                        string dayoftheweek = Asset_DateTime_of_Uplift.ToString("ddd");
                        dayoftheweek = dayoftheweek.Substring(0, 3);
                        dayoftheweek = dayoftheweek.ToUpper();
                        PacketUtilities.WriteToStream(oMS, dayoftheweek);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Uplift and Delivery Grade Differ Flag
                        byte splitloadindicator = (byte)Convert.ToByte(Asset_Uplift_LogGradeDiffer);
                        PacketUtilities.WriteToStream(oMS, splitloadindicator);

                        //job order
                        uint num2 = Convert.ToUInt32(this.Asset_DateTime_of_Uplift.ToString("yy")) * 0x10;
                        num2 += Convert.ToUInt32(this.Asset_DateTime_of_Uplift.ToString("MM"));
                        num2 *= 0x100;
                        num2 += Convert.ToUInt32(this.Asset_DateTime_of_Uplift.ToString("dd"));
                        num2 *= 0x100;
                        num2 += Convert.ToUInt32(this.Asset_DateTime_of_Uplift.ToString("HH"));
                        num2 *= 0x100;
                        num2 += Convert.ToUInt32(this.Asset_DateTime_of_Uplift.ToString("mm"));
                        PacketUtilities.WriteToStream(oMS, num2);

                        //Crew Uplift Location
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Crew.Latitude);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Crew.Longitude);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Crew.Radius);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Crew.Radius2);
                        PacketUtilities.WriteToStream(oMS, (byte)this.Asset_Waypoint_Info_Crew.DwellTime);

                        //Start Location waypoint information
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Start.Latitude);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Start.Longitude);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Start.Radius);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Start.Radius2);
                        PacketUtilities.WriteToStream(oMS, (byte)this.Asset_Waypoint_Info_Start.DwellTime);

                        //Job Time for delivery added.
                        PacketUtilities.WriteToStream(oMS, (byte)0xAA);
                        //job time for delivery in text format.  for firmware revision 2.20 and above.
                        PacketUtilities.WriteToStream(oMS, Asset_Date_of_Delivery.ToString("HH").ToString());
                        PacketUtilities.WriteToStream(oMS, Asset_Date_of_Delivery.ToString("mm").ToString());
                        dayoftheweek = Asset_Date_of_Delivery.ToString("ddd");
                        dayoftheweek = dayoftheweek.Substring(0, 3);
                        dayoftheweek = dayoftheweek.ToUpper();
                        PacketUtilities.WriteToStream(oMS, dayoftheweek);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        PacketUtilities.WriteToStream(oMS, (byte)0xAB);
                        PacketUtilities.WriteToStream(oMS, (ushort)Asset_CrewToCustomerTime_Minutes);

						//Stage 2D section 1.0
						PacketUtilities.WriteToStream(oMS, (byte)0xAD);
						PacketUtilities.WriteToStream(oMS, (bool)Asset_AutoStatusOnly);
						
						PacketUtilities.WriteToStream(oMS, Asset_Loader_Code);
						PacketUtilities.WriteToStream(oMS, (byte)0x0F);
						
                        #endregion
                        break;
                    case "A":
                        #region Add Job Stop
                        // A - Add Job Leg
                        if (_log != null) _log.Info("Building Add Job Leg Packet");

                        //unique job id.
                        PacketUtilities.WriteToStream(oMS, JobID);									// [JobID (4 bytes)]

                        //Uplift number (stop number)
                        PacketUtilities.WriteToStream(oMS, (byte)Asset_UpliftNo);

                        //DataBaseID
                        PacketUtilities.WriteToStream(oMS, Asset_DataBaseID);

                        //Asset LogOrg
                        PacketUtilities.WriteToStream(oMS, (string)Asset_LogOrg);
                        PacketUtilities.WriteToStream(oMS, (byte)0x05);

                        //Uplift Time
                        //yy
                        PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_DateTime_of_Uplift.ToString("yy")));
                        PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_DateTime_of_Uplift.ToString("MM")));
                        PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_DateTime_of_Uplift.ToString("dd")));
                        PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_DateTime_of_Uplift.ToString("HH")));
                        PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_DateTime_of_Uplift.ToString("mm")));

                        //Delivery Time		
                        try
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_Date_of_Delivery.ToString("yy")));
                            PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_Date_of_Delivery.ToString("MM")));
                            PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_Date_of_Delivery.ToString("dd")));
                        }
                        catch (Exception)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)0);
                            PacketUtilities.WriteToStream(oMS, (byte)0);
                            PacketUtilities.WriteToStream(oMS, (byte)0);
                        }
                        //Crew Code
                        PacketUtilities.WriteToStream(oMS, Asset_Crew_Code);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Loaders code
                        PacketUtilities.WriteToStream(oMS, Asset_Loader_Code);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Loaders driver name
                        PacketUtilities.WriteToStream(oMS, Asset_Loader_Driver_Name);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Uplift Indicator
                        PacketUtilities.WriteToStream(oMS, (byte)Convert.ToByte(Asset_Uplift_Indicator));

                        //Backload Indicator
                        PacketUtilities.WriteToStream(oMS, Asset_BackLoad_Indicator);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Uplift wood grade
                        PacketUtilities.WriteToStream(oMS, Asset_Uplift_WoodType);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Forest 
                        PacketUtilities.WriteToStream(oMS, Asset_Location_Code);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Compartment Code
                        PacketUtilities.WriteToStream(oMS, Asset_Compartment_Code);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Setting Code
                        PacketUtilities.WriteToStream(oMS, Asset_Setting_Code);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        PacketUtilities.WriteToStream(oMS, this.Asset_Delivery_WoodType);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);


                        PacketUtilities.WriteToStream(oMS, this.Asset_Customer_Code);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Customer waypoint information
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Customer.Latitude);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Customer.Longitude);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Customer.Radius);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Customer.Radius2);
                        PacketUtilities.WriteToStream(oMS, this.Asset_Waypoint_Info_Customer.DwellTime);

                        //Docket number
                        PacketUtilities.WriteToStream(oMS, Asset_Docket_Number);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Grosss Weight
                        if (this.Asset_Gross_Weight > 0M)
                        {
                            PacketUtilities.WriteToStream(oMS, this.Asset_Gross_Weight.ToString("N3"));
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Tare Weight
                        if (this.Asset_Tare_Weight > 0M)
                        {
                            PacketUtilities.WriteToStream(oMS, this.Asset_Tare_Weight.ToString("N3"));
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //net weight
                        if (this.Asset_Net_Weight > 0M)
                        {
                            PacketUtilities.WriteToStream(oMS, this.Asset_Net_Weight.ToString("N3"));
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Volume
                        if (this.Asset_Volume > 0M)
                        {
                            PacketUtilities.WriteToStream(oMS, this.Asset_Volume.ToString("N3"));
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Number of logs
                        PacketUtilities.WriteToStream(oMS, Asset_Number_of_Logs);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //reference number
                        PacketUtilities.WriteToStream(oMS, Asset_Reference_No);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //directions
                        PacketUtilities.WriteToStream(oMS, Asset_Directions);
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                        //Uplift ID
                        PacketUtilities.WriteToStream(oMS, Asset_Uplift_ID_Out);

                        //Fell Date
                        try
                        {
                            if (this.Asset_Fell_Date.Year == 2000)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0);
                                PacketUtilities.WriteToStream(oMS, (byte)0);
                                PacketUtilities.WriteToStream(oMS, (byte)0);
                            }
                            else
                            {
                                PacketUtilities.WriteToStream(oMS, Convert.ToByte(this.Asset_Fell_Date.ToString("yy")));
                                PacketUtilities.WriteToStream(oMS, Convert.ToByte(this.Asset_Fell_Date.ToString("MM")));
                                PacketUtilities.WriteToStream(oMS, Convert.ToByte(this.Asset_Fell_Date.ToString("dd")));
                            }
                        }
                        catch (Exception)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)0);
                            PacketUtilities.WriteToStream(oMS, (byte)0);
                            PacketUtilities.WriteToStream(oMS, (byte)0);
                        }

						//Stage 2D
						PacketUtilities.WriteToStream(oMS, (byte)0xAD);
						PacketUtilities.WriteToStream(oMS, (byte)Asset_DocketDigits);

						//Send through the weights as int32*1,000
						PacketUtilities.WriteToStream(oMS, (int)(Asset_Gross_Weight * 1000));
						PacketUtilities.WriteToStream(oMS, (int)(Asset_Net_Weight * 1000));
						PacketUtilities.WriteToStream(oMS, (int)(Asset_Tare_Weight * 1000));
						
                        #endregion
                        break;
                    case "I":
                        #region Logon Response
                        // I - Logon Response - [Result (1 Byte Ascii)][Drivers Name (var Ascii)][Break (1 Byte) 0x0A][DaylightSavings (1 Byte Ascii)]
                        //								[GMT Offset Hours (1 Byte)][GMT Offset Mins (1 Byte)]
                        //								[Network Keep Alive (2 Bytes)][New Mobile ID (2 Bytes)]
                        //								[Firmware Ver Major (1 Byte)][Firmware Ver Minor (1 Byte)]

                        if (_log != null) _log.Info("Building Login Response Packet");
                        PacketUtilities.WriteToStream(oMS, Logon_Result[0]);									// [Result (1 Byte Ascii)]
                        PacketUtilities.WriteToStream(oMS, Logon_DriversName);																// [Drivers Name (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);																			// [Break (1 Byte) 0x0A]
                        PacketUtilities.WriteToStream(oMS, Logon_VehicleType[0]);
                        PacketUtilities.WriteToStream(oMS, Logon_VehicleLocation[0]);
                        if (Logon_DaylightSavings != "Y" && Logon_DaylightSavings != "N")
                            Logon_DaylightSavings = "Y";
                        PacketUtilities.WriteToStream(oMS, Logon_DaylightSavings.PadLeft(1, 'Y').Substring(0, 1));	// [DaylightSavings (1 Byte Ascii)]
                        PacketUtilities.WriteToStream(oMS, (sbyte)Logon_GMT_OffsetHrs);												// [GMT Offset Hours (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)Logon_GMT_OffsetMins);											// [GMT Offset Mins (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)Logon_ServerMajor_FirmwareVersion);					// [Firmware Ver Major (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)Logon_ServerMinor_FirmwareVersion);					// [Firmware Ver Minor (1 Byte)]

                        //added free dial option on the 25th of feb 2009
                        PacketUtilities.WriteToStream(oMS, (byte)0xAA);													// [Extra Data included (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)Logon_DriverFlags);									// [Driver Free Dial Flag (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (byte)Logon_AutoLogoffTimer);								// [Autologoff (1 Byte)]
                        #endregion
                        break;
                    case "D":		// D - Delete Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][History Flag (1 Byte)]
                        #region Delete job or job leg packet
                        if (_log != null) _log.Info("Building Delete job or job leg packet");
                        PacketUtilities.WriteToStream(oMS, JobID);																		// [JobID (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, (byte)Asset_UpliftNo);													// [Stop Number (1 Byte)]                        
                        #endregion
                        break;
                    case "M":		// M - Message To Driver - [Message ID (4 Bytes)][Job ID (4 Bytes)][Message (var Ascii)][Break (1 Byte) 0x0F]
                        #region Message to the driver packet

                        if (_log != null)
                            _log.Info("Building A new message for the driver packet type:" + SubCommandType);

                        PacketUtilities.WriteToStream(oMS, SubCommandType);																			// [Message ID (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, Message_ID);																				// [Message ID (4 Bytes)]
                        PacketUtilities.WriteToStream(oMS, Asset_DataBaseID);																		// [DataBase ID (4 Bytes)]
                        switch (SubCommandType)
                        {
                            case "m":
                                PacketUtilities.WriteToStream(oMS, Message_Text);																		// [Message (var Ascii)]
								PacketUtilities.WriteToStream(oMS, (byte)0x12);																			// [Break (1 Byte) 0x0F]
								PacketUtilities.WriteToStream(oMS, (byte)0xAD);																			// 
								PacketUtilities.WriteToStream(oMS, Message_SendTimeStamp);																// [Message (var Ascii)]
								PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
                                break;
                            case "p":
                            case "c":															//new type added in version 2.20
                                PacketUtilities.WriteToStream(oMS, (byte)Proximity_MessageLines.Count);
                                foreach (string MessageLine in Proximity_MessageLines)
                                {
                                    PacketUtilities.WriteToStream(oMS, MessageLine);
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0F);
                                }
                                break;
                            default:
                                break;
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);																					// [Break (1 Byte) 0x0F]
                        #endregion
                        break;
                    case "K":
                        #region Downloadable Lists
                        PacketUtilities.WriteToStream(oMS, SubCommandType);																					// [Sub Command (1 Byte Ascii)]
                        switch (SubCommandType)
                        {
                            case "P":
                            case "D":
                                #region Phonebook download
                                if (_log != null) _log.Info("Building the phonebook packet for: " + SubCommandType);

                                PacketUtilities.WriteToStream(oMS, PhoneBook_ListID);																		// [phone book ID (4 Bytes)]

                                int phonenumberscount = 0;
                                if (PhoneBookNumbers != null)
                                    if (PhoneBookNumbers.Count > phonenumberscount)
                                        phonenumberscount = PhoneBookNumbers.Count;
                                if (PhoneBookNames != null)
                                    if (PhoneBookNames.Count > phonenumberscount)
                                        phonenumberscount = PhoneBookNames.Count;
                                if (phonenumberscount > 50)
                                    phonenumberscount = 50;

                                PacketUtilities.WriteToStream(oMS, (short)phonenumberscount);

                                for (int i = 0; i < phonenumberscount; i++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);					//start of phone number
                                    PacketUtilities.WriteToStream(oMS, (byte)i);					//location
                                    PacketUtilities.WriteToStream(oMS, PhoneBookNumbers[i]);
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);
                                    PacketUtilities.WriteToStream(oMS, PhoneBookNames[i] != null ? PhoneBookNames[i] : PhoneBookNumbers[i]);
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);
                                }
                                #endregion
                                break;
                            case "M":
                                #region Predefined Message list download
                                if (_log != null) _log.Info("Building the predefined message list packet");
                                PacketUtilities.WriteToStream(oMS, (byte)PreDefined_MessageListID);																		// [Message ID (4 Bytes)]
                                PacketUtilities.WriteToStream(oMS, (byte)PreDefined_MessageAmount);
                                for (int i = 0; i < oPreDefinedMessageList.Count; i++)
                                {
                                    PacketUtilities.WriteToStream(oMS, ((JobLegProduct)oPreDefinedMessageList[i]).ProductID);
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);																	// [Message Seperator (1 Byte) 0x0A]
                                    PacketUtilities.WriteToStream(oMS, (byte)i);																			// [Message List Number (1 Byte)]
                                    string zTempString = (string)oPreDefinedMessageList[i];
                                    PacketUtilities.WriteToStream(oMS, zTempString);																	// [Message (var Ascii)]
                                }
                                PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
                                #endregion
                                break;
                            case "r":
                                #region Message Recipient List download
                                //[CmdType][SubCommand][ListID][NumOfMessages]{[MSS][RecipientID][DatabaseID][AllowDocketData][RecipientName][RecipientEndSep]}[ListEndDelimiter]

                                //[ListID]
                                PacketUtilities.WriteToStream(oMS, MessageList_Recipient_List_ID);

                                //[NumOfMessages]
                                PacketUtilities.WriteToStream(oMS, (short)MessageList_Recipients.Count);

                                byte location = 0;
                                foreach (DriverMessageRecipientList RecipientItem in MessageList_Recipients)
                                {
                                    //[MSS]
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);

                                    //[location]
                                    PacketUtilities.WriteToStream(oMS, location++);

                                    //[RecipientID]
                                    PacketUtilities.WriteToStream(oMS, RecipientItem.RecipientID);

                                    //[DatabaseID]
                                    PacketUtilities.WriteToStream(oMS, RecipientItem.DataBaseID);

                                    //[AllowDocketData]
                                    PacketUtilities.WriteToStream(oMS, RecipientItem.AllowDocketDataEntry);

                                    //[RecipientName]
                                    PacketUtilities.WriteToStream(oMS, RecipientItem.RecipientName);

                                    //[RecipientEndSep]
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);

                                }

                                //[ListEndDelimiter]
                                PacketUtilities.WriteToStream(oMS, (byte)0x0F);

                                #endregion
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                    case "R":
                        #region Delete all jobs and messages on the unit

                        //Items in array added into version 2.20
                        PacketUtilities.WriteToStream(oMS, (byte)Asset_LogOrg_DatabaseID_t.Count);
                        foreach (Asset_LogOrg_DatabaseID items in Asset_LogOrg_DatabaseID_t)
                        {
                            //DataBaseID
                            PacketUtilities.WriteToStream(oMS, items.DataBaseID);
                            //Asset LogOrg
                            PacketUtilities.WriteToStream(oMS, (string)items.LogOrg);
                            //Seperator
                            PacketUtilities.WriteToStream(oMS, (byte)0x05);
                        }
                        //terminator
                        PacketUtilities.WriteToStream(oMS, (byte)0x06);

                        if (_log != null)
                            _log.Info("Building Delete all jobs and messages packet with " + Asset_LogOrg_DatabaseID_t.Count.ToString() + " items");

                        #endregion
                        break;
                    case "r":
                        #region Reset the 5040 request
                        if (_log != null) _log.Info("Building reset request pacekt");
                        PacketUtilities.WriteToStream(oMS, ResetRequest_String);												// [Reset string  (var Ascii)]
                        PacketUtilities.WriteToStream(oMS, (byte)0x0F);												// [RES 0x0F]
                        #endregion
                        break;
                    case "T":
                        #region Update Current Time on the 5050
                        if (_log != null) _log.Info("Building UTC Time update packet");
                        PacketUtilities.WriteToStream(oMS, (byte)iTimeUTC_Hour);
                        PacketUtilities.WriteToStream(oMS, (byte)iTimeUTC_Min);
                        #endregion
                        break;
                    case "d":
                        #region Download Packets

                        PacketUtilities.WriteToStream(oMS, SubCommandType);															// [SubCmd (1 Byte Ascii)]
                        if (SubCommandType == "d")
                        {
                            //[CmdType]			= 'd'
                            //[MobileID]		
                            //[SubCmd]			= 'd'
                            //[CurrentVersionMSB]
                            //[CurrentVersionLSB]
                            //[TotalNoOfParcels]
                            //[NoOfParcels]
                            //{
                            //	[ParcelNo]
                            //	[ProgramFragment]
                            //	[DownloadcheckSum]
                            //	[DSB]
                            //}
                            //[DESB]
                            #region Download Parcel
                            if (_log != null) _log.Info("Building Download Header Packet");

                            PacketUtilities.WriteToStream(oMS, this.SoftwareVersionMajor);							// [Software Ver Major (1 Byte)
                            PacketUtilities.WriteToStream(oMS, this.SoftwareVersionMinor);							// [Software Ver Minor (1 Byte)						
                            PacketUtilities.WriteToStream(oMS, (ushort)DownloadMotParcel.Count);					// [NumberOfDataSegments (2 Bytes)
                            ushort iMaxDownloadMotParcelNumber = (ushort)(DownloadMotParcel.Count - DownloadMotParcelNumber);
                            if (iMaxDownloadMotParcelNumber > 2)
                                iMaxDownloadMotParcelNumber = 2;
                            PacketUtilities.WriteToStream(oMS, (byte)iMaxDownloadMotParcelNumber);						// [NoOfParcels] (1 Bytes)]						
                            for (ushort i = DownloadMotParcelNumber; i < iMaxDownloadMotParcelNumber + DownloadMotParcelNumber; i++)
                            {
                                cParcel oParcel = (cParcel)DownloadMotParcel[i];
                                PacketUtilities.WriteToStream(oMS, (ushort)i);					// [PacketNumber] (2 Byte)]																						
                                PacketUtilities.WriteToStream(oMS, oParcel.Data);				// [ProgramFragment] = 250 bytes
                                PacketUtilities.WriteToStream(oMS, (ushort)oParcel.CheckSum);			// [DownloadcheckSum] = 2 bytes
                                PacketUtilities.WriteToStream(oMS, (byte)0xBB);				// [DSB] = 1 byte
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0xCC);					// [DESB]
                            #endregion
                        }
                        else if (SubCommandType == "v")
                        {
                            //[CmdType]
                            //[MobileID]
                            //[SubCmd]
                            //[DownloadVersion]
                            //[ProgramTotalCheckSum]
                            //[PacketCheckSum]
                            //[PacketCountTotal]
                            //[ProgramByteTotal]
                            //[Checksum]
                            #region Download Verify
                            if (_log != null) _log.Info("Building Download Verify Packet");

                            PacketUtilities.WriteToStream(oMS, (byte)this.DownloadVersion);
                            PacketUtilities.WriteToStream(oMS, (int)ProgramTotalCheckSum);
                            PacketUtilities.WriteToStream(oMS, (int)PacketTotalCheckSum);
                            PacketUtilities.WriteToStream(oMS, (ushort)ProgramPacketCountTotal);
                            PacketUtilities.WriteToStream(oMS, (int)ProgramByteTotal);
                            PacketUtilities.WriteToStream(oMS, (byte)ProgramDataBytesCheckSum);
                            #endregion
                        }
                        #endregion
                        break;
                    default:
                        break;
                }

                PacketUtilities.WriteToStream(oMS, (byte)0x07); // [SBS (1 Byte) 0x07]
                PacketUtilities.WriteToStream(oMS, (byte)0x03); // [EOP (1 Byte) 0x03]
                bRet = oMS.ToArray();
                #region Update the length bytes
                bConvert = new byte[4];
                bConvert = BitConverter.GetBytes(bRet.Length);
                bRet[5] = bConvert[0];
                bRet[6] = bConvert[1];
                #endregion
                string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
                if (_log != null) _log.Info(sData);
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                    _log.Error("AssetForestry_PacketProcesssor.BuildOutgoingPacket()", ex);
            }
            return bRet;
        }

        public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
        {
            // Special Field Defs
            // [GPS] = [LSC (1 Byte)] [Lat (4 Bytes)] [Long (4 Bytes)] [GPS Time  (6 Bytes)] [Speed (1 Byte)] [Heading (2 Bytes)] [DeviceTimer (2 Bytes)][Distance (4 Bytes)] 
            // [Header] = [MobileID (4 Bytes)][VehicleID (2 Bytes)][FleetID (1 Bytes)][GPS (24 Bytes)][DriverID (4 Byte Ascii)]
            // [Packet] = [CmdType (1 Byte)][Data][SBS (1 Byte)][Spare (1 Byte)]

            // 
            // If [CmdType] then [Data] =
            //	N - ACK / NACK - No Payload
            //	I - Login Packet - [Login Password (4 Byte Ascii)][MessageListID (1 Byte)]
            //	O - Logout Packet - No Payload
            //	A - Job Accept Packet - [Job ID (4 Bytes)]
            //	S - Job Stop Complete Packet  - 
            //				[Sub Section] = [Sub Cmd Type (1 Byte)][Sub Cmd Data]
            //				If [Sub Cmd Type] then [Sub Cmd Data] =
            //					0x0A - Barcodes - ([Barcode (Var Ascii)][Break (1 Byte 0x0C)][Quantity (1 Byte)])[Break (1 Byte 0x0B) Or (1 Byte 0x0A)]
            //					0x0C - Job Leg Complete - [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]
            //					0x0B - Signature Data - [Length (2 bytes)][Signature Data ([Length] Bytes)]
            //		[Job Stop Complete Packet]  = [Job ID (4 Bytes)][JobStop_Number (1 Bytes)]([Sub Section X])[LegComplete_CompleteStatus (1 Byte)]
            //	C - Job Complete Packet - [Job ID (4 Bytes)]
            //	M - Message Packet  - [SubCmd][Sub Cmd Data]
            //		If [SubCmd] then [Sub Cmd Data] = 
            //			F - Incoming Freeform Message Packet - [Sub Cmd Data] = [Message (Var Ascii)][Break (1 Byte 0x06)]
            //			P - Incoming Predefined Message Packet - [Sub Cmd Data] = [MessageID (1 Byte)]
            //			R - Message Read by Driver Packet - [Sub Cmd Data] = [MessageID (4 Byte)]
            //	B - Arrive Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
            //	D - Depart Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
            //	d - Request Data Segment - [Send Segment (4 Bytes)]
            #region Local Variables
            byte bValue = (byte)0x00;
            sbyte sbValue = (sbyte)0x00;
            short isValue = 0;
            ushort usValue = 0;
            int iPosition = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            string sTemp = "";
            byte[] bTempArray = new byte[1];
            #endregion

            try
            {
                #region Get the command type
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                this.CommandType = Convert.ToString((char)bValue);
                #endregion
                #region Get the Fleet, Vehicle and Mobile IDs
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref usValue);
                iMobileID = (int)usValue;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
                VehicleID = (int)isValue;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                FleetID = (int)bValue;
                #endregion
                #region [GPS]
                this.GPSPosition = new cGPSData();

                // LSC Byte
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                switch (bValue)
                {
                    case 0x12:			//standard
                        break;
                    case 0x13:			//has offset included
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref sbValue);
                        this.GPSPosition.TimeZone_OffSet_Hours = sbValue;
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.GPSPosition.TimeZone_OffSet_Minutes = bValue;
                        break;
                    default:
                        break;
                }

                // Lat
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref bTempArray);
                this.GPSPosition.dLatitude = LatLonConversion.ConvLatLon(bTempArray);
                // Long
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref bTempArray);
                this.GPSPosition.dLongitude = LatLonConversion.ConvLatLon(bTempArray);
                #region Get GPS Time string
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iDay = (int)bValue;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iMonth = (int)bValue;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iYear = ((int)bValue) + 2000;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iHour = (int)bValue;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iMinute = (int)bValue;
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iSecond = (int)bValue;
                try
                {
                    this.GPSPosition.dtGPSTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                }
                catch (System.Exception)
                {
                }
                #endregion
                // Speed
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                this.GPSPosition.iSpeed = (int)bValue;
                // Heading
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
                this.GPSPosition.iHeading = (int)isValue;
                // Device Time is Seconds
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
                this.DeviceTimeSeconds = (int)isValue;
                //distance odo
                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Distance);
                #endregion
                #region ready the subcommand if availabe
                switch (this.CommandType)
                {
                    case "M":
                    case "R":
                    case "S":
                    case "K":
                    case "d":
                    case "J":
                    case "A":
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.SubCommandType = Convert.ToString((char)bValue);
                        break;
                    default:
                        break;
                }
                #endregion
                #region Get the Driver ID
                try
                {
                    int temposition = iPosition;
                    PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0, ref sTemp);
                    if (sTemp.Length > 6)
                    {
                        sTemp = sTemp.Substring(0, 6);
                    }
                    iPosition = temposition + 6;
                    this.DriverID = Convert.ToInt32(sTemp);
                }
                catch (System.Exception)
                {
                    this.DriverID = 0;
                }

                byte tempbyte = 0;

                #endregion
                switch (CommandType)
                {
                    case "N":	// N - ACK / NACK - No Payload
                        #region	ACK / NACK
                        if (_log != null) _log.Info("Decoding ACK/NAK Packet");
                        try
                        {
                            //Result
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue); // [Result (1 Bytes)]
                            this.Asset_Uplift_Ack_Result = (int)bValue;

                            if (this.Asset_Uplift_Ack_Result == 0)
                                iPosition += 2;
                            else
                                iPosition += 4;

                            //Job ID
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID); // [Job ID (4 Bytes)]

                            //Leg number
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue); // [JobStop_Number (1 Bytes)]
                            this.Asset_UpliftNo = (int)bValue;

                            //DataBase ID
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_DataBaseID);

                            //log org	   
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)5, ref this.Asset_LogOrg);

                            try
                            {
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_Uplift_ID_In);
                            }
                            catch (Exception)
                            {
                                //old firmware support
                            }
                        }
                        catch (Exception ex)
                        {
                            if (_log != null) _log.Error("Error decoding ack nak packet", ex);
                        }
                        #endregion
                        break;
                    case "R":
                        #region Request Jobs Keep Alive
                        if (_log != null) _log.Info("Decoding Request for Jobs Packet (KeepAlive)");
                        //PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);
                        //PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        //JobStop_Number = (int)bValue;
                        #endregion
                        break;
                    case "I":	// I - Login Packet - [Login Password (4 Byte Ascii)][MessageListID (1 Byte)]
                        #region Login Request Packet
                        if (_log != null) _log.Info("Decoding Login Packet");

                        //password
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref sTemp);
                        this.Login_Password = Convert.ToInt32(sTemp);

                        //predefined currently stored message list id
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.PreDefined_MessageListID = (int)bValue;

                        //Logon question list id stored on the PDT
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.LogonQuestion_ListID = (int)bValue;

                        //logoff question list id stored on the PDT
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.LogoffQuestion_ListID = (int)bValue;

                        //phone book id stored on the PDT	
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.PhoneBook_ListID = (int)bValue;

                        //PDT serial number
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, 9, ref this.PDT_SerialNumber);

                        //firwmare version major,
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.FirwmareVerMajor = (int)bValue;

                        //firmware version minor,
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.FirmwareVerMinor = (int)bValue;

                        //hardware version major,
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.HardwareVerMajor = (int)bValue;

                        //hardware version minor,
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.HardwareVerMinor = (int)bValue;

                        //Battery percentage
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.BatteryPercentage = (int)bValue;

                        #endregion
                        break;
                    case "L":
                        #region Driver login Report

                        if (_log != null) _log.Info("Decoding Driver login report");
                        try
                        {
                            byte NoOfQuestion = 0;

                            //Fit For Duty
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempbyte);
                            Logon_Report_FitForDuty = Convert.ToChar(tempbyte).ToString();

                            //Question List Used
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempbyte);
                            Logon_Report_QuestionListID_Used = Convert.ToInt32(tempbyte);

                            //Number of Questions/Answers in this list
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref NoOfQuestion);

                            //Get the answers
                            Logon_Report_QuestionAnswers.Clear();

                            for (int i = 0; i < NoOfQuestion; i++)
                            {
                                DriverLoginAnswers Answer = new DriverLoginAnswers();

                                //Answer ID
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempbyte);
                                Answer.AnswerID = tempbyte;

                                //Answer Type
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempbyte);
                                Answer.AnswerType = Convert.ToChar(tempbyte).ToString();

                                //Answer
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x07, ref Answer.Answer);

                                Logon_Report_QuestionAnswers.Add(Answer);
                            }

                            //Answers end delimter
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);

                            #region Signature Decode
                            //Signature
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                            if (bValue == 0x0B)
                            {
                                //encode the vector image into a gif

                                //signature legnth
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);																	// [Length (2 Bytes)]
                                //read vector signature into byte array
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)isValue, ref Logon_Report_Signature);				// [Signature Data ([Length] Bytes)]
                                //read end byte of 0x03
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);																	// [LegComplete_CompleteStatus (1 Byte)]

                                //decode signature into a gif format
                                if (Logon_Report_Signature.Length > 0)
                                {
                                    string path = ConfigurationManager.AppSettings["PicturePath"];
                                    if (path.EndsWith("/"))
                                        path += "/";

                                    try
                                    {

                                        Logon_Report_SignatureAddress = string.Format("{0}-{1}.gif", DateTime.Now.ToString("yyMMddHHmmss"), Convert.ToString(iMobileID));

                                        string DefaultSignature = ConfigurationManager.AppSettings["UseBackgroundGIFforSignatures"];
                                        if (!DefaultSignature.Contains(":"))
                                        {
                                            string exePath = System.Reflection.Assembly.GetExecutingAssembly().Location;
                                            exePath = System.IO.Path.GetDirectoryName(path);
                                            DefaultSignature = exePath + "\\" + DefaultSignature;
                                        }

                                        cVectorSignature oSig = new cVectorSignature(false, 80, 300, 200, 600, path, DefaultSignature, 0, 0, 0.5, true,
                                                                                     100000, true, System.Drawing.Color.White, true, 10);
                                        oSig.FileName = Logon_Report_SignatureAddress;
                                        Logon_Report_Signature = oSig.TranslateSignature(Logon_Report_Signature, Logon_Report_SignatureAddress);
                                        //convert the signature into gif file
                                    }
                                    catch (Exception ex)
                                    {
                                        if (_log != null) _log.Info("Decode of signature:" + ex);
                                    }
                                    Logon_Report_SignatureAddress = path + Logon_Report_SignatureAddress;
                                }
                            }
                            else if (bValue == 0x0C)
                            {
                                //copy the already compliled gif image to the byte array
                                //read the length of the gif image
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);																	// [Length (2 Bytes)]
                                //copy the gif image into the signature byte array
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)isValue, ref Logon_Report_Signature);				// [Signature Data ([Length] Bytes)]
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);																	// [LegComplete_CompleteStatus (1 Byte)]
                            }

                            #endregion
                        }
                        catch (Exception ex)
                        {
                            if (_log != null)
                                _log.Error("Error: with the decode of the driver login report", ex);
                        }

                        #endregion
                        break;
                    case "O":	// O - Logout Packet - No Payload
                        #region Logout Packet
                        if (_log != null) _log.Info("Decoding Logout Packet");
                        #endregion
                        break;
                    case "A":		// Adhoc Docket Entry Packet
                        #region Adhoc Docket Entry Packet
                        if (_log != null) _log.Info("Decoding Adhoc Docket Entry Packet");
                        //[CmdType][CommonData][SubCmdType]DriverID][ DataBase ID][ Recipient ID][Type][ Docket No.][STB][Docket Issuer][STB][TruckNo.][STB][TrailerNo.][STB][Crew][STB][Forest][STB][Compartment][STB][Setting][STB][Customer][STB][UplGrade][STB][DelvGrade][STB][Gross][STB][Tare][STB][Net][STB][FellDate][STB][Pieces][STB][DeliveryDate][STB][Weighbridge][STB][Loader][STB][Misc1][STB][Misc2][STB][Misc3][STB][Comments][STB]

                        //[ DataBase ID]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_DataBaseID);

                        //[ Recipient ID]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_RecipientID);

                        //[ Docket No.]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.DocketNumber);

                        //[ Docket Issuer.]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.DocketIssuer);

                        //[ Truck No.]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Truck_No);

                        //[ Trailer No.]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Trailer_No);

                        //[ Crew]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Crew);

                        //[ Forest]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Forest);

                        //[ Compartment]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Compartment);

                        //[ Compartment]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Setting);

                        //[ Customer]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Customer);

                        // [ UplGrade ]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.UPL_Grade);

                        // [ DelvGrade ]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.DELV_Grade);

                        // [ Gross ]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Gross);

                        // [ Tare ]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Tare);

                        // [ Net ]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Net);

                        // [FellDate]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Fell_Date);

                        // [Pieces]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Pieces);

                        // [DeliveryDate]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Delv_Date);

                        // [Weighbridge]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Weighbrdidge);

                        // [Loader]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Loader);

                        // [Misc1]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Misc1);

                        // [Misc2]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Misc2);

                        // [Misc3]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Misc3);

                        // [Comments]
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.Comment);

                        try
                        {
                            //[ Delivery Pnt]
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Asset_Adhoc_Docket.DeliveryPnt);
                        }
                        catch (Exception)
                        {
                        }

                        //Stage 2C]
                        //[UsePoundsSep][UsePoundsSep][UsePounds]
                        try
                        {
                            byte tempByte = 0;
                            //[UsePoundsSep]
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempByte);
                            if (tempByte == 0xAA)
                            {
                                //[UsePoundsSep]
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempByte);
                                if (tempByte == 0xAA)
                                {
                                    //[UsePounds]
                                    PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_Adhoc_Docket.UsePounds);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //did not find the use pounds option
                        }
                        #endregion
                        break;
                    case "J":
                        #region Job State Change Update
                        if (_log != null) _log.Info("Decoding Job State Change Packet");


                        this.JobState = SubCommandType[0].ToString();
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempbyte);
                        JobState_GeneratedType = Convert.ToChar(tempbyte).ToString();

                        //DataBase ID
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_DataBaseID);

                        // log org
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)5, ref this.Asset_LogOrg);

						//loader code
						//stage 2D.1 14.07.13
						PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref this.Asset_Loader_Code);

                        #endregion
                        break;
                    case "S":
                        #region Job Stop Complete Packet
                        // S - Job Stop Complete Packet  - 
                        //			[Sub Section] = [Sub Cmd Type (1 Byte)][Sub Cmd Data]
                        //			If [Sub Cmd Type] = 
                        if (_log != null) _log.Info("Decoding Job Uplift Complete Packet");
                        //jop id
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);			// [Job ID (4 Bytes)]

                        //Uplift complete type
                        this.LegComplete_UpliftType = this.SubCommandType;

                        //Leg number
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_Number (1 Bytes)]
                        this.LegComplete_StopID = (int)bValue;

                        //Docket Number
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_DocketNumber);

                        //Gross Weight
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_GrossWeight);

                        //Tare Weight
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_TareWeight);

                        //Net Weight
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_NetWeight);

                        //Scaled Volume Weight
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_ScaledVolume);

                        //Fell Date,
                        try
                        {
                            byte TempDay = 0, TempMonth = 0, TempYear = 0;
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref TempDay);
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref TempMonth);
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref TempYear);
                            int Year = TempYear + 2000;
                            LegComplete_FellDate = new DateTime(Year, (int)TempMonth, (int)TempDay);
                        }
                        catch (System.Exception)
                        {
                            if (_log != null)
                                _log.Warn("Warning: Could not convert Fell Date for Job Uplift Complete");
                        }
                        //number of logs
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_NoOfLogs);

                        //Reference Number
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_ReferenceNo);

                        //Uplift ID
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_Uplift_ID_In);

                        //Database ID
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_DataBaseID);

                        // Logorg  
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)5, ref this.Asset_LogOrg);

                        //Stage 2C]
                        //[UsePoundsSep][UsePoundsSep][UsePounds]
                        try
                        {
                            byte tempByte = 0;
                            //[UsePoundsSep]
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempByte);
                            if (tempByte == 0xAA)
                            {
                                //[UsePoundsSep]
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref tempByte);
                                if (tempByte == 0xAA)
                                {
                                    //[UsePounds]
                                    PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref LegComplete_UsePounds);
                                }
                            }
                        }
                        catch (Exception)
                        {
                            //did not find the use pounds option
                        }

                        #endregion
                        break;
                    case "C":	// C - Job Complete Packet - [Job ID (4 Bytes)]
                        #region Job Complete Packet
                        if (_log != null) _log.Info("Decoding Job Complete Packet");

                        //JobID
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);

                        //DataBase ID
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_DataBaseID);

                        //LogOrg
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)5, ref this.Asset_LogOrg);
                        #endregion
                        break;
                    case "M":
                        #region Incoming driver message from driver
                        // M - Message Packet  - [SubCmd][Sub Cmd Data]
                        //		If [SubCmd] = 
                        //			F - Incoming Freeform Message Packet - [Sub Cmd Data] = [Message (Var Ascii)][Break (1 Byte 0x06)]
                        //			P - Incoming Predefined Message Packet - [Sub Cmd Data] = [MessageID (1 Byte 0x06)]
                        //			R - Message Read by Driver Packet - [Sub Cmd Data] = [MessageID (4 Byte)]
                        if (_log != null) _log.Info("Decoding Message from unit Packet");
                        switch (SubCommandType)
                        {
                            case "F":			//free formed messages
                                //related job id
                                //PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_RelatedJobID);
                                //free hand message text
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_ID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_DataBaseID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_RecipientID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x06, ref Message_Text);	// [Message (Var Ascii)][Break (1 Byte 0x06)]
                                break;
                            case "P":			//predefined message
                                //related job id
                                //	PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_RelatedJobID);
                                // predefined mesage List ID
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_ID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_DataBaseID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Asset_DataBaseID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);										//  [MessageID (1 Byte)]
                                this.PreDefined_MessageListID = (int)bValue;
                                // predefined mesage ID
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);										//  [MessageID (1 Byte)]
                                //	this.PreDefined_MessageID = (int) bValue;
                                break;
                            case "R":			//message read by driver
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Message_ID);						// [MessageID (4 Byte)]
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_DataBaseID);
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Asset_Message_Status);
                                break;
                        }
                        #endregion
                        break;
                    case "B":	// B - Arrive Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
                    case "D":	// D - Depart Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
                        #region Arrive or Depart Job Leg Packet
                        if (_log != null) _log.Info("Decoding Arrive or Depart Job Leg Packet");
                        //job id
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);		// [Job ID (4 Bytes)]
                        //stop number
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_Number (1 Byte)]
                        //			this.JobStop_Number = (int)bValue;
                        //type - auto or entered
                        PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_ArriveDepartType (1 Byte)]
                        //			this.JobStop_ArriveDepartType = Convert.ToString((char) bValue);


                        #endregion
                        break;
                    case "d":	// d - Request Data Segment - [Send Segment (4 Bytes)]
                        #region Request a Download Packet
                        if (_log != null) _log.Info("Decoding Download Request Packet");
                        switch (this.SubCommandType)
                        {
                            case "r":			//request new download parcel
                                this.SubCommandType = "d";
                                PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.RequestedParcelNo);
                                break;
                            case "v":			//verify							
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                    case "K":
                        #region Phone call report

                        if (_log != null) _log.Info("Decoding Phone call report packet");
                        try
                        {
                            //call line id, phone number in use for this phone call
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0A, ref this.PhoneCall_Report_CLI);

                            //Direction of phone call
                            PhoneCall_Report_CallDirection = Convert.ToChar(this.SubCommandType[0]);

                            //Call Duration
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref PhoneCall_report_CallDuration);

                            //Sim card number used 
                            PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0A, ref this.PhoneCall_Report_SIMCardNo);

                        }
                        catch (Exception ex)
                        {
                            if (_log != null) _log.Error("Error: Decoding Phone call report packet", ex);
                        }

                        #endregion
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                {
                    if (bMsg == null)
                        _log.Error(sClassName + "TranslateMsgToHost(byte[] bMsg = null, string sUnitID = '" + sUnitID + "')", ex);
                    else
                        _log.Error(sClassName + "TranslateMsgToHost(byte[] bMsg, string sUnitID = '" + sUnitID + "')", ex);
                }
            }
            bRawData = bMsg;
            return "";
        }

        public string GetPacketTypeName()
        {
            string str = "";
            switch (this.CommandType)
            {
                case "N":
                    return "ACK / NACK";

                case "I":
                    return "Login Packet";

                case "O":
                    return "Logout Packet";

                case "A":
                    return "Job Accept Packet";

                case "S":
                    return "Job Stop Complete Packet";

                case "C":
                    return "Job Complete Packet";

                case "M":
                    if (!(this.SubCommandType == "F"))
                    {
                        if (this.SubCommandType == "P")
                        {
                            return "Incoming Predefined Message Packet";
                        }
                        if (this.SubCommandType == "R")
                        {
                            str = "Message Read by Driver Packet";
                        }
                        return str;
                    }
                    return "Incoming Freeform Message Packet";

                case "B":
                case "D":
                    return "Depart Job Leg Packet";

                case "K":
                    return "Pre-Defined Message List Packet";

                case "d":
                    return "Request a Download Packet";

                case "R":
                    return "Request jobs packet";

                case "r":
                    return "Reset Request Packet";

                case "T":
                    return "Time UTC update packet for the 5040";
            }
            return "MTData Terminal Interfcae Packet";
        }

        public class JobLegProduct
        {
            #region  Private vars
            private string _productID;
            private string _description;
            private byte _quantity;

            #endregion

            #region Properties
            public string ProductID
            {
                get
                {
                    return _productID;
                }
                set
                {
                    _productID = value;

                }
            }
            public string Description
            {
                get
                {
                    return _description;
                }
                set
                {
                    _description = value;
                }
            }

            public byte Quantity
            {
                get
                {
                    return _quantity;
                }
                set
                {
                    _quantity = value;
                }
            }




            #endregion
        }


        public class PhoneBookEntry
        {
            #region  Private vars
            private string _name;
            private string _number;

            #endregion

            #region Properties

            public string Name
            {
                get
                {
                    return _name;
                }
                set
                {
                    _name = value;
                }
            }

            public string Number
            {
                get
                {
                    return _number;
                }
                set
                {
                    _number = value;
                }
            }
            #endregion
        }


    }
}

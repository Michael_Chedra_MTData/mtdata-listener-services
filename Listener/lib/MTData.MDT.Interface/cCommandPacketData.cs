using System;
using System.Configuration;
using log4net;
using MTData.Common.Utilities;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cCommandPacketData.
    /// </summary>
    public class cCommandPacketData
    {

        #region Incomming Packet Defs
        // Wake Up -			[CmdType] [Card No] [CR]
        // Attach Delay -		[CmdType] [Card No] [Delay] [CR]
        // Coded Message -		[CmdType] [Card No] [Vehicle] [Message No] [Packet No] [CR]
        // Job Dispatch -		[CmdType] [Card No] [Vehicle] [Packet No] [Orig Plant] [Job Lat] [Job Long] [Customer Name] [Load Weight] [Item Code] 
        //						[Order Number] [Load Number] [Job Address] [Job Instructions] [Time Required as Job] [Time Required at Orig Plant] 
        //						[CR]
        #endregion

        #region Outgoing Packet Defs
        // Version Ack Resp -	[CmdType] [Card No] [Version] [CR]
        // Ack Responce		-	[CmdType] [Card No] [CR]
        // Nack Responce	-	[CmdType] [Card No] [CR]
        // Status Message	-	[CmdType] [Card No] [Vehicle] [Status No] [Packet No] [Time Off] [CR]
        // Left On Message  -	[CmdType] [Card No] [Vehicle] [Quantity Left] [Packet No] [Time Off] [CR]
        // GPS Message		-	[CmdType] [Card No] [Vehicle] [UTC] [Lat] [Lat Hem] [Long] [Long Hem] [Velocity] [CR]
        #endregion

        #region Private Vars
        private ILog _log = LogManager.GetLogger(typeof(cCommandPacketData));
        private string sPacketName = "";
        private string sCommandProtocol = "";
        #endregion

        #region Public Vars
        public byte bSeperator = (byte)0x00;
        public byte bCommandType = (byte)0x00;
        public byte bCardNumber = (byte)0x30;
        public byte bCR = (byte)0x0D;
        public string sSumCmd = "";
        public string sVehicle = "0000";
        public string sDelay = "00";
        public string sMessageNo = "00";
        public string sPacketNo = "0";
        public string sReturnPlant = "000";
        public string sMessage = "";
        //		public string sOrigPlant = "000";
        //		public string sJobLat = "0000.0000S";
        //		public string sJobLong = "00000.0000E";
        //		public int iJobLat = 0;
        //		public int iJobLong = 0;
        //		public string sCustomerName = "";
        //		public string sLoadWeight = "000.000";
        public string sItemCode = "000000000000";
        public string sOrderNumber = "000000000000";
        public string sTicketCode = "00000000";
        //		public string sLoadNumber = "00";
        //		public string sJobAddress = "";
        //		public string sJobInstruction = "";
        //		public string sTimeRequiredAtJob = "00";
        //		public string sTimeRequiredAtPlant = "00";
        public DateTime dtOrderDate = new DateTime(2000, 1, 1, 1, 1, 1);
        public DateTime dtTimeRequiredAtJob = new DateTime(2000, 1, 1, 1, 1, 1);
        public DateTime dtTimeRequiredAtPlant = new DateTime(2000, 1, 1, 1, 1, 1);

        public string sFreeTextMessage = "";

        public string sVersion = ""; // e.g. "VB";
        public string sStatusNo = "00";
        public string sTimeOff = "000";
        public string sQuantityLeft = "000";
        public string sUTCTime = "000000";
        public string sVehicleLat = "0000.0000S";
        public string sVehicleLong = "00000.0000E";
        public string sVehicleVelocity = "000.0";
        #endregion

        public cCommandPacketData()
        {
            try
            {
                sCommandProtocol = ConfigurationManager.AppSettings["InterfaceProtocol"];
            }
            catch (System.Exception)
            {
                sCommandProtocol = "DiNet";
            }

        }

        #region Build Packet Data
        public byte[] BuildOutgoingPacket()
        {
            sPacketName = "Invalid Packet";
            byte[] bRet = new byte[1];
            if (sCommandProtocol == "DiNet")
            {
                bRet[0] = (byte)0x31;
            }
            else
            {
                bRet[0] = bCommandType;
            }
            switch (bCommandType)
            {
                case (byte)0x06:
                    #region Ack Packet
                    // [CmdType] [Card No] [CR]
                    // or
                    //[CmdType] [Card No] [Version] [CR]
                    bRet = PacketUtilities.AppendToPacket(bRet, bCardNumber);
                    if (sVersion.Length > 0)
                    {
                        bRet = PacketUtilities.AppendToPacket(bRet, sVersion);
                        sPacketName = "Ack Version Response";
                    }
                    else
                    {
                        sPacketName = "Ack Response";
                    }
                    bRet = PacketUtilities.AppendToPacket(bRet, bCR);
                    #endregion
                    break;
                case (byte)0x15:
                    #region Nack Packet
                    // [CmdType] [Card No] [CR]

                    bRet = PacketUtilities.AppendToPacket(bRet, bCardNumber);
                    bRet = PacketUtilities.AppendToPacket(bRet, bCR);
                    sPacketName = "Nack Response";
                    #endregion
                    break;
                case (byte)0x53: // S
                    #region Status Message
                    if (sCommandProtocol == "DiNet")
                    {
                        // [CmdType]-[Vehicle]-[Status No][CR] (1-3356-8<cr>)
                        bRet = PacketUtilities.AppendToPacket(bRet, "-");
                        bRet = PacketUtilities.AppendToPacket(bRet, sVehicle);
                        bRet = PacketUtilities.AppendToPacket(bRet, "-");
                        bRet = PacketUtilities.AppendToPacket(bRet, sStatusNo);
                        bRet = PacketUtilities.AppendToPacket(bRet, bCR);
                        sPacketName = "Status Message";
                    }
                    else
                    {
                        #region Command Signal Interace
                        // [CmdType] [Card No] [Vehicle] [Status No] [Packet No] [Time Off] [CR]  (S03356080000<cr>)
                        bRet = PacketUtilities.AppendToPacket(bRet, bCardNumber);
                        bRet = PacketUtilities.AppendToPacket(bRet, sVehicle);
                        bRet = PacketUtilities.AppendToPacket(bRet, sStatusNo);
                        bRet = PacketUtilities.AppendToPacket(bRet, sPacketNo);
                        bRet = PacketUtilities.AppendToPacket(bRet, sTimeOff);
                        bRet = PacketUtilities.AppendToPacket(bRet, bCR);
                        sPacketName = "Status Message";
                        #endregion
                    }
                    #endregion
                    break;
                case (byte)0x4C: // L
                    #region Left On Message
                    //  [CmdType] [Card No] [Vehicle] [Quantity Left] [Packet No] [Time Off] [CR]
                    bRet = PacketUtilities.AppendToPacket(bRet, bCardNumber);
                    bRet = PacketUtilities.AppendToPacket(bRet, sVehicle);
                    bRet = PacketUtilities.AppendToPacket(bRet, sQuantityLeft);
                    bRet = PacketUtilities.AppendToPacket(bRet, sPacketNo);
                    bRet = PacketUtilities.AppendToPacket(bRet, sTimeOff);
                    bRet = PacketUtilities.AppendToPacket(bRet, bCR);
                    sPacketName = "Left On Message";
                    #endregion
                    break;
                case (byte)0x50: // P
                    #region GPS Message
                    //  [CmdType] [Card No] [Vehicle] [UTC] [Lat] [Long] [Velocity] [CR]
                    bRet = PacketUtilities.AppendToPacket(bRet, bCardNumber);
                    bRet = PacketUtilities.AppendToPacket(bRet, sVehicle);
                    bRet = PacketUtilities.AppendToPacket(bRet, sUTCTime);
                    bRet = PacketUtilities.AppendToPacket(bRet, sVehicleLat);
                    bRet = PacketUtilities.AppendToPacket(bRet, sVehicleLong);
                    bRet = PacketUtilities.AppendToPacket(bRet, sVehicleVelocity);
                    bRet = PacketUtilities.AppendToPacket(bRet, bCR);
                    sPacketName = "GPS Message";
                    #endregion
                    break;
                default:
                    bRet = null;
                    break;
            }
            string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
            if (_log != null) _log.Info(sData);
            return bRet;
        }
        #endregion

        #region Parse Incomming Packet
        public string ParseIncommingPacket(byte[] bMsg)
        {
            try
            {
                sPacketName = TranslateMsgToHost(bMsg);
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                {
                    if (bMsg == null)
                        _log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = null) " + ex.InnerException.Message + " " + ex.Message, ex);
                    else
                        _log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = " + BitConverter.ToString(bMsg) + ") " + ex.InnerException.Message + ex.Message, ex);
                }
            }
            return sPacketName;
        }

        private string TranslateMsgToHost(byte[] bMsg)
        {
            string sRet = "Invalid Packet";
            // Wake Up -			[CmdType] [Card No] [CR]
            // Attach Delay -		[CmdType] [Card No] [Delay] [CR]
            // Coded Message -		[CmdType] [Card No] [Vehicle] [Message No] [Packet No] [CR]
            // Free Text -			[CmdType] [Card No] [Vehicle] [Packet No] [Message Text] [CR}
            // Job Dispatch -		[CmdType] [Card No] [Vehicle] [Packet No] [Sub Cmd] [Orig Plant] [Job Lat] [Job Long] 
            //						[Customer Name] [Load Weight] [Item Code] [Order Number] [Load Number] [Job Address] 
            //						[Job Instructions] [Delivery Date] [CR]
            bCommandType = bMsg[0];
            switch (bCommandType)
            {
                #region Command SignalInterface
                case (byte)0x3F:
                    #region Wake Up or Attack Delay
                    try
                    {
                        if (bMsg[2] == bCR || bMsg[3] == bCR)
                        {
                            bCardNumber = bMsg[1];
                            if (bMsg[2] != bCR)
                            {
                                sDelay = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 2);
                                sRet = "Attack Delay";
                            }
                            else
                                sRet = "Wake Up";
                        }
                    }
                    catch (System.Exception ex)
                    {
                        if (_log != null) _log.Error("Wake up/Attack Delay message parsing error", ex);
                        sRet = "Send Nack";
                    }
                    #endregion
                    break;
                case (byte)0x4D:
                    #region Coded Message
                    try
                    {
                        if (bMsg[9] == bCR)
                        {
                            bCardNumber = bMsg[1];
                            sVehicle = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                            sMessageNo = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 6, 2);
                            sPacketNo = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 8, 1);
                            sRet = "Coded Message";
                        }
                    }
                    catch (System.Exception ex)
                    {
                        if (_log != null) _log.Error("Coded message parsing error", ex);
                        sRet = "Send Nack";
                    }
                    #endregion
                    break;
                case (byte)0x54:
                    #region Job Dispatch or Free Text Message

                    if (_log != null) _log.Info("Job Dispatch or Free text message found");
                    if ((char)bMsg[7] == '}')
                    {
                        if (_log != null) _log.Info("Job Dispatch message found");
                        #region Job Dispatch Packet
                        try
                        {
                            bCardNumber = bMsg[1];
                            sVehicle = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                            sPacketNo = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 6, 1);
                            sSumCmd = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 7, 1);
                            string sYear = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 8, 2);
                            string sMonth = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 10, 2);
                            string sDay = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 12, 2);
                            dtTimeRequiredAtJob = new DateTime(Convert.ToInt32(sYear) + 2000, Convert.ToInt32(sMonth), Convert.ToInt32(sDay), 0, 0, 0, 0);
                            dtTimeRequiredAtPlant = dtTimeRequiredAtJob;
                            sOrderNumber = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 14, 12);
                            sTicketCode = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 26, 8);
                            sItemCode = "";
                            for (int X = 34; X < bMsg.Length - 1; X++)
                                sItemCode += Convert.ToString((char)bMsg[X]);

                        }
                        catch (System.Exception ex)
                        {
                            if (_log != null) _log.Error("Job Dispatch message parsing error", ex);
                            sRet = "Send Nack";
                        }

                        if (sRet == "Invalid Packet")
                        {
                            try
                            {
                                Int64 iItemCode = Convert.ToInt64(sItemCode);
                                sItemCode = sItemCode.PadLeft(12, '0');
                            }
                            catch (System.Exception)
                            {
                                sItemCode = sItemCode.PadRight(12, ' ');
                            }

                            try
                            {
                                sRet = "Job Dispatch";
                                if (_log != null) _log.Info("Job Dispatch message complete : Vehicle = " + sVehicle + " Packet = " + sPacketNo + " Date " + dtTimeRequiredAtJob.ToString("dd/MM/yyyy") + " Order = '" + sOrderNumber + "' Ticket = '" + sTicketCode + "' Item Code = '" + sItemCode + "'");
                            }
                            catch (System.Exception ex)
                            {

                                sRet = "Send Nack";
                                if (_log != null) _log.Error("Job Dispatch message parsing error", ex);
                            }
                        }
                        #endregion
                    }
                    else if ((char)bMsg[7] == ']')
                    {
                        #region Change Return Plant
                        try
                        {
                            bCardNumber = bMsg[1];
                            sVehicle = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                            sPacketNo = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 5, 1);
                            sSumCmd = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 7, 1);
                            sReturnPlant = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 8, 3);
                            sRet = "Change Return Plant";
                        }
                        catch (System.Exception ex)
                        {
                            if (_log != null) _log.Error("Wake up/Attack Delay message parsing error", ex);
                            sRet = "Send Nack";
                        }
                        #endregion
                    }
                    else if ((char)bMsg[7] == '[')
                    {
                        #region Put Unit in Standby
                        try
                        {
                            bCardNumber = bMsg[1];
                            sVehicle = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                            sPacketNo = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 5, 1);
                            sSumCmd = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 7, 1);
                            sRet = "Unit to Standby";
                        }
                        catch (System.Exception ex)
                        {
                            if (_log != null) _log.Error("'Put Unit in Standby' message parsing error", ex);
                            sRet = "Send Nack";
                        }
                        #endregion
                    }
                    else
                    {
                        #region Free Text Message
                        try
                        {
                            bCardNumber = bMsg[1];
                            sVehicle = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                            sPacketNo = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 5, 1);
                            sFreeTextMessage = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 7, bMsg.Length - 7);
                            sRet = "Free Text";
                        }
                        catch (System.Exception ex)
                        {
                            if (_log != null) _log.Error("Free Text parsing message parsing error", ex);
                            sRet = "Send Nack";
                        }
                        #endregion
                    }
                    #endregion
                    break;
                #endregion
                case (byte)0x32:
                case (byte)0x31:
                case (byte)0x30:
                    #region DiNet Interface
                    // [Console ID]-[Vehicle]-[Cmd Type]-[20070]-[Sub Cmd][Order Date][Order Code][Tick Code][Item Code][CR]
                    // Field					Size in Bytes		Position	Description
                    // Console ID				1						0				Always 2 (0x32)
                    // Separator				1						1				Always - (0x2D)
                    // Vehicle					4						2 - 4		ASCII Representation of a vehicle number 0000 to 9999
                    // Separator				1						5				Always - (0x2D)
                    // CmdType				1						6				Always T (0x54)
                    // Separator				1						7				Always - (0x2D)
                    // 20070					5						8 - 12		Always the numbers 20070 (can be ignored)
                    // Separator				1						13			Always - (0x2D)
                    // Job Sub Cmd			1						14			Always }, this is used to differentiate a Job packet from a free form text packet.
                    // Order Date			6						15 - 20	An ASCII string in the format yymmdd
                    //			YY				2						15 - 16
                    //			MM				2						17 - 18
                    //			dd				2						19 - 20
                    // Order Code			12					21 - 32	ASCII representation of the 12 digit order number 000000000000 to 99999999999
                    // Tick Code				8						33 - 40	ASCII representation of the 8 digit load number for the job 00000000 to 99999999
                    // Item Code				12					41 - 52	A 12 character ASCII string for the product code.
                    // CR							1						53			0x0D

                    sVehicle = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                    sSumCmd = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 15, 1);
                    if (sSumCmd == "}")
                    {
                        #region Job Dispatch
                        string sYear = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 16, 2);
                        string sMonth = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 18, 2);
                        string sDay = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 20, 2);
                        dtOrderDate = new DateTime(Convert.ToInt32(sYear) + 2000, Convert.ToInt32(sMonth), Convert.ToInt32(sDay), 0, 0, 0, 0);
                        dtTimeRequiredAtJob = new DateTime(Convert.ToInt32(sYear) + 2000, Convert.ToInt32(sMonth), Convert.ToInt32(sDay), 0, 0, 0, 0);
                        dtTimeRequiredAtPlant = dtTimeRequiredAtJob;
                        sOrderNumber = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 22, 12);
                        sTicketCode = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 34, 8);
                        sItemCode = "";
                        for (int X = 42; X < bMsg.Length - 1; X++)
                            sItemCode += Convert.ToString((char)bMsg[X]);
                        sRet = "DiNet Ticket";
                        #endregion
                    }
                    else if (sSumCmd == "]")
                    {
                        #region Change of Plant Message
                        try
                        {
                            sMessage = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 16, bMsg.Length - 17);
                            int iSpacePos = sMessage.IndexOf(" ");
                            if (iSpacePos > 0)
                            {
                                sMessage = sMessage.Substring(iSpacePos);
                                iSpacePos = sMessage.LastIndexOf(" ");
                                sMessage = sMessage.Substring(0, iSpacePos);
                                sRet = "Change Return Plant";
                            }
                        }
                        catch (System.Exception)
                        {
                            sMessage = "";
                            sRet = "";
                        }
                        #endregion
                    }
                    else if (sSumCmd == "[")
                    {
                        #region Put Unit in Standby
                        try
                        {
                            sRet = "Unit to Standby";
                        }
                        catch (System.Exception ex)
                        {
                            if (_log != null) _log.Error("'Put Unit in Standby' message parsing error", ex);
                            sRet = "";
                        }
                        #endregion
                    }
                    else
                    {
                        #region Driver Message
                        try
                        {
                            sMessage = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 15, bMsg.Length - 16);
                            sFreeTextMessage = sMessage;
                            sRet = "Driver Message";
                        }
                        catch (System.Exception)
                        {
                            sMessage = "";
                            sRet = "";
                        }
                        #endregion
                    }
                    #endregion
                    break;
                default:
                    break;
            }
            return sRet;
        }
        #endregion

        #region Utility Functions
        public string GetPacketTypeName()
        {
            return sPacketName;
        }

        // These functions expect a value in decimal degrees.
        public string ConvertLatToCommandString(double dLat)
        {
            string sLat = "0000.0000S";
            int iDegrees = 0;
            double dTemp = 0;
            string sHemisphere = "";

            try
            {
                if (dLat < 0)
                    sHemisphere = "S";
                else
                    sHemisphere = "N";
                iDegrees = Convert.ToInt32(Math.Round(dLat - 0.5, 0));
                sLat = Convert.ToString(iDegrees).PadLeft(2, '0');
                dTemp = dLat - Convert.ToDouble(iDegrees);
                dTemp = 60 * dTemp;
                dTemp = Math.Round(dTemp, 4);
                sLat += Convert.ToString(dTemp).PadRight(7, '0');
                sLat += sHemisphere;
            }
            catch (System.Exception ex)
            {
                if (_log != null) _log.Error("Error Converting Lattitude to Command Format.", ex);
            }
            return sLat;
        }

        public string ConvertLongToCommandString(double dLong)
        {
            string sLong = "0000.0000E";
            int iDegrees = 0;
            double dTemp = 0;
            string sHemisphere = "";

            try
            {
                if (dLong < 0)
                    sHemisphere = "E";
                else
                    sHemisphere = "W";
                iDegrees = Convert.ToInt32(Math.Round(dLong - 0.5, 0));
                if (iDegrees < 0)
                    iDegrees = iDegrees * -1;
                sLong = Convert.ToString(iDegrees).PadLeft(3, '0');
                dTemp = dLong - Convert.ToDouble(iDegrees);
                dTemp = 60 * dTemp;
                dTemp = Math.Round(dTemp, 4);
                sLong += Convert.ToString(dTemp).PadRight(7, '0');
                sLong += sHemisphere;
            }
            catch (System.Exception ex)
            {
                if (_log != null) _log.Error("Error Converting Longitude to Command Format.", ex);
            }
            return sLong;
        }


        #endregion
    }
}

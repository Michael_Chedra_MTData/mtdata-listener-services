using System;
using MTData.Common.Utilities;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cGPSData.
    /// </summary>
    public class cGPSData
    {

        public double dLatitude = 0;
        public double dLongitude = 0;
        public System.DateTime dtGPSTime = new System.DateTime(1, 1, 1);
        public int iSpeed = 0;
        public int iHeading = 0;
        public sbyte TimeZone_OffSet_Hours;
        public byte TimeZone_OffSet_Minutes;

        public cGPSData()
        {
        }

        public cGPSData(byte[] bMsg)
        {
            CreateFromBytes(bMsg, 0);
        }

        public cGPSData(byte[] bMsg, int iStartPoint)
        {
            CreateFromBytes(bMsg, iStartPoint);
        }

        public void CreateFromBytes(byte[] bMsg, int iStartPoint)
        {
            byte[] bLat = new byte[8];
            byte[] bLon = new byte[8];
            byte[] bGPSTimeDay = new byte[8];
            byte[] bGPSTimeMonth = new byte[8];
            byte[] bGPSTimeYear = new byte[8];
            byte[] bGPSTimeHour = new byte[8];
            byte[] bGPSTimeMinute = new byte[8];
            byte[] bGPSTimeSecond = new byte[8];
            byte[] bSpeed = new byte[8];
            byte[] bHeading = new byte[8];

            int i = 0;
            int j = 0;

            if (iStartPoint > 0)
                i = iStartPoint;

            if (bMsg[i] == (byte)0x12)
                i++;

            bLat[j++] = bMsg[i++];
            bLat[j++] = bMsg[i++];
            bLat[j++] = bMsg[i++];
            bLat[j++] = bMsg[i++];
            bLat[j++] = (byte)0x00;
            bLat[j++] = (byte)0x00;
            bLat[j++] = (byte)0x00;
            bLat[j++] = (byte)0x00;

            j = 0;

            bLon[j++] = bMsg[i++];
            bLon[j++] = bMsg[i++];
            bLon[j++] = bMsg[i++];
            bLon[j++] = bMsg[i++];
            bLon[j++] = (byte)0x00;
            bLon[j++] = (byte)0x00;
            bLon[j++] = (byte)0x00;
            bLon[j++] = (byte)0x00;


            j = 0;

            bGPSTimeDay[j++] = bMsg[i++]; // day
            bGPSTimeDay[j++] = (byte)0x00;
            bGPSTimeDay[j++] = (byte)0x00;
            bGPSTimeDay[j++] = (byte)0x00;
            bGPSTimeDay[j++] = (byte)0x00;
            bGPSTimeDay[j++] = (byte)0x00;
            bGPSTimeDay[j++] = (byte)0x00;
            bGPSTimeDay[j++] = (byte)0x00;
            j = 0;

            bGPSTimeMonth[j++] = bMsg[i++]; // month
            bGPSTimeMonth[j++] = (byte)0x00;
            bGPSTimeMonth[j++] = (byte)0x00;
            bGPSTimeMonth[j++] = (byte)0x00;
            bGPSTimeMonth[j++] = (byte)0x00;
            bGPSTimeMonth[j++] = (byte)0x00;
            bGPSTimeMonth[j++] = (byte)0x00;
            bGPSTimeMonth[j++] = (byte)0x00;
            j = 0;

            bGPSTimeYear[j++] = bMsg[i++]; // year
            bGPSTimeYear[j++] = (byte)0x00;
            bGPSTimeYear[j++] = (byte)0x00;
            bGPSTimeYear[j++] = (byte)0x00;
            bGPSTimeYear[j++] = (byte)0x00;
            bGPSTimeYear[j++] = (byte)0x00;
            bGPSTimeYear[j++] = (byte)0x00;
            bGPSTimeYear[j++] = (byte)0x00;
            j = 0;

            bGPSTimeHour[j++] = bMsg[i++]; // hour
            bGPSTimeHour[j++] = (byte)0x00;
            bGPSTimeHour[j++] = (byte)0x00;
            bGPSTimeHour[j++] = (byte)0x00;
            bGPSTimeHour[j++] = (byte)0x00;
            bGPSTimeHour[j++] = (byte)0x00;
            bGPSTimeHour[j++] = (byte)0x00;
            bGPSTimeHour[j++] = (byte)0x00;
            j = 0;

            bGPSTimeMinute[j++] = bMsg[i++]; // minute
            bGPSTimeMinute[j++] = (byte)0x00;
            bGPSTimeMinute[j++] = (byte)0x00;
            bGPSTimeMinute[j++] = (byte)0x00;
            bGPSTimeMinute[j++] = (byte)0x00;
            bGPSTimeMinute[j++] = (byte)0x00;
            bGPSTimeMinute[j++] = (byte)0x00;
            bGPSTimeMinute[j++] = (byte)0x00;
            j = 0;

            bGPSTimeSecond[j++] = bMsg[i++]; // second
            bGPSTimeSecond[j++] = (byte)0x00;
            bGPSTimeSecond[j++] = (byte)0x00;
            bGPSTimeSecond[j++] = (byte)0x00;
            bGPSTimeSecond[j++] = (byte)0x00;
            bGPSTimeSecond[j++] = (byte)0x00;
            bGPSTimeSecond[j++] = (byte)0x00;
            bGPSTimeSecond[j++] = (byte)0x00;


            j = 0;

            bSpeed[j++] = bMsg[i++];
            bSpeed[j++] = (byte)0x00;
            bSpeed[j++] = (byte)0x00;
            bSpeed[j++] = (byte)0x00;
            bSpeed[j++] = (byte)0x00;
            bSpeed[j++] = (byte)0x00;
            bSpeed[j++] = (byte)0x00;
            bSpeed[j++] = (byte)0x00;

            j = 0;

            bHeading[j++] = bMsg[i++];
            bHeading[j++] = bMsg[i++];
            bHeading[j++] = (byte)0x00;
            bHeading[j++] = (byte)0x00;
            bHeading[j++] = (byte)0x00;
            bHeading[j++] = (byte)0x00;
            bHeading[j++] = (byte)0x00;
            bHeading[j++] = (byte)0x00;

            j = 0;

            dLatitude = LatLonConversion.ConvLatLon(bLat);

            dLongitude = LatLonConversion.ConvLatLon(bLon);

            int day = BitConverter.ToInt32(bGPSTimeDay, 0);
            int month = BitConverter.ToInt32(bGPSTimeMonth, 0);
            int year = BitConverter.ToInt32(bGPSTimeYear, 0);
            int hour = BitConverter.ToInt32(bGPSTimeHour, 0);
            int minute = BitConverter.ToInt32(bGPSTimeMinute, 0);
            int second = BitConverter.ToInt32(bGPSTimeSecond, 0);

            if (year < 100)
                year += 2000;

            if (Util.IsValidDate(year, month, day, hour, minute, second))
                dtGPSTime = new DateTime(year, month, day, hour, minute, second);
            else
            {
                dtGPSTime = System.DateTime.Now.ToUniversalTime();
                dLatitude = 0;
                dLongitude = 0;
            }

            //			try
            //			{
            //				dtGPSTime	= new DateTime(year, month, day, hour, minute, second);			
            //			}
            //			catch(Exception)
            //			{
            //				
            //				dtGPSTime = System.DateTime.Now.ToUniversalTime();			
            //				dLatitude = 0;
            //				dLongitude = 0;
            //			}

            iSpeed = BitConverter.ToInt32(bSpeed, 0);
            iHeading = BitConverter.ToInt32(bHeading, 0);
        }
    }


    /// <summary>
    /// This class converts lat lon from byte array given from 5040 into double.
    /// </summary>
    public class LatLonConversion
    {

        /// <summary>
        /// This method converts lat lon from byte array given from 5040 into double.
        /// </summary>
        /// <param name="bLatLon"></param>
        /// <returns></returns>
        public static double ConvLatLon(byte[] bLatLon)
        {
            int iValue = 0;
            if (bLatLon[3] >= (int)0x80)
            {
                // 5010 and 5040 MSTP pass a signed int value.
                iValue = BitConverter.ToInt32(bLatLon, 0);
                return Convert.ToDouble(iValue) / Convert.ToDouble(600000);
            }
            else
                return ConvLatLon(BitConverter.ToUInt32(bLatLon, 0));
        }

        public static double ConvLatLon(uint uiLatLon)
        {
            uint tempUint = 0;
            double dLatLon = 0;
            bool bNeg = false;

            if ((uiLatLon & 0x10000000) == 0x10000000)
            {
                // The 5080 passes an unsigned int value with 0x10000000 set it indicate a negitive number
                tempUint = uiLatLon - (int)0x10000000;
                bNeg = true;
            }
            else
                tempUint = uiLatLon;
            dLatLon = Convert.ToDouble(tempUint) / 600000;
            if (bNeg)
                dLatLon = dLatLon * -1;
            return dLatLon;
        }
    }
}

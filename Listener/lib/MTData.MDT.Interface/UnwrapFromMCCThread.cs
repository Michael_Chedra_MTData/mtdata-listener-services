using System;
using System.Threading;
using System.Collections;
using MTData.Common.Network;
using MTData.Common.Utilities;
using MTData.Transport.GenericProtocol;
using MTData.Common.Interface;
using MTData.Common.Threading;
using log4net;
using MTData.Common.Config;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for UnwrapFromMCCThread.
    /// </summary>
    public class UnwrapFromMCCThread
    {
        private int _bufferSize;																						// The packet buffer size
        private QueueInterface _mccInbound;																// For recieving packets from MCC
        private QueueInterface _mccReturn;																// For sending back to the MCC layer
        private QueueInterface _interfaceOutbound;													// For sending updates to the interface
        private ILog _log = LogManager.GetLogger(typeof(UnwrapFromMCCThread));							// For logging messages/errors to file.
        private EnhancedThread _thread;																		// The thread to unwrap MCC packets and queue them for the interface
        private ConvertSignatureConfig _signatureConfig;		// The options for processing a signature
        private string _TranslatorType = "";																	// The name of the translator object in use.
        private bool _bLogInboundComms = false;																	// Log communications from the data terminals
        private bool _bLogOutboundComms = false;																	// Log communications to the data terminals
        private DeviceLayerPacketParser _deviceLayerPacketParser;

        public UnwrapFromMCCThread(
            int bufferSize,
            string TranslatorType,
            QueueInterface mccInbound,
            QueueInterface mccReturn,
            QueueInterface interfaceOutbound,
            ConvertSignatureConfig signatureConfig)
        {
            InitialiseThread(bufferSize, TranslatorType, mccInbound, mccReturn, interfaceOutbound, signatureConfig, true, true);
        }

        public UnwrapFromMCCThread(
            int bufferSize,
            string TranslatorType,
            QueueInterface mccInbound,
            QueueInterface mccReturn,
            QueueInterface interfaceOutbound,
            ConvertSignatureConfig signatureConfig,
            bool LogInboundComms,
            bool LogOutboundComms)
        {
            InitialiseThread(bufferSize, TranslatorType, mccInbound, mccReturn, interfaceOutbound, signatureConfig, LogInboundComms, LogOutboundComms);
        }

        public void InitialiseThread(
            int bufferSize,
            string TranslatorType,
            QueueInterface mccInbound,
            QueueInterface mccReturn,
            QueueInterface interfaceOutbound,
            ConvertSignatureConfig signatureConfig,
            bool LogInboundComms,
            bool LogOutboundComms)
        {
            _bufferSize = bufferSize;
            _TranslatorType = TranslatorType;
            _mccInbound = mccInbound;
            _mccReturn = mccReturn;
            _interfaceOutbound = interfaceOutbound;
            _signatureConfig = signatureConfig;
            _bLogInboundComms = LogInboundComms;
            _bLogOutboundComms = LogOutboundComms;
            _deviceLayerPacketParser = new DeviceLayerPacketParser();
            _thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
            _thread.Name = "MCC to Interface Translator";
            _thread.Start();
        }

        private enum PacketState
        {
            SeekingStart,
            ReadingPacketLength,
            ReadingData,
            ReadingDeviceLayerPacket
        }

        public void Stop()
        {
            _thread.Stop();
        }

        /// <summary>
        /// Start Reading form the inbound queue.. determine the start point of the packet,
        /// 
        /// Convert the packet to the Interface format, ad place it in the outbound queue.. job done.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadHandler(EnhancedThread sender, object data)
        {
            int iLastStart = 0;
            int iPos = 0;
            int bufferNextInsert = 0;
            int packetLength = 0;
            int packetLengthOffset = 0;
            byte[] packetLengthBuffer = new byte[4];
            PacketState packetState = PacketState.SeekingStart;
            byte[] bRemainingBytes = null;
            byte[] buffer = new byte[_bufferSize];

            _log.Info("MCC to Interface converter Started");
            try
            {
                while (!sender.Stopping)
                {
                    byte[] entry = null;

                    try
                    {
                        lock (_mccInbound.SyncRoot)
                            if (_mccInbound.Count > 0)
                                entry = (byte[])_mccInbound.Dequeue();
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error("Bad entry in MCC inbound Queue.", ex);
                        entry = null;
                    }
                    //	Append piece of packet to 
                    if (entry == null)
                    {
                        Thread.Sleep(10);
                    }
                    else
                    {
                        if (_log != null && _bLogInboundComms) _log.Info("MCC to Interface converter : Processing Bytes : " + MTData.Common.Utilities.PacketUtilities.ConvertToAscii(entry));

                        if (bRemainingBytes != null)
                        {
                            if (_log != null && _bLogInboundComms) _log.Info("MCC to Interface converter : Appending Bytes from previous packet: " + MTData.Common.Utilities.PacketUtilities.ConvertToAscii(bRemainingBytes));
                            byte[] bTemp = new byte[bRemainingBytes.Length + entry.Length];
                            iPos = 0;
                            for (int X = 0; X < bRemainingBytes.Length; X++)
                                bTemp[iPos++] = bRemainingBytes[X];
                            for (int X = 0; X < entry.Length; X++)
                                bTemp[iPos++] = entry[X];
                            entry = new byte[bTemp.Length];
                            for (int X = 0; X < bTemp.Length; X++)
                                entry[X] = bTemp[X];
                            packetState = PacketState.SeekingStart;

                            //makesure the buffer is big enough
                            if (buffer.Length < entry.Length)
                            {
                                buffer = new byte[entry.Length];
                            }
                        }

                        DeviceLayerPacketParser.AddByteResult addByteResult;
                        for (int loop = 0; loop < entry.Length; loop++)
                        {
                            switch (packetState)
                            {
                                // Packet format = [SOP][Length1234][Unit ID][Sep][Job ID][Sep][Data][EOP]
                                //                                 ^
                                //							Byte 0

                                case PacketState.SeekingStart:
                                    {
                                        #region Seeking Start Byte
                                        try
                                        {
                                            //	Keep looking for the start of the packet..
                                            if (entry[loop] == (byte)0x0A)
                                            {
                                                bufferNextInsert = 0;
                                                packetState = PacketState.ReadingPacketLength;
                                                packetLength = 0;
                                                packetLengthOffset = 0;
                                                iLastStart = loop;
                                            }
                                            else if (entry[loop] == (byte)0x02)
                                            {
                                                _deviceLayerPacketParser.Reset();
                                                addByteResult = _deviceLayerPacketParser.AddByte(entry[loop]);
                                                if (addByteResult == DeviceLayerPacketParser.AddByteResult.ReadingPacket)
                                                {
                                                    iLastStart = loop;
                                                    packetState = PacketState.ReadingDeviceLayerPacket;
                                                }
                                            }
                                        }
                                        catch (System.Exception ex)
                                        {
                                            _log.Error("Error searching for start entry in MCC packet.", ex);
                                            bufferNextInsert = 0;
                                            packetState = PacketState.SeekingStart;
                                        }
                                        #endregion
                                        break;
                                    }
                                case PacketState.ReadingPacketLength:
                                    {
                                        #region Seeking Packet length
                                        try
                                        {
                                            packetLengthBuffer[packetLengthOffset++] = entry[loop];
                                            if (packetLengthOffset >= 4)
                                            {
                                                packetLength = BitConverter.ToInt32(packetLengthBuffer, 0);
                                                if (packetLength > 20000)
                                                {
                                                    _log.Error("MCC Payload Error : Invalid Packet Length - Read " + Convert.ToString(packetLength) + " as length.  Seeking another start of packet from cursor position " + Convert.ToString(loop - 5) + ". Bytes : " + MTData.Common.Utilities.PacketUtilities.ConvertToAscii(entry), null);
                                                    bufferNextInsert = 0;
                                                    loop = iLastStart;
                                                    packetState = PacketState.SeekingStart;
                                                }
                                                else
                                                {
                                                    if (packetLength > 0)
                                                        packetState = PacketState.ReadingData;
                                                    else
                                                        packetState = PacketState.SeekingStart;
                                                }
                                            }
                                        }
                                        catch (System.Exception ex)
                                        {
                                            _log.Error("Error searching for packet length in MCC packet.", ex);
                                            bufferNextInsert = 0;
                                            loop = iLastStart;
                                            packetState = PacketState.SeekingStart;
                                        }
                                        #endregion
                                        break;
                                    }
                                case PacketState.ReadingData:
                                    {
                                        #region Reading Packet Data
                                        try
                                        {
                                            if (bufferNextInsert < buffer.Length)
                                            {
                                                buffer[bufferNextInsert++] = entry[loop];

                                                //  if the end point is found.. convert the buffer..
                                                //	NOTE : We want to include the EOP flag, but it is not
                                                //	included in the packet length...
                                                if (bufferNextInsert == (packetLength + 1))
                                                {
                                                    if (buffer[bufferNextInsert - 1] != (byte)0x0C)
                                                    {
                                                        if (buffer[bufferNextInsert - 2] == (byte)0x0C)
                                                        {
                                                            _log.Info("Invalid Packet Received from MCC - Length was 1 greater than it should be, continuing anyway.");
                                                            loop = loop - 1;
                                                        }
                                                    }

                                                    //	Translate Packet
                                                    if (!ThreadTranslatePacket(buffer, packetLength))
                                                    {
                                                        bufferNextInsert = 0;
                                                        loop = iLastStart;
                                                    }
                                                    else
                                                    {
                                                        iLastStart = loop;
                                                    }
                                                    //  switch back to seeking start
                                                    packetState = PacketState.SeekingStart;
                                                }
                                            }
                                        }
                                        catch (System.Exception ex)
                                        {
                                            _log.Error("Error reading data from MCC packet.", ex);
                                            bufferNextInsert = 0;
                                            loop = iLastStart;
                                            packetState = PacketState.SeekingStart;
                                        }
                                        #endregion
                                        break;
                                    }
                                case PacketState.ReadingDeviceLayerPacket:
                                    try
                                    {
                                        addByteResult = _deviceLayerPacketParser.AddByte(entry[loop]);
                                        if (addByteResult == DeviceLayerPacketParser.AddByteResult.PacketReady)
                                        {
                                            iLastStart = loop;
                                            packetState = PacketState.SeekingStart;

                                            DeviceLayerExtended packet = _deviceLayerPacketParser.LastPacket;
                                            if (packet.Type == DeviceLayer.KEEP_ALIVE_TYPE)
                                            {
                                                if (packet.UnitId == DeviceLayer.KEEP_ALIVE_START)
                                                {
                                                    //send an ack back
                                                    lock (_mccReturn.SyncRoot)
                                                        _mccReturn.Enqueue(DeviceLayer.CreateKeepAliveAckPacket().GetBytes());
                                                }
                                            }
                                            else
                                            {
                                                //add device layer packet to interface outbound queue
                                                if (_log != null && _bLogInboundComms)
                                                {
                                                    _log.Info(string.Format("MCC Packet Translated : DeviceLayer Type {0}, UnitId {1}", packet.Type, packet.CreateUnitKey()));
                                                }
                                                lock (_interfaceOutbound.SyncRoot)
                                                {
                                                    packet.EnqueueTimeTicks = DateTime.Now.Ticks;
                                                    StatsHelper.IncrementPacketsReceived();
                                                    _interfaceOutbound.Enqueue(packet);
                                                    StatsHelper.SetMaxQueueSize(_interfaceOutbound.Count);
                                                }
                                            }
                                        }
                                        else if (addByteResult == DeviceLayerPacketParser.AddByteResult.Error)
                                        {
                                            //only time we get here is if checksum is wrong, set start after this packet so that it will read the next one
                                            _log.Info("Invalid device layer packet received from MCC. " + _deviceLayerPacketParser.LastError);
                                            iLastStart = loop;
                                            packetState = PacketState.SeekingStart;
                                        }
                                    }
                                    catch (Exception exp)
                                    {
                                        _log.Error("Error reading device layer data from MCC packet.", exp);
                                        loop = iLastStart;
                                        packetState = PacketState.SeekingStart;
                                    }
                                    break;
                            }
                        }
                        #region If we haven't found a complete packet
                        if (packetState != PacketState.SeekingStart)
                        {
                            iPos = 0;
                            bRemainingBytes = new byte[entry.Length - iLastStart];
                            for (int X = iLastStart; X < entry.Length; X++)
                            {
                                bRemainingBytes[iPos++] = entry[X];
                            }
                        }
                        else
                        {
                            bRemainingBytes = null;
                        }
                        #endregion
                    }
                }
            }
            finally
            {
                _log.Info("MCC to Interface converter Stopped");
            }
            return null;
        }

        private int Convert4ByteArrayToInt(byte[] data, int offset)
        {
            byte[] temp = new byte[4];
            temp[0] = data[offset];
            temp[1] = data[offset + 1];
            temp[2] = data[offset + 2];
            temp[3] = data[offset + 3];

            return BitConverter.ToInt32(temp, 0);

        }

        private enum MCCPacketState
        {
            ReadingMobileId,
            ReadingSep1,
            ReadingJobId,
            ReadingSep2,
            ReadingData,
            ReadingEOP,
            Complete
        }

        private bool ThreadTranslatePacket(byte[] buffer, int packetLength)
        {
            bool bRet = true;
            bool isKeepAlive = false;
            byte[] numberBuffer = new byte[4];
            int numberBufferOffset = 0;
            int mobileID = 0;
            int jobId = 0;

            if (_log != null && _bLogInboundComms) _log.Info("MCC Packet Found : " + Util.EncodeByteArrayForLogging(buffer, packetLength));
            try
            {

                // Packet format = [SOP][Length 1234][Unit ID][Sep][Job ID][Sep][Data][EOP]
                // [SOP] = 0x0A
                // [Sep] = 0x0B
                // [EOP] = 0x0C
                // Length includes [Unit ID][Sep][Job ID][Sep][Data]
                // [SOP][Length1234] have already been stripped out of the buffer..
                //	NOTE : Packet Length DOES NOT include [EOP]..
                //POD byte[] data = new byte[packetLength+1];
                //POD for(int loop = 0; loop <= packetLength; loop++)
                //POD 	data[loop] = buffer[loop];

                MCCPacketState packetState = MCCPacketState.ReadingMobileId;

                byte[] rawData = null;
                int rawDataNextInsert = 0;

                for (int loop = 0; loop <= packetLength; loop++)
                {
                    switch (packetState)
                    {
                        case MCCPacketState.Complete:
                            {
                                break;
                            }
                        case MCCPacketState.ReadingMobileId:
                            {
                                numberBuffer[numberBufferOffset++] = buffer[loop];
                                if (numberBufferOffset >= 4)
                                {
                                    mobileID = Convert4ByteArrayToInt(numberBuffer, 0);
                                    isKeepAlive = (mobileID == 0);
                                    packetState = MCCPacketState.ReadingSep1;
                                }
                                break;
                            }
                        case MCCPacketState.ReadingSep1:
                            {
                                if (buffer[loop] != (byte)0x0B)
                                    throw new Exception("Invalid Packet Received from Unit " + Convert.ToString(mobileID) + " - Did not find seperator 1 in correct location.");
                                packetState = MCCPacketState.ReadingJobId;
                                numberBufferOffset = 0;
                                break;
                            }
                        case MCCPacketState.ReadingJobId:
                            {
                                numberBuffer[numberBufferOffset++] = buffer[loop];
                                if (numberBufferOffset >= 4)
                                {
                                    jobId = Convert4ByteArrayToInt(numberBuffer, 0);
                                    if (isKeepAlive)
                                    {
                                        packetState = MCCPacketState.Complete;
                                    }
                                    else
                                        packetState = MCCPacketState.ReadingSep2;
                                }
                                break;
                            }
                        case MCCPacketState.ReadingSep2:
                            {
                                if (buffer[loop] != (byte)0x0B)
                                    throw new Exception("Invalid Packet Received from Unit " + Convert.ToString(mobileID) + " - Did not find seperator 2 in correct location.");
                                packetState = MCCPacketState.ReadingData;
                                rawData = new byte[packetLength - 10];
                                break;
                            }
                        case MCCPacketState.ReadingData:
                            {
                                if (loop == (packetLength))
                                {
                                    if (buffer[loop] != (byte)0x0C)
                                    {
                                        if (loop > 1)
                                        {
                                            if (buffer[loop - 1] == (byte)0x0C)
                                            {
                                                _log.Info("Invalid Packet Received from Unit " + Convert.ToString(mobileID) + " - Length was 1 greater than it should be, continuing anyway.");
                                                packetLength = packetLength - 1;
                                            }
                                            else
                                                throw new Exception("Invalid Packet Received from Unit " + Convert.ToString(mobileID) + " - Did not find EOP in correct location.");
                                        }
                                        else
                                            throw new Exception("Invalid Packet Received from Unit " + Convert.ToString(mobileID) + " - Did not find EOP in correct location.");
                                    }
                                    packetState = MCCPacketState.Complete;
                                }
                                else
                                    rawData[rawDataNextInsert++] = buffer[loop];
                                break;
                            }
                    }
                    if (packetState == MCCPacketState.Complete)
                        break;
                }

                if (packetState == MCCPacketState.Complete)
                {
                    if (isKeepAlive)
                    {
                        if (jobId == 256)
                        {
                            _log.Info("MCCTranslation : KeepAlive returned by MCC");
                        }
                        else
                        {
                            _log.Info("MCCTranslation : KeepAlive sent by MCC - Sending Ack");
                            lock (_mccReturn.SyncRoot)
                                _mccReturn.Enqueue(Globals.BuildKeepAlive(256));
                        }
                    }
                    else
                    {
                        //	Translate into a host Interface packet, and send on its way
                        object oPacketData = null;

                        // We have real data from units, reset email counters
                        //_log.Info("EMAILRECEIVED");

                        if (_TranslatorType == "5040" || _TranslatorType == "Raw")
                            oPacketData = new c5040PacketData(_signatureConfig, false);
                        else if (_TranslatorType == "Camerons")
                            oPacketData = new cCameronsPacketData(_signatureConfig, false);
                        else if (_TranslatorType == "DownUnder")
                            oPacketData = new cDownUnderPacketData(_signatureConfig, false);
                        else if (_TranslatorType == "MSTP")
                            oPacketData = new cMSTPPacketData(false);
                        else if (_TranslatorType == cBunkersPacketData.TranslatorType)
                            oPacketData = new cBunkersPacketData(_signatureConfig, false);
                        else if (_TranslatorType == "Rinkers.Quarry")
                            oPacketData = new cRinkersQuarryPacketData(_signatureConfig.PicturePath, false);
                        else if (_TranslatorType == "Yellow.Couriers")
                            oPacketData = new cYellowCouriersPacketData(_signatureConfig.PicturePath, false);
                        else if (_TranslatorType == "WinCE.Dialler")
                            oPacketData = new cWinCEDiallerPacketData();
                        else if (_TranslatorType == "MTData.Logistics")
                            oPacketData = new cLogisticsPacketData(_signatureConfig, _bLogInboundComms, _bLogOutboundComms);
                        else if (_TranslatorType == "K.S_Dispatcher")
                            oPacketData = new TerminalInterfacePacketProcessor(_signatureConfig.PicturePath, false);
                        else if (_TranslatorType == "AssetForestry_Dispatcher")
                            oPacketData = new AssetForestry_PacketProcessor(_signatureConfig.PicturePath, false);

                        if (oPacketData != null)
                        {
                            if (_log != null && _bLogInboundComms) _log.Info(string.Format("MCC Packet Translating.. : UnitId {0}, JobId {1} : ", mobileID.ToString(), Convert.ToString(jobId)) + Util.EncodeByteArrayForLogging(rawData));

                            // Decode the packet data.
                            string sErrMsg = "";
                            string sTypeName = "";

                            if (_TranslatorType == "5040" || _TranslatorType == "Raw")
                            {
                                sErrMsg = ((c5040PacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((c5040PacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "Camerons")
                            {
                                sErrMsg = ((cCameronsPacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cCameronsPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "DownUnder")
                            {
                                sErrMsg = ((cDownUnderPacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cDownUnderPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "MSTP")
                            {
                                sErrMsg = ((cMSTPPacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cMSTPPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "WinCE.Dialler")
                            {
                                sErrMsg = ((cWinCEDiallerPacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cWinCEDiallerPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType.EndsWith(cBunkersPacketData.TranslatorType))
                            {
                                oPacketData = cBunkersPacketData.GetFromUnitsInstance(rawData);
                                //								sErrMsg = ((cBunkersPacketData) oPacketData).ParseIncommingPacket(rawData,  mobileID.ToString());
                                //								if (oPacketData != null) sTypeName = ((cBunkersPacketData) oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "Rinkers.Quarry")
                            {
                                sErrMsg = ((cRinkersQuarryPacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cRinkersQuarryPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "Yellow.Couriers")
                            {
                                sErrMsg = ((cYellowCouriersPacketData)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cYellowCouriersPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "MTData.Logistics")
                            {
                                sErrMsg = ((cLogisticsPacketData)oPacketData).TranslateMsgToHost(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((cLogisticsPacketData)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "K.S_Dispatcher")
                            {
                                sErrMsg = ((TerminalInterfacePacketProcessor)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((TerminalInterfacePacketProcessor)oPacketData).GetPacketTypeName();
                            }
                            else if (_TranslatorType == "AssetForestry_Dispatcher")
                            {
                                sErrMsg = ((AssetForestry_PacketProcessor)oPacketData).ParseIncommingPacket(rawData, mobileID.ToString());
                                if (oPacketData != null) sTypeName = ((AssetForestry_PacketProcessor)oPacketData).GetPacketTypeName();
                            }

                            if (sErrMsg.Length > 0)
                            {
                                if (rawData != null)
                                    _log.Info("Interface Packet Creation Failed : " + sErrMsg + " Raw Data : " + BitConverter.ToString(rawData));
                                else
                                    _log.Info("Interface Packet Creation Failed : " + sErrMsg + " Raw Data : null");
                            }
                            if (oPacketData == null)
                                _log.Info(string.Format("MCC Packet Translation : No Interface Packets found : UnitId {0}, JobId {1} : ", mobileID.ToString(), Convert.ToString(jobId)) + Util.EncodeByteArrayForLogging(rawData));

                            if (oPacketData != null && sErrMsg.Length == 0)
                            {
                                if (_log != null && _bLogInboundComms) _log.Info(string.Format("MCC Packet Translated : UnitId {0}, JobId {1}, Packet Type : {2} ", mobileID.ToString(), Convert.ToString(jobId), sTypeName));

                                lock (_interfaceOutbound.SyncRoot)
                                    _interfaceOutbound.Enqueue(oPacketData);
                            }
                        }
                        else
                        {
                            _log.Info("Invalid Translator Type - " + _TranslatorType);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("MCC Payload Error", ex);
                bRet = false;
            }
            return bRet;
        }
    }
}

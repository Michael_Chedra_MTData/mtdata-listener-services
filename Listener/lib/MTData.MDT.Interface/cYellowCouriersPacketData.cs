using System;
using System.IO;
using System.Collections;
using MTData.MotFileReader;
using log4net;
using MTData.Common.Utilities;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cYellowCouriersPacketData.
    /// </summary>
    public class cYellowCouriersPacketData
    {
        #region Incomming Packet Defs
        // [Standard Packet] = [SOP] [Length (2 Bytes)] [CmdType (1 Byte)][PacketData][SBS (1 Byte)][EOP (1 Byte)]
        //	[GPS] = [LSC] [Lat] [Long] [GPS Time] [Speed] [Heading] [Distance] [DeviceTimer]
        //	[Header] = [Mobile ID] [Fleet ID] [Vehicle ID] [Protocol Ver] [Hardware Type] [GPS] [Username] [USB]
        // [Packet Data] Definitions
        //	Download Request - [SOP] [Length (2 Bytes)] [CmdType "D" (1 Byte)][Send Segment][SBS (1 Byte)][EOP (1 Byte)]
        #endregion
        #region Outgoing Packet Defs
        // [Standard Packet] = [SOP] [Length (2 Bytes)] [CmdType (1 Byte)][PacketData][SBS (1 Byte)][EOP (1 Byte)]
        // [Packet Data] Definitions
        //	Download Packets - CmdType = "D"
        //		Download Header :		[SubCmd = H][Software Version (2 Bytes)][Number of Segments (4 Bytes)]
        //		Download Data :			[SubCmd = D][Segment Number (4 Bytes)][Segment Length (2 Bytes)][Data (Var Length][CheckSum (2 bytes)]
        #endregion
        #region Private Vars
        private const string sClassName = "MTData.MDT.Interface.cYellowCouriersPacketData.";
        private ILog _log = LogManager.GetLogger(typeof(cYellowCouriersPacketData));
        private bool bTranslateRJs = true;
        #endregion
        #region Public Vars
        // File Download vars
        public ushort MotFileRequestedParcelNo = 0;
        public byte SoftwareVersionMajor = 0;
        public byte SoftwareVersionMinor = 0;
        public ushort TotalNumberOfDataParcels = 0;
        public ushort NumberOfDataParcels = 0;
        public ushort DownloadMotParcelNumber = 0;
        public ArrayList DownloadMotParcel = null;
        public ushort[] DownloadMotParcelCheckSum = null;
        public byte DownloadVersion = 0;
        public ushort PacketNumber = 0;
        public ushort RequestedParcelNo;

        //download verfity variables
        public int ProgramTotalCheckSum = 0;
        public int PacketTotalCheckSum = 0;
        public int ProgramPacketCountTotal = 0;
        public int ProgramByteTotal = 0;
        public int ProgramDataBytesCheckSum = 0;

        public string sSigFilePath = "";
        public byte[] bRawData = null;

        // Shared Public variables
        public string CommandType = "";						// [CmdType]
        /// <summary>
        /// Maps to the driver PIN in the MTData system
        /// </summary>
        public int iMobileID = 0;							// [Mobile ID] 
        public string SubCommandType = "";					// [SubCmd]
        public int VehicleID = 0;							// [VehicleID]
        public int DriverID = 0;							// [DriverID]
        public int FleetID = 0;								// [FleetID]
        public int JobID = 0;								// [JobID]
        public int ProtocolVersion = 0;						// [Protocol Version]
        public int HardwareType = 0;						// [Hardware Type]
        public int Distance = 0;							// [Distance]
        public int DeviceTimeSeconds = 0;					// Time since last GPS lock
        public cGPSData GPSPosition = null;					// [GPS] 

        //Login Response
        public string Logon_Result = "";		// Login result
        //	C: Login accepted
        //	U: Unknown username
        //	P: Incorrect password
        //	Q: Contact query channe
        public string Logon_DriversName = "";		// Login Drivers name
        public int Logon_VehicleType = 0;		// Flag set if the vehicle is a truck, 1 for truck
        public string Logon_DaylightSavings = "";		// Login daylights savings active, "Y", or "N"
        public int Logon_GMT_OffsetHrs = 0;		// GMT Offset Hours
        public int Logon_GMT_OffsetMins = 0;		// GMT Offset Minutes
        public int Logon_NetworkKeepAlive = 0;		// network keep alive time in seconds
        public int Logon_NewMobileID = 0;		// change of mobile id
        public int Logon_ServerMajor_FirmwareVersion = 0;
        public int Logon_ServerMinor_FirmwareVersion = 0;

        //Logout Response
        public string Logoff_Result = "";		// logout response,
        //	C: Logout accepted
        //	Q: Contact query channel

        //Current Operators change information
        public string Operators_DispatcherName = "";		// dispatchers operator name
        public string Operators_Queryname = "";	// query operator name

        //job header variables
        public string JobHeader_JobNumber = "";		// job number in ASCII
        public int JobHeader_LegCount = 0;		// number of stops
        public string JobHeader_Type = "";		// job type
        //	P � Permanent
        //	M � Mess. Post
        //	A � Advanced
        //	C � On Call
        public short JobHeader_Priority = 0;		// 0 = Highest priority - 255 = Lowest
        public string JobHeader_Text = "";		// job and job stop header text
        public string JobHeader_ServiceType = "";		// job service type
        //	VIP
        //	Blood
        //	AAE
        //	Code Red
        //	Express
        //	Super
        public string JobHeader_ChargeMethod = "";		// job charge method
        //	Basic
        //	Hrs/KMs
        //	B. KMs
        public int JobHeader_TimeStart_Day = 0;		// job time start   
        public int JobHeader_TimeStart_Hour = 0;		// job time start   
        public int JobHeader_TimeStart_Min = 0;		// job time start   
        public string JobHeader_VehcileType = "";		// job vehicle type, max 30 characters.
        public string JobHeader_DebtorsName = "";		// job debtors name, max 50 characters.

        //job stop variables
        public int JobStop_Number = 0;			// number of this leg
        public string JobStop_Type = "";			// leg type "P" or "D"
        public int JobStop_Flags = 0;			//	Bit 1 SET = Signature Flag 
        //	Bit 2 SET = Quantity Flag
        //	Bit 3 SET = Barcode Flag
        //	Bit 4 SET = Arrive Flag
        //	Bit 5 SET = Depart Flag
        public string JobStop_Status = "";			// Job Stop leg state
        public int JobStop_Quantity = 0;			// job stop barcodes or items quantity
        public string JobStop_ItemDescription = "";			// job stop item description
        public int JobStop_Latitude = 0;			// job stop latitude
        public int JobStop_Longitude = 0;			// job stop longitude
        public string JobStop_HeaderText = "";			// job stop header text		max 24 characters
        public string JobStop_CustomerName = "";			// job stop customer name	max 50 characters
        public string JobStop_Address = "";			// job stop address			max 100 characters
        public string JobStop_PhoneNo = "";			// job stop phone number	max 10 characters
        public string JobStop_Remaks = "";			// job stop remarks,		max 600 characters

        //Delete Job/Leg
        public int JobDelete_HistoryFLG = 0;			// delete job history flags,
        //	0 for delete permantly, or 
        //	1 for move job/leg to complete status
        //  2 to indicated an update is coming.

        //Message to Driver
        public int Message_ID = 0;			// message id of this message,
        public string Message_Text = "";			// message text 
        public int Message_RelatedJobID = 0;			// related job id to the message

        //PreDefined Message Download List
        public int PreDefined_MessageListID = 0;			// predefined message list ID
        public int PreDefined_MessageAmount = 0;
        public int PreDefined_MessageID = 0;			// predefined message ID
        public ArrayList oPreDefinedMessageList = new ArrayList();

        //******Incoming packets from the MCC / MDT*******************/
        //Login request packet
        public int Login_Password = 0;			// password
        public string PDT_SerialNumber = "";			// Serail number of the PDT
        public int FirwmareVerMajor = 0;
        public int FirmwareVerMinor = 0;
        public int HardwareVerMajor = 0;
        public int HardwareVerMinor = 0;
        public int BatteryPercentage = 0;

        //Leg Complete
        public ArrayList oLegComplete_BarCodes = new ArrayList();		// barcodes stored in string array with the quantity in the second array, upto 100 barcodes
        public int LegComplete_ItemsRequired = 0;		// items required
        public int LegComplete_ItemsPickedUpOrDropped = 0;		// items picked up or delivered
        public string LegComplete_POD = "";		// pod name
        public byte[] LegComplete_Signature = null;		// signature
        public int LegComplete_Quantity = 0;		// Quantity
        public byte LegComplete_CompleteStatus = 0;		// indicates the success or failure of the pickup or delivery completion

        //Arrive and Depart menssages,
        public string JobStop_ArriveDepartType = "";		// Method the Depart message was generated
        //	A � Automatic
        //	E � Driver Entry

        //reset 5050 request packet defines
        public string ResetRequest_String = "";		// text must be PLEASE RESET THIS UNIT to reset the unit

        //update time in UTC to the Dats5050
        public int iTimeUTC_Hour = 0;
        public int iTimeUTC_Min = 0;		//

        //driver Query Channel Selection Update
        public string zRadioChannelSelection = null;

        #endregion

        #region Constructor

        public cYellowCouriersPacketData(string SigFilePath, bool TranslateRJs)
        {
            sSigFilePath = SigFilePath;
            bTranslateRJs = TranslateRJs;
        }

        #endregion

        public int LoginId { get; set; }

        #region Public Methods

        public byte[] BuildOutgoingPacket() //to MDT
        {
            // [Packet] = [SOP (1 Byte) 0x02][Mobile ID (4 Ascii String)][Length (2 Bytes)][CmdType (1 Byte Ascii)][Data][SBS (1 Byte) 0x07][EOP (1 Byte) 0x03]
            // 
            // If [CmdType] then [Data] =
            //	J - New Job - [JobID (4 bytes)][Job Type (1 byte)][Leg Priority (1 Byte)][Leg Count (1 Byte)]
            //							[Job Title (Var Ascii)][Break 0x0E][Service Type (1 Byte Ascii)][ChargeMethod (2 Byte Ascii)]
            //							[Time Start Day (1 Byte)][Time Start Hour  (1 Byte)][Time Start Min (1 Byte)]
            //							[Vehcile Type (Var Ascii)][Break (1 Byte 0x0F)]
            //							[Debtors Namee (Var Ascii)][Break (1 Byte 0x0F)]
            // A - Add Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][Type (1 Byte)][Flags (2 Bytes)][Status (1 Byte)][Quantity (1 Byte)]
            //								[Latitude (4 Bytes)][Longitude (4 Bytes)]
            //								[Header Text (var Ascii)][Break (1 Byte) 0x0F]
            //								[CustomerName (var Ascii)][Break (1 Byte) 0x0F]
            //								[Address (var Ascii)][Break (1 Byte) 0x0F]
            //								[Phone Number (var Ascii)][Break (1 Byte) 0x0F]
            //								[Remarks (var Ascii)][Break (1 Byte) 0x0F]
            // I - Logon Response - [Result (1 Byte Ascii)][Drivers Name (var Ascii)][Break (1 Byte) 0x0A][DaylightSavings (1 Byte Ascii)]
            //								[GMT Offset Hours (1 Byte)][GMT Offset Mins (1 Byte)]
            //								[Network Keep Alive (2 Bytes)][New Mobile ID (2 Bytes)]
            //								[Firmware Ver Major (1 Byte)][Firmware Ver Minor (1 Byte)]
            // O - Logoff Response - [Result (1 Byte Ascii)]
            // D - Delete Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][History Flag (1 Byte)]
            // M - Message To Driver - [Message ID (4 Bytes)][Job ID (4 Bytes)][Message (var Ascii)][Break (1 Byte) 0x0F]
            // Y - Operator Change Packet - [Dispatcher Name (var Ascii)][Break (1 Byte) 0x0F][Query Name (var Ascii)][Break (1 Byte) 0x0F]
            // K - Predefined Message List Packet - [Sub Command (1 Byte Ascii)][Message List ID (1 Byte)]
            //																([Message Seperator (1 Byte) 0x0A][Message List Number (1 Byte)][Message (var Ascii)])[Break (1 Byte) 0x0F]
            // d - Dowload Packet - [SubCmd (1 Byte Ascii)][SubCmd Data]
            // If [SubCmd] Then [SubCmd Data] = 
            //	H - Download Header = [Software Ver (2 Bytes)][NumberOfDataSegments (4 Bytes)]
            //	D - Download Header = [Segment Number (4 Bytes)][Segment Length (2 Bytes)][Data Segment (Segment Length Bytes)][CheckSum (2 Bytes)]

            MemoryStream oMS = null;
            byte[] bRet = null;
            byte[] bConvert = null;

            string sUnitID = "";

            try
            {
                oMS = new MemoryStream();

                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x02);					// [SOP (1 Byte) 0x02]
                sUnitID = Convert.ToString(iMobileID).PadLeft(4, '0').Substring(0, 4);
                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, sUnitID);							// [Unit ID (4 Byte Ascii)]
                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (short)0);							// [Length (2 Bytes)]
                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, CommandType);				// [Command Type (1 Byte Ascii)]

                switch (CommandType)
                {
                    case "J":
                        //	J - New Job - [JobID (4 bytes)][Job Type (1 byte)][Leg Priority (1 Byte)][Leg Count (1 Byte)]
                        //							[Job Title (Var Ascii)][Break 0x0E][Service Type (1 Byte Ascii)][ChargeMethod (2 Byte Ascii)]
                        //							[Time Start Day (1 Byte)][Time Start Hour  (1 Byte)][Time Start Min (1 Byte)]
                        //							[Vehcile Type (Var Ascii)][Break (1 Byte 0x0F)]
                        //							[Debtors Namee (Var Ascii)][Break (1 Byte 0x0F)]
                        #region New Job
                        if (_log != null) _log.Info("Building Job Header Packet");
                        // job id
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobID);							// [JobID (4 bytes)]
                        //job number
                        int iLength = JobHeader_JobNumber.Length;
                        if (iLength < 12)
                            JobHeader_JobNumber = JobHeader_JobNumber.PadRight(12, ' ');
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_JobNumber);				// [Job Number 12 bytes of ascii]
                        //job header type
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_Type);					// [Job Type (1 byte)]
                        //job priority
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobHeader_Priority);			// [Leg Priority (1 Byte)]
                        //leg count
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobHeader_LegCount);			// [Leg Count (1 Byte)]
                        //header text
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_Text);					// [Job Title (Var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0E);						// [Break 0x0E]
                        //service type
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_ServiceType);				// [Service Type (10 Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0E);						// [Break 0x0E]
                        //charge method
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_ChargeMethod);			// [ChargeMethod (8 Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0E);						// [Break 0x0E]
                        //time job booked
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobHeader_TimeStart_Day);		// [Time Start Day (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobHeader_TimeStart_Hour);	// [Time Start Hour  (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobHeader_TimeStart_Min);		// [Time Start Min (1 Byte)]
                        //vehicle type
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_VehcileType);				// [Vehcile Type (Var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);						// [Break (1 Byte 0x0F)]
                        //debtors name
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobHeader_DebtorsName);				// [Debtors Namee (Var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);						// [Break (1 Byte 0x0F)]
                        #endregion
                        break;
                    case "A":
                        // A - Add Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][Type (1 Byte)][Flags (2 Bytes)][Status (1 Byte)][Quantity (1 Byte)]
                        //								[Latitude (4 Bytes)][Longitude (4 Bytes)]
                        //								[Header Text (var Ascii)][Break (1 Byte) 0x0F]
                        //								[Customer Name (var Ascii)][Break (1 Byte) 0x0F]
                        //								[Address (var Ascii)][Break (1 Byte) 0x0F]
                        //								[Phone Number (var Ascii)][Break (1 Byte) 0x0F]
                        //								[Remarks (var Ascii)][Break (1 Byte) 0x0F]
                        #region Add Job Stop
                        if (_log != null) _log.Info("Building Add Job Leg Packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobID);									// [JobID (4 bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobStop_Number);					// [Stop Number (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_Type.PadLeft(1, ' ').Substring(0, 1));	// [Type (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (short)JobStop_Flags);					// [Flags (2 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_Status.PadLeft(1, (char)0x00).Substring(0, 1)); // [Status (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobStop_Quantity);				// [Quantity (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_ItemDescription);				// [Item Description]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0E);							// [Break (1 Byte) 0x0E]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_Latitude);						// [Latitude (4 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_Longitude);						// [Longitude (4 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_HeaderText);					// [Header Text (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_CustomerName);					// [Customer Name (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_Address);						// [Address (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_PhoneNo);						// [Phone Number (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobStop_Remaks);						// [Remarks (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);							// [Break (1 Byte) 0x0F]
                        #endregion
                        break;
                    case "I":
                        // I - Logon Response - [Result (1 Byte Ascii)][Drivers Name (var Ascii)][Break (1 Byte) 0x0A][DaylightSavings (1 Byte Ascii)]
                        //								[GMT Offset Hours (1 Byte)][GMT Offset Mins (1 Byte)]
                        //								[Network Keep Alive (2 Bytes)][New Mobile ID (2 Bytes)]
                        //								[Firmware Ver Major (1 Byte)][Firmware Ver Minor (1 Byte)]
                        #region Logon Response
                        if (_log != null) _log.Info("Building Login Response Packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Logon_Result.PadLeft(1, (char)0x00).Substring(0, 1));		// [Result (1 Byte Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Logon_DriversName);																// [Drivers Name (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0A);																			// [Break (1 Byte) 0x0A]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)Logon_VehicleType);
                        if (Logon_DaylightSavings != "Y" && Logon_DaylightSavings != "N")
                            Logon_DaylightSavings = "Y";
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Logon_DaylightSavings.PadLeft(1, 'Y').Substring(0, 1));	// [DaylightSavings (1 Byte Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)Logon_GMT_OffsetHrs);												// [GMT Offset Hours (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)Logon_GMT_OffsetMins);											// [GMT Offset Mins (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (short)Logon_NetworkKeepAlive);										// [Network Keep Alive (2 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (short)Logon_NewMobileID);												// [New Mobile ID (2 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)Logon_ServerMajor_FirmwareVersion);					// [Firmware Ver Major (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)Logon_ServerMinor_FirmwareVersion);					// [Firmware Ver Minor (1 Byte)]
                        #endregion
                        break;
                    case "O":	// O - Logoff Response - [Result (1 Byte Ascii)]
                        #region Logoff Response
                        if (_log != null) _log.Info("Building Logoff Response Packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Logoff_Result.PadLeft(1, 'U').Substring(0, 1));					// [Result (1 Byte Ascii)]
                        #endregion
                        break;
                    case "D":		// D - Delete Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][History Flag (1 Byte)]
                        #region Delete job or job leg packet
                        if (_log != null) _log.Info("Building Delete job or job leg packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobID);																						// [JobID (4 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobStop_Number);														// [Stop Number (1 Byte)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)JobDelete_HistoryFLG);												// [History Flag (1 Byte)]
                        #endregion
                        break;
                    case "M":	// M - Message To Driver - [Message ID (4 Bytes)][Job ID (4 Bytes)][Message (var Ascii)][Break (1 Byte) 0x0F]
                        #region Message to the driver packet
                        if (_log != null) _log.Info("Building A new message for the driver packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Message_ID);																			// [Message ID (4 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, JobID);																						// [Job ID (4 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Message_Text);																		// [Message (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);																				// [Break (1 Byte) 0x0F]
                        #endregion
                        break;
                    case "K":
                        // K - Predefined Message List Packet - [Sub Command (1 Byte Ascii)][Message List ID (1 Byte)]
                        //																([Message Seperator (1 Byte) 0x0A][Message List Number (1 Byte)][Message (var Ascii)])[Break (1 Byte) 0x0F]
                        #region Predefined Message list download
                        if (_log != null) _log.Info("Building the predefined message list packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, "M");																						// [Sub Command (1 Byte Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)PreDefined_MessageListID);																		// [Message ID (4 Bytes)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)PreDefined_MessageAmount);
                        for (int i = 0; i < oPreDefinedMessageList.Count; i++)
                        {
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0A);																	// [Message Seperator (1 Byte) 0x0A]
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)i);																			// [Message List Number (1 Byte)]
                            string zTempString = (string)oPreDefinedMessageList[i];
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, zTempString);																	// [Message (var Ascii)]
                        }
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
                        #endregion
                        break;
                    case "Y":		// Y - Operator Change Packet - [Dispatcher Name (var Ascii)][Break (1 Byte) 0x0F][Query Name (var Ascii)][Break (1 Byte) 0x0F]
                        #region Current Operators update packet
                        if (_log != null) _log.Info("Building Login Response Packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Operators_DispatcherName);											// [Dispatcher Name (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, Operators_Queryname);													// [Query Name (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);																			// [Break (1 Byte) 0x0F]
                        #endregion
                        break;
                    case "R":
                        #region Delete all jobs and messages on the unit
                        if (_log != null) _log.Info("Building Delete all jobs and messages packet");
                        #endregion
                        break;
                    case "r":
                        #region Reset the 5050 request
                        if (_log != null) _log.Info("Building reset request pacekt");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, ResetRequest_String);												// [Reset string  (var Ascii)]
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x0F);												// [RES 0x0F]
                        #endregion
                        break;
                    case "T":
                        #region Update Current Time on the 5050
                        if (_log != null) _log.Info("Building UTC Time update packet");
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)iTimeUTC_Hour);
                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)iTimeUTC_Min);
                        #endregion
                        break;
                    case "d":
                        #region Download Packets

                        MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, SubCommandType);															// [SubCmd (1 Byte Ascii)]
                        if (SubCommandType == "d")
                        {
                            //[CmdType]			= 'd'
                            //[MobileID]		
                            //[SubCmd]			= 'd'
                            //[CurrentVersionMSB]
                            //[CurrentVersionLSB]
                            //[TotalNoOfParcels]
                            //[NoOfParcels]
                            //{
                            //	[ParcelNo]
                            //	[ProgramFragment]
                            //	[DownloadcheckSum]
                            //	[DSB]
                            //}
                            //[DESB]
                            #region Download Parcel
                            if (_log != null) _log.Info("Building Download Header Packet");

                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, this.SoftwareVersionMajor);							// [Software Ver Major (1 Byte)
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, this.SoftwareVersionMinor);							// [Software Ver Minor (1 Byte)						
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (ushort)DownloadMotParcel.Count);					// [NumberOfDataSegments (2 Bytes)
                            ushort iMaxDownloadMotParcelNumber = (ushort)(DownloadMotParcel.Count - DownloadMotParcelNumber);
                            if (iMaxDownloadMotParcelNumber > 3)
                                iMaxDownloadMotParcelNumber = 3;
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)iMaxDownloadMotParcelNumber);						// [NoOfParcels] (1 Bytes)]						
                            for (ushort i = DownloadMotParcelNumber; i < iMaxDownloadMotParcelNumber + DownloadMotParcelNumber; i++)
                            {
                                cParcel oParcel = (cParcel)DownloadMotParcel[i];
                                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (ushort)i);					// [PacketNumber] (2 Byte)]																						
                                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, oParcel.Data);				// [ProgramFragment] = 250 bytes
                                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (ushort)oParcel.CheckSum);			// [DownloadcheckSum] = 2 bytes
                                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0xBB);				// [DSB] = 1 byte
                            }
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0xCC);					// [DESB]
                            #endregion
                        }
                        else if (SubCommandType == "v")
                        {
                            //[CmdType]
                            //[MobileID]
                            //[SubCmd]
                            //[DownloadVersion]
                            //[ProgramTotalCheckSum]
                            //[PacketCheckSum]
                            //[PacketCountTotal]
                            //[ProgramByteTotal]
                            //[Checksum]
                            #region Download Verify
                            if (_log != null) _log.Info("Building Download Verify Packet");

                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)this.DownloadVersion);
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (int)ProgramTotalCheckSum);
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (int)PacketTotalCheckSum);
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (ushort)ProgramPacketCountTotal);
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (int)ProgramByteTotal);
                            MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)ProgramDataBytesCheckSum);
                            #endregion
                        }
                        #endregion
                        break;
                    default:
                        break;
                }

                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x07); // [SBS (1 Byte) 0x03]
                MTData.Common.Utilities.PacketUtilities.WriteToStream(oMS, (byte)0x03); // [EOP (1 Byte) 0x07]
                bRet = oMS.ToArray();
                #region Update the length bytes
                bConvert = new byte[4];
                bConvert = BitConverter.GetBytes(bRet.Length);
                bRet[5] = bConvert[0];
                bRet[6] = bConvert[1];
                #endregion
                string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
                if (_log != null) _log.Info(sData);
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                    _log.Error("MTData.MDTInterface.cYellowCouriersPacketData BuildOutgoingPacket()", ex);
            }
            return bRet;
        }

        public string ParseServerPacket(byte[] bMsg)
        {
            // Special Field Defs
            // [Packet] = [SOP (1 Byte) 0x02][Mobile ID (4 Ascii String)][Length (2 Bytes)][CmdType (1 Byte Ascii)][Data][SBS (1 Byte) 0x07][EOP (1 Byte) 0x03]
            // 
            // If [CmdType] then [Data] =
            //	J - New Job - [JobID (4 bytes)][Job Type (1 byte)][Leg Priority (1 Byte)][Leg Count (1 Byte)]
            //							[Job Title (Var Ascii)][Break 0x0E][Service Type (1 Byte Ascii)][ChargeMethod (2 Byte Ascii)]
            //							[Time Start Day (1 Byte)][Time Start Hour  (1 Byte)][Time Start Min (1 Byte)]
            //							[Vehcile Type (Var Ascii)][Break (1 Byte 0x0F)]
            //							[Debtors Namee (Var Ascii)][Break (1 Byte 0x0F)]
            // A - Add Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][Type (1 Byte)][Flags (2 Bytes)][Status (1 Byte)][Quantity (1 Byte)]
            //								[Latitude (4 Bytes)][Longitude (4 Bytes)]
            //								[Header Text (var Ascii)][Break (1 Byte) 0x0F]
            //								[CustomerName (var Ascii)][Break (1 Byte) 0x0F]
            //								[Address (var Ascii)][Break (1 Byte) 0x0F]
            //								[Phone Number (var Ascii)][Break (1 Byte) 0x0F]
            //								Remarks (var Ascii)][Break (1 Byte) 0x0F]
            // I - Logon Response - [Result (1 Byte Ascii)][Drivers Name (var Ascii)][Break (1 Byte) 0x0A][DaylightSavings (1 Byte Ascii)]
            //								[GMT Offset Hours (1 Byte)][GMT Offset Mins (1 Byte)]
            //								[Network Keep Alive (2 Bytes)][New Mobile ID (2 Bytes)]
            //								[Firmware Ver Major (1 Byte)][Firmware Ver Minor (1 Byte)]
            // O - Logoff Response - [Result (1 Byte Ascii)]
            // D - Delete Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][History Flag (1 Byte)]
            // M - Message To Driver - [Message ID (4 Bytes)][Job ID (4 Bytes)][Message (var Ascii)][Break (1 Byte) 0x0F]
            // Y - Operator Change Packet - [Dispatcher Name (var Ascii)][Break (1 Byte) 0x0F][Query Name (var Ascii)][Break (1 Byte) 0x0F]
            // K - Predefined Message List Packet - [Sub Command (1 Byte Ascii)][Message List ID (1 Byte)]
            //																([Message Seperator (1 Byte) 0x0A][Message List Number (1 Byte)][Message (var Ascii)])[Break (1 Byte) 0x0F]
            // d - Dowload Packet - [SubCmd (1 Byte Ascii)][SubCmd Data]
            // If [SubCmd] Then [SubCmd Data] = 
            //	H - Download Header = [Software Ver (2 Bytes)][NumberOfDataSegments (4 Bytes)]
            //	D - Download Header = [Segment Number (4 Bytes)][Segment Length (2 Bytes)][Data Segment (Segment Length Bytes)][CheckSum (2 Bytes)]

            int iPosition = 0;
            byte bValue = (byte)0x00;
            short isValue = 0;
            string sTemp = "";

            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);					// [SOP (1 Byte) 0x02]
            if (bValue != (byte)0x02)
                return "SOP was not found";
            #region Read the unit id
            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref sTemp);		// [Mobile ID (4 Ascii String)]
            this.iMobileID = Convert.ToInt32(sTemp);
            #endregion
            #region Validate the packet
            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
            if (bMsg.Length != isValue)
                return "Packet length is incorrect";
            if (bMsg[isValue - 2] != (byte)0x07)																											// [SBS (1 Byte) 0x07]
                return "SBS was not found";
            if (bMsg[isValue - 1] != (byte)0x03)																											// [EOP (1 Byte) 0x03]
                return "EOP was not found";
            #endregion
            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
            this.CommandType = Convert.ToString((char)bValue);
            switch (CommandType)
            {
                case "J":		// J - New Job - [JobID (4 bytes)][Job Type (1 byte)][Leg Priority (1 Byte)][Leg Count (1 Byte)]
                    //						[Job Title (Var Ascii)][Break 0x0E][Service Type (1 Byte Ascii)][ChargeMethod (2 Byte Ascii)]
                    //						[Time Start Day (1 Byte)][Time Start Hour  (1 Byte)][Time Start Min (1 Byte)]
                    //						[Vehcile Type (Var Ascii)][Break (1 Byte 0x0F)]
                    //						[Debtors Namee (Var Ascii)][Break (1 Byte 0x0F)]
                    #region New Job
                    //job id
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);						// [JobID (4 bytes)]
                    //job number
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)12, ref JobHeader_JobNumber);
                    //job header type
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    JobHeader_Type = Convert.ToString((char)bValue);																					// [Job Type (1 byte)]
                    //job priority
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    this.JobHeader_Priority = (short)bValue;																											// [Leg Priority (1 Byte)]
                    //leg count
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    this.JobHeader_LegCount = (int)bValue;																										// [Leg Count (1 Byte)]
                    //job header text
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0E, ref JobHeader_Text);	// [Job Title (Var Ascii)][Break 0x0E]
                    //service type
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0E, ref JobHeader_ServiceType);	// [Service Type 10 ASCII Chars]
                    //charge method
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0E, ref JobHeader_ChargeMethod);	// [ChargeMethod 8 ASCII Chars]
                    //time job was booked
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						// [Time Start Day (1 Byte)]
                    JobHeader_TimeStart_Day = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						// [Time Start Hour  (1 Byte)]
                    JobHeader_TimeStart_Hour = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						// [Time Start Min (1 Byte)]
                    JobHeader_TimeStart_Min = (int)bValue;
                    //vehicle type
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobHeader_VehcileType);	// [Vehcile Type (Var Ascii)][Break (1 Byte 0x0F)]
                    //debtors name
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobHeader_DebtorsName);	// [Debtors Namee (Var Ascii)][Break (1 Byte 0x0F)]
                    #endregion
                    break;
                case "A":		// A - Add Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][Type (1 Byte)][Flags (2 Bytes)][Status (1 Byte)][Quantity (1 Byte)]
                    //								[Latitude (4 Bytes)][Longitude (4 Bytes)]
                    //								[Header Text (var Ascii)][Break (1 Byte) 0x0F]
                    //								[CustomerName (var Ascii)][Break (1 Byte) 0x0F]
                    //								[Address (var Ascii)][Break (1 Byte) 0x0F]
                    //								[Phone Number (var Ascii)][Break (1 Byte) 0x0F]
                    //								Remarks (var Ascii)][Break (1 Byte) 0x0F]
                    #region Add Job Stop
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);						// [JobID (4 Bytes)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						//[Number (1 Byte)]
                    JobStop_Number = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						//[Type (1 Byte)]
                    JobStop_Type = Convert.ToString((char)bValue);
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);						//[Flags (2 Bytes)]
                    JobStop_Flags = (int)isValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						//[Status (1 Byte)]
                    JobStop_Status = Convert.ToString((char)bValue);
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);						//[Quantity (1 Byte)]
                    JobStop_Quantity = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0E, ref JobStop_ItemDescription);	//[Description of items]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobStop_Latitude);	//[Latitude (4 Bytes)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobStop_Longitude);//[Longitude (4 Bytes)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobStop_HeaderText);	//[Header Text (var Ascii)][Break (1 Byte) 0x0F]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobStop_CustomerName);	//[CustomerName (var Ascii)][Break (1 Byte) 0x0F]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobStop_Address);	// [Address (var Ascii)][Break (1 Byte) 0x0F]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobStop_PhoneNo);	// [Phone Number (var Ascii)][Break (1 Byte) 0x0F]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref JobStop_Remaks);	// [Remarks (var Ascii)][Break (1 Byte) 0x0F]
                    #endregion
                    break;
                case "I":	// I - Logon Response - [Result (1 Byte Ascii)][Drivers Name (var Ascii)][Break (1 Byte) 0x0A][DaylightSavings (1 Byte Ascii)]
                    //								[GMT Offset Hours (1 Byte)][GMT Offset Mins (1 Byte)]
                    //								[Network Keep Alive (2 Bytes)][New Mobile ID (2 Bytes)]
                    //								[Firmware Ver Major (1 Byte)][Firmware Ver Minor (1 Byte)]
                    #region Logon Response
                    //result
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    Logon_Result = Convert.ToString((char)bValue);																															// [Result (1 Byte)]
                    //Driver name
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0A, ref Logon_DriversName);			// [Drivers Name (var Ascii)][Break (1 Byte) 0x0A]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    Logon_VehicleType = (int)bValue;																											// [DaylightSavings (1 Byte Ascii)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    Logon_DaylightSavings = Convert.ToString((char)bValue);																											// [DaylightSavings (1 Byte Ascii)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [GMT Offset Hours (1 Byte)]
                    Logon_GMT_OffsetHrs = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [GMT Offset Mins (1 Byte)]
                    Logon_GMT_OffsetMins = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);														// [Network Keep Alive (2 Bytes)]
                    Logon_NetworkKeepAlive = (int)isValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);														// [New Mobile ID (2 Bytes)]
                    Logon_NewMobileID = (int)isValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [Firmware Ver Major (1 Byte)]
                    Logon_ServerMajor_FirmwareVersion = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [Firmware Ver Minor (1 Byte)]
                    Logon_ServerMinor_FirmwareVersion = (int)bValue;
                    #endregion
                    break;
                case "O":	// O - Logoff Response - [Result (1 Byte Ascii)]
                    #region Logoff Response
                    //result
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                    Logoff_Result = Convert.ToString((char)bValue);																															//[Result (1 Byte Ascii)]
                    #endregion
                    break;
                case "D":		// D - Delete Job Stop - [JobID (4 Bytes)][Stop Number (1 Byte)][History Flag (1 Byte)]
                    #region Delete Job Stop
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);														// [JobID (4 Bytes)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [Stop Number (1 Byte)]
                    JobStop_Number = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [History Flag (1 Byte)]
                    JobDelete_HistoryFLG = (int)bValue;
                    #endregion
                    break;
                case "M":	// M - Message To Driver - [Message ID (4 Bytes)][Job ID (4 Bytes)][Message (var Ascii)][Break (1 Byte) 0x0F]
                    #region Message to the driver packet
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_ID);											// [Message ID (4 Bytes)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);														// [Job ID (4 Bytes)]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Message_Text);					// [Message (var Ascii)][Break (1 Byte) 0x0F]
                    #endregion
                    break;
                case "Y":		// Y - Operator Change Packet - [Dispatcher Name (var Ascii)][Break (1 Byte) 0x0F][Query Name (var Ascii)][Break (1 Byte) 0x0F]
                    #region Operator Change Packet
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Operators_DispatcherName);	// [Dispatcher Name (var Ascii)][Break (1 Byte) 0x0F]
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref Operators_Queryname);			// [Query Name (var Ascii)][Break (1 Byte) 0x0F]
                    break;
                    #endregion
                case "K":		// K - Predefined Message List Packet - [Sub Command (1 Byte Ascii)][Message List ID (1 Byte)]
                    //																([Message Seperator (1 Byte) 0x0A][Message List Number (1 Byte)][Message (var Ascii)])[Break (1 Byte) 0x0F]
                    #region Predefined Message List Packet
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [Sub Command (1 Byte Ascii)]
                    char cTemp = Convert.ToChar(bValue);
                    SubCommandType = Convert.ToString(cTemp);
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [Message List ID (1 Byte)]
                    PreDefined_MessageListID = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [Break (1 Byte) 0x0A] OR [EOP (1 Byte) 0x0F]
                    PreDefined_MessageAmount = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);			//read the MSS character, start of message seperator
                    if (bValue == 0x0A)
                    {
                        for (int i = 0; i < PreDefined_MessageAmount; i++)
                        {
                            sTemp = "";
                            //read the message id
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                            //message
                            for (int j = 0; j < 20; j++)
                            {
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                                if ((bValue == 0x0A) || (bValue == 0x0F))
                                    break;
                                sTemp += Convert.ToChar(bValue);
                            }
                            if (sTemp != "")
                                oPreDefinedMessageList.Add(sTemp);
                            if (bValue == 0x0F)
                                break;
                        }
                    }
                    #endregion
                    break;
                case "R":
                    #region Removal of jobs and messages

                    #endregion
                    break;
                case "r":
                    #region Reset Request
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref ResetRequest_String);
                    #endregion
                    break;
                case "T":
                    #region Time Update in UTC
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [
                    iTimeUTC_Hour = (int)bValue;
                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);														// [
                    iTimeUTC_Min = (int)bValue;
                    #endregion
                    break;
                case "d":
                    break;
                default:
                    break;
            }
            return "";
        }

        public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
        {
            // Special Field Defs
            // [GPS] = [LSC (1 Byte)] [Lat (4 Bytes)] [Long (4 Bytes)] [GPS Time  (6 Bytes)] [Speed (1 Byte)] [Heading (2 Bytes)] [DeviceTimer (2 Bytes)][Distance (4 Bytes)] 
            // [Header] = [MobileID (4 Bytes)][VehicleID (2 Bytes)][FleetID (1 Bytes)][GPS (24 Bytes)][DriverID (4 Byte Ascii)]
            // [Packet] = [CmdType (1 Byte)][Data][SBS (1 Byte)][Spare (1 Byte)]

            // 
            // If [CmdType] then [Data] =
            //	N - ACK / NACK - No Payload
            //	I - Login Packet - [Login Password (4 Byte Ascii)][MessageListID (1 Byte)]
            //	O - Logout Packet - No Payload
            //	A - Job Accept Packet - [Job ID (4 Bytes)]
            //	S - Job Stop Complete Packet  - 
            //				[Sub Section] = [Sub Cmd Type (1 Byte)][Sub Cmd Data]
            //				If [Sub Cmd Type] then [Sub Cmd Data] =
            //					0x0A - Barcodes - ([Barcode (Var Ascii)][Break (1 Byte 0x0C)][Quantity (1 Byte)])[Break (1 Byte 0x0B) Or (1 Byte 0x0A)]
            //					0x0C - Job Leg Complete - [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]
            //					0x0B - Signature Data - [Length (2 bytes)][Signature Data ([Length] Bytes)]
            //		[Job Stop Complete Packet]  = [Job ID (4 Bytes)][JobStop_Number (1 Bytes)]([Sub Section X])[LegComplete_CompleteStatus (1 Byte)]
            //	C - Job Complete Packet - [Job ID (4 Bytes)]
            //	M - Message Packet  - [SubCmd][Sub Cmd Data]
            //		If [SubCmd] then [Sub Cmd Data] = 
            //			F - Incoming Freeform Message Packet - [Sub Cmd Data] = [Message (Var Ascii)][Break (1 Byte 0x06)]
            //			P - Incoming Predefined Message Packet - [Sub Cmd Data] = [MessageID (1 Byte)]
            //			R - Message Read by Driver Packet - [Sub Cmd Data] = [MessageID (4 Byte)]
            //	B - Arrive Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
            //	D - Depart Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
            //	d - Request Data Segment - [Send Segment (4 Bytes)]
            #region Local Variables
            byte bValue = (byte)0x00;
            short isValue = 0;
            int iPosition = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            string sTemp = "";
            byte[] bTempArray = new byte[1];
            #endregion

            try
            {
                #region Get the command type
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                this.CommandType = Convert.ToString((char)bValue);
                #endregion
                #region If the packet type has a sub command, read it
                switch (this.CommandType)
                {
                    case "d":
                    case "S":
                    case "M":
                    case "R":
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.SubCommandType = Convert.ToString((char)bValue);
                        break;
                    default:
                        break;
                }
                #endregion
                #region Get the Fleet, Vehicle and Mobile IDs
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, 4, ref sTemp);
                iMobileID = Convert.ToInt32(sTemp);
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
                VehicleID = (int)isValue;
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                FleetID = (int)bValue;
                #endregion
                #region [GPS]
                this.GPSPosition = new cGPSData();
                // LSC Byte
                iPosition++;
                // Lat
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref bTempArray);
                this.GPSPosition.dLatitude = LatLonConversion.ConvLatLon(bTempArray);
                // Long
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref bTempArray);
                this.GPSPosition.dLongitude = LatLonConversion.ConvLatLon(bTempArray);
                #region Get GPS Time string
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iDay = (int)bValue;
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iMonth = (int)bValue;
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iYear = ((int)bValue) + 2000;
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iHour = (int)bValue;
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iMinute = (int)bValue;
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                iSecond = (int)bValue;
                try
                {
                    this.GPSPosition.dtGPSTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                }
                catch (System.Exception)
                {
                }
                #endregion
                // Speed
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                this.GPSPosition.iSpeed = (int)bValue;
                // Heading
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
                this.GPSPosition.iHeading = (int)isValue;
                // Device Time is Seconds
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);
                this.DeviceTimeSeconds = (int)isValue;
                //distance odo
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Distance);
                #endregion
                #region Get the Driver ID
                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref sTemp);
                this.DriverID = Convert.ToInt32(sTemp);
                #endregion
                switch (CommandType)
                {
                    case "N":	// N - ACK / NACK - No Payload
                        #region	ACK / NACK
                        if (_log != null) _log.Info("Decoding ACK/NAK Packet");
                        #endregion
                        break;
                    case "R":
                        #region Request Jobs Keep Alive
                        if (_log != null) _log.Info("Decoding Request for Jobs Packet (KeepAlive)");
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref JobID);
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        JobStop_Number = (int)bValue;
                        #endregion
                        break;
                    case "I":	// I - Login Packet - [Login Password (4 Byte Ascii)][MessageListID (1 Byte)]
                        #region Login Packet
                        if (_log != null) _log.Info("Decoding Login Packet");

                        //password
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)4, ref sTemp);
                        this.Login_Password = Convert.ToInt32(sTemp);

                        //predefined currently stored message list id
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.PreDefined_MessageListID = (int)bValue;

                        //PDT serial number
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, 9, ref this.PDT_SerialNumber);

                        //firwmare version major,
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.FirwmareVerMajor = (int)bValue;

                        //firmware version minor,
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.FirmwareVerMinor = (int)bValue;

                        //hardware version major,
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.HardwareVerMajor = (int)bValue;

                        //hardware version minor,
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.HardwareVerMinor = (int)bValue;

                        //Battery percentage
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        this.BatteryPercentage = (int)bValue;

                        #endregion
                        break;
                    case "O":	// O - Logout Packet - No Payload
                        #region Logout Packet
                        if (_log != null) _log.Info("Decoding Logout Packet");
                        #endregion
                        break;
                    case "A":		// A - Job Accept Packet - [Job ID (4 Bytes)]
                        #region Job Accept Packet
                        if (_log != null) _log.Info("Decoding Job Leg Accept");
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);
                        #endregion
                        break;
                    case "S":
                        #region Job Stop Complete Packet
                        // S - Job Stop Complete Packet  - 
                        //			[Sub Section] = [Sub Cmd Type (1 Byte)][Sub Cmd Data]
                        //			If [Sub Cmd Type] = 
                        //				0x0A - Barcodes - ([Barcode X (Var Ascii)][Break (1 Byte 0x0C)][Quantity X (1 Byte)])[Break (1 Byte 0x0B) Or (1 Byte 0x0A)]
                        //				0x0C - Job Leg Complete - [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]
                        //				0x0B - Signature Data - [Length (2 bytes)][Signature Data ([Length] Bytes)]
                        //		[Job Stop Complete Packet]  = [Job ID (4 Bytes)][JobStop_Number (1 Bytes)]([Sub Section X])[LegComplete_CompleteStatus (1 Byte)]
                        if (_log != null) _log.Info("Decoding Job Leg Complete Packet");
                        //jop id
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);	// [Job ID (4 Bytes)]
                        //job stop number
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);			// [JobStop_Number (1 Bytes)]
                        this.JobStop_Number = (int)bValue;

                        //items required
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);			// [JobStop_Number (1 Bytes)]
                        this.LegComplete_ItemsRequired = (int)bValue;

                        //items picked up or delivered
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);			// [JobStop_Number (1 Bytes)]
                        this.LegComplete_ItemsPickedUpOrDropped = (int)bValue;


                        //barcodes start list 
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        if (bValue == 0x0A)																																// Barcodes
                        {
                            //barcodes list start
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                            if (bValue == 0x0B)
                            {
                                string TempString = "";
                                for (int i = 0; i < 100; i++)
                                {
                                    TempString = "";
                                    ArrayList oTempArray = new ArrayList();
                                    //read the barcode
                                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0C, ref TempString);	// [Barcode X (Var Ascii)][Break (1 Byte 0x0C)]
                                    //read the quantities
                                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                                    oTempArray.Add(TempString);
                                    oTempArray.Add(bValue.ToString());
                                    //add to arraylist
                                    oLegComplete_BarCodes.Add(oTempArray);
                                    MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                                    if (bValue == 0x0A)				//if barcodes list end
                                        break;																																										// [Break (1 Byte 0x0A)]
                                }
                            }
                        }

                        //completion status
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref LegComplete_CompleteStatus);

                        //POD
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        if (bValue == 0x0C)
                        {
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref LegComplete_POD);							// [LegComplete_POD (Var Ascii)][Break (1 Byte 0x0F)]
                        }
                        //Signature
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);
                        if (bValue == 0x0B)
                        {
                            //encode the vector image into a gif

                            //signature legnth
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);																	// [Length (2 Bytes)]
                            //read vector signature into byte array
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)isValue, ref LegComplete_Signature);				// [Signature Data ([Length] Bytes)]
                            //read end byte of 0x03
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);																	// [LegComplete_CompleteStatus (1 Byte)]

                            //decode signature into a gif format
                            if (LegComplete_Signature.Length > 0)
                            {
                                string FileName = System.Configuration.ConfigurationManager.AppSettings["PicturePath"];
                                if (FileName.EndsWith("/"))
                                    FileName += "/";
                                FileName += DateTime.Now.ToString("yyMMddHHmmss-");
                                FileName += Convert.ToString(iMobileID);
                                FileName += "-";
                                FileName += Convert.ToString(JobID);
                                FileName += "-";
                                FileName += Convert.ToString(JobStop_Number);
                                FileName += "-";

                                string DefaultSignature = System.Configuration.ConfigurationManager.AppSettings["UseBackgroundGIFforSignatures"];
                                if (!DefaultSignature.Contains(":"))
                                {
                                    string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                                    path = System.IO.Path.GetDirectoryName(path);
                                    DefaultSignature = path + "\\" + DefaultSignature;
                                }

                                cVectorSignature oSig = new cVectorSignature( false, 80, 300, 200, 600, FileName, DefaultSignature, 0, 0, 0.5, true, 100000, true, System.Drawing.Color.White, true, 10);
                                LegComplete_Signature = oSig.TranslateSignature(LegComplete_Signature, (byte)0x0B);			//convert the signature into gif file
                            }
                        }
                        else if (bValue == 0x0C)
                        {
                            //copy the already compliled gif image to the byte array
                            //read the length of the gif image
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref isValue);																	// [Length (2 Bytes)]
                            //copy the gif image into the signature byte array
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (int)isValue, ref LegComplete_Signature);				// [Signature Data ([Length] Bytes)]
                            MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);																	// [LegComplete_CompleteStatus (1 Byte)]
                        }
                        #endregion
                        break;
                    case "C":	// C - Job Complete Packet - [Job ID (4 Bytes)]
                        #region Job Complete Packet
                        if (_log != null) _log.Info("Decoding Job Complete Packet");
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);
                        #endregion
                        break;
                    case "M":
                        #region Incoming driver message from driver
                        // M - Message Packet  - [SubCmd][Sub Cmd Data]
                        //		If [SubCmd] = 
                        //			F - Incoming Freeform Message Packet - [Sub Cmd Data] = [Message (Var Ascii)][Break (1 Byte 0x06)]
                        //			P - Incoming Predefined Message Packet - [Sub Cmd Data] = [MessageID (1 Byte 0x06)]
                        //			R - Message Read by Driver Packet - [Sub Cmd Data] = [MessageID (4 Byte)]
                        if (_log != null) _log.Info("Decoding Message from unit Packet");
                        switch (SubCommandType)
                        {
                            case "F":			//free formed messages
                                //related job id
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_RelatedJobID);
                                //free hand message text
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x06, ref Message_Text);	// [Message (Var Ascii)][Break (1 Byte 0x06)]
                                break;
                            case "P":			//predefined message
                                //related job id
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref Message_RelatedJobID);
                                // predefined mesage List ID
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);										//  [MessageID (1 Byte)]
                                this.PreDefined_MessageListID = (int)bValue;
                                // predefined mesage ID
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);										//  [MessageID (1 Byte)]
                                this.PreDefined_MessageID = (int)bValue;
                                break;
                            case "R":			//message read by driver
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.Message_ID);						// [MessageID (4 Byte)]
                                break;
                        }
                        #endregion
                        break;
                    case "B":	// B - Arrive Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
                    case "D":	// D - Depart Job Leg Packet - [Job ID (4 Bytes)][JobStop_Number (1 Byte)][JobStop_ArriveDepartType (1 Byte)]
                        #region Arrive or Depart Job Leg Packet
                        if (_log != null) _log.Info("Decoding Arrive or Depart Job Leg Packet");
                        //job id
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.JobID);		// [Job ID (4 Bytes)]
                        //stop number
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_Number (1 Byte)]
                        this.JobStop_Number = (int)bValue;
                        //type - auto or entered
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref bValue);				// [JobStop_ArriveDepartType (1 Byte)]
                        this.JobStop_ArriveDepartType = Convert.ToString((char)bValue);
                        #endregion
                        break;
                    case "d":	// d - Request Data Segment - [Send Segment (4 Bytes)]
                        #region Request a Download Packet
                        if (_log != null) _log.Info("Decoding Download Request Packet");
                        switch (this.SubCommandType)
                        {
                            case "r":			//request new download parcel
                                this.SubCommandType = "d";
                                MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, ref this.RequestedParcelNo);
                                break;
                            case "v":			//verify							
                                break;
                            default:
                                break;
                        }
                        #endregion
                        break;
                    case "H":
                        #region Driver Channel Selection Update
                        MTData.Common.Utilities.PacketUtilities.ReadFromPacketAtPos(bMsg, ref iPosition, (byte)0x0F, ref zRadioChannelSelection);
                        #endregion
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                {
                    if (bMsg == null)
                        _log.Error(sClassName + "TranslateMsgToHost(byte[] bMsg = null, string sUnitID = '" + sUnitID + "')", ex);
                    else
                        _log.Error(sClassName + "TranslateMsgToHost(byte[] bMsg, string sUnitID = '" + sUnitID + "')", ex);
                }
            }
            bRawData = bMsg;
            return "";
        }

        public byte[] BuildCustomerPacket()
        {
            byte[] rawData;

            rawData = System.Text.Encoding.ASCII.GetBytes(CommandType.Substring(0, 1));

            switch (CommandType)
            {
                case "S":
                case "M":
                    rawData = PacketUtilities.AppendToPacket(rawData, System.Text.Encoding.ASCII.GetBytes(SubCommandType.Substring(0, 1)));
                    break;
            }
            
            string sMobileID = iMobileID.ToString();
            sMobileID = sMobileID.PadLeft(4, '0').Substring(0, 4);
            rawData = PacketUtilities.AppendToPacket(rawData, sMobileID);
            rawData = PacketUtilities.AppendToPacket(rawData, (short)VehicleID);            
            rawData = PacketUtilities.AppendToPacket(rawData, (byte)FleetID);            
            rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x12);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null ? Convert.ToInt32(GPSPosition.dLatitude * 600000) : (int)0);
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null ? Convert.ToInt32(GPSPosition.dLongitude * 600000) : (int)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null && GPSPosition.dtGPSTime > DateTime.MinValue ? (byte)GPSPosition.dtGPSTime.Day : (byte)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null && GPSPosition.dtGPSTime > DateTime.MinValue ? (byte)GPSPosition.dtGPSTime.Month : (byte)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null && GPSPosition.dtGPSTime > DateTime.MinValue ? (byte)(GPSPosition.dtGPSTime.Year - 2000) : (byte)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null && GPSPosition.dtGPSTime > DateTime.MinValue ? (byte)GPSPosition.dtGPSTime.Hour : (byte)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null && GPSPosition.dtGPSTime > DateTime.MinValue ? (byte)GPSPosition.dtGPSTime.Minute : (byte)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null && GPSPosition.dtGPSTime > DateTime.MinValue ? (byte)GPSPosition.dtGPSTime.Second : (byte)0);            
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null ? (byte)GPSPosition.iSpeed : (byte)0);
            rawData = PacketUtilities.AppendToPacket(rawData, GPSPosition != null ? (short)GPSPosition.iHeading : (short)0);
                        
            rawData = PacketUtilities.AppendToPacket(rawData, (short)DeviceTimeSeconds);            
            rawData = PacketUtilities.AppendToPacket(rawData, (int)Distance);
            
            string strLoginId = LoginId.ToString();
            strLoginId = strLoginId.PadLeft(4, '0');
            rawData = PacketUtilities.AppendToPacket(rawData, strLoginId);

            switch (CommandType)
            {
                case "I":
                    _log.Info("Building login packet to send to customer");
                    string loginPasswordString = Login_Password.ToString();
                    rawData = PacketUtilities.AppendToPacket(rawData, loginPasswordString.PadLeft(4, '0').Substring(0, 4));
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)PreDefined_MessageListID);
                    if (PDT_SerialNumber == null)
                    {
                        _log.Error("No serial number found for vehicle (VehicleID = " + VehicleID + ", FleetID = " + FleetID + ")");
                        rawData = PacketUtilities.AppendToPacket(rawData, "000000000");
                    }
                    else if (PDT_SerialNumber.Length > 9)
                    {
                        _log.Error("Truncating serial number " + PDT_SerialNumber + " for vehicle (VehicleID = " + VehicleID + ", FleetID = " + FleetID + ")");
                        rawData = PacketUtilities.AppendToPacket(rawData, PDT_SerialNumber.Substring(0, 9));
                    }
                    else
                    {
                        rawData = PacketUtilities.AppendToPacket(rawData, PDT_SerialNumber.PadLeft(9, '0'));
                    }
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)FirwmareVerMajor);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)FirmwareVerMinor);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)HardwareVerMajor);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)HardwareVerMinor);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)BatteryPercentage);
                    rawData = PacketUtilities.AppendToPacket(rawData, 0x03); // End of text
                    break;
                case "O":
                    _log.Info("Building logout packet to send to customer");
                    break;
                case "A":
                    _log.Info("Building job accept packet to send to customer");
                    rawData = PacketUtilities.AppendToPacket(rawData, JobID);
                    break;
                case "S":
                    _log.Info("Building leg complete packet to send to customer");                    
                    rawData = PacketUtilities.AppendToPacket(rawData, (int)JobID);

                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)JobStop_Number);

                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)LegComplete_ItemsRequired);

                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)LegComplete_ItemsPickedUpOrDropped);
                                                                                                                                       
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x0A);
                    if (oLegComplete_BarCodes != null)
                    {
                        for (int i = 0; i < oLegComplete_BarCodes.Count; i++)
                        {
                            ArrayList oTemp = (ArrayList)oLegComplete_BarCodes[i];
                            rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x0B);
                            rawData = PacketUtilities.AppendToPacket(rawData, System.Text.Encoding.ASCII.GetBytes((string)oTemp[0]));
                            rawData = PacketUtilities.AppendToPacket(rawData, (byte)oTemp[1]);                            
                            rawData = PacketUtilities.AppendToPacket(rawData, (byte)(int)oTemp[2]);
                        }
                    }
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x0A);

                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)LegComplete_CompleteStatus);
                    
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x0C);
                    rawData = PacketUtilities.AppendToPacket(rawData, System.Text.Encoding.ASCII.GetBytes(LegComplete_POD));
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x0F);
                                                                                  
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x0B);
                    if (LegComplete_Signature != null && LegComplete_Signature.Length != 0)
                    {
                        rawData = PacketUtilities.AppendToPacket(rawData, (short)LegComplete_Signature.Length);
                        rawData = PacketUtilities.AppendToPacket(rawData, LegComplete_Signature);
                    }
                    else
                    {
                        rawData = PacketUtilities.AppendToPacket(rawData, (short)0x0000);                      
                    }
                    break;
                case "C":
                    _log.Info("Building job complete packet to send to customer");
                    rawData = PacketUtilities.AppendToPacket(rawData, JobID);
                    break;
                case "M":
                    switch (SubCommandType)
                    {
                        case "P":
                            _log.Info("Building predefined message from driver packet to send to customer");
                            rawData = PacketUtilities.AppendToPacket(rawData, JobID);
                            rawData = PacketUtilities.AppendToPacket(rawData, PreDefined_MessageListID);
                            rawData = PacketUtilities.AppendToPacket(rawData, PreDefined_MessageID);
                            break;
                        case "F":
                            _log.Info("Building free form message from driver packet to send to customer");
                            rawData = PacketUtilities.AppendToPacket(rawData, JobID);
                            rawData = PacketUtilities.AppendToPacket(rawData, Message_Text);
                            rawData = PacketUtilities.AppendToPacket(rawData, (byte)0x06);
                            break;
                        case "R":
                            _log.Info("Building message read from driver packet to send to customer");
                            rawData = PacketUtilities.AppendToPacket(rawData, Message_ID);
                            break;
                    }
                    break;
                case "B":
                    _log.Info("Building job leg arrive packet to send to customer");
                    rawData = PacketUtilities.AppendToPacket(rawData, JobID);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)JobStop_Number);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)'A');
                    break;
                case "D":
                    _log.Info("Building job leg complete packet to send to customer");
                    rawData = PacketUtilities.AppendToPacket(rawData, JobID);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)JobStop_Number);
                    rawData = PacketUtilities.AppendToPacket(rawData, (byte)'A');
                    break;
            }

            byte[] packet = new byte[3];
            byte[] rawDataLength = BitConverter.GetBytes(rawData.Length);
            packet[0] = (byte)0x02;
            packet[1] = rawDataLength[0];
            packet[2] = rawDataLength[1];
            packet = PacketUtilities.AppendToPacket(packet, rawData);
            packet = PacketUtilities.AppendToPacket(packet, (byte)0x07);
            packet = PacketUtilities.AppendToPacket(packet, (byte)0x03);

            return packet;
        }

        public string GetPacketTypeName()
        {
            string sRet = "";
            switch (CommandType)
            {
                case "N":
                    sRet = "ACK / NACK";
                    break;
                case "I":
                    sRet = "Login Packet";
                    break;
                case "O":
                    sRet = "Logout Packet";
                    break;
                case "A":
                    sRet = "Job Accept Packet";
                    break;
                case "S":
                    sRet = "Job Stop Complete Packet";
                    break;
                case "C":
                    sRet = "Job Complete Packet";
                    break;
                case "M":
                    if (SubCommandType == "F")
                        sRet = "Incoming Freeform Message Packet";
                    else if (SubCommandType == "P")
                        sRet = "Incoming Predefined Message Packet";
                    else if (SubCommandType == "R")
                        sRet = "Message Read by Driver Packet";
                    break;
                case "B":			//Arrive at job leg
                case "D":
                    sRet = "Depart Job Leg Packet";
                    break;
                case "K":
                    sRet = "Pre-Defined Message List Packet";
                    break;
                case "d":
                    sRet = "Request a Download Packet";
                    break;
                case "R":
                    sRet = "Request jobs packet";
                    break;
                case "r":
                    sRet = "Reset Request Packet";
                    break;
                case "T":
                    sRet = "Time UTC update packet for the 5050";
                    break;
                default:
                    sRet = "Yellow Couriers Packet";
                    break;
            }
            return sRet;
        }

        #endregion
    }
}

using System;
using System.Text;
using System.Collections;
using System.Collections.Specialized;
using System.Drawing;
using System.IO;
using MTData.Common.Network;
using MTData.Common.Utilities;
using log4net;
using MTData.Common.Config;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cCameronsPacketData.
    /// </summary>

    public class cBunkersPacketData
    {
        #region Private Vars
        private ConvertSignatureConfig _sigConfig = null;
        private ILog _log = LogManager.GetLogger(typeof(cBunkersPacketData));
        private bool bTranslateRJs = true;
        #endregion

        public const string TranslatorType = "Bunkers";
        public int DeviceTimeSeconds = 0;

        public cBunkersPacketData(ConvertSignatureConfig sigConfig, bool TranslateRJs)
        {
            _sigConfig = sigConfig;

            bTranslateRJs = TranslateRJs;
        }

        // Need a separate get instance for incomming messages from units as the
        // headers and footers are not present.
        public static IExternalMessage GetFromUnitsInstance(byte[] data)
        {
            FromMobileUnits fmu = new FromMobileUnits(data); //adds the header and footer
            return GetInstance(fmu.Data, false);
        }

        public static IExternalMessage GetInstance(byte[] data, bool toUnits)
        {
            // To Vehicles
            if (toUnits)
            {
                if (NewJobHeader.IsType(data))
                    return new NewJobHeader(data);
                if (AddJobStop.IsType(data))
                    return new AddJobStop(data);
                if (UpdateJobStop.IsType(data))
                    return new UpdateJobStop(data);
                if (DeleteJob.IsType(data))
                    return new DeleteJob(data);
                if (MessageToDriver.IsType(data))
                    return new MessageToDriver(data);
                if (LogonResponse.IsType(data))
                    return new LogonResponse(data);
                if (KMessageType.IsType(data))
                {
                    if (LogonQuestionCheckList.IsListType(data))
                        return new LogonQuestionCheckList(data);
                    if (LogoffQuestionCheckList.IsListType(data))
                        return new LogoffQuestionCheckList(data);
                    if (PreTripQuestionList.IsListType(data))
                        return new PreTripQuestionList(data);
                    if (FuelStationGPSCoordinatesList.IsListType(data))
                        return new FuelStationGPSCoordinatesList(data);
                }
                if (OffRouteAlert.IsType(data))
                    return new OffRouteAlert(data);
            }
            else
            {
                // Incoming to simulator as we have headers now
                if (DriverLogin.IsType(data))
                {
                    DriverLogin oPacket = new DriverLogin(data);
                    return oPacket;
                }
                if (DriverLogout.IsType(data))
                    return new DriverLogout(data);
                if (DriverContactedOps.IsType(data))
                    return new DriverContactedOps(data);
                if (JobRequest.IsType(data))
                    return new JobRequest(data);
                if (JobComplete.IsType(data))
                    return new JobComplete(data);
                if (JobAccept.IsType(data))
                    return new JobAccept(data);
                if (LegComplete.IsType(data))
                    return new LegComplete(data);
                if (DriverLogonQuestionResponseAndDamageReport.IsType(data))
                    return new DriverLogonQuestionResponseAndDamageReport(data);
                if (MessageFromDriver.IsType(data))
                {
                    if (MessageFromServerHasBeenReadByDriver.IsSubType(data))
                        return new MessageFromServerHasBeenReadByDriver(data);
                    if (MessageFromDriver.IsSubType(data))
                        return new MessageFromDriver(data);
                }
                if (FuelFill.IsType(data))
                    return new FuelFill(data);

            }
            throw new Exception("Unknown message type: " + data[cCommonPacketData.COMMAND_TYPE_INDEX].ToString());

        }

        #region Message Classes InBound (From Bunkers)

        #region Stations class
        public class Stations
        {
            public const byte STATION_START_DELIMITER = 0x06;
            public ArrayList StationList = new ArrayList();
            public ArrayList Data = new ArrayList();

            public Stations() { }
            public Stations(ArrayList data)
            {
                Data.Clear();
                Data.AddRange(data);
                Get();
            }

            public void Add(double lat, double lng, int radius)
            {
                this.StationList.Add(new Station(lat, lng, radius));
            }

            public void Get()
            {
                for (int i = 0; i < Data.Count; i++)
                {
                    if (Data[i].Equals(STATION_START_DELIMITER))
                    {
                        ArrayList a = new ArrayList();

                        while (!Data[++i].Equals(STATION_START_DELIMITER) && i < Data.Count - 1)
                            a.Add(Data[i]);
                        StationList.Add(new Station(a));
                        i--;
                    }
                }
            }

            public void Set()
            {
                Data.Clear();
                foreach (Station q in this.StationList)
                {
                    q.Set();
                    Data.Add(STATION_START_DELIMITER);
                    Data.AddRange(q.Data);
                }
            }

            public override string ToString()
            {
                string s = "";
                for (int i = 0; i < this.StationList.Count; i++)
                    s += i + " " + this.StationList[i].ToString();
                return s;
            }

            public class Station
            {
                public double Latitude;
                public double Longitude;
                public int Radius;

                public ArrayList Data = new ArrayList();
                public Station() { }
                public Station(double lat, double lng, int radius)
                {
                    this.Latitude = lat;
                    this.Longitude = lng;
                    this.Radius = radius;
                    Set();
                }
                public Station(ArrayList data)
                {
                    Data.Clear();
                    Data.AddRange(data);
                    Get();
                }

                public void Get()
                {
                    int i = 0;
                    byte[] b = new byte[4];
                    b[0] = (byte)Data[i++];
                    b[1] = (byte)Data[i++];
                    b[2] = (byte)Data[i++];
                    b[3] = (byte)Data[i++];
                    this.Latitude = LatLonConversion.ConvLatLon(b);
                    b[0] = (byte)Data[i++];
                    b[1] = (byte)Data[i++];
                    b[2] = (byte)Data[i++];
                    b[3] = (byte)Data[i++];
                    this.Longitude = LatLonConversion.ConvLatLon(b);
                    this.Radius = Convert.ToInt32(Data[i++]);
                }

                public void Set()
                {
                    Data.Clear();
                    byte[] b = BitConverter.GetBytes(this.Latitude);
                    Data.Add(b[0]);
                    Data.Add(b[1]);
                    Data.Add(b[2]);
                    Data.Add(b[3]);

                    b = BitConverter.GetBytes(this.Longitude);
                    Data.Add(b[0]);
                    Data.Add(b[1]);
                    Data.Add(b[2]);
                    Data.Add(b[3]);

                    Data.Add((byte)this.Radius);
                }

                public override string ToString()
                {
                    return "Latitude = " + this.Latitude + " Longitude = " + this.Longitude + " Radius= " + Radius;
                }

            }
        }
        #endregion Stations class

        #region Questions class
        public class Questions
        {
            public const byte QUESTION_START_DELIMITER = 0x06;
            public const byte QUESTION_DELIMITER = 0x07;
            public ArrayList QuestionList = new ArrayList();
            public ArrayList Data = new ArrayList();

            public Questions() { }
            public Questions(ArrayList data)
            {
                Data.Clear();
                Data.AddRange(data);
                Get();
            }

            public void Add(int number, string text, string response)
            {
                this.QuestionList.Add(new Question(number, text, response));
            }

            public void Get()
            {
                try
                {
                    for (int i = 0; i < Data.Count; i++)
                    {
                        if (Data[i].Equals(QUESTION_START_DELIMITER))
                        {
                            ArrayList a = new ArrayList();
                            while (!Data[i++].Equals(QUESTION_DELIMITER) && i < Data.Count)
                                a.Add(Data[i]);
                            if (Data[i - 1].Equals(QUESTION_DELIMITER))
                                a.Add(Data[i]); //for the response
                            QuestionList.Add(new Question(a));
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    Console.Write(ex.Message);
                }
            }

            public void Set()
            {
                Data.Clear();
                foreach (Question q in QuestionList)
                {
                    q.Set();
                    Data.Add(QUESTION_START_DELIMITER);
                    Data.AddRange(q.Data);
                }
            }

            public override string ToString()
            {
                string s = "";
                for (int i = 0; i < this.QuestionList.Count; i++)
                    s += i + " " + this.QuestionList[i].ToString();
                return s;
            }

            public class Question
            {
                public int Number;
                public string Text;
                public string Response;

                public ArrayList Data = new ArrayList();
                public Question() { }
                public Question(int number, string text, string resp)
                {
                    this.Number = number + 30;
                    this.Text = text;
                    this.Response = resp;
                    Set();
                }
                public Question(ArrayList data)
                {
                    Data.Clear();
                    Data.AddRange(data);
                    Get();
                }
                public void Get()
                {
                    try
                    {
                        int i = 0;
                        this.Number = Convert.ToInt32(Data[i++]);
                        int iPos = Data.IndexOf(QUESTION_DELIMITER) - 1;
                        if (iPos < 0)
                        {
                            Console.WriteLine("Couldn't find question delimiter.");
                        }
                        else
                        {
                            ArrayList a = Data.GetRange(i, iPos);
                            this.Text = System.Text.ASCIIEncoding.ASCII.GetString((byte[])a.ToArray(typeof(byte)));
                            byte[] b = new byte[1];
                            i++; //Question Delimiter
                            b[0] = (byte)Data[i + (a.Count)];
                            this.Response = System.Text.ASCIIEncoding.ASCII.GetString(b);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        Console.WriteLine(ex.Message);
                    }

                }

                public void Set()
                {
                    Data.Clear();
                    Data.Add((byte)Number);
                    Data.AddRange(System.Text.ASCIIEncoding.ASCII.GetBytes(this.Text));
                    Data.Add(QUESTION_DELIMITER);
                    Data.Add((byte)this.Response[0]);
                }

                public override string ToString()
                {
                    return "Number = " + this.Number + " Text = " + Text + " Response = " + Response;
                }

            }
        }
        #endregion Questions class

        #region Trailers class
        public class Trailers
        {
            public const byte TRAILER_LIST_START = 0x05;
            public const byte TRAILER_START = 0x06;
            public const byte TRAILER_LIST_END = 0x08;

            public ArrayList trailerList = new ArrayList();
            // The byte array does NOT hold the start and end delimiting bytes
            public ArrayList Data = new ArrayList();
            private int supposedNumberOfTrailers = -1;
            public int ByteCnt = 0;


            public Trailers()
            {
            }

            public void Get()
            {
                for (int i = 0; i < Data.Count; i++)
                {
                    if (Data[i].Equals(TRAILER_START))
                    {
                        int end = Data.IndexOf(Trailer.TRAILER_ID_DELIMITER, i);
                        end += 3 + Trailer.HEX_CODE_ID_LENGTH;
                        ArrayList a = Data.GetRange(++i, (end - i));
                        trailerList.Add(new Trailer(a));
                        i += a.Count - 1;
                    }

                    if (Data[i].Equals(TRAILER_LIST_END) && trailerList.Count == supposedNumberOfTrailers)
                    {
                        ByteCnt = i;
                        break;
                    }
                }
            }

            public void Set()
            {
                this.Data.Clear();
                foreach (Trailer t in trailerList)
                {
                    this.Data.Add(TRAILER_START);
                    this.Data.AddRange(t.Data);
                }
            }

            public Trailers(ArrayList b, int numberTrailers)
            {
                supposedNumberOfTrailers = numberTrailers;
                Data.Clear();
                Data.AddRange(b);
                Get();
            }

            public void Add(Trailer t)
            {
                trailerList.Add(t);
            }

            public void Add(string id, byte[] hexId, string type, string action)
            {
                trailerList.Add(new Trailer(id, hexId, type, action));
            }

            public int Count()
            {
                return trailerList.Count;
            }

            public override string ToString()
            {
                string s = "";
                foreach (Trailer t in trailerList)
                    s += t.ToString() + " ";
                return s;
            }

            public class Trailer
            {
                public const byte TRAILER_NAME_DELIMITER = 0x0a;
                public const byte TRAILER_ID_DELIMITER = 0x07;
                public const int TRAILER_ID_MAX_LENGTH = 9;
                public const int HEX_CODE_ID_LENGTH = 6;
                public string ID = "";
                public string Type = "";
                public string Action = "";
                public byte[] HexCodeID;

                public ArrayList Data = new ArrayList();
                private ByteStreamReader reader;
                private ByteStreamWriter writer;

                public Trailer(string id, byte[] hexId, string type, string action)
                {
                    this.ID = id;
                    this.HexCodeID = hexId;
                    this.Type = type;
                    this.Action = action;
                    Set();
                }

                public Trailer(ArrayList byts)
                {
                    this.Data.Clear();
                    this.Data.AddRange(byts);
                    Get();
                }

                public void Get()
                {
                    this.reader = new ByteStreamReader((byte[])this.Data.ToArray(typeof(byte)));
                    this.ID = reader.ReadASCIIStringDelimited(TRAILER_ID_MAX_LENGTH, TRAILER_ID_DELIMITER, false);
                    reader.ReadByte();
                    //byte[]  b = reader.ReadBytes(HEX_CODE_ID_LENGTH);
                    this.HexCodeID = reader.ReadBytes(HEX_CODE_ID_LENGTH);
                    //for (int i=0; i<HEX_CODE_ID_LENGTH; i++)
                    //	this.HexCodeID[i] = b[i];
                    this.Type = reader.ReadASCIIStringFixedLength(1, 0x00);
                    this.Action = reader.ReadASCIIStringFixedLength(1, 0x00);
                }

                public void Set()
                {
                    this.writer = new ByteStreamWriter();
                    this.Data.Clear();
                    writer.WriteASCIIStringDelimited(this.ID, TRAILER_ID_MAX_LENGTH, TRAILER_ID_DELIMITER);

                    for (int i = 0; i < HEX_CODE_ID_LENGTH; i++)
                        writer.WriteByte((byte)this.HexCodeID[i]);

                    if (this.Type.Length == 0)
                        writer.WriteByte((byte)0x00);
                    else
                        writer.WriteByte((byte)this.Type[0]);

                    if (this.Action.Length == 0)
                        writer.WriteByte((byte)0x00);
                    else
                        writer.WriteByte((byte)this.Action[0]);
                    this.Data.AddRange(writer.ToByteArray());
                }

                public override string ToString()
                {
                    return "TrailerID = " + ID + " TrailerType = " + Type + " TrailerAction = " + Action;
                }
            }
        }
        #endregion Trailers class

        // Incoming from bunkers

        #region NewJobHeader
        public class NewJobHeader : cCommonPacketData
        {
            public int LegCount = -1;
            public string JobTitle = "";

            public const string Type = "J";
            public static int LEG_COUNT_INDEX = (cCommonPacketData.JOB_ID_INDEX + cCommonPacketData.JOB_ID_LENGTH);
            public static int LEG_COUNT_LENGTH = 1;
            public static int TYPE_OF_JOB_INDEX = (LEG_COUNT_INDEX + LEG_COUNT_LENGTH);
            public static int TYPE_OF_JOB_LENGTH = 1;
            public static int JOB_TITLE_INDEX = (TYPE_OF_JOB_INDEX + TYPE_OF_JOB_LENGTH);
            public static int JOB_TITLE_LENGTH_MAX = 100;
            public static byte JOB_TITLE_DELIMITER = (byte)0x0E;

            public NewJobHeader()
                : base(null)
            {
                SetType(Type);
            }

            public NewJobHeader(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
            }

            public new void Get()
            {
                base.Get();

                //Leg Count
                LegCount = Convert.ToInt32((byte)this.data[LEG_COUNT_INDEX]);

                // Type of job for future use
                // JobTitle
                int cnt = data.IndexOf(JOB_TITLE_DELIMITER, JOB_TITLE_INDEX);
                ArrayList dat = this.data.GetRange(JOB_TITLE_INDEX, cnt - JOB_TITLE_INDEX);
                this.JobTitle = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));

            }

            public new void Set()
            {
                base.Set();

                //Leg Count
                data[LEG_COUNT_INDEX] = (byte)LegCount;

                // Type of job
                data.Add((byte)0x00);

                //JobTitle
                data.AddRange(System.Text.ASCIIEncoding.ASCII.GetBytes(JobTitle));
                data.Add(JOB_TITLE_DELIMITER);
                base.Finalise();
            }

            public static bool IsType(byte[] data)
            {
                byte[] b = new byte[1];
                b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(NewJobHeader.Type);
            }

            public new void Process()
            {
                Console.WriteLine("Processing NewJobHeader message");
                this.Get();
            }

            public override string ToString()
            {
                this.Get();
                string ret = "UnitID = " + UnitID + " JobID = " + JobID;
                ret += " LegCount = " + LegCount.ToString();
                ret += " JobTitle = " + JobTitle;
                return ret;
            }
        }
        #endregion NewJobHeader

        #region AddJobStop
        public class AddJobStop : cCommonPacketData
        {
            public int LegNumber = -1;
            public string LegType = "";
            public double JobLegLatitude = -1;
            public double JobLegLongitude = -1;
            public int NumberOfTrailers = -1;
            public Trailers Trailers = null;
            public DateTime TimeToStart;
            public bool DangerousGoods = false;
            public string LegTitle = "";
            public string CustomerLocation = "";
            public string CompanyName = "";
            public string CompanyAddress = "";
            public string DestinationDetails = "";
            public string ExtraInformation = "";
            public int StateOfJobLeg = -1;

            public const string Type = "A";
            public const byte LEG_TITLE_DELIMITER = 0x0f;

            public static int LEG_NUMBER_INDEX = (cCommonPacketData.JOB_ID_INDEX + cCommonPacketData.JOB_ID_LENGTH);
            public static int LEG_NUMBER_LENGTH = 1;
            public static int LEG_TYPE_INDEX = (LEG_NUMBER_INDEX + LEG_NUMBER_LENGTH);
            public static int LEG_TYPE_LENGTH = 1;
            public static int JOB_LEG_LATITUDE_INDEX = (LEG_TYPE_INDEX + LEG_TYPE_LENGTH);
            public static int JOB_LEG_LATITUDE_LENGTH = 4;
            public static int JOB_LEG_LONGITUDE_INDEX = (JOB_LEG_LATITUDE_INDEX + JOB_LEG_LATITUDE_LENGTH);
            public static int JOB_LEG_LONGITUDE_LENGTH = 4;
            public static int NUMBER_OF_TRAILERS_INDEX = (JOB_LEG_LONGITUDE_INDEX + JOB_LEG_LONGITUDE_LENGTH);
            public static int NUMBER_OF_TRAILERS_LENGTH = 1;
            public static int TRAILER_LIST_START_INDEX = (NUMBER_OF_TRAILERS_INDEX + NUMBER_OF_TRAILERS_LENGTH);

            public static int TIME_TO_START_LENGTH = 5;

            public AddJobStop()
                : base(null)
            {
                SetType(Type);
            }

            public AddJobStop(byte[] data)
            {
                this.data.Clear();
                this.data.AddRange(data);
                Get();
            }

            protected new void Get()
            {
                Get(AddJobStop.LEG_NUMBER_INDEX, AddJobStop.LEG_TYPE_INDEX, AddJobStop.JOB_LEG_LATITUDE_INDEX,
                    AddJobStop.JOB_LEG_LONGITUDE_INDEX, AddJobStop.NUMBER_OF_TRAILERS_INDEX,
                    AddJobStop.TRAILER_LIST_START_INDEX);
            }

            public void Get(int legNumberIndex, int legTypeIndex, int jobLegLatitudeIndex,
                int jobLegLongitudeIndex, int numberOfTrailersIndex, int trailerListStartIndex)
            {
                base.Get();
                reader = new ByteStreamReader((byte[])this.data.ToArray(typeof(byte)));
                reader.MoveBytes(legNumberIndex);
                // Leg Number
                LegNumber = Convert.ToInt32(reader.ReadByte());//(byte)this.data[legNumberIndex]);

                // Leg Type
                byte[] v = new byte[1];
                v[0] = (byte)this.data[legTypeIndex];
                LegType = reader.ReadASCIIStringFixedLength(1, 0x00);//System.Text.ASCIIEncoding.ASCII.GetString(v);

                //ArrayList a = this.data.GetRange (jobLegLatitudeIndex, JOB_LEG_LATITUDE_LENGTH);


                // JobLegLatitude				
                JobLegLatitude = GPSUtilities.ConvertToDouble(reader.Read4ByteUnsignedInteger());
                // JobLegLongitude
                JobLegLongitude = GPSUtilities.ConvertToDouble(reader.Read4ByteUnsignedInteger());

                // NumberOfTrailers
                NumberOfTrailers = reader.ReadByte(); //Convert.ToInt32 ((byte)this.data[numberOfTrailersIndex]);

                int index = trailerListStartIndex;
                // Trailers
                ArrayList dat = this.data.GetRange(index, (this.data.Count - index - 1));
                Trailers = new Trailers(dat, NumberOfTrailers);
                index += Trailers.ByteCnt + 1;

                // TimeToStart
                byte[] msg = (byte[])this.data.GetRange(index, TIME_TO_START_LENGTH).ToArray(typeof(byte));
                //this.TimeToStart = GetYYMMDDHHMM(msg);				
                int year = msg[0];
                int month = msg[1];
                int day = msg[2];
                int hour = msg[3];
                int min = msg[4];
                this.TimeToStart = new DateTime(year, month, day, hour, min, DateTime.MinValue.Second);
                index = (index + TIME_TO_START_LENGTH);

                // DangerousGoods
                v = new byte[1];
                v[0] = (byte)this.data[index];
                DangerousGoods = System.Text.ASCIIEncoding.ASCII.GetString(v).Equals("Y");

                // Spare Flags
                index++;
                index++;

                // Leg Title
                index++;
                int cnt = data.IndexOf(LEG_TITLE_DELIMITER, index);
                dat = this.data.GetRange(index, cnt - index);
                LegTitle = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));
                index += dat.Count + 1;

                // Customer Location
                cnt = data.IndexOf(LEG_TITLE_DELIMITER, index);
                dat = this.data.GetRange(index, cnt - index);
                CustomerLocation = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));
                index += dat.Count + 1;

                // Company Name
                cnt = data.IndexOf(LEG_TITLE_DELIMITER, index);
                dat = this.data.GetRange(index, cnt - index);
                this.CompanyName = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));
                index += dat.Count + 1;

                // Company Address
                cnt = data.IndexOf(LEG_TITLE_DELIMITER, index);
                dat = this.data.GetRange(index, cnt - index);
                this.CompanyAddress = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));
                index += dat.Count + 1;

                // DestinationDetails
                cnt = data.IndexOf(LEG_TITLE_DELIMITER, index);
                dat = this.data.GetRange(index, cnt - index);
                this.DestinationDetails = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));
                index += dat.Count + 1;

                // ExtraInformation
                cnt = data.IndexOf(LEG_TITLE_DELIMITER, index);
                dat = this.data.GetRange(index, cnt - index);
                this.ExtraInformation = System.Text.ASCIIEncoding.ASCII.GetString((byte[])dat.ToArray(typeof(byte)));
                index += dat.Count + 1;

                // State Of Job Leg
                dat = this.data.GetRange(index, 1);
                StateOfJobLeg = Convert.ToInt32(dat[0]);

            }

            public void SetData()
            {
                //this.data = new ArrayList();
                writer = new ByteStreamWriter();
                base.Set();
                writer = new ByteStreamWriter();
                data.RemoveAt(data.Count - 1);
                writer.WriteByteArray((byte[])data.ToArray(typeof(byte)));

                PadToLength(TRAILER_LIST_START_INDEX);
                // leg Number
                writer.WriteByte((byte)LegNumber);
                //this.data[LEG_NUMBER_INDEX] = (byte)LegNumber;

                // Leg Type
                if (LegType != null && (LegType.ToUpper().Equals("P") || LegType.ToUpper().Equals("D")))
                    //this.data [LEG_TYPE_INDEX] = (byte)LegType[0];
                    writer.WriteByte((byte)LegType[0]);
                else
                    throw new Exception("Invalid LegType, Allowed values include 'P' or 'D'");

                // JobLegLatitude
                writer.Write4ByteUnsignedInteger(GPSUtilities.ConvertToUInt32(JobLegLatitude));

                // JobLegLongitude
                writer.Write4ByteUnsignedInteger(GPSUtilities.ConvertToUInt32(JobLegLongitude));
                this.data = new ArrayList(writer.ToByteArray());


                // NumberOfTrailers
                //this.data.Add((byte)NumberOfTrailers);

                // Trailers
                //PadToLength(TRAILER_LIST_START_INDEX);
                this.data.Add((byte)Trailers.Count());  //NumberOfTrailers
                this.data.Add(Trailers.TRAILER_LIST_START);
                foreach (byte b in Trailers.Data)
                    this.data.Add(b);
                this.data.Add(Trailers.TRAILER_LIST_END);

                // TimeToStart
                byte[] year = BitConverter.GetBytes((ushort)TimeToStart.Year);
                byte[] month = BitConverter.GetBytes((ushort)TimeToStart.Month);
                byte[] day = BitConverter.GetBytes((ushort)TimeToStart.Day);
                byte[] hour = BitConverter.GetBytes((ushort)TimeToStart.Hour);
                byte[] min = BitConverter.GetBytes((ushort)TimeToStart.Minute);
                this.data.Add(year[1]);
                this.data.Add(month[0]);
                this.data.Add(day[0]);
                this.data.Add(hour[0]);
                this.data.Add(min[0]);

                // DangerousGoods
                this.data.Add((byte)GetYN(DangerousGoods)[0]);

                // Spare Flags
                this.data.Add((byte)0x00);
                this.data.Add((byte)0x00);

                // LegTitle
                byte[] msg = System.Text.ASCIIEncoding.ASCII.GetBytes(LegTitle);
                for (int i = 0; i < msg.Length; i++)
                    this.data.Add((byte)msg[i]);
                this.data.Add(LEG_TITLE_DELIMITER);

                // Customer Location
                msg = System.Text.ASCIIEncoding.ASCII.GetBytes(CustomerLocation);
                for (int i = 0; i < msg.Length; i++)
                    this.data.Add((byte)msg[i]);
                this.data.Add(LEG_TITLE_DELIMITER);

                // Company Name
                msg = System.Text.ASCIIEncoding.ASCII.GetBytes(CompanyName);
                for (int i = 0; i < msg.Length; i++)
                    this.data.Add((byte)msg[i]);
                this.data.Add(LEG_TITLE_DELIMITER);

                // Company Address
                msg = System.Text.ASCIIEncoding.ASCII.GetBytes(this.CompanyAddress);
                this.data.AddRange((byte[])msg);
                this.data.Add(LEG_TITLE_DELIMITER);

                // DestinationDetails
                msg = System.Text.ASCIIEncoding.ASCII.GetBytes(this.DestinationDetails);
                for (int i = 0; i < msg.Length; i++)
                    this.data.Add((byte)msg[i]);
                this.data.Add(LEG_TITLE_DELIMITER);

                // ExtraInformation
                msg = System.Text.ASCIIEncoding.ASCII.GetBytes(this.ExtraInformation);
                for (int i = 0; i < msg.Length; i++)
                    this.data.Add((byte)msg[i]);
                this.data.Add(LEG_TITLE_DELIMITER);

                // State of Job Leg
                this.data.Add((byte)this.StateOfJobLeg);
            }

            public new void Set()
            {
                SetData();
                base.Finalise();
            }


            public override string ToString()
            {
                this.Get();
                string ret = base.ToString() + " LegNumber = " + LegNumber + " LegType = " + LegType + " JobLegLatitude = " + JobLegLatitude + " JobLegLongitude = " + JobLegLongitude + " NumberOfTrailers = " + NumberOfTrailers;
                ret += Trailers.ToString();
                ret += " TimeToStart = " + this.TimeToStart.ToShortTimeString();
                ret += " DangerousGoods = " + this.DangerousGoods;
                ret += " LegTitle = " + LegTitle;
                ret += " CustomerLocation = " + CustomerLocation;
                ret += " CompanyName = " + this.CompanyName;
                ret += " CompanyAddress = " + this.CompanyAddress;
                ret += " DestinationDetails = " + this.DestinationDetails;
                ret += " ExtraInformation = " + this.ExtraInformation;
                ret += " StateOfJobLeg = " + this.StateOfJobLeg;
                return ret;
            }

            public static bool IsType(byte[] data)
            {
                byte[] b = new byte[1];
                b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(AddJobStop.Type);
            }

            public override byte[] Process()
            {
                // Do db lookup of the trailer name-id

                return (byte[])this.data.ToArray(typeof(byte));
            }
        }
        #endregion AddJobStop

        #region UpdateJobStop
        public class UpdateJobStop : AddJobStop
        {
            public new const string Type = "U";
            public static int STATE_JOB_LEG_INDEX = (cCommonPacketData.JOB_ID_INDEX + cCommonPacketData.JOB_ID_LENGTH);
            public static int STATE_JOB_LEG_LENGTH = 1;

            private void CommonConstructor()
            {
                LEG_NUMBER_INDEX = (STATE_JOB_LEG_INDEX + STATE_JOB_LEG_LENGTH);
                LEG_TYPE_INDEX = (LEG_NUMBER_INDEX + LEG_NUMBER_LENGTH);
                JOB_LEG_LATITUDE_INDEX = (LEG_TYPE_INDEX + LEG_TYPE_LENGTH);
                JOB_LEG_LONGITUDE_INDEX = (JOB_LEG_LATITUDE_INDEX + JOB_LEG_LATITUDE_LENGTH);
                NUMBER_OF_TRAILERS_INDEX = (JOB_LEG_LONGITUDE_INDEX + JOB_LEG_LONGITUDE_LENGTH);
                TRAILER_LIST_START_INDEX = (NUMBER_OF_TRAILERS_INDEX + NUMBER_OF_TRAILERS_LENGTH);
            }

            public UpdateJobStop()
            {
                SetType(Type);
                CommonConstructor();
            }

            public UpdateJobStop(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
                CommonConstructor();
            }

            public new void Get()
            {
                base.Get();
            }

            public new void Set()
            {
                base.Set();
            }

            public override string ToString()
            {
                Get();
                return base.ToString();
            }

            public new static bool IsType(byte[] data)
            {
                byte[] b = new byte[1];
                b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(UpdateJobStop.Type);
            }
        }
        #endregion UpdateJobStop

        #region DeleteJob
        public class DeleteJob : cCommonPacketData
        {
            public const string Type = "D";

            public DeleteJob()
            {
                SetType(Type);
            }

            public DeleteJob(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
            }

            public new void Get()
            {
                base.Get();
            }

            public new void Set()
            {
                base.Set();
                base.Finalise();
            }

            public override string ToString()
            {
                Get();
                return base.ToString();
            }

            public static bool IsType(byte[] data)
            {
                byte[] b = new byte[1];
                b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(DeleteJob.Type);
            }
        }
        #endregion DeleteJob

        #region MessageToDriver
        public class MessageToDriver : cCommonPacketData
        {
            public const byte MESSAGE_DELIMITER = 0x0f;

            public const string Type = "M";
            public string Message = "";

            public MessageToDriver()
            {
                SetType(Type);
            }

            public MessageToDriver(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
            }

            // Reuse the job id 4 bytes just call it message id here.
            public int MessageID
            {
                get { return base.JobID; }
                set { base.JobID = value; }
            }

            public new void Get()
            {
                int msgIndex = base.GetUnitID();
                this.MessageID = reader.Read4ByteInteger();
                this.Message = reader.ReadASCIIStringDelimited(9999, MESSAGE_DELIMITER);
            }

            public new void Set()
            {
                base.SetUnitID();
                this.writer.Write4ByteInteger(this.MessageID);
                this.writer.WriteASCIIStringDelimited(this.Message.Trim(), 128, MESSAGE_DELIMITER);
                this.data = new ArrayList(writer.ToByteArray());
                base.Finalise();
            }

            public override string ToString()
            {
                Get();
                return base.ToString() + " Message To Driver is: " + this.Message;
            }
            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }
            public static bool IsType(byte[] data)
            {
                byte[] b = new byte[1];
                b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(MessageToDriver.Type);
            }
        }
        #endregion MessageToDriver

        #region LogonResponse
        public class LogonResponse : cCommonPacketData
        {
            public const byte DRIVER_NAME_DELIMITER = 0x0a;
            public const string Type = "L";

            public string LogonResult = "";
            public bool DangerousGoodsAllowed = false;
            public string LocalDriver = "";
            public string DriverName = "";
            public bool DaylightSavings = false;
            public byte GMTOffset = 10;
            public int RequiredLoginNumberOfQuestions = -1;
            public int QuestionNumber = -1;


            public LogonResponse()
            {
                SetType(Type);
            }

            public LogonResponse(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
            }

            public new void Get()
            {
                int msgIndex = base.GetUnitID();
                byte[] aByte = new byte[1];
                aByte[0] = (byte)this.data[msgIndex++];
                LogonResult = System.Text.ASCIIEncoding.ASCII.GetString(aByte);
                aByte[0] = (byte)this.data[msgIndex++];

                DangerousGoodsAllowed = (System.Text.ASCIIEncoding.ASCII.GetString(aByte).ToUpper().Trim().Equals("Y"));

                aByte[0] = (byte)this.data[msgIndex++];
                this.LocalDriver = System.Text.ASCIIEncoding.ASCII.GetString(aByte);
                int endIndex = this.data.IndexOf(DRIVER_NAME_DELIMITER, msgIndex);
                ArrayList a = data.GetRange(msgIndex, endIndex - msgIndex);
                this.DriverName = System.Text.ASCIIEncoding.ASCII.GetString((byte[])a.ToArray(typeof(byte)));
                msgIndex = endIndex + 1;
                this.DaylightSavings = ((byte)this.data[msgIndex++]).Equals((byte)'Y');
                this.GMTOffset = (byte)this.data[msgIndex++];
                this.RequiredLoginNumberOfQuestions = Convert.ToInt32(this.data[msgIndex++]);
                this.QuestionNumber = Convert.ToInt32(this.data[msgIndex++]);

            }

            public new void Set()
            {
                base.SetUnitID();
                this.data.Add((byte)this.LogonResult[0]);
                if (DangerousGoodsAllowed)
                    this.data.Add((byte)"Y"[0]);
                else
                    this.data.Add((byte)"N"[0]);
                this.data.Add((byte)this.LocalDriver[0]);
                this.data.AddRange(System.Text.ASCIIEncoding.ASCII.GetBytes(this.DriverName));
                this.data.Add(DRIVER_NAME_DELIMITER);
                if (this.DaylightSavings)
                    this.data.Add((byte)'Y');
                else
                    this.data.Add((byte)'N');
                this.data.Add(this.GMTOffset);
                this.data.Add((byte)this.RequiredLoginNumberOfQuestions);
                this.data.Add((byte)this.QuestionNumber);
                base.Finalise();
            }

            public override string ToString()
            {
                Get();
                return base.ToString() + " LogonResult = " + this.LogonResult + " DangerousGoodsAllowed = " + DangerousGoodsAllowed + " LocalDriver = " + LocalDriver + " DriverName = " + DriverName + " RequiredLoginNumberOfQuestions = " + RequiredLoginNumberOfQuestions + " QuestionNumber = " + QuestionNumber;
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, LogonResponse.Type);
            }
        }
        #endregion LogonResponse

        #region OffRouteAlert
        public class OffRouteAlert : cCommonPacketData
        {
            public const string Type = "O";
            public OffRouteAlert()
            {
                SetType(Type);
            }

            public OffRouteAlert(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
                Get();
            }

            public new void Get()
            {
                base.GetUnitID();
                // next 2 bytes are spare flags do nothing for now.
            }

            public new void Set()
            {
                base.SetUnitID();
                this.data.Add((byte)0x00); //spare flags
                this.data.Add((byte)0x00); //spare flags
                base.Finalise();
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, OffRouteAlert.Type);
            }

            public override string ToString()
            {
                return "Command = " + OffRouteAlert.Type + base.ToString();
            }

        }
        #endregion OffRouteAlert

        #region KMessageType
        public class KMessageType : cCommonPacketData
        {
            public const string Type = "K";
            public static int LIST_TYPE_INDEX = cCommonPacketData.UNIT_ID_INDEX + cCommonPacketData.UNIT_ID_LENGTH;
            public const byte LIST_START_DELIMITER = 0x05;
            public const byte LIST_END_DELIMITER = 0x08;

            public KMessageType() { }
            public KMessageType(byte[] data)
                : base(data)
            {
                this.data.Clear();
                this.data.AddRange(data);
            }

            public static bool IsType(byte[] data)
            {
                byte[] b = new byte[1];
                b[0] = data[cCommonPacketData.COMMAND_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(KMessageType.Type);
            }

            public static bool IsListType(byte[] data, string type)
            {
                byte[] b = new byte[1];
                b[0] = data[LIST_TYPE_INDEX];
                return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals(type);
            }
        }
        #endregion KMessageType

        #region LogonQuestionCheckList
        public class LogonQuestionCheckList : KMessageType
        {
            protected const string ListType = "l";

            public int LogonCheckListID = -1;
            public Questions Questions = null;

            public LogonQuestionCheckList()
            {
                base.SetType(Type);
            }

            public LogonQuestionCheckList(byte[] data)
                : base(data)
            {
                //				this.data.Clear ();
                //				this.data.AddRange (data);
            }

            public new void Get()
            {
                int msgIndex = base.GetUnitID();
                byte[] aByte = new byte[1];
                aByte[0] = (byte)this.data[msgIndex++];
                this.LogonCheckListID = Convert.ToInt32(this.data[msgIndex++]);
                int noQuestions = Convert.ToInt32(this.data[msgIndex++]);
                Questions = new Questions(this.data.GetRange(msgIndex, (this.data.Count - msgIndex)));
                msgIndex += Questions.Data.Count;
            }

            public new void Set()
            {
                base.SetUnitID();
                this.data.Add((byte)ListType[0]);
                this.data.Add((byte)this.LogonCheckListID);
                this.data.Add((byte)this.Questions.QuestionList.Count);
                this.Questions.Set();
                this.data.Add(LIST_START_DELIMITER);
                this.data.AddRange(this.Questions.Data);
                this.data.Add(LIST_END_DELIMITER);
                base.Finalise();
            }

            public override string ToString()
            {
                Get();
                return base.ToString() + " ListType = " + ListType +
                    " LogonCheckListID = " + LogonCheckListID + " NumberOfQuestions = " + Questions.QuestionList.Count +
                    "Questions = " + this.Questions.ToString();
            }

            public static bool IsListType(byte[] data)
            {
                return IsListType(data, LogonQuestionCheckList.ListType);
            }
        }
        #endregion LogonQuestionCheckList

        #region LogoffQuestionCheckList
        public class LogoffQuestionCheckList : KMessageType
        {
            protected const string ListType = "o";

            public int LogoffCheckListID = -1;
            public Questions Questions = null;

            public LogoffQuestionCheckList()
            {
                base.SetType(Type);
            }

            public LogoffQuestionCheckList(byte[] data)
                : base(data)
            {
                //				this.data.Clear ();
                //				this.data.AddRange (data);
            }

            public new void Get()
            {
                int msgIndex = base.GetUnitID();
                byte[] aByte = new byte[1];
                aByte[0] = (byte)this.data[msgIndex++];
                this.LogoffCheckListID = Convert.ToInt32(this.data[msgIndex++]);
                int noQuestions = Convert.ToInt32(this.data[msgIndex++]);
                Questions = new Questions(this.data.GetRange(msgIndex, (this.data.Count - msgIndex)));
                msgIndex += Questions.Data.Count;
            }

            public new void Set()
            {
                base.SetUnitID();
                this.data.Add((byte)ListType[0]);
                this.data.Add((byte)this.LogoffCheckListID);
                this.data.Add((byte)this.Questions.QuestionList.Count);
                this.Questions.Set();
                this.data.Add(LIST_START_DELIMITER);
                this.data.AddRange(this.Questions.Data);
                this.data.Add(LIST_END_DELIMITER);
                base.Finalise();
            }

            public override string ToString()
            {
                Get();
                return base.ToString() + " ListType = " + ListType +
                    " LogoffCheckListID = " + LogoffCheckListID + " NumberOfQuestions = " + Questions.QuestionList.Count +
                    "Questions = " + this.Questions.ToString();
            }

            public static bool IsListType(byte[] data)
            {
                return IsListType(data, LogoffQuestionCheckList.ListType);
            }



        }
        #endregion LogoffQuestionCheckList

        #region PreTripQuestionList
        public class PreTripQuestionList : LogonQuestionCheckList
        {
            public new const string ListType = "p";
            public PreTripQuestionList()
                : base()
            {
            }

            public PreTripQuestionList(byte[] b) : base(b) { }

            // just rename same byte, same position same range of values
            public int PreTripCheckListID
            {
                get
                {
                    return this.LogonCheckListID;
                }
                set
                {
                    this.LogonCheckListID = value;
                }
            }

            public new void Get()
            {
                base.Get();
            }

            public new void Set()
            {
                base.Set();
                this.data[KMessageType.LIST_TYPE_INDEX] = (byte)ListType[0];
            }

            public override string ToString()
            {
                Get();
                return base.ToString().Replace("ListType = " + LogonQuestionCheckList.ListType, "ListType = " + ListType);
            }

            public new static bool IsListType(byte[] data)
            {
                return IsListType(data, PreTripQuestionList.ListType);
            }

        }

        #endregion PreTripQuestionList

        #region FuelStationGPSCoordinatesList
        public class FuelStationGPSCoordinatesList : KMessageType
        {
            public const string ListType = "f";

            public int StationListID = -1;
            public Stations Stations = null;

            public FuelStationGPSCoordinatesList()
            {
                base.SetType(Type);
                this.Stations = new Stations();
            }
            public FuelStationGPSCoordinatesList(byte[] b)
                : base(b)
            {
            }

            public new void Get()
            {
                int msgIndex = base.GetUnitID();
                byte[] aByte = new byte[1];
                aByte[0] = (byte)this.data[msgIndex++]; // List type
                this.StationListID = Convert.ToInt32(this.data[msgIndex++]);
                int numStations = Convert.ToInt32(this.data[msgIndex++]);
                Stations = new Stations(this.data.GetRange(msgIndex, (this.data.Count - msgIndex)));
                msgIndex += Stations.Data.Count;
            }

            public new void Set()
            {
                base.SetUnitID();
                this.data.Add((byte)ListType[0]);
                this.data.Add((byte)this.StationListID);
                this.data.Add((byte)this.Stations.StationList.Count); // num stations
                this.Stations.Set();
                this.data.Add(LIST_START_DELIMITER);
                this.data.AddRange(this.Stations.Data);
                this.data.Add(LIST_END_DELIMITER);
                base.Finalise();
            }

            public static bool IsListType(byte[] data)
            {
                return IsListType(data, FuelStationGPSCoordinatesList.ListType);
            }

            public override string ToString()
            {
                Get();
                return "Command = " + FuelStationGPSCoordinatesList.Type + " UnitID = " + this.UnitID +
                    " ListType = " + FuelStationGPSCoordinatesList.ListType +
                    " StationListID = " + this.StationListID + " No Stations = " + this.Stations.StationList.Count +
                    this.Stations.ToString();
            }


        }
        #endregion PreTripQuestionList



        // Outgoing to bunkers

        #region FromMobileUnits
        public class FromMobileUnits : cCommonPacketData
        {
            public FromMobileUnits() : base() { }
            public FromMobileUnits(byte[] data)
            {
                // Packets from mobile units dont add the headers and footers so we put them in

                // Header
                // Add 2 bytes for length

                this.data.Clear();
                this.data.AddRange(data);
                if (!this.ContainsHeaderAndFooter())
                {
                    this.data.Insert(0, 0x00);
                    this.data.Insert(0, 0x00);
                    this.data.Insert(0, cCommonPacketData.StartOfPacketIndicator);
                    this.Finalise();
                }
            }

            //			public static bool IsType(byte[] data, string type, int index)
            //			{
            //				byte[] b = new byte [1];
            //				// exclude header and length since we dont get them			
            //				
            //				b[0] = data[index];	 
            //				return System.Text.ASCIIEncoding.ASCII.GetString(b).Equals (type); 
            //			}

            public new static bool IsType(byte[] data, string type)
            {
                return IsType(data, type, cCommonPacketData.COMMAND_TYPE_INDEX);
            }
        }
        #endregion FromMobileUnits

        #region Driver
        public class Driver : FromMobileUnits
        {
            public const int DRIVER_ID_MAX_LENGTH = 6;
            public static byte DRIVER_DELIMITER = (byte)0x05;
            public new const int COMMAND_TYPE_INDEX = 0;

            public int VehicleUnitID = -1;
            public int VehicleFleetID = -1;
            public cGPSData GPSData = new cGPSData();
            public string DriverID = "";
            public int GPS_DeviceTime = -1;

            public Driver(byte[] data) : base(data) { }
            public Driver()
                : base()
            {
                //				this.data.Clear ();
                //				this.data.Add(cCommonPacketData.StartOfPacketIndicator);
                //				this.data.Add ((byte)0x00);
                //				this.data.Add ((byte)0x00);
                //
                //				this.data.Add (cCommonPacketData.SpareBytesSeparator);
                //				this.data.Add (cCommonPacketData.EndOfPacket);
            }

            public new void Get()
            {
                reader = new ByteStreamReader((byte[])this.data.ToArray(typeof(byte)));
                int i = base.GetUnitID();
                //reader.MoveBytes (i);
                this.VehicleUnitID = reader.Read2ByteInteger();
                this.VehicleFleetID = reader.ReadByte();
                this.GPSData = new cGPSData(reader.ReadBytes(GPS_DATA_LENGTH));
                this.GPS_DeviceTime = reader.Read2ByteInteger();
                this.DriverID = reader.ReadASCIIStringDelimited(DRIVER_ID_MAX_LENGTH, Driver.DRIVER_DELIMITER);
            }

            public new void Set()
            {
                base.SetUnitID();
                writer = new ByteStreamWriter();
                writer.WriteByteArray((byte[])this.data.ToArray(typeof(byte)));
                writer.Write2ByteInteger(this.VehicleUnitID);
                writer.WriteByte((byte)this.VehicleFleetID);
                ArrayList a = new ArrayList(18);
                for (int i = 0; i < GPS_DATA_LENGTH; i++)
                    a.Add((byte)0x00);
                writer.WriteByteArray((byte[])a.ToArray(typeof(byte)));
                writer.Write2ByteInteger(GPS_DeviceTime);
                writer.WriteASCIIStringDelimited(this.DriverID, DRIVER_ID_MAX_LENGTH, DRIVER_DELIMITER);
                this.data = new ArrayList(writer.ToByteArray());
            }
        }
        #endregion Driver

        #region DriverLogin
        public class DriverLogin : Driver
        {
            public const string Type = "I";
            public const int PASSWORD_LENGTH = 4;

            public string Password = "";
            public int PreTripQuestionListID = -1;
            public int QuestionLogonListID = -1;
            public int QuestionLogoffListID = -1;
            public int FuelStationListID = -1;
            public int FirmwareVersion = -1;

            public DriverLogin()
            {
                base.SetType(Type);
            }
            public DriverLogin(byte[] data)
                : base(data)
            {
                Get();
            }

            public new void Get()
            {
                base.Get();
                this.Password = reader.ReadASCIIStringFixedLength(PASSWORD_LENGTH, 0x00);
                this.PreTripQuestionListID = reader.ReadByte();
                this.QuestionLogonListID = reader.ReadByte();
                this.QuestionLogoffListID = reader.ReadByte();
                this.FuelStationListID = reader.ReadByte();
                this.FirmwareVersion = reader.Read2ByteInteger();


            }

            public new void Set()
            {
                base.Set();
                writer.WriteASCIIStringFixedLength(this.Password, PASSWORD_LENGTH, 0x00);
                writer.WriteByte((byte)this.PreTripQuestionListID);
                writer.WriteByte((byte)this.QuestionLogonListID);
                writer.WriteByte((byte)this.QuestionLogoffListID);
                writer.WriteByte((byte)this.FuelStationListID);
                writer.Write2ByteInteger(this.FirmwareVersion);
                this.data = new ArrayList(writer.ToByteArray());
                this.Finalise();
            }

            public override byte[] Process()
            {
                // send on to bunkers
                return (byte[])this.data.ToArray(typeof(byte));
            }

            public override string ToString()
            {
                Get();
                return base.ToString() + "Password = " + Password + " PreTripQuestionListID = " +
                    PreTripQuestionListID + " QuestionLogonListID = " +
                    QuestionLogonListID + " QuestionLogoffListID = " +
                    QuestionLogoffListID + "FuelStationListID = " + FuelStationListID +
                    " FirmwareVersion = " + FirmwareVersion;
            }

            public static bool IsType(byte[] data)
            {
                return Driver.IsType(data, Type);
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }
        }
        #endregion DriverLogin

        #region DriverLogout
        public class DriverLogout : Driver
        {
            public const byte ANSWERS_DELIMITER_START = 0x06;
            public const byte RESPONSE_DELIMITER = 0x07;
            public const byte ANSWERS_DELIMITER_END = 0x08;
            public const int MAX_RESPONSE_SIZE = 100;

            public string LegType = "";
            public ArrayList Responses = new ArrayList();

            public const string Type = "O";
            public DriverLogout() : base() { SetType(Type); }
            public DriverLogout(byte[] data)
                : base(data)
            {
                Get();
            }
            public new void Get()
            {
                base.Get();

                if (reader.ReadByte().Equals(ANSWERS_DELIMITER_START))
                {
                    int numberOfQuestions = (int)reader.ReadByte();
                    if (numberOfQuestions == 0)
                    {
                        int b = reader.ReadByte(); // should match ANSWERS_DELIMITER_END
                        if (b == ANSWERS_DELIMITER_END)
                            return;
                        throw new Exception("Missing ANSWERS_DELIMITER_END " + ANSWERS_DELIMITER_END);
                    }

                    for (int i = 0; i < numberOfQuestions; i++)
                    {
                        int n = reader.ReadByte(); // Question Number;
                        if (n == ANSWERS_DELIMITER_END)
                            break;

                        // add byte for length
                        string r = reader.ReadASCIIStringDelimited(MAX_RESPONSE_SIZE, RESPONSE_DELIMITER, false);

                        this.Responses.Add(new Response(n, r));
                        n = reader.ReadByte();
                        if (n != RESPONSE_DELIMITER)
                            break;
                    }

                    #region Send Logoff message to the listener.
                    //send off login message to listener so it's logged in the tracking client
                    string sDisplayName = "";
                    string sListenerMsg = "";
                    sDisplayName = sDisplayName.Replace(",", "||");
                    sListenerMsg = "L,[FleetID],[VehicleID],[DriverID],[DriverName],[Lat],[Long],[Day],[Month],[Year],[Hour],[Minute],[Second]";
                    sListenerMsg = sListenerMsg.Replace("[FleetID]", Convert.ToString(this.VehicleFleetID));
                    sListenerMsg = sListenerMsg.Replace("[VehicleID]", Convert.ToString(this.VehicleUnitID));
                    sListenerMsg = sListenerMsg.Replace("[DriverID]", "");
                    sListenerMsg = sListenerMsg.Replace("[DriverName]", "");
                    sListenerMsg = sListenerMsg.Replace("[Lat]", Convert.ToString(this.GPSData.dLatitude));
                    sListenerMsg = sListenerMsg.Replace("[Long]", Convert.ToString(this.GPSData.dLongitude));

                    try
                    {
                        DateTime oReportTime = this.GPSData.dtGPSTime;
                        oReportTime = oReportTime.AddSeconds(this.GPS_DeviceTime);
                        sListenerMsg = sListenerMsg.Replace("[Day]", Convert.ToString(oReportTime.Day));
                        sListenerMsg = sListenerMsg.Replace("[Month]", Convert.ToString(oReportTime.Month));
                        sListenerMsg = sListenerMsg.Replace("[Year]", Convert.ToString(oReportTime.Year));
                        sListenerMsg = sListenerMsg.Replace("[Hour]", Convert.ToString(oReportTime.Hour));
                        sListenerMsg = sListenerMsg.Replace("[Minute]", Convert.ToString(oReportTime.Minute));
                        sListenerMsg = sListenerMsg.Replace("[Second]", Convert.ToString(oReportTime.Second));
                    }
                    catch (System.Exception)
                    {
                        sListenerMsg = sListenerMsg.Replace("[Day]", Convert.ToString(DateTime.Now.Day));
                        sListenerMsg = sListenerMsg.Replace("[Month]", Convert.ToString(DateTime.Now.Month));
                        sListenerMsg = sListenerMsg.Replace("[Year]", Convert.ToString(DateTime.Now.Year));
                        sListenerMsg = sListenerMsg.Replace("[Hour]", Convert.ToString(DateTime.Now.Hour));
                        sListenerMsg = sListenerMsg.Replace("[Minute]", Convert.ToString(DateTime.Now.Minute));
                        sListenerMsg = sListenerMsg.Replace("[Second]", Convert.ToString(DateTime.Now.Second));
                    }

                    string sType = System.Configuration.ConfigurationManager.AppSettings["ActiveConfiguration"];
                    string ListenerIP = System.Configuration.ConfigurationManager.AppSettings[sType + "ListenerIP"];
                    string ListenerPort = System.Configuration.ConfigurationManager.AppSettings[sType + "ListenerPort"];

                    UDPSender.UDPClientSendMessage(ListenerIP, Convert.ToInt32(ListenerPort), sListenerMsg);
                    #endregion
                }
            }

            public new void Set()
            {
                base.Set();
                Finalise();
            }

            public override string ToString()
            {
                return base.ToString();
            }

            public static bool IsType(byte[] data)
            {
                return Driver.IsType(data, Type);
            }
            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public class Response
            {
                public int QuestionNumber = -1;
                public string ResponseText = "";
                public Response(int n, string resp)
                {
                    this.QuestionNumber = n;
                    this.ResponseText = resp;
                }
                public override string ToString()
                {
                    return "QuestionNumber = " + QuestionNumber + " ResponseText = " + this.ResponseText;
                }
            }
        }
        #endregion DriverLogout

        #region DriverLogonQuestionResponseAndDamageReport
        public class DriverLogonQuestionResponseAndDamageReport : Driver
        {
            public const string Type = "L";
            public const byte ANSWERS_DELIMITER_START = 0x06;
            public const byte RESPONSE_DELIMITER = 0x07;
            public const byte ANSWERS_DELIMITER_END = 0x08;
            public const int MAX_RESPONSE_SIZE = 100;

            public bool FitForDutyAnser = false;
            public int QuestionNumber = -1;
            public int SignatureSize = -1;
            public byte[] Signature = null;
            public ArrayList Responses = new ArrayList();

            public DriverLogonQuestionResponseAndDamageReport() : base() { SetType(Type); }
            public DriverLogonQuestionResponseAndDamageReport(byte[] data)
                : base(data)
            {
                Get();
            }
            public new void Get()
            {
                base.Get();
                byte fitForDuty = reader.ReadByte();
                this.FitForDutyAnser = fitForDuty.Equals((byte)'Y');
                byte b;
                if (reader.ReadByte().Equals(ANSWERS_DELIMITER_START))
                {
                    int numberOfQuestions = (int)reader.ReadByte();
                    if (numberOfQuestions == 0)
                    {
                        b = reader.ReadByte(); // should match ANSWERS_DELIMITER_END
                        if (b.Equals(ANSWERS_DELIMITER_END))
                            return;
                        throw new Exception("Missing ANSWERS_DELIMITER_END in DriverLogonQuestionResponseAndDamageReport" + ANSWERS_DELIMITER_END);
                    }

                    for (int i = 0; i < numberOfQuestions; i++)
                    {
                        int n = reader.ReadByte(); // Question Number;
                        if (n == ANSWERS_DELIMITER_END)
                            break;

                        // add byte for length
                        string r = reader.ReadASCIIStringDelimited(MAX_RESPONSE_SIZE, RESPONSE_DELIMITER, false);

                        this.Responses.Add(new Response(n, r));
                        n = reader.ReadByte();
                        if (n != RESPONSE_DELIMITER)
                            break;
                    }
                }
                b = reader.ReadByte();  // end of questions list
                b = reader.ReadByte();  // signature start
                this.SignatureSize = reader.Read2ByteInteger();
                if (this.SignatureSize <= reader.RemainingLength)
                {
                    this.Signature = reader.ReadBytes(this.SignatureSize);
                }

                #region Send Login message to the listener.
                //send off login message to listener so it's logged in the tracking client
                string sDisplayName = "";
                string sListenerMsg = "";
                sDisplayName = sDisplayName.Replace(",", "||");
                sListenerMsg = "L,[FleetID],[VehicleID],[DriverID],[DriverName],[Lat],[Long],[Day],[Month],[Year],[Hour],[Minute],[Second]";
                sListenerMsg = sListenerMsg.Replace("[FleetID]", Convert.ToString(this.VehicleFleetID));
                sListenerMsg = sListenerMsg.Replace("[VehicleID]", Convert.ToString(this.VehicleUnitID));
                sListenerMsg = sListenerMsg.Replace("[DriverID]", this.DriverID);
                sListenerMsg = sListenerMsg.Replace("[DriverName]", "");
                sListenerMsg = sListenerMsg.Replace("[Lat]", Convert.ToString(this.GPSData.dLatitude));
                sListenerMsg = sListenerMsg.Replace("[Long]", Convert.ToString(this.GPSData.dLongitude));

                try
                {
                    DateTime oReportTime = this.GPSData.dtGPSTime;
                    oReportTime = oReportTime.AddSeconds(this.GPS_DeviceTime);
                    sListenerMsg = sListenerMsg.Replace("[Day]", Convert.ToString(oReportTime.Day));
                    sListenerMsg = sListenerMsg.Replace("[Month]", Convert.ToString(oReportTime.Month));
                    sListenerMsg = sListenerMsg.Replace("[Year]", Convert.ToString(oReportTime.Year));
                    sListenerMsg = sListenerMsg.Replace("[Hour]", Convert.ToString(oReportTime.Hour));
                    sListenerMsg = sListenerMsg.Replace("[Minute]", Convert.ToString(oReportTime.Minute));
                    sListenerMsg = sListenerMsg.Replace("[Second]", Convert.ToString(oReportTime.Second));
                }
                catch (System.Exception)
                {
                    sListenerMsg = sListenerMsg.Replace("[Day]", Convert.ToString(DateTime.Now.Day));
                    sListenerMsg = sListenerMsg.Replace("[Month]", Convert.ToString(DateTime.Now.Month));
                    sListenerMsg = sListenerMsg.Replace("[Year]", Convert.ToString(DateTime.Now.Year));
                    sListenerMsg = sListenerMsg.Replace("[Hour]", Convert.ToString(DateTime.Now.Hour));
                    sListenerMsg = sListenerMsg.Replace("[Minute]", Convert.ToString(DateTime.Now.Minute));
                    sListenerMsg = sListenerMsg.Replace("[Second]", Convert.ToString(DateTime.Now.Second));
                }

                string sType = System.Configuration.ConfigurationManager.AppSettings["ActiveConfiguration"];
                string ListenerIP = System.Configuration.ConfigurationManager.AppSettings[sType + "ListenerIP"];
                string ListenerPort = System.Configuration.ConfigurationManager.AppSettings[sType + "ListenerPort"];

                UDPSender.UDPClientSendMessage(ListenerIP, Convert.ToInt32(ListenerPort), sListenerMsg);
                #endregion

            }

            public new void Set()
            {
                base.Set();
                if (this.FitForDutyAnser)
                    writer.WriteByte((byte)'Y');
                else
                    writer.WriteByte((byte)'N');

            }

            public override string ToString()
            {
                string s = base.ToString() + "FitForDutyAnser = " + FitForDutyAnser + " QuestionNumber = " + QuestionNumber;
                foreach (Response r in this.Responses)
                    s += r.ToString();
                s += "SignatureSize = " + this.SignatureSize;
                return s;
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public class Response
            {
                public int QuestionNumber = -1;
                public string ResponseText = "";
                public Response(int n, string resp)
                {
                    this.QuestionNumber = n;
                    this.ResponseText = resp;
                }
                public override string ToString()
                {
                    return "QuestionNumber = " + QuestionNumber + " ResponseText = " + this.ResponseText;
                }
            }
        }
        #endregion DriverLogonQuestionResponseAndDamageReport

        #region DriverContactedOps
        public class DriverContactedOps : Driver
        {
            public const string Type = "P";

            public int NumberOfIncorrectAnswers = -1;
            public ArrayList IncorrectAnswerArray = new ArrayList();
            public string zListType = "";

            public DriverContactedOps()
                : base()
            {
                base.SetType(Type);
            }

            public DriverContactedOps(byte[] data)
                : base(data)
            {
                Get();
            }

            public new void Get()
            {
                base.Get();

                zListType = Convert.ToString((char)reader.ReadByte());
                this.NumberOfIncorrectAnswers = Convert.ToInt32(reader.ReadByte());
                for (int i = 0; i < this.NumberOfIncorrectAnswers; i++)
                {
                    IncorrectAnswerArray.Add((int)reader.ReadByte());
                }
            }

            public new void Set()
            {
                base.Set();
                zListType = Convert.ToString((char)reader.ReadByte());
                writer.WriteByte((byte)this.NumberOfIncorrectAnswers);
                for (int i = 0; i < this.NumberOfIncorrectAnswers; i++)
                {
                    writer.WriteByte((byte)this.IncorrectAnswerArray[i]);
                }
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, DriverContactedOps.Type);
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public override string ToString()
            {
                return base.ToString() + "NumberOfIncorrectAnswers = " + NumberOfIncorrectAnswers;
            }
        }
        #endregion DriverContactedOps

        #region JobRequest
        public class JobRequest : Driver
        {
            public const string Type = "R";
            public const string SubType = "J";


            public JobRequest()
                : base()
            {
                base.SetType(Type);
            }
            public JobRequest(byte[] data)
                : base(data)
            {
                Get();
            }

            public new void Get()
            {
                base.Get();
            }

            public new void Set()
            {
                base.Set();
                this.Finalise();
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public override string ToString()
            {
                return base.ToString() + "Type = " + Type + " SubType = " + SubType + " VehicleUnitID = " + VehicleUnitID + " VehicleFleetID = " + VehicleFleetID + " GPSData " + this.GPSData.ToString();
            }
        }
        #endregion JobRequest

        #region JobAccept
        public class JobAccept : Driver
        {
            public const string Type = "A";

            public JobAccept() : base() { base.SetType(Type); }
            public JobAccept(byte[] data)
                : base(data)
            {
                Get();
            }

            public new void Get()
            {
                base.Get();
                base.JobID = this.reader.Read4ByteInteger();
            }

            public new void Set()
            {
                base.Set();
                writer.Write4ByteInteger(base.JobID);
                this.data = new ArrayList(writer.ToByteArray());
                Finalise();
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public override string ToString()
            {
                return base.ToString() + "Type = " + Type + " VehicleUnitID = " + VehicleUnitID + " VehicleFleetID = " + VehicleFleetID + " GPSData " + this.GPSData.ToString();
            }
        }
        #endregion JobAccept

        #region JobComplete
        public class JobComplete : Driver
        {
            public const string Type = "C";
            public string LegType = "";

            public JobComplete()
                : base()
            {
                base.SetType(Type);
            }
            public JobComplete(byte[] data)
                : base(data)
            {
                Get();
            }
            public new void Get()
            {
                base.Get();
                this.LegType = reader.ReadASCIIStringFixedLength(1, 0x00);
            }

            public new void Set()
            {
                base.Set();
                writer.WriteASCIIStringFixedLength(this.LegType, 1, 0x00);
                this.data = new ArrayList(writer.ToByteArray());
                this.Finalise();
            }
            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }
            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }
            public override string ToString()
            {
                return base.ToString() + "Type = " + Type +
                    " VehicleUnitID = " + VehicleUnitID + " VehicleFleetID = " + VehicleFleetID + " GPSData " +
                    this.GPSData.ToString() + " LegType = " + this.LegType;
            }

        }
        #endregion JobComplete

        #region LegComplete
        public class LegComplete : Driver
        {
            public const string Type = "S";
            public const byte ANSWERS_DELIMITER_START = (byte)0x06;
            public const byte RESPONSE_DELIMITER = (byte)0x07;
            public const byte ANSWERS_DELIMITER_END = (byte)0x08;
            public const int MAX_RESPONSE_SIZE = (byte)100;
            public const byte SPANumberSeparator = (byte)0x04;
            public const byte TRAILERS_LIST_START = (byte)0x05;
            public const byte TRAILERS_LIST_END = (byte)0x08;
            public const byte TRAILER_START = (byte)0x06;
            public const byte TRAILER_END_OF_NAME = (byte)0x07;
            public const byte TRAILER_LIST_END = (byte)0x08;
            public const byte POD_NAME_DELIMETER = (byte)0x0A;
            public const byte SIGNATURE_START = (byte)0x0B;

            public const int MAX_SPA_NUMBER_LENGTH = 20;

            public string LegType = "";
            public int LegNumber = -1;
            public string SPANumber = "";
            public int DestinationState = 0;
            public string PickupDestinationCorrect = "U";
            public string WaquisCollected = "U";
            public string DangerousGoods = "U";
            public string CorrectPlacards = "U";
            public string TrailersHookedCorrectly = "U";
            public int TrailerCheckNuts = 0;
            public int NumberOfTrailers = -1;
            public Trailers JobTrailers = new Trailers();
            public byte[] SpareFlags = new byte[2];
            public string POD_Name = "";
            public int SignatureSize = -1;
            public byte[] Signature = null;
            public ArrayList Responses = new ArrayList();

            public LegComplete() : base() { base.SetType(Type); }
            public LegComplete(byte[] data)
                : base(data)
            {
                Get();
            }

            public new void Get()
            {
                byte bTemp = (byte)0x00;
                string sTrailerName = "";
                string sTrailerType = "";
                string sTrailerAction = "";
                byte[] TrailerESN = null;

                base.Get();
                this.LegType = reader.ReadASCIIStringFixedLength(1, 0x00);
                base.JobID = reader.Read4ByteInteger();
                this.LegNumber = reader.ReadByte();
                this.SPANumber = reader.ReadASCIIStringDelimited(20, SPANumberSeparator, true);
                this.DestinationState = (int)reader.ReadByte();
                this.PickupDestinationCorrect = Convert.ToString((char)reader.ReadByte());
                this.WaquisCollected = Convert.ToString((char)reader.ReadByte());
                this.DangerousGoods = Convert.ToString((char)reader.ReadByte());
                this.CorrectPlacards = Convert.ToString((char)reader.ReadByte());
                this.TrailersHookedCorrectly = Convert.ToString((char)reader.ReadByte());
                this.TrailerCheckNuts = (int)reader.ReadByte();

                //Qustions decode
                if (reader.ReadByte().Equals(ANSWERS_DELIMITER_START))
                {
                    int numberOfQuestions = (int)reader.ReadByte();
                    if (numberOfQuestions == 0)
                    {
                        int b = reader.ReadByte(); // should match ANSWERS_DELIMITER_END
                        if (b != ANSWERS_DELIMITER_END)
                            throw new Exception("Missing ANSWERS_DELIMITER_END " + ANSWERS_DELIMITER_END);
                    }

                    for (int i = 0; i < numberOfQuestions; i++)
                    {
                        int n = reader.ReadByte(); // Question Number;
                        if (n == ANSWERS_DELIMITER_END)
                        {
                            break;
                        }

                        // add byte for length
                        string r = reader.ReadASCIIStringDelimited(MAX_RESPONSE_SIZE, RESPONSE_DELIMITER, false);

                        this.Responses.Add(new Response(n, r));
                        n = reader.ReadByte();
                        if (n != RESPONSE_DELIMITER)
                            break;
                    }
                    if (numberOfQuestions > 0)
                    {
                        if (reader.ReadByte().Equals(ANSWERS_DELIMITER_END))
                        {

                        }
                    }
                }

                //Trailer Decode
                if (reader.ReadByte().Equals(TRAILERS_LIST_START))
                {
                    this.NumberOfTrailers = (int)reader.ReadByte();

                    bTemp = (byte)0x00;
                    bTemp = reader.ReadByte();
                    while (bTemp == TRAILER_START)
                    {
                        sTrailerName = "";
                        sTrailerName = reader.ReadASCIIStringDelimited(9, TRAILER_END_OF_NAME);
                        TrailerESN = new byte[6];
                        for (int X = 0; X < 6; X++)
                            TrailerESN[X] = reader.ReadByte();
                        sTrailerType = Convert.ToString((char)reader.ReadByte());
                        sTrailerAction = Convert.ToString((char)reader.ReadByte());
                        Trailers.Trailer oTrailer = new MTData.MDT.Interface.cBunkersPacketData.Trailers.Trailer(sTrailerName, TrailerESN, sTrailerType, sTrailerAction);
                        JobTrailers.Add(oTrailer);
                        bTemp = reader.ReadByte();
                    }
                    if (bTemp != TRAILERS_LIST_END)
                    {
                        throw new Exception("Missing TRAILERS_LIST_END " + TRAILERS_LIST_END);
                    }
                }

                SpareFlags[0] = reader.ReadByte();
                SpareFlags[1] = reader.ReadByte();
                POD_Name = reader.ReadASCIIStringDelimited(20, POD_NAME_DELIMETER);
                if (reader.ReadByte().Equals(SIGNATURE_START))
                {

                    int iSigLength = reader.Read2ByteInteger();
                    if ((iSigLength > 0) && (iSigLength <= reader.RemainingLength))
                    {
                        byte[] bInputData = new byte[1];
                        bInputData[0] = reader.ReadByte();
                        for (int iSig = 1; iSig < iSigLength; iSig++)
                        {
                            bInputData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bInputData, reader.ReadByte());
                        }
                        string sSigFilePath = System.Configuration.ConfigurationManager.AppSettings["SignatureFilePath"];
                        if (sSigFilePath == null)
                            sSigFilePath = "c:\\";
                        if (!System.IO.Directory.Exists(sSigFilePath))
                            System.IO.Directory.CreateDirectory(sSigFilePath);

                        string sSigFileName = sSigFilePath + "Unit " + Convert.ToString(base.UnitID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                        //Signature = DecodeSignature(bInputData, sSigFileName);
                        bInputData = null;
                    }
                }

            }

            public new void Set()
            {
                base.Set();
                writer.WriteASCIIStringFixedLength(this.LegType, 1, 0x00);
                writer.Write4ByteInteger(base.JobID);
                writer.WriteByte((byte)this.LegNumber);
                writer.WriteASCIIStringDelimited(this.SPANumber, MAX_SPA_NUMBER_LENGTH, SPANumberSeparator);
                writer.WriteByte((byte)this.JobTrailers.Count());
                writer.WriteByte(Trailers.TRAILER_LIST_START);
                this.JobTrailers.Set();
                writer.WriteByteArray((byte[])this.JobTrailers.Data.ToArray(typeof(byte)));
                writer.WriteByte(Trailers.TRAILER_LIST_END);
                writer.Write2ByteInteger(this.SignatureSize);
                writer.WriteByteArray(this.Signature);
                this.data = new ArrayList(writer.ToByteArray());
                Finalise();
            }
            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }
            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public override byte[] Process()
            {
                return base.Process();
            }

            public override string ToString()
            {
                return base.ToString() + "Type = " + Type + " VehicleUnitID = " +
                    VehicleUnitID + " VehicleFleetID = " + VehicleFleetID +
                    " GPSData " + this.GPSData.ToString() + " LegType = " + LegType + " JobID = " + JobID + " LegNumber = " + LegNumber +
                    " SPANumber =" + SPANumber + " " + JobTrailers.ToString() + " SignatureSize = " + SignatureSize;
            }

            public class Response
            {
                public int QuestionNumber = -1;
                public string ResponseText = "";
                public Response(int n, string resp)
                {
                    this.QuestionNumber = n;
                    this.ResponseText = resp;
                }
                public override string ToString()
                {
                    return "QuestionNumber = " + QuestionNumber + " ResponseText = " + this.ResponseText;
                }
            }
            private byte[] DecodeSignature(byte[] bData, string sFileName)
            {
                Bitmap oBMP = null;
                byte[] bRet = null;
                byte[] bConvert = null;
                ArrayList oEncodedBytes = new ArrayList();
                ArrayList oDecodedBytes = new ArrayList();
                int iPos = 0;
                int iTop = 0;
                int iLeft = 0;
                int iWidth = 0;
                int iHeight = 0;
                int iWhiteBits = 0;
                int iBlackBits = 0;
                int iState = 0;
                int iMask = 0;
                int iShift = 0;
                int iTemp = 0;
                byte bTemp = (byte)0x00;
                string sInstruction = "";
                string sColor = "";
                int iLength = 0;
                int iRow = 0;

                try
                {
                    #region Read the header information (Top, Left, Height, Width, Black Bits, White Bits)
                    // Get the top
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iTop = BitConverter.ToInt32(bConvert, 0);
                    // Get the left
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iLeft = BitConverter.ToInt32(bConvert, 0);
                    // Get the width
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iWidth = BitConverter.ToInt32(bConvert, 0);
                    // Get the height
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iHeight = BitConverter.ToInt32(bConvert, 0);
                    // Get the number of white bits in the encoding
                    bConvert = new byte[4];
                    bConvert[0] = bData[iPos++];
                    bConvert[1] = bData[iPos++];
                    iWhiteBits = BitConverter.ToInt32(bConvert, 0);
                    // Get the number of black bits in the encoding
                    bConvert = new byte[4];
                    bConvert[0] = bData[iPos++];
                    bConvert[1] = bData[iPos++];
                    iBlackBits = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    #region Expand the byte array out to a list of bits
                    while (iPos < bData.Length)
                    {
                        bTemp = bData[iPos++];
                        for (int X = 7; X >= 0; X--)
                        {
                            iMask = 1 << X;
                            if (((int)bTemp & iMask) == iMask)
                                oEncodedBytes.Add(1);
                            else
                                oEncodedBytes.Add(0);
                        }
                    }
                    #endregion
                    #region Decode the data to create an array list (oDecodedBytes) of instructions ("B~X" for X black bytes or "W~X" for X white bytes)
                    iState = 0;
                    for (int X = 0; X < oEncodedBytes.Count; X++)
                    {
                        switch (iState)
                        {
                            case 0:
                                #region Determine if we are decoding black or white
                                if ((int)oEncodedBytes[X] == 0)
                                {
                                    iState = 1;
                                    iShift = iBlackBits - 1;
                                }
                                else
                                {
                                    iState = 2;
                                    iShift = iWhiteBits - 1;
                                }
                                iTemp = 0;
                                #endregion
                                break;
                            case 1:
                                #region Decode Black Bytes
                                if (iShift == 0)
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    oDecodedBytes.Add("B~" + Convert.ToString(iTemp));
                                    iState = 0;
                                }
                                else
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    iShift--;
                                }
                                #endregion
                                break;
                            case 2:
                                #region Decode White Bytes
                                if (iShift == 0)
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    oDecodedBytes.Add("W~" + Convert.ToString(iTemp));
                                    iState = 0;
                                }
                                else
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    iShift--;
                                }
                                #endregion
                                break;
                        }
                    }
                    #endregion

                    if (oDecodedBytes.Count > 0)
                    {
                        string sDefaultBMP = System.Configuration.ConfigurationManager.AppSettings["UseBackgroundGIFforSignatures"];
                        if (!sDefaultBMP.Contains(":"))
                        {
                            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                            path = System.IO.Path.GetDirectoryName(path);
                            sDefaultBMP = path + "\\" + sDefaultBMP;
                        }
                        FileStream oFS = new FileStream(sDefaultBMP, FileMode.Open);
                        oBMP = (Bitmap)Bitmap.FromStream(oFS);
                        oFS.Close();
                        oFS = null;
                        iPos = iLeft;
                        iRow = iTop;
                        for (int Z = 0; Z < oDecodedBytes.Count; Z++)
                        {
                            sInstruction = (string)oDecodedBytes[Z];
                            sColor = sInstruction.Split("~".ToCharArray())[0];
                            iLength = Convert.ToInt32(sInstruction.Split("~".ToCharArray())[1]);
                            if (sColor == "B")
                            {
                                for (int X = 0; X < iLength; X++)
                                {
                                    if ((iPos - iLeft) > iWidth)
                                    {
                                        iRow++;
                                        iPos = iLeft;
                                    }
                                    oBMP.SetPixel(iPos, iRow, Color.Black);
                                    iPos++;
                                }
                            }
                            else
                            {
                                for (int X = 0; X < iLength; X++)
                                {
                                    if ((iPos - iLeft) > iWidth)
                                    {
                                        iRow++;
                                        iPos = iLeft;
                                    }
                                    iPos++;
                                }
                            }
                        }
                        oBMP.Save(sFileName, System.Drawing.Imaging.ImageFormat.Gif);
                        oBMP.Dispose();
                        oBMP = null;
                        oFS = new FileStream(sFileName, FileMode.Open);
                        bRet = new byte[oFS.Length];
                        oFS.Read(bRet, 0, Convert.ToInt32(oFS.Length));
                        oFS.Close();
                        oFS = null;
                    }
                    else
                    {
                        bRet = new byte[1];
                        bRet[0] = (byte)0x00;
                        throw new Exception("Blank Signature on Packet.");
                    }
                }
                catch (System.Exception ex)
                {
                    throw new Exception("Error decoding signature. " + ex.Message);
                }
                return bRet;
            }


        }
        #endregion LegComplete

        #region TrailerESNQueryResponse  (MTData only)
        public class TrailerESNQueryResponse : Driver
        {
            public const string Type = "T";
            public byte[] TrailerESN = new byte[6];
            public byte TrailerType;
            public byte TrailerCategory;
            public string TrailerName;

            public TrailerESNQueryResponse()
            {
                base.SetType(Type);
                sendOnToOutsideClient = false;
            }
            public TrailerESNQueryResponse(byte[] data)
                : base(data)
            {
                Get();
                sendOnToOutsideClient = false;
            }
            public new void Get()
            {
                base.Get();
                TrailerESN = reader.ReadBytes(TrailerESN.Length);
                TrailerType = reader.ReadByte();
                TrailerCategory = reader.ReadByte();
                this.TrailerName = reader.ReadASCIIStringDelimited(9, Trailers.Trailer.TRAILER_NAME_DELIMITER);

            }

            public new void Set()
            {
                base.Set();
                this.writer.WriteByteArray(this.TrailerESN);
                this.writer.WriteByte(this.TrailerType);
                this.writer.WriteByte(this.TrailerCategory);
                this.writer.WriteASCIIStringDelimited(this.TrailerName, 9, Trailers.Trailer.TRAILER_NAME_DELIMITER);
                this.Finalise();
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }
            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public override byte[] Process()
            {
                // Load Trailer Name from ESN
                //DAO d = new DAO ();
                //this.TrailerName = d.GetTrailerName (this.TrailerESN);
                this.TrailerName = "";
                Set();
                return this.Data;
            }

        }
        #endregion TrailerESNQueryResponse

        #region FuelFIll
        public class FuelFill : Driver
        {
            public const string Type = "F";
            public int FuelLiters = -1;

            public FuelFill() : base() { base.SetType(Type); }
            public FuelFill(byte[] data)
                : base(data)
            {
                Get();
            }

            public new void Get()
            {
                base.Get();
                string TempStr = reader.ReadASCIIStringDelimited(6, 0x0A, false);
                this.FuelLiters = Convert.ToInt32(TempStr);
            }

            public new void Set()
            {
                base.Set();
                string FuelFill = "";
                FuelFill = Convert.ToString(this.FuelLiters);
                writer.WriteASCIIStringDelimited(FuelFill, 6, 0x0A);
                this.data = new ArrayList(writer.ToByteArray());
                this.Finalise();
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public override string ToString()
            {
                return base.ToString() + "Type = " + Type + " VehicleUnitID = " + VehicleUnitID + " VehicleFleetID = " + VehicleFleetID + " GPSData " + this.GPSData.ToString() + this.FuelLiters;
            }
        }
        #endregion FuelFill

        #region MessageFromDriver
        public class MessageFromDriver : Driver
        {
            public const string Type = "M";
            public const char SubType = 'F';
            public const int SUB_TYPE_INDEX = 31;

            public string DriverMessage = "";

            public MessageFromDriver()
            {
                base.SetType(Type);
            }
            public MessageFromDriver(byte[] data)
                : base(data)
            {
                Get();
            }
            public new void Get()
            {
                base.Get();
                reader.ReadByte();			//the sub command
                DriverMessage = reader.ReadASCIIStringDelimited(MESSAGE_MAX_LENGTH, EndOfLine, false);
            }

            public new void Set()
            {
                base.Set();
                this.writer.WriteASCIIStringDelimited(DriverMessage, MESSAGE_MAX_LENGTH, EndOfLine);
                this.Finalise();
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public static bool IsSubType(byte[] data)
            {
                int k = SUB_TYPE_INDEX;
                //find the sub command
                for (int i = 0; i < 6; i++)
                {
                    if (data[k++] == 0x05)  //find the driver id seperator
                    {
                        break;
                    }
                }
                return (data[k].Equals((byte)SubType));
            }

            public override byte[] Process()
            {
                return this.Data;
            }
            public override string ToString()
            {
                return base.ToString() + " Message " + DriverMessage;
            }

        }
        #endregion MessageFromDriver

        #region MessageFromServerHasBeenReadByDriver
        public class MessageFromServerHasBeenReadByDriver : Driver
        {
            public const string Type = "M";
            public const char SubType = 'R';
            public const int SUB_TYPE_INDEX = 31;

            public int MessageID = -1;


            public MessageFromServerHasBeenReadByDriver()
            {
                base.SetType(Type);
            }
            public MessageFromServerHasBeenReadByDriver(byte[] data)
                : base(data)
            {
                Get();
            }
            public new void Get()
            {
                base.Get();
                reader.ReadByte();				//sub command
                MessageID = reader.Read4ByteInteger();
            }

            public new void Set()
            {
                base.Set();
                this.writer.WriteByte((byte)SubType);
                this.writer.Write4ByteInteger(this.MessageID);
                this.Finalise();
            }

            public static bool IsType(byte[] data, int index)
            {
                return IsType(data, Type, index);
            }

            public static bool IsType(byte[] data)
            {
                return IsType(data, Type);
            }

            public static bool IsSubType(byte[] data)
            {
                int k = SUB_TYPE_INDEX;
                //find the sub command
                for (int i = 0; i < 6; i++)
                {
                    if (data[k++] == 0x05)  //find the driver id seperator
                    {
                        break;
                    }
                }
                return (data[k].Equals((byte)SubType));
            }

            public override byte[] Process()
            {
                return this.Data;
            }
            public override string ToString()
            {
                return base.ToString() + " MessageID " + MessageID;
            }
        }
        #endregion MessageFromServerHasBeenReadByDriver

        //public class JobAccept : 
        #endregion Message Classes



    }

}

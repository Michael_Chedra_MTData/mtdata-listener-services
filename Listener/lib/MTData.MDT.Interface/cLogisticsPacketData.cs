using System;
using System.Drawing;
using System.IO;
using System.Collections;

using MTData.Common.Utilities;
using MTData.MotFileReader;
using log4net;
using MTData.Common.Config;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cLogisticsPacketData.
    /// </summary>

    public class cLogisticsPacketData
    {
        public class LocationEntry
        {
            public int ID = 0;
            public string LocationName = "";
            public uint iLat = 0;
            public uint iLong = 0;
            public double Latitude
            {
                get { try { return cLogisticsPacketData.ConvLatLon(this.iLat); } catch (System.Exception ex) { throw (ex); } }
                set { try { this.iLat = cLogisticsPacketData.ConvLatLon(value); } catch (System.Exception ex) { throw (ex); } }
            }
            public double Longitude
            {
                get { try { return cLogisticsPacketData.ConvLatLon(this.iLong); } catch (System.Exception ex) { throw (ex); } }
                set { try { this.iLong = cLogisticsPacketData.ConvLatLon(value); } catch (System.Exception ex) { throw (ex); } }
            }
            public ushort Tolerance = 0;
        }

        private const string sClassName = "cLogisticsPacketData.";
        private ILog oLogging = LogManager.GetLogger(typeof(cLogisticsPacketData));
        private bool _bLogInbound = false;
        private bool _bLogOutbound = false;
        private int iMaxPacketSize = 600;
        private string sSigFilePath = "";
        ConvertSignatureConfig oSigConfig = null;

        #region Packet Vars
        public byte bProtocolVer = (byte)0x00;
        public byte bPacketNum = (byte)0x01;
        public byte bPacketTotal = (byte)0x01;
        public string CommandType = "";
        public string SubCommandType = "";
        public byte AllowFreeDial = (byte)0x00;
        public string LoadType = "";
        public int MobileID = 0;
        public int JobID = 0;
        public byte FleetID = 0;
        public int VehicleID = 0;
        public int DriverID = 0;
        public uint VehicleLatitude = 0;
        public uint VehicleLongitude = 0;
        public double dVehicleLatitude
        {
            get { try { return cLogisticsPacketData.ConvLatLon(VehicleLatitude); } catch (System.Exception ex) { throw (ex); } }
            set { try { this.VehicleLatitude = cLogisticsPacketData.ConvLatLon(value); } catch (System.Exception ex) { throw (ex); } }
        }
        public double dVehicleLongitude
        {
            get { try { return cLogisticsPacketData.ConvLatLon(VehicleLongitude); } catch (System.Exception ex) { throw (ex); } }
            set { try { this.VehicleLongitude = cLogisticsPacketData.ConvLatLon(value); } catch (System.Exception ex) { throw (ex); } }
        }
        public string sVehicleLatitude
        {
            get { try { return Convert.ToString(cLogisticsPacketData.ConvLatLon(VehicleLatitude)); } catch (System.Exception ex) { throw (ex); } }
            set { try { this.VehicleLatitude = cLogisticsPacketData.ConvLatLon(Convert.ToDouble(value)); } catch (System.Exception ex) { throw (ex); } }
        }
        public string sVehicleLongitude
        {
            get { try { return Convert.ToString(cLogisticsPacketData.ConvLatLon(VehicleLongitude)); } catch (System.Exception ex) { throw (ex); } }
            set { try { this.VehicleLongitude = cLogisticsPacketData.ConvLatLon(Convert.ToDouble(value)); } catch (System.Exception ex) { throw (ex); } }
        }
        public byte VehicleSpeed = (byte)0x00;
        public ushort VehicleHeading = 0;
        public short VehicleDeviceTimer = 0;
        public DateTime VehicleGPSTime = DateTime.MinValue;
        public byte DriverHasDocumentation = (byte)0x00;

        // Address List Vars
        public int ItemID = 0;
        public string ItemName = "";
        public string ItemAddress = "";
        public string ItemComment = "";
        public double ItemLatitude = 0;
        public double ItemLongitude = 0;

        // List Packet Vars
        public int ListID = 0;
        public int ListVersion = 0;
        public int ListPositionOffset = 0;
        public ArrayList oListLocations = new ArrayList();
        public ArrayList oListIDs = new ArrayList();
        public ArrayList oListValues = new ArrayList();
        public ArrayList oListESNs = new ArrayList();
        // Download firmware and terminal login vars
        public byte FirmwareVersionMajor = (byte)0x00;
        public byte FirmwareVersionMinor = (byte)0x00;
        public byte WinCEVersionMajor = (byte)0x00;
        public byte WinCEVersionMinor = (byte)0x00;
        public byte CPLDVersionMajor = (byte)0x00;
        public byte CPLDVersionMinor = (byte)0x00;
        public byte TemperatureMajor = (byte)0x00;
        public byte TemperatureMinor = (byte)0x00;
        public byte BatteryPercentage = (byte)0x00;
        public byte MDTOptionsFlags1 = (byte)0x00;
        public byte MDTOptionsFlags2 = (byte)0x00;
        public byte MDTOptionsFlags3 = (byte)0x00;
        public byte MDTOptionsFlags4 = (byte)0x00;
        public byte MDTOptionsFlags5 = (byte)0x00;
        public byte MDTOptionsFlags6 = (byte)0x00;
        public byte MDTOptionsFlags7 = (byte)0x00;
        public string MDTVariantName = "";
        public ArrayList DownloadMotParcel = null;
        public byte[] DownloadCEParcel = null;

        public ushort SegmentNumber = 0;
        public ushort NumberOfDataSegments = 0;
        public byte DownloadVersion = 0;
        public byte NumberOfDataSegmentsInThisPacket = 0;
        public byte[] bDataSegment = null;
        public uint ProgramTotalCheckSum = 0;
        public uint PacketTotalCheckSum = 0;
        public ushort ProgramPacketCountTotal = 0;
        public uint ProgramByteTotal = 0;
        public byte ProgramDataBytesCheckSum = (byte)0x00;

        // Job Packet vars
        public int JobDispatchID = 0;
        public int AllocationID = 0;
        public byte LegCount = (byte)0x00;
        public byte LegNumber = (byte)0x00;
        public bool IsReturnDelivery = false;
        public byte JobEmailFlags = (byte)0x00;
        public bool IsDangerousGoods = false;
        public bool IsPODRequired = false;
        public int JobLatitude = 0;
        public int JobLongitude = 0;
        public int DivisionID = 0;
        public int DelayReasonID = 0;
        public int RejectReasonID = 0;
        public ushort JobRadius = 0;
        public ushort EmailAtDistance = 0;
        public ushort LoadWeight = 0;
        public string LegDescription = "";
        public string JobCancelMessage = "";
        public string JobTitle = "";
        public int JobNum = 0;
        public bool AutoPickupJob = false;
        public bool AutoDeliverJob = false;
        public string LocationName = "";
        public string LocationAddress = "";
        public string LocationPhoneNumber = "";
        public string ExchangePalletOrder = "";
        public bool bFullReturn = false;
        public string JobNotes = "";
        public string PODName = "";
        public DateTime ArriveTime = DateTime.MinValue;
        public ushort ArriveTimeTolerance = 0;
        public DateTime DepartTime = DateTime.MinValue;
        public ushort DepartTimeTolerance = 0;
        public ArrayList PalletTypeList = new ArrayList();
        public ArrayList PalletCount = new ArrayList();
        public ArrayList TrailerList = new ArrayList();
        public ArrayList ReturnPalletTypeList = new ArrayList();
        public ArrayList ReturnPalletCount = new ArrayList();
        public ArrayList ExchangePalletTypeList = new ArrayList();
        public ArrayList ExchangePalletCount = new ArrayList();
        public ArrayList EmptyPalletTypeList = new ArrayList();
        public ArrayList EmptyPalletCount = new ArrayList();
        public int PickupQuestionListID = 0;
        public int PickupQuestionListVer = 0;
        public int DeliveryQuestionListID = 0;
        public int DeliveryQuestionListVer = 0;
        public ArrayList JobLegQuestionNum = new ArrayList();
        public ArrayList JobLegQuestionAnswer = new ArrayList();

        // Login Reply Packet Vars
        public bool ForceFirmwareUpdate = false;
        public bool ForceCPLDUpdate = false;
        public bool ForceWinCEUpdate = false;
        public bool NewPreTripList = false;
        public short GMTOffsetMins = 0;
        public string LoginResponse = "";
        public string DriverName = "";
        public string VehicleType = "";

        // Message Packet Vars
        public bool RequiresReply = false;
        public string MessageText = "";

        // List Packet Vars
        public int FleetAddressListVer = 0;
        public int DriverAddressListVer = 0;
        public int FleetEmailAddressListVer = -1;
        public int DriverEmailAddressListVer = -1;
        public int EntryListID = 0;
        public int EntryListVer = 0;
        public int PreTripListID = 0;
        public int PreTripListVer = 0;
        public int PostTripListID = 0;
        public int PostTripListVer = 0;
        public byte bIsLoginAnswers = (byte)0x00;
        public bool SignatureRequired = false;
        public ArrayList PreTripQuestionList = new ArrayList();
        public ArrayList PreTripQuestionAnswerType = new ArrayList();
        public ArrayList PreTripCheckAnswer = new ArrayList();
        public ArrayList PreTripIncorrectAnswerMsg = new ArrayList();

        // Fuels Stations
        public int FuelStationListID = 0;
        public int FuelStationListVer = 0;
        public int FuelStationID = 0;
        public ArrayList FuelStationLocations = new ArrayList();

        // Predefined Function Vars
        public bool StartFunction = false;
        public byte FunctionID = (byte)0x00;
        public bool FunctionStatus = false;
        public byte[] FunctionParamers = null;
        public byte[] FunctionResult = null;

        // Driver Channel Selection
        public string ChannelSelection = "";

        // Driver Login
        public int DriverLoginID = 0;
        public ushort DriverPIN = 0;
        public int SerialNumber = 0;
        public int HardwareType = 0;
        public byte HardwareVersionMajor = (byte)0x00;
        public byte HardwareVersionMinor = (byte)0x00;
        public int Odometer = 0;
        public int TotalFuelUsed = 0;
        public ushort PalletsOnTruck = 0;
        public int GlobalPhonebookVer = 0;
        public int DriverPhonebookVer = 0;
        public int DivisionGroupID = 0;
        public int DivisionListVer = 0;
        public int DelayReasonListVer = 0;
        public int RejectReasonListVer = 0;
        public int PalletTypeListVer = 0;
        public int TrailerListVer = 0;
        public int UsageListVer = 0;
        public int PredefineMsgListID = 0;
        public int PredefineMsgListVer = 0;
        public string RouteURLWebService = "";
        public int DelayReasonListID = 0;
        public int RejectReasonListID = 0;
        public int PalletTypeListID = 0;
        public int TrailerListID = 0;
        public int UsageListID = 0;

        // Phone Call Report
        public string PhoneNumber = "";
        public string SimCardNumber = "";
        public ushort PhoneCallDuration = 0;

        // Driver Messaging
        public int MessageID = 0;
        public string Message = "";
        public string EmailAddress = "";
        public int PredefinedMessageID = 0;

        // Pre-Trip Answers
        public byte NumOfAnswers = (byte)0x00;
        public ArrayList PreTripQuestionNum = new ArrayList();
        public ArrayList PreTripQuestionAnswer = new ArrayList();
        public byte bSigType = (byte)0x00;
        public byte[] SignatureData = null;

        // Vehicle Usage 
        public int UsageTypeID = 0;
        public bool IsUsageStart = false;

        // Trailer Usage 
        public int TrailerID = 0;
        public byte[] TrailerESN = null;
        public string TrailerName = "";

        // Fuel Usage
        public double dFuelCostPerLitre = 0;
        public double dFuelLitresPumped = 0;
        public double dFuelTotalCost = 0;

        // Address Lists 
        public int AddressListID = 0;
        public ArrayList oAddressListIDs = new ArrayList();
        public ArrayList oAddressListNames = new ArrayList();
        public ArrayList oAddressListAddresses = new ArrayList();
        public ArrayList oAddressListComment = new ArrayList();
        public ArrayList oAddressListLats = new ArrayList();
        public ArrayList oAddressListLongs = new ArrayList();

        public c5080AssetPacket o5080AssetPacket = null;
        #endregion
        #region Object Constructor
        public cLogisticsPacketData(ConvertSignatureConfig sigConfig, bool LogInboundComms, bool LogOutboundComms)
        {
            try
            {
                CreateObject(sigConfig, LogInboundComms, LogOutboundComms, iMaxPacketSize);
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "cLogisticsPacketData( ConvertSignatureConfig sigConfig, bool LogInboundComms, bool LogOutboundComms, int MaxPacketSize)", ex);
            }
        }
        public cLogisticsPacketData(ConvertSignatureConfig sigConfig, bool LogInboundComms, bool LogOutboundComms, int MaxPacketSize)
        {
            try
            {
                CreateObject(sigConfig, LogInboundComms, LogOutboundComms, MaxPacketSize);
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "cLogisticsPacketData( ConvertSignatureConfig sigConfig, bool LogInboundComms, bool LogOutboundComms, int MaxPacketSize)", ex);
            }
        }
        public void CreateObject(ConvertSignatureConfig sigConfig, bool LogInboundComms, bool LogOutboundComms, int MaxPacketSize)
        {
            try
            {
                _bLogInbound = LogInboundComms;
                _bLogOutbound = LogOutboundComms;
                iMaxPacketSize = MaxPacketSize;
                sSigFilePath = System.Configuration.ConfigurationManager.AppSettings["PicturePath"];
                oSigConfig = sigConfig;
                if (Directory.Exists(sSigFilePath))
                {
                    Directory.CreateDirectory(sSigFilePath);
                }
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "cLogisticsPacketData(  ConvertSignatureConfig sigConfig, bool TranslateRJs)", ex);
            }
        }
        #endregion
        #region Messages from the Host
        // This function will return and array of byte[]'s, one entry for each packet to be sent to MCC
        public object[] BuildOutgoingPacket()
        {
            object[] oRet = null;
            byte[] bRet = null;
            byte[] bConvert = null;
            byte[] bPayload = null;
            byte[] bDataSegment = null;
            MemoryStream oMS = null;
            int iEndSeg = iMaxPacketSize;
            int iPacketCount = 0;
            int iPos = 0;
            ArrayList oPacketArray = null;

            try
            {
                bPayload = BuildOutgoingPacketPayload();

                oMS = new MemoryStream();
                if (bPayload.Length < iMaxPacketSize)
                {
                    #region Setup the packet counts and length variables
                    bPacketNum = (byte)0x01;
                    bPacketTotal = (byte)0x01;
                    bConvert = BitConverter.GetBytes(bPayload.Length);
                    #endregion
                    #region Create the packet header [Protocol Ver][Length][Packet Num][Packet Total][Command Type][Mobile ID]
                    PacketUtilities.WriteToStream(oMS, bProtocolVer);				// [Protocol Ver (1 Byte)]
                    PacketUtilities.WriteToStream(oMS, (short)bPayload.Length);		// [Length (2 Bytes)]
                    PacketUtilities.WriteToStream(oMS, bPacketNum);					// [Packet Number]
                    PacketUtilities.WriteToStream(oMS, bPacketTotal);				// [Total Packets]
                    #endregion
                    PacketUtilities.WriteToStream(oMS, bPayload);					// Payload
                    #region Create the packet footer [SBS][EOP]
                    PacketUtilities.WriteToStream(oMS, (byte)0x07);																		// SBS
                    PacketUtilities.WriteToStream(oMS, (byte)0x03);																		// EOP
                    #endregion
                    bRet = oMS.ToArray();
                    oRet = new object[1];
                    oRet[0] = bRet;
                    if (oLogging != null && _bLogOutbound) oLogging.Info("Writing Host Data -> " + BitConverter.ToString(bRet, 0));
                }
                else
                {
                    #region Read the commmand type and mobile ID
                    //iPos = 0;
                    //PacketUtilities.ReadFromPacketAtPos(bPayload, ref iPos, (int)1, ref CommandType);
                    //iPos = 1;
                    //PacketUtilities.ReadFromPacketAtPos(bPayload, ref iPos, ref MobileID);
                    #endregion
                    #region Setup the packet counts and length variables

                    int iBytePos = 0;
                    oPacketArray = new ArrayList();
                    oMS = new MemoryStream();
                    while (iBytePos < bPayload.Length)
                    {
                        PacketUtilities.WriteToStream(oMS, (byte)bPayload[iBytePos]);
                        iPos++;
                        iBytePos++;
                        if (iPos >= iMaxPacketSize)
                        {
                            bDataSegment = oMS.ToArray();
                            oPacketArray.Add(bDataSegment);
                            oMS.Dispose();
                            oMS = new MemoryStream();
                            iPos = 0;
                        }
                    }
                    bDataSegment = oMS.ToArray();
                    if (bDataSegment.Length > 0)
                        oPacketArray.Add(bDataSegment);
                    oMS.Dispose();
                    #endregion
                    iPacketCount = 0;
                    iEndSeg = iMaxPacketSize;
                    bPacketNum = (byte)0x00;

                    oRet = new object[oPacketArray.Count];
                    for (iPacketCount = 0; iPacketCount < oPacketArray.Count; iPacketCount++)
                    {
                        bDataSegment = (byte[])oPacketArray[iPacketCount];
                        bPacketNum = (byte)(iPacketCount + 1);
                        bPacketTotal = (byte)oPacketArray.Count;

                        oMS = new MemoryStream();
                        #region Create the packet header [Protocol Ver][Length][Packet Num][Packet Total][Data Segment][SBS][EOP]
                        PacketUtilities.WriteToStream(oMS, bProtocolVer);				// [Protocol Ver (1 Byte)]
                        PacketUtilities.WriteToStream(oMS, (short)bDataSegment.Length);	// [Length (2 Bytes)]
                        PacketUtilities.WriteToStream(oMS, bPacketNum);					// [Packet Number]
                        PacketUtilities.WriteToStream(oMS, bPacketTotal);				// [Total Packets]
                        //PacketUtilities.WriteToStream(oMS, CommandType);				// Command Type
                        //PacketUtilities.WriteToStream(oMS, MobileID);					// Mobile ID
                        #endregion
                        PacketUtilities.WriteToStream(oMS, bDataSegment);				// Payload
                        #region Create the packet footer [SBS][EOP]
                        PacketUtilities.WriteToStream(oMS, (byte)0x07);					// SBS
                        PacketUtilities.WriteToStream(oMS, (byte)0x03);					// EOP
                        #endregion
                        #region Get the bytes for this packet and copy them into teh oMultiPart memory stream
                        bRet = oMS.ToArray();
                        oMS.Dispose();
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Writing Host Data -> " + BitConverter.ToString(bRet, 0));
                        oRet[iPacketCount] = bRet;
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "BuildOutgoingPacket()", ex);
            }
            return oRet;
        }
        private byte[] BuildOutgoingPacketPayload()
        {
            MemoryStream oMS = new MemoryStream();
            int iListCount = 0;
            uint iLat = 0;
            byte[] bConvert = null;
            byte[] bRet = null;
            int iCheckSum = 0;
            try
            {
                PacketUtilities.WriteToStream(oMS, CommandType);				                                // Command Type
                PacketUtilities.WriteToStream(oMS, MobileID);						                            // Mobile ID
                switch (CommandType)
                {
                    case "B":
                        #region Predefined Message List - [Message List ID][Message List Ver][Message Count] [Message Num][Message Text][TSC]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Predefined Message List Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, ListID);                                             // Message List ID
                        PacketUtilities.WriteToStream(oMS, ListVersion);                                        // Message List Ver
                        iListCount = (oListIDs.Count <= oListValues.Count) ? oListIDs.Count : oListValues.Count;
                        PacketUtilities.WriteToStream(oMS, (byte)iListCount);                                   // Message Count
                        for (int X = 0; X < iListCount; X++)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)oListIDs[X]);                              // Message Num
                            PacketUtilities.WriteToStream(oMS, (string)oListValues[X]);                         // Message Text
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);                                     // TSC
                        }
                        #endregion
                        break;
                    case "C":
                        #region Send Type List  - [Sub Command][List Version][List Count][ESC][Location][Item ID][ISC][Item Text]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building List Packet (" + SubCommandType + ") for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, SubCommandType);										// Sub Command

                        if (SubCommandType == "F" || SubCommandType == "G")
                            PacketUtilities.WriteToStream(oMS, AddressListID);								    // List ID
                        if ((int)bProtocolVer > 2)
                        {
                            if (SubCommandType == "E" || SubCommandType == "R" || SubCommandType == "P" || SubCommandType == "T" || SubCommandType == "V")
                                PacketUtilities.WriteToStream(oMS, ListID);
                        }
                        if (SubCommandType == "D")
                            PacketUtilities.WriteToStream(oMS, DivisionGroupID);

                        PacketUtilities.WriteToStream(oMS, ListVersion);										// List Version
                        if (SubCommandType == "F" || SubCommandType == "G")
                            iListCount = oAddressListIDs.Count;
                        else
                            iListCount = (oListIDs.Count <= oListValues.Count) ? oListIDs.Count : oListValues.Count;
                        PacketUtilities.WriteToStream(oMS, (ushort)iListCount);								// List Count
                        for (int X = 0; X < iListCount; X++)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)0x0A);									// ESC
                            if (SubCommandType == "F" || SubCommandType == "G")
                            {

                                #region Address List
                                PacketUtilities.WriteToStream(oMS, (int)oAddressListIDs[X]);					// Item ID
                                if (oAddressListNames.Count > X)
                                    PacketUtilities.WriteToStream(oMS, (string)oAddressListNames[X]);			// Name
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                if (oAddressListAddresses.Count > X)
                                    PacketUtilities.WriteToStream(oMS, (string)oAddressListAddresses[X]);       // Address
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                if (oAddressListComment.Count > X)
                                    PacketUtilities.WriteToStream(oMS, (string)oAddressListComment[X]);			// Comments
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                if (oAddressListLats.Count > X)
                                {
                                    iLat = ConvLatLon(Convert.ToDouble(oAddressListLats[X]));                   // Latitude
                                    PacketUtilities.WriteToStream(oMS, iLat);
                                }
                                else
                                {
                                    iLat = 0;
                                    PacketUtilities.WriteToStream(oMS, iLat);
                                }
                                if (oAddressListLongs.Count > X)
                                {
                                    iLat = ConvLatLon(Convert.ToDouble(oAddressListLongs[X]));                   // Latitude
                                    PacketUtilities.WriteToStream(oMS, iLat);
                                }
                                else
                                {
                                    iLat = 0;
                                    PacketUtilities.WriteToStream(oMS, iLat);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Other list types
                                PacketUtilities.WriteToStream(oMS, (byte)(ListPositionOffset + X));				// Location
                                if (SubCommandType == "A" || SubCommandType == "B")
                                {
                                    PacketUtilities.WriteToStream(oMS, (string)oListIDs[X]);						// Item ID
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);									// ISC
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, (int)oListIDs[X]);							// Item ID
                                }
                                PacketUtilities.WriteToStream(oMS, (string)oListValues[X]);						// Item Text
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                if (SubCommandType == "T")
                                {
                                    bConvert = (byte[])oListESNs[X];
                                    PacketUtilities.WriteToStream(oMS, bConvert);									// ESN
                                }
                                #endregion
                            }
                        }
                        #endregion
                        break;
                    case "c":
                        #region Download CPLD Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                        PacketUtilities.WriteToStream(oMS, SubCommandType);                                     // Command Type
                        if (SubCommandType == "H")
                        {
                            #region Download Header
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building CPLD Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);                           // Firmware Version Major
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);                           // Firmware Version Minor
                            PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);                           // Segment Count
                            PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Download Packet Data
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building CPLD Download Data Packet for mobile ID " + Convert.ToString(MobileID) + " - Segment Number = " + Convert.ToString(SegmentNumber));
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);			                // Firmware Ver Major (1 Byte)
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);			                // Firmware Ver Minor (1 Byte)
                            PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);	                        // Number of Segements (2 Bytes)
                            PacketUtilities.WriteToStream(oMS, SegmentNumber);	                                // Segment Number
                            PacketUtilities.WriteToStream(oMS, (ushort)DownloadCEParcel.Length);	            // Segment Length
                            PacketUtilities.WriteToStream(oMS, DownloadCEParcel);	                            // Segment
                            for (int X = 0; X < DownloadCEParcel.Length; X++)
                            {
                                iCheckSum += (int)DownloadCEParcel[X];
                            }
                            bConvert = BitConverter.GetBytes(iCheckSum);
                            PacketUtilities.WriteToStream(oMS, bConvert[0]);	                                // Segment Checksum 1
                            PacketUtilities.WriteToStream(oMS, bConvert[1]);	                                // Segment Checksum 2
                            #endregion
                        }
                        else if (SubCommandType == "V")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building CPLD Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                            #region Download Verify
                            PacketUtilities.WriteToStream(oMS, DownloadVersion);                                // Download Version
                            PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                            PacketUtilities.WriteToStream(oMS, ProgramPacketCountTotal);                        // Segment Count
                            PacketUtilities.WriteToStream(oMS, ProgramByteTotal);                               // Program Byte Total
                            #endregion
                        }
                        #endregion
                        break;
                    case "d":
                        #region Download Win CE Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                        PacketUtilities.WriteToStream(oMS, SubCommandType);                                     // Command Type
                        if (SubCommandType == "H")
                        {
                            #region Download Header
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);                           // Firmware Version Major
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);                           // Firmware Version Minor
                            PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);                           // Segment Count
                            PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Download Packet Data
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Download Data Packet for mobile ID " + Convert.ToString(MobileID) + " - Segment Number = " + Convert.ToString(SegmentNumber));
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);			                // Firmware Ver Major (1 Byte)
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);			                // Firmware Ver Minor (1 Byte)
                            PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);	                        // Number of Segements (2 Bytes)
                            PacketUtilities.WriteToStream(oMS, SegmentNumber);	                                // Segment Number
                            PacketUtilities.WriteToStream(oMS, (ushort)DownloadCEParcel.Length);	            // Segment Length
                            PacketUtilities.WriteToStream(oMS, DownloadCEParcel);	                            // Segment
                            for (int X = 0; X < DownloadCEParcel.Length; X++)
                            {
                                iCheckSum += (int)DownloadCEParcel[X];
                            }
                            bConvert = BitConverter.GetBytes(iCheckSum);
                            PacketUtilities.WriteToStream(oMS, bConvert[0]);	                                // Segment Checksum 1
                            PacketUtilities.WriteToStream(oMS, bConvert[1]);	                                // Segment Checksum 2
                            #endregion
                        }
                        else if (SubCommandType == "V")
                        {
                            #region Download Verify
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building WinCE Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, DownloadVersion);                                // Download Version
                            PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                            PacketUtilities.WriteToStream(oMS, ProgramPacketCountTotal);                        // Segment Count
                            PacketUtilities.WriteToStream(oMS, ProgramByteTotal);                               // Program Byte Total
                            #endregion
                        }
                        #endregion
                        break;
                    case "D":
                        #region Download MOT Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                        PacketUtilities.WriteToStream(oMS, SubCommandType);                                     // Command Type
                        if (SubCommandType == "h")
                        {
                            #region Download Header
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building MOT Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);                           // Firmware Version Major
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);                           // Firmware Version Minor
                            PacketUtilities.WriteToStream(oMS, NumberOfDataSegments);                           // Segment Count
                            PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                            #endregion
                        }
                        else if (SubCommandType == "d")
                        {
                            #region Download Packet Data
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building MOT Download Data Packet for mobile ID " + Convert.ToString(MobileID) + " - Segment Number = " + Convert.ToString(SegmentNumber));
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);			                // Firmware Ver Major (1 Byte)
                            PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);			                // Firmware Ver Minor (1 Byte)
                            PacketUtilities.WriteToStream(oMS, (ushort)DownloadMotParcel.Count);	            // Number of Segements (2 Bytes)
                            ushort iMaxDownloadMotParcelNumber = Convert.ToUInt16(DownloadMotParcel.Count - SegmentNumber);
                            if (iMaxDownloadMotParcelNumber > 2)
                                iMaxDownloadMotParcelNumber = 2;
                            if (iMaxDownloadMotParcelNumber >= 0)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)iMaxDownloadMotParcelNumber);	        // Number of segments in this packet (1 Byte)
                                for (ushort i = SegmentNumber; i < iMaxDownloadMotParcelNumber + SegmentNumber; i++)
                                {
                                    cParcel oParcel = (cParcel)DownloadMotParcel[(int)i];
                                    PacketUtilities.WriteToStream(oMS, i);										// PacketNumber (2 Bytes)
                                    PacketUtilities.WriteToStream(oMS, oParcel.Data);					        // Segment Data (250 Bytes)
                                    PacketUtilities.WriteToStream(oMS, (ushort)oParcel.CheckSum);		        // Segment Checksum (2 Bytes)
                                    PacketUtilities.WriteToStream(oMS, (byte)0xBB);						        // End of Segment (1 Byte)
                                }
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0xCC);						                // End of Downlaod Data (1 Byte)
                            #endregion
                        }
                        else if (SubCommandType == "v")
                        {
                            #region Download Verify
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building MOT Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, DownloadVersion);                                // Download Version
                            PacketUtilities.WriteToStream(oMS, ProgramTotalCheckSum);                           // Program Checksum
                            PacketUtilities.WriteToStream(oMS, PacketTotalCheckSum);                            // Total Checksum
                            PacketUtilities.WriteToStream(oMS, ProgramPacketCountTotal);                        // Program Packet Count
                            PacketUtilities.WriteToStream(oMS, ProgramByteTotal);                               // Program Byte Total
                            PacketUtilities.WriteToStream(oMS, ProgramDataBytesCheckSum);                       // Program Data Bytes checksum
                            #endregion
                        }
                        #endregion
                        break;
                    case "E":
                        #region New Job / Cancel Job / Delete Job / Update Job Packets.
                        PacketUtilities.WriteToStream(oMS, SubCommandType);										// Sub Command
                        PacketUtilities.WriteToStream(oMS, AllocationID);										// Allocation ID
                        PacketUtilities.WriteToStream(oMS, JobDispatchID);										// Job Dispatch ID
                        PacketUtilities.WriteToStream(oMS, JobID);												// Job Reference
                        if (SubCommandType == "N" || SubCommandType == "U")
                        {
                            #region New Job Packet / Update Job Packet
                            if (oLogging != null && _bLogOutbound)
                            {
                                if (SubCommandType == "N")
                                    oLogging.Info("Building New Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                                else
                                    oLogging.Info("Building Update Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            }
                            //New Job / Update Job
                            //[Sub Command][Allocation ID][Job Dispatch ID][Job Reference][Job Num][Leg Count][Job Title/Ext Ref][TSC]
                            //[Pickup Location Name][TSC][Pickup Location Address][TSC][Phone Number][TSC]
                            //[Pickup Lat][Pickup Long][Pickup Radius][Pickup Time][Time Tolerance][Pickup Depart Time][Time Tolerance]
                            //[Division ID][Load Weight][Dangerous Goods][Pickup Notes][TSC][Email Flags][Email At Distance]
                            //[SPS][Pallet Type][Pallet Count][EPS][STS][Trailer ID] [ETS]					
                            PacketUtilities.WriteToStream(oMS, JobNum);											// Job Num
                            PacketUtilities.WriteToStream(oMS, (AutoPickupJob ? (byte)0x01 : (byte)0x00));	    // Auto Pickup
                            PacketUtilities.WriteToStream(oMS, LegCount);										// Leg Count
                            PacketUtilities.WriteToStream(oMS, JobTitle);										// Job Title
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            PacketUtilities.WriteToStream(oMS, LocationName);									// Pickup Location Name
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            PacketUtilities.WriteToStream(oMS, LocationAddress);							    // Pickup Address
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            LocationPhoneNumber = ConvertPhoneNumber(LocationPhoneNumber);
                            PacketUtilities.WriteToStream(oMS, LocationPhoneNumber);							// Pickup Phone Number
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC

                            PacketUtilities.WriteToStream(oMS, JobLatitude);									// Pickup Lat
                            PacketUtilities.WriteToStream(oMS, JobLongitude);									// Pickup Long
                            PacketUtilities.WriteToStream(oMS, JobRadius);										// Pickup Radius
                            #region Add the arrive time
                            bConvert = new byte[6];
                            if (ArriveTime.Equals(DateTime.MinValue))
                            {
                                PacketUtilities.WriteToStream(oMS, bConvert);                                   // Arrive Date
                                PacketUtilities.WriteToStream(oMS, ArriveTimeTolerance);					    // Tolerance
                            }
                            else
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Day);						// Arrive Date - Day
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Month);					    // Arrive Date - Month
                                iListCount = ArriveTime.Year;
                                if (iListCount > 2000)
                                    iListCount = iListCount - 2000;
                                if (iListCount > 256 || iListCount < 0)
                                    iListCount = 0;
                                PacketUtilities.WriteToStream(oMS, (byte)iListCount);							// Arrive Date - Year
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Hour);						// Arrive Date - Hour
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Minute);					// Arrive Date - Minute
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Second);					// Arrive Date - Second
                                PacketUtilities.WriteToStream(oMS, ArriveTimeTolerance);					    // Arrive Date - Tolerance                                
                            }
                            #endregion
                            #region Add the depart time
                            bConvert = new byte[6];
                            if (DepartTime.Equals(DateTime.MinValue))
                            {
                                PacketUtilities.WriteToStream(oMS, bConvert);                                   // Depart Date
                                PacketUtilities.WriteToStream(oMS, DepartTimeTolerance);					    // Tolerance
                            }
                            else
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Day);						// Depart Date - Day
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Month);					    // Depart Date - Month
                                iListCount = ArriveTime.Year;
                                if (iListCount > 2000)
                                    iListCount = iListCount - 2000;
                                if (iListCount > 256 || iListCount < 0)
                                    iListCount = 0;
                                PacketUtilities.WriteToStream(oMS, (byte)iListCount);							// Depart Date - Year
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Hour);						// Depart Date - Hour
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Minute);					// Depart Date - Minute
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Second);					// Depart Date - Second
                                PacketUtilities.WriteToStream(oMS, DepartTimeTolerance);					    // Depart Date - Tolerance
                            }
                            #endregion
                            PacketUtilities.WriteToStream(oMS, DivisionID);										// Division ID
                            PacketUtilities.WriteToStream(oMS, LoadWeight);										// Load Weight
                            PacketUtilities.WriteToStream(oMS, (IsDangerousGoods ? (byte)0x01 : (byte)0x00));		// Dangerous Goods
                            PacketUtilities.WriteToStream(oMS, JobNotes);										// Pickup Notes
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            PacketUtilities.WriteToStream(oMS, JobEmailFlags);									// Email Flags
                            PacketUtilities.WriteToStream(oMS, EmailAtDistance);								// Email At Distance
                            iListCount = (PalletTypeList.Count <= PalletCount.Count) ? PalletTypeList.Count : PalletCount.Count;
                            for (int X = 0; X < iListCount; X++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// SPS
                                PacketUtilities.WriteToStream(oMS, (int)PalletTypeList[X]);					// Pallet Type ID
                                PacketUtilities.WriteToStream(oMS, (byte)PalletCount[X]);						// Pallet Count
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0x0B);									// EPS
                            for (int X = 0; X < TrailerList.Count; X++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0C);								// STS
                                PacketUtilities.WriteToStream(oMS, (int)TrailerList[X]);					    // Trailer ID
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0x0D);									// ETS
                            PacketUtilities.WriteToStream(oMS, LoadType);									    // Load Type String
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);										// TSC
                            #endregion
                        }
                        else if (SubCommandType == "C")
                        {
                            #region Cancel Job Packet - [Sub Command][Allocation ID][Job Reference][Job Cancel Message][TSC]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Cancel Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            PacketUtilities.WriteToStream(oMS, JobCancelMessage);								// Job Cancel Message
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Delete Job Packet
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Delete Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            #endregion
                        }
                        #endregion
                        break;
                    case "F":
                        #region New Job Leg / Cancel Job Leg / Delete Job Leg / Update Job Leg Packet
                        PacketUtilities.WriteToStream(oMS, SubCommandType);										// Sub Command
                        PacketUtilities.WriteToStream(oMS, AllocationID);										// Allocation ID
                        PacketUtilities.WriteToStream(oMS, JobDispatchID);										// Job Dispatch ID
                        PacketUtilities.WriteToStream(oMS, JobID);												// Job Reference
                        if (SubCommandType == "N" || SubCommandType == "U" || SubCommandType == "R")
                        {
                            #region New Job Packet / Update Job Packet
                            if (oLogging != null && _bLogOutbound)
                            {
                                if (SubCommandType == "N")
                                    oLogging.Info("Building New Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                                else if (SubCommandType == "U")
                                    oLogging.Info("Building Update Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                                else
                                    oLogging.Info("Building Reject Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            }
                            //New Job Leg / Update Job Leg
                            //[Sub Command][Allocation ID][Job Dispatch ID][Job Reference][Leg Num][Leg Description][TSC][Location Name][TSC]
                            //[Location Address][TSC][Phone Number][TSC][Lat][Long][Radius][Arrive Time][Time Tolerance][Depart Time][Time Tolerance]
                            //[Delivery Notes][TSC][POD Required][Email Flags][Email At Distance]
                            //[SPS][Pallet Type][Pallet Count] [EPS][Exchange Pallet Order][TSC][SEPS][Pallet Type][Pallet Count] [EEPS]
                            PacketUtilities.WriteToStream(oMS, LegNumber);										// Leg Number
                            PacketUtilities.WriteToStream(oMS, (AutoDeliverJob ? (byte)0x01 : (byte)0x00));		// Auto Deliver Job
                            PacketUtilities.WriteToStream(oMS, LegDescription);									// Leg Description
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            PacketUtilities.WriteToStream(oMS, LocationName);									// Location Name
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            PacketUtilities.WriteToStream(oMS, LocationAddress);								// Location Address
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            LocationPhoneNumber = ConvertPhoneNumber(LocationPhoneNumber);
                            PacketUtilities.WriteToStream(oMS, LocationPhoneNumber);							// Location Phone Number
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            PacketUtilities.WriteToStream(oMS, JobLatitude);									// Lat
                            PacketUtilities.WriteToStream(oMS, JobLongitude);									// Long
                            PacketUtilities.WriteToStream(oMS, JobRadius);										// Radius
                            #region Add the arrive time
                            bConvert = new byte[6];
                            if (ArriveTime.Equals(DateTime.MinValue))
                            {
                                PacketUtilities.WriteToStream(oMS, bConvert);                                   // Arrive Date
                                PacketUtilities.WriteToStream(oMS, ArriveTimeTolerance);					    // Tolerance
                            }
                            else
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Day);						// Arrive Date - Day
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Month);					    // Arrive Date - Month
                                iListCount = ArriveTime.Year;
                                if (iListCount > 2000)
                                    iListCount = iListCount - 2000;
                                if (iListCount > 256 || iListCount < 0)
                                    iListCount = 0;
                                PacketUtilities.WriteToStream(oMS, (byte)iListCount);							// Arrive Date - Year
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Hour);						// Arrive Date - Hour
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Minute);					// Arrive Date - Minute
                                PacketUtilities.WriteToStream(oMS, (byte)ArriveTime.Second);					// Arrive Date - Second
                                PacketUtilities.WriteToStream(oMS, ArriveTimeTolerance);					    // Arrive Date - Tolerance                                
                            }
                            #endregion
                            #region Add the depart time
                            bConvert = new byte[6];
                            if (DepartTime.Equals(DateTime.MinValue))
                            {
                                PacketUtilities.WriteToStream(oMS, bConvert);                                   // Depart Date
                                PacketUtilities.WriteToStream(oMS, DepartTimeTolerance);					    // Tolerance
                            }
                            else
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Day);						// Depart Date - Day
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Month);					    // Depart Date - Month
                                iListCount = ArriveTime.Year;
                                if (iListCount > 2000)
                                    iListCount = iListCount - 2000;
                                if (iListCount > 256 || iListCount < 0)
                                    iListCount = 0;
                                PacketUtilities.WriteToStream(oMS, (byte)iListCount);							// Depart Date - Year
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Hour);						// Depart Date - Hour
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Minute);					// Depart Date - Minute
                                PacketUtilities.WriteToStream(oMS, (byte)DepartTime.Second);					// Depart Date - Second
                                PacketUtilities.WriteToStream(oMS, DepartTimeTolerance);					    // Depart Date - Tolerance
                            }
                            #endregion
                            PacketUtilities.WriteToStream(oMS, JobNotes);										// Delivery Notes
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            PacketUtilities.WriteToStream(oMS, (IsPODRequired ? (byte)0x01 : (byte)0x00));		// Is POD/Signature required
                            PacketUtilities.WriteToStream(oMS, JobEmailFlags);									// Email Flags
                            PacketUtilities.WriteToStream(oMS, EmailAtDistance);								// Email At Distance

                            iListCount = (PalletTypeList.Count <= PalletCount.Count) ? PalletTypeList.Count : PalletCount.Count;
                            for (int X = 0; X < iListCount; X++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// SPS
                                PacketUtilities.WriteToStream(oMS, (int)PalletTypeList[X]);					// Pallet Type ID
                                PacketUtilities.WriteToStream(oMS, (byte)PalletCount[X]);						// Pallet Count
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0x0B);									// EPS
                            PacketUtilities.WriteToStream(oMS, ExchangePalletOrder);							// Exchange Pallet Order
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            iListCount = (ExchangePalletTypeList.Count <= ExchangePalletCount.Count) ? ExchangePalletTypeList.Count : ExchangePalletCount.Count;
                            for (int X = 0; X < iListCount; X++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0D);								// SEPS
                                PacketUtilities.WriteToStream(oMS, (int)ExchangePalletTypeList[X]);			// Pallet Type ID
                                PacketUtilities.WriteToStream(oMS, (byte)ExchangePalletCount[X]);				// Pallet Count
                            }
                            PacketUtilities.WriteToStream(oMS, (byte)0x0E);									// EPS
                            #endregion
                        }
                        else if (SubCommandType == "C")
                        {
                            #region Cancel Job Leg
                            //[Sub Command][Allocation ID][Job Reference][Leg Num][Job Cancel Message][TSC]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Cancel Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            PacketUtilities.WriteToStream(oMS, LegNumber);										// Leg Number
                            PacketUtilities.WriteToStream(oMS, JobCancelMessage);								// Leg Cancel Message
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Delete Job Leg
                            //[Sub Command][Job Reference][Leg Num]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Delete Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            PacketUtilities.WriteToStream(oMS, LegNumber);										// Leg Number
                            #endregion
                        }
                        #endregion
                        break;
                    case "L":
                        #region Login Reply  - [Login Response][Firmware Major][Firmware Minor][Force Update][New Pre-Trip List][GMT Offset Mins][Driver Name][TSC][Vehicle Type][TSC]
                        if (oLogging != null && _bLogOutbound)
                        {
                            if (AllowFreeDial == (byte)0x01)
                                oLogging.Info("Building Login Reply Packet for mobile ID " + Convert.ToString(MobileID) + " - Response = " + Convert.ToString(LoginResponse) + ", Allow Free Dial = true, Driver ID = " + Convert.ToString(DriverID) + ", Driver Name = '" + DriverName + "'");
                            else
                                oLogging.Info("Building Login Reply Packet for mobile ID " + Convert.ToString(MobileID) + " - Response = " + Convert.ToString(LoginResponse) + ", Allow Free Dial = false, Driver ID = " + Convert.ToString(DriverID) + ", Driver Name = '" + DriverName + "'");
                        }
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Login Reply Packet");
                        PacketUtilities.WriteToStream(oMS, LoginResponse);										// Sub Command
                        PacketUtilities.WriteToStream(oMS, AllowFreeDial);										// Allow Free Dial
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);								// Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);								// Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, (ForceFirmwareUpdate ? (byte)0x01 : (byte)0x00));		// Force Firmware Update
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMajor);								    // CPLD Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMinor);								    // CPLD Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, (ForceCPLDUpdate ? (byte)0x01 : (byte)0x00));	    // CPLD Force Firmware Update
                        PacketUtilities.WriteToStream(oMS, (NewPreTripList ? (byte)0x01 : (byte)0x00));			// New Pre-Trip List
                        PacketUtilities.WriteToStream(oMS, GMTOffsetMins);										// GMT Offset Mins
                        PacketUtilities.WriteToStream(oMS, DriverID);											// Driver ID
                        PacketUtilities.WriteToStream(oMS, DriverName);											// Driver Name
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);										// TSC
                        PacketUtilities.WriteToStream(oMS, VehicleType);										// Vehicle Type
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);										// TSC
                        PacketUtilities.WriteToStream(oMS, RouteURLWebService);									// RouteURLWebService
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);										// TSC     
                        PacketUtilities.WriteToStream(oMS, LoadType);									        // Load Type String
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);										// TSC                        
                        #endregion
                        break;
                    case "M":
                        #region Message Packet - [Message ID][Message Text][TSC][Requires Reply]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Driver Message Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, MessageID);											// Message ID
                        PacketUtilities.WriteToStream(oMS, MessageText);										// Message Text
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);										// TSC
                        PacketUtilities.WriteToStream(oMS, (RequiresReply ? (byte)0x01 : (byte)0x00));			// Message Requires a Reply
                        if (oAddressListNames.Count > 0)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)0x01);			                            // Location Attached
                            if (oAddressListNames.Count > 0)
                                PacketUtilities.WriteToStream(oMS, (string)oAddressListNames[0]);			    // Name
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            if (oAddressListAddresses.Count > 0)
                                PacketUtilities.WriteToStream(oMS, (string)oAddressListAddresses[0]);           // Address
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            if (oAddressListComment.Count > 0)
                                PacketUtilities.WriteToStream(oMS, (string)oAddressListComment[0]);			    // Phonenumber
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            if (oAddressListLats.Count > 0)
                            {
                                iLat = ConvLatLon(Convert.ToDouble(oAddressListLats[0]));                       // Latitude
                                PacketUtilities.WriteToStream(oMS, iLat);
                            }
                            else
                            {
                                iLat = 0;
                                PacketUtilities.WriteToStream(oMS, iLat);
                            }
                            if (oAddressListLongs.Count > 0)
                            {
                                iLat = ConvLatLon(Convert.ToDouble(oAddressListLongs[0]));                      // Latitude
                                PacketUtilities.WriteToStream(oMS, iLat);
                            }
                            else
                            {
                                iLat = 0;
                                PacketUtilities.WriteToStream(oMS, iLat);
                            }
                        }
                        else
                            PacketUtilities.WriteToStream(oMS, (byte)0x00);			                            // Location Attached
                        #endregion
                        break;
                    case "O":
                        #region Remote Logout Packet -
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Remote Logout Packet for mobile ID " + Convert.ToString(MobileID));
                        #endregion
                        break;
                    case "P":
                        #region List Packet - [Sub Command][Question List ID][Question List Ver][Signature Required][Question Count][SQS][Question Text][TSC][Answer Type][EQS]
                        if (oLogging != null && _bLogOutbound)
                        {
                            if (SubCommandType == "A")
                                oLogging.Info("Building Pre-Trip Question List Packet for mobile ID " + Convert.ToString(MobileID) + ", List ID = " + Convert.ToString(EntryListID) + ", List Version = " + Convert.ToString(EntryListVer));
                            else if (SubCommandType == "B")
                                oLogging.Info("Building Post-Trip Question List Packet for mobile ID " + Convert.ToString(MobileID) + ", List ID = " + Convert.ToString(EntryListID) + ", List Version = " + Convert.ToString(EntryListVer));
                            else if (SubCommandType == "D")
                                oLogging.Info("Building Delivery Question List Packet for mobile ID " + Convert.ToString(MobileID) + ", List ID = " + Convert.ToString(EntryListID) + ", List Version = " + Convert.ToString(EntryListVer));
                            else if (SubCommandType == "P")
                                oLogging.Info("Building Pickup Question List Packet for mobile ID " + Convert.ToString(MobileID) + ", List ID = " + Convert.ToString(EntryListID) + ", List Version = " + Convert.ToString(EntryListVer));
                            else if (SubCommandType == "F")
                                oLogging.Info("Building Fuel Station List Packet for mobile ID " + Convert.ToString(MobileID) + ", List ID = " + Convert.ToString(EntryListID) + ", List Version = " + Convert.ToString(EntryListVer));
                            else
                                oLogging.Info("Building Unknown Question List Packet for mobile ID " + Convert.ToString(MobileID) + ", List ID = " + Convert.ToString(EntryListID) + ", List Version = " + Convert.ToString(EntryListVer));
                        }
                        PacketUtilities.WriteToStream(oMS, SubCommandType);										// Sub Command Type
                        PacketUtilities.WriteToStream(oMS, EntryListID);										// Question List ID
                        PacketUtilities.WriteToStream(oMS, EntryListVer);										// Question List Ver
                        if (SubCommandType == "A" || SubCommandType == "B" || SubCommandType == "D" || SubCommandType == "P")
                        {
                            #region Question Lists
                            PacketUtilities.WriteToStream(oMS, (SignatureRequired ? (byte)0x01 : (byte)0x00));	// Signature Required
                            iListCount = PreTripQuestionList.Count;
                            PacketUtilities.WriteToStream(oMS, (byte)iListCount);									// Question Count
                            for (int X = 0; X < iListCount; X++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);									// SES
                                PacketUtilities.WriteToStream(oMS, (string)PreTripQuestionList[X]);				// Question Text
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                PacketUtilities.WriteToStream(oMS, ((string)PreTripQuestionAnswerType[X]).Substring(0, 1));		// Answer Type
                                if (SubCommandType == "A" || SubCommandType == "B")
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)((int)PreTripCheckAnswer[X]));        // Check Answer
                                    PacketUtilities.WriteToStream(oMS, (string)PreTripIncorrectAnswerMsg[X]);       // Incorrect Answer Msg
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                }
                            }
                            #endregion
                        }
                        else if (SubCommandType == "F")
                        {
                            #region Location Lists
                            iListCount = FuelStationLocations.Count;
                            PacketUtilities.WriteToStream(oMS, (byte)iListCount);								// Question Count
                            for (int X = 0; X < iListCount; X++)
                            {
                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);									// SES
                                LocationEntry oLocation = (LocationEntry)FuelStationLocations[X];
                                PacketUtilities.WriteToStream(oMS, oLocation.ID);				                // Fuel Staion ID
                                PacketUtilities.WriteToStream(oMS, oLocation.LocationName);				        // Fuel Staion Name
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);									// TSC
                                PacketUtilities.WriteToStream(oMS, oLocation.iLat);				                // Latitude
                                PacketUtilities.WriteToStream(oMS, oLocation.iLong);				            // Longitude
                                PacketUtilities.WriteToStream(oMS, oLocation.Tolerance);				        // Tolerance
                            }
                            #endregion
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x0B);										// EES
                        #endregion
                        break;
                    case "Q":
                        #region Start Pre-defined Function Packet - [Function ID][Start / Stop][Parameter Length][Function Parameters][EOP]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Predefined Function Packet for mobile ID " + Convert.ToString(MobileID) + ", Function ID = " + Convert.ToString(FunctionID));
                        PacketUtilities.WriteToStream(oMS, FunctionID);											// Function ID
                        PacketUtilities.WriteToStream(oMS, (StartFunction ? (byte)0x01 : (byte)0x00));			// Start / Stop
                        if (FunctionParamers != null)
                        {
                            PacketUtilities.WriteToStream(oMS, (ushort)FunctionParamers.Length);				// Parameter Length
                            PacketUtilities.WriteToStream(oMS, FunctionParamers);								// Function Parameters
                        }
                        else
                        {
                            PacketUtilities.WriteToStream(oMS, (ushort)0);										// Parameter Length
                        }
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);										// EOP
                        #endregion
                        break;
                    case "V":
                        #region Trailer Query Reply Packet  - [Trailer ID] [Trailer Name][TSC][Trailer ESN]
                        if (oLogging != null && _bLogInbound) oLogging.Info("Building Trailer Query Reply Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, TrailerID);											// TrailerID
                        PacketUtilities.WriteToStream(oMS, TrailerName);										// Trailer Name
                        PacketUtilities.WriteToStream(oMS, (byte)0x012);										// TSC
                        PacketUtilities.WriteToStream(oMS, TrailerESN);											// Trailer ESN
                        #endregion
                        break;
                    case "v":
                        #region Force Update Packet - [MDT Firmware Major][ MDTFirmware Minor][Force Update][WinCE Major][WinCE Minor][Force Update][NEC Major][NEC Minor][NEC Force Update]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Forced Update Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);								// Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);								// Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, (ForceFirmwareUpdate ? (byte)0x01 : (byte)0x00));		// Force Firmware Update
                        PacketUtilities.WriteToStream(oMS, WinCEVersionMajor);								    // WinCE Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, WinCEVersionMinor);								    // WinCE Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, (ForceWinCEUpdate ? (byte)0x01 : (byte)0x00));	    // WinCE Force Firmware Update
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMajor);								    // CPLD Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, CPLDVersionMinor);								    // CPLD Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, (ForceCPLDUpdate ? (byte)0x01 : (byte)0x00));	    // CPLD Force Firmware Update
                        #endregion
                        break;
                }
                bRet = oMS.ToArray();
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "BuildOutgoingPacket()", ex);
            }
            return bRet;
        }
        public byte[] GetPayloadFromHostPacket(byte[] bData, ref int iPacketNumber, ref int iPacketTotal)
        {
            byte[] bRet = null;
            MemoryStream oMS = null;
            byte bTemp = (byte)0x00;
            ushort usTemp = 0;
            string sErrorMsg = "";
            try
            {
                if (oLogging != null && _bLogInbound) oLogging.Info("Reading Host Data -> " + BitConverter.ToString(bData, 0));
                oMS = new MemoryStream(bData);
                #region Read the packet header [Protocol Ver][Length][Packet Num][Packet Total]
                PacketUtilities.ReadFromStream(oMS, ref bProtocolVer);									// Protocol Ver
                PacketUtilities.ReadFromStream(oMS, ref usTemp);										// Length
                if ((int)usTemp != bData.Length - 7)
                {
                    if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : Packet is not the correct length - Byte Data " + BitConverter.ToString(bData, 0));
                    throw new System.Exception("Packet is not the correct length");
                }
                if (bData[usTemp + 5] != (byte)0x07)
                {
                    if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : SBS was not found in the correct place - Byte Data " + BitConverter.ToString(bData, 0));
                    throw new System.Exception("SBS was not found in the correct place");
                }
                if (bData[usTemp + 6] != (byte)0x03)
                {
                    if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : EOP was not found in the correct place - Byte Data " + BitConverter.ToString(bData, 0));
                    throw new System.Exception("EOP was not found in the correct place");
                }
                PacketUtilities.ReadFromStream(oMS, ref bPacketNum);											// Packet Number
                PacketUtilities.ReadFromStream(oMS, ref bPacketTotal);											// Total Packets
                iPacketNumber = (int)bPacketNum;
                iPacketTotal = (int)bPacketTotal;
                #endregion
                #region Read the payload data
                bRet = new byte[(int)usTemp];
                for (int X = 0; X < bRet.Length; X++)
                {
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);
                    bRet[X] = bTemp;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "TranslateMessageFromHost(byte[] bData)", ex);
                sErrorMsg = ex.Message;
                bRet = null;
            }
            #region If there was an error parsing the packet, raise the exception to the next level
            if (sErrorMsg != "")
                throw new System.Exception(sErrorMsg);
            #endregion
            return bRet;
        }
        public string TranslateMsgFromHost(byte[] bCompletePayload)
        {
            string sRet = "";
            byte bTemp = (byte)0x00;
            byte[] baTemp = null;
            MemoryStream oMS = null;
            int iTemp = 0;
            int iCurID = 0;
            string sTemp = "";
            ushort usTemp = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            //uint iLat = 0;
            uint uiTemp = 0;

            try
            {
                oMS = new MemoryStream(bCompletePayload);
                #region Read the packet Payload
                PacketUtilities.ReadFromStream(oMS, ref bTemp);											        // Command Type
                CommandType = Convert.ToString((char)bTemp);
                PacketUtilities.ReadFromStream(oMS, ref MobileID);									            // Mobile ID
                switch (CommandType)
                {
                    case "B":
                        #region Predefined Message List - [Message List ID][Message List Ver][Message Count] [Message Num][Message Text][TSC]
                        if (oLogging != null && _bLogInbound) oLogging.Info("Reading Predefined Message List Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.ReadFromStream(oMS, ref ListID);								        // List ID
                        PacketUtilities.ReadFromStream(oMS, ref ListVersion);							        // List Version
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // List Count
                        iTemp = (int)bTemp;
                        for (int X = 0; X < iTemp; X++)
                        {
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);									    // Message Num
                            oListIDs.Add(bTemp);
                            sTemp = "";
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);		                    // Message Text
                            oListValues.Add(sTemp);
                        }
                        #endregion
                        break;
                    case "C":
                        #region Send Type List  - [Sub Command][List Version][List Count][ESC][Location][Item ID][ISC][Item Text]
                        if (oLogging != null && _bLogInbound) oLogging.Info("Reading List Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        //  Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "F" || SubCommandType == "G")
                            PacketUtilities.ReadFromStream(oMS, ref AddressListID);								// List ID
                        if (SubCommandType == "D")
                            PacketUtilities.ReadFromStream(oMS, ref DivisionGroupID);
                        if ((int)bProtocolVer > 2)
                        {
                            if (SubCommandType == "E" || SubCommandType == "R" || SubCommandType == "P" || SubCommandType == "T" || SubCommandType == "V")
                                PacketUtilities.ReadFromStream(oMS, ref ListID);
                        }
                        PacketUtilities.ReadFromStream(oMS, ref ListVersion);							        // List Version
                        PacketUtilities.ReadFromStream(oMS, ref usTemp);								        // List Count
                        iTemp = (int)usTemp;
                        for (int X = 0; X < iTemp; X++)
                        {
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);									    // ESC
                            if (bTemp != 0x0A)
                            {
                                sRet = "Did not find ESC in the correct place";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                            if (SubCommandType == "F" || SubCommandType == "G")
                            {
                                #region Address List
                                PacketUtilities.ReadFromStream(oMS, ref iTemp);									    // Item ID
                                oAddressListIDs.Add(iTemp);
                                sTemp = "";
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                        // Name
                                oAddressListNames.Add(sTemp);
                                sTemp = "";
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                        // Address
                                oAddressListAddresses.Add(sTemp);
                                sTemp = "";
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                        // Comments
                                oAddressListComment.Add(sTemp);
                                PacketUtilities.ReadFromStream(oMS, ref uiTemp);	                                // Latitude
                                oAddressListLats.Add(ConvLatLon(uiTemp));
                                PacketUtilities.ReadFromStream(oMS, ref uiTemp);	                                // Latitude
                                oAddressListLongs.Add(ConvLatLon(uiTemp));
                                #endregion
                            }
                            else
                            {
                                #region Read the other types of list packet
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);					                    // List Location
                                oListLocations.Add(bTemp);
                                if (SubCommandType == "A" || SubCommandType == "B")
                                {
                                    sTemp = "";
                                    PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                    // Item ID
                                    oListIDs.Add(sTemp);
                                }
                                else
                                {
                                    PacketUtilities.ReadFromStream(oMS, ref iCurID);	                                // Item ID
                                    oListIDs.Add(iCurID);
                                }
                                sTemp = "";
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);		                    // Item Text
                                oListValues.Add(sTemp);
                                if (SubCommandType == "T")
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)6, ref baTemp);			            // ESN
                                    oListESNs.Add(baTemp);
                                }
                                #endregion
                            }
                        }
                        #endregion
                        break;
                    case "c":
                        #region Download CPLD Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        //  Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "H")
                        {
                            #region Download Header
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading CPLD Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                //  Firmware Version Major
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                //  Firmware Version Minor
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegments);	                    //  Segment Count
                            PacketUtilities.ReadFromStream(oMS, ref ProgramTotalCheckSum);                           // Program Checksum
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Download Packet Data
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading CPLD Download Data Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                //  Firmware Version Major
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                //  Firmware Version Minor
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegments);	                    //  Segment Count
                            PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);	                            //  Segment Number
                            PacketUtilities.ReadFromStream(oMS, ref usTemp);	                                //  Segment Length
                            PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref DownloadCEParcel);            //  Segment
                            PacketUtilities.ReadFromStream(oMS, ref usTemp);	                                        //  Segment Checksum
                            #endregion
                        }
                        else if (SubCommandType == "V")
                        {
                            #region Download Verify
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading CPLD Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref DownloadVersion);                                // Download Version
                            PacketUtilities.ReadFromStream(oMS, ref ProgramTotalCheckSum);                           // Program Checksum
                            PacketUtilities.ReadFromStream(oMS, ref PacketTotalCheckSum);                            // Total Checksum
                            PacketUtilities.ReadFromStream(oMS, ref ProgramByteTotal);                               // Program Byte Total
                            PacketUtilities.ReadFromStream(oMS, ref ProgramDataBytesCheckSum);                       // Program Data Bytes checksum
                            #endregion
                        }
                        #endregion
                        break;
                    case "d":
                        #region Download Win CE Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        //  Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "H")
                        {
                            #region Download Header
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading WinCE Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                //  Firmware Version Major
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                //  Firmware Version Minor
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegments);	                    //  Segment Count
                            PacketUtilities.ReadFromStream(oMS, ref ProgramTotalCheckSum);                           // Program Checksum
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Download Packet Data
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading WinCE Download Data Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                //  Firmware Version Major
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                //  Firmware Version Minor
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegments);	                    //  Segment Count
                            PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);	                            //  Segment Number
                            PacketUtilities.ReadFromStream(oMS, ref usTemp);	                                //  Segment Length
                            PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref DownloadCEParcel);            //  Segment
                            PacketUtilities.ReadFromStream(oMS, ref usTemp);	                                        //  Segment Checksum
                            #endregion
                        }
                        else if (SubCommandType == "V")
                        {
                            #region Download Verify
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading WinCE Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref DownloadVersion);                                // Download Version
                            PacketUtilities.ReadFromStream(oMS, ref ProgramTotalCheckSum);                           // Program Checksum
                            PacketUtilities.ReadFromStream(oMS, ref PacketTotalCheckSum);                            // Total Checksum
                            PacketUtilities.ReadFromStream(oMS, ref ProgramByteTotal);                               // Program Byte Total
                            PacketUtilities.ReadFromStream(oMS, ref ProgramDataBytesCheckSum);                       // Program Data Bytes checksum
                            #endregion
                        }
                        #endregion
                        break;
                    case "D":
                        #region Download MOT Packets - Download Header Packet - [Sub Command][Firmware Version Major][Firmware Version Minor][Segment Count] / Download Data Packet - [Sub Command][Segment Number][Segment Length][Data Segment]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        //  Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "h")
                        {
                            #region Download Header
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Mot Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                //  Firmware Version Major
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                //  Firmware Version Minor
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegments);	                    //  Segment Count
                            PacketUtilities.ReadFromStream(oMS, ref ProgramTotalCheckSum);                           // Program Checksum
                            #endregion
                        }
                        else if (SubCommandType == "d")
                        {
                            #region Download Packet Data
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Mot Download Data Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);	                    // Firmware Ver Major (1 Byte)
                            PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);	                    // Firmware Ver Minor (1 Byte)
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegments);	                    // Number of Segements (2 Bytes)
                            PacketUtilities.ReadFromStream(oMS, ref NumberOfDataSegmentsInThisPacket);          // Number of segments in this packet (1 Byte) 
                            DownloadMotParcel = new ArrayList();
                            for (ushort i = 0; i < (ushort)NumberOfDataSegmentsInThisPacket; i++)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);						        // PacketNumber (2 Bytes)
                                PacketUtilities.ReadFromStream(oMS, (int)250, ref baTemp);		                // Segment Data (250 Bytes)
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);						        // Segment Checksum (2 Bytes)
                                cParcel oParcel = new cParcel(baTemp, usTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							        // End of Segment (1 Byte)
                                if (bTemp != (byte)0xBB)
                                {
                                    sRet = "End of data segment was not found in the correct place.";
                                    if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                    return sRet;
                                }
                            }
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // End Data Packet (1 Byte)
                            if (bTemp != (byte)0xCC)
                            {
                                sRet = "End of dowload data byte was not found in the correct place.";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                            #endregion
                        }
                        else if (SubCommandType == "v")
                        {
                            #region Download Verify
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Mot Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.ReadFromStream(oMS, ref DownloadVersion);			                //  Download Version
                            PacketUtilities.ReadFromStream(oMS, ref ProgramTotalCheckSum);	                    //  Program Total CheckSum
                            PacketUtilities.ReadFromStream(oMS, ref PacketTotalCheckSum);	                    //  Packet Total CheckSum
                            PacketUtilities.ReadFromStream(oMS, ref ProgramPacketCountTotal);		            //  Program Packet Count Total
                            PacketUtilities.ReadFromStream(oMS, ref ProgramByteTotal);			                //  Program Byte Total
                            PacketUtilities.ReadFromStream(oMS, ref ProgramDataBytesCheckSum);	                //  Program Data Bytes Check Sum
                            #endregion
                        }
                        #endregion
                        break;
                    case "E":
                        #region New Job / Cancel Job / Delete Job / Update Job Packets.
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        //  Sub Command Type
                        PacketUtilities.ReadFromStream(oMS, ref AllocationID);							        // Allocation ID
                        SubCommandType = Convert.ToString((char)bTemp);
                        PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);							        // Job Dispatch ID
                        PacketUtilities.ReadFromStream(oMS, ref JobID);									        // Job Reference
                        if (SubCommandType == "N" || SubCommandType == "U")
                        {
                            #region New Job Packet / Update Job Packet
                            if (oLogging != null && _bLogOutbound)
                            {
                                if (SubCommandType == "N")
                                    oLogging.Info("Reading New Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                                else
                                    oLogging.Info("Reading Update Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            }
                            //New Job / Update Job
                            //[Sub Command][Job Dispatch ID][Job Reference][Job Num][Leg Count][Job Title/Ext Ref][TSC]
                            //[Pickup Location Name][TSC][Pickup Location Address][TSC][Phone Number][TSC]
                            //[Pickup Lat][Pickup Long][Pickup Radius][Pickup Time][Time Tolerance][Pickup Depart Time][Time Tolerance]
                            //[Division ID][Load Weight][Dangerous Goods][Pickup Notes][TSC][Email Flags][Email At Distance]
                            //[SPS][Pallet Type][Pallet Count][EPS][STS][Trailer ID] [ETS]

                            if (oLogging != null && _bLogInbound)
                            {
                                if (SubCommandType == "N")
                                    oLogging.Info("Reading New Job Packet - Job Ref = " + Convert.ToString(JobID));
                                else
                                    oLogging.Info("Reading Update Job Packet - Job Ref = " + Convert.ToString(JobID));
                            }
                            JobTitle = "";
                            JobNotes = "";
                            LocationName = "";
                            LocationAddress = "";
                            LocationPhoneNumber = "";

                            PacketUtilities.ReadFromStream(oMS, ref JobNum);							        // Job Number
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Auto Pickup Job
                            if (bTemp == (byte)0x00)
                                AutoPickupJob = false;
                            else
                                AutoPickupJob = true;
                            PacketUtilities.ReadFromStream(oMS, ref LegCount);							        // Leg Count
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref JobTitle);		                // Job Title
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LocationName);	                // Pickup Location Name
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LocationAddress);	            // Pickup Location Address
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LocationPhoneNumber);	        // Pickup Location Phone Number
                            PacketUtilities.ReadFromStream(oMS, ref JobLatitude);					            // Pickup Lat
                            PacketUtilities.ReadFromStream(oMS, ref JobLongitude);					            // Pickup Long
                            PacketUtilities.ReadFromStream(oMS, ref JobRadius);						            // Pickup Radius
                            #region Read the arrive time
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Day
                            iDay = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Month
                            iMonth = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Year
                            iYear = (int)bTemp;
                            iYear = iYear + 2000;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Hour
                            iHour = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Minute
                            iMinute = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Second
                            iSecond = (int)bTemp;
                            try
                            {
                                if (iDay > 0 && iMonth > 0)
                                {
                                    ArriveTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                                }
                                else
                                {
                                    ArriveTime = DateTime.MinValue;
                                }
                            }
                            catch (System.Exception exDateConvert)
                            {
                                sRet = "Failed to convert arrive time to a valid datetime - Day = " + Convert.ToString(iDay) + ", Month = " + Convert.ToString(iMonth) + ", Year = " + Convert.ToString(iYear) + ", Hour = " + Convert.ToString(iHour) + ", Minute = " + Convert.ToString(iMinute) + ", Second = " + Convert.ToString(iSecond);
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0) + "\r\n" + exDateConvert.Message);
                                return sRet;
                            }
                            PacketUtilities.ReadFromStream(oMS, ref ArriveTimeTolerance);						// Arrive Time Tolerance
                            #endregion
                            #region Read the depart time
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Day
                            iDay = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Month
                            iMonth = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Year
                            iYear = (int)bTemp;
                            iYear = iYear + 2000;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Hour
                            iHour = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Minute
                            iMinute = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Second
                            iSecond = (int)bTemp;
                            try
                            {
                                if (iDay > 0 && iMonth > 0)
                                {
                                    DepartTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                                }
                                else
                                {
                                    DepartTime = DateTime.MinValue;
                                }
                            }
                            catch (System.Exception exDateConvert)
                            {
                                sRet = "Failed to convert depart time to a valid datetime - Day = " + Convert.ToString(iDay) + ", Month = " + Convert.ToString(iMonth) + ", Year = " + Convert.ToString(iYear) + ", Hour = " + Convert.ToString(iHour) + ", Minute = " + Convert.ToString(iMinute) + ", Second = " + Convert.ToString(iSecond);
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0) + "\r\n" + exDateConvert.Message);
                                return sRet;
                            }
                            PacketUtilities.ReadFromStream(oMS, ref DepartTimeTolerance);						// Depart Time Tolerance

                            #endregion
                            PacketUtilities.ReadFromStream(oMS, ref DivisionID);						        // Division ID
                            PacketUtilities.ReadFromStream(oMS, ref LoadWeight);					            // Load Weight
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Dangerous Goods
                            if (bTemp == (byte)0x00)
                                IsDangerousGoods = false;
                            else
                                IsDangerousGoods = true;
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref JobNotes);	                    // Pickup Notes
                            PacketUtilities.ReadFromStream(oMS, ref JobEmailFlags);				                // Email Flags
                            PacketUtilities.ReadFromStream(oMS, ref EmailAtDistance);			                // Email At Distance
                            #region Read Pallet List
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								// SPS or EPS
                            while (bTemp != (byte)0x0B && oMS.Position < bCompletePayload.Length)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref iTemp);						// Pallet Type ID
                                PalletTypeList.Add(iTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						// Pallet Count
                                PalletCount.Add(bTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							// SPS or EPS
                            }
                            if (oMS.Position >= bCompletePayload.Length)
                            {
                                sRet = "Did not find SPS or EPS in the correct position";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                            #endregion
                            #region Read Trailer List
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // STS or ETS
                            while (bTemp != (byte)0x0D && oMS.Position < bCompletePayload.Length)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref iTemp);	                                // Trailer ID
                                TrailerList.Add(iTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // STS or ETS
                            }
                            #endregion
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LoadType);	                    // Load Type String
                            #endregion
                        }
                        else if (SubCommandType == "C")
                        {
                            #region Cancel Job Packet - [Sub Command][Job Reference][Job Cancel Message][TSC]
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Cancel Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            JobCancelMessage = "";
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref JobCancelMessage);	            // Job Cancel Message
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Delete Job Packet
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Delete Job Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            #endregion
                        }
                        #endregion
                        break;
                    case "F":
                        #region New Job Leg / Cancel Job Leg / Delete Job Leg / Update Job Leg Packet
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        PacketUtilities.ReadFromStream(oMS, ref AllocationID);							        // Allocation ID
                        PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);							        // Job Dispatch ID
                        SubCommandType = Convert.ToString((char)bTemp);
                        PacketUtilities.ReadFromStream(oMS, ref JobID);									        // Job Reference
                        PacketUtilities.ReadFromStream(oMS, ref LegNumber);							            // Leg Number
                        if (SubCommandType == "N" || SubCommandType == "U" || SubCommandType == "R")
                        {
                            #region New Job Packet / Update Job Packet
                            if (oLogging != null && _bLogOutbound)
                            {
                                if (SubCommandType == "N")
                                    oLogging.Info("Reading New Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                                else
                                    oLogging.Info("Reading Update Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            }
                            //New Job Leg / Update Job Leg
                            //[Sub Command][Job Dispatch ID][Job Reference][Leg Num][Leg Description][TSC][Location Name][TSC]
                            //[Location Address][TSC][Phone Number][TSC][Lat][Long][Radius][Arrive Time][Time Tolerance][Depart Time][Time Tolerance]
                            //[Delivery Notes][TSC][POD Required][Email Flags][Email At Distance]
                            //[SPS][Pallet Type][Pallet Count] [EPS][Exchange Pallet Order][TSC][SEPS][Pallet Type][Pallet Count] [EEPS]
                            if (oLogging != null && _bLogInbound)
                            {
                                if (SubCommandType == "N")
                                    oLogging.Info("Reading New Job Leg Packet - Job Ref = " + Convert.ToString(JobID));
                                else
                                    oLogging.Info("Reading Update Job Leg Packet - Job Ref = " + Convert.ToString(JobID));
                            }
                            LegDescription = "";
                            LocationName = "";
                            LocationAddress = "";
                            LocationPhoneNumber = "";
                            JobNotes = "";
                            ExchangePalletOrder = "";
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // AutoDeliverJob
                            if (bTemp == (byte)0x00)
                                AutoDeliverJob = false;
                            else
                                AutoDeliverJob = true;
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LegDescription);		        // Leg Description
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LocationName);		            // Location Name
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LocationAddress);	            // Location Address
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LocationPhoneNumber);	        // Location Phone Number
                            PacketUtilities.ReadFromStream(oMS, ref JobLatitude);					            // Lat
                            PacketUtilities.ReadFromStream(oMS, ref JobLongitude);					            // Long
                            PacketUtilities.ReadFromStream(oMS, ref JobRadius);						            // Radius
                            #region Read the arrive time
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Day
                            iDay = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Month
                            iMonth = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Year
                            iYear = (int)bTemp;
                            iYear = iYear + 2000;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Hour
                            iHour = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Minute
                            iMinute = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Arrive Date - Second
                            iSecond = (int)bTemp;
                            try
                            {
                                if (iDay > 0 && iMonth > 0)
                                {
                                    ArriveTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                                }
                                else
                                {
                                    ArriveTime = DateTime.MinValue;
                                }
                            }
                            catch (System.Exception exDateConvert)
                            {
                                sRet = "Failed to convert arrive time to a valid datetime - Day = " + Convert.ToString(iDay) + ", Month = " + Convert.ToString(iMonth) + ", Year = " + Convert.ToString(iYear) + ", Hour = " + Convert.ToString(iHour) + ", Minute = " + Convert.ToString(iMinute) + ", Second = " + Convert.ToString(iSecond);
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0) + "\r\n" + exDateConvert.Message);
                                return sRet;
                            }
                            PacketUtilities.ReadFromStream(oMS, ref ArriveTimeTolerance);						// Arrive Time Tolerance
                            #endregion
                            #region Read the depart time
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Day
                            iDay = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Month
                            iMonth = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Year
                            iYear = (int)bTemp;
                            iYear = iYear + 2000;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Hour
                            iHour = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Minute
                            iMinute = (int)bTemp;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // Depart Date - Second
                            iSecond = (int)bTemp;
                            try
                            {
                                if (iDay > 0 && iMonth > 0)
                                {
                                    DepartTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                                }
                                else
                                {
                                    DepartTime = DateTime.MinValue;
                                }
                            }
                            catch (System.Exception exDateConvert)
                            {
                                sRet = "Failed to convert depart time to a valid datetime - Day = " + Convert.ToString(iDay) + ", Month = " + Convert.ToString(iMonth) + ", Year = " + Convert.ToString(iYear) + ", Hour = " + Convert.ToString(iHour) + ", Minute = " + Convert.ToString(iMinute) + ", Second = " + Convert.ToString(iSecond);
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0) + "\r\n" + exDateConvert.Message);
                                return sRet;
                            }
                            PacketUtilities.ReadFromStream(oMS, ref DepartTimeTolerance);						// Depart Time Tolerance
                            #endregion
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref JobNotes);	                    // Delivery Notes
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // POD Required
                            if (bTemp == (byte)0x00)
                                IsPODRequired = false;
                            else
                                IsPODRequired = true;
                            PacketUtilities.ReadFromStream(oMS, ref JobEmailFlags);				                // Email Flags
                            PacketUtilities.ReadFromStream(oMS, ref EmailAtDistance);			                // Email At Distance
                            #region Read the pallet list
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SPS or EPS
                            while (bTemp == (byte)0x0A && oMS.Position < bCompletePayload.Length)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                PalletTypeList.Add(iTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                PalletCount.Add(bTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // SPS or EPS
                            }
                            if (oMS.Position >= bCompletePayload.Length)
                            {
                                sRet = "Did not find SPS or EPS in the correct position";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                            #endregion
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ExchangePalletOrder);	        // Exchange Pallet Order
                            #region Read the exchange pallet list
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SEPS or EEPS
                            while (bTemp == (byte)0x0D && oMS.Position < bCompletePayload.Length)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                ExchangePalletTypeList.Add(iTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                ExchangePalletCount.Add(bTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SEPS or EEPS
                            }
                            if (oMS.Position > bCompletePayload.Length)
                            {
                                sRet = "Did not find SEPS or EEPS in the correct position";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                            #endregion
                            #endregion
                        }
                        else if (SubCommandType == "C")
                        {
                            #region Cancel Job Leg
                            //[Sub Command][Job Dispatch ID][Job Reference][Leg Num][Job Cancel Message][TSC]
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Cancel Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            JobCancelMessage = "";
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref JobCancelMessage);	            // Leg Cancel Message
                            #endregion
                        }
                        else if (SubCommandType == "D")
                        {
                            #region Delete Job Leg
                            //[Sub Command][Job Dispatch ID][Job Reference][Leg Num]
                            if (oLogging != null && _bLogInbound) oLogging.Info("Reading Delete Job Leg Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Job Ref = " + Convert.ToString(JobID));
                            #endregion
                        }
                        #endregion
                        break;
                    case "L":
                        #region Login Reply  - [Login Response][Firmware Major][Firmware Minor][Force Update][New Pre-Trip List][GMT Offset Mins][Driver Name][TSC][Vehicle Type][TSC]
                        if (oLogging != null && _bLogInbound) oLogging.Info("Reading Login Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        LoginResponse = Convert.ToString((char)bTemp);
                        PacketUtilities.ReadFromStream(oMS, ref AllowFreeDial);		                            // Allow Free Dial
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                    // Firmware Version Major
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                    // Firmware Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Force Firmware Update
                        if (bTemp == (byte)0x00)
                            ForceFirmwareUpdate = false;
                        else
                            ForceFirmwareUpdate = true;
                        PacketUtilities.ReadFromStream(oMS, ref CPLDVersionMajor);		                        // CPLD Version Major
                        PacketUtilities.ReadFromStream(oMS, ref CPLDVersionMinor);		                        // CPLD Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Force CPLD Update
                        if (bTemp == (byte)0x00)
                            ForceCPLDUpdate = false;
                        else
                            ForceCPLDUpdate = true;
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // New Pre-Trip List
                        if (bTemp == (byte)0x00)
                            NewPreTripList = false;
                        else
                            NewPreTripList = true;
                        PacketUtilities.ReadFromStream(oMS, ref GMTOffsetMins);		                            // GMT Offset Mins
                        PacketUtilities.ReadFromStream(oMS, ref DriverID);								        // Driver ID
                        DriverName = "";
                        VehicleType = "";
                        RouteURLWebService = "";

                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref DriverName);	                    // Driver Name
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref VehicleType);	                    // Vehicle Type
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref RouteURLWebService);	            // RouteURLWebService
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref LoadType);	                        // Load Type String

                        if (AllowFreeDial == (byte)0x01)
                            oLogging.Info("Read Login Reply Packet for mobile ID " + Convert.ToString(MobileID) + " - Response = " + Convert.ToString(LoginResponse) + ", Allow Free Dial = true, Driver ID = " + Convert.ToString(DriverID) + ", Driver Name = '" + DriverName + "'");
                        else
                            oLogging.Info("Read Login Reply Packet for mobile ID " + Convert.ToString(MobileID) + " - Response = " + Convert.ToString(LoginResponse) + ", Allow Free Dial = false, Driver ID = " + Convert.ToString(DriverID) + ", Driver Name = '" + DriverName + "'");
                        #endregion
                        break;
                    case "M":
                        #region Message Packet - [Message Text][TSC][Requires Reply]
                        if (oLogging != null && _bLogInbound) oLogging.Info("Reading Message Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.ReadFromStream(oMS, ref MessageID);									    // Message ID
                        MessageText = "";
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref MessageText);	                    // Driver Name
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);										    // Message Requires a Reply
                        if (bTemp == (byte)0x00)
                            RequiresReply = false;
                        else
                            RequiresReply = true;
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);										    // Location Attached
                        if (bTemp == (byte)0x01)
                        {
                            sTemp = "";
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                        // Name
                            oAddressListNames.Add(sTemp);
                            sTemp = "";
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                        // Address
                            oAddressListAddresses.Add(sTemp);
                            sTemp = "";
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                        // Phonenumber
                            oAddressListComment.Add(sTemp);
                            PacketUtilities.ReadFromStream(oMS, ref uiTemp);	                                // Latitude
                            oAddressListLats.Add(ConvLatLon(uiTemp));
                            PacketUtilities.ReadFromStream(oMS, ref uiTemp);	                                // Latitude
                            oAddressListLongs.Add(ConvLatLon(uiTemp));
                        }
                        #endregion
                        break;
                    case "O":
                        #region Remote Logout Packet -
                        if (oLogging != null && _bLogInbound) oLogging.Info("Reading Remote Logout Packet for mobile ID " + Convert.ToString(MobileID));
                        #endregion
                        break;
                    case "P":
                        #region List Packet - [Entry List ID][Entry List Ver][Signature Required][Question Count][SQS][Question Text][TSC][Answer Type][EQS]
                        if (oLogging != null && _bLogOutbound)
                        {
                            if (SubCommandType == "A")
                                oLogging.Info("Reading Pre-Trip Question List Packet for mobile ID " + Convert.ToString(MobileID));
                            else if (SubCommandType == "B")
                                oLogging.Info("Reading Post-Trip Question List Packet for mobile ID " + Convert.ToString(MobileID));
                            else if (SubCommandType == "D")
                                oLogging.Info("Reading Delivery Question List Packet for mobile ID " + Convert.ToString(MobileID));
                            else if (SubCommandType == "P")
                                oLogging.Info("Reading Pickup Question List Packet for mobile ID " + Convert.ToString(MobileID));
                            else if (SubCommandType == "P")
                                oLogging.Info("Reading Fuel Station List Packet for mobile ID " + Convert.ToString(MobileID));
                            else
                                oLogging.Info("Reading Unknown List Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        //  Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        PacketUtilities.ReadFromStream(oMS, ref EntryListID);						            // Entry List ID
                        PacketUtilities.ReadFromStream(oMS, ref EntryListVer);					                // Entry List Ver
                        if (SubCommandType == "A" || SubCommandType == "B" || SubCommandType == "D" || SubCommandType == "P")
                        {
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Signature Required
                            if (bTemp == (byte)0x00)
                                SignatureRequired = false;
                            else
                                SignatureRequired = true;
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // List Count
                            iTemp = (int)bTemp;
                            for (int X = 0; X < iTemp; X++)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);									    // SES
                                if (bTemp != (byte)0x0A)
                                {
                                    sRet = "Failed to find SES in the correct position";
                                    if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                    return sRet;
                                }
                                sTemp = "";
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);			                // Question Text
                                PreTripQuestionList.Add(sTemp);
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);									    // Answer Type
                                sTemp = Convert.ToString((char)bTemp);
                                PreTripQuestionAnswerType.Add(sTemp);
                                if (SubCommandType == "A" || SubCommandType == "B")
                                {
                                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									// Check Answer
                                    PreTripCheckAnswer.Add((int)bTemp);
                                    PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);			            // Incorrect Answer Msg
                                    PreTripIncorrectAnswerMsg.Add(sTemp);
                                }
                            }
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);										    //	EES
                            if (bTemp != (byte)0x0B)
                            {
                                sRet = "Failed to find EES in the correct position";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                        }
                        else if (SubCommandType == "F")
                        {
                            #region Location Lists
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // List Count
                            iTemp = (int)bTemp;
                            for (int X = 0; X < iTemp; X++)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);									    // SES
                                if (bTemp != (byte)0x0A)
                                {
                                    sRet = "Failed to find SES in the correct position";
                                    if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                    return sRet;
                                }
                                LocationEntry oLocation = new LocationEntry();
                                PacketUtilities.ReadFromStream(oMS, ref oLocation.ID);					            // Location ID
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref oLocation.LocationName);	    // Location Name
                                uiTemp = 0;
                                PacketUtilities.ReadFromStream(oMS, ref uiTemp);	                                    // Location Lat
                                oLocation.iLat = uiTemp;
                                PacketUtilities.ReadFromStream(oMS, ref uiTemp);	                                    // Location Long
                                oLocation.iLong = uiTemp;
                                PacketUtilities.ReadFromStream(oMS, ref oLocation.Tolerance);					    // Location Tolerance
                            }
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);										    //	EES
                            if (bTemp != (byte)0x0B)
                            {
                                sRet = "Failed to find EES in the correct position";
                                if (oLogging != null) oLogging.Info("Inbound Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bCompletePayload, 0));
                                return sRet;
                            }
                            #endregion
                        }
                        #endregion
                        break;
                    case "Q":
                        #region Start Pre-defined Function Packet - [Function ID][Start / Stop][Parameter Length][Function Parameters][EOP]

                        if (oLogging != null && _bLogInbound) oLogging.Info("Building Predefined Function Packet for mobile ID " + Convert.ToString(MobileID));

                        PacketUtilities.ReadFromStream(oMS, ref FunctionID);									    // Function ID
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									            // Start / Stop
                        if (bTemp == (byte)0x00)
                            StartFunction = false;
                        else
                            StartFunction = true;
                        PacketUtilities.ReadFromStream(oMS, ref usTemp);									        // Function Parameters Length
                        FunctionParamers = new byte[(int)usTemp];
                        PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref FunctionParamers);					    // Function Parameters
                        #endregion
                        break;
                    case "V":
                        #region Trailer Query Reply Packet  - [Trailer ID] [Trailer Name][TSC][Trailer ESN]
                        if (oLogging != null && _bLogInbound) oLogging.Info("Reading Trailer Query Reply Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.ReadFromStream(oMS, ref TrailerID);						                // Trailer ID
                        TrailerName = "";
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref TrailerName);	                    // Trailer Name
                        TrailerESN = new byte[6];
                        PacketUtilities.ReadFromStream(oMS, (int)6, ref TrailerESN);						    // Trailer ESN
                        #endregion
                        break;
                    case "v":
                        #region Force Update Packet - [MDT Firmware Major][ MDTFirmware Minor][Force Update][WinCE Major][WinCE Minor][Force Update][NEC Major][NEC Minor][NEC Force Update]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Forced Update Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);								// Firmware Version Major
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);								// Firmware Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Force Firmware Update
                        if (bTemp == (byte)0x00)
                            ForceFirmwareUpdate = false;
                        else
                            ForceFirmwareUpdate = true;
                        PacketUtilities.ReadFromStream(oMS, ref WinCEVersionMajor);		                        // WinCE Version Major
                        PacketUtilities.ReadFromStream(oMS, ref WinCEVersionMinor);		                        // WinCE Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Force WinCE Update
                        if (bTemp == (byte)0x00)
                            ForceWinCEUpdate = false;
                        else
                            ForceWinCEUpdate = true;
                        PacketUtilities.ReadFromStream(oMS, ref CPLDVersionMajor);		                        // CPLD Version Major
                        PacketUtilities.ReadFromStream(oMS, ref CPLDVersionMinor);		                        // CPLD Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Force CPLD Update
                        if (bTemp == (byte)0x00)
                            ForceCPLDUpdate = false;
                        else
                            ForceCPLDUpdate = true;
                        #endregion
                        break;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "TranslateMessageFromHost(byte[] bData)", ex);
            }
            return sRet;
        }
        #endregion
        #region Messages to the Host
        public byte[] BuildMsgToHost()
        {
            byte[] bRet = null;
            byte[] bConvert = null;
            MemoryStream oMS = null;
            int iListCount = 0;
            int iTemp = 0;
            uint uiTemp = 0;
            try
            {

                oMS = new MemoryStream();
                #region Create the packet header [Protocol Version][Length][Data][SBS][EOP]
                PacketUtilities.WriteToStream(oMS, bProtocolVer);					                            // Protocol Version
                PacketUtilities.WriteToStream(oMS, (short)0);						                            // Length
                #endregion
                if (CommandType != "c" && CommandType != "d" && CommandType != "D" && CommandType != "T")
                {
                    #region Create the [Data] header [Command Type][Fleet ID][Vehicle ID][Driver ID][GPS][Packet Data]
                    PacketUtilities.WriteToStream(oMS, CommandType);				                            // Command Type
                    PacketUtilities.WriteToStream(oMS, FleetID);						                        // Fleet ID
                    PacketUtilities.WriteToStream(oMS, VehicleID);					                            // Vehicle ID
                    PacketUtilities.WriteToStream(oMS, DriverID);						                        // Driver ID
                    #region Create GPS Data - [LSC][Lat][Long][GPS Time][Speed][Heading][Device Timer]
                    PacketUtilities.WriteToStream(oMS, (byte)0x12);					                        // LSC
                    PacketUtilities.WriteToStream(oMS, VehicleLatitude);			                            // Latitude
                    PacketUtilities.WriteToStream(oMS, VehicleLongitude);		                                // Longitude
                    bConvert = new byte[6];
                    if (!VehicleGPSTime.Equals(DateTime.MinValue))
                    {
                        bConvert[0] = (byte)VehicleGPSTime.Day;
                        bConvert[1] = (byte)VehicleGPSTime.Month;
                        bConvert[2] = (byte)(VehicleGPSTime.Year - 2000);
                        bConvert[3] = (byte)VehicleGPSTime.Hour;
                        bConvert[4] = (byte)VehicleGPSTime.Minute;
                        bConvert[5] = (byte)VehicleGPSTime.Second;
                    }
                    PacketUtilities.WriteToStream(oMS, bConvert);						                        // GPSTime
                    PacketUtilities.WriteToStream(oMS, VehicleSpeed);				                            // Speed
                    PacketUtilities.WriteToStream(oMS, VehicleHeading);			                                // Heading
                    PacketUtilities.WriteToStream(oMS, VehicleDeviceTimer);			                            // Device Time
                    #endregion
                    #endregion
                }
                switch (CommandType)
                {
                    case "A":
                        #region Arrive Site Packet - [Sub Command][Job Dispatch ID][Job Reference][Leg Num][Odometer][Total Fuel Used]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Arrive Site Packet Packet for mobile ID " + Convert.ToString(MobileID) + " - JobDispatchID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber) + ", Job Ref = " + Convert.ToString(JobID));
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Command Type
                        PacketUtilities.WriteToStream(oMS, JobDispatchID);									    // Job Dispatch ID
                        PacketUtilities.WriteToStream(oMS, JobID);											    // Job Reference Number
                        PacketUtilities.WriteToStream(oMS, LegNumber);									        // Leg Number
                        PacketUtilities.WriteToStream(oMS, Odometer);									        // Odometer
                        PacketUtilities.WriteToStream(oMS, TotalFuelUsed);									    // Total Fuel Used
                        #endregion
                        break;
                    case "B":
                        #region Driver Address Maintenance
                        // New Address (N) 
                        // Update Driver Address (U)
                        //      [Sub Command][Item ID][Name][TSC][Address][TSC][Comment][TSC][Lat][Long] 
                        // Delete Driver Address (D)
                        //      [Sub Command][Item ID] 
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Driver Address Maintenance Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Sub Command Type
                        if (SubCommandType == "N" || SubCommandType == "U")
                        {
                            PacketUtilities.WriteToStream(oMS, ItemID);	        // Item ID
                            PacketUtilities.WriteToStream(oMS, ItemName);     // Item Name
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            PacketUtilities.WriteToStream(oMS, ItemAddress);	// Item Address
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            PacketUtilities.WriteToStream(oMS, ItemComment);	// Item Comment
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                            uiTemp = ConvLatLon(ItemLatitude);
                            PacketUtilities.WriteToStream(oMS, uiTemp);									        // Latitude
                            uiTemp = ConvLatLon(ItemLongitude);
                            PacketUtilities.WriteToStream(oMS, uiTemp);									        // Longitude
                        }
                        else if (SubCommandType == "D")
                        {
                            PacketUtilities.WriteToStream(oMS, ItemID);									        // Item ID
                        }
                        else
                        {
                            if (oLogging != null) oLogging.Info("Unknown Driver Address Packet : Sub Command " + SubCommandType);
                        }
                        break;
                        #endregion
                    case "c":
                        #region Download CPLD Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Command Type
                        if (SubCommandType == "d")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building CPLD File Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "r")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building CPLD File Download Data Request Packet for mobile ID " + Convert.ToString(MobileID) + ", Segment Number = " + Convert.ToString(SegmentNumber));
                            PacketUtilities.WriteToStream(oMS, SegmentNumber);						            // Segment Number
                        }
                        else if (SubCommandType == "v")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building CPLD File Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        #endregion
                        break;
                    case "d":
                        #region Download Win CE Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Command Type
                        if (SubCommandType == "d")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building WinCE File Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "r")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building WinCE File Download Data Request Packet for mobile ID " + Convert.ToString(MobileID) + ", Segment Number = " + Convert.ToString(SegmentNumber));
                            PacketUtilities.WriteToStream(oMS, SegmentNumber);						            // Segment Number
                        }
                        else if (SubCommandType == "v")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building WinCE File Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        #endregion
                        break;
                    case "D":
                        #region Download MOT Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Command Type
                        if (SubCommandType == "d")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building MOT File Download Header Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "r")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building MOT File Download Data Request Packet for mobile ID " + Convert.ToString(MobileID) + ", Segment Number = " + Convert.ToString(SegmentNumber));
                            PacketUtilities.WriteToStream(oMS, SegmentNumber);						            // Segment Number
                        }
                        else if (SubCommandType == "v")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building MOT File Download Verify Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        #endregion
                        break;
                    case "E":
                        #region Fuel Report Packet -  [Fuel Station List ID][Fuel Station List Ver][Fuel Station ID][Cost Per Litre][Liters Pumped][Total Cost]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Fuel Report Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, FuelStationListID);						            // Fuel Station List ID
                        PacketUtilities.WriteToStream(oMS, FuelStationListVer);						            // Fuel Station List Ver
                        PacketUtilities.WriteToStream(oMS, FuelStationID);						                // Fuel Station ID
                        iTemp = Convert.ToInt32(dFuelCostPerLitre * 100);
                        PacketUtilities.WriteToStream(oMS, iTemp);						                        // Cost Per Litre
                        iTemp = Convert.ToInt32(dFuelLitresPumped * 100);
                        PacketUtilities.WriteToStream(oMS, iTemp);						                        // Liters Pumped
                        iTemp = Convert.ToInt32(dFuelTotalCost * 100);
                        PacketUtilities.WriteToStream(oMS, iTemp);						                        // Total Cost
                        PacketUtilities.WriteToStream(oMS, Odometer);					                        // Odometer
                        PacketUtilities.WriteToStream(oMS, TotalFuelUsed);				                        // Total Fuel Used
                        #endregion
                        break;
                    case "F":
                        #region Lunch Break Packet -  [Sub Command][Odometer][Total Fuel Used]
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Sub Command Type
                        if (SubCommandType == "S")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Start Lunch Break Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "F")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building End Lunch Break Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        PacketUtilities.WriteToStream(oMS, Odometer);									        // Odometer
                        PacketUtilities.WriteToStream(oMS, TotalFuelUsed);									    // Total Fuel Used
                        #endregion
                        break;
                    case "G":
                        #region Job Recieved Packet - [Allocation ID]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Allocation Ack Packet for mobile ID " + Convert.ToString(MobileID) + ", Allocation ID = " + Convert.ToString(AllocationID));
                        PacketUtilities.WriteToStream(oMS, AllocationID);						            // AllocationID
                        #endregion
                        break;
                    case "H":
                        #region Driver Channel Selection Packet - [Channel Selection][TSC]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Driver Channel Selection Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, ChannelSelection);						            // Channel Selection
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);									    // TSC
                        #endregion
                        break;
                    case "I":
                        #region Driver Login Packet - [Driver Login ID] [Driver PIN][Serial Number][Firmware Ver Major] [Firmware Ver Minor] [Hardware Type][Hardware Ver Major] [Hardware Ver Minor][Odometer][Total Fuel Used][Pallets on Truck][Global Phonebook Ver][Driver Phonebook Ver][Division Group ID][Division List Ver][Delay Reason List Ver][Rejection Reason List Ver][Pallet Type List Ver][Trailer List Ver][Usage List Ver][Predefined Msg List ID] [Predefined Msg List Ver][Pre-Trip Question List ID][Pre-Trip Question List Ver][Post-Trip Question List ID][Post-Trip Question List Ver][Pickup Question List ID][Pickup Question List Ver][Delivery Question List ID][Delivery Question List Ver][Fuel Station List ID][Fuel Station List Ver]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Driver Login Packet Packet for mobile ID " + Convert.ToString(MobileID) + ", Login ID = " + Convert.ToString(DriverLoginID) + ", Password = " + Convert.ToString(DriverPIN));
                        PacketUtilities.WriteToStream(oMS, DriverLoginID);							            // Driver Login ID
                        PacketUtilities.WriteToStream(oMS, DriverPIN);								            // Driver PIN
                        PacketUtilities.WriteToStream(oMS, SerialNumber);							            // Serial Number
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);				                // Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);				                // Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, HardwareType);				                        // Hardware Type
                        PacketUtilities.WriteToStream(oMS, HardwareVersionMajor);				                // Hardware Version Major
                        PacketUtilities.WriteToStream(oMS, HardwareVersionMinor);				                // Hardware Version Minor
                        PacketUtilities.WriteToStream(oMS, Odometer);								            // Odometer
                        PacketUtilities.WriteToStream(oMS, TotalFuelUsed);							            // Total Fuel Used
                        PacketUtilities.WriteToStream(oMS, PalletsOnTruck);							            // Pallets On Truck
                        PacketUtilities.WriteToStream(oMS, GlobalPhonebookVer);					                // Global Phonebook Ver
                        PacketUtilities.WriteToStream(oMS, DriverPhonebookVer);					                // Driver Phonebook Ver
                        PacketUtilities.WriteToStream(oMS, DivisionGroupID);						            // Division Group ID
                        PacketUtilities.WriteToStream(oMS, DivisionListVer);						            // Division List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.WriteToStream(oMS, DelayReasonListID);					            // Delay Reason List ID
                        PacketUtilities.WriteToStream(oMS, DelayReasonListVer);					                // Delay Reason List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.WriteToStream(oMS, RejectReasonListID);					            // Reject Reason List ID
                        PacketUtilities.WriteToStream(oMS, RejectReasonListVer);					            // Reject Reason List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.WriteToStream(oMS, PalletTypeListID);					            // Pallet Type List ID
                        PacketUtilities.WriteToStream(oMS, PalletTypeListVer);						            // Pallet Type List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.WriteToStream(oMS, TrailerListID);					                // Trailer List ID
                        PacketUtilities.WriteToStream(oMS, TrailerListVer);							            // Trailer List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.WriteToStream(oMS, UsageListID);					                // Usage List ID
                        PacketUtilities.WriteToStream(oMS, UsageListVer);							            // Usage List Ver
                        PacketUtilities.WriteToStream(oMS, PredefineMsgListID);					                // Predefined Msg List ID
                        PacketUtilities.WriteToStream(oMS, PredefineMsgListVer);					            // Predefined Msg List Ver
                        PacketUtilities.WriteToStream(oMS, PreTripListID);							            // Pre-Trip Question List ID
                        PacketUtilities.WriteToStream(oMS, PreTripListVer);							            // Pre-Trip Question List Ver
                        PacketUtilities.WriteToStream(oMS, PostTripListID);							            // Post-Trip Question List ID
                        PacketUtilities.WriteToStream(oMS, PostTripListVer);							        // Post-Trip Question List Ver
                        PacketUtilities.WriteToStream(oMS, PickupQuestionListID);					            // Pickup Question List ID
                        PacketUtilities.WriteToStream(oMS, PickupQuestionListVer);				                // Pickup Question List Ver
                        PacketUtilities.WriteToStream(oMS, DeliveryQuestionListID);					            // Delivery Question List ID
                        PacketUtilities.WriteToStream(oMS, DeliveryQuestionListVer);				            // Delivery Question List Ver
                        PacketUtilities.WriteToStream(oMS, FuelStationListID);					                // Fuel Station List ID
                        PacketUtilities.WriteToStream(oMS, FuelStationListVer);				                    // Fuel Station List Ver
                        PacketUtilities.WriteToStream(oMS, FleetAddressListVer);				                // Fleet Address List Ver
                        PacketUtilities.WriteToStream(oMS, DriverAddressListVer);				                // Driver Address List Ver
                        #endregion
                        break;
                    case "K":
                        #region Phonecall Report Packet - [Sub Command] [Call Duration][Phone Number][TSC][SIM Card Number][TSC]
                        if (SubCommandType == "I")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Incomming Phonecall Report Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "O")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Outgoing Phonecall Report Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "S")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Incomming SMS Report Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        else if (SubCommandType == "T")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Outgoing SMS Report Packet for mobile ID " + Convert.ToString(MobileID));
                        }
                        PacketUtilities.WriteToStream(oMS, SubCommandType);					                    // Sub Command Type
                        PacketUtilities.WriteToStream(oMS, PhoneCallDuration);					                // Call Duration
                        PacketUtilities.WriteToStream(oMS, PhoneNumber);							            // Phone Number
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);								        // TSC
                        PacketUtilities.WriteToStream(oMS, SimCardNumber);						                // Sim Card Number
                        PacketUtilities.WriteToStream(oMS, (byte)0x12);								        // TSC
                        #endregion
                        break;
                    case "M":
                        #region Driver Message Packet
                        PacketUtilities.WriteToStream(oMS, SubCommandType);					                    // Sub Command Type
                        if (SubCommandType == "C")
                        {
                            #region Message Received: [Sub Command = 'C'][Message ID]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Message Received Packet for mobile ID " + Convert.ToString(MobileID) + ", Message ID = " + Convert.ToString(MessageID));
                            PacketUtilities.WriteToStream(oMS, MessageID);							            // Message ID
                            #endregion
                        }
                        else if (SubCommandType == "R")
                        {
                            #region Message Read: [Sub Command = 'R'][Message ID]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Message Read Packet for mobile ID " + Convert.ToString(MobileID) + ", Message ID = " + Convert.ToString(MessageID));
                            PacketUtilities.WriteToStream(oMS, MessageID);							            // Message ID
                            #endregion
                        }
                        else if (SubCommandType == "A")
                        {
                            #region Reply to Base: [Sub Command = 'A'][Message ID][Message][TSC]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Message Reply Packet for mobile ID " + Convert.ToString(MobileID) + ", Message ID = " + Convert.ToString(MessageID));
                            PacketUtilities.WriteToStream(oMS, MessageID);							            // Message ID
                            PacketUtilities.WriteToStream(oMS, Message);								        // Message 
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);							        // TSC
                            #endregion
                        }
                        else if (SubCommandType == "P")
                        {
                            #region Driver Initiated Pre-defined Message: [Sub Command = 'P'][Pre-defined Message List ID][Pre-defined Message Ver][Pre-defined Message ID]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Pre-defined Packet for mobile ID " + Convert.ToString(MobileID) + ", Pre-defined Message List ID = " + Convert.ToString(PredefineMsgListID) + ", Pre-defined Message Ver = " + Convert.ToString(PredefineMsgListVer) + ", Pre-defined Message ID = " + Convert.ToString(PredefinedMessageID));
                            PacketUtilities.WriteToStream(oMS, PredefineMsgListID);		                        // Pre-defined Message List ID
                            PacketUtilities.WriteToStream(oMS, PredefineMsgListVer);			                // Pre-defined Message Ver
                            PacketUtilities.WriteToStream(oMS, PredefinedMessageID);			                // Pre-defined Message ID
                            #endregion
                        }
                        else if (SubCommandType == "F")
                        {
                            #region Driver Initiated Free-Form Message: [Sub Command = 'F'][Message][TSC]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Driver Initiated Free-Form Message Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, Message);								        // Message 
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);							        // TSC
                            #endregion
                        }
                        else if (SubCommandType == "E")
                        {
                            #region Driver Initiated Email Message: [Sub Command = 'E'][Email Address][TSC][Message][TSC]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Driver Initiated Email Packet for mobile ID " + Convert.ToString(MobileID));
                            PacketUtilities.WriteToStream(oMS, EmailAddress);						            // Email Address
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);							        // TSC
                            PacketUtilities.WriteToStream(oMS, Message);								        // Message 
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);							        // TSC
                            #endregion
                        }
                        else if (SubCommandType == "J")
                        {
                            #region Job Email Alert Message : [Sub Command = 'J'][Email Flags][Job Dispatch ID][Leg No]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Job Email Alert Packet for mobile ID " + Convert.ToString(MobileID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber));
                            PacketUtilities.WriteToStream(oMS, JobEmailFlags);						            // Email Flags
                            PacketUtilities.WriteToStream(oMS, JobDispatchID);						            // Job Dispatch ID
                            PacketUtilities.WriteToStream(oMS, LegNumber);						                // Leg Number
                            #endregion
                        }
                        #endregion
                        break;
                    case "O":
                        #region Logout Packet - [Odometer][Total Fuel Used][Pallets on Truck]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Logout Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, Odometer);								            // Odometer
                        PacketUtilities.WriteToStream(oMS, TotalFuelUsed);							            // Total Fuel Used
                        PacketUtilities.WriteToStream(oMS, PalletsOnTruck);							            // Pallets On Truck
                        #endregion
                        break;
                    case "P":
                        #region Pre-Trip Answers Packet  - [IsLoginAnswers][Pre-Trip Message List ID][Pre-Trip Message List Ver][Num of Answers] [ASC][Question Num][Answer][TSC] [Signature Type][Signature Length][Signature]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Pre-Trip Answers Packet for mobile ID " + Convert.ToString(MobileID));
                        NumOfAnswers = (byte)PreTripQuestionAnswer.Count;
                        PacketUtilities.WriteToStream(oMS, bIsLoginAnswers);								    // Is Login Answers
                        PacketUtilities.WriteToStream(oMS, PreTripListID);								        // Pre-Trip Message List ID
                        PacketUtilities.WriteToStream(oMS, PreTripListVer);							            // Pre-Trip Message List Ver
                        PacketUtilities.WriteToStream(oMS, NumOfAnswers);							            // Num of Answers
                        for (int X = 0; X < PreTripQuestionAnswer.Count; X++)
                        {
                            PacketUtilities.WriteToStream(oMS, (byte)0x0A);								    // ASC
                            PacketUtilities.WriteToStream(oMS, (byte)Convert.ToInt32(PreTripQuestionNum[X]));  // Question Num
                            PacketUtilities.WriteToStream(oMS, (string)PreTripQuestionAnswer[X]);		        // Answer
                            PacketUtilities.WriteToStream(oMS, (byte)0x12);								    // TSC							
                        }
                        if (SignatureData != null)
                        {

                            PacketUtilities.WriteToStream(oMS, bSigType);	                                    // Signature Type
                            PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	                // Signature Length
                            PacketUtilities.WriteToStream(oMS, SignatureData);						            // Signature
                        }
                        else
                        {
                            PacketUtilities.WriteToStream(oMS, (ushort)0);									    // Signature Length
                        }
                        #endregion
                        break;
                    case "Q":
                        #region Predefined Function Result Packet - [Function ID][Status][Result Length][Result]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Pre-Trip Answers Packet for mobile ID " + Convert.ToString(MobileID) + ", Function ID = " + Convert.ToString(FunctionID));
                        PacketUtilities.WriteToStream(oMS, FunctionID);									        // Function ID
                        if (FunctionStatus)
                            PacketUtilities.WriteToStream(oMS, (byte)0x01);							        // Status
                        else
                            PacketUtilities.WriteToStream(oMS, (byte)0x00);
                        if (FunctionResult != null)
                        {
                            PacketUtilities.WriteToStream(oMS, (ushort)FunctionResult.Length);	                // Result Length
                            PacketUtilities.WriteToStream(oMS, FunctionResult);						            // Result
                        }
                        else
                        {
                            PacketUtilities.WriteToStream(oMS, (ushort)0);									    // Result Length
                        }
                        #endregion
                        break;
                    case "R":
                        #region Request Packet - [Sub Command]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Job Request Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Sub Command Type
                        #endregion
                        break;
                    case "S":
                        #region Job Stop Packet
                        PacketUtilities.WriteToStream(oMS, SubCommandType);						                // Sub Command Type
                        if (SubCommandType == "R")
                        {
                            #region Job Packet Read by Driver - [Sub Command][Job Dispatch ID][Job Reference]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Job Packet Read for mobile ID " + Convert.ToString(MobileID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobID));
                            PacketUtilities.WriteToStream(oMS, JobDispatchID);						            // Job Dispatch ID
                            PacketUtilities.WriteToStream(oMS, JobID);									        // Job Reference
                            #endregion
                        }
                        else if (SubCommandType == "P")
                        {
                            #region Pickup Packet - [Sub Command][Job Dispatch ID][Job Reference][Job Num][Division ID][Delay Reason ID][Odometer][Total Fuel Used][Driver Has Documentation][SPS][Pallet Type ID][Pallet Count] [EPS][Pickup Question List ID][Pickup Question List Ver][Num of Answers] [ASC][Question Num][Answer][TSC] [Signature Type][Signature Length][Signature]
                            PacketUtilities.WriteToStream(oMS, JobDispatchID);						            // Job Dispatch ID
                            PacketUtilities.WriteToStream(oMS, JobID);									        // Job Reference
                            PacketUtilities.WriteToStream(oMS, JobNum);									        // Job Number
                            if ((int)bProtocolVer <= 2)
                            {
                                #region Protocol <= 2
                                if (oLogging != null && _bLogOutbound) oLogging.Info("Building Job Pickup Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobID));
                                PacketUtilities.WriteToStream(oMS, DivisionID);								        // Division ID
                                PacketUtilities.WriteToStream(oMS, DelayReasonID);						            // Delay Reason ID
                                PacketUtilities.WriteToStream(oMS, Odometer);									    // Odometer
                                PacketUtilities.WriteToStream(oMS, TotalFuelUsed);									// Total Fuel Used
                                PacketUtilities.WriteToStream(oMS, DriverHasDocumentation);							// Driver Has Documentation
                                iListCount = (PalletTypeList.Count <= PalletCount.Count ? PalletTypeList.Count : PalletCount.Count);
                                for (int X = 0; X < iListCount; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);							    // SPS
                                    PacketUtilities.WriteToStream(oMS, (int)PalletTypeList[X]);		            // Pallet Type ID
                                    PacketUtilities.WriteToStream(oMS, (byte)PalletCount[X]);			            // Pallet Count
                                }
                                PacketUtilities.WriteToStream(oMS, (byte)0x0B);								    // EPS
                                PacketUtilities.WriteToStream(oMS, PickupQuestionListID);							// Pickup Question List ID
                                PacketUtilities.WriteToStream(oMS, PickupQuestionListVer);							// Pickup Question List Ver
                                NumOfAnswers = (byte)JobLegQuestionAnswer.Count;
                                PacketUtilities.WriteToStream(oMS, NumOfAnswers);							        // Num of Answers
                                for (int X = 0; X < JobLegQuestionAnswer.Count; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// ASC
                                    PacketUtilities.WriteToStream(oMS, (byte)Convert.ToInt32(JobLegQuestionNum[X]));  // Question Num
                                    PacketUtilities.WriteToStream(oMS, (string)JobLegQuestionAnswer[X]);		    // Answer
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);								// TSC							
                                }
                                if (SignatureData != null)
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	            // Signature Length
                                    PacketUtilities.WriteToStream(oMS, SignatureData);						        // Signature
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)0);									// Signature Length
                                }
                                #endregion
                            }
                            else
                            {
                                #region Protcol > 2
                                if (oLogging != null && _bLogOutbound)
                                    oLogging.Info("Reading Job Stop Pickup Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobID));
                                //Pickup Packet - Protocol version 2
                                //[Sub Command][Job Dispatch ID][Job Reference][Job Num]
                                //[DVLS][Division List ID][Division List Ver][Division ID] 
                                //[DRLS][Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                //[EDS][Odometer][Total Fuel Used]
                                //[Driver Has Documentation]
                                //[PTS][Pallet List ID][Pallet List Version][SPS][Pallet Type ID][Pallet Count][EPS]
                                //[QLS][Question List ID][ Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                                //[TSC] [Signature Type][Signature Length][Signature]

                                if (DivisionID > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // DVLS
                                    #region [Division List ID][Division List Ver][Division ID]
                                    PacketUtilities.WriteToStream(oMS, DivisionGroupID);					        // Division List ID
                                    PacketUtilities.WriteToStream(oMS, DivisionListVer);					        // Division List Ver
                                    PacketUtilities.WriteToStream(oMS, DivisionID);					                // Division ID
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // DVLS

                                if (DelayReasonID > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // DRLS
                                    #region [Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                    PacketUtilities.WriteToStream(oMS, DelayReasonListID);					        // Delay Rsn List ID
                                    PacketUtilities.WriteToStream(oMS, DelayReasonListVer);					        // Delay Rsn List Ver
                                    PacketUtilities.WriteToStream(oMS, DelayReasonID);					            // Delay Rsn ID
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // DRLS

                                if (Odometer > 0 || TotalFuelUsed > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // EDS
                                    #region [Odometer][Total Fuel Used]
                                    PacketUtilities.WriteToStream(oMS, Odometer);								    // Odometer
                                    PacketUtilities.WriteToStream(oMS, TotalFuelUsed);							    // Total Fuel Used
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // EDS
                                PacketUtilities.WriteToStream(oMS, DriverHasDocumentation);					        // Driver Has Documentation
                                if (PalletTypeListID > 0 || PalletTypeListVer > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // PTS
                                    #region [Pallet List ID][Pallet List Version][SPS][Pallet Type ID][Pallet Count][EPS]
                                    PacketUtilities.WriteToStream(oMS, PalletTypeListID);						    // Pallet List ID
                                    PacketUtilities.WriteToStream(oMS, PalletTypeListVer);					        // Pallet List Version
                                    if (PalletTypeList.Count > 0)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);                             // SPS
                                        for (int X = 0; X < PalletTypeList.Count; X++)
                                        {
                                            PacketUtilities.WriteToStream(oMS, (int)PalletTypeList[X]);             // Pallet Type ID
                                            PacketUtilities.WriteToStream(oMS, (byte)PalletCount[X]);               // Pallet Count
                                            if (X != (PalletTypeList.Count - 1))
                                                PacketUtilities.WriteToStream(oMS, (byte)0x0A);                     // SPS
                                            else
                                                PacketUtilities.WriteToStream(oMS, (byte)0x0B);                    // EPS
                                        }
                                    }
                                    else
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0B);                             // EPS
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // PTS

                                if ((PickupQuestionListID > 0 || PickupQuestionListVer > 0) && JobLegQuestionNum.Count > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // QLS
                                    #region [Question List ID][Question List Ver][Num of Answers][ASC][Question Num][Answer]
                                    PacketUtilities.WriteToStream(oMS, PickupQuestionListID);					    // Pickup Question List ID
                                    PacketUtilities.WriteToStream(oMS, PickupQuestionListVer);					    // Pickup Question List Ver
                                    PacketUtilities.WriteToStream(oMS, (byte)JobLegQuestionNum.Count);			    // Num of Answers
                                    for (int X = 0; X < JobLegQuestionNum.Count; X++)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);							// ASC
                                        PacketUtilities.WriteToStream(oMS, (byte)X);							    // Question Num
                                        PacketUtilities.WriteToStream(oMS, (string)JobLegQuestionAnswer[X]);        // Answer
                                        PacketUtilities.WriteToStream(oMS, (byte)0x12);							// TSC
                                    }
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // QLS
                                #region Signature Data
                                if (SignatureData != null)
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	            // Signature Length
                                    PacketUtilities.WriteToStream(oMS, SignatureData);						        // Signature
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)0);									// Signature Length
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        else if (SubCommandType == "D" || SubCommandType == "d")
                        {
                            #region Delivery Packet - [Sub Command][Job Reference][Leg Number][IsReturnDelivery][Delay Reason ID][Reject Reason ID][Odometer][Total Fuel Used][POD Name][TSC] [SPS][Pallet Type ID][Pallet Count] [EPS][Exchange Pallet Order][SEPS][Pallet Type ID][Pallet Count][EEPS][Full Return][SEPS][Pallet Type ID][Pallet Count][EEPS][Signature Length][Signature]
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Job Delivery Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobNum) + ", Leg Number = " + Convert.ToString(LegNumber));
                            PacketUtilities.WriteToStream(oMS, JobDispatchID);						            // Job Dispatch ID
                            PacketUtilities.WriteToStream(oMS, JobID);										    // Job Reference
                            PacketUtilities.WriteToStream(oMS, JobNum);									        // Job Number
                            PacketUtilities.WriteToStream(oMS, LegNumber);								        // Leg Number
                            if (IsReturnDelivery)
                                PacketUtilities.WriteToStream(oMS, (byte)0x01);						        // IsReturnDelivery
                            else
                                PacketUtilities.WriteToStream(oMS, (byte)0x00);
                            if ((int)bProtocolVer <= 2)
                            {
                                #region Protocol Ver <= 2
                                PacketUtilities.WriteToStream(oMS, DelayReasonID);						            // Delay Reason ID
                                PacketUtilities.WriteToStream(oMS, RejectReasonID);						            // Reject Reason ID
                                PacketUtilities.WriteToStream(oMS, Odometer);									    // Odometer
                                PacketUtilities.WriteToStream(oMS, TotalFuelUsed);									// Total Fuel Used
                                PacketUtilities.WriteToStream(oMS, PODName);								        // POD Name
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);								    // TSC
                                iListCount = (PalletTypeList.Count <= PalletCount.Count ? PalletTypeList.Count : PalletCount.Count);
                                for (int X = 0; X < iListCount; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);							    // SPS
                                    PacketUtilities.WriteToStream(oMS, (int)PalletTypeList[X]);		            // Pallet Type ID
                                    PacketUtilities.WriteToStream(oMS, (byte)PalletCount[X]);			            // Pallet Count
                                }
                                PacketUtilities.WriteToStream(oMS, (byte)0x0B);								    // EPS
                                iListCount = (ReturnPalletTypeList.Count <= ReturnPalletCount.Count ? ReturnPalletTypeList.Count : ReturnPalletCount.Count);
                                for (int X = 0; X < iListCount; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0E);							    // SRPS
                                    PacketUtilities.WriteToStream(oMS, (int)ReturnPalletTypeList[X]);		        // Pallet Type ID
                                    PacketUtilities.WriteToStream(oMS, (byte)ReturnPalletCount[X]);			    // Pallet Count
                                }
                                PacketUtilities.WriteToStream(oMS, (byte)0x0F);								    // ERPS
                                PacketUtilities.WriteToStream(oMS, ExchangePalletOrder);						    // Exchange Pallet Order Num
                                PacketUtilities.WriteToStream(oMS, (byte)0x12);								        // TSC
                                iListCount = (ExchangePalletTypeList.Count <= ExchangePalletCount.Count ? ExchangePalletTypeList.Count : ExchangePalletCount.Count);
                                for (int X = 0; X < iListCount; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0C);							        // SEPS
                                    PacketUtilities.WriteToStream(oMS, (int)ExchangePalletTypeList[X]);		        // Pallet Type ID
                                    PacketUtilities.WriteToStream(oMS, (byte)ExchangePalletCount[X]);			    // Pallet Count
                                }
                                PacketUtilities.WriteToStream(oMS, (byte)0x0D);								        // EEPS
                                if (bFullReturn)
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // Full Return
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);
                                iListCount = (EmptyPalletTypeList.Count <= EmptyPalletCount.Count ? EmptyPalletTypeList.Count : EmptyPalletCount.Count);
                                for (int X = 0; X < iListCount; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0C);							        // SEPS
                                    PacketUtilities.WriteToStream(oMS, (int)EmptyPalletTypeList[X]);		        // Pallet Type ID
                                    PacketUtilities.WriteToStream(oMS, (byte)EmptyPalletCount[X]);			        // Pallet Count
                                }
                                PacketUtilities.WriteToStream(oMS, (byte)0x0D);								        // EEPS
                                PacketUtilities.WriteToStream(oMS, DeliveryQuestionListID);							// Pickup Question List ID
                                PacketUtilities.WriteToStream(oMS, DeliveryQuestionListVer);						// Pickup Question List Ver
                                NumOfAnswers = (byte)JobLegQuestionAnswer.Count;
                                PacketUtilities.WriteToStream(oMS, NumOfAnswers);							        // Num of Answers
                                for (int X = 0; X < JobLegQuestionAnswer.Count; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// ASC
                                    PacketUtilities.WriteToStream(oMS, (byte)Convert.ToInt32(JobLegQuestionNum[X]));  // Question Num
                                    PacketUtilities.WriteToStream(oMS, (string)JobLegQuestionAnswer[X]);		    // Answer
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);								// TSC							
                                }
                                if (SignatureData != null)
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	            // Signature Length
                                    PacketUtilities.WriteToStream(oMS, SignatureData);					            // Signature 
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, (ushort)0);	                                // Signature Length
                                }
                                #endregion
                            }
                            else
                            {
                                #region Protcol Ver > 2
                                #region Delay Reason [DRLS][Delay Rsn List ID][Delay Rsn List Ver][ [Delay Rsn ID]
                                if (DelayReasonID > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // DRLS
                                    #region [Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                    PacketUtilities.WriteToStream(oMS, DelayReasonListID);					        // Delay Rsn List ID
                                    PacketUtilities.WriteToStream(oMS, DelayReasonListVer);					        // Delay Rsn List Ver
                                    PacketUtilities.WriteToStream(oMS, DelayReasonID);					            // Delay Rsn ID
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // DRLS
                                #endregion
                                #region Reject Reason [RRLS ][Reject Rsn List ID ][ Reject Rsn List Ver ][Reject Rsn ID]
                                if (RejectReasonID > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // RRLS 
                                    #region [Reject Rsn List ID ][ Reject Rsn List Ver ][Reject Rsn ID]
                                    PacketUtilities.WriteToStream(oMS, RejectReasonListID);					        // Reject Rsn List ID
                                    PacketUtilities.WriteToStream(oMS, RejectReasonListVer);					    // Reject Rsn List Ver
                                    PacketUtilities.WriteToStream(oMS, RejectReasonID);					            // Reject Rsn ID
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // RRLS 
                                #endregion
                                #region Engine Data [EDS][Odometer][Total Fuel Used]
                                if (Odometer > 0 || TotalFuelUsed > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // EDS
                                    #region [Odometer][Total Fuel Used]
                                    PacketUtilities.WriteToStream(oMS, Odometer);								    // Odometer
                                    PacketUtilities.WriteToStream(oMS, TotalFuelUsed);							    // Total Fuel Used
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // EDS
                                #endregion
                                #region POD Name [PODS][POD Name][TSC]
                                if (PODName.Length > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // PODS 
                                    #region [POD Name][TSC]
                                    PacketUtilities.WriteToStream(oMS, PODName);								    // POD Name
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);							    // TSC
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // PODS 
                                #endregion
                                #region Pallet Section [PTS][Pallet List ID][Pallet List Version]
                                if (PalletTypeListID > 0 || PalletTypeListVer > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // PTS
                                    #region [SPS][Pallet Type ID][Pallet Count] [EPS]
                                    iListCount = (PalletTypeList.Count <= PalletCount.Count ? PalletTypeList.Count : PalletCount.Count);
                                    for (int X = 0; X < iListCount; X++)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);							// SPS
                                        PacketUtilities.WriteToStream(oMS, (int)PalletTypeList[X]);		        // Pallet Type ID
                                        PacketUtilities.WriteToStream(oMS, (byte)PalletCount[X]);			    // Pallet Count
                                    }
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0B);								// EPS
                                    #endregion
                                    #region [SRPS][Pallet Type ID][Pallet Count] [ERPS]
                                    iListCount = (ReturnPalletTypeList.Count <= ReturnPalletCount.Count ? ReturnPalletTypeList.Count : ReturnPalletCount.Count);
                                    for (int X = 0; X < iListCount; X++)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0E);							    // SRPS
                                        PacketUtilities.WriteToStream(oMS, (int)ReturnPalletTypeList[X]);		    // Pallet Type ID
                                        PacketUtilities.WriteToStream(oMS, (byte)ReturnPalletCount[X]);			    // Pallet Count
                                    }
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0F);								    // ERPS
                                    #endregion
                                    #region [EPLS][Exchange Pallet Order][TSC][SEPS][Pallet Type ID][Pallet Count] [EEPS]
                                    iListCount = (ExchangePalletTypeList.Count <= ExchangePalletCount.Count ? ExchangePalletTypeList.Count : ExchangePalletCount.Count);
                                    if (iListCount > 0)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x01);								        // EPLS
                                        PacketUtilities.WriteToStream(oMS, ExchangePalletOrder);						    // Exchange Pallet Order Num
                                        PacketUtilities.WriteToStream(oMS, (byte)0x12);								        // TSC
                                        for (int X = 0; X < iListCount; X++)
                                        {
                                            PacketUtilities.WriteToStream(oMS, (byte)0x0C);							        // SEPS
                                            PacketUtilities.WriteToStream(oMS, (int)ExchangePalletTypeList[X]);		        // Pallet Type ID
                                            PacketUtilities.WriteToStream(oMS, (byte)ExchangePalletCount[X]);			    // Pallet Count
                                        }
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0D);								        // EEPS
                                    }
                                    else
                                        PacketUtilities.WriteToStream(oMS, (byte)0x00);								        // EPLS
                                    #endregion
                                    #region [Full Return][SEPS][Pallet Type ID][Pallet Count] [EEPS]
                                    if (bFullReturn)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x01);								// Full Return
                                        iListCount = (EmptyPalletTypeList.Count <= EmptyPalletCount.Count ? EmptyPalletTypeList.Count : EmptyPalletCount.Count);
                                        for (int X = 0; X < iListCount; X++)
                                        {
                                            PacketUtilities.WriteToStream(oMS, (byte)0x0C);							// SEPS
                                            PacketUtilities.WriteToStream(oMS, (int)EmptyPalletTypeList[X]);		// Pallet Type ID
                                            PacketUtilities.WriteToStream(oMS, (byte)EmptyPalletCount[X]);			// Pallet Count
                                        }
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0D);								// EEPS
                                    }
                                    else
                                        PacketUtilities.WriteToStream(oMS, (byte)0x00);                             // Full Return
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // PTS
                                #endregion
                                #region Delivery Questions
                                if ((DeliveryQuestionListID > 0 || DeliveryQuestionListVer > 0) && JobLegQuestionNum.Count > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // QLS
                                    #region [Question List ID][Question List Ver][Num of Answers][ASC][Question Num][Answer]
                                    PacketUtilities.WriteToStream(oMS, DeliveryQuestionListID);					    // Delivery Question List ID
                                    PacketUtilities.WriteToStream(oMS, DeliveryQuestionListVer);					// Delivery Question List Ver
                                    PacketUtilities.WriteToStream(oMS, (byte)JobLegQuestionNum.Count);			    // Num of Answers
                                    for (int X = 0; X < JobLegQuestionNum.Count; X++)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);							    // ASC
                                        PacketUtilities.WriteToStream(oMS, (byte)X);							    // Question Num
                                        PacketUtilities.WriteToStream(oMS, (string)JobLegQuestionAnswer[X]);        // Answer
                                        PacketUtilities.WriteToStream(oMS, (byte)0x12);							    // TSC
                                    }
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // QLS
                                #endregion
                                #region Signature Data
                                if (SignatureData != null)
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	            // Signature Length
                                    PacketUtilities.WriteToStream(oMS, SignatureData);						        // Signature
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)0);									// Signature Length
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        else if (SubCommandType == "G")
                        {
                            #region Group Pickup Packet
                            //Protocol Version <= 2

                            //[Sub Command][Job Reference][Division ID][Delay Reason ID][Odometer][Total Fuel Used][Driver Has Documentation]
                            //[Pickup Question List ID][Pickup Question List Ver][Num of Answers] [ASC][Question Num][Answer][TSC] [Signature Type][Signature Length][Signature]

                            //Protocol Versions > 2

                            //[Sub Command][Job Reference]
                            //[DVLS][Division List ID][Division List Ver][Division ID] 
                            //[DRLS][Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                            //[EDS][Odometer][Total Fuel Used]
                            //[Driver Has Documentation]
                            //[QLS][Question List ID][Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                            //[TSC] [Signature Type][Signature Length][Signature]

                            if (oLogging != null && _bLogOutbound) oLogging.Info("Building Job Group Pickup Packet for mobile ID " + Convert.ToString(MobileID) + ", Reference Number = " + Convert.ToString(JobNum));
                            PacketUtilities.WriteToStream(oMS, JobNum);									        // Job Number
                            if ((int)bProtocolVer <= 2)
                            {
                                #region Protocol Version <= 2
                                PacketUtilities.WriteToStream(oMS, DivisionID);								        // Division ID
                                PacketUtilities.WriteToStream(oMS, DelayReasonID);						            // Delay Reason ID
                                PacketUtilities.WriteToStream(oMS, Odometer);									    // Odometer
                                PacketUtilities.WriteToStream(oMS, TotalFuelUsed);									// Total Fuel Used
                                PacketUtilities.WriteToStream(oMS, DriverHasDocumentation);							// Driver Has Documentation
                                PacketUtilities.WriteToStream(oMS, PickupQuestionListID);							// Pickup Question List ID
                                PacketUtilities.WriteToStream(oMS, PickupQuestionListVer);							// Pickup Question List Ver
                                NumOfAnswers = (byte)JobLegQuestionAnswer.Count;
                                PacketUtilities.WriteToStream(oMS, NumOfAnswers);							        // Num of Answers
                                for (int X = 0; X < JobLegQuestionAnswer.Count; X++)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x0A);								// ASC
                                    PacketUtilities.WriteToStream(oMS, (byte)Convert.ToInt32(JobLegQuestionNum[X]));  // Question Num
                                    PacketUtilities.WriteToStream(oMS, (string)JobLegQuestionAnswer[X]);		    // Answer
                                    PacketUtilities.WriteToStream(oMS, (byte)0x12);								// TSC							
                                }
                                if (SignatureData != null)
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	            // Signature Length
                                    PacketUtilities.WriteToStream(oMS, SignatureData);						        // Signature
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, (ushort)0);									// Signature Length
                                }
                                #endregion
                            }
                            else
                            {
                                #region Protocol Version > 2
                                if (DivisionID > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // DVLS
                                    #region [Division List ID][Division List Ver][Division ID]
                                    PacketUtilities.WriteToStream(oMS, DivisionGroupID);					        // Division List ID
                                    PacketUtilities.WriteToStream(oMS, DivisionListVer);					        // Division List Ver
                                    PacketUtilities.WriteToStream(oMS, DivisionID);					                // Division ID
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // DVLS

                                if (DelayReasonID > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // DRLS
                                    #region [Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                    PacketUtilities.WriteToStream(oMS, DelayReasonListID);					        // Delay Rsn List ID
                                    PacketUtilities.WriteToStream(oMS, DelayReasonListVer);					        // Delay Rsn List Ver
                                    PacketUtilities.WriteToStream(oMS, DelayReasonID);					            // Delay Rsn ID
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // DRLS

                                if (Odometer > 0 || TotalFuelUsed > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // EDS
                                    #region [Odometer][Total Fuel Used]
                                    PacketUtilities.WriteToStream(oMS, Odometer);								    // Odometer
                                    PacketUtilities.WriteToStream(oMS, TotalFuelUsed);							    // Total Fuel Used
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // EDS
                                PacketUtilities.WriteToStream(oMS, DriverHasDocumentation);					        // Driver Has Documentation
                                if ((PickupQuestionListID > 0 || PickupQuestionListVer > 0) && JobLegQuestionNum.Count > 0)
                                {
                                    PacketUtilities.WriteToStream(oMS, (byte)0x01);								    // QLS
                                    #region [Question List ID][Question List Ver][Num of Answers][ASC][Question Num][Answer]
                                    PacketUtilities.WriteToStream(oMS, PickupQuestionListID);					    // Pickup Question List ID
                                    PacketUtilities.WriteToStream(oMS, PickupQuestionListVer);					    // Pickup Question List Ver
                                    PacketUtilities.WriteToStream(oMS, (byte)JobLegQuestionNum.Count);			    // Num of Answers
                                    for (int X = 0; X < JobLegQuestionNum.Count; X++)
                                    {
                                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);							// ASC
                                        PacketUtilities.WriteToStream(oMS, (byte)X);							    // Question Num
                                        PacketUtilities.WriteToStream(oMS, (string)JobLegQuestionAnswer[X]);        // Answer
                                        PacketUtilities.WriteToStream(oMS, (byte)0x12);							// TSC
                                    }
                                    #endregion
                                }
                                else
                                    PacketUtilities.WriteToStream(oMS, (byte)0x00);								    // QLS
                                #region Signature Data
                                if (SignatureData != null)
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)SignatureData.Length);	            // Signature Length
                                    PacketUtilities.WriteToStream(oMS, SignatureData);						        // Signature
                                }
                                else
                                {
                                    PacketUtilities.WriteToStream(oMS, bSigType);	                                // Signature Type
                                    PacketUtilities.WriteToStream(oMS, (ushort)0);									// Signature Length
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        #endregion
                        break;
                    case "T":
                        #region Terminal Startup Packet - [Mobile ID][Firmware Ver Major][Firmware Ver Minor][Temperature Major][Temperature Minor][Battery Percentage][MDT Option Flags][MDT Variant Name][EOVN]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Terminal Startup Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, (short)MobileID);						            // Mobile ID
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMajor);			                    // Firmware Version Major
                        PacketUtilities.WriteToStream(oMS, FirmwareVersionMinor);			                    // Firmware Version Minor
                        PacketUtilities.WriteToStream(oMS, TemperatureMajor);					                // Temperature Major
                        PacketUtilities.WriteToStream(oMS, TemperatureMinor);					                // Temperature Minor
                        PacketUtilities.WriteToStream(oMS, BatteryPercentage);					                // Battery Percentage
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags1);					                // MDT Options Flags Byte 1
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags2);					                // MDT Options Flags Byte 2
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags3);					                // MDT Options Flags Byte 3
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags4);					                // MDT Options Flags Byte 4
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags5);					                // MDT Options Flags Byte 5
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags6);					                // MDT Options Flags Byte 6
                        PacketUtilities.WriteToStream(oMS, MDTOptionsFlags7);					                // MDT Options Flags Byte 7
                        PacketUtilities.WriteToStream(oMS, MDTVariantName);					                    // MDT Variant Name
                        PacketUtilities.WriteToStream(oMS, (byte)0x0A);								        // EOVN
                        #endregion
                        break;
                    case "V":
                        #region Trailer Usage Packet - [Sub Command][Trailer ID][Trailer ESN]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Trailer Usage Packet for mobile ID " + Convert.ToString(MobileID));
                        PacketUtilities.WriteToStream(oMS, SubCommandType);					                    // Sub Command Type
                        if (SubCommandType == "Q")
                        {
                            #region Create trailer query record
                            PacketUtilities.WriteToStream(oMS, TrailerID);							            // Trailer ID
                            PacketUtilities.WriteToStream(oMS, TrailerESN);								        // Trailer ESN
                            #endregion
                        }
                        else
                        {
                            #region Create Hitch/De-hitch record
                            PacketUtilities.WriteToStream(oMS, TrailerID);									    // Trailer ID
                            PacketUtilities.WriteToStream(oMS, TrailerESN);								        // Trailer ESN
                            PacketUtilities.WriteToStream(oMS, Odometer);									    // Odometer
                            if ((int)bProtocolVer > 2)
                                PacketUtilities.WriteToStream(oMS, TotalFuelUsed);								// Total Fuel Used
                            #endregion
                        }
                        #endregion
                        break;
                    case "U":
                        #region Vehicle Usage Packet - [ListID][List Version][Usage Type ID][Start or End Usage][Odometer][Total Fuel Used]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Building Vehicle Usage Packet for mobile ID " + Convert.ToString(MobileID));
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.WriteToStream(oMS, ListID);						                    // List ID
                        PacketUtilities.WriteToStream(oMS, ListVersion);							            // List Version
                        PacketUtilities.WriteToStream(oMS, UsageTypeID);							            // Usage Type ID
                        if (IsUsageStart)
                            PacketUtilities.WriteToStream(oMS, (byte)0x53);							        // Start or End of Usage
                        else
                            PacketUtilities.WriteToStream(oMS, (byte)0x46);
                        PacketUtilities.WriteToStream(oMS, Odometer);									        // Odometer
                        PacketUtilities.WriteToStream(oMS, TotalFuelUsed);									    // Total Fuel Used
                        #endregion
                        break;
                }
                #region Create the packet footer [SBS][EOP]
                PacketUtilities.WriteToStream(oMS, (byte)0x07);													// SBS
                PacketUtilities.WriteToStream(oMS, (byte)0x03);													// EOP
                #endregion
                bRet = oMS.ToArray();
                #region Update the length bytes
                bConvert = new byte[4];
                bConvert = BitConverter.GetBytes(bRet.Length - 5);
                bRet[1] = bConvert[0];
                bRet[2] = bConvert[1];
                #endregion
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "BuildMsgToHost()", ex);
            }
            return bRet;
        }
        public string TranslateMsgToHost(byte[] bData, string sUnitID)
        {
            string sRet = "";
            string sSigFileName = "";
            MemoryStream oMS = null;
            byte bTemp = (byte)0x00;
            int iListCount = 0;
            string sTemp = "";
            ushort usTemp = 0;
            uint uiTemp = 0;
            int iTemp = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            double dLat = 0;
            double dLong = 0;
            bool bDateCorrected = false;
            try
            {
                if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Terminal Data -> " + BitConverter.ToString(bData, 0));
                MobileID = Convert.ToInt32(sUnitID);
                oMS = new MemoryStream(bData);
                #region Read the packet header [Protocol Version][Length][Data][SBS][EOP]
                PacketUtilities.ReadFromStream(oMS, ref bProtocolVer);										     // Protocol Version
                PacketUtilities.ReadFromStream(oMS, ref usTemp);										        // Length
                if ((int)usTemp != bData.Length - 5)
                {
                    sRet = "Packet is not the correct length (Expecting " + Convert.ToString(bData.Length - 5) + " - Recieved " + Convert.ToString(usTemp) + ")";
                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                    return sRet;
                }
                if (bData[usTemp + 3] != (byte)0x07)
                {
                    sRet = "SBS was not found in the correct place";
                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                    return sRet;
                }
                if (bData[usTemp + 4] != (byte)0x03)
                {
                    sRet = "EOP was not found in the correct place";
                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                    return sRet;
                }
                #endregion
                PacketUtilities.ReadFromStream(oMS, ref bTemp);											        // Command Type
                CommandType = Convert.ToString((char)bTemp);
                if (CommandType != "d" && CommandType != "c" && CommandType != "T")
                {
                    #region Read the [Data] header - [Command Type][Fleet ID][Vehicle ID][Driver ID][GPS][Packet Data]
                    PacketUtilities.ReadFromStream(oMS, ref FleetID);										    // Fleet ID	
                    PacketUtilities.ReadFromStream(oMS, ref VehicleID);									        // Vehicle ID
                    PacketUtilities.ReadFromStream(oMS, ref DriverID);										    // Driver ID
                    #endregion
                    #region Read GPS Data - [LSC][Lat][Long][GPS Time][Speed][Heading][Device Timer]
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);											    // LSC
                    if (bTemp != (byte)0x12)
                    {
                        sRet = "Failed to find LSC";
                        oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                        return sRet;
                    }
                    PacketUtilities.ReadFromStream(oMS, ref VehicleLatitude);							        // Lat
                    PacketUtilities.ReadFromStream(oMS, ref VehicleLongitude);						            // Long
                    dLat = ConvLatLon(VehicleLatitude);
                    dLong = ConvLatLon(VehicleLongitude);
                    #region Read the vehicles GPS time
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								                // Vehicle GPS Time - Day
                    iDay = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								                // Vehicle GPS Time - Month
                    iMonth = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								                // Vehicle GPS Time - Year
                    iYear = (int)bTemp;
                    iYear = iYear + 2000;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								                // Vehicle GPS Time - Hour
                    iHour = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								                // Vehicle GPS Time - Minute
                    iMinute = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								                // Vehicle GPS Time - Second
                    iSecond = (int)bTemp;
                    bDateCorrected = false;
                    try
                    {
                        if (iDay > 0 && iMonth > 0)
                        {
                            VehicleGPSTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                        }
                        else
                        {
                            bDateCorrected = true;
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Invalid GPS Time on packet, defaulting to current time.");
                            VehicleGPSTime = DateTime.Now.ToUniversalTime();
                        }
                    }
                    catch (System.Exception exDateConvert)
                    {
                        bDateCorrected = true;
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Invalid GPS Time on packet, defaulting to current time." + "\r\n" + exDateConvert.Message);
                        VehicleGPSTime = DateTime.Now.ToUniversalTime();
                    }

                    if (VehicleGPSTime.ToOADate() > DateTime.Now.ToUniversalTime().AddDays(1).ToOADate())
                    {
                        bDateCorrected = true;
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Invalid GPS Time on packet (" + VehicleGPSTime.ToString("dd/MM/yyyy HH:mm:ss") + "), defaulting to current time.");
                        VehicleGPSTime = DateTime.Now.ToUniversalTime();
                    }
                    #endregion
                    PacketUtilities.ReadFromStream(oMS, ref VehicleSpeed);								        // Speed
                    PacketUtilities.ReadFromStream(oMS, ref VehicleHeading);							        // Heading
                    PacketUtilities.ReadFromStream(oMS, ref VehicleDeviceTimer);					            // Device Time
                    if (bDateCorrected)
                        VehicleDeviceTimer = 0;
                    #endregion
                }
                switch (CommandType)
                {
                    case "A":
                        #region Arrive Site Packet - [Sub Command][Job Dispatch ID][Job Reference][Leg Num][Odometer][Total Fuel Used]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);									// Job Dispatch ID
                        PacketUtilities.ReadFromStream(oMS, ref JobID);											// Job Reference Number
                        PacketUtilities.ReadFromStream(oMS, ref LegNumber);									    // Leg Number
                        PacketUtilities.ReadFromStream(oMS, ref Odometer);									    // Odometer
                        PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);									// Total Fuel Used
                        if (oLogging != null && _bLogOutbound)
                        {
                            if (SubCommandType == "A")
                                oLogging.Info("Reading Arrive at Site for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobNum));
                            else if (SubCommandType == "D")
                                oLogging.Info("Reading Depart Site for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobNum));
                            else if (SubCommandType == "P")
                            {
                                if (LegNumber == 0)
                                    oLogging.Info("Reading Start Pickup for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobNum));
                                else
                                    oLogging.Info("Reading Start Delivery for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobNum));
                            }
                            else
                                oLogging.Info("Reading Arrive Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobNum));
                        }
                        break;
                        #endregion
                    case "B":
                        #region Driver Address Maintenance
                        // New Address (N) 
                        // Update Driver Address (U)
                        //      [Sub Command][Item ID][Name][TSC][Address][TSC][Comment][TSC][Lat][Long] 
                        // Delete Driver Address (D)
                        //      [Sub Command][Item ID] 
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);

                        if (SubCommandType == "N" || SubCommandType == "U")
                        {
                            ItemID = 0;
                            ItemName = "";
                            ItemAddress = "";
                            ItemComment = "";
                            ItemLatitude = 0;
                            ItemLongitude = 0;

                            PacketUtilities.ReadFromStream(oMS, ref ItemID);									// Item ID
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ItemName);	                    // ItemName
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ItemAddress);	                // ItemAddress
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ItemComment);	                // ItemComment
                            uiTemp = 0;
                            PacketUtilities.ReadFromStream(oMS, ref uiTemp);							        // Lat
                            ItemLatitude = ConvLatLon(uiTemp);
                            uiTemp = 0;
                            PacketUtilities.ReadFromStream(oMS, ref uiTemp);						            // Long
                            ItemLongitude = ConvLatLon(uiTemp);
                        }
                        else if (SubCommandType == "D")
                        {
                            PacketUtilities.ReadFromStream(oMS, ref ItemID);									// Item ID
                        }
                        else
                        {
                            if (oLogging != null) oLogging.Info("Unknown Driver Address Packet : Sub Command " + SubCommandType);
                        }
                        break;
                        #endregion
                    case "c":
                        #region Download CPLD Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "d")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading CPLD Download Header Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        if (SubCommandType == "r")
                        {
                            PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);				                // Segment Number
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading CPLD Download Data Packet for mobile ID " + Convert.ToString(MobileID) + ", Segment Number = " + Convert.ToString(SegmentNumber) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        else if (SubCommandType == "v")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading CPLD Download Verify Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        #endregion
                        break;
                    case "d":
                        #region Download Win CE Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "d")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading WinCE Download Header Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        if (SubCommandType == "r")
                        {
                            PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);				                // Segment Number
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading WinCE Download Data Packet for mobile ID " + Convert.ToString(MobileID) + ", Segment Number = " + Convert.ToString(SegmentNumber) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        else if (SubCommandType == "v")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading WinCE Download Verify Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        #endregion
                        break;
                    case "D":
                        #region Download MOT Firmware Packet - Request Header : [Sub Command = 'd'] / Request Segment : [Sub Command = 'r'][Segment Number] / Verify Download : [Sub Command = 'v']
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "d")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading MOT Download Header Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        if (SubCommandType == "r")
                        {
                            PacketUtilities.ReadFromStream(oMS, ref SegmentNumber);				                // Segment Number
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading MOT Download Data Packet for mobile ID " + Convert.ToString(MobileID) + ", Segment Number = " + Convert.ToString(SegmentNumber) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        else if (SubCommandType == "v")
                        {
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading MOT Download Verify Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        #endregion
                        break;
                    case "E":
                        #region Fuel Report Packet -  [Fuel Station List ID][Fuel Station List Ver][Fuel Station ID][Cost Per Litre][Liters Pumped][Total Cost]
                        PacketUtilities.ReadFromStream(oMS, ref FuelStationListID);						        // Fuel Station List ID
                        PacketUtilities.ReadFromStream(oMS, ref FuelStationListVer);						    // Fuel Station List Ver
                        PacketUtilities.ReadFromStream(oMS, ref FuelStationID);						            // Fuel Station ID
                        PacketUtilities.ReadFromStream(oMS, ref iTemp);                                         // Cost Per Litre
                        if ((int)bProtocolVer <= 2)
                            dFuelCostPerLitre = Convert.ToDouble(iTemp) / Convert.ToDouble(100);
                        else
                            dFuelCostPerLitre = Convert.ToDouble(iTemp) / Convert.ToDouble(1000);
                        PacketUtilities.ReadFromStream(oMS, ref iTemp);                                         // Liters Pumped
                        dFuelLitresPumped = Convert.ToDouble(iTemp) / Convert.ToDouble(100);
                        PacketUtilities.ReadFromStream(oMS, ref iTemp);                                         // Total Cost
                        dFuelTotalCost = Convert.ToDouble(iTemp) / Convert.ToDouble(100);
                        PacketUtilities.ReadFromStream(oMS, ref Odometer);                                      // Odometer
                        PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);                                 // Total Fuel Used
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Fuel Report Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        #endregion
                        break;
                    case "F":
                        #region Lunch Break Packet -  [Sub Command][Odometer][Total Fuel Used]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "S")
                            StartFunction = true;
                        else if (SubCommandType == "F")
                            StartFunction = false;
                        PacketUtilities.ReadFromStream(oMS, ref Odometer);									    // Odometer
                        PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);									// Total Fuel Used
                        if (oLogging != null && _bLogOutbound)
                        {
                            if (SubCommandType == "S")
                                oLogging.Info("Reading Lunch Start Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            else
                                oLogging.Info("Reading Lunch End Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        #endregion
                        break;
                    case "G":
                        #region Job Allocation Ack Packet - [Allocation ID]
                        PacketUtilities.ReadFromStream(oMS, ref AllocationID);						            // Allocation ID
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Job Allocation Ack for mobile ID " + Convert.ToString(MobileID) + ", Allocation ID = " + Convert.ToString(AllocationID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        #endregion
                        break;
                    case "H":
                        #region Driver Channel Selection Packet - [Channel Selection][TSC]
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ChannelSelection);	                // Channel Selection
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Driver Channel Selection for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        #endregion
                        break;
                    case "I":
                        #region Driver Login Packet - [Driver Login ID] [Driver PIN][Serial Number][Firmware Ver Major] [Firmware Ver Minor] [Hardware Type][Hardware Ver Major] [Hardware Ver Minor][Odometer][Total Fuel Used][Pallets on Truck][Global Phonebook Ver][Driver Phonebook Ver][Division Group ID][Division List Ver][Delay Reason List Ver][Rejection Reason List Ver][Pallet Type List Ver][Trailer List Ver][Usage List Ver][Predefined Msg List ID] [Predefined Msg List Ver][Pre-Trip Question List ID][Pre-Trip Question List Ver][Post-Trip Question List ID][Post-Trip Question List Ver][Pickup Question List ID][Pickup Question List Ver][Delivery Question List ID][Delivery Question List Ver][Fuel Station List ID][Fuel Station List Ver][Fleet Address List Version][Driver Address List Version]
                        PacketUtilities.ReadFromStream(oMS, ref DriverLoginID);					                // Driver Login ID
                        PacketUtilities.ReadFromStream(oMS, ref DriverPIN);						                // Driver PIN
                        PacketUtilities.ReadFromStream(oMS, ref SerialNumber);		                            // Serial Number
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                    // Firmware Version Major
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                    // Firmware Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref HardwareType);		                            // Hardware Type
                        PacketUtilities.ReadFromStream(oMS, ref HardwareVersionMajor);		                    // Hardware Version Major
                        PacketUtilities.ReadFromStream(oMS, ref HardwareVersionMinor);		                    // Hardware Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref Odometer);						                // Odometer
                        PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);				                    // Total Fuel Used
                        PacketUtilities.ReadFromStream(oMS, ref PalletsOnTruck);				                // Pallets on Truck
                        PacketUtilities.ReadFromStream(oMS, ref GlobalPhonebookVer);			                // Global Phonebook Ver
                        PacketUtilities.ReadFromStream(oMS, ref DriverPhonebookVer);			                // Driver Phonebook Ver
                        PacketUtilities.ReadFromStream(oMS, ref DivisionGroupID);				                // Division Group ID
                        PacketUtilities.ReadFromStream(oMS, ref DivisionListVer);				                // Division List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.ReadFromStream(oMS, ref DelayReasonListID);			                // Delay Reason List ID
                        PacketUtilities.ReadFromStream(oMS, ref DelayReasonListVer);			                // Delay Reason List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.ReadFromStream(oMS, ref RejectReasonListID);			            // Reject Reason List ID
                        PacketUtilities.ReadFromStream(oMS, ref RejectReasonListVer);			                // Reject Reason List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.ReadFromStream(oMS, ref PalletTypeListID);			                // Pallet Type List ID
                        PacketUtilities.ReadFromStream(oMS, ref PalletTypeListVer);				                // Pallet Type List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.ReadFromStream(oMS, ref TrailerListID);			                    // Trailer List ID
                        PacketUtilities.ReadFromStream(oMS, ref TrailerListVer);				                // Trailer List Ver
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.ReadFromStream(oMS, ref UsageListID);			                    // Usage List ID
                        PacketUtilities.ReadFromStream(oMS, ref UsageListVer);					                // Usage List Ver
                        PacketUtilities.ReadFromStream(oMS, ref PredefineMsgListID);			                // Predefined Msg List ID
                        PacketUtilities.ReadFromStream(oMS, ref PredefineMsgListVer);		                    // Predefined Msg List Ver
                        PacketUtilities.ReadFromStream(oMS, ref PreTripListID);					                // Pre-Trip Question List ID
                        PacketUtilities.ReadFromStream(oMS, ref PreTripListVer);				                // Pre-Trip Question List Ver
                        PacketUtilities.ReadFromStream(oMS, ref PostTripListID);							    // Post-Trip Question List ID
                        PacketUtilities.ReadFromStream(oMS, ref PostTripListVer);							    // Post-Trip Question List Ver
                        PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListID);					        // Pickup Question List ID
                        PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListVer);				            // Pickup Question List Ver
                        PacketUtilities.ReadFromStream(oMS, ref DeliveryQuestionListID);					    // Delivery Question List ID
                        PacketUtilities.ReadFromStream(oMS, ref DeliveryQuestionListVer);				        // Delivery Question List Ver
                        PacketUtilities.ReadFromStream(oMS, ref FuelStationListID);					            // Fuel Station List ID
                        PacketUtilities.ReadFromStream(oMS, ref FuelStationListVer);				            // Fuel Station List Ver
                        try
                        {
                            if (oMS.Position + 8 < oMS.Length)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref FleetAddressListVer);				            // Fleet Address List Ver
                                PacketUtilities.ReadFromStream(oMS, ref DriverAddressListVer);				            // Vehicle Address List Ver
                            }
                            else
                            {
                                FleetAddressListVer = -1;
                                DriverAddressListVer = -1;
                            }
                            if (oMS.Position + 8 < oMS.Length)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref FleetEmailAddressListVer);				            // Fleet Email Address List Ver
                                PacketUtilities.ReadFromStream(oMS, ref DriverEmailAddressListVer);				            // Vehicle Email Address List Ver
                            }
                            else
                            {
                                FleetEmailAddressListVer = -1;
                                DriverEmailAddressListVer = -1;
                            }
                        }
                        catch (System.Exception)
                        {
                            FleetAddressListVer = -1;
                            DriverAddressListVer = -1;
                            FleetEmailAddressListVer = -1;
                            DriverEmailAddressListVer = -1;
                        }
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Driver Login Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver Login = " + Convert.ToString(DriverLoginID) + ", Password = " + Convert.ToString(DriverPIN) + ", Firmware Ver = " + Convert.ToString((int)FirmwareVersionMajor) + "." + Convert.ToString((int)FirmwareVersionMinor) + ", Hardware Type = " + Convert.ToString(HardwareType) + ", Hardware Ver = " + Convert.ToString((int)HardwareVersionMajor) + "." + Convert.ToString((int)HardwareVersionMinor));
                        #endregion
                        break;
                    case "K":
                        #region Phonecall Report Packet - [Sub Command] [Call Duration][Phone Number][TSC][SIM Card Number][TSC]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        PacketUtilities.ReadFromStream(oMS, ref PhoneCallDuration);				                // Call Duration
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref PhoneNumber);	                    // Phone Number
                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref SimCardNumber);	                // Sim Card Number	
                        if (oLogging != null && _bLogOutbound)
                        {
                            if (SubCommandType == "I")
                                oLogging.Info("Reading Incomming Phonecall Report Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            if (SubCommandType == "O")
                                oLogging.Info("Reading Outgoing Phonecall Report Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            if (SubCommandType == "S")
                                oLogging.Info("Reading Incomming SMS Report Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            if (SubCommandType == "T")
                                oLogging.Info("Reading Outgoing SMS Report Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        }
                        #endregion
                        break;
                    case "M":
                        #region Driver Message Packet
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "C")
                        {
                            #region Message Received: [Sub Command = 'C'][Message ID]
                            PacketUtilities.ReadFromStream(oMS, ref MessageID);						            // Message ID
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Message Received Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            #endregion
                        }
                        else if (SubCommandType == "R")
                        {
                            #region Message Read: [Sub Command = 'R'][Message ID]
                            PacketUtilities.ReadFromStream(oMS, ref MessageID);						            // Message ID
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Message Read Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            #endregion
                        }
                        else if (SubCommandType == "A")
                        {
                            #region Reply to Base: [Sub Command = 'A'][Message ID][Message][TSC]
                            PacketUtilities.ReadFromStream(oMS, ref MessageID);						            // Message ID
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref Message);	                    // Message
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Message Reply Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            #endregion
                        }
                        else if (SubCommandType == "P")
                        {
                            #region Driver Initiated Pre-defined Message: [Sub Command = 'P'][Pre-defined Message List ID][Pre-defined Message Ver][Pre-defined Message ID]
                            PacketUtilities.ReadFromStream(oMS, ref PredefineMsgListID);	                    // Pre-defined Message List ID
                            PacketUtilities.ReadFromStream(oMS, ref PredefineMsgListVer);			            // Pre-defined Message Ver
                            PacketUtilities.ReadFromStream(oMS, ref PredefinedMessageID);			            // Pre-defined Message ID
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Driver Initiated Pre-defined Message Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            #endregion
                        }
                        else if (SubCommandType == "F")
                        {
                            #region Driver Initiated Free-Form Message: [Sub Command = 'F'][Message][TSC]
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref Message);	                    // Message
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Driver Initiated Free-Form Message Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            #endregion
                        }
                        else if (SubCommandType == "E")
                        {
                            #region Driver Initiated Email Message: [Sub Command = 'E'][Email Address][TSC][Message][TSC]
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref EmailAddress);	                // Email Address
                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref Message);	                    // Message
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Driver Initiated Email Message Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            #endregion
                        }
                        else if (SubCommandType == "J")
                        {
                            #region Job Email Alert Message : [Sub Command = 'J'][Email Flags][Job Dispatch ID][Leg No]
                            PacketUtilities.ReadFromStream(oMS, ref JobEmailFlags);	                            // Email Flags
                            PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);	                            // Job Dispatch ID
                            PacketUtilities.ReadFromStream(oMS, ref LegNumber);	                                // Leg Number
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Job Email Alert Message Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Leg Number = " + Convert.ToString(LegNumber));
                            #endregion
                        }
                        #endregion
                        break;
                    case "O":
                        #region Logout Packet - [Odometer][Pallets on Truck]
                        PacketUtilities.ReadFromStream(oMS, ref Odometer);								        // Odometer
                        PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);							        // Total Fuel Used
                        PacketUtilities.ReadFromStream(oMS, ref PalletsOnTruck);						        // Pallets On Truck
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Logout Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        #endregion
                        break;
                    case "P":
                        #region Pre-Trip Answers Packet  - [IsLoginAnswers][Pre-Trip Message List ID][Pre-Trip Message List Ver][Num of Answers] [ASC][Question Num][Answer][TSC] [Signature Type][Signature Length][Signature]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Pre-Trip Answers Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                        PacketUtilities.ReadFromStream(oMS, ref bIsLoginAnswers);							    // IsLoginAnswers
                        PacketUtilities.ReadFromStream(oMS, ref PreTripListID);						            // Pre-Trip Message List ID
                        PacketUtilities.ReadFromStream(oMS, ref PreTripListVer);					            // Pre-Trip Message List Ver
                        PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					                // Num of Answers
                        if ((int)NumOfAnswers > 0)
                        {
                            iListCount = (int)NumOfAnswers;
                            for (int X = 0; X < iListCount; X++)
                            {
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							        // ASC
                                if (bTemp != 0x0A)
                                {
                                    sRet = "Did not find ASC in the correct position";
                                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                    return sRet;
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							        // Question Num
                                PreTripQuestionNum.Add(bTemp);
                                sTemp = "";
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                // Answer
                                PreTripQuestionAnswer.Add(sTemp);
                            }
                        }
                        PacketUtilities.ReadFromStream(oMS, ref bSigType);								        // Signature Type
                        PacketUtilities.ReadFromStream(oMS, ref usTemp);								        // Signature Length
                        if (usTemp > 0)
                        {
                            PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	                // Signature Data
                            sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                            SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                        }
                        else
                            SignatureData = null;
                        #endregion
                        break;
                    case "Q":
                        #region Predefined Function Result Packet - [Function ID][Status][Result Length][Result]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Logout Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Function ID = " + Convert.ToString(FunctionID));
                        PacketUtilities.ReadFromStream(oMS, ref FunctionID);							        // Function ID
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Status
                        if (bTemp == (byte)0x00)
                            FunctionStatus = false;
                        else
                            FunctionStatus = true;
                        PacketUtilities.ReadFromStream(oMS, ref usTemp);								        // Result Length
                        if (usTemp > 0)
                        {
                            PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref FunctionResult);	                // Result
                        }
                        else
                        {
                            FunctionResult = new byte[0];
                        }
                        #endregion
                        break;
                    case "R":
                        #region Request Packet - [Sub Command]
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Request Packet");
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        #endregion
                        break;
                    case "S":
                        #region Job Stop Packet
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "R")
                        {
                            #region Job Packet Read by Driver - [Sub Command][Job Dispatch ID][Job Reference]
                            PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);								// Job Dispatch ID
                            PacketUtilities.ReadFromStream(oMS, ref JobID);								        // Job Reference
                            #endregion
                        }
                        else if (SubCommandType == "P")
                        {
                            #region Pickup Packet
                            //Pickup Packet - Protocol version 1
                            //[Sub Command][Job Dispatch ID][Job Reference][Job Num]
                            //[Division ID][Delay Reason ID][Odometer][Total Fuel Used][Driver Has Documentation]
                            //[SPS][Pallet Type ID][Pallet Count] [EPS]
                            //[Question List ID][Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                            //[TSC][Signature Type][Signature Length][Signature]

                            //Pickup Packet - Protocol version 2
                            //[Sub Command][Job Dispatch ID][Job Reference][Job Num]
                            //[DVLS][Division List ID][Division List Ver][Division ID] 
                            //[DRLS][Delay Rsn List ID][Delay Rsn List Ver][ [Delay Rsn ID]
                            //[EDS][Odometer][Total Fuel Used]
                            //[Driver Has Documentation]
                            //[PTS][Pallet List ID][Pallet List Version][SPS][Pallet Type ID][Pallet Count] [EPS]
                            //[QLS][Question List ID][ Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                            //[TSC] [Signature Type][Signature Length][Signature]

                            //Notes :

                            //�	If [DRLS] is set to 0x01 then there is [Delay Rsn List ID][Delay Rsn List Ver][ [Delay Rsn ID] data, otherwise these byte are omitted.
                            //�	If [EDS] is set to 0x01 then there is [Odometer][Total Fuel Used] data, otherwise these bytes are omitted.
                            //�	If [PTS] is set to 0x01 then there is [Pallet List ID][Pallet List Version] data, otherwise these bytes are omitted.  If there are any pallet entries in this packet, then this field must be included.
                            //�	If [QLS] is set to 0x01 then there is [Pickup Question List ID][Pickup Question List Ver][Num of Answers] [ASC][Question Num][Answer] data, otherwise these bytes are omitted
                            //�	Always include a signature type, set the length to 0 is there is no signature data.

                            PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);								// Job Dispatch ID
                            PacketUtilities.ReadFromStream(oMS, ref JobID);								        // Job Reference
                            PacketUtilities.ReadFromStream(oMS, ref JobNum);							        // Job Number
                            if ((int)bProtocolVer < 2)
                            {
                                #region Protocol Ver <= 2
                                if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Job Stop Pickup Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobID));
                                PacketUtilities.ReadFromStream(oMS, ref DivisionID);						        // Division ID
                                PacketUtilities.ReadFromStream(oMS, ref DelayReasonID);				                // Delay Reason ID
                                PacketUtilities.ReadFromStream(oMS, ref Odometer);									// Odometer
                                PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);								// Total Fuel Used
                                PacketUtilities.ReadFromStream(oMS, ref DriverHasDocumentation);					// Driver Has Documentation
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SPS or EPS
                                if (bTemp == (byte)0x0A)
                                {
                                    while (bTemp != (byte)0x0B)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                        PalletTypeList.Add(iTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                        PalletCount.Add(bTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // SPS or EPS
                                    }
                                }
                                if (bTemp != (byte)0x0B)
                                {
                                    sRet = "Did not find SPS or EPS in the correct position";
                                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                    return sRet;
                                }
                                PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListID);					    // Pickup Question List ID
                                PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListVer);					    // Pickup Question List Ver
                                PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					            // Num of Answers
                                if ((int)NumOfAnswers > 0)
                                {
                                    iListCount = (int)NumOfAnswers;
                                    for (int X = 0; X < iListCount; X++)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // ASC
                                        if (bTemp != 0x0A)
                                        {
                                            sRet = "Did not find ASC in the correct position";
                                            oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                            return sRet;
                                        }
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // Question Num
                                        JobLegQuestionNum.Add(bTemp);
                                        sTemp = "";
                                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                // Answer
                                        JobLegQuestionAnswer.Add(sTemp);
                                    }
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bSigType);								    // Signature Type
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);								    // Signature Length
                                if (usTemp > 0)
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	        // Signature
                                    sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                                    SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Protcol > 2
                                if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Job Stop Pickup Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobID));
                                //Pickup Packet - Protocol version 2
                                //[Sub Command][Job Dispatch ID][Job Reference][Job Num]
                                //[DVLS][Division List ID][Division List Ver][Division ID] 
                                //[DRLS][Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                //[EDS][Odometer][Total Fuel Used]
                                //[Driver Has Documentation]
                                //[PTS][Pallet List ID][Pallet List Version][SPS][Pallet Type ID][Pallet Count][EPS]
                                //[QLS][Question List ID][ Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                                //[TSC] [Signature Type][Signature Length][Signature]
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // DVLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Division List ID][Division List Ver][Division ID]
                                    PacketUtilities.ReadFromStream(oMS, ref DivisionGroupID);					    // Division List ID
                                    PacketUtilities.ReadFromStream(oMS, ref DivisionListVer);					    // Division List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref DivisionID);					        // Division ID
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // DRLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonListID);					    // Delay Rsn List ID
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonListVer);					// Delay Rsn List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonID);					        // Delay Rsn ID
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // EDS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Odometer][Total Fuel Used]
                                    PacketUtilities.ReadFromStream(oMS, ref Odometer);								// Odometer
                                    PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);							// Total Fuel Used
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref DriverHasDocumentation);					// Driver Has Documentation
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // PTS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Pallet List ID][Pallet List Version][SPS][Pallet Type ID][Pallet Count][EPS]
                                    PacketUtilities.ReadFromStream(oMS, ref PalletTypeListID);						// Pallet List ID
                                    PacketUtilities.ReadFromStream(oMS, ref PalletTypeListVer);					    // Pallet List Version
                                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								    // SPS or EPS
                                    if (bTemp == (byte)0x0A)
                                    {
                                        while (bTemp != (byte)0x0B)
                                        {
                                            PacketUtilities.ReadFromStream(oMS, ref iTemp);						    // Pallet Type ID
                                            PalletTypeList.Add(iTemp);
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);						    // Pallet Count
                                            PalletCount.Add(bTemp);
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							// SPS or EPS
                                        }
                                    }
                                    if (bTemp != (byte)0x0B)
                                    {
                                        sRet = "Did not find SPS or EPS in the correct position";
                                        oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                        return sRet;
                                    }
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // QLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Question List ID][Question List Ver][Num of Answers][ASC][Question Num][Answer]
                                    PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListID);					    // Pickup Question List ID
                                    PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListVer);					    // Pickup Question List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					            // Num of Answers
                                    if ((int)NumOfAnswers > 0)
                                    {
                                        iListCount = (int)NumOfAnswers;
                                        for (int X = 0; X < iListCount; X++)
                                        {
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // ASC
                                            if (bTemp != 0x0A)
                                            {
                                                sRet = "Did not find ASC in the correct position";
                                                oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                                return sRet;
                                            }
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // Question Num
                                            JobLegQuestionNum.Add(bTemp);
                                            sTemp = "";
                                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                // Answer
                                            JobLegQuestionAnswer.Add(sTemp);
                                        }
                                    }
                                    #endregion
                                }
                                #region Signature
                                PacketUtilities.ReadFromStream(oMS, ref bSigType);								    // Signature Type
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);								    // Signature Length
                                if (usTemp > 0)
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	        // Signature
                                    sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                                    SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        else if (SubCommandType == "D" || SubCommandType == "d")
                        {
                            #region Delivery Packet
                            //Protocol Version <= 2

                            //[Sub Command][Job Dispatch ID][Job Reference][Job Num][Leg Number][IsReturnDelivery][Delay Reason ID][Reject Reason ID][Odometer][Total Fuel Used][POD Name][TSC]
                            //[SPS][Pallet Type ID][Pallet Count] [EPS]
                            //[SRPS][Pallet Type ID][Pallet Count] [ERPS]
                            //[Exchange Pallet Order][TSC][SEPS][Pallet Type ID][Pallet Count] [EEPS]
                            //[Full Return][SEPS][Pallet Type ID][Pallet Count] [EEPS]
                            //[Question List ID][Question List Ver]
                            //[Num of Answers] [ASC][Question Num][Answer][TSC] 
                            //[Signature Type][Signature Length][Signature]

                            //Protocol Version > 2

                            //[Sub Command][Job Dispatch ID][Job Reference][Job Num][Leg Number][IsReturnDelivery]
                            //[DRLS][Delay Rsn List ID][Delay Rsn List Ver][ [Delay Rsn ID]
                            //[RRLS ][Reject Rsn List ID ][ Reject Rsn List Ver ][Reject Rsn ID]
                            //[EDS][Odometer][Total Fuel Used]
                            //[PODS ][POD Name][TSC]
                            //[PTS][Pallet List ID][Pallet List Version]
                            //[SPS][Pallet Type ID][Pallet Count] [EPS]
                            //[SRPS][Pallet Type ID][Pallet Count] [ERPS]
                            //[EPLS][Exchange Pallet Order][TSC][SEPS][Pallet Type ID][Pallet Count] [EEPS]
                            //[Full Return][SEPS][Pallet Type ID][Pallet Count] [EEPS]
                            //[QLS][Question List ID][Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                            //[TSC] [Signature Type][Signature Length][Signature]

                            VehicleDeviceTimer++;
                            PacketUtilities.ReadFromStream(oMS, ref JobDispatchID);								// Job Dispatch ID
                            PacketUtilities.ReadFromStream(oMS, ref JobID);								        // Job Reference
                            PacketUtilities.ReadFromStream(oMS, ref JobNum);							        // Job Number
                            PacketUtilities.ReadFromStream(oMS, ref LegNumber);						            // Leg Number
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Job Stop Delivery Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Job Dispatch ID = " + Convert.ToString(JobDispatchID) + ", Reference Number = " + Convert.ToString(JobID) + " Leg Number = " + Convert.ToString(LegNumber));
                            PacketUtilities.ReadFromStream(oMS, ref bTemp);                                     // IsReturnDelivery
                            if (bTemp == (byte)0x01)
                                IsReturnDelivery = true;
                            else
                                IsReturnDelivery = false;
                            if ((int)bProtocolVer < 2)
                            {
                                #region Protocol Ver <= 2
                                PacketUtilities.ReadFromStream(oMS, ref DelayReasonID);				                // Delay Reason ID
                                PacketUtilities.ReadFromStream(oMS, ref RejectReasonID);				            // Reject Reason ID
                                PacketUtilities.ReadFromStream(oMS, ref Odometer);									// Odometer
                                PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);								// Total Fuel Used
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref PODName);	                    // POD Name
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SPS or EPS
                                if (bTemp == (byte)0x0A)
                                {
                                    while (bTemp != (byte)0x0B && oMS.Position < bData.Length)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                        PalletTypeList.Add(iTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                        PalletCount.Add(bTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SPS or EPS
                                    }
                                }
                                if (bTemp != (byte)0x0B)
                                {
                                    sRet = "Did not find SPS or EPS in the correct position";
                                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                    return sRet;
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SRPS or ERPS
                                if (bTemp == (byte)0x0E)
                                {
                                    while (bTemp != (byte)0x0F && oMS.Position < bData.Length)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                        ReturnPalletTypeList.Add(iTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                        ReturnPalletCount.Add(bTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SRPS or ERPS
                                    }
                                }
                                if (bTemp != (byte)0x0F)
                                {
                                    sRet = "Did not find SRPS or ERPS in the correct position";
                                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                    return sRet;
                                }
                                PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ExchangePalletOrder);	        // POD Name
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SEPS or EEPS
                                if (bTemp == (byte)0x0C)
                                {
                                    while (bTemp != (byte)0x0D && oMS.Position < bData.Length)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                        ExchangePalletTypeList.Add(iTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                        ExchangePalletCount.Add(bTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SEPS or EEPS
                                    }
                                }
                                if (bTemp != (byte)0x0D)
                                {
                                    sRet = "Did not find SEPS or EEPS in the correct position";
                                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                    return sRet;
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						                // Full Return
                                if (bTemp == (byte)0x01)
                                    bFullReturn = true;
                                else
                                    bFullReturn = false;
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);								        // SEPS or EEPS
                                if (bTemp == (byte)0x0C)
                                {
                                    while (bTemp != (byte)0x0D && oMS.Position < bData.Length)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                        EmptyPalletTypeList.Add(iTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                        EmptyPalletCount.Add(bTemp);
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SEPS or EEPS
                                    }
                                }
                                if (bTemp != (byte)0x0D)
                                {
                                    sRet = "Did not find SEPS or EEPS in the correct position";
                                    oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                    return sRet;
                                }
                                PacketUtilities.ReadFromStream(oMS, ref DeliveryQuestionListID);					// Delivery Question List ID
                                PacketUtilities.ReadFromStream(oMS, ref DeliveryQuestionListVer);					// Delivery Question List Ver
                                PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					            // Num of Answers
                                if ((int)NumOfAnswers > 0)
                                {
                                    iListCount = (int)NumOfAnswers;
                                    for (int X = 0; X < iListCount; X++)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // ASC
                                        if (bTemp != 0x0A)
                                        {
                                            sRet = "Did not find ASC in the correct position";
                                            oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                            return sRet;
                                        }
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // Question Num
                                        JobLegQuestionNum.Add(bTemp);
                                        sTemp = "";
                                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                // Answer
                                        JobLegQuestionAnswer.Add(sTemp);
                                    }
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bSigType);								    // Signature Type
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);								    // Signature Length
                                if (usTemp > 0)
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	            // Signature
                                    sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                                    SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Protcol Ver > 2
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // DRLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonListID);					    // Delay Rsn List ID
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonListVer);					// Delay Rsn List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonID);					        // Delay Rsn ID
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // RRLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Reject Rsn List ID][Reject Rsn List Ver][Reject Rsn ID]
                                    PacketUtilities.ReadFromStream(oMS, ref RejectReasonListID);					// Reject Rsn List ID
                                    PacketUtilities.ReadFromStream(oMS, ref RejectReasonListVer);					// Reject Rsn List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref RejectReasonID);					    // Reject Rsn ID
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // EDS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Odometer][Total Fuel Used]
                                    PacketUtilities.ReadFromStream(oMS, ref Odometer);								// Odometer
                                    PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);							// Total Fuel Used
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // PODS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [POD Name][TSC]
                                    PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref PODName);	                // POD Name
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // PTS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Pallet List ID][Pallet List Version]
                                    PacketUtilities.ReadFromStream(oMS, ref PalletTypeListID);						// Pallet List ID
                                    PacketUtilities.ReadFromStream(oMS, ref PalletTypeListVer);						// Pallet List Version
                                    #endregion
                                    #region Delivered Pallets
                                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								     // SPS or EPS
                                    if (bTemp == (byte)0x0A)
                                    {
                                        #region [Pallet Type ID][Pallet Count] ([SPS] or [EPS])
                                        while (bTemp != (byte)0x0B && oMS.Position < bData.Length)
                                        {
                                            PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                            PalletTypeList.Add(iTemp);
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                            PalletCount.Add(bTemp);
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SPS or EPS
                                        }
                                        #endregion
                                    }
                                    if (bTemp != (byte)0x0B)                                                         // EPS
                                    {
                                        sRet = "Did not find SPS or EPS in the correct position";
                                        oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                        return sRet;
                                    }
                                    #endregion
                                    #region Rejected Pallets
                                    PacketUtilities.ReadFromStream(oMS, ref bTemp);								    // SRPS or ERPS
                                    if (bTemp == (byte)0x0E)
                                    {
                                        #region [Pallet Type ID][Pallet Count] ([SRPS] or [ERPS])
                                        while (bTemp != (byte)0x0F && oMS.Position < bData.Length)
                                        {
                                            PacketUtilities.ReadFromStream(oMS, ref iTemp);						        // Pallet Type ID
                                            ReturnPalletTypeList.Add(iTemp);
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // Pallet Count
                                            ReturnPalletCount.Add(bTemp);
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);						        // SRPS or ERPS
                                        }
                                        #endregion
                                    }
                                    if (bTemp != (byte)0x0F)                                                        // ERPS
                                    {
                                        sRet = "Did not find SRPS or ERPS in the correct position";
                                        oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                        return sRet;
                                    }
                                    #endregion
                                    #region Exchange Pallets
                                    PacketUtilities.ReadFromStream(oMS, ref bTemp);							        // EPLS
                                    if (bTemp == (byte)0x01)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref ExchangePalletOrder);	// Exchange Pallet Order
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);								// SEPS or EEPS
                                        if (bTemp == (byte)0x0C)
                                        {
                                            #region [Pallet Type ID][Pallet Count] ([SEPS] or [EEPS])
                                            while (bTemp != (byte)0x0D && oMS.Position < bData.Length)
                                            {
                                                PacketUtilities.ReadFromStream(oMS, ref iTemp);						// Pallet Type ID
                                                ExchangePalletTypeList.Add(iTemp);
                                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						// Pallet Count
                                                ExchangePalletCount.Add(bTemp);
                                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						// SEPS or EEPS
                                            }
                                            #endregion
                                        }
                                        if (bTemp != (byte)0x0D)                                                    // EEPS
                                        {
                                            sRet = "Did not find SEPS or EEPS in the correct position";
                                            oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                            return sRet;
                                        }
                                    }
                                    #endregion
                                    #region Empty Pallets
                                    PacketUtilities.ReadFromStream(oMS, ref bTemp);						            // Full Return
                                    if (bTemp == (byte)0x01)
                                        bFullReturn = true;
                                    else
                                        bFullReturn = false;
                                    if (bFullReturn)
                                    {
                                        #region [SEPS][Pallet Type ID][Pallet Count] ([SEPS] or [EEPS])
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);								    // SEPS or EEPS
                                        if (bTemp == (byte)0x0C)
                                        {
                                            while (bTemp != (byte)0x0D && oMS.Position < bData.Length)
                                            {
                                                PacketUtilities.ReadFromStream(oMS, ref iTemp);						    // Pallet Type ID
                                                EmptyPalletTypeList.Add(iTemp);
                                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						    // Pallet Count
                                                EmptyPalletCount.Add(bTemp);
                                                PacketUtilities.ReadFromStream(oMS, ref bTemp);						    // SEPS or EEPS
                                            }
                                        }
                                        if (bTemp != (byte)0x0D)
                                        {
                                            sRet = "Did not find SEPS or EEPS in the correct position";
                                            oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                            return sRet;
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // QLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Question List ID][Question List Ver][Num of Answers][ASC][Question Num][Answer]
                                    PacketUtilities.ReadFromStream(oMS, ref DeliveryQuestionListID);				// Delivery Question List ID
                                    PacketUtilities.ReadFromStream(oMS, ref DeliveryQuestionListVer);				// Delivery Question List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					        // Num of Answers
                                    if ((int)NumOfAnswers > 0)
                                    {
                                        iListCount = (int)NumOfAnswers;
                                        for (int X = 0; X < iListCount; X++)
                                        {
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							// ASC
                                            if (bTemp != 0x0A)
                                            {
                                                sRet = "Did not find ASC in the correct position";
                                                oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                                return sRet;
                                            }
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							// Question Num
                                            JobLegQuestionNum.Add(bTemp);
                                            sTemp = "";
                                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	            // Answer
                                            JobLegQuestionAnswer.Add(sTemp);
                                        }
                                    }
                                    #endregion
                                }
                                #region Signature Data
                                PacketUtilities.ReadFromStream(oMS, ref bSigType);								    // Signature Type
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);								    // Signature Length
                                if (usTemp > 0)
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	        // Signature
                                    sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                                    SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        else if (SubCommandType == "G")
                        {
                            #region Group Pickup Packet
                            //Protocol Version <= 2

                            //[Sub Command][Job Reference][Division ID][Delay Reason ID][Odometer][Total Fuel Used][Driver Has Documentation]
                            //[Pickup Question List ID][Pickup Question List Ver][Num of Answers] [ASC][Question Num][Answer][TSC] [Signature Type][Signature Length][Signature]

                            //Protocol Versions > 2

                            //[Sub Command][Job Reference]
                            //[DVLS][Division List ID][Division List Ver][Division ID] 
                            //[DRLS][Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                            //[EDS][Odometer][Total Fuel Used]
                            //[Driver Has Documentation]
                            //[QLS][Question List ID][Question List Ver][Num of Answers] [ASC][Question Num][Answer]
                            //[TSC] [Signature Type][Signature Length][Signature]

                            PacketUtilities.ReadFromStream(oMS, ref JobID);								            // Job Reference
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Job Group Pickup Packet (Protocol : " + Convert.ToString((int)bProtocolVer) + ") for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Reference Number = " + Convert.ToString(JobID));
                            if ((int)bProtocolVer < 2)
                            {
                                #region Protocol Ver <= 2
                                PacketUtilities.ReadFromStream(oMS, ref DivisionID);						        // Division ID
                                PacketUtilities.ReadFromStream(oMS, ref DelayReasonID);				                // Delay Reason ID
                                PacketUtilities.ReadFromStream(oMS, ref Odometer);									// Odometer
                                PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);								// Total Fuel Used
                                PacketUtilities.ReadFromStream(oMS, ref DriverHasDocumentation);					// Driver Has Documentation
                                PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListID);					    // Pickup Question List ID
                                PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListVer);					    // Pickup Question List Ver
                                PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					            // Num of Answers
                                if ((int)NumOfAnswers > 0)
                                {
                                    iListCount = (int)NumOfAnswers;
                                    for (int X = 0; X < iListCount; X++)
                                    {
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // ASC
                                        if (bTemp != 0x0A)
                                        {
                                            sRet = "Did not find ASC in the correct position";
                                            oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                            return sRet;
                                        }
                                        PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // Question Num
                                        JobLegQuestionNum.Add(bTemp);
                                        sTemp = "";
                                        PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                // Answer
                                        JobLegQuestionAnswer.Add(sTemp);
                                    }
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bSigType);								    // Signature Type
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);								    // Signature Length
                                if (usTemp > 0)
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	        // Signature
                                    sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                                    SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                                }
                                #endregion
                            }
                            else
                            {
                                #region Protocol Ver > 2
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // DVLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Division List ID][Division List Ver][Division ID]
                                    PacketUtilities.ReadFromStream(oMS, ref DivisionGroupID);					    // Division List ID
                                    PacketUtilities.ReadFromStream(oMS, ref DivisionListVer);					    // Division List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref DivisionID);					        // Division ID
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // DRLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Delay Rsn List ID][Delay Rsn List Ver][Delay Rsn ID]
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonListID);					    // Delay Rsn List ID
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonListVer);					// Delay Rsn List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref DelayReasonID);					        // Delay Rsn ID
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // EDS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Odometer][Total Fuel Used]
                                    PacketUtilities.ReadFromStream(oMS, ref Odometer);								// Odometer
                                    PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);							// Total Fuel Used
                                    #endregion
                                }
                                PacketUtilities.ReadFromStream(oMS, ref DriverHasDocumentation);					// Driver Has Documentation
                                PacketUtilities.ReadFromStream(oMS, ref bTemp);							            // QLS
                                if (bTemp == (byte)0x01)
                                {
                                    #region [Question List ID][Question List Ver][Num of Answers][ASC][Question Num][Answer]
                                    PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListID);					    // Pickup Question List ID
                                    PacketUtilities.ReadFromStream(oMS, ref PickupQuestionListVer);					    // Pickup Question List Ver
                                    PacketUtilities.ReadFromStream(oMS, ref NumOfAnswers);					            // Num of Answers
                                    if ((int)NumOfAnswers > 0)
                                    {
                                        iListCount = (int)NumOfAnswers;
                                        for (int X = 0; X < iListCount; X++)
                                        {
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // ASC
                                            if (bTemp != 0x0A)
                                            {
                                                sRet = "Did not find ASC in the correct position";
                                                oLogging.Info("Server Packet Parse Error : " + sRet + " Byte Data " + BitConverter.ToString(bData, 0));
                                                return sRet;
                                            }
                                            PacketUtilities.ReadFromStream(oMS, ref bTemp);							    // Question Num
                                            JobLegQuestionNum.Add(bTemp);
                                            sTemp = "";
                                            PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sTemp);	                // Answer
                                            JobLegQuestionAnswer.Add(sTemp);
                                        }
                                    }
                                    #endregion
                                }
                                #region Signature
                                PacketUtilities.ReadFromStream(oMS, ref bSigType);								    // Signature Type
                                PacketUtilities.ReadFromStream(oMS, ref usTemp);								    // Signature Length
                                if (usTemp > 0)
                                {
                                    PacketUtilities.ReadFromStream(oMS, (int)usTemp, ref SignatureData);	        // Signature
                                    sSigFileName = sSigFilePath + "Fleet " + Convert.ToString(FleetID) + " Vehicle " + Convert.ToString(VehicleID) + " Signature - " + System.DateTime.Now.Day.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Month.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Year.ToString().PadLeft(4, '0') + " " + System.DateTime.Now.Hour.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Minute.ToString().PadLeft(2, '0') + "-" + System.DateTime.Now.Second.ToString().PadLeft(2, '0') + ".gif";
                                    SignatureData = this.DecodeSignature(SignatureData, sSigFileName);
                                }
                                #endregion
                                #endregion
                            }
                            #endregion
                        }
                        #endregion
                        break;
                    case "T":
                        #region Terminal Startup Packet - [Mobile ID][Firmware Ver Major][Firmware Ver Minor][Temperature Major][Temperature Minor][Battery Percentage][MDT Option Flags][MDT Variant Name][EOVN]
                        PacketUtilities.ReadFromStream(oMS, ref usTemp);								        // Mobile ID
                        MobileID = Convert.ToInt32(usTemp);
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMajor);		                    // Firmware Version Major
                        PacketUtilities.ReadFromStream(oMS, ref FirmwareVersionMinor);		                    // Firmware Version Minor
                        PacketUtilities.ReadFromStream(oMS, ref TemperatureMajor);				                // Temperature Major
                        PacketUtilities.ReadFromStream(oMS, ref TemperatureMinor);				                // Temperature Minor
                        PacketUtilities.ReadFromStream(oMS, ref BatteryPercentage);				                // Battery Percentage
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags1);				                // MDT Options Flags Byte 1
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags2);			                    // MDT Options Flags Byte 2
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags3);			                    // MDT Options Flags Byte 3
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags4);			                    // MDT Options Flags Byte 4
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags5);			                    // MDT Options Flags Byte 5
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags6);			                    // MDT Options Flags Byte 6
                        PacketUtilities.ReadFromStream(oMS, ref MDTOptionsFlags7);			                    // MDT Options Flags Byte 7
                        PacketUtilities.ReadFromStream(oMS, (byte)0x0A, ref MDTVariantName);	                // MDT Variant Name
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Terminal Startup Packet for mobile ID " + Convert.ToString(MobileID));
                        #endregion
                        break;
                    case "V":
                        #region Trailer Usage Packet - Hitch and De-hitch messages -[Sub Command][Trailer ID][Trailer ESN] - Trailer Query [Sub Command][Trailer ID][Trailer ESN]
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Sub Command Type
                        SubCommandType = Convert.ToString((char)bTemp);
                        if (SubCommandType == "Q")
                        {
                            #region Create trailer query record
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Trailer Query Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            PacketUtilities.ReadFromStream(oMS, ref TrailerID);	                                // Trailer Name
                            PacketUtilities.ReadFromStream(oMS, (int)6, ref TrailerESN);			            // Trailer ESN
                            #endregion
                        }
                        else
                        {
                            #region Create Hitch/De-hitch record
                            if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Trailer Hitch/Dehitch Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID));
                            PacketUtilities.ReadFromStream(oMS, ref TrailerID);							        // Trailer ID
                            PacketUtilities.ReadFromStream(oMS, (int)6, ref TrailerESN);			            // Trailer ESN
                            PacketUtilities.ReadFromStream(oMS, ref Odometer);							        // Odometer
                            if ((int)bProtocolVer > 2)
                                PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);							// Total Fuel Used
                            #endregion
                        }
                        #endregion
                        break;
                    case "U":
                        #region Vehicle Usage Packet -  [ListID][List Version][Item ID][Start Or End][Odometer][Total Fuel Used]
                        if ((int)bProtocolVer > 2)
                            PacketUtilities.ReadFromStream(oMS, ref ListID);						            // List ID
                        PacketUtilities.ReadFromStream(oMS, ref ListVersion);						            // List Version
                        PacketUtilities.ReadFromStream(oMS, ref UsageTypeID);						            // Usage Type ID
                        PacketUtilities.ReadFromStream(oMS, ref bTemp);									        // Start or End of Usage
                        if (bTemp == (byte)0x46)
                            IsUsageStart = false;
                        else
                            IsUsageStart = true;
                        PacketUtilities.ReadFromStream(oMS, ref Odometer);									    // Odometer
                        PacketUtilities.ReadFromStream(oMS, ref TotalFuelUsed);								    // Total Fuel Used
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading Vehicle Usage Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID) + ", Driver ID = " + Convert.ToString(DriverID) + ", Usage Type ID = " + Convert.ToString(UsageTypeID));

                        #endregion
                        break;
                    case "Z":
                        #region 5080 Asset Packet - [CustomerBuildMajor][CustomerBuildMinor][CustomerName][TSC][MapDllMajor][MapDllMinor][MapDataMajor][MapDataMinor][MapDataName][TSC][ShellMajor][ShellMinor][ApiMajor][ApiMinor][CoreMajor][CoreMinor][CoreUiMajor][CoreUiMinor][GuiMajor][GuiMinor][IapMajor][IapMinor][JobsMajor][JobsMinor][MapMajor][MapMinor][PreTripMajor][PreTripMinor][ResourcesMajor][ResourcesMinor][ScreenMajor][ScreenMinor][RouteMajor][RouteMinor] [WebServiceMajor][WebServiceMinor][SysMajor][SysMinor][PhoneMajor][PhoneMinor][DownloadUpdatesMajor][DownloadUpdatesMinor][PaltformMajor][PlatformMinor][WinCEMajor][WinCEMinor][CFCardTotal][CFCardFree][MCCQueueCount][Battery]
                        o5080AssetPacket = new c5080AssetPacket();
                        o5080AssetPacket.Decode(oMS);
                        if (oLogging != null && _bLogOutbound) oLogging.Info("Reading 5080 Asset Packet for mobile ID " + Convert.ToString(MobileID) + ", Fleet ID = " + Convert.ToString(FleetID) + ", Vehicle ID = " + Convert.ToString(VehicleID));
                        #endregion
                        break;
                }
            }
            catch (System.Exception ex)
            {
                if (bData != null)
                {
                    if (oLogging != null) { oLogging.Error(sClassName + "TranslateMsgToHost(byte[] bData = " + BitConverter.ToString(bData) + ", string sUnitID)", ex); }
                }
                else
                {
                    if (oLogging != null) { oLogging.Error(sClassName + "TranslateMsgToHost(byte[] bData = null, string sUnitID)", ex); }
                }
            }
            return sRet;
        }
        #endregion
        #region Utils
        private string ConvertPhoneNumber(string sPhoneNumber)
        {
            string sRet = "";
            char[] sSplit = null;
            try
            {
                sSplit = sPhoneNumber.ToUpper().ToCharArray();
                for (int X = 0; X < sSplit.Length; X++)
                {
                    if (Convert.ToInt32((byte)sSplit[X]) < 48 || Convert.ToInt32((byte)sSplit[X]) > 57)
                    {
                        #region If the entry is a letter A-Z, map it to the number
                        if (sSplit[X] == '*' || sSplit[X] == '#')
                            sRet += Convert.ToString(sSplit[X]);
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 65 && Convert.ToInt32((byte)sSplit[X]) <= 67)
                            sRet += "2";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 68 && Convert.ToInt32((byte)sSplit[X]) <= 70)
                            sRet += "3";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 71 && Convert.ToInt32((byte)sSplit[X]) <= 73)
                            sRet += "4";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 74 && Convert.ToInt32((byte)sSplit[X]) <= 76)
                            sRet += "5";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 77 && Convert.ToInt32((byte)sSplit[X]) <= 79)
                            sRet += "6";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 80 && Convert.ToInt32((byte)sSplit[X]) <= 83)
                            sRet += "7";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 84 && Convert.ToInt32((byte)sSplit[X]) <= 86)
                            sRet += "8";
                        else if (Convert.ToInt32((byte)sSplit[X]) >= 87 && Convert.ToInt32((byte)sSplit[X]) <= 90)
                            sRet += "9";
                        #endregion
                    }
                    else
                        sRet += Convert.ToString(sSplit[X]);
                }
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "ConvertPhoneNumber(string sPhoneNumber = '" + sPhoneNumber + "')", ex);
                sRet = sPhoneNumber;
            }
            return sRet;
        }
        public string GetPacketTypeName()
        {
            string sRet = "Unknown Packet";
            try
            {
                switch (CommandType)
                {
                    case "A":
                        sRet = "Arrive at Site Packet";
                        break;
                    case "c":
                        if (SubCommandType == "d")
                            sRet = "Download CPLD Header Packet";
                        if (SubCommandType == "r")
                            sRet = "Download CPLD Data Segment Packet";
                        else if (SubCommandType == "v")
                            sRet = "Download CPLD Verify Packet";
                        else
                            sRet = "Unknown CPLD Download Packet";
                        break;
                    case "d":
                        if (SubCommandType == "d")
                            sRet = "Download Win CE Header Packet";
                        if (SubCommandType == "r")
                            sRet = "Download Win CE Data Segment Packet";
                        else if (SubCommandType == "v")
                            sRet = "Download Win CE Verify Packet";
                        else
                            sRet = "Unknown Win CE Download Packet";
                        break;
                    case "D":
                        if (SubCommandType == "d")
                            sRet = "Download MOT File Header Packet";
                        if (SubCommandType == "r")
                            sRet = "Download MOT File Data Segment Packet";
                        else if (SubCommandType == "v")
                            sRet = "Download MOT File Verify Packet";
                        else
                            sRet = "Unknown MOT Download Packet";
                        break;
                    case "E":
                        sRet = "Fuel Report Packet";
                        break;
                    case "F":
                        sRet = "Lunch Break Packet";
                        break;
                    case "G":
                        sRet = "Allocation Ack Packet";
                        break;
                    case "H":
                        sRet = "Driver Channel Selection Packet";
                        break;
                    case "I":
                        sRet = "Driver Login Packet ";
                        break;
                    case "K":
                        if (SubCommandType == "I")
                            sRet = "Incomming Phonecall Packet";
                        if (SubCommandType == "O")
                            sRet = "Outgoing Phonecall Packet";
                        else if (SubCommandType == "S")
                            sRet = "Incomming SMS Packet";
                        else if (SubCommandType == "T")
                            sRet = "Outgoing SMS Packet";
                        else
                            sRet = "Unknown Phonecall Packet";
                        break;
                    case "M":
                        if (SubCommandType == "C")
                            sRet = "Message Received";
                        else if (SubCommandType == "R")
                            sRet = "Message Read By Driver";
                        else if (SubCommandType == "A")
                            sRet = "Driver Message Responce";
                        else if (SubCommandType == "P")
                            sRet = "Driver Initiated Pre-defined Message";
                        else if (SubCommandType == "F")
                            sRet = "Driver Initiated Free-Form Message";
                        else if (SubCommandType == "E")
                            sRet = "Driver Initiated Email Message";
                        else if (SubCommandType == "J")
                            sRet = "Job Email Alert Message";
                        else
                            sRet = "Unknown Message Packet";
                        break;
                    case "O":
                        sRet = "Logout Packet";
                        break;
                    case "P":
                        sRet = "Pre-Trip Answers Packet";
                        break;
                    case "Q":
                        sRet = "Predefined Function Result Packet";
                        break;
                    case "R":
                        sRet = "Request Job Packet";
                        break;
                    case "S":
                        if (SubCommandType == "P")
                            sRet = "Job Stop Pickup Packet";
                        else if (SubCommandType == "D")
                            sRet = "Job Stop Delivery Packet";
                        else
                            sRet = "Unknown Job Packet";
                        break;
                    case "T":
                        sRet = "Trailer Usage Packet";
                        break;
                    case "V":
                        if (SubCommandType == "Q")
                            sRet = "Trailer Query Packet";
                        else
                            sRet = "Trailer Usage Packet";
                        break;
                    case "U":
                        sRet = "Vehicle Usage Packet";
                        break;
                    case "Z":
                        sRet = "Asset Packet";
                        break;
                }
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "GetPacketTypeName()", ex);
                sRet = "Unknown Packet";
            }
            return sRet;
        }
        public static double ConvLatLon(uint uiLatLon)
        {
            uint tempUint = 0;
            double dLatLon = 0;
            bool bNeg = false;

            if ((uiLatLon & 0x10000000) == 0x10000000)
            {
                tempUint = uiLatLon - (int)0x10000000;
                bNeg = true;
            }
            else
                tempUint = uiLatLon;
            dLatLon = Convert.ToDouble(tempUint) / 600000;
            if (bNeg)
                dLatLon = dLatLon * -1;
            return dLatLon;
        }
        public static uint ConvLatLon(double dLatLon)
        {
            uint iResult = 0;

            if (dLatLon < 0)
            {
                iResult = Convert.ToUInt32((dLatLon * -1) * 600000);
                iResult = iResult + (int)0x10000000;
            }
            else
            {
                iResult = Convert.ToUInt32(dLatLon * 600000);
            }
            return iResult;
        }
        private byte[] DecodeSignature(byte[] bData, string sFileName)
        {
            Bitmap oBMP = null;
            byte[] bRet = null;
            byte[] bConvert = null;
            ArrayList oEncodedBytes = new ArrayList();
            ArrayList oDecodedBytes = new ArrayList();
            int iPos = 0;
            int iTop = 0;
            int iLeft = 0;
            int iWidth = 0;
            int iHeight = 0;
            int iWhiteBits = 0;
            int iBlackBits = 0;
            int iState = 0;
            int iMask = 0;
            int iShift = 0;
            int iTemp = 0;
            byte bTemp = (byte)0x00;
            string sInstruction = "";
            string sColor = "";
            int iLength = 0;
            int iRow = 0;

            try
            {
                if (bSigType == (byte)0x0A)
                    oLogging.Info("Generating Signature Image Type - Bitmap");
                else
                {
                    if (bSigType != (byte)0x0B)
                        oLogging.Info("Generating Signature Image Type - Vector (Ver 1)");
                    else
                        oLogging.Info("Generating Signature Image Type - Vector (Ver 2)");
                }
                if (bSigType == (byte)0x0A)
                {
                    #region Bitmap encoded signature
                    #region Read the header information (Top, Left, Height, Width, Black Bits, White Bits)
                    // Get the top
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iTop = BitConverter.ToInt32(bConvert, 0);
                    // Get the left
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iLeft = BitConverter.ToInt32(bConvert, 0);
                    // Get the width
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iWidth = BitConverter.ToInt32(bConvert, 0);
                    // Get the height
                    bConvert = new byte[4];
                    for (int X = 0; X < 4; X++)
                        bConvert[X] = bData[iPos++];
                    iHeight = BitConverter.ToInt32(bConvert, 0);
                    // Get the number of white bits in the encoding
                    bConvert = new byte[4];
                    bConvert[0] = bData[iPos++];
                    bConvert[1] = bData[iPos++];
                    iWhiteBits = BitConverter.ToInt32(bConvert, 0);
                    // Get the number of black bits in the encoding
                    bConvert = new byte[4];
                    bConvert[0] = bData[iPos++];
                    bConvert[1] = bData[iPos++];
                    iBlackBits = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    #region Expand the byte array out to a list of bits
                    while (iPos < bData.Length)
                    {
                        bTemp = bData[iPos++];
                        for (int X = 7; X >= 0; X--)
                        {
                            iMask = 1 << X;
                            if (((int)bTemp & iMask) == iMask)
                                oEncodedBytes.Add(1);
                            else
                                oEncodedBytes.Add(0);
                        }
                    }
                    #endregion
                    #region Decode the data to create an array list (oDecodedBytes) of instructions ("B~X" for X black bytes or "W~X" for X white bytes)
                    iState = 0;
                    for (int X = 0; X < oEncodedBytes.Count; X++)
                    {
                        switch (iState)
                        {
                            case 0:
                                #region Determine if we are decoding black or white
                                if ((int)oEncodedBytes[X] == 0)
                                {
                                    iState = 1;
                                    iShift = iBlackBits - 1;
                                }
                                else
                                {
                                    iState = 2;
                                    iShift = iWhiteBits - 1;
                                }
                                iTemp = 0;
                                #endregion
                                break;
                            case 1:
                                #region Decode Black Bytes
                                if (iShift == 0)
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    oDecodedBytes.Add("B~" + Convert.ToString(iTemp));
                                    iState = 0;
                                }
                                else
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    iShift--;
                                }
                                #endregion
                                break;
                            case 2:
                                #region Decode White Bytes
                                if (iShift == 0)
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    oDecodedBytes.Add("W~" + Convert.ToString(iTemp));
                                    iState = 0;
                                }
                                else
                                {
                                    iTemp += (int)oEncodedBytes[X] << iShift;
                                    iShift--;
                                }
                                #endregion
                                break;
                        }
                    }
                    #endregion
                    #region If decoded bytes exist
                    if (oDecodedBytes.Count > 0)
                    {
                        string sDefaultBMP = System.Configuration.ConfigurationManager.AppSettings["BitmapBackgroundImage"];
                        if (!sDefaultBMP.Contains(":"))
                        {
                            string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                            path = System.IO.Path.GetDirectoryName(path);
                            sDefaultBMP = path + "\\" + sDefaultBMP;
                        }
                        FileStream oFS = new FileStream(sDefaultBMP, FileMode.Open);
                        oBMP = (Bitmap)Bitmap.FromStream(oFS);
                        oFS.Close();
                        oFS = null;
                        iPos = iLeft;
                        iRow = iTop;
                        oLogging.Info("Generating Image");
                        for (int Z = 0; Z < oDecodedBytes.Count; Z++)
                        {
                            sInstruction = (string)oDecodedBytes[Z];
                            sColor = sInstruction.Split("~".ToCharArray())[0];
                            iLength = Convert.ToInt32(sInstruction.Split("~".ToCharArray())[1]);
                            if (sColor == "B")
                            {
                                for (int X = 0; X < iLength; X++)
                                {
                                    if ((iPos - iLeft) > iWidth)
                                    {
                                        iRow++;
                                        iPos = iLeft;
                                    }
                                    oBMP.SetPixel(iPos, iRow, Color.Black);
                                    iPos++;
                                }
                            }
                            else
                            {
                                for (int X = 0; X < iLength; X++)
                                {
                                    if ((iPos - iLeft) > iWidth)
                                    {
                                        iRow++;
                                        iPos = iLeft;
                                    }
                                    iPos++;
                                }
                            }
                        }

                        oLogging.Info("Saving Signature to " + sFileName);
                        oBMP.Save(sFileName, System.Drawing.Imaging.ImageFormat.Gif);
                        oBMP.Dispose();
                        oBMP = null;
                        oFS = new FileStream(sFileName, FileMode.Open);
                        bRet = new byte[oFS.Length];
                        oFS.Read(bRet, 0, Convert.ToInt32(oFS.Length));
                        oFS.Close();
                        oFS = null;
                    }
                    else
                    {
                        oLogging.Info("Blank Signature on Packet.");
                        bRet = new byte[1];
                        bRet[0] = (byte)0x00;
                    }
                    #endregion
                    #endregion
                }
                else if (bSigType == (byte)0x0B)
                {
                    #region Vector Encoded Signature
                    cVectorSignature oVectorSig = new cVectorSignature(oSigConfig.LogSignatureData, oSigConfig.MaxHeight, oSigConfig.MaxWidth, oSigConfig.MaxVectorHeight, oSigConfig.MaxVectorWidth, oSigConfig.PicturePath, oSigConfig.DefaultBackgroundGif, 0, 0, oSigConfig.ResizeFactor, oSigConfig.IncreaseContrast, oSigConfig.ContrastTolerance, oSigConfig.MakeSignatureTransparent, oSigConfig.SignatureBackgroundColor, oSigConfig.SaveImageFile, 50);
                    bRet = oVectorSig.TranslateSignature(bData, bSigType);
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                if (oLogging != null)
                    oLogging.Error("Error decoding signature", ex);
            }
            return bRet;
        }
        #endregion
        #region Packet Wrapping Functions
        public byte[] CreateMCCAck(int iMobileID, byte bSeqNum)
        {
            byte[] bSendPacket = null;
            ushort iTemp = 0;

            byte[] bConvert = null;
            MemoryStream oMS = null;
            int iCheckSum = 0;
            try
            {
                oMS = new MemoryStream(14);
                PacketUtilities.WriteToStream(oMS, (byte)0x01);						// SOT
                PacketUtilities.WriteToStream(oMS, (byte)0xAC);						// Packet Type
                iTemp = Convert.ToUInt16(iMobileID);
                bConvert = BitConverter.GetBytes(iTemp);
                PacketUtilities.WriteToStream(oMS, bConvert[1]);						// MobileID
                PacketUtilities.WriteToStream(oMS, bConvert[0]);
                PacketUtilities.WriteToStream(oMS, bSeqNum);							// Seq Num
                PacketUtilities.WriteToStream(oMS, (byte)0x01);						    // Length
                PacketUtilities.WriteToStream(oMS, (byte)0x00);
                PacketUtilities.WriteToStream(oMS, (byte)0x01);						    // Packet Num
                PacketUtilities.WriteToStream(oMS, (byte)0x01);						    // Total Packets
                PacketUtilities.WriteToStream(oMS, (byte)0xAA);					        // EOH
                PacketUtilities.WriteToStream(oMS, (byte)0x00);                         // Data
                bSendPacket = oMS.GetBuffer();
                // Calculate the Check sum
                for (int X = 0; X < bSendPacket.Length; X++)
                {
                    iCheckSum += (int)bSendPacket[X];
                    iCheckSum = iCheckSum & 0xFFFF;
                }
                bConvert = BitConverter.GetBytes(iCheckSum);
                bSendPacket[bSendPacket.Length - 3] = bConvert[1];
                bSendPacket[bSendPacket.Length - 2] = bConvert[0];
                bSendPacket[bSendPacket.Length - 1] = (byte)0x04;
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "Wrap302xLayer(byte[] bData, int iMobileID, byte bSeqNum)", ex);
                bSendPacket = null;
            }
            return bSendPacket;
        }
        // This function will break teh data into [iMaxSize] chuncks and return an array of byte[]'s.  Each entry is a packet to send to MCC.
        public object[] Wrap302xLayer(byte[] bData, int iMobileID, byte bSeqNum)
        {

            MemoryStream oMS = null;
            object[] oRet = null;
            byte[] bConvert = null;
            byte[] bSendPacket = null;
            byte[] bDataSegment = null;
            byte[] bResult = null;
            ushort iTemp = 0;
            ulong iCheckSum = 0;
            int iStartSeg = 0;
            int iEndSeg = 0;
            byte bPacketNum = (byte)0x00;
            byte bPacketTotal = (byte)0x00;
            int iPacket = 0;
            int iPos = 0;
            int iCounter = 0;

            try
            {
                // Packet format = [SOP][Message Type][Unit ID][Sequence Number][Payload Length][Packet Number][Packets Total][Spare][Payload][Crc][EOT]
                // [SOP] = 0x01
                // [Message Type] - 1 byte - 0xAC = Ack, 0xFF = Nak, D = Data, X = Sign off, K = Keep Alive
                // [Unit ID] - 2 bytes
                // [Sequence Number] - 1 Byte
                // [Payload Length] - 2 Bytes
                // [Packet Number] - 1 Byte
                // [Packets Total] - 1 Byte
                // [Spare] - 1 Byte
                // [Payload] - X Bytes
                // [Crc] - 2 bytes
                // [EOT] = 0x04

                //bConvert = WrapForMCC(bData, iMobileID, 0);

                if (bData.Length < iMaxPacketSize)
                {
                    #region Pack all the data in a single packet
                    oRet = new object[1];
                    oMS = new MemoryStream(bData.Length + 13);
                    PacketUtilities.WriteToStream(oMS, (byte)0x01);						// SOT
                    PacketUtilities.WriteToStream(oMS, 'D');									// Packet Type
                    iTemp = Convert.ToUInt16(iMobileID);
                    bConvert = BitConverter.GetBytes(iTemp);
                    PacketUtilities.WriteToStream(oMS, bConvert[1]);						// MobileID
                    PacketUtilities.WriteToStream(oMS, bConvert[0]);
                    PacketUtilities.WriteToStream(oMS, bSeqNum);							// Seq Num
                    iTemp = Convert.ToUInt16(bData.Length);
                    bConvert = BitConverter.GetBytes(iTemp);
                    PacketUtilities.WriteToStream(oMS, bConvert[1]);						// Length
                    PacketUtilities.WriteToStream(oMS, bConvert[0]);
                    PacketUtilities.WriteToStream(oMS, (byte)0x01);						// Packet Num
                    PacketUtilities.WriteToStream(oMS, (byte)0x01);						// Total Packets
                    PacketUtilities.WriteToStream(oMS, (byte)0xAA);					// EOH
                    if (bData != null)
                        PacketUtilities.WriteToStream(oMS, bData);						// Data
                    else
                        PacketUtilities.WriteToStream(oMS, (byte)0x00);

                    bSendPacket = oMS.GetBuffer();
                    bResult = new byte[oMS.Position + 3];
                    oMS.Dispose();
                    oMS = null;
                    // Calculate the Check sum
                    for (int X = 0; X < bResult.Length; X++)
                    {
                        iCheckSum += (ulong)bSendPacket[X];
                        bResult[X] = bSendPacket[X];
                    }
                    bConvert = BitConverter.GetBytes(iCheckSum);
                    bResult[bResult.Length - 3] = bConvert[1];
                    bResult[bResult.Length - 2] = bConvert[0];
                    bResult[bResult.Length - 1] = (byte)0x04;
                    oRet[0] = bResult;
                    #endregion
                }
                else
                {
                    #region Pack all the data in a multiple packets
                    #region Setup the packet counts and length variables
                    iStartSeg = 0;
                    iEndSeg = iMaxPacketSize;
                    bPacketNum = (byte)0x00;
                    // Calculate the total number of packets
                    bPacketTotal = (byte)Convert.ToInt32(Math.Round(Convert.ToDecimal(bData.Length) / Convert.ToDecimal(iMaxPacketSize), 0, MidpointRounding.AwayFromZero));
                    oRet = new object[(int)bPacketTotal];
                    #endregion
                    while (iStartSeg < bData.Length)
                    {
                        #region Get the first chunk of data of [iMaxPacketSize] bytes
                        if (bData.Length - iStartSeg > iMaxPacketSize)
                            bDataSegment = new byte[iMaxPacketSize];
                        else
                            bDataSegment = new byte[bData.Length - iStartSeg];
                        iPos = 0;
                        iCounter = 0;
                        for (iCounter = iStartSeg; iCounter < iStartSeg + bDataSegment.Length; iCounter++)
                        {
                            bDataSegment[iPos++] = bData[iCounter];
                        }
                        iStartSeg = iCounter;
                        bPacketNum = (byte)((int)bPacketNum + 1);
                        #endregion
                        #region Wrap this data segment for MCC
                        oMS = new MemoryStream(bData.Length + 13);
                        PacketUtilities.WriteToStream(oMS, (byte)0x01);						// SOT
                        PacketUtilities.WriteToStream(oMS, 'D');									// Packet Type
                        iTemp = Convert.ToUInt16(iMobileID);
                        bConvert = BitConverter.GetBytes(iTemp);
                        PacketUtilities.WriteToStream(oMS, bConvert[1]);						// MobileID
                        PacketUtilities.WriteToStream(oMS, bConvert[0]);
                        PacketUtilities.WriteToStream(oMS, bSeqNum);							// Seq Num
                        iTemp = Convert.ToUInt16(bDataSegment.Length);
                        bConvert = BitConverter.GetBytes(iTemp);
                        PacketUtilities.WriteToStream(oMS, bConvert[1]);						// Length
                        PacketUtilities.WriteToStream(oMS, bConvert[0]);
                        PacketUtilities.WriteToStream(oMS, bPacketNum);						// Packet Num
                        PacketUtilities.WriteToStream(oMS, bPacketTotal);						// Total Packets
                        PacketUtilities.WriteToStream(oMS, (byte)0xAA);					// EOH
                        if (bDataSegment != null)
                            PacketUtilities.WriteToStream(oMS, bDataSegment);						// Data
                        else
                            PacketUtilities.WriteToStream(oMS, (byte)0x00);
                        bSendPacket = oMS.GetBuffer();
                        bResult = new byte[oMS.Position + 3];
                        oMS.Dispose();
                        oMS = null;
                        // Calculate the Check sum
                        for (int X = 0; X < bResult.Length; X++)
                        {
                            iCheckSum += (ulong)bSendPacket[X];
                            bResult[X] = bSendPacket[X];
                        }
                        bConvert = BitConverter.GetBytes(iCheckSum);
                        //bResult[bResult.Length - 3] = bConvert[1];
                        //bResult[bResult.Length - 2] = bConvert[0];
                        bResult[bResult.Length - 3] = (byte)0x00;
                        bResult[bResult.Length - 2] = (byte)0x00;
                        bResult[bResult.Length - 1] = (byte)0x04;
                        #endregion
                        oRet[iPacket++] = bResult;
                    }
                    #endregion
                }

            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "Wrap302xLayer(byte[] bData, int iMobileID, byte bSeqNum)", ex);
                oRet = null;
            }
            return oRet;
        }
        public byte[] UnWrap302xLayer(byte[] bData, ref int iMobileID, ref byte bSeqNum)
        {
            MemoryStream oMS = null;
            byte bTemp = (byte)0x00;
            ushort usTemp = 0;
            int iLength = 0;
            byte[] bPayload = null;
            byte[] bLength = null;
            byte[] bConvert = null;

            try
            {
                // Packet format = [SOP][Message Type][Unit ID][Sequence Number][Payload Length][Packet Number][Packets Total][Spare][Payload][Crc][EOT]
                // [SOP] = 0x01
                // [Message Type] - 1 byte - 0xAC = Ack, 0xFF = Nak, D = Data, X = Sign off, K = Keep Alive
                // [Unit ID] - 2 bytes
                // [Sequence Number] - 1 Byte
                // [Payload Length] - 2 Bytes
                // [Packet Number] - 1 Byte
                // [Packets Total] - 1 Byte
                // [Spare] - 1 Byte
                // [Payload] - X Bytes
                // [Crc] - 2 bytes
                // [EOT] = 0x04

                oMS = new MemoryStream(bData);
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // SOP
                if (bTemp != (byte)0x01)
                    throw new System.Exception("SOP was not found in the correct location.");
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // Message Type
                PacketUtilities.ReadFromStream(oMS, ref usTemp);                                                    // Mobile ID
                iMobileID = (int)usTemp;
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // Sequence Number
                bSeqNum = bTemp;
                bLength = new byte[2];
                PacketUtilities.ReadFromStream(oMS, (int)2, ref bLength);                                          // Payload Length
                bConvert = new byte[4];
                bConvert[0] = bLength[1];
                bConvert[1] = bLength[0];
                iLength = BitConverter.ToInt32(bConvert, 0);
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // Packet Number
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // Packets Total
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // Spare
                PacketUtilities.ReadFromStream(oMS, iLength, ref bPayload);                                         // Payload
                PacketUtilities.ReadFromStream(oMS, ref usTemp);                                                    // CRC
                PacketUtilities.ReadFromStream(oMS, ref bTemp);                                                     // EOP
                if (bTemp != (byte)0x04)
                    throw new System.Exception("EOP was not found in the correct location.");
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "Wrap302xLayer(byte[] bData, int iMobileID, byte bSeqNum)", ex);
                bPayload = null;
            }
            return bPayload;
        }
        public byte[] WrapForMCC(byte[] bData, int iMobileID, int iJobID)
        {
            MemoryStream oMS = null;
            byte[] bConvert = null;
            byte[] bSendPacket = null;

            try
            {
                // Packet format = [SOP][Length][Unit ID][Sep][Job ID][Sep][Data][EOP]
                // [SOP] = 0x0A
                // [Length] = 4 Bytes
                // [Unit ID] = 4 Bytes
                // [Sep] = 0x0B
                // [Job ID] = 4 Bytes
                // [Sep] = 0x0B
                // [Data] = Var length data
                // [EOP] = 0x0C
                // Length includes [Unit ID][Sep][Job ID][Sep][Data]

                oMS = new MemoryStream(bData.Length + 16);
                bConvert = new byte[4];
                PacketUtilities.WriteToStream(oMS, (byte)0x0A);						// SOP
                PacketUtilities.WriteToStream(oMS, bConvert);							// Length
                PacketUtilities.WriteToStream(oMS, iMobileID);						// Mobile ID
                PacketUtilities.WriteToStream(oMS, (byte)0x0B);						// Sep
                PacketUtilities.WriteToStream(oMS, iJobID);								// Job ID
                PacketUtilities.WriteToStream(oMS, (byte)0x0B);						// Sep
                if (bData != null)
                    PacketUtilities.WriteToStream(oMS, bData);								// Data
                else
                    PacketUtilities.WriteToStream(oMS, (byte)0x00);
                PacketUtilities.WriteToStream(oMS, (byte)0x0C);						// EOP
                // Get the bytes
                bSendPacket = oMS.GetBuffer();
                // Populate the Length field
                bConvert = BitConverter.GetBytes(bSendPacket.Length - 6);
                bSendPacket[1] = bConvert[0];
                bSendPacket[2] = bConvert[1];
                bSendPacket[3] = bConvert[2];
                bSendPacket[4] = bConvert[3];
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "WrapForMCC(byte[] bData, int iMobileID, int iJobID)", ex);
                bSendPacket = null;
            }
            return bSendPacket;
        }
        public byte[] UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID)
        {
            MemoryStream oMS = null;
            int iLength = 0;
            byte[] bRet = null;
            byte bTemp = (byte)0x00;
            string sRet = "";
            try
            {
                // Packet format = [SOP][Length][Unit ID][Sep][Job ID][Sep][Data][EOP]
                // [SOP] = 0x0A
                // [Length] = 4 Bytes
                // [Unit ID] = 4 Bytes
                // [Sep] = 0x0B
                // [Job ID] = 4 Bytes
                // [Sep] = 0x0B
                // [Data] = Var length data
                // [EOP] = 0x0C
                // Length includes [Unit ID][Sep][Job ID][Sep][Data]

                oMS = new MemoryStream(bData);

                PacketUtilities.ReadFromStream(oMS, ref bTemp);											// SOP
                if (bTemp != (byte)0x01)
                {
                    sRet = "Failed to find SOP";
                    if (oLogging != null) oLogging.Info(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID) - " + sRet);
                    return null;
                }
                PacketUtilities.ReadFromStream(oMS, ref iLength);										// Length
                if (iLength != bData.Length - 6)
                {
                    sRet = "Packet is not the correct length";
                    if (oLogging != null) oLogging.Info(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID) - " + sRet);
                    return null;
                }
                if (bData[iLength + 5] != (byte)0x0C)
                {
                    sRet = "EOP was not found in the correct place";
                    if (oLogging != null) oLogging.Info(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID) - " + sRet);
                    return null;
                }
                PacketUtilities.ReadFromStream(oMS, ref MobileID);									// Mobile ID
                PacketUtilities.ReadFromStream(oMS, ref bTemp);											// Sep
                if (bTemp != (byte)0x0B)
                {
                    sRet = "First seperator char was not found in the correct place";
                    if (oLogging != null) oLogging.Info(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID) - " + sRet);
                    return null;
                }
                PacketUtilities.ReadFromStream(oMS, ref JobID);											// Job ID
                PacketUtilities.ReadFromStream(oMS, ref bTemp);											// Sep
                if (bTemp != (byte)0x0B)
                {
                    sRet = "Second seperator char was not found in the correct place";
                    if (oLogging != null) oLogging.Info(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID) - " + sRet);
                    return null;
                }
                PacketUtilities.ReadFromStream(oMS, iLength - 10, ref bRet);						// Data
                PacketUtilities.ReadFromStream(oMS, ref bTemp);											// EOP
                if (bTemp != (byte)0x0C)
                {
                    sRet = "EOP was not found in the correct place";
                    if (oLogging != null) oLogging.Info(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID) - " + sRet);
                    return null;
                }
            }
            catch (System.Exception ex)
            {
                if (oLogging != null) oLogging.Error(sClassName + "UnWrapFromMCC(byte[] bData, ref int MobileID, ref int JobID)", ex);
                bRet = null;
            }
            return bRet;
        }
        #endregion
    }
}

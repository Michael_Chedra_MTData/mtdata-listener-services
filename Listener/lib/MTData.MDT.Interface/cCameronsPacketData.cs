using System;
using System.Collections;
using System.Collections.Specialized;
using log4net;
using MTData.Common.Config;
using MTData.Common.Network;
using MTData.Common.Utilities;
using MTData.MotFileReader;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for cCameronsPacketData.
    /// </summary>

    public class cCameronsPacketData
    {
        #region Incomming Packet Defs
        // N - Ack an NAck Response - [CmdType][Mobile ID][Result Code][Message][Job ID][Stop #][SBS][Spare]
        // I - Logon to System - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][Password][PhoneBookID][MessagesID][DivisionReasonID][DelayReasonID][RejectedReasonID][Distance] [SBS][Spare]
        // O - Log off the System - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][Distance][SBS][Spare]
        // S - Stop Packet -
        //		P - Complete a Stop (pickup) - 	[CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Distance][ReferenceNo][TimeLoading][Division][DelayTime][DelayReason][DelayDescription]{[PalletType][PNS][PES]}[SBS][Spare]
        //		D - Complete a Stop (deliver) - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Distance][ReferenceNo][TimeLoading][DelayTime][DelayReason][DelayDescription][POD Name][PNS][RejectCode][SOS][SIG LEN][SIG DATA][SBS][Spare]
        // R - Request Packets
        //		J - Request Jobs - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]
        // F - Lunch Break on off Packet
        //		S - Start Lunch - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]
        //		F - Finished Lunch - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]
        // Y - Odometer Reading Update Packet - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][ODO][SBS][Spare]
        // M - Messages
        //		F - Driver initiated message (Freeform). - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd[Message Data][MSDC][SBS][Spare]
        //		P - Driver initiated message (Predefined). - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Message ID][SBS][Spare]
        //		f - Driver initiated Email message (Freeform). - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Email][EMS][MessageData][MSDC][SBS][Spare]
        //		p - Driver initiated Email message (Predefined). - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Email][EMS][Message ID][SBS][Spare]
        // K - Phone Call Report
        //		I - Incomming Calls - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Number][PSP][Time][SBS][Spare]
        //		O - Outgoing Calls - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Number][PSP][Time][SBS][Spare]
        // d - Download on demand
        //		r - request new parcel - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][ParcelNo]
        //		v - request verify of download - [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][ParcelNo]
        #endregion

        #region Outgoing Packet Defs
        // N - Ack -								[CmdType][Mobile ID][Result Code][Message][Job ID][Stop #][SBS][Spare]
        // J - New Job -							[CmdType][Mobile ID][Job ID][Num of Stops][Description][DSC][Contact Name] [EOL] [Contact Phone Nr] [EOL][Stop 1 Description][EOL] [SBS][Spare]
        // A - Add a Job Stop -						[CmdType][Mobile ID][Job ID][Stop #][Description][EOL][SBS][Spare]
        // U - Update Job Stop -					[CmdType][Mobile ID][Job ID][Stop #][Description][EOL] [SBS][Spare]
        // R - Remove a Job Stop -					[CmdType][Mobile ID][Job ID][Stop #][SBS][Spare]
        // D - Delete a Job -						[CmdType][Mobile ID][Job ID][SBS][Spare]
        // L - Logon Response -						[CmdType][Mobile ID][SubCmd][SBS][CurrentVersionMSB][CurrentVersionLSB][Spare]
        // K - Send Phonebook Entry -		
        //		P - [CmdType][Mobile ID][SubCmd][phonebookID][PSS][Location][Number][NOS][Name][SBS][Spare]
        //		D -									[CmdType][Mobile ID][SubCmd][phonebookID][PSS][Location][Number][NOS][Name][SBS][Spare]
        //		d - Send Divisions that are to be selected when picking up or delivering
        //											[CmdType][Mobile ID][SubCmd][DivisionListID][DSS][DivisionID][DivisionName][EDS][SBS][Spare]
        //		r - Send Delay Reasons Delivering
        //											[CmdType][Mobile ID][SubCmd][DelayReasonListID][DSS][DelayReasonID][DelayReasonName][EDS][SBS][Spare]
        //		j - Send Rejected Futile Reasons When Delivering
        //											[CmdType][Mobile ID][SubCmd][RejectedListID][DSS][RejectedReasonID][RejectedReasonName][EDS][SBS][Spare]
        // M - Send Predefined Messages to for unit�s memory -			
        //		D -									[CmdType][Mobile ID][SubCmd][MessageID][MSS][Location][MessageName][SBS][Spare]
        // d - Download on Demand support
        //		d - Download parcel	-				[CmdType][MobileID][SubCmd][CurrentVersionMSB][CurrentVersionLSB][TotalNoOfParcels][NoOfParcels]{[ParcelNo][Parcel][DSB]}[DESB]
        //		v - Verify download	-				[CmdType][MobileID][SubCmd][DownloadVersion][ProgramTotalCheckSum][PacketCheckSum][PacketCountTotal][ProgramByteTotal][Checksum]
        #endregion

        #region Private Vars
        private ConvertSignatureConfig _sigConfig = null;
        private ILog _log = LogManager.GetLogger(typeof(cCameronsPacketData));
        private bool bTranslateRJs = true;
        #endregion

        #region Public Vars
        // Shareed Public variables
        public int iMobileID = 0;
        public string CommandType = "";				// [CmdType]
        public int JobID = 0;								// [Job ID]
        public int StopID = 0;								// [Stop #]
        public int CodeNumber = 0;						// [FailCode], [SucessCode], [Function ID]
        public string Message = "";						// [Message Data]
        public byte bAllowFreeDial = (byte)0x00;        // [Allow Free Dial]
        // Incomming Packet Specific
        public string SubCommandType = "";		// [SubCmd]
        public bool StartFunction = false;			// [Start / Stop]
        public int ReferenceNumber = 0;
        public int TimeLoading = 0;
        public int DivisionID = 0;
        public int CallTime = 0;
        public string CallType = "";
        public int SuburbID = 0;							// [Suburb ID]
        public int MessageID = 0;							// [Message ID]
        public int VehicleID = 0;							// [VehicleID]
        public int DriverID = 0;                            // [DriverID]
        public int FleetID = 0;								// [FleetID]
        public int MessagebookID = 0;					// [MessageBook ID]
        public int DivisionReasonID = 0;				// [DivisionReasonID]
        public int DelayReasonID = 0;					// [DelayReasonID]
        public int RejectedReasonID = 0;			// [RejectedReasonID]
        public int Distance = 0;								// [Distance]
        public int Odometer = 0;
        public int DeviceTimeSeconds = 0;					// Time since last GPS lock
        public byte FirmwareRevMajor = 0;
        public byte FirmwareRevMinor = 0;
        public int HardwareRevMajor = 0;
        public int HardwareRevMinor = 0;

        public ushort MotFileRequestedParcelNo = 0;
        public byte SoftwareVersionMajor = 0;
        public byte SoftwareVersionMinor = 0;
        public ushort TotalNumberOfDataParcels = 0;
        public ushort NumberOfDataParcels = 0;
        public ushort DownloadMotParcelNumber = 0;
        public ArrayList DownloadMotParcel = null;
        public ushort[] DownloadMotParcelCheckSum = null;
        public byte DownloadVersion = 0;
        public ushort PacketNumber = 0;
        public ushort RequestedParcelNo;

        //download verfity variables
        public int ProgramTotalCheckSum = 0;
        public int PacketTotalCheckSum = 0;
        public int ProgramPacketCountTotal = 0;
        public int ProgramByteTotal = 0;
        public int ProgramDataBytesCheckSum = 0;

        public int BatteryLevel = 0;
        public int Options = 0;
        public double CurTemp = 0;
        public string Spare1 = "";
        public string Spare2 = "";
        public string Spare3 = "";
        public string Spare4 = "";
        public string Spare5 = "";
        public string FirmwareVariant = "";
        public string DelayDescription = "";
        public string NoOfPallets = "";
        public string EmailAddress = "";
        public string PhoneNumber = "";
        public string Username = "";						// [Username]
        public string Password = "";						// [Password]
        public string PODName = "";					// [POD Name]
        public string JobTime = "";						// [Time], [Time in Mins]
        public string BusinessName = "";				// [Business Name]
        public string InvoiceNumber = "";			// [Invoice Number]
        public StringCollection BarCodes = null;				// [Bar Code x]
        public StringCollection ReturnBarCodes = null;	// [Return Bar Code x]
        public StringCollection PalletTypeCodes = null;
        public StringCollection PalletNumbers = null;
        public StringCollection Trailers = null;
        public StringCollection TrailerOdometers = null;
        public byte[] bGenericData = null;			// [200 bytes]
        public byte[] SignatureData = null;				// [Signature]



        // Outgoing Packet Specific
        public int TotalNumberOfStops = 0;
        public int PhoneBookID = 0;
        public int DivisionListID = 0;
        public int DelayReasonListID = 0;
        public int RejectionReasonListID = 0;
        public string JobTitle = "";
        public string ContactName = "";
        public string ContactPh = "";
        public string JobDescription = "";
        public string LoginReply = "";
        public StringCollection PhoneBookLocations = new StringCollection();
        public StringCollection PhoneBookNumbers = new StringCollection();
        public StringCollection PhoneBookNames = new StringCollection();
        public StringCollection MessageNames = new StringCollection();
        public StringCollection DivisionIDs = new StringCollection();
        public StringCollection DivisionNames = new StringCollection();
        public StringCollection DelayReasonNames = new StringCollection();
        public StringCollection DelayReasonIDs = new StringCollection();
        public StringCollection RejectionReasonNames = new StringCollection();
        public StringCollection RejectionReasonIDs = new StringCollection();
        public int iServerSideFirmwareDownloadVer_Major = 0;
        public int iServerSideFirmwareDownloadVer_Minor = 0;

        // GPS Data
        public cGPSData GPSPosition = null;
        #endregion

        public cCameronsPacketData(ConvertSignatureConfig sigConfig, bool TranslateRJs)
        {
            _sigConfig = sigConfig;
            bTranslateRJs = TranslateRJs;

        }


        #region Build Packet Data
        public byte[] BuildOutgoingPacket()
        {
            byte[] bRet = null;
            byte[] bConvert = null;
            int iEntries = 0;


            bRet = System.Text.ASCIIEncoding.ASCII.GetBytes(CommandType);									// Cmd Type

            bConvert = new byte[4];																								// Mobile ID
            bConvert = BitConverter.GetBytes(Convert.ToUInt32(iMobileID));
            bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
            bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);

            switch (CommandType)
            {
                // At this point the packet consists of [CmdType][Mobile ID]
                case "d":
                    #region Download Packets

                    bRet = PacketUtilities.AppendToPacket(bRet, this.SubCommandType);					// SubCmd
                    if (SubCommandType == "d")
                    {
                        //[CmdType]			= 'd'
                        //[MobileID]		
                        //[SubCmd]			= 'd'
                        //[CurrentVersionMSB]
                        //[CurrentVersionLSB]
                        //[TotalNoOfParcels]
                        //[NoOfParcels]
                        //{
                        //	[ParcelNo]
                        //	[ProgramFragment]
                        //	[DownloadcheckSum]
                        //	[DSB]
                        //}
                        //[DESB]
                        #region Download Parcel
                        bRet = PacketUtilities.AppendToPacket(bRet, this.SoftwareVersionMajor);								// [Software Ver Major (1 Byte)
                        bRet = PacketUtilities.AppendToPacket(bRet, this.SoftwareVersionMinor);								// [Software Ver Minor (1 Byte)						
                        bRet = PacketUtilities.AppendToPacket(bRet, (ushort)DownloadMotParcel.Count);						// [NumberOfDataSegments (2 Bytes)
                        ushort iMaxDownloadMotParcelNumber = (ushort)(DownloadMotParcel.Count - DownloadMotParcelNumber);
                        if (iMaxDownloadMotParcelNumber > 2)
                            iMaxDownloadMotParcelNumber = 2;
                        bRet = PacketUtilities.AppendToPacket(bRet, (byte)iMaxDownloadMotParcelNumber);				// [NoOfParcels] (1 Bytes)]
                        for (ushort i = DownloadMotParcelNumber; i < iMaxDownloadMotParcelNumber + DownloadMotParcelNumber; i++)
                        {
                            cParcel oParcel = (cParcel)DownloadMotParcel[i];
                            bRet = PacketUtilities.AppendToPacket(bRet, (ushort)i);															// [PacketNumber] (2 Byte)]
                            bRet = PacketUtilities.AppendToPacket(bRet, oParcel.Data);														// [ProgramFragment] = 250 bytes
                            bRet = PacketUtilities.AppendToPacket(bRet, (ushort)oParcel.CheckSum);								// [DownloadcheckSum] = 2 bytes
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0xBB);														// [DSB] = 1 byte
                        }
                        bRet = PacketUtilities.AppendToPacket(bRet, (byte)0xCC);															// [DESB]
                        #endregion
                    }
                    else if (SubCommandType == "v")
                    {
                        //[CmdType]
                        //[MobileID]
                        //[SubCmd]
                        //[DownloadVersion]
                        //[ProgramTotalCheckSum]
                        //[PacketCheckSum]
                        //[PacketCountTotal]
                        //[ProgramByteTotal]
                        //[Checksum]
                        #region Download Verify
                        bRet = PacketUtilities.AppendToPacket(bRet, (byte)this.DownloadVersion);
                        bRet = PacketUtilities.AppendToPacket(bRet, (int)ProgramTotalCheckSum);
                        bRet = PacketUtilities.AppendToPacket(bRet, (int)PacketTotalCheckSum);
                        bRet = PacketUtilities.AppendToPacket(bRet, (ushort)ProgramPacketCountTotal);
                        bRet = PacketUtilities.AppendToPacket(bRet, (int)ProgramByteTotal);
                        bRet = PacketUtilities.AppendToPacket(bRet, (byte)ProgramDataBytesCheckSum);
                        #endregion
                    }
                    #endregion
                    break;
                case "N":
                    #region Ack Packet
                    // [CmdType][Mobile ID][Result Code][Message][Job ID][Stop #][SBS][Spare]
                    bConvert = new byte[4];																								// Result Code
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.CodeNumber));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                    if (this.CodeNumber == 0)
                    {
                        bRet = PacketUtilities.AppendToPacket(bRet, "0");															// Message
                        bRet = PacketUtilities.AppendToPacket(bRet, "OK");
                    }
                    else
                    {
                        bRet = PacketUtilities.AppendToPacket(bRet, Convert.ToString(this.CodeNumber));	// Message
                        bRet = PacketUtilities.AppendToPacket(bRet, "FAIL");
                    }

                    bConvert = new byte[4];																								// Job ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.JobID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bConvert = new byte[4];																								// Stop ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.StopID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                    #endregion
                    break;
                case "L":
                    #region Login Reply packet
                    //					[CmdType][Mobile ID][SubCmd][SBS][CurrentVersionMSB][CurrentVersionLSB][Spare]

                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(this.TotalNumberOfStops);

                    // Overwrite the 2 byte mobile id with a 4 byte mobile id.
                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("L");									// Cmd Type
                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.iMobileID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[1]);									// Mobile ID
                    bRet = PacketUtilities.AppendToPacket(bRet, LoginReply);										// Login Responce
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(0x07));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// SBS
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(iServerSideFirmwareDownloadVer_Major));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);				//server firware current software version
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(iServerSideFirmwareDownloadVer_Minor));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);				//server firware current software version
                    bRet = PacketUtilities.AppendToPacket(bRet, bAllowFreeDial);		    //Allow Free Dial

                    #endregion
                    break;
                case "J":
                    #region New Job Packet:
                    // [CmdType][Mobile ID][Job ID][Num of Stops][Job Title][DSC][Contact Name] [EOL] [Contact Phone Nr] [EOL][Stop 1 Description][EOL] [SBS][Spare]

                    bConvert = new byte[4];																								// Job ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.JobID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bConvert = new byte[4];																								// Num of Stops
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.TotalNumberOfStops));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobTitle);														// Job Title
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0E);										// DSC - Description Seperator Char

                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.ContactName);												// Contact Name
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);											// EOL - End of LIne

                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.ContactPh);													// Contact Phone Nr
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);											// EOL - End of LIne

                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription);											// Stop 1 Description
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);											// EOL - End of LIne
                    #endregion
                    break;
                case "A":
                    #region Add a job stop
                    //[CmdType][Mobile ID][Job ID][Stop #][Description][EOL][SBS][Spare]
                    bConvert = new byte[4];																								// Job ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.JobID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bConvert = new byte[4];																								// Stops ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.StopID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription);											// Stop Description
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);											// EOL - End of LIne
                    #endregion
                    break;
                case "U":
                    #region Update a job stop
                    // [CmdType][Mobile ID][Job ID][Stop #][Description][EOL] [SBS][Spare]
                    bConvert = new byte[4];																								// Job ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.JobID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bConvert = new byte[4];																								// Stops ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.StopID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.JobDescription);											// Stop Description
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);											// EOL - End of LIne
                    #endregion
                    break;
                case "R":
                    #region Remove a job stop
                    // [CmdType][Mobile ID][Job ID][Stop #][SBS][Spare]
                    bConvert = new byte[4];																								// Job ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.JobID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);

                    bConvert = new byte[4];																								// Stops ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.StopID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    #endregion
                    break;
                case "D":
                    #region Delete a job
                    // [CmdType][Mobile ID][Job ID][SBS][Spare]
                    bConvert = new byte[4];																								// Job ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.JobID));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    #endregion
                    break;
                case "M":
                    #region Send a driver message
                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.Message);														// Message
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert);
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0F);

                    /*
                    // after 34 chars we have to have EOL separator
                    bConvert = System.Text.ASCIIEncoding.ASCII.GetBytes(this.Message);				// Message
                    byte[] bConvertMsg = new byte[this.Message.Length + Convert.ToInt32(System.Math.Ceiling(this.Message.Length / 34))];
                    int iCount = 0;
                    int iCountMsg = 0;
                    while (iCount < bConvert.Length)
                    {
                        if (iCount % 34 == 0 && iCount > 0)
                        {
                            // add EOL
                            bConvertMsg[iCountMsg] = (byte) 0x0F;
                            iCountMsg+=1;
                        }
                        bConvertMsg[iCountMsg] = bConvert[iCount];
                        iCount+=1;
                        iCountMsg+=1;
                    }

                    bRet = PacketUtilities.AppendToPacket(bRet, bConvertMsg);

                    // EOL
                    bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0F);		// EOL - End of LIne
                    */

                    #endregion
                    break;
                case "P":
                    #region Send a pre-defined function messagfe to the 5040
                    bConvert = new byte[4];																								// Function ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.CodeNumber));
                    bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                    if (this.StartFunction)  // Start or stop function
                    {
                        bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x01);
                    }
                    else
                    {
                        bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x00);
                    }
                    #endregion
                    break;
                case "K":
                    #region Send a phone book Entry
                    bRet = PacketUtilities.AppendToPacket(bRet, this.SubCommandType);					// SubCmd

                    if (this.SubCommandType == "P")
                    {
                        #region Global Phonebook
                        // [CmdType][Mobile ID][SubCmd][phonebookID][PSS][Location][Number][NOS][Name][SBS][Spare]

                        bConvert = new byte[4];																											// phonebookID
                        bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.PhoneBookID));
                        bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                        iEntries = 0;
                        if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > iEntries) iEntries = PhoneBookNumbers.Count;
                        if (PhoneBookNames != null) if (PhoneBookNames.Count > iEntries) iEntries = PhoneBookNames.Count;


                        if (iEntries > 0)
                        {
                            // according to specs, max number for global phb is 50
                            if (iEntries > 50) iEntries = 50;
                            for (int X = 0; X < iEntries; X++)
                            {
                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);											// PSS - Phone book separator

                                bConvert = new byte[4];																									// Location
                                bConvert = BitConverter.GetBytes(Convert.ToUInt32(X));
                                bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                                if (PhoneBookNumbers != null)
                                    bRet = PacketUtilities.AppendToPacket(bRet, PhoneBookNumbers[X]);					// Phone Number
                                else
                                    bRet = PacketUtilities.AppendToPacket(bRet, "0");

                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);											// PSS - Phone book separator

                                if (PhoneBookNames != null)
                                    bRet = PacketUtilities.AppendToPacket(bRet, PhoneBookNames[X]);						// Entry Name
                                else
                                    bRet = PacketUtilities.AppendToPacket(bRet, "N/A");
                            }
                        }
                        else
                        {
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);											// PSS - Phone book separator
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "D")
                    {
                        #region Personal Phonebook
                        // [CmdType][Mobile ID][SubCmd][PSS][Location][Number][NOS][Name][SBS][Spare]

                        //						bConvert = new byte[4];																								// phonebookID
                        //						bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.PhoneBookID));
                        //						bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                        iEntries = 0;
                        if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > iEntries) iEntries = PhoneBookNumbers.Count;
                        if (PhoneBookNames != null) if (PhoneBookNames.Count > iEntries) iEntries = PhoneBookNames.Count;

                        if (iEntries > 0)
                        {
                            // according to specs, max number for personal phb is 5
                            if (iEntries > 5) iEntries = 5;
                            for (int X = 0; X < iEntries; X++)
                            {
                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);											// PSS - Phone book separator

                                bConvert = new byte[4];																								// Location
                                bConvert = BitConverter.GetBytes(Convert.ToUInt32(X + 50));
                                bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                                if (PhoneBookNumbers != null)
                                    bRet = PacketUtilities.AppendToPacket(bRet, PhoneBookNumbers[X]);					// Phone Number
                                else
                                    bRet = PacketUtilities.AppendToPacket(bRet, "0");

                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// PSS - Phone book separator

                                if (PhoneBookNames != null)
                                    bRet = PacketUtilities.AppendToPacket(bRet, PhoneBookNames[X]);						// Entry Name
                                else
                                    bRet = PacketUtilities.AppendToPacket(bRet, "N/A");						// Entry Name

                            }
                        }
                        else
                        {
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);											// PSS - Phone book separator
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "d")
                    {
                        #region Send Divisions that are to be selected when picking up or delivering
                        //[CmdType][Mobile ID][SubCmd][DivisionListID][DSS][DivisionID][DivisionName][EDS][SBS][Spare]
                        bConvert = new byte[4];																									// DivisionListID
                        bConvert = BitConverter.GetBytes(Convert.ToUInt32(DivisionListID));
                        bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                        iEntries = DivisionNames.Count;
                        if (iEntries > 0)
                        {
                            // according to specs, max number of divisions is 15
                            if (iEntries > 15) iEntries = 15;
                            for (int X = 0; X < iEntries; X++)
                            {
                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);										// DSS - Disvision Section Start

                                bConvert = new byte[4];																								// Division ID
                                bConvert = BitConverter.GetBytes(Convert.ToUInt32(DivisionIDs[X]));
                                bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                                bRet = PacketUtilities.AppendToPacket(bRet, DivisionNames[X]);								// Division Name

                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// EDS - End Disvision Section

                            }
                        }
                        else
                        {
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);										// DSS - Disvision Section Start
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// EDS - End Disvision Section
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "j")
                    {
                        #region Send Rejected Futile Reasons When Delivering
                        //[CmdType][Mobile ID][SubCmd][RejectedListID][RSS][RejectedReasonID][RejectedReasonName][ERS][SBS][Spare]
                        bConvert = new byte[4];																								// RejectedListID
                        bConvert = BitConverter.GetBytes(Convert.ToUInt32(RejectionReasonListID));
                        bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                        iEntries = RejectionReasonNames.Count;
                        if (iEntries > 0)
                        {
                            // according to specs, max number of rejected reasons is 15
                            if (iEntries > 15) iEntries = 15;
                            for (int X = 0; X < iEntries; X++)
                            {
                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);										// RSS - Reason Section Start

                                bConvert = new byte[4];																								// RejectedReasonID
                                bConvert = BitConverter.GetBytes(Convert.ToUInt32(RejectionReasonIDs[X]));
                                bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                                bRet = PacketUtilities.AppendToPacket(bRet, RejectionReasonNames[X]);				// RejectedReasonName

                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// ERS - End Reason Section

                            }
                        }
                        else
                        {
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);										// RSS - Reason Section Start
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// ERS - End Reason Section
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "r")
                    {
                        #region Send Delay Reasons Delivering
                        //[CmdType][Mobile ID][SubCmd][DelayReasonListID][RSS][DelayReasonID][DelayReasonName][ERS][SBS][Spare]
                        bConvert = new byte[4];																								// DelayReasonListID
                        bConvert = BitConverter.GetBytes(Convert.ToUInt32(DelayReasonListID));
                        bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                        iEntries = DelayReasonNames.Count;
                        if (iEntries > 0)
                        {
                            // according to specs a maximum of 15 delayreasons can be sent
                            if (iEntries > 15) iEntries = 15;
                            for (int X = 0; X < iEntries; X++)
                            {
                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);										// RSS - Reason Section Start

                                bConvert = new byte[4];																								// DelayReasonID
                                bConvert = BitConverter.GetBytes(Convert.ToUInt32(DelayReasonIDs[X]));
                                bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                                bRet = PacketUtilities.AppendToPacket(bRet, DelayReasonNames[X]);						// DelayReasonName

                                bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// ERS - End Reason Section

                            }
                        }
                        else
                        {
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0A);										// RSS - Reason Section Start
                            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x0B);										// ERS - End Reason Section
                        }
                        #endregion
                    }
                    else
                        bRet = null;
                    #endregion
                    break;
                case "m":
                    #region Send Predefined Messages to for unit�s memory (NOT IMPLEMENTED)
                    //					// [CmdType][Mobile ID][SubCmd][MessageID][MSS][Location][MessageName][SBS][Spare]
                    //
                    //					bRet = PacketUtilities.AppendToPacket(bRet, this.SubCommandType);					// SubCmd
                    //
                    //					if (this.SubCommandType == "D")
                    //					{
                    //						bRet = PacketUtilities.AppendToPacket(bRet, this.MessageID);							// Message ID
                    //
                    //						iEntries = MessageNames.Count;
                    //
                    //						if (iEntries > 0)
                    //						{
                    //							for (int X = 0; X < iEntries; X++)
                    //							{
                    //								bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);											// MSS - Message separator start
                    //
                    //								bConvert = new byte[4];																								// Location
                    //								bConvert = BitConverter.GetBytes(Convert.ToUInt32(X));
                    //								bRet = PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    //						
                    //								bRet = PacketUtilities.AppendToPacket(bRet, MessageNames[X]);							// MessageName
                    //							}
                    //						}
                    //						else
                    //						{
                    //							bRet = PacketUtilities.AppendToPacket(bRet, (byte) 0x0A);											// PSS - Phone book separator
                    //						}
                    //					
                    //					}
                    //					else
                    //						bRet = null;
                    //	
                    #endregion
                    break;
                default:
                    bRet = null;
                    break;
            }

            bRet = PacketUtilities.AppendToPacket(bRet, (byte)0x07); // SBS - Spare Bytes Seperator

            string sData = "Outgoing Data -> " + BitConverter.ToString(bRet, 0);
            if (_log != null) _log.Info(sData);

            return bRet;
        }
        #endregion

        #region Parse Incomming Packet
        public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
        {
            string sRet = "";
            try
            {
                sRet = TranslateMsgToHost(bMsg, sUnitID);
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                {
                    if (bMsg == null)
                    {
                        _log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = null, sUnitID = '" + sUnitID + "')", ex);
                        sRet = "Incomming packet was null";
                    }
                    else
                    {
                        _log.Error("MTData.MDTInterface.cCameronsPacketData ParseIncommingPacket(byte[] bMsg = " + BitConverter.ToString(bMsg) + ", sUnitID = '" + sUnitID + "')", ex);
                        sRet = "Packet Parse Error : " + ex.Message;
                    }
                }
            }
            return sRet;
        }

        private string TranslateMsgToHost(byte[] bMsg, string sUnitID)
        {
            // NOTES :
            //
            // All packets from the unit (except Nack) start with [Cmd Type][GPS]
            //
            // All msgs to the host start with bSOP (0x02), 2 bytes of length and end with bEOP (0x03);
            //		i.e. [SOP][Length of Data (2 Bytes)][Data][EOP]
            //
            // [GPS]
            // All packets from the unit to the host include the following GPS information;
            // [LSC][Lat][Long][GPS Time][Speed][Heading] = 1 + 4 + 4 + 6 + 1 + 2 = 18 bytes
            // 
            // These 18 bytes can just be copied to the host packet as a block, no interpretation is required.
            //
            // [SBS] - Spare bytes seperator
            // Most of the packets have a SBS before the EOP byte.  This has been added to allow for expansion
            // without changing the protocol.

            #region Local Variables
            byte[] bFailCode = new byte[4];
            byte[] bMBPPacket = new byte[1];
            byte[] bStopID = new byte[4];
            byte[] bConvert = new byte[4];
            byte[] bSignatureData = null;
            byte[] bGPS = null;
            byte[] bSignatureLen = new byte[4];
            byte[] bFunctionID = new byte[1];
            byte[] bStartStop = new byte[1];
            UInt32 iJobID = 0;
            int iPosition = 0;
            string strErrMsg = "";
            int z = 0;
            int y = 0;
            string sDD = ""; // delay description helper
            string sPN = ""; // pod name helper
            #endregion

            // keep packet data in bGenericData byte array
            bGenericData = new byte[bMsg.Length];
            bGenericData = bMsg;

            string sData = "Incomming Data -> " + BitConverter.ToString(bMsg, 0);

            #region Valisdate Packet Length and Structure
            bConvert = BitConverter.GetBytes(0);

            // Make sure the unit ID is only 4 chars long.
            if (sUnitID.Length > 4)
                sUnitID = sUnitID.Substring(0, 4);

            sUnitID = sUnitID.PadLeft(4, '0');

            // Get the Cmd Type for the packet
            this.CommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
            // Get the unit ID 
            if (Util.IsIntegerPositive(sUnitID))
                this.iMobileID = Convert.ToInt32(sUnitID);

            // debug only
            //			int xxx =0;
            //			if (this.CommandType == "K")
            //				xxx = 3;


            // check if old RJ packet - then ignore it
            if (this.CommandType == "R" && bMsg.Length < 25)
                return strErrMsg;

            // If the Cmd Type != N then extract the standard header fields [FleetID][VehicleID][GPS]
            if (this.CommandType != "N" && this.CommandType != "T") //&& this.CommandType != "R" 
            {
                if (bMsg.Length < 20)
                {
                    // If the packet is too small, just return.
                    return strErrMsg;
                }
                else
                {
                    // A login packet has extra fields in the header...
                    // [CmdType][MobileID][FleetID][VehicleID][GPS][.....]
                    //					CmdType =  1 char
                    //					Mobile ID = 2 bytes number
                    //					FleetID = 3021 Fleet ID (1 byte number 0 - 255)
                    //					VehicleID = 3021 Unit ID (2 byte number 0 - 65000)
                    //					GPS = See above.

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[1];
                    bConvert[1] = bMsg[2];
                    this.iMobileID = BitConverter.ToInt32(bConvert, 0);

                    // Get the fleet
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[3];
                    this.FleetID = BitConverter.ToInt32(bConvert, 0);

                    // Get the vehicle
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[4];
                    bConvert[1] = bMsg[5];
                    this.VehicleID = BitConverter.ToInt32(bConvert, 0);

                    // Get the GPS Data
                    bGPS = GetSubBytes(bMsg, 6, 18);
                    this.GPSPosition = new cGPSData(bGPS);

                    // Strip off the [CmdType][MobileID][FleetID][VehicleID][GPS] fields from bMsg
                    // Get the elapsed seconds since the last lock.
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[24];
                    bConvert[1] = bMsg[25];
                    this.DeviceTimeSeconds = BitConverter.ToInt32(bConvert, 0);

                    // don't add devicetimer if we don't have gps lock
                    if (this.DeviceTimeSeconds > 0 && (this.GPSPosition.dLatitude != 0 && this.GPSPosition.dLongitude != 0))
                    {
                        this.GPSPosition.dtGPSTime = this.GPSPosition.dtGPSTime.AddSeconds(Convert.ToDouble(this.DeviceTimeSeconds));
                    }

                    bMsg = GetSubBytes(bMsg, 26, bMsg.Length);

                    // vaughan's request if no username specified on logon packet only
                    if (this.CommandType == "I")
                        if (bMsg[0] == (byte)0x0A)
                            return strErrMsg;

                    // Get the username
                    for (int X = 0; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0A) // If this byte is the USB byte
                        {
                            bConvert = new byte[X];
                            for (int Y = 0; Y < X; Y++)
                            {
                                bConvert[Y] = bMsg[Y];
                            }
                            this.Username = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPosition = X + 1;
                            break;
                        }
                    }


                    bMsg = GetSubBytes(bMsg, iPosition, bMsg.Length);
                }
            }
            else
            {
                if (this.CommandType == "R")
                {
                    // Get the GPS Data
                    bGPS = GetSubBytes(bMsg, 1, 18);
                    this.GPSPosition = new cGPSData(bGPS);
                    bMsg = GetSubBytes(bMsg, 19, bMsg.Length);
                }
                else
                {
                    if (bMsg.Length < 9)
                    {
                        // If the packet is too small, just return.
                        return strErrMsg;
                    }
                    else
                    {
                        // Strip off the [CmdType][MobileID] fileds from bMsg
                        bMsg = GetSubBytes(bMsg, 3, bMsg.Length);
                    }
                }
            }
            #endregion

            // bMsg is now the remaining bytes after the standard header ([CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB] has been stripped out)


            switch (CommandType)
            {
                case "d":	// d - Request Data Segment - [Send Segment (4 Bytes)]
                    #region Request a Download Packet
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "r")
                    {
                        bConvert = new byte[2];
                        bConvert[0] = bMsg[1];
                        bConvert[1] = bMsg[2];
                        this.RequestedParcelNo = BitConverter.ToUInt16(bConvert, 0);
                    }
                    #endregion
                    break;
                case "N":
                    #region Ack an NAck Responce
                    // [CmdType][Mobile ID][Result Code][Message][Job ID][Stop #][SBS][Spare]
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[1];
                    this.CodeNumber = BitConverter.ToInt32(bConvert, 0);

                    // Get the Nack/Ack Message
                    this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                    // Get the job as stop ids
                    if (this.Message.Substring(0, 2).ToUpper() == "OK")
                    {
                        this.JobID = BitConverter.ToInt32(bMsg, 4);
                        // Get the stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[8];
                        this.StopID = BitConverter.ToInt32(bConvert, 0);
                    }
                    else
                    {
                        this.JobID = BitConverter.ToInt32(bMsg, 6);
                        // Get the stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[10];
                        this.StopID = BitConverter.ToInt32(bConvert, 0);
                    }
                    #endregion
                    break;
                case "I":
                    #region Logon to System
                    // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB]
                    //[Password][PhoneBookID][MessagesID][DivisionReasonID][DelayReasonID][RejectedReasonID][Distance] [SBS][Spare]

                    iPosition = 0;

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Password = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.PhoneBookID = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.MessagebookID = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.DivisionReasonID = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.DelayReasonID = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.RejectedReasonID = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    break;
                case "O":
                    #region Log off the System
                    // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][Distance][SBS][Spare]
                    iPosition = 0;
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    break;
                case "S":
                    #region Stop Packet
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);

                    iPosition = 1;
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.ReferenceNumber = BitConverter.ToInt32(bConvert, 0);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.TimeLoading = BitConverter.ToInt32(bConvert, 0);

                    if (this.SubCommandType == "P")
                    {
                        #region Stop Packet - Pickup
                        // [CmdType][MobileID][FleetID][VehicleID][GPS] [Username][USB][SubCmd]
                        //[Distance][ReferenceNo][TimeLoading]
                        //[Division][DelayReason][DelayDescription]{[PalletType][PNS][PES]}[SBS][Spare]

                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        this.DivisionID = BitConverter.ToInt32(bConvert, 0);

                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        this.DelayReasonID = BitConverter.ToInt32(bConvert, 0);


                        z = 0;
                        y = 0;
                        bConvert = new byte[100];
                        for (y = iPosition; y <= iPosition + 100; y++)
                        {
                            if (bMsg[y] == (byte)0x0B)
                            {
                                break;
                            }
                            else
                            {
                                bConvert[z++] = bMsg[y];
                            }
                        }
                        iPosition = y + 1;
                        sDD = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                        sDD = sDD.Replace("\0", "");
                        this.DelayDescription = sDD;

                        // Pallet Section Start Byte
                        if (bMsg[iPosition] == (byte)0x09)
                        {
                            iPosition += 1;
                        }

                        bool bLoop = true;
                        this.PalletTypeCodes = new StringCollection();
                        this.PalletNumbers = new StringCollection();
                        while (bLoop && iPosition < bMsg.Length)
                        {
                            bConvert = new byte[2];
                            bConvert[0] = bMsg[iPosition++];
                            bConvert[1] = bMsg[iPosition++];
                            this.PalletTypeCodes.Add(System.Text.ASCIIEncoding.ASCII.GetString(bConvert));


                            for (int X = iPosition; X < bMsg.Length; X++)
                            {
                                if (bMsg[X] == (byte)0x09 || bMsg[X] == (byte)0x08) // If this byte is the USB byte
                                {
                                    if (X - iPosition > 0)
                                    {
                                        bConvert = new byte[X - iPosition];
                                        for (int Y = 0; Y < X - iPosition; Y++)
                                        {
                                            bConvert[Y] = bMsg[iPosition + Y];
                                        }
                                        this.PalletNumbers.Add(System.Text.ASCIIEncoding.ASCII.GetString(bConvert));
                                        if (bMsg[X] == (byte)0x08)
                                        {
                                            // The PES marker has been found.
                                            // Set iPosition to the next byte after the USB byte.
                                            iPosition = X + 1;
                                            bLoop = false;
                                        }
                                        else
                                        {
                                            // Set iPosition to the next byte after the USB byte.
                                            iPosition = X + 1;
                                        }
                                    }
                                    // Break out back to the while loop.
                                    break;
                                }
                            }

                            if (bMsg[iPosition] == (byte)0x08)
                            {
                                iPosition = iPosition + 1;
                                bLoop = false;
                            }
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "D")
                    {
                        #region Stop Packet - Delivery
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB]
                        // [SubCmd][Distance][ReferenceNo][TimeLoading]
                        // [DelayReason][DelayDescription][POD Name][PNS][RejectCode][SOS][SIG LEN][SIG DATA][SBS][Spare]
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        this.DelayReasonID = BitConverter.ToInt32(bConvert, 0);



                        z = 0;
                        y = 0;
                        bConvert = new byte[100];
                        for (y = iPosition; y <= iPosition + 100; y++)
                        {
                            if (bMsg[y] == (byte)0x0B)
                            {
                                break;
                            }
                            else
                            {
                                bConvert[z++] = bMsg[y];
                            }
                        }
                        iPosition = y + 1;
                        sDD = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                        sDD = sDD.Replace("\0", "");
                        this.DelayDescription = sDD;




                        //POD Name = 3 � 23 characters of text.
                        //PNS = POD name separator 0x0B
                        z = 0;
                        y = 0;
                        bConvert = new byte[23];
                        for (y = iPosition; y <= iPosition + 23; y++)
                        {
                            if (bMsg[y] == (byte)0x0B)
                            {
                                break;
                            }
                            else
                            {
                                bConvert[z++] = bMsg[y];
                            }
                        }
                        iPosition = y + 1;
                        sPN = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                        sPN = sPN.Replace("\0", "");
                        this.PODName = sPN;


                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        this.RejectedReasonID = BitConverter.ToInt32(bConvert, 0);

                        try
                        {
                            byte bSigType = bMsg[iPosition++];

                            if (bSigType == (byte)0x0C)
                            {
                                // siglen
                                bConvert = new byte[4];
                                bConvert[0] = bMsg[iPosition++];
                                bConvert[1] = bMsg[iPosition++];
                                int iSigLen = BitConverter.ToInt32(bConvert, 0);
                                if (iSigLen == 0)
                                    bSignatureData = new byte[1];
                                else
                                {
                                    bSignatureData = GetSubBytes(bMsg, iPosition, iSigLen);
                                    if (bSignatureData.Length > 12)
                                    {
                                        if (_sigConfig.LogSignatureData && (_log != null))
                                            _log.Info("Converting Adhoc Signature for Unit : " + sUnitID + ", Job : " + iJobID + ", Data : " + ConvertToAscii(bSignatureData) + "\n");

                                        // Create a new conversion object
                                        CameronsConvertSignature oConv = new CameronsConvertSignature(_sigConfig);
                                        // Translate the data into a GIF and return the image as bytes
                                        bSignatureData = oConv.TranslateSignature(bSignatureData, Convert.ToString(this.ReferenceNumber));

                                        // Dispose an object
                                        oConv = null;
                                    }
                                }
                            }
                            else
                            {
                                if (bSigType == (byte)0x0B)
                                {
                                    // siglen
                                    bConvert = new byte[4];
                                    bConvert[0] = bMsg[iPosition++];
                                    bConvert[1] = bMsg[iPosition++];
                                    int iSigLen = BitConverter.ToInt32(bConvert, 0);
                                    if (iSigLen == 0)
                                        bSignatureData = new byte[1];
                                    else
                                    {
                                        // Get the signature data
                                        bSignatureData = GetSubBytes(bMsg, iPosition, iSigLen);
                                        if (bSignatureData.Length > 12)
                                        {

                                            if (_sigConfig.LogSignatureData && (_log != null))
                                                _log.Info("Converting Adhoc Signature for Unit : " + sUnitID + ", Job : " + iJobID + ", Data : " + ConvertToAscii(bSignatureData) + "\n");


                                            string sSigFileName = _sigConfig.PicturePath;


                                            cVectorSignature oSig = new cVectorSignature( false, _sigConfig.MaxHeight, _sigConfig.MaxWidth, _sigConfig.MaxHeight, _sigConfig.MaxWidth, sSigFileName, _sigConfig.DefaultBackgroundGif, 0, 0, 0.5, true, 100000, true, System.Drawing.Color.White, true, 10);
                                            bSignatureData = oSig.TranslateSignature(bSignatureData, (byte)0x0B);			//convert the signature into gif file

                                            oSig = null;
                                        }
                                        else
                                        {
                                            // put default no signature image
                                            if (_sigConfig.LogSignatureData && (_log != null))
                                                _log.Info("Using Default Signature Back.gif for Unit : " + sUnitID + ", Job : " + iJobID + ", Data : " + ConvertToAscii(bSignatureData) + "\n");

                                            // load back.gif as default image
                                            byte[] bLoadData = new byte[4000];
                                            int iBytes = System.IO.File.OpenRead(_sigConfig.DefaultBackgroundGif).Read(bLoadData, 0, 4000);
                                            bSignatureData = new byte[iBytes];
                                            for (int iStrip = 0; iStrip < iBytes; iStrip++)
                                                bSignatureData[iStrip] = bLoadData[iStrip];

                                            // save image as new referencenumber.gif in sigtemp folder
                                            string sPath = _sigConfig.PicturePath + Convert.ToString(this.ReferenceNumber) + ".gif";
                                            System.IO.File.OpenWrite(sPath).Write(bSignatureData, 0, bSignatureData.Length);

                                        }
                                    }
                                }

                                if (bSignatureData != null)
                                {
                                    // Get the length of bytes.
                                    bSignatureLen = BitConverter.GetBytes(bSignatureData.Length);
                                    if (_sigConfig.LogSignatureData && (_log != null))
                                        _log.Info("Converted Adhoc/Default Signature for Unit : " + sUnitID + ", Job : " + iJobID + "\n");
                                    this.SignatureData = bSignatureData;
                                }
                                else
                                    this.SignatureData = null;
                            }
                        }
                        catch (System.Exception ex)
                        {
                            Console.Write(ex.Message + "\n" + ex.Source + "\n" + ex.StackTrace);
                            this.SignatureData = null;
                        }
                        #endregion
                    }
                    #endregion
                    break;
                case "R":
                    #region Request Packets
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "J")
                    {
                        #region Request Jobs
                        iPosition = 1;
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.Distance = BitConverter.ToInt32(bConvert, 0);
                        #endregion
                    }
                    #endregion
                    break;
                case "F":
                    #region Lunch Break on off Packet
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "S")
                    {
                        #region Start Lunch Break
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]						
                        iPosition = 1;
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.Distance = BitConverter.ToInt32(bConvert, 0);
                        #endregion
                    }
                    else if (this.SubCommandType == "F")
                    {
                        #region Finish Lunch
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][SBS][Spare]						
                        iPosition = 1;
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition++];
                        bConvert[2] = bMsg[iPosition++];
                        bConvert[3] = bMsg[iPosition++];
                        this.Distance = BitConverter.ToInt32(bConvert, 0);
                        #endregion
                    }
                    #endregion
                    break;
                case "M":
                    #region Messages
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "F")
                    {
                        #region Driver initiated message (Freeform)
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd]
                        //[Message Data][MSDC][SBS][Spare]
                        iPosition = 1;
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x06) // If this byte is the MSDC byte
                            {
                                bConvert = new byte[X - 1];
                                for (int Y = 1; Y < X; Y++)
                                {
                                    bConvert[Y - 1] = bMsg[Y];
                                }
                                this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the PNS byte.
                                iPosition = X + 1;
                                break;
                            }
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "P")
                    {
                        #region Driver initiated message (Predefined)
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Message ID][SBS][Spare]
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[1];
                        this.MessageID = BitConverter.ToInt32(bConvert, 0);
                        #endregion
                    }
                    if (this.SubCommandType == "f")
                    {
                        #region Driver initiated Email message (Freeform)
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Email][EMS][MessageData][MSDC][SBS][Spare]
                        iPosition = 1;
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the EMS byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                }
                                this.EmailAddress = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the PNS byte.
                                iPosition = X + 1;
                                break;
                            }
                        }

                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x06) // If this byte is the MDSC byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                }
                                this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the PNS byte.
                                iPosition = X + 1;
                                break;
                            }
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "p")
                    {
                        #region Driver initiated Email message (Predefined)
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Email][EMS][Message ID][SBS][Spare]
                        iPosition = 1;
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the EMS byte
                            {
                                bConvert = new byte[X];
                                for (int Y = 0; Y < X; Y++)
                                {
                                    bConvert[Y] = bMsg[Y];
                                }
                                this.EmailAddress = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the PNS byte.
                                iPosition = X + 1;
                                break;
                            }
                        }

                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition];
                        this.MessageID = BitConverter.ToInt32(bConvert, 0);

                        #endregion
                    }
                    #endregion
                    break;
                case "K":
                    #region Phone Call Report
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    this.CallType = this.SubCommandType;
                    iPosition = 1;
                    if (this.SubCommandType == "I")
                    {
                        #region Incomming Calls
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd]
                        // [Number][PSP][Time][SBS][Spare]						
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the PSP byte
                            {
                                bConvert = new byte[X - 1];
                                for (int Y = 0; Y < X - 1; Y++)
                                {
                                    bConvert[Y] = bMsg[Y + iPosition];
                                }
                                this.PhoneNumber = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the PSP byte.
                                iPosition = X + 1;
                                break;
                            }
                        }

                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition];
                        this.CallTime = BitConverter.ToInt32(bConvert, 0);
                        #endregion
                    }
                    else if (this.SubCommandType == "O")
                    {
                        #region Outgoing Calls
                        // [CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB][SubCmd][Number][PSP][Time][SBS][Spare]						
                        for (int X = iPosition; X < bMsg.Length; X++)
                        {
                            if (bMsg[X] == (byte)0x0A) // If this byte is the PSP byte
                            {
                                bConvert = new byte[X - 1];
                                for (int Y = 0; Y < X - 1; Y++)
                                {
                                    bConvert[Y] = bMsg[Y + iPosition];
                                }
                                this.PhoneNumber = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                                // Set iPosition to the next byte after the PSP byte.
                                iPosition = X + 1;
                                break;
                            }
                        }

                        bConvert = new byte[4];
                        bConvert[0] = bMsg[iPosition++];
                        bConvert[1] = bMsg[iPosition];
                        this.CallTime = BitConverter.ToInt32(bConvert, 0);
                        #endregion
                    }
                    #endregion
                    break;
                case "Y":
                    #region Odometer Reading
                    //[CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB]
                    //[ODO][SBS][Spare]

                    for (int X = 0; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x07) // If this byte is the SBS  byte
                        {
                            bConvert = new byte[X];
                            for (int Y = 0; Y < X; Y++)
                            {
                                bConvert[Y] = bMsg[Y];
                            }
                            string sOdo = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            sOdo = sOdo.Replace("\0", "");

                            this.Odometer = 0;
                            if (Util.IsIntegerPositive(sOdo))
                                this.Odometer = Convert.ToInt32(sOdo);


                            break;
                        }
                    }
                    #endregion
                    break;
                case "U":
                    #region Vehicle Usage
                    //[CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB]
                    //[SubCmd][SBS][Spare]
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    iPosition = 1;
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    break;
                case "X":
                    #region Arrive Depart
                    //[CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB]
                    //[SubCmd][Distance][SBS][Spare]
                    iPosition = 1;
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    break;
                case "B":
                    #region Trailer Hitch and De-Hitch Report Packet
                    //[CmdType][MobileID][FleetID][VehicleID][GPS][Username][USB]
                    //[SubCMD]{[TNS][TrailerNo][TOS][ODO]}[TES][SBS][Spare]
                    this.Trailers = new StringCollection();
                    this.TrailerOdometers = new StringCollection();
                    string sTmp = "";
                    iPosition = 1;
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    int k = 0;
                    //{[TNS][TrailerNo][TOS][ODO]}[TES]					
                    do
                    {
                        // skip [TNS] 0x0A
                        iPosition++;
                        k = 0;
                        bConvert = new byte[15];
                        while (bMsg[iPosition] != (byte)0x0B) // [TOS] 0x0B
                        {
                            // read TrailerNo data
                            bConvert[k++] = bMsg[iPosition];
                            iPosition++;
                        }
                        sTmp = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                        sTmp = sTmp.Replace("\0", "");
                        this.Trailers.Add(sTmp);

                        iPosition++;
                        k = 0;
                        bConvert = new byte[10];
                        while (bMsg[iPosition] != (byte)0x0C) // [TES] 0x0C
                        {
                            // read odometer data
                            bConvert[k++] = bMsg[iPosition];
                            iPosition++;
                        }

                        sTmp = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                        sTmp = sTmp.Replace("\0", "");
                        this.TrailerOdometers.Add(sTmp);

                    } while (bMsg[iPosition] != (byte)0x0C); //[TES] 0x0C

                    iPosition++;

                    // distance
                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = BitConverter.ToInt32(bConvert, 0);

                    #endregion
                    break;
                case "V":
                    #region Firmware Version Packet
                    iPosition = 0;
                    this.FirmwareRevMajor = bMsg[iPosition++];
                    this.FirmwareRevMinor = bMsg[iPosition++];
                    #endregion
                    break;
                default:
                    break;
            }

            if (_log != null) _log.Info(sData);
            return "";
        }
        #endregion


        #region GetPacketTypeName
        public string GetPacketTypeName()
        {
            string sRet = "";

            switch (CommandType)
            {
                case "N":
                    sRet = "Interface Ack Packet";
                    break;
                case "O":
                    sRet = "Login Packet";
                    break;
                case "A":
                    sRet = "Accept Job Packet";
                    break;
                case "X":
                    sRet = "Arrive at Site";
                    break;
                case "S":
                    sRet = GetAdhocPacketType();
                    break;
                case "C":
                    sRet = "Complete Job Packet";
                    break;
                case "R":
                    sRet = GetRequestPacketType();
                    break;
                case "P":
                    sRet = "Pre-defined Function Packet";
                    break;
                case "B":
                    sRet = GetNettrackPacketType();
                    break;
                case "M":
                    sRet = "Message Packet";
                    break;
                case "d":
                    sRet = "Download On Demand Request Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetAdhocPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "P":
                    sRet = "Adhoc Pickup  Packet";
                    break;
                case "D":
                    sRet = "Adhoc Delivery Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetRequestPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "J":
                    sRet = "Request Job Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetNettrackPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "G":
                    sRet = "Nettrack Generic Data Packet";
                    break;
                case "S":
                    sRet = "Nettrack Start Job Packet";
                    break;
                case "P":
                    sRet = "Nettrack Picjup Packet";
                    break;
                case "D":
                    sRet = "Nettrack Pre-Delivery Packet";
                    break;
                case "C":
                    sRet = "Nettrack Complete Job Packet";
                    break;
                case "O":
                    sRet = "Nettrack Delivery Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }
        #endregion

        #region Private Functions
        private static byte[] CreateBadPacketNak()
        {
            byte[] bPacketData = new byte[1];
            byte[] bPacket = new byte[1];
            byte[] bPacketLen = null;
            byte[] bCmdType = null;

            bCmdType = System.Text.ASCIIEncoding.ASCII.GetBytes("N");

            bPacketData[0] = bCmdType[0];
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, Convert.ToInt32((byte)0x00));
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, "4");
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x07);
            bPacketData = PacketUtilities.AppendToPacket(bPacketData, (byte)0x03);

            bPacketLen = BitConverter.GetBytes(Convert.ToUInt32(bPacketData.Length));
            bPacket = PacketUtilities.AppendToPacket(bPacket, (byte)bPacketLen[0]);
            bPacket = PacketUtilities.AppendToPacket(bPacket, (byte)bPacketLen[1]);
            bPacket = PacketUtilities.AppendToPacket(bPacket, bPacketData);
            return bPacket;
        }

        private static byte[] GetSubBytes(byte[] bMsg, int iStartAt, int iNumOfBytes)
        {
            byte[] bResult = null;
            int iResultCount = 0;

            if (bMsg == null) return null;

            if (bMsg.Length >= (iStartAt + iNumOfBytes))
            {

                bResult = new byte[iNumOfBytes];

                for (int X = iStartAt; X < iStartAt + iNumOfBytes; X++)
                {
                    bResult[iResultCount] = bMsg[X];
                    iResultCount++;
                }
                return bResult;
            }
            else
            {
                iNumOfBytes = bMsg.Length;
                bResult = new byte[iNumOfBytes - iStartAt];
                for (int X = iStartAt; X < iNumOfBytes; X++)
                {
                    bResult[iResultCount] = bMsg[X];
                    iResultCount++;
                }
                return bResult;
            }
        }

        private static string RemoveLeadingReturnChars(string strData)
        {
            string strTemp = strData.Trim();

            if (strTemp.Length > 0)
            {
                while (strTemp[0] == '\r' || strTemp[0] == '\n')
                {
                    if (strTemp.Length == 1)
                    {
                        strTemp = "";
                        break;
                    }
                    else
                    {
                        strTemp = strTemp.Substring(1, strTemp.Length - 1);
                    }
                }
            }
            return strTemp;
        }

        private static string WordWrapString(string strDescLines, int iLineLength)
        {
            char[] cDesc = strDescLines.ToCharArray(0, strDescLines.Length);
            string sResult = "";
            string sLine = "";
            int iCurrentPos = 1;

            if (cDesc.Length > 0)
            {
                for (int X = 0; X < cDesc.Length; X++)
                {
                    // For each char in the array
                    if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                    {
                        if (iCurrentPos > 0)
                        {
                            sResult += sLine + "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                        else
                        {
                            sResult += "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                    }
                    else
                    {
                        if (iCurrentPos > iLineLength)
                        {
                            int iLastSpace = sLine.LastIndexOf(' ');
                            if (iLastSpace > 0)
                            {
                                sResult += sLine.Substring(0, iLastSpace) + "\n";
                                sLine = sLine.Substring(iLastSpace + 1).Trim();
                                iCurrentPos = sLine.Length;
                            }
                            else
                            {
                                sResult += sLine + "\n";
                                sLine = "";
                                iCurrentPos = 0;
                            }
                            if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                            {
                                if (iCurrentPos > 0)
                                {
                                    sResult += sLine + "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                                else
                                {
                                    sResult += "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                            }
                            else
                            {
                                if (iCurrentPos == 0 && cDesc[X] == ' ')
                                {
                                    // Don't add white space to the front of the line.
                                }
                                else
                                {
                                    sLine += cDesc[X];
                                    iCurrentPos++;
                                }
                            }
                        }
                        else
                        {
                            if (iCurrentPos == 0 && cDesc[X] == ' ')
                            {
                                // Don't add white space to the front of the line.

                            }
                            else
                            {
                                sLine += cDesc[X];
                                iCurrentPos++;
                            }
                        }
                    }
                }
            }

            if (sLine.Length > 0)
                sResult += sLine;

            return sResult;
        }

        public static string ConvertToAscii(byte[] bPacket)
        {
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
        {
            string sRet = "";
            int X = 0;
            //			byte bLower = 0x21;	// 33
            //			byte bUpper = 0x7E;  // 126
            byte bTest = 0x00;  // null
            byte[] bTestArray = new byte[1];

            if (bPacket.Length > 0)
            {
                try
                {
                    for (X = 0; X < bPacket.Length; X++)
                    {
                        bTest = bPacket[X];
                        bTestArray = new byte[1];
                        bTestArray[0] = bTest;
                        if (bTest >= 33 && bTest <= 126)  // If the character is in printable range
                            if (bAddSpacing)
                                if (sRet.Length > 0)
                                    sRet += " - " + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                                else
                                    sRet = System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                            else
                                sRet += System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                        else  // Show it as a number
                            if (bAddSpacing)
                                if (sRet.Length > 0)
                                    sRet += " - [" + bTest + "]";
                                else
                                    sRet = "[" + Convert.ToString(bTest) + "]";
                            else
                                sRet += "[" + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray) + "]";
                    }

                }
                catch (System.Exception ex)
                {
                    Console.Write(ex.Message + "\n");
                }
            }
            return sRet;
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind)
        {
            return FindFirstIndexOfByte(sData, bFind, 0, sData.Length);
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt)
        {
            return FindFirstIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
        {
            int iRet = -1;

            try
            {
                iRet = sData.IndexOf((char)bFind + "", iStartAt, iLength);
            }
            catch (System.Exception)
            {
                return -1;
            }
            return iRet;
        }

        private static int FindLastIndexOfByte(string sData, byte bFind)
        {
            return FindLastIndexOfByte(sData, bFind, sData.Length, sData.Length);
        }

        private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt)
        {
            return FindLastIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
        }

        private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
        {
            int iRet = -1;

            try
            {
                iRet = sData.LastIndexOf((char)bFind + "", iStartAt, iLength);
            }
            catch (System.Exception)
            {
                return -1;
            }
            return iRet;
        }


        private static string TruncateString(string sData, int iPos)
        {
            string sRet = "";

            try
            {
                if (sData.Length < iPos + 1)
                    return "";
                else
                    sRet = sData.Substring((iPos + 1), sData.Length - (iPos + 1));
            }
            catch (System.Exception)
            {
                return "";
            }
            return sRet;
        }

        public string GenerateSQLParameters()
        {
            string sRet = "";
            if (CommandType == "S")
            {
                if (SubCommandType == "P")
                {
                    sRet = "DECLARE @RC int, @PickupID int, @DeliveryID int, @DriverID int, @FleetID int, @VehicleID int, @MobileID int, @Latitude float, @Longitude float, @GPSTime datetime, @ReferenceNumber int, @DivisionID int, @FullOrEmpty bit, @DelayReasonID int, @DelayReason nvarchar(255), @Distance int, @LoadingTime int, @CustomerID int, @TravelTime int, @ExchangedPalletID int, @SupervisorID int, @DeviceTimer int, @PersistAction nvarchar(20), @JobReferencePalletID int, @NumberOfPallets int, @PalletTypeID int, @PalletTypeCode nvarchar(3)\r\n";
                    sRet += "SET @DriverID  = " + Convert.ToString(this.Username) + "\r\n";
                    sRet += "SET @FleetID  = " + Convert.ToString(this.FleetID) + "\r\n";
                    sRet += "SET @VehicleID  = " + Convert.ToString(this.VehicleID) + "\r\n";
                    sRet += "SET @MobileID  = " + Convert.ToString(this.iMobileID) + "\r\n";
                    sRet += "SET @Latitude  = " + Convert.ToString(this.GPSPosition.dLatitude) + "\r\n";
                    sRet += "SET @Longitude  = " + Convert.ToString(this.GPSPosition.dLongitude) + "\r\n";
                    sRet += "SET @GPSTime = cast('" + this.GPSPosition.dtGPSTime.ToString("MM/dd/yyyy HH:mm:ss") + "' as datetime)\r\n";
                    sRet += "SET @ReferenceNumber  = " + Convert.ToString(this.ReferenceNumber) + "\r\n";
                    sRet += "SET @DivisionID  = " + Convert.ToString(this.DivisionID) + "\r\n";
                    sRet += "SET @FullOrEmpty  = 0\r\n";
                    sRet += "SET @DelayReasonID  = " + Convert.ToString(this.DelayReasonID) + "\r\n";
                    sRet += "SET @DelayReason  = '" + this.DelayDescription + "'\r\n";
                    sRet += "SET @Distance  = " + Convert.ToString(this.Distance) + "\r\n";
                    sRet += "SET @LoadingTime  = " + Convert.ToString(this.TimeLoading) + "\r\n";
                    sRet += "SET @CustomerID  = -1\r\n";
                    sRet += "SET @TravelTime  = 0\r\n";
                    sRet += "SET @ExchangedPalletID  = 0\r\n";
                    sRet += "SET @SupervisorID  = 0\r\n";
                    sRet += "SET @DeviceTimer  = 0\r\n";
                    sRet += "SET @PersistAction = 'Insert'\r\n";
                    sRet += "SET @DeviceTimer  = 0\r\n";
                    sRet += "\r\n";
                    sRet += "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[T_InsertNewPickup]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1) BEGIN\r\n";
                    sRet += "	CREATE TABLE [dbo].[T_InsertNewPickup](\r\n";
                    sRet += "		[PickupID] [int] NOT NULL,\r\n";
                    sRet += "	CONSTRAINT [PK_T_InsertNewPickup] PRIMARY KEY CLUSTERED \r\n";
                    sRet += "	(\r\n";
                    sRet += "		[PickupID] ASC\r\n";
                    sRet += "	)WITH FILLFACTOR = 100 ON [PRIMARY]\r\n";
                    sRet += "	) ON [PRIMARY]\r\n";
                    sRet += "END\r\n";
                    sRet += "\r\n";
                    sRet += "IF NOT EXISTS (SELECT * FROM dbo.sysobjects WHERE id = OBJECT_ID(N'[dbo].[T_InsertNewPalletEntry]') AND OBJECTPROPERTY(id, N'IsUserTable') = 1) BEGIN\r\n";
                    sRet += "	CREATE TABLE [dbo].[T_InsertNewPalletEntry](\r\n";
                    sRet += "		[JobReferencePalletID] [int] NOT NULL,\r\n";
                    sRet += "	CONSTRAINT [PK_T_InsertNewPalletEntry] PRIMARY KEY CLUSTERED \r\n";
                    sRet += "	(\r\n";
                    sRet += "		[JobReferencePalletID] ASC\r\n";
                    sRet += "	)WITH FILLFACTOR = 100 ON [PRIMARY]\r\n";
                    sRet += "	) ON [PRIMARY]\r\n";
                    sRet += "END\r\n";
                    sRet += "\r\n";
                    sRet += "IF EXISTS (SELECT DeliveryID FROM T_Deliveries WHERE ReferenceNumber = @ReferenceNumber AND GPSTime > @GPSTime) BEGIN\r\n";
                    sRet += "	SELECT @DeliveryID = min(DeliveryID) FROM T_Deliveries WHERE ReferenceNumber = @ReferenceNumber AND GPSTime > @GPSTime\r\n";
                    sRet += "END ELSE BEGIN\r\n";
                    sRet += "	SET @DeliveryID = null\r\n";
                    sRet += "END\r\n";
                    sRet += "\r\n";
                    sRet += "TRUNCATE TABLE T_InsertNewPickup\r\n";
                    sRet += "\r\n";
                    sRet += "INSERT INTO [T_InsertNewPickup](PickupID)\r\n";
                    sRet += "EXEC [usp_PickupsPersist] @PickupID, @DriverID, @FleetID, @VehicleID, @MobileID, @Latitude, @Longitude, @GPSTime, @ReferenceNumber, @DivisionID, @FullOrEmpty, @DelayReasonID, @DelayReason, @Distance, @LoadingTime, @CustomerID, @TravelTime, @ExchangedPalletID, @SupervisorID, @DeviceTimer, @PersistAction\r\n";
                    sRet += "\r\n";
                    sRet += "SELECT @PickupID = max(PickupID) FROM [T_InsertNewPickup]\r\n";
                    sRet += "\r\n";
                    sRet += "UPDATE T_Deliveries SET PickupID = @PickupID WHERE DeliveryID = @DeliveryID\r\n";
                    sRet += "\r\n";
                    if (this.PalletTypeCodes != null && this.PalletNumbers != null)
                    {
                        for (int X = 0; X < this.PalletTypeCodes.Count; X++)
                        {
                            sRet += "SET @PalletTypeCode = '" + this.PalletTypeCodes[X] + "'\r\n";
                            sRet += "IF EXISTS (SELECT PalletTypeID FROM T_PalletTypes WHERE Type = @PalletTypeCode) BEGIN\r\n";
                            sRet += "	SELECT @PalletTypeID = PalletTypeID FROM T_PalletTypes WHERE Type = @PalletTypeCode\r\n";
                            sRet += "   SET @JobReferencePalletID = -1\r\n";
                            sRet += "   SET @NumberOfPallets = " + this.PalletNumbers[X] + "\r\n";
                            sRet += "	TRUNCATE TABLE [T_InsertNewPalletEntry]\r\n";
                            sRet += "\r\n";
                            sRet += "	INSERT INTO [T_InsertNewPalletEntry](JobReferencePalletID)\r\n";
                            sRet += "	EXEC [usp_JobReferencePalletsPersist] @JobReferencePalletID, @ReferenceNumber, @NumberOfPallets, @PalletTypeID, @SupervisorID, @PickupID, @PersistAction\r\n";
                            sRet += "\r\n";
                            sRet += "	IF EXISTS (SELECT JobReferencePalletID FROM [T_InsertNewPalletEntry]) BEGIN\r\n";
                            sRet += "		IF @DeliveryID is not null BEGIN\r\n";
                            sRet += "			SELECT @JobReferencePalletID = max(JobReferencePalletID) FROM [T_InsertNewPalletEntry]\r\n";
                            sRet += "			UPDATE T_JobReferencePallets SET DeliveryID = @DeliveryID WHERE JobReferencePalletID = @JobReferencePalletID\r\n";
                            sRet += "		END\r\n";
                            sRet += "	END\r\n";
                            sRet += "END\r\n";
                            sRet += "\r\n";
                        }
                    }
                    sRet += "GO\r\n";
                }
            }
            return sRet;
        }
        #endregion
    }

}

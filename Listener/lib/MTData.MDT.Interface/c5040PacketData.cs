using System;
using System.Collections;
using System.Collections.Specialized;
using log4net;
using MTData.Common.Config;
using MTData.Common.Network;

namespace MTData.MDT.Interface
{
    /// <summary>
    /// Summary description for c5040PacketData.
    /// </summary>
    public class c5040PacketData
    {
        #region Incomming Packet Defs
        // N - ACK Packet - [CmdType][GPS][FailCode][Message][Job ID][Stop #]
        // I -  Login Packet - [CmdType][MobileID][FleetID][VehicleID][GPS]   [Username][USB][Password][PhoneBookID][MessagesID][DivisionReasonID][DelayReasonID][RejectedReasonID][Distance] [SBS][Spare]
        // O - Logout Packet - [CmdType][GPS][Password]
        // A - Generic Accept - [CmdType][GPS][Job ID]
        // X - Generic Reject Job - [CmdType][GPS][Job ID]
        // S - Generic Pickup
        //		Pickup SubCmd = P -	[CmdType][GPS][SubCmd][Job ID][Stop #]
        //		Deliver SubCmd = D -	
        //			[CmdType][GPS][SubCmd][Job ID][Stop #][POD Name][BCS][Bar Code 1][BCS] � [Bar Code x][BCS][SOS][Signature]
        // C - Generic Complete Job - [CmdType][GPS][Job ID][Success Code]
        // R - SubCmd = J - Request Jobs / Keepalive - [CmdType][GPS][SubCmd]
        // P - Start a predefined function on the server - [CmdType][GPS][Function ID][Start / Stop]
        // B - Nettrack Packets
        //	G - Generic Data - [CmdType][GPS][Sub Cmd][200 bytes]
        //	S - Start Job - [CmdType][Sub Cmd][JobID][Time][GPS][SBS][Spare]
        //	P - Pickup - [CmdType][GPS][Sub Cmd][JobID][BCS][Bar Code 1][BCS] � [Bar Code x][BCS][SBS][Spare]
        //	D - Pre-Delivery - [CmdType][GPS][Sub Cmd][JobID][StopID][Suburb ID][Business Name][SBS][Spare]
        //	O - Delivery - [CmdType][GPS][SubCmd][Job ID][Stop #][POD Name][BCS][Bar Code 1][BCS] � [Bar Code x][BCS][RBCS]...[Bar Code x][RBCS]
        //							[ICS][Invoice Number][ICS][SOS][Signature][SBS][Spare]
        //	C - Complete Job - [CmdType][GPS][Sub Cmd][JobID][Time in Mins][SBS][Spare]
        //M - Driver Message
        //	F - Freeform message - [CmdType][SubCmd][Message Data][MDSC][GPS][SBS][Spare]
        //	P - Predefined Message - [CmdType][SubCmd][Message ID][GPS][SBS][Spare]
        #endregion

        #region Outgoing Packet Defs
        // N - Ack Packet - [CmdType][Code Number][Ack Msg][JobID][Length]
        // J - New Job Header - [CmdType][Job ID][No of Stops][JobTitle][0x0E][Contact Name][0x0D][Contact Phone][0x0D]
        // A - Add a job stop - [CmdType][JobID][StopID][Description][0x0D]
        // U - Update a job stop - [CmdType][Job ID][Stop ID][Description][0x0D]
        // R - Remove a job stop - [CmdType][JobID][StopID]
        // M - Send a driver message - [CmdType][Message][0x0D]
        #endregion

        #region Private Vars
        private ConvertSignatureConfig _sigConfig = null;
        private ILog _log = LogManager.GetLogger(typeof(c5040PacketData));
        private bool bTranslateRJs = true;
        #endregion

        #region Public Vars
        // Shareed Public variables
        public int iMobileID = 0;
        public string CommandType = "";				// [CmdType]
        public int JobID = 0;								// [Job ID]
        public int StopID = 0;								// [Stop #]
        public int CodeNumber = 0;						// [FailCode], [SucessCode], [Function ID]
        public string Message = "";						// [Message Data]

        // Incomming Packet Specific
        public string SubCommandType = "";		// [SubCmd]
        public bool StartFunction = false;			// [Start / Stop]
        public int SuburbID = 0;							// [Suburb ID]
        public int MessageID = 0;							// [Message ID]
        public int VehicleID = 0;							// [VehicleID]
        public int FleetID = 0;								// [FleetID]
        public int PhonebookID = 0;						// [PhoneBook ID]
        public int MessagebookID = 0;					// [MessageBook ID]
        public int DivisionReasonID = 0;				// [DivisionReasonID]
        public int DelayReasonID = 0;					// [DelayReasonID]
        public int RejectedReasonID = 0;			// [RejectedReasonID]
        public int Distance = 0;								// [Distance]
        public string Username = "";						// [Username]
        public string Password = "";						// [Password]
        public string PODName = "";					// [POD Name]
        public string JobTime = "";						// [Time], [Time in Mins]
        public string BusinessName = "";				// [Business Name]
        public string InvoiceNumber = "";			// [Invoice Number]
        public StringCollection BarCodes = null;				// [Bar Code x]
        public StringCollection ReturnBarCodes = null;	// [Return Bar Code x]
        public byte[] bGenericData = null;			// [200 bytes]
        public byte[] SignatureData = null;				// [Signature]

        // Outgoing Packet Specific
        public int TotalNumberOfStops = 0;
        public string JobTitle = "";
        public string ContactName = "";
        public string ContactPh = "";
        public string JobDescription = "";
        public string LoginReply = "";
        public int PhoneBookID = 0;
        public int iEntries = 0;
        public StringCollection PhoneBookLocations = new StringCollection();
        public StringCollection PhoneBookNumbers = new StringCollection();
        public StringCollection PhoneBookNames = new StringCollection();

        // GPS Data
        public cGPSData GPSPosition = null;
        #endregion

        #region Constructor
        public c5040PacketData(ConvertSignatureConfig sigConfig, bool TranslateRJs)
        {
            _sigConfig = sigConfig;

            bTranslateRJs = TranslateRJs;

            // Shared Packet Vars
            iMobileID = 0;
            CommandType = "";			// [CmdType]
            JobID = 0;							// [Job ID]
            StopID = 0;						// [Stop #]
            CodeNumber = 0;				// [FailCode], [SucessCode], [Function ID]
            Message = "";						// [Message Data]

            // Incomming Packets
            SubCommandType = "";		// [SubCmd]
            StartFunction = false;		// [Start / Stop]
            SuburbID = 0;					// [Suburb ID]
            MessageID = 0;					// [Message ID]
            Password = "";					// [Password]
            PODName = "";					// [POD Name]
            JobTime = "";						// [Time], [Time in Mins]
            BusinessName = "";			// [Business Name]
            InvoiceNumber = "";			// [Invoice Number]
            BarCodes = null;				// [Bar Code x]
            ReturnBarCodes = null;	// [Return Bar Code x]
            bGenericData = null;			// [200 bytes]
            SignatureData = null;		// [Signature]

            // Outgoing Packet Vars
            TotalNumberOfStops = 0;
            JobTitle = "";
            ContactName = "";
            ContactPh = "";
        }
        #endregion

        #region Build Packet Data
        public byte[] BuildOutgoingPacket()
        {
            byte[] bRet = null;
            byte[] bConvert = new byte[4];

            switch (CommandType)
            {
                case "N":
                    #region Ack Packet
                    // N - Ack Packet - [CmdType][Code Number][Ack Msg][JobID][Length]
                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("N");									// Cmd Type
                    if (this.CodeNumber == 0)
                    {
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, "0");										// Fail code
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, "OK");
                    }
                    else
                    {
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, Convert.ToString(this.CodeNumber));										// Fail code
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, "FAIL");
                    }
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, BitConverter.GetBytes(this.JobID));

                    byte[] bPacketLen = BitConverter.GetBytes(bRet.Length);
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bPacketLen[0]);
                    #endregion
                    break;
                case "L":
                    #region Login Reply packet
                    //					[CmdType][Mobile ID][SubCmd][SBS][Spare]

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("L");									// Cmd Type
                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.iMobileID));
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// Mobile ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[1]);										// Mobile ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, LoginReply);										// Login Responce
                    #endregion
                    break;
                case "J":
                    #region New Job Packet:

                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(this.TotalNumberOfStops);

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("J");									// Cmd Type
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, BitConverter.GetBytes(this.JobID));	// Job ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);					// No of Stops
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.JobTitle);											// Job Title
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0E);										// DSC 
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.ContactName);									// Contact Name
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0D);										//EOL 
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.ContactPh);										//Contact Ph Nr
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0D);										//EOL 
                    #endregion
                    break;
                case "A":
                    #region Add a job stop

                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(this.StopID);

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("A");									// Cmd Type
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, BitConverter.GetBytes(this.JobID));	// Job ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// Stop ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.JobDescription);									// Description 
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0D);										// EOL 
                    #endregion
                    break;
                case "U":
                    #region Update a job stop
                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(this.StopID);

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("U");									// Cmd Type
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, BitConverter.GetBytes(this.JobID));	// Job ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// Stop ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.JobDescription);									// Description 
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0D);										// EOL 
                    #endregion
                    break;
                case "R":
                    #region Remove a job stop
                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(this.StopID);

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("R");									// Cmd Type
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, BitConverter.GetBytes(this.JobID));	// Job ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// Stop ID
                    #endregion
                    break;
                case "D":
                    #region Delete a job
                    // D - Delete a job - [CmdType][Job ID]
                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("D");									// Cmd Type
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, BitConverter.GetBytes(this.JobID));	// Job ID
                    #endregion
                    break;
                case "M":
                    #region Send a driver message


                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("M");									// Cmd Type
                    bConvert = new byte[4];																								// Mobile ID
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(iMobileID));
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[1]);
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.Message);										// Message 
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0D);										// EOL 
                    #endregion
                    break;
                case "P":
                    #region Send a pre-defined function messagfe to the 5040
                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(this.CodeNumber);

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("P");									// Cmd Type
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);							// Function ID
                    if (this.StartFunction)																												// "1" = start "0" = stop 
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x01);
                    else
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x00);
                    #endregion
                    break;
                case "K":
                    #region Send a phone book Entry

                    bRet = System.Text.ASCIIEncoding.ASCII.GetBytes("K");									// Cmd Type
                    bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.iMobileID));
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);									// Mobile ID
                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[1]);										// Mobile ID

                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, this.SubCommandType);					// SubCmd

                    if (this.SubCommandType == "P")
                    {
                        #region Global Phonebook
                        // [CmdType][Mobile ID][SubCmd][phonebookID][PSS][Location][Number][NOS][Name][SBS][Spare]

                        bConvert = new byte[4];																											// phonebookID
                        bConvert = BitConverter.GetBytes(Convert.ToUInt32(this.PhoneBookID));
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                        iEntries = 0;
                        if (PhoneBookNumbers != null) if (PhoneBookNumbers.Count > iEntries) iEntries = PhoneBookNumbers.Count;
                        if (PhoneBookNames != null) if (PhoneBookNames.Count > iEntries) iEntries = PhoneBookNames.Count;


                        if (iEntries > 0)
                        {
                            for (int X = 0; X < iEntries; X++)
                            {
                                bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0A);											// PSS - Phone book separator

                                bConvert = new byte[4];																									// Location
                                bConvert = BitConverter.GetBytes(Convert.ToUInt32(X));
                                bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, bConvert[0]);

                                if (PhoneBookNumbers != null)
                                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, PhoneBookNumbers[X]);					// Phone Number
                                else
                                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, "0");

                                bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0B);											// PSS - Phone book separator

                                if (PhoneBookNames != null)
                                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, PhoneBookNames[X]);						// Entry Name
                                else
                                    bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, "N/A");
                            }
                        }
                        else
                        {
                            bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x0A);											// PSS - Phone book separator
                        }
                        bRet = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bRet, (byte)0x07);											// PSS - Phone book separator
                        #endregion
                    }
                    else
                        bRet = null;
                    #endregion
                    break;
                default:
                    bRet = null;
                    break;
            }
            return bRet;
        }

        #endregion

        #region Parse Incomming Packet
        public string ParseIncommingPacket(byte[] bMsg, string sUnitID)
        {
            string sRet = "";
            try
            {
                sRet = TranslateMsgToHost(bMsg, this, sUnitID);
            }
            catch (System.Exception ex)
            {
                if (_log != null)
                {
                    if (bMsg == null)
                        _log.Error("MTData.MDTInterface.c5040PacketData ParseIncommingPacket(byte[] bMsg = null, sUnitID = '" + sUnitID + "')", ex);
                    else
                        _log.Error("MTData.MDTInterface.c5040PacketData ParseIncommingPacket(byte[] bMsg = " + BitConverter.ToString(bMsg) + ", sUnitID = '" + sUnitID + "')", ex);
                }
            }
            return sRet;
        }

        private string TranslateMsgToHost(byte[] bMsg, c5040PacketData oPacketData, string sUnitID)
        {
            // NOTES :
            //
            // All packets from the unit (except Nack) start with [Cmd Type][GPS]
            //
            // All msgs to the host start with bSOP (0x02), 2 bytes of length and end with bEOP (0x03);
            //		i.e. [SOP][Length of Data (2 Bytes)][Data][EOP]
            //
            // [GPS]
            // All packets from the unit to the host include the following GPS information;
            // [LSC][Lat][Long][GPS Time][Speed][Heading] = 1 + 4 + 4 + 6 + 1 + 2 = 18 bytes
            // 
            // These 18 bytes can just be copied to the host packet as a block, no interpretation is required.
            //
            // [SBS] - Spare bytes seperator
            // Most of the packets have a SBS before the EOP byte.  This has been added to allow for expansion
            // without changing the protocol.

            this.bGenericData = bMsg;

            #region Constant Bytes
            //			byte bSucess = (byte) 0x00;
            //			byte bSOP = (byte) 0x02;
            //			byte bEOP = (byte) 0x03;
            byte bMDSC = (byte)0x06;
            byte bSBS = (byte)0x07;
            byte bICS = (byte)0x08;
            byte bRBCS = (byte)0x09;
            byte bBCS = (byte)0x0A;
            byte bSOS = (byte)0x0B;
            byte bEOBN = (byte)0x0C;
            #endregion

            #region Local Variables
            byte[] bFailCode = new byte[4];
            byte[] bMBPPacket = new byte[1];
            byte[] bStopID = new byte[4];
            byte[] bConvert = new byte[4];
            byte[] bSignatureData = null;
            byte[] bGPS = null;
            byte[] bSignatureLen = new byte[4];
            byte[] bFunctionID = new byte[1];
            byte[] bStartStop = new byte[1];
            byte[] bTime = null;
            UInt32 iJobID = 0;
            int iPosition = 0;
            int iPosOfBCS = 0;
            int iPosOfSBS = 0;
            int iPosOfEOBN = 0;
            int iPosOfRBSC = 0;
            int iPosOfICS = 0;
            int iPosOfSOS = 0;
            int iPosOfMDSC = 0;
            int iFindSigStart = 0;
            string sTemp = "";
            string strErrMsg = "";
            #endregion

            #region Valisdate Packet Length and Structure
            bConvert = BitConverter.GetBytes(0);

            // Make sure the unit ID is only 4 chars long.
            if (sUnitID.Length > 4)
                sUnitID = sUnitID.Substring(0, 4);

            sUnitID = sUnitID.PadLeft(4, '0');

            // Get the Cmd Type for the packet
            this.CommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
            // Get the unit ID 
            this.iMobileID = Convert.ToInt32(sUnitID);

            // If the Cmd Type <> N then get the 18 bytes of GPS info.
            if (this.CommandType != "N")
            {
                if (bMsg.Length < 20)
                {
                    // If the packet is too small, just return.
                    return strErrMsg;
                }
            }
            else
            {
                if (bMsg.Length < 9)
                {
                    // If the packet is too small, just return.
                    return strErrMsg;
                }
            }
            #endregion

            // If this packet has GPS data, then..
            if (bMsg.Length > 20)
            {
                if (CommandType == "I" || CommandType == "O")
                {
                    // A login packet has extra fields in the header...
                    // [CmdType][MobileID][FleetID][VehicleID][GPS][.....]
                    //					CmdType = �I� 1 char
                    //					Mobile ID = 2 bytes number
                    //					FleetID = 3021 Fleet ID (1 byte number 0 - 255)
                    //					VehicleID = 3021 Unit ID (2 byte number 0 - 65000)
                    //					GPS = See above.

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[3];
                    this.FleetID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[4];
                    bConvert[1] = bMsg[5];
                    this.VehicleID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    // Get the GPS Data
                    bGPS = GetSubBytes(bMsg, 6, 18);
                    this.GPSPosition = new cGPSData(bGPS);
                    // Trim off the Cmd Type and GPS info from the data.
                    bMsg = GetSubBytes(bMsg, 26, bMsg.Length);
                }
                else
                {
                    // Astandard packet starts with [CmdType][GPS][.....]

                    // Get the GPS Data
                    bGPS = GetSubBytes(bMsg, 1, 18);
                    this.GPSPosition = new cGPSData(bGPS);
                    // Trim off the Cmd Type and GPS info from the data.
                    bMsg = GetSubBytes(bMsg, 19, bMsg.Length);
                }
            }


            // bMsg is now the remaining bytes after the [CmdType][GPS]

            switch (this.CommandType)
            {
                case "N":
                    #region Nack / Ack
                    //[CmdType][FailCode][Message][Job ID][Stop #]
                    // Get the fail code.

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[1];
                    this.CodeNumber = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    // Get the Nack/Ack Message
                    this.Message = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 2, 4);
                    // Get the job as stop ids
                    if (this.Message.Substring(0, 2).ToUpper() == "OK")
                    {
                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 4));
                        // Get the stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[8];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    }
                    else
                    {
                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 6));
                        // Get the stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[10];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    }
                    break;
                    #endregion
                case "I":
                    #region Login Packet

                    for (int X = 0; X < bMsg.Length; X++)
                    {
                        if (bMsg[X] == (byte)0x0A) // If this byte is the USB byte
                        {
                            bConvert = new byte[X];
                            for (int Y = 0; Y < X; Y++)
                            {
                                bConvert[Y] = bMsg[Y];
                            }
                            this.Username = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);
                            // Set iPosition to the next byte after the USB byte.
                            iPosition = X + 1;
                            break;
                        }
                    }

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Password = System.Text.ASCIIEncoding.ASCII.GetString(bConvert);

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.PhonebookID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.MessagebookID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.DivisionReasonID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.DelayReasonID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    this.RejectedReasonID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[iPosition++];
                    bConvert[1] = bMsg[iPosition++];
                    bConvert[2] = bMsg[iPosition++];
                    bConvert[3] = bMsg[iPosition++];
                    this.Distance = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    break;
                    #endregion
                case "O":
                    #region Logout Packet
                    // [CmdType][GPS][Password]
                    this.Password = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, bMsg.Length);
                    break;
                    #endregion
                case "A":
                    #region KFC Job Accept Packet
                    //
                    // [CmdType][Job ID][GPS]
                    this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 0));
                    break;
                    #endregion
                case "X":
                    #region Job Reject Packet
                    //[CmdType][GPS][Job ID]

                    this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 0));
                    break;
                    #endregion
                case "S":
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    if (this.SubCommandType == "P")
                    {
                        #region KFC Pickup Packet
                        // [CmdType][GPS][SubCmd][Job ID][Stop #]

                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));
                        bConvert = new byte[4];
                        bStopID[0] = bMsg[5];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bStopID, 0));
                        #endregion
                    }
                    else if (this.SubCommandType == "D")
                    {
                        #region KFC Deliver Packet
                        //
                        // [CmdType][GPS][SubCmd][Job ID][Stop #][POD Name]
                        //		[BCS][Bar Code 1][BCS] � [Bar Code x][BCS][SOS][Signature]

                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));
                        bStopID[0] = bMsg[5];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bStopID, 0));

                        #region Extract the delivery Data and Signature.
                        // Convert the rest of the packet to a string for parsing
                        sTemp = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 6, bMsg.Length - 6);
                        // Find the first occurance of BSC, start at char 1
                        iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                        if (iPosOfBCS >= 0)
                        {
                            if (iPosOfBCS > 0)
                                this.PODName = sTemp.Substring(0, iPosOfBCS);
                            // Remove POD name from the front of the string
                            sTemp = TruncateString(sTemp, iPosOfBCS);
                            // Check for barcodes
                            iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                            iPosOfSOS = FindFirstIndexOfByte(sTemp, bSOS);
                            if (iPosOfSOS >= 0)
                            {
                                if (iPosOfSOS < iPosOfBCS)
                                    iPosOfBCS = -1;
                            }

                            this.BarCodes = new StringCollection();
                            while (iPosOfBCS >= 0)
                            {
                                if (iPosOfBCS > 0)
                                {
                                    this.BarCodes.Add(sTemp.Substring(0, iPosOfBCS));
                                }

                                sTemp = TruncateString(sTemp, iPosOfBCS);
                                iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                                iPosOfSOS = FindFirstIndexOfByte(sTemp, bSOS);
                                if (iPosOfSOS >= 0)
                                {
                                    if (iPosOfSOS < iPosOfBCS)
                                        iPosOfBCS = -1;
                                }
                            }

                            // Get the signature, it runs to the end of the packet.
                            if (iPosOfSOS >= 0)
                            {
                                for (iFindSigStart = 7; iFindSigStart < bMsg.Length; iFindSigStart++)
                                {
                                    if (bMsg[iFindSigStart] == (byte)0x0B)
                                    {
                                        // Get the signature data
                                        bSignatureData = GetSubBytes(bMsg, iFindSigStart, bMsg.Length - iFindSigStart);
                                        if (_sigConfig.LogSignatureData && (_log != null))
                                            _log.Info("Converting Adhoc Signature for Unit : " + sUnitID + ", Job : " + iJobID + ", Data : " + ConvertToAscii(bSignatureData) + "\n");
                                        // Create a new conversion object
                                        cConvertSignature oConv = new cConvertSignature(_sigConfig);
                                        // Translate the data into a GIF and return the image as bytes
                                        bSignatureData = oConv.TranslateSignature(bSignatureData);
                                        // Get the length of bytes.
                                        bSignatureLen = BitConverter.GetBytes(bSignatureData.Length);
                                        if (_sigConfig.LogSignatureData && (_log != null))
                                            _log.Info("Converted Adhoc Signature for Unit : " + sUnitID + ", Job : " + iJobID + "\n");
                                        break;
                                    }
                                }
                            }
                        }
                        #endregion

                        if (bSignatureData == null)
                        {
                            if (_sigConfig.LogSignatureData)
                                strErrMsg += "Couldn't find the signature for : " + sUnitID + ", Job : " + iJobID + ", Data : " + ConvertToAscii(bSignatureData) + "\n";
                        }
                        this.SignatureData = bSignatureData;
                        #endregion
                    }
                    break;
                case "C":
                    #region Complete KFC Job
                    // [CmdType][GPS][Job ID][Success Code]

                    this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[5];
                    this.CodeNumber = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                    break;
                    #endregion
                case "R":
                    #region Request Job Packet
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);
                    #endregion
                    break;
                case "P":
                    #region Start / Stop a predefined function packet.
                    // [CmdType][GPS][Function ID][Start / Stop]

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[0];
                    this.CodeNumber = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                    bConvert = new byte[4];
                    bConvert[0] = bMsg[1];
                    if (Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0)) == 0)
                        this.StartFunction = false;
                    else
                        this.StartFunction = true;
                    break;
                    #endregion
                case "B":
                    //------------------------------------ Nettrak packets -----------------------------------------------------
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);

                    if (this.SubCommandType == "G")
                    {
                        #region  Send Generic Data packet
                        // [CmdType][GPS][Sub Cmd][200 bytes]			
                        //this.bGenericData = GetSubBytes(bMsg, 1, bMsg.Length);
                        #endregion
                    }
                    else if (this.SubCommandType == "S")
                    {
                        #region  Start a Nettrack Job Packet
                        // [CmdType][GPS][Sub Cmd][JobID][Time][SBS][Spare]

                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));

                        bTime = new byte[4];
                        bTime[0] = bMsg[5];
                        bTime[1] = bMsg[6];
                        bTime[2] = bMsg[7];
                        bTime[3] = bMsg[8];

                        this.JobTime = BitConverter.ToString(bTime);

                        #endregion
                    }
                    else if (this.SubCommandType == "P")
                    {
                        #region  Nettrack Pickup Packet
                        //
                        // [CmdType][GPS][Sub Cmd][JobID][BCS][Bar Code 1][BCS] � [Bar Code x][BCS][SBS][Spare]
                        // to
                        // [CmdType][Mobile ID][Sub Cmd][JobID][BCS][Bar Code 1][BCS] � [Bar Code x][BCS][GPS][SBS][Spare]

                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[5];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                        if (bMsg[6] != bBCS)
                        {
                            // If the next character isn't start of barcode...
                            return strErrMsg;
                        }

                        #region Extract the barcodes
                        // Convert the bytes to a string for parsing
                        sTemp = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 6, bMsg.Length - 6);

                        // Find the first occurance of [BSC] (0A) and [SBS] (07)
                        iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                        iPosOfSBS = FindFirstIndexOfByte(sTemp, bSBS);

                        // If the [SBS] is encountered before the [BSC] then
                        // there are no barcodes in the packet.
                        if (iPosOfSBS >= 0 && iPosOfBCS >= 0)
                        {
                            if (iPosOfSBS < iPosOfBCS)
                                iPosOfBCS = -1;
                        }

                        // Create a new collection for the barcodes.
                        this.BarCodes = new StringCollection();
                        if (iPosOfBCS >= 0)
                        {
                            while (iPosOfBCS >= 0)
                            {
                                if (iPosOfBCS > 0)
                                {
                                    this.BarCodes.Add(sTemp.Substring(0, iPosOfBCS));
                                }

                                sTemp = TruncateString(sTemp, iPosOfBCS);
                                iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                                iPosOfSBS = FindFirstIndexOfByte(sTemp, bSBS);
                                if (iPosOfSBS >= 0)
                                {
                                    if (iPosOfSBS < iPosOfBCS)
                                        iPosOfBCS = -1;
                                }
                            }
                        }
                        #endregion
                        #endregion
                    }
                    else if (this.SubCommandType == "D")
                    {
                        #region  The NetTrack 'Pre-Delivery' Packet
                        //	[CmdType][GPS][Sub Cmd][JobID][StopID][Suburb ID][Business Name][SBS][Spare]

                        // Extract the Job ID
                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));

                        // Extract the Stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[5];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                        // Extract the Suburb ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[6];
                        bConvert[1] = bMsg[7];
                        this.SuburbID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                        // Extract the Business name
                        this.BusinessName = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 8, bMsg.Length - 8);
                        iPosOfEOBN = FindFirstIndexOfByte(this.BusinessName, bEOBN);
                        if (iPosOfEOBN >= 0)
                        {
                            this.BusinessName = this.BusinessName.Substring(0, iPosOfEOBN);
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "C")
                    {
                        #region  The Nettrack Complete Job Packet
                        // [CmdType][GPS][Sub Cmd][JobID][Time in Mins][SBS][Spare]

                        byte[] bTimeInMins = new byte[3];

                        // Extract the Job ID
                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));

                        // Extract the Stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[5];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                        // Extract the 3 char 'Time in Mins' field
                        bTimeInMins[0] = bMsg[7];
                        bTimeInMins[1] = bMsg[8];
                        bTimeInMins[2] = bMsg[9];
                        this.JobTime = BitConverter.ToString(bTimeInMins, 0);
                        #endregion
                    }
                    else if (this.SubCommandType == "O")
                    {
                        #region  The Nettrack Deliver Packet (POD Name, Barcodes, Return Barcodes, Invoice and Sig)
                        // [CmdType][GPS][SubCmd][Job ID][Stop #][POD Name][BCS][Bar Code 1][BCS] � [Bar Code x][BCS][RBCS]...[Bar Code x][RBCS]
                        //		[ICS][Invoice Number][ICS][SOS][Signature][SBS][Spare]

                        // Extract the Job ID
                        this.JobID = Convert.ToInt32(BitConverter.ToUInt32(bMsg, 1));

                        // Extract the Stop ID
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[5];
                        this.StopID = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));

                        // Extract the POD Name
                        sTemp = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 6, bMsg.Length - 6);
                        iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                        if (iPosOfBCS >= 0)
                        {
                            // Get the POD Name
                            this.PODName = sTemp.Substring(0, iPosOfBCS);
                            sTemp = TruncateString(sTemp, iPosOfBCS);
                        }

                        // Find the first occurance of [BSC], [RBCS], [ICS], [SOS] and the last occurance of [SBS]
                        iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                        iPosOfRBSC = FindFirstIndexOfByte(sTemp, bRBCS);
                        iPosOfICS = FindFirstIndexOfByte(sTemp, bICS);
                        iPosOfSBS = FindLastIndexOfByte(sTemp, bSBS);

                        #region Extract the Bar Codes
                        // This section determines what is and what isn't in the packet
                        if (iPosOfRBSC >= 0 && iPosOfBCS >= 0)
                        {
                            if (iPosOfRBSC < iPosOfBCS)
                            {
                                iPosOfBCS = -1;
                            }
                        }


                        // Create a new collection for the barcodes.
                        this.BarCodes = new StringCollection();
                        if (iPosOfBCS >= 0)
                        {
                            while (iPosOfBCS >= 0)
                            {
                                if (iPosOfBCS > 0)
                                {
                                    this.BarCodes.Add(sTemp.Substring(0, iPosOfBCS));
                                }

                                sTemp = TruncateString(sTemp, iPosOfBCS);
                                iPosOfBCS = FindFirstIndexOfByte(sTemp, bBCS);
                                iPosOfRBSC = FindFirstIndexOfByte(sTemp, bRBCS);
                                if (iPosOfRBSC >= 0 && iPosOfBCS >= 0)
                                {
                                    if (iPosOfRBSC < iPosOfBCS)
                                    {
                                        iPosOfBCS = -1;
                                    }
                                }
                            }
                        }

                        #endregion

                        iPosOfRBSC = FindFirstIndexOfByte(sTemp, bRBCS);
                        iPosOfICS = FindFirstIndexOfByte(sTemp, bICS);
                        if (iPosOfICS >= 0 && iPosOfRBSC >= 0)
                        {
                            if (iPosOfICS < iPosOfRBSC)
                            {
                                iPosOfRBSC = -1;
                                sTemp = TruncateString(sTemp, iPosOfICS);
                            }
                        }

                        #region Extract the Return Bar Codes.
                        // Create a new collection for the return barcodes.
                        this.ReturnBarCodes = new StringCollection();
                        if (iPosOfRBSC >= 0)
                        {
                            iPosOfRBSC = FindFirstIndexOfByte(sTemp, bRBCS);
                            while (iPosOfRBSC >= 0)
                            {
                                if (iPosOfRBSC > 0)
                                {
                                    this.ReturnBarCodes.Add(sTemp.Substring(0, iPosOfRBSC));
                                }

                                sTemp = TruncateString(sTemp, iPosOfRBSC);
                                iPosOfRBSC = FindFirstIndexOfByte(sTemp, bRBCS);
                                iPosOfICS = FindFirstIndexOfByte(sTemp, bICS);

                                if (iPosOfICS >= 0 && iPosOfRBSC >= 0)
                                {
                                    if (iPosOfICS < iPosOfRBSC)
                                    {
                                        iPosOfRBSC = -1;
                                        sTemp = TruncateString(sTemp, iPosOfICS);
                                    }
                                }
                            }
                        }
                        #endregion

                        #region Extract the Invoice number
                        // Get the invoice number
                        iPosOfICS = FindFirstIndexOfByte(sTemp, bICS);
                        iPosOfSOS = FindFirstIndexOfByte(sTemp, bSOS);
                        if (iPosOfICS >= 0 && iPosOfSOS >= 0)
                        {
                            if (iPosOfSOS < iPosOfICS)
                                iPosOfICS = -1;
                        }
                        // Find the next ICS character
                        if (iPosOfICS > 0)
                        {
                            this.InvoiceNumber = sTemp.Substring(0, iPosOfICS);
                        }
                        #endregion

                        #region Extract the signature data.
                        // Find the start of the signature.
                        for (iFindSigStart = 8; iFindSigStart < bMsg.Length; iFindSigStart++)
                        {
                            if (bMsg[iFindSigStart] == (byte)0x0B)
                            {
                                // Get the signature data
                                bSignatureData = GetSubBytes(bMsg, iFindSigStart, bMsg.Length - iFindSigStart);
                                if (_sigConfig.LogSignatureData && (_log != null))
                                    _log.Info("Converting Adhoc Signature for Unit : " + sUnitID + ", Job : " + iJobID + ", Data : " + ConvertToAscii(bSignatureData) + "\n");
                                // Create a new conversion object
                                cConvertSignature oConv = new cConvertSignature(_sigConfig);
                                // Translate the data into a GIF and return the image as bytes
                                bSignatureData = oConv.TranslateSignature(bSignatureData);
                                if (_sigConfig.LogSignatureData && (_log != null))
                                    _log.Info("Converted Nettrack Signature for Unit : " + sUnitID + ", Job : " + iJobID + "\n");
                                break;
                            }
                        }

                        // Get the length of bytes.
                        if (bSignatureData == null)
                        {
                            Console.Write("Error converting signature.");
                            bSignatureData = new byte[1];
                        }
                        this.SignatureData = bSignatureData;
                        #endregion
                        #endregion
                    }
                    break;
                case "M":
                    this.SubCommandType = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 0, 1);

                    if (this.SubCommandType == "F")
                    {
                        #region Free-form Message from the Driver
                        // [CmdType][SubCmd][Message Data][MDSC][GPS][SBS][Spare]

                        sTemp = System.Text.ASCIIEncoding.ASCII.GetString(bMsg, 1, bMsg.Length - 1);

                        iPosOfMDSC = FindLastIndexOfByte(sTemp, bMDSC);
                        if (iPosOfMDSC >= 0)
                        {
                            this.Message = sTemp.Substring(0, iPosOfMDSC);
                        }
                        #endregion
                    }
                    else if (this.SubCommandType == "P")
                    {
                        #region A Pre-defined message from the driver.
                        // [CmdType][SubCmd][Message ID][GPS][SBS][Spare]
                        bConvert = new byte[4];
                        bConvert[0] = bMsg[1];
                        this.CodeNumber = Convert.ToInt32(BitConverter.ToUInt32(bConvert, 0));
                        #endregion
                    }
                    break;
                default:
                    return strErrMsg;
            }
            return strErrMsg;
        }
        #endregion

        #region GetPacketTypeName
        public string GetPacketTypeName()
        {
            string sRet = "";

            switch (CommandType)
            {
                case "N":
                    sRet = "Interface Ack Packet";
                    break;
                case "O":
                    sRet = "Login Packet";
                    break;
                case "A":
                    sRet = "Accept Job Packet";
                    break;
                case "X":
                    sRet = "Reject Job Packet";
                    break;
                case "S":
                    sRet = GetAdhocPacketType();
                    break;
                case "C":
                    sRet = "Complete Job Packet";
                    break;
                case "R":
                    sRet = GetRequestPacketType();
                    break;
                case "P":
                    sRet = "Pre-defined Function Packet";
                    break;
                case "B":
                    sRet = GetNettrackPacketType();
                    break;
                case "M":
                    sRet = "Message Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetAdhocPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "P":
                    sRet = "Adhoc Pickup  Packet";
                    break;
                case "D":
                    sRet = "Adhoc Delivery Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetRequestPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "J":
                    sRet = "Request Job Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }

        private string GetNettrackPacketType()
        {
            string sRet = "";
            switch (SubCommandType)
            {
                case "G":
                    sRet = "Nettrack Generic Data Packet";
                    break;
                case "S":
                    sRet = "Nettrack Start Job Packet";
                    break;
                case "P":
                    sRet = "Nettrack Picjup Packet";
                    break;
                case "D":
                    sRet = "Nettrack Pre-Delivery Packet";
                    break;
                case "C":
                    sRet = "Nettrack Complete Job Packet";
                    break;
                case "O":
                    sRet = "Nettrack Delivery Packet";
                    break;
                default:
                    sRet = "";
                    break;
            }
            return sRet;
        }
        #endregion

        #region Private Functions
        private static byte[] CreateBadPacketNak()
        {
            byte[] bPacketData = new byte[1];
            byte[] bPacket = new byte[1];
            byte[] bPacketLen = null;
            byte[] bCmdType = null;

            bCmdType = System.Text.ASCIIEncoding.ASCII.GetBytes("N");

            bPacketData[0] = bCmdType[0];
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, Convert.ToInt32((byte)0x00));
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, "4");
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x00);
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x07);
            bPacketData = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacketData, (byte)0x03);

            bPacketLen = BitConverter.GetBytes(Convert.ToUInt32(bPacketData.Length));
            bPacket = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacket, (byte)bPacketLen[0]);
            bPacket = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacket, (byte)bPacketLen[1]);
            bPacket = MTData.Common.Utilities.PacketUtilities.AppendToPacket(bPacket, bPacketData);
            return bPacket;
        }

        private static byte[] GetSubBytes(byte[] bMsg, int iStartAt, int iNumOfBytes)
        {
            byte[] bResult = null;
            int iResultCount = 0;

            if (bMsg == null) return null;

            if (bMsg.Length >= (iStartAt + iNumOfBytes))
            {

                bResult = new byte[iNumOfBytes];

                for (int X = iStartAt; X < iStartAt + iNumOfBytes; X++)
                {
                    bResult[iResultCount] = bMsg[X];
                    iResultCount++;
                }
                return bResult;
            }
            else
            {
                iNumOfBytes = bMsg.Length;
                bResult = new byte[iNumOfBytes - iStartAt];
                for (int X = iStartAt; X < iNumOfBytes; X++)
                {
                    bResult[iResultCount] = bMsg[X];
                    iResultCount++;
                }
                return bResult;
            }
        }

        private static string RemoveLeadingReturnChars(string strData)
        {
            string strTemp = strData.Trim();

            if (strTemp.Length > 0)
            {
                while (strTemp[0] == '\r' || strTemp[0] == '\n')
                {
                    if (strTemp.Length == 1)
                    {
                        strTemp = "";
                        break;
                    }
                    else
                    {
                        strTemp = strTemp.Substring(1, strTemp.Length - 1);
                    }
                }
            }
            return strTemp;
        }

        private static string WordWrapString(string strDescLines, int iLineLength)
        {
            char[] cDesc = strDescLines.ToCharArray(0, strDescLines.Length);
            string sResult = "";
            string sLine = "";
            int iCurrentPos = 1;

            if (cDesc.Length > 0)
            {
                for (int X = 0; X < cDesc.Length; X++)
                {
                    // For each char in the array
                    if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                    {
                        if (iCurrentPos > 0)
                        {
                            sResult += sLine + "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                        else
                        {
                            sResult += "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                    }
                    else
                    {
                        if (iCurrentPos > iLineLength)
                        {
                            int iLastSpace = sLine.LastIndexOf(' ');
                            if (iLastSpace > 0)
                            {
                                sResult += sLine.Substring(0, iLastSpace) + "\n";
                                sLine = sLine.Substring(iLastSpace + 1).Trim();
                                iCurrentPos = sLine.Length;
                            }
                            else
                            {
                                sResult += sLine + "\n";
                                sLine = "";
                                iCurrentPos = 0;
                            }
                            if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                            {
                                if (iCurrentPos > 0)
                                {
                                    sResult += sLine + "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                                else
                                {
                                    sResult += "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                            }
                            else
                            {
                                if (iCurrentPos == 0 && cDesc[X] == ' ')
                                {
                                    // Don't add white space to the front of the line.
                                }
                                else
                                {
                                    sLine += cDesc[X];
                                    iCurrentPos++;
                                }
                            }
                        }
                        else
                        {
                            if (iCurrentPos == 0 && cDesc[X] == ' ')
                            {
                                // Don't add white space to the front of the line.

                            }
                            else
                            {
                                sLine += cDesc[X];
                                iCurrentPos++;
                            }
                        }
                    }
                }
            }

            if (sLine.Length > 0)
                sResult += sLine;

            return sResult;
        }

        public static string ConvertToAscii(byte[] bPacket)
        {
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
        {
            string sRet = "";
            int X = 0;
            //			byte bLower = 0x21;	// 33
            //			byte bUpper = 0x7E;  // 126
            byte bTest = 0x00;  // null
            byte[] bTestArray = new byte[1];

            if (bPacket.Length > 0)
            {
                try
                {
                    for (X = 0; X < bPacket.Length; X++)
                    {
                        bTest = bPacket[X];
                        bTestArray = new byte[1];
                        bTestArray[0] = bTest;
                        if (bTest >= 33 && bTest <= 126)  // If the character is in printable range
                            if (bAddSpacing)
                                if (sRet.Length > 0)
                                    sRet += " - " + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                                else
                                    sRet = System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                            else
                                sRet += System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
                        else  // Show it as a number
                            if (bAddSpacing)
                                if (sRet.Length > 0)
                                    sRet += " - [" + bTest + "]";
                                else
                                    sRet = "[" + Convert.ToString(bTest) + "]";
                            else
                                sRet += "[" + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray) + "]";
                    }

                }
                catch (System.Exception ex)
                {
                    Console.Write(ex.Message + "\n");
                }
            }
            return sRet;
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind)
        {
            return FindFirstIndexOfByte(sData, bFind, 0, sData.Length);
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt)
        {
            return FindFirstIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
        }

        private static int FindFirstIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
        {
            int iRet = -1;

            try
            {
                iRet = sData.IndexOf((char)bFind + "", iStartAt, iLength);
            }
            catch (System.Exception)
            {
                return -1;
            }
            return iRet;
        }

        private static int FindLastIndexOfByte(string sData, byte bFind)
        {
            return FindLastIndexOfByte(sData, bFind, sData.Length, sData.Length);
        }

        private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt)
        {
            return FindLastIndexOfByte(sData, bFind, iStartAt, sData.Length - iStartAt);
        }

        private static int FindLastIndexOfByte(string sData, byte bFind, int iStartAt, int iLength)
        {
            int iRet = -1;

            try
            {
                iRet = sData.LastIndexOf((char)bFind + "", iStartAt, iLength);
            }
            catch (System.Exception)
            {
                return -1;
            }
            return iRet;
        }


        private static string TruncateString(string sData, int iPos)
        {
            string sRet = "";

            try
            {
                if (sData.Length < iPos + 1)
                    return "";
                else
                    sRet = sData.Substring((iPos + 1), sData.Length - (iPos + 1));
            }
            catch (System.Exception)
            {
                return "";
            }
            return sRet;
        }
        #endregion
    }
}

﻿Build Date	: 10/10/2006
Version		: 5.6.0

First Release entries
=====================
5.6.0 is the final development release.  This document will track changes between versions.  
The code is based on our standard interfacing, therefore we start with the version of the base code (v5.6)
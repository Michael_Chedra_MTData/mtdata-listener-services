using System;
using System.Collections;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Common.Config;
using MTData.Common.Network;
using MTData.Common.Queues;
using MTData.Common.Threading;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// Summary description for Gen5040ClientHandler.
	/// </summary>
	public class Gen5040ClientHandler
	{
		private object SyncRoot = new object();														// To make this object thread safe.
		private GenConfigReader _config = null;													// Configuration options
        private ILog _log = LogManager.GetLogger(typeof(Gen5040ClientHandler));													// The logging interface.
		private TCPServer _mccServer = null;														// The TCP listener for client connection
		private Hashtable _fromRemoteIPQs = null;												// The client incomming queues
		private Hashtable _toRemoteIPQs = null;													// The client outgoing queues
		private ArrayList _mccConnectionKeys = null;											// A list of hashtable keys for the above two tables.
		private StandardQueue _fromRemoteIPQ = null;										// The queue to pass a message from the interface to one/many client
		private StandardQueue _toRemoteIPQ = null;											// The queue to pass messages from the clients to the interface.
		private EnhancedThread _thMonitorIncomming;										// The thread to monitor the incomming queues
		private EnhancedThread _thMonitorOutgoing;											// The thread to distribute outgoing messages.

		public Gen5040ClientHandler(StandardQueue InterfaceInboundQueue, StandardQueue InterfaceOutboundQueue, GenConfigReader configuration)
		{
			// Setup the logging 
			_config = configuration;
			_fromRemoteIPQ = InterfaceOutboundQueue;
			_toRemoteIPQ = InterfaceInboundQueue;
			_toRemoteIPQs = new Hashtable();
			_mccConnectionKeys = new ArrayList();


			// Start listening for a connection from MCC
			TCPServer.InboundConnectionHandler clientHandler = new TCPServer.InboundConnectionHandler(MCC_HandleInboundConnection);
			_mccServer = new TCPServer("0.0.0.0", _config.MCCPort, "Client Connections", clientHandler);
			_mccServer.Start();

			_log.Info("Client Handler Ready.");
		}

		#region Thread Handling
		public void Stop()
		{
			if(_thMonitorIncomming != null) _thMonitorIncomming.Stop();
			if(_thMonitorIncomming != null) _thMonitorOutgoing.Stop();
		}

		public void Start()
		{
			_thMonitorIncomming = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(IncommingThread), null);
			_thMonitorIncomming.Name = "Monitor Client Incomming Messages";
			_thMonitorIncomming.Start();

			_thMonitorOutgoing = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(OutgoingThread), null);
			_thMonitorOutgoing.Name = "Monitor Client Outgoing Messages";
			_thMonitorOutgoing.Start();
		}

		private object IncommingThread(EnhancedThread sender, object data)
		{
/*
			// This thread checks all the connection queues in the _fromRemoteIPQs object.
			// If the queues contain data, then it dequeues the data and queues ot on the _fromRemoteIPQ for the interface.
			int X = 0;
			int iKey = 0;
			Queue oTempQ = new Queue();
			StandardQueue oClientQ = null;
			ArrayList oKeys = null;
			bool bItemsQueued = false;

			// Whilst the parent object is active
			while(!sender.Stopping)
			{
				lock(_mccConnectionKeys.SyncRoot)
				{
					oKeys = _mccConnectionKeys.Clone();
				}
					// For each key in the list
				for (X = 0; X < oKeys.Count; X++)
				{
					iKey = (int) oKeys[X];
					// if there is an entry in the _fromRemoteIPQs hashtable for this key
					if(_fromRemoteIPQs.ContainsKey(iKey))
					{
						lock(_fromRemoteIPQs.SyncRoot)
						{
							if(((StandardQueue) _fromRemoteIPQs[iKey]).Count > 0)
							{
								oClientQ = (StandardQueue) _fromRemoteIPQs[iKey];
							}
							else
							{
								oClientQ = null;
							}
						}
						// Check if there is anything in the incomming queue
						if(oClientQ != null)
						{
							// Pull one message from the queue, queue it for the interface and move on
							byte[] bData = null;
							lock(oClientQ.SyncRoot)
							{
								bData = (byte[]) oClientQ.Dequeue();
							}
							// Package the message for the interface.
							cClientData oData = new cClientData(iKey, bData);
							lock(_fromRemoteIPQ.SyncRoot)
							{
								_fromRemoteIPQ.Enqueue(oData);
							}
						}
					}
					
					Thread.Sleep(10);
				}
			}
			*/
			return null;
		}

		private object OutgoingThread(EnhancedThread sender, object data)
		{
			/*
			// This thread checks the _toRemoteIPQ object for queued data.  The queue consists 
			// If the queues contain data, then it dequeues the data and queues ot on the _fromRemoteIPQ for the interface.

			cClientData oData = null;
			StandardQueue oClientQ = null;
			ArrayList oKeys = null;
			int iKey = 0;

			// Whilst the parent object is active
			while(!sender.Stopping)
			{
				lock(_toRemoteIPQ.SyncRoot)
				{
					if (_toRemoteIPQ.Count > 0)
					{
						oData = (cClientData) _toRemoteIPQ.Dequeue();
					}
				}

				if (oData != null)
				{
					if (oData.Key > 0)
					{
						// Send to a specific client.
						lock(_toRemoteIPQs.SyncRoot)
						{
							if (_toRemoteIPQs.ContainsKey(oData.Key))
							{
								oClientQ = (StandardQueue) _toRemoteIPQs[iKey];
								if(oClientQ != null)
								{
									oClientQ.Enqueue(oData.bData);
								}
								oClientQ = null;
							}
						}
					}
					else
					{
						// Send to all clients
						lock(_mccConnectionKeys.SyncRoot)
						{
							oKeys = _mccConnectionKeys.Clone();
						}

						// For each key in the list
						for (X = 0; X < oKeys.Count; X++)
						{
							iKey = (int) oKeys[X];
							// if there is an entry in the _fromRemoteIPQs hashtable for this key
							lock(_toRemoteIPQs.SyncRoot)
							{
								if(_toRemoteIPQs.ContainsKey(iKey))
								{
									oClientQ = (StandardQueue) _toRemoteIPQs[iKey];
									if(oClientQ != null)
									{
										oClientQ.Enqueue(oData.bData);
									}
									oClientQ = null;			
								}
							}
						}
					}

				Thread.Sleep(10);
				oData = null;
			}
			*/
			return null;
		}

		#endregion

		#region Connection Handling
		public void MCC_HandleInboundConnection(TcpListener listener)
		{
			// Create the new connection queues
			StandardQueue _newfromRemoteIPQ = new StandardQueue();
			StandardQueue _newtoRemoteIPQ = new StandardQueue();

			// Accept the connection
			TcpClient client = listener.AcceptTcpClient();

			// Create a new TCP connection and bind the in and out queues for a client connection
			// pass _fromRemoteIPQ (shared queue) to pass messages back to the interface, create a _newRemoteIPQ for outgoing client messages.
			TCPConnection connection = new TCPConnection(client, "MCCConnection", _newfromRemoteIPQ, _newtoRemoteIPQ, new TCPConnection.ConnectionClosedHandler(mcc_connectionclosed), _config.PacketSize, _config.PacketWaitPeriod, _config.SleepWaitPeriod);
		
			// Add it to the list of connections.
			int iHashCode = connection.HashCode;
			lock(this.SyncRoot)
			{
				// Add the incomming comms to the table of incomming comms
				lock(_fromRemoteIPQs.SyncRoot)
					_fromRemoteIPQs.Add(iHashCode, _newfromRemoteIPQ);
				// Add the outgoing comms to the table of outgoing comms
				lock(_toRemoteIPQs.SyncRoot)
					_toRemoteIPQs.Add(iHashCode, _newtoRemoteIPQ);
				// Add the key to a list of hashtable keys
				lock(_mccConnectionKeys.SyncRoot)
				_mccConnectionKeys.Add(iHashCode);
			}
		}

		public void mcc_connectionclosed(TCPConnection sender)
		{
			_log.Info("Client (Port : " + Convert.ToString(sender.Port) + ") has disconnected.");

			// Remove the connections from the list of connections.
			int iHashCode = sender.HashCode;

			lock(this.SyncRoot)
			{
				if(_fromRemoteIPQs.ContainsKey(iHashCode))
					lock(_fromRemoteIPQs.SyncRoot)
						_fromRemoteIPQs.Remove(iHashCode);

				if(_toRemoteIPQs.ContainsKey(iHashCode))
					lock(_toRemoteIPQs.SyncRoot)
						_toRemoteIPQs.Remove(iHashCode);

				if(_mccConnectionKeys.Contains(iHashCode))
					lock(_mccConnectionKeys.SyncRoot)
						_mccConnectionKeys.Remove(iHashCode);
			}
		}
		#endregion
	}
}

using System;

namespace MTData.MDT.Interface
{
	/// <summary>
	/// Summary description for cWPData.
	/// </summary>
	public class cWPData
	{
		public int iLatitude = 0;
		public int iLongitude = 0;
		public int iRadius = 0;
		public string sPointName = "";

		public cWPData(double Latitude, double Longitude, int Radius, string PointName)
		{
			iLatitude = Convert.ToInt32(Latitude * Convert.ToDouble(600000));
			iLongitude = Convert.ToInt32(Longitude * Convert.ToDouble(600000));
			iRadius = Radius;
			sPointName = PointName;
		}
	}
}

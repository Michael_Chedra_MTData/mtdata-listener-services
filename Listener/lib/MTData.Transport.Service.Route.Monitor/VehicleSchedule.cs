using System;

namespace MTData.Transport.Service.Route.Monitor
{
	/// <summary>
	/// Summary description for VehicleSchedule.
	/// </summary>
	public class VehicleSchedule
	{
		private int		_id;
		private Vehicle	_vehicle;
		private int		_downloadedRouteScheduleId;

		public VehicleSchedule(int id, Vehicle vehicle, int downloadedRouteScheduleId)
		{
			_id							= id;
			_vehicle					= vehicle;
			_downloadedRouteScheduleId	= downloadedRouteScheduleId;
		}

		public int Id
		{
			get { return _id; }
		}

		public Vehicle Vehicle
		{
			get { return _vehicle; }
		}

		public int DownloadedRouteScheduleId
		{
			get { return _downloadedRouteScheduleId; }
		}

		public static object GetUniqueKey(Vehicle vehicle, int routeScheduleId)
		{
			return string.Format("{0}.{1}", vehicle.UniqueKey(), routeScheduleId);
		}

		public object UniqueKey
		{
			get { return GetUniqueKey(_vehicle, _downloadedRouteScheduleId); }
		}
	}
}

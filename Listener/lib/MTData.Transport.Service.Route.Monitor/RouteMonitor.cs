using System;
using System.Collections;
using System.Data;
using System.Text;
using System.Threading;
using MTData.Common.Utilities;
using log4net;
using MTData.Transport.Service.Route.Data;
using MTData.Transport.Gateway.Packet;
//using MTData.Transport.Plugins.WebService.Route;
using MTData.Common.Plugin.Configuration;

namespace MTData.Transport.Service.Route.Monitor
{
    /// <summary>
    /// Class to provide multi-threaded Route Monitoring for Vehicles assigned to Route Schedules.
    /// A single instance can process multiple thread requests simultaneously.  It will raise events
    /// when Route events occur such as OffRoute, OnRoute, RouteAttached and RouteDetached.
    /// </summary>
    public class RouteMonitor
    {
        private const string sClassName = "MTData.Transport.Route.RouteMonitor";
        private static ILog _log = LogManager.GetLogger(typeof(RouteMonitor));
        public delegate void VehicleDelegate(RouteMonitor routeMonitor, Vehicle vehicle, object context);

        public event VehicleDelegate OffRoute;
        public event VehicleDelegate OnRoute;
        public event VehicleDelegate DetachedRoute;
        public event VehicleDelegate AttachedRoute;

        private Hashtable _vehicleSchedulesById = new Hashtable();
        private Hashtable _vehicleSchedulesByVehicleAndScheduleId = new Hashtable();
        private object _vehicleSchedulesSync = new object();

        private Hashtable _vehicles = new Hashtable();
        private object _vehiclesSync = new object();

        private Hashtable _cachedRoutesSchedulesById = new Hashtable();
        private object _cachedRoutesSchedulesSync = new object();

        private bool _flushing = false;
        private object _flushingSync = new object();
        private TimeSpan _flushUnusedRouteTimeSpan = new TimeSpan(1, 0, 0, 0);
        private DateTime _lastFlushTimeUTC = DateTime.MinValue;


        private string _dsn;

        public RouteMonitor(string dsn)
        {
            _dsn = dsn;

            // Get all Vehicles and load initial On/Off Route state
            ReloadAllVehiclesNow();
        }

        private void ReloadAllVehiclesNow()
        {
            lock (_vehiclesSync)
            {
                _vehicles = new Hashtable();
                /*
                 * 			AdministrationVehicle administrationVehicle = new AdministrationVehicle(_dsn);
                            DataSetVehicleNotification dataSetVehicle = administrationVehicle.
                            foreach(DataSetVehicle.T_VehicleRow vehicleRow in dataSetVehicle.T_Vehicle)
                            {
                                Vehicle vehicle = vehicleRow.
                            }
                */
            }
        }

        public bool ProcessMessage(object context, double latitude, double longitude, int vehicleId, int fleetId, int vehicleScheduleId)
        {
            try
            {
                Vehicle vehicle = GetVehicle(vehicleId, fleetId);
                if (vehicle != null)
                {
                    if (vehicleScheduleId != 0)
                    {
                        VehicleSchedule vehicleSchedule = GetVehicleSchedule(vehicleScheduleId);
                        if (vehicleSchedule == null)
                            _log.Info("Route Processing - Fleet/Vehicle " + Convert.ToString(fleetId) + "/" + Convert.ToString(vehicleId) + " : Could not find VehicleSchedule " + vehicleScheduleId.ToString());
                        else
                        {
                            RouteSchedule routeSchedule = GetRouteSchedule(vehicleSchedule.DownloadedRouteScheduleId);
                            return vehicle.Update(context, latitude, longitude, routeSchedule);
                        }
                    }
                    else
                    {
                        return vehicle.Update(context, latitude, longitude, null);
                    }
                }
            }
            catch (Exception ex)
            {
                if (context is GatewayProtocolPacket && context == null)
                {
                    GatewayProtocolPacket packet = (context as GatewayProtocolPacket);
                    string sRawBytes = PacketUtilities.ConvertToAscii(packet.mRawBytes);
                    _log.Error(sClassName + "XXX() - " + string.Format("ProcessMessage Fleet={0} Vehicle={1} VehicleSchedule = {2} RawByte = ", fleetId, vehicleId, vehicleScheduleId, sRawBytes), ex);

                }
                else
                {
                    _log.Error(sClassName + "ProcessMessage(object context, double latitude = " + latitude + ", double longitude = " + longitude + ", int vehicleId = " + vehicleId + ", int fleetId = " + fleetId + ", int vehicleScheduleId = " + vehicleScheduleId + ")", ex);
                }
            }
            finally
            {
                if (IsFlushUnusedRoutesDue())
                    FlushUnusedRoutes();
            }
            return false;
        }

        public string GetRouteNameFromVehicleScheduleID(int vehicleScheduleID)
        {
            string result = null;
            VehicleSchedule vehicleSchedule = GetVehicleSchedule(vehicleScheduleID);
            if (vehicleSchedule != null)
            {
                RouteSchedule routeSchedule = GetRouteSchedule(vehicleSchedule.DownloadedRouteScheduleId);
                if ((routeSchedule != null) && (routeSchedule.Route != null))
                    result = routeSchedule.Route.Name;
            }
            return result;
        }

        public int GetCloneIDFromFromVehicleScheduleID(int vehicleScheduleID)
        {
            int result = 0;
            VehicleSchedule vehicleSchedule = GetVehicleSchedule(vehicleScheduleID);
            if (vehicleSchedule != null)
                result = vehicleSchedule.DownloadedRouteScheduleId;
            return result;
        }


        private VehicleSchedule GetVehicleSchedule(int vehicleScheduleId)
        {
            VehicleSchedule vehicleSchedule = (VehicleSchedule)_vehicleSchedulesById[vehicleScheduleId];
            if (vehicleSchedule == null)
            {
                lock (_vehicleSchedulesSync)
                {
                    vehicleSchedule = (VehicleSchedule)_vehicleSchedulesById[vehicleScheduleId];
                    if (vehicleSchedule == null)
                    {
                        using (VehicleScheduleBiz biz = new VehicleScheduleBiz(_dsn))
                        {
                            PluginMethodResult result = biz.GetVehicleSchedule(vehicleScheduleId);
                            if ((result != null) && (result.DataSet != null))
                            {
                                DataSetVehicleSchedule ds = result.DataSet.Convert<DataSetVehicleSchedule>(); // DataSetVehicleScheduleHelper.Convert(result.DataSet);
                                if (ds.T_VehicleSchedule.Count > 0)
                                {
                                    DataSetVehicleSchedule.T_VehicleScheduleRow row = ds.T_VehicleSchedule[0];

                                    Vehicle vehicle = GetVehicle(row.VehicleID, row.FleetID);
                                    vehicleSchedule = new VehicleSchedule(row.ID, vehicle, row.DownloadedRouteScheduleID);

                                    _vehicleSchedulesById[vehicleScheduleId] = vehicleSchedule;
                                    _vehicleSchedulesByVehicleAndScheduleId[vehicleSchedule.UniqueKey] = vehicleSchedule;
                                }
                            }
                        }
                    }
                }
            }
            return vehicleSchedule;
        }

        private Vehicle GetVehicle(int vehicleId, int fleetId)
        {
            object key = Vehicle.GetUniqueKey(vehicleId, fleetId);
            Vehicle vehicle = (Vehicle)_vehicles[key];
            if (vehicle == null)
            {
                lock (_vehiclesSync)
                {
                    vehicle = (Vehicle)_vehicles[key];
                    if (vehicle == null)
                    {
                        vehicle = new Vehicle(vehicleId, fleetId, this);
                        _vehicles[key] = vehicle;
                    }
                }
            }
            return vehicle;
        }

        #region Raise Event methods
        internal void RaiseVehicleBackOnRoute(Vehicle vehicle, object context)
        {
            try
            {
                if (OnRoute != null)
                    OnRoute(this, vehicle, context);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "RaiseVehicleBackOnRoute(Vehicle vehicle, object context)", ex);
            }

        }

        internal void RaiseVehicleGoneOffRoute(Vehicle vehicle, object context)
        {
            try
            {
                if (OffRoute != null)
                    OffRoute(this, vehicle, context);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "RaiseVehicleGoneOffRoute(Vehicle vehicle, object context)", ex);
            }
        }

        internal void RaiseVehicleDetachedRoute(Vehicle vehicle, object context)
        {
            // Since vehicle has Detached from a Route we can flush the VehicleSchedule its on
            FlushVehicleScheduleBySchedule(vehicle);

            try
            {
                if (DetachedRoute != null)
                    DetachedRoute(this, vehicle, context);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "RaiseVehicleDetachedRoute(Vehicle vehicle, object context)", ex);
            }
        }

        /// <summary>
        /// Remove the VehicleSchedule associated with the Vehicle
        /// </summary>
        /// <param name="vehicle">The Vehicle to flush for</param>
        private void FlushVehicleScheduleBySchedule(Vehicle vehicle)
        {
            object vehicleAndScheduleKey = VehicleSchedule.GetUniqueKey(vehicle, vehicle.RouteSchedule.RouteScheduleId);
            VehicleSchedule vehicleSchedule = (VehicleSchedule)_vehicleSchedulesByVehicleAndScheduleId[vehicleAndScheduleKey];
            if (vehicleSchedule != null)
            {
                lock (_vehicleSchedulesSync)
                {
                    _vehicleSchedulesByVehicleAndScheduleId.Remove(vehicleAndScheduleKey);
                    _vehicleSchedulesById.Remove(vehicleSchedule.Id);
                }
            }
        }

        internal void RaiseVehicleAttachedRoute(Vehicle vehicle, object context)
        {
            try
            {
                if (AttachedRoute != null)
                    AttachedRoute(this, vehicle, context);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "RaiseVehicleAttachedRoute(Vehicle vehicle, object context)", ex);
            }
        }
        #endregion

        /// <summary>
        /// Thread Safe get a Route Schedule
        /// </summary>
        /// <param name="routeScheduleId">Route Schedule to retrieve</param>
        /// <returns></returns>
        internal RouteSchedule GetRouteSchedule(int routeScheduleId)
        {
            if (routeScheduleId < 0)
                return null;

            RouteSchedule routeSchedule = null;
            try
            {
                object key = RouteSchedule.GetUniqueKey(routeScheduleId);
                routeSchedule = (RouteSchedule)_cachedRoutesSchedulesById[key];
                if (routeSchedule == null)
                {
                    lock (_cachedRoutesSchedulesSync)
                    {
                        routeSchedule = (RouteSchedule)_cachedRoutesSchedulesById[key];
                        if (routeSchedule == null)
                        {
                            // Schedule not in cache, get the Schedule and Route now
                            using (Route2Biz route2Biz = new Route2Biz(_dsn))
                            {
                                DataSet ds = route2Biz.GetRoute2BySchedule(routeScheduleId);

                                if (ds != null)
                                {
                                    DataSetRoute2 dataSetRoute2 = ds.Convert<DataSetRoute2>();
                                    Route route = new Route(dataSetRoute2);

                                    foreach (RouteSchedule cachingRouteSchedule in route.Schedules)
                                        _cachedRoutesSchedulesById[cachingRouteSchedule.UniqueKey] = cachingRouteSchedule;

                                    routeSchedule = (RouteSchedule)_cachedRoutesSchedulesById[key];
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "GetRouteSchedule(int routeScheduleId = " + routeScheduleId + ")", ex);
            }
            return routeSchedule;
        }

        #region Route Flushing
        private bool IsFlushUnusedRoutesDue()
        {
            return DateTime.UtcNow.Subtract(_lastFlushTimeUTC) > _flushUnusedRouteTimeSpan;
        }


        /// <summary>
        /// Thread safe FlushUnusedRoutes.  If not already Flushing then will Flush now.
        /// </summary>
        private void FlushUnusedRoutes()
        {
            // Make sure only 1 thread at a time tries to Flush Unused Routes ..
            if (!_flushing)
            {
                lock (_flushingSync)
                {
                    if (!_flushing)
                    {
                        _flushing = true;
                        try
                        {
                            UnsafeFlushUnusedRoutes();
                        }
                        finally
                        {
                            _flushing = false;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Thread Unsafe Flush Unused Routes.  Will flush any Routes that have not been used for a period of time.
        /// </summary>
        private void UnsafeFlushUnusedRoutes()
        {
            try
            {
                _lastFlushTimeUTC = DateTime.UtcNow;

                // Gather list of Routes which have not been used recently
                ArrayList flushSchedules = new ArrayList();
                foreach (RouteSchedule routeSchedule in _cachedRoutesSchedulesById.Values)
                {
                    if ((routeSchedule.ReferenceCount == 0) &&
                        (_lastFlushTimeUTC.Subtract(routeSchedule.LastTimeUsedUTC) > _flushUnusedRouteTimeSpan))
                        flushSchedules.Add(routeSchedule);
                }

                // Flush any Routes which were found to be unused
                if (flushSchedules.Count > 0)
                {
                    lock (_cachedRoutesSchedulesSync)
                    {
                        foreach (RouteSchedule routeSchedule in flushSchedules)
                        {
                            _cachedRoutesSchedulesById.Remove(routeSchedule.UniqueKey);

                            routeSchedule.Dispose();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "UnsafeFlushUnusedRoutes()", ex);
            }
        }
        #endregion
    }
}

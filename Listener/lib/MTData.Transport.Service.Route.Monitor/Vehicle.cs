using System;

namespace MTData.Transport.Service.Route.Monitor
{
	/// <summary>
	/// Summary description for Vehicle.
	/// </summary>
	public class Vehicle
	{
		private	RouteMonitor		_routeMonitor;
		private bool				_offRoute;
		private int					_vehicleId;
		private int					_fleetId;
		private RouteSchedule		_routeSchedule;

		public Vehicle(int vehicleId, int fleetId, RouteMonitor routeMonitor)
		{
			_vehicleId				= vehicleId;
			_fleetId				= fleetId;
			_routeMonitor	= routeMonitor;
		}

		public static object GetUniqueKey(int vehicleId, int fleetId)
		{
			return vehicleId.ToString() + "." + fleetId.ToString();
		}

		public object UniqueKey()
		{
			return GetUniqueKey(_vehicleId, _fleetId);
		}

		public bool OffRoute
		{
			get { return _offRoute; }
		}

		public int VehicleId
		{
			get { return _vehicleId; }
		}

		public int FleetId
		{
			get { return _fleetId; }
		}

		public RouteSchedule RouteSchedule
		{
			get { return _routeSchedule; }
		}

		public RouteMonitor RouteMonitor
		{
			get { return _routeMonitor; }
		}

		public bool Update(object context, double latitude, double longitude, RouteSchedule routeSchedule)
		{
			if (_routeSchedule != routeSchedule)
			{
				if (_routeSchedule != null)
				{
					_routeMonitor.RaiseVehicleDetachedRoute(this, context);
					_routeSchedule.RemoveRef();
				}

				_routeSchedule = routeSchedule;

				if (_routeSchedule != null)
				{
					_routeSchedule.AddRef();
					_routeMonitor.RaiseVehicleAttachedRoute(this, context);
				}
			}

			if (_routeSchedule != null)
			{
				bool vehicleIfOffRoute		= _routeSchedule.IsOffRoute(latitude, longitude);
				if (vehicleIfOffRoute)
				{
					if (!_offRoute)
						_routeMonitor.RaiseVehicleGoneOffRoute(this, context);
				}
				else
				{
					if (_offRoute)
						_routeMonitor.RaiseVehicleBackOnRoute(this, context);
				}
				_offRoute = vehicleIfOffRoute;
			}
			else
				_offRoute = false;
			return _offRoute;
		}

	}
}

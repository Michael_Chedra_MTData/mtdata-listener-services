using System;
using MTData.Transport.Service.Route.Data;

namespace MTData.Transport.Service.Route.Monitor
{
    /// <summary>
    /// Summary description for RouteSchedule.
    /// </summary>
    public class RouteSchedule : IDisposable
    {
        private Route _route;
        private DataSetRoute2.T_Route2ScheduleRow _scheduleRow;

        public RouteSchedule(Route route, DataSetRoute2.T_Route2ScheduleRow scheduleRow)
        {
            _route = route;
            _scheduleRow = scheduleRow;
        }

        public long AddRef()
        {
            return _route.AddRef();
        }

        public long RemoveRef()
        {
            return _route.RemoveRef();
        }

        public long ReferenceCount { get { return _route.ReferenceCount; } }

        public Route Route
        {
            get { return _route; }
        }

        public int RouteScheduleId
        {
            get { return _scheduleRow.ID; }
        }

        public string Name
        {
            get { return _scheduleRow.Name; }
        }

        public static string GetUniqueKey(int routeScheduleId)
        {
            return routeScheduleId.ToString();
        }

        public string UniqueKey
        {
            get { return GetUniqueKey(_scheduleRow.ID); }
        }

        public void Dispose()
        {
            if (_route != null)
                _route.Dispose();
            _route = null;
            _scheduleRow = null;
        }

        public DateTime LastTimeUsedUTC
        {
            get { return _route.LastTimeUsedUTC; }
        }

        public bool IsOffRoute(double latitude, double longitude)
        {
            return _route.IsOffRoute(latitude, longitude);
        }
    }
}

using System;
using System.Collections;
using MTData.Common.Mathematics;
using MTData.Transport.Service.Route.Data;

namespace MTData.Transport.Service.Route.Monitor
{
	/// <summary>
	/// Summary description for Route.
	/// </summary>
	public class Route : IDisposable
	{
		private DataSetRoute2.T_Route2Row		_routeRow;
		private PolyLine2D						_routePointPolyLine;
		private DateTime						_lastTimeUsedUTC;
		private EarthSphere						_earth;
		private double							_offRouteToleranceMetres;
		private Hashtable						_routeSchedules;
		private bool							_disposed;
		private long _referenceCount = 0;

		public Route(DataSetRoute2 routeDataSet)
		{
			_routeRow					= routeDataSet.T_Route2[0];
			_offRouteToleranceMetres	= _routeRow.Tolerance;

			_earth						= new EarthSphere();
			_routePointPolyLine			= new PolyLine2D();
			foreach(DataSetRoute2.T_Route2PointRow pointRow in routeDataSet.T_Route2Point)
				_routePointPolyLine.Add(Sphere.DegreesToRadians(pointRow.Longitude), Sphere.DegreesToRadians(pointRow.Latitude));

			_routeSchedules = new Hashtable();
			foreach(DataSetRoute2.T_Route2ScheduleRow scheduleRow in routeDataSet.T_Route2Schedule)
			{
				RouteSchedule routeSchedule					= new RouteSchedule(this, scheduleRow);
				_routeSchedules[routeSchedule.UniqueKey]	= routeSchedule;
			}
		}

		public long AddRef()
		{
			return System.Threading.Interlocked.Increment(ref _referenceCount);
		}

		public long RemoveRef()
		{
			return System.Threading.Interlocked.Decrement(ref _referenceCount);
		}

		public long ReferenceCount { get{ return _referenceCount; }}

		public string Name
		{
			get 
			{
				_lastTimeUsedUTC = DateTime.UtcNow;
				return _routeRow.Name; 
			}
		}

		public bool IsOffRoute(double latitude, double longitude)
		{
			_lastTimeUsedUTC = DateTime.UtcNow;
			return !_earth.IsClose(new Point2D(Sphere.DegreesToRadians(longitude), Sphere.DegreesToRadians(latitude)), _offRouteToleranceMetres, _routePointPolyLine);
		}

		public DateTime LastTimeUsedUTC
		{
			get { return _lastTimeUsedUTC; }
		}

		public void Dispose()
		{
			if (_disposed)
				return;
			_disposed = true;

			_routeRow			= null;
			_routePointPolyLine	= null;
			_earth				= null;
		}

		public RouteSchedule GetSchedule(int routeScheduleId)
		{
			return (RouteSchedule) _routeSchedules[RouteSchedule.GetUniqueKey(routeScheduleId)];
		}

		public IEnumerable Schedules
		{
			get { return _routeSchedules.Values; }
		}

	}
}

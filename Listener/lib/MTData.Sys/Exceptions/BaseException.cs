using System;

namespace MTData.Sys.Exceptions
{
	/// <summary>
	/// This is the exception class that all MTData System Exceptions will drive from.
	/// </summary>
#if !NETCF
	[Serializable]
#endif
	public class BaseException : ApplicationException
	{
#if !NETCF
		/// <summary>
		/// Prepare the class form a serialized version of the object
		/// </summary>
		/// <param name="info">Serialization Informaiton</param>
		/// <param name="context">Context in which object was serialized</param>
		public BaseException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
		{
			
		}
#endif		
		/// <summary>
		/// Prepare the exception class given a base exception that has further detail
		/// </summary>
		/// <param name="message">Message text to be included in the exception</param>
		/// <param name="baseException">Base exception that this exception was triggered by</param>
		public BaseException(string message, System.Exception baseException) : base(message, baseException)
		{
			
		}
		
		/// <summary>
		/// Prepare the exception class given a message of the error.
		/// </summary>
		/// <param name="message">Message text to be included in the exception</param>
		public BaseException(string message) : base(message)
		{
			
		}

		/// <summary>
		/// Prepare an empty instance of the exception class 
		/// </summary>
		public BaseException() : base()
		{
			
		}
	}
}

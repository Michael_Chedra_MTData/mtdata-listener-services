#if !NETCF && !NETCF2
using System;
using System.Collections;
using System.Drawing;

namespace MTData.Sys.Math
{
	/// <summary>
	/// Summary description for PolyLine2D.
	/// </summary>
	public class PolyLine2D : IEnumerable, IPoint2DProvider
	{
		public class IsCloseProvider : IIsCloseProvider
		{
			private double _distanceSqrd;

			public IsCloseProvider(double distance)
			{
				_distanceSqrd = distance * distance;
			}

			public bool IsClose(Point2D a, Point2D b)
			{
				return (b - a).LengthSqrd <= _distanceSqrd;
			}
		}

		private ArrayList	_pts = new ArrayList();

		public PolyLine2D()
		{
		}

		public Point2D this[int index]
		{
			get { return (Point2D) _pts[index]; }
			set { _pts[index] = value; }
		}

		public Point2D Add(Point2D pt)
		{
			_pts.Add(pt);
			return pt;
		}

		public Point2D Add(double X, double Y)
		{
			Point2D pt = new Point2D(X, Y);
			_pts.Add(pt);
			return pt;
		}

		public void AddRange(Point2D[] range)
		{
			_pts.AddRange(range);
		}

		public Point2D Insert(int before, Point2D pt)
		{
			_pts.Insert(before, pt);
			return pt;
		}

		public void RemoveAt(int index)
		{
			_pts.RemoveAt(index);
		}

		public int Count
		{
			get { return _pts.Count; }
		}

		public IEnumerator GetEnumerator()
		{
			return _pts.GetEnumerator();
		}

		public PolyLine2D Widen(double width)
		{
			PolyLine2D result = new PolyLine2D();
			if (_pts.Count >= 2)
			{				
				width /= 2.0;			// Width is either side of the lines

				double tA, tB;
				ArrayList	newP_1Pts = new ArrayList();
				ArrayList	newP_2Pts = new ArrayList();

				Point2D p1_1;
				Point2D p1_2;
				
				// First Point is an EndPoint
				WidenEndPoint((Point2D)_pts[0], (Point2D)_pts[1], width, out p1_1, out p1_2);

				newP_1Pts.Add(p1_1);
				newP_2Pts.Add(p1_2);

				for(int i = 1 ; i < _pts.Count - 1; i++)
				{
					Point2D p0 = (Point2D) _pts[i - 1];
					Point2D p1 = (Point2D) _pts[i];
					Point2D p2 = (Point2D) _pts[i + 1];
					
					Vector2D p1p2 = p2 - p1;
					Vector2D p0p1 = p1 - p0;

					// n is dividing vector, normalised
					Vector2D n1 = p1p2.GetUnitVector() + p0p1.GetUnitVector();
					Vector2D n2 = n1 / 2.0;
					Vector2D n = n2.GetNormal();

					p1_1 = new Point2D();
					p1_1.IntersectAt((Point2D)newP_1Pts[i - 1], p0p1, p1, n, out tA, out tB);
					newP_1Pts.Add(p1_1);

					p1_2 = new Point2D();
					p1_2.IntersectAt((Point2D)newP_2Pts[i - 1], p0p1, p1, -n, out tA, out tB);
					newP_2Pts.Add(p1_2);
				}

				// Last Point is an EndPoint
				WidenEndPoint((Point2D)_pts[_pts.Count - 1], (Point2D)_pts[_pts.Count - 2], width, out p1_2, out p1_1);	// Intentional swap since endpoint is reversed

				newP_1Pts.Add(p1_1);
				newP_2Pts.Add(p1_2);

				Point2D[] p1Pts = (Point2D[]) newP_1Pts.ToArray(typeof(Point2D));
				newP_2Pts.Reverse();
				Point2D[] p2Pts = (Point2D[]) newP_2Pts.ToArray(typeof(Point2D));

				result.AddRange(p1Pts);
				result.AddRange(p2Pts);
				result.Add(p1Pts[0]);
			}
			return result;
		}

		private void WidenEndPoint(Point2D p1, Point2D p2, double width, out Point2D p1_1, out Point2D p1_2)
		{
			Vector2D p1p2	= p2 - p1;
			Vector2D n		= p1p2.GetNormal().GetUnitVector();

			p1_1 = p1 + width * n;
			p1_2 = p1 - width * n;
		}

		public static explicit operator Point[] (PolyLine2D polyLine)
		{
			Point[] result = new Point[polyLine.Count];	
			for(int i = 0 ; i < result.Length ; i++)
				result[i] = polyLine[i].ToPoint();
			return result;
		}

		public static bool IsClose(Point2D testPt, IPoint2DProvider ptProvider, IIsCloseProvider isCloseProvider)
		{
			int ptCount = ptProvider.Count;

			// Check closest to each Vector
			Point2D intersect = new Point2D();
			for(int i = 0 ; i < ptCount - 1 ; i++)
			{
				Point2D p0		= ptProvider[i];
				Point2D p1		= ptProvider[i + 1];

				Vector2D p0p1	= p1 - p0;
				Vector2D n		= p0p1.GetNormal().GetUnitVector();

				double tA, tB;
				intersect.IntersectAt(p0, p0p1, testPt, n, out tA, out tB);

				if ((tA >= 0) && (tA <= 1))
				{
					// Quick test first
					if (isCloseProvider.IsClose(testPt, intersect))
						return true;
				}
			}

			// Check closest to each Point
			for(int i = 0 ; i < ptCount ; i++)
			{
				if (isCloseProvider.IsClose(testPt, ptProvider[i]))
					return true;
			}
			return false;
		}

		public bool IsClose(double x, double y, double distance)
		{
			return IsClose(new Point2D(x, y), this, new IsCloseProvider(distance));
		}

		public bool IsClose(Point2D testPt, double distance)
		{
			return IsClose(testPt, this, new IsCloseProvider(distance));
		}

		public static int[] GetRedundantPointIndices(IPoint2DProvider ptProvider, double distance)
		{
			ArrayList indices = new ArrayList();
			int count = ptProvider.Count;
			if (ptProvider.Count > 1)
			{
				Point2D ptA = null;
				Point2D ptB = null;
				Point2D ptC = null;
				Point2D intersectionPt = new Point2D();
				double tA, tB;

				for(int i = 0 ; i < count ; i++)
				{
					bool advancePtA = true;

					ptC = ptProvider[i];

					if (ptA != null)
					{
						// Check if pb is close to the vector pc - pa
						Vector2D v = (ptC - ptA).GetUnitVector();
						intersectionPt.IntersectAt(ptA, v, ptB, v.GetNormal(), out tA, out tB);

						if (System.Math.Abs(tB) <= distance)
						{
							indices.Add(i - 1);
							advancePtA = false;
						}
					}

					if (advancePtA)
						ptA = ptB;
					ptB = ptC;
				}
			}
			return (int[]) indices.ToArray(typeof(int));
		}
	}
}
#endif
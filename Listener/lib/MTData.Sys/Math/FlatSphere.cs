#if !NETCF && !NETCF2
using System;

namespace MTData.Sys.Math
{
	/// <summary>
	/// Summary description for FlatEarth.
	/// </summary>
	public class FlatSphere : IIsCloseProvider
	{
		private double _distanceSqrd;

		public FlatSphere(double distance) 
		{
			_distanceSqrd = distance * distance;
		}

		public bool IsClose(Point2D a, Point2D b)
		{
			return (b - a).LengthSqrd <= _distanceSqrd;
		}
	}

	public class FlatEarth : FlatSphere
	{
		public const double DegreesToMetres = 1852 * 60;
		public const double MetresToDegrees = 1.0 / DegreesToMetres;
		public const double	MetresToRadians = MetresToDegrees * System.Math.PI / 180.0;
		public const double	RadiansToMetres = 1.0 / MetresToRadians;

		public FlatEarth(double distanceMetres) : base(MetresToRadians * distanceMetres)
		{
		}
	}
}
#endif
#if !NETCF && !NETCF2
using System;
using System.Diagnostics;

namespace MTData.Sys.Math
{
	/// <summary>
	/// Summary description for Sphere.
	/// </summary>
	public class Sphere 
	{
		public class IsCloseProvider : IIsCloseProvider
		{
			private Sphere	_sphere;
			private double	_distance;

			protected internal IsCloseProvider(Sphere sphere, double distance)
			{
				_sphere = sphere;
				_distance = distance;
			}

			public bool IsClose(Point2D a, Point2D b)
			{
				return _sphere.CalculateDistance(a, b) <= _distance;
			}
		}

		private double _radius;

		public static double DegreesToRadians(double degrees)
		{
			return degrees * System.Math.PI / 180.0;
		}

		public static Point2D DegreesToRadians(Point2D degrees)
		{
			return new Point2D(DegreesToRadians(degrees.X), Sphere.DegreesToRadians(degrees.Y));
		}

		public Sphere(double radius)
		{
			_radius = radius;
		}

		public double CalculateDistance(Point2D p0, Point2D p1)
		{
			double deltaLat					= p1.Y - p0.Y;
			double deltaLon					= p1.X - p0.X;
			double sinDeltaLatOn2			= System.Math.Sin(deltaLat / 2);
			double sinDeltaLatOn2Sqrd		= sinDeltaLatOn2 * sinDeltaLatOn2;
			double sinDeltaLonOn2			= System.Math.Sin(deltaLon / 2);
			double sinDeltaLonOn2Sqrd		= sinDeltaLonOn2 * sinDeltaLonOn2;
			double a						= sinDeltaLatOn2Sqrd + System.Math.Cos(p0.Y) * System.Math.Cos(p1.Y) * sinDeltaLonOn2Sqrd;
			double c						= 2 * System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));
			return c * _radius;
		}

		public double CalculateDistanceSqrd(Point2D a, Point2D b)
		{
			double distance = CalculateDistance(a, b);
			return distance * distance;
		}

		public IIsCloseProvider CreateIsCloseProvider(double distance)
		{
			return new IsCloseProvider(this, distance);
		}


		public bool IsClose(Point2D p0, double distance, IPoint2DProvider ptProvider)
		{
			int ptCount = ptProvider.Count;

			double cosTestPtY		= System.Math.Cos(p0.Y);
			double distanceOnRadius = (distance / (_radius * 2));

			// Check closest to each Vector
			Point2D intersect = new Point2D();
			for(int i = 0 ; i < ptCount ; i++)
			{
				Point2D p1		= ptProvider[i];

				// Test to see if in range of p0
				double deltaLat					= p1.Y - p0.Y;
				double deltaLon					= p1.X - p0.X;
				double sinDeltaLatOn2			= System.Math.Sin(deltaLat / 2);
				double sinDeltaLatOn2Sqrd		= sinDeltaLatOn2 * sinDeltaLatOn2;
				double sinDeltaLonOn2			= System.Math.Sin(deltaLon / 2);
				double sinDeltaLonOn2Sqrd		= sinDeltaLonOn2 * sinDeltaLonOn2;
				double a						= sinDeltaLatOn2Sqrd + cosTestPtY * System.Math.Cos(p1.Y) * sinDeltaLonOn2Sqrd;
				double c						= System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));
				if (c < distanceOnRadius)
					return true;

				if (i < ptCount - 1)
				{
					// Not in range of p0, get next point and see if in range of vector p1 - p0
					Point2D p2		= ptProvider[i + 1];

					Vector2D p2p1	= p2 - p1;
					Vector2D n		= p2p1.GetNormal().GetUnitVector();

					double tA, tB;
					intersect.IntersectAt(p1, p2p1, p0, n, out tA, out tB);

					if ((tA >= 0) && (tA <= 1))
					{
						deltaLat				= intersect.Y - p0.Y;
						deltaLon				= intersect.X - p0.X;
						sinDeltaLatOn2			= System.Math.Sin(deltaLat / 2);
						sinDeltaLatOn2Sqrd		= sinDeltaLatOn2 * sinDeltaLatOn2;
						sinDeltaLonOn2			= System.Math.Sin(deltaLon / 2);
						sinDeltaLonOn2Sqrd		= sinDeltaLonOn2 * sinDeltaLonOn2;
						a						= sinDeltaLatOn2Sqrd + cosTestPtY * System.Math.Cos(intersect.Y) * sinDeltaLonOn2Sqrd;
						c						= System.Math.Atan2(System.Math.Sqrt(a), System.Math.Sqrt(1 - a));
						if (c < distanceOnRadius)
							return true;
					}
				}
			}

			return false;

		}
	}

	public class EarthSphere : Sphere
	{
		public const double EarthRadiusMetres  = 6371000;
		
		public EarthSphere() : base(EarthRadiusMetres)
		{
		}
	}
}
#endif
#if !NETCF && !NETCF2
using System;
using System.Drawing;

namespace MTData.Sys.Math
{
	/// <summary>
	/// 2 dimensional Vector representation.
	/// </summary>
	public class Vector2D
	{
		/// <summary>
		/// X & Y components of the Vector
		/// </summary>
		public double	X;
		public double	Y;

		public Vector2D()
		{
		}

		public Vector2D(double x, double y)
		{
			X = x;
			Y = y;
		}

		public Vector2D(Vector2D copy)
		{
			X = copy.X;
			Y = copy.Y;
		}

		public Vector2D(Point pt)
		{
			X = pt.X;
			Y = pt.Y;
		}

		public Vector2D(PointF pt)
		{
			X = pt.X;
			Y = pt.Y;
		}

		/// <summary>
		/// Create an array of Vector2D instances from an array of Point's
		/// </summary>
		/// <param name="points">Points to create from</param>
		/// <returns>Array of Vector2D</returns>
		public static Vector2D[] CreateFrom(Point[] points)
		{
			Vector2D[] result = new Vector2D[points.Length];
			for(int i = 0 ; i < result.Length ; i++)
				result[i] = new Vector2D(points[i]);
			return result;
		}

		/// <summary>
		/// Create an array of Vector2D instances from an array of PointF's
		/// </summary>
		/// <param name="points">PointF's to create from</param>
		/// <returns>Array of Vector2D</returns>
		public static Vector2D[] CreateFrom(PointF[] points)
		{
			Vector2D[] result = new Vector2D[points.Length];
			for(int i = 0 ; i < result.Length ; i++)
				result[i] = new Vector2D(points[i]);
			return result;
		}

		/// <summary>
		/// Equality test against another Vector2D. Compares for equality on both X & Y components
		/// </summary>
		/// <param name="obj">Object to compare against. Anything but a Vector2D will return False</param>
		/// <returns>True if Compare has exact same X & Y components</returns>
		public override bool Equals(object obj)
		{
			Vector2D compare = obj as Vector2D;
			if (compare == null)
				return false;

			return (compare.X == X) && (compare.Y == Y);
		}

		/// <summary>
		/// Return HashCode of the Vector
		/// </summary>
		/// <returns></returns>
		public override int GetHashCode()
		{
			return X.GetHashCode() + Y.GetHashCode();
		}

		/// <summary>
		/// Add one Vector to another
		/// </summary>
		/// <param name="a">Vector A</param>
		/// <param name="b">Vector B</param>
		/// <returns>A new Vector with X & Y components equal the sum of Vector A and Vector B</returns>
		public static Vector2D operator+ (Vector2D a, Vector2D b)
		{
			return new Vector2D(a.X + b.X, a.Y + b.Y);
		}

		/// <summary>
		/// Subtract one Vector from another
		/// </summary>
		/// <param name="a">Vector A</param>
		/// <param name="b">Vector B</param>
		/// <returns>A new Vector with X & Y components equal the difference between Vector A and Vector B</returns>
		public static Vector2D operator- (Vector2D a, Vector2D b)
		{
			return new Vector2D(a.X - b.X, a.Y - b.Y);
		}

		/// <summary>
		/// Scale a Vector's X & Y components
		/// </summary>
		/// <param name="vector">Vector to scale</param>
		/// <param name="scale">Scaling factor</param>
		/// <returns>A new Vector with X & Y components scaled by Scale</returns>
		public static Vector2D operator* (Vector2D vector, double scale)
		{
			return new Vector2D(vector.X * scale, vector.Y * scale);
		}

		/// <summary>
		/// Scale a Vector's X & Y components
		/// </summary>
		/// <param name="vector">Vector to scale</param>
		/// <param name="scale">Scaling factor</param>
		/// <returns>A new Vector with X & Y components scaled by Scale</returns>
		public static Vector2D operator* (double scale, Vector2D vector)
		{
			return new Vector2D(vector.X * scale, vector.Y * scale);
		}

		/// <summary>
		/// Scale a Vector's X & Y components
		/// </summary>
		/// <param name="vector">Vector to scale</param>
		/// <param name="scale">Scaling factor</param>
		/// <returns>A new Vector with X & Y components scaled by Scale</returns>
		public static Vector2D operator/ (Vector2D a, double scale)
		{
			return new Vector2D(a.X / scale, a.Y / scale);
		}

		/// <summary>
		/// Negate a Vector
		/// </summary>
		/// <param name="a">Vector to negate</param>
		/// <returns>A new Vector with X & Y components negated</returns>
		public static Vector2D operator- (Vector2D a)
		{
			return new Vector2D(-a.X, -a.Y);
		}

		/// <summary>
		/// Calculate the Dot Product of two Vectors
		/// </summary>
		/// <param name="a">Vector A</param>
		/// <param name="b">Vector B</param>
		/// <returns>The Dot Product of the two Vectors</returns>
		public static double operator*(Vector2D a, Vector2D b)
		{
			return a.X * b.X + a.Y * b.Y;
		}

		/// <summary>
		/// Calculate the Angle between two Vectors
		/// </summary>
		/// <param name="a">Vector A</param>
		/// <param name="b">Vector B</param>
		/// <returns>Angle in radians between the two Vectors</returns>
		public static double operator%(Vector2D a, Vector2D b)
		{
			double theta = a.GetUnitVector() * b.GetUnitVector();
			return System.Math.Acos(theta);
		}

		/// <summary>
		/// Get the Length of the Vector
		/// </summary>
		public double Length
		{
			get { return System.Math.Sqrt(X * X + Y * Y); }
		}

		/// <summary>
		/// Get the Length Squared of the Vector
		/// </summary>
		public double LengthSqrd
		{
			get { return X * X + Y * Y; }			
		}

		/// <summary>
		/// Normalise this instance such that it equals its own Unit Vector
		/// </summary>
		public void Normalise()
		{
			double length = Length;
			X /= length;
			Y /= length;
		}

		/// <summary>
		/// Get the Unit Vector of this Vector
		/// </summary>
		/// <returns>A Vector which has the same Direction but a Length of 1</returns>
		public Vector2D GetUnitVector()
		{
			double length = Length;
			return new Vector2D(X / length, Y / length);
		}

		/// <summary>
		/// Get a Vector Normal (Perpendicular) to this Vector
		/// </summary>
		/// <returns>The Normal vector to this Vector.  Returned Normal is not a Unit Vector</returns>
		public Vector2D GetNormal()
		{
			return new Vector2D(-Y, X);
		}

		/// <summary>
		/// Calculate the Angle between this Vector and another
		/// </summary>
		/// <param name="vector">Vector to compare Angle</param>
		/// <returns>Angle in radians between this instance and the other</returns>
		public double AngleBetween(Vector2D vector)
		{
			return this % vector;
		}

		/// <summary>
		/// Project this Vector onto 'onto' returning a new Vector that has the same direction
		/// as 'onto' but a Length equal to the projection.  If vector's are perpendicular then returned
		/// Vector will have a length of 0.
		/// </summary>
		/// <param name="onto">Vector to project onto</param>
		/// <returns>Projected Vector</returns>
		public Vector2D Project(Vector2D onto)
		{
			return new Vector2D(((this * onto) / onto.LengthSqrd) * onto);			
		}

		/// <summary>
		/// Convert to a Point2D
		/// </summary>
		/// <returns></returns>
		public Point2D ToPoint2D()
		{
			return new Point2D(X, Y);
		}

		/// <summary>
		/// Convert to a Point
		/// </summary>
		/// <returns></returns>
		public System.Drawing.Point ToPoint()
		{
			return new Point((int)X, (int)Y);
		}

		/// <summary>
		/// Convert to a PointF
		/// </summary>
		/// <returns></returns>
		public System.Drawing.PointF ToPointF()
		{
			return new PointF((float) X, (float)Y);
		}

		/// <summary>
		/// Return String representation
		/// </summary>
		/// <returns>X, Y</returns>
		public override string ToString()
		{
			return string.Format("{0},{1}", X, Y);
		}

		/// <summary>
		/// Given a 'time' value return a Point2D which 
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public Point2D PointAtT(double t)
		{
			return new Point2D(X * t, Y * t);
		}

		/// <summary>
		/// Given a 'time' value return a Point2D which 
		/// </summary>
		/// <param name="t"></param>
		/// <returns></returns>
		public Point2D PointAtT(Point2D from, double t)
		{
			return new Point2D(from.X + X * t, from.Y + Y * t);
		}
	}
}
#endif
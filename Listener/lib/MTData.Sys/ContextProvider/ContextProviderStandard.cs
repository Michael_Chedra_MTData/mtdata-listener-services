#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Reflection;
using System.Collections;

namespace MTData.Sys.ContextProvider
{
	/// <summary>
	/// This class will provide an string addressable data context
	/// that can contain any number of objects, or custom attributes
	/// for an object without the need to modify the class structure 
	/// or hierarchy.
	/// Currently the class will process 
	///		Properties
	///		Fields
	///		Descents into IDictionary properties
	///		Descents into IContextProvider implementations
	/// </summary>
	public class ContextProviderStandard : IContextProvider, IContextProviderRegistration, IDisposable
	{
		#region Internal Context Object

		/// <summary>
		/// This is the wrapper around the object in the context.
		/// </summary>
		protected class ContextEntry 
		{
			/// <summary>
			/// Name of entry.
			/// </summary>
			private string _name = null;

			/// <summary>
			/// Value entry.
			/// </summary>
			private object _value = null;

			/// <summary>
			/// Instantiate the Context Entry.
			/// </summary>
			/// <param name="name"></param>
			public ContextEntry(string name, object value)
			{
				_name = name;
				_value = value;
			}

			public string Name { get{ return _name; }}
			public object Value { get{ return _value; } set { _value = value; }}
		}

		#endregion

		/// <summary>
		/// Holds references to context objects by name.
		/// </summary>
		private Hashtable _namedList = null;

		/// <summary>
		/// Syncronisation object
		/// </summary>
		private object _syncRoot = new object();

		/// <summary>
		/// Access to the named list by descendent classes.
		/// </summary>
		protected ContextEntry[] GetContextEntries()
		{
			ArrayList result = new ArrayList(_namedList.Count);
			foreach (ContextEntry entry in _namedList.Values)
				result.Add(entry);
			return (ContextEntry[])result.ToArray(typeof(ContextEntry));
		}

		/// <summary>
		/// Prepare the class for use.
		/// </summary>
		public ContextProviderStandard()
		{
			_namedList = new Hashtable();
		}

		/// <summary>
		/// Identify if the path exists or not.
		/// </summary>
		/// <param name="currentInstance"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		private bool GetPathExists(object currentInstance, string path)
		{
			bool result = false;

			if ((currentInstance != null) && (currentInstance is IContextProvider))
			{
				//	delegate to that context provider..
				result = ((IContextProvider)currentInstance).Contains(path);
			}
			else
			{
				int index = path.IndexOf('.');
				string name = path;
				if (index >= 0)
				{
					name = path.Substring(0,index);
					path = path.Substring(index+1, path.Length - (index + 1));
				}
				else
					path = "";
			
				if (currentInstance == null)
				{
					//	Check in the hashtable for the node concerned.
					ContextEntry entry = (ContextEntry)_namedList[name];
					if (entry != null)
					{
						if (path.Length > 0)
							result = GetPathExists(entry.Value, path);
						else
							result = true;
					}
				} 
				else
				{
					if (currentInstance is IDictionary)
					{
						if (((IDictionary)currentInstance).Contains(name))
						{
							if (path.Length > 0)
								result = GetPathExists(((IDictionary)currentInstance)[name], path);
							else
								result = true;
						}
						else
							result = false;
					}
					else
					{
						//	Check the instance passed for a property of the appropriate name..
						Type type = currentInstance.GetType();
						PropertyInfo property = type.GetProperty(name);
						if (property != null)
						{
							//	This object has a property called by this name..
							//	use that.
							if (path.Length > 0)
								result = GetPathExists(property.GetValue(currentInstance, null), path);
							else
								result = true;
						}
						else
						{
							FieldInfo field = type.GetField(name);
							if (field != null)
							{
								//	This object has a property called by this name..
								//	use that.
								if (path.Length > 0)
									result = GetPathExists(field.GetValue(currentInstance), path);
								else
									result = true;
							}
						}

					}
				}
			}
			return result;
		}

		/// <summary>
		/// This method will check through the context store for the field identified by the path.
		/// </summary>
		/// <param name="currentInstance"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		private object GetValueFromPath(object currentInstance, string path)
		{
			object result = null;

			if ((currentInstance != null) && (currentInstance is IContextProvider))
			{
				//	delegate to that context provider..
				result = ((IContextProvider)currentInstance)[path];
			}
			else
			{
				int index = path.IndexOf('.');
				string name = path;
				if (index >= 0)
				{
					name = path.Substring(0,index);
					path = path.Substring(index+1, path.Length - (index + 1));
				}
				else
					path = "";
			
				if (currentInstance == null)
				{
					//	Check in the hashtable for the node concerned.
					ContextEntry entry = (ContextEntry)_namedList[name];
					if (entry != null)
					{
						if (path.Length > 0)
							result = GetValueFromPath(entry.Value, path);
						else
							result = entry.Value;
					}
				} 
				else
				{
					if (currentInstance is IDictionary)
					{
						if (((IDictionary)currentInstance).Contains(name))
						{
							if (path.Length > 0)
								result = GetValueFromPath(((IDictionary)currentInstance)[name], path);
							else
								result = ((IDictionary)currentInstance)[name];
						}
						else
							result = null;
					}
					else
					{
						//	Check the instance passed for a property of the appropriate name..
						Type type = currentInstance.GetType();
						PropertyInfo property = type.GetProperty(name);
						if (property != null)
						{
							//	This object has a property called by this name..
							//	use that.
							if (path.Length > 0)
								result = GetValueFromPath(property.GetValue(currentInstance, null), path);
							else
								result = property.GetValue(currentInstance, null);
						}
						else
						{
							FieldInfo field = type.GetField(name);
							if (field != null)
							{
								//	This object has a property called by this name..
								//	use that.
								if (path.Length > 0)
									result = GetValueFromPath(field.GetValue(currentInstance), path);
								else
									result = field.GetValue(currentInstance);
							}
						}
					}
				}
			}
			return result;
		}

		/// <summary>
		/// This method will check through the context store for the field identified by the path
		/// and set it's value
		/// </summary>
		/// <param name="currentInstance"></param>
		/// <param name="path"></param>
		/// <param name="value"></param>
		/// <returns></returns>
		private void SetValueFromPath(object currentInstance, string path, object value)
		{
			if ((currentInstance != null) && (currentInstance is IContextProvider))
			{
				//	delegate to that context provider..
				((IContextProvider)currentInstance)[path] = value;
			}
			else
			{
				int index = path.IndexOf('.');
				string name = path;
				if (index >= 0)
				{
					name = path.Substring(0,index);
					path = path.Substring(index+1, path.Length - (index + 1));
				}
				else
					path = "";
			
				if (currentInstance == null)
				{
					//	Check in the hashtable for the node concerned.
					ContextEntry entry = (ContextEntry)_namedList[name];
                    if (entry != null)
                    {
                        if (path.Length > 0)
                            SetValueFromPath(entry.Value, path, value);
                        else
                            entry.Value = value;
                    }
                    else if (path.Length == 0)
                        Register(name, value);
				} 
				else
				{
					if (currentInstance is IDictionary)
					{
						if (((IDictionary)currentInstance).Contains(name))
						{
							if (path.Length > 0)
								SetValueFromPath(((IDictionary)currentInstance)[name], path, value);
							else
								((IDictionary)currentInstance)[name] = value;
						} 
                        else if (path.Length == 0)
                            ((IDictionary)currentInstance)[name] = value;
                    }
					else
					{
						//	Check the instance passed for a property of the appropriate name..
						Type type = currentInstance.GetType();
						PropertyInfo property = type.GetProperty(name);
						if (property != null)
						{
							//	This object has a property called by this name..
							//	use that.
                            if (path.Length > 0)
                                SetValueFromPath(property.GetValue(currentInstance, null), path, value);
                            else
                            {
                                object convertedValue = Convert.ChangeType(value, property.PropertyType, null);
                                property.SetValue(currentInstance, convertedValue, null);
                            }
						}
						else
						{
							FieldInfo field = type.GetField(name);
							if (field != null)
							{
								//	This object has a property called by this name..
								//	use that.
								if (path.Length > 0)
									SetValueFromPath(field.GetValue(currentInstance), path, value);
								else
									field.SetValue(currentInstance, value);
							}
						}
					}
				}
			}
		}


		#region IContextProvider Members

		public object SyncRoot
		{
			get
			{
				return _syncRoot;
			}
		}

		public object this[string path]
		{
			get
			{
				return GetValueFromPath(null, path);
			}
			set
			{
				SetValueFromPath(null, path, value);
			}
		}

		public bool Contains(string path)
		{
			return GetPathExists(null, path);
		}

		public void RegisterChange(string path)
		{
			if (Change != null)
				Change(this, path);
		}

		public event ContextProvider.ContextProviderChangedDelegate Change;

		#endregion

		#region IContextProviderRegistration Members

		public void Register(string name, object value)
		{
			if (_namedList.Contains(name))
			{
				((ContextEntry)_namedList[name]).Value = value;
			}
			else
				_namedList.Add(name, new ContextEntry(name, value));
		}

		public void UnRegister(string name, object value)
		{
			if (_namedList.Contains(name))
				_namedList.Remove(name);
		}

		#endregion

        #region IDisposable Members

        public void Dispose()
        {
            foreach (ContextEntry entry in _namedList.Values)
            {
                IDisposable disposable = entry.Value as IDisposable;
                if (disposable != null)
                    disposable.Dispose();
            }
            _namedList = null;
        }

        #endregion
    }
}

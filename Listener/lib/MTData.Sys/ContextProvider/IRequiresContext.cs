using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.ContextProvider
{
	public interface IRequiresContext
	{
		void SetContext(IContextProvider provider);
	}
}

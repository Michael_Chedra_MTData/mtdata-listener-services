#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

namespace MTData.Sys.ContextProvider
{
	/// <summary>
	/// This context provider is simply a hashtable with name value pairs..
	/// </summary>
	public class ContextProviderSimple : IContextProvider
	{
		private object _syncRoot = new object();
		private System.Collections.Hashtable _hashTable = null;
        
		/// <summary>
		/// This is a wrapped context provider, so if a value is not found in this 
		/// context provider, the wrapped one will be checked.
		/// </summary>
		private IContextProvider _provider = null;

		public ContextProviderSimple(IContextProvider provider) : this()
		{
			_provider = provider;
		}

		public ContextProviderSimple(System.Collections.Hashtable hashTable)
		{
			_hashTable = hashTable;
		}

		public ContextProviderSimple()
		{
			_hashTable = new System.Collections.Hashtable();
		}

		#region IContextProvider Members

		public object SyncRoot
		{
			get{ return _syncRoot; }
		}

		/// <summary>
		/// If this instance wraps another instance, then updates will only 
		/// affect this instance, and not the underlying one.
		/// </summary>
		public object this[string path]
		{
			get
			{
				object result = _hashTable[path];
				if ((result == null) && (_provider != null))
					result = _provider[path];

				return result;
			}
			set
			{
				_hashTable[path] = value;
			}
		}

		public bool Contains(string path)
		{
			return _hashTable.Contains(path);
		}

		public void RegisterChange(string path)
		{
			if (Change != null)
				Change(this, path);
		}

		public event ContextProvider.ContextProviderChangedDelegate Change;

		#endregion
	}
}

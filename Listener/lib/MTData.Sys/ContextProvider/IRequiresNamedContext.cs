using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.ContextProvider
{
	public interface IRequiresNamedContext : IRequiresContext
	{
		string RequiredContextName
		{
			get;
		}
	}
}

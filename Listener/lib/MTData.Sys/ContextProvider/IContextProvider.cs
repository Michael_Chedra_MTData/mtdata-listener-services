#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

namespace MTData.Sys.ContextProvider
{
	#region IContextProvider

	/// <summary>
	/// This interface provides all functionality of the Context Provider
	/// </summary>
	public interface IContextProvider
	{
		/// <summary>
		/// Syncronistion object
		/// </summary>
		object SyncRoot { get; }

		/// <summary>
		/// Returne the path tot he object in the context provider
		/// </summary>
		object this[string path] { get; set; }

		/// <summary>
		/// Identifies if an object exists within the Context Provider or not.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		bool Contains(string path);

		/// <summary>
		/// This allows the code that builds the provider to raise change events.
		/// </summary>
		/// <param name="name"></param>
		void RegisterChange(string path);

		/// <summary>
		/// Change event of the provider
		/// </summary>
		event ContextProviderChangedDelegate Change;
	}

	#endregion

	#region IContextProviderRegistration

	/// <summary>
	/// This interface allows the registration of objects into the context provider
	/// </summary>
	public interface IContextProviderRegistration : IContextProvider
	{
		/// <summary>
		/// Register a named object.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		void Register(string name, object value);

		/// <summary>
		/// This will remove an item from the context.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="value"></param>
		void UnRegister(string name, object value);
	}

	#endregion

	#region ContextProviderChangedDelegate

	/// <summary>
	/// This is the format of an event raised whenever the value of an object changes,
	/// when changed through the context provider.
	/// </summary>
	public delegate void ContextProviderChangedDelegate(IContextProvider provider, string path);

	#endregion

}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.ContextProvider
{
	/// <summary>
	/// This class provides a class to load up all of the instances of items required for an application
	/// </summary>
	public class Model : ContextProviderStandard, IDisposable
	{
		public Model()
			: base()
		{

		}

		public static Model CreateFromConfig(Config.Config config, string path, object context)
		{
			Model result = new Model();

			string[] modelPaths = config.GetSectionNames(path);
			foreach (string modelPath in modelPaths)
			{
				object modelInstance = config.GetSection(path + "/" + modelPath, result);
				result.Register(modelPath, modelInstance);
			}
			return result;
		}

		#region IDisposable Members

		public new void Dispose()
		{
			ContextEntry[] entries = base.GetContextEntries();
			foreach (ContextEntry entry in entries)
			{
				if (entry.Value is IDisposable)
					((IDisposable)entry.Value).Dispose();
			}
            base.Dispose();
		}
		#endregion
	}
}

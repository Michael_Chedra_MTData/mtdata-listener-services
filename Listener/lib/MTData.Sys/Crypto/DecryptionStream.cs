#if !NETCF && !NETCF2
using System;
using System.IO;
using System.Security.Cryptography;

namespace MTData.Sys.Crypto
{
	/// <summary>
	/// Summary description for DecryptionStream.
	/// </summary>
	public class DecryptionStream : Stream
	{
		private Stream _source = null;
		private byte[] _cryptoKey = null;
		private Rijndael _alg;
		private CryptoStream _hashOut = null;
		private CryptoStream _streamIn = null;
		private HashAlgorithm _hasher = null;
		private long _fileSize;
		private long _fileRead;
		private bool _checkHash;

		public DecryptionStream(Stream sourceStream, byte[] key, bool checkHash)
		{
			_checkHash = checkHash;
			_source = sourceStream;
			_cryptoKey = key;

			byte[] salt = new byte[16];
				
			_alg = Rijndael.Create();
			_alg.BlockSize = 128;
			_alg.Padding = PaddingMode.PKCS7;
			_alg.KeySize = 128;
			_alg.Mode = CipherMode.ECB;

			byte[] IV = new byte[16];

			sourceStream.Read(IV, 0, 16);
			sourceStream.Read(salt, 0, 16);
				
			if (_checkHash)
			{
				_hasher = SHA256.Create();
				_hashOut = new CryptoStream(Stream.Null, _hasher, CryptoStreamMode.Write);
			}

			_streamIn = new CryptoStream(sourceStream, _alg.CreateDecryptor(_cryptoKey, IV), CryptoStreamMode.Read);

			byte[] buffer = new byte[_alg.BlockSize];

			BinaryReader reader = new BinaryReader(sourceStream);
			_fileSize = reader.ReadInt64();
			if (reader.ReadByte() != (byte)0x7E)
				throw new Exception("Crypto:Invalid Stream Format character");

			_fileRead = _fileSize;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			int bytesRead = _streamIn.Read(buffer, offset, Convert.ToInt32((_fileRead < count)?_fileRead:count));
			if (_checkHash)
				_hashOut.Write(buffer, offset, bytesRead);
			_fileRead -= bytesRead;

			return bytesRead;
		}

		private bool _closed = false;
		public override void Close()
		{
			base.Close ();
			if (!_closed)
			{
				_closed = true;
				if (_fileRead > 0)
				{
					//	Loop to end of stream for hash..
					byte[] buffer = new byte[_alg.BlockSize];

					int bytesRead = _streamIn.Read(buffer, 0, Convert.ToInt32((_fileRead < _alg.BlockSize)?_fileRead:_alg.BlockSize));
					while(bytesRead > 0)
					{
						if (_checkHash)
							_hashOut.Write(buffer, 0, bytesRead);
						_fileRead -= bytesRead;
						bytesRead = _streamIn.Read(buffer, 0, Convert.ToInt32((_fileRead < _alg.BlockSize)?_fileRead:_alg.BlockSize));
					}
				}

				byte[] hash = null;

				if (_checkHash)
				{
					_hashOut.Flush();
					_hashOut.Close();
					_hashOut = null;

					hash = _hasher.Hash;
				}

				//	still need to read the hash before closing the in stream
				byte[] oldHash = new byte[_hasher.HashSize/8];
				_streamIn.Read(oldHash, 0, oldHash.Length);

				if (_checkHash)
				{
					if (oldHash.Length != hash.Length) 
						throw new Exception("Crypto:Hash length different");

					for(int loop = 0; loop < oldHash.Length; loop++)
						if (oldHash[loop] != hash[loop])
							throw new Exception("Crypto:Hash different");
				}
				_streamIn.Close();
			}
		}

		public override bool CanRead
		{
			get
			{
				return true;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return false;
			}
		}

		public override void Flush()
		{

		}

		public override long Length
		{
			get
			{
				return _fileSize;
			}
		}

		public override long Position
		{
			get
			{
				return _fileSize - _fileRead;
			}
			set
			{
			}
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			return 0;
		}

		public override void SetLength(long value)
		{
			
		}

		public override void Write(byte[] buffer, int offset, int count)
		{
			
		}




	}
}
#endif
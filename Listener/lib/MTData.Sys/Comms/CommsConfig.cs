#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;

namespace MTData.Sys.Comms
{
	public class CommsConfig : Loader.ILoaderConfigurable, IConfigurable
	{
		private string _bindIP = "";
		private string _targetIP = "";
		private int _portNumber = 0;

		public CommsConfig()
		{

		}

		#region ILoaderConfigurable Members

		public void Configure(MTData.Sys.Loader.Loader loader, object context, XmlNode configNode)
		{
			if (configNode != null)
			{
				_bindIP = configNode.Attributes.GetNamedItem("bindIP").Value;
				_targetIP = configNode.Attributes.GetNamedItem("targetIP").Value;
				_portNumber = Int32.Parse(configNode.Attributes.GetNamedItem("port").Value);
			}
		}

		#endregion

		public string BindIP
		{
			get
			{
				return _bindIP;
			}
		}

		public string TargetIP
		{
			get
			{
				return _targetIP;
			}
		}

		public int PortNumber
		{
			get
			{
				return _portNumber;
			}
		}


		#region IConfigurable Members

		public void Configure(object context, Config.Config config, string path, XmlNode configNode)
		{
			Configure(null, context, configNode);
		}

		#endregion
	}
}

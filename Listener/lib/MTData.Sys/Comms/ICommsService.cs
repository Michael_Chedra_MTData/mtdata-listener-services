﻿using System;

namespace MTData.Sys.Comms
{
    public delegate void CommsPacketSentOrReceived(bool sent);
    public delegate void CommsDelegate();

    public interface ICommsService
    {
        /// <summary>
        /// has the comms service recieved data since it started
        /// </summary>
        bool HasCommsReceivedData { get; }

        /// <summary>
        /// Is the comms service currently interrupted
        /// </summary>
        bool IsCommsInterrupted { get; }

        /// <summary>
        /// time of the last packet sent by this comms service (UTC)
        /// </summary>
        DateTime LastPacketSent { get; }
        /// <summary>
        /// time of the last packet received by this comms service (UTC)
        /// </summary>
        DateTime LastPacketReceived { get; }

        /// <summary>
        /// packet has been sent or receievd by this interface
        /// </summary>
        event CommsPacketSentOrReceived OnPacketSentOrRecieved;
        /// <summary>
        /// the comms on this interface have been interrupted
        /// </summary>
        event CommsDelegate OnCommsInterrupted;
        /// <summary>
        /// the comms have been reconnected
        /// </summary>
        event CommsDelegate OnCommsReconnected;

    }
}

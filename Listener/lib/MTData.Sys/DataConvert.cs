using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys
{
	/// <summary>
	/// This class provides standard conversion commands for operating with data.
	/// It takes into consideration what a DBNull value would be and returns/uses
	/// the appropriate real value.
	/// </summary>
	public class DataConvert
	{
		#region long

		/// <summary>
		/// Conversion from a reader value to a long.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static long ToLong(object returnedValue, long defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt64(returnedValue);
		}

		/// <summary>
		/// Convert from a standard long to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromLong(long fieldValue, long defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region ulong

		/// <summary>
		/// Conversion from a reader value to a ulong.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static ulong ToULong(object returnedValue, ulong defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToUInt64(returnedValue);
		}

		/// <summary>
		/// Convert form a standard ulong to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromULong(ulong fieldValue, ulong defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return Convert.ToInt64(fieldValue);
		}

		#endregion

		#region uint
		/// <summary>
		/// Conversion from a reader value to an int.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static uint ToUInt(object returnedValue, uint defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToUInt32(returnedValue);
		}

		#endregion

		#region int

		/// <summary>
		/// Conversion from a reader value to an int.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static int ToInt(object returnedValue, int defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt32(returnedValue);
		}

		/// <summary>
		/// Convert from a standard int to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromInt(int fieldValue, int defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region short

		/// <summary>
		/// Conversion from a reader value to a short.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static short ToShort(object returnedValue, short defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt16(returnedValue);
		}

		/// <summary>
		/// Convert from a standard short to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromInt(short fieldValue, short defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region UShort

		/// <summary>
		/// Conversion from a reader value to an unsigned short.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static ushort ToUShort(object returnedValue, ushort defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToUInt16(returnedValue);
		}

		/// <summary>
		/// Convert from a standard unsigned short to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromUShort(ushort fieldValue, ushort defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region double

		/// <summary>
		/// Convert from a reader value to a double
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static double ToDouble(object returnedValue, double defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToDouble(returnedValue);
		}

		/// <summary>
		/// Convert from standard double to reader value
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static object FromDouble(double fieldValue, double defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region Byte

		/// <summary>
		/// Convert from a reader value to a byte
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static double ToByte(object returnedValue, byte defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToByte(returnedValue);
		}

		/// <summary>
		/// Convert from standard byte to reader value
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static object FromByte(byte fieldValue, byte defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region Byte Array

		/// <summary>
		/// Convert from a reader value to a byte array
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static byte[] ToByteArray(object returnedValue, byte[] defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return (byte[])returnedValue;
		}

		/// <summary>
		/// Convert from standard byte array to reader value
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static object FromByteArray(byte[] fieldValue, object defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion Byte Array

		#region Decimal

		/// <summary>
		/// Convert from a reader value to a decimal
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns>Converted decimal value</returns>
		public static Decimal ToDecimal(object returnedValue, Decimal defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToDecimal(returnedValue);
		}

		/// <summary>
		/// Convert from standard Decimal to reader value
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns>Converted reader value</returns>
		public static object FromDecimal(Decimal fieldValue, Decimal defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region DateTime

		/// <summary>
		/// Conversion from a reader value to an int.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static DateTime ToDateTime(object returnedValue, DateTime defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToDateTime(returnedValue);
		}

		/// <summary>
		/// Convert from a standard int to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromDateTime(DateTime fieldValue, DateTime defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region String

		/// <summary>
		/// Convert from reader value to standard string
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static string ToString(object returnedValue, string defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return (string)returnedValue;
		}

		/// <summary>
		/// Convert from standard string to a reader value
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static object FromString(string fieldValue, string defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region Boolean
		/// <summary>
		/// Conversion from a reader value to an boolean
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static bool ToBool(object returnedValue, bool defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToBoolean(returnedValue);
		}

		#endregion
	}
}

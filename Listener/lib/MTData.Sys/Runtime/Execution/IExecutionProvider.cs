#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Execution
{
	public delegate void ExecutionProviderFinishedDelegate(IExecutionProvider provider, IExecutionTask context);
	public delegate void ExecutionProviderFailedDelegate(IExecutionProvider provider, IExecutionTask context, string reason);

	public delegate void ExecutionProviderTaskDelegate(object taskContext, IExecutionContext providerContext);
	public delegate void ExecutionProviderNullDelegate();

	/// <summary>
	/// This interface will be supported by all providers providing 
	/// execution models in the Execution Service.
	/// </summary>
	public interface IExecutionProvider
	{
		/// <summary>
		/// This is the name of the execution provider
		/// </summary>
		string Name
		{
			get;
		}

		/// <summary>
		/// This method will take a delegate and queue/execute the method.
		/// The returned Task interface allows the task execution to be monitored, and
		/// cancelled if required.
		/// </summary>
		/// <param name="taskCallback">This is the delegate to call</param>
		/// <param name="context">This is the context information to be passed to the delegate</param>
		/// <param name="name">This is an optional name for the task</param>
		/// <returns></returns>
		IExecutionTask CreateTask(ExecutionProviderTaskDelegate taskCallback, object context, string name);

		/// <summary>
		/// This method will allow any method with no parameters to be processed as a task.
		/// </summary>
		/// <param name="nullTaskCallback"></param>
		/// <param name="name">This is an optional name for the task</param>
		/// <returns></returns>
		IExecutionTask CreateTask(ExecutionProviderNullDelegate nullTaskCallback, string name);

		/// <summary>
		/// This event is raised whenever a context is finished execution
		/// </summary>
		event ExecutionProviderFinishedDelegate Complete;

		/// <summary>
		/// The event is raised when the execution of the context is cancelled
		/// </summary>
		event ExecutionProviderFinishedDelegate Cancelled;

		/// <summary>
		/// This event is raised whenever the execution has failed.
		/// </summary>
		event ExecutionProviderFailedDelegate Failed;
	}
}
#endif

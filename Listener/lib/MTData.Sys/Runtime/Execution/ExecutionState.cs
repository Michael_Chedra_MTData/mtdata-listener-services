#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Execution
{
	/// <summary>
	/// This is the report execution state.
	/// </summary>
	public enum ExecutionState : int
	{
		/// <summary>
		/// The state is not known
		/// </summary>
		Unknown,

		/// <summary>
		/// This is a new request that has not been queued yet.
		/// </summary>
		New,

		/// <summary>
		/// Indicates that the context has been queued with a provider
		/// </summary>
		Queued,

		/// <summary>
		/// The report is currently being processed
		/// </summary>
		Processing,

		/// <summary>
		/// The report has completed successfully
		/// </summary>
		Complete,

		/// <summary>
		/// The report has finished execution due to an error condition.
		/// </summary>
		Failed,

		/// <summary>
		/// This state indicates that the report was cancelled due to user action
		/// </summary>
		Cancelled,

		/// <summary>
		/// This state indicates that the execution of the report was aborted due to the 
		/// execution provider shutting down.
		/// </summary>
		Aborted
	}
}
#endif
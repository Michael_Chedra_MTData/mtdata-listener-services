#if !NETCF && !NETCF2
using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;
using log4net;
using MTData.Sys.Services;

namespace MTData.Sys.Runtime.Execution
{
	public class ThreadCappedExecutionProvider : ProviderBaseClass, IExecutionProvider
	{
		#region Execution Context

		/// <summary>
		/// An instance of this class is passed to the called delegate, and allows a measure of 
		/// feedback to be presented to the initial Requester.
		/// </summary>
		private class ExecutionContext : IExecutionContext
		{
			private bool _cancelRequested = false;
			private ExecutionTask _task = null;

			public ExecutionContext(ExecutionTask task)
			{
				_task = task;
			}

			internal void RequestCancel()
			{
				_cancelRequested = true;
                if (_task.State == ExecutionState.New || _task.State == ExecutionState.Queued)
                {
                    SetCancelled();
                }
			}
			#region IExecutionContext Members

			public bool CancelRequested
			{
				get
				{
					return _cancelRequested;
				}
			}

			public void Info(string message)
			{
				_task.OnInfo(message);
			}

			public void Warn(string message)
			{
				_task.OnWarning(message);
			}

			public void Error(string message, Exception ex)
			{
				_task.OnError(message, ex);
			}

			public void SetProgress(int value)
			{
				_task.SetProgress(value);
			}

			public void SetComplete()
			{
				_task.OnComplete();
			}

			public void SetCancelled()
			{
				_task.OnCancelled();
			}

            public void SetFailed(string reason)
			{
				_task.OnFailed(reason);
			}

			#endregion
		}
		#endregion

		private static ILog _logger = log4net.LogManager.GetLogger(typeof(ThreadCappedExecutionProvider));

		private class ExecutionTask : IExecutionTask
		{
			/// <summary>
			/// This is a user-specified friendly name for the task
			/// </summary>
			private string _name = null;

			/// <summary>
			/// This is the id given to the task within the provider.
			/// </summary>
			private int _id = -1;

			/// <summary>
			/// This is the progress value indicated by the called method
			/// </summary>
			private int _progress = 0;

			/// <summary>
			/// This is the current state of the executing task.
			/// </summary>
			private ExecutionState _state = ExecutionState.New;
			
			private ExecutionContext _providerContext = null;
			private object _taskContext = null;

			private ThreadCappedExecutionProvider _provider = null;

			private ExecutionProviderTaskDelegate _providerTask = null;
			private ExecutionProviderNullDelegate _nullTask = null;

			private System.Threading.Thread _executionThread = null;

			public ExecutionTask(ThreadCappedExecutionProvider provider, int id, ExecutionProviderTaskDelegate task, object context, string name)
			{
				_provider = provider;
				_id = id;
				_providerContext = new ExecutionContext(this);
				_providerTask = task;
				_taskContext = context;
				_name = FormatName(name);
			}

			public ExecutionTask(ThreadCappedExecutionProvider provider, int id, ExecutionProviderNullDelegate task, string name)
			{
				_provider = provider;
				_id = id;
				_nullTask = task;
				_name = FormatName(name);
			}

			private string FormatName(string name)
			{
				if ((name != null) && (name.Trim().Length > 0))
					return name;
				else
					return string.Format("Task {0}", _id);
			}

			public override string ToString()
			{
				return _name;
			}

			/// <summary>
			/// Indicate that the state has been changed to Queued
			/// </summary>
			internal void Queued()
			{
				SetState(ExecutionState.Queued);
			}

			/// <summary>
			/// Triggers the start of the task.
			/// </summary>
			internal void Start()
			{
				_executionThread = new Thread(new ThreadStart(ExecutionHandler));
				_executionThread.Name = "Task:" + _id.ToString();
				_executionThread.IsBackground = true;
				_executionThread.Start();
			}

			public void Abort()
			{
				if (_executionThread != null)
					_executionThread.Abort();
				_executionThread = null;
				OnAborted();	
			}

			/// <summary>
			/// This method handles all execution of the actual delegate.
			/// </summary>
			private void ExecutionHandler()
			{
				try
				{
					//	Indicate that we are processing.
					SetState(ExecutionState.Processing);

					if (_providerTask != null)
					{
						_providerTask(_taskContext, _providerContext);
					}
					else if (_nullTask != null)
						_nullTask();

					//	If the state has not been changed by the method, set it here.
					if (_state == ExecutionState.Processing)
						OnComplete();
				}
				catch (Exception ex)
				{
					OnError("Error Occurred Processing the request", ex);
					OnFailed("Exception processing the task");
				}
			}

			/// <summary>
			/// This allows any information to be passed back to anyone listening
			/// </summary>
			/// <param name="message"></param>
			internal void OnInfo(string message)
			{
				if (Info != null)
					Info(this, message);
			}

			/// <summary>
			/// This allows any warnings to be passed back to anyone listening.
			/// </summary>
			/// <param name="message"></param>
			internal void OnWarning(string message)
			{
				if (Warning != null)
					Warning(this, message);
			}

			/// <summary>
			/// This allows any errors to be passed back to anyone listeing.
			/// </summary>
			/// <param name="message"></param>
			/// <param name="ex"></param>
			internal void OnError(string message, Exception ex)
			{
				if (Error != null)
					Error(this, message, ex);
			}

			/// <summary>
			/// Allows feedback ot the caller from the called method.
			/// </summary>
			/// <param name="value"></param>
			internal void SetProgress(int value)
			{
				if (value != _progress)
				{
					_progress = value;
					if (ProgressChanged != null)
						ProgressChanged(this, _progress);
				}
			}

			internal void OnComplete()
			{
				if (SetState(ExecutionState.Complete))
					if (Complete != null)
						Complete(this);
			}

			internal void OnCancelled()
			{
				if (SetState(ExecutionState.Cancelled))
					if (Cancelled != null)
						Cancelled(this);
			}

			internal void OnAborted()
			{
				if (SetState(ExecutionState.Aborted))
					if (Aborted != null)
						Aborted(this);
			}

			internal void OnFailed(string reason)
			{
				if (SetState(ExecutionState.Failed))
					if (Failed != null)
						Failed(this, reason);
			}

			internal bool SetState(ExecutionState state)
			{
				if (_state != state)
				{
					_state = state;
					if (StateChanged != null)
						StateChanged(this, _state);
					return true;
				}
				else
					return false;
			}

			#region IExecutionTask Members

			public void Submit()
			{
				if (_provider != null)
					_provider.QueueTask(this);
			}

			public void Cancel()
			{
                if ((_providerTask != null) && (_providerContext != null))
                {
                    _providerContext.RequestCancel();
                }
			}

			public ExecutionState State
			{
				get
				{
					return _state;
				}
			}

			public int ID
			{
				get
				{
					return _id;
				}
			}

			public int Progress
			{
				get
				{
					return _progress;
				}
			}

			public object Context
			{
				get
				{
					return _taskContext;
				}
			}

			public event ExecutionTaskStateChangeDelegate StateChanged;

			public event ExecutionTaskNotifyDelegate Complete;

			public event ExecutionTaskNotifyDelegate Cancelled;

			public event ExecutionTaskNotifyDelegate Aborted;

			public event ExecutionTaskNotifyTextDelegate Failed;

			public event ExecutionTaskNotifyTextDelegate Info;

			public event ExecutionTaskNotifyTextDelegate Warning;

			public event ExecutionTaskNotifyErrorDelegate Error;

			public event ExecutionTaskNotifyProgressDelegate ProgressChanged;

			#endregion
		}

		private static int _lastID = 0;

		/// <summary>
		/// REturn the next id to be used by the task caller
		/// </summary>
		/// <returns></returns>
		private static int GetNextID()
		{
			return System.Threading.Interlocked.Increment(ref _lastID);
		}

		private string _name = "Queued";
		private bool _disposed = false;
		private List<ExecutionTask> _queue = new List<ExecutionTask>();
		private List<ExecutionTask> _active = new List<ExecutionTask>();

		private Thread _monitorThread = null;
		private AutoResetEvent _waitHandle = new AutoResetEvent(true);
		private int _maxThreadCount = 1;

		public event ExecutionProviderFinishedDelegate Complete;
		public event ExecutionProviderFinishedDelegate Cancelled;
		event ExecutionProviderFailedDelegate Failed;

		/// <summary>
		/// This is the maximum concurrent threads allowed.
		/// </summary>
		public int MaxThreadCount
		{
			get
			{
				return _maxThreadCount;
			}
			set
			{
				_maxThreadCount = value;
				base.Description = string.Format("Thread Capped Execution with {0} concurrent threads", _maxThreadCount);
				_waitHandle.Set();
			}
		}

		public ThreadCappedExecutionProvider() : this(1)
		{

		}

		public ThreadCappedExecutionProvider(int maxThreadCount) : base("ThreadCappedExecutionProvider", string.Format("Thread Capped Execution with {0} concurrent threads", maxThreadCount))
		{
			_maxThreadCount = maxThreadCount;
			_monitorThread = new Thread(new ThreadStart(MonitorHandler));
			_monitorThread.Name = "QueuedExecutionThread";
			_monitorThread.IsBackground = false;
			_monitorThread.Start();
		}

		#region IDisposable Members

		public override void Dispose()
		{
			_disposed = true;
			if (_logger.IsInfoEnabled)
				_logger.Info("Dispose() : Aborting existing calls");

			_monitorThread = null;
			_waitHandle.Set();

			ExecutionTask[] tasks = null;

			lock (_queue)
			{
				tasks = _queue.ToArray();
				_queue.Clear();
			}

			foreach (ExecutionTask task in tasks)
			{
				try
				{
					task.Abort();
				}
				catch(Exception ex)
				{
					if (_logger.IsErrorEnabled)
						_logger.Error(string.Format("Dispose():Failed to abort task {0}", task.ToString()), ex);
				}
			}
			lock (_queue)
			{
				tasks = _active.ToArray();
			}

			foreach (ExecutionTask task in tasks)
			{
				try
				{
					task.Abort();
				}
				catch (Exception ex)
				{
					if (_logger.IsErrorEnabled)
						_logger.Error(string.Format("Dispose():Failed to abort context {0}", task.ToString()), ex);
				}
			}
			base.Dispose();
		}

		#endregion

		#region Thread Handling

		/// <summary>
		/// This thread will monitor the queues and be responsible for activating
		/// an item off of the execution queue.
		/// </summary>
		private void MonitorHandler()
		{
			while (_monitorThread != null)
			{
				_waitHandle.WaitOne(TimeSpan.FromSeconds(2), true);
				lock (_queue)
				{
					while (_active.Count < _maxThreadCount)
					{
						if (_queue.Count == 0)
							break;
						else
						{
							ExecutionTask thread = _queue[0];
							_queue.RemoveAt(0);
							_active.Add(thread);
							thread.Start();
                            UpdateQueuePositionMessage();
						}
					}
				}
			}
		}
		#endregion

		private void QueueTask(ExecutionTask task)
		{
			if (_disposed)
				throw new ObjectDisposedException("ExecutionProvider");

			task.Complete += new ExecutionTaskNotifyDelegate(task_Complete);
			task.Aborted += new ExecutionTaskNotifyDelegate(task_Aborted);
			task.Cancelled += new ExecutionTaskNotifyDelegate(task_Cancelled);
			task.Failed += new ExecutionTaskNotifyTextDelegate(task_Failed);
			lock (_queue)
			{
				_queue.Add(task);
                UpdateQueuePositionMessage();
            }
			task.Queued();
			_waitHandle.Set();
		}

		void task_Cancelled(IExecutionTask sender)
		{
			OnExecutionComplete((ExecutionTask)sender);
			if (Cancelled != null)
				Cancelled(this, sender);
		}

		void task_Aborted(IExecutionTask sender)
		{
			OnExecutionComplete((ExecutionTask)sender);
			if (Failed != null)
				Failed(this, sender, "Operation Aborted");
		}

		void task_Failed(IExecutionTask sender, string message)
		{
			OnExecutionComplete((ExecutionTask)sender);
			if (Failed != null)
				Failed(this, sender, message);
		}

		void task_Complete(IExecutionTask sender)
		{
			OnExecutionComplete((ExecutionTask)sender);
			if (Complete != null)
				Complete(this, sender);
		}

		#region IExecutionProvider Members
		
		string IExecutionProvider.Name
		{
			get
			{
				return _name;
			}
		}

		IExecutionTask IExecutionProvider.CreateTask(ExecutionProviderTaskDelegate taskCallback, object context, string name)
		{
			if (_disposed)
				throw new ObjectDisposedException("ExecutionProvider");

			return new ExecutionTask(this, GetNextID(), taskCallback, context, name);
		}

		IExecutionTask IExecutionProvider.CreateTask(ExecutionProviderNullDelegate nullTaskCallback, string name)
		{
			if (_disposed)
				throw new ObjectDisposedException("ExecutionProvider");

			return new ExecutionTask(this, GetNextID(), nullTaskCallback, name);
		}

		event ExecutionProviderFinishedDelegate IExecutionProvider.Complete
		{
			add
			{
				Complete += value;
			}
			remove
			{
				Complete -= value;
			}
		}

		event ExecutionProviderFinishedDelegate IExecutionProvider.Cancelled
		{
			add
			{
				Cancelled += value;
			}
			remove
			{
				Cancelled -= value;
			}
		}

		event ExecutionProviderFailedDelegate IExecutionProvider.Failed
		{
			add
			{
				Failed += value;
			}
			remove
			{
				Failed -= value;
			}
		}


		/// <summary>
		/// This event handler is triggered when the execution is complete,be it through an error or not.
		/// </summary>
		/// <param name="thread"></param>
		private void OnExecutionComplete(ThreadCappedExecutionProvider.ExecutionTask task)
		{
			//	always lock queue...
			lock (_queue)
			{
				if (_active.Contains(task))
					_active.Remove(task);
				if (_queue.Contains(task))
					_queue.Remove(task);
                UpdateQueuePositionMessage();
            }
			_waitHandle.Set();
		}

        private void UpdateQueuePositionMessage()
        {
            int count = _queue.Count;
            int position=1;
            foreach (ExecutionTask task in _queue)
            {
                task.OnInfo(string.Format("Queue Position {0} of {1}", position, count));
                position++;
            }
        }
		#endregion

    }
}
#endif

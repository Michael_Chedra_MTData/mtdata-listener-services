#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Execution
{
	/// <summary>
	/// This interface identifies functions that are available to tasks
	/// queued through an Execution provider
	/// </summary>
	public interface IExecutionContext
	{
		/// <summary>
		/// This property indicates that the initiator of the Task has requested a cancellation.
		/// </summary>
		bool CancelRequested
		{
			get;
		}

		/// <summary>
		/// This allows the task to 
		/// </summary>
		/// <param name="message"></param>
		void Info(string message);

		/// <summary>
		/// Indicate to the task initiator that a warning condition has occurred
		/// </summary>
		/// <param name="message"></param>
		void Warn(string message);

		/// <summary>
		/// Indicate that an error condition has occurred.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="ex"></param>
		void Error(string message, Exception ex);

		/// <summary>
		/// Allows the running method to indicate progress back through to the Task.
		/// </summary>
		/// <param name="value"></param>
		void SetProgress(int value);

		/// <summary>
		/// Indicates that the task has completed
		/// </summary>
		void SetComplete();

		/// <summary>
		/// Indicates that the task has responded to the CancelRequest and has cancelled gracefully
		/// </summary>
		void SetCancelled();

		/// <summary>
		/// Indicates that the task has failed for the specified reason.
		/// </summary>
		/// <param name="reason"></param>
		void SetFailed(string reason);
	}
}
#endif

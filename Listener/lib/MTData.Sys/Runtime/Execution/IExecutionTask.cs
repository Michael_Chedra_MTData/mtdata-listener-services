#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Execution
{
	public delegate void ExecutionTaskStateChangeDelegate(IExecutionTask sender, ExecutionState newState);
	public delegate void ExecutionTaskNotifyDelegate(IExecutionTask sender);
	public delegate void ExecutionTaskNotifyTextDelegate(IExecutionTask sender, string message);
	public delegate void ExecutionTaskNotifyErrorDelegate(IExecutionTask sender, string message, Exception ex);
	public delegate void ExecutionTaskNotifyProgressDelegate(IExecutionTask sender, int value);


	/// <summary>
	/// This is the Task interface that allows monitoring of the task and it's execution.
	/// </summary>
	public interface IExecutionTask
	{
		/// <summary>
		/// This will notify the provider that the task should be executed.
		/// </summary>
		void Submit();
		
		/// <summary>
		/// Cancel the report through a user action
		/// </summary>
		void Cancel();

        /// <summary>
        /// Abort the report through a service action
        /// </summary>
        void Abort();

        /// <summary>
		/// This is the state of execution of the report.
		/// </summary>
		ExecutionState State
		{
			get;
		}

		/// <summary>
		/// This ID is a handle to the executing context. It is unique accross all execution contexts and
		/// is assigned by the report service.
		/// </summary>
		int ID
		{
			get;
		}

		/// <summary>
		/// Indicates the progress percentage within the task.
		/// </summary>
		int Progress
		{
			get;
		}

		/// <summary>
		/// This property allows access to the context that was submitted with the task.
		/// </summary>
		object Context
		{
			get;
		}

		/// <summary>
		/// This event is raised whenever a state change occurs.
		/// </summary>
		event ExecutionTaskStateChangeDelegate StateChanged;

		/// <summary>
		/// Indicates that the task is complete
		/// </summary>
		event ExecutionTaskNotifyDelegate Complete;

		/// <summary>
		/// This event is raised whenever the Cancel method is called to cancel the request.
		/// </summary>
		event ExecutionTaskNotifyDelegate Cancelled;

		/// <summary>
		/// This event is raised whenever execution is aborted pre-maturely by the provider
		/// </summary>
		event ExecutionTaskNotifyDelegate Aborted;

		/// <summary>
		/// Event that indicates that the task has failed.
		/// </summary>
		event ExecutionTaskNotifyTextDelegate Failed;

		/// <summary>
		/// This is raised whenever information is passed back by the delegate
		/// </summary>
		event ExecutionTaskNotifyTextDelegate Info;

		/// <summary>
		/// This event is raised whenever there is a warning issued by the delegate
		/// </summary>
		event ExecutionTaskNotifyTextDelegate Warning;

		/// <summary>
		/// This event is raised whenever there is an exception
		/// </summary>
		event ExecutionTaskNotifyErrorDelegate Error;

		/// <summary>
		/// This event will be raised whenever there is a change to the progress.
		/// </summary>
		event ExecutionTaskNotifyProgressDelegate ProgressChanged;

	}
}
#endif

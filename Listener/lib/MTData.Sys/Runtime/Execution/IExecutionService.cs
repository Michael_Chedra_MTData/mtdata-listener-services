#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Execution
{
	public delegate void ExecutionServiceFinishedDelegate(string provider, IExecutionTask context);
	public delegate void ExecutionServiceFailedDelegate(string provider, IExecutionTask context, string reason);

	/// <summary>
	/// This interface represents the methods available to anything requiring a named executiuon provider.
	/// </summary>
	public interface IExecutionService
	{
		/// <summary>
		/// Get a named execution provider
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		//IExecutionProvider GetProvider(string name);

		/// <summary>
		/// This method will take a delegate and queue/execute the method.
		/// The returned Task interface allows the task execution to be monitored, and
		/// cancelled if required.
		/// </summary>
		/// <param name="taskCallback">This is the delegate to call</param>
		/// <param name="context">This is the context information to be passed to the delegate</param>
		/// <param name="name">This is an optional name for the task</param>
		/// <returns></returns>
		IExecutionTask CreateTask(string provider, ExecutionProviderTaskDelegate taskCallback, object context, string name);

		/// <summary>
		/// This method will allow any method with no parameters to be processed as a task.
		/// </summary>
		/// <param name="nullTaskCallback"></param>
		/// <param name="name">This is an optional name for the task</param>
		/// <returns></returns>
		IExecutionTask CreateTask(string provider, ExecutionProviderNullDelegate nullTaskCallback, string name);

		/// <summary>
		/// This event is raised whenever a context is finished execution
		/// </summary>
		event ExecutionServiceFinishedDelegate Complete;

		/// <summary>
		/// The event is raised when the execution of the context is cancelled
		/// </summary>
		event ExecutionServiceFinishedDelegate Cancelled;

		/// <summary>
		/// This event is raised whenever the execution has failed.
		/// </summary>
		event ExecutionServiceFailedDelegate Failed;
	}
}
#endif

#if !NETCF && !NETCF2
using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;
using MTData.Sys.Loader;
using MTData.Sys.Services;

namespace MTData.Sys.Runtime.Execution
{
	/// <summary>
	/// This class will provide access to a number of named execution models to allow 
	/// multiple executions, or queued execution as a service to other contexts.
	/// </summary>
	public class ExecutionService : ServiceBaseClass, IExecutionService
	{
		private Dictionary<string, IExecutionProvider> _providers = new Dictionary<string, IExecutionProvider>();

		public static IExecutionService GetExecutionService()
		{
			return GetExecutionService("ExecutionService");
		}

		public static IExecutionService GetExecutionService(string name)
		{
			return GetExecutionService(ServicesList.Instance, name);
		}

		public static IExecutionService GetExecutionService(ServicesList list, string name)
		{
			IService service = list[name];
			if (service == null)
				throw new ServiceNotFoundException(name);
			if (!(service is IExecutionService))
				throw new ServiceInterfaceNotSupportedException(name, "IExecutionService");
			return service as IExecutionService;
		}

		public ExecutionService()
			: base("ExecutionService")
		{

		}

		internal event ExecutionServiceFinishedDelegate Complete;
		internal event ExecutionServiceFinishedDelegate Cancelled;
		internal event ExecutionServiceFailedDelegate Failed;

		protected override void AddProvider(IProvider provider)
		{
			IExecutionProvider executionProvider = provider as IExecutionProvider;
			executionProvider.Complete += new ExecutionProviderFinishedDelegate(executionProvider_Complete);
			executionProvider.Cancelled += new ExecutionProviderFinishedDelegate(executionProvider_Cancelled);
			executionProvider.Failed += new ExecutionProviderFailedDelegate(executionProvider_Failed);
			base.AddProvider(provider);
		}

		void executionProvider_Failed(IExecutionProvider provider, IExecutionTask context, string reason)
		{
			if (Failed != null)
				Failed(provider.Name, context, reason);
		}

		void executionProvider_Cancelled(IExecutionProvider provider, IExecutionTask context)
		{
			if (Cancelled != null)
				Cancelled(provider.Name, context);
		}

		void executionProvider_Complete(IExecutionProvider provider, IExecutionTask context)
		{
			if (Complete != null)
				Complete(provider.Name, context);
		}

		private IExecutionProvider GetExecutionProvider(string provider)
		{
			IProvider providerImplementation = base.GetProvider(provider);
			if (providerImplementation == null)
				throw new ServiceProviderNotFoundException(Name, provider);
			if (!(providerImplementation is IExecutionProvider))
				throw new ProviderInterfaceNotSupportedException(Name, "IExecutionProvider", provider);
			return providerImplementation as IExecutionProvider;
		}

		#region IExecutionService Members

		IExecutionTask IExecutionService.CreateTask(string provider, ExecutionProviderTaskDelegate taskCallback, object context, string name)
		{
			return GetExecutionProvider(provider).CreateTask(taskCallback, context, name);
		}

		IExecutionTask IExecutionService.CreateTask(string provider, ExecutionProviderNullDelegate nullTaskCallback, string name)
		{
			return GetExecutionProvider(provider).CreateTask(nullTaskCallback, name);
		}

		event ExecutionServiceFinishedDelegate IExecutionService.Complete
		{
			add
			{
				Complete += value;
			}
			remove
			{
				Complete -= value;
			}
		}

		event ExecutionServiceFinishedDelegate IExecutionService.Cancelled
		{
			add
			{
				Cancelled += value;
			}
			remove
			{
				Cancelled -= value;
			}
		}

		event ExecutionServiceFailedDelegate IExecutionService.Failed
		{
			add
			{
				Failed += value;
			}
			remove
			{
				Failed -= value;
			}
		}

		#endregion
	}
}
#endif

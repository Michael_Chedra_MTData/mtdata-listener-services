using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Hosting
{
	/// <summary>
	/// This interface will implement the basic methods that a service host
	/// requires to host a guest implementation
	/// </summary>
	public interface IGuest
	{
		/// <summary>
		/// Start the ServiceGuest
		/// </summary>
		void Start();

		/// <summary>
		/// Pause execution in the service guest.
		/// </summary>
		void Pause();

		/// <summary>
		/// Resume execution in the service guest
		/// </summary>
		void Resume();

		/// <summary>
		/// Stop execution in the service guest
		/// </summary>
		/// <param name="abnormalShutdownEx">This is the exception causing the shutdown if it is an abnormal shutdown</param>
		void Stop(Exception abnormalShutdownEx);
	}
}
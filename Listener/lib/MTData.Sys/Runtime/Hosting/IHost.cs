using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Hosting
{
	/// <summary>
	/// This will be passed to the GuestFactory to allow any required callbacks.
	/// </summary>
	public interface IHost
	{
		/// <summary>
		/// Causes the host to stop activity
		/// </summary>
		/// <param name="reason"></param>
		void Stop(string reason);

		/// <summary>
		/// Causes the host to restart activity
		/// </summary>
		/// <param name="reason"></param>
		void Restart(string reason);

		/// <summary>
		/// Causes the host to log a message
		/// </summary>
		/// <param name="message"></param>
		void LogEventMessage(string message);

		/// <summary>
		/// Causes the host to log an error message.
		/// </summary>
		/// <param name="message"></param>
		void LogEventError(string message, Exception ex);

		/// <summary>
		/// This is the name of the host
		/// </summary>
		string Name
		{
			get;
		}
	}
}

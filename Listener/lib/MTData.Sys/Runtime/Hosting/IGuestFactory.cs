using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Hosting
{
	/// <summary>
	/// This factory interface will support the creation of code to 
	/// be hosted by the host.
	/// </summary>
	public interface IGuestFactory
	{
		/// <summary>
		/// Create an instance of the guest implementation.
		/// </summary>
		/// <param name="host"></param>
		/// <param name="args"></param>
		/// <returns></returns>
		IGuest Create(IHost host, string[] args);
	}
}

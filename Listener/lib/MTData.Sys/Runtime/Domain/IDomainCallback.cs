#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	public enum DomainTaskStatus : int
	{
		Unknown,
		Started,
		Aborted,
		Failed,
		Complete
	}


	public interface IDomainCallback
	{
		void TaskComplete(DomainHost sender, string taskID, object result);
		void TaskError(DomainHost sender, string taskID, string message, Exception ex);
		void TaskInfo(DomainHost sender, string taskID, string message);
		void TaskStatusChanged(DomainHost sender, string taskID, DomainTaskStatus status);
		void TaskMessageOut(DomainHost sender, string taskID, byte[] messageData);
	}
}
#endif
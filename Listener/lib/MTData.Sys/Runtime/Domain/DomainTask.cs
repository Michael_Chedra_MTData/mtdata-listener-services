#if !NETCF && !NETCF2
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	[Serializable]
	public struct DomainTask
	{
		public string TaskID;
		public string Config;
		public string TypeName;
		public string Description;
	}

	[Serializable]
	public struct DomainTaskState
	{
		public bool Hosted;
		public DomainTask Task;
		public DomainTaskStatus Status;
	}
}
#endif
#if !NETCF && !NETCF2
using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using log4net;
using log4net.Config;
using System.Runtime.Serialization.Formatters.Binary;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This class will be hosted within an AppDomain and will receive
	/// all interaction from the DomainManager
	/// </summary>
	public class DomainHost : MarshalByRefObject
	{
		private ILog _log = null;

		private MTData.Sys.Loader.Loader _loader;
		private BinaryFormatter _binaryFormatter = new BinaryFormatter();

		public DomainHost()
		{
			OnInfo("", MTData.Sys.Services.ServicesList.Instance.Count.ToString());
			XmlConfigurator.Configure(new FileInfo(AppDomain.CurrentDomain.SetupInformation.ConfigurationFile));
			_log = LogManager.GetLogger(typeof(DomainHost));
			if (_log.IsInfoEnabled)
			    _log.InfoFormat("DomainHost for domain '{0}' Starting", AppDomain.CurrentDomain.FriendlyName);

			_loader = new MTData.Sys.Loader.Loader();
		}

		public string DomainName
		{
			get
			{
				return AppDomain.CurrentDomain.FriendlyName;
			}
		}

		private IDomainCallback _callback;
		public IDomainCallback Callback
		{
			get
			{
				return _callback;
			}
			set
			{
				_callback = value;
			}
		}

		private void OnInfo(string taskID, string message)
		{
			if (_callback != null)
				_callback.TaskInfo(this, taskID, message);
		}

		#region Error Handling

		private void OnError(string taskID, string message, Exception ex)
		{

			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' {1} Error '{2}' {3}", AppDomain.CurrentDomain.FriendlyName, taskID, message, ex);

			if (_callback != null)
				_callback.TaskError(this, taskID, message, ex);
		}

		#endregion

		private Dictionary<string, HostedTask> _tasks = new Dictionary<string, HostedTask>();

		#region Task Execution

		public void ExecuteTask(string taskID, string typeName, string config)
		{
			DomainTask task = new DomainTask();
			task.TaskID = taskID;
			task.TypeName = typeName;
			task.Config = config;
			ExecuteTask(task);
		}

		public void ExecuteTask(DomainTask task)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' Execute Task '{1}' [{2}]", AppDomain.CurrentDomain.FriendlyName, task.TaskID, task.TypeName);

			lock (_tasks)
			{
				if (!_tasks.ContainsKey(task.TaskID))
				{
					System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ParameterizedThreadStart(ThreadTask));
					thread.IsBackground = true;
					
					RunnableTask runnableTask = new RunnableTask(task, (IRunnable)CreateInstance(task.TypeName, task.Config), thread);
					Unsafe_AddTask(runnableTask);
					thread.Start(runnableTask);
				}
			}
		}

		private void Unsafe_AddTask(HostedTask task)
		{
			task.StatusChanged += new HostedTask.HostedTaskChangedDelegate(task_StatusChanged);
			task.MessageOut += new HostedTask.HostedTaskMessageDelegate(task_MessageOut);
			_tasks.Add(task.Task.TaskID, task);
		}

		private HostedTask Unsafe_GetTaskFromID(string taskID)
		{
			HostedTask result = null;
			if (_tasks.TryGetValue(taskID, out result))
				return result;
			return null;
		}

		private void Unsafe_RemoveTask(string taskID)
		{
			HostedTask task = Unsafe_GetTaskFromID(taskID);
			if (task != null)
				Unsafe_RemoveTask(task);
		}

		private void Unsafe_RemoveTask(HostedTask task)
		{
			_tasks.Remove(task.Task.TaskID);
			task.StatusChanged -= new HostedTask.HostedTaskChangedDelegate(task_StatusChanged);
			task.MessageOut -= new HostedTask.HostedTaskMessageDelegate(task_MessageOut);
			task.Dispose();
		}

		void task_MessageOut(DomainHost.HostedTask hostedTask, object sender, object message)
		{
			OnTaskMessageOut(hostedTask.Task.TaskID, message);
		}

		void task_StatusChanged(DomainHost.HostedTask hostedTask)
		{
			OnTaskStatusChanged(hostedTask);
		}

		#region HostedTask

		private abstract class HostedTask : IDisposable
		{
			private DomainTask _task;
			private object _taskInstance;

			public HostedTask(DomainTask task, object taskInstance)
			{
				_task = task;
				_taskInstance = taskInstance;

				if (_taskInstance is IMessageSource)
					((IMessageSource)_taskInstance).MessageOut += new MessageDelegate(HostedTask_MessageOut);
			}

			public delegate void HostedTaskChangedDelegate(HostedTask hostedTask);

			public delegate void HostedTaskMessageDelegate(HostedTask hostedTask, object sender, object message);

			public event HostedTaskMessageDelegate MessageOut;

			void HostedTask_MessageOut(object sender, object message)
			{
				if (MessageOut != null)
					MessageOut(this, sender, message);
			}

			private void OnStatusChanged()
			{
				if (StatusChanged != null)
					StatusChanged(this);
			}

			public event HostedTaskChangedDelegate StatusChanged;

			public DomainTask Task
			{
				get{ return _task; }
			}

			private DomainTaskStatus _status = DomainTaskStatus.Unknown;

			public DomainTaskStatus Status
			{
				get
				{
					return _status;
				}
				set
				{
					if (value != _status)
					{
						_status = value;
						OnStatusChanged();
					}
				}
			}

			public object TaskInstance
			{
				get
				{
					return _taskInstance;
				}
			}

			public bool IsMessageSink
			{
				get
				{
					return (_taskInstance is IMessageSink);
				}
			}

			public void MessageIn(object message)
			{
				if (IsMessageSink)
					((IMessageSink)_taskInstance).MessageIn(message);
			}

			public bool IsResultsProvider
			{
				get
				{
					return (_taskInstance is IResultProvider);
				}
			}

			public object Result
			{
				get
				{
					if (IsResultsProvider)
						return ((IResultProvider)_taskInstance).Result;
					else
						return null;
				}
			}

			public abstract void Abort();

			#region IDisposable Members

			public void Dispose()
			{
				if (_taskInstance != null)
				{
					if (_taskInstance is IMessageSource)
						((IMessageSource)_taskInstance).MessageOut -= new MessageDelegate(HostedTask_MessageOut);

					if (_taskInstance is IDisposable)
						((IDisposable)_taskInstance).Dispose();
					_taskInstance = null;
				}
			}

			#endregion
		}

		#endregion

		#region Runnable Task

		private class RunnableTask : HostedTask
		{
			public RunnableTask(DomainTask task, object taskInstance, System.Threading.Thread thread)
				: base(task, taskInstance)
			{
				Thread = thread;
			}

			public IRunnable RunningTask
			{
				get
				{
					return base.TaskInstance as IRunnable;
				}
			}

			public System.Threading.Thread Thread = null;

			public override void Abort()
			{
				if (Thread != null)
					Thread.Abort();
				Thread = null;
			}
		}

		#endregion

		#region Hostable Task

		private class HostableTask : HostedTask
		{
			public HostableTask(DomainTask task, IHostable guest)
				: base(task, guest)
			{
				
			}

			public IHostable Guest
			{
				get
				{
					return base.TaskInstance as IHostable;
				}
			}

			public override void Abort()
			{
				Guest.Stop();
			}
		}

		#endregion

		private void ThreadTask(object param)
		{
			RunnableTask taskEntry = (RunnableTask)param;
			try
			{
				taskEntry.Status = DomainTaskStatus.Started;
				taskEntry.RunningTask.Execute();
				//object result = null;
				taskEntry.Status = DomainTaskStatus.Complete;
				OnTaskComplete(taskEntry);
			}
			catch (System.Threading.ThreadAbortException)
			{
				taskEntry.Status = DomainTaskStatus.Aborted;
			}
			catch (Exception ex)
			{
				OnError(taskEntry.Task.TaskID, "ThreadTask", ex);
				taskEntry.Status = DomainTaskStatus.Failed;
			}
			finally
			{
				lock (_tasks)
					Unsafe_RemoveTask(taskEntry);
			}
		}

		private void OnTaskComplete(HostedTask hostedTask)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' Task Complete '{1}'", AppDomain.CurrentDomain.FriendlyName, hostedTask.Task.TaskID);
			if (_callback != null)
			{
				if (hostedTask.IsResultsProvider)
					_callback.TaskComplete(this, hostedTask.Task.TaskID, hostedTask.Result);
				else
					_callback.TaskComplete(this, hostedTask.Task.TaskID, null);
			}
		}

		#endregion

		#region status Reporting

		private void OnTaskStatusChanged(HostedTask task)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' Task Status Changed '{1}' [{2}]", AppDomain.CurrentDomain.FriendlyName, task.Task.TaskID, task.Status);
			if (_callback != null)
				_callback.TaskStatusChanged(this, task.Task.TaskID, task.Status);
		}

		private void OnTaskMessageOut(string taskID, object message)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' Task '{1}' Message Out '{2}'", AppDomain.CurrentDomain.FriendlyName, taskID, message);

			try
			{
				MemoryStream stream = new MemoryStream();
				_binaryFormatter.Serialize(stream, message);
				OnTaskMessageOut(taskID, stream.ToArray());
				stream.Close();
			}
			catch (Exception ex)
			{
				if (_log.IsErrorEnabled)
					_log.Error(string.Format("DomainHost for domain '{0}' Task '{1}' Error", AppDomain.CurrentDomain.FriendlyName, taskID), ex);
			}

		}

		private void OnTaskMessageOut(string taskID, byte[] messageData)
		{
			if (_callback != null)
				_callback.TaskMessageOut(this, taskID, messageData);
		}

		public void TaskMessageIn(string taskID, byte[] messageData)
		{
			MemoryStream stream = new MemoryStream(messageData);
			object message = _binaryFormatter.Deserialize(stream);

			HostedTask hostedTask = null;
			lock(_tasks)
				if (_tasks.TryGetValue(taskID, out hostedTask))
				{
					if (hostedTask.IsMessageSink)
						hostedTask.MessageIn(message);
				}
		}

		#endregion

		#region Hosting

		public void StartGuest(string taskID, string typeName, string config)
		{
			DomainTask task = new DomainTask();
			task.TaskID = taskID;
			task.TypeName = typeName;
			task.Config = config;
			StartGuest(task);
		}

		public void StartGuest(DomainTask task)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' StartGuest '{1}' [{2}]", AppDomain.CurrentDomain.FriendlyName, task.TaskID, task.TypeName);

			lock(_tasks)
				if (_tasks.ContainsKey(task.TaskID))
					throw new Exception(string.Format("DomainHost for domain '{0}' : Task {1}, typename {2} is already running", AppDomain.CurrentDomain.FriendlyName, task.TaskID, task.TypeName));

			IHostable guest = (IHostable)CreateInstance(task.TypeName, task.Config);
			HostableTask taskEntry = new HostableTask(task, guest);

			lock (_tasks)
				Unsafe_AddTask(taskEntry);

			guest.Start();
			taskEntry.Status = DomainTaskStatus.Started;
		}

		public void StopGuest(string taskID)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("DomainHost for domain '{0}' StopGuest '{1}'", AppDomain.CurrentDomain.FriendlyName, taskID);

			HostedTask taskEntry = null;
			lock (_tasks)
				if (_tasks.TryGetValue(taskID, out taskEntry))
				{
					if (taskEntry is HostableTask)
						((HostableTask)taskEntry).Guest.Stop();
					taskEntry.Status = DomainTaskStatus.Complete;
					Unsafe_RemoveTask(taskEntry);
					OnTaskComplete(taskEntry);
				}
		}

		public void StopAllGuests()
		{
			List<HostedTask> tasks = new List<HostedTask>();
			lock (_tasks)
			{
				foreach (HostedTask hostedTask in _tasks.Values)
				{
					tasks.Add(hostedTask);
					hostedTask.Abort();
				}
				foreach (HostedTask task in tasks)
				{
					task.Status = DomainTaskStatus.Aborted;
					Unsafe_RemoveTask(task);
				}
			}
		}

		public DomainTaskState[] GetHostedTasks()
		{
			List<DomainTaskState> tasks = new List<DomainTaskState>();
			lock (_tasks)
				foreach (HostedTask hostedTask in _tasks.Values)
				{
					DomainTaskState state = new DomainTaskState();
					state.Status = hostedTask.Status;
					state.Task = hostedTask.Task;
					state.Hosted = (hostedTask is HostableTask);
					tasks.Add(state);
				}
			return tasks.ToArray();
		}

		#endregion

		#region Supporting Methods

		/// <summary>
		/// Create the task instance
		/// </summary>
		/// <param name="typeName"></param>
		/// <param name="config"></param>
		/// <returns></returns>
		private object CreateInstance(string typeName, string config)
		{
			object task = null;

			XmlDocument document = null;
			if (config != null)
			{
				document = new XmlDocument();
				document.LoadXml(config);

				task = _loader.Create(this, typeName, document.DocumentElement);
			}
			else
				task = _loader.Create(this, typeName, null);
			return task;
		}

		#endregion

	}
}
#endif
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This interface is implemented by tasks that want to be hosted
	/// in the grid.
	/// </summary>
	public interface IHostable
	{
		/// <summary>
		/// Start the task
		/// </summary>
		void Start();

		/// <summary>
		/// Stop the task
		/// </summary>
		void Stop();
	}
}

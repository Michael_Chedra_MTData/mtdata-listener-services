#if !NETCF && !NETCF2
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using log4net;
using System.Security;
using System.Security.Permissions;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This class will be responsible for all actions within an 
	/// execution/hosting domain
	/// </summary>
	public class DomainManager : MarshalByRefObject, IDisposable, IDomainCallback
	{
		private static ILog _log = LogManager.GetLogger(typeof(DomainManager));

		private string _basePath = ".";

		public DomainManager(string basePath)
		{
			_basePath = basePath;
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager Starting; BasePath = {0}", basePath);
			FileIOPermission fileIOPermission = new FileIOPermission(PermissionState.Unrestricted);
			fileIOPermission.Assert();

//			SecurityPermission permission = new SecurityPermission(PermissionState.Unrestricted);
//			permission.Assert();
		}

		private class ActiveDomainEntry
		{
			public string Name;
			public AppDomain Domain;
			public DomainHost Host;
		}

		private Dictionary<string, ActiveDomainEntry> _domains = new Dictionary<string, ActiveDomainEntry>();

		private string GetDomainPath(string domainName)
		{
			return Path.Combine(_basePath, domainName);
		}

		/// <summary>
		/// This will iterate base-path and load the appropriate domains.
		/// </summary>
		public DomainDetails GetDomainDetails(string domainName)
		{
			string domainPath = Path.Combine(_basePath, domainName);
			if (Directory.Exists(domainPath))
			{
				DomainDetails result = new DomainDetails();
				result.DomainName = domainName;

				string[] fileNames = Directory.GetFiles(GetDomainPath(domainName));
				foreach(string fileName in fileNames)
				{
					FileDetails file = new FileDetails();
					file.FileName = Path.GetFileName(fileName);
					FileInfo fileInfo = new FileInfo(fileName);
					file.FileSize = fileInfo.Length;
					file.FileTimeUTC = fileInfo.LastWriteTimeUtc;

					result.AddFile(file);
				}
				return result;
			}
			else
				return null;
		}

		public void UpdateDomain(DomainDetails domainDetails)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Updating domainDetails for {0}", domainDetails.DomainName);

			string domainPath = GetDomainPath(domainDetails.DomainName);
			if (!Directory.Exists(domainPath))
				Directory.CreateDirectory(domainPath);

			CleanDirectory(domainDetails, domainPath, domainPath);
		}

		private string GetRelativeFilePath(string domainPath, string fileName)
		{
			return fileName.Substring(domainPath.Length + 1, fileName.Length - domainPath.Length - 1);
		}

		private void CleanDirectory(DomainDetails domainDetails, string domainPath, string directory)
		{
			//	iterate the files, and if any of them are not matching the details, delete them.
			string[] fileNames = Directory.GetFiles(directory);
			foreach(string fileName in fileNames)
			{
				FileDetails fileDetails = domainDetails[GetRelativeFilePath(domainPath, fileName)];
				if ((fileDetails == null) || ((fileDetails != null) && (File.GetLastWriteTimeUtc(fileName) != fileDetails.FileTimeUTC)))
					File.Delete(fileName);
			}

			string[] directories = Directory.GetDirectories(directory);
			foreach(string subDirectory in directories)
				CleanDirectory(domainDetails, domainPath, subDirectory);
		}

		public void SetDomainFile(string domainName, FileDetails fileDetails, byte[] fileData)
		{
			string domainPath = Path.Combine(_basePath, domainName);
			if (!Directory.Exists(domainPath))
				Directory.CreateDirectory(domainPath);

			string filePath = Path.Combine(domainPath, fileDetails.FileName);
			string fileDirectory = Path.GetDirectoryName(filePath);
			if (!Directory.Exists(fileDirectory))
				Directory.CreateDirectory(fileDirectory);

			if (File.Exists(filePath))
				File.Delete(filePath);

			File.WriteAllBytes(filePath, fileData);
			File.SetLastWriteTimeUtc(filePath, fileDetails.FileTimeUTC);
		}

		public byte[] GetDomainFile(string domainName, FileDetails fileDetails)
		{
			string domainPath = Path.Combine(_basePath, domainName);
			if (!Directory.Exists(domainPath))
				throw new DirectoryNotFoundException(string.Format("Directory {0} not found", domainPath));

			string filePath = Path.Combine(domainPath, fileDetails.FileName);
			string fileDirectory = Path.GetDirectoryName(filePath);
			if (!Directory.Exists(fileDirectory))
				throw new DirectoryNotFoundException(string.Format("Directory {0} not found", fileDirectory));

			byte[] fileData = File.ReadAllBytes(filePath);
			return fileData;
		}

		public bool IsDomainActive(string domainName)
		{
			return _domains.ContainsKey(domainName);
		}

		public enum DomainStatus
		{
			Inactive,
			Active
		}

		public delegate void DomainStatusChangedDelegate(DomainManager manager, string domainName, DomainStatus status);

		public event DomainStatusChangedDelegate DomainStatusChanged;

		private void OnDomainStatusChanged(string domainName, DomainStatus status)
		{
			if (DomainStatusChanged != null)
				DomainStatusChanged(this, domainName, status);
		}

		public void ActivateDomain(string domainName, string configFilePath)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Activating Domain {0} [{1}]", domainName, configFilePath);
			ActiveDomainEntry entry = new ActiveDomainEntry();
			entry.Name = domainName;

			AppDomainSetup setup = new AppDomainSetup();
			setup.ApplicationBase = Path.Combine(_basePath, domainName);
			if (configFilePath != null)
				setup.ConfigurationFile = configFilePath;

			System.Security.Policy.Evidence evidence = new System.Security.Policy.Evidence();
			evidence.Merge(System.Reflection.Assembly.GetExecutingAssembly().Evidence);
			
			entry.Domain = AppDomain.CreateDomain(domainName, evidence, setup);
			entry.Host = (DomainHost)entry.Domain.CreateInstanceAndUnwrap("MTData.Sys", "MTData.Sys.Runtime.Domain.DomainHost");
			entry.Host.Callback = this;

			_domains.Add(domainName, entry);

			OnDomainStatusChanged(domainName, DomainStatus.Active);
		}

		public delegate void DomainErrorDelegate(DomainManager manager, string domainName, string taskID, string message, Exception ex);

		public event DomainErrorDelegate DomainTaskError;

		void Host_TaskError(DomainHost sender, string taskID, string message, Exception ex)
		{
			if (DomainTaskError != null)
				DomainTaskError(this, sender.DomainName, taskID, message, ex);
		}

		public delegate void DomainTaskCompleteDelegate(DomainManager manager, string domainName, string taskID, object result);

		public event DomainTaskCompleteDelegate DomainTaskComplete;

		void Host_TaskComplete(DomainHost sender, string taskID, object result)
		{
			if (DomainTaskComplete != null)
				DomainTaskComplete(this, sender.DomainName, taskID, result);
		}

		public delegate void DomainTaskStatusChangeDelegate(DomainManager manager, string domainName, string taskID, DomainTaskStatus status);

		public event DomainTaskStatusChangeDelegate DomainTaskStatusChanged;

		void Host_TaskStatusChanged(DomainHost sender, string taskID, DomainTaskStatus status)
		{
			if (DomainTaskStatusChanged != null)
				DomainTaskStatusChanged(this, sender.DomainName, taskID, status);
		}

		public delegate void DomainTaskMessageOutDelegate(DomainManager manager, string domainName, string taskID, byte[] messageData);

		public event DomainTaskMessageOutDelegate DomainTaskMessageOut;

		void Host_TaskMessageOut(DomainHost sender, string taskID, byte[] messageData)
		{
			if (DomainTaskMessageOut != null)
				DomainTaskMessageOut(this, sender.DomainName, taskID, messageData);
		}

		public void TaskMessageIn(string domainName, string taskID, byte[] messageData)
		{
			if (_log.IsDebugEnabled)
				_log.DebugFormat("Domain Manager : Task Message In {0} {1} [{2}]", domainName, taskID, BitConverter.ToString(messageData));
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Task Message In {0} {1} {2} bytes", domainName, taskID, messageData.Length);
			ActiveDomainEntry entry = null;
			if (_domains.TryGetValue(domainName, out entry))
			{
				if (entry.Host != null)
					entry.Host.TaskMessageIn(taskID, messageData);
			}
		}

		public void ReleaseDomain(string domainName)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Releasing Domain {0}", domainName);
			ActiveDomainEntry entry = null;
			if (_domains.TryGetValue(domainName, out entry))
			{
				if (entry.Host != null)
					entry.Host.StopAllGuests();
				_domains.Remove(domainName);
				AppDomain.Unload(entry.Domain);
				entry.Domain = null;
			}
			OnDomainStatusChanged(domainName, DomainStatus.Inactive);
		}

		public void ExecuteTask(string domainName, string taskID, string typeName, string config)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Execute Task {0} {1} [{2}]", domainName, taskID, typeName);

			ActiveDomainEntry entry = null;
			if (_domains.TryGetValue(domainName, out entry))
			{
				entry.Host.ExecuteTask(taskID, typeName, config);
			}
			else
				throw new Exception(string.Format("Could not find domain '{0}'", domainName));
		}

		public void StartGuest(string domainName, string taskID, string typeName, string config)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Start Guest {0} {1} [{2}]", domainName, taskID, typeName);
			ActiveDomainEntry entry = null;
			if (_domains.TryGetValue(domainName, out entry))
			{
				entry.Host.StartGuest(taskID, typeName, config);
			}
			else
				throw new Exception(string.Format("Could not find domain '{0}'", domainName));
		}

		public void StopGuest(string domainName, string taskID)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Stop Guest {0} {1}", domainName, taskID);
			ActiveDomainEntry entry = null;
			if (_domains.TryGetValue(domainName, out entry))
			{
				entry.Host.StopGuest(taskID);
			}
			else
				throw new Exception(string.Format("Could not find domain '{0}'", domainName));
		}

		public void TaskComplete(DomainHost sender, string taskID, object result)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Task Complete {0} {1}", sender.DomainName, taskID);
			Host_TaskComplete(sender, taskID, result);
		}

		public void TaskError(DomainHost sender, string taskID, string message, Exception ex)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Task Error {0} {1} '{2}' : {3}", sender.DomainName, taskID, message, ex);
			Host_TaskError(sender, taskID, message, ex);
		}

		public void TaskInfo(DomainHost sender, string taskID, string message)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Task Info {0} {1} '{2}'", sender.DomainName, taskID, message);
		}

		public void TaskStatusChanged(DomainHost sender, string taskID, DomainTaskStatus status)
		{
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Task State Changed {0} {1} '{2}'", sender.DomainName, taskID, status);
			Host_TaskStatusChanged(sender, taskID, status);
		}

		public void TaskMessageOut(DomainHost sender, string taskID, byte[] messageData)
		{
			if (_log.IsDebugEnabled)
				_log.DebugFormat("Domain Manager : Task Message Out {0} {1} [{2}]", sender.DomainName, taskID, BitConverter.ToString(messageData));
			if (_log.IsInfoEnabled)
				_log.InfoFormat("Domain Manager : Task Message Out {0} {1} {2} bytes", sender.DomainName, taskID, messageData.Length);
			Host_TaskMessageOut(sender, taskID, messageData);
		}

		public string[] GetActiveDomains()
		{
			List<string> result = new List<string>();
			foreach (ActiveDomainEntry entry in _domains.Values)
				result.Add(entry.Name);
			return result.ToArray();
		}

		public DomainTaskState[] GetHostedTasks(string domainName)
		{
			ActiveDomainEntry domain = null;
			if (_domains.TryGetValue(domainName, out domain))
				if (domain.Host != null)
					return domain.Host.GetHostedTasks();
			return null;
		}

		#region IDisposable Members

		public void Dispose()
		{
			List<ActiveDomainEntry> entries = new List<ActiveDomainEntry>();
			foreach (ActiveDomainEntry entry in _domains.Values)
				entries.Add(entry);

			foreach (ActiveDomainEntry entry in entries)
				ReleaseDomain(entry.Name);

			//CodeAccessPermission.RevertAssert();	//	Security Permission
			//CodeAccessPermission.RevertAssert();	//	File IO Permission
		}

		#endregion
	}
}
#endif
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	public delegate void MessageDelegate(object sender, object message);

	/// <summary>
	/// This interface will be implemented by any object that supports custom messages
	/// and will be processed by the DomainHost and passed to the domain manager.
	/// </summary>
	public interface IMessageSource
	{
		event MessageDelegate MessageOut;
	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	public class FileDetails
	{
		private string _fileName;
		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				_fileName = value;
			}
		}

		private DateTime _fileTimeUTC;
		public DateTime FileTimeUTC
		{
			get
			{
				return _fileTimeUTC;
			}
			set
			{
				_fileTimeUTC = value;
			}
		}

		private long _fileSize;
		public long FileSize
		{
			get
			{
				return _fileSize;
			}
			set
			{
				_fileSize = value;
			}
		}

		public override string ToString()
		{
			return string.Format("{0} [{1}] {2}", _fileName, _fileTimeUTC, _fileSize);
		}
	}
}

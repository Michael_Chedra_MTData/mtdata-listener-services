using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This class will be populated with details about the domain name, config, 
	/// and the filesystem files involved in it,
	/// or required for it.
	/// </summary>
	public class DomainDetails
	{
		private string _domainName;
		public string DomainName
		{
			get
			{
				return _domainName;
			}
			set
			{
				_domainName = value;
			}
		}

		/// <summary>
		/// This is vcalculated based on the list of files.. if there is a file
		/// with the domain name, and a .config extension, it is used as the config file.
		/// </summary>
		public string ConfigFilePath
		{
			get
			{
				string result = null;

				if (!String.IsNullOrEmpty(_domainName))
				{
					string test = string.Format("{0}.config", _domainName);

					foreach (FileDetails file in _files)
					{
						if (file.FileName == test)
						{
							result = test;
							break;
						}
					}
				}

				return result;
			}
		}

		private List<FileDetails> _files = new List<FileDetails>();
		public int FileCount
		{
			get
			{
				return _files.Count;
			}
		}

		public FileDetails this[int index]
		{
			get
			{
				return _files[index];
			}
		}

		public void AddFile(FileDetails file)
		{
			_files.Add(file);
		}

		public FileDetails this[string fileName]
		{
			get
			{
				foreach (FileDetails file in _files)
					if (file.FileName == fileName)
						return file;

				return null;
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("DomainName : ");
			builder.Append(_domainName);
			builder.Append("; Files :");
			foreach (FileDetails file in _files)
			{
				builder.Append(file.ToString());
				builder.Append("; ");
			}

			return builder.ToString();
		}
	}
}

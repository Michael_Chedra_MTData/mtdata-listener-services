using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This is a task that will be processed individually
	/// and the result returned.
	/// </summary>
	public interface IRunnable
	{
		/// <summary>
		/// Run the task concerned given the config provided.
		/// </summary>
		void Execute();
	}
}

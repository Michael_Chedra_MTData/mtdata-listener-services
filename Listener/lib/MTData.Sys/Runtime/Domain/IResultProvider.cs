using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This interface is implemented by any runnable objects that will provide an output of some kind
	/// </summary>
	public interface IResultProvider
	{
		/// <summary>
		/// This is the result object from the runnable task
		/// </summary>
		object Result
		{
			get;
		}
	}
}

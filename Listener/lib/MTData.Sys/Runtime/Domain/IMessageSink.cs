using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Runtime.Domain
{
	/// <summary>
	/// This interface will be implemented by any object that supports being sent custom messages.
	/// </summary>
	public interface IMessageSink
	{
		void MessageIn(object message);
	}
}

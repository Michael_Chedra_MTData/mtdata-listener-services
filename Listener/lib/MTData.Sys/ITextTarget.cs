using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys
{
	public interface ITextTarget
	{
		string Text { get; set; }
	}
}

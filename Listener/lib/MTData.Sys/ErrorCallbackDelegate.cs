using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys
{
	/// <summary>
	/// This delegate will be used to identify all error callbacks.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="message"></param>
	/// <param name="ex"></param>
	public delegate void ErrorCallbackDelegate(object sender, string message, Exception ex);

}

using System;
using System.Collections;
using System.Threading;

namespace MTData.Sys
{
	/// <summary>
	/// CookieTimer is to allow a single timer object to respond
	/// at specified time intervals, and raise an event, returning
	/// a cookie that was provided when specifying a timer event.
	/// </summary>
	public class CookieTimer : IDisposable
	{
		public delegate void CookieTimerEvent(object cookie, ref object data);
		public event CookieTimerEvent CookieTimedOut;

		protected class CookieExpireTime
		{
			public object	Cookie;
			public object	Data;
			public int		ExpireMilliseconds;

			public CookieExpireTime(object cookie, object data, int expireMilliseconds)
			{
				Cookie		= cookie;
				Data		= data;
				ExpireMilliseconds	= expireMilliseconds;
			}

			public int MillisecondsBeforeExpiry(int compareTime)
			{
				return ExpireMilliseconds - compareTime;
			}
		}

		private Thread			_cookieTimerThread;
		protected Hashtable		_cookieTimers = new Hashtable();
		private AutoResetEvent	_responseEvent;
		private bool			_disposed;
		protected object		_lockObj = new object();

		public CookieTimer()
		{
			_responseEvent = new AutoResetEvent(false);
			_cookieTimerThread = new Thread(new ThreadStart(CookieTimerThread));
			_cookieTimerThread.IsBackground = true;
			_cookieTimerThread.Start();
		}

		private void CookieTimerThread()
		{
			do
			{
				CookieExpireTime nextExpireCookie = null;

				lock(_lockObj)
				{
					foreach (CookieExpireTime cookieExpireTime in _cookieTimers.Values)
					{
						if ((nextExpireCookie == null) || (cookieExpireTime.MillisecondsBeforeExpiry(nextExpireCookie.ExpireMilliseconds) < 0))
							nextExpireCookie = cookieExpireTime;
					}

					_responseEvent.Reset();
				}
			
				bool signalled = true;
				if (nextExpireCookie != null)
				{
					int waitTime   = nextExpireCookie.MillisecondsBeforeExpiry(System.Environment.TickCount);
					if (waitTime > 0)
						signalled = _responseEvent.WaitOne(waitTime, false);
					else
						signalled = false;
				}
				else
					_responseEvent.WaitOne();

				if (!signalled)
				{
					// Remove the Timer first, this allows the CookieTimeOut event to add it back if necessary
					lock(_lockObj)
						_cookieTimers.Remove(nextExpireCookie.Cookie);

					try
					{
						if (CookieTimedOut != null)
							CookieTimedOut(nextExpireCookie.Cookie, ref nextExpireCookie.Data);
					}
					catch (Exception ex)
					{
						string exMsg = "";
						if (ex.InnerException != null)
							exMsg = ex.InnerException.ToString();

						exMsg = string.Concat(exMsg, " | ", ex.ToString());

					}
				} 
			} while (!_disposed);
		}

		public void Dispose()
		{
			if (_disposed)
				return;

			_disposed = true;

			_responseEvent.Set();
		}

		/// <summary>
		/// Add a new item to the cookie timer list, with the
		/// specified time to elapse before raising an event, 
		/// in the case that the item does not get cleared.
		/// </summary>
		/// <param name="cookie"></param>
		/// <param name="data"></param>
		/// <param name="millisecondTimeOut"></param>
		public void SetTimer(object cookie, object data, int millisecondTimeOut)
		{
			lock(_lockObj)
			{
				_cookieTimers[cookie] = new CookieExpireTime(cookie, data, System.Environment.TickCount + millisecondTimeOut);
				_responseEvent.Set();
			}
		}

		/// <summary>
		/// Remove the specified index entry from the cookie timer list.
		/// </summary>
		/// <param name="cookie"></param>
		public void ClearTimer(object cookie)
		{
			lock(_lockObj)
			{
				if (_cookieTimers.ContainsKey(cookie))
				{
					_cookieTimers.Remove(cookie);
					_responseEvent.Set();
				}
			}
		}

		/// <summary>
		/// Return the number of cookies in the timer event list.
		/// </summary>
		public int Length
		{
			get { return _cookieTimers.Count; }
		}

		/// <summary>
		/// This method will indicate if a certain cookie is currently in use.
		/// </summary>
		/// <param name="cookie"></param>
		/// <returns></returns>
		public bool CookieInUse(object cookie)
		{
			bool result = false;

			lock(_lockObj)
			{
				result = _cookieTimers.ContainsKey(cookie);
			}
			return result;
		}

		public object GetCookieData(object cookie)
		{
			object result = null;

			lock(_lockObj)
			{
				if (_cookieTimers.ContainsKey(cookie))
				result = ((CookieExpireTime)_cookieTimers[cookie]).Data;
			}
			return result;
		}
	}
}

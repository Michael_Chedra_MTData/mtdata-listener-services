using System;
using MTData.Sys.Exceptions;

namespace MTData.Sys.TaxiConfig
{
	/// <summary>
	/// This excepiton class handles any exceptions thrown by the config classes
	/// </summary>
#if !NETCF
	[Serializable]
#endif
	public class ConfigException : BaseException
	{

#if !NETCF
		/// <summary>
		/// Prepare the class form a serialized version of the object
		/// </summary>
		/// <param name="info">Serialization Informaiton</param>
		/// <param name="context">Context in which object was serialized</param>
		public ConfigException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context)
		{
			
		}
#endif
		
		/// <summary>
		/// Prepare the exception class given a base exception that has further detail
		/// </summary>
		/// <param name="message">Message text to be included in the exception</param>
		/// <param name="ConfigException">Base exception that this exception was triggered by</param>
		public ConfigException(string message, System.Exception ConfigException) : base(message, ConfigException)
		{
			
		}
		
		/// <summary>
		/// Prepare the exception class given a message of the error.
		/// </summary>
		/// <param name="message">Message text to be included in the exception</param>
		public ConfigException(string message) : base(message)
		{
			
		}

		/// <summary>
		/// Prepare an empty instance of the exception class 
		/// </summary>
		public ConfigException() : base()
		{
			
		}
	}
}

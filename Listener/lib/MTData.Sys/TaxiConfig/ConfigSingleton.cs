using System;
using System.Xml;
using Microsoft.Win32;
using System.IO;
using System.Reflection;

namespace MTData.Sys.TaxiConfig
{
	/// <summary>
	/// This class manages configuration information for the TelOp Taxi System system.  
	/// </summary>
	public class ConfigSingleton : ConfigSection
	{
		protected XmlDocument _configDOM = new XmlDocument();
		protected string _configFilename;
		private string _configurationName = "Production";
		private bool _launchWFHandler = false;
		private string _launchCommIDs = "";

		#region Properties

		/// <summary>
		/// Return the active configuration name
		/// </summary>
		public string ConfigurationName
		{
			get { return this._configurationName; }
			set { this._configurationName = value; }
		}

		/// <summary>
		/// Returns the current configuration filename
		/// </summary>
		public string Filename
		{
			get { return this._configFilename; }
		}

		/// <summary>
		/// Returns the DSN for the system specified
		/// </summary>
		public string DSN(string databaseName)
		{
			return GetStringValue("Database/" + databaseName, "DSN");
		}

		/// <summary>
		/// Returns a flag indicating whether the 
		/// Workflow Handler is to be launched.
		/// </summary>
		public bool LaunchWFHandler
		{
			get { return this._launchWFHandler; }
		}

		/// <summary>
		/// Returns a string listing the CommInterfaceID's
		/// to launch a CommHandler for.
		/// </summary>
		public string LaunchCommIDs
		{
			get { return this._launchCommIDs; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs and opens a configuration file based on the following search rule:
		/// - Looks for path to config.xml file in registry HKEY_LOCAL_MACHINE\SOFTWARE\MTD\TAXI
		/// - Looks for config.xml file in current assembly folder.
		/// </summary>
		public ConfigSingleton() : base()
		{
			this._configFilename = GetConfigPath();

			GetWorkflowManagerSettings();

			Load();
		}

		/// <summary>
		/// Opens the specified configuration file
		/// </summary>
		/// <param name="configPath">Path (including filename) to config file.</param>
		public ConfigSingleton(string configPath) : base()
		{
			this._configFilename = Path.GetFullPath(configPath);

			GetWorkflowManagerSettings();

			Load();
		}

		/// <summary>
		/// Prepare the class for use from a Pre-Loaded Xml Document.
		/// </summary>
		/// <param name="configNode"></param>
		public ConfigSingleton(XmlDocument configDOM) : base(configDOM.DocumentElement)
		{
			this._configFilename = "";
			this._configDOM = configDOM;

			GetWorkflowManagerSettings();

			Refresh();
		}

		#endregion

		#region Configuration Loading and Preparation

		/// <summary>
		/// This method ensures we are looking at the correct configuration
		/// </summary>
		/// <param name="xPath"></param>
		/// <returns></returns>
		protected override string PreProcessXPath(string xPath)
		{
			if ((xPath == null) || ((xPath != null) && (xPath == "")))
				return string.Format("//Configs/{0}/Config", this._configurationName);
			else
				return string.Format("//Configs/{0}/Config/{1}", this._configurationName, xPath);
		}

		/// <summary>
		/// Return the list of available configurations
		/// </summary>
		/// <returns>IList</returns>
		public System.Collections.IList GetConfigList()
		{
			System.Collections.IList configurations = new System.Collections.ArrayList();
			XmlNodeList configs = this._configDOM.SelectSingleNode("Configs").ChildNodes;

			foreach(XmlNode node in configs)
			{
				configurations.Add(node.Name);
			}

			return configurations;
		}

		/// <summary>
		/// Determine the path to the configuration file Config.XML.
		/// </summary>
		/// <returns></returns>
		public string GetConfigPath()
		{
			string configFolder = "";

			// Determine how we're locating the configuration file:
			RegistryKey configKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\MTD\\TAXI");
			if (configKey != null)
				configFolder = (string)configKey.GetValue("Config", "");		// - Registry entry providing absoute path to file

            if (configFolder.Length == 0)
            { 
                // Look in same folder as executing assembly
                configFolder = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
                if (configFolder.StartsWith(@"file:\"))
                    configFolder = configFolder.Remove(0, 6);
            }

			// Determine the name of the default configuration
			configKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\MTD\\TAXI");
			if (configKey != null)
				this._configurationName = (string)configKey.GetValue("defaultConfiguration", this._configurationName);				

			// Now look for Config.XML located in the configFolder
			return Path.Combine(configFolder, "Config.xml");
		}

		/// <summary>
		/// Load the Workflow Manager settings, for determining
		/// if the WorkflowHandler is to be launched, and which
		/// CommInterfaceID's are to have a CommHandler launched.
		/// </summary>
		public void GetWorkflowManagerSettings()
		{
			// Determine the name of the default configuration
			RegistryKey configKey = Registry.LocalMachine.OpenSubKey("SOFTWARE\\MTD\\TAXI");
			if (configKey != null)
			{
				string regValue = (string)configKey.GetValue("WFMLaunchWFHandler", this._launchWFHandler.ToString());
				this._launchWFHandler = (((string)regValue).ToUpper() == "TRUE");
				this._launchCommIDs = (string)configKey.GetValue("WFMLaunchCommIDs", this._launchCommIDs);
			}
		}

		/// <summary>
		/// Reloads the internal representation of the config file by re-reading the raw file.
		/// </summary>
		public void Load()
		{
			FileInfo fi = new FileInfo(this._configFilename);

			if (!fi.Exists)
				throw new FileNotFoundException(string.Format("{0} : Configuration file does not exist.", this._configFilename));
			
			this._configDOM.Load(this._configFilename);	
			Refresh();
		}

		/// <summary>
		/// Refreshes the internal primary node.
		/// </summary>
		public void Refresh()
		{
			this._configNode = this._configDOM.DocumentElement;
		}

		#endregion
	}
}

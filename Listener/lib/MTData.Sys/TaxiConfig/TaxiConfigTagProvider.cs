using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.IO.Network;
using MTData.Sys.Services;
using MTData.Sys.Services.TagExpansion;

namespace MTData.Sys.TaxiConfig
{
	/// <summary>
	/// This class will provide Tag Expansion capabilities to gain access
	/// to certain items within the Taxi config.xml file. 
	/// These include DSN entries, IPAddresses, and Port numbers
	/// Currently supported tags are as follows..
	///		DSN.System
	///		DSN.TAXI
	///		DSN.TaxiHistory
	///		DSN.StatusReport
	///		DSN.CityTemplate
	/// </summary>
	public class TaxiConfigTagProvider : ProviderBaseClass, ITagExpansionProvider
	{
		internal delegate string GetStringTagDelegate(TagHandler handler, string tag);
		internal delegate NetworkEndPoint GetNetworkTagDelegate(TagHandler handler, string tag);

		#region Tag Handler

		internal abstract class TagNode
		{
			private string _tag;

			public TagNode(string tag)
			{
				_tag = tag;
			}

			public string Tag
			{
				get
				{
					return _tag;
				}
			}
		}

		/// <summary>
		/// This is a link tag.
		/// </summary>
		internal class TagTreeNode : TagNode
		{
			Dictionary<string, TagNode> _tagNodes = new Dictionary<string, TagNode>();

			public TagTreeNode(string tag)
				: base(tag)
			{

			}

			public TagNode AddChild(TagNode node)
			{
				_tagNodes.Add(node.Tag, node);
				return node;
			}

			public TagNode GetChildNode(string tag)
			{
				TagNode result = null;
				if (_tagNodes.TryGetValue(tag, out result))
					return result;
				else
					return null;
			}
		}

		/// <summary>
		/// This class will be used to provide the links to the callback methods for each tag.
		/// </summary>
		internal class TagHandler : TagNode
		{
			private GetStringTagDelegate _stringCallback;
			private GetNetworkTagDelegate _networkCallback;

			public TagHandler(string tag, GetStringTagDelegate stringCallback, GetNetworkTagDelegate networkCallback) : base(tag)
			{
				_stringCallback = stringCallback;
				_networkCallback = networkCallback;
			}

			public string GetString(string tag)
			{
				if (_stringCallback != null)
					return _stringCallback(this, tag);
				else
					return null;
			}

			public NetworkEndPoint GetNetworkEndPoint(string tag)
			{
				if (_networkCallback != null)
					return _networkCallback(this, tag);
				else
					return null;
			}
		}

		#endregion

		private TagTreeNode _rootNode = new TagTreeNode("Root");

		public TaxiConfigTagProvider()
			: base("TaxiConfigTagExpansionProvider", "Provides Tag-Expansion capabilities within the Taxi Config File")
		{
			_rootNode.AddChild(new TagHandler("DSN", new GetStringTagDelegate(DSNStringHandler), null));
		}

		#region Handler methods

		/// <summary>
		/// Return the DSN connection string for the specified tag.
		/// </summary>
		/// <param name="handler"></param>
		/// <param name="tag"></param>
		/// <returns></returns>
		private string DSNStringHandler(TagHandler handler, string tag)
		{
			TaxiConfig.Config config = new Config();
			return config.DSN(tag);
		}

		#endregion

		#region ITagExpansionProvider Members

		/// <summary>
		/// Return the available tag values
		/// </summary>
		/// <param name="tag"></param>
		/// <returns></returns>
		public string GetString(string tag)
		{
			string result = null;

			//	iterate through the nodes to find the appropriate tag..
			string remainingTag = tag;
			int index = remainingTag.IndexOf('.');
			string workingTag = remainingTag.Substring(0, index);
			while (workingTag.Trim().Length > 0)
			{
				remainingTag = remainingTag.Substring(index + 1, remainingTag.Length - index - 1);

				TagNode node = _rootNode.GetChildNode(workingTag);
				if (node is TagHandler)
				{
					result = ((TagHandler)node).GetString(remainingTag);
					break;
				}

				index = remainingTag.IndexOf('.');
				workingTag = remainingTag.Substring(0, index);
			}
			return result;
		}

		public MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string tag)
		{
			MTData.Sys.IO.Network.NetworkEndPoint result = null;

			//	iterate through the nodes to find the appropriate tag..
			string remainingTag = tag;
			int index = remainingTag.IndexOf('.');
			string workingTag = remainingTag.Substring(0, index);
			while (workingTag.Trim().Length > 0)
			{
				remainingTag = remainingTag.Substring(index + 1, remainingTag.Length - index - 1);

				TagNode node = _rootNode.GetChildNode(workingTag);
				if (node is TagHandler)
				{
					result = ((TagHandler)node).GetNetworkEndPoint(remainingTag);
					break;
				}

				index = remainingTag.IndexOf('.');
				workingTag = remainingTag.Substring(0, index);
			}
			return result;
		}

		#endregion
	}
}

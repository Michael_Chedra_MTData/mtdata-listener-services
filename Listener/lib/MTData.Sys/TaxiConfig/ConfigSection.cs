using System;
using System.Collections;
using System.Xml;
using System.IO;
//using MTData.Taxi.Core.Exceptions;

namespace MTData.Sys.TaxiConfig
{
	/// <summary>
	/// This class provides a means to access fields and data within a
	/// specific section of the Config File. A method this this class
	/// allows a child config Section to be retreved.
	/// </summary>
	public class ConfigSection
	{
		protected XmlNode _configNode = null;

		/// <summary>
		/// Default constructor for the config section
		/// </summary>
		public ConfigSection()
		{

		}
		
		/// <summary>
		/// Standard constructor for the config section
		/// </summary>
		/// <param name="configNode"></param>
		public ConfigSection(XmlNode configNode)
		{
			this._configNode = configNode;
		}

		/// <summary>
		/// This method allows descendants to pre process the xpath to anything at all.
		/// </summary>
		/// <param name="xPath"></param>
		/// <returns></returns>
		protected virtual string PreProcessXPath(string xPath)
		{
			return xPath;
		}

		/// <summary>
		/// Property allowing access to the underlying config node.
		/// </summary>
		public XmlNode ConfigNode
		{
			get
			{
				return this._configNode;
			}
			set
			{
				this._configNode = value;
			}
		}

		/// <summary>
		/// Returns a path checking for the paths syntactical validity
		/// </summary>
		/// <param name="stringXPath"></param>
		/// <returns></returns>
		public string GetPath(string stringXPath)
		{
			return Path.GetFullPath(GetStringValue(stringXPath,null));
		}

		public int GetNumericValue(string stringXPath)
		{
			if(Utility.IsNumeric(GetStringValue(stringXPath,null)))
				return Convert.ToInt32(GetStringValue(stringXPath,null));
			else
				throw new ConfigException(string.Format("{0} is not a number",stringXPath));
		}

		public int GetNumericValue(string stringXPath,string attribName)
		{
			return GetNumericValue(stringXPath, attribName, true);
		}

		public int GetNumericValue(string stringXPath,string attribName, bool required)
		{
			string itemValue = GetStringValue(stringXPath,attribName, required);
			if (itemValue == null)
			{
				return -1;
			}
			else
			{
				if(Utility.IsNumeric(itemValue))
					return Convert.ToInt32(GetStringValue(stringXPath,attribName));
				else
					throw new ConfigException(string.Format("{0}[{1}] is not a number",stringXPath,attribName));
			}
		}


		/// <summary>
		/// Returns a configuration string
		/// </summary>
		/// <param name="stringXPath">An XML path to the required string</param>
		/// <returns>The string pointed to by XPath</returns>
		public string GetStringValue(string stringXPath)
		{
			return GetStringValue(stringXPath,null);
		}

		/// <summary>
		/// This method will return the requested node given the path
		/// </summary>
		/// <param name="stringXPath">XPath to node required</param>
		/// <param name="required">Indicates whether an exception should be raised if the node is not found</param>
		/// <returns></returns>
		private XmlNode GetNodeForPath(string stringXPath, bool required)
		{
			XmlNode node = null;

			// Build the XPath query
			string xpath = PreProcessXPath(stringXPath);

			if (xpath == "")
			{
				node = this._configNode;
			}
			else
			{
				node = this._configNode.SelectSingleNode(xpath);
			}
			if ((required) && (node == null))
			{
				throw new ConfigException(string.Format("{0} : not found in config file.",xpath));	
			}
			return node;
		}

		/// <summary>
		/// Return a stirng value for the specified entry.
		/// </summary>
		/// <param name="stringXPath"></param>
		/// <param name="attribName"></param>
		/// <returns></returns>
		public string GetStringValue(string stringXPath,string attribName)
		{
			return GetStringValue(stringXPath, attribName, true);
		}

		/// <summary>
		/// Returns a configuration string (element or attribute)
		/// </summary>
		/// <param name="stringXPath"></param>
		/// <param name="attribName"></param>
		/// <returns></returns>
		public string GetStringValue(string stringXPath,string attribName, bool required)
		{
			string result ="";
			XmlNode node = null;

			node = GetNodeForPath(stringXPath, required);
			if (node == null)
			{
				if (required)
					throw new Exception(string.Format("{0} : node does not exist.",stringXPath));	
				result = null;
			}
			else
			{
				if((attribName != null) && (attribName != string.Empty))
				{
					XmlNode attribute = node.Attributes.GetNamedItem(attribName);
					if (attribute != null)
					{
						result = attribute.Value;
					}
					else
					{
						if (required)
							throw new Exception(string.Format("{0}[{1}] : attribute does not exist.",stringXPath,attribName));	
					}
				}
				else
				{
					result = node.InnerText;				
				}
			}	
			return result;
		}

		/// <summary>
		/// REturn a boolean field value for the specified xpath/attribute combination
		/// </summary>
		/// <param name="stringXPath"></param>
		/// <param name="attribName"></param>
		/// <returns></returns>
		public bool GetFlagValue(string stringXPath, string attribName)
		{			
			return Convert.ToBoolean(GetStringValue(stringXPath,attribName));
		}

		/// <summary>
		/// This method will retrieve a list of values matching the location specified.
		/// </summary>
		/// <param name="stringXPath">xpath to nodes required</param>
		/// <returns>A string array of the results</returns>
		public string[] GetStringValues(string stringXPath)
		{
			return GetStringValues(stringXPath, null);
		}

		/// <summary>
		/// This method will retrieve a list of values matching the location specified.
		/// </summary>
		/// <param name="stringXPath">xpath to nodes required</param>
		/// <param name="attribName">attribute name for values of these nodes</param>
		/// <returns>A string array of the results</returns>
		public string[] GetStringValues(string stringXPath, string attribName)
		{
			string[] result = null;
			XmlNodeList list = this._configNode.SelectNodes(PreProcessXPath(stringXPath));

			result = new string[list.Count];

			for(int loop = 0; loop < list.Count; loop++)
			{
				if ((attribName == null) || ((attribName == null) && (attribName == "")))
				{
					result[loop] = list[loop].InnerText;
				}
				else
				{
					XmlNode attrib = list[loop].Attributes.GetNamedItem(attribName);
					if (attrib != null)
					{
						result[loop] = attrib.InnerText;
					}
					else
					{
						result[loop] = "";
					}
				}

			}

			return result;
		}

		/// <summary>
		/// This method will return a list of the nodenames below the specified node.
		/// </summary>
		/// <param name="stringXPath">xpath to parent node</param>
		/// <returns>array of node names below the specified node</returns>
		public string[] GetStringNames(string stringXPath)
		{
			string[] result = null;
			XmlNode parentNode = GetNodeForPath(stringXPath, true);
			result = new string[parentNode.ChildNodes.Count];

			for(int loop = 0; loop < parentNode.ChildNodes.Count; loop++)
			{
				result[loop] = parentNode.ChildNodes[loop].Name;
			}

			return result;
		}

		/// <summary>
		/// Return a configsection loaded form the specified point.
		/// the configSection class is primariky an Accessor class so creating a newe instance
		/// should not have a large overhead.
		/// </summary>
		/// <param name="stringXPath">xpath to required parent node</param>
		/// <returns>Instance of a ConfigSection loaded for the point concerned</returns>
		public ConfigSection GetConfigSection(string stringXPath)
		{
			XmlNode node = GetNodeForPath(stringXPath, true);
			return new ConfigSection(node);
		}

		/// <summary>
		/// Tries to retrieve a section without raising an exception if none is found.
		/// </summary>
		/// <param name="stringXPath"></param>
		/// <param name="required"></param>
		/// <returns></returns>
		public ConfigSection GetConfigSection(string stringXPath, bool required)
		{
			XmlNode node = GetNodeForPath(stringXPath, required);
			if (node != null)
			{
				return new ConfigSection(node);
			}
			else
			{
				return null;
			}
		}

		/// <summary>
		/// Return an array of configsections loaded from the specified point.
		/// the configSection class is primariky an Accessor class so creating a newe instance
		/// should not have a large overhead.
		/// </summary>
		/// <param name="stringXPath">xpath to required parent node</param>
		/// <returns>Instance of a ConfigSection loaded for the point concerned</returns>
		public ConfigSection[] GetConfigSections(string stringXPath)
		{
			XmlNodeList list = this._configNode.SelectNodes(PreProcessXPath(stringXPath));
			ConfigSection[] result = new ConfigSection[list.Count];

			for(int loop = 0; loop < list.Count; loop++)
				result[loop] = new ConfigSection(list[loop]);

			return result;
		}
	}
}

#if !NETCF && !NETCF2
using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Services;
using MTData.Sys.Services.TagExpansion;

namespace MTData.Sys.TaxiConfig.InstalledOptions
{
	/// <summary>
	/// This class will implement a tag expansion provider for access
	/// to Installed Options tags in the Taxi system.
	/// </summary>
	public class TaxiInstalledOptionTagProvider : ProviderBaseClass, ITagExpansionProvider
	{
		public TaxiInstalledOptionTagProvider() : base("TaxiInstalledOptionTagProvider", "Provides Tag Expansion services for installed options settings")
		{
		
		}

		#region ITagExpansionProvider Members

		/// <summary>
		/// The first element in the tag indicates the installed option.
		/// The next level is either Enabled, or Data.
		/// The rest of the tag is an xpath into the config fragment.
		/// </summary>
		/// <param name="tag"></param>
		/// <returns></returns>
		public string GetString(string tag)
		{
			string result = null;

			string[] tags = tag.Split('.');
			if (tags.Length < 2)
				throw new ArgumentException(string.Format("Tag '{0}' not valid for provider '{1}'", tag, this.Name));
			
			if (!Enum.IsDefined(typeof(InstalledOptions), tags[0]))
				throw new ArgumentException(string.Format("Tag '{0}' not valid for provider '{1}'", tag, this.Name));

			InstalledOptions option = (InstalledOptions)Enum.Parse(typeof(InstalledOptions), tags[0], false);

			InstalledOptionsDAO dao = new InstalledOptionsDAO();
			bool installed = dao.GetInstalled(option);

			if (tags[1] == "Enabled")
				result = installed.ToString();
			else if (tags[1] == "Data")
			{
				if (installed)
				{
					InstalledOptionSettings settings = dao.GetSettings(option);
					if (settings != null)
					{
						//	now we iterate the remaining tags to get the value we want.
						XmlNode currentNode = settings.DocumentElement;
						int index = 2;
						while (index < tags.Length)
						{
							string currentTag = tags[index];
							XmlNode newNode = currentNode.SelectSingleNode(currentTag);
							if (newNode == null)
								newNode = currentNode.Attributes.GetNamedItem(currentTag);
							
							if (newNode == null)
								throw new ArgumentException(string.Format("Tag '{0}' not valid for provider '{1}'", tag, this.Name));

							currentNode = newNode;
							index++;
						}

						if (currentNode is XmlAttribute)
							result = currentNode.Value;
						else
							result = currentNode.InnerXml;

					}
				}
			} else
				throw new ArgumentException(string.Format("Tag '{0}' not valid for provider '{1}'", tag, this.Name));

			return result;
		}

		/// <summary>
		/// If we are retrieving a network endpoint, then we add IP and Port to the tag to get the appropriate field.
		/// </summary>
		/// <param name="tag"></param>
		/// <returns></returns>
		public MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string tag)
		{
			MTData.Sys.IO.Network.NetworkEndPoint endPoint = null;
			string ip = GetString(tag + "IP");
			string port = GetString(tag + "Port");
			int portNumber = 0;
			if (port != null)
				if (Int32.TryParse(port, out portNumber))
					endPoint = new MTData.Sys.IO.Network.NetworkEndPoint(ip, portNumber);
			if (endPoint == null)
				throw new ArgumentException(string.Format("Tag '{0}' not valid for EndPoint for provider '{1}'", tag, this.Name));
			return endPoint;
		}

		#endregion
	}
}
#endif

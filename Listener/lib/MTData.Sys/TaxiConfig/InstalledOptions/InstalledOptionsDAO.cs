#if !NETCF && !NETCF2
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.TaxiConfig.InstalledOptions
{
    /// <summary>
    /// This method will check that the installed option is installed, and retrieve the settings for it.
    /// </summary>
    public class InstalledOptionsDAO : MTData.Sys.Data.BaseDao
    {
        private TaxiConfig.Config _config = null;

        public InstalledOptionsDAO()
            : base()
        {
            _config = new Config();
        }

        private object ProcessGetSettings(SqlConnection connection, object[] parameters)
        {
            InstalledOptionSettings result = null;

            using (SqlCommand command = new SqlCommand("usp_InstalledOptionsSettingsGet", connection) { CommandType = CommandType.StoredProcedure })
            {
                command.Parameters.AddWithValue("@OptionID", (int)((InstalledOptions)parameters[0]));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        string fragment = MTData.Sys.DataConvert.ToString(reader["OptionSettings"], null);
                        if (fragment != null)
                            result = new InstalledOptionSettings(fragment);
                    }
                }
            }

            return result;
        }

        private object ProcessGetEnabled(SqlConnection connection, object[] parameters)
        {
            bool result = false;

            using (SqlCommand command = new SqlCommand("usp_InstalledOptionsRetrieve", connection) { CommandType = CommandType.StoredProcedure })
            {
                command.Parameters.AddWithValue("@OptionID", (int)((InstalledOptions)parameters[0]));

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    result = reader.Read();
                }
            }

            return result;
        }

        /// <summary>
        /// Returns the xml fragment for the options.
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public InstalledOptionSettings GetSettings(InstalledOptions option)
        {
            return (InstalledOptionSettings)base.PopulateDataWithConnection(_config.DSN("System"), new PopulateDataDelegate(ProcessGetSettings), new object[] { option });
        }

        /// <summary>
        /// Returns whether the option is installed or not
        /// </summary>
        /// <param name="option"></param>
        /// <returns></returns>
        public bool GetInstalled(InstalledOptions option)
        {
            return (bool)base.PopulateDataWithConnection(_config.DSN("System"), new PopulateDataDelegate(ProcessGetEnabled), new object[] { option });
        }
    }
}
#endif

using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.TaxiConfig.InstalledOptions
{
	public class InstalledOptionSettings
	{
		private XmlDocument _document;

		public InstalledOptionSettings()
			: this(null)
		{

		}

		public InstalledOptionSettings(string settingFragment)
		{
			XmlFragment = settingFragment;
		}

		public virtual string GetDefaultFragment()
		{
			return "";
		}

		public XmlNode DocumentElement
		{
			get
			{
				return _document.DocumentElement;
			}
		}

		public string XmlFragment
		{
			get
			{
				return InstalledOptionsHandler.ExtractXmlFragment(_document);
			}
			set
			{
				if ((value != null) && (value != ""))
					_document = InstalledOptionsHandler.ParseXMLFragment(value);
				else
					_document = InstalledOptionsHandler.ParseXMLFragment(GetDefaultFragment());
			}
		}
	}
}

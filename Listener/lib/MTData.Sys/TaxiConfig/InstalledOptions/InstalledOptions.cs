using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.TaxiConfig.InstalledOptions
{
	/// <summary>
	/// shows what options have been installed with the Taxi System.
	/// </summary>
	public enum InstalledOptions : int
	{
		Billing = 1,
		GNAFAddressLookup = 2,
		TotalMobility = 3,
		VectorMapping = 4,
		DriverPoints = 5,
		TrackingIntegration = 6,
		SystemMonitoring = 7,
		AdvanceBooking = 8,
		Monitoring = 9,
		AssetManagement = 10,
		DriverDestination = 11,
		Navigation = 12,
		PhoneIntegration = 13,
		FareEstimation = 14
	}
}

using System;
using System.Xml;
using System.Text;

namespace MTData.Sys.TaxiConfig.InstalledOptions
{
	/// <summary>
	/// This class will handle creating and parsing xml fragments 
	/// for setting sof the installed options.
	/// </summary>
	public class InstalledOptionsHandler
	{
		public InstalledOptionsHandler()
		{

		}

		/// <summary>
		/// This method will create an xml document from the fragment provided, and parse it correctly.
		/// </summary>
		/// <param name="fragment"></param>
		/// <returns></returns>
		public static System.Xml.XmlDocument ParseXMLFragment(string fragment)
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("<?xml version='1.0'?><Fragment>");
			builder.Append(fragment);
			builder.Append("</Fragment>");

			System.Xml.XmlDocument document = new System.Xml.XmlDocument();
			document.LoadXml(builder.ToString());
			return document;
		}

		public static string ExtractXmlFragment(System.Xml.XmlDocument document)
		{
			return document.DocumentElement.InnerXml;
		}

		public static string GetAttributeFromNode(System.Xml.XmlNode node, string attributeName, string defaultValue)
		{
			System.Xml.XmlNode attribute = node.Attributes.GetNamedItem(attributeName);
			if ((attribute != null) && (attribute.Value != null))
				return attribute.Value;
			else
				return defaultValue;
		}

		public static void SetAttributeForNode(System.Xml.XmlNode node, string attributeName, string value)
		{
			System.Xml.XmlNode attribute = node.Attributes.GetNamedItem(attributeName);
			if (attribute == null) 
			{
				attribute = node.OwnerDocument.CreateAttribute(attributeName);
				node.Attributes.SetNamedItem(attribute);
			}
			attribute.Value = value;
		}

		/// <summary>
		/// Create the nodes neccessary for the path provided.
		/// </summary>
		/// <param name="parentNode"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public static System.Xml.XmlNode GetNodeFromPath(System.Xml.XmlNode parentNode, string path)
		{
			string[] splits = path.Split('/');

			System.Xml.XmlNode node = null;

			for(int loop = 0; loop < splits.Length; loop++)
			{
				node = parentNode.SelectSingleNode(splits[loop]);
				if (node == null)
				{
					node = parentNode.OwnerDocument.CreateNode(System.Xml.XmlNodeType.Element, splits[loop], "");
					parentNode.AppendChild(node);
				}
				parentNode = node;
			}
			return node;
		}

		/// <summary>
		/// Append a node to the parent node
		/// </summary>
		/// <param name="parentNode"></param>
		/// <param name="nodeName"></param>
		/// <returns></returns>
		public static System.Xml.XmlNode AppendChildNode(System.Xml.XmlNode parentNode, string nodeName)
		{
			System.Xml.XmlNode node = parentNode.OwnerDocument.CreateNode(System.Xml.XmlNodeType.Element, nodeName, "");
			parentNode.AppendChild(node);
			return node;
		}
	}
}

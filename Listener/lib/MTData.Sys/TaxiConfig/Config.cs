using System;
using System.Xml;
using Microsoft.Win32;
using System.IO;
using System.Reflection;

namespace MTData.Sys.TaxiConfig
{
	/// <summary>
	/// This class manages configuration information for the TelOp Taxi System system.  
	/// </summary>
	public class Config : ConfigSection
	{
		protected static ConfigSingleton _singleton = new ConfigSingleton();

		#region Properties

		/// <summary>
		/// Return the active configuration name
		/// </summary>
		public string ConfigurationName
		{
			get { return _singleton.ConfigurationName; }
			set { _singleton.ConfigurationName = value; }
		}

		/// <summary>
		/// Returns the current configuration filename
		/// </summary>
		public string Filename
		{
			get { return _singleton.Filename; }
		}

		/// <summary>
		/// Returns the DSN for the system specified
		/// </summary>
		public string DSN(string databaseName)
		{
			return _singleton.DSN(databaseName);
		}

		/// <summary>
		/// Returns a flag indicating whether the 
		/// Workflow Handler is to be launched.
		/// </summary>
		public bool LaunchWFHandler
		{
			get { return _singleton.LaunchWFHandler; }
		}

		/// <summary>
		/// Returns a string listing the CommInterfaceID's
		/// to launch a CommHandler for.
		/// </summary>
		public string LaunchCommIDs
		{
			get { return _singleton.LaunchCommIDs; }
		}

		#endregion

		#region Constructors

		/// <summary>
		/// Constructs and opens a configuration file based on the following search rule:
		/// - Looks for path to config.xml file in registry HKEY_LOCAL_MACHINE\SOFTWARE\MTD\TAXI
		/// - Looks for config.xml file in current assembly folder.
		/// </summary>
		public Config() : base()
		{
			_configNode = _singleton.ConfigNode;
		}

		/// <summary>
		/// Opens the specified configuration file
		/// </summary>
		/// <param name="configPath">Path (including filename) to config file.</param>
		public Config(string configPath) : base()
		{
			_configNode = _singleton.ConfigNode;
		}

		/// <summary>
		/// Prepare the class for use from a Pre-Loaded Xml Document.
		/// </summary>
		/// <param name="configNode"></param>
		public Config(XmlDocument configDOM) : base(configDOM.DocumentElement)
		{
			_singleton.ConfigNode = configDOM.DocumentElement;
		}

		#endregion

		#region Configuration Loading and Preparation


		/// <summary>
		/// Return the list of available configurations
		/// </summary>
		/// <returns>IList</returns>
		public System.Collections.IList GetConfigList()
		{
			return _singleton.GetConfigList();
		}

		/// <summary>
		/// This method ensures we are looking at the correct configuration
		/// </summary>
		/// <param name="xPath"></param>
		/// <returns></returns>
		protected override string PreProcessXPath(string xPath)
		{
			if ((xPath == null) || ((xPath != null) && (xPath == "")))
				return string.Format("//Configs/{0}/Config", _singleton.ConfigurationName);
			else
				return string.Format("//Configs/{0}/Config/{1}", _singleton.ConfigurationName, xPath);
		}

		/// <summary>
		/// Determine the path to the configuration file Config.XML.
		/// </summary>
		/// <returns></returns>
		public string GetConfigPath()
		{
			return _singleton.GetConfigPath();
		}

		/// <summary>
		/// Load the Workflow Manager settings, for determining
		/// if the WorkflowHandler is to be launched, and which
		/// CommInterfaceID's are to have a CommHandler launched.
		/// </summary>
		public void GetWorkflowManagerSettings()
		{
		}

		/// <summary>
		/// Reloads the internal representation of the config file by re-reading the raw file.
		/// </summary>
		public void Load()
		{
		}

		/// <summary>
		/// Refreshes the internal primary node.
		/// </summary>
		public void Refresh()
		{
		}

		#endregion
	}
}

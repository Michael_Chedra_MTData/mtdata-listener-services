using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Timezone
{
	/// <summary>
	/// This interface identifies what should be available for a timezone
	/// </summary>
	public interface ITimezone
	{
		/// <summary>
		/// This is the ID of the timezone in the system
		/// </summary>
		int ID
		{
			get;
		}

		/// <summary>
		/// This is the name of the timezone in the system
		/// </summary>
		string Name
		{
			get;
		}

		/// <summary>
		/// This is the offset in minutes from utc time
		/// </summary>
		int Offset
		{
			get;
		}

		/// <summary>
		/// This method will convert the datetime provided local to the timezone into utc time
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ToUtc(DateTime dateTime);

		/// <summary>
		/// This method will take a utc time and convert it into a datetime local to this timezone
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime FromUtc(DateTime dateTime);
    }
}

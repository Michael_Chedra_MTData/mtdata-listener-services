using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Timezone
{
	/// <summary>
	/// This is the daylight savings component of the Timezone
	/// </summary>
	public interface ITimezoneDaylightSavings
	{
		/// <summary>
		/// This is the id of the TimeZone to which this belongs.
		/// </summary>
		int ID
		{
			get;
		}

		/// <summary>
		/// Startdate of application
		/// </summary>
		DateTime StartDate
		{
			get;
		}

		/// <summary>
		/// End date of application
		/// </summary>
		DateTime EndDate
		{
			get;
		}

		/// <summary>
		/// Offset in minutes form UTC
		/// </summary>
		int Offset
		{
			get;
		}
	}
}

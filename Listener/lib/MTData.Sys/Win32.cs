﻿using System;

using System.Collections.Generic;
using System.Runtime.InteropServices;
using System.Text;
#if Android
using Android.OS;
#endif

namespace MTData.Sys
{
    public class Win32
    {

#if NETCF || NETCF2
        private const string KERNEL32DLL    = "coredll";
        private const string GDI32DLL       = "coredll";
        private const string USER32DLL      = "coredll";
        private const string WINMM          = "coredll";
#else
        private const string KERNEL32DLL	= "kernel32";
		private const string GDI32DLL		= "gdi32";
		private const string USER32DLL		= "user32";
        private const string WINMM          = "winmm";
#endif
        #region PInvokes
        public struct SYSTEM_INFO
        {
            public UInt16 wProcessorArchitecture;
            public UInt16 wReserved;
            public UInt32 dwPageSize;
            public IntPtr lpMinimumApplicationAddress;
            public IntPtr lpMaximumApplicationAddress;
            public UInt32 dwActiveProcessorMask;
            public UInt32 dwNumberOfProcessors;
            public UInt32 dwProcessorType;
            public UInt32 dwAllocationGranularity;
            public UInt16 wProcessorLevel;
            public UInt16 wProcessorRevision;
        }

        public struct BITMAP
        {
            public int bmType;
            public int bmWidth;
            public int bmHeight;
            public int bmWidthBytes;
            public ushort bmPlanes;
            public ushort bmBitsPixel;
            public IntPtr bmBits;
        }

        public struct BITMAPINFOHEADER
        {
            public uint biSize;
            public int biWidth;
            public int biHeight;
            public ushort biPlanes;
            public ushort biBitCount;
            public uint biCompression;
            public uint biSizeImage;
            public int biXPelsPerMeter;
            public int biYPelsPerMeter;
            public uint biClrUsed;
            public uint biClrImportant;
        }

        public const uint PAGE_NOACCESS = 0x01;
        public const uint PAGE_READWRITE = 0x04;
        public const uint PAGE_NOCACHE = 0x200;
        public const uint PAGE_PHYSICAL = 0x400;
        public const uint MEM_COMMIT = 0x1000;
        public const uint MEM_RESERVE = 0x2000;
        public const uint MEM_RESET = 0x80000;
        public const uint MEM_TOP_DOWN = 0x100000;


        public const uint STANDARD_RIGHTS_REQUIRED = 0x000F0000;
        public const uint SECTION_QUERY = 0x0001;
        public const uint SECTION_MAP_WRITE = 0x0002;
        public const uint SECTION_MAP_READ = 0x0004;
        public const uint SECTION_MAP_EXECUTE = 0x0008;
        public const uint SECTION_EXTEND_SIZE = 0x0010;
        public const uint SECTION_ALL_ACCESS = STANDARD_RIGHTS_REQUIRED | SECTION_QUERY | SECTION_MAP_WRITE | SECTION_MAP_READ | SECTION_MAP_EXECUTE | SECTION_EXTEND_SIZE;
        public const uint FILE_MAP_COPY = SECTION_QUERY;
        public const uint FILE_MAP_WRITE = SECTION_MAP_WRITE;
        public const uint FILE_MAP_READ = SECTION_MAP_READ;
        public const uint FILE_MAP_ALL_ACCESS = SECTION_ALL_ACCESS;
#if NETCF || NETCF2
		[DllImport(KERNEL32DLL)]
		public static extern IntPtr SetSystemMemoryDivision(int dwStorePages);
#endif
        [DllImport(KERNEL32DLL)]
        public static extern IntPtr VirtualAlloc(IntPtr lpAddress, uint dwSize, uint flAllocationType, uint flProtect);

        [DllImport(KERNEL32DLL)]
        public static extern uint VirtualCopy(IntPtr lpvDest, IntPtr lpvSrc, uint cbSize, uint fdwProtect);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr CreateFileMapping(IntPtr hFile, uint lpFileMappingAttributes, uint flProtect, uint dwMaximumSizeHigh, uint dwMaximumSizeLow, string lpName);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr MapViewOfFile(IntPtr hFileMappingObject, uint dwDesiredAccess, uint dwFileOffsetHigh, uint dwFileOffsetLow, uint dwNumberOfBytesToMap);

        [DllImport(KERNEL32DLL)]
        public static extern bool UnmapViewOfFile(IntPtr lpBaseAddress);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr CreateEvent(int lpEventAttributes, int bManualReset, int bInitialState, string lpName);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr CloseHandle(IntPtr hEvent);

        public const uint GENERIC_READ = 0x80000000;
        public const uint GENERIC_WRITE = 0x40000000;
        public const uint GENERIC_EXECUTE = 0x20000000;
        public const uint GENERIC_ALL = 0x10000000;


        public const uint FILE_SHARE_READ = 0x00000001;
        public const uint FILE_SHARE_WRITE = 0x00000002;

        //
        // File creation flags must start at the high end since they
        // are combined with the attributes
        //

        public const uint FILE_FLAG_WRITE_THROUGH = 0x80000000;
        public const uint FILE_FLAG_OVERLAPPED = 0x40000000;
        public const uint FILE_FLAG_NO_BUFFERING = 0x20000000;
        public const uint FILE_FLAG_RANDOM_ACCESS = 0x10000000;
        public const uint FILE_FLAG_SEQUENTIAL_SCAN = 0x08000000;
        public const uint FILE_FLAG_DELETE_ON_CLOSE = 0x04000000;
        public const uint FILE_FLAG_BACKUP_SEMANTICS = 0x02000000;
        public const uint FILE_FLAG_POSIX_SEMANTICS = 0x01000000;

        public const uint CREATE_NEW = 1;
        public const uint CREATE_ALWAYS = 2;
        public const uint OPEN_EXISTING = 3;
        public const uint OPEN_ALWAYS = 4;
        public const uint TRUNCATE_EXISTING = 5;
        public const uint OPEN_FOR_LOADER = 6;

        public const uint FILE_ATTRIBUTE_READONLY = 0x00000001;
        public const uint FILE_ATTRIBUTE_HIDDEN = 0x00000002;
        public const uint FILE_ATTRIBUTE_SYSTEM = 0x00000004;
        public const uint FILE_ATTRIBUTE_DIRECTORY = 0x00000010;
        public const uint FILE_ATTRIBUTE_ARCHIVE = 0x00000020;
        public const uint FILE_ATTRIBUTE_INROM = 0x00000040;
        public const uint FILE_ATTRIBUTE_ENCRYPTED = 0x00000040;
        public const uint FILE_ATTRIBUTE_NORMAL = 0x00000080;
        public const uint FILE_ATTRIBUTE_TEMPORARY = 0x00000100;
        public const uint FILE_ATTRIBUTE_SPARSE_FILE = 0x00000200;
        public const uint MODULE_ATTR_NOT_TRUSTED = 0x00000200;
        public const uint FILE_ATTRIBUTE_REPARSE_POINT = 0x00000400;
        public const uint MODULE_ATTR_NODEBUG = 0x00000400;
        public const uint FILE_ATTRIBUTE_COMPRESSED = 0x00000800;
        public const uint FILE_ATTRIBUTE_OFFLINE = 0x00001000;
        public const uint FILE_ATTRIBUTE_ROMSTATICREF = 0x00001000;
        public const uint FILE_ATTRIBUTE_NOT_CONTENT_INDEXED = 0x00002000;
        public const uint FILE_ATTRIBUTE_ROMMODULE = 0x00002000;

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr CreateFile(string lpFileName, uint dwDesiredAccess, uint dwShareMode, IntPtr lpSecurityAttributes, uint dwCreationDispostion, uint dwFlagsAndAttributes, IntPtr hTemplateFile);

        [DllImport(KERNEL32DLL)]
        public static extern uint GetFileSize(IntPtr hFile, ref uint fileSizeHigh);
        [DllImport(KERNEL32DLL)]
        public static extern uint GetFileSize(IntPtr hFile, uint zero);

        [DllImport(KERNEL32DLL)]
        public static extern int ReadFile(IntPtr hFile, IntPtr lpBuffer, uint nNumberOfBytesToRead, ref uint lpNumberOfBytesRead, int zero);

        [DllImport(KERNEL32DLL)]
        public static extern int EventModify(IntPtr hEvent, int func);

        [DllImport(KERNEL32DLL)]
        public static extern uint GetEventData(IntPtr hEvent);

        [DllImport(KERNEL32DLL)]
        public static extern bool SetEventData(IntPtr hEvent, uint dwData);

        [DllImport(KERNEL32DLL)]
        public static extern int WaitForSingleObject(IntPtr lpHandle, int dwMilliseconds);

        public const int EVENT_PULSE = 1;
        public const int EVENT_RESET = 2;
        public const int EVENT_SET = 3;

        [DllImport(KERNEL32DLL)]
        public static extern int WaitForMultipleObjects(int nCount, IntPtr[] lpHandles, int fWaitAll, int dwMilliseconds);

        public const int INFINITE = -1;
        public const int WAIT_OBJECT_0 = 0x00000000;
        public const int WAIT_ABANDONED_0 = 0x00000080;
        public const int WAIT_TIMEOUT = 258;

#if NETCF || NETCF2
        [DllImport(KERNEL32DLL, EntryPoint = "memmove")]
#else
		[DllImport(KERNEL32DLL)]
#endif
        public static extern int RtlMoveMemory(IntPtr dest, IntPtr src, uint count);

#if NETCF || NETCF2
        [DllImport(KERNEL32DLL, EntryPoint = "memmove")]
#else
		[DllImport(KERNEL32DLL)]
#endif
        public static extern int RtlMoveMemory(
            IntPtr hgdiobj,
            byte[] buf,
            int c);

#if NETCF || NETCF2
		[DllImport("coredll.Dll", CharSet=CharSet.Auto)] 
#else
		[DllImport("kernel32")]
#endif

		public static extern int CreateProcess(string strImageName, string strCmdLine, IntPtr pProcessAttributes, IntPtr pThreadAttributes, int bInheritsHandle, int
			wCreationFlags, IntPtr pEnvironment, IntPtr pCurrentDir, Byte[] bArray, ProcessInfo oProc);

#if NETCF || NETCF2
		[DllImport("coredll.dll", CharSet = CharSet.Auto)]
#else
		[DllImport("user32")]
#endif
		private static extern IntPtr FindWindow(string className, string windowName);

        public static readonly IntPtr INVALID_HANDLE_VALUE = new IntPtr(-1);

        public const int BI_RGB = 0;
        public const int LMEM_FIXED = 0x40;
        public const int DPI72 = 0xb12;

        [DllImport(KERNEL32DLL)]
        public static extern int GetObject(IntPtr hgdiobj, int cbBuffer, ref BITMAP lpvObject);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr SelectObject(IntPtr hDC, IntPtr hObject);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr GetDC(IntPtr hWnd);

        [DllImport(KERNEL32DLL)]
        public static extern void DeleteObject(IntPtr hObject);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr CreateCompatibleDC(IntPtr hDC);

        [DllImport(KERNEL32DLL)]
        public static extern void DeleteDC(IntPtr hDC);

        [DllImport(KERNEL32DLL)]
        public static extern void ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr CreateDIBSection(IntPtr hdc, ref BITMAPINFOHEADER hdr, uint colors, out IntPtr pBits, IntPtr hFile, uint offset);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr LoadLibrary(string lib);

        [DllImport(KERNEL32DLL)]
        public static extern IntPtr GetModuleHandle(string lib);

		[DllImport(KERNEL32DLL)]
		private static extern int GetModuleFileNameW(IntPtr hModule, byte[] lpFilename, int nSize);

        public const int SRCCOPY = 0xCC0020;

        [DllImport(KERNEL32DLL)]
        public static extern int BitBlt(IntPtr hdcDest, int nXDest, int nYDest, int nWidth, int nHeight, IntPtr hdcSrc, int nXSrc, int nYSrc, uint dwRop);

        [DllImport(KERNEL32DLL)]
        public static extern int StretchBlt(IntPtr hdcDest, int nXOriginDest, int nYOriginDest, int nWidthDest, int nHeightDest, IntPtr hdcSrc, int nXOriginSrc, int nYOriginSrc, int nWidthSrc, int nHeightSrc, uint dwRop);

#if Android
		public static int GetTickCount()
		{
			return (int)SystemClock.ElapsedRealtime();
		}
#else
        [DllImport(KERNEL32DLL)]
        public static extern int GetTickCount();
#endif
        [DllImport(KERNEL32DLL)]
        public static extern int AddFontResource(string lpszFilename);

        [DllImport(KERNEL32DLL)]
        public static extern int ShowWindow(IntPtr handle, int i);

        [DllImport(KERNEL32DLL)]
        public static extern int SetForegroundWindow(IntPtr handle);

        [DllImport(USER32DLL)]
        public static extern bool IsWindowVisible(IntPtr handle);

        [DllImport(KERNEL32DLL)]
        public static extern void GetSystemInfo(ref SYSTEM_INFO lpSystemInfo);

        public enum SND
        {
            SYNC = 0x00000000,  /* play synchronously (default) */
            ASYNC = 0x00000001,  /* play asynchronously */
            NODEFAULT = 0x00000002,  /* silence (!default) if sound not found */
            MEMORY = 0x00000004,  /* pszSound points to a memory file */
            LOOP = 0x00000008,  /* loop the sound until next sndPlaySound */
            NOSTOP = 0x00000010,  /* don't stop any currently playing sound */
            NOWAIT = 0x00002000, /* don't wait if the driver is busy */
            ALIAS = 0x00010000, /* name is a registry alias */
            ALIAS_ID = 0x00110000, /* alias is a predefined ID */
            FILENAME = 0x00020000, /* name is file name */
            RESOURCE = 0x00040004  /* name is resource name or atom */
        }

		[DllImport(WINMM, EntryPoint="PlaySoundW", SetLastError=true) ]
        public extern static bool PlaySoundW(string lpszName, IntPtr hModule, uint dwFlags);

		[DllImport(WINMM, EntryPoint="PlaySoundW", SetLastError=true) ]
        public extern static bool PlaySoundW(byte[] szSound, IntPtr hModule, uint dwFlags);
        #endregion

		/// <summary>
		/// This default constructor is required in order to descend from the class.
		/// </summary>
		protected Win32()
        {
        }

        private static SYSTEM_INFO _systemInfo = new SYSTEM_INFO();
        private static bool _systemInfoInit = false;
        public static SYSTEM_INFO SystemInfo
        {
            get
            {
                if (!_systemInfoInit)
                {
                    GetSystemInfo(ref _systemInfo);
                    _systemInfoInit = true;
                }
                return _systemInfo;
            }
        }

		public class ProcessInfo
		{
			public int Process;
			public int Thread;
			public int ProcessID;
			public int ThreadID;
		}

		/// <summary>
		/// Activate the running instance of this application
		/// </summary>
		/// <param name="title"></param>
		public static void ActivateApplication(string title)
		{
			IntPtr hWnd = FindWindow(null, title);
			if (hWnd != IntPtr.Zero)
				SetForegroundWindow(hWnd);
		}

		/// <summary>
		/// Is the application running?
		/// </summary>
		/// <param name="title"></param>
		/// <returns></returns>
		public static bool ApplicationRunning(string title)
		{
			IntPtr hWnd = IntPtr.Zero;
			hWnd = FindWindow(null, title);
			return (hWnd != IntPtr.Zero);
		}

		/// <summary>
		/// Return the entry assembly
		/// </summary>
		/// <returns></returns>
		public static System.Reflection.Assembly GetEntryAssembly()
		{
#if NETCF || NETCF2
			return GetEntryAssemblyNETCF();
#else
			return System.Reflection.Assembly.GetEntryAssembly();
#endif
		}

		/// <summary>
		/// return the path and filename of the entry assembly
		/// </summary>
		/// <returns></returns>
		public static string GetEntryAssemblyLocation()
		{
#if NETCF || NETCF2
			string filePath = GetModuleFileNameNETCF();
			return System.IO.Path.GetDirectoryName(filePath);
#else
			System.Reflection.Assembly assembly = System.Reflection.Assembly.GetEntryAssembly();
			if (assembly != null)
				return System.Reflection.Assembly.GetEntryAssembly().Location;
			else
				return GetModuleFileNameNETCF();
#endif
		}

		/// <summary>
		/// This is the NETCF implementation of the GetEntryAssembly part.
		/// </summary>
		/// <returns></returns>
		protected static System.Reflection.Assembly GetEntryAssemblyNETCF()
		{
			return System.Reflection.Assembly.LoadFrom(GetModuleFileNameNETCF());
		}

		/// <summary>
		/// Return the filename of the entry module
		/// </summary>
		/// <returns></returns>
		protected static string GetModuleFileNameNETCF()
		{
			const int maxPath = 255;
			byte[] buffer = new byte[maxPath * 2];

			// Gets the executable name by passing null to the module handle 
			int read = GetModuleFileNameW(IntPtr.Zero, buffer, maxPath);

			if (read == 0)
				throw new Exception("Could not get Module File Name");

			return Encoding.Unicode.GetString(buffer, 0, read * 2);
		}

		public static void SetSystemPageMemory(int storePages)
		{ 
#if NETCF || NETCF2
			SetSystemMemoryDivision(storePages);
#endif
		}

		public static string GetBaseDirectory()
		{
#if NETCF || NETCF2
			return GetEntryAssemblyLocation();
#else
			return AppDomain.CurrentDomain.BaseDirectory;
#endif
		}
    }

}

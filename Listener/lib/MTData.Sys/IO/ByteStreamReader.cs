using System;

namespace MTData.Sys.IO
{
	/// <summary>
	/// Specific exception for the ByteStreamReader, for use when the bufffer is not capable
	/// of performing the task required.
	/// </summary>
	public class ByteStreamReaderException : ApplicationException
	{	
		private int _availableSize = 0;
		private int _requiredSize = 0;
		private int _nextRead = 0;

		/// <summary>
		/// Constructor to prepare the exception for use.
		/// </summary>
		/// <param name="message"></param>
		/// <param name="availableSize"></param>
		/// <param name="requiredSize"></param>
		/// <param name="nextRead"></param>
		public ByteStreamReaderException(string message, int availableSize, int requiredSize, int nextRead) : base(message)
		{
			_availableSize = availableSize;
			_requiredSize = requiredSize;
			_nextRead = nextRead;
		}

		/// <summary>
		/// Property allowing read access to the AvailableSize
		/// </summary>
		public int AvailableSize
		{
			get{return _availableSize;}
		}

		/// <summary>
		/// Property allowing read access to the required size
		/// </summary>
		public int RequiredSize
		{
			get{return _requiredSize;}
		}

		/// <summary>
		/// Property allowing read access to the next read position
		/// </summary>
		public int NextRead
		{
			get{return _nextRead;}
		}

	}

	/// <summary>
	/// This class will provide simple parsing methods to extract 
	/// data from a byte array
	/// </summary>
	public class ByteStreamReader
	{
		private byte[] _buffer = null;
		private int _nextRead = 0;

		/// <summary>
		/// This constructor prepares the buffer for reading.
		/// </summary>
		/// <param name="buffer"></param>
		public ByteStreamReader(byte[] buffer)
		{
			_buffer = buffer;
			_nextRead = 0;
		}

		/// <summary>
		/// This is a property that identifies that we are past the end of the buffer.
		/// </summary>
		public bool EndOfBuffer
		{
			get{ return (_nextRead >= _buffer.Length); }
		}

		/// <summary>
		/// Read a single byte out of the buffer
		/// </summary>
		/// <returns></returns>
		public byte ReadByte()
		{
			EnsureSize(1);

			return _buffer[_nextRead++];
		}

		/// <summary>
		/// Indicates the length of buffer remaining to read.
		/// </summary>
		public int RemainingLength
		{
			get{ return _buffer.Length - _nextRead; }
		}

		/// <summary>
		/// Move the specified number of bytes through the stream, with
		/// the least number of operations.
		/// </summary>
		/// <param name="length"></param>
		public void MoveBytes(int length)
		{
			EnsureSize(length);
			_nextRead += length;
		}

		/// <summary>
		/// This method will peek the next value in the buffer, without moving the read pointer.
		/// </summary>
		/// <returns></returns>
		public byte PeekByte()
		{
			EnsureSize(1);
			return _buffer[_nextRead];
		}

		/// <summary>
		/// This method will allow a backlook at a previous byte.
		/// </summary>
		/// <returns></returns>
		public byte PeekLastByte()
		{
			EnsureSize(0);
			if (_nextRead == 0)
				throw new ByteStreamReaderException("CAnont read prior to Start of List", _buffer.Length, 1, _nextRead-1);

			return _buffer[_nextRead -1];
		}

		/// <summary>
		/// Copy out a byte array of a specific length.
		/// </summary>
		/// <param name="length"></param>
		/// <returns></returns>
		public byte[] ReadBytes(int length)
		{
			EnsureSize(length);
			byte[] result = new byte[length];
			Array.Copy(_buffer, _nextRead, result, 0, length);
			_nextRead += length;
			return result;
		}

		/// <summary>
		/// This method will read 4 bytes form the buffer and convert them into an integer.
		/// </summary>
		/// <returns></returns>
		public int Read4ByteInteger()
		{
			EnsureSize(4);
			int result = 0;
			result = _buffer[_nextRead+3];
			result = result << 8;
			result += _buffer[_nextRead+2];
			result = result << 8;
			result += _buffer[_nextRead+1];
			result = result << 8;
			result += _buffer[_nextRead];
			_nextRead += 4;
			return result;
		}

        /// <summary>
        /// This method will read 4 bytes form the buffer and convert them into an Uinteger.
        /// </summary>
        /// <returns></returns>
        public uint Read4ByteUInteger()
        {
            EnsureSize(4);
            uint result = 0;
            result = _buffer[_nextRead + 3];
            result = result << 8;
            result += _buffer[_nextRead + 2];
            result = result << 8;
            result += _buffer[_nextRead + 1];
            result = result << 8;
            result += _buffer[_nextRead];
            _nextRead += 4;
            return result;
        }
        /// <summary>
		/// This method will extract a 4 byte unsigned integer from the stream.
		/// </summary>
		/// <returns></returns>
		public uint Read4ByteUnsignedInteger()
		{
			EnsureSize(4);
			uint result = 0;
			result = _buffer[_nextRead+3];
			result = result << 8;
			result += _buffer[_nextRead+2];
			result = result << 8;
			result += _buffer[_nextRead+1];
			result = result << 8;
			result += _buffer[_nextRead];
			_nextRead += 4;
			return result;
		}

		/// <summary>
		/// This method will read a 2 byte integer from the stream.
		/// </summary>
		/// <returns></returns>
		public int Read2ByteInteger()
		{
			EnsureSize(2);
			int result = 0;
			result = _buffer[_nextRead+1];
			result = result << 8;
			result += _buffer[_nextRead];
			_nextRead += 2;
			return result;
		}

        /// <summary>
        /// This method will read 4 bytes form the buffer and convert them into an integer, msb to lsb.
        /// </summary>
        /// <returns></returns>
        public int Read4ByteIntegerMsbToLsb()
        {
            EnsureSize(4);
            int result = 0;
            result = _buffer[_nextRead];
            result = result << 8;
            result += _buffer[_nextRead + 1];
            result = result << 8;
            result += _buffer[_nextRead + 2];
            result = result << 8;
            result += _buffer[_nextRead + 3];
            _nextRead += 4;
            return result;
        }

        /// <summary>
        /// This method will extract a 4 byte unsigned integer from the stream, msb to lsb.
        /// </summary>
        /// <returns></returns>
        public uint Read4ByteUnsignedIntegerMsbToLsb()
        {
            EnsureSize(4);
            uint result = 0;
            result = _buffer[_nextRead];
            result = result << 8;
            result += _buffer[_nextRead + 1];
            result = result << 8;
            result += _buffer[_nextRead + 2];
            result = result << 8;
            result += _buffer[_nextRead + 3];
            _nextRead += 4;
            return result;
        }

        /// <summary>
        /// This method will read a 2 byte integer from the stream, msb to lsb.
        /// </summary>
        /// <returns></returns>
        public int Read2ByteIntegerMsbToLsb()
        {
            EnsureSize(2);
            int result = 0;
            result = _buffer[_nextRead];
            result = result << 8;
            result += _buffer[_nextRead + 1];
            _nextRead += 2;
            return result;
        }
        /// <summary>
		/// This method will extract a fixed length string, which has been padded by the padding value specified.
		/// </summary>
		/// <param name="length"></param>
		/// <param name="padding"></param>
		/// <returns></returns>
		public string ReadASCIIStringFixedLength(int length, byte padding)
		{
			EnsureSize(length);


			int loop = 0; 
			while(loop < length) 
			{
				if(_buffer[_nextRead + loop] == padding)
					break;
				loop++;
			}

			//	find the first 
			string value = System.Text.ASCIIEncoding.ASCII.GetString(_buffer, _nextRead, loop);
			_nextRead += length;
			return value;
		}

		/// <summary>
		/// This method will read a variable length string from the stream.
		/// If maxLength == -1, then there is no upper size limit.
		/// maxLength is the maximum number of bytes for the string, EXCLUSIVE of delimiter.
		/// </summary>
		/// <param name="maxLength"></param>
		/// <param name="delimiter"></param>
		/// <returns></returns>
		public string ReadASCIIStringDelimited(int maxLength, byte delimiter)
		{
			return ReadASCIIStringDelimited(maxLength, delimiter, true);
		}

		/// <summary>
		/// This method will read a variable length string from the stream.
		/// If maxLength == -1, then there is no upper size limit.
		/// maxLength is the maximum number of bytes for the string, EXCLUSIVE of delimiter.
		/// </summary>
		/// <param name="maxLength"></param>
		/// <param name="delimiter"></param>
		/// <param name="readDelimiter">Indicates that the delimiter should also be read from the buffer</param>
		/// <returns></returns>
		public string ReadASCIIStringDelimited(int maxLength, byte delimiter, bool readDelimiter)
		{
			int loop = 0;
			while(true)
			{
				EnsureSize(loop+1);

				//	Note that maxlength is the max length of the string in bytes.. exclusive of delimiter
				if ((maxLength >= 0) && (loop > maxLength))
					break;

				if (_buffer[_nextRead + loop] == delimiter)
					break;
				loop++;
			}

			string value = System.Text.ASCIIEncoding.ASCII.GetString(_buffer, _nextRead, loop);

			if (readDelimiter)
				_nextRead += loop+1; //Loop + delimiter
			else
				_nextRead += loop;	//loop

			return value;
		}

		/// <summary>
		/// This method will read a variable length string from the stream.
		/// If maxLength == -1, then there is no upper size limit.
		/// maxLength is the maximum number of bytes for the string, EXCLUSIVE of delimiter.
		/// This method takes an array of possible delimiters.
		/// </summary>
		/// <param name="maxLength"></param>
		/// <param name="delimiter"></param>
		/// <returns></returns>
		public string ReadASCIIStringDelimited(int maxLength, byte[] delimiters)
		{
			return ReadASCIIStringDelimited(maxLength, delimiters, true);
		}

		/// <summary>
		/// This method will read a variable length string from the stream.
		/// If maxLength == -1, then there is no upper size limit.
		/// maxLength is the maximum number of bytes for the string, EXCLUSIVE of delimiter.
		/// This method takes an array of possible delimiters.
		/// </summary>
		/// <param name="maxLength"></param>
		/// <param name="delimiters"></param>
		/// <param name="readDelimiter"></param>
		/// <returns></returns>
		public string ReadASCIIStringDelimited(int maxLength, byte[] delimiters, bool readDelimiter)
		{
			int loop = 0;
			while(true)
			{
				EnsureSize(loop+1);

				//	Note that maxlength is the max length of the string in bytes.. exclusive of delimiter
				if ((maxLength >= 0) && (loop > maxLength))
					break;

				bool isDelimiter = false;
				for(int delimiterLoop = 0; delimiterLoop < delimiters.Length; delimiterLoop++)
					if (_buffer[_nextRead + loop] == delimiters[delimiterLoop])
					{
						isDelimiter = true;
						break;
					}
				if (isDelimiter)
					break;
				loop++;
			}

			string value = System.Text.ASCIIEncoding.ASCII.GetString(_buffer, _nextRead, loop);
			if (readDelimiter)
				_nextRead += loop+1; //Loop + delimiter
			else
				_nextRead += loop;		//Loop

			return value;
		}

        public float Read4ByteFloat()
        {
            byte[] data = ReadBytes(4);
            return BitConverter.ToSingle(data, 0);
        }

        public float Read4ByteFloatMsbToLsb()
        {
            byte[] data = ReadBytes(4);
            
            byte tmp = data[0];
            data[0] = data[3];
            data[3] = tmp;

            tmp = data[1];
            data[1] = data[2];
            data[2] = tmp;
            
            return BitConverter.ToSingle(data, 0);
        }

        /// <summary>
		/// This method ensures that there is enough space in the buffer for the item requested.
		/// </summary>
		/// <param name="requiredSize"></param>
		private void EnsureSize(int requiredSize)
		{
			if ((_nextRead + requiredSize) > _buffer.Length)
				throw new ByteStreamReaderException("Attempt to read past end of buffer", _buffer.Length, requiredSize, _nextRead);
		}
	}
}

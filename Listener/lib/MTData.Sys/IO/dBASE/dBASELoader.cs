using System;
using System.Collections;
using System.Text;
using System.IO;
using System.Data;

namespace MTData.Sys.IO.dBASE
{
	public class dBASEReader : ContextProvider.IContextProvider
	{
		private string _fileName = "";

		private Stream _stream = null;

		private dBASEHeader _header = null;
		private dBASEFields _fields = null;

		public dBASEReader(Stream stream)
		{
			_fileName = "";
			_stream = stream;
			LoadHeader();
			LoadFieldDefinitions();
		}

		public dBASEReader(string fileName)
		{
			_fileName = fileName;
			_stream = new FileStream(_fileName, FileMode.Open, FileAccess.Read, FileShare.Read);
			LoadHeader();
			LoadFieldDefinitions();
		}

		public void Close()
		{
			_stream.Close();
		}

		public dBASEHeader Header{ get{ return _header; }}
		public dBASEFields Fields{ get{ return _fields; }}

		protected void LoadHeader()
		{
			_header = new dBASEHeader();
			_header.Version = _stream.ReadByte();
			_header.LastModified = new DateTime(1900 + _stream.ReadByte(), _stream.ReadByte(), _stream.ReadByte());
			_header.RecordCount = Read4ByteInteger(_stream);
			_header.HeaderLength = Read2ByteInteger(_stream);
			_header.RecordLength = Read2ByteInteger(_stream);
			int temp = _stream.ReadByte();
			temp = _stream.ReadByte();
			_header.IncompleteTransaction = _stream.ReadByte();
			_header.EncryptionFlag = _stream.ReadByte();
			for(int loop = 0; loop < 12; loop++)
				temp = _stream.ReadByte();
			_header.MDXFlag = _stream.ReadByte();
			_header.DriverID = _stream.ReadByte();
			temp = _stream.ReadByte();
			temp = _stream.ReadByte();
		}

		protected void LoadFieldDefinitions()
		{
			_fields = new dBASEFields();

			//	Field Descriptor array
			int start = _stream.ReadByte();
			while (start != (byte)0x0D)
			{
				dBASEFieldDescriptor field = new dBASEFieldDescriptor();
				byte[] name = new byte[11];
				name[0] = Convert.ToByte(start);
				_stream.Read(name, 1, 10);
				int length = 0;
				while(length < name.Length)
				{
					if (name[length] == 0)
						break;
					length++;
				}
				field.Name = System.Text.ASCIIEncoding.ASCII.GetString(name, 0, length);
				string tempName = field.Name;
				int tempInt = 1;
				while(_fields.FieldExists(tempName))
					tempName = string.Format("{0}({1})", field.Name, tempInt++);
				field.Name = tempName;
				char type = (char)_stream.ReadByte();
				int temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				field.Length = _stream.ReadByte();
				field.DecimalCount = _stream.ReadByte();

				switch(type)
				{
					case 'C' :
						field.FieldType = DbType.String;
						break;
					case 'F' :
						if (field.DecimalCount > 0)
							field.FieldType = DbType.Double;
						else
							field.FieldType = DbType.Int32;
						break;
					case 'N' :
						if (field.DecimalCount > 0)
							field.FieldType = DbType.Double;
						else
							field.FieldType = DbType.Int32;
						break;
					case 'D' : 
						field.FieldType = DbType.Date;
						break;
					default :
						field.FieldType = DbType.Object;
						break;
				}
						
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				field.WorkAreaID = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				field.SetFieldsFlag = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();
				temp = _stream.ReadByte();

				field.IndexFieldFlag = _stream.ReadByte();

				_fields.AddFieldDefinition(field);
					
				start = _stream.ReadByte();
			}
		}

		private int _currentRecord = -1;
		private byte[] _buffer = null;

		public bool Read()
		{
			_currentRecord++;
			if (_currentRecord < _header.RecordCount)
			{
				//	Read in the full length of the record.
				_buffer = new byte[_header.RecordLength];

				_stream.Read(_buffer, 0, _header.RecordLength);
				return true;
			}
			else
				return false;
		}

		/// <summary>
		/// REturn the value for the specified field..
		/// </summary>
		public object GetValue(string name)
		{
            dBASEFieldDescriptor field = (dBASEFieldDescriptor)_fields[name];
			if (field != null)
				return GetValue(field.Index);
			else
				return null;
		}

		public object this[int index]
		{
			get{ return GetValue(index); }
		}

		public object this[string name]
		{
			get{ return GetValue(name); }
		}

		public dBASERecord.dBASERecordState State
		{
			get{ return (dBASERecord.dBASERecordState)_buffer[0]; }
		}

		public object GetValue(int index)
		{
			return GetValue(_fields[index]);
		}

		private string ReadString(int offset, int length)
		{
			int index = offset;
			while((index < (offset + length)) && (_buffer[index] != 0))
				index++;

			return System.Text.ASCIIEncoding.ASCII.GetString(_buffer, offset, index - offset).Trim();
		}

		private int Read2ByteInteger(Stream stream)
		{
			int result = stream.ReadByte();
			result += (stream.ReadByte() << 8);
			return result;
		}

		private int Read4ByteInteger(Stream stream)
		{
			int result = stream.ReadByte();
			result += (stream.ReadByte() << 8);
			result += (stream.ReadByte() << 16);
			result += (stream.ReadByte() << 24);
			return result;
		}

		private object GetValue(dBASEFieldDescriptor field)
		{
			int offset = 1;	//	don't forget state byte
			for(int loop = 0; loop < field.Index; loop++)
				offset += ((dBASEFieldDescriptor)_fields[loop]).Length;
	
			object result = null;
			switch (field.FieldType)
			{
				case DbType.String :
				{
					result = ReadString(offset, field.Length);
					break;
				}
				case DbType.Double :
				{
					string newValue = ReadString(offset, field.Length);
					if (newValue == "")
						result = 0;
					else
						result = Double.Parse(newValue);
					break;
				}
				case DbType.Int32 :
				{
					string newValue = ReadString(offset, field.Length);
					if (newValue == "")
						result = 0;
					else
						result = Int32.Parse(newValue);
					break;
				}
				case DbType.Date :
				{
					string newValue = ReadString(offset, field.Length);
					if (newValue.Length == 0)
						result = DateTime.MinValue;
					else
					{
						int year = Int32.Parse(newValue.Substring(0, 4));
						int month = Int32.Parse(newValue.Substring(4, 2));
						int day = Int32.Parse(newValue.Substring(6, 2));

						result = new DateTime(year, month, day); 
					}
					break;
				}
				default : 
				{
					// read the correct bytes
					//for(int byteLoop = 0; byteLoop < field.Length; byteLoop++)
					//	_stream.ReadByte();
					break;
				}
			}
			return result;
		}
		#region IContextProvider Members

		public void RegisterChange(string path)
		{
			if (Change != null)
				Change(this, path);
		}

		object ContextProvider.IContextProvider.this[string path]
		{
			get
			{
				return this[path];
			}
			set
			{
				//	Does nothing..
				//throw new NotSupportedException("The settings of field values in dBaseRecords is not currently supported");
			}
		}

		public bool Contains(string path)
		{
			return (_fields[path] != null);
		}

		private object _syncRoot = new object();

		public object SyncRoot

		{
			get
			{
				return _syncRoot;
			}
		}

		public event ContextProvider.ContextProviderChangedDelegate Change;

		#endregion
	}

	/// <summary>
	/// Summary description for DBaseLoader.
	/// </summary>
	public class dBASELoader
	{
		private string _fileName = "";
		private Hashtable _fields = new Hashtable();

		public dBASELoader(string fileName)
		{
			_fileName = fileName;	
		}

		public dBASEDataSet LoadFile(string fileName)
		{
			dBASEDataSet result = new dBASEDataSet();
			result.FileName = fileName;

			dBASEReader reader = new dBASEReader(fileName);
			try
			{
				result.Header = reader.Header;
				result.Fields = reader.Fields;

				//	the next item is the database container???
				//for(int loop = 0; loop < 264; loop++)
				//	stream.ReadByte();
				
				while (reader.Read())
				{
					//	Record structures...
					dBASERecord record = new dBASERecord(result.Fields);
					record.State = reader.State;
					object[] values = new object[result.Fields.Count];

					for(int fieldLoop = 0; fieldLoop < result.Fields.Count; fieldLoop++)
						values[fieldLoop] = reader[fieldLoop];
							
					record.SetValues(values);
					result.Records.AddRecord(record);
				}
			}
			finally
			{
				reader.Close();
			}
			return result;
		}

		private byte[] _buffer = new byte[1024];

		public string ReadString(Stream stream, int length)
		{
			stream.Read(_buffer, 0, length);
			int textLength = 0;
			while(textLength < length)
			{
				if (_buffer[textLength] == 0)
					break;
				textLength++;
			}
			return (string)System.Text.ASCIIEncoding.ASCII.GetString(_buffer, 0, textLength);
		}
	}
}

using System;
using System.Collections;
using System.Text;

namespace MTData.Sys.IO.dBASE
{
	/// <summary>
	/// This class controls a list of records, each of which has data from one dBASE row.
	/// </summary>
	public class dBASERecords
	{
		private ArrayList _records = new ArrayList();
		private dBASEFields _fields = null;

		/// <summary>
		/// This constructor takes the field definitions as a parameter.
		/// </summary>
		/// <param name="fields"></param>
		public dBASERecords(dBASEFields fields)
		{
			_fields = fields;
		}

		/// <summary>
		/// Add a record to the list
		/// </summary>
		/// <param name="record"></param>
		public void AddRecord(dBASERecord record)
		{
			_records.Add(record);
		}

		/// <summary>
		/// This is the number of records in the list.
		/// </summary>
		public int Count
		{
			get
			{
				return _records.Count;
			}
		}

		/// <summary>
		/// This is an accessor for the list.
		/// </summary>
		public dBASERecord this[int index]
		{
			get
			{
				return (dBASERecord)_records[index];
			}
		}

		/// <summary>
		/// Returns a human readable version of the dataset.
		/// </summary>
		/// <returns></returns>
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append("\r\n=============== Records ====================\r\n");

			for(int fieldLoop = 0; fieldLoop < _fields.Count; fieldLoop++)
			{
				dBASEFieldDescriptor field = _fields[fieldLoop];
				builder.Append(field.Name.PadRight(field.Length + 10, ' '));
			}

			for(int loop = 0; loop < _records.Count; loop++)
			{
				builder.Append("\r\n");
				dBASERecord record = (dBASERecord)_records[loop];
				builder.Append(record.ToString());
			}

			return builder.ToString();
		}

	}

	/// <summary>
	/// dBaseRecord.
	/// </summary>
	public class dBASERecord
	{
		public enum dBASERecordState : byte
		{
			Valid = (byte)0x20,
			Deleted = (byte)0x2A
		}

		public dBASERecordState State = dBASERecordState.Valid;
		private dBASEFields _fields = null;
		private object[] _values = null;

		public dBASERecord(dBASEFields fields)
		{
			_fields = fields;
			_values = new object[fields.Count];
		}

		public void SetValues(object[] values)
		{
			_values = values;
		}

		public object this[int index]
		{
			get
			{
				return _values[index];
			}
			set
			{
				_values[index] = value;
			}
		}

		public object this[string fieldName]
		{
			get
			{
				dBASEFieldDescriptor field = _fields[fieldName];
				return _values[field.Index];
			}
			set
			{
				dBASEFieldDescriptor field = _fields[fieldName];
				_values[field.Index] = value;
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			for(int fieldLoop = 0; fieldLoop < _fields.Count; fieldLoop++)
			{
				dBASEFieldDescriptor field = _fields[fieldLoop];
				if (_values[fieldLoop] == null)
					_values[fieldLoop] = "NULL";

				builder.Append(_values[fieldLoop].ToString().PadRight(field.Length + 10, ' '));
			}
			return builder.ToString();
		}

	}
}

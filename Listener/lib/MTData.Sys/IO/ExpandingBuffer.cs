﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

namespace MTData.Sys.IO
{
    public class ExpandingByteBuffer
    {
        private byte[] _buffer = null;
        protected int _size = 0;

        public ExpandingByteBuffer(int initialSize)
        {
            _buffer = new byte[initialSize];
        }

        public virtual void Reset()
        {
            _size = 0;
        }

        public virtual void Add(byte[] bytes, int offset, int count)
        {
            if (_size + count > _buffer.Length)
            {
                // Resize the buffer
                byte[] newBuffer = new byte[_buffer.Length * 2 + count];
                _buffer.CopyTo(newBuffer, 0);
                _buffer = newBuffer;
            }

            Array.Copy(bytes, offset, _buffer, _size, count);
            _size += count;
        }

        public virtual void Add(byte[] bytes)
        {
            Add(bytes, 0, bytes.Length);
        }

        public virtual void Add(byte[] bytes, int count)
        {
            Add(bytes, 0, count);
        }

        public virtual void Add(byte b)
        {
            if (_size + 1 > _buffer.Length)
            {
                // Resize the buffer
                byte[] newBuffer = new byte[_buffer.Length * 2 + 1];
                _buffer.CopyTo(newBuffer, 0);
                _buffer = newBuffer;
            }

            _buffer[_size++] = b;
        }

        public virtual byte[] TrimmedBuffer
        {
            get
            {
                byte[] trimmedBuffer = new byte[_size];
                Array.Copy(_buffer, 0, trimmedBuffer, 0, _size);
                return trimmedBuffer;
            }
        }
    }
}

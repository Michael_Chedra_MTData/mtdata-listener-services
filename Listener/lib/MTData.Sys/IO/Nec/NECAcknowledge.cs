﻿using System;

namespace MTData.Sys.IO.Nec
{
    public class NECAcknowledge : BasePacket
    {
        private int _sequenceNumber;

        public int SequenceNumber
        {
            get { return _sequenceNumber; }
            set { _sequenceNumber = value; }
        }

        #region Constructor / Destructor

        // Constructor
        public NECAcknowledge(int sequenceNumber)
        {
            _sequenceNumber = sequenceNumber;
        }
        #endregion

        #region Public Conversion Functions

        // Convert Packet Object to Byte Array
        public override byte[] ToByte()
        {
            // Variables
            ExpandingByteBuffer buffer = new ExpandingByteBuffer(2);

            buffer.Add((byte)MessageByte.ACK);
            buffer.Add((byte)_sequenceNumber);

            return buffer.TrimmedBuffer;
        }

        // Convert Source Bytes -> Packet Object
        public override void Parse(byte[] data, ref int byteIndex)
        {
            if (data[0] != (byte)MessageByte.ACK)
                throw new InvalidPacketException("Expected ACK byte for ACK packet");

            _sequenceNumber = data[1];
        }
        #endregion
    }

}

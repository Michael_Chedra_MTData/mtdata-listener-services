using System;
using System.Xml;
using System.Threading;
using log4net;
using MTData.Sys.Comms;
using MTData.Sys.Config;
using MTData.Sys.IO.Serial;
using MTData.Sys.Services;
using MTData.Sys.Threading;

namespace MTData.Sys.IO.Nec
{
    /// <summary>
    /// The Nec Service
    /// </summary>
    public class NecService : IService, INECComs, IRunnable, IConfigurable
    {
        #region private fields
        private ILog _log = LogManager.GetLogger(typeof(NecService));

        private string _name = "NEC";
        private ProviderCollection _providers = new ProviderCollection();

        private ISerialIO _serialIo;
        private INECComs _necComms;

        private IConfigSection _streamSection;
        private ISerialIOFactory _factory;
        private Timer _timer;
        private IEventFactory _eventFactory;
        private bool _startNec;
        private int _retryTimeOut;
        #endregion

        #region IService Members

        public string Name
        {
            get { return _name; }
        }

        public void AddProvider(IProvider provider)
        {
            _providers.Add(provider);
        }

        public void Initialise(string name, IConfigSection configSection)
        {
            _name = name;
            
            //create the NEC comms
            _factory = (ISerialIOFactory)configSection.CreateInstance("SerialIOFactoryType");
            _streamSection = configSection.GetConfigSection("NECComs");
            _eventFactory = (IEventFactory)configSection.CreateInstance("EventFactoryType");
            _retryTimeOut = configSection["RetryTimeout", 5000];

            CreateNec();
        }

        #endregion

		#region IConfigurable Members

		/// <summary>
		/// This caters for loading by the alternative config type.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="config"></param>
		/// <param name="path"></param>
		/// <param name="configNode"></param>
		public void Configure(object context, MTData.Sys.Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			if (configNode != null)
			{
				Loader.Loader loader = new Loader.Loader();
				if (_log.IsInfoEnabled)
					_log.Info("Configuring NEC Stream Factory");

				XmlNode serialIONode = configNode.SelectSingleNode("SerialIO");
				if (serialIONode == null)
				{
					if (_log.IsWarnEnabled)
						_log.Warn("No config node available for Stream Factory");
				}
				else
				{
					ISerialIOFactoryEx serialIOFactory = (ISerialIOFactoryEx)loader.Create(context, serialIONode);
					if (_log.IsInfoEnabled)
						_log.InfoFormat("Configuring NEC Stream Factory : {0}", serialIOFactory.GetType().FullName);

					while (_serialIo == null)
					{
						try
						{
							_serialIo = serialIOFactory.Create();
						}
						catch (Exception e)
						{
							_log.ErrorFormat("Unable to create NECComs. {0}", e.Message);
							Thread.Sleep(1000);
						}
					}
				}

				XmlNode eventFactoryNode = configNode.SelectSingleNode("EventFactory");
				if (eventFactoryNode == null)
				{
					if (_log.IsWarnEnabled)
						_log.Warn("No config node available for NEC Event Factory");
				}
				else
				{
					IEventFactory eventFactory = (IEventFactory)loader.Create(context, eventFactoryNode);
					_necComms = new NECComs(_serialIo, eventFactory);

					_necComms.AckPacketReceived += new PacketHandler(NecAckPacketReceived);
					_necComms.PacketReceived += new PacketHandler(NecPacketReceived);
                    _necComms.OnPacketSentOrRecieved += new CommsPacketSentOrReceived(NecPacketSentOrRecieved);
                    _necComms.OnCommsInterrupted += new CommsDelegate(CommsInterrupted);
                    _necComms.OnCommsReconnected += new CommsDelegate(CommsReconnected);

                    XmlNode node = configNode.Attributes.GetNamedItem("Name");
					if (node != null)
						_name = node.Value;

					node = configNode.Attributes.GetNamedItem("RetryTimeout");
					if (node != null)
						_necComms.RetryTimeout = Int32.Parse(node.Value);
				}
			}
			else
				if (_log.IsWarnEnabled)
					_log.Warn("No config node available for NecService");
		}

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_providers != null)
            {
                foreach (IProvider provider in _providers)
                {
                    if (provider is IDisposable)
                        ((IDisposable)provider).Dispose();
                }
            }
            _providers = null;

            if (_necComms != null)
            {
                _necComms.Dispose();
            }
            if (_serialIo != null)
            {
                _serialIo.Dispose();
            }
        }

        #endregion

        #region IVersion Members

        public string VersionNumber
        {
            get { return string.Format("{0}.{1}", MajorNumber, MinorNumber); }
        }

        public int MajorNumber
        {
            get { return GetType().Assembly.GetName().Version.Major; }
        }

        public int MinorNumber
        {
            get { return GetType().Assembly.GetName().Version.Minor; }
        }

        public DateTime Date
        {
            get
            {
                String path = GetType().Assembly.ManifestModule.FullyQualifiedName;
                System.IO.FileInfo f = new System.IO.FileInfo(path);
                return f.CreationTime;
            }
        }

        #endregion

        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return _providers.GetEnumerator();
        }

        #endregion

        #region INECComs Members

        public NECPacket SendPacket(NECPacket necPacket, int retries)
        {
            if (_necComms != null)
            {
                return _necComms.SendPacket(necPacket, retries);
            }
            return necPacket;
        }

        public event PacketHandler PacketReceived;

        public event PacketHandler AckPacketReceived;

        public CommsCounters Counters
        {
            get 
            {
                if (_necComms != null)
                {
                    return _necComms.Counters;
                }
                return new CommsCounters();
            }
        }

        public void Start()
        {
            if (_necComms != null)
            {
                _necComms.Start();
            }
            else
            {
                _startNec = true;
            }
        }

        public int RetryTimeout
        {
            get
            {
                if (_necComms != null)
                {
                    return _necComms.RetryTimeout;
                }
                return _retryTimeOut;
            }
            set
            {
                if (_necComms != null)
                {
                    _necComms.RetryTimeout = value;
                }
            }
        }

        #endregion

        #region Nec Events
        private void NecPacketReceived(NECPacket necPacket, ref bool handled)
        {
            if (PacketReceived != null)
            {
                PacketReceived(necPacket, ref handled);
            }
        }

        private void NecAckPacketReceived(NECPacket necPacket, ref bool handled)
        {
            if (AckPacketReceived != null)
            {
                AckPacketReceived(necPacket, ref handled);
            }
        }

        private void NecPacketSentOrRecieved(bool sent)
        {
            if (OnPacketSentOrRecieved != null)
            {
                OnPacketSentOrRecieved(sent);
            }
        }

        private void CommsInterrupted()
        {
            if (OnCommsInterrupted != null)
            {
                OnCommsInterrupted();
            }
        }

        private void CommsReconnected()
        {
            if (OnCommsReconnected != null)
            {
                OnCommsReconnected();
            }
        }

        #endregion

        #region IRunnable Members

        public void Run()
        {
            //start the nec
            Start();
        }

        #endregion

        #region private methods
        private void CreateNec()
        {
            try
            {
                _serialIo = _factory.Create(_streamSection, this);

                _necComms = new NECComs(_serialIo, _eventFactory);
                _necComms.AckPacketReceived += new PacketHandler(NecAckPacketReceived);
                _necComms.PacketReceived += new PacketHandler(NecPacketReceived);
                _necComms.OnPacketSentOrRecieved += new CommsPacketSentOrReceived(NecPacketSentOrRecieved);
                _necComms.OnCommsInterrupted += new CommsDelegate(CommsInterrupted);
                _necComms.OnCommsReconnected += new CommsDelegate(CommsReconnected);
                _necComms.RetryTimeout = _retryTimeOut;

                if (_startNec)
                {
                    _necComms.Start();
                }
            }
            catch (Exception e)
            {
                _log.ErrorFormat("Unable to create NECComs. {0}", e.Message);
                _timer = new Timer(new TimerCallback(RetryCreateNec), null, 1000, Timeout.Infinite);
            }
        }

        private void RetryCreateNec(object state)
        {
            _timer.Dispose();
            CreateNec();
        }
        #endregion

        #region ICommsService
        public bool HasCommsReceivedData { get { return _necComms != null ? _necComms.HasCommsReceivedData : false; } }
        public bool IsCommsInterrupted { get { return _necComms != null ? _necComms.IsCommsInterrupted : false; } }
        public DateTime LastPacketSent { get { return _necComms != null ? _necComms.LastPacketSent : DateTime.MinValue; } }
        public DateTime LastPacketReceived { get { return _necComms != null ? _necComms.LastPacketSent : DateTime.MinValue; } }

        public event CommsPacketSentOrReceived OnPacketSentOrRecieved;
        public event CommsDelegate OnCommsInterrupted;
        public event CommsDelegate OnCommsReconnected;
        #endregion
    }
}

﻿using System;

namespace MTData.Sys.IO.Nec
{
    public class BufferTooShortException : ApplicationException
    {
        public BufferTooShortException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public BufferTooShortException(string message)
            : base(message)
        {
        }

        public BufferTooShortException()
            : base("Buffer too Short")
        {
        }
    }
}

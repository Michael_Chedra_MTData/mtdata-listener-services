﻿using System;

namespace MTData.Sys.IO.Nec
{
    public class InvalidPacketException : ApplicationException
    {
        public InvalidPacketException(string message, Exception innerException)
            : base(message, innerException)
        {
        }

        public InvalidPacketException(string message)
            : base(message)
        {
        }

        public InvalidPacketException()
            : base("Invalid Packet")
        {
        }
    }
}

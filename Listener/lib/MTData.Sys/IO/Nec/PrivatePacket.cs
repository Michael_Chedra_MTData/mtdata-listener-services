﻿using System;
using System.IO;

namespace MTData.Sys.IO.Nec
{
    public class PrivatePacket : BasePacket
    {
        #region Private Variables
        byte _privateMessage;
        #endregion

        #region constructor
        public PrivatePacket(byte privateMessage)
        {
            _privateMessage = (byte)privateMessage;
        }
        #endregion


        #region Public Properties

        public byte PrivateMessage
        {
            get { return _privateMessage; }
            set { _privateMessage = value; }
        }

        #endregion


        #region Public Functions

        /// <summary>
        /// Convert the object properties into a byte array.
        /// </summary>
        /// <returns></returns>
        public override void ToStream(Stream stream)
        {
            stream.WriteByte((byte)_privateMessage);
        }

        public override void Parse(byte[] data, ref int byteIndex)
        {
            byte received = data[byteIndex++];

            if (received != _privateMessage)
                throw new InvalidPacketException("NEC Private Command Invalid, expected " + _privateMessage.ToString() + " but received " + received.ToString());
        }

        #endregion
    }
}

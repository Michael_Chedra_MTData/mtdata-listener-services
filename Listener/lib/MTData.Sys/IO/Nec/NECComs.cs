﻿using System;
using System.Collections;
using System.Threading;
using log4net;
using MTData.Sys.Comms;
using MTData.Sys.IO.Serial;
using MTData.Sys.Threading;

namespace MTData.Sys.IO.Nec
{
    public class NECComs : INECComs
    {
        private readonly string HEARTBEAT = "HEARTBEAT";

        private class PriorityQueue : IComparer
        {
            private int _priority;
            private Queue _queue = new Queue();

            public PriorityQueue(int priority)
            {
                _priority = priority;
            }

            public void Enqueue(NECPacket item)
            {
                lock (_queue)
                    _queue.Enqueue(item);
            }

            public NECPacket Dequeue()
            {
                lock (_queue)
                    return _queue.Dequeue() as NECPacket;
            }

            public int Count
            {
                get
                {
                    lock (_queue)
                        return _queue.Count;
                }
            }

            public int Priority
            {
                get { return _priority; }
            }

            public int Compare(object x, object y)
            {
                PriorityQueue queueX = x as PriorityQueue;
                PriorityQueue queueY = y as PriorityQueue;

                return queueX.Priority - queueY.Priority;
            }
        }

        protected static readonly ILog _log = LogManager.GetLogger(typeof(NECComs));
        protected static readonly bool _logIsDebugEnabled = _log.IsDebugEnabled;
        protected static readonly bool _logIsErrorEnabled = _log.IsErrorEnabled;
        protected static readonly bool _logIsFatalEnabled = _log.IsFatalEnabled;
        protected static readonly bool _logIsInfoEnabled = _log.IsInfoEnabled;
        protected static readonly bool _logIsWarnEnabled = _log.IsWarnEnabled;

        private ISerialIO _serialIO;
        private bool _disposed = false;

        private NECPacketParser _packetParser = new NECPacketParser(128);
        private UnescapingBuffer _unescaper;

        private Queue _rxPackets;
        private Queue _rxAckPackets;
        private PriorityQueue[] _txPriorityQueues;

        private IEventFactory _eventFactory;
        private IResetEvent _disposeEvent;
        private IResetEvent _txEvent;
        private IResetEvent _rxEvent;
        private IResetEvent _rxAckEvent;

        private IResetEvent[] _txWaitHandles;
        private IResetEvent[] _rxWaitHandles;
        private IResetEvent[] _rxAckWaitHandles;

        private Thread _txThread;
        private Thread _rxThread;
        private Thread _rxAckThread;
        private object _sendSync = new object();

        private byte _nextTxSequenceNumber = 0;
        private int _lastRxSequenceNumber = -1;		// Duplicate detection
        private int _retryTimeout = 5000;

        private NECAcknowledge _ackNecPacket;
        private CommsCounters _counters = new CommsCounters();

        private ResponseTimer _retryTimer;
        private DateTime _lastNecPacketSent;
        private DateTime _lastNecPacketReceived;
        private bool _hasCommsReceivedData;
        private bool _isCommsInterrupted;


        public event PacketHandler PacketReceived;
        public event PacketHandler AckPacketReceived;

        public bool DisableTransmit = false;

        public CommsCounters Counters
        {
            get { return _counters; }
        }

        public int RetryTimeout
        {
            get { return _retryTimeout; }
            set { _retryTimeout = value; }
        }

        public NECComs(ISerialIO serialIO, IEventFactory eventFactory)
        {
            _serialIO = serialIO;
            _serialIO.RxHandler = new SerialIORxHandler(ReceiveData);

            _unescaper = new UnescapingBuffer(0, 0xE7, 0x1B, 0x20);
            _ackNecPacket = new NECAcknowledge(0);

            _rxPackets = new Queue();
            _rxAckPackets = new Queue();
            _txPriorityQueues = new PriorityQueue[0];

            _eventFactory = eventFactory;
            _retryTimer = new ResponseTimer(eventFactory);
            _disposeEvent = _eventFactory.CreateManualResetEvent(false);
            _txEvent = _eventFactory.CreateManualResetEvent(false);
            _rxEvent = _eventFactory.CreateManualResetEvent(false);
            _rxAckEvent = _eventFactory.CreateManualResetEvent(false);
            _lastNecPacketReceived = DateTime.MinValue;
            _lastNecPacketSent = DateTime.MinValue;

            _txWaitHandles = new IResetEvent[] { _disposeEvent, _txEvent };
            _rxWaitHandles = new IResetEvent[] { _disposeEvent, _rxEvent };
            _rxAckWaitHandles = new IResetEvent[] { _disposeEvent, _rxAckEvent };

            _unescaper.UnescapeComplete += new UnescapingBuffer.DataEvent(UnescapingBuffer_Complete);
            _retryTimer.ResponseTimedOut += new ResponseTimer.ResponseTimerEvent(_retryTimer_ResponseTimedOut);

            _txThread = new Thread(new ThreadStart(TxThread));
            _rxThread = new Thread(new ThreadStart(RxThread));
            _rxAckThread = new Thread(new ThreadStart(RxAckThread));
        }

        private void _retryTimer_ResponseTimedOut(object cookie, object data)
        {
            if (cookie.Equals(HEARTBEAT))
            {
                PrivatePacket payload = new PrivatePacket((byte)NECMessageType.NEC_HEARTBEAT);
                NECPacket packet = new NECPacket(MessageType.NEC_PRIVATE_COMMAND, payload.ToByte());
                SendPacket(packet, 0);

                _retryTimer.SetTimer(HEARTBEAT, null, 1000);
            }
            else
            {
                NECPacket necPacket = (NECPacket)data;

                if (_logIsDebugEnabled)
                    _log.Debug(string.Format("Packet # {0} Timed Out RL {1}", necPacket.SequenceNumber, necPacket.RetriesLeft));

                if (--necPacket.RetriesLeft > 0)
                {
                    _counters.Retries++;
                    EnqueueRequest(necPacket);
                }
                else
                {
                    //comms is interrupted when no retries left.
                    if (!_isCommsInterrupted)
                    {
                        _isCommsInterrupted = true;
                        if (OnCommsInterrupted != null)
                        {
                            OnCommsInterrupted();
                        }
                    }
                    _counters.FailedTx++;
                    if (necPacket.TxFailEvent != null)
                        necPacket.TxFailEvent.Set();
                }
            }
        }

        public void Start()
        {
            _hasCommsReceivedData = false;
            _isCommsInterrupted = false;
            _txThread.Start();
            _rxThread.Start();
            _rxAckThread.Start();

            //start heatbeat timer
            _retryTimer.SetTimer(HEARTBEAT, null, 1000);

            if (OnCommsReconnected != null)
            {
                OnCommsReconnected();
            }
        }

        private void TxThread()
        {
            do
            {
                try
                {
                    // Wait for { DisposeEvent, TxEvent, RxEvent }
                    switch (_eventFactory.WaitAny(_txWaitHandles))
                    {
                        case (WaitResult.Object_0 + 1):						// Tx Event
                            {
                                TxPacket();
                                break;
                            }

                        default:										// Disposed or Abandoned
                            break;
                    }
                }
                catch (Exception ex)
                {
                    if (_logIsErrorEnabled)
                        _log.ErrorFormat("TxThread(): Exception {0}", ex.Message);
                }
            } while (!_disposed);
        }

        private void RxThread()
        {
            do
            {
                try
                {
                    // Wait for { DisposeEvent, RxEvent }
                    switch (_eventFactory.WaitAny(_rxWaitHandles))
                    {
                        case (WaitResult.Object_0 + 1):					// Rx Event
                            {
                                RxPacket();
                                break;
                            }

                        default:										// Disposed or Abandoned
                            break;
                    }
                }
                catch (Exception ex)
                {
                    if (_logIsErrorEnabled)
                        _log.ErrorFormat("RxThread(): Exception {0}", ex.Message);
                }
            } while (!_disposed);
        }

        private void RxPacket()
        {
            NECPacket rxNecPacket;
            lock (_rxPackets)
            {
                BasePacket packet = (BasePacket)_rxPackets.Dequeue();
                rxNecPacket = packet as NECPacket;

                if (_rxPackets.Count == 0)
                    _rxEvent.Reset();
            }

            if (rxNecPacket != null)
                ProcessRxPacket(rxNecPacket);

            else if (_logIsErrorEnabled)
                _log.Error("RxPacket(): Unexpected Packet Type");
        }

        private void ProcessRxPacket(NECPacket packet)
        {
            if (packet != null)
            {
                if (packet.ExpectingAcknowledge && IsDuplicate(packet))
                    return;

                bool handled = false;
                if (PacketReceived != null)
                {
                    try
                    {
                        long start = DateTime.Now.Ticks;
                        PacketReceived(packet, ref handled);
                        long diff = DateTime.Now.Ticks - start;

                        if (_log.IsDebugEnabled)
                            _log.DebugFormat("Time to process {0} ticks packet {1}", diff.ToString(), packet.Payload[0]);
                    }
                    catch (Exception ex)
                    {
                        if (_logIsErrorEnabled)
                            _log.ErrorFormat("PacketReceived Failed {0}", ex.ToString());
                    }
                }
            }
        }

        private bool IsDuplicate(NECPacket packet)
        {
            if (packet.SequenceNumber == _lastRxSequenceNumber)
                return true;

            _lastRxSequenceNumber = packet.SequenceNumber;
            return false;
        }

        private void RxAckThread()
        {
            do
            {
                try
                {
                    // Wait for { DisposeEvent, RxEvent }
                    switch (_eventFactory.WaitAny(_rxAckWaitHandles))
                    {
                        case (WaitResult.Object_0 + 1):					// Rx Event
                            {
                                RxAckPacket();
                                break;
                            }

                        default:										// Disposed or Abandoned
                            break;
                    }
                }
                catch (Exception ex)
                {
                    if (_logIsErrorEnabled)
                        _log.ErrorFormat("RxAckThread(): Exception {0}", ex.Message);
                }
            } while (!_disposed);
        }

        private void RxAckPacket()
        {
            NECPacket rxNecAckPacket;
            lock (_rxAckPackets)
            {
                BasePacket packet = (BasePacket)_rxAckPackets.Dequeue();
                rxNecAckPacket = packet as NECPacket;

                if (_rxAckPackets.Count == 0)
                    _rxAckEvent.Reset();
            }

            if (rxNecAckPacket != null)
                ProcessRxAckPacket(rxNecAckPacket);

            else if (_logIsErrorEnabled)
                _log.Error("RxAckPacket(): Unexpected Packet Type");
        }

        private void ProcessRxAckPacket(NECPacket packet)
        {
            if (packet != null)
            {
                bool handled = false;
                if (AckPacketReceived != null)
                {
                    try
                    {
                        long start = DateTime.Now.Ticks;
                        AckPacketReceived(packet, ref handled);
                        long diff = DateTime.Now.Ticks - start;

                        if (_log.IsDebugEnabled)
                            _log.DebugFormat("Time to process {0} ticks", diff);
                    }
                    catch (Exception ex)
                    {
                        if (_logIsErrorEnabled)
                            _log.ErrorFormat("AckPacketReceived Failed {0}", ex.Message);
                    }
                }
            }
        }







        private void TxPacket()
        {
            NECPacket txNecPacket = null;
            int txTotal = 0;
            lock (_txPriorityQueues)
            {
                foreach (PriorityQueue queue in _txPriorityQueues)
                {
                    lock (queue)
                    {
                        if (txNecPacket == null)
                        {
                            if (queue.Count > 0)
                                txNecPacket = queue.Dequeue();
                        }
                        txTotal += queue.Count;
                    }
                }
            }

            if (txTotal == 0)
                _txEvent.Reset();

            TxPacket(txNecPacket);
        }

        private void TxPacket(NECPacket txNecPacket)
        {
            if (txNecPacket != null)
            {
                byte expectedAckSequenceNumber = _nextTxSequenceNumber++;
                txNecPacket.SequenceNumber = expectedAckSequenceNumber;

                if (_logIsDebugEnabled)
                    _log.Debug(string.Format("Sending # {0} RL {1}, subType {2}", txNecPacket.SequenceNumber, txNecPacket.RetriesLeft, txNecPacket.Payload[0]));

                SendData(txNecPacket.ToByte());

                // If no Retries are due then we dont expect an Ack
                if (txNecPacket.RetriesLeft == 0)
                    return;

                _retryTimer.SetTimer(txNecPacket.SequenceNumber, txNecPacket, _retryTimeout);
            }
        }

        protected void SendData(byte[] data)
        {
            if (DisableTransmit)
                return;

            // Escape the data first
            EscapingBuffer escapingBuffer = new EscapingBuffer(data.Length, 0xE7, 0x1B, 0x20);
            escapingBuffer.Add(data);
            byte[] escapedData = escapingBuffer.TrimmedBuffer;
            if (_logIsDebugEnabled)
                _log.Debug("SendData() - " + StringHelper.GetBufferHexString(escapedData, true, false));

            _counters.TotalTxBytes += escapedData.Length;

            lock (_sendSync)
                _serialIO.TxBytes(escapedData);

            _lastNecPacketSent = DateTime.Now;
            if (OnPacketSentOrRecieved != null)
            {
                OnPacketSentOrRecieved(true);
            }

        }

        public NECPacket SendPacket(NECPacket necPacket, int retries)
        {
            necPacket.RetriesLeft = retries;

            EnqueueRequest(necPacket);

            return necPacket;
        }

        private void EnqueueRequest(NECPacket necPacket)
        {
            PriorityQueue queue = null;
            int priority = necPacket.Priority;

            lock (_txPriorityQueues)
            {
                for (int i = 0; i < _txPriorityQueues.Length; i++)
                {
                    if (_txPriorityQueues[i].Priority == priority)
                    {
                        queue = _txPriorityQueues[i];
                        break;
                    }
                }

                if (queue == null)
                {
                    queue = new PriorityQueue(priority);

                    PriorityQueue[] newArray = new PriorityQueue[_txPriorityQueues.Length + 1];
                    Array.Copy(_txPriorityQueues, 0, newArray, 0, _txPriorityQueues.Length);
                    newArray[newArray.Length - 1] = queue;
                    _txPriorityQueues = newArray;

                    Array.Sort(_txPriorityQueues, 0, _txPriorityQueues.Length, queue);
                }
            }

            lock (queue)
                queue.Enqueue(necPacket);

            _txEvent.Set();
        }

        protected void ReceiveData(ISerialIO sender, byte[] data)
        {
            _counters.TotalRxBytes += data.Length;

            if (_logIsDebugEnabled)
                _log.Debug("ReceiveData() - " + StringHelper.GetBufferHexString(data, true, false));

            _unescaper.Add(data);
        }

        private void UnescapingBuffer_Complete(byte[] data)
        {
            _packetParser.Reset();
            foreach (byte b in data)
            {
                switch (_packetParser.AddByte(b))
                {
                    case NECPacketParser.AddByteResult.BadCheckSum:
                        if (_logIsDebugEnabled)
                            _log.Debug("ReceiveData() BadCheck Sum!");
                        _counters.BadCheckSumRx++;
                        break;

                    case NECPacketParser.AddByteResult.UnexpectedByte:
                        if (_logIsDebugEnabled)
                            _log.Debug("ReceiveData() Unexpected Byte!");
                        _counters.PartialRx++;
                        break;

                    case NECPacketParser.AddByteResult.PacketReady:
                        _lastNecPacketReceived = DateTime.Now;
                        if (OnPacketSentOrRecieved != null)
                        {
                            OnPacketSentOrRecieved(false);
                        }
                        if (!_hasCommsReceivedData)
                        {
                            _hasCommsReceivedData = true;
                        }
                        //set comms reconnected
                        if (_isCommsInterrupted)
                        {
                            _isCommsInterrupted = false;
                            if (OnCommsReconnected != null)
                            {
                                OnCommsReconnected();
                            }
                        }
                        lock (_rxPackets)
                        {
                            BasePacket basePacket = _packetParser.LastNecPacket;
                            NECPacket necPacket = basePacket as NECPacket;
                            NECAcknowledge necAckPacket = basePacket as NECAcknowledge;

                            if (necPacket != null)
                            {
                                if (necPacket.ExpectingAcknowledge)
                                {
                                    lock (_ackNecPacket)
                                    {
                                        _ackNecPacket.SequenceNumber = necPacket.SequenceNumber;
                                        if (_logIsDebugEnabled)
                                            _log.Debug(string.Format("Acking # {0}", _ackNecPacket.SequenceNumber));
                                        SendData(_ackNecPacket.ToByte());
                                    }
                                }
                                _counters.GoodRx++;
                                _rxPackets.Enqueue(basePacket);
                                _rxEvent.Set();
                            }
                            else if (necAckPacket != null)
                            {
                                _counters.GoodTx++;
                                NECPacket txSuccessPacket = _retryTimer.GetData(necAckPacket.SequenceNumber) as NECPacket;
                                if (txSuccessPacket != null)
                                {
                                    if (txSuccessPacket.SentEvent != null)
                                        txSuccessPacket.SentEvent.Set();
                                    _rxAckPackets.Enqueue(txSuccessPacket);
                                    _rxAckEvent.Set();

                                }
                                _retryTimer.ClearTimer(necAckPacket.SequenceNumber);
                            }
                        }
                        break;
                }
            }
        }

        public void Dispose()
        {
            _disposed = true;

            if (_disposeEvent != null)
            {
                _disposeEvent.Set();
                _disposeEvent.Close();
                _disposeEvent = null;
            }

            if (_txEvent != null)
            {
                _txEvent.Close();
                _txEvent = null;
            }

            if (_rxEvent != null)
            {
                _rxEvent.Close();
                _rxEvent = null;
            }

            if (_retryTimer != null)
                _retryTimer.Dispose();
        }


        #region ICommsService
        public bool HasCommsReceivedData { get { return _hasCommsReceivedData; } }
        public bool IsCommsInterrupted { get { return _isCommsInterrupted; } }
        public DateTime LastPacketSent { get { return _lastNecPacketSent; } }
        public DateTime LastPacketReceived { get { return _lastNecPacketReceived; } }

        public event CommsPacketSentOrReceived OnPacketSentOrRecieved;
        public event CommsDelegate OnCommsInterrupted;
        public event CommsDelegate OnCommsReconnected;
        #endregion


    }

}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Threading;

namespace MTData.Sys.IO.Nec
{
    #region Packet Class

    /// <summary>
    /// Summary description for Packet.
    /// </summary>
    public class NECPacket : BasePacket
    {
        #region Constants / Enumerations

        // Length Constants
        public static readonly int LENGTH_MAX_PAYLOAD = 2048;
        public static readonly int LENGTH_HEADER = 5;
        public static readonly int LENGTH_TRAILER = 2;	//1 - Checksum, 1 - EOT
        public static readonly int LENGTH_OVERHEAD = LENGTH_HEADER + LENGTH_TRAILER;

        #endregion

        #region Private Variables

        // Header variables
        private MessageType _messageType;
        private int _sequenceNumber;
        protected byte[] _payload;
        private int _maxRetries = 3;

        public bool ExpectingAcknowledge = false;
        private int _priority = 0;
        private IResetEvent _sentEvent;
        private IResetEvent _txFailEvent;

        #endregion

        #region Public Properties

        public MessageType MessageType
        {
            get { return this._messageType; }
            set { this._messageType = value; }
        }

        public int SequenceNumber
        {
            get { return _sequenceNumber; }
            set { _sequenceNumber = value; }
        }

        public int PayloadLength
        {
            get
            {
                if (_payload == null)
                    return 0;

                return _payload.Length;
            }
        }

        public byte[] Payload
        {
            get { return this._payload; }
            set
            {
                if (value != this._payload)
                    this._payload = value;
            }
        }

        public int PacketLength
        {
            get { return PayloadLength + LENGTH_OVERHEAD; }
        }

        #endregion

        #region Constructor / Destructor

        // Constructor
        public NECPacket() : this(0, null, 0) { }
        public NECPacket(MessageType tMessageType) : this(tMessageType, null, 0) { }
        public NECPacket(MessageType tMessageType, byte[] tPayload) : this(tMessageType, tPayload, 0) { }
        public NECPacket(MessageType tMessageType, byte[] tPayload, int iSequenceNumber)
        {
            try
            {
                this._messageType = tMessageType;
                this._sequenceNumber = iSequenceNumber;

                if (tPayload != null)
                    _payload = tPayload;
            }
            catch { }
        }

        #endregion

        #region Public Conversion Functions

        // Convert Packet Object to Byte Array
        public override byte[] ToByte()
        {
            // Variables
            ExpandingByteBuffer buffer = new ExpandingByteBuffer(PayloadLength);
            int checkSum = 0;

            //Set SOH and MessageType
            if (_maxRetries > 0)
            {
                buffer.Add((byte)MessageByte.DataWithAck);
                checkSum += (int)MessageByte.DataWithAck;
            }
            else
            {
                buffer.Add((byte)MessageByte.DataNoAck);
                checkSum += (int)MessageByte.DataNoAck;
            }

            buffer.Add((byte)_messageType);
            checkSum += (int)_messageType;

            // Set Payload Length
            buffer.Add((byte)(PayloadLength >> 8));
            checkSum += PayloadLength >> 8;
            buffer.Add((byte)(PayloadLength >> 0));
            checkSum += PayloadLength >> 0;

            // Set Sequence Number
            buffer.Add((byte)(_sequenceNumber));
            checkSum += _sequenceNumber;

            // Set Payload
            if (_payload != null)
            {
                for (int i = 0; i < _payload.Length; i++)
                {
                    buffer.Add(_payload[i]);
                    checkSum += _payload[i];
                }
            }

            // Set Checksum
            buffer.Add((byte)checkSum);

            buffer.Add((byte)MessageByte.EOT);

            // Return Result
            return buffer.TrimmedBuffer;
        }

        // Convert Source Bytes -> Packet Object
        public override void Parse(byte[] data, ref int byteIndex)
        {
            if (byteIndex + 4 > data.Length)
                throw new BufferTooShortException();

            switch (data[0])
            {
                case (byte)MessageByte.DataNoAck:
                case (byte)MessageByte.DataWithAck:
                    break;

                default:
                    throw new InvalidPacketException(ERROR_PACKET_NO_SOH);
            }

            // Extract MessageType (1 BYTE)
            byteIndex = 1;							// Skip the first SOH
            _messageType = (MessageType)data[byteIndex++];

            // Extract Packet Length (2 BYTES) and Check Source buffer length OK
            int payloadLength = ((int)data[byteIndex++] << 8);
            payloadLength += (int)data[byteIndex++];

            if (data.Length < (payloadLength + LENGTH_OVERHEAD))
                throw new BufferTooShortException();

            // Check Last Byte = EOT (1 BYTE)
            if (data[payloadLength + LENGTH_OVERHEAD - 1] != (byte)MessageByte.EOT)
                throw new InvalidPacketException(ERROR_PACKET_NO_EOT);

            // Verify CheckSum (After Payload)  (1 BYTE)
            byte checkSum = (byte)Utility.CalculateCheckSum(data, 0, (LENGTH_HEADER + payloadLength));
            if (checkSum != data[LENGTH_HEADER + payloadLength])
                throw new InvalidPacketException(ERROR_PACKET_INVALID_CHECKSUM);


            // Extract Sequence Number (1 BYTE)
            _sequenceNumber = (int)data[byteIndex++];

            // Extract Payload Data
            _payload = null;
            if (payloadLength > 0)
            {
                _payload = new byte[payloadLength];
                Array.Copy(data, byteIndex, _payload, 0, payloadLength);
                byteIndex += payloadLength;
            }
        }


        public int RetriesLeft
        {
            get { return _maxRetries; }
            set { _maxRetries = System.Math.Max(0, value); }
        }

        public int Priority
        {
            get { return _priority; }
            set { _priority = value; }
        }

        public IResetEvent SentEvent
        {
            get { return _sentEvent; }
            set { _sentEvent = value; }
        }

        public IResetEvent TxFailEvent
        {
            get { return _txFailEvent; }
            set { _txFailEvent = value; }
        }

        #endregion
    }

    #endregion
}

﻿using System;

using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Nec
{
    public class NECPacketParser
    {
        /// <summary>
        /// State Machine states of a Packet
        /// </summary>
        private enum PacketState
        {
            SOH,
            MessageType,
            DataLength1,
            DataLength2,
            TxSequenceNumber,
            Data,
            CheckSum,
            EOT,

            AcknowledgeSequenceNumber
        }

        public enum AddByteResult
        {
            NoneYet,
            BadCheckSum,
            UnexpectedByte,
            PacketReady
        }
        private ExpandingByteBuffer _buffer;
        private PacketState _packetState;
        private int _dataLength;
        private int _dataCountDown;
        private byte _checkSum;
        private BasePacket _lastNecPacket;
        private bool _expectingAcknowledge;

        public NECPacketParser(int initialSize)
        {
            _buffer = new ExpandingByteBuffer(128);
            _packetState = PacketState.SOH;
        }

        public void Reset()
        {
            _packetState = PacketState.SOH;
        }

        public AddByteResult AddByte(byte b)
        {
            switch (_packetState)
            {
                case PacketState.SOH:
                    if ((b == (byte)MessageByte.DataWithAck) || (b == (byte)MessageByte.DataNoAck))
                    {
                        _expectingAcknowledge = (b == (byte)MessageByte.DataWithAck);
                        _buffer.Reset();
                        _buffer.Add(b);
                        _checkSum = b;
                        _packetState = PacketState.MessageType;
                    }
                    else if (b == (byte)MessageByte.ACK)
                        _packetState = PacketState.AcknowledgeSequenceNumber;
                    else
                        return AddByteResult.UnexpectedByte;
                    break;

                case PacketState.MessageType:
                    _buffer.Add(b);

                    _checkSum += b;
                    _packetState = PacketState.DataLength1;
                    break;

                case PacketState.DataLength1:
                    _buffer.Add(b);
                    _dataLength = b << 8;

                    _checkSum += b;
                    _packetState = PacketState.DataLength2;
                    break;

                case PacketState.DataLength2:
                    _buffer.Add(b);
                    _dataLength |= b;
                    _dataCountDown = _dataLength;

                    _checkSum += b;
                    _packetState = PacketState.TxSequenceNumber;
                    break;

                case PacketState.TxSequenceNumber:
                    _buffer.Add(b);

                    _checkSum += b;
                    if (_dataCountDown > 0)
                        _packetState = PacketState.Data;
                    else
                        _packetState = PacketState.CheckSum;
                    break;

                case PacketState.Data:
                    _buffer.Add(b);
                    _checkSum += b;
                    if (--_dataCountDown == 0)
                        _packetState = PacketState.CheckSum;
                    break;

                case PacketState.CheckSum:
                    _buffer.Add(b);
                    if (b == _checkSum)
                        _packetState = PacketState.EOT;
                    else
                    {
                        _packetState = PacketState.SOH;
                        return AddByteResult.BadCheckSum;
                    }
                    break;

                case PacketState.EOT:
                    _packetState = PacketState.SOH;
                    if (b == (byte)MessageByte.EOT)
                    {
                        _buffer.Add(b);
                        NECPacket necPacket = new NECPacket();
                        necPacket.Parse(_buffer.TrimmedBuffer);
                        necPacket.ExpectingAcknowledge = _expectingAcknowledge;

                        _lastNecPacket = necPacket;
                        return AddByteResult.PacketReady;
                    }
                    return AddByteResult.UnexpectedByte;

                case PacketState.AcknowledgeSequenceNumber:
                    _packetState = PacketState.SOH;
                    _lastNecPacket = new NECAcknowledge(b);
                    return AddByteResult.PacketReady;
            }
            return AddByteResult.NoneYet;
        }

        public BasePacket LastNecPacket
        {
            get
            {
                BasePacket result = _lastNecPacket;
                _lastNecPacket = null;
                return result;
            }
        }
    }

}

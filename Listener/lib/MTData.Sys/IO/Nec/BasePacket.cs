﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.IO;
using System.Text;

namespace MTData.Sys.IO.Nec
{
    public abstract class BasePacket
    {
        #region Public Constants

        // Public Error Codes
        public const string ERROR_INVALID_INPUT = "Invalid Input";
        public const string ERROR_NULL_REFERENCE = "Null Reference";
        public const string ERROR_SRC_ARRAY_NULL = "Source array was null";
        public const string ERROR_SRC_ARRAY_ZERO_LENGTH = "Source array was Zero-length";
        public const string ERROR_SRC_ARRAY_TOO_SHORT = "Source array too short";
        public const string ERROR_PACKET_NO_SOH = "Packet does not begin with SOH";
        public const string ERROR_PACKET_NO_EOT = "Packet does not end with EOT";
        public const string ERROR_PACKET_TOO_LONG = "Packet too long";
        public const string ERROR_PACKET_TOO_SHORT = "Packet too short";
        public const string ERROR_PACKET_INVALID_CHECKSUM = "Packet does not have correct CheckSum";

        #endregion

        #region Protected Variables
        #endregion

        #region Public Abstract Functions

        /// <summary>
        /// Convert the object properties into a byte array.
        /// </summary>
        /// <returns></returns>
        public virtual byte[] ToByte()
        {
            MemoryStream stream = new MemoryStream();
            try
            {
                ToStream(stream);
                return stream.ToArray();
            }
            finally
            {
                stream.Close();
            }
        }

        public virtual void ToStream(Stream stream)
        {
            throw new NotSupportedException("Cannot ToStream()" + this.GetType().Name);
        }

        /// <summary>
        /// Parse the supplied byte array into properties
        /// for the payload class.
        /// </summary>
        /// <param name="SourceBytes"></param>
        public abstract void Parse(byte[] SourceBytes, ref int byteIndex);

        public virtual void Parse(byte[] SourceBytes)
        {
            int dummy = 0;
            Parse(SourceBytes, ref dummy);
        }
        #endregion

        #region Little Endian Writes
        protected internal virtual int WriteLittleEndian(Stream stream, int value)
        {
            stream.WriteByte((byte)(value >> 0));
            stream.WriteByte((byte)(value >> 8));
            stream.WriteByte((byte)(value >> 16));
            stream.WriteByte((byte)(value >> 24));
            return 4;
        }

        protected internal virtual int WriteLittleEndian(Stream stream, uint value)
        {
            stream.WriteByte((byte)(value >> 0));
            stream.WriteByte((byte)(value >> 8));
            stream.WriteByte((byte)(value >> 16));
            stream.WriteByte((byte)(value >> 24));
            return 4;
        }

        protected internal virtual int WriteLittleEndian(Stream stream, short value)
        {
            stream.WriteByte((byte)(value >> 0));
            stream.WriteByte((byte)(value >> 8));
            return 2;
        }

        protected internal virtual int WriteLittleEndian(Stream stream, ushort value)
        {
            stream.WriteByte((byte)(value >> 0));
            stream.WriteByte((byte)(value >> 8));
            return 2;
        }

        protected internal virtual int WriteLittleEndian(Stream stream, byte value)
        {
            stream.WriteByte(value);
            return 1;
        }

        protected internal virtual int WriteLittleEndian(Stream stream, byte[] bytes)
        {
            stream.Write(bytes, 0, bytes.Length);
            return bytes.Length;
        }

        protected internal virtual int WriteLittleEndian(Stream stream, byte[] bytes, int offset, int length)
        {
            stream.Write(bytes, offset, length);
            return length;
        }

        protected internal virtual int WriteLittleEndianNullMaxLengthString(Stream stream, string text, int maxLength)
        {
            byte[] data = ASCIIEncoding.ASCII.GetBytes(text);

            for (int i = 0; i < maxLength - 1; i++)			// -1 to make space for ending NULL
            {
                if (i < data.Length)
                    stream.WriteByte(data[i]);
                else
                    stream.WriteByte(0);
            }
            // Make sure a NULL is always appended
            stream.WriteByte(0);

            return maxLength;
        }

        protected internal virtual int WriteLittleEndianMaxLengthString(Stream stream, string text, int maxLength, byte delimeter)
        {
            byte[] data = ASCIIEncoding.ASCII.GetBytes(text);

            for (int i = 0; i < maxLength - 1; i++)			// -1 to make space for ending NULL
            {
                if (i < data.Length)
                    stream.WriteByte(data[i]);
                else
                    stream.WriteByte(delimeter);
            }
            // Make sure a NULL is always appended
            stream.WriteByte(delimeter);

            return maxLength;
        }

        protected internal virtual int WriteLittleEndianNullString(Stream stream, string text)
        {
            return WriteLittleEndianNullMaxLengthString(stream, text, text.Length + 1);
        }

        protected internal virtual int WriteLittleEndianFixedLengthString(Stream stream, string text, int maxLength, byte fillcharacter)
        {
            byte[] data = ASCIIEncoding.ASCII.GetBytes(text);

            for (int i = 0; i < maxLength - 1; i++)			// -1 to make space for ending NULL
            {
                if (i < data.Length)
                    stream.WriteByte(data[i]);
                else
                    stream.WriteByte(fillcharacter);
            }
            return maxLength;
        }

        protected internal virtual int WriteLittleEndianString(Stream stream, string text, byte delimeter)
        {
            int length = WriteLittleEndianString(stream, text);
            length += WriteLittleEndian(stream, delimeter);
            return length;
        }

        protected internal virtual int WriteLittleEndianString(Stream stream, string text)
        {
            byte[] data = ASCIIEncoding.ASCII.GetBytes(text);

            return WriteLittleEndian(stream, data);
        }
        #endregion

        #region Big Endian Writes
        protected internal virtual int WriteBigEndian(Stream stream, int value)
        {
            stream.WriteByte((byte)(value >> 24));
            stream.WriteByte((byte)(value >> 16));
            stream.WriteByte((byte)(value >> 8));
            stream.WriteByte((byte)(value >> 0));
            return 4;
        }

        protected internal virtual int WriteBigEndian(Stream stream, uint value)
        {
            stream.WriteByte((byte)(value >> 24));
            stream.WriteByte((byte)(value >> 16));
            stream.WriteByte((byte)(value >> 8));
            stream.WriteByte((byte)(value >> 0));
            return 4;
        }

        protected internal virtual int WriteBigEndian(Stream stream, short value)
        {
            stream.WriteByte((byte)(value >> 8));
            stream.WriteByte((byte)(value >> 0));
            return 2;
        }

        protected internal virtual int WriteBigEndian(Stream stream, ushort value)
        {
            stream.WriteByte((byte)(value >> 8));
            stream.WriteByte((byte)(value >> 0));
            return 2;
        }

        protected internal virtual int WriteBigEndian(Stream stream, byte value)
        {
            stream.WriteByte(value);
            return 1;
        }

        protected internal virtual int WriteBigEndian(Stream stream, byte[] bytes)
        {
            stream.Write(bytes, 0, bytes.Length);
            return bytes.Length;
        }

        protected internal virtual int WriteBigEndian(Stream stream, byte[] bytes, int offset, int length)
        {
            stream.Write(bytes, offset, length);
            return length;
        }

        protected internal virtual int WriteBigEndianNullMaxLengthString(Stream stream, string text, int maxLength)
        {
            return WriteLittleEndianNullMaxLengthString(stream, text, maxLength);
        }

        protected internal virtual int WriteBigEndianNullString(Stream stream, string text)
        {
            return WriteLittleEndianNullMaxLengthString(stream, text, text.Length + 1);
        }

        protected internal virtual int WriteBigEndianMoreFlag(Stream stream, int value)
        {
            int bytesWritten = 0;
            while (value > 0x7F)
            {
                bytesWritten += WriteBigEndian(stream, (byte)((value & 0x7F) | 0x80));
                value >>= 7;
            }
            bytesWritten += WriteBigEndian(stream, (byte)(value & 0x7F));
            return bytesWritten;
        }

        protected internal virtual int Write16BitBigEndianMoreFlag(Stream stream, int value)
        {
            int bytesWritten = 0;
            while (value > 0x7FFF)
            {
                bytesWritten += WriteBigEndian(stream, (byte)(((value >> 8) & 0x7F) | 0x80));
                bytesWritten += WriteBigEndian(stream, (byte)(value & 0xFF));
                value >>= 15;
            }
            bytesWritten += WriteBigEndian(stream, (byte)((value >> 8) & 0x7F));
            bytesWritten += WriteBigEndian(stream, (byte)(value & 0xFF));
            return bytesWritten;
        }

        protected internal virtual int Write16BitBigEndianMoreFlag(Stream stream, long value)
        {
            int bytesWritten = 0;
            while (value > 0x7FFF)
            {
                bytesWritten += WriteBigEndian(stream, (byte)(((value >> 8) & 0x7F) | 0x80));
                bytesWritten += WriteBigEndian(stream, (byte)(value & 0xFF));
                value >>= 15;
            }
            bytesWritten += WriteBigEndian(stream, (byte)((value >> 8) & 0x7F));
            bytesWritten += WriteBigEndian(stream, (byte)(value & 0xFF));
            return bytesWritten;
        }

        protected internal virtual int WriteBigEndianFixedLengthString(Stream stream, string text, int maxLength, byte fillcharacter)
        {
            return WriteLittleEndianFixedLengthString(stream, text, maxLength, fillcharacter);
        }

        protected internal virtual int WriteBigEndianMaxLengthString(Stream stream, string text, int maxLength, byte delimeter)
        {
            return WriteLittleEndianMaxLengthString(stream, text, maxLength, delimeter);
        }

        protected internal virtual int WriteBigEndianString(Stream stream, string text, byte delimeter)
        {
            return WriteLittleEndianString(stream, text, delimeter);
        }

        protected internal virtual int WriteBigEndianString(Stream stream, string text)
        {
            return WriteLittleEndianString(stream, text);
        }
        #endregion

        #region Little Endian Reads
        protected internal virtual void ReadLittleEndian(byte[] data, ref int byteIndex, ref byte value)
        {
            value = data[byteIndex++];
        }

        protected internal virtual void ReadLittleEndian(byte[] data, ref int byteIndex, ref short value)
        {
            value = (short)((data[byteIndex++] << 0) | (data[byteIndex++] << 8));
        }

        protected internal virtual void ReadLittleEndian(byte[] data, ref int byteIndex, ref ushort value)
        {
            value = (ushort)((data[byteIndex++] << 0) | (data[byteIndex++] << 8));
        }

        protected internal virtual void ReadLittleEndian(byte[] data, ref int byteIndex, ref int value)
        {
            value = (data[byteIndex++] << 0) | (data[byteIndex++] << 8) | (data[byteIndex++] << 16) | (data[byteIndex++] << 24);
        }

        protected internal virtual void ReadLittleEndian(byte[] data, ref int byteIndex, ref uint value)
        {
            value = (uint)((data[byteIndex++] << 0) | (data[byteIndex++] << 8) | (data[byteIndex++] << 16) | (data[byteIndex++] << 24));
        }

        protected internal virtual void ReadLittleEndian(byte[] data, ref int byteIndex, ref byte[] target)
        {
            Array.Copy(data, byteIndex, target, 0, target.Length);
            byteIndex += target.Length;
        }

        protected internal virtual void ReadLittleEndianNullMaxLengthString(byte[] data, ref int byteIndex, ref string text, int maxLength)
        {
            int nullPos = -1;

            for (int i = byteIndex; i < byteIndex + maxLength; i++)
            {
                if (i >= data.Length || data[i] == 0)
                {
                    nullPos = i;
                    break;
                }
            }
            if (nullPos == -1)
                nullPos = byteIndex + maxLength;

            int textLength = nullPos - byteIndex;
            text = ASCIIEncoding.ASCII.GetString(data, byteIndex, textLength);
            byteIndex += textLength + 1;
        }

        protected internal virtual void ReadLittleEndianMaxLengthString(byte[] data, ref int byteIndex, ref string text, int maxLength, byte delimeter)
        {
            int nullPos = -1;
            for (int i = byteIndex; i < byteIndex + maxLength; i++)
            {
                if (i >= data.Length || data[i] == delimeter)
                {
                    nullPos = i;
                    break;
                }
            }
            if (nullPos == -1)
                nullPos = byteIndex + maxLength;

            int textLength = nullPos - byteIndex;
            text = ASCIIEncoding.ASCII.GetString(data, byteIndex, textLength);
            byteIndex += textLength + 1;
        }

        protected internal virtual void ReadLittleEndianString(byte[] data, ref int byteIndex, ref string text, byte delimeter)
        {
            int nullPos = -1;
            for (int i = byteIndex; i < data.Length; i++)
            {
                if (data[i] == delimeter)
                {
                    nullPos = i;
                    break;
                }
            }
            if (nullPos == -1)
                throw new Exception("No Delimeter found");

            int textLength = nullPos - byteIndex;
            text = ASCIIEncoding.ASCII.GetString(data, byteIndex, textLength);
            byteIndex += textLength + 1;
        }

        #endregion

        #region Big Endian Reads
        protected internal virtual void ReadBigEndianString(byte[] data, ref int byteIndex, ref string text, byte delimeter)
        {
            ReadLittleEndianString(data, ref byteIndex, ref text, delimeter);
        }

        protected internal virtual void ReadBigEndianMaxLengthString(byte[] data, ref int byteIndex, ref string text, int maxLength, byte delimeter)
        {
            ReadLittleEndianMaxLengthString(data, ref byteIndex, ref text, maxLength, delimeter);
        }

        protected internal virtual void ReadBigEndianNullMaxLengthString(byte[] data, ref int byteIndex, ref string text, int maxLength)
        {
            ReadLittleEndianNullMaxLengthString(data, ref byteIndex, ref text, maxLength);
        }

        protected internal virtual void ReadBigEndianFixedLengthString(byte[] data, ref int byteIndex, ref string text, int fixedLength)
        {
            ReadLittleEndianFixedLengthString(data, ref byteIndex, ref text, fixedLength);
        }

        public void ReadLittleEndianFixedLengthString(byte[] data, ref int byteIndex, ref string text, int fixedLength)
        {
            int nullPos = -1;
            for (int i = byteIndex; i < byteIndex + fixedLength; i++)
            {
                if (data[i] == 0)
                {
                    nullPos = i;
                    break;
                }
            }

            if (nullPos == -1)
                nullPos = byteIndex + fixedLength;

            int textLength = nullPos - byteIndex;
            text = ASCIIEncoding.ASCII.GetString(data, byteIndex, textLength);
            byteIndex += fixedLength;
        }

        protected internal virtual void ReadBigEndianMoreFlag(byte[] data, ref int byteIndex, ref int value)
        {
            byte b;
            int shift = 0;
            value = 0;
            do
            {
                b = data[byteIndex++];
                value |= (b & 0x7F) << shift;
                shift += 7;
            } while ((b & 0x80) != 0);
        }

        protected internal virtual void Read16BitBigEndianMoreFlag(byte[] data, ref int byteIndex, ref int value)
        {
            ushort b;
            int shift = 0;
            value = 0;
            do
            {
                byte b1 = data[byteIndex++];
                byte b2 = data[byteIndex++];
                b = (ushort)((b1 << 8) | b2);
                value |= (b & 0x7FFF) << shift;
                shift += 15;
            } while ((b & 0x8000) != 0);
        }

        protected internal virtual void Read16BitBigEndianMoreFlag(byte[] data, ref int byteIndex, ref long value)
        {
            ushort b;
            int shift = 0;
            value = 0;
            do
            {
                byte b1 = data[byteIndex++];
                byte b2 = data[byteIndex++];
                b = (ushort)((b1 << 8) | b2);
                value |= ((long)(b & 0x7FFF)) << shift;
                shift += 15;
            } while ((b & 0x8000) != 0);
        }

        protected internal virtual void ReadBigEndian(byte[] data, ref int byteIndex, ref byte value)
        {
            value = data[byteIndex++];
        }

        protected internal virtual void ReadBigEndian(byte[] data, ref int byteIndex, ref short value)
        {
            value = (short)((data[byteIndex++] << 8) | (data[byteIndex++] << 0));
        }

        protected internal virtual void ReadBigEndian(byte[] data, ref int byteIndex, ref ushort value)
        {
            value = (ushort)((data[byteIndex++] << 8) | (data[byteIndex++] << 0));
        }

        protected internal virtual void ReadBigEndian(byte[] data, ref int byteIndex, ref int value)
        {
            value = (data[byteIndex++] << 24) | (data[byteIndex++] << 16) | (data[byteIndex++] << 8) | (data[byteIndex++] << 0);
        }

        protected internal virtual void ReadBigEndian(byte[] data, ref int byteIndex, ref uint value)
        {
            value = (uint)((data[byteIndex++] << 24) | (data[byteIndex++] << 16) | (data[byteIndex++] << 8) | (data[byteIndex++] << 0));
        }

        protected internal virtual void ReadBigEndian(byte[] data, ref int byteIndex, ref byte[] target)
        {
            Array.Copy(data, byteIndex, target, 0, target.Length);
            byteIndex += target.Length;
        }


        #endregion
    }
}

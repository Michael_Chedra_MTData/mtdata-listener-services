﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using MTData.Sys.Comms;

namespace MTData.Sys.IO.Nec
{
    public delegate void PacketHandler(NECPacket necPacket, ref bool handled);

    public interface INECComs : IDisposable, ICommsService
    {
        NECPacket SendPacket(NECPacket necPacket, int retries);

        event PacketHandler PacketReceived;
        event PacketHandler AckPacketReceived;

        CommsCounters Counters { get; }

        void Start();

        int RetryTimeout
        {
            get;
            set;
        }
    }

    public struct CommsCounters
    {
        public int GoodTx;
        public int FailedTx;
        public int Retries;

        public int GoodRx;
        public int BadCheckSumRx;
        public int PartialRx;

        public int OutOfSequenceRxACK;

        public int TotalRxBytes;
        public int TotalTxBytes;
    }
}

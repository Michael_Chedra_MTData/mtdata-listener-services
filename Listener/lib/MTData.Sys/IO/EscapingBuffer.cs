﻿using System;

namespace MTData.Sys.IO
{
    public class EscapingBuffer : ExpandingByteBuffer
    {
        private byte FrameByte;
        private byte EscapeByte;
        private byte EscapeXor;

        public EscapingBuffer(int initialSize)
            : this(initialSize, 0x7E, 0x1B, 0x20)
        {
        }

        public EscapingBuffer(int initialSize, byte frameByte, byte escapeByte, byte escapeXor)
            : base(initialSize)
        {
            FrameByte = frameByte;
            EscapeByte = escapeByte;
            EscapeXor = escapeXor;
            base.Add(FrameByte);
        }

        public override void Reset()
        {
            base.Reset();
            base.Add(FrameByte);
        }

        public override void Add(byte b)
        {
            if ((b == FrameByte) || (b == EscapeByte))
            {
                base.Add(EscapeByte);
                base.Add((byte)(b ^ EscapeXor));
            }
            else
                base.Add(b);
        }

        public override void Add(byte[] bytes)
        {
            for (int i = 0; i < bytes.Length; i++)
                Add(bytes[i]);
        }

        public override byte[] TrimmedBuffer
        {
            get
            {
                base.Add(FrameByte);
                return base.TrimmedBuffer;
            }
        }
    }

}

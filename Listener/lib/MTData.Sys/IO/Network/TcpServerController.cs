using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Sys.IO.Serial;
using MTData.Sys.Config;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// This client connection controller will manage the acceptance of client connections.
	/// </summary>
	public class TcpServerController : IDisposable
	{
		private static ILog _log = null; 

		private IPEndPoint _bindEndPoint = new IPEndPoint(IPAddress.Any, 5678);
		private TcpListener _listener = null;
		private Thread _listenThread = null;
		private IConfigSection _configSection = null;
		private bool _active = false;

		public delegate void TcpServerControllerInboundSocketDelegate(TcpServerController sender, Socket client);

		public event TcpServerControllerInboundSocketDelegate InboundSocket;

		public TcpServerController(IConfigSection configSection)
		{
			_log = LogManager.GetLogger(typeof (TcpServerController));

			_configSection = configSection;

			int portNumber = configSection["Port", 5678];
			string bindAddress = configSection["BindAddress", "0.0.0.0"];

			if (_log.IsInfoEnabled)
				_log.Info(string.Format("TcpServerController : BindAddress - {0}; Port - {1} ", bindAddress, portNumber));

			_bindEndPoint = new IPEndPoint(IPAddress.Parse(bindAddress), portNumber);
            
			if (_log.IsInfoEnabled)
				_log.Info("TcpServerController : BindAddress : " + _bindEndPoint.ToString());

			_active = true;
			_listenThread = new Thread(new ThreadStart(_listenThread_Handler));
#if NETCF
#else
			_listenThread.IsBackground = true;
#endif
			_listenThread.Start();

		}

		public void _listenThread_Handler()
		{
			if (_log.IsInfoEnabled)
				_log.Info("TcpServerController : Initialising");

			bool started = false;
			try
			{
				_listener = new TcpListener(_bindEndPoint);
				try
				{
					_listener.Start();
					started = true;

					while(_active)
					{
						System.Net.Sockets.Socket socket = _listener.AcceptSocket();
						
						if (_log.IsInfoEnabled)
							_log.Info(string.Format("TcpServerController : Accepted Inbound Client : {0}", socket.RemoteEndPoint.ToString()));
						try
						{
							if (InboundSocket != null)
								InboundSocket(this, socket);
						}
						catch(Exception ex1)
						{
							if (_log.IsErrorEnabled)
								_log.Error("TcpServerController : InboundClient Event Error", ex1);
						}
					}
				}
				finally
				{
					if (_log.IsInfoEnabled)
						_log.Info("TcpServerController : Stopping");

					if (started)
						_listener.Stop();

					_listener = null;
					if (_log.IsInfoEnabled)
						_log.Info("TcpServerController : Terminating");
				}
			}
			catch(Exception ex)
			{
				if (_log.IsErrorEnabled)
					_log.Error("TcpServerController : HandlerLoop Error", ex);
			}
		}
		#region IDisposable Members

		public void Dispose()
		{
			if (_log.IsInfoEnabled)
				_log.Info("TcpServerController : Disposing");
			if ((_active) && (_listener != null))
			{					
				_active = false;
				_listener.Stop();
			}
		}

		#endregion
	}
}

﻿using System;

using System.Collections.Generic;
using System.Net;
using System.Text;
using MTData.Sys.Config;
using MTData.Sys.IO.Serial;

namespace MTData.Sys.IO.Network
{
	public class UdpSerialIOFactory : ISerialIOFactory, ISerialIOFactoryEx, IConfigurable
    {
        public UdpSerialIOFactory()
        {
		}

		#region Private Instance Data

		private string _rxAddress = null;
		private int _rxPortNumber;
		private string _txAddress = null;
		private int _txPortNumber;
		private bool _reuseAddress;

		#endregion

		#region Properties

		public string RxAddress
		{
			get
			{
				return _rxAddress;
			}
			set
			{
				_rxAddress = value;
			}
		}

		public string TxAddress
		{
			get
			{
				return _txAddress;
			}
			set
			{
				_txAddress = value;
			}
		}

		public int RxPortNumber
		{
			get
			{
				return _rxPortNumber;
			}
			set
			{
				_rxPortNumber = value;
			}
		}

		public int TxPortNumber
		{
			get
			{
				return _txPortNumber;
			}
			set
			{
				_txPortNumber = value;
			}
		}

		public bool ReuseAddress
		{
			get
			{
				return _reuseAddress;
			}
			set
			{
				_reuseAddress = value;
			}
		}

		#endregion

		public UdpSerialIOFactory(string rxAddress, int rxPortNumber, string txAddress, int txPortNumber, bool reuseAddress)
		{
			_rxAddress = rxAddress;
			_txAddress = txAddress;
			_rxPortNumber = rxPortNumber;
			_txPortNumber = txPortNumber;
			_reuseAddress = reuseAddress;
		}

        public ISerialIO Create(IConfigSection configSection, object context)
        {
            string[] rxEndpointText = configSection["RxIPEndpoint", "127.0.0.0:1"].Split(':');
            string[] txEndpointText = configSection["TxIPEndpoint", "127.0.0.0:1"].Split(':');

            int rxPort = int.Parse(rxEndpointText[1]);
            IPAddress rxAddress = IPAddress.Parse(rxEndpointText[0]);

            int txPort = int.Parse(txEndpointText[1]);
            IPAddress txAddress = IPAddress.Parse(txEndpointText[0]);

            bool reuseAddress = configSection["ReuseAddress", false];

            return new UdpSerialIO(new IPEndPoint(rxAddress, rxPort), new IPEndPoint(txAddress, txPort), reuseAddress);
        }

		#region ISerialIOFactoryEx Members

		public ISerialIO Create()
		{
			if ((_rxAddress == null) || (_txAddress == null))
				throw new ConfigurationException("Cannot create ISerialIO without a configured Factory");

			IPAddress rxAddress = IPAddress.Parse(_rxAddress);
			IPAddress txAddress = IPAddress.Parse(_txAddress);

			return new UdpSerialIO(new IPEndPoint(rxAddress, _rxPortNumber), new IPEndPoint(txAddress, _txPortNumber), _reuseAddress);
		}

		#endregion

		#region IConfigurable Members

		public void Configure(object context, Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			Loader.Loader.PopulatePropertiesFromNode(this, configNode);
		}

		#endregion
	}
}

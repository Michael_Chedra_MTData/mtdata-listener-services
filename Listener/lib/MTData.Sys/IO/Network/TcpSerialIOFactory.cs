using log4net;
using System;
using System.Net;
using System.Net.Sockets;
using MTData.Sys.IO.Serial;
using MTData.Sys.Config;
using MTData.Sys.Loader;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// TcpSerialIOFactory is used to instantiate Tcp Serial interface implementations from TcpClient connections.
	/// </summary>
	public class TcpSerialIOFactory : ISerialIOFactory, ISerialIOFactoryEx, IConfigurable
	{
		private string _fixedEndpointIP = null;
		private int _fixedEndpointPort = 0;

		private static ILog _log = null; 

		private TcpSerialIO.TcpSerialIOSerialErrorDelegate _errorCallback = null;

		public TcpSerialIOFactory() : this(null)
		{
			
		}

		public TcpSerialIOFactory(string targetIP, int portNumber) : this(targetIP, portNumber, null)
		{
			
		}

		public TcpSerialIOFactory(string targetIP, int portNumber, TcpSerialIO.TcpSerialIOSerialErrorDelegate errorCallback)
		{
			if (_log == null)
				_log = LogManager.GetLogger(typeof (TcpSerialIOFactory));

			_errorCallback = errorCallback;
			_fixedEndpointIP = targetIP;
			_fixedEndpointPort = portNumber;
		}

		public TcpSerialIOFactory(TcpSerialIO.TcpSerialIOSerialErrorDelegate errorCallback)
		{
			if (_log == null)
				_log = LogManager.GetLogger(typeof (TcpSerialIOFactory));

			_fixedEndpointIP = null;
			_fixedEndpointPort = 0;
			_errorCallback = errorCallback;
		}

/*		public ISerialIO CreateFromIPAndPortNumber(string targetIP, int portNumber, object context)
		{
			TcpSerialIO instance = CreateSerialIOFromContext(context);
			if (instance == null)
				instance = CreateSerialIOFromParams(targetIP, portNumber);

			instance.SerialError += _errorCallback;
			return instance;
		}*/

		private TcpSerialIO CreateSerialIOFromContext(object context)
		{
			TcpSerialIO instance = null;
			if (context != null)
			{
				if (context is Socket)
					instance = new TcpSerialIO((Socket)context);
				if (context is TcpClient)
					instance = new TcpSerialIO((TcpClient)context);
			}
			if ((instance != null) && (_errorCallback != null))
				instance.TcpSerialError += _errorCallback;
			
			return instance;
		}

		private TcpSerialIO CreateSerialIOFromParams(string targetIP, int portNumber)
		{
			if (_log.IsDebugEnabled)
				_log.Debug("TcpSerialIOFactory:CreateSerialIOFromParams : Creating TcpClient");

			TcpClient client = new TcpClient(targetIP, portNumber);

			if (_log.IsDebugEnabled)
				_log.Debug("TcpSerialIOFactory:CreateSerialIOFromParams : Creating TcpSerialIO Instance");
			TcpSerialIO instance = new TcpSerialIO(client);

			if (_errorCallback != null)
				instance.TcpSerialError += _errorCallback;
			if (_log.IsDebugEnabled)
				_log.Debug("TcpSerialIOFactory:CreateSerialIOFromParams : Out");
			return instance;
		}

		#region ISerialIOFactory Members

		public ISerialIO Create(IConfigSection configSection, object context)
		{
			if (_log.IsDebugEnabled)
				_log.Debug("TcpSerialIOFactory:Create : In");

			TcpSerialIO instance = CreateSerialIOFromContext(context);
			if (instance == null)
			{
				if (_log.IsDebugEnabled)
					_log.Debug("TcpSerialIOFactory:Create : No Instance from context");

				if ((_fixedEndpointIP != null) && (_fixedEndpointIP.Trim().Length > 0))
				{
					if (_log.IsDebugEnabled)
						_log.Debug("TcpSerialIOFactory:Create : Creating connection from FixedIP");

					instance = CreateSerialIOFromParams(_fixedEndpointIP, _fixedEndpointPort);
				}
				else
				{
					if (_log.IsDebugEnabled)
						_log.Debug("TcpSerialIOFactory:Create : Creating connection from Config");

					string targetIP = configSection["TargetIP", "127.0.0.1"];
					int portNumber = configSection["TargetPort", 5678];

					if (_log.IsDebugEnabled)
						_log.Debug(string.Format("TcpSerialIOFactory:Create : Creating connection from Config : {0}:{1}", targetIP, portNumber));
					instance = CreateSerialIOFromParams(targetIP, portNumber);
				}
			}
			if (_log.IsDebugEnabled)
				_log.Debug("TcpSerialIOFactory:Create : Out");
			return instance;
		}

		#endregion

		#region ISerialIOFactoryEx Members

		/// <summary>
		/// this allows parameterless consturction of underlying entries.
		/// </summary>
		/// <returns></returns>
		ISerialIO ISerialIOFactoryEx.Create()
		{
			if ((_fixedEndpointIP != null) && (_fixedEndpointIP.Trim().Length > 0))
				return CreateSerialIOFromParams(_fixedEndpointIP, _fixedEndpointPort);
			else
				return null;
		}

		#endregion


		#region IConfigurable Members

		public string TargetIP
		{
			get
			{
				return _fixedEndpointIP;
			}
			set
			{
				_fixedEndpointIP = value;
			}
		}

		public int Port
		{
			get
			{
				return _fixedEndpointPort;
			}
			set
			{
				_fixedEndpointPort = value;
			}
		}

		public void Configure(object context, Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			Loader.Loader.PopulatePropertiesFromNode(this, configNode);			
		}

		#endregion
	}
}

using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network.Nexus
{
	public interface ITcpNexusWriter
	{
		int Write(byte[] data);
		void Close();
	}

	/// <summary>
	/// This class will act as the client conneciton to a TcpNexus
	/// </summary>
	public class TcpNexusClient : ITcpNexusWriter
	{
		public delegate void ClosedDelegate(TcpNexusClient client);
		public delegate void ErrorDelegate(TcpNexusClient client, Exception ex);
		public delegate void DataReceivedDelegate(TcpNexusClient client, byte[] data);
		public delegate void HeartBeatDelegate(TcpNexusClient client);

		public event ClosedDelegate Closed;
		public event DataReceivedDelegate DataReceived;
		public event ErrorDelegate Error;
		public event HeartBeatDelegate HeartBeat;

		public object SyncRoot = new object();

		private TcpClient _client = null;
		private NetworkStream _stream = null;

		private bool _stopping = false;
		private byte[] _length = new byte[4];
		private byte[] _data = null;
		private int _lengthRead = 0;
		private int _dataRead = 0;
		private int _datalength = 0;
		private DateTime _lastValidComms = DateTime.MinValue;

		public DateTime LastValidComms
		{
			get
			{
				return _lastValidComms;
			}
		}

		public TcpNexusClient(TcpClient client)
		{
			_client = client;
			_stream = _client.GetStream();
			_lengthRead = 0;
			_lastValidComms = DateTime.UtcNow;
			_stream.BeginRead(_length, 0, 4, new AsyncCallback(readCallback), null);
		}

		public TcpNexusClient(string targetIP, int portNumber)
			: this(new TcpClient(targetIP, portNumber))
		{

		}

		private int _heartBeatCount = 0;

		private class NoDataReadException : ApplicationException
		{
			public NoDataReadException()
				: base()
			{

			}
		}

		private void readCallback(IAsyncResult ar)
		{
			try
			{
				int bytesRead = _stream.EndRead(ar);
				if (!_stopping)
				{
					if (bytesRead == 0)
						throw new NoDataReadException(); //("EndRead returned without any data");

					if (_lengthRead < 4)
					{
						_lengthRead += bytesRead;

						if (_lengthRead == 4)
						{
							//	check for a heartbeat..
							if ((_length[0] == 0xFF) && (_length[1] == 0xFF) && (_length[2] == 0xFF) && (_length[3] == 0xFF))
							{
								//	The heartbeat is actually 12 bytes - three groups of 4 x FF
								if (++_heartBeatCount == 3)
								{
									_heartBeatCount = 0;
									_lastValidComms = DateTime.UtcNow;
									//	this is a keepalive.. ignore it.
									if (HeartBeat != null)
										HeartBeat(this);
								}
								_lengthRead = 0;
								_stream.BeginRead(_length, 0, 4, new AsyncCallback(readCallback), null);
							}
							else
							{
								_datalength = BitConverter.ToInt32(_length, 0);
								_data = new byte[_datalength];
								_dataRead = 0;

								_stream.BeginRead(_data, 0, _datalength, new AsyncCallback(readCallback), null);
							}
						}
						else
							_stream.BeginRead(_length, _lengthRead, 4 - _lengthRead, new AsyncCallback(readCallback), null);
					}
					else
					{
						_dataRead += bytesRead;
						if (_dataRead < _datalength)
							_stream.BeginRead(_data, _dataRead, _datalength - _dataRead, new AsyncCallback(readCallback), null);
						else
						{
							if (!_stopping)
							{
								_lastValidComms = DateTime.UtcNow;
								if ((_data != null) && (DataReceived != null))
									DataReceived(this, _data);

								_lengthRead = 0;
								_stream.BeginRead(_length, 0, 4, new AsyncCallback(readCallback), null);
							}
						}

					}
				}
			}
			catch (NoDataReadException)
			{
				if (!_stopping)
					Close();
			}
			catch (System.ObjectDisposedException exd)
			{
				//	Only report this if we are not supposed to be stopping.
				if (!_stopping)
				{
					OnError(exd);
					Close();
				}
			}
			catch (Exception ex)
			{
				OnError(ex);
				if (!_stopping)
					Close();
			}
		}

		private void OnError(Exception ex)
		{
			if (Error != null)
				Error(this, ex);
		}

		public void WriteHeartBeat()
		{
			if (!_stopping)
			{
				byte[] length = new byte[] { 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF };

				lock (SyncRoot)
				{
					_stream.Write(length, 0, length.Length);
					_stream.Flush();
					_lastValidComms = DateTime.UtcNow;
				}
			}
		}

		public int Write(byte[] data)
		{
			if (!_stopping)
			{
				byte[] length = BitConverter.GetBytes(data.Length);
				MemoryStream stream = new MemoryStream(data.Length + length.Length);
				stream.Write(length, 0, length.Length);
				stream.Write(data, 0, data.Length);
				lock (SyncRoot)
				{
					//_stream.Write(length, 0, length.Length);
					//_stream.Write(data, 0, data.Length);
					_stream.Write(stream.ToArray(), 0, data.Length + length.Length);
					_stream.Flush();
					_lastValidComms = DateTime.UtcNow;
				}
				return data.Length;
			}
			else
				return 0;
		}

		private void OnClosed()
		{
			if (Closed != null)
				Closed(this);
		}

		public void Close()
		{
			if (!_stopping)
			{
				_stopping = true;
				lock (SyncRoot)
				{
					_stream.Close();
					_client.Close();
					OnClosed();
				}
			}
		}

	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network.Nexus
{
	/// <summary>
	/// This class will manage the communication over one or more monitored clients
	/// ensuring that all requests and responses are matched up appropriately.
	/// </summary>
	public class TcpNexusRequestCoordinator
	{
		#region Connection Management

		/// <summary>
		/// This class identifies the connection details for initialisation.
		/// </summary>
		public class ConnectionSettings
		{
			public int ConnectionCount = 1;
			public string HostName = "localhost";
			public int PortNumber = 0;
			public string Name = "";

			public ConnectionSettings(string name, int count, string hostName, int port)
			{
				Name = name;
				ConnectionCount = count;
				HostName = hostName;
				PortNumber = port;
			}
		}

		private class Connections
		{
			protected object SyncObj = new object();
			private int _clientIndex = -1;
			private TcpNexusMonitoredClient[] _clients = null;
			private static ConnectionSettings _settings = null;

			private Hashtable _pendingRequests = new Hashtable();

			public int NextIdentifier = 0;

			public Hashtable PendingRequests
			{
				get
				{
					return _pendingRequests;
				}
			}

			public Connections(ConnectionSettings settings)
			{
				_settings = settings;
			}

			public TcpNexusMonitoredClient GetConnection()
			{
				if (_clients == null)
					lock (SyncObj)
						if (_clients == null)
						{
							if (_settings != null)
							{
								TcpNexusMonitoredClient[] clients = new TcpNexusMonitoredClient[_settings.ConnectionCount];
								for (int loop = 0; loop < _settings.ConnectionCount; loop++)
								{
									TcpNexusMonitoredClient client = new TcpNexusMonitoredClient(_settings.HostName, _settings.PortNumber);
									client.DataReceived += new TcpNexusMonitoredClient.DataReceivedDelegate(_client_DataReceived);
									client.Error += new TcpNexusMonitoredClient.ErrorDelegate(_client_Error);
									client.Open();
									clients[loop] = client;
								}
								_clients = clients;
							}
						}

				if (_clients == null)
					throw new Exception("Could not initiate Managed Connection");

				if (_clientIndex > _clients.Length)
					lock (SyncObj)
					{
						if (_clientIndex > _clients.Length)
							_clientIndex = -1;
					}

				//	rotate the connections
				int newValue = System.Threading.Interlocked.Increment(ref _clientIndex);
				int indexToUse = (newValue % _clients.Length);
				return _clients[(indexToUse < 0) ? _clients.Length + indexToUse : indexToUse];
			}

			/// <summary>
			/// Close off all of the connections
			/// </summary>
			public void Close()
			{
				if (_clients != null)
					lock (SyncObj)
					{
						if (_clients != null)
						{
							for (int loop = 0; loop < _clients.Length; loop++)
								_clients[loop].Close();
							_clients = null;
						}
					}
			}

			private void _client_DataReceived(TcpNexusMonitoredClient client, byte[] data)
			{
				ProcessResponse(data);
			}

			private void _client_Error(TcpNexusMonitoredClient client, Exception ex)
			{
				//MTData.Taxi.Core.ClientLogger.LogError(-1, 0, false, string.Format("{0} : Monitored Client Error : Exception={1}", _settings.Name, ex.ToString()));
				//TODO : POD
			}

			/// <summary>
			/// Take the response and release the appropriate thread
			/// </summary>
			/// <param name="data"></param>
			private void ProcessResponse(byte[] data)
			{
				try
				{
					TcpNexusResponse response = new TcpNexusResponse();
					response.Decode(data);
					RequestHandle handle = (RequestHandle)_pendingRequests[response.RequestIdentifier];
					if (handle != null)
						handle.SetResponse(data);
//POD					else
//POD						MTData.Taxi.Core.ClientLogger.LogInformation(-1, 0, false, string.Format("{0} : Basic Response Parsing : No waiting request found : {1}", _settings.Name, response.ToString()));
//TODO POD
				}
				catch (Exception)
				{
//POD					MTData.Taxi.Core.ClientLogger.LogError(-1, 0, false, string.Format("{0} : Basic Response Parsing Error : Data={1}; Exception={2}", _settings.Name, BitConverter.ToString(data), ex.ToString()));
					//TODO POD
				}
			}
		}


		/// <summary>
		/// Get the connection settings for the link.
		/// </summary>
		/// <returns></returns>
		protected virtual ConnectionSettings GetConnectionSettings()
		{
			return null;
		}

		private static Hashtable _connections = new Hashtable();
		protected static object ConnectionsSyncObj = new object();

		/// <summary>
		/// This method will return a list of connections for the speicfied type.
		/// </summary>
		/// <returns></returns>
		private Connections GetTypeConnections()
		{
			Connections connections = (Connections)_connections[GetType()];
			if (connections == null)
				lock (ConnectionsSyncObj)
				{
					connections = (Connections)_connections[GetType()];
					if (connections == null)
					{
						connections = new Connections(GetConnectionSettings());
						_connections.Add(GetType(), connections);
					}
				}
			return connections;
		}

		/// <summary>
		/// Prepare the connections to be used.
		/// </summary>
		/// <returns></returns>
		protected TcpNexusMonitoredClient GetConnection()
		{
			Connections connections = GetTypeConnections();
			return connections.GetConnection();
		}

		public virtual string GetName()
		{
			return "NexusRequestCoordinator";
		}

		/// <summary>
		/// Close off all of the connections.
		/// </summary>
		public void Close()
		{
			Connections connections = (Connections)_connections[GetType()];
			if (connections != null)
				lock (ConnectionsSyncObj)
				{
					connections = (Connections)_connections[GetType()];
					if (connections != null)
					{
						_connections.Remove(GetType());
						connections.Close();
					}
				}
		}

		#endregion

		#region Request Maangement

		private enum RequestState
		{
			Success,
			Timeout,
			ResponseCode,
			Exception,
			Pending,
			NonPendingOnResponse
		}

		public enum ExceptionCause
		{
			Timeout,
			ResponseCode,
			Exception,
			NonPendingOnResponse,
			MisMatchedRequestIDs,
			NoConnectionAvailable
		}

		private class RequestHandle
		{
			private RequestState _state = RequestState.Pending;
			private TcpNexusPayload _request = null;

			public int RequestIdentifier
			{
				get
				{
					return _request.RequestIdentifier;
				}
			}
			public TcpNexusPayload Response;
			public RequestState State
			{
				get
				{
					return _state;
				}
			}
			public Exception ParseException
			{
				get
				{
					return _exception;
				}
			}

			private Exception _exception = null;

			private string _name = "";

			private Hashtable _pendingRequests = null;

			public System.Threading.AutoResetEvent _waitHandle;

			public RequestHandle(string name, TcpNexusPayload request, TcpNexusPayload response, Hashtable pendingRequests)
			{
				_pendingRequests = pendingRequests;

				_name = name;
				_request = request;
				Response = response;
				_waitHandle = new System.Threading.AutoResetEvent(false);
			}

			public RequestState WaitOne(TcpNexusMonitoredClient connection, int timeout)
			{
				//MTData.Taxi.Core.ClientLogger.LogInformation(-1, 0, false, string.Format("{0} : Request WaitOne : RID={1};", _name, RequestIdentifier));
				lock (_pendingRequests)
				{
					RequestHandle handle = (RequestHandle)_pendingRequests[RequestIdentifier];
					if (handle != null)
						handle.Timeout();
					_pendingRequests.Add(this.RequestIdentifier, this);
				}
				connection.Write(_request.Encode());

				if (!_waitHandle.WaitOne(timeout, false))
					Timeout();

				return _state;
			}

			public void Timeout()
			{
				lock (_pendingRequests)
					_pendingRequests.Remove(RequestIdentifier);
				if (_state == RequestState.Pending)
					_state = RequestState.Timeout;
				_waitHandle.Set();
				//MTData.Taxi.Core.ClientLogger.LogInformation(-1, 0, false, string.Format("{0} : Request Timeout : RID={1};",_name, RequestIdentifier));
			}

			public void SetResponse(byte[] data)
			{
				lock (_pendingRequests)
					_pendingRequests.Remove(RequestIdentifier);

				if (_state == RequestState.Pending)
				{
					try
					{
						if (Response.Decode(data))
							_state = RequestState.Success;
						else
							_state = RequestState.ResponseCode;
					}
					catch (Exception ex)
					{
						_state = RequestState.Exception;
						_exception = ex;
					}
				}
				else
					_state = RequestState.NonPendingOnResponse;
				_waitHandle.Set();
				//MTData.Taxi.Core.ClientLogger.LogInformation(-1, 0, false, string.Format("{0} : Request Response : RID={1};",_name, RequestIdentifier));
			}
		}

		public Hashtable GetTypeRequests()
		{
			Connections connections = GetTypeConnections();
			return connections.PendingRequests;
		}

		/// <summary>
		/// This method will ensure that the calling thread waits for the appropriate response.
		/// Only a matching requestIdentifier will release the thread
		/// </summary>
		/// <param name="request"></param>
		private void WaitForResponse(TcpNexusMonitoredClient connection, TcpNexusPayload request, TcpNexusPayload response, int timeout)
		{
			RequestHandle handle = new RequestHandle(GetName(), request, response, GetTypeRequests());
			RequestState state = handle.WaitOne(connection, timeout);

			switch (state)
			{
				case RequestState.Timeout:
					//throw new Exception(string.Format("{0} Timeout : Request = {1}", GetName(), request.ToString()));
					throw new TcpNexusRequestException(string.Format("{0} Timeout : Request = {1}", GetName(), request.ToString()), GetName(), request, ExceptionCause.Timeout);
				case RequestState.Exception:
					//throw new Exception(string.Format("{0} Exception", GetName()), handle.ParseException);
					throw new TcpNexusRequestException(string.Format("{0} Exception", GetName()), GetName(), request, ExceptionCause.Exception, handle.ParseException);
				case RequestState.ResponseCode:
					//throw new Exception(string.Format("{0} Error : ErrorCode = {1};Request={2}", GetName(), response.ErrorCode, request.ToString()));
					throw new TcpNexusRequestException(string.Format("{0} Error : ErrorCode = {1};Request={2}", GetName(), response.ErrorCode, request.ToString()), GetName(), request, ExceptionCause.ResponseCode);
				case RequestState.NonPendingOnResponse:
					//throw new Exception(string.Format("{0} Error : Response to non-Pending State;Request={1}", GetName(), request.ToString()));
					throw new TcpNexusRequestException(string.Format("{0} Error : Response to non-Pending State;Request={1}", GetName(), request.ToString()), GetName(), request, ExceptionCause.NonPendingOnResponse);
			}
		}

		/// <summary>
		/// REturn the next identifier for the target platform
		/// </summary>
		/// <returns></returns>
		private int GetTypeRequestIdentifier()
		{
			Connections connections = GetTypeConnections();
			return System.Threading.Interlocked.Increment(ref connections.NextIdentifier);
		}

		/// <summary>
		/// Process a formulated request for the plot manager.
		/// </summary>
		/// <param name="request"></param>
		/// <param name="response"></param>
		protected void ProcessRequest(TcpNexusPayload request, TcpNexusPayload response)
		{
			TcpNexusMonitoredClient connection = GetConnection();
			if (connection != null)
			{
				request.RequestIdentifier = GetTypeRequestIdentifier();

				WaitForResponse(connection, request, response, 15000);
				if (request.RequestIdentifier != response.RequestIdentifier)
					throw new TcpNexusRequestException(string.Format("{0} : MisMatched Request Identifiers - Request {1}", GetName(), request.ToString()), GetName(), request, ExceptionCause.MisMatchedRequestIDs);
				//throw new Exception(string.Format("{0} : MisMatched Request Identifiers - Request {1}", GetName(), request.ToString()));
			}
			else
				throw new TcpNexusRequestException(string.Format("{0} : No Connection Available - Request {1}", GetName(), request.ToString()), GetName(), request, ExceptionCause.NoConnectionAvailable);
			//throw new Exception(string.Format("{0} : No Connection Available - Request {1}", GetName(), request.ToString()));
		}

		#endregion

		/// <summary>
		/// Prepare the class for use.
		/// </summary>
		public TcpNexusRequestCoordinator()
		{

		}
	}
}

using System;
using System.Net;
using System.Net.Sockets;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Text;

namespace MTData.Sys.IO.Network.Nexus
{
	/// <summary>
	/// This class will coordinate the acceptance of inbound Socket connections.
	/// It will also monitor the activity of the connections, maintaining
	/// a list of what is currently connected.
	/// </summary>
	public class TcpNexus
	{
		private class QueueEntry
		{
			public byte[] Data;

			public QueueEntry(byte[] data)
			{
				Data = data;
			}
		}

		private System.Collections.Queue _outboundQueue = null;
		private System.Threading.AutoResetEvent _outboundWaitHandle = null;
		private System.Threading.Thread _outboundThread = null;

		public delegate void ErrorDelegate(TcpNexus nexus, TcpNexusClient client, Exception ex);
		public delegate void DataReceivedDelegate(TcpNexus nexus, TcpNexusClient client, byte[] data);

		public delegate void ConnectionDelegate(TcpNexus nexus, TcpNexusClient client);

		public event ErrorDelegate Error;
		public event DataReceivedDelegate DataReceived;
		public event ConnectionDelegate ConnectionAdded;
		public event ConnectionDelegate ConnectionRemoved;

		private IPEndPoint _localIPEndPoint = null;
		private TcpListener _listenerSocket = null;
		private Thread _listenerThread = null;
		private bool _stopping = false;

		private object _syncRoot = new object();
		private ArrayList _clients = new ArrayList();
		private int _keepAliveSeconds = 5;
		private bool _delayedStart = false;

		public TcpNexus()
		{
			_delayedStart = true;
		}

		public void Start(string localBindIP, int portNumber, int keepAliveSeconds)
		{
			if (!_delayedStart)
				throw new InvalidOperationException("Cannot DelayStart a running instance");
			_delayedStart = false;
			Initialise(localBindIP, portNumber, keepAliveSeconds);
		}

		public void Start(IPEndPoint endPoint, int keepAliveSeconds)
		{
			if (!_delayedStart)
				throw new InvalidOperationException("Cannot DelayStart a running instance");
			_delayedStart = false;
			Initialise(endPoint, keepAliveSeconds);
		}

		public TcpNexus(string localBindIP, int portNumber)
			: this(localBindIP, portNumber, 5)
		{

		}

		public TcpNexus(IPEndPoint endPoint, int keepAliveSeconds)
		{
			Initialise(endPoint, keepAliveSeconds);
		}

		public TcpNexus(string localBindIP, int portNumber, int keepAliveSeconds)
		{
			Initialise(localBindIP, portNumber, keepAliveSeconds);
		}

		private void Initialise(string localBindIP, int portNumber, int keepAliveSeconds)
		{
			IPEndPoint endPoint = null;
			if ((localBindIP != null) && (localBindIP.Trim() != ""))
				endPoint = new IPEndPoint(System.Net.IPAddress.Parse(localBindIP), portNumber);
			else
				endPoint = new IPEndPoint(System.Net.IPAddress.Any, portNumber);
			Initialise(endPoint, keepAliveSeconds);
		}

		private void Initialise(IPEndPoint endPoint, int keepAliveSeconds)
		{
			_keepAliveSeconds = keepAliveSeconds;
			_localIPEndPoint = endPoint;

			_outboundQueue = new System.Collections.Queue();
			_outboundWaitHandle = new AutoResetEvent(false);
			_outboundThread = new Thread(new ThreadStart(OutboundHandler));
			_outboundThread.IsBackground = true;


			_listenerSocket = new TcpListener(_localIPEndPoint);
			_listenerSocket.Start();
			_listenerThread = new Thread(new ThreadStart(ListenHandler));
			_listenerThread.IsBackground = true;
			_listenerThread.Name = "TCPNexus : " + _localIPEndPoint.ToString();
			_listenerThread.Start();

			_outboundThread.Start();
		}

		private void ListenHandler()
		{
			try
			{
				while (!_stopping)
				{
					TcpClient client = _listenerSocket.AcceptTcpClient();
					if (!_stopping)
						ProcessInboundConnection(client);
				}
			}
			catch (Exception ex)
			{
				if (Error != null)
					Error(this, null, ex);
			}
		}


		private void ProcessInboundConnection(TcpClient clientConnection)
		{
			lock (_syncRoot)
			{
				if (!_stopping)
				{
					TcpNexusClient client = new TcpNexusClient(clientConnection);
					client.Closed += new TcpNexusClient.ClosedDelegate(client_Closed);
					client.DataReceived += new TcpNexusClient.DataReceivedDelegate(client_DataReceived);
					client.Error += new TcpNexusClient.ErrorDelegate(client_Error);
					_clients.Add(client);
					if (ConnectionAdded != null)
						ConnectionAdded(this, client);
				}
			}
		}

		/// <summary>
		/// Return a list of all active clients connected to the nexus.
		/// </summary>
		/// <returns></returns>
		public TcpNexusClient[] GetActiveClients()
		{
			lock (_syncRoot)
				return (TcpNexusClient[])_clients.ToArray(typeof(TcpNexusClient));
		}

		private void CloseAllClients()
		{
			TcpNexusClient[] clients = null;
			lock (_syncRoot)
				clients = (TcpNexusClient[])_clients.ToArray(typeof(TcpNexusClient));

			foreach (TcpNexusClient client in clients)
			{
				client.Close();
				if (ConnectionRemoved != null)
					ConnectionRemoved(this, client);
			}

			_clients.Clear();
		}

		public void Stop()
		{
			try
			{
				_stopping = true;
				_outboundWaitHandle.Set();

				CloseAllClients();

				_listenerSocket.Stop();
			}
			catch (Exception ex)
			{
				if (Error != null)
					Error(this, null, ex);
			}
		}

		public void Broadcast(byte[] data)
		{
			lock (_outboundQueue)
			{
				_outboundQueue.Enqueue(new QueueEntry(data));
				_outboundWaitHandle.Set();
			}
		}

		private void OutboundHandler()
		{
			//	using it on 250 millisecond increments ensures
			//	that we process each connection twice as often as the timeout, and since 
			//	Heartbeat timeout on the server is half that on the monitored client anyway,
			//	It guarantees we will catch all connections within their timeout period.
			int keepAliveTimeout = (_keepAliveSeconds * 500);

			bool signalled = false;
			while (!_stopping)
			{
				if (keepAliveTimeout != 0)
					signalled = _outboundWaitHandle.WaitOne(keepAliveTimeout, false);
				else
					signalled = _outboundWaitHandle.WaitOne();

				if (!_stopping)
				{
					if (signalled)
					{
						QueueEntry queueEntry = null;
						lock (_outboundQueue)
							if (_outboundQueue.Count > 0)
								queueEntry = (QueueEntry)_outboundQueue.Dequeue();

						while (queueEntry != null)
						{
							ThreadBroadcast((byte[])queueEntry.Data);

							queueEntry = null;
							lock (_outboundQueue)
								if (_outboundQueue.Count > 0)
									queueEntry = (QueueEntry)_outboundQueue.Dequeue();
						}
					}
					else
						PerformHouskeeping();
				}
			}
		}

		ArrayList _transmissionErrors = new ArrayList();

		private void ThreadBroadcast(byte[] data)
		{
			TcpNexusClient[] clients = null;
			lock (_syncRoot)
				clients = (TcpNexusClient[])_clients.ToArray(typeof(TcpNexusClient));

			if ((clients != null) && (clients.Length > 0))
			{
				_transmissionErrors.Clear();

				foreach (TcpNexusClient connection in clients)
					try
					{
						connection.Write(data);
					}
					catch (Exception ex)
					{
						if (Error != null)
							Error(this, connection, ex);
						_transmissionErrors.Add(connection);
					}

				if (_transmissionErrors.Count > 0)
				{
					foreach (TcpNexusClient connection in _transmissionErrors)
						connection.Close();
					_transmissionErrors.Clear();
				}
			}
		}

		private void PerformHouskeeping()
		{
			//	Iterate all of the connections, and if no data has been processed within the 
			//	time period, send a heartbeat packet.
			TcpNexusClient[] clients = null;
			lock (_syncRoot)
				clients = (TcpNexusClient[])_clients.ToArray(typeof(TcpNexusClient));

			if ((clients != null) && (clients.Length > 0))
			{
				_transmissionErrors.Clear();

				DateTime cutoff = DateTime.UtcNow.AddSeconds(-_keepAliveSeconds);

				foreach (TcpNexusClient connection in clients)
					try
					{
						if (connection.LastValidComms < cutoff)
							connection.WriteHeartBeat();
					}
					catch (Exception ex)
					{
						if (Error != null)
							Error(this, connection, ex);
						_transmissionErrors.Add(connection);
					}

				if (_transmissionErrors.Count > 0)
				{
					foreach (TcpNexusClient connection in _transmissionErrors)
						connection.Close();
					_transmissionErrors.Clear();
				}

			}
		}

		private void client_Closed(TcpNexusClient client)
		{
			lock (_syncRoot)
				_clients.Remove(client);
			if (ConnectionRemoved != null)
				ConnectionRemoved(this, client);
		}

		private void client_DataReceived(TcpNexusClient client, byte[] data)
		{
			if (DataReceived != null)
				DataReceived(this, client, data);
		}

		private void client_Error(TcpNexusClient client, Exception ex)
		{
			if (Error != null)
				Error(this, client, ex);
			client.Close();
		}
	}
}

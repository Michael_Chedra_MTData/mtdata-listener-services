using System;
using System.Threading;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network.Nexus
{
	/// <summary>
	/// This class will maintain a connection from the client side.
	/// IF the connection fails for some reason, it 
	/// will be re-opened to ensure reliable comms.
	/// </summary>
	public class TcpNexusMonitoredClient : ITcpNexusWriter
	{
		public enum TimeoutMonitorCommand
		{
			None,
			MonitorStart,
			MonitorRun,
			Close
		}

		public enum ConnectionState
		{
			Open,
			Closed
		}

		private string _targetIP = null;
		private int _portNumber = 0;
		private ConnectionState _state = ConnectionState.Closed;
		private bool _active = false;

		private DateTime _lastValidAction = DateTime.MinValue;
		private long _timeoutIntervalSeconds = 0;
		private AutoResetEvent _timeoutWaitHandle = null;
		private Thread _timeoutMonitor = null;
		private TimeoutMonitorCommand _timeoutCommand = TimeoutMonitorCommand.None;
		private object SyncRoot = new object();

		public delegate void ErrorDelegate(TcpNexusMonitoredClient client, Exception ex);
		public delegate void DataReceivedDelegate(TcpNexusMonitoredClient client, byte[] data);
		public delegate void StateChangedDelegate(TcpNexusMonitoredClient client, ConnectionState state);

		public event DataReceivedDelegate DataReceived;
		public event ErrorDelegate Error;
		public event StateChangedDelegate StateChanged;

		public ConnectionState State
		{
			get
			{
				return _state;
			}
		}

		private TcpNexusClient _client = null;

		public TcpNexusMonitoredClient(string targetIP, int portNumber)
			: this(targetIP, portNumber, 10)
		{

		}

		public TcpNexusMonitoredClient(string targetIP, int portNumber, long timeoutIntervalSeconds)
		{
			_timeoutIntervalSeconds = timeoutIntervalSeconds;
			if (_timeoutIntervalSeconds > 0)
			{
				_timeoutWaitHandle = new AutoResetEvent(false);
				_timeoutMonitor = new Thread(new ThreadStart(timeoutMonitorHandler));
				_timeoutMonitor.IsBackground = true;
				_timeoutMonitor.Start();
			}

			_targetIP = targetIP;
			_portNumber = portNumber;

			_client = null;
		}

		private void timeoutMonitorHandler()
		{
			bool running = true;
			bool monitoring = false;
			bool signalled = true;
			int nextTimeout = 0;
			while (running)
			{
				if (monitoring)
				{
					DateTime target = DateTime.MinValue;

					long timeoutInterval = _timeoutIntervalSeconds;

					//	if the connection is currently closed, we work off of a smaller 5 second delay..
					if (_client == null)
						timeoutInterval = 5;
					if ((_client != null) && (_lastValidAction != DateTime.MinValue))
						target = _lastValidAction;
					else
						target = DateTime.UtcNow;

					target = target.AddSeconds(timeoutInterval);

					if (target != DateTime.MinValue)
					{
						nextTimeout = Convert.ToInt32(((TimeSpan)(target - DateTime.UtcNow)).TotalMilliseconds);
						if (nextTimeout > 0)
							signalled = _timeoutWaitHandle.WaitOne(nextTimeout, false);
					}
					else
					{
						_timeoutWaitHandle.WaitOne();
						signalled = true;
					}

				}
				else
				{
					_timeoutWaitHandle.WaitOne();
					signalled = true;
				}

				switch (_timeoutCommand)
				{
					case TimeoutMonitorCommand.MonitorStart:
						{
							monitoring = true;
							_timeoutCommand = TimeoutMonitorCommand.MonitorRun;
							OpenClient();
							break;
						}
					case TimeoutMonitorCommand.MonitorRun:
						{
							if (!signalled)
							{
								//	connection has timed out..
								CloseClient();
								OpenClient();
							}
							break;
						}
					case TimeoutMonitorCommand.Close:
						{
							monitoring = false;
							running = false;
							break;
						}
				}
			}
		}

		private void OnStateChanged(ConnectionState newState)
		{
			if (newState != _state)
			{
				_state = newState;
				if (StateChanged != null)
					StateChanged(this, newState);
			}
		}

		public void Open()
		{
			if (!_active)
			{
				_active = true;
				if (_timeoutMonitor != null)
				{
					_timeoutCommand = TimeoutMonitorCommand.MonitorStart;
					_timeoutWaitHandle.Set();
				}
			}
		}

		private void CloseClient()
		{
			CloseClient(false);
		}

		private void CloseClient(bool closed)
		{
			bool triggerStateChange = false;

			if (_client != null)
				lock (SyncRoot)
				{
					if (_client != null)
					{
						_client.Closed -= new TcpNexusClient.ClosedDelegate(_client_Closed);
						if (!closed)
							_client.Close();
						_client.DataReceived -= new TcpNexusClient.DataReceivedDelegate(_client_DataReceived);
						_client.Error -= new TcpNexusClient.ErrorDelegate(_client_Error);
						_client.HeartBeat -= new TcpNexusClient.HeartBeatDelegate(_client_HeartBeat);
						_client = null;
						triggerStateChange = true;
						_timeoutWaitHandle.Set();
					}
				}
			if (triggerStateChange)
				OnStateChanged(ConnectionState.Closed);
		}

		private void OpenClient()
		{
			bool triggerStateChange = false;
			if (_active)
				lock (SyncRoot)
				{
					if (_active)
					{
						try
						{
							if (_client == null)
							{
								_client = new TcpNexusClient(_targetIP, _portNumber);
								_client.DataReceived += new TcpNexusClient.DataReceivedDelegate(_client_DataReceived);
								_client.Error += new TcpNexusClient.ErrorDelegate(_client_Error);
								_client.Closed += new TcpNexusClient.ClosedDelegate(_client_Closed);
								_client.HeartBeat += new TcpNexusClient.HeartBeatDelegate(_client_HeartBeat);
								_lastValidAction = DateTime.UtcNow;
								triggerStateChange = true;
							}
						}
						catch (Exception ex)
						{
							OnError(ex);
						}
					}
				}
			if (triggerStateChange)
				OnStateChanged(ConnectionState.Open);
		}

		public void Close()
		{
			_active = false;
			if (_timeoutMonitor != null)
			{
				_timeoutCommand = TimeoutMonitorCommand.Close;
				_timeoutWaitHandle.Set();
			}

			CloseClient();
		}

		private void OnError(Exception ex)
		{
			if (Error != null)
				Error(this, ex);
			CloseClient();
		}

		private void _client_DataReceived(TcpNexusClient client, byte[] data)
		{
			_lastValidAction = DateTime.UtcNow;
			if (_timeoutMonitor != null)
				_timeoutWaitHandle.Set();
			if (DataReceived != null)
				DataReceived(this, data);
		}

		public int Write(byte[] data)
		{
			OpenClient();
			if (_client != null)
			{
				_client.Write(data);
				_lastValidAction = DateTime.UtcNow;
				if (_timeoutMonitor != null)
					_timeoutWaitHandle.Set();

				return data.Length;
			}
			else
				return 0;
		}

		private void _client_Error(TcpNexusClient client, Exception ex)
		{
			OnError(ex);
		}

		private void _client_Closed(TcpNexusClient client)
		{
			CloseClient(true);
		}

		private void _client_HeartBeat(TcpNexusClient client)
		{
			_lastValidAction = DateTime.UtcNow;
			if (_timeoutMonitor != null)
				_timeoutWaitHandle.Set();
		}
	}
}

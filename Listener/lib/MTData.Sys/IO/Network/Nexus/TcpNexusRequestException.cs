using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network.Nexus
{
	public class TcpNexusRequestException : ApplicationException
	{
		private string _name;
		private TcpNexusPayload _request;
		private TcpNexusRequestCoordinator.ExceptionCause _cause;

		public TcpNexusRequestException(string message, string name, TcpNexusPayload request, TcpNexusRequestCoordinator.ExceptionCause cause)
			: this(message, name, request, cause, null)
		{

		}

		public TcpNexusRequestException(string message, string name, TcpNexusPayload request, TcpNexusRequestCoordinator.ExceptionCause cause, Exception innerException)
			: base(message, innerException)
		{
			_name = name;
			_request = request;
			_cause = cause;
		}

		public string Name
		{
			get
			{
				return _name;
			}
		}

		public TcpNexusPayload Request
		{
			get
			{
				return _request;
			}
		}

		public TcpNexusRequestCoordinator.ExceptionCause Cause
		{
			get
			{
				return _cause;
			}
		}

	}
}

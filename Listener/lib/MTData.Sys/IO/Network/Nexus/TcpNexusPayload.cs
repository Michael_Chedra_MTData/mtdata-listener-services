using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network.Nexus
{
	/// <summary>
	/// This interface supports payload transfers
	/// </summary>
	public interface TcpNexusPayload
	{
		int RequestIdentifier
		{
			get;
			set;
		}
		byte ErrorCode
		{
			get;
			set;
		}

		byte[] Encode();
		bool Decode(byte[] data);
		string ToString();
	}

	public class TcpNexusRequest : TcpNexusPayload
	{
		private int _requestIdentifier;
		protected byte _errorCode = 0;

		protected virtual bool ProcessDecode(BinaryReader reader)
		{
			_requestIdentifier = reader.ReadInt32();
			return true;
		}

		protected virtual void ProcessEncode(BinaryWriter writer)
		{
			writer.Write(RequestIdentifier);
		}

		#region TcpNexusPayload Members

		public byte ErrorCode
		{
			get
			{
				return _errorCode;
			}
			set
			{
				_errorCode = value;
			}
		}
		public int RequestIdentifier
		{
			get
			{
				return _requestIdentifier;
			}
			set
			{
				_requestIdentifier = value;
			}
		}

		public bool Decode(byte[] data)
		{
			MemoryStream stream = new MemoryStream(data);
			BinaryReader reader = new BinaryReader(stream);
			return ProcessDecode(reader);
		}

		public byte[] Encode()
		{
			MemoryStream stream = new MemoryStream();
			BinaryWriter writer = new BinaryWriter(stream);
			ProcessEncode(writer);
			return stream.ToArray();
		}

		public override string ToString()
		{
			return string.Format("RID={0};", _requestIdentifier);
		}

		#endregion
	}

	public class TcpNexusResponse : TcpNexusRequest
	{
		protected override bool ProcessDecode(BinaryReader reader)
		{
			_errorCode = reader.ReadByte();
			base.ProcessDecode(reader);
			return (_errorCode == 0);
		}

		protected override void ProcessEncode(BinaryWriter writer)
		{
			writer.Write(_errorCode);
			base.ProcessEncode(writer);
		}

		#region TcpNexusPayload Members


		public override string ToString()
		{
			return string.Format("ErrorCode={0};{1}", _errorCode, base.ToString());
		}

		#endregion
	}
}

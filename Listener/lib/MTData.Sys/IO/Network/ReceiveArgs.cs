using System;
using System.Net;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// Event definition for received event.
	/// </summary>
	/// <param name="sender">Sender of the event</param>
	/// <param name="args">Arguments describing receipt event</param>
	public delegate void ReceiveDelegate(object sender, ReceiveArgs args);

	/// <summary>
	/// This class represents the arguments of the receive event for
	/// inbound communications.
	/// </summary>
	public class ReceiveArgs
	{
		/// <summary>
		/// Received data
		/// </summary>
		private byte[] _data;

		/// <summary>
		/// Source EndPoint
		/// </summary>
		private IPEndPoint _endPoint;

		/// <summary>
		/// Population constructor
		/// </summary>
		/// <param name="endPoint">Source EndPoint of data</param>
		/// <param name="data">Data received</param>
		public ReceiveArgs(IPEndPoint endPoint, byte[] data)
		{
			_endPoint = endPoint;
			_data = data;
		}

		/// <summary>
		/// EndPoint that sourced the data
		/// </summary>
		public IPEndPoint EndPoint
		{
			get
			{
				return _endPoint;
			}
		}

		/// <summary>
		/// Data received from source
		/// </summary>
		public byte[] Data
		{
			get
			{
				return _data;
			}
		}
	}
}

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using log4net;
using MTData.Sys.IO.Serial;
using MTData.Sys;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// This is a TcpClient implementation of SerialIO, to allow use within 
	/// a standard BaseRptrChannel
	/// </summary>
	public class TcpSerialIO : ISerialIO //, IHasInstanceName
	{
		public enum Mode
		{
			Socket,
			TcpClient
		}

		public delegate void TcpSerialIOSerialErrorDelegate(TcpSerialIO sender, Exception ex);

		private static ILog _log = LogManager.GetLogger(typeof (TcpSerialIO));
		private static readonly bool _logIsDebugEnabled = _log.IsDebugEnabled;
		private static readonly bool _logIsErrorEnabled = _log.IsErrorEnabled;
		private static readonly bool _logIsFatalEnabled = _log.IsFatalEnabled;
		private static readonly bool _logIsInfoEnabled = _log.IsInfoEnabled;
		private static readonly bool _logIsWarnEnabled = _log.IsWarnEnabled;

		private const int READ_BUFFER_SIZE = 1024;

		private Mode				_mode = Mode.Socket;
		private Socket				_socket = null;
		private TcpClient			_client = null;
		private NetworkStream		_stream = null;

		private SerialIORxHandler	_rxHandler;
		private SerialIOErrorHandler _errorHandler;

		private Queue				_rxQueue = null;
		private AutoResetEvent		_eventIn;
		private bool				_active = false;
		private Thread				_threadIn;
		private byte[]				_buffer = null;

		private Thread				_threadOut;
		private AutoResetEvent		_eventOut;
		private Queue				_txQueue = null;

		public event TcpSerialIOSerialErrorDelegate TcpSerialError;

		public TcpSerialIO(TcpClient client)
		{
			if (_logIsDebugEnabled)
				_log.Debug(InstanceName + " Ctor : Creating from TcpClient");

			_mode = Mode.TcpClient;
			_socket = null;
			_client = client;
			_stream = client.GetStream();
			Initalise();
		}

		public TcpSerialIO(Socket socket)
		{
			if (_logIsDebugEnabled)
				_log.Debug(InstanceName + " Ctor : Creating from Socket");

			_mode = Mode.Socket;
			_socket = socket;
			_client = null;
			_stream = null;
			Initalise();
		}

		private void Initalise()
		{
			if (_logIsDebugEnabled)
				_log.Debug(InstanceName + " Initialise : In");

			_eventIn = new AutoResetEvent(false);
			_eventOut = new AutoResetEvent(false);
			
			_active = true;
			_rxQueue = new Queue();
			_txQueue = new Queue();

			_threadIn = new Thread(new ThreadStart(threadIn_Handler));
#if NETCF
#else
			_threadIn.IsBackground = true;
#endif
			_threadIn.Start();

			_threadOut = new Thread(new ThreadStart(threadOut_Handler));
#if NETCF
#else
			_threadOut.IsBackground = true;
#endif
			_threadOut.Start();

			_buffer = new byte[READ_BUFFER_SIZE];
			if (_mode == Mode.TcpClient)
				_stream.BeginRead(_buffer, 0, READ_BUFFER_SIZE, new System.AsyncCallback(socket_ReadCallback), _stream);
			else
				_socket.BeginReceive(_buffer, 0, READ_BUFFER_SIZE, SocketFlags.None, new System.AsyncCallback(socket_ReadCallback), _socket);

			if (_logIsDebugEnabled)
				_log.Debug(InstanceName + " Initialise : Out");
		}

		/// <summary>
		/// Take received data and trigger events on it. This will ensure the data received is passed 
		/// in the event in the correct order.
		/// </summary>
		private void threadIn_Handler()
		{
			Queue current = new Queue();
			while(_active)
			{
				current.Clear();
				lock(_rxQueue)
				{
					while(_rxQueue.Count > 0)
						current.Enqueue(_rxQueue.Dequeue());
				}

				while(current.Count > 0)
				{
					try
					{
						HandleRxData((byte[])current.Dequeue());
					}
					catch(Exception ex)
					{
						if (_logIsErrorEnabled)
							_log.Error(InstanceName + " ThreadIn_Handler : Exception", ex);
					}
				}

				_eventIn.WaitOne();
			}
			if (_logIsDebugEnabled)
				_log.Debug(InstanceName + " ThreadIn_Handler : Thread Exiting");

		}

		/// <summary>
		/// Take items from the queue and transmit them
		/// </summary>
		private void threadOut_Handler()
		{
			Queue current = new Queue();
			Exception error = null;
			while(_active && (error == null))
			{
				current.Clear();
				lock(_txQueue)
				{
					while(_txQueue.Count > 0)
						current.Enqueue(_txQueue.Dequeue());
				}

				while(current.Count > 0)
				{
					byte[] data = (byte[])current.Dequeue();
					try
					{
						if (_mode == Mode.TcpClient)
							_stream.Write(data, 0, data.Length);
						else
							_socket.Send(data, 0, data.Length, SocketFlags.None);
					}
					catch(Exception ex)
					{
						if (_logIsErrorEnabled)
							_log.Error(InstanceName + " txBytes failed", ex);
						error = ex;
					}
				}
				if (error == null)
					_eventOut.WaitOne();
			}
			if (error != null) 
			{
				if (TcpSerialError != null)
					TcpSerialError(this, error);
				if (ErrorHandler != null)
					ErrorHandler(this, error);
			}
		}

		private void ProcessRxBuffer(int byteCount)
		{
			if(byteCount > 0)
			{
				byte[] rxBuffer = new byte[byteCount];
				Array.Copy(_buffer, 0, rxBuffer, 0, byteCount);
				
				if (_logIsDebugEnabled)
					_log.Debug(InstanceName + " : In : " + StringHelper.GetBufferHexString(rxBuffer));

				lock(_rxQueue)
					_rxQueue.Enqueue(rxBuffer);

				_eventIn.Set();
			}
		}

		/// <summary>
		/// This will be triggered by the BeginRead method.
		/// </summary>
		/// <param name="result"></param>
        private void socket_ReadCallback(IAsyncResult result)
		{
			try
			{
				if (result.AsyncState is NetworkStream)
				{
					NetworkStream stream = (NetworkStream)result.AsyncState;
					int count = stream.EndRead(result);
					if (count == 0)
						throw new Exception(InstanceName + " EndRead returned 0 bytes");

					ProcessRxBuffer(count);
					stream.BeginRead(_buffer, 0, READ_BUFFER_SIZE, new System.AsyncCallback(socket_ReadCallback), stream);
				}
				else
				{
					Socket socket = (Socket)result.AsyncState;
					int count = socket.EndReceive(result);
					if (count == 0)
						throw new Exception(InstanceName + " EndRead returned 0 bytes");
					ProcessRxBuffer(count);
					socket.BeginReceive(_buffer, 0, READ_BUFFER_SIZE, SocketFlags.None, new System.AsyncCallback(socket_ReadCallback), socket);
				}
			}
			catch(Exception ex)
			{
				if (_logIsErrorEnabled)
					_log.Error(InstanceName + " readCallBack failed", ex);

				if (TcpSerialError != null)
					TcpSerialError(this, ex);
				if (ErrorHandler != null)
					ErrorHandler(this, ex);
			}
		}

		private void HandleRxData(byte[] data)
		{
			if (_active && (_rxHandler != null))
				_rxHandler(this, data);
		}

		#region ISerialIO Members

		/// <summary>
		/// Write out the data to the network stream.
		/// </summary>
		/// <param name="data"></param>
		public void TxBytes(byte[] data)
		{
			if (_logIsDebugEnabled)
				_log.Debug(InstanceName + " : Out : " + StringHelper.GetBufferHexString(data));
			lock(_txQueue)
				_txQueue.Enqueue(data);
			_eventOut.Set();
		}

		public SerialIORxHandler RxHandler
		{
			get{ return _rxHandler; } set{ _rxHandler = value; }
		}

		public SerialIOErrorHandler ErrorHandler 
		{ 
			get{ return _errorHandler; } 
			set{ _errorHandler = value; } 
		}


		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			_active = false;
			_eventIn.Set();
			_eventOut.Set();
			if (_mode == Mode.TcpClient)
			{
				_stream.Close();			
				_client.Close();
			}
			else
				_socket.Close();
		}

		#endregion

		public string InstanceName
		{
			get
			{
				string name = this.GetType().Name;
				if (_mode == Mode.Socket)
				{
					try
					{
						if ((_socket != null) && (_socket.RemoteEndPoint != null))
							name += "_" + _socket.RemoteEndPoint.ToString();
					}
					catch(Exception e)
					{
						name += "_" + e.Message;
					}
				}
				else
					name += "_TCPClient";
				return name;
			}
		}
	}
}

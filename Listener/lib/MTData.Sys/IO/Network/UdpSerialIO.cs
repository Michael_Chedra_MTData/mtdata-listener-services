﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using MTData.Sys.IO.Serial;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// Summary description for UdpSerialIO.
	/// </summary>
	public class UdpSerialIO : ISerialIO
	{
		//private Socket _socket;
		//private byte[] _buffer = new byte[1500];
		private SerialIORxHandler _rxHandler;
		private SerialIOErrorHandler _errorHandler;
		private IPEndPoint _sendEndPoint;

		private UdpClient _client = null;
		private System.Threading.Thread _readThread;

		public UdpSerialIO(IPEndPoint receiveEndpoint, IPEndPoint sendEndpoint, bool reuseAddress)
		{
			_sendEndPoint = sendEndpoint;
			//_socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
			//if (reuseAddress)
			//	_socket.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, 1);

			//_socket.Bind(receiveEndpoint);
			//_socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(Socket_EndReceive), null);
			int failCount = 20;
			while (failCount > 0)
			{
				try
				{
					_client = new UdpClient(receiveEndpoint);
					break;
				}
				catch (Exception ex)
				{
					if (--failCount == 0)
						throw ex;
					System.Threading.Thread.Sleep(500);
				}
			}

			_readThread = new System.Threading.Thread(new System.Threading.ThreadStart(ClientReadThread));
			_readThread.IsBackground = true;
			_readThread.Start();
		}

		private void ClientReadThread()
		{
			while (_client != null)
			{
				IPEndPoint remoteEP = new IPEndPoint(IPAddress.Any, 0);
				try
				{
					byte[] data = _client.Receive(ref remoteEP);
					if ((data != null) && (data.Length > 0))
					{
						if (_sendEndPoint.Equals(remoteEP))
							ReceiveData(data);

						//OnDataReceived(remoteEP, data);
					}
				}
				catch (SocketException socketEx)
				{
					if (socketEx.ErrorCode != (int)10004) //SocketError.Interrupted)
						if (_errorHandler != null)
							_errorHandler(this, socketEx);
				}
				catch (Exception ex)
				{
					//OnInformation(ex.ToString());
					if (_errorHandler != null)
						_errorHandler(this, ex);
				}
			}
		}

		//private void Socket_EndReceive(IAsyncResult ar)
		//{
		//    try
		//    {
		//        int count = _socket.EndReceive(ar);

		//        if (count > 0)
		//        {
		//            byte[] data = new byte[count];
		//            Array.Copy(_buffer, 0, data, 0, count);

		//            ReceiveData(data);
		//        }
		//    }
		//    catch (Exception ex)
		//    {
		//        if (_errorHandler != null)
		//            _errorHandler(this, ex);
		//    }
		//    finally
		//    {
		//        try
		//        {
		//            if (_socket != null)
		//                _socket.BeginReceive(_buffer, 0, _buffer.Length, SocketFlags.None, new AsyncCallback(Socket_EndReceive), null);
		//        }
		//        catch (Exception ex1)
		//        {
		//            if (_errorHandler != null)
		//                _errorHandler(this, ex1);
		//        }
		//    }
		//}

		public void TxBytes(byte[] data)
		{
			// _socket.Send(data);
			//_socket.SendTo(data, _sendEndPoint);
			_client.Send(data, data.Length, _sendEndPoint);
		}

		protected virtual void ReceiveData(byte[] data)
		{
			if (_rxHandler != null)
				_rxHandler(this, data);
		}

		public SerialIORxHandler RxHandler
		{
			get
			{
				return _rxHandler;
			}
			set
			{
				_rxHandler = value;
			}
		}

		public SerialIOErrorHandler ErrorHandler
		{
			get
			{
				return _errorHandler;
			}
			set
			{
				_errorHandler = value;
			}
		}

		public void Dispose()
		{
			if (_client != null)
				_client.Close();
			_client = null;
			//if (_socket != null)
			//    _socket.Close();
			//_socket = null;
		}
	}
}

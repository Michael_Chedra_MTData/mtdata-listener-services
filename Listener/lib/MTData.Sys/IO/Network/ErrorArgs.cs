using System;
using System.Net;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// Delegate describing format of error event.
	/// </summary>
	/// <param name="sender">Sender of event</param>
	/// <param name="args">Arguments identifying the exception</param>
	public delegate void ErrorDelegate(object sender, ErrorArgs args);

	/// <summary>
	/// Arguments class to hold the cause of the error
	/// </summary>
	public class ErrorArgs
	{
		/// <summary>
		/// Exception causing the error
		/// </summary>
		private Exception _cause;

		/// <summary>
		/// Message attached to the error.
		/// </summary>
		private string _message;

		/// <summary>
		/// constructor to instantiat ethe argument class
		/// </summary>
		/// <param name="cause">This is the exception causing the event to be raised</param>
		public ErrorArgs(string message, Exception cause)
		{
			_message = message;
			_cause = cause;
		}

		/// <summary>
		/// Message to describe source
		/// </summary>
		public string Message
		{
			get
			{
				return _message;
			}
		}
		/// <summary>
		/// Cause of the error
		/// </summary>
		public Exception Cause
		{
			get
			{
				return _cause;
			}
		}
	}
}

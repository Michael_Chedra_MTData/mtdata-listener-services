using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys;

namespace MTData.Sys.IO.Network
{
	public class NetworkEndPoint : MTData.Sys.Config.IConfigurable
	{
		private string _ipAddress = null;
		private int _port = 0;

		public NetworkEndPoint()
		{

		}

		public NetworkEndPoint(string ipAddress, int port)
		{
			_ipAddress = ipAddress;
			_port = port;
		}

		/// <summary>
		/// The IP Address.
		/// </summary>
		public string IPAddress
		{
			get
			{
				return _ipAddress;
			}
			set
			{
				_ipAddress = value;
			}
		}

		/// <summary>
		/// The Port Number involved.
		/// </summary>
		public int Port
		{
			get
			{
				return _port;
			}
			set
			{
				_port = value;
			}
		}

		public System.Net.IPEndPoint AsEndPoint
		{
			get
			{
				if (_ipAddress == "0.0.0.0")
					return new System.Net.IPEndPoint(System.Net.IPAddress.Parse(_ipAddress), _port);
				else
				{
#if !NETCF && !NETCF2
					System.Net.IPAddress[] addresses = System.Net.Dns.GetHostAddresses(_ipAddress);
					if (addresses.Length > 0)
						return new System.Net.IPEndPoint(addresses[0], _port);
					else
						return null;
#else
                    try
                    {
                        System.Net.IPHostEntry entry = System.Net.Dns.GetHostEntry(_ipAddress);
                        if (entry.AddressList.Length > 0)
                            return new System.Net.IPEndPoint(entry.AddressList[0], _port);
                        else
                            return null;
                    }
                    catch 
                    {
                        return null;
                    }
#endif
                }
			}
		}

		#region IConfigurable Members

		public void Configure(object context, MTData.Sys.Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			// Populate the standard properties
			Loader.Loader.PopulatePropertiesFromNode(this, configNode);

		}

		#endregion

		public override string ToString()
		{
			return string.Format("{0}:{1}", _ipAddress, _port);
		}
	}
}

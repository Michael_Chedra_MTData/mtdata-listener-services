using System;
using System.Net;
using System.Net.Sockets;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// This class will be responsible for udp multicast communications.
	/// </summary>
	public class UdpMulticastClient : UdpClient, IDisposable
	{
		/// <summary>
		/// Default multicast address to join through constructor
		/// </summary>
		private IPEndPoint _multiCastEndPoint = new IPEndPoint(IPAddress.Any, 0);

		/// <summary>
		/// Constructor to prepare the multicast address and port.
		/// </summary>
		/// <param name="multiCastEndPoint">Address of MultiCast</param>
		/// <param name="timeToLive">Time to Live for the udp packet in router hops</param>
		public UdpMulticastClient(IPEndPoint multiCastEndPoint, int timeToLive)
			: this(multiCastEndPoint.Port)
		{
			_multiCastEndPoint = multiCastEndPoint;
			JoinMulticastGroup(multiCastEndPoint.Address, timeToLive);
		}

		/// <summary>
		/// Standard constructor for listening to the specified port number
		/// </summary>
		/// <param name="port">Port Number to listen to</param>
		public UdpMulticastClient(int port)
			: base(port)
		{
			this.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.Broadcast, 1);
		}

#if !NETCF && !NETCF2

		/// <summary>
		/// dispose method to clean up the multicast registration
		/// </summary>
		/// <param name="disposing">Indicates whtehr it is actually being disposed</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing)
			{
				if (_multiCastEndPoint.Port != 0)
					DropMulticastGroup(_multiCastEndPoint.Address);
			}
			base.Dispose(disposing);
		}

#endif

		#region IDisposable Members

		void IDisposable.Dispose()
		{
			if (_multiCastEndPoint.Port != 0)
				DropMulticastGroup(_multiCastEndPoint.Address);
		}

		#endregion
	}
}

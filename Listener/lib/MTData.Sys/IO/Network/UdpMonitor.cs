using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Network
{
	/// <summary>
	/// This class will take a UdpClient in its constructor, and will
	/// control access to, and error/receipt monitoring for that client.
	/// </summary>
	public class UdpMonitor : IDisposable
	{
		/// <summary>
		/// Client controlled by this class
		/// </summary>
		private UdpClient _client = null;

		/// <summary>
		/// Thread to control the receipt of data
		/// </summary>
		private Thread _listener = null;

		/// <summary>
		/// This indicates when the class has been disposed.
		/// </summary>
		private bool _disposed = false;

		/// <summary>
		/// This event is raised when data is received.
		/// </summary>
		public event ReceiveDelegate Received;

		/// <summary>
		/// Event raised whenever an asynchronous event is raised.
		/// </summary>
		public event ErrorDelegate Error;

		/// <summary>
		/// Constructor to initialise the class.
		/// </summary>
		/// <param name="client">Underlying UdpClient to monitor</param>
		public UdpMonitor(UdpClient client)
		{
			_client = client;
		}

		/// <summary>
		/// Start the class listening to the client socket.
		/// </summary>
		public void Start()
		{
			if (_disposed)
				throw new ObjectDisposedException("UdpMonitor");

			_listener = new Thread(new ThreadStart(ThreadListener));
			_listener.IsBackground = true;
			_listener.Start();
		}

		/// <summary>
		/// This function controls the calling of the Received event
		/// </summary>
		/// <param name="endPoint">Source of the data</param>
		/// <param name="data">Data received</param>
		protected virtual void OnReceived(IPEndPoint endPoint, byte[] data)
		{
			if (Received != null)
				Received(this, new ReceiveArgs(endPoint, data));
		}

		/// <summary>
		/// Method controlling the raising of the Error event
		/// </summary>
		/// <param name="message">Text describing source</param>
		/// <param name="ex">Exception that occurred</param>
		protected virtual void OnError(string message, Exception ex)
		{
			if (Error != null)
				Error(this, new ErrorArgs(message, ex));
		}

		/// <summary>
		/// This method will control listening to the underlying client for
		/// inbound data
		/// </summary>
		private void ThreadListener()
		{
			while (!_disposed)
			{
				IPEndPoint remoteEndpoint = new IPEndPoint(IPAddress.Any, 0);
				try
				{
					byte[] data = _client.Receive(ref remoteEndpoint);
					if (data != null)
						OnReceived(remoteEndpoint, data);
				}
				catch (Exception ex)
				{
					OnError("UdpMonitor::threadListener", ex);
				}
			}
		}

		#region IDisposable Members

		/// <summary>
		/// Dispose of the class and close the underlying udpClient
		/// </summary>
		public void Dispose()
		{
			if (_disposed)
				throw new ObjectDisposedException("UdpHandler");
			_disposed = true;
			_client.Close();
		}

		#endregion
	}
}

using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;
using System.Threading;

namespace MTData.Sys.IO
{
	/// <summary>
	/// Summary description for SharedMemory.
	/// </summary>
	public class SharedMemoryStream : Stream
	{
		private int			_offset;
		private int			_size;
		private string		_name;
		private IntPtr		_handle;
		private static uint _systemInfoAllocationGranularity;
		
		static SharedMemoryStream()
		{
			_systemInfoAllocationGranularity = Win32.SystemInfo.dwAllocationGranularity;
		}

		public static SharedMemoryStream FromFile(string fileName)
		{

			FileInfo fi = new FileInfo(fileName);
			SharedMemoryStream stream = new SharedMemoryStream(fileName, (int) fi.Length);
			
			byte[] buffer = new byte[_systemInfoAllocationGranularity];
			FileStream fs = fi.OpenRead();
			try
			{
				int count;
				do
				{
					count = fs.Read(buffer, 0, buffer.Length);
					if (count > 0)
						stream.Write(buffer, 0, count);
				} while (count > 0);
			}
			finally
			{
				fs.Close();
			}
			return stream;
		}

		public SharedMemoryStream(string name, int size)
		{
			_name			= name;
			_handle	= Win32.CreateFileMapping(Win32.INVALID_HANDLE_VALUE, 0, Win32.PAGE_READWRITE, 0, (uint)size, "MTData.Shared." + name);
			if (_handle == IntPtr.Zero)
				throw new OutOfMemoryException("CreateFileMapping file with length = " + size.ToString());
			_size			= size;
		}

		public string Name
		{
			get { return _name; }
		}

		public override void Close()
		{
			if (_handle != IntPtr.Zero)
			{
				Win32.CloseHandle(_handle);
				_handle = IntPtr.Zero;
			}
			base.Close();
		}

		public override void Flush()
		{
			// Nothing to do			
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			switch(origin)
			{
				case SeekOrigin.Begin:
					_offset = (int) offset;
					break;

				case SeekOrigin.Current:
					_offset += (int)offset;
					break;

				case SeekOrigin.End:
					_offset = (int) (_size - 1 - offset);
					break;
			}
			return Position;
		}

		public override void SetLength(long value)
		{
			throw new NotSupportedException("Cannot SetLength on SharedMemoryStream");
		}

		public object Read(Type type)
		{
			int readCount = Marshal.SizeOf(type);
			if (readCount + Position > Length)
				readCount = (int) (Length - Position);

			IntPtr _ptr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint) _offset, (uint) readCount); 
			if (_ptr == IntPtr.Zero)
				throw new OutOfMemoryException("Unable to MapViewOfFile for Read of " + readCount.ToString() + " bytes");
			try
			{
				return Marshal.PtrToStructure(_ptr, type);
			}
			finally
			{
				Win32.UnmapViewOfFile(_ptr);
				Seek(readCount, SeekOrigin.Current);
			}
		}

		public int Read(IntPtr buffer, int count)
		{
			int readCount = count;
			if (readCount + Position > Length)
				readCount = (int) (Length - Position);

			IntPtr _ptr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint) _offset, (uint) readCount); 
			if (_ptr == IntPtr.Zero)
				throw new OutOfMemoryException("Unable to MapViewOfFile for Read of " + readCount.ToString() + " bytes");
			try
			{
				Win32.RtlMoveMemory(buffer, _ptr, (uint) readCount);
			}
			finally
			{
				Win32.UnmapViewOfFile(_ptr);
			}
			Seek(readCount, SeekOrigin.Current);
			return readCount;
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
//			Debug.WriteLine(string.Format("{0} BeginRead : {1}, {2}", Thread.CurrentThread.GetHashCode(), _offset, count));
			int readCount = count;
			if (readCount + Position > Length)
				readCount = (int) (Length - Position);

			int totalReadCount	= readCount;
			while (readCount != 0)
			{
				int srcOffset	= (int) (_offset % _systemInfoAllocationGranularity);
				int srcPage		= (int) _offset - srcOffset;
				int srcSize = (int)System.Math.Min(srcOffset + readCount, _systemInfoAllocationGranularity);
				int readSize	= srcSize - srcOffset;
				IntPtr ptr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint)srcPage, (uint)srcSize); 
//				Debug.WriteLine(string.Format("{0} PartialRead : {1}, {2}, {3}, {4}, {5}, {6}", Thread.CurrentThread.GetHashCode(), _offset, srcOffset, srcPage, readSize, srcSize, ptr));
				if (ptr == IntPtr.Zero)
					throw new OutOfMemoryException("Unable to MapViewOfFile for Read of " + readCount.ToString() + " bytes");
				try
				{
					IntPtr pagePtr = (IntPtr) (ptr.ToInt32() + srcOffset);
					Marshal.Copy(pagePtr, buffer, offset, readSize);
				}
				finally
				{
					Win32.UnmapViewOfFile(ptr);
				}
				readCount -= readSize;
				_offset   += readSize;
				offset    += readSize;
			}
			return totalReadCount;
		}

		public void Write(object o)
		{
			int writeCount = Marshal.SizeOf(o);
			if (writeCount + Position > Length)
				writeCount = (int) (Length - Position);
			
			IntPtr _ptr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint) _offset, (uint) writeCount); 
			if (_ptr == IntPtr.Zero)
				throw new OutOfMemoryException("Unable to MapViewOfFile for Write of " + writeCount.ToString() + " bytes");
			try
			{
				Marshal.StructureToPtr(o, _ptr, false);
			}
			finally
			{
				Win32.UnmapViewOfFile(_ptr);
			}
			Seek(writeCount, SeekOrigin.Current);
		}

		public void Write(IntPtr ptr, int count)
		{
			int writeCount = count;
			if (writeCount + Position > Length)
				writeCount = (int) (Length - Position);

			IntPtr _ptr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint) _offset, (uint) writeCount); 
			if (_ptr == IntPtr.Zero)
				throw new OutOfMemoryException("Unable to MapViewOfFile for Write of " + writeCount.ToString() + " bytes");
			try
			{
				Win32.RtlMoveMemory(_ptr, ptr, (uint) count);
			}
			finally
			{
				Win32.UnmapViewOfFile(_ptr);
			}
			Seek(writeCount, SeekOrigin.Current);
		}


		public override void Write(byte[] buffer, int offset, int count)
		{
			int writeCount = count;
			if (writeCount + Position > Length)
				writeCount = (int) (Length - Position);

			IntPtr _ptr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint) _offset, (uint) writeCount); 
			if (_ptr == IntPtr.Zero)
				throw new OutOfMemoryException("Unable to MapViewOfFile for Write of " + writeCount.ToString() + " bytes");
			try
			{
				Marshal.Copy(buffer, offset, _ptr, writeCount);
			}
			finally
			{
				Win32.UnmapViewOfFile(_ptr);
			}
			Seek(writeCount, SeekOrigin.Current);
		}

		public override bool CanRead
		{
			get { return true; }
		}

		public override bool CanSeek
		{
			get { return true; }
		}

		public override bool CanWrite
		{
			get { return true; }
		}

		public override long Length
		{
			get { return _size; }
		}

		public override long Position
		{
			get { return _offset; }
			set
			{
				if ((value < 0) || (value >= _size))
					throw new IndexOutOfRangeException("Position is < 0 or > Size");

				_offset = (int) value;
			}
		}

		public IntPtr Lock()
		{
			IntPtr lockPtr = Win32.MapViewOfFile(_handle, Win32.FILE_MAP_ALL_ACCESS, 0, (uint) _offset, (uint) _size); 
			if (lockPtr == IntPtr.Zero)
				throw new OutOfMemoryException("Unable to MapViewOfFile for Lock of " + _size.ToString() + " bytes");

			return lockPtr;
		}

		public void Unlock(IntPtr lockPtr)
		{
			Win32.UnmapViewOfFile(lockPtr);
		}

		public IntPtr FileMappingHandle
		{
			get { return _handle; }
		}
	}
}

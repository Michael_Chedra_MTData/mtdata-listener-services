#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO
{
	/// <summary>
	/// This class will iterate an array of data until it finds the EndOfLine character, 
	/// and it will then return the data appropriately split.
	/// </summary>
	public class DataLineParser
	{
		/// <summary>
		/// This is the list of markers that are used for end of line indicators.
		/// Any one of these will count as an End-Of-Line
		/// </summary>
		private byte[] _endOfLineMarkers = null;
		
		/// <summary>
		/// This is the overflow from the last amount of data passed.
		/// </summary>
		private MemoryStream _overflow = new MemoryStream();

		/// <summary>
		/// Prepare the class for parsing the inbound data
		/// </summary>
		/// <param name="endOfLineMarkers"></param>
		public DataLineParser(byte[] endOfLineMarkers)
		{
			_endOfLineMarkers = endOfLineMarkers;
		}

		/// <summary>
		/// Take the bytes read in, and split them into lines of data.
		/// </summary>
		/// <param name="data"></param>
		/// <returns></returns>
		public List<byte[]> Parse(byte[] data)
		{
			return Parse(data, 0, data.Length);
		}

		/// <summary>
		/// This will take the data provided from the specified offset, with the specified length,
		/// and process it against the data it already has spare.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="offset"></param>
		/// <param name="count"></param>
		/// <returns></returns>
		public List<byte[]> Parse(byte[] data, int offset, int count)
		{
			List<byte[]> result = new List<byte[]>();
			for (int loop = 0; loop < count; loop++)
			{
				byte dataByte = data[loop + offset];
				bool eolFound = false;
				foreach (byte eolByte in _endOfLineMarkers)
					if (eolByte == dataByte)
					{
						eolFound = true;
						break;
					}

				if (eolFound)
				{
					//	End of line found..
					//	take the overflow at this time, and copy it into an output data array..
					if (_overflow.Length > 0)
					{
						byte[] resultLine = _overflow.ToArray();
						result.Add(resultLine);
						Reset();
					}
				}
				else
					_overflow.Write(data, loop + offset, 1);
			}
			return result;
		}

		/// <summary>
		/// This will reset the buffer
		/// </summary>
		public void Reset()
		{
			_overflow.Close();
			_overflow = new MemoryStream();
		}
	}
}

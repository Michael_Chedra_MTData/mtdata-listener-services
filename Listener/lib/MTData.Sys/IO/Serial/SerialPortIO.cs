#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Serial
{
	public class SerialPortIO : ISerialIO
	{
		private SerialIORxHandler _rxHandler = null;
		private SerialIOErrorHandler _errorHandler = null;

		private System.IO.Ports.SerialPort _serialPort;

		public SerialPortIO(int comPort, int baudRate, System.IO.Ports.Parity parity, int dataBits, System.IO.Ports.StopBits stopBits)
		{
			_serialPort = new System.IO.Ports.SerialPort(string.Format("COM{0}", comPort), baudRate, parity, dataBits, stopBits);
			_serialPort.DataReceived += new System.IO.Ports.SerialDataReceivedEventHandler(_serialPort_DataReceived);
			_serialPort.ErrorReceived += new System.IO.Ports.SerialErrorReceivedEventHandler(_serialPort_ErrorReceived);
			_serialPort.Open();
		}

		void _serialPort_ErrorReceived(object sender, System.IO.Ports.SerialErrorReceivedEventArgs e)
		{
			if (_errorHandler != null)
			{
				_errorHandler(this, new Exception("SerialIO Error : " + e.EventType.ToString()));
			}
		}

		void _serialPort_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
		{
			if (_rxHandler != null)
			{
				switch (e.EventType)
				{
					case System.IO.Ports.SerialData.Chars:
					case System.IO.Ports.SerialData.Eof:
						{
							int count = _serialPort.BytesToRead;
							if (count > 0)
							{
								byte[] buffer = new byte[count];
								_serialPort.Read(buffer, 0, count);
								_rxHandler(this, buffer);
							}
							break;
						}

				}
			}
		}

		#region ISerialIO Members

		public void TxBytes(byte[] data)
		{
			_serialPort.Write(data, 0, data.Length);
		}

		public SerialIORxHandler RxHandler
		{
			get
			{
				return _rxHandler;
			}
			set
			{
				_rxHandler = value;
			}
		}

		public SerialIOErrorHandler ErrorHandler
		{
			get
			{
				return _errorHandler;
			}
			set
			{
				_errorHandler = value;
			}
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			if (_serialPort != null)
				_serialPort.Close();
			_serialPort = null;
		}

		#endregion
	}
}

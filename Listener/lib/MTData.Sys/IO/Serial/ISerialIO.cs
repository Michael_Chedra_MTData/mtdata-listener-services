﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO.Serial
{
    public delegate void SerialIORxHandler(ISerialIO sender, byte[] data);

    public delegate void SerialIOErrorHandler(ISerialIO sender, Exception ex);

    public interface ISerialIO : IDisposable
    {
        void TxBytes(byte[] data);
        SerialIORxHandler RxHandler { get; set; }
        SerialIOErrorHandler ErrorHandler { get; set; }
    }
}

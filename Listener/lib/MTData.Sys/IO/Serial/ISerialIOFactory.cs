﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;

namespace MTData.Sys.IO.Serial
{
    public interface ISerialIOFactory
    {
        ISerialIO Create(IConfigSection configSection, object context);
    }

    public interface ISerialIOFactoryEx
    {
        ISerialIO Create();
    }

    /// <summary>
    /// This class will wrap a ISerialIOFactory interface providing a parameterless Create if required.
    /// </summary>
    public class SerialIOFactoryWrapper : ISerialIOFactoryEx
    {
        private ISerialIOFactory _factory;
        private IConfigSection _configSection;
        private object _context;

        public SerialIOFactoryWrapper(ISerialIOFactory factory, IConfigSection configSection, object context)
        {
            _factory = factory;
            _configSection = configSection;
            _context = context;
        }
        #region ISerialIOFactoryEx Members

        public ISerialIO Create()
        {
            return _factory.Create(_configSection, _context);
        }

        #endregion

    }
}

#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;
using MTData.Sys.Loader;
using System.IO.Ports;


namespace MTData.Sys.IO.Serial
{
    public class SerialIOFactory : ISerialIOFactory, ISerialIOFactoryEx, IConfigurable
    {
        #region ISerialIOFactory Members

        public ISerialIO Create(IConfigSection configSection, object context)
        {
            int portNumber = Int32.Parse(configSection["PortNumber", "2"]);
            int baudRate = Int32.Parse(configSection["BaudRate", "9600"]);
            byte dataSize = Byte.Parse(configSection["DataSize", "8"]);
            string stopBitsStr = configSection["StopBits", "1"];
            string parityStr = configSection["Parity", "None"];

            return CreateSerialPort(portNumber, baudRate, dataSize, stopBitsStr, parityStr);
        }

        private ISerialIO CreateSerialPort(int portNumber, int baudRate, byte dataSize, string stopBitsStr, string parityStr)
        {
            System.IO.Ports.StopBits stopBits = System.IO.Ports.StopBits.One;
            if (Enum.IsDefined(typeof(System.IO.Ports.StopBits), stopBitsStr))
                stopBits = (System.IO.Ports.StopBits)Enum.Parse(typeof(System.IO.Ports.StopBits), stopBitsStr, true);

            System.IO.Ports.Parity parity = System.IO.Ports.Parity.None;
            if (Enum.IsDefined(typeof(System.IO.Ports.Parity), parityStr))
                parity = (System.IO.Ports.Parity)Enum.Parse(typeof(System.IO.Ports.Parity), parityStr, true);

            return new SerialPortIO(portNumber, baudRate, parity, dataSize, stopBits);
        }

        #endregion

        #region Private Instance Data

        private int _comPort;
        private int _baudRate;
        private string _parityStr;
        private byte _dataSize;
        private string _stopBitsStr;

        #endregion

        #region Public Instance Data

        /// <summary>
        /// Some work off of portnumber, others off of ComPort.. same thing..
        /// </summary>
        public int PortNumber
        {
            get
            {
                return _comPort;
            }
            set
            {
                _comPort = value;
            }
        }

        public int ComPort
        {
            get
            {
                return _comPort;
            }
            set
            {
                _comPort = value;
            }
        }

        public int BaudRate
        {
            get
            {
                return _baudRate;
            }
            set
            {
                _baudRate = value;
            }
        }

        public string Parity
        {
            get
            {
                return _parityStr;
            }
            set
            {
                _parityStr = value;
            }
        }

        public byte DataSize
        {
            get
            {
                return _dataSize;
            }
            set
            {
                _dataSize = value;
            }
        }

        public string StopBits
        {
            get
            {
                return _stopBitsStr;
            }
            set
            {
                _stopBitsStr = value;
            }
        }

        #endregion

        public SerialIOFactory()
        {

        }

        public SerialIOFactory(int comPort, int baudRate, string parityStr, byte dataSize, string stopBitsStr)
        {
            _comPort = comPort;
            _baudRate = baudRate;
            _parityStr = parityStr;
            _dataSize = dataSize;
            _stopBitsStr = stopBitsStr;
        }

        #region ISerialIOFactoryEx Members

        public ISerialIO Create()
        {
            return CreateSerialPort(_comPort, _baudRate, _dataSize, _stopBitsStr, _parityStr);
        }

        #endregion

        #region IConfigurable Members

        public void Configure(object context, Config.Config config, string path, System.Xml.XmlNode configNode)
        {
            Loader.Loader.PopulatePropertiesFromNode(this, configNode);
        }

        #endregion

        public override string ToString()
        {
            System.Text.StringBuilder builder = new StringBuilder();
            builder.Append(this.GetType().FullName);
            builder.Append(":ComPort : ");
            builder.Append(_comPort);
            builder.Append(", BaudRate : ");
            builder.Append(_baudRate);
            builder.Append(", Parity : ");
            builder.Append(_parityStr);
            builder.Append(", DataSize : ");
            builder.Append(_dataSize);
            builder.Append(", StopBits : ");
            builder.Append(_stopBitsStr);

            return builder.ToString();
        }
    }
}

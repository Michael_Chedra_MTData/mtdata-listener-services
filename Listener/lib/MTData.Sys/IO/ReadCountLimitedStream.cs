using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.IO
{
	/// <summary>
	/// This class will wrap another stream, and provide a fixed length of bytes read.
	/// </summary>
	public class ReadCountLimitedStream : Stream
	{
		private long _maxLength = 0;
		private long _startPosition = 0;
		private long _relativePosition = 0;
		private Stream _source = null;
		private object _context = null;
		private bool _closed = false;

		public delegate void ClosedDelegate(ReadCountLimitedStream stream, object context);

		public event ClosedDelegate Closed;

		private void OnClose()
		{
			if (Closed != null)
				Closed(this, _context);
		}

		public ReadCountLimitedStream(Stream source, long maxLength, object context) : base()
		{
			_maxLength = maxLength;
			_source = source;
			if (source.CanSeek)
				_startPosition = source.Position;
			else
				_startPosition = 0;
			_relativePosition = 0;
			_context = context;
		}

		public ReadCountLimitedStream(Stream source, long maxLength) : this(source, maxLength, null)
		{
			
		}

		public override bool CanRead
		{
			get
			{
				return _source.CanRead;
			}
		}

		public override bool CanWrite
		{
			get
			{
				return false;
			}
		}

		public override bool CanSeek
		{
			get
			{
				return _source.CanSeek;
			}
		}

		public override bool CanTimeout
		{
			get
			{
				return _source.CanTimeout;
			}
		}

		public override void Close()
		{
			_source.Close();
			OnClose();
			_closed = true;
		}

		protected override void Dispose(bool disposing)
		{
			if (!_closed)
				Close();
			base.Dispose(disposing);
			if (_source is IDisposable)
				((IDisposable)_source).Dispose();
		}

		public override void SetLength(long value)
		{
			//	this does not do anything.
		}

		public override long Seek(long offset, SeekOrigin origin)
		{
			if (CanSeek)
			{
				switch (origin)
				{
					case SeekOrigin.Begin:
						Position = offset;
						break;
					case SeekOrigin.Current:
						Position += offset;
						break;
					case SeekOrigin.End:
						Position = (_maxLength + offset);
						break;
				}
			}
			return Position;
		}

		public override void Flush()
		{
			_source.Flush();
		}

		public override long Length
		{
			get
			{
				return _maxLength;
			}
		}

		public override long Position
		{
			get
			{
				return _relativePosition;
			}
			set
			{
				if (!_source.CanSeek)
					throw new NotSupportedException("Cannot set position");
				//	position = 0 => source.Position = _startingPosition
				long targetPosition = value;
				if (value > _maxLength)
					targetPosition = _maxLength;
				if (targetPosition < 0)
					targetPosition = 0;

				_relativePosition = targetPosition;
				_source.Position = targetPosition + _startPosition;
			}
		}

		public override int Read(byte[] buffer, int offset, int count)
		{
			if ((_relativePosition + count) > _maxLength)
				count = (int)(_maxLength - _relativePosition);
			
			int readCount = _source.Read(buffer, offset, count);
			_relativePosition += readCount;
			return readCount;
		}

		public override int ReadByte()
		{
			if (_relativePosition >= _maxLength)
				return -1;

			_relativePosition++;
			return _source.ReadByte();
		}

		//public override IAsyncResult BeginRead(byte[] buffer, int offset, int count, AsyncCallback callback, object state)
		//{
		//    if ((_relativePosition + count) > _maxLength)
		//        count = (_maxLength - _relativePosition);
			
		//    return _source.BeginRead(buffer, offset, count, callback, state);
		//}

		//public override int EndRead(IAsyncResult asyncResult)
		//{
		//    int readCount = _source.EndRead(asyncResult);
		//    _relativePosition += readCount;
		//    return readCount;
		//}

		public override void Write(byte[] buffer, int offset, int count)
		{
			throw new NotSupportedException("Write functonality is not supported in this class");
		}
	}
}

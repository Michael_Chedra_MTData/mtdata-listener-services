#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System.IO;
using System.Reflection;

namespace MTData.Sys.IO
{
	public class SatelliteFile
	{
		private static string _applicationPath;

		static SatelliteFile()
		{
			_applicationPath = Path.GetDirectoryName(Assembly.GetExecutingAssembly().GetName().CodeBase);
			if (_applicationPath.StartsWith(@"file:\"))
				_applicationPath = _applicationPath.Remove(0, 6);
		}

		public static string ResolveFileName(string fileName)
		{
			if (fileName == null)
				return null;

			if (!Path.IsPathRooted(fileName))
				fileName = Path.Combine(_applicationPath, fileName);
			
			return fileName;
		}
	}
}

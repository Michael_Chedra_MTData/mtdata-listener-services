﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

namespace MTData.Sys.IO
{
    public class UnescapingBuffer : ExpandingByteBuffer
    {
        private byte FrameByte;
        private byte EscapeByte;
        private byte EscapeXor;

        private bool _escaping = false;
        private bool _isComplete = false;

        public delegate void DataEvent(byte[] data);
        public event DataEvent UnescapeComplete;

        public UnescapingBuffer(int initialSize, byte frameByte, byte escapeByte, byte escapeXor)
            : base(initialSize)
        {
            FrameByte = frameByte;
            EscapeByte = escapeByte;
            EscapeXor = escapeXor;
        }

        public UnescapingBuffer()
            : this(0, 0x7E, 0x1B, 0x20)
        {
        }

        public override void Reset()
        {
            try
            {
                if (_size > 0)
                    raiseUnescapeComplete();
            }
            finally
            {
                base.Reset();
            }
        }

        private void raiseUnescapeComplete()
        {
            _isComplete = true;
            if (UnescapeComplete != null)
                UnescapeComplete(TrimmedBuffer);
        }

        public override void Add(byte b)
        {
            _isComplete = false;
            if (b == FrameByte)
                Reset();

            else if (_escaping)
            {
                _escaping = false;
                base.Add((byte)(b ^ EscapeXor));
            }
            else if (b == EscapeByte)
                _escaping = true;

            else
                base.Add(b);
        }

        public override void Add(byte[] bytes, int offset, int count)
        {
            for (int i = offset; i < offset + count; i++)
                Add(bytes[i]);
        }

        public override void Add(byte[] bytes, int count)
        {
            Add(bytes, 0, count);
        }

        public override void Add(byte[] bytes)
        {
            Add(bytes, 0, bytes.Length);
        }

        public bool IsComplete
        {
            get { return _isComplete; }
        }
    }

}

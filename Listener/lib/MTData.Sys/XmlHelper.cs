using System;
using System.Xml;

namespace MTData.Sys
{
	/// <summary>
	/// Summary description for XmlHelper.
	/// </summary>
	public class XmlHelper
	{
		private XmlHelper()
		{
			//
			// TODO: Add constructor logic here
			//
		}

		public static string GetAttribute(XmlNode node, string attributeName, string defaultValue)
		{
			XmlAttribute attribute = node.Attributes[attributeName];
			if (attribute == null)
				return defaultValue;
			return attribute.Value;
		}

		public static int GetAttribute(XmlNode node, string attributeName, int defaultValue)
		{
			XmlAttribute attribute = node.Attributes[attributeName];
			if (attribute == null)
				return defaultValue;
			
			try
			{
				return int.Parse(attribute.Value);				
			}
			catch(Exception )
			{
				return defaultValue;
			}
		}

		public static bool GetAttribute(XmlNode node, string attributeName, bool defaultValue)
		{
			XmlAttribute attribute = node.Attributes[attributeName];
			if (attribute == null)
				return defaultValue;
			
			try
			{
				return bool.Parse(attribute.Value);				
			}
			catch(Exception )
			{
				return defaultValue;
			}
		}

		public static decimal GetAttribute(XmlNode node, string attributeName, decimal defaultValue)
		{
			XmlAttribute attribute = node.Attributes[attributeName];
			if (attribute == null)
				return defaultValue;
			
			try
			{
				return decimal.Parse(attribute.Value);				
			}
			catch(Exception )
			{
				return defaultValue;
			}
		}

		public static double GetAttribute(XmlNode node, string attributeName, double defaultValue)
		{
			XmlAttribute attribute = node.Attributes[attributeName];
			if (attribute == null)
				return defaultValue;
			
			try
			{
				return double.Parse(attribute.Value);				
			}
			catch(Exception )
			{
				return defaultValue;
			}
		}


	}
}

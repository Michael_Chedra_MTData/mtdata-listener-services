using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys
{
	public interface ISetSelection
	{
		object Selection { get; set; }
	}
}

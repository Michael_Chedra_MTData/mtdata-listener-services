using System;
using System.Globalization;
using System.Text;
#if NETCF
using OpenNETCF.Security.Cryptography;
#else
using System.Security.Cryptography;
#endif

namespace MTData.Sys.Encryption
{
	/// <summary>
	/// Summary description for MaliciousChangeProtection.
	/// </summary>
	public class MaliciousChangeProtection
	{
		private const string					_rsaPublicKey = "<RSAKeyValue><Modulus>rKNzHA7tD3E5j+oduLRalrKJQJCgpkm7J39VjyqEVzS81KmNxirwHW4t6HPozQvKkZyNnEffSgdp/22RPoDjLMp+hvbAgEIyTnUImFGYoaxj33GSWZWhFoqCcHT3Rcby8GiSaGvOCJAEc9MkExNFFet8jjxzRk9MsMSGZeOTWJM=</Modulus><Exponent>AQAB</Exponent></RSAKeyValue>";
		private static RSACryptoServiceProvider	_rsa;

		private MaliciousChangeProtection()
		{
		}

		private static RSACryptoServiceProvider RSA
		{
			get
			{
				if (_rsa == null)
				{
					lock(typeof(MaliciousChangeProtection))
					{
						if (_rsa == null)
						{
							_rsa = new RSACryptoServiceProvider();
							_rsa.FromXmlString(_rsaPublicKey);
						}
					}
				}
				return _rsa;
			}
		}

		public static bool IsUnchanged(string text, string MD5Hash)
		{
			return IsUnchanged(UnicodeEncoding.Unicode.GetBytes(text), GetMD5Bytes(MD5Hash));
		}

		private static byte[] GetMD5Bytes(string md5Hash)
		{
			byte[] encryptedHash = new byte[md5Hash.Length / 2];
			for(int i = 0 ; i < encryptedHash.Length ; i++)
				encryptedHash[i] = byte.Parse(md5Hash.Substring(i * 2, 2), NumberStyles.HexNumber);
			return encryptedHash;
		}

		public static bool IsUnchanged(byte[] data, byte[] MD5Hash)
		{
			return RSA.VerifyData(data, new MD5CryptoServiceProvider(), MD5Hash);
		}
	}
}

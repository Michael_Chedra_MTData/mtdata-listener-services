using System;
using System.Collections;

namespace MTData.Sys
{
	/// <summary>
	/// Summary description for FriendlyFormatter.
	/// </summary>
	public class FriendlyFormatter : IFormatProvider, ICustomFormatter
	{
		private static Hashtable _formatReplacers = new Hashtable();

		public static void AddFormatReplacer(string key, string replace)
		{
			_formatReplacers[key] = replace;
		}

		public object GetFormat(Type formatType)
		{
			if (formatType == typeof(ICustomFormatter))
				return this;
			else
				return null;
		}

        private string pluralise(int count, string text)
        {
            switch (count)
            {
                case 0:
                    return string.Empty;

                case 1:
                    return "1 " + text;

                default:
                    return count.ToString() + " " + text + "s";
            }
        }

		public string Format(string format, object arg, IFormatProvider formatProvider)
		{
            try
            {
                switch (format)
                {
                    case "distance":
                        {
                            Int64 metres = Convert.ToInt64(arg);

                            if (metres < 100)
                                return string.Format("{0} Metres", metres);
                            else
                                return string.Format("{0:0.0} Kms", metres / 1000M);
                        }

                    case "seconds_duration_short":
                        {
                            TimeSpan sp = TimeSpan.MinValue;
                            if (arg is TimeSpan)
                                sp = (TimeSpan)arg;
                            else
                            {
                                Int64 v = Convert.ToInt64(arg);
                                sp = new TimeSpan(v * TimeSpan.TicksPerSecond);
                            }

                            if ((int)sp.TotalDays > 0)
                                return string.Format("{0:0} days {1:00}:{2:00}:{3:00}", (int)sp.TotalDays, sp.Hours, sp.Minutes, sp.Seconds);
                            else
                                return string.Format("{1:00}:{2:00}:{3:00}", (int)sp.TotalDays, sp.Hours, sp.Minutes, sp.Seconds);
                        }

                    case "seconds_duration":
                        {
                            TimeSpan sp = TimeSpan.MinValue;
                            if (arg is TimeSpan)
                                sp = (TimeSpan)arg;
                            else
                            {
                                Int64 v = Convert.ToInt64(arg);
                                sp = new TimeSpan(v * TimeSpan.TicksPerSecond);
                            }

                            return string.Format("{0} {1} {2}", pluralise((int)sp.TotalHours, "Hour"), pluralise(sp.Minutes, "Minute"), pluralise(sp.Seconds, "Second"));
                        }

                    case "minutes_duration":
                        {
                            TimeSpan sp = TimeSpan.MinValue;
                            if (arg is TimeSpan)
                                sp = (TimeSpan)arg;
                            else
                            {
                                Int64 v = Convert.ToInt64(arg);
                                sp = new TimeSpan(v * TimeSpan.TicksPerMinute);
                            }

                            return string.Format("{0} {1}", pluralise((int)sp.TotalHours, "Hour"), pluralise(sp.Minutes, "Minute"));
                        }

                    case "OnOff":
                        {
                            bool on = Convert.ToBoolean(arg);
                            if (on)
                                return "On";
                            else
                                return "Off";
                        }

                    default:
                        {
                            foreach (string key in _formatReplacers.Keys)
                                if (format == key)
                                    format = (string)_formatReplacers[key];

                            IFormattable formattable = arg as IFormattable;
                            if (formattable != null)
                                return formattable.ToString(format, formatProvider);
                            else
                                return arg.ToString();
                        }
                }
            }
            catch
            {
                return string.Empty;
            }
		}
	}	
}

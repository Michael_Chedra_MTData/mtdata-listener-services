using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

namespace MTData.Sys
{
	/// <summary>
	/// A Typed CookieTimer is to allow a single timer object to respond
	/// at specified time intervals, and raise an event, returning
	/// a cookie that was provided when specifying a timer event.
	/// </summary>
    public class TypedCookieTimer<KeyType, DataType> : IDisposable
	{
        public delegate void CookieTimerEvent(KeyType cookie, DataType data);
		public event CookieTimerEvent CookieTimedOut;

		protected class CookieExpireTime<KeyType, DataType>
		{
			public KeyType  Cookie;
            public DataType Data;
			public int		ExpireMilliseconds;

			public CookieExpireTime(KeyType cookie, DataType data, int expireMilliseconds)
			{
				Cookie		= cookie;
                Data        = data;
				ExpireMilliseconds	= expireMilliseconds;
			}

			public int MillisecondsBeforeExpiry(int compareTime)
			{
				return ExpireMilliseconds - compareTime;
			}
		}

		private Thread			_cookieTimerThread;
        protected Dictionary<KeyType, CookieExpireTime<KeyType, DataType>> _cookieTimers = new Dictionary<KeyType, CookieExpireTime<KeyType, DataType>>();
		private AutoResetEvent	_responseEvent;
		private bool			_disposed;
		protected object		_lockObj = new object();

		public TypedCookieTimer()
		{
			_responseEvent = new AutoResetEvent(false);
			_cookieTimerThread = new Thread(new ThreadStart(CookieTimerThread));
			_cookieTimerThread.IsBackground = true;
			_cookieTimerThread.Start();
		}

		private void CookieTimerThread()
		{
			do
			{
                CookieExpireTime<KeyType, DataType> nextExpireCookie = null;

				lock(_lockObj)
				{
					foreach (CookieExpireTime<KeyType, DataType> cookieExpireTime in _cookieTimers.Values)
					{
						if ((nextExpireCookie == null) || (cookieExpireTime.MillisecondsBeforeExpiry(nextExpireCookie.ExpireMilliseconds) < 0))
							nextExpireCookie = cookieExpireTime;
					}

					_responseEvent.Reset();
				}
			
				bool signalled = true;
				if (nextExpireCookie != null)
				{
					int waitTime   = nextExpireCookie.MillisecondsBeforeExpiry(System.Environment.TickCount);
					if (waitTime > 0)
						signalled = _responseEvent.WaitOne(waitTime, false);
					else
						signalled = false;
				}
				else
					_responseEvent.WaitOne();

				if (!signalled)
				{
					// Remove the Timer first, this allows the CookieTimeOut event to add it back if necessary
					lock(_lockObj)
						_cookieTimers.Remove(nextExpireCookie.Cookie);

					try
					{
						if (CookieTimedOut != null)
							CookieTimedOut(nextExpireCookie.Cookie, nextExpireCookie.Data);
					}
					catch (Exception ex)
					{
						string exMsg = "";
						if (ex.InnerException != null)
							exMsg = ex.InnerException.ToString();

						exMsg = string.Concat(exMsg, " | ", ex.ToString());

					}
				} 
			} while (!_disposed);
		}

		public void Dispose()
		{
			if (_disposed)
				return;

			_disposed = true;

			_responseEvent.Set();
		}

		/// <summary>
		/// Add a new item to the cookie timer list, with the
		/// specified time to elapse before raising an event, 
		/// in the case that the item does not get cleared.
		/// </summary>
		/// <param name="cookie"></param>
		/// <param name="data"></param>
		/// <param name="millisecondTimeOut"></param>
		public void Add(KeyType cookie, DataType data, int millisecondTimeOut)
		{
			lock(_lockObj)
			{
                _cookieTimers[cookie] = new CookieExpireTime<KeyType, DataType>(cookie, data, System.Environment.TickCount + millisecondTimeOut);
				_responseEvent.Set();
			}
		}

		/// <summary>
		/// Remove the specified index entry from the cookie timer list.
		/// </summary>
		/// <param name="cookie"></param>
		public void Remove(KeyType cookie)
		{
			lock(_lockObj)
			{
				if (_cookieTimers.ContainsKey(cookie))
				{
					_cookieTimers.Remove(cookie);
					_responseEvent.Set();
				}
			}
		}

		/// <summary>
		/// Return the number of cookies in the timer event list.
		/// </summary>
		public int Count
		{
			get { return _cookieTimers.Count; }
		}

		/// <summary>
		/// This method will indicate if a certain cookie is currently in use.
		/// </summary>
		/// <param name="cookie"></param>
		/// <returns></returns>
		public bool Contains(KeyType cookie)
		{
			lock(_lockObj)
				return _cookieTimers.ContainsKey(cookie);
		}

        public bool TryGetValue(KeyType cookie, out DataType data)
        {
            lock (_lockObj)
            {
                CookieExpireTime<KeyType, DataType> value;
                data = default(DataType);
                bool result = _cookieTimers.TryGetValue(cookie, out value);
                if (result)
                    data = value.Data;
                return result;
            }
        }

        public IEnumerable Keys
        {
            get
            {
                return _cookieTimers.Keys;
            }
        }

        public void Clear()
        {
            lock (_lockObj)
            {
                _cookieTimers.Clear();
                _responseEvent.Set();
            }
        }
	}
}

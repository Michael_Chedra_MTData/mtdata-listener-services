using System;
using System.IO;

namespace MTData.Sys
{
	/// <summary>
	/// This class is a collection of functionality that is available to all applications.
	/// </summary>
	public class Utility 
	{		
		/// <summary>
		/// Checks to see if the string passed in is a number (including comma, and decimal dot).
		/// Note: This only tests
		/// a certain number of characters before assertaining its a number.
		/// </summary>
		/// <param name="stringToCheck">String to Test</param>
		/// <param name="NumberOfCharsToTest">Number of characters in string to test.</param>
		/// <returns>Boolean indicating pass/fail of test</returns>
		public static bool IsNumeric(string stringToCheck, int NumberOfCharsToTest)
		{
			bool valid = true;

			// Ensure we dont break the test by testing too many chars
			if (NumberOfCharsToTest <= stringToCheck.Length)
			{
				for (int i = 0; i < NumberOfCharsToTest; i++)
				{
					valid = IsNumeric(stringToCheck[i].ToString());
					if (!valid) break;
				}
			}
			else
				return false;

			return valid;
		}

		private const string NUMERIC_CHARS = "0123456789";
		private const string NUMERIC_SEPARATORS = ".,";
		private const string NUMERIC_DECIMALCHAR = ".";

		/// <summary>
		/// Checks to see if the string passed represents a number
		/// Also check for only 1 decimal place.
		/// </summary>
		/// <param name="sourceDate">String to be tested</param>
		/// <returns>True if it does</returns>
		public static bool IsNumeric(string stringNumber)
		{
			bool valid = true;
			int decimalCount = 0;

			int startIndex = 0;
			if ((stringNumber.Length > 0) && (stringNumber[0] == '-'))
				startIndex = 1;

			// if the length is zero its not numeric
			if (stringNumber.Length == 0)
				valid = false;

			for (int loop = startIndex; loop < stringNumber.Length; loop++)
			{
				// Check if the character is a number.
				if (NUMERIC_CHARS.IndexOf(stringNumber[loop]) == -1)
				{
					// If it is not a number, check if it is a separator.
					if (NUMERIC_SEPARATORS.IndexOf(stringNumber[loop]) == -1)
					{
						valid = false;
						break;
					}
					else
					{
						// If it is a decimal, check there is only one.
						if (NUMERIC_DECIMALCHAR.IndexOf(stringNumber[loop]) != -1)
						{
							decimalCount++;

							if (decimalCount > 1)
							{
								valid = false;
								break;
							}
						}
					}
				}
			}

			return valid;
		}

        // Calculate CheckSum
        public static short CalculateCheckSum(byte[] tBuffer, int iStartPos, int iEndPos)
        {
            short result = 0;
            for (int pos = iStartPos; pos < iEndPos; pos++)
                result += tBuffer[pos];
            return result;
        }

        public static short CalculateCheckSum(Stream stream)
        {
            short result = 0;
            byte[] buffer = new byte[1024];
            int bytesRead;

            do
            {
                bytesRead = stream.Read(buffer, 0, buffer.Length);
                for (int i = 0; i < bytesRead; i++)
                    result += buffer[i];
            } while (bytesRead > 0);

            return result;
        }
    }
}

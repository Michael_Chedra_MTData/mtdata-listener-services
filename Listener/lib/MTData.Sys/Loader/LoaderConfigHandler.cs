#if !NETCF && !NETCF2
using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace MTData.Sys.Loader
{
	public class LoaderConfigHandler : IConfigurationSectionHandler
	{
		#region IConfigurationSectionHandler Members

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			LoaderConfig result = new LoaderConfig();
			if (section != null)
			{
				XmlNodeList list = section.SelectNodes("TypeMapping/Type");
				if ((list != null) && (list.Count > 0))
					foreach (XmlNode node in list)
					{
						XmlNode attrib = node.Attributes.GetNamedItem("name");
						if (attrib != null)
						{
							string typeName = attrib.Value;
							if (typeName.Trim().Length > 0)
							{
								string mapping = node.InnerText;
								if (mapping.Trim().Length > 0)
								{
									string targetConfigNodeName = null;
									XmlNode configNode = node.Attributes.GetNamedItem("configNodeName");
									if (configNode != null)
										targetConfigNodeName = configNode.Value;

									result.AddTypeConversion(typeName, mapping, targetConfigNodeName);
								}
							}
						}

					}
			}
			return result;
		}

		#endregion
	}
}
#endif
#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.IO;
using System.Xml;
using System.Reflection;
using System.Collections;
using System.Collections.Specialized;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;

namespace MTData.Sys.Loader
{
    public class Loader
    {
        public interface ILoaderPropertyTypeHandler
        {
            Type[] GetTypes();
            void SetValue(object instance, PropertyInfo property, string value);
        }

        private static Dictionary<Type, ILoaderPropertyTypeHandler> _propertyHandlers = new Dictionary<Type, ILoaderPropertyTypeHandler>();

        private const string ERROR_TYPENODE_NOTFOUND = "Could not find 'type' attribute in supplied Xml Node";
        private const string ERROR_TYPENAME_INVALID = "The type name supplied is invalid";
        private const string ERROR_TYPE_NOTFOUND = "The type '{0}' could not be found";
        private const string ERROR_TYPE_NOTFOUND_TRANSLATED = "The type '{0}' translated from '{1}' could not be found";
        private const string ERROR_TYPE_NOTINSTANTIATED = "The type '{0}' could not be instantiated";
        private const string ERROR_TYPE_NOTCONFIGURED = "The type '{0}' could not be configured";

        private IHostConfigProvider _hostConfigProvider = null;

        private static LoaderConfig _defaultConfig;
        private static bool _defaultConfigLoaded = false;

        private LoaderConfig _config = null;

        /// <summary>
        /// Return the default config for the loader.
        /// </summary>
        /// <returns></returns>
        private static LoaderConfig GetDefaultConfig()
        {
            if (!_defaultConfigLoaded)
            {
                lock (typeof(Loader))
                {
                    if (!_defaultConfigLoaded)
                    {
                        try
                        {
                            string configPath = typeof(Loader).FullName.Replace(".", "/");
#if !NETCF && !NETCF2 && !Android && !NETSTANDARD2_0
							_defaultConfig = (LoaderConfig)System.Configuration.ConfigurationSettings.GetConfig(configPath);
#elif Android || NETSTANDARD2_0
                            //TODO This needs to be tested.
                            _defaultConfig = new LoaderConfig();
                            string documentsPath = Environment.GetFolderPath(Environment.SpecialFolder.Personal);

                            string configFilePath = Path.Combine(documentsPath, Assembly.GetEntryAssembly().FullName + ".config");
                            var config = new Config.Config(configFilePath);
                            _defaultConfig = (LoaderConfig)config.GetSection(configPath);
#else
                            _defaultConfig = (LoaderConfig)OpenNETCF.Configuration.ConfigurationSettings.GetConfig(configPath);
#endif
                        }
                        catch (Exception)
                        {
                            _defaultConfig = null;
                        }
                        _defaultConfigLoaded = true;
                    }
                }
            }
            return _defaultConfig;
        }

        /// <summary>
        /// Prepare the class for use.
        /// </summary>
        public Loader()
        {
            _config = GetDefaultConfig();
        }

        /// <summary>
        /// Create a loader to use a specific config.
        /// </summary>
        /// <param name="config"></param>
        public Loader(LoaderConfig config)
        {
            _config = config;
        }

        /// <summary>
        /// This will instantiate the class, providing a host config provider 
        /// to the loader.
        /// </summary>
        /// <param name="hostConfigProvider"></param>
        public Loader(IHostConfigProvider hostConfigProvider)
            : this()
        {
            _hostConfigProvider = hostConfigProvider;
        }

        /// <summary>
        /// This will instantiate the class, providing a host config provider 
        /// to the loader.
        /// </summary>
        /// <param name="config"></param>
        /// <param name="hostConfigProvider"></param>
        public Loader(LoaderConfig config, IHostConfigProvider hostConfigProvider)
            : this(config)
        {
            _hostConfigProvider = hostConfigProvider;
        }

        public IHostConfigProvider HostConfigProvider
        {
            get
            {
                return _hostConfigProvider;
            }
            set
            {
                _hostConfigProvider = value;
            }
        }

        /// <summary>
        /// Load and instantiate a class instance based on an xml node supplied.
        /// There are two methods used to configure the type to be loaded.
        ///		type Attribute, specifying the full typename and assembly reference
        ///		assembly and class Attributes, which can be combined to provide the typename
        /// </summary>
        /// <param name="configNode"></param>
        /// <returns></returns>
        public object Create(object context, System.Xml.XmlNode configNode)
        {
            string typeName = null;
            System.Xml.XmlNode typeNode = configNode.Attributes.GetNamedItem("type");
            if (typeNode == null)
            {
                typeNode = configNode.Attributes.GetNamedItem("assembly");
                if (typeNode != null)
                {
                    typeName = typeNode.Value;
                    typeNode = configNode.Attributes.GetNamedItem("class");
                    if (typeNode != null)
                        typeName = string.Format("{0},{1}", typeNode.Value, typeName);
                    else
                        typeName = null;
                }
            }
            else
                typeName = typeNode.Value;

            if (typeName == null)
                throw new LoaderException(ERROR_TYPENODE_NOTFOUND);
            else
                return Create(context, typeName, configNode);
        }

#if !NETCF && !NETCF2 && !Android
        static System.Reflection.Assembly CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)
        {
            Assembly assembly = null;
            try
            {
                // Check if the DLL is in the same directory as the service
                string assemblyPath = Path.Combine(Environment.CurrentDirectory, new AssemblyName(args.Name).Name + ".dll");
                if (File.Exists(assemblyPath) == false)
                {
                    // Check if the dll is in a relative directory (Debug Testing)
                    assemblyPath = Path.Combine(Environment.CurrentDirectory + "\\..\\" + new AssemblyName(args.Name).Name, new AssemblyName(args.Name).Name + ".dll");
                    if (File.Exists(assemblyPath) == false)
                    {
                        //_log.Info("Could not locate " + new AssemblyName(args.Name).Name + ".dll, please make sure the interface dll is in the same directory as the service.");
                        return null;
                    }
                }
                assembly = Assembly.LoadFrom(assemblyPath);
            }
            catch (Exception ex)
            {
                throw new System.Exception("MTData.Sys.CurrentDomain_AssemblyResolve(object sender, ResolveEventArgs args)", ex);
            }
            return assembly;
        }
#endif

        /// <summary>
        /// Create an instance of the typename supplied.
        /// Type conversion will apply to this method
        /// </summary>
        /// <param name="context"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public object Create(object context, string typeName)
        {
            return Create(context, typeName, null);
        }

        /// <summary>
        /// Create an instance of the specified type
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <returns></returns>
        public object Create(object context, Type type)
        {
            return Create(context, type, null);
        }

        /// <summary>
        /// Create an instance of the typename supplied, and configure it from the configNode
        /// Type conversion will apply to this method
        /// </summary>
        /// <param name="context"></param>
        /// <param name="typeName"></param>
        /// <param name="configNode"></param>
        /// <returns></returns>
        public object Create(object context, string typeName, System.Xml.XmlNode configNode)
        {
            if ((typeName == null) || (typeName == ""))
                throw new LoaderException(ERROR_TYPENAME_INVALID);

            //	check to see if a translation is required.
            string activeTypeName = null;
            if ((_config != null) && (_config.HasTypeConversions))
                activeTypeName = _config.ConvertTypeName(typeName);
            if (activeTypeName == null)
                activeTypeName = typeName;

            //	Note if we are altering the type name, we may need to still go to the old or new config node..
            string configNodeName = null;
            if (activeTypeName != typeName)
                configNodeName = _config.ConvertConfigNodeName(typeName);

            Type type = Type.GetType(activeTypeName);

#if !NETCF && !NETCF2 && !Android
            //	If a codebase has been set, try accessing the assembly through that..
            if ((type == null))
            {
                if (_codeBase == null)
                {
                    _codeBase = Environment.CurrentDirectory;
                }
                Assembly assembly = null;
                string[] splits = activeTypeName.Split(',');
                if (splits.Length >= 2)
                {
                    string assemblyName = Path.Combine(_codeBase, splits[1]);
                    if (assemblyName != null)
                    {
                        string assemblyPath = Path.Combine(Environment.CurrentDirectory, assemblyName + ".dll");
                        if (File.Exists(assemblyPath) == false)
                        {
                            assemblyPath = Path.Combine(Environment.CurrentDirectory, assemblyName + ".exe");
                            if (File.Exists(assemblyPath) == false)
                            {
                                // Check if the dll is in a relative directory (Debug Testing)
                                assemblyPath = Path.Combine(Environment.CurrentDirectory + "\\..\\" + assemblyName, assemblyName + ".dll");
                                if (File.Exists(assemblyPath) == false)
                                {
                                    assemblyPath = Path.Combine(Environment.CurrentDirectory + "\\..\\" + assemblyName, assemblyName + ".exe");
                                    if (File.Exists(assemblyPath) == false)
                                    {
                                        //_log.Info("Could not locate " + new AssemblyName(args.Name).Name + ".dll, please make sure the interface dll is in the same directory as the service.");
                                        return null;
                                    }
                                }
                            }
                        }
                        assembly = Assembly.LoadFrom(assemblyPath);
                    }
                    type = assembly.GetType(splits[0]);
                }
            }
#else
            //	If a codebase has been set, try accessing the assembly through that..
            if ((type == null) && (_codeBase != null))
            {
                string[] splits = activeTypeName.Split(',');
                if (splits.Length >= 2)
                {
                    string assemblyName = Path.Combine(_codeBase, splits[1] + ".dll");
                    Assembly assembly = Assembly.LoadFrom(assemblyName);
                    type = assembly.GetType(splits[0]);
                }
            }
#endif


            if (type == null)
                if (activeTypeName != typeName)
                    throw new LoaderException(string.Format(ERROR_TYPE_NOTFOUND_TRANSLATED, activeTypeName, typeName));
                else
                    throw new LoaderException(string.Format(ERROR_TYPE_NOTFOUND, typeName));

            if (configNodeName != null)
            {
                //	determine if the config node name has been modified.
                return Create(context, type, configNode, configNodeName);
            }
            else
                return Create(context, type, configNode);
        }

        /// <summary>
        /// This method will create an instance of a givne type, and configure it.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="configNode"></param>
        /// <returns></returns>
        public object Create(object context, Type type, System.Xml.XmlNode configNode)
        {
            return Create(context, type, configNode, type.Name);
        }

        /// <summary>
        /// This method creates an instance of the specified type and configures it with the 
        /// subnode with the name specified in the configNodeName parameter
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="configNode"></param>
        /// <param name="configNodeName"></param>
        /// <returns></returns>
        public object Create(object context, Type type, System.Xml.XmlNode configNode, string configNodeName)
        {
            object instance = null;
            try
            {
                instance = Activator.CreateInstance(type);
                if (instance == null)
                    throw new LoaderException(string.Format(ERROR_TYPE_NOTINSTANTIATED, type.Name));
            }
            catch (Exception ex)
            {
                throw new LoaderException(string.Format(ERROR_TYPE_NOTINSTANTIATED, type.Name), ex);
            }

            try
            {
                // Check to see if the instance allows Configuration
                System.Xml.XmlNode typeConfigNode = null;
                if (configNode != null)
                    typeConfigNode = configNode.SelectSingleNode(configNodeName);

                if (typeConfigNode == null)
                    typeConfigNode = configNode;

                bool configured = false;

                if ((instance is ILoaderConfigurable) || (instance is IConfigurable))
                {
                    if (instance is ILoaderConfigurable)
                        ((ILoaderConfigurable)instance).Configure(this, context, typeConfigNode);
                    else
                        ((IConfigurable)instance).Configure(context, null, null, typeConfigNode);
                    configured = true;
                }

                //	Check to see if the object supports host configuration
                if ((_hostConfigProvider != null) && (instance is IHostConfigurable))
                {
                    _hostConfigProvider.ConfigureGuest(context, instance as IHostConfigurable);
                    configured = true;
                }

                if (!configured && (typeConfigNode != null))
                    PopulatePropertiesFromNode(instance, typeConfigNode);

            }
            catch (Exception ex)
            {
                throw new LoaderException(string.Format(ERROR_TYPE_NOTCONFIGURED, type.Name), ex);
            }

            return instance;
        }

        private string _codeBase = null;

        /// <summary>
        /// This is the base path from which an assembly should be loaded if it is not found through standard mechanisms.
        /// </summary>
        public string CodeBase
        {
            get
            {
                return _codeBase;
            }
            set
            {
                _codeBase = value;
            }
        }

        /// <summary>
        /// This method will allow all callers to quickly populate a  number of properties based on the
        /// xml node passed in. It currently only used the attributes of the node.
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="configNode"></param>
        public static void PopulatePropertiesFromNode(object instance, XmlNode configNode)
        {
            //	iterate thorugh the attributes and properties to see if there are any matches
            foreach (XmlAttribute attributeNode in configNode.Attributes)
                PopulatePropertyFromNameValue(instance, attributeNode.Name, attributeNode.Value);
        }

        public static void PopulatePropertiesFromNode(object instance, XmlNode configNode, MTData.Sys.ContextProvider.IContextProvider templateContextProvider)
        {
            //	iterate thorugh the attributes and properties to see if there are any matches
            foreach (XmlAttribute attributeNode in configNode.Attributes)
            {
                MTData.Sys.Template.TemplateProcessor tp = new MTData.Sys.Template.TemplateProcessor(new MTData.Sys.Template.TemplateStringParser(attributeNode.Value));
                PopulatePropertyFromNameValue(instance, attributeNode.Name, tp.Evaluate(templateContextProvider));
            }
        }

        /// <summary>
        /// Populate the properties of an object form a NameValueCollection
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="nameValueCollection"></param>
        public static void PopulatePropertiesFromCollection(object instance, NameValueCollection nameValueCollection)
        {
            foreach (string key in nameValueCollection.Keys)
                PopulatePropertyFromNameValue(instance, key, nameValueCollection[key]);
        }

        /// <summary>
        /// Populate a property given the field name and field value
        /// </summary>
        /// <param name="instance"></param>
        /// <param name="name"></param>
        /// <param name="value"></param>
        public static void PopulatePropertyFromNameValue(object instance, string name, string value)
        {
            PropertyInfo info = instance.GetType().GetProperty(name);
            if (info != null)
            {
                if (!info.CanWrite)
                    throw new InvalidOperationException(string.Format("The property {0} in type {1} is read-only and cannot be assigned the value {2}", name, instance.GetType().Name, value));

                ILoaderPropertyTypeHandler handler = null;
                if (_propertyHandlers.TryGetValue(info.PropertyType, out handler))
                    handler.SetValue(instance, info, value);
                else if (info.PropertyType.IsEnum)
                {
                    if (Enum.IsDefined(info.PropertyType, value))
                    {
                        info.SetValue(instance, Convert.ChangeType(Enum.Parse(info.PropertyType, value, true), info.PropertyType, null), null);
                    }
                }
                else
                {
                    info.SetValue(instance, Convert.ChangeType(value, info.PropertyType, null), null);
                }
            }
        }

        /// <summary>
        /// This will register a handler against each type that it handles.
        /// Note that any previous registration for this type will be overwritten.
        /// </summary>
        /// <param name="handler"></param>
        public static void RegisterPropertyTypeHandler(ILoaderPropertyTypeHandler handler)
        {
            Type[] types = handler.GetTypes();
            foreach (Type type in types)
                _propertyHandlers[type] = handler;
        }

        #region Shutdown Notification

        /// <summary>
        /// List of objects requiring shutdown notification
        /// </summary>
        private static ArrayList _shutdownNotifications = new ArrayList();

        /// <summary>
        /// Synchronication object for shutdown registration
        /// </summary>
        private static object _shutdownSyncRoot = new object();

        /// <summary>
        /// This method will allow a class to register for shutdown notification.
        /// On shutdown, this class will be disposed.
        /// </summary>
        /// <param name="instance"></param>
        public static void RegisterForShutdown(IDisposable instance)
        {
            if (_shutdownNotifications != null)
            {
                lock (_shutdownSyncRoot)
                {
                    if ((_shutdownNotifications != null) && (!_shutdownNotifications.Contains(instance)))
                        _shutdownNotifications.Add(instance);
                }
            }
        }

        /// <summary>
        /// This method will be called when the application is being shutdown..
        /// </summary>
        public static void Shutdown()
        {
            ArrayList notifications = null;
            lock (_shutdownSyncRoot)
            {
                notifications = _shutdownNotifications;
                _shutdownNotifications = null;
            }

            if (notifications != null)
            {
                foreach (IDisposable disposableInstance in notifications)
                    disposableInstance.Dispose();
                notifications.Clear();
            }
        }

        #endregion
    }
}

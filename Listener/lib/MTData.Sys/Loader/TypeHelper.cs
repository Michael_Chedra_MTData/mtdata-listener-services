#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Reflection;
using MTData.Sys.Config;

namespace MTData.Sys.Loader
{
	public class TypeHelper
	{
		public static event TypeHelperEvent UnableToLoadType;


		public delegate void TypeHelperEvent(string typeName);

		private TypeHelper()
		{
		}

		/// <summary>
		/// Load the specified Type specified by the format "[TypeName],[AssemblyName]"
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns>The Type loaded or null</returns>
		public static Type GetType(string typeName)
		{
			Type type = Type.GetType(typeName);
			if (type == null)
			{
				string[] parts = typeName.Split(new char[] {','});
				if (parts.Length > 1)
				{
					try
					{
						Assembly assembly = Assembly.LoadFrom(parts[1] + ".dll");

						if (assembly != null)
							type = assembly.GetType(parts[0], true);
					}
					catch(Exception)
					{
						if (UnableToLoadType != null)
							UnableToLoadType(typeName);
					}
				}
			}
			return type;
		}

		/// <summary>
		/// .NET Compact Framework version of the missing Object.Equals method
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
#if NETCF
		public static bool Equals(object a, object b)
#else
		public static new bool Equals(object a, object b)
#endif
		{
			if (a == b)
				return true;

			if (a == null)
				return false;

			if (b == null)
				return false;

			return a.Equals(b);
		}

		public static object CreateConfiguredInstance(IConfigSection configSection, string configKey)
		{
			return CreateInstance(configSection[configKey, configKey + " not defined"]);
		}

		public static object CreateInstance(string typeName)
		{
			Type type = TypeHelper.GetType(typeName);

			if (type == null)
				throw new TypeLoadException("Unable to create Instance of '" + typeName + "'");

			return Activator.CreateInstance(type);
		}
	}
}

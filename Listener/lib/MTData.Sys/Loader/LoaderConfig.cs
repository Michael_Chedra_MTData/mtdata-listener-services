using System;
using System.Collections;
using System.Text;

namespace MTData.Sys.Loader
{
	/// <summary>
	/// This class provides additional configuration details for the Loader class.
	/// </summary>
	public class LoaderConfig
	{
		private class ConversionEntry
		{
			private string _typeName;
			private string _targetTypeName;
			private string _targetConfigNode;

			public ConversionEntry(string typeName, string targetTypeName, string targetConfigNode)
			{
				_typeName = typeName;
				_targetTypeName = targetTypeName;
				_targetConfigNode = targetConfigNode;
			}

			public string TypeName { get { return _typeName; } }
			public string TargetTypeName { get { return _targetTypeName; } }
			public string TargetConfigNode { get { return _targetConfigNode; } }
		}

		private Hashtable _typeConversionMap = new Hashtable();
		

		/// <summary>
		/// Config has some type conversions registered.
		/// </summary>
		public bool HasTypeConversions
		{
			get { return (_typeConversionMap.Count > 0); }
		}

		/// <summary>
		/// Add a type conversion mapping.
		/// </summary>
		/// <param name="typeName"></param>
		/// <param name="targetTypeName"></param>
		public void AddTypeConversion(string typeName, string targetTypeName, string targetConfigNode)
		{
			_typeConversionMap[typeName] = new ConversionEntry(typeName, targetTypeName, targetConfigNode);
		}

		/// <summary>
		/// convert a given typename into the appropriate one to be loaded.
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public string ConvertTypeName(string typeName)
		{
			ConversionEntry entry = (ConversionEntry)_typeConversionMap[typeName];
			if (entry != null)
				return entry.TargetTypeName;
			else
				return typeName;
		}

		/// <summary>
		/// Convert the target config node name if neccessary,
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public string ConvertConfigNodeName(string typeName)
		{
			ConversionEntry entry = (ConversionEntry)_typeConversionMap[typeName];
			if (entry != null)
				return entry.TargetConfigNode;
			else
				return null;
		}

		/// <summary>
		/// Return the converted typename.
		/// </summary>
		/// <param name="typeName"></param>
		/// <returns></returns>
		public string this[string typeName]
		{
			get { return ConvertTypeName(typeName); }
		}
	}
}

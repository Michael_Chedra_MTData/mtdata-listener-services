#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Loader
{
	/// <summary>
	/// This interface identifies the fact that the class that implements it
	/// is configurable by the parent of the class instantiating it.
	/// It is used where a generic engine is loading the class, but the class
	/// requires application specific information from a higher level in the food chain.
	/// </summary>
	public interface IHostConfigurable
	{
		bool Configured
		{
			get;
		}
	}
}

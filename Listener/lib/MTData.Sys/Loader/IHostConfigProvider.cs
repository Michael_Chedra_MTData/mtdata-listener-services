#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Loader
{
	/// <summary>
	/// This interface is paired with the IHostConfigurable interface to allow the 
	/// host of a generic engine to configure an item loaded by that engine.
	/// </summary>
	public interface IHostConfigProvider
	{
		/// <summary>
		/// This will be called by the intermediate engine passing the class
		/// that is to be configured.
		/// </summary>
		/// <param name="context">This is the context of the call to the intermediate engine</param>
		/// <param name="guest">The class to be configured.</param>
		void ConfigureGuest(object context, IHostConfigurable guest);
	}
}

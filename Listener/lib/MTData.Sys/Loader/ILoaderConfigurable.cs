#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Loader
{
	/// <summary>
	/// This interface allows an object to be configured when loaded by the Loader class.
	/// The loader class normally loads from an Xml file or config file, and this interface 
	/// allows a consistent method for configuring those classes.
	/// If a class being loaded supports this interface, then the loader will look
	/// for a sub-node of the type loading node, with the same name as the type being loaded.
	/// NOTE 1: This is not a full qualified name with a namespace, but just the actual type name
	/// If this node exists, the laoder will pass it in to the configure method.
	/// NOTE 2: Regardless of whether the config node is found or not, the method will be called with
	/// the loader and context parameters, and configNode = NULL
	/// </summary>
	public interface ILoaderConfigurable
	{
		/// <summary>
		/// Configure the object.
		/// </summary>
		/// <param name="loader">Loader that instantiated the object</param>
		/// <param name="context">Context passed to the loader</param>
		/// <param name="configNode">Config node for the context.</param>
		void Configure(Loader loader, object context, System.Xml.XmlNode configNode);
	}
}

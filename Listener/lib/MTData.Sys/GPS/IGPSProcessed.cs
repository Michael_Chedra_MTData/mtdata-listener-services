using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	/// <summary>
	/// This delegate allows a listener to listen for updates.
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="position"></param>
	public delegate void GPSValueChanged(IGPSProcessed sender, GPRMCPosition position);

	/// <summary>
	/// This interface identifies events that can be linked to to get processing
	/// updates from the underlying GPS Model
	/// </summary>
	public interface IGPSProcessed
	{
		/// <summary>
		/// Current positional information
		/// </summary>
		GPRMCPosition Position
		{
			get;
		}

		/// <summary>
		/// This event is triggered whenever a GPRMC message is processed
		/// </summary>
		event GPSValueChanged GPRMCReceived;

		/// <summary>
		/// This event is raised whenever the validity of the GPS data ahs changed.
		/// </summary>
		event GPSValueChanged ValidChanged;
	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	public interface ILatLongProvider
	{
		double Latitude {get;}
		double Longitude
		{
			get;
		}

		event EventHandler LocationChanged;
	}
}

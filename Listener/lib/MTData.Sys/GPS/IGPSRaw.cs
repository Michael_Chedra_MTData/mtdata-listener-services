#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	/// <summary>
	/// This delegate identifies how the model will be build up 
	/// </summary>
	/// <param name="sender"></param>
	/// <param name="rawString"></param>
	public delegate void GPSRawValueChanged(IGPSRaw sender, string rawString);

	/// <summary>
	/// This interface identifies the raw GPS data being provided 
	/// by the GPS model
	/// </summary>
	public interface IGPSRaw
	{
		/// <summary>
		/// This will be called whenever there is a new string value retrieved.
		/// </summary>
		event GPSRawValueChanged RawValue;

		/// <summary>
		/// This will be called whenever there is a NMEA string received
		/// </summary>
		event GPSRawValueChanged NMEARawValue;

		/// <summary>
		/// This will be called when we know that a new GPRMC string has been received.
		/// </summary>
		event GPSRawValueChanged GPRMCRawValue;

		/// <summary>
		/// This event is raised whenever a ZDA string is received
		/// </summary>
		event GPSRawValueChanged GPZDARawValue;

		/// <summary>
		/// This event is raised whenever a GGA string is received
		/// </summary>
		event GPSRawValueChanged GPGGARawValue;

	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	public class GPSAxis : ICloneable
	{
		public enum CardinalPoint
		{
			North,
			South,
			East,
			West
		}

		internal int _degrees = 0;
		internal int _minutes = 0;
		internal double _seconds = 0;
		internal double _degreesAndDecimalMinutes = 0;
		internal CardinalPoint _direction;

		public int Degrees
		{
			get
			{
				return _degrees;
			}
		}

		public int Minutes
		{
			get
			{
				return _minutes;
			}
		}

		public double Seconds
		{
			get
			{
				return _seconds;
			}
		}

		public double DegreesDecimalMinutes
		{
			get
			{
				return _degreesAndDecimalMinutes;
			}
		}

		public CardinalPoint Direction
		{
			get
			{
				return _direction;
			}
		}


		#region ICloneable Members

		public object Clone()
		{
			GPSAxis clone = new GPSAxis();
			clone._degrees = this._degrees;
			clone._minutes = this._minutes;
			clone._seconds = this._seconds;
			clone._degreesAndDecimalMinutes = this._degreesAndDecimalMinutes;
			clone._direction = this._direction;
			return clone;
		}

		#endregion
	}
}

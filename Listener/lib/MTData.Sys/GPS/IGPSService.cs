using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	/// <summary>
	/// This interface allows access to the required underlying GPS Model
	/// </summary>
	public interface IGPSService
	{
		IGPSProcessed Processed
		{
			get;
		}

		IGPSRaw Raw
		{
			get;
		}
	}
}

#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	/// <summary>
	/// This class provides a parsed NMEA string for positional, directional and speed data
	/// </summary>
	public class GPRMCPosition : ICloneable
	{
        public enum ModeIndicatorTypes
        {
            Unknown,
            Autonomous,
            Differential,
            Estimated,
            DataNotValid,
        }


		#region attributes

		internal int _fix = 0;
		internal double _geoidalHeight = 0;
		internal int _year = 0;
		internal int _month = 0;
		internal int _day = 0;
		internal int _hour = 0;
		internal int _minute = 0;
		internal int _second = 0;
		internal int _millisecond = 0;
		internal double _altitude = 0;
		internal double _heading = 0;
		internal double _HDOP = 0;
		internal uint _satelliteCount = 0;
		
		internal GPSAxis _latitude = new GPSAxis();
		internal GPSAxis _longitude = new GPSAxis();

		internal int _localZoneHours = 0;
		internal int _localZoneMinutes = 0;
		internal double _speedAsKmh = 0.0;
		internal double _speedAsKnots = 0.0;
		internal double _magneticHeading = 0.0;
		internal double _magneticVariation = 0.0;
		internal GPSAxis.CardinalPoint _magneticVariationDirection = GPSAxis.CardinalPoint.East;

        internal ModeIndicatorTypes _modeIndicator = ModeIndicatorTypes.Unknown;

		#endregion

		#region properties
		public int Fix
		{
			get
			{
				return _fix;
			}
		}

		public double Geoidalheight
		{
			get
			{
				return _geoidalHeight;
			}
		}
		public DateTime UTCDateTime
		{
			get
			{
				try
				{
					return
						new DateTime(_year, _month, _day, _hour, _minute, _second, _millisecond);
				}
				catch
				{
					return DateTime.MinValue;
				}
			}
		}

		public int Year
		{
			get
			{
				return _year;
			}
		}

		public int Month
		{
			get
			{
				return _month;
			}
		}

		public int Day
		{
			get
			{
				return _day;
			}
		}

		public int Hour
		{
			get
			{
				return _hour;
			}
		}

		public int Minute
		{
			get
			{
				return _minute;
			}
		}

		public int Second
		{
			get
			{
				return _second;
			}
		}

		public int Millisecond
		{
			get
			{
				return _millisecond;
			}
		}

		public double HDOP
		{
			get
			{
				return _HDOP;
			}
		}
		public double Altitude
		{
			get
			{
				return _altitude;
			}
		}

		public double Heading
		{
			get
			{
				return _heading;
			}
		}

		public uint SatelliteCount
		{
			get
			{
				return _satelliteCount;
			}
		}

		public GPSAxis Latitude
		{
			get
			{
				return _latitude;
			}
		}

		public GPSAxis Longitude
		{
			get
			{
				return _longitude;
			}
		}

		public int LocalZoneHours
		{
			get
			{
				return _localZoneHours;
			}
		}
		public int LocalZoneminutes
		{
			get
			{
				return _localZoneMinutes;
			}
		}
		public double SpeedAsKmh
		{
			get
			{
				return _speedAsKmh;
			}
		}

		public double SpeedAsKnots
		{
			get
			{
				return _speedAsKnots;
			}
		}

        public ModeIndicatorTypes ModeIndicator
        {
            get { return _modeIndicator; }
        }
		#endregion


		#region ICloneable Members

		public object Clone()
		{
			GPRMCPosition clone = new GPRMCPosition();
			clone._fix = this._fix;
			clone._geoidalHeight = this._geoidalHeight;
			clone._year = this._year;
			clone._month = this._month;
			clone._day = this._day;
			clone._hour = this._hour;
			clone._minute = this._minute;
			clone._second = this._second;
			clone._millisecond = this._millisecond;
			clone._altitude = this._altitude;
			clone._heading = this._heading;
			clone._HDOP = this._HDOP;
			clone._satelliteCount = this._satelliteCount;
		
			clone._latitude = (GPSAxis)this._latitude.Clone();
			clone._longitude = (GPSAxis)this._longitude.Clone();

			clone._localZoneHours = this._localZoneHours;
			clone._localZoneMinutes = this._localZoneMinutes;
			clone._speedAsKmh = this._speedAsKmh;
			clone._speedAsKnots = this._speedAsKnots;
			clone._magneticHeading = this._magneticHeading;
			clone._magneticVariation = this._magneticVariation;
			clone._magneticVariationDirection = this._magneticVariationDirection;

            clone._modeIndicator = this._modeIndicator;
			return clone;
		}

		#endregion
	}
}

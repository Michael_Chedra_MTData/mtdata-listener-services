using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.IO.Serial;
using MTData.Sys.Services;
using MTData.Sys.Loader;
using MTData.Sys.Config;
using log4net;

namespace MTData.Sys.GPS
{
	public class GPSService : IService, IConfigurable, IGPSService
	{
		private static ILog _log = LogManager.GetLogger(typeof(GPSService));

		private string _name = "GPS";
		private ProviderCollection _providers = new ProviderCollection();

		private ISerialIOFactoryEx _streamFactory = null;
		private ISerialIO _gpsStream = null;
		private GPSModel _gpsModel = null;

		#region IService Members

		public string Name
		{
			get
			{
				return _name;
			}
		}

		public GPSService() : this(null)
		{

		}

		public GPSService(ISerialIOFactoryEx streamFactory)
		{
			_streamFactory = streamFactory;
		}

        /// <summary>
        /// Add a provider to the service
        /// </summary>
        /// <param name="provider">a provider</param>
        public void AddProvider(IProvider provider)
		{
			_providers.Add(provider);
		}

		public void Initialise(string name, IConfigSection configSection)
		{
			_name = name;

			IConfigSection streamSection = configSection.GetConfigSection("Stream");

			//	find the factory..
			ISerialIOFactory factory = (ISerialIOFactory)TypeHelper.CreateConfiguredInstance(streamSection, "ClassName");

			_gpsStream = factory.Create(streamSection, this);
			_gpsModel = new GPSModel(_gpsStream);
		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			if (_providers != null)
			{
				foreach (IProvider provider in _providers)
				{
					if (provider is IDisposable)
						((IDisposable)provider).Dispose();
				}
			}
			_providers = null;
            if (_gpsModel != null)
            {
                _gpsModel.Dispose();
                _gpsModel = null;
            }
            if (_gpsStream != null)
            {
                _gpsStream.Dispose();
                _gpsStream = null;
            }
        }

		#endregion

		#region IConfigurable Members

		/// <summary>
		/// Load up the serial interface to use.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="configNode"></param>
		public void Configure(object context, Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			if (configNode != null)
			{
				if (_log.IsInfoEnabled)
					_log.Info("Configuring GPS Stream Factory");

				Loader.Loader loader = new Loader.Loader();
				_streamFactory = (ISerialIOFactoryEx)loader.Create(context, configNode);
				if (_log.IsInfoEnabled)
					_log.InfoFormat("Configuring GPS Stream Factory : {0}", _streamFactory.GetType().FullName);
			}
			else
				if (_log.IsWarnEnabled)
					_log.Warn("No config node available for Stream Factory");
		}

		#endregion

		/// <summary>
		/// Instantiate and return the GPSModel
		/// </summary>
		/// <returns></returns>
		private GPSModel GetGPSModel()
		{
			if (_gpsStream == null)
			{
				lock (this)
				{
					if (_gpsStream == null)
					{
						if (_log.IsInfoEnabled)
							_log.Info("Creating GPS Stream");
						_gpsStream = _streamFactory.Create();
						_gpsModel = new GPSModel(_gpsStream);
					}
				}

			}
			return _gpsModel;
		}
			
		

		#region IGPSService Members

		public IGPSProcessed Processed
		{
			get
			{
				return (GetGPSModel() as IGPSProcessed);
			}
		}

		public IGPSRaw Raw
		{
			get
			{
				return (GetGPSModel() as IGPSRaw);
			}
		}

		#endregion

        #region IVersion Members

        public string VersionNumber
        {
            get { return string.Format("{0}.{1}", MajorNumber, MinorNumber); }
        }

        public int MajorNumber
        {
            get { return GetType().Assembly.GetName().Version.Major; }
        }

        public int MinorNumber
        {
            get { return GetType().Assembly.GetName().Version.Minor; }
        }

        public DateTime Date
        {
            get
            {
                String path = GetType().Assembly.ManifestModule.FullyQualifiedName;
                System.IO.FileInfo f = new System.IO.FileInfo(path);
                return f.CreationTime;
            }
        }
        #endregion

        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return _providers.GetEnumerator();
        }

        #endregion
    }
}

#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.IO;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using MTData.Sys.IO;
using MTData.Sys.IO.Serial;
using log4net;

namespace MTData.Sys.GPS
{
    /// <summary>
    /// This class will be responsible for parsing inbound GPS strings, 
    /// and allowing calling classes to register for different details.
    /// It supports both a parsed, and a raw interface.
    /// </summary>
    public class GPSModel : IDisposable, IGPSRaw, IGPSProcessed
    {
        private static ILog _log = LogManager.GetLogger(typeof(GPSModel));

        private const int BUFFER_SIZE = 1024;

        /// <summary>
        /// This is the stream from which GPS data is coming.
        /// </summary>
        private ISerialIO _serialIO;

        /// <summary>
        /// This is the parser for the binary line data.
        /// </summary>
        private DataLineParser _lineParser = null;

        /// <summary>
        /// This is the buffer that will be used for retrieving information from the Stream
        /// </summary>
        private byte[] _buffer = new byte[BUFFER_SIZE];

        /// <summary>
        /// Indicates that the checksum should be ignored.
        /// </summary>
        private bool _ignoreChecksum = false;

        /// <summary>
        /// Underlying positional data.
        /// </summary>
        private GPRMCPosition _position = new GPRMCPosition();

        /// <summary>
        /// This event will be raised whenever there is an error;
        /// </summary>
        public event ErrorCallbackDelegate Error;

        public GPRMCPosition Position
        {
            get
            {
                return _position;
            }
        }

        /// <summary>
        /// This initialises the model, passing it the stream from which it will read.
        /// </summary>
        /// <param name="serialIO"></param>
        public GPSModel(ISerialIO serialIO)
        {
            _lineParser = new DataLineParser(new byte[] { (byte)'\r', (byte)'\n' });
            _serialIO = serialIO;
            _serialIO.ErrorHandler = new SerialIOErrorHandler(serialIO_ErrorHandler);
            _serialIO.RxHandler = new SerialIORxHandler(serialIO_rxHandler);
        }

        /// <summary>
        /// This will handle errors from the caller.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="ex"></param>
        private void serialIO_ErrorHandler(ISerialIO sender, Exception ex)
        {
            OnError("SerialIOError", ex);
        }

        /// <summary>
        /// This is the handler for received data blocks
        /// </summary>
        /// <param name="ar"></param>
        private void serialIO_rxHandler(ISerialIO sender, byte[] data)
        {
            try
            {
                List<byte[]> lines = _lineParser.Parse(data);
                foreach (byte[] dataLine in lines)
                {
                    string dataText = System.Text.ASCIIEncoding.ASCII.GetString(dataLine, 0, dataLine.Length);
                    if ((dataText != null) && (dataText.Length > 0))
                        OnReceivedString(dataText);
                }
            }
            catch (Exception ex)
            {
                OnError("GPSReadCallback", ex);
            }
        }

        #region Event Processing

        /// <summary>
        /// This is called whenever a string line is received from the GPS.
        /// </summary>
        /// <param name="dataText"></param>
        private void OnReceivedString(string dataText)
        {
            if (RawValue != null)
                RawValue(this, dataText);

            //	Check to see if this is a valid NMEA string
            if (IsValidNMEACommand(dataText))
                OnNMEAStringReceived(dataText);
        }

        /// <summary>
        /// This method will process the nmea string and determine 
        /// which command type it is.
        /// </summary>
        /// <param name="dataText"></param>
        private void OnNMEAStringReceived(string dataText)
        {
            if (NMEARawValue != null)
                NMEARawValue(this, dataText);

            switch (dataText.Substring(1, 5))
            {
                case "GPZDA":
                    OnGPZDAReceived(dataText);
                    break;
                case "GPGGA":
                    OnGPGGAReceived(dataText);
                    break;
                case "GPRMC":
                    OnGPRMCReceived(dataText);
                    break;
            }
        }

        /// <summary>
        /// Parse the GPZDA Message
        /// </summary>
        /// <param name="dataText"></param>
        private void OnGPZDAReceived(string dataText)
        {
            if (GPZDARawValue != null)
                GPZDARawValue(this, dataText);
        }

        /// <summary>
        /// Parse the GPZDA Message
        /// </summary>
        /// <param name="dataText"></param>
        private void OnGPGGAReceived(string dataText)
        {
            string[] strings = dataText.Split(',');
            if (strings.Length > 7 && _position != null)
            {
                try { _position._satelliteCount = Convert.ToUInt32(strings[7]); }
                catch { }
            }

            if (GPGGARawValue != null)
                GPGGARawValue(this, dataText);
        }

        /// <summary>
        /// REturn the next comma separated field.
        /// </summary>
        /// <param name="nmeaData"></param>
        /// <returns></returns>
        private string GetNextCommaValue(ref string nmeaData)
        {
            return GetNextSplitValue(ref nmeaData, ',');
        }

        /// <summary>
        /// Split off the next split data value;
        /// </summary>
        /// <param name="nmeaData"></param>
        /// <returns></returns>
        private string GetNextSplitValue(ref string nmeaData, char splitChar)
        {
            string result = null;

            int index = nmeaData.IndexOf(splitChar);
            if (index >= 0)
            {
                result = nmeaData.Substring(0, index);
                nmeaData = nmeaData.Substring(index + 1, nmeaData.Length - index - 1);
            }
            else
            {
                result = nmeaData;
                nmeaData = string.Empty;
            }
            return result;
        }

        /// <summary>
        /// Take the string specified, and if the last maxByteCount are available, convert them to an integer.
        /// If not, convert what is there into an integer.
        /// Remove anything converted from the string passed.
        /// </summary>
        /// <param name="text"></param>
        /// <param name="maxByteCount"></param>
        /// <returns></returns>
        private int GetLastBytesAsInteger(ref string text, int maxByteCount)
        {
            int result = 0;
            if (text.Length < maxByteCount)
            {
                result = Int32.Parse(text);
                text = string.Empty;
            }
            else
            {
                result = Int32.Parse(text.Substring(text.Length - maxByteCount, maxByteCount));
                text = text.Substring(0, text.Length - maxByteCount);
            }
            return result;
        }

        /// <summary>
        /// Called whenever there is a utc time change.
        /// </summary>
        /// <param name="utcValue"></param>
        /// <returns>Indicates if a change occurred or not</returns>
        private bool OnUTCTimeChanged(string utcValue)
        {
            bool result = false;
            if ((utcValue != null) && (utcValue.Length > 0))
            {
                try
                {
                    string time = GetNextSplitValue(ref utcValue, '.');
                    int milli = 0;
                    if (utcValue.Length > 0)
                        milli = Convert.ToInt32(utcValue);

                    int second = GetLastBytesAsInteger(ref time, 2);
                    int minute = GetLastBytesAsInteger(ref time, 2);
                    int hour = GetLastBytesAsInteger(ref time, 2);

                    _position._millisecond = milli;
                    _position._second = second;
                    _position._minute = minute;
                    _position._hour = hour;
                    result = true;
                }
                catch
                {

                }
            }
            return result;
        }

        /// <summary>
        /// This occurs if an invalid GPRMC message is received
        /// </summary>
        /// <param name="dataText"></param>
        private void OnGPRMCInvalid(string dataText)
        {
            if (_position._fix != 0)
            {
                _position._fix = 0;
                if (ValidChanged != null)
                    ValidChanged(this, _position);
            }
        }

        /// <summary>
        /// This is called whenever a valid packet causes the valid state to change.
        /// </summary>
        /// <param name="dataText"></param>
        private void OnGPRMCValid(string dataText)
        {
            if (_position._fix <= 0)
            {
                _position._fix = 1;
                if (ValidChanged != null)
                    ValidChanged(this, _position);
            }
        }

        /// <summary>
        /// This is called with the next values.
        /// </summary>
        /// <param name="dataText"></param>
        private bool OnCoordinateChanged(string coordinateValue, GPSAxis axis)
        {
            bool result = false;
            if ((coordinateValue != null) && (coordinateValue.Length > 0))
            {
                try
                {
                    string degreesText = GetNextSplitValue(ref coordinateValue, '.');

                    double second = 0;
                    if ((coordinateValue != null) && (coordinateValue.Length > 0))
                        second = ((double)(Convert.ToInt32(coordinateValue) * 60)) / System.Math.Pow(10.0, (double)coordinateValue.Length);

                    int minute = GetLastBytesAsInteger(ref degreesText, 2);
                    int degree = GetLastBytesAsInteger(ref degreesText, 3);

                    if ((axis._degrees != degree) || (axis._minutes != minute) || (axis._seconds != second))
                    {
                        axis._degrees = degree;
                        axis._minutes = minute;
                        axis._seconds = second;
                        axis._degreesAndDecimalMinutes = (double)degree + ((double)minute / 60) + ((double)second / 3600);
                        result = true;
                    }
                }
                catch
                {

                }
            }
            return result;
        }

        private bool OnCoordinateDirectionChanged(string coordinateValue, GPSAxis axis)
        {
            bool result = false;
            if ((coordinateValue != null) && (coordinateValue.Length > 0))
            {
                try
                {
                    switch (coordinateValue)
                    {
                        case "N":
                            if ((axis.Direction != GPSAxis.CardinalPoint.North) || (axis._degreesAndDecimalMinutes < 0))
                            {
                                axis._direction = GPSAxis.CardinalPoint.North;
                                if (axis._degreesAndDecimalMinutes < 0)
                                    axis._degreesAndDecimalMinutes = -axis._degreesAndDecimalMinutes;
                                result = true;
                            }
                            break;
                        case "S":
                            if ((axis.Direction != GPSAxis.CardinalPoint.South) || (axis._degreesAndDecimalMinutes > 0))
                            {
                                axis._direction = GPSAxis.CardinalPoint.South;
                                if (axis._degreesAndDecimalMinutes > 0)
                                    axis._degreesAndDecimalMinutes = -axis._degreesAndDecimalMinutes;
                                result = true;
                            }
                            break;
                        case "E":
                            if ((axis.Direction != GPSAxis.CardinalPoint.East) || (axis._degreesAndDecimalMinutes < 0))
                            {
                                axis._direction = GPSAxis.CardinalPoint.East;
                                if (axis._degreesAndDecimalMinutes < 0)
                                    axis._degreesAndDecimalMinutes = -axis._degreesAndDecimalMinutes;
                                result = true;
                            }
                            break;
                        case "W":
                            if ((axis.Direction != GPSAxis.CardinalPoint.West) || (axis._degreesAndDecimalMinutes > 0))
                            {
                                axis._direction = GPSAxis.CardinalPoint.West;
                                if (axis._degreesAndDecimalMinutes > 0)
                                    axis._degreesAndDecimalMinutes = -axis.DegreesDecimalMinutes;
                                result = true;
                            }
                            break;
                    }
                }
                catch
                {
                }
            }
            return result;
        }

        private bool OnDoubleChanged(string doubleText, ref double value)
        {
            bool result = false;
            if ((doubleText != null) && (doubleText.Length > 0))
            {
                //	replace the . with the relevant decimal separator
                doubleText = doubleText.Replace(".", NumberFormatInfo.CurrentInfo.CurrencyDecimalSeparator);
                double parsedValue = double.Parse(doubleText);
                if (parsedValue != value)
                {
                    value = parsedValue;
                    result = true;
                }
            }
            return result;
        }

        public bool OnDateChanged(string dateText)
        {
            bool result = false;
            if ((dateText != null) && (dateText.Length > 0))
            {
                try
                {
                    int year = 0;
                    if (dateText.Length > 6)
                        year = GetLastBytesAsInteger(ref dateText, 4);
                    else
                        year = 2000 + GetLastBytesAsInteger(ref dateText, 2);

                    int month = GetLastBytesAsInteger(ref dateText, 2);
                    int day = GetLastBytesAsInteger(ref dateText, 2);

                    if ((_position.Year != year) || (_position.Month != month) || (_position.Day != day))
                    {
                        _position._year = year;
                        _position._month = month;
                        _position._day = day;
                        result = true;
                    }
                }
                catch
                {

                }
            }
            return result;
        }

        private bool OnMagVariationDirectionChanged(string variationText)
        {
            bool result = false;
            if ((variationText != null) && (variationText.Length > 0))
            {
                GPSAxis.CardinalPoint direction = GPSAxis.CardinalPoint.West;
                if (string.Compare(variationText, "E", true) == 0)
                    direction = GPSAxis.CardinalPoint.East;

                if (_position._magneticVariationDirection != direction)
                {
                    _position._magneticVariationDirection = direction;
                    result = true;
                }
            }
            return result;
        }

        private void OnModeIndicatorChanged(string modeIndicator)
        {
            _position._modeIndicator = GPRMCPosition.ModeIndicatorTypes.Unknown;
            if ((modeIndicator != null) && (modeIndicator.Length > 0))
            {
                try
                {
                    switch (modeIndicator)
                    {
                        case "A":
                            _position._modeIndicator = GPRMCPosition.ModeIndicatorTypes.Autonomous;
                            break;
                        case "D":
                            _position._modeIndicator = GPRMCPosition.ModeIndicatorTypes.Differential;
                            break;
                        case "E":
                            _position._modeIndicator = GPRMCPosition.ModeIndicatorTypes.Estimated;
                            break;
                        case "N":
                            _position._modeIndicator = GPRMCPosition.ModeIndicatorTypes.DataNotValid;
                            break;
                    }
                }
                catch
                {
                }
            }
        }

        /// <summary>
        /// Parse the GPRMC Message
        /// </summary>
        /// <param name="dataText"></param>
        private void OnGPRMCReceived(string dataText)
        {
            if (GPRMCRawValue != null)
                GPRMCRawValue(this, dataText);

            bool timeChanged = false;
            bool latChanged = false;
            bool lonChanged = false;
            bool speedChanged = false;
            bool headingChanged = false;
            bool dateChanged = false;
            bool magVariationChanged = false;
            bool magVariationDirectionChanged = false;

            //	cut out the $GPRMC
            string gprmcText = dataText;
            GetNextCommaValue(ref gprmcText);
            string timeText = GetNextCommaValue(ref gprmcText);
            if (GetNextCommaValue(ref gprmcText) != "A")
                OnGPRMCInvalid(dataText);
            else
            {
                timeChanged = OnUTCTimeChanged(timeText);
                latChanged = OnCoordinateChanged(GetNextCommaValue(ref gprmcText), _position._latitude);
                latChanged |= OnCoordinateDirectionChanged(GetNextCommaValue(ref gprmcText), _position._latitude);
                lonChanged = OnCoordinateChanged(GetNextCommaValue(ref gprmcText), _position._longitude);
                lonChanged |= OnCoordinateDirectionChanged(GetNextCommaValue(ref gprmcText), _position._longitude);

                speedChanged = OnDoubleChanged(GetNextCommaValue(ref gprmcText), ref _position._speedAsKnots);
                if (speedChanged)
                    _position._speedAsKmh = _position.SpeedAsKnots * 1.852;

                headingChanged = OnDoubleChanged(GetNextCommaValue(ref gprmcText), ref _position._heading);
                dateChanged = OnDateChanged(GetNextCommaValue(ref gprmcText));
                magVariationChanged = OnDoubleChanged(GetNextCommaValue(ref gprmcText), ref _position._magneticVariation);

                //	magVariationDirection is optional..
                int index = gprmcText.IndexOf(",");
                if (index < 0)
                    index = gprmcText.IndexOf("*");
                magVariationDirectionChanged = OnMagVariationDirectionChanged(gprmcText.Substring(0, index));

                //	Mode Indicator has been added for NEMA 0183 version 3.00
                gprmcText = gprmcText.Substring(index + 1);
                index = gprmcText.IndexOf(",");
                if (index < 0)
                    index = gprmcText.IndexOf("*");
                if (index < 0)
                {
                    _position._modeIndicator = GPRMCPosition.ModeIndicatorTypes.Unknown;
                }
                else
                {
                    OnModeIndicatorChanged(gprmcText.Substring(0, index));
                }

                OnGPRMCValid(dataText);
            }
            if (GPRMCReceived != null)
                GPRMCReceived(this, _position);
        }

        private void OnError(string message, Exception ex)
        {
            if (Error != null)
                Error(this, message, ex);
        }

        private List<string> _validNMEA = PrepareNMEAStrings();

        private static List<string> PrepareNMEAStrings()
        {
            List<string> results = new List<string>();
            results.Add("GLL");
            results.Add("GGA");
            results.Add("VTG");
            results.Add("RMC");
            results.Add("GSA");
            results.Add("GSV");
            results.Add("ZDA");
            results.Add("MSS");
            results.Add("AAM");
            results.Add("RMB");
            results.Add("BWC");

            return results;
        }

        private bool IsValidNMEACommand(string NMEAString)
        {
            bool result = true;

            if (!_ignoreChecksum)
                result = ValidateCheckSum(NMEAString);

            if (result)
                result = (NMEAString.Substring(0, 1) == "$");

            if (result)
            {
                string cmd = NMEAString.Substring(3, 3);
                result = (_validNMEA.IndexOf(cmd) >= 0);
            }

            return result;
        }

        public static bool ValidateCheckSum(string NMEAString)
        {
            try
            {
                int index = NMEAString.IndexOf("*");
                if (index >= 0)
                {
                    string str2 = NMEAString.Substring(index + 1, (NMEAString.Length - index) - 1);
                    return (CalculateNMEACheckSum(NMEAString.Substring(0, index)) == Convert.ToInt32("0x" + str2, 0x10));
                }
                return false;
            }
            catch
            {
                return false;
            }
        }

        public static int CalculateNMEACheckSum(string NMEAString)
        {
            if (NMEAString == null)
            {
                return 0;
            }
            int num = Convert.ToInt32(NMEAString[1]);
            for (int i = 2; i < NMEAString.Length; i++)
            {
                if (NMEAString[i] == '*')
                {
                    return num;
                }
                num ^= Convert.ToInt32(NMEAString[i]);
            }
            return num;
        }

        #endregion

        #region IGPSRaw Members

        public event GPSRawValueChanged RawValue;

        public event GPSRawValueChanged NMEARawValue;

        public event GPSRawValueChanged GPRMCRawValue;

        public event GPSRawValueChanged GPGGARawValue;

        public event GPSRawValueChanged GPZDARawValue;

        #endregion

        #region IGPSProcessed Members

        public event GPSValueChanged GPRMCReceived;

        public event GPSValueChanged ValidChanged;

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_serialIO != null)
                _serialIO.Dispose();
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.GPS
{
	public interface IDirectionProvider
	{
		double DirectionDegrees
		{
			get;
		}

		double SpeedKms
		{
			get;
		}

		double SpeedKnots
		{
			get;
		}

		event EventHandler DirectionChanged;
	}
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using Amazon;
using Amazon.S3;
using Amazon.S3.IO;
using Amazon.S3.Model;
using log4net;

namespace MTData.Sys.Services.Storage
{
    public class AWSS3StorageProvider : ProviderBaseClass, IStorageProvider
    {
        private AmazonS3Client _client;
        private object _lockObject;
        private ILog _log = LogManager.GetLogger(typeof(AWSS3StorageProvider));

        #region public properties
        public string RegionEndpointName { get; set; }
        public string AccessKey { get; set; }
        public string SecretKey { get; set; }
        public string Bucket { get; set; }
        public string BasePath { get; set; }
        public int BufferLowerLimit { get; set; }
        public int BufferUpperLimit { get; set; }
        #endregion

        public AWSS3StorageProvider()
            :base("AWSS3Provider", "AWS S3 Storage Provider")
        {
            _lockObject = new object();
            BufferLowerLimit = 2048;
            BufferUpperLimit = 1048576;
        }

        #region implement IStorageProvider
        public void CreatePath(string path)
        {
            CreateClient();

            S3DirectoryInfo directory = new S3DirectoryInfo(_client, Bucket, Path.Combine(BasePath, path));
            if (!directory.Exists)
            {
                directory.Create();
            }
        }

        public void DeleteFile(string path)
        {
            CreateClient();
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, path));
            if (info.Exists)
            {
                info.Delete();
            }
        }

        public void DeletePath(string path, bool recursive)
        {
            CreateClient();
            S3DirectoryInfo directory = new S3DirectoryInfo(_client, Bucket, Path.Combine(BasePath, path));
            if (directory.Exists)
            {
                if (recursive)
                {
                    DeleteDirectory(directory);
                }
                else
                {
                    //check if any files or folders
                    if (directory.GetDirectories().Length > 0 || directory.GetFiles().Length > 0)
                    {
                        throw new DirectoryNotEmptyException("The directory is not empty", path);
                    }
                    directory.Delete();
                }
            }
        }

        public void ExportFile(string filePath, string fileSystemPath, bool deleteWhenExported)
        {
            CreateClient();
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            info.CopyToLocal(fileSystemPath);

            if (deleteWhenExported)
            {
                info.Delete();
            }
        }

        public bool FileExists(string filePath)
        {
            CreateClient();
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            return info.Exists;
        }

        public string GetFileNameFromFilePath(string filePath)
        {
            return Path.GetFileName(filePath);
        }

        public string[] GetFileNames(string path, string filter)
        {
            CreateClient();

            S3DirectoryInfo directory = new S3DirectoryInfo(_client, Bucket, Path.Combine(BasePath, path));
            if (!directory.Exists)
                throw new DirectoryNotFoundException(string.Format("Could not find path '{0}' in provider {1}", path, base.Name));

            var files = directory.GetFiles(filter);
            string[] results = new string[files.Length];
            for (int i = 0; i < files.Length; i++)
                results[i] = files[i].Name;
            return results;
        }

        public string GetPathFromFilePath(string filePath)
        {
            return Path.GetDirectoryName(filePath);
        }

        public string[] GetPathNames(string path, string filter)
        {
            CreateClient();

            S3DirectoryInfo directory = new S3DirectoryInfo(_client, Bucket, Path.Combine(BasePath, path));
            if (!directory.Exists)
                throw new DirectoryNotFoundException(string.Format("Could not find path '{0}' in provider {1}", path, base.Name));

            var dirs = directory.GetDirectories(filter);
            string[] results = new string[dirs.Length];
            for (int i = 0; i < dirs.Length; i++)
                results[i] = dirs[i].Name;
            return results;
        }

        public void ImportFile(string filePath, string fileSystemPath, bool deleteWhenImported)
        {
            CreateClient();
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            info.CopyFromLocal(fileSystemPath);

            if (deleteWhenImported)
            {
                File.Delete(fileSystemPath);
            }
        }

        public object LoadObject(string filePath, Type type)
        {
            _log.Debug("Loading object (FilePath: " + filePath + ", type: " + type);
            CreateClient();
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            object result = null;
            Stream stream = null;
            try
            {
                stream = info.OpenRead();
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(type);
                result = serializer.Deserialize(stream);
            }
            catch (Exception ex)
            {
                _log.Error("Failed to load object (FilePath: " + filePath + ", type: " + type, ex);
                _log.Error("Stream Position: " + stream.Position);
                throw;
            }
            finally
            {
                if (stream != null)
                {
                    stream.Dispose();
                }
            }
            return result;
        }

        public Stream LoadStream(string filePath)
        {
            CreateClient();

            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            return info.OpenRead();
        }

        public XmlDocument LoadXmlDocument(string filePath)
        {
            CreateClient();

            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            XmlDocument result = new XmlDocument();
            using (Stream open = info.OpenRead())
            {
                result.Load(open);
            }
            return result;
        }

        public bool PathExists(string path)
        {
            CreateClient();
            S3DirectoryInfo directory = new S3DirectoryInfo(_client, Bucket, Path.Combine(BasePath, path));
            return directory.Exists;
        }

        public void SaveObject(string filePath, object graph)
        {
            _log.Debug("Saving Object: " + filePath + " Type: " + graph.GetType());
            CreateClient();
            CreatePath(GetPathFromFilePath(filePath));
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            using (Stream open = info.OpenWrite())
            {
                System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(graph.GetType());
                serializer.Serialize(open, graph);
            }
        }

        public void SaveStream(string filePath, Stream stream)
        {
            CreateClient();

            CreatePath(GetPathFromFilePath(filePath));

            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            using (Stream open = info.OpenWrite())
            {
                int bufferSize = GetBufferSize(stream.Length);
                byte[] buffer = new byte[bufferSize];
                stream.Position = 0;
                int count = stream.Read(buffer, 0, buffer.Length);
                while (count > 0)
                {
                    open.Write(buffer, 0, count);
                    count = stream.Read(buffer, 0, buffer.Length);
                }
            }
        }

        public void SaveXmlDocument(string filePath, XmlDocument document)
        {
            CreateClient();
            CreatePath(GetPathFromFilePath(filePath));
            S3FileInfo info = new S3FileInfo(_client, Bucket, Path.Combine(BasePath, filePath));
            using (Stream open = info.OpenWrite())
            {
                document.Save(open);
            }
        }
        #endregion

        #region private methods
        private void CreateClient()
        {
            if (_client == null)
            {
                lock (_lockObject)
                {
                    if (_client == null)
                    {
                        RegionEndpoint endPoint = RegionEndpoint.GetBySystemName(RegionEndpointName);

                        // if access key or secret key is provide the use these credential
                        if (!string.IsNullOrEmpty(AccessKey) || !string.IsNullOrEmpty(SecretKey))
                        {
                            _client = new AmazonS3Client(AccessKey, SecretKey, endPoint);
                        }
                        else
                        {
                            _client = new AmazonS3Client(endPoint);
                        }

                        //check bucket exists
                        var buckets = _client.ListBuckets();
                        S3Bucket bucket = buckets.Buckets.FirstOrDefault(b => b.BucketName == Bucket);
                        if (bucket == null)
                        {
                            var bResponse = _client.PutBucket(new PutBucketRequest() { BucketName = Bucket });
                            bucket = buckets.Buckets.FirstOrDefault(b => b.BucketName == Bucket);
                        }
                        //check base path exists
                        S3DirectoryInfo directory = new S3DirectoryInfo(_client, Bucket, BasePath);
                        if (!directory.Exists)
                        {
                            directory.Create();
                        }
                    }
                }
            }
        }

        private void DeleteDirectory(S3DirectoryInfo dir)
        {
            var files = dir.GetFiles();
            foreach (var f in files)
            {
                Console.WriteLine(string.Format("Delete file {0}", f.Name));
                f.Delete();
            }
            var directories = dir.GetDirectories();
            foreach (var d in directories)
            {
                DeleteDirectory(d);
            }
            dir.Delete();
        }

        private int GetBufferSize(long targetSize)
        {
            long result = targetSize / 4;
            result = (result / 1024) * 1024;
            if (result < BufferLowerLimit)
                result = BufferLowerLimit;
            if (result > BufferUpperLimit)
                result = BufferUpperLimit;
            if (result > Int32.MaxValue)
                return Int32.MaxValue;
            else
                return (int)result;
        }
        #endregion
    }
}

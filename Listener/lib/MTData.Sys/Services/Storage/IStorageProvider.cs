using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.Storage
{
	/// <summary>
	/// Thisis the storage provider interface.
	/// </summary>
	public interface IStorageProvider
	{
		/// <summary>
		/// Return the path without the file name at the end.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		string GetPathFromFilePath(string filePath);

		/// <summary>
		/// Return the path without the file name
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		string GetFileNameFromFilePath(string filePath);

		/// <summary>
		/// Indicates whether a specific path exists or not.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		bool PathExists(string path);

		/// <summary>
		/// Indicates whether a specific file exists or not.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		bool FileExists(string filePath);

		/// <summary>
		/// Load up a stream given a path.
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		Stream LoadStream(string filePath);

		/// <summary>
		/// Load an xml document form the given path
		/// </summary>
		/// <param name="filePath"></param>
		/// <returns></returns>
		XmlDocument LoadXmlDocument(string filePath);

		/// <summary>
		/// This method will DeSerialize an object from the filepath specified.
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="type">Type of object expected to be deserialized</param>
		/// <returns></returns>
		object LoadObject(string filePath, Type type);

		/// <summary>
		/// This method will allow a standard stream to be stored
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="stream"></param>
		void SaveStream(string filePath, Stream stream);

		/// <summary>
		/// This method will store an xml document to the path specified.
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="document"></param>
		void SaveXmlDocument(string filePath, XmlDocument document);

		/// <summary>
		/// This method will serialize an object to the specified file path
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="graph"></param>
		void SaveObject(string filePath, object graph);

		/// <summary>
		/// This method will return a list of filenames in the given path
		/// </summary>
		/// <param name="path"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		string[] GetFileNames(string path, string filter);

		/// <summary>
		/// This method will return a list of child path entries for the specified path.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		string[] GetPathNames(string path, string filter);

		/// <summary>
		/// Delete the file with the specified file path.
		/// </summary>
		/// <param name="path"></param>
		void DeleteFile(string path);

		/// <summary>
		/// Delete the specified path, either entirely, or just the last path entry.
		/// </summary>
		/// <param name="path"></param>
		/// <param name="recursive">Delete all files below this path</param>
		void DeletePath(string path, bool recursive);

		/// <summary>
		/// This method will allow the creation of a full path
		/// </summary>
		/// <param name="path"></param>
		void CreatePath(string path);

		/// <summary>
		/// This will allow a file on the hard drive to be imported into the storage provider.
		/// It will NOT be deleted from the hard-drive once imported, unless deleteWhenImported is set to true
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="fileSystemPath"></param>
		/// <param name="deleteWhenImported"></param>
		void ImportFile(string filePath, string fileSystemPath, bool deleteWhenImported);

		/// <summary>
		/// Export a file from the provider to the file system path supplied. If deleteWhenExported is set to true, 
		/// then the provider copy of the file will be deleted once the export is completed.
		/// </summary>
		/// <param name="filePath"></param>
		/// <param name="fileSystemPath"></param>
		/// <param name="deleteWhenExported"></param>
		void ExportFile(string filePath, string fileSystemPath, bool deleteWhenExported);

	}
}

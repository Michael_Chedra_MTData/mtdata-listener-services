using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys;
using MTData.Sys.Services;
using MTData.Sys.Config;

namespace MTData.Sys.Services.Storage
{
	/// <summary>
	/// This is a service that provdes storage funcitonality through a number of providers.
	/// </summary>
	public class StorageService : ServiceBaseClass, IStorageService
	{
		public StorageService()
			: base("StorageService")
		{

		}

		public static IStorageService GetStorageService()
		{
			return GetStorageService("StorageService");
		}

		public static IStorageService GetStorageService(string name)
		{
			return GetStorageService(ServicesList.Instance, name);
		}

		public static IStorageService GetStorageService(ServicesList servicesList, string name)
		{
			IService service = servicesList[name];
			if (service == null)
				throw new ServiceNotFoundException(name);
			if (!(service is IStorageService))
				throw new ServiceInterfaceNotSupportedException(name, "IStorageService");
			return service as IStorageService;
		}

		/// <summary>
		/// Return the converted and verified Storage Provider.
		/// </summary>
		/// <param name="provider"></param>
		/// <returns></returns>
		private IStorageProvider GetStorageProvider(string provider)
		{
			IProvider providerImplementation = base.GetProvider(provider);
			if (providerImplementation == null)
				throw new ServiceProviderNotFoundException(Name, provider);
			if (!(providerImplementation is IStorageProvider))
				throw new ProviderInterfaceNotSupportedException(Name, "IStorageProvider", provider);

			return providerImplementation as IStorageProvider;
		}

		#region IStorageService Members

		/// <summary>
		/// Return the path without the file name
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public string GetPathFromFilePath(string provider, string filePath)
		{
			return GetStorageProvider(provider).GetPathFromFilePath(filePath);
		}

		/// <summary>
		/// Return the path without the file name
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public string GetFileNameFromFilePath(string provider, string filePath)
		{
			return GetStorageProvider(provider).GetFileNameFromFilePath(filePath);
		}

		/// <summary>
		/// Load up a stream from the filepath provided
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public System.IO.Stream LoadStream(string provider, string filePath)
		{
			return GetStorageProvider(provider).LoadStream(filePath);	
		}

		/// <summary>
		/// Load up an xml document from the file Path provided
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public System.Xml.XmlDocument LoadXmlDocument(string provider, string filePath)
		{
			return GetStorageProvider(provider).LoadXmlDocument(filePath);
		}

		/// <summary>
		/// This method will DeSerialize an object from the filepath specified.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <param name="type">Type of object to be deserialized</param>
		/// <returns></returns>
		public object LoadObject(string provider, string filePath, Type type)
		{
			return GetStorageProvider(provider).LoadObject(filePath, type);
		}

		/// <summary>
		/// Save off the supplied stream to the filepath provided.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <param name="stream"></param>
		public void SaveStream(string provider, string filePath, System.IO.Stream stream)
		{
			GetStorageProvider(provider).SaveStream(filePath, stream);
		}

		/// <summary>
		/// Save off the xml document to the file path provided
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <param name="document"></param>
		public void SaveXmlDocument(string provider, string filePath, System.Xml.XmlDocument document)
		{
			GetStorageProvider(provider).SaveXmlDocument(filePath, document);
		}

		/// <summary>
		/// This method will serialize an object to the specified file path
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <param name="graph"></param>
		public void SaveObject(string provider, string filePath, object graph)
		{
			GetStorageProvider(provider).SaveObject(filePath, graph);
		}

		/// <summary>
		/// Get a list of files available at this location
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="path"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		public string[] GetFileNames(string provider, string path, string filter)
		{
			return GetStorageProvider(provider).GetFileNames(path, filter);
		}

		/// <summary>
		/// Return the list of path offsets from the current location
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="path"></param>
		/// <param name="filter"></param>
		/// <returns></returns>
		public string[] GetPathNames(string provider, string path, string filter)
		{
			return GetStorageProvider(provider).GetPathNames(path, filter);
		}

		/// <summary>
		/// Delete a specific file form the storage
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		public void DeleteFile(string provider, string filePath)
		{
			GetStorageProvider(provider).DeleteFile(filePath);
		}

		/// <summary>
		/// Delete the directory at the specified path, and all recursive files and path if neccessary
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="path"></param>
		/// <param name="recursive"></param>
		public void DeletePath(string provider, string path, bool recursive)
		{
			GetStorageProvider(provider).DeletePath(path, recursive);
		}

		/// <summary>
		/// Indicates whether a specific path exists or not.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="path"></param>
		/// <returns></returns>
		public bool PathExists(string provider, string path)
		{
			return GetStorageProvider(provider).PathExists(path);
		}

		/// <summary>
		/// Indicates whether a specific file exists or not.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <returns></returns>
		public bool FileExists(string provider, string filePath)
		{
			return GetStorageProvider(provider).FileExists(filePath);
		}

		/// <summary>
		/// This method will allow the creation of a full path
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="path"></param>
		public void CreatePath(string provider, string path)
		{
			GetStorageProvider(provider).CreatePath(path);
		}

		/// <summary>
		/// This will allow a file on the hard drive to be imported into the storage provider.
		/// It will NOT be deleted from the hard-drive once imported, unless deleteWhenImported is set to true
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <param name="fileSystemPath"></param>
		/// <param name="deleteWhenImported"></param>
		public void ImportFile(string provider, string filePath, string fileSystemPath, bool deleteWhenImported)
		{
			GetStorageProvider(provider).ImportFile(filePath, fileSystemPath, deleteWhenImported);
		}

		/// <summary>
		/// Export a file from the provider to the file system path supplied. If deleteWhenExported is set to true, 
		/// then the provider copy of the file will be deleted once the export is completed.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="filePath"></param>
		/// <param name="fileSystemPath"></param>
		/// <param name="deleteWhenExported"></param>
		public void ExportFile(string provider, string filePath, string fileSystemPath, bool deleteWhenExported)
		{
			GetStorageProvider(provider).ExportFile(filePath, fileSystemPath, deleteWhenExported);
		}

		#endregion
	}
}

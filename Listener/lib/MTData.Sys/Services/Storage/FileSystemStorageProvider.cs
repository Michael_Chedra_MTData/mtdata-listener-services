using System;
using System.IO;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Services;
using log4net;

namespace MTData.Sys.Services.Storage
{
    /// <summary>
    /// This provider will support the StorageService by providing structured storage
    /// on the filesystem.
    /// </summary>
    public class FileSystemStorageProvider : ProviderBaseClass, IStorageProvider
    {
        private static ILog _log = LogManager.GetLogger(typeof(FileSystemStorageProvider));

        private string _basePath = "~";
        private int _bufferLowerLimit = 2048;
        private int _bufferUpperLimit = 1048576;

        private Dictionary<string, object> _lockObjects = new Dictionary<string, object>();

        public FileSystemStorageProvider()
            : this("~")
        {

        }

        public FileSystemStorageProvider(string basePath)
            : base("FileSystemProvider", string.Format("Structured Storage on Filesystem with BasePath '{0}'", basePath))
        {
            if (_log.IsInfoEnabled)
                _log.Info("Ctor : BasePath = " + ((basePath != null) ? basePath : "<null>"));

            _basePath = basePath;
            if (!basePath.EndsWith(@"\"))
                _basePath += @"\";

        }

        private object GetLockObject(string path)
        {
            object result = null;
            lock (_lockObjects)
                if (!_lockObjects.TryGetValue(path, out result))
                {
                    result = new object();
                    _lockObjects.Add(path, result);
                }
            return result;
        }

        #region Public Properties

        /// <summary>
        /// This is the lower limit for buffer size.
        /// </summary>
        public int BufferLowerLimit
        {
            get
            {
                return _bufferLowerLimit;
            }
            set
            {
                _bufferLowerLimit = value;
            }
        }

        /// <summary>
        /// This is the upper limit for buffer size.
        /// </summary>
        public int BufferUpperLimit
        {
            get
            {
                return _bufferUpperLimit;
            }
            set
            {
                _bufferUpperLimit = value;
            }
        }

        /// <summary>
        /// This is the base path on the filesystem for the provider.
        /// </summary>
        public string BasePath
        {
            get
            {
                return _basePath;
            }
            set
            {
                _basePath = value;
            }
        }

        #endregion

        /// <summary>
        /// Return the full path on the file system.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        private string GetFullPath(string path)
        {
            string basePath = _basePath;
            if (basePath.StartsWith(@"~\"))
                basePath = basePath.Replace(@"~\", Win32.GetBaseDirectory());

            return Path.Combine(basePath, path);
        }

        /// <summary>
        /// Calculate a buffer size between the proposed limits that will be divisible by 2 and will 
        /// deal wit the targetSize appropriately
        /// </summary>
        /// <param name="targetSize"></param>
        /// <returns></returns>
        private int GetBufferSize(long targetSize)
        {
            long result = targetSize / 4;
            result = (result / 1024) * 1024;
            if (result < _bufferLowerLimit)
                result = _bufferLowerLimit;
            if (result > _bufferUpperLimit)
                result = _bufferUpperLimit;
            if (result > Int32.MaxValue)
                return Int32.MaxValue;
            else
                return (int)result;
        }

        #region IStorageProvider Members

        /// <summary>
        /// Return the path without the file name at the end.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string GetPathFromFilePath(string filePath)
        {
            return System.IO.Path.GetDirectoryName(filePath);
        }

        /// <summary>
        /// Return the path without the file name
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public string GetFileNameFromFilePath(string filePath)
        {
            return System.IO.Path.GetFileName(filePath);
        }

        /// <summary>
        /// Create a stream with the required data in it.
        /// If the location does not exist, return null.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public System.IO.Stream LoadStream(string filePath)
        {
            MemoryStream result = null;
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                string fileName = GetFullPath(filePath);
                if (!File.Exists(fileName))
                    throw new FileNotFoundException(string.Format("Could not find stream '{0}' in provider {1}", filePath, base.Name), filePath);

                FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read);
                try
                {
                    int bufferSize = GetBufferSize(fileStream.Length);
                    if (fileStream.Length > Int32.MaxValue)
                        result = new MemoryStream(Int32.MaxValue);
                    else
                        result = new MemoryStream((int)fileStream.Length);

                    byte[] buffer = new byte[bufferSize];

                    int count = fileStream.Read(buffer, 0, buffer.Length);
                    while (count > 0)
                    {
                        result.Write(buffer, 0, count);
                        count = fileStream.Read(buffer, 0, buffer.Length);
                    }
                }
                finally
                {
                    fileStream.Close();
                }
                result.Position = 0;
            }
            return result;
        }

        /// <summary>
        /// Load an Xml Document form the path given
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public System.Xml.XmlDocument LoadXmlDocument(string filePath)
        {
            XmlDocument result = null;
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                string fileName = GetFullPath(filePath);
                if (!File.Exists(fileName))
                    throw new FileNotFoundException(string.Format("Could not find XmlDocument '{0}' in provider {1}", filePath, base.Name), filePath);

                result = new XmlDocument();
                result.Load(GetFullPath(filePath));
            }
            return result;
        }

        /// <summary>
        /// This method will DeSerialize an object from the filepath specified.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="type">The type of the object to be deserialized</param>
        /// <returns></returns>
        public object LoadObject(string filePath, Type type)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                string fileName = GetFullPath(filePath);
                if (!File.Exists(fileName))
                {
                    _log.ErrorFormat("Could not find object '{0}' in provider {1}", filePath, base.Name);
                    return null;
                }

                try
                {
                    using (FileStream fileStream = new FileStream(fileName, FileMode.Open, FileAccess.Read))
                    {
                        System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(type);
                        object result = serializer.Deserialize(fileStream);
                        return result;
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Could not load object '{0}' in provider {1}.  {2}", filePath, base.Name, ex.Message), ex);
                    return null;
                }
            }
        }

        /// <summary>
        /// Save the contents of the specified stream to the filePath given.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="stream"></param>
        public void SaveStream(string filePath, System.IO.Stream stream)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                CreatePath(GetPathFromFilePath(filePath));

                string fileName = GetFullPath(filePath);
                FileStream fileStream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                try
                {
                    int bufferSize = GetBufferSize(stream.Length);
                    byte[] buffer = new byte[bufferSize];
                    stream.Position = 0;
                    int count = stream.Read(buffer, 0, buffer.Length);
                    while (count > 0)
                    {
                        fileStream.Write(buffer, 0, count);
                        count = stream.Read(buffer, 0, buffer.Length);
                    }
                }
                finally
                {
                    fileStream.Close();
                }
            }
        }

        /// <summary>
        /// Save off the Xml document to the specified location in the fileSystem.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="document"></param>
        public void SaveXmlDocument(string filePath, System.Xml.XmlDocument document)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {

                CreatePath(GetPathFromFilePath(filePath));

                string fileName = GetFullPath(filePath);
                document.Save(fileName);
            }
        }

        /// <summary>
        /// This method will serialize an object to the specified file path
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="graph"></param>
        public void SaveObject(string filePath, object graph)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {

                CreatePath(GetPathFromFilePath(filePath));

                string fileName = GetFullPath(filePath);

                FileStream stream = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                try
                {
                    System.Xml.Serialization.XmlSerializer serializer = new System.Xml.Serialization.XmlSerializer(graph.GetType());
                    serializer.Serialize(stream, graph);

                    //	Binary serialization not supported in Compact Framework
                    //System.Runtime.Serialization.Formatters.Binary.BinaryFormatter formatter = new System.Runtime.Serialization.Formatters.Binary.BinaryFormatter();
                    //formatter.Serialize(stream, graph);
                }
                finally
                {
                    try { stream.Flush(); }
                    catch { }
                    try { stream.Close(); }
                    catch { }
                }
            }
        }

        /// <summary>
        /// Indicates whether a specific path exists or not.
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public bool PathExists(string path)
        {
            string fullPath = GetFullPath(path);
            return System.IO.Directory.Exists(fullPath);
        }

        /// <summary>
        /// Indicates whether a specific file exists or not.
        /// </summary>
        /// <param name="filePath"></param>
        /// <returns></returns>
        public bool FileExists(string filePath)
        {
            string fileName = GetFullPath(filePath);
            return System.IO.File.Exists(fileName);
        }

        /// <summary>
        /// REturn a list of the filenames within the specified path, given the filter provided.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public string[] GetFileNames(string path, string filter)
        {
            string fullPath = GetFullPath(path);
            if (!System.IO.Directory.Exists(fullPath))
                throw new DirectoryNotFoundException(string.Format("Could not find path '{0}' in provider {1}", path, base.Name));

            string[] files = System.IO.Directory.GetFiles(fullPath, filter);
            string[] results = new string[files.Length];
            for (int loop = 0; loop < files.Length; loop++)
                results[loop] = Path.GetFileName(files[loop]);
            return results;
        }

        /// <summary>
        /// REturn the list of sub-paths available from the path specified.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="filter"></param>
        /// <returns></returns>
        public string[] GetPathNames(string path, string filter)
        {
            string fullPath = GetFullPath(path);
            if (!System.IO.Directory.Exists(fullPath))
                throw new DirectoryNotFoundException(string.Format("Could not find path '{0}' in provider {1}", path, base.Name));

            string[] directories = System.IO.Directory.GetDirectories(fullPath, filter);
            string[] results = new string[directories.Length];
            for (int loop = 0; loop < directories.Length; loop++)
                results[loop] = Path.GetFileName(directories[loop]);
            return results;

        }

        /// <summary>
        /// Delete the file specified, if it exists.
        /// </summary>
        /// <param name="filePath"></param>
        public void DeleteFile(string filePath)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                string fileName = GetFullPath(filePath);
                if (System.IO.File.Exists(fileName))
                    System.IO.File.Delete(fileName);
            }
        }

        /// <summary>
        /// Delete the directory and all contents of the directory.
        /// </summary>
        /// <param name="path"></param>
        /// <param name="recursive">Delete all child files and paths</param>
        public void DeletePath(string path, bool recursive)
        {
            string fullPath = GetFullPath(path);
            if (System.IO.Directory.Exists(fullPath))
            {
                if (recursive)
                    System.IO.Directory.Delete(fullPath, recursive);
                else
                {
                    if ((System.IO.Directory.GetFiles(fullPath).Length > 0) ||
                        (System.IO.Directory.GetDirectories(fullPath).Length > 0))
                        throw new DirectoryNotEmptyException("The directory is not empty", path);
                    System.IO.Directory.Delete(fullPath);
                }
            }
        }

        /// <summary>
        /// Create the full path 
        /// </summary>
        /// <param name="path"></param>
        public void CreatePath(string path)
        {
            string fullPath = GetFullPath(path);
            if (!System.IO.Directory.Exists(fullPath))
                System.IO.Directory.CreateDirectory(fullPath);
        }

        /// <summary>
        /// This will allow a file on the hard drive to be imported into the storage provider.
        /// It will NOT be deleted from the hard-drive once imported, unless deleteWhenImported is set to true
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileSystemPath"></param>
        /// <param name="deleteWhenImported"></param>
        public void ImportFile(string filePath, string fileSystemPath, bool deleteWhenImported)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                if (!System.IO.File.Exists(fileSystemPath))
                    throw new FileNotFoundException(string.Format("Could not find file '{0}' to import into path '{1}' in provider {2}", fileSystemPath, filePath, base.Name), fileSystemPath);

                CreatePath(GetPathFromFilePath(filePath));

                string targetFileName = GetFullPath(filePath);

                System.IO.File.Copy(fileSystemPath, targetFileName, true);

                if (deleteWhenImported)
                    System.IO.File.Delete(fileSystemPath);
            }
        }

        /// <summary>
        /// Export a file from the provider to the file system path supplied. If deleteWhenExported is set to true, 
        /// then the provider copy of the file will be deleted once the export is completed.
        /// </summary>
        /// <param name="filePath"></param>
        /// <param name="fileSystemPath"></param>
        /// <param name="deleteWhenExported"></param>
        public void ExportFile(string filePath, string fileSystemPath, bool deleteWhenExported)
        {
            object lockObject = GetLockObject(filePath);
            lock (lockObject)
            {
                string sourceFileName = GetFullPath(filePath);
                if (!System.IO.File.Exists(sourceFileName))
                    throw new FileNotFoundException(string.Format("Could not find filePath '{0}' to export into directory '{1}' from provider {2}", filePath, fileSystemPath, base.Name), filePath);

                System.IO.File.Copy(sourceFileName, fileSystemPath, true);

                if (deleteWhenExported)
                    DeleteFile(filePath);
            }
        }

        #endregion
    }
}

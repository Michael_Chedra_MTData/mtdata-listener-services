using System;
using System.IO;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.Storage
{
	public class DirectoryNotEmptyException : DirectoryNotFoundException
	{
		private string _directory;

		public DirectoryNotEmptyException(string message, string directory) : this(message)
		{
			_directory = directory;
		}

		public DirectoryNotEmptyException(string message) : base(message)
		{

		}

		public string Directory
		{
			get
			{
				return _directory;
			}
		}
	}
}

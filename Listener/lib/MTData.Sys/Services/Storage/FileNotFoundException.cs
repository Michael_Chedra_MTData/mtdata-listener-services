using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.Storage
{
	/// <summary>
	/// This fileNotFound Exception extends the standard one for allow FileName
	/// as a parameter in WinCE.
	/// </summary>
	public class FileNotFoundException : System.IO.FileNotFoundException
	{
		public FileNotFoundException()
			: base()
		{

		}

		public FileNotFoundException(string message)
			: base(message)
		{

		}

#if NETCF || NETCF2
		public FileNotFoundException(string message, string fileName)
			: base(message)
		{
			_fileName = fileName;
		}

		private string _fileName;

		public string FileName
		{
			get
			{
				return _fileName;
			}
		}
#else
		public FileNotFoundException(string message, string fileName) : base(message, fileName)
		{

		}
#endif
	}
}

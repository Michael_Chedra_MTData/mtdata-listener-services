using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	/// <summary>
	/// This exception shoudl be raised when a service is required, but there is
	/// no corresponding service available from the service list.
	/// </summary>
	public class ServiceNotFoundException : ApplicationException
	{
		private string _serviceName;

		public ServiceNotFoundException() : this("Service Not Found exception", "")
		{

		}

		public ServiceNotFoundException(string serviceName) : this(string.Format("The service '{0}' is not available in the ServicesList", (serviceName == null)?"<NULL>":serviceName), serviceName)
		{

		}

		public ServiceNotFoundException(string message, string serviceName)
			: base(message)
		{
			_serviceName = serviceName;
		}

		public string ServiceName
		{
			get
			{
				return _serviceName;
			}
		}
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
    /// <summary>
    /// A Collection of all the providers for a service
    /// </summary>
    public class ProviderCollection : IEnumerable
    {
        #region private fields
        /// <summary>
        /// dictionary containing the providers for this service
        /// </summary>
        private Dictionary<string, IProvider> _providers;
        #endregion

        #region public properties
        /// <summary>
        /// get a provider of a given name
        /// </summary>
        /// <param name="name">the name of the provider</param>
        /// <returns>the Provider</returns>
        /// <exception cref="KeyNotFoundException">if the provider does not exist in the collection</exception>
        public IProvider this[string name]
        {
            get
            {
                return _providers[name];
            }
        }

        #endregion

        #region constructor
        /// <summary>
        /// create a new providers collection
        /// </summary>
        public ProviderCollection()
        {
            _providers = new Dictionary<string, IProvider>();
        }
        #endregion

        #region public methods
        /// <summary>
        /// Add a provider
        /// </summary>
        /// <param name="provider">the provider to add to the collection</param>
        public void Add(IProvider provider)
        {
            if (!_providers.ContainsKey(provider.Name))
            {
                _providers.Add(provider.Name, provider);
            }
        }

        /// <summary>
        /// remove a provider
        /// </summary>
        /// <param name="name">the name of the provider to remove</param>
        public void Remove(string name)
        {
            if (_providers.ContainsKey(name))
            {
                _providers.Remove(name);
            }
        }

		/// <summary>
		/// Return the number of providers in the list.
		/// </summary>
		public int Count
		{
			get
			{
				return _providers.Count;
			}
		}

        /// <summary>
        /// Does this provider collection contain the provider
        /// </summary>
        /// <param name="providerName">the name of the provider</param>
        /// <returns>true if the provider is in the collection</returns>
        public bool ContainsProvider(string providerName)
        {
            return _providers.ContainsKey(providerName);
        }
        #endregion

        #region IEnumerable Members
        /// <summary>
        /// return an enumerator whcih enumerates through all the providers
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return _providers.Values.GetEnumerator();
        }

        #endregion
    }
}

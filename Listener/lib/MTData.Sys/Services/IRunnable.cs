using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	/// <summary>
	/// Implementing this interface allows a Service, or a Service Provider to be started once it 
	/// has been initialised correctly.
	/// </summary>
	public interface IRunnable
	{
		/// <summary>
		/// This method will be called to start execution of the provider.
		/// </summary>
		void Run();
	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	/// <summary>
	/// This class will implement the IVersion component of the IProvider and IService interfaces.
	/// </summary>
	public abstract class VersionedBaseClass : IVersion
	{
		/// <summary>
		/// This method returns the Major Version of the containing Assembly.
		/// </summary>
		/// <returns></returns>
		private int GetVersionMajorNumber()
		{
			return GetType().Assembly.GetName().Version.Major;
		}

		/// <summary>
		/// This method returns the Minor Version of the containing Assembly.
		/// </summary>
		/// <returns></returns>
		protected int GetVersionMinorNumber()
		{
			return GetType().Assembly.GetName().Version.Minor;
		}

		#region IVersion Members

		string IVersion.VersionNumber
		{
			get
			{
				return string.Format("{0}.{1}", GetVersionMajorNumber(), GetVersionMinorNumber());
			}
		}

		int IVersion.MajorNumber
		{
			get
			{
				return GetVersionMajorNumber();
			}
		}

		int IVersion.MinorNumber
		{
			get
			{
				return GetVersionMinorNumber();
			}
		}

		DateTime IVersion.Date
		{
			get
			{
				String path = GetType().Assembly.ManifestModule.FullyQualifiedName;
				System.IO.FileInfo f = new System.IO.FileInfo(path);
				return f.CreationTime;
			}
		}

		#endregion
	}
}

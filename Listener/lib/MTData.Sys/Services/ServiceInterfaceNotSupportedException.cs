using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	public class ServiceInterfaceNotSupportedException : ServiceNotFoundException
	{
		private string _interfaceName;

		public ServiceInterfaceNotSupportedException() : this("Interface not supported by Service", "", "")
		{

		}

		public ServiceInterfaceNotSupportedException(string serviceName, string interfaceName)
			: this(string.Format("The service '{0}' does not support the interface '{1}'", (serviceName == null)?"<NULL>":serviceName, (interfaceName == null)?"<NULL>":interfaceName), serviceName, interfaceName)
		{
			
		}

		public ServiceInterfaceNotSupportedException(string message, string serviceName, string interfaceName)
			: base(message, serviceName)
		{
			_interfaceName = interfaceName;
		}

		public string InterfaceName
		{
			get
			{
				return _interfaceName;
			}
		}
	}
}

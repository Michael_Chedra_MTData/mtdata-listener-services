using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;

namespace MTData.Sys.Services
{
    /// <summary>
    /// The base interface for all services
    /// </summary>
    public interface IService: IDisposable, IVersion, IEnumerable
    {
		/// <summary>
		/// This is the name of the service
		/// </summary>
		string Name
		{
			get;
		}

        /// <summary>
        /// Add a provider to the service
        /// </summary>
        /// <param name="provider">a provider</param>
        void AddProvider(IProvider provider);

		/// <summary>
		/// initialise the provider
		/// </summary>
		/// <param name="name">the name of the provider</param>
		/// <param name="configSection">the configuration settings for this provider</param>
		void Initialise(string name, IConfigSection configSection);

    }
}

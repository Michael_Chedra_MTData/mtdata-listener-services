using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using log4net;
using MTData.Sys.Config;

namespace MTData.Sys.Services
{
    /// <summary>
    /// Holds all the Services configured.
    /// Note that if the ServicesList is configured programmatically, then the caller is 
    /// responsible for calling the Start() method, which then iterates through the services and providers
    /// and ensures that if they support IRunnable, that the Run() method is called on them to start operation.
    /// The Dispose() method is used for cleanup in all Services and Providers.
    /// </summary>
    public class ServicesList : IDisposable, IEnumerable
    {
        #region Static Fields

        /// <summary>
        /// This is the stack of instances available through the instance property
        /// </summary>
        private static Stack<ServicesList> _instanceStack = new Stack<ServicesList>();

        /// <summary>
        /// This method allows another serviceslist to be pushed in on top of the current
        /// global instance.
        /// </summary>
        /// <param name="config"></param>
        public static void Push(ServicesList list)
        {
            _instanceStack.Push(list);
        }

        /// <summary>
        /// This method will pop a servicesList instance off of the globally available servicelist stack.
        /// </summary>
        /// <returns></returns>
        public static ServicesList Pop()
        {
            if (_instanceStack.Count > 0)
                return _instanceStack.Pop();
            else
                return null;
        }

        /// <summary>
        /// Initialise a service list from an IConfigSection. This is generally used for WinCE
        /// configuration files.		
        /// </summary>
        /// <param name="configSection"></param>
        /// <returns></returns>
        public static ServicesList InitialiseFromConfig(IConfigSection configSection)
        {
            ServicesList instance;
            if (_instanceStack.Count == 0)
            {
                instance = new ServicesList();
                _instanceStack.Push(instance);
            }
            else
            {
                instance = _instanceStack.Peek();
            }
            instance.Initialise(configSection);
            return instance;
        }

        /// <summary>
        /// This static property allows global access to the currently active ServicesList
        /// </summary>
        public static ServicesList Instance
        {
            get
            {
                if (_instanceStack.Count == 0)
                {
                    lock (typeof(ServicesList))
                    {
                        if (_instanceStack.Count == 0)
                        {
                            ServicesList list = CreateFromConfig(Config.Config.ConfigurationManager, typeof(ServicesList).FullName.Replace(".", "/"), null);
                            _instanceStack.Push(list);
                            list.Start();
                        }
                    }
                }
                return _instanceStack.Peek();
            }
        }


        #endregion

        #region private fields
        private static readonly ILog _log = LogManager.GetLogger(typeof(ServicesList));

        /// <summary>
        /// hashtable containing all the services, key is service name
        /// </summary>
        private Dictionary<string, IService> _services;
        #endregion

        #region constructor
        /// <summary>
        /// create an new ServicesList
        /// </summary>
        public ServicesList()
        {
            _services = new Dictionary<string, IService>();
        }
        #endregion

        #region public methods
        /// <summary>
        /// initailise the services from the config file
        /// </summary>
        /// <param name="configSection">the section of the config file which contains the services</param>
        public void Initialise(IConfigSection configSection)
        {
            if (configSection == null)
            {
                return;
            }

            //iterate though each service
            MTData.Sys.Loader.Loader loader = new MTData.Sys.Loader.Loader();
            IConfigSection[] serviceConfigs = configSection.GetSections();
            foreach (IConfigSection serviceConfig in serviceConfigs)
            {
                _log.DebugFormat("Loading service {0} from config", serviceConfig.Key);
                try
                {
                    //check to see if service already exists
                    IService service;
                    if (_services.ContainsKey(serviceConfig.Key))
                    {
                        service = _services[serviceConfig.Key];

                        //	verify that the new serviceConfig is of the same type as the original
                        string className = serviceConfig["ClassName", null];
                        Type type = Type.GetType(className);
                        if (type != service.GetType())
                            throw new InvalidOperationException(string.Format("Cannot merge named service '{0}'. The Service class-names differ", serviceConfig.Key));
                    }
                    else
                    {
                        //Create the service
                        service = (IService)loader.Create(this, serviceConfig["ClassName", null]);
                        _services.Add(serviceConfig.Key, service);
                    }

                    if (service != null)
                    {
                        service.Initialise(serviceConfig.Key, serviceConfig);
                        //create all the providers and set them on the service
                        CreateProvidersForService(service, serviceConfig, loader);
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat("Error loading service {0}: {1}", serviceConfig.Key, ex.Message);
                }
            }
        }

        /// <summary>
        /// get a service
        /// </summary>
        /// <param name="id">the name of the service</param>
        /// <returns>the service or null if not found</returns>
        public IService this[string name]
        {
            get
            {
                if (_services.ContainsKey(name))
                {
                    return _services[name];
                }
                return null;
            }
        }

        /// <summary>
        /// Return the number of services available.
        /// </summary>
        public int Count
        {
            get
            {
                return _services.Count;
            }
        }

        /// <summary>
        /// This method will iterate through all Services and all Providers for these services.
        /// If any of the services or providers support the IRunnable interface, then the Run
        /// method will be executed on them. This will occur automatically for the Instance property
        /// upon initial instantiation, but will need to be Explicitly called where the code creates
        /// a ServicesList instance programmatically.
        /// </summary>
        public void Start()
        {
            foreach (IService service in _services.Values)
            {
                try
                {
                    IRunnable runnable = service as IRunnable;
                    if (runnable != null)
                    {
                        runnable.Run();
                    }
                }
                catch (Exception ex)
                {
                    _log.Error(string.Format("Error initialise Service: {0}, {1}", service.Name, ex.Message), ex);
                }

                foreach (IProvider provider in service)
                {
                    try
                    {
                        IRunnable runnable = provider as IRunnable;

                        if (runnable != null)
                            runnable.Run();
                    }
                    catch (Exception ex)
                    {
                        _log.Error(string.Format("Error initialise Service: {0}, Provider: {1}, {2}", service.Name, provider.Name, ex.Message), ex);
                    }
                }

            }
        }

        /// <summary>
        /// This method will merge the ServicesList instance passed in
        /// into this instance of the ServicesList
        /// </summary>
        /// <param name="list"></param>
        public void Merge(ServicesList list)
        {
            foreach (KeyValuePair<string, IService> serviceEntry in list._services)
            {
                IService targetService = null;
                if (this._services.TryGetValue(serviceEntry.Key, out targetService))
                {
                    //	if a service with the same name exists in both servicelists, 
                    //	then verify the service types are the same, and then merge the providers.
                    if (serviceEntry.Value.GetType() != targetService.GetType())
                        throw new InvalidOperationException(string.Format("The service entries for service '{0}' cannot be merged as they are different types", serviceEntry.Key));

                    foreach (IProvider provider in serviceEntry.Value)
                        targetService.AddProvider(provider);
                }
                else
                {
                    this._services.Add(serviceEntry.Key, serviceEntry.Value);
                }
            }


        }

        #endregion

        #region private methods
        /// <summary>
        /// Create all the providers configured for a service
        /// </summary>
        /// <param name="service">the service to create the providers for</param>
        /// <param name="serviceConfig">the part of the config file which describes the service</param>
        /// <param name="loader">A loader helper class</param>
        private void CreateProvidersForService(IService service, IConfigSection serviceConfig, MTData.Sys.Loader.Loader loader)
        {
            //iterate though each provider
            IConfigSection providerSection = serviceConfig.GetConfigSection("Providers");

            IConfigSection[] providerConfigs = providerSection.GetSections();
            foreach (IConfigSection providerConfig in providerConfigs)
            {
                _log.DebugFormat("Loading provider {0} from config", providerConfig.Key);
                try
                {
                    //create the provider
                    IProvider provider = (IProvider)loader.Create(providerConfig, providerConfig["ClassName", null]);
                    if (provider != null)
                    {
                        //initialise the provider and add it to the collection
                        provider.Initialise(providerConfig.Key, providerConfig);
                        service.AddProvider(provider);
                    }
                }
                catch (Exception ex)
                {
                    _log.ErrorFormat("Error loading provider {0}: {1}", providerConfig.Key, ex.Message);
                }
            }
        }
        #endregion


        #region IDisposable Members
        /// <summary>
        /// dispose of all the services
        /// </summary>
        public void Dispose()
        {
            if (_services != null)
            {
                foreach (IService service in _services.Values)
                {
                    if (service is IDisposable)
                    {
                        try
                        {
                            ((IDisposable)service).Dispose();
                        }
                        catch
                        {
                        }
                    }
                }

                _services.Clear();
                _services = null;
            }
        }

        #endregion

        #region IEnumerable Members
        /// <summary>
        /// return an enumerator whcih enumerates through all the services
        /// </summary>
        /// <returns></returns>
        public IEnumerator GetEnumerator()
        {
            return _services.Values.GetEnumerator();
        }

        #endregion

        /// <summary>
        /// This method will create and instantiate the list of services from the config File
        /// </summary>
        /// <param name="config"></param>
        /// <param name="path"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public static ServicesList CreateFromConfig(Config.Config config, string path, object context)
        {
            string[] serviceNames = config.GetSectionNames(path);

            Loader.Loader loader = new MTData.Sys.Loader.Loader();

            ServicesList list = new ServicesList();
            foreach (string serviceName in serviceNames)
            {
                string configPath = string.Format("{0}/{1}", path, serviceName);
                IService service = (IService)config.GetSection(configPath, context);
                list._services.Add(service.Name, service);

                //	if the service is not configurable, and the provider count is zero, attempt to load any providers..
                XmlNode configNode = config.GetSectionConfigNode(configPath);
                if (configNode != null)
                {
                    XmlNodeList providerNodes = configNode.SelectNodes("Provider");
                    foreach (XmlNode providerNode in providerNodes)
                    {
                        IProvider provider = (IProvider)loader.Create(context, providerNode);
                        service.AddProvider(provider);
                    }
                }
            }

            return list;
        }


    }
}

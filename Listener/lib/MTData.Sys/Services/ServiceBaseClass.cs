using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;

namespace MTData.Sys.Services
{
	public abstract class ServiceBaseClass : VersionedBaseClass, IService
	{
		/// <summary>
		/// This is the name of the service.
		/// </summary>
		protected string _name = "ServiceBaseClass";

		/// <summary>
		/// List of providers that will provide the functionality.
		/// </summary>
		private ProviderCollection _providers = new ProviderCollection();

		/// <summary>
		/// This holds the name of the default provider.
		/// </summary>
		private string _defaultProvider = null;

		/// <summary>
		/// Return the named provider.
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		protected IProvider GetProvider(string name)
		{
			return _providers[name];
		}

		/// <summary>
		/// This named constructor is the only one, which ensures that the loader class
		/// cannot instantiate it in error.
		/// </summary>
		/// <param name="name"></param>
		public ServiceBaseClass(string name)
		{
			_name = name;
		}

		#region Properties

		/// <summary>
		/// Public property to allow access to the name field.
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		/// <summary>
		/// This is the default provider to be used for the functionality
		/// </summary>
		public string DefaultProvider
		{
			get
			{
				return _defaultProvider;
			}
			set
			{
				_defaultProvider = value;
			}
		}

		#endregion

		protected virtual void AddProvider(IProvider provider)
		{
			if (_defaultProvider == null)
				_defaultProvider = provider.Name;
			_providers.Add(provider);
		}

		#region IService Members

		string IService.Name
		{
			get
			{
				return _name;
			}
		}

		void IService.AddProvider(IProvider provider)
		{
			AddProvider(provider);
		}

		void IService.Initialise(string name, MTData.Sys.Config.IConfigSection configSection)
		{

		}

		#endregion

		#region IDisposable Members

		void IDisposable.Dispose()
		{
			foreach (IProvider provider in _providers)
				if (provider is IDisposable)
					((IDisposable)provider).Dispose();
		}

		#endregion

		#region IEnumerable Members

		System.Collections.IEnumerator System.Collections.IEnumerable.GetEnumerator()
		{
			return _providers.GetEnumerator();
		}

		#endregion

	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	public class ServiceProviderNotFoundException : ServiceNotFoundException
	{
		private string _providerName;

		public ServiceProviderNotFoundException() : this("Provider not found for Service", "", "")
		{

		}

		public ServiceProviderNotFoundException(string message, string serviceName, string providerName) : base(message, serviceName)
		{
			_providerName = providerName;
		}

		public ServiceProviderNotFoundException(string serviceName, string providerName)
			: this(string.Format("Provider '{0}' not found for Service '{1}'", (providerName == null)?"<NULL>":providerName, (serviceName == null)?"<NULL>":serviceName), serviceName, providerName)
		{
			
		}
	}
}

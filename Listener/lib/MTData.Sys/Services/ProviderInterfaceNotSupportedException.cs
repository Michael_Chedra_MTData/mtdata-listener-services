using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	public class ProviderInterfaceNotSupportedException : ServiceInterfaceNotSupportedException
	{
		private string _providerName = null;

		public ProviderInterfaceNotSupportedException()
			: this("Interface not supported by Service Provider", "", "", "")
		{

		}

		public ProviderInterfaceNotSupportedException(string serviceName, string interfaceName, string providerName)
			: this(string.Format("The provider '{0}' for service '{1}' does not support the interface '{2}'", (providerName == null)?"<NULL>":providerName, (serviceName == null) ? "<NULL>" : serviceName, (interfaceName == null) ? "<NULL>" : interfaceName), serviceName, interfaceName, providerName)
		{
			_providerName = providerName;
		}

		public ProviderInterfaceNotSupportedException(string message, string serviceName, string interfaceName, string providerName)
			: base(message, serviceName, interfaceName)
		{
			_providerName = providerName;
		}

		public string ProviderName
		{
			get
			{
				return _providerName;
			}
		}
	}
}

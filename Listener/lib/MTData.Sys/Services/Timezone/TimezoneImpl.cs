using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Timezone;

namespace MTData.Sys.Services.Timezone
{
	/// <summary>
	/// This class is an implementation of a timeZone as returned by the TimeZone service
	/// </summary>
	public class TimezoneImpl : ITimezone
	{
		/// <summary>
		/// This is the name of the timezone
		/// </summary>
		private readonly string _name;
		
		/// <summary>
		/// This is the id of the timezone
		/// </summary>
		private readonly int _id;

		/// <summary>
		/// This is the offset from utc.
		/// </summary>
		private readonly int _offset;

		/// <summary>
		/// This is a list of the daylight savings.
		/// </summary>
		private readonly IEnumerable<TimezoneDaylightSavingsImpl> _daylightSavings;

		/// <summary>
		/// This constructor prepares the class for use.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="name"></param>
		/// <param name="offset"></param>
		/// <param name="daylightSavings"></param>
		public TimezoneImpl(int id, string name, int offset, IEnumerable<TimezoneDaylightSavingsImpl> daylightSavings)
		{
			_id = id;
			_name = name;
			_offset = offset;
			_daylightSavings = daylightSavings;
		}

		#region ITimezone Members

		/// <summary>
		/// This is the id of the timezone.
		/// </summary>
		public int ID
		{
			get
			{
				return _id;
			}
		}

		/// <summary>
		/// This is the name of the timezone
		/// </summary>
		public string Name
		{
			get
			{
				return _name;
			}
		}

		/// <summary>
		/// This is the standard offset from Utc Time
		/// </summary>
		public int Offset
		{
			get
			{
				return _offset;
			}
		}

		/// <summary>
		/// Convert the date to Utc.. Note that offset is added to localtime to get utctime.
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public DateTime ToUtc(DateTime dateTimeLocal)
		{
            foreach (var period in _daylightSavings)
            {
                int dstDiff = period.Offset - Offset;
                DateTime startDsLocal = period.StartDate.AddMinutes(Offset * -1);
                DateTime endDsLocal = period.EndDate.AddMinutes(Offset * -1);
                DateTime gapEndLocal = startDsLocal.AddMinutes(dstDiff * -1);
                if (dateTimeLocal >= startDsLocal)
                {
                    // Check if time falls inside spring forward gap
                    if (dateTimeLocal <= gapEndLocal)
                    {
                        return new DateTime(period.StartDate.Ticks, DateTimeKind.Utc);
                    }
                    else if (dateTimeLocal <= endDsLocal)
                    {
                        return new DateTime(dateTimeLocal.AddMinutes(period.Offset).Ticks, DateTimeKind.Utc);
                    }
                    // Check if time falls inside fallback
                    else if (dateTimeLocal < endDsLocal.AddMinutes(dstDiff * -1))
                    {
                        // local time maps to 2 possible utc times!
                        // strategy is to choose the first occurance
                        return new DateTime(dateTimeLocal.AddMinutes(period.Offset).Ticks, DateTimeKind.Utc);
                    }
                }
            }
            return new DateTime(dateTimeLocal.AddMinutes(Offset).Ticks, DateTimeKind.Utc);
        }

		/// <summary>
		/// Convert date from Utc. Note that offset is added to localtime to get Utc time.
		/// </summary>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		public DateTime FromUtc(DateTime dateTime)
		{
            int offset = GetOffsetAtTime(dateTime);
            return new DateTime(dateTime.AddMinutes(offset * -1).Ticks, DateTimeKind.Local);
        }

        private int GetOffsetAtTime(DateTime dateTimeUtc)
        {
            // check if falls within a daylight savings period
            foreach (var period in _daylightSavings)
            {
                if (dateTimeUtc >= period.StartDate && dateTimeUtc < period.EndDate)
                {
                    return period.Offset;
                }
            }
            return Offset;
        }

        #endregion
    }
}

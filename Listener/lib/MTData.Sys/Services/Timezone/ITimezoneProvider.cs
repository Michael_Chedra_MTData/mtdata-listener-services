using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.Timezone
{
	/// <summary>
	/// This interface will be implemented by all Timezone providers.
	/// The provider will be responsible for loading the timezones into memory from whatever 
	/// source is configured.
	/// </summary>
	public interface ITimezoneProvider : ITimezoneService
	{
		/// <summary>
		/// This is the name of the provider implementation.
		/// </summary>
		string Name
		{
			get;
		}
	}
}

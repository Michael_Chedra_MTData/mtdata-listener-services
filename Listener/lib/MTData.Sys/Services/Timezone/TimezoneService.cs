using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Services;
using MTData.Sys.Config;

namespace MTData.Sys.Services.Timezone
{
	/// <summary>
	/// This class will provide the timezone functionality for the ITimezoneService.
	/// </summary>
	public class TimezoneService : ServiceBaseClass, ITimezoneService
	{
		public TimezoneService()
			: base("TimezoneService")
		{

		}

		public static ITimezoneService GetTimezoneService()
		{
			return GetTimezoneService("TimezoneService");
		}

		public static ITimezoneService GetTimezoneService(string name)
		{
			return GetTimezoneService(ServicesList.Instance, name);
		}

		public static ITimezoneService GetTimezoneService(ServicesList servicesList, string name)
		{
			IService service = servicesList[name];
			if (service == null)
				throw new ServiceNotFoundException(name);
			if (!(service is ITimezoneService))
				throw new ServiceInterfaceNotSupportedException(name, "ITimezoneService");
			return service as ITimezoneService;
		}

		/// <summary>
		/// Return the default provider typecast as the appropriate interface.
		/// </summary>
		/// <returns></returns>
		private ITimezoneProvider GetTimezoneProvider()
		{
			IProvider provider = base.GetProvider(base.DefaultProvider);
			if (provider == null)
				throw new ServiceProviderNotFoundException(Name, base.DefaultProvider);
			if (!(provider is ITimezoneProvider))
				throw new ProviderInterfaceNotSupportedException(Name, "ITimezoneProvider", base.DefaultProvider);

			return provider as ITimezoneProvider;
		}

		#region ITimezoneService Members

		/// <summary>
		/// REturn the timezone based on it's timezoneID
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		MTData.Sys.Timezone.ITimezone ITimezoneService.GetTimezoneByID(int id)
		{
			return GetTimezoneProvider().GetTimezoneByID(id);
		}

		/// <summary>
		/// Return the timezone based on its Timezone Name
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		MTData.Sys.Timezone.ITimezone ITimezoneService.GetTimezoneByName(string name)
		{
			return GetTimezoneProvider().GetTimezoneByName(name);
		}

		/// <summary>
		/// Convert a datetime to the appropriate utc time given the timezone name
		/// </summary>
		/// <param name="timezoneName"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ITimezoneService.ToUtc(string timezoneName, DateTime dateTime)
		{
			return GetTimezoneProvider().GetTimezoneByName(timezoneName).ToUtc(dateTime);
		}

		/// <summary>
		/// Convert a datetime from the appropriate utc time given the timezone name
		/// </summary>
		/// <param name="timezoneName"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ITimezoneService.FromUtc(string timezoneName, DateTime dateTime)
		{
			return GetTimezoneProvider().GetTimezoneByName(timezoneName).FromUtc(dateTime);
		}

		/// <summary>
		/// Convert a datetime to the appropriate utc time given the timezone id
		/// </summary>
		/// <param name="timezoneID"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ITimezoneService.ToUtc(int timezoneID, DateTime dateTime)
		{
			return GetTimezoneProvider().GetTimezoneByID(timezoneID).ToUtc(dateTime);
		}

		/// <summary>
		/// Convert a datetime from the appropriate utc time given the timezone id
		/// </summary>
		/// <param name="timezoneID"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ITimezoneService.FromUtc(int timezoneID, DateTime dateTime)
		{
			return GetTimezoneProvider().GetTimezoneByID(timezoneID).FromUtc(dateTime);
		}

		/// <summary>
		/// REturn a list of the available timezones
		/// </summary>
		/// <returns></returns>
		IList<MTData.Sys.Timezone.ITimezone> ITimezoneService.GetTimezones()
		{
			return GetTimezoneProvider().GetTimezones();
		}

		#endregion

	}
}

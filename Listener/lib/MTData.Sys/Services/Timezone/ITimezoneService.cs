using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Timezone;

namespace MTData.Sys.Services.Timezone
{
	/// <summary>
	/// This service will provide timezone funcitonality to all points of an application
	/// without the requirement to separately implement it.
	/// </summary>
	public interface ITimezoneService
	{
		/// <summary>
		/// Return the timezone given the id.
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
		ITimezone GetTimezoneByID(int id);

		/// <summary>
		/// Return the timezone given the name
		/// </summary>
		/// <param name="name"></param>
		/// <returns></returns>
		ITimezone GetTimezoneByName(string name);

		/// <summary>
		/// Given a timezone name, convert the date/time provided to Utc
		/// </summary>
		/// <param name="timezoneName"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ToUtc(string timezoneName, DateTime dateTime);

		/// <summary>
		/// Given the timezone name, convert the date/time provided from Utc to local for that timezone.
		/// </summary>
		/// <param name="timezoneName"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime FromUtc(string timezoneName, DateTime dateTime);

		/// <summary>
		/// Given a timezone ID, convert the date/time provided to Utc
		/// </summary>
		/// <param name="timezoneID"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime ToUtc(int timezoneID, DateTime dateTime);

		/// <summary>
		/// Given the timezone ID, convert the date/time provided from Utc to local for that timezone.
		/// </summary>
		/// <param name="timezoneID"></param>
		/// <param name="dateTime"></param>
		/// <returns></returns>
		DateTime FromUtc(int timezoneID, DateTime dateTime);

		/// <summary>
		/// Return a list fo all of the available timezones
		/// </summary>
		/// <returns></returns>
		IList<ITimezone> GetTimezones();
	}

}

#if !NETCF && !NETCF2
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Services.TagExpansion;

namespace MTData.Sys.Services.Timezone
{
    /// <summary>
    /// This class will provide a Sql implementation of the Timezone provider, and will load
    /// the data from a SqlServer database based upon the ConnectionStringTag property, which 
    /// will be retrieve from the ITagExpansionService, and the ActiveSchema which indicates what
    /// tablenames and field names to use.
    /// </summary>
    /// <remarks>This class is not supported in the Compact Framework as it relies upon the System.Data.SqlClient namespace</remarks>
    public class SqlTimezoneProvider : ProviderBaseClass, ITimezoneProvider
    {
        #region Timezone Schema

        private class TimezoneSchema
        {
            private string _timeZoneCommand = null;
            private CommandType _timeZoneCommandType = CommandType.Text;
            private string _daylightSavingsCommand = null;
            private CommandType _daylightSavingsCommandType = CommandType.Text;
            private string _timeZoneIDField = null;
            private string _timezoneNameField = null;
            private string _timezoneOffsetField = null;
            private string _daylightSavingsIDField = null;
            private string _daylightSavingsStartDateField = null;
            private string _daylightSavingsEndDateField = null;

            public TimezoneSchema(string timeZoneCommand,
                CommandType timeZoneCommandType,
                string daylightSavingsCommand,
                CommandType daylightSavingsCommandType,
                string timeZoneIDField,
                string timezoneNameField,
                string timezoneOffsetField,
                string daylightSavingsIDField,
                string daylightSavingsStartDateField,
                string daylightSavingsEndDateField)
            {
                _timeZoneCommand = timeZoneCommand;
                _timeZoneCommandType = timeZoneCommandType;
                _timeZoneIDField = timeZoneIDField;
                _timezoneNameField = timezoneNameField;
                _timezoneOffsetField = timezoneOffsetField;

                _daylightSavingsCommand = daylightSavingsCommand;
                _daylightSavingsCommandType = daylightSavingsCommandType;
                _daylightSavingsIDField = daylightSavingsIDField;
                _daylightSavingsStartDateField = daylightSavingsStartDateField;
                _daylightSavingsEndDateField = daylightSavingsEndDateField;
            }

            public string TimeZoneCommand
            {
                get
                {
                    return _timeZoneCommand;
                }
            }

            public CommandType TimeZoneCommandType
            {
                get
                {
                    return _timeZoneCommandType;
                }
            }

            public string DaylightSavingsCommand
            {
                get
                {
                    return _daylightSavingsCommand;
                }
            }

            public CommandType DaylightSavingsCommandType
            {
                get
                {
                    return _daylightSavingsCommandType;
                }
            }

            public string TimeZoneIDField
            {
                get
                {
                    return _timeZoneIDField;
                }
            }

            public string TimezoneNameField
            {
                get
                {
                    return _timezoneNameField;
                }
            }

            public string TimezoneOffsetField
            {
                get
                {
                    return _timezoneOffsetField;
                }
            }

            public string DaylightSavingsIDField
            {
                get
                {
                    return _daylightSavingsIDField;
                }
            }

            public string DaylightSavingsStartDateField
            {
                get
                {
                    return _daylightSavingsStartDateField;
                }
            }

            public string DaylightSavingsEndDateField
            {
                get
                {
                    return _daylightSavingsEndDateField;
                }
            }
        }

        #endregion

        /// <summary>
        /// Timezones indexed by id
        /// </summary>
        private Dictionary<int, TimezoneImpl> _timeZonesByID = null;

        /// <summary>
        /// Timezones indexed by name
        /// </summary>
        private Dictionary<string, TimezoneImpl> _timeZonesByName = null;

        /// <summary>
        /// This holds a list of the available schemas.
        /// </summary>
        private Dictionary<string, TimezoneSchema> _schemas = new Dictionary<string, TimezoneSchema>();

        /// <summary>
        /// This is the tag to pass to the ITagExpansionService to retreive the actual conneciton string.
        /// </summary>
        private string _connectionStringTag;

        /// <summary>
        /// Active Schema to use when loading the information.
        /// </summary>
        private string _activeSchema = "Taxi";

        public SqlTimezoneProvider() : base("SqlTimezoneProvider", "Sources Timezones from Sql Database")
        {
            _schemas.Add("Taxi", new TimezoneSchema(
                "usp_TimezonesRetrieveAll", CommandType.StoredProcedure,
                "usp_TimezoneDaylightSavingGet", CommandType.StoredProcedure,
                "TimezoneID", "Name", "Offset",
                "TimezoneID", "StartDate", "EndDate"));
            _schemas.Add("Tracking", new TimezoneSchema(
                "SELECT * FROM T_Timezones", CommandType.Text,
                "SELECT * FROM T_TimezoneDaylightSaving", CommandType.Text,
                "ID", "Name", "Offset",
                "TimezoneID", "StartDS", "EndDS"));

        }

        /// <summary>
        /// This tag identifies the tag to be passed to the ITagExpanderService to retrieve the connection string.
        /// This string is then uised to connect to the database to retrieve the data.
        /// </summary>
        public string ConnectionStringTag
        {
            get
            {
                return _connectionStringTag;
            }
            set
            {
                _connectionStringTag = value;
            }
        }

        /// <summary>
        /// This porperty allows the specification of the schema to use for the
        /// loading of the time information.
        /// </summary>
        public string ActiveSchema
        {
            get
            {
                return _activeSchema;
            }
            set
            {
                _activeSchema = value;
            }
        }

        #region Timezone loading methods

        /// <summary>
        /// Retrieve the connection string from the ITagExpanderService
        /// </summary>
        /// <returns></returns>
        private string GetConnectionString()
        {
            ITagExpansionService service = TagExpansionService.GetTagExpansionService();
            string connectionString = service.GetString(_connectionStringTag);
            if (connectionString == null)
                throw new ArgumentNullException(string.Format("The ConnectionStringTag '{0}' returned a null connection string", _connectionStringTag));
            return connectionString;
        }

        /// <summary>
        /// REturn the appropriate shcmea for accessing the timezone data.
        /// </summary>
        /// <returns></returns>
        private TimezoneSchema GetSchema()
        {
            TimezoneSchema schema = null;
            if (!_schemas.TryGetValue(_activeSchema, out schema))
                throw new NotSupportedException(string.Format("The schema '{0}' is not currently supported", (_activeSchema == null) ? "<null>" : _activeSchema));
            return schema;
        }

        /// <summary>
        /// Load the daylight savings map
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="schema"></param>
        /// <returns></returns>
        private Dictionary<int, List<TimezoneDaylightSavingsImpl>> LoadDaylightSavings(SqlConnection connection, TimezoneSchema schema)
        {
            Dictionary<int, List<TimezoneDaylightSavingsImpl>> daylightSavings = new Dictionary<int, List<TimezoneDaylightSavingsImpl>>();

            //	Load all of the daylight savings first..
            using (SqlCommand command = new SqlCommand(schema.DaylightSavingsCommand, connection) { CommandType = schema.DaylightSavingsCommandType })
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    int id = (int)reader[schema.DaylightSavingsIDField];
                    List<TimezoneDaylightSavingsImpl> list = null;
                    if (!daylightSavings.TryGetValue(id, out list))
                    {
                        list = new List<TimezoneDaylightSavingsImpl>();
                        daylightSavings.Add(id, list);
                    }

                    list.Add(new TimezoneDaylightSavingsImpl(
                        id,
                        (DateTime)reader[schema.DaylightSavingsStartDateField],
                        (DateTime)reader[schema.DaylightSavingsEndDateField],
                        (int)reader[schema.TimezoneOffsetField]));
                }
            }
            return daylightSavings;
        }

        /// <summary>
        /// This method will take the xml document and load the timezones from it.
        /// </summary>
        private void PopulateTimeZones()
        {
            if (_timeZonesByID == null)
            {
                lock (this)
                {
                    if (_timeZonesByID == null)
                    {
                        _timeZonesByID = new Dictionary<int, TimezoneImpl>();
                        _timeZonesByName = new Dictionary<string, TimezoneImpl>();

                        TimezoneSchema schema = GetSchema();

                        //	Get the connection string
                        string connectionString = GetConnectionString();
                        using (SqlConnection connection = new SqlConnection(connectionString))
                        using (SqlCommand command = new SqlCommand(schema.TimeZoneCommand, connection) { CommandType = schema.TimeZoneCommandType })
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            connection.Open();

                            Dictionary<int, List<TimezoneDaylightSavingsImpl>> daylightSavings = LoadDaylightSavings(connection, schema);

                            //	Load all of the Timezone entries.

                            while (reader.Read())
                            {
                                int id = (int)reader[schema.TimeZoneIDField];

                                List<TimezoneDaylightSavingsImpl> list = null;
                                if (!daylightSavings.TryGetValue(id, out list))
                                {
                                    list = new List<TimezoneDaylightSavingsImpl>();
                                }

                                TimezoneImpl timezone = new TimezoneImpl(id, (string)reader[schema.TimezoneNameField], (int)reader[schema.TimezoneOffsetField], list);

                                _timeZonesByID.Add(id, timezone);
                                _timeZonesByName.Add(timezone.Name, timezone);
                            }


                        }

                    }
                }
            }
        }

        #endregion

        #region ITimezoneProvider Members

        string ITimezoneProvider.Name
        {
            get
            {
                return base.Name;
            }
        }

        #endregion

        #region ITimezoneService Members

        MTData.Sys.Timezone.ITimezone ITimezoneService.GetTimezoneByID(int id)
        {
            PopulateTimeZones();
            TimezoneImpl result = null;
            if (_timeZonesByID.TryGetValue(id, out result))
                return result;
            else
                return null;
        }

        MTData.Sys.Timezone.ITimezone ITimezoneService.GetTimezoneByName(string name)
        {
            PopulateTimeZones();
            TimezoneImpl result = null;
            if (_timeZonesByName.TryGetValue(name, out result))
                return result;
            else
                return null;
        }

        DateTime ITimezoneService.ToUtc(string timezoneName, DateTime dateTime)
        {
            PopulateTimeZones();
            TimezoneImpl impl = _timeZonesByName[timezoneName];
            return impl.ToUtc(dateTime);
        }

        DateTime ITimezoneService.FromUtc(string timezoneName, DateTime dateTime)
        {
            PopulateTimeZones();
            TimezoneImpl impl = _timeZonesByName[timezoneName];
            return impl.FromUtc(dateTime);
        }

        DateTime ITimezoneService.ToUtc(int timezoneID, DateTime dateTime)
        {
            PopulateTimeZones();
            TimezoneImpl impl = _timeZonesByID[timezoneID];
            return impl.ToUtc(dateTime);
        }

        DateTime ITimezoneService.FromUtc(int timezoneID, DateTime dateTime)
        {
            PopulateTimeZones();
            TimezoneImpl impl = _timeZonesByID[timezoneID];
            return impl.ToUtc(dateTime);
        }

        IList<MTData.Sys.Timezone.ITimezone> ITimezoneService.GetTimezones()
        {
            PopulateTimeZones();
            List<MTData.Sys.Timezone.ITimezone> result = new List<MTData.Sys.Timezone.ITimezone>();
            foreach (TimezoneImpl impl in _timeZonesByID.Values)
                result.Add(impl);
            return result;
        }

        #endregion
    }
}

#endif
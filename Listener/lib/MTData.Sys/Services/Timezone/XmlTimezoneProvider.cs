using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Services.Storage;

namespace MTData.Sys.Services.Timezone
{
	public class XmlTimezoneProvider : ProviderBaseClass, ITimezoneProvider
	{
		public XmlTimezoneProvider()
			: base("XmlTimezoneProvider", "Xml Timezone Provider")
		{

		}

		/// <summary>
		/// This is the filename on the hard-drive, or an offset within the storage provider 
		/// that holds the data
		/// </summary>
		private string _fileName = "Timezones.xml";

		/// <summary>
		/// This is the provider in the storage provider webService that contains the data.
		/// </summary>
		private string _storageProvider = null;

		/// <summary>
		/// Timezones indexed by id
		/// </summary>
		private Dictionary<int, TimezoneImpl> _timeZonesByID = null;

		/// <summary>
		/// Timezones indexed by name
		/// </summary>
		private Dictionary<string, TimezoneImpl> _timeZonesByName = null;

		/// <summary>
		/// This is the file holding the timezone information
		/// </summary>
		public string FileName
		{
			get
			{
				return _fileName;
			}
			set
			{
				_fileName = value;
			}
		}

		/// <summary>
		/// This is the name of the storage provider that holds the information
		/// </summary>
		public string StorageProvider
		{
			get
			{
				return _storageProvider;
			}
			set
			{
				_storageProvider = value;
			}
		}

		/// <summary>
		/// This method will load the document from the reference storage.
		/// </summary>
		/// <returns></returns>
		private XmlDocument LoadDocument()
		{
			if (_storageProvider != null)
			{
				IStorageService service = StorageService.GetStorageService();
				return service.LoadXmlDocument(_storageProvider, _fileName);
			}
			else
			{
				XmlDocument document = new XmlDocument();
				document.Load(_fileName);
				return document;
			}
		}

		/// <summary>
		/// This method will take the xml document and load the timezones from it.
		/// </summary>
		private void PopulateTimeZones()
		{
			if (_timeZonesByID == null)
			{
                lock (this)
				{
                    if (_timeZonesByID == null)
					{
                        Dictionary<int, TimezoneImpl> timeZonesByID = new Dictionary<int, TimezoneImpl>();
						_timeZonesByName = new Dictionary<string, TimezoneImpl>();

						XmlDocument document = LoadDocument();
						XmlNodeList timeZoneNodes = document.DocumentElement.SelectNodes("Timezone");
						foreach (XmlNode timeZoneNode in timeZoneNodes)
						{
							List<TimezoneDaylightSavingsImpl> dayLightSavings = new List<TimezoneDaylightSavingsImpl>();

							int id = Convert.ToInt32(timeZoneNode.Attributes.GetNamedItem("ID").Value);

							XmlNodeList dayLightSavingsNodes = timeZoneNode.SelectNodes("DaylightSaving");
							if ((dayLightSavingsNodes != null) && (dayLightSavingsNodes.Count > 0))
							{
								foreach (XmlNode dayLightSavingsNode in dayLightSavingsNodes)
								{
									DateTime startTime = DateTime.ParseExact(dayLightSavingsNode.Attributes.GetNamedItem("StartDate").Value, "yyyy-MM-dd HH:mm:ss", null);
									DateTime endTime = DateTime.ParseExact(dayLightSavingsNode.Attributes.GetNamedItem("EndDate").Value, "yyyy-MM-dd HH:mm:ss", null);
									int offset = Convert.ToInt32(dayLightSavingsNode.Attributes.GetNamedItem("Offset").Value);
									dayLightSavings.Add(new TimezoneDaylightSavingsImpl(id, startTime, endTime, offset));
								}
							}

							TimezoneImpl timezone = new TimezoneImpl(id, timeZoneNode.Attributes.GetNamedItem("Name").Value, Convert.ToInt32(timeZoneNode.Attributes.GetNamedItem("Offset").Value), dayLightSavings);

							timeZonesByID.Add(id, timezone);
							_timeZonesByName.Add(timezone.Name, timezone);
						}
                        _timeZonesByID = timeZonesByID;
                    }
				}
			}
		}

		#region ITimezoneService Members

		public MTData.Sys.Timezone.ITimezone GetTimezoneByID(int id)
		{
			PopulateTimeZones();
			return _timeZonesByID[id] as MTData.Sys.Timezone.ITimezone;
		}

		public MTData.Sys.Timezone.ITimezone GetTimezoneByName(string name)
		{
			PopulateTimeZones();
			return _timeZonesByName[name] as MTData.Sys.Timezone.ITimezone;
		}

		public DateTime ToUtc(string timezoneName, DateTime dateTime)
		{
			PopulateTimeZones();
			TimezoneImpl impl = _timeZonesByName[timezoneName];
			return impl.ToUtc(dateTime);
		}

		public DateTime FromUtc(string timezoneName, DateTime dateTime)
		{
			PopulateTimeZones();
			TimezoneImpl impl = _timeZonesByName[timezoneName];
			return impl.FromUtc(dateTime);
		}

		public DateTime ToUtc(int timezoneID, DateTime dateTime)
		{
			PopulateTimeZones();
			TimezoneImpl impl = _timeZonesByID[timezoneID];
			return impl.ToUtc(dateTime);
		}

		public DateTime FromUtc(int timezoneID, DateTime dateTime)
		{
			PopulateTimeZones();
			TimezoneImpl impl = _timeZonesByID[timezoneID];
			return impl.FromUtc(dateTime);
		}

		public IList<MTData.Sys.Timezone.ITimezone> GetTimezones()
		{
			PopulateTimeZones();
			List<MTData.Sys.Timezone.ITimezone> result = new List<MTData.Sys.Timezone.ITimezone>();
			foreach (TimezoneImpl impl in _timeZonesByID.Values)
				result.Add(impl);
			return result;
		}

		#endregion
	}
}

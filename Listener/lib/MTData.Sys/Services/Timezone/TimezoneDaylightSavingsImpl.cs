using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Timezone;

namespace MTData.Sys.Services.Timezone
{
	/// <summary>
	/// This class is an implementation of the Daylight Savings interface
	/// </summary>
	public class TimezoneDaylightSavingsImpl : ITimezoneDaylightSavings
	{
		/// <summary>
		/// This constructor will prepare the class for use.
		/// </summary>
		/// <param name="id"></param>
		/// <param name="startDate"></param>
		/// <param name="endDate"></param>
		/// <param name="offset"></param>
		public TimezoneDaylightSavingsImpl(int id, DateTime startDate, DateTime endDate, int offset)
		{
			_id = id;
			_startDate = startDate;
			_endDate = endDate;
			_offset = offset;
		}

		/// <summary>
		/// This is the owning timezone id
		/// </summary>
		private int _id;

		/// <summary>
		/// This is the startdate of the offset.
		/// </summary>
		private DateTime _startDate;

		/// <summary>
		/// This is the enddate of the conversion
		/// </summary>
		private DateTime _endDate;

		/// <summary>
		/// This is the offset during the daylight savings period in minutes.
		/// </summary>
		private int _offset;

		#region ITimezoneDaylightSavings Members

		/// <summary>
		/// Return the id of the timezone
		/// </summary>
		public int ID
		{
			get
			{
				return _id;
			}
		}

		/// <summary>
		/// This is the start date of the period
		/// </summary>
		public DateTime StartDate
		{
			get
			{
				return _startDate;
			}
		}

		/// <summary>
		/// This is the enddate of the period
		/// </summary>
		public DateTime EndDate
		{
			get
			{
				return _endDate;
			}
		}

		/// <summary>
		/// This is the offset that applies during this time period in minutes
		/// </summary>
		public int Offset
		{
			get
			{
				return _offset;
			}
		}

		#endregion
	}
}

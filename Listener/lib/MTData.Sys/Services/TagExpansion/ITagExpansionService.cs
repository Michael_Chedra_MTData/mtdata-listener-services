using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.TagExpansion
{
	/// <summary>
	/// This interface is supported by the TagExpansionService
	/// The method calls are the same, as the Service will use the tag to
	/// determine to procedure names.
	/// Tag examples are as follows : 
	///		@ProviderName:TagName
	/// </summary>
	public interface ITagExpansionService : ITagExpansionProvider
	{
		/// <summary>
		/// REturn a string form the tag provided
		/// </summary>
		/// <param name="providerName">Name of provider to use</param>
		/// <param name="tag">Tag Name without the @ProviderName header</param>
		/// <returns></returns>
		string GetString(string providerName, string tag);

		/// <summary>
		/// Return a network endpoint.
		/// </summary>
		/// <param name="providerName">Name of provider to use</param>
		/// <param name="tag">Tag Name without the @ProviderName header</param>
		/// <returns></returns>
		MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string providerName, string tag);

	}
}

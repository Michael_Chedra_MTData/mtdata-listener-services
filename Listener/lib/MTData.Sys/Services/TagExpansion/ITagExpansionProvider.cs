using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.TagExpansion
{
	/// <summary>
	/// This is implemented by all providers to this service
	/// </summary>
	public interface ITagExpansionProvider
	{
		/// <summary>
		/// REturn a string form the tag provided
		/// </summary>
		/// <param name="tag"></param>
		/// <returns></returns>
		string GetString(string tag);

		/// <summary>
		/// Return a network endpoint.
		/// </summary>
		/// <param name="tag"></param>
		/// <returns></returns>
		MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string tag);

	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services.TagExpansion
{
	/// <summary>
	/// This class provides a Tag Expansion service.
	///	Tags can be passed in in two differing formats..
	///		GetString("@ProviderName:TagName")
	/// or
	///		GetString(ProviderName, TagName)
	/// 
	/// Both of these methods will yield the same result. The first method will split
	/// the provider name off and use that.
	/// If the first method is called without an @ProviderName: segment, then the default provider
	/// will be used to expand the tag
	/// </summary>
	public class TagExpansionService : ServiceBaseClass, ITagExpansionService
	{

		public static ITagExpansionService GetTagExpansionService()
		{
			return GetTagExpansionService("TagExpansionService");
		}

		public static ITagExpansionService GetTagExpansionService(string name)
		{
			return GetTagExpansionService(ServicesList.Instance, name);
		}

		public static ITagExpansionService GetTagExpansionService(ServicesList servicesList, string name)
		{
			IService service = servicesList[name];
			if (service == null)
				throw new ServiceNotFoundException(name);
			if (!(service is ITagExpansionService))
				throw new ServiceInterfaceNotSupportedException(name, "ITagExpansionService");
			return service as ITagExpansionService;
		}

		#region Tag class

		internal class Tag
		{
			private string _tag = null;
			private string _providerName = null;

			public Tag(string tag)
			{
				int index = tag.IndexOf(':');
				if ((index >= 0) && (tag.StartsWith("@")))
				{
					_providerName = tag.Substring(0, index);
					if (_providerName.StartsWith("@"))
						_providerName = _providerName.Substring(1, _providerName.Length - 1);

					_tag = tag.Substring(index + 1, tag.Length - index - 1);
				}
				else
					_tag = tag;

			}

			public string TagName
			{
				get
				{
					return _tag;
				}
			}

			public string ProviderName
			{
				get
				{
					return _providerName;
				}
			}
		}

		#endregion

		/// <summary>
		/// This is a standard constructor.
		/// </summary>
		public TagExpansionService() : base("TagExpansionService")
		{

		}

		/// <summary>
		/// Return the provider by the name provided.
		/// </summary>
		/// <param name="providerName"></param>
		/// <returns></returns>
		private ITagExpansionProvider GetProviderByName(string providerName)
		{
			if ((providerName == null) || ((providerName != null) && (providerName.Trim().Length == 0)))
				providerName = base.DefaultProvider;

			IProvider provider = base.GetProvider(providerName);
			if (!(provider is ITagExpansionProvider))
				throw new ProviderInterfaceNotSupportedException(this.Name, "ITagExpansionProvider", providerName);

			return provider as ITagExpansionProvider;
		}

		#region ITagExpansionService Members

		/// <summary>
		/// Return the string value equivalent to the tag given from the provider specified.
		/// </summary>
		/// <param name="providerName"></param>
		/// <param name="tag"></param>
		/// <returns></returns>
		public string GetString(string providerName, string tag)
		{
			ITagExpansionProvider provider = GetProviderByName(providerName);
			return provider.GetString(tag);
		}

		/// <summary>
		/// This method will return a Network Endpoint given the providername and tag specified.
		/// </summary>
		/// <param name="providerName"></param>
		/// <param name="tag"></param>
		/// <returns></returns>
		public MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string providerName, string tag)
		{
			ITagExpansionProvider provider = GetProviderByName(providerName);
			return provider.GetNetworkEndPoint(tag);
		}

		#endregion

		#region ITagExpansionProvider Members

		/// <summary>
		/// This method will return a string from the tag provided.
		/// The tag will be of the following format : 
		///		@ProviderName:TagName
		/// The providerName will be split out of the full tag and the remaining tag will
		/// be passed to the underlying provider.
		/// If either of the @ character or : characters are not present, then the tag will not be 
		/// split, and the full text will be passed to the underlying default provider.
		/// </summary>
		/// <param name="tag">Tag in format @ProviderName:TagName</param>
		/// <returns></returns>
		public string GetString(string tag)
		{
			Tag splitTag = new Tag(tag);
			ITagExpansionProvider provider = GetProviderByName(splitTag.ProviderName);
			return provider.GetString(splitTag.TagName);
		}

		/// <summary>
		/// This method will return a populated Network Endpoint from the tag provided.
		/// The tag will be of the following format : 
		///		@ProviderName:TagName
		/// The providerName will be split out of the full tag and the remaining tag will
		/// be passed to the underlying provider.
		/// If either of the @ character or : characters are not present, then the tag will not be 
		/// split, and the full text will be passed to the underlying default provider.
		/// </summary>
		/// <param name="tag">Tag in format @ProviderName:TagName</param>
		/// <returns></returns>
		public MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string tag)
		{
			Tag splitTag = new Tag(tag);
			ITagExpansionProvider provider = GetProviderByName(splitTag.ProviderName);
			return provider.GetNetworkEndPoint(splitTag.TagName);
		}

		#endregion
	}
}

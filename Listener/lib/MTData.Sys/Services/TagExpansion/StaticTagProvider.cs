using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys;
using MTData.Sys.IO.Network;

namespace MTData.Sys.Services.TagExpansion
{
	/// <summary>
	/// This tag provider will provide information tht is statically loaded form the 
	/// config file at load time.
	/// </summary>
	public class StaticTagProvider : ProviderBaseClass, ITagExpansionProvider, Config.IConfigurable
	{
		private Dictionary<string, NetworkEndPoint> _endPoints = new Dictionary<string, NetworkEndPoint>();
		private Dictionary<string, string> _strings = new Dictionary<string, string>();

		public StaticTagProvider()
			: base("StaticTagProvider", "Static Tags loaded form config file or programmatically")
		{

		}

		/// <summary>
		/// Add a string value to the list.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="value"></param>
		public void AddTag(string tag, string value)
		{
			_strings.Add(tag, value);
		}

		/// <summary>
		/// Add a network endpoint to the list.
		/// </summary>
		/// <param name="tag"></param>
		/// <param name="endPoint"></param>
		public void AddNetworkEndPoint(string tag, NetworkEndPoint endPoint)
		{
			_endPoints.Add(tag, endPoint);
		}

		#region ITagExpansionProvider Members

		/// <summary>
		/// Return the string with the appropriate tag.
		/// </summary>
		/// <param name="tag">Tag for which value is required</param>
		/// <returns>Returns value if found, null if not entry found</returns>
		public string GetString(string tag)
		{
			string result = null;
			if (_strings.TryGetValue(tag, out result))
				return result;
			else
				return null;
		}

		/// <summary>
		/// Return the network endpoint for the approriate tag.
		/// </summary>
		/// <param name="tag">Tag for which value is required</param>
		/// <returns>Returns value if found, null if not entry found</returns>
		public MTData.Sys.IO.Network.NetworkEndPoint GetNetworkEndPoint(string tag)
		{
			MTData.Sys.IO.Network.NetworkEndPoint result = null;
			if (_endPoints.TryGetValue(tag, out result))
				return result;
			else
				return null;
		}

		#endregion

		#region IConfigurable Members

		/// <summary>
		/// This method will load up all of the tags to be provided by this instance.
		/// </summary>
		/// <param name="context"></param>
		/// <param name="config"></param>
		/// <param name="path"></param>
		/// <param name="configNode"></param>
		public void Configure(object context, MTData.Sys.Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			if (configNode != null)
			{
				Loader.Loader.PopulatePropertiesFromNode(this, configNode);

				XmlNodeList nodes = configNode.SelectNodes("NetworkEndPoint");
				foreach (XmlNode node in nodes)
				{
					string name = node.Attributes.GetNamedItem("Name").Value;
					string ipAddress = node.Attributes.GetNamedItem("IPAddress").Value;
					int port = Int32.Parse(node.Attributes.GetNamedItem("Port").Value);
					_endPoints.Add(name, new NetworkEndPoint(ipAddress, port));
				}

				nodes = configNode.SelectNodes("String");
				foreach (XmlNode node in nodes)
				{
					string name = node.Attributes.GetNamedItem("Name").Value;
					_strings.Add(name, node.InnerXml);
				}
			}

		}

		#endregion
	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Services
{
	/// <summary>
	/// This class can act as a basic provider class fullfilling all requirements for versioning etc.
	/// </summary>
	public abstract class ProviderBaseClass : VersionedBaseClass, IProvider
	{
		private string _name;
		private string _description;

		public ProviderBaseClass(string name, string description)
		{
			_name = name;
			_description = description;
		}

		protected virtual void Initialise(string name, MTData.Sys.Config.IConfigSection configSection)
		{
			_name = name;
		}

		protected virtual string GetDescription()
		{
			return _description;
		}

		protected virtual void SetDescription(string value)
		{
			_description = value;
		}

		public string Description
		{
			get
			{
				return GetDescription();
			}
			set
			{
				SetDescription(value);
			}
		}

		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
			}
		}

		#region IProvider Members

		string IProvider.Name
		{
			get
			{
				return _name;
			}
		}

		string IProvider.Description
		{
			get
			{
				return _description;
			}
		}

		void IProvider.Initialise(string name, MTData.Sys.Config.IConfigSection configSection)
		{
			this.Initialise(name, configSection);
		}

		#endregion


		#region IDisposable Members

		public virtual void Dispose()
		{
			
		}

		#endregion
	}
}

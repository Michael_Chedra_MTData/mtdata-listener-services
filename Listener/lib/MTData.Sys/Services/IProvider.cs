using System;
using System.Collections.Specialized;
using MTData.Sys.Config;

namespace MTData.Sys.Services
{
    /// <summary>
    /// The base interface for a provider
    /// </summary>
    public interface IProvider : IDisposable, IVersion
    {
        /// <summary>
        /// The name of the provider
        /// </summary>
        string Name {get;}
        
        /// <summary>
        /// a description of the provider
        /// </summary>
        string Description { get;}

        /// <summary>
        /// initialise the provider
        /// </summary>
        /// <param name="name">the name of the provider</param>
        /// <param name="configValues">the configuration settings for this provider</param>
		void Initialise(string name, IConfigSection configSection);

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.ContextProvider;

namespace MTData.Sys.Services.Model
{
	/// <summary>
	/// This interface identifies the methods and properties available
	/// through the model service.
	/// </summary>
	public interface IModelService : IContextProvider
	{
		/// <summary>
		/// Register a model with the service.
		/// </summary>
		/// <param name="model"></param>
		void RegisterModel(IModelProvider model);
	}
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.ContextProvider;

namespace MTData.Sys.Services.Model
{
	/// <summary>
	/// This service will allow coordination of models within the system.
	/// It means that the application models can all be initialised with the 
	/// ServiceList, and also that models can be added via a configuration file 
	/// without the need for code changes in the application.
	/// By implementing the IContextProvider interface, all aspects of the 
	/// installed models can be access by name.
	/// </summary>
	public class ModelService : ServiceBaseClass, IContextProvider, IModelService, IDisposable
	{
		private const string SERVICE_NAME = "ModelService";

		/// <summary>
		/// This method is a static accessor for the standard model service.
		/// </summary>
		/// <returns></returns>
		public static IModelService GetModelService()
		{
			return GetModelService(SERVICE_NAME);
		}

		/// <summary>
		/// This method is a static accessor for the standard model service.
		/// </summary>
		/// <returns></returns>
		public static IModelService GetModelService(string name)
		{
			return GetModelService(ServicesList.Instance, name);
		}

		/// <summary>
		/// This method is a static accessor for the standard model service.
		/// </summary>
		/// <returns></returns>
		public static IModelService GetModelService(ServicesList services, string name)
		{
			IService service = (IService)services[name];
			if (service == null)
				throw new ServiceNotFoundException(name);
			if (!(service is IModelService))
				throw new ServiceInterfaceNotSupportedException(name, "IModelService");

			return service as IModelService;
		}

		/// <summary>
		/// Prepare the service for use.
		/// </summary>
		public ModelService()
			: base(SERVICE_NAME)
		{
			_contextProvider.Change += new ContextProviderChangedDelegate(_contextProvider_Change);
		}

		/// <summary>
		/// Return the converted and verified Model Provider.
		/// </summary>
		/// <param name="provider"></param>
		/// <returns></returns>
		private IModelProvider GetModelProvider(string provider)
		{
			IProvider providerImplementation = base.GetProvider(provider);
			if (providerImplementation == null)
				throw new ServiceProviderNotFoundException(Name, provider);
			if (!(providerImplementation is IModelProvider))
				throw new ProviderInterfaceNotSupportedException(Name, "IModelProvider", provider);

			return providerImplementation as IModelProvider;
		}

		/// <summary>
		/// This will allow us to add providers to the context provider mapping as well.
		/// </summary>
		/// <param name="provider"></param>
		protected override void AddProvider(IProvider provider)
		{
			base.AddProvider(provider);
			_contextProvider.Register(provider.Name, provider);
		}

		/// <summary>
		/// This event handler listens for changes in raised specifically by the context provider.
		/// </summary>
		/// <param name="provider"></param>
		/// <param name="path"></param>
		void _contextProvider_Change(IContextProvider provider, string path)
		{
			if (Change != null)
				Change(provider, path);
		}

		/// <summary>
		/// This class is used to allow deep text based access to the underlying models
		/// </summary>
		private ContextProviderStandard _contextProvider = new ContextProviderStandard();

		#region IContextProvider Members

		/// <summary>
		/// This is used to lock the provider for thread synchronisation
		/// </summary>
		public object SyncRoot
		{
			get { return this._contextProvider; }
		}

		/// <summary>
		/// This property allows the getting/setting of a path value
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public object this[string path]
		{
			get
			{
				return _contextProvider[path];
			}
			set
			{
				_contextProvider[path] = value;
			}
		}

		/// <summary>
		/// This inidcates if the specified path is available.
		/// </summary>
		/// <param name="path"></param>
		/// <returns></returns>
		public bool Contains(string path)
		{
			return _contextProvider.Contains(path);
		}

		/// <summary>
		/// This allows a caller to raise the Changed event for a specific path.
		/// </summary>
		/// <param name="path"></param>
		public void RegisterChange(string path)
		{
			if (Change != null)
				Change(this, path);
		}

		/// <summary>
		/// This event will be raised when a change is registered with the service.
		/// </summary>
		public event ContextProviderChangedDelegate Change;

		#endregion

		#region IModelService Members

		/// <summary>
		/// Register a model after service initialisation
		/// </summary>
		/// <param name="model"></param>
		public void RegisterModel(IModelProvider model)
		{
			this.AddProvider(model);
		}

		#endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_contextProvider != null)
                _contextProvider.Dispose();
            _contextProvider = null;
        }

        #endregion
    }
}

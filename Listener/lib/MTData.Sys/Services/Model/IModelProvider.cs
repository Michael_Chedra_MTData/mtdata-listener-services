using System;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Services;

namespace MTData.Sys.Services.Model
{
	/// <summary>
	/// This interface will be implemented by a model provider.
	/// </summary>
	public interface IModelProvider : IProvider
	{

	}
}

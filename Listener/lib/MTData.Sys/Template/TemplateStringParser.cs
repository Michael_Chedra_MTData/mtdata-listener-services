using System;
using System.Text;
using System.Collections;

namespace MTData.Sys.Template
{
	/// <summary>
	/// This will take a string template and parse and tokenise it
	/// to produce an ITemplateDescriptor.
	/// </summary>
	public class TemplateStringParser : ITemplateDescriptor
	{
		private string _formatString = null;
		private string[] _fieldNames = null;

		public TemplateStringParser(string template)
		{
			ProcessTemplate(template);
		}

		/// <summary>
		/// Take a string and look for the %fieldname% elements within it.
		/// Each of these will be replaced by an incrementing number in the format string
		/// </summary>
		/// <param name="template"></param>
		private void ProcessTemplate(string template)
		{
			StringBuilder builder = new StringBuilder();
			ArrayList fields = new ArrayList();

			int index = template.IndexOf("{%");
			while (index >= 0)
			{
				builder.Append(template.Substring(0, index + 1));
				template = template.Substring(index + 1, template.Length - index - 1);
				
				index = template.IndexOf("%", 1);
				if (index >= 0)
				{
					//	remember to drop the % symbols from the fieldname
					string fieldName = template.Substring(1, index - 1);

					//	check for formatting specifiers
					string formatting = null;
					int formatIndex = fieldName.IndexOf(':');
					if (formatIndex >= 0)
					{
						formatting = fieldName.Substring(formatIndex + 1, fieldName.Length - formatIndex - 1);
						fieldName = fieldName.Substring(0, formatIndex);
					}
					int fieldIndex = fields.IndexOf(fieldName);
					if (fieldIndex >= 0)
					{
						builder.Append(fieldIndex);
					}
					else
					{
						fields.Add(fieldName);
						builder.Append(fields.Count - 1);
					}
					if (formatting != null)
					{
						builder.Append(":");
						builder.Append(formatting);
					}

					template = template.Substring(index + 1, template.Length - index - 1);

					index = template.IndexOf("{%");
				}
				else
				{
					//	no more closes.. finished..
					break;
				}
			}
			//	add in the remaining template formatting..
			builder.Append(template);

			_formatString = builder.ToString();
			_fieldNames = (string[])fields.ToArray(typeof(string));
		}

		#region ITemplateDescriptor Members

		public string FormattingString
		{
			get
			{
				return _formatString;
			}
		}

		public int FieldCount
		{
			get
			{
				return _fieldNames.Length;
			}
		}

		public string this[int fieldIndex]
		{
			get
			{
				if ((_fieldNames == null) || 
					(fieldIndex >= _fieldNames.Length) ||
					(fieldIndex < 0))
					return null;
				else
					return _fieldNames[fieldIndex];
			}
		}

		#endregion
	}
}

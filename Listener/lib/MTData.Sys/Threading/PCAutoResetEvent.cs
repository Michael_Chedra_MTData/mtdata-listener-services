﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MTData.Sys.Threading
{
	#if !NETCF && !NETCF2
    public class PCAutoResetEvent : IResetEvent, IWaitHandleProvider
    {
        private AutoResetEvent _event;

        public PCAutoResetEvent(bool initialState)
        {
            _event = new AutoResetEvent(initialState);
        }

        public void Set()
        {
            _event.Set();
        }

        public void Reset()
        {
            _event.Reset();
        }

        public void Close()
        {
            _event.Close();
        }

        public WaitResult WaitOne()
        {
            return _event.WaitOne() ? WaitResult.Object_0 : WaitResult.TimeOut;
        }

        public WaitResult WaitOne(int timeout)
        {
            return _event.WaitOne(timeout, false) ? WaitResult.Object_0 : WaitResult.TimeOut;
        }

        public WaitHandle WaitHandle
        {
            get { return _event; }
        }
    }
	#endif
}

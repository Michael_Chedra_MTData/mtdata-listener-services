using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MTData.Sys.Threading
{
    public class EventQueue<T> : IDisposable
    {
        /// <summary>
        /// Set True when disposed
        /// </summary>
        private bool _disposed = false;

        /// <summary>
        /// Set when the Queue is Not Empty
        /// </summary>
        private ManualResetEvent _notEmptyEvent = new ManualResetEvent(false);

        /// <summary>
        /// The underlying Queue
        /// </summary>
        private Queue<T> _queue = new Queue<T>();

        /// <summary>
        /// Get count of entries in the Queue
        /// </summary>
        public int Count
        {
            get { return _queue.Count; }
        }

        /// <summary>
        /// Dequeue next item.  Will block until an item is Queued if the Queue is empty.
        /// </summary>
        /// <returns>An item</returns>
        public T Dequeue()
        {
            return Dequeue(-1);
        }

        /// <summary>
        /// Dequeue next item.  Will block until an item is Queued if the Queue is empty.
        /// </summary>
        /// <returns>An item or null if timeout elapsed</returns>
        public T Dequeue(int millisecondTimeout)
        {
            do
            {
                lock (_queue)
                {
                    if (_queue.Count > 0)
                    {
                        T item = _queue.Dequeue();
                        if (_queue.Count == 0)
                            _notEmptyEvent.Reset();
                        return item;
                    }
                }

                if (millisecondTimeout == 0)
                    return default(T);

                if (millisecondTimeout != -1)
                {
                    if (!_notEmptyEvent.WaitOne(millisecondTimeout, false))
                        return default(T);
                }
                else
                    _notEmptyEvent.WaitOne();
            } while (!_disposed);

            throw new ObjectDisposedException("EventQueue");
        }

        /// <summary>
        /// Enqueue an item
        /// </summary>
        /// <param name="item">Item to Enqueue</param>
        public void Enqueue(T item)
        {
            lock (_queue)
            {
                _queue.Enqueue(item);
                _notEmptyEvent.Set();
            }
        }

        /// <summary>
        /// Clear the Queue of items
        /// </summary>
        public void Clear()
        {
            lock (_queue)
            {
                _queue.Clear();
                _notEmptyEvent.Reset();
            }
        }



        #region IDisposable Members

        public void Dispose()
        {
            _disposed = true;
            if (_notEmptyEvent != null)
                _notEmptyEvent.Set();        
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;

namespace MTData.Sys.Threading
{
	#if !NETCF && !NETCF2
    public class PCManualResetEvent : IResetEvent, IWaitHandleProvider
    {
        private ManualResetEvent _event;

        public PCManualResetEvent(bool initialState)
        {
            _event = new ManualResetEvent(initialState);
        }

        public void Set()
        {
            _event.Set();
        }

        public void Reset()
        {
            _event.Reset();
        }

        public void Close()
        {
            _event.Close();
        }

        public WaitResult WaitOne()
        {
            return _event.WaitOne() ? WaitResult.TimeOut : WaitResult.Object_0;
        }

        public WaitResult WaitOne(int timeout)
        {
            return _event.WaitOne(timeout, false) ? WaitResult.TimeOut : WaitResult.Object_0;
        }

        public WaitHandle WaitHandle
        {
            get { return _event; }
        }
    }
	#endif
}

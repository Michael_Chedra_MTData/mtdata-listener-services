﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using MTData.Sys;

namespace MTData.Sys.Threading
{
    public delegate void ExceptionThrownEvent(Exception ex);

    public class ResponseTimer : IDisposable
    {
        public delegate void ResponseTimerEvent(object cookie, object data);
        public event ResponseTimerEvent ResponseTimedOut;
        public event ExceptionThrownEvent ExceptionThrown;

        private class CookieExpireTime
        {
            public object Cookie;
            public object Data;
            public int ExpireMilliseconds;

            public CookieExpireTime(object cookie, object data, int expireMilliseconds)
            {
                Cookie = cookie;
                Data = data;
                ExpireMilliseconds = expireMilliseconds;
            }

            public int MillisecondsBeforeExpiry(int compareTime)
            {
                return ExpireMilliseconds - compareTime;
            }
        }

        private Thread _responseTimerThread;
        private Hashtable _responseTimers = new Hashtable();
        private IResetEvent _responseEvent;
        private bool _disposed;
        private object _lockObj = new object();
#if NETSTANDARD
        private long initiatedTicks = DateTime.Now.Ticks;
#endif
        public ResponseTimer(IEventFactory eventFactory)
        {
            _responseEvent = eventFactory.CreateAutoResetEvent(false);
            _responseTimerThread = new Thread(new ThreadStart(responseTimerThread));
            _responseTimerThread.Start();
        }
        private void responseTimerThread()
        {
            do
            {
                try
                {
                    CookieExpireTime nextExpireCookie = null;
                    lock (_lockObj)
                    {
                        foreach (CookieExpireTime cookieExpireTime in _responseTimers.Values)
                        {
                            if ((nextExpireCookie == null) ||
                                (cookieExpireTime.MillisecondsBeforeExpiry(nextExpireCookie.ExpireMilliseconds) < 0))
                                nextExpireCookie = cookieExpireTime;
                        }
                        _responseEvent.Reset();
                    }

                    WaitResult signalled = WaitResult.Object_0;
                    if (nextExpireCookie != null)
                    {
#if NETSTANDARD
                        int waitTime = nextExpireCookie.MillisecondsBeforeExpiry((int)(new TimeSpan((DateTime.Now.Ticks - initiatedTicks)).TotalMilliseconds));
#else
                        int waitTime = nextExpireCookie.MillisecondsBeforeExpiry(Win32.GetTickCount());
#endif
                        if (waitTime > 0)
                            signalled = _responseEvent.WaitOne(waitTime);
                        else
                            signalled = WaitResult.TimeOut;
                    }
                    else
                        _responseEvent.WaitOne();

                    if (signalled == WaitResult.TimeOut)
                    {
                        // The timer Expired ..

                        // Remove the Timer first, this allows the ResponseTimeOut event to add it back if necessary
                        lock (_lockObj)
                            _responseTimers.Remove(nextExpireCookie.Cookie);

                        try
                        {
                            if (ResponseTimedOut != null)
                                ResponseTimedOut(nextExpireCookie.Cookie, nextExpireCookie.Data);
                        }
                        catch (Exception ex)
                        {
                            try
                            {
                                if (ExceptionThrown != null)
                                    ExceptionThrown(ex);
                            }
                            catch
                            {
                                // Throw it away, Client barfed on handling the first Exception !
                            }
                        }
                    }
                }
                catch
                {
                    // Should not have Thrown in here but it did, will carry on though
                }
            } while (!_disposed);
        }

        public void Dispose()
        {
            if (_disposed)
                return;

            _disposed = true;

            _responseEvent.Set();
        }

        public void SetTimer(object cookie, object data, int millisecondTimeOut)
        {
            if (millisecondTimeOut < 0)
                return;

            lock (_lockObj)
            {
#if NETSTANDARD
                //                _responseTimers[cookie] = new CookieExpireTime(cookie, data, (int)(DateTime.Now.Ticks - initiatedTicks) + millisecondTimeOut);
               _responseTimers[cookie] = new CookieExpireTime(cookie, data, (int)((new TimeSpan(DateTime.Now.Ticks - initiatedTicks)).TotalMilliseconds + millisecondTimeOut));
#else
                _responseTimers[cookie] = new CookieExpireTime(cookie, data, Win32.GetTickCount() + millisecondTimeOut);
#endif

                _responseEvent.Set();
            }
        }

        public void ClearTimer(object cookie)
        {
            lock (_lockObj)
            {
                if (_responseTimers.ContainsKey(cookie))
                {
                    _responseTimers.Remove(cookie);
                    _responseEvent.Set();
                }
            }
        }

        public object GetData(object cookie)
        {
            lock (_lockObj)
            {
                CookieExpireTime cookieExpireTime = _responseTimers[cookie] as CookieExpireTime;
                if (cookieExpireTime == null)
                    return null;

                return cookieExpireTime.Data;
            }
        }
    }

}

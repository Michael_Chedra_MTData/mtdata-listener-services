﻿using System;

using System.Collections.Generic;
using System.Runtime.InteropServices;

namespace MTData.Sys.Threading
{
#if NETCF || NETCF2
    public class CEResetEvent : IResetEvent
    {
        [DllImport("coredll.dll")]
        private static extern int CreateEvent(int lpEventAttributes, int bManualReset, int bInitialState, string lpName);

        [DllImport("coredll.dll")]
        private static extern int CloseHandle(int handle);

        [DllImport("coredll.dll")]
        private static extern int EventModify(int handle, int func);

        [DllImport("coredll.dll")]
        private static extern int WaitForSingleObject(int lpHandle, int dwMilliseconds);

        private const int EVENT_PULSE = 1;
        private const int EVENT_RESET = 2;
        private const int EVENT_SET = 3;

        private int _eventHandle;

        public CEResetEvent(bool manualReset, bool initialState)
        {
            _eventHandle = CreateEvent(0, manualReset ? 1 : 0, initialState ? 1 : 0, null);
        }

        public void Set()
        {
            EventModify(_eventHandle, EVENT_SET);
        }


        public void Reset()
        {
            EventModify(_eventHandle, EVENT_RESET);
        }

        public void Close()
        {
            if (_eventHandle != 0)
            {
                CloseHandle(_eventHandle);
                _eventHandle = 0;
            }
        }

        public WaitResult WaitOne()
        {
            return (WaitResult)EventFactory.WaitForSingleObject(_eventHandle, EventFactory.INFINITE);
        }

        public WaitResult WaitOne(int timeout)
        {
            return (WaitResult)EventFactory.WaitForSingleObject(_eventHandle, timeout);
        }

        public int Handle { get { return _eventHandle; } }
    }
#endif
}

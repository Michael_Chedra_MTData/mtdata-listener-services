﻿using System;

namespace MTData.Sys.Threading
{
    public interface IResetEvent
    {
        void Set();
        void Reset();
        void Close();
        WaitResult WaitOne();
        WaitResult WaitOne(int timeout);
    }
}

﻿using System;
using System.Threading;

namespace MTData.Sys.Threading
{
    public interface IWaitHandleProvider
    {
        WaitHandle WaitHandle { get; }
    }
}

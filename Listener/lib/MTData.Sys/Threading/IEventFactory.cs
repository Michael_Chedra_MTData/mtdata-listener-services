﻿#region Copyright
//------------------------------------------------------------------------------
// ©2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;

namespace MTData.Sys.Threading
{
    public enum WaitResult
    {
        Object_0 = 0x00000000,
        Abandoned_0 = 0x00000080,
        TimeOut = 258,
        Error = -1
    }

    public interface IEventFactory
    {
        IResetEvent CreateAutoResetEvent(bool initialState);
        IResetEvent CreateManualResetEvent(bool initialState);

        WaitResult WaitOne(IResetEvent oneEvent);
        WaitResult WaitOne(IResetEvent oneEvent, int timeOutMs);

        WaitResult WaitAny(IResetEvent[] events, int timeOutMs);
        WaitResult WaitAny(IResetEvent[] events);
    }
}

using System;
#if NETCF || NETCF2
using System.Runtime.InteropServices;
#else
using System.Threading;
#endif

namespace MTData.Sys.Threading
{
    public class EventFactory : IEventFactory
    {
#if NETCF || NETCF2
        [DllImport("coredll.dll")]
        public static extern int WaitForMultipleObjects(int nCount, int[] lpHandles, int fWaitAll, int dwMilliseconds);

        [DllImport("coredll.dll")]
        public static extern int WaitForSingleObject(int lpHandle, int dwMilliseconds);

        public const int INFINITE = -1;
        public const int WAIT_OBJECT_0 = 0x00000000;
        public const int WAIT_ABANDONED_0 = 0x00000080;
        public const int WAIT_TIMEOUT = 258;
#endif

        #region IEventFactory Members

        public IResetEvent CreateAutoResetEvent(bool initialState)
        {
#if NETCF || NETCF2
			return new CEResetEvent(false, initialState);
#else
            return new PCAutoResetEvent(initialState);
#endif
        }

        public IResetEvent CreateManualResetEvent(bool initialState)
        {
#if NETCF || NETCF2
            return new CEResetEvent(true, initialState);
#else
            return new PCManualResetEvent(initialState);
#endif
        }

        public WaitResult WaitOne(IResetEvent oneEvent)
        {
#if NETCF || NETCF2
            return (WaitResult)WaitForSingleObject(((CEResetEvent)oneEvent).Handle, INFINITE);
#else
            WaitHandle waitHandle = ((IWaitHandleProvider)oneEvent).WaitHandle;
            if (waitHandle.WaitOne())
                return WaitResult.Object_0;
            else
                return WaitResult.Abandoned_0;
#endif
        }

        public WaitResult WaitOne(IResetEvent oneEvent, int timeOutMs)
        {
#if NETCF || NETCF2
            return (WaitResult)WaitForSingleObject(((CEResetEvent)oneEvent).Handle, timeOutMs);
#else
            WaitHandle waitHandle = ((IWaitHandleProvider)oneEvent).WaitHandle;
            if (waitHandle.WaitOne(timeOutMs, false))
                return WaitResult.Object_0;
            else
                return WaitResult.TimeOut;
#endif
        }

        public WaitResult WaitAny(IResetEvent[] events, int timeOutMs)
        {
#if NETCF || NETCF2
            int[] handles = new int[events.Length];
            for (int i = 0; i < events.Length; i++)
                handles[i] = ((CEResetEvent)events[i]).Handle;

            return (WaitResult)WaitForMultipleObjects(handles.Length, handles, 0, timeOutMs);
#else
            WaitHandle[] waitHandles = new WaitHandle[events.Length];
            for (int i = 0; i < waitHandles.Length; i++)
                waitHandles[i] = ((IWaitHandleProvider)events[i]).WaitHandle;

            int result = WaitHandle.WaitAny(waitHandles, timeOutMs, true);
            if (result == WaitHandle.WaitTimeout)
                return WaitResult.TimeOut;
            if (result > 128)
                return WaitResult.Abandoned_0 + (result - 128);

            return WaitResult.Object_0 + result;
#endif
        }

        public WaitResult WaitAny(IResetEvent[] events)
        {
#if NETCF || NETCF2
            int[] handles = new int[events.Length];
            for (int i = 0; i < events.Length; i++)
                handles[i] = ((CEResetEvent)events[i]).Handle;

            return (WaitResult)WaitForMultipleObjects(handles.Length, handles, 0, INFINITE);
#else
            WaitHandle[] waitHandles = new WaitHandle[events.Length];
            for (int i = 0; i < waitHandles.Length; i++)
                waitHandles[i] = ((IWaitHandleProvider)events[i]).WaitHandle;

            int result = WaitHandle.WaitAny(waitHandles);
            if (result == WaitHandle.WaitTimeout)
                return WaitResult.TimeOut;
            if (result > 128)
                return WaitResult.Abandoned_0 + (result - 128);

            return WaitResult.Object_0 + result;
#endif
        }

        #endregion
    }
}

#if !NETCF && !NETCF2
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;
using System.Text;

namespace MTData.Sys.Data
{
    public class BaseDao
    {
        public BaseDao()
        {

        }

        #region Object Population Methods

        protected delegate object PopulateDataDelegate(SqlConnection connection, object[] parameters);

        /// <summary>
        /// This method will open the desired connection and then call the passed delegate.
        /// </summary>
        /// <param name="connectionString"></param>
        /// <param name="dataSet"></param>
        /// <param name="callback"></param>
        /// <param name="parameters"></param>
        protected object PopulateDataWithConnection(string connectionString, PopulateDataDelegate callback, object[] parameters)
        {
            object result = null;
            using (SqlConnection connection = new SqlConnection(connectionString))
            {
                connection.Open();

                if (callback != null)
                    result = callback(connection, parameters);
            }
            return result;
        }

        protected object PopulateDataWithConnection(string connectionString, PopulateDataDelegate callback)
        {
            return PopulateDataWithConnection(connectionString, callback, null);
        }

        #endregion
    }
}
#endif

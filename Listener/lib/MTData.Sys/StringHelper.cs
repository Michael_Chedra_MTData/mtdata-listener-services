using System;
using System.Text;

namespace MTData.Sys
{
	/// <summary>
	/// Summary description for StringHelper.
	/// </summary>
	public class StringHelper
	{
		private StringHelper()
		{
		}

		public static string GetBufferHexString(byte[] buffer)
		{
			return GetBufferHexString(buffer, true, true);
		}

		public static string GetBufferHexString(byte[] buffer, bool includeLength, bool includeASCII)
		{
            return GetBufferHexString(buffer, buffer.Length, includeLength, includeASCII);
		}

        public static string GetBufferHexString(byte[] buffer, int count, bool includeLength, bool includeASCII)
        {
            if ((buffer == null) || ((buffer != null) && (count == 0)))
                return string.Empty;

            StringBuilder sb = new StringBuilder();
            if (includeLength)
            {
                sb.Append(count.ToString());
                sb.Append(" ");
            }
            sb.Append("[");
            for (int i = 0; i < count ; i++)
            {
                if (i > 0)
                    sb.Append(",");
                sb.Append(buffer[i].ToString("X2"));
            }
            sb.Append("]");
            if (includeASCII)
            {
                sb.Append(" '");
                sb.Append(ASCIIEncoding.ASCII.GetString(buffer, 0, count));
                sb.Append("'");
            }


            return sb.ToString();
        }

        private const string HEXVALUES = "0123456789abcdef";

		public static byte[] GetStringBufferHex(string bufferString)
		{
			string buffer = bufferString.ToLower();

			int startIndex = 0;
			if (buffer[startIndex] == '[')
				startIndex++;

			int endIndex = buffer.Length-1;
			if (buffer[endIndex] == ']')
				endIndex--;

			//	assume properly formatted.. comma separated
			string[] splits = buffer.Substring(startIndex, (endIndex - startIndex) + 1).Split(',');
			byte[] result = new byte[splits.Length];

			int byteIndex = -1;
			int mode = 0;
			while (startIndex <= endIndex)
			{
				switch(mode)
				{
					case 0 :
						int nibble1Index = HEXVALUES.IndexOf(buffer[startIndex]);

						if (nibble1Index < 0)
							throw new Exception(string.Format("Hex expression error at index {0}, character {1}", startIndex, buffer[startIndex]));
						else
							result[++byteIndex] = Convert.ToByte(nibble1Index << 4);
						mode = 1;
						break;
					case 1 :
						int nibble2Index = HEXVALUES.IndexOf(buffer[startIndex]);

						if (nibble2Index < 0)
							throw new Exception(string.Format("Hex expression error at index {0}, character {1}", startIndex, buffer[startIndex]));
						else
							result[byteIndex] |= Convert.ToByte(nibble2Index);
						mode = 2;
						break;
					case 2 :
						if (buffer[startIndex] != ',')
							throw new Exception(string.Format("Hex expression error at index {0}, character {1}", startIndex, buffer[startIndex]));
						mode = 0;
						break;
				}
				startIndex++;
			}
			return result;
		}

	}
}

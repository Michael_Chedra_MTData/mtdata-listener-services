using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MTData.Sys")]
[assembly: AssemblyDescriptionAttribute("Release Build")]
[assembly: AssemblyConfigurationAttribute("Release")]
[assembly: AssemblyCompany("Mobile Tracking and Data")]
[assembly: AssemblyProductAttribute("MTData - Sys")]
[assembly: AssemblyCopyright("Copyright © 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("9651b7ae-b0e6-4b7e-b756-eab01b30980b")]
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Revision and Build Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersionAttribute("8.2.0.0")]
#if !NETCF && !NETCF2
[assembly: AssemblyFileVersion("8.2.0.0")]
#endif

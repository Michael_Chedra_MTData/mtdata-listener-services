using System;
using System.Runtime.InteropServices;

namespace MTData.Sys
{
	/// <summary>
	/// This class allows lockdown to kiosk mode
	/// </summary>
	public class LockDown
	{

		public LockDown()
		{
		}

#if NETCF || NETCF2
		[DllImport("coredll.dll", EntryPoint = "FindWindow")]
		private static extern IntPtr FindWindow(string lpClassName, string lpWindowName);

		[DllImport("coredll.dll", EntryPoint = "EnableWindow")]
		private static extern bool EnableWindow(IntPtr hwnd, bool bEnable);

		[DllImport("coredll.dll", CharSet = CharSet.Auto)]
		public static extern bool ShowWindow(IntPtr hwnd, int nCmdShow);

		private const int SW_HIDE = 0x0000;
		private const int SW_SHOW = 0x0001;

		public static bool Execute(bool enabled)
		{

			IntPtr hwnd = FindWindow("HHTaskBar", null);

			if (!hwnd.Equals(IntPtr.Zero))
			{

				if (enabled)
				{
					ShowWindow(hwnd, SW_HIDE);
					return EnableWindow(hwnd, false);
				}
				else
				{
					ShowWindow(hwnd, SW_SHOW);
					return EnableWindow(hwnd, true);
				}
			}

			return true;

		}
		public static bool LockTaskBar(bool enabled)
		{

			IntPtr hwnd = FindWindow("HHTaskBar", null);

			if (!hwnd.Equals(IntPtr.Zero))
			{

				if (enabled)
				{
					//ShowWindow(hwnd, SW_HIDE);
					return EnableWindow(hwnd, false);
				}
				else
				{
					//ShowWindow(hwnd, SW_SHOW);
					return EnableWindow(hwnd, true);
				}
			}

			return true;

		}
#endif

	}
}

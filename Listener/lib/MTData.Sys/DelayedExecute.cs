using System;

namespace MTData.Sys
{
	/// <summary>
	/// This class will perform any task given a delay period, and a callback delegate.
	/// It can be used for monitoring a function, or for display of tooltip screens etc.
	/// Once initiated, it will execute the method after the specified delay.
	/// If the reset method is called, it will reset the timer, and either start again,
	/// or stop it altogether.
	/// </summary>
	public class DelayedExecute : IDisposable
	{
		/// <summary>
		/// This is the delay applied whenever a set is performed.
		/// </summary>
		private int _delayMilliseconds = 500;

		/// <summary>
		/// This is a state object passed to the callback methods.
		/// </summary>
		private object _state = null;

		/// <summary>
		/// This is the timer used to process the request
		/// </summary>
		private System.Threading.Timer _timer = null;

		/// <summary>
		/// Indicates that the reset method will stop the timer outright 
		/// which will require a further Set to proceed.
		/// False indicates that it will just reset the timeout time, and continue on 
		/// it's way.
		/// </summary>
		private bool _resetStop = false;

		/// <summary>
		/// This is the format of the callback method.
		/// </summary>
		public delegate void CallBackDelegate(object sender, object state);

		/// <summary>
		/// This is the event that is raised once the time has expired.
		/// </summary>
		public event CallBackDelegate CallBack;

		/// <summary>
		/// This constructor identifies the delay period involved.
		/// </summary>
		/// <param name="delayMilliseconds">Number of milliseconds to delay before execution</param>
		/// <param name="state">Optional state object passed to event handler</param>
		/// <param name="resetStop">Indicates that a reset stops the timer, as opposed to resetting the timeout</param>
		public DelayedExecute(int delayMilliseconds, object state, bool resetStop)
		{
			_delayMilliseconds = delayMilliseconds;
			_state = state;
			_resetStop = resetStop;
		}

		/// <summary>
		/// Get/Set the State dynamically
		/// </summary>
		public object State
		{
			get { return _state; }
			set { _state = value; }
		}

		/// <summary>
		/// Set the timer ticking.
		/// </summary>
		public void Set()
		{
			if (_timer == null)
				_timer = new System.Threading.Timer(new System.Threading.TimerCallback(TimerCallbackMethod), null, _delayMilliseconds, System.Threading.Timeout.Infinite);
			else
				_timer.Change(_delayMilliseconds, System.Threading.Timeout.Infinite);
		}

		/// <summary>
		/// Reset the timeout based on the resetStop flag.
		/// </summary>
		public void Reset()
		{
			if (_timer != null)
			{
				_timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
				if (!_resetStop)
					_timer.Change(_delayMilliseconds, System.Threading.Timeout.Infinite);
			}
		}

		/// <summary>
		/// This method is called on expoiry of the timeout.
		/// </summary>
		/// <param name="state"></param>
		private void TimerCallbackMethod(object state)
		{
			if (CallBack != null)
				CallBack(this, _state);
		}

		#region IDisposable Members

		/// <summary>
		/// Clean up the instance
		/// </summary>
		public void Dispose()
		{
			if (_timer != null)
			{
				_timer.Change(System.Threading.Timeout.Infinite, System.Threading.Timeout.Infinite);
				_timer.Dispose();
				_timer = null;
			}
		}


		#endregion
	}
}

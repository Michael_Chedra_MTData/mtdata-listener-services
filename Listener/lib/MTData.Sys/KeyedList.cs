using System;
using System.Xml;
using System.Collections.Generic;
using System.Text;
using MTData.Sys.Config;

namespace MTData.Sys
{
	public class KeyedList : IConfigurable
	{
		private Dictionary<string, string> _values = new Dictionary<string, string>();

		#region IConfigurable Members

		public void Configure(object context, Config.Config config, string path, System.Xml.XmlNode configNode)
		{
			_values.Clear();
			if (configNode != null)
			{
				XmlNodeList nodes = configNode.SelectNodes("add");
				foreach (XmlNode node in nodes)
				{
					string key = node.Attributes.GetNamedItem("key").Value;
					string value = node.Attributes.GetNamedItem("value").Value;

					_values[key] = value;
				}
			}
		}

		#endregion

		public string this[string key]
		{
			get
			{
				string result = null;
				_values.TryGetValue(key, out result);
				return result;
			}
		}
	}
}

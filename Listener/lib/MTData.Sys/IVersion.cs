using System;

namespace MTData.Sys
{
    /// <summary>
    /// The interface which gets the version number of the object
    /// </summary>
    public interface IVersion
    {
        /// <summary>
        /// string version of the verion number, incoorpating the Major and Minor numbers
        /// </summary>
        string VersionNumber
        {
            get;
        }

        /// <summary>
        /// the major number of this version
        /// </summary>
        int MajorNumber
        {
            get;
        }

        /// <summary>
        /// the minor number of this version
        /// </summary>
        int MinorNumber
        {
            get;
        }

        /// <summary>
        /// the date of this version
        /// </summary>
        DateTime Date
        {
            get;
        }
    }
}

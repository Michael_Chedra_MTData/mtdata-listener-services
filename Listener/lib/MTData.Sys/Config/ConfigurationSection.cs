using log4net;
using System;
using System.Collections;
using System.Collections.Specialized;
using System.Xml;
using MTData.Sys.IO;
using MTData.Sys.Encryption;
using MTData.Sys.Loader;

namespace MTData.Sys.Config
{
    public class ConfigurationException : Exception
    {
        public ConfigurationException()
        {
        }

        public ConfigurationException(string message)
            : base(message)
        {
        }

        public ConfigurationException(string message, System.Exception innerException)
            : base(message, innerException)
        {
        }
    }

    public class ConfigurationSection : IConfigSection
    {
        public static readonly ConfigurationSection EmptyConfigurationSection = new ConfigurationSection(null, null);

        protected static ILog _log = LogManager.GetLogger(typeof(ConfigurationSection));
        protected string _key;
        protected NameValueCollection _singleValues = new NameValueCollection();
        protected Hashtable _sections = new Hashtable();
        protected ArrayList _orderedSections = new ArrayList();
        protected XmlNode _customNode;
        protected bool _isSecure = false;
        protected Hashtable _filter = null;

        public ConfigurationSection(string key)
        {
            _key = key;
            _filter = null;
        }

        public ConfigurationSection(string key, Hashtable filter)
        {
            _key = key;
            _filter = filter;
        }

        /// <summary>
        /// Allows elements of the filter to be updated by a descendant class.
        /// </summary>
        /// <param name="name"></param>
        /// <param name="value"></param>
        protected void SetFilter(string name, string value)
        {
            if (_filter == null)
                _filter = new Hashtable();
            _filter[name] = value;
        }

        protected void loadFromConfiguration(XmlElement configurationElm)
        {
            checkSecured(configurationElm);

            foreach (XmlNode node in configurationElm)
            {
                if (node.NodeType == XmlNodeType.Element)
                {
                    switch (node.Name)
                    {
                        case "filter":
                            if (MatchesFilter(node))
                                loadFromConfiguration((XmlElement)node);
                            break;

                        case "custom":
                            _customNode = node;
                            break;

                        case "add":
                            {
                                string key = node.Attributes["key"].Value;
                                string value = node.Attributes["value"].Value;
                                _singleValues[key] = value;

                                if (_log.IsDebugEnabled)
                                    _log.Debug("Configuration[" + key + "] = " + value);
                                break;
                            }
                        case "remove":
                            {
                                string key = node.Attributes["key"].Value;

                                _singleValues[key] = null;

                                if (_log.IsDebugEnabled)
                                    _log.Debug("Configuration[" + key + "] = null");
                                break;
                            }
                        case "section":
                            {
                                string key = node.Attributes["key"].Value;

                                ConfigurationSection configurationSection = (ConfigurationSection)_sections[key];
                                if (configurationSection == null)
                                {
                                    configurationSection = new ConfigurationSection(key, _filter);
                                    _sections[key] = configurationSection;
                                    _orderedSections.Add(configurationSection);
                                }

                                configurationSection.loadFromConfiguration(node as XmlElement);

                                if (_log.IsDebugEnabled)
                                    _log.Debug("Configuration Section [" + key + "]");
                                break;

                            }
                    }
                }
            }
        }

        private void checkSecured(XmlElement elm)
        {
            XmlAttribute mtd_secure = elm.Attributes["mtd_secure"];
            if (mtd_secure == null)
                return;

            if (mtd_secure.Value == "MD5")
            {
                string innerXml = elm.InnerXml;

                XmlAttribute MD5Attribute = elm.Attributes["MD5"];
                if (MD5Attribute != null)
                {
                    string MD5Text = MD5Attribute.Value;
                    if (MD5Text != null)
                    {
                        if (MaliciousChangeProtection.IsUnchanged(innerXml, MD5Text))
                            _isSecure = true;
                    }
                }
            }
        }


        public string GetFileName(string key, string defaultFileName)
        {
            return SatelliteFile.ResolveFileName(this[key, defaultFileName]);
        }

        public object CreateInstance(string key)
        {
            string typeName = this[key, null];
            if (typeName == null)
            {
                //				if (_logIsErrorEnabled)
                //					_log.Error("CreateInstance() " + key + " not configured");
                return null;
            }

            Type type = TypeHelper.GetType(typeName);
            if (type == null)
                throw new NullReferenceException("CreateInstance() Could not load Type " + typeName);

            return Activator.CreateInstance(type);
        }

        public bool IsSecure
        {
            get { return _isSecure; }
        }

        public string GetValue(string key)
        {
            key = key.ToLower();
            foreach (string s in _singleValues.Keys)
            {
                if (s.ToLower() == key)
                    return _singleValues[s];
            }
            return null;
        }

        public string this[string key, string defaultValue]
        {
            get
            {
                string value = _singleValues[key];
                if (value == null)
                    return defaultValue;
                return value;
            }
        }

        public int this[string key, int defaultValue]
        {
            get
            {
                string value = _singleValues[key];
                if (value == null)
                    return defaultValue;

                try
                {
                    return int.Parse(value);
                }
                catch (Exception)
                {
                    //					if (_logIsErrorEnabled)
                    //						_log.Error(string.Format("GetInt parsed invalid Int '{0}' for key '{1}'", value, key), ex);
                    return defaultValue;
                }
            }
        }


        public bool this[string key, bool defaultValue]
        {
            get
            {
                string value = _singleValues[key];
                if (value == null)
                    return defaultValue;

                try
                {
                    return bool.Parse(value);
                }
                catch (Exception)
                {
                    //					if (_logIsErrorEnabled)
                    //						_log.Error(string.Format("GetBool parsed invalid Bool '{0}' for key '{1}'", value, key), ex);
                    return defaultValue;
                }
            }
        }

        public NameValueCollection GetStrings()
        {
            return _singleValues;
        }

        public IConfigSection GetConfigSection(string section, bool ignoreCase)
        {
            if (!ignoreCase)
                return GetConfigSection(section);

            section = section.ToLower();
            foreach (DictionaryEntry de in _sections)
            {
                if (de.Key.ToString().ToLower() == section)
                    return de.Value as IConfigSection;
            }

            return EmptyConfigurationSection;
        }

        public IConfigSection GetConfigSection(string section)
        {
            if (_log.IsInfoEnabled)
                _log.Info("Loading " + section);

            IConfigSection configSection = (IConfigSection)_sections[section];
            if (configSection == null)
            {
                if (_log.IsInfoEnabled)
                    _log.Info("section " + section + " not found");
                return EmptyConfigurationSection;
            }
            return configSection;
        }

        public IConfigSection[] GetSections()
        {
            return (IConfigSection[])_orderedSections.ToArray(typeof(IConfigSection));
        }

        public string Key
        {
            get { return _key; }
        }

        public XmlNode CustomNode
        {
            get { return _customNode; }
        }

        public Hashtable Filter
        {
            get { return _filter; }
        }

        protected bool MatchesFilter(XmlNode configElm)
        {
            if (_filter != null)
            {
                foreach (DictionaryEntry de in _filter)
                {
                    XmlAttribute attrib = configElm.Attributes[de.Key.ToString()];
                    if (attrib != null)
                    {
                        if (string.Compare(attrib.Value, de.Value.ToString(), true) != 0)
                        {
                            if (_log.IsDebugEnabled)
                                _log.Debug("MatchesFilter " + attrib.Value + " != " + de.Value.ToString());
                            return false;
                        }
                    }
                }
            }
            return true;
        }
    }
}

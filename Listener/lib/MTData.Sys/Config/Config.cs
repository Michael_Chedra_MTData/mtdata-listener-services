#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Xml;
using System.Configuration;
using System.Collections.Generic;
using System.Text;
		  
namespace MTData.Sys.Config
{
	public interface IConfigurable
	{
		void Configure(object context, Config config, string path, XmlNode configNode);
	}

	public class Config
	{
		#region  Static Accessors.

		/// <summary>
		/// This method allows another config to be pushed in on top of the current
		/// global instance.
		/// </summary>
		/// <param name="config"></param>
		public static void Push(Config config)
		{
			_instanceStack.Push(config);
		}

		/// <summary>
		/// This method will pop a config instance off of the globally available config stack.
		/// </summary>
		/// <returns></returns>
		public static Config Pop()
		{
			if (_instanceStack.Count > 0)
				return _instanceStack.Pop();
			else
				return null;
		}

		/// <summary>
		/// This is the filename for the default configuration.
		/// </summary>
		private static string _configFileName = null;

		/// <summary>
		/// This is the filename used by the constructor with no parameters.
		/// </summary>
		public static string ConfigFileName
		{
			get
			{
				if (_configFileName == null)
				{
					lock (typeof(Config))
					{
                        if (_configFileName == null)
                        {
#if !NETCF && !NETCF2 && !NETSTANDARD2_0
                            _configFileName = AppDomain.CurrentDomain.SetupInformation.ConfigurationFile; //Win32.GetEntryAssemblyLocation() + ".config";
#else
                            _configFileName = Win32.GetEntryAssemblyLocation() + ".config";
#endif
                        }
					}
				}

				return _configFileName;
			}
			set
			{
				_configFileName = value;
			}
		}

		/// <summary>
		/// This holds a stack of the entries to be processed as the globally available config
		/// </summary>
		private static Stack<Config> _instanceStack = new Stack<Config>();

		/// <summary>
		/// This property allows access to the globally availble config.
		/// </summary>
		public static Config ConfigurationManager
		{
			get
			{
				if (_instanceStack.Count == 0)
				{
					lock (typeof(Config))
					{
						if (_instanceStack.Count == 0)
							_instanceStack.Push(new Config());
					}
				}
				return _instanceStack.Peek();
			}
		}

		#endregion

		private string _fileName = null;
		private XmlDocument _document = null;
		private XmlNode _configurationNode = null;
		private KeyedList _appSettings = null;

//		[System.Security.Permissions.FileIOPermission(System.Security.Permissions.SecurityAction.Demand)]
		public Config(System.Xml.XmlNode configuration)
		{
			_fileName = null;
			_document = null;
			_configurationNode = configuration;
		}

		public Config()
			: this(ConfigFileName)
		{

		}

		public Config(string fileName)
		{
			if (fileName != null)
				_fileName = fileName;
			else
			{
				//	calculate out the filename from the executing assembly.
				if (ConfigFileName != null)
					_fileName = MTData.Sys.IO.SatelliteFile.ResolveFileName(ConfigFileName);
				else
					throw new ArgumentNullException("Config Filename not specified");
			}
			_document = new XmlDocument();
			_document.Load(_fileName);
			_configurationNode = _document.DocumentElement;
		}

		public string[] GetSectionNames(string path)
		{
			List<string> result = new List<string>();

			XmlNode node = GetSectionDefinitionNode(path, true, true);
			XmlNodeList sectionNodes = node.SelectNodes("section");
			foreach (XmlNode sectionNode in sectionNodes)
				result.Add(sectionNode.Attributes.GetNamedItem("name").Value);

			return result.ToArray();
		}

		public object GetSection(string path)
		{
			return GetSection(path, null);
		}

		internal XmlNode GetSectionConfigNode(string path)
		{
			return _configurationNode.SelectSingleNode(path);
		}

		public object GetSection(string path, object context)
		{
			XmlNode node = GetSectionDefinitionNode(path, true, false);

			XmlNode typeNode = node.Attributes.GetNamedItem("type");
			if (typeNode == null)
				throw new Exception(string.Format("No type node available for path {0}", path));

			string typeName = typeNode.Value;
			Type type = Type.GetType(typeName);
			if (type == null)
				throw new Exception(string.Format("The Type '{0}' not available for path {1}", typeName, path));

			object instance = Activator.CreateInstance(type);
			if (instance == null)
				throw new Exception(string.Format("The Type '{0}' could not be instantiated from path {1}", typeName, path));

			//	Get the config node..
			XmlNode configNode = _configurationNode.SelectSingleNode(path);
			if (configNode == null)
				throw new ArgumentNullException(string.Format("Node for path '{0}' could not be found in the configuration document", path));

			if (instance is IConfigurable)
			{
				((IConfigurable)instance).Configure(context, this, path, configNode);
			}
			else if (instance is IConfigurationSectionHandler)
				instance = ((IConfigurationSectionHandler)instance).Create(this, context, configNode);
			else
				Loader.Loader.PopulatePropertiesFromNode(instance, configNode);
			
			return instance;
		}

		private XmlNode GetSectionDefinitionNode(XmlNode currentNode, string nodeName, bool group)
		{
			string path = "section[@name='{0}']";
			if (group)
				path = "sectionGroup[@name='{0}']";

			return currentNode.SelectSingleNode(string.Format(path, nodeName));
		}

		private XmlNode GetSectionDefinitionNode(string path, bool required, bool groupOnly)
		{
			string[] paths = path.Split('/');

			XmlNode node = _configurationNode.SelectSingleNode("configSections");
			if ((node == null) && (required))
				throw new Exception(string.Format("Path not found - Path : {0}; Section : configSections;", path));

			if (node != null)
			{
				for (int loop = 0; loop < paths.Length; loop++)
				{
					node = GetSectionDefinitionNode(node, paths[loop], groupOnly || (loop != (paths.Length - 1)));
					if (node == null)
					{
						if (required)
							throw new Exception(string.Format("Path not found - Path : {0}; Section : {1};", path, paths[loop]));
						else
							break;
					}
				}
			}
			return node;
		}

		public KeyedList AppSettings
		{
			get
			{
				if (_appSettings == null)
					lock(this)
						if (_appSettings == null)
						{
							_appSettings = new KeyedList();
							string path = "appSettings";
							XmlNode appSettingsNode = _configurationNode.SelectSingleNode(path);
							if (appSettingsNode != null)
								_appSettings.Configure(null, this, path, appSettingsNode);
						}
				return _appSettings;
			}
		}
	}
}

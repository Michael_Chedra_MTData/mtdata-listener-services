#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
#if !NETCF && !NETCF2
using System.Configuration;
#endif

namespace MTData.Sys.Config
{
#if NETCF || NETCF2 || Android || NETSTANDARD2_0
    // Summary:
    //     Handles the access to certain previous configuration sections.
    public interface IConfigurationSectionHandler
	{
		// Summary:
		//     Creates a configuration section handler.
		//
		// Parameters:
		//   p arent:
		//     Parent object.
		//
		//   s ection:
		//     Section XML node.
		//
		//   configContext:
		//     Configuration context object.
		//
		// Returns:
		//     The created section handler object.
		object Create(object parent, object configContext, XmlNode section);
	}
#endif

	/// <summary>
	/// This class implements IConfigSectionHandler, and simply returns the 
	/// xml node of the config section.
	/// </summary>
	public class XmlConfigSectionHandler : IConfigurationSectionHandler
	{
		public XmlConfigSectionHandler()
		{

		}
		#region IConfigurationSectionHandler Members

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			System.Xml.XmlNode clone = section.Clone();
			return clone;
		}

		#endregion
	}

}

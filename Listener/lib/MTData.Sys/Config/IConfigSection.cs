#region Copyright
//------------------------------------------------------------------------------
// �2008 Mobile Tracking and Data Pty Ltd
//	     www.mtdata.com.au
//------------------------------------------------------------------------------
#endregion
using System.Collections.Specialized;
using System.Xml;

namespace MTData.Sys.Config
{
	public interface IConfigSection
	{
		string	GetFileName(string key, string defaultFileName);		// Gets an absolute path to a keyed file
		
		object CreateInstance(string key);

		bool	IsSecure							  { get; }
		string	this[string key, string defaultValue] { get; }
		int		this[string key, int	defaultValue] { get; }
		bool	this[string key, bool	defaultValue] { get; }
		
		NameValueCollection GetStrings();

		IConfigSection GetConfigSection(string section);
		IConfigSection[] GetSections();

		string Key { get; }

		XmlNode CustomNode { get; }
	}
}

using System;
using System.Collections;
using System.IO;
using System.Xml;
using MTData.Sys.IO;

namespace MTData.Sys.Config
{
    public class ConfigurationSettings : ConfigurationSection
    {
        private static ConfigurationSettings _singleton;
        private static IConfigSection NullConfigSection = new ConfigurationSection(string.Empty, null);

        private bool _initialised = false;
        private int? _versionMajor = null;

        public int? VersionMajor
        {
            get { return _versionMajor; }
        }

        private int? _versionMinor = null;

        public int? VersionMinor
        {
            get { return _versionMinor; }
        }

        private string _configName = null;

        public string ConfigName
        {
            get { return _configName; }
            set { _configName = value; }
        }


        public static ConfigurationSettings Singleton
        {
            get { return _singleton; }
        }

        public ConfigurationSettings(string fileName, string environment, string platform)
            : base("root")
        {
            if (environment != null)
                base.SetFilter("environment", environment);
            if (platform != null)
                base.SetFilter("platform", platform);
            SharedConstruction(fileName);
        }

        public ConfigurationSettings(Stream fileStream, string environment, string platform)
            : base("root")
        {
            if (environment != null)
                base.SetFilter("environment", environment);
            if (platform != null)
                base.SetFilter("platform", platform);
            SharedConstruction(fileStream);
        }

        public ConfigurationSettings(string fileName, Hashtable filter)
            : base("root", filter)
        {
            SharedConstruction(fileName);
        }

        private void SharedConstruction(string fileName)
        {
            // First instance is Singleton
            if (_singleton == null)
                _singleton = this;

            try
            {
                // Get Executing Assembly
                string configFile = SatelliteFile.ResolveFileName(fileName);

                if (!File.Exists(configFile))
                    throw new FileNotFoundException("Configuration File '" + configFile + "' Not Found");
                
                parseFile(configFile);
                
            }
            catch (Exception)
            {
                //				if (_logIsErrorEnabled)
                //					_log.Error("ConfigurationSettings(): Failed", ex);
            }
        }

        private void SharedConstruction(Stream fileStream)
        {
            // First instance is Singleton
            if (_singleton == null)
                _singleton = this;

            parseFile(fileStream);
        }

        private void parseFile(Stream configFile)
        {
            if (_log.IsDebugEnabled)
                _log.Debug("ParseFile " + configFile.ToString());

            XmlDocument oXml = new XmlDocument();
            oXml.Load(configFile);
            FindPlatformConfig(oXml);
        }

        private void parseFile(string configFile)
        {
            if (_log.IsDebugEnabled)
                _log.Debug("ParseFile " + configFile);

            XmlDocument oXml = new XmlDocument();
            oXml.Load(configFile);
            FindPlatformConfig(oXml);
        }

        // Find the appropriate Platform Configuration
        private void FindPlatformConfig(XmlDocument oXml)
        {
            foreach (XmlNode configNode in oXml.ChildNodes)
            {
                XmlElement configElm = configNode as XmlElement;
                if (configElm != null && (configElm.NodeType == XmlNodeType.Element) && (configElm.Name == "configurations"))
                {


                    int? versionMajor = null;
                    int? versionMinor = null;
                    string configName = null;
                    if (configElm.HasAttribute("versionMajor"))
                    {
                        try { versionMajor = Convert.ToInt32(configElm.Attributes["versionMajor"].Value); }
                        catch { }
                    }

                    if (configElm.HasAttribute("versionMinor"))
                    {
                        try { versionMinor = Convert.ToInt32(configElm.Attributes["versionMinor"].Value); }
                        catch { }
                    }

                    if (configElm.HasAttribute("name"))
                    {
                        try { configName = configElm.Attributes["name"].Value; }
                        catch { }
                    }


                    if (!_initialised || versionMajor != this._versionMajor || versionMajor == this._versionMajor && versionMinor > this._versionMinor)
                    {
                        this._configName = configName;
                        this._versionMajor = versionMajor;
                        this._versionMinor = versionMinor;

                        loadFromConfigurations(configElm as XmlElement);

                        _initialised = true;
                    }
                }
            }
        }



        public void Merge(XmlElement configurationsElm)
        {
            if (_log.IsDebugEnabled)
                _log.Debug("Merging from " + configurationsElm.InnerText);

            // Find the appropriate Platform Configuration
            if (configurationsElm.NodeType == XmlNodeType.Element)
            {
                switch (configurationsElm.Name)
                {
                    case "configuration":
                        loadFromConfigurations(configurationsElm as XmlElement);
                        break;
                }
            }
        }

        private void loadFromConfigurations(XmlElement configurationsElm)
        {
            // Find the appropriate Platform Configuration

            foreach (XmlNode configElm in configurationsElm.ChildNodes)
            {
                if (configElm.NodeType == XmlNodeType.Element)
                {
                    switch (configElm.Name)
                    {
                        case "configuration":
                            if (MatchesFilter(configElm))
                                loadFromConfiguration(configElm as XmlElement);
                            break;

                        case "includes":
                            processIncludes(configElm);
                            break;
                    }
                }
            }
        }

        private void processIncludes(XmlNode includesElm)
        {
            foreach (XmlNode includeElm in includesElm.ChildNodes)
            {
                if (includeElm.NodeType == XmlNodeType.Element)
                {
                    switch (includeElm.Name)
                    {
                        case "include":
                            processInclude(includeElm);
                            break;
                    }
                }
            }
        }

        private void processInclude(XmlNode includeElm)
        {
            XmlAttribute valueAttrib = includeElm.Attributes["value"];
            if (valueAttrib == null)
                return;

            if (MatchesFilter(includeElm))
            {
                string filePattern = SatelliteFile.ResolveFileName(valueAttrib.Value);
                string path = Path.GetDirectoryName(filePattern);
                string filter = Path.GetFileName(filePattern);
                string[] files = Directory.GetFiles(path, filter);

                foreach (string fileName in files)
                    parseFile(fileName);
            }
        }

        public static IConfigSection GetForType(Type type)
        {
            IConfigSection configSection = _singleton.GetConfigSection("Types").GetConfigSection(type.FullName);
            if (configSection == null)
            {
                //				if (_logIsWarnEnabled)
                //					_log.Warn("Type ConfigSection '" + type.FullName + "' is missing");
                return NullConfigSection;
            }
            return configSection;
        }

        public static IConfigSection GetForType(object o)
        {
            return GetForType(o.GetType());
        }

    }
}

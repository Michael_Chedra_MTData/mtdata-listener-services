using System;
using System.Collections.Generic;
using System.Text;
using System.Configuration;

namespace MTData.Sys.Config
{
	/// <summary>
	/// This config section handler will simply take the node provided, look for a type attribute, and built and populate the 
	/// object concerned.
	/// </summary>
	public class TypedConfigSectionHandler : IConfigurationSectionHandler
	{
		#region IConfigurationSectionHandler Members

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			//	take the current node, and use the loader class to instantiate it.
			//	Return the configured instance.
			Loader.Loader loader = new MTData.Sys.Loader.Loader();

			return loader.Create(configContext, section);
		}

		#endregion
	}
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Common.DataAccess;
using MTData.Transport.Service.Route.Data;
using MTData.Common.Plugin.Configuration;

namespace MTData.Transport.Service.Route.Data
{
    public class RouteScheduleBiz : BaseBusinessLogic
    {
        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors

        /// <summary>
        /// The default Constructor.
        /// </summary>
        public RouteScheduleBiz(string dsn)
            : base(dsn)
        {
        }

        #endregion

        #region Public Methods

        public PluginMethodResult UpdateRouteScheduleDataSet(DataSetRouteSchedule dataSetRouteSchedule, int userID)
        {
            PluginMethodResult result = new PluginMethodResult();
            using (RouteScheduleDAL dal = (RouteScheduleDAL)_Open(new RouteScheduleDAL()))
            {
                result.DataSet = dal.UpdateRouteScheduleDataSet(dataSetRouteSchedule, userID);
            }

            return result;

        }
        #endregion

        #region Private Methods
        #endregion



        public PluginMethodResult GetRouteScheduleDataSet(int userID)
        {
            PluginMethodResult result = new PluginMethodResult();


            using (RouteScheduleDAL dal = (RouteScheduleDAL)_Open(new RouteScheduleDAL()))
            {
                result.DataSet = dal.GetRouteScheduleDataSet(userID);
            }



            return result;
        }
    }
}

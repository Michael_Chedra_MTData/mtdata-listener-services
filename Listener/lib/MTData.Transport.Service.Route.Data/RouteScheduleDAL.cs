using System;
using System.Collections.Generic;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using MTData.Common.DataAccess;

namespace MTData.Transport.Service.Route.Data
{
    public class RouteScheduleDAL : BaseDataAccess
    {
        #region Private Variables
        #endregion

        #region Properties
        #endregion

        #region Constructors

        /// <summary>
        /// The default Constructor.
        /// </summary>
        public RouteScheduleDAL()
        {
        }

        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion


        internal DataSet UpdateRouteScheduleDataSet(DataSetRouteSchedule dataSetRouteSchedule, int userID)
        {

            //Update TemplateGroup
            if (dataSetRouteSchedule.T_RS_Template.Count > 0)
            {
                using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_UpdateRS_Template")))
                using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_UpdateRS_Template")))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    PrepareSqlParameters(dataSetRouteSchedule.T_RS_Template, updateCmd);
                    PrepareSqlParameters(dataSetRouteSchedule.T_RS_Template, insertCmd, "ID");
                    adapter.UpdateCommand = updateCmd;
                    adapter.InsertCommand = insertCmd;
                    adapter.Update(dataSetRouteSchedule.T_RS_Template);
                }
            }
            if (dataSetRouteSchedule.T_RS_TemplateGroup.Count > 0)
            {
                using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_UpdateRS_TemplateGroup")))
                using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_UpdateRS_TemplateGroup")))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    PrepareSqlParameters(dataSetRouteSchedule.T_RS_TemplateGroup, updateCmd);
                    PrepareSqlParameters(dataSetRouteSchedule.T_RS_TemplateGroup, insertCmd, "ID");
                    adapter.UpdateCommand = updateCmd;
                    adapter.InsertCommand = insertCmd;
                    adapter.Update(dataSetRouteSchedule.T_RS_TemplateGroup);
                }
            }

            if (dataSetRouteSchedule.T_RS_RouteSchedule.Count > 0)
            {
                using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_UpdateRS_RouteSchedule")))
                using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_UpdateRS_RouteSchedule")))
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    PrepareSqlParameters(dataSetRouteSchedule.T_RS_RouteSchedule, updateCmd);
                    PrepareSqlParameters(dataSetRouteSchedule.T_RS_RouteSchedule, insertCmd, "ID");
                    adapter.UpdateCommand = updateCmd;
                    adapter.InsertCommand = insertCmd;
                    adapter.Update(dataSetRouteSchedule.T_RS_RouteSchedule);
                }
            }

            return GetRouteScheduleDataSet(userID);


        }

        internal DataSet GetRouteScheduleDataSet(int userID)
        {
            DataSetRouteSchedule result = new DataSetRouteSchedule();
            result.EnforceConstraints = false;


            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand()))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                ConfigureStoredProc(cmd);
                cmd.Parameters.AddWithValue("@UserID", userID);

                adapter.TableMappings.Add("Table", result.T_RS_TemplateGroup.TableName);
                adapter.TableMappings.Add("Table1", result.T_RS_Template.TableName);
                adapter.TableMappings.Add("Table2", result.T_RS_RouteSchedule.TableName);

                cmd.CommandText = "usp_GetRouteScheduleByUserID";
                adapter.Fill(result);

            }

            //result.EnforceConstraints = true;
            return result;
        }

        private void PrepareSqlParameters(DataTable dt, SqlCommand cmd, params string[] excludeColumn)
        {
            List<string> excludeList = new List<string>(excludeColumn);
            SqlDbType sqlType;
            int sqlLength;
            foreach (DataColumn dc in dt.Columns)
            {
                if (excludeList.Contains(dc.ColumnName))
                    continue;

                if (dc.DataType == typeof(int))
                {
                    sqlLength = 4;
                    sqlType = SqlDbType.Int;
                }
                else if (dc.DataType == typeof(bool))
                {
                    sqlLength = 1;
                    sqlType = SqlDbType.Bit;
                }
                else if (dc.DataType == typeof(string))
                {
                    sqlLength = dc.MaxLength;
                    sqlType = SqlDbType.NVarChar;
                }
                else if (dc.DataType == typeof(DateTime))
                {
                    sqlLength = 8;
                    sqlType = SqlDbType.DateTime;
                }
                else
                {
                    continue;
                }

                cmd.Parameters.Add(new SqlParameter("@" + dc.ColumnName, sqlType, sqlLength, dc.ColumnName));
            }


        }

    }
}

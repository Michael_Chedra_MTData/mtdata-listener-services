using System;
using System.Data;
using System.Data.SqlClient;
using MTData.Common.DataAccess;

namespace MTData.Transport.Service.Route.Data
{
	/// <summary>
	/// Summary description for Route2.
	/// </summary>
	public class Route2DAL : BaseDataAccess
	{
		public Route2DAL()
		{
		}

		#region Get Methods
		public DataSetRoute2 GetRoute2RouteById(DataSetRoute2 ds, int route2Id)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2Get")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = route2Id;
                adapter.Fill(ds.T_Route2);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2RoutesByFleetId(DataSetRoute2 ds, int fleetId, bool includeRoutesInError)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2GetByFleet")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@FleetID", SqlDbType.Int).Value = fleetId;
                cmd.Parameters.Add("@IncludeRoutesInError", SqlDbType.Int).Value = includeRoutesInError;
                adapter.Fill(ds.T_Route2);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2PointsByRouteId(DataSetRoute2 ds, int route2Id)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2PointsGetByRoute")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@RouteID", SqlDbType.Int).Value = route2Id;
                adapter.Fill(ds.T_Route2Point);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2CheckPointsByRouteId(DataSetRoute2 ds, int route2Id)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2CheckPointsGetByRoute")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@RouteID", SqlDbType.Int).Value = route2Id;
                adapter.Fill(ds.T_Route2CheckPoint);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2ScheduleById(DataSetRoute2 ds, int route2ScheduleId)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleGet")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = route2ScheduleId;
                adapter.Fill(ds.T_Route2Schedule);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2SchedulesByRouteId(DataSetRoute2 ds, int route2Id)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2SchedulesGetByRoute")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@RouteID", SqlDbType.Int).Value = route2Id;
                adapter.Fill(ds.T_Route2Schedule);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2SchedulesByFleetId(DataSetRoute2 ds, int fleetId, bool includeSchedulesInError)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2SchedulesGetByFleet")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@FleetID", SqlDbType.Int).Value = fleetId;
                cmd.Parameters.Add("@IncludeSchedulesInError", SqlDbType.Int).Value = includeSchedulesInError;
                adapter.Fill(ds.T_Route2Schedule);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2ScheduleCheckPointById(DataSetRoute2 ds, int route2ScheduleCheckPointId)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleGet")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = route2ScheduleCheckPointId;
                adapter.Fill(ds.T_Route2ScheduleCheckPoint);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2ScheduleCheckPointsByScheduleId(DataSetRoute2 ds, int route2ScheduleId)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleCheckPointsGetBySchedule")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int).Value = route2ScheduleId;
                adapter.Fill(ds.T_Route2ScheduleCheckPoint);
                return ds;
            }
		}

		public DataSetRoute2 GetRoute2ScheduleCheckPointsByRouteId(DataSetRoute2 ds, int route2Id)
		{
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleCheckPointsGetByRoute")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.Add("@RouteID", SqlDbType.Int).Value = route2Id;
                adapter.Fill(ds.T_Route2ScheduleCheckPoint);
                return ds;
            }
		}

		#endregion

		#region Insert/Update Methods
		public DataSetRoute2 InsertUpdateRoute2Rows(DataSetRoute2 ds)
		{
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_Route2Insert")))
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_Route2Update")))
            {
                insertCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                insertCmd.Parameters.Add("@Name", SqlDbType.NVarChar, 255, "Name");
                insertCmd.Parameters.Add("@Tolerance", SqlDbType.Real, 8, "Tolerance");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                updateCmd.Parameters.Add("@Name", SqlDbType.NVarChar, 255, "Name");
                updateCmd.Parameters.Add("@Tolerance", SqlDbType.Real, 8, "Tolerance");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.InsertCommand = insertCmd;
                adapter.UpdateCommand = updateCmd;
                adapter.Update(ds.T_Route2);
                return ds;
            }
		}

		public DataSetRoute2 InsertUpdateRoute2PointRows(DataSetRoute2 ds)
		{
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_Route2PointInsert")))
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_Route2PointUpdate")))
            {
                insertCmd.Parameters.Add("@RouteID", SqlDbType.Int, 4, "RouteID");
                insertCmd.Parameters.Add("@Index", SqlDbType.Real, 8, "Index");
                insertCmd.Parameters.Add("@Latitude", SqlDbType.Real, 8, "Latitude");
                insertCmd.Parameters.Add("@Longitude", SqlDbType.Real, 8, "Longitude");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                // Note updating a Point cannot change its Index or its RouteID
                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@Latitude", SqlDbType.Real, 8, "Latitude");
                updateCmd.Parameters.Add("@Longitude", SqlDbType.Real, 8, "Longitude");
                updateCmd.Parameters.Add("@Index", SqlDbType.Real, 8, "Index");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.InsertCommand = insertCmd;
                adapter.UpdateCommand = updateCmd;
                adapter.Update(ds.T_Route2Point);
                return ds;
            }
		}

		public DataSetRoute2 InsertUpdateRoute2CheckPointRows(DataSetRoute2 ds)
		{
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_Route2CheckPointInsert")))
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_Route2CheckPointUpdate")))
            {
                insertCmd.Parameters.Add("@RoutePointID", SqlDbType.Int, 4, "RoutePointID");
                insertCmd.Parameters.Add("@Name", SqlDbType.NVarChar, 255, "Name");
                insertCmd.Parameters.Add("@LatitudeTolerance", SqlDbType.Real, 8, "LatitudeTolerance");
                insertCmd.Parameters.Add("@LongitudeTolerance", SqlDbType.Real, 8, "LongitudeTolerance");
                insertCmd.Parameters.Add("@RuleFlag", SqlDbType.BigInt, 8, "RuleFlag");
                insertCmd.Parameters.Add("@DependentOnPoint", SqlDbType.Int, 4, "DependentOnPoint");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@Name", SqlDbType.NVarChar, 255, "Name");
                updateCmd.Parameters.Add("@RoutePointID", SqlDbType.Int, 4, "RoutePointID");
                updateCmd.Parameters.Add("@LatitudeTolerance", SqlDbType.Real, 8, "LatitudeTolerance");
                updateCmd.Parameters.Add("@LongitudeTolerance", SqlDbType.Real, 8, "LongitudeTolerance");
                updateCmd.Parameters.Add("@RuleFlag", SqlDbType.BigInt, 8, "RuleFlag");
                updateCmd.Parameters.Add("@DependentOnPoint", SqlDbType.Int, 4, "DependentOnPoint");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.InsertCommand = insertCmd;
                adapter.UpdateCommand = updateCmd;
                adapter.Update(ds.T_Route2CheckPoint);
                return ds;
            }
		}

		public DataSetRoute2 InsertUpdateRoute2ScheduleRows(DataSetRoute2 ds)
		{
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleInsert")))
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleUpdate")))
            {
                insertCmd.Parameters.Add("@RouteID", SqlDbType.Int, 4, "RouteID");
                insertCmd.Parameters.Add("@Name", SqlDbType.NVarChar, 255, "Name");
                insertCmd.Parameters.Add("@ReverseDirection", SqlDbType.Int, 1, "ReverseDirection");
                insertCmd.Parameters.Add("@FixedStartTime", SqlDbType.DateTime, 8, "FixedStartTime");
                insertCmd.Parameters.Add("@InError", SqlDbType.Int, 1, "InError");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@RouteID", SqlDbType.Int, 4, "RouteID");
                updateCmd.Parameters.Add("@Name", SqlDbType.NVarChar, 255, "Name");
                updateCmd.Parameters.Add("@ReverseDirection", SqlDbType.Int, 1, "ReverseDirection");
                updateCmd.Parameters.Add("@FixedStartTime", SqlDbType.DateTime, 8, "FixedStartTime");
                updateCmd.Parameters.Add("@InError", SqlDbType.Int, 1, "InError");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.InsertCommand = insertCmd;
                adapter.UpdateCommand = updateCmd;
                adapter.Update(ds.T_Route2Schedule);
                return ds;
            }
		}

		public DataSetRoute2 InsertUpdateRoute2ScheduleCheckPointRows(DataSetRoute2 ds)
		{
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleCheckPointInsert")))
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleCheckPointUpdate")))
            {
                insertCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                insertCmd.Parameters.Add("@RouteCheckPointID", SqlDbType.Int, 4, "RouteCheckPointID");
                insertCmd.Parameters.Add("@RelativeMinimumArriveTimeMins", SqlDbType.Int, 4, "RelativeMinimumArriveTimeMins");
                insertCmd.Parameters.Add("@RelativeMaximumArriveTimeMins", SqlDbType.Int, 4, "RelativeMaximumArriveTimeMins");
                insertCmd.Parameters.Add("@RelativeMinimumDepartTimeMins", SqlDbType.Int, 4, "RelativeMinimumDepartTimeMins");
                insertCmd.Parameters.Add("@RelativeMaximumDepartTimeMins", SqlDbType.Int, 4, "RelativeMaximumDepartTimeMins");
                insertCmd.Parameters.Add("@MinimumStopTimeMins", SqlDbType.Int, 4, "MinimumStopTimeMins");
                insertCmd.Parameters.Add("@MaximumStopTimeMins", SqlDbType.Int, 4, "MaximumStopTimeMins");
                insertCmd.Parameters.Add("@RestTimeMins", SqlDbType.Int, 4, "RestTimeMins");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                updateCmd.Parameters.Add("@RouteCheckPointID", SqlDbType.Int, 4, "RouteCheckPointID");
                updateCmd.Parameters.Add("@RelativeMinimumArriveTimeMins", SqlDbType.Int, 4, "RelativeMinimumArriveTimeMins");
                updateCmd.Parameters.Add("@RelativeMaximumArriveTimeMins", SqlDbType.Int, 4, "RelativeMaximumArriveTimeMins");
                updateCmd.Parameters.Add("@RelativeMinimumDepartTimeMins", SqlDbType.Int, 4, "RelativeMinimumDepartTimeMins");
                updateCmd.Parameters.Add("@RelativeMaximumDepartTimeMins", SqlDbType.Int, 4, "RelativeMaximumDepartTimeMins");
                updateCmd.Parameters.Add("@MinimumStopTimeMins", SqlDbType.Int, 4, "MinimumStopTimeMins");
                updateCmd.Parameters.Add("@MaximumStopTimeMins", SqlDbType.Int, 4, "MaximumStopTimeMins");
                updateCmd.Parameters.Add("@RestTimeMins", SqlDbType.Int, 4, "RestTimeMins");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.InsertCommand = insertCmd;
                adapter.UpdateCommand = updateCmd;

                adapter.Update(ds.T_Route2ScheduleCheckPoint);

                return ds;
            }
		}
		#endregion

		#region Delete Methods
		public DataSetRoute2 DeleteRoute2Rows(DataSetRoute2 ds)
		{
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_Route2Delete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.DeleteCommand = deleteCmd;
                adapter.Update(ds.T_Route2);
                return ds;
            }
		}

		public DataSetRoute2 DeleteRoute2PointRows(DataSetRoute2 ds)
		{
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_Route2PointDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.DeleteCommand = deleteCmd;
                adapter.Update(ds.T_Route2Point);
                return ds;
            }
		}

		public DataSetRoute2 DeleteRoute2CheckPointRows(DataSetRoute2 ds)
		{
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_Route2CheckPointDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.DeleteCommand = deleteCmd;
                adapter.Update(ds.T_Route2CheckPoint);
                return ds;
            }
		}

		public DataSetRoute2 DeleteRoute2ScheduleRows(DataSetRoute2 ds)
		{
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.DeleteCommand = deleteCmd;
                adapter.Update(ds.T_Route2Schedule);
                return ds;
            }
		}

		public DataSetRoute2 DeleteRoute2ScheduleCheckPointRows(DataSetRoute2 ds)
		{
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_Route2ScheduleCheckPointDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                adapter.DeleteCommand = deleteCmd;
                adapter.Update(ds.T_Route2ScheduleCheckPoint);
                return ds;
            }
		}
		#endregion

		/// <summary>
		/// Touch a Route's modified date
		/// </summary>
		/// <param name="originalRouteId">Id of Original Route</param>
		public void Touch(int originalRouteId)
		{
            using (SqlCommand command = ConfigureStoredProc(new SqlCommand("usp_Route2Touch")))
            {
                command.Parameters.Add("@ID", SqlDbType.Int, 4).Value = originalRouteId;
                command.ExecuteNonQuery();
            }
		}

		/// <summary>
		/// Get or Create a Clone for the specified Original Route.  If Original Route is new or has been
		/// modified since last Clone was created of it then a new Clone is created, else existing
		/// Clone is returned.
		/// </summary>
		/// <param name="originalRouteId">Id of Original Route</param>
		/// <returns>Id of an exact Clone of the Original Route</returns>
		public int GetRoute2CloneId(int originalRouteId)
		{
            using (SqlCommand command = ConfigureStoredProc(new SqlCommand("usp_Route2GetUptoDateClone")))
            {
                command.Parameters.Add("@OriginalRouteId", SqlDbType.Int, 4).Value = originalRouteId;
                return (int)command.ExecuteScalar();
            }
		}
	}
}

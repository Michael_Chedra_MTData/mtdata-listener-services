using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Service.Route.Data.Schedule
{
    /// <summary>
    /// represents a Weekly schedule
    /// </summary>
    [Serializable]
    public class WeeklySchedule : AbstractSchedule
    {
        #region private fields
        /// <summary>
        /// repeat the schedule in x number of weeks
        /// </summary>
        private int _repeatInXWeeks;

        /// <summary>
        /// list of the days of the week the Route is run
        /// </summary>
        private bool[] _daysOfWeek;
        #endregion

        public WeeklySchedule()
            : base()
        {
            _repeatInXWeeks = 1;
            _daysOfWeek = new bool[7];
        }
        public WeeklySchedule(ISchedule schedule)
            : base(schedule)
        {
            _repeatInXWeeks = 1;
            _daysOfWeek = new bool[7];
            if (schedule is WeeklySchedule)
            {
                WeeklySchedule week = (WeeklySchedule)schedule;
                _repeatInXWeeks = week._repeatInXWeeks;
                for (int i = 0; i < _daysOfWeek.Length; i++)
                {
                    _daysOfWeek[i] = week._daysOfWeek[i];
                }
            }
        }

        public int RepeatInXWeeks
        {
            get { return _repeatInXWeeks; }
            set { _repeatInXWeeks = value; }
        }

        internal bool IsScheduledDay(DayOfWeek day)
        {
            return _daysOfWeek[(int)day];
        }
        internal void SetScheduledDay(DayOfWeek day, bool scheduled)
        {
            _daysOfWeek[(int)day] = scheduled;
        }

        public bool Monday
        {
            get { return IsScheduledDay(DayOfWeek.Monday); }
            set { SetScheduledDay(DayOfWeek.Monday, value); }
        }
        public bool Tuesday
        {
            get { return IsScheduledDay(DayOfWeek.Tuesday); }
            set { SetScheduledDay(DayOfWeek.Tuesday, value); }
        }
        public bool Wednesday
        {
            get { return IsScheduledDay(DayOfWeek.Wednesday); }
            set { SetScheduledDay(DayOfWeek.Wednesday, value); }
        }
        public bool Thursday
        {
            get { return IsScheduledDay(DayOfWeek.Thursday); }
            set { SetScheduledDay(DayOfWeek.Thursday, value); }
        }
        public bool Friday
        {
            get { return IsScheduledDay(DayOfWeek.Friday); }
            set { SetScheduledDay(DayOfWeek.Friday, value); }
        }
        public bool Saturday
        {
            get { return IsScheduledDay(DayOfWeek.Saturday); }
            set { SetScheduledDay(DayOfWeek.Saturday, value); }
        }
        public bool Sunday
        {
            get { return IsScheduledDay(DayOfWeek.Sunday); }
            set { SetScheduledDay(DayOfWeek.Sunday, value); }
        }
        #region ISchedule Members

        [System.Xml.Serialization.XmlIgnore]
        public override string Description
        {
            get
            {
                string error = ValidateSchedule();
                if (error != null)
                {
                    return error;
                }


                string des = "Route will be scheduled every Week on ";
                if (_repeatInXWeeks > 1)
                {
                    des = string.Format("Route will be scheduled every {0} weeks on", _repeatInXWeeks);
                }
                else if (_repeatInXWeeks <= 0)
                {
                    des = "Route will be scheduled for one week on ";
                }
                Array days = Enum.GetValues(typeof(DayOfWeek));
                bool comma = false;
                foreach (DayOfWeek day in days)
                {
                    if (IsScheduledDay(day))
                    {
                        if (comma)
                        {
                            des = string.Format("{0}, {1}", des, day.ToString());
                        }
                        else
                        {
                            des = string.Format("{0} {1}", des, day.ToString());
                        }
                        comma = true;
                    }
                }

                if (EndDate.HasValue)
                    des = string.Format("{0}. Route will be scheduled between {1:dd/MM/yyyy} and {2:dd/MM/yyyy}.", des, StartDate.Date, EndDate.Value.Date);
                else
                    des = string.Format("{0}. Route will be scheduled from {1:dd/MM/yyyy}.", des, StartDate.Date);


                return des;
            }
        }

        public override string ValidateSchedule()
        {
            string error = base.ValidateSchedule();
            if (error != null)
            {
                return error;
            }
            bool daySet = false;
            foreach (bool b in _daysOfWeek)
            {
                if (b)
                {
                    daySet = true;
                }
            }
            if (!daySet)
            {
                return "No day has been selected";
            }
            return null;
        }

        public override DateTime NextScheduledDate(DateTime dateTimeUTC)
        {
            DateTime next;
            if (dateTimeUTC < StartDate)
            {
                next = StartDate;
                while (!IsScheduledDay(next.DayOfWeek))
                {
                    next = next.AddDays(1);
                }
                return next;
            }
            if (dateTimeUTC > EndDate)
            {
                return GetLastNextDate();
            }
            next = new DateTime(dateTimeUTC.Year, dateTimeUTC.Month, dateTimeUTC.Day);//, StartDate.Hour, StartDate.Minute, StartDate.Second);
            if (next < dateTimeUTC)
            {
                next = next.AddDays(1);
            }

            while ((!IsScheduledDay(next.DayOfWeek) || !IsCorrectWeek(next)) && next < DateTime.MaxValue.AddDays(-1))
            {
                next = next.AddDays(1);
                if (next > EndDate)
                {
                    return GetLastNextDate();
                }
            }
            if (!EndDate.HasValue)
            {
                return next;
            }
            if (next < EndDate)
            {
                return next;
            }
            return GetLastNextDate();
        }


        #endregion

        private DateTime GetLastNextDate()
        {
            DateTime end = (EndDate.HasValue ? EndDate.Value : DateTime.MaxValue);
            DateTime next = new DateTime(end.Year, end.Month, end.Day);//, StartDate.Hour, StartDate.Minute, StartDate.Second);
            if (next > EndDate)
            {
                next.AddDays(-1);
            }
            while ((!IsScheduledDay(next.DayOfWeek) || !IsCorrectWeek(next)) && next > DateTime.MinValue.AddDays(1))
            {
                next = next.AddDays(-1);
            }
            return next;
        }

        private bool IsCorrectWeek(DateTime next)
        {
            if (_repeatInXWeeks == 1)
            {
                return true;
            }
            TimeSpan span = next - StartDate;
            if (_repeatInXWeeks <= 0)
            {
                if (span.TotalDays < 7)
                {
                    return true;
                }
                return false;
            }
            int daysfromStartOfWeek = span.Days % (_repeatInXWeeks * 7);
            if (daysfromStartOfWeek < 7)
            {
                return true;
            }
            return false;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Service.Route.Data.Schedule
{
    [Flags]
    [Serializable]
    public enum MulityDayOfWeek
    {
        None = 0,
        Sunday = 1,
        Monday = 2,
        Tuesday = 4,
        Wednesday = 8,
        Thursday = 16,
        Friday = 32,
        Saturday = 64
    }

    public enum Position
    {
        First = 0,
        Second,
        Third,
        Fourth,
        Last
    }

    public enum Day
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Day,
        WeekDay,
        WeekEndDay
    }

    public enum RecurrenceType
    {
        Daily = 0,
        Weekly = 1,
        MonthlyByDay = 2,
        MonthlyByPosition = 3
    }
}

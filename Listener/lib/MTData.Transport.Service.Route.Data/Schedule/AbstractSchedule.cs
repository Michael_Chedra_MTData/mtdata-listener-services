using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Service.Route.Data.Schedule
{
    public abstract class AbstractSchedule : ISchedule
    {
        #region private fields
        /// <summary>
        /// the start date of this schedule
        /// </summary>
        private DateTime _startDate;
        /// <summary>
        /// the end date of this schedule
        /// </summary>
        private DateTime? _endDate;

        private TimeSpan _engineRunTime;
        #endregion

        public AbstractSchedule()
        {
            _startDate = DateTime.MinValue;
            _endDate = DateTime.MaxValue;
        }
        public AbstractSchedule(ISchedule schedule)
        {
            _startDate = schedule.StartDate;
            _endDate = schedule.EndDate;
        }

        #region ISchedule Members

        public abstract string Description
        {
            get;
        }

        public virtual string ValidateSchedule()
        {
            if (_startDate == DateTime.MinValue)
            {
                return "Start Date not set";
            }
            if (_endDate == DateTime.MaxValue)
            {
                return "End Date not set";
            }
            return null;
        }

        public abstract DateTime NextScheduledDate(DateTime dateTimeUTC);

        public DateTime StartDate
        {
            get { return _startDate; }
            set { _startDate = DateTime.SpecifyKind(value, DateTimeKind.Unspecified); }
        }
        
        public DateTime? EndDate
        {
            get { return _endDate; }
            set
            {
                if (value.HasValue)
                    _endDate = DateTime.SpecifyKind(value.Value, DateTimeKind.Unspecified);
                else _endDate = null;
            }
        }

        public TimeSpan EngineRunTime
        {
            get { return _engineRunTime; }
            set { _engineRunTime = value; }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Service.Route.Data.Schedule
{
    /// <summary>
    /// represents a daily schedule
    /// </summary>
    [Serializable]
    public class DailySchedule : AbstractSchedule
    {
        #region private fields
        /// <summary>
        /// repeat the schedule in x number of days
        /// </summary>
        private int _repeatInXDays;
        #endregion

        public DailySchedule()
            : base()
        {
            _repeatInXDays = 1;
        }
        public DailySchedule(ISchedule schedule)
            : base(schedule)
        {
            _repeatInXDays = 1;
            if (schedule is DailySchedule)
            {
                _repeatInXDays = ((DailySchedule)schedule).RepeatInXDays;
            }
        }

        public int RepeatInXDays
        {
            get { return _repeatInXDays; }
            set { _repeatInXDays = value; }
        }

        #region ISchedule Members

        [System.Xml.Serialization.XmlIgnore]
        public override string Description
        {
            get
            {
                string error = ValidateSchedule();
                if (error != null)
                {
                    return error;
                }
                if (_repeatInXDays <= 0)
                {
                    return string.Format("Route will be scheduled once on {0}", StartDate.ToString("dd/MM/yyyy"));
                }

                string des = "Route will be scheduled every day.";
                if (_repeatInXDays > 1)
                {
                    des = string.Format("Route will be scheduled every {0} days.", _repeatInXDays);
                }
                if (EndDate.HasValue)
                    des = string.Format("{0}. Route will be scheduled between {1:dd/MM/yyyy} and {2:dd/MM/yyyy}.", des, StartDate.Date, EndDate.Value.Date);
                else
                    des = string.Format("{0}. Route will be scheduled from {1:dd/MM/yyyy}.", des, StartDate.Date);


                return des;
            }
        }

        public override DateTime NextScheduledDate(DateTime dateTimeUTC)
        {
            if (dateTimeUTC < StartDate || _repeatInXDays <= 0)
            {
                return StartDate;
            }
            if (dateTimeUTC > EndDate)
            {
                return GetLastNextDate();
            }
            DateTime next = new DateTime(dateTimeUTC.Year, dateTimeUTC.Month, dateTimeUTC.Day);//, StartDate.Hour, StartDate.Minute, StartDate.Second);
            if (next < dateTimeUTC)
            {
                next = next.AddDays(1);
            }
            TimeSpan span = next - StartDate;
            int nextDay = span.Days % _repeatInXDays;
            if (nextDay > 0)
            {
                nextDay = _repeatInXDays - nextDay;
                next = next.AddDays(nextDay);
            }

            if (!EndDate.HasValue)
            {
                return next;
            }
            if (next < EndDate)
            {
                return next;
            }
            return GetLastNextDate();
        }

        #endregion

        private DateTime GetLastNextDate()
        {
            DateTime end = (EndDate.HasValue ? EndDate.Value : DateTime.MaxValue);
            DateTime next = new DateTime(end.Year, end.Month, end.Day, end.Hour, end.Minute, end.Second);
            if (next > EndDate)
            {
                next.AddDays(-1);
            }
            TimeSpan span = next - StartDate;
            int nextDay = (span.Days % _repeatInXDays) * -1;
            next = next.AddDays(nextDay);
            return next;
        }

    }
}

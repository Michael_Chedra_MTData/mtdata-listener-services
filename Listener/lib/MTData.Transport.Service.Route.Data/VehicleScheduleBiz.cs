using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using MTData.Common.DataAccess;
using MTData.Common.Plugin.Configuration;

namespace MTData.Transport.Service.Route.Data
{
    /// <summary>
    /// Summary description for Route2.
    /// </summary>
    public class VehicleScheduleBiz : BaseBusinessLogic
    {
        public VehicleScheduleBiz(string dsn)
            : base(dsn)
        {
        }

        public PluginMethodResult GetVehicleSchedule(int vehicleScheduleId)
        {
            PluginMethodResult result = new PluginMethodResult();

            DataSetVehicleSchedule ds = new DataSetVehicleSchedule();
            ds.EnforceConstraints = false;

            result.DataSet = GetVehicleSchedule(ds, vehicleScheduleId);

            return result;
        }

        protected DataSetVehicleSchedule GetVehicleSchedule(DataSetVehicleSchedule ds, int vehicleScheduleId)
        {
            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            {
                dal.GetVehicleScheduleById(ds, vehicleScheduleId);
            }

            return ds;
        }

        public PluginMethodResult FindVehicleSchedule(int[] fleetIds, DateTime utcFromDateInclusive, DateTime utcToDateExclusive)
        {
            PluginMethodResult result = new PluginMethodResult();

            DataSetVehicleSchedule ds = new DataSetVehicleSchedule();
            ds.EnforceConstraints = false;

            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            {
                foreach (int fleetId in fleetIds)
                    dal.FindVehicleSchedule(ds, fleetId, utcFromDateInclusive, utcToDateExclusive);
            }

            result.DataSet = ds;

            return result;
        }

        public PluginMethodResult VehicleScheduleUpdate(DataSetVehicleSchedule ds, int assignedUserId)
        {
            PluginMethodResult result = new PluginMethodResult();

            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            using (var trans = _connection.BeginTransaction())
            {
                try
                {
                    ArrayList rowsToAccept = new ArrayList();

                    ds.EnforceConstraints = false;      // Cannot use local constraints, let SQLServer perform constraints
                                                        // the DataSet may only contain Changes and referential data (that has not changed) will incorrectly throw

                    foreach (DataSetVehicleSchedule.T_VehicleScheduleRow scheduleRow in ds.T_VehicleSchedule)
                    {
                        try
                        {
                            if (scheduleRow.RowState == DataRowState.Added)
                                dal.InsertVehicleScheduleRow(scheduleRow, assignedUserId);

                            else if (scheduleRow.RowState == DataRowState.Modified)
                                dal.UpdateVehicleScheduleRow(scheduleRow, assignedUserId);

                            else if (scheduleRow.RowState == DataRowState.Deleted)
                                dal.DeleteVehicleScheduleRow(scheduleRow);

                            rowsToAccept.Add(scheduleRow);
                        }
                        catch (SqlException se)
                        {
                            if (se.Number > 50000)		// User defined Errors
                                result.AddWarning(new PluginMethodResult.RowResult(scheduleRow.ID, se.Number, se.Message));
                            else
                                throw se;				// This will Rollback and none of the Rows will AcceptChanges
                        }
                    }

                    trans.Commit();

                    foreach (DataRow dataRow in rowsToAccept)
                        dataRow.AcceptChanges();

                    result.DataSet = ds;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    result.AddError(ex.Message);
                }
            }
            return result;
        }



        public PluginMethodResult GetActiveRouteStatus(int fleetID, bool activeRowOnly, int? vehicle)
        {
            PluginMethodResult result = new PluginMethodResult();
            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            {
                result.DataSet = dal.GetActiveRouteStatus(fleetID, activeRowOnly, vehicle);
            }
            return result;

        }

        public PluginMethodResult GetActiveRouteStatusByUser(int userID, bool activeRowOnly)
        {
            PluginMethodResult result = new PluginMethodResult();
            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            {
                result.DataSet = dal.GetActiveRouteStatusByUser(userID, activeRowOnly);
            }
            return result;
        }

        public PluginMethodResult GetDriverScheduleGetById(int id)
        {
            PluginMethodResult result = new PluginMethodResult();

            var ds = new DataSetDriverRouteAllocation();
            ds.EnforceConstraints = false;
            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            {
                result.DataSet = dal.GetDriverScheduleGetById(ds, id);
            }

            return result;
        }

        public PluginMethodResult FindDriverSchedule(int[] fleetIds, DateTime utcFromDateInclusive, DateTime utcToDateExclusive, bool includeFatigue)
        {
            PluginMethodResult result = new PluginMethodResult();

            DataSetDriverRouteAllocation ds = new DataSetDriverRouteAllocation();
            ds.EnforceConstraints = false;

            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            {
                foreach (int fleetId in fleetIds)
                    dal.FindDriverSchedule(ds, fleetId, utcFromDateInclusive, utcToDateExclusive);



                if (includeFatigue)
                {
                    using (SqlCommand cmd = new SqlCommand("fsp_PredictDriverRoute", dal.Connection))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        SqlParameter parameter1 = new SqlParameter("@RouteToDriverID", SqlDbType.Int);
                        cmd.Parameters.Add(parameter1);
                        SqlParameter parameter2 = new SqlParameter("@Result", SqlDbType.Int);
                        parameter2.Direction = ParameterDirection.Output;
                        cmd.Parameters.Add(parameter2);


                        foreach (DataSetDriverRouteAllocation.T_RouteToDriverAllocationsRow dr in ds.T_RouteToDriverAllocations)
                        {
                            try
                            {
                                if (dr.StartTime < DateTime.UtcNow.AddDays(2) && dr.EndTime > DateTime.UtcNow)
                                {

                                    cmd.Parameters["@RouteToDriverID"].Value = dr.ID;
                                    cmd.ExecuteNonQuery();
                                    dr.FatigueState = Convert.ToInt32(cmd.Parameters["@Result"].Value);
                                }
                            }
                            catch (Exception ex)
                            {
                                Console.WriteLine(this.GetType().FullName + ".FindDriverSchedule(int[] fleetIds, DateTime utcFromDateInclusive, DateTime utcToDateExclusive, bool includeFatigue) Error : " + ex.Message);
                            }
                        }
                    }
                }

            }


            result.DataSet = ds;

            return result;
        }



        public PluginMethodResult RouteToDriverAllocationUpdate(DataSetDriverRouteAllocation ds, int userId)
        {
            PluginMethodResult result = new PluginMethodResult();
            using (VehicleScheduleDAL dal = (VehicleScheduleDAL)_Open(new VehicleScheduleDAL()))
            using (var trans = _connection.BeginTransaction())
            {
                try
                {
                    ArrayList rowsToAccept = new ArrayList();

                    ds.EnforceConstraints = false;      // Cannot use local constraints, let SQLServer perform constraints
                                                        // the DataSet may only contain Changes and referential data (that has not changed) will incorrectly throw

                    foreach (DataSetDriverRouteAllocation.T_RouteToDriverAllocationsRow scheduleRow in ds.T_RouteToDriverAllocations)
                    {
                        try
                        {
                            if (scheduleRow.RowState == DataRowState.Added)
                                dal.InsertDriverScheduleRow(scheduleRow, userId);

                            else if (scheduleRow.RowState == DataRowState.Modified)
                                dal.UpdateDriverScheduleRow(scheduleRow, userId);

                            else if (scheduleRow.RowState == DataRowState.Deleted)
                                dal.DeleteDriverScheduleRow(scheduleRow, userId);

                            rowsToAccept.Add(scheduleRow);
                        }
                        catch (SqlException se)
                        {
                            if (se.Number > 50000)		// User defined Errors
                                result.AddWarning(new PluginMethodResult.RowResult(scheduleRow.ID, se.Number, se.Message));
                            else
                                throw se;				// This will Rollback and none of the Rows will AcceptChanges
                        }
                    }

                    trans.Commit();

                    foreach (DataRow dataRow in rowsToAccept)
                        dataRow.AcceptChanges();

                    result.DataSet = ds;
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    result.AddError(ex.Message);
                }
            }
            return result;
        }
    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using MTData.Common.DataAccess;

namespace MTData.Transport.Service.Route.Data
{
    /// <summary>
    /// Data Access Layer for VehicleSchedule
    /// </summary>
    public class VehicleScheduleDAL : BaseDataAccess
    {
        public VehicleScheduleDAL()
        {
        }
        private static void _PrepareAdapter(OptimizedSqlDataAdapter adapter)
        {
            adapter.AddColumnLoaderFactory("UtcStartDate", new OptimizedSqlDataAdapter.DateTimeToDoubleConverterFactory());
            adapter.AddColumnLoaderFactory("UtcEndDate", new OptimizedSqlDataAdapter.DateTimeToDoubleConverterFactory());
            adapter.AddColumnLoaderFactory("UtcModifiedDate", new OptimizedSqlDataAdapter.DateTimeToDoubleConverterFactory());
            adapter.AddColumnLoaderFactory("UtcDownloadedDate", new OptimizedSqlDataAdapter.DateTimeToDoubleConverterFactory());
        }

        #region Get Methods
        public DataSetVehicleSchedule GetVehicleScheduleById(DataSetVehicleSchedule ds, int vehicleScheduleId)
        {
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_GetVehicleScheduleByID")))
            {
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = vehicleScheduleId;
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter(cmd);
                _PrepareAdapter(adapter);
                adapter.Fill(ds.T_VehicleSchedule);
                return ds;
            }
        }

        public DataSetVehicleSchedule FindVehicleSchedule(DataSetVehicleSchedule ds, int fleetId, DateTime utcFromDateInclusive, DateTime utcToDateExclusive)
        {
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_FindVehicleSchedule")))
            {
                cmd.Parameters.Add("@FleetID", SqlDbType.Int).Value = fleetId;
                cmd.Parameters.Add("@UtcFromDateInclusive", SqlDbType.DateTime).Value = utcFromDateInclusive;
                cmd.Parameters.Add("@UtcToDateExclusive", SqlDbType.DateTime).Value = utcToDateExclusive;
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter(cmd);
                _PrepareAdapter(adapter);
                adapter.Fill(ds.T_VehicleSchedule);
                return ds;
            }
        }

        #endregion

        #region Insert/Update Methods
        public DataSetVehicleSchedule InsertUpdateVehicleScheduleRows(DataSetVehicleSchedule ds, int assignedUserID)
        {
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleInsert")))
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleUpdate")))
            {
                insertCmd.Parameters.Add("@VehicleID", SqlDbType.Int, 4, "VehicleID");
                insertCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                insertCmd.Parameters.Add("@UtcStartDate", SqlDbType.DateTime, 8, "UtcStartDate");
                insertCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                insertCmd.Parameters.Add("@AssignedUserID", SqlDbType.Int, 4, "AssignedUserID");
                insertCmd.Parameters.Add("@RequestedState", SqlDbType.Int, 4, "State");
                insertCmd.Parameters.Add("@Destination", SqlDbType.NVarChar, 255, "Destination");
                insertCmd.Parameters.Add("@LoadNumber", SqlDbType.NVarChar, 255, "LoadNumber");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@VehicleID", SqlDbType.Int, 4, "VehicleID");
                updateCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                updateCmd.Parameters.Add("@UtcStartDate", SqlDbType.DateTime, 8, "UtcStartDate");
                updateCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                updateCmd.Parameters.Add("@AssignedUserID", SqlDbType.Int, 4, "AssignedUserID");
                updateCmd.Parameters.Add("@RequestedState", SqlDbType.Int, 4, "State");
                updateCmd.Parameters.Add("@Destination", SqlDbType.NVarChar, 255, "Destination");
                updateCmd.Parameters.Add("@LoadNumber", SqlDbType.NVarChar, 255, "LoadNumber");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                _PrepareAdapter(adapter);
                adapter.AddParameterOverride("@AssignedUserID", assignedUserID);
                adapter.InsertCommand = insertCmd;
                adapter.UpdateCommand = updateCmd;
                adapter.Update(ds.T_VehicleSchedule);
                return ds;
            }
        }

        public int InsertVehicleScheduleRow(DataSetVehicleSchedule.T_VehicleScheduleRow row, int assignedUserID)
        {
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleInsert")))
            {
                insertCmd.Parameters.Add("@VehicleID", SqlDbType.Int, 4, "VehicleID");
                insertCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                insertCmd.Parameters.Add("@UtcStartDate", SqlDbType.DateTime, 8, "UtcStartDate");
                insertCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                insertCmd.Parameters.Add("@AssignedUserID", SqlDbType.Int, 4, "AssignedUserID");
                insertCmd.Parameters.Add("@RequestedState", SqlDbType.Int, 4, "State");
                insertCmd.Parameters.Add("@Destination", SqlDbType.NVarChar, 255, "Destination");
                insertCmd.Parameters.Add("@LoadNumber", SqlDbType.NVarChar, 255, "LoadNumber");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                _PrepareAdapter(adapter);
                adapter.AddParameterOverride("@AssignedUserID", assignedUserID);
                adapter.InsertCommand = insertCmd;
                return adapter.Update(row);
            }
        }

        public int UpdateVehicleScheduleRow(DataSetVehicleSchedule.T_VehicleScheduleRow row, int assignedUserID)
        {
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleUpdate")))
            {
                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@VehicleID", SqlDbType.Int, 4, "VehicleID");
                updateCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                updateCmd.Parameters.Add("@UtcStartDate", SqlDbType.DateTime, 8, "UtcStartDate");
                updateCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                updateCmd.Parameters.Add("@AssignedUserID", SqlDbType.Int, 4, "AssignedUserID");
                updateCmd.Parameters.Add("@RequestedState", SqlDbType.Int, 4, "State");
                updateCmd.Parameters.Add("@Destination", SqlDbType.NVarChar, 255, "Destination");
                updateCmd.Parameters.Add("@LoadNumber", SqlDbType.NVarChar, 255, "LoadNumber");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                _PrepareAdapter(adapter);
                adapter.AddParameterOverride("@AssignedUserID", assignedUserID);
                adapter.UpdateCommand = updateCmd;
                return adapter.Update(row);
            }
        }

        public DataSetVehicleSchedule VehicleScheduleDownloadedRows(DataSetVehicleSchedule ds)
        {
            using (SqlCommand command = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleDownloaded")))
            {
                command.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                foreach (DataRow dataRow in ds.T_VehicleSchedule)
                {
                    OptimizedSqlDataAdapter.PrepareCommand(command, dataRow, DataRowVersion.Current);
                    command.ExecuteNonQuery();
                }
                return ds;
            }
        }
        #endregion

        #region Delete Methods
        public DataSetVehicleSchedule DeleteVehicleScheduleRows(DataSetVehicleSchedule ds)
        {
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                _PrepareAdapter(adapter);
                adapter.DeleteCommand = deleteCmd;
                adapter.Update(ds.T_VehicleSchedule);
                return ds;
            }
        }

        public int DeleteVehicleScheduleRow(DataSetVehicleSchedule.T_VehicleScheduleRow row)
        {
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_VehicleScheduleDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                _PrepareAdapter(adapter);
                adapter.DeleteCommand = deleteCmd;
                return adapter.Update(row);
            }
        }
        #endregion


        public DataSet GetActiveRouteStatus(int fleetID, bool activeRowOnly, int? vehicle)
        {
            DataSet ds = new DataSet();
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2GetActiveRouteStatus")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.AddWithValue("@FleetID", fleetID);
                cmd.Parameters.AddWithValue("@ActiveRowOnly", activeRowOnly);
                if (vehicle.HasValue)
                    cmd.Parameters.AddWithValue("@VehicleID", vehicle.Value);

                adapter.FillSchema(ds, SchemaType.Source);
                if (ds.Tables.Count > 0)
                {
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        if (dc.DataType == typeof(DateTime))
                            dc.DateTimeMode = DataSetDateTime.Utc;
                    }
                }

                adapter.Fill(ds);

                return ds;
            }
        }

        internal DataSet GetActiveRouteStatusByUser(int userID, bool activeRowOnly)
        {
            DataSet ds = new DataSet();
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_Route2GetActiveRouteStatusByUser")))
            using (SqlDataAdapter adapter = new SqlDataAdapter(cmd))
            {
                cmd.Parameters.AddWithValue("@UserID", userID);
                cmd.Parameters.AddWithValue("@ActiveRowOnly", activeRowOnly);

                adapter.FillSchema(ds, SchemaType.Source);
                if (ds.Tables.Count > 0)
                {
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        if (dc.DataType == typeof(DateTime))
                            dc.DateTimeMode = DataSetDateTime.Utc;
                    }
                }

                adapter.Fill(ds);

                return ds;
            }
        }

        internal DataSet FindDriverSchedule(DataSetDriverRouteAllocation ds, int fleetId, DateTime utcFromDateInclusive, DateTime utcToDateExclusive)
        {
            using (SqlCommand cmd = ConfigureStoredProc(new SqlCommand("usp_FindDriverRouteSchedule")))
            {
                cmd.Parameters.Add("@FleetID", SqlDbType.Int).Value = fleetId;
                cmd.Parameters.Add("@UtcFromDateInclusive", SqlDbType.DateTime).Value = utcFromDateInclusive;
                cmd.Parameters.Add("@UtcToDateExclusive", SqlDbType.DateTime).Value = utcToDateExclusive;

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter(cmd);
                //_PrepareAdapter(adapter);
                adapter.Fill(ds.T_RouteToDriverAllocations);
                return ds;
            }
        }

        public DataSet GetDriverScheduleGetById(DataSetDriverRouteAllocation ds, int id)
        {
            using (SqlCommand cmd = ConfigureText(new SqlCommand("select ID, DriverID, RouteScheduleID, StartTime, FleetID, Destination, LoadNumber, TimeToCheck, TimeToFail, VehicleScheduleID, TimeProcessed, UserID, Active, Failed, ConflictType from T_RouteToDriverAllocations where ID =@ID")))
            {
                cmd.Parameters.Add("@ID", SqlDbType.Int).Value = id;

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter(cmd);
                _PrepareAdapter(adapter);
                adapter.Fill(ds.T_RouteToDriverAllocations);
                return ds;
            }
        }

        public int InsertDriverScheduleRow(DataSetDriverRouteAllocation.T_RouteToDriverAllocationsRow row, int assignedUserID)
        {
            using (SqlCommand insertCmd = ConfigureStoredProc(new SqlCommand("usp_RouteToDriverAllocationsInsert")))
            {
                insertCmd.Parameters.Add("@DriverID", SqlDbType.Int, 4, "DriverID");
                insertCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                insertCmd.Parameters.Add("@StartTime", SqlDbType.DateTime, 8, "StartTime");
                insertCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                insertCmd.Parameters.Add("@Destination", SqlDbType.NVarChar, 255, "Destination");
                insertCmd.Parameters.Add("@LoadNumber", SqlDbType.NVarChar, 255, "LoadNumber");
                insertCmd.Parameters.Add("@TimeToCheck", SqlDbType.DateTime, 8, "TimeToCheck");
                insertCmd.Parameters.Add("@TimeToFail", SqlDbType.DateTime, 8, "TimeToFail");
                insertCmd.Parameters.Add("@VehicleScheduleID", SqlDbType.Int, 4, "VehicleScheduleID");
                insertCmd.Parameters.Add("@TimeProcessed", SqlDbType.DateTime, 8, "TimeProcessed");
                insertCmd.Parameters.Add("@UserID", SqlDbType.Int, 4, "UserID");
                insertCmd.Parameters.Add("@Active", SqlDbType.Bit, 1, "Active");
                insertCmd.Parameters.Add("@ConflictType", SqlDbType.Int, 4, "ConflictType");
                insertCmd.UpdatedRowSource = UpdateRowSource.FirstReturnedRecord;

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                //_PrepareAdapter(adapter);
                //adapter.AddParameterOverride("@AssignedUserID", assignedUserID);
                adapter.InsertCommand = insertCmd;
                adapter.AddParameterOverride("@UserID", assignedUserID);
                return adapter.Update(row);
            }
        }

        public int UpdateDriverScheduleRow(DataSetDriverRouteAllocation.T_RouteToDriverAllocationsRow row, int assignedUserID)
        {
            using (SqlCommand updateCmd = ConfigureStoredProc(new SqlCommand("usp_RouteToDriverAllocationsUpdate")))
            {
                updateCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");
                updateCmd.Parameters.Add("@DriverID", SqlDbType.Int, 4, "DriverID");
                updateCmd.Parameters.Add("@RouteScheduleID", SqlDbType.Int, 4, "RouteScheduleID");
                updateCmd.Parameters.Add("@StartTime", SqlDbType.DateTime, 8, "StartTime");
                updateCmd.Parameters.Add("@FleetID", SqlDbType.Int, 4, "FleetID");
                updateCmd.Parameters.Add("@Destination", SqlDbType.NVarChar, 255, "Destination");
                updateCmd.Parameters.Add("@LoadNumber", SqlDbType.NVarChar, 255, "LoadNumber");
                updateCmd.Parameters.Add("@TimeToCheck", SqlDbType.DateTime, 8, "TimeToCheck");
                updateCmd.Parameters.Add("@TimeToFail", SqlDbType.DateTime, 8, "TimeToFail");
                updateCmd.Parameters.Add("@VehicleScheduleID", SqlDbType.Int, 4, "VehicleScheduleID");
                updateCmd.Parameters.Add("@TimeProcessed", SqlDbType.DateTime, 8, "TimeProcessed");
                updateCmd.Parameters.Add("@UserID", SqlDbType.Int, 4, "UserID");
                updateCmd.Parameters.Add("@Active", SqlDbType.Bit, 1, "Active");
                updateCmd.Parameters.Add("@ConflictType", SqlDbType.Int, 4, "ConflictType");
                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                //_PrepareAdapter(adapter);
                //adapter.AddParameterOverride("@AssignedUserID", assignedUserID);
                adapter.UpdateCommand = updateCmd;
                adapter.AddParameterOverride("@UserID", assignedUserID);
                return adapter.Update(row);
            }
        }


        public int DeleteDriverScheduleRow(DataSetDriverRouteAllocation.T_RouteToDriverAllocationsRow row, int userID)
        {
            using (SqlCommand deleteCmd = ConfigureStoredProc(new SqlCommand("usp_RouteToDriverAllocationsDelete")))
            {
                deleteCmd.Parameters.Add("@ID", SqlDbType.Int, 4, "ID");

                OptimizedSqlDataAdapter adapter = new OptimizedSqlDataAdapter();
                //_PrepareAdapter(adapter);
                adapter.DeleteCommand = deleteCmd;
                adapter.AddParameterOverride("@UserID", userID);
                return adapter.Update(row);
            }
        }
    }
}

using System;
using System.Collections;

namespace MTData.Transport.Service.Route.Data
{
	public class T_Route2PointRowIndexComparer : IComparer
	{
		private int _reverseDirection = 1;

		public T_Route2PointRowIndexComparer()
		{
		}

		public T_Route2PointRowIndexComparer(bool reverseDirection)
		{
			_reverseDirection = reverseDirection ? -1 : 1;
		}

		public int Compare(object x, object y)
		{
			DataSetRoute2.T_Route2PointRow pointX = (DataSetRoute2.T_Route2PointRow) x;
			if (x == null)
				return -1 * _reverseDirection;
			DataSetRoute2.T_Route2PointRow pointY = (DataSetRoute2.T_Route2PointRow) y;
			if (y == null)
				return 1 * _reverseDirection;
			
			double diff = pointX.Index - pointY.Index;
			if (diff < 0)
				return -1 * _reverseDirection;
			else if (diff > 0)
				return 1 * _reverseDirection;

			return 0;
		}
	}

	public class T_Route2CheckPointRowIndexComparer : IComparer
	{
		private int _reverseDirection = 1;

		public T_Route2CheckPointRowIndexComparer()
		{
		}

		public T_Route2CheckPointRowIndexComparer(bool reverseDirection)
		{
			_reverseDirection = reverseDirection ? -1 : 1;
		}

		public int Compare(object x, object y)
		{
			DataSetRoute2.T_Route2CheckPointRow checkPointX = x as DataSetRoute2.T_Route2CheckPointRow;
			if (x == null)
				return -1 * _reverseDirection;
			DataSetRoute2.T_Route2CheckPointRow checkPointY = y as DataSetRoute2.T_Route2CheckPointRow;
			if (y == null)
				return 1 * _reverseDirection;
			
			double diff = checkPointX.T_Route2PointRow.Index - checkPointY.T_Route2PointRow.Index;
			if (diff < 0)
				return -1 * _reverseDirection;
			else if (diff > 0)
				return 1 * _reverseDirection;

			return 0;
		}
	}

	public class T_Route2ScheduleCheckPointRowIndexComparer : IComparer
	{
		private int _reverseDirection = 1;

		public T_Route2ScheduleCheckPointRowIndexComparer()
		{
		}

		public T_Route2ScheduleCheckPointRowIndexComparer(bool reverseDirection)
		{
			_reverseDirection = reverseDirection ? -1 : 1;
		}

		public int Compare(object x, object y)
		{
			DataSetRoute2.T_Route2ScheduleCheckPointRow scheduleCheckPointX = x as DataSetRoute2.T_Route2ScheduleCheckPointRow;
			if (x == null)
				return -1 * _reverseDirection;
			DataSetRoute2.T_Route2ScheduleCheckPointRow scheduleCheckPointY = y as DataSetRoute2.T_Route2ScheduleCheckPointRow;
			if (y == null)
				return 1 * _reverseDirection;
			
			double diff = scheduleCheckPointX.T_Route2CheckPointRow.T_Route2PointRow.Index - scheduleCheckPointY.T_Route2CheckPointRow.T_Route2PointRow.Index;
			if (diff < 0)
				return -1 * _reverseDirection;
			else if (diff > 0)
				return 1 * _reverseDirection;

			return 0;
		}
	}
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.Service.Route.Data
{
    public class VehicleScheduleUpdate
    {
        #region Private Variables

        private object _context;

        private int _fleetID;

        private int _vehicleID;

        private int _vehicleScheduleID;

        private int _checkPointIndex;

        private DateTime? _actualDepartTimeUTC;

        private DateTime? _scheduledDepartTimeUTC;

        private DateTime? _scheduledCPArrivalTimeUTC;

        private DateTime? _scheduledCustomerArrivalTimeUTC;

        private DateTime? _scheduledRouteEndTimeUTC;

        #endregion

        #region Properties

        public object Context
        {
            get { return _context; }
            set { _context = value; }
        }

        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public int VehicleID
        {
            get { return _vehicleID; }
            set { _vehicleID = value; }
        }

        public int VehicleScheduleID
        {
            get { return _vehicleScheduleID; }
            set { _vehicleScheduleID = value; }
        }

        public int CheckPointIndex
        {
            get { return _checkPointIndex; }
            set { _checkPointIndex = value; }
        }

        public DateTime? ActualDepartTimeUCT
        {
            get { return _actualDepartTimeUTC; }
            set { _actualDepartTimeUTC = value; }
        }

        public DateTime? ScheduledDepartTimeUTC
        {
            get { return _scheduledDepartTimeUTC; }
            set { _scheduledDepartTimeUTC = value; }
        }

        public DateTime? ScheduledCPArrivalTimeUTC
        {
            get { return _scheduledCPArrivalTimeUTC; }
            set { _scheduledCPArrivalTimeUTC = value; }
        }

        public DateTime? ScheduledCustomerArrivalTimeUTC
        {
            get { return _scheduledCustomerArrivalTimeUTC; }
            set { _scheduledCustomerArrivalTimeUTC = value; }
        }

        public DateTime? ScheduledRouteEndTimeUTC
        {
            get { return _scheduledRouteEndTimeUTC; }
            set { _scheduledRouteEndTimeUTC = value; }

        }

        public double? TimeDifferenceInMinutes
        {
            get
            {
                if (ActualDepartTimeUCT.HasValue && ScheduledDepartTimeUTC.HasValue)
                {
                    if (ScheduledCPArrivalTimeUTC.HasValue && DateTime.UtcNow > ScheduledCPArrivalTimeUTC)
                    {
                        TimeSpan arrivalDiff = (DateTime.UtcNow - ScheduledCPArrivalTimeUTC.Value);
                        TimeSpan departDiff = (ActualDepartTimeUCT.Value - ScheduledDepartTimeUTC.Value);

                        return System.Math.Max(arrivalDiff.TotalMinutes, departDiff.TotalMinutes);
                    }
                    else
                    {
                        TimeSpan departDiff = (ActualDepartTimeUCT.Value - ScheduledDepartTimeUTC.Value);
                        return departDiff.TotalMinutes;
                    }
                }
                else
                {
                    if (ScheduledCPArrivalTimeUTC.HasValue && DateTime.UtcNow > ScheduledCPArrivalTimeUTC)
                    {
                        TimeSpan arrivalDiff = (DateTime.UtcNow - ScheduledCPArrivalTimeUTC.Value);
                        return arrivalDiff.TotalMinutes;
                    }
                    else
                    {
                        return null;
                    }
                }


            }
        }

        public DateTime? NextCustomerEstArrivalTimeUTC
        {
            get
            {
                if (this.ScheduledCustomerArrivalTimeUTC.HasValue && TimeDifferenceInMinutes.HasValue)
                    return ScheduledCustomerArrivalTimeUTC.Value.AddMinutes(TimeDifferenceInMinutes.Value);
                else
                    return null;
            }
        }
        #endregion

        #region Constructors

        /// <summary>
        /// The default Constructor.
        /// </summary>
        public VehicleScheduleUpdate()
        {
        }

        #endregion

        #region Public Methods
        #endregion

        #region Private Methods
        #endregion

    }
}

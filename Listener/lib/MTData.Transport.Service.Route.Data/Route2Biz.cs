using System;
using System.Data;
using MTData.Common.DataAccess;

namespace MTData.Transport.Service.Route.Data
{
    /// <summary>
    /// Summary description for Route2.
    /// </summary>
    public class Route2Biz : BaseBusinessLogic
    {
        public Route2Biz(string dsn) : base(dsn)
        {
        }

        /// <summary>
        /// Get a Route and all details attached to the Route 
        /// (Points, CheckPoints, Schedules and ScheduleCheckPoints)
        /// </summary>
        /// <param name="route2Id">Id of Route to get</param>
        /// <returns>A populated DataSet with Route details or an Empty DataSet</returns>
        public DataSetRoute2 GetRoute2(int route2Id)
        {
            DataSetRoute2 ds = new DataSetRoute2();
            ds.EnforceConstraints = false;

            return GetRoute2(ds, route2Id);
        }

        /// <summary>
        /// Populates an existing DataSet with a Route and all details attached to the Route 
        /// (Points, CheckPoints, Schedules and ScheduleCheckPoints)
        /// </summary>
        /// <param name="route2Id">Id of Route to get</param>
        /// <returns>A populated DataSet with Route details or an Empty DataSet</returns>
        public DataSetRoute2 GetRoute2(DataSetRoute2 ds, int route2Id)
        {
            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                dal.GetRoute2RouteById(ds, route2Id);
                dal.GetRoute2PointsByRouteId(ds, route2Id);
                dal.GetRoute2CheckPointsByRouteId(ds, route2Id);
                dal.GetRoute2SchedulesByRouteId(ds, route2Id);
                dal.GetRoute2ScheduleCheckPointsByRouteId(ds, route2Id);
            }

            return ds;
        }


        /// <summary>
        /// Populates an existing DataSet with a Route Schedules details.
        /// Resulting DataSet includes the Route, any Check Points and any Schedule Check Points
        /// basically excluding the Points
        /// </summary>
        /// <param name="route2ScheduleId">Id of Route Schedule to get</param>
        /// <returns>A populated DataSet with Route details or an Empty DataSet</returns>
        public DataSetRoute2 GetRoute2ScheduleCheckPoints(DataSetRoute2 ds, int route2ScheduleId)
        {
            DataSetRoute2 mergeDS = new DataSetRoute2();
            mergeDS.EnforceConstraints = false;

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                // Get the Route Schedule
                dal.GetRoute2ScheduleById(mergeDS, route2ScheduleId);

                if (mergeDS.T_Route2Schedule.Count > 0)
                {
                    int route2Id = mergeDS.T_Route2Schedule[0].RouteID;

                    // Get the attached Route
                    dal.GetRoute2RouteById(mergeDS, route2Id);

                    // And its CheckPoints
                    dal.GetRoute2CheckPointsByRouteId(mergeDS, route2Id);

                    // And the Schedule's CheckPoints
                    dal.GetRoute2ScheduleCheckPointsByScheduleId(mergeDS, route2ScheduleId);
                }
            }

            ds.Merge(mergeDS, true);

            return ds;
        }


        /// <summary>
        /// Given a Route Schedule will retrieve the full Route's details
        /// </summary>
        /// <param name="route2ScheduleId">A Route Schedule Id</param>
        /// <returns>A populated DataSet with Route details or an Empty DataSet</returns>
        public DataSetRoute2 GetRoute2CheckPointsByRouteId(int route2Id)
        {
            DataSetRoute2 ds = new DataSetRoute2();
            ds.EnforceConstraints = false;

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                dal.GetRoute2CheckPointsByRouteId(ds, route2Id);
            }
            return ds;
        }

        /// <summary>
        /// Given a Route Schedule will retrieve the full Route's details
        /// </summary>
        /// <param name="route2ScheduleId">A Route Schedule Id</param>
        /// <returns>A populated DataSet with Route details or an Empty DataSet</returns>
        public DataSetRoute2 GetRoute2BySchedule(int route2ScheduleId)
        {
            DataSetRoute2 ds = new DataSetRoute2();
            ds.EnforceConstraints = false;

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                dal.GetRoute2ScheduleById(ds, route2ScheduleId);
                if (ds.T_Route2Schedule.Rows.Count > 0)
                {
                    int route2Id = ds.T_Route2Schedule[0].RouteID;
                    return GetRoute2(ds, route2Id);
                }
            }

            return ds;
        }

        /// <summary>
        /// Find all Routes within specified list of Fleets.  Will *not* populate Route details
        /// </summary>
        /// <param name="fleetIds">List of Fleets to retrieve from</param>
        /// <param name="includeRoutesInError">True if to return Routes that are In Error, else False</param>
        /// <returns>List of Routes for each specified Fleet</returns>
        public DataSetRoute2 FindRoute2ByFleet(int[] fleetIds, bool includeRoutesInError)
        {
            DataSetRoute2 ds = new DataSetRoute2();
            ds.EnforceConstraints = false;

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                foreach (int fleetId in fleetIds)
                    dal.GetRoute2RoutesByFleetId(ds, fleetId, includeRoutesInError);
            }

            return ds;
        }

        /// <summary>
        /// Find all Route and their attached Schedules within specified list of Fleets.  Will *not* populate Route details
        /// </summary>
        /// <param name="fleetIds">List of Fleets to retrieve from</param>
        /// <param name="includeRoutesInError">True if to return Routes that are In Error, else False</param>
        /// <returns>List of Routes and Schedules for each specified Fleet</returns>
        public DataSetRoute2 FindRoute2AndSchedulesByFleet(int[] fleetIds, bool includeRoutesInError)
        {
            DataSetRoute2 ds = new DataSetRoute2();
            ds.EnforceConstraints = false;

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                foreach (int fleetId in fleetIds)
                {
                    dal.GetRoute2RoutesByFleetId(ds, fleetId, includeRoutesInError);
                    dal.GetRoute2SchedulesByFleetId(ds, fleetId, includeRoutesInError);
                }
            }

            return ds;
        }

        /// <summary>
        /// Get a Route's Schedule's
        /// </summary>
        /// <param name="route2Id">Route Schedule;s Route Id</param>
        /// <returns>A DataSet with Route Schedule's or an empty DataSet</returns>
        public DataSetRoute2 GetRouteSchedulesByRouteId(int route2Id)
        {
            DataSetRoute2 ds = new DataSetRoute2();
            ds.EnforceConstraints = false;

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
                dal.GetRoute2SchedulesByRouteId(ds, route2Id);

            return ds;
        }

        /// <summary>
        /// Get a Route Schedule
        /// </summary>
        /// <param name="routeScheduleId">Route Schedule Id to get</param>
        /// <returns>A DataSet with Route Schedule or an empty DataSet</returns>
        public DataSetRoute2 GetRouteScheduleById(int routeScheduleId)
        {
            DataSetRoute2 ds = new DataSetRoute2();

            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            {
                dal.GetRoute2ScheduleById(ds, routeScheduleId);
                dal.GetRoute2ScheduleCheckPointsByScheduleId(ds, routeScheduleId);
            }

            return ds;
        }

        /// <summary>
        /// Update a Route's details.  Will delete,insert,update all Route,Point,CheckPoint,Schedule and ScheduleCheckPoint rows
        /// in correct order.
        /// </summary>
        /// <param name="ds">The DataSet to update from</param>
        /// <returns>The results of the changes</returns>
        public DataSetRoute2 Route2Update(DataSetRoute2 ds)
        {
            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            using (var trans = _connection.BeginTransaction())
            {
                try
                {
                    ds.EnforceConstraints = false;      // Cannot use local constraints, let SQLServer perform constraints
                                                        // the DataSet may only contain Changes and referential data (that has not changed) will incorrectly throw

                    dal.DeleteRoute2ScheduleCheckPointRows(ds);
                    dal.DeleteRoute2ScheduleRows(ds);
                    dal.DeleteRoute2CheckPointRows(ds);
                    dal.DeleteRoute2PointRows(ds);
                    dal.DeleteRoute2Rows(ds);

                    dal.InsertUpdateRoute2Rows(ds);
                    dal.InsertUpdateRoute2PointRows(ds);
                    dal.InsertUpdateRoute2CheckPointRows(ds);
                    dal.InsertUpdateRoute2ScheduleRows(ds);
                    dal.InsertUpdateRoute2ScheduleCheckPointRows(ds);

                    // Any changes to any part of the Route should update its modified date
                    if ((ds.T_Route2.Count > 0) && (ds.T_Route2[0].RowState != DataRowState.Deleted))
                        dal.Touch(ds.T_Route2[0].ID);

                    trans.Commit();

                    ds.AcceptChanges();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }
            }
            return ds;
        }


        /// <summary>
        /// Get a Clone of a Route ready for download to a Vehicle
        /// </summary>
        /// <param name="route2Id">Id of Original Route to Clone</param>
        /// <returns>Id of a Clone Route that is most recent clone of original Route</returns>
        public int GetRoute2CloneId(int route2Id)
        {
            using (Route2DAL dal = (Route2DAL)_Open(new Route2DAL()))
            using (var trans = _connection.BeginTransaction())
            {
                int cloneId = 0;
                try
                {
                    cloneId = dal.GetRoute2CloneId(route2Id);

                    trans.Commit();
                }
                catch (Exception ex)
                {
                    trans.Rollback();
                    throw ex;
                }

                return cloneId;
            }
        }
    }
}

using System;
using System.Data;
using MTData.Common.Mathematics;

namespace MTData.Transport.Service.Route.Data
{
	/// <summary>
	/// Summary description for Route2PointPoint2DProvider.
	/// </summary>
	public class Route2PointPoint2DProvider : IPoint2DProvider
	{
		private DataView	_sortedPoints;

		public Route2PointPoint2DProvider(DataSetRoute2.T_Route2PointDataTable table)
		{
			_sortedPoints = new DataView(table);
			_sortedPoints.Sort = "Index";
		}

		public Point2D this[int index]
		{
			get
			{
				DataSetRoute2.T_Route2PointRow row = (DataSetRoute2.T_Route2PointRow)_sortedPoints[index].Row;
				return new Point2D(row.Longitude, row.Latitude);
			}
		}

		public int Count
		{
			get { return _sortedPoints.Count; }
		}
	}
}

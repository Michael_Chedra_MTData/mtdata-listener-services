#region

using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Xml.Serialization;
using log4net;
using MTData.Transport.Service.Map.Interface;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class MapImageResponse : IMapDataResponse
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapImageResponse));
        private MapImageBounds _bounds;
        private string _error;
        private Image _image;
        private MapImageRequest _request;

        public MapImageResponse(MapImageRequest request, MapImageBounds bounds, Image image)
        {
            try
            {
                _request = request;
                _bounds = bounds;
                _image = image;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageResponse(MapImageRequest request, MapImageBounds bounds, Image image)", ex);
                throw new Exception(GetType().FullName + ".MapImageResponse(MapImageRequest request, MapImageBounds bounds, Image image)", ex);
            }
        }

        public MapImageResponse()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageResponse()", ex);
                throw new Exception(GetType().FullName + ".MapImageResponse()", ex);
            }
        }

        public MapImageRequest Request
        {
            get { return _request; }
            set { _request = value; }
        }

        public MapImageBounds Bounds
        {
            get { return _bounds; }
            set { _bounds = value; }
        }

        [XmlIgnore]
        public Image Image
        {
            get { return _image; }
            set { _image = value; }
        }

        public byte[] ImageData
        {
            get
            {
                try
                {
                    return GetImageData();
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".ImageData (Get)", ex);
                    throw new Exception(GetType().FullName + ".ImageData (Get)", ex);
                }
            }
            set
            {
                try
                {
                    SetImageData(value);
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".ImageData (Set)", ex);
                    throw new Exception(GetType().FullName + ".ImageData (Set)", ex);
                }
            }
        }

        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }

        private byte[] GetImageData()
        {
            try
            {
                MemoryStream stream = new MemoryStream();
                _image.Save(stream, ImageFormat.Jpeg);
                return stream.ToArray();
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".GetImageData()", ex);
                throw new Exception(GetType().FullName + ".GetImageData()", ex);
            }
        }

        private void SetImageData(byte[] data)
        {
            try
            {
                MemoryStream stream = new MemoryStream(data);
                _image = new Bitmap(stream);
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".SetImageData(byte[] data)", ex);
                throw new Exception(GetType().FullName + ".SetImageData(byte[] data)", ex);
            }
        }
    }
}
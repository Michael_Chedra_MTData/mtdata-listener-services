#region

using System;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class StreetInfo
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (StreetInfo));

        private string _roadType;
        private int _speedLimit;
        private string _streetName;

        public StreetInfo()
        {
            _streetName = "";
            _roadType = "";
            _speedLimit = 0;
        }

        public StreetInfo(string streetName, string roadType, int speedLimit)
        {
            _streetName = streetName;
            _roadType = roadType;
            _speedLimit = speedLimit;
        }

        public string StreetName
        {
            get { return _streetName; }
            set { _streetName = value; }
        }

        public string RoadType
        {
            get { return _roadType; }
            set { _roadType = value; }
        }

        public int SpeedLimit
        {
            get { return _speedLimit; }
            set { _speedLimit = value; }
        }
    }
}
#region

using System;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    /// <summary>
    ///   This class holds the details of an address match
    /// </summary>
    [Serializable]
    public class AddressLookupMatchEntry
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupMatchEntry));
        private string _address;
        private double _latitude;
        private double _longitude;
        private int _percentMatch;
        private string _source;

        public AddressLookupMatchEntry(string address, string source, double latitude, double longitude, int percentMatch)
        {
            try
            {
                _address = address;
                _source = source;
                _latitude = latitude;
                _longitude = longitude;
                _percentMatch = percentMatch;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupMatchEntry(string address, string source, double latitude, double longitude, int percentMatch)", ex);
                throw new Exception(GetType().FullName + ".AddressLookupMatchEntry(string address, string source, double latitude, double longitude, int percentMatch)", ex);
            }
        }

        public AddressLookupMatchEntry()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupMatchEntry()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupMatchEntry()", ex);
            }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public int PercentMatch
        {
            get { return _percentMatch; }
            set { _percentMatch = value; }
        }
    }
}
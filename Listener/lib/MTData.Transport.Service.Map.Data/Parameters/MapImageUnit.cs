#region

using System;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class MapImageUnit
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapImageUnit));
        private int _fleetID;
        private int _unitID;

        public MapImageUnit(int fleetID, int unitID)
        {
            try
            {
                _fleetID = fleetID;
                _unitID = unitID;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageUnit(int fleetID = " + fleetID + ", int unitID = " + unitID + ")", ex);
                throw new Exception(GetType().FullName + ".MapImageUnit(int fleetID = " + fleetID + ", int unitID = " + unitID + ")", ex);
            }
        }

        public MapImageUnit()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageUnit()", ex);
                throw new Exception(GetType().FullName + ".MapImageUnit()", ex);
            }
        }

        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public int UnitID
        {
            get { return _unitID; }
            set { _unitID = value; }
        }
    }
}
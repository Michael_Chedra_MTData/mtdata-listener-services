#region

using System;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class MapImageReference
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapImageReference));
        private double _latitude;
        private double _longitude;
        private string _name;

        public MapImageReference(string name, double latitude, double longitude)
        {
            try
            {
                _name = name;
                _latitude = latitude;
                _longitude = longitude;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageReference(string name = '" + name + "', double latitude = " + latitude + ", double longitude = " + longitude + ")", ex);
                throw new Exception(GetType().FullName + ".MapImageReference(string name = '" + name + "', double latitude = " + latitude + ", double longitude = " + longitude + ")", ex);
            }
        }

        public MapImageReference()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageReference()", ex);
                throw new Exception(GetType().FullName + ".MapImageReference()", ex);
            }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
    }
}
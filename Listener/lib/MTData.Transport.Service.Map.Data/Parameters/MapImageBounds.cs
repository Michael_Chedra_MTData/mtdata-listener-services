#region

using System;
using System.Drawing;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class MapImageBounds
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapImageBounds));
        private double _latitudeN;
        private double _latitudeS;
        private double _longitudeE;
        private double _longitudeW;
        private Size _viewerSize;

        public MapImageBounds(Size viewerSize, double latitudeN, double latitudeS, double longitudeW, double longitudeE)
        {
            try
            {
                _viewerSize = viewerSize;
                _latitudeN = latitudeN;
                _latitudeS = latitudeS;
                _longitudeW = longitudeW;
                _longitudeE = longitudeE;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageBounds(Size viewerSize, double latitudeN = " + latitudeN + ", double latitudeS = " + latitudeS + ", double longitudeW = " + longitudeW + ", double longitudeE = " + longitudeE + ")", ex);
                throw new Exception(GetType().FullName + ".MapImageBounds(Size viewerSize, double latitudeN = " + latitudeN + ", double latitudeS = " + latitudeS + ", double longitudeW = " + longitudeW + ", double longitudeE = " + longitudeE + ")", ex);
            }
        }

        public MapImageBounds()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageBounds()", ex);
                throw new Exception(GetType().FullName + ".MapImageBounds()", ex);
            }
        }

        public Size ViewerSize
        {
            get { return _viewerSize; }
            set { _viewerSize = value; }
        }

        public double LatitudeN
        {
            get { return _latitudeN; }
            set { _latitudeN = value; }
        }

        public double LatitudeS
        {
            get { return _latitudeS; }
            set { _latitudeS = value; }
        }

        public double LongitudeW
        {
            get { return _longitudeW; }
            set { _longitudeW = value; }
        }

        public double LongitudeE
        {
            get { return _longitudeE; }
            set { _longitudeE = value; }
        }
    }
}
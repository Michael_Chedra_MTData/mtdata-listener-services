#region

using System;
using System.Collections;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class AddressLookupViaGoogleResponse : IMapDataResponse
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupViaGoogleResponse));
        private readonly ArrayList _matchEntries = new ArrayList();
        private string _error;
        private AddressLookupViaGoogleRequest _request;

        public AddressLookupViaGoogleResponse(AddressLookupViaGoogleRequest request, AddressLookupMatchEntry[] matches)
        {
            try
            {
                _request = request;
                Matches = matches;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupViaGoogleResponse(AddressLookupViaGoogleRequest request, AddressLookupMatchEntry[] matches)", ex);
                throw new Exception(GetType().FullName + ".AddressLookupViaGoogleResponse(AddressLookupViaGoogleRequest request, AddressLookupMatchEntry[] matches)", ex);
            }
        }

        public AddressLookupViaGoogleResponse()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupViaGoogleResponse()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupViaGoogleResponse()", ex);
            }
        }

        public AddressLookupViaGoogleRequest Request
        {
            get { return _request; }
            set { _request = value; }
        }

        public AddressLookupMatchEntry[] Matches
        {
            get
            {
                try
                {
                    return (AddressLookupMatchEntry[]) _matchEntries.ToArray(typeof (AddressLookupMatchEntry));
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".Matches (Get)", ex);
                    throw new Exception(GetType().FullName + ".Matches (Get)", ex);
                }
            }
            set
            {
                try
                {
                    _matchEntries.Clear();
                    if ((value != null) && (value.Length > 0))
                        _matchEntries.AddRange(value);
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".Matches (Set)", ex);
                    throw new Exception(GetType().FullName + ".Matches (Set)", ex);
                }
            }
        }

        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
    }
}
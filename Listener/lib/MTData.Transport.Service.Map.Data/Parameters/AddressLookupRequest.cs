#region

using System;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    /// <summary>
    ///   This class is used for an address lookup function.
    /// </summary>
    [Serializable]
    public class AddressLookupRequest : IMapDataRequest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupRequest));
        private string _address;
        private string _source;

        public AddressLookupRequest(string address, string source)
        {
            try
            {
                _address = address;
                _source = source;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupRequest(string address = '" + address + "', string source = '" + source + "')", ex);
                throw new Exception(GetType().FullName + ".AddressLookupRequest(string address = '" + address + "', string source = '" + source + "')", ex);
            }
        }

        public AddressLookupRequest()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupRequest()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupRequest()", ex);
            }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
    }
}
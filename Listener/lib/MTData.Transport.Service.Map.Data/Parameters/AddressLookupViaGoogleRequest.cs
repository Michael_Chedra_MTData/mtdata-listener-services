#region

using System;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class AddressLookupViaGoogleRequest : IMapDataRequest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupViaGoogleRequest));
        private string _address;
        private string _source;

        public AddressLookupViaGoogleRequest(string address, string source)
        {
            try
            {
                _address = address;
                _source = source;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupViaGoogleRequest(string address = '" + address + "', string source = '" + source + "')", ex);
                throw new Exception(GetType().FullName + ".AddressLookupViaGoogleRequest(string address = '" + address + "', string source = '" + source + "')", ex);
            }
        }

        public AddressLookupViaGoogleRequest()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupViaGoogleRequest()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupViaGoogleRequest()", ex);
            }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string Source
        {
            get { return _source; }
            set { _source = value; }
        }
    }
}
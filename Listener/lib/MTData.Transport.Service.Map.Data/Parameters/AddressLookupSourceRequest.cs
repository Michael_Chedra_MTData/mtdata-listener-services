#region

using System;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class AddressLookupSourceRequest : IMapDataRequest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupSourceRequest));

        public AddressLookupSourceRequest()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupSourceRequest()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupSourceRequest()", ex);
            }
        }
    }
}
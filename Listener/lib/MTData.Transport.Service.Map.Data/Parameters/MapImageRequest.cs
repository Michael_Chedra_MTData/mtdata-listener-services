#region

using System;
using System.Collections;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class MapImageRequest : IMapDataRequest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapImageRequest));
        private readonly ArrayList _references = new ArrayList();
        private readonly ArrayList _units = new ArrayList();
        private MapImageBounds _bounds = new MapImageBounds();

        public MapImageRequest(MapImageBounds bounds, MapImageReference[] references, MapImageUnit[] units)
        {
            try
            {
                _bounds = bounds;
                References = references;
                Units = units;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageRequest(MapImageBounds bounds, MapImageReference[] references, MapImageUnit[] units)", ex);
                throw new Exception(GetType().FullName + ".MapImageRequest(MapImageBounds bounds, MapImageReference[] references, MapImageUnit[] units)", ex);
            }
        }

        public MapImageRequest()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapImageRequest()", ex);
                throw new Exception(GetType().FullName + ".MapImageRequest()", ex);
            }
        }

        /// <summary>
        ///   Reference points
        /// </summary>
        public MapImageReference[] References
        {
            get
            {
                try
                {
                    return (MapImageReference[]) _references.ToArray(typeof (MapImageReference));
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".References (Get)", ex);
                    throw new Exception(GetType().FullName + ".References (Get)", ex);
                }
            }
            set
            {
                try
                {
                    _references.Clear();
                    if ((value != null) && (value.Length > 0))
                        _references.AddRange(value);
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".References (Set)", ex);
                    throw new Exception(GetType().FullName + ".References (Set)", ex);
                }
            }
        }

        /// <summary>
        ///   Unit Details
        /// </summary>
        public MapImageUnit[] Units
        {
            get
            {
                try
                {
                    return (MapImageUnit[]) _units.ToArray(typeof (MapImageUnit));
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".Units (Get)", ex);
                    throw new Exception(GetType().FullName + ".Units (Get)", ex);
                }
            }
            set
            {
                try
                {
                    _units.Clear();
                    if ((value != null) && (value.Length > 0))
                        _units.AddRange(value);
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".Units (Set)", ex);
                    throw new Exception(GetType().FullName + ".Units (Set)", ex);
                }
            }
        }

        /// <summary>
        ///   This is the bounds of the request, both viewer, and geographic
        /// </summary>
        public MapImageBounds Bounds
        {
            get { return _bounds; }
            set { _bounds = value; }
        }
    }
}
#region

using System;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class LatLongLookupRequest : IMapDataRequest
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (LatLongLookupRequest));
        private int? _heading;
        private float _latitude;
        private float _longitude;
        private int _requestId;
        private bool _resolveCountry;
        private bool _resolveRoadType;
        private bool _resolveSpeedLimit;
        private bool _resolveStreetNumber;
        private DateTime? _utcTime;


        public LatLongLookupRequest()
        {
            try
            {
                _requestId = 0;
                _latitude = 0;
                _longitude = 0;
                _heading = null;
                _utcTime = null;
                _resolveSpeedLimit = false;
                _resolveStreetNumber = false;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".LatLongLookupRequest()", ex);
                throw new Exception(GetType().FullName + ".LatLongLookupRequest()", ex);
            }
        }

        public LatLongLookupRequest(int requestId, float latitude, float longitude)
        {
            try
            {
                _requestId = requestId;
                _latitude = latitude;
                _longitude = longitude;
                _heading = null;
                _utcTime = null;
                _resolveSpeedLimit = false;
                _resolveStreetNumber = false;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".LatLongLookupRequest(int requestId = " + requestId + ", float latitude = " + latitude + ", float longitude = " + longitude + ")", ex);
                throw new Exception(GetType().FullName + ".LatLongLookupRequest(int requestId = " + requestId + ", float latitude = " + latitude + ", float longitude = " + longitude + ")", ex);
            }
        }

        public LatLongLookupRequest(int requestId, float latitude, float longitude, int? heading, DateTime? utcTime, bool resolveSpeedLimit, bool resolveStreetNumber, bool resolveCountry, bool resolveRoadType)
        {
            try
            {
                _requestId = requestId;
                _latitude = latitude;
                _longitude = longitude;
                _heading = heading;
                _utcTime = utcTime;
                _resolveSpeedLimit = resolveSpeedLimit;
                _resolveStreetNumber = resolveStreetNumber;
                _resolveCountry = resolveCountry;
                _resolveRoadType = resolveRoadType;
                if (_resolveSpeedLimit)
                {
                    if (_utcTime == null)
                        _utcTime = DateTime.Now.ToUniversalTime();
                    if (_heading == null)
                        _heading = 0;
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".LatLongLookupRequest(int requestId = " + requestId + ", float latitude = " + latitude + ", float longitude = " + longitude + ", int heading = " + heading + ", DateTime? utcTime = " + (utcTime == null ? "null" : ((DateTime) utcTime).ToString("dd/MM/yyyy HH:mm:ss")) + ", bool resolveSpeedLimit = " + (resolveSpeedLimit ? "True" : "False") + ")", ex);
                throw new Exception(GetType().FullName + ".LatLongLookupRequest(int requestId = " + requestId + ", float latitude = " + latitude + ", float longitude = " + longitude + ", int heading = " + heading + ", DateTime? utcTime = " + (utcTime == null ? "null" : ((DateTime) utcTime).ToString("dd/MM/yyyy HH:mm:ss")) + ", bool resolveSpeedLimit = " + (resolveSpeedLimit ? "True" : "False") + ")", ex);
            }
        }

        public int RequestId
        {
            get { return _requestId; }
            set { _requestId = value; }
        }

        public float Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public float Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public int? Heading
        {
            get
            {
                if (_heading == null)
                    return 0;
                return _heading;
            }
            set { _heading = value; }
        }

        public DateTime? UTCTime
        {
            get
            {
                if (_utcTime == null)
                    return DateTime.Now.ToUniversalTime();
                return _utcTime;
            }
            set { _utcTime = value; }
        }

        public bool ResolveSpeedLimit
        {
            get { return _resolveSpeedLimit; }
            set
            {
                _resolveSpeedLimit = value;
                if (_resolveSpeedLimit)
                {
                    if (_utcTime == null)
                        _utcTime = DateTime.Now.ToUniversalTime();
                    if (_heading == null)
                        _heading = 0;
                }
            }
        }

        public bool ResolveStreetNumber
        {
            get { return _resolveStreetNumber; }
            set { _resolveStreetNumber = value; }
        }

        public bool ResolveCountry
        {
            get { return _resolveCountry; }
            set { _resolveCountry = value; }
        }

        public bool ResolveRoadType
        {
            get { return _resolveRoadType; }
            set { _resolveRoadType = value; }
        }
    }
}
#region

using System;
using System.Collections;
using log4net;
using MTData.Transport.Service.Map.Interface;


#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class AddressLookupSourceResponse : IMapDataResponse
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupSourceResponse));
        private readonly ArrayList _sources = new ArrayList();
        private string _error;
        private AddressLookupSourceRequest _request;

        public AddressLookupSourceResponse(AddressLookupSourceRequest request, AddressLookupSourceEntry[] sources)
        {
            try
            {
                Sources = sources;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupSourceResponse(AddressLookupSourceRequest request, AddressLookupSourceEntry[] sources)", ex);
                throw new Exception(GetType().FullName + ".AddressLookupSourceResponse(AddressLookupSourceRequest request, AddressLookupSourceEntry[] sources)", ex);
            }
        }

        public AddressLookupSourceResponse()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupSourceResponse()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupSourceResponse()", ex);
            }
        }

        public AddressLookupSourceRequest Request
        {
            get { return _request; }
            set { _request = value; }
        }

        public AddressLookupSourceEntry[] Sources
        {
            get
            {
                try
                {
                    return (AddressLookupSourceEntry[]) _sources.ToArray(typeof (AddressLookupSourceEntry));
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".Sources (Get)", ex);
                    throw new Exception(GetType().FullName + ".Sources (Get)", ex);
                }
            }
            set
            {
                try
                {
                    _sources.Clear();
                    if ((value != null) && (value.Length > 0))
                        _sources.AddRange(value);
                }
                catch (Exception ex)
                {
                    _log.Error(GetType().FullName + ".Sources (Set)", ex);
                    throw new Exception(GetType().FullName + ".Sources (Set)", ex);
                }
            }
        }

        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }

        public void AddSource(AddressLookupSourceEntry source)
        {
            try
            {
                _sources.Add(source);
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddSource(AddressLookupSourceEntry source)", ex);
                throw new Exception(GetType().FullName + ".AddSource(AddressLookupSourceEntry source)", ex);
            }
        }
    }
}
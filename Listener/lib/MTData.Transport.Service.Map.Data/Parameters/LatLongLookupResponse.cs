using System;
using log4net;
using MTData.Transport.Service.Map.Interface;


namespace MTData.Transport.Service.Map.Data.Parameters
{
    [Serializable]
    public class LatLongLookupResponse : IMapDataResponse
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (LatLongLookupResponse));
        private string _error;
        private LatLongLookupRequest _request;
        private StreetInfo _result;

        public LatLongLookupResponse(LatLongLookupRequest request, StreetInfo result)
        {
            try
            {
                _request = request;
                _result = result;
                _error = "";
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".LatLongLookupResponse(LatLongLookupRequest request, StreetInfo result)", ex);
                throw new Exception(GetType().FullName + ".LatLongLookupResponse(LatLongLookupRequest request, StreetInfo result)", ex);
            }
        }

        public LatLongLookupResponse()
        {
            try
            {
                _request = null;
                _result = null;
                _error = "";
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".LatLongLookupResponse()", ex);
                throw new Exception(GetType().FullName + ".LatLongLookupResponse()", ex);
            }
        }

        public LatLongLookupRequest Request
        {
            get { return _request; }
            set { _request = value; }
        }

        public StreetInfo Result
        {
            get { return _result; }
            set { _result = value; }
        }

        public string Error
        {
            get { return _error; }
            set { _error = value; }
        }
    }
}
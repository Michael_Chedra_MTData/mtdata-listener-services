#region

using System;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Parameters
{
    /// <summary>
    ///   This is an individual entry.
    /// </summary>
    [Serializable]
    public class AddressLookupSourceEntry
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (AddressLookupSourceEntry));

        private string _description;
        private string _name;

        public AddressLookupSourceEntry(string name, string description)
        {
            try
            {
                _name = name;
                _description = description;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupSourceEntry(string name, string description)", ex);
                throw new Exception(GetType().FullName + ".AddressLookupSourceEntry(string name, string description)", ex);
            }
        }

        public AddressLookupSourceEntry()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddressLookupSourceEntry()", ex);
                throw new Exception(GetType().FullName + ".AddressLookupSourceEntry()", ex);
            }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public override string ToString()
        {
            try
            {
                return _description;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ToString()", ex);
                throw new Exception(GetType().FullName + ".ToString()", ex);
            }
        }
    }
}
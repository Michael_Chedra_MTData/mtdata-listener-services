#region

using System;
using log4net;
using MTData.Transport.Service.Map.Interface;

#endregion

namespace MTData.Transport.Service.Map.Data.Utilities
{
    public class HandlerEntry
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (HandlerEntry));
        private readonly object _context;
        private readonly IRequestHandler _handler;

        public HandlerEntry(IRequestHandler handler, object context)
        {
            try
            {
                _handler = handler;
                _context = context;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".HandlerEntry(IRequestHandler handler, object context)", ex);
                throw new Exception(GetType().FullName + ".HandlerEntry(IRequestHandler handler, object context)", ex);
            }
        }

        public IRequestHandler Handler
        {
            get { return _handler; }
        }

        public object Context
        {
            get { return _context; }
        }
    }
}
#region

using System;
using System.Collections;
using System.Threading;
using log4net;
using MTData.Transport.Service.Map.Interface;

#endregion

namespace MTData.Transport.Service.Map.Data.Utilities
{
    /// <summary>
    ///   This class will hadnle multiple requests and sequence them to a number of registered request handlers.
    /// </summary>
    public class RequestCoordinator : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (RequestCoordinator));

        private readonly ArrayList _active = new ArrayList();
        private readonly ArrayList _available = new ArrayList();
        private readonly object _syncRoot = new object();
        private readonly ManualResetEvent _waitHandle = new ManualResetEvent(true);
        private bool _disposed;

        public RequestCoordinator()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".RequestCoordinator()", ex);
                throw new Exception(GetType().FullName + ".RequestCoordinator()", ex);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                _disposed = true;
                _waitHandle.Set();
                lock (_syncRoot)
                {
                    foreach (object handler in _available)
                    {
                        if (handler is IDisposable)
                            ((IDisposable) handler).Dispose();
                    }
                    foreach (object handler in _active)
                    {
                        if (handler is IDisposable)
                            ((IDisposable) handler).Dispose();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".Dispose()", ex);
                throw new Exception(GetType().FullName + ".Dispose()", ex);
            }
        }

        #endregion

        public void AddHandler(IRequestHandler handler, object context)
        {
            try
            {
                lock (_syncRoot)
                {
                    _available.Add(new HandlerEntry(handler, context));
                    _waitHandle.Set();
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddHandler(IRequestHandler handler, object context)", ex);
                throw new Exception(GetType().FullName + ".AddHandler(IRequestHandler handler, object context)", ex);
            }
        }

        public object ProcessRequest(object request)
        {
            object result = null;
            try
            {
                HandlerEntry entry = null;
                while ((entry == null) && (!_disposed))
                {
                    _waitHandle.WaitOne();
                    lock (_syncRoot)
                    {
                        if (_available.Count > 0)
                        {
                            entry = (HandlerEntry) _available[0];
                            _active.Add(entry);
                            _available.RemoveAt(0);
                        }
                        if (_available.Count == 0)
                            _waitHandle.Reset();
                    }
                }
                if (entry != null)
                {
                    //	Entry will be avalable at this point to this thread only.
                    result = entry.Handler.Execute(request, entry.Context);

                    //	once we are finished with it, add it back into the list.
                    lock (_syncRoot)
                    {
                        _available.Add(entry);
                        _active.Remove(entry);
                        _waitHandle.Set();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(object request)", ex);
                throw new Exception(GetType().FullName + ".ProcessRequest(object request)", ex);
            }
            return result;
        }
    }
}
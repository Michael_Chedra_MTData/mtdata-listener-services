#region

using System;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Utilities
{
    /// <summary>
    ///   This class will read and write packets to the designated stream.
    /// </summary>
    public class PacketFormatter
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (PacketFormatter));

        private byte[] _packet;

        public PacketFormatter()
        {
            try
            {
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".PacketFormatter()", ex);
                throw new Exception(GetType().FullName + ".PacketFormatter()", ex);
            }
        }

        public object Read(byte[] buffer, int length)
        {
            try
            {
                object request = null;

                byte[] packet = new byte[((_packet != null) ? _packet.Length : 0) + length];
                int index = 0;
                if (_packet != null)
                    for (int loop = 0; loop < _packet.Length; loop++)
                        packet[index++] = _packet[loop];

                for (int loop = 0; loop < length; loop++)
                    packet[index++] = buffer[loop];

                if (packet.Length >= 4)
                {
                    int dataLength = BitConverter.ToInt32(packet, 0);
                    if (packet.Length == dataLength + 4)
                    {
                        BinaryFormatter formatter = new BinaryFormatter();
                        MemoryStream stream = new MemoryStream(packet, 4, dataLength);

                        request = formatter.Deserialize(stream);
                        packet = null;
                    }
                }
                _packet = packet;
                return request;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".Read(byte[] buffer, int length)", ex);
                throw new Exception(GetType().FullName + ".Read(byte[] buffer, int length)", ex);
            }
        }

        public void Write(Stream stream, object response)
        {
            try
            {
                MemoryStream mstream = new MemoryStream();
                BinaryFormatter formatter = new BinaryFormatter();
                formatter.Serialize(mstream, response);
                byte[] responseBytes = mstream.ToArray();
                stream.Write(BitConverter.GetBytes(responseBytes.Length), 0, 4);
                stream.Write(responseBytes, 0, responseBytes.Length);
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".Write(Stream stream, object response)", ex);
                throw new Exception(GetType().FullName + ".Write(Stream stream, object response)", ex);
            }
        }
    }
}
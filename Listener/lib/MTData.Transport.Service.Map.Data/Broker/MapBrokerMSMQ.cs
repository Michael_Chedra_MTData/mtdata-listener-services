using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Xml;
using log4net;
using MTData.Transport.Application.Communication;
using MTData.Transport.Service.Map.Data.Parameters;
using MTData.Transport.Service.Map.Interface;


namespace MTData.Transport.Service.Map.Data.Broker
{
    public class MapBrokerMSMQ : IMapBroker
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MapBrokerMSMQ));

        private int _processingTimeoutMS;
        bool _threadRunning = false;
        private Thread _queueProcessingThread;
        private Queue<MapServerQueueItem> _requestQueue;
        private Dictionary<ulong, IMapServer> _serverList;
        
        public MapBrokerMSMQ()
        {
            _requestQueue = new Queue<MapServerQueueItem>();
            _serverList = new Dictionary<ulong, IMapServer>();
        }

        public MapBrokerMSMQ(string hostName, string requestQueue, string responseQueue, int requestTimeoutMS, Dictionary<ulong, IMapServer> serverList)
        {
            CreateObject(hostName, requestQueue, responseQueue, requestTimeoutMS, serverList);
        }

        public int ProcessingTimeoutMs
        {
            get { return _processingTimeoutMS; }
            set { _processingTimeoutMS = value; }
        }

        public Dictionary<ulong, IMapServer> ServiceList
        {
            set
            {
                if(value != null)
                    _serverList = value;
                else
                    _serverList = new Dictionary<ulong, IMapServer>();
            }
        }

        public void ProcessRequest(IMapDataRequest request, int threadId)
        {
            try
            {
                // You need to queue your requests on the MSMQ to process data through this object.
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(IMapDataRequest request, int threadId)", ex);
                throw new System.Exception(GetType().FullName + ".ProcessRequest(IMapDataRequest request, int threadId)", ex);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            _threadRunning = false;
            _requestQueue = null;

        }

        #endregion

        /// <summary>
        ///   The format of the XML string is
        ///   <code>
        ///     <MapBroker type ="MSMQ">
        ///       <ServerName>MTData-AD</ServerName>
        ///       <ReadQueueName>mtdata.mapdatarequest.queue</ReadQueueName>
        ///       <WriteQueueName>mtdata.mapdataresponse.queue</WriteQueueName>
        ///       <RequestTimeoutMS>20000</RequestTimeoutMS>
        ///     </MapBroker>
        ///   </code>
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "configContext"></param>
        /// <param name = "section"></param>
        /// <returns></returns>
        public static MapBrokerMSMQ CreateFromXML(XmlNode node, Dictionary<ulong, IMapServer> serverList)
        {
            MapBrokerMSMQ ms = null;
            try
            {
                string hostname = node.SelectSingleNode("ServerName").InnerText.Trim();
                string readQueueName = node.SelectSingleNode("ReadQueueName").InnerText.Trim();
                string writeQueueName = node.SelectSingleNode("WriteQueueName").InnerText.Trim();
                int requestTimeout = Convert.ToInt32(node.SelectSingleNode("RequestTimeoutMS").InnerText.Trim());
                ms = new MapBrokerMSMQ(hostname, readQueueName, writeQueueName, requestTimeout, serverList);
            }
            catch (Exception ex)
            {
                throw new Exception("MTData.Transport.Service.Map.Data.Broker.MapBrokerTCP.CreateFromXML(XmlNode node)", ex);
            }
            return ms;
        }

        private void CreateObject(string hostName, string requestQueue, string responseQueue, int requestTimeoutMS, Dictionary<ulong, IMapServer> serverList)
        {
            try
            {
                
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".CreateObject(string hostName, string requestQueue, string responseQueue, Dictionary<ulong, IMapServer> serverList)", ex);
                throw new Exception(GetType().FullName + ".CreateObject(string hostName, string requestQueue, string responseQueue, Dictionary<ulong, IMapServer> serverList)", ex);
            }
        }

    }
}

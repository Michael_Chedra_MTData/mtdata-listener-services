#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using log4net;
using MTData.Common.Network;
using MTData.Transport.Service.Map.Data.Parameters;
using MTData.Transport.Service.Map.Data.Utilities;
using MTData.Transport.Service.Map.Interface;

#endregion

namespace MTData.Transport.Service.Map.Data.Broker
{
    public class MapServers : IDisposable, IConfigurationSectionHandler
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MapServers));

        #region Events
        public delegate void ResultsTimeoutEvent(int requestIndex);
        public event ResultsTimeoutEvent eResultsTimeoutEvent;
        public delegate void ResultsAvailableEvent(int requestIndex, object oData);
        public event ResultsAvailableEvent eResultsAvailable;
        #endregion

        private List<IMapBroker> _mapBrokers;
        private Dictionary<ulong, IMapServer> _serverList;
        private Thread _timeoutProcessing;
        private long _timeoutOffset;
        private bool _timeoutProcessingRunning;
        private long _requestTimeoutInTicks;

        public MapServers()
        {
            try
            {
                _mapBrokers = new List<IMapBroker>();
                _serverList = new Dictionary<ulong, IMapServer>();
                _processingTimeouts = new Dictionary<int, long>();
                _timeoutProcessing = new Thread(new ThreadStart(TimeoutProcessingThread));
                _requestTimeoutInTicks = 200000;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapServers()", ex);
                throw new System.Exception(GetType().FullName + ".MapServers()", ex);
            }
        }

        public List<IMapBroker> MapBrokers
        {
            get
            {
                return _mapBrokers;
            }
        }

        public void AddMapBroker(IMapBroker broker)
        {
            _mapBrokers.Add(broker);
            if (broker is MapBroker)
            {
                ((MapBroker)broker).eResultsAvailable += new MapBroker.ResultsAvailableEvent(BrokerResultsAvailable);
            }
        }

        public Dictionary<ulong, IMapServer> ServiceList
        {
            get
            {
                return _serverList;
            }
        }

        public void ProcessRequest(IMapDataRequest request, int threadId)
        {
            try
            {
                if (_mapBrokers != null)
                {
                    foreach (IMapBroker broker in _mapBrokers)
                    {
                        if (broker is MapBroker)
                        {
                            AddRequestTimeout(threadId, DateTime.Now.Ticks);
                            broker.ProcessRequest(request, threadId);
                            break;
                        }
                    }
                }
                else
                {
                    throw new System.Exception("No 'Object' map brokers are available.");
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(MapServerQueueItem qItem)", ex);
                throw new System.Exception(GetType().FullName + ".ProcessRequest(MapServerQueueItem qItem)", ex);
            }
        }

        private Dictionary<int, long> _processingTimeouts;

        private object _processingTimeoutSync = new object();

        private void AddRequestTimeout(int threadId, long startTicks)
        {
            try
            {
                lock (_processingTimeoutSync)
                {
                    if (_processingTimeouts.ContainsKey(threadId))
                        _processingTimeouts[threadId] = startTicks;
                    else
                        _processingTimeouts.Add(threadId, startTicks);
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".AddRequestTimeout(int threadId, long startTicks)", ex);
                throw new System.Exception(GetType().FullName + ".AddRequestTimeout(int threadId, long startTicks)", ex);
            }
        }

        private void TimeoutProcessingThread()
        {
            try
            {
                while (_timeoutProcessingRunning)
                {
                    try
                    {
                        lock (_processingTimeoutSync)
                        {
                            foreach (int key in _processingTimeouts.Keys)
                            {
                                long currentTicks = DateTime.Now.Ticks;
                                if (currentTicks - _processingTimeouts[key] > _requestTimeoutInTicks)
                                {
                                    _processingTimeouts.Remove(key);
                                    if (eResultsTimeoutEvent != null)
                                        eResultsTimeoutEvent.BeginInvoke(key, null, null);
                                }
                            }

                        }

                    }
                    catch (Exception exMain)
                    {
                        _log.Error(GetType().FullName + ".TimeoutProcessingThread() - Main Loop", exMain);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".TimeoutProcessingThread()", ex);
            }
        }

        private void BrokerResultsAvailable(int requestIndex, object oData)
        {
            try
            {

                lock (_processingTimeoutSync)
                {
                    if (_processingTimeouts.ContainsKey(requestIndex))
                    {
                        if (eResultsAvailable != null)
                            eResultsAvailable.BeginInvoke(requestIndex, oData, null, null);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".BrokerResultsAvailable(int requestIndex, object oData)", ex);
            }
        }

        #region IConfigurationSectionHandler Members

        /// <summary>
        ///   The format of the descriptors in the app config will be the following..
        ///   <code>
        ///   <MapServers>
        ///     <MapBroker type ="TCP">
        ///       <ServerName>MTData-AD</ServerName>
        ///       <Port>9901</Port>
        ///       <RequestTimeoutMS>20000</RequestTimeoutMS>
        ///     </MapBroker>
        ///     <MapBroker type ="MSMQ">
        ///       <ServerName>MTData-AD</ServerName>
        ///       <ReadQueueName>mtdata.mapdatarequest.queue</ReadQueueName>
        ///       <WriteQueueName>mtdata.mapdataresponse.queue</WriteQueueName>
        ///       <RequestTimeoutMS>20000</RequestTimeoutMS>
        ///     </MapBroker>
        ///     <MappingServices>
        ///       <MapServer servername="MTData-AD" port="36401" connectionTimeoutMS="20000" />
        ///       <MapServer servername="MTData-AD" port="36402" connectionTimeoutMS="20000" />
        ///       <MapServer servername="MTData-AD" port="36403" connectionTimeoutMS="20000" />
        ///       <MapServer servername="MTData-AD" port="36404" connectionTimeoutMS="20000" />
        ///     </MappingServices>
        ///   </MapServers>
        ///   </code>
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "configContext"></param>
        /// <param name = "section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, XmlNode section)
        {
            //MapServers ms = null;
            try
            {

               // ms = new MapServers();
                XmlNode mappingServices = section.SelectSingleNode("MappingServices");
                XmlNodeList mapServers = mappingServices.SelectNodes("MapServer");
                if (mapServers != null)
                {
                    foreach (XmlNode mapServerNode in mapServers)
                    {
                        string hostname = mapServerNode.Attributes.GetNamedItem("servername").Value.Trim();
                        int port = Convert.ToInt32(mapServerNode.Attributes.GetNamedItem("port").Value.Trim());
                        int connectionTimeoutMs = Convert.ToInt32(mapServerNode.Attributes.GetNamedItem("connectionTimeoutMS").Value.Trim());
                        MapServer mapServer = new MapServer(hostname, port, connectionTimeoutMs);
                        ulong mapKey = mapServer.Key;
                        if (!_serverList.ContainsKey(mapKey))
                            _serverList.Add(mapKey, mapServer);
                        else
                            mapServer.Dispose();
                    }
                }



                XmlNodeList brokerSettings = section.SelectNodes("MapBroker");

                foreach (XmlNode node in brokerSettings)
                {
                    string brokerType = node.Attributes["type"].Value.Trim();
                    XmlNode RequestTimeoutMS = node.SelectSingleNode("RequestTimeoutMS");
                    int requestTimeoutMS = Convert.ToInt32(RequestTimeoutMS.InnerText);

                    _requestTimeoutInTicks = ((long)requestTimeoutMS) * TimeSpan.TicksPerMillisecond;
                    switch (brokerType.ToUpper())
                    {
                        case "TCP":
                            MapBrokerTCP tcpBroker = MapBrokerTCP.CreateFromXML(node, _serverList);
                            AddMapBroker(tcpBroker);
                            break;
                        case "OBJECT":
                            MapBroker broker = MapBroker.CreateFromXML(node, _serverList);
                            AddMapBroker(broker);
                            break;
                        default:
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }
        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            if (_mapBrokers != null)
            {
                foreach (IMapBroker broker in _mapBrokers)
                    broker.Dispose();
                _mapBrokers.Clear();
            }
            _mapBrokers = null;

            if (_serverList != null)
            {
                foreach (ulong key in _serverList.Keys)
                    _serverList[key].Dispose();
                _serverList.Clear();
            }
            _serverList = null;
            if (_processingTimeouts != null)
                _processingTimeouts.Clear();
            _processingTimeouts = null;
        }

        #endregion
    }
}
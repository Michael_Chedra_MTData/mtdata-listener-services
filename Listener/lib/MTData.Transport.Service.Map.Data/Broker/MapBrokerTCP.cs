#region

using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Xml;
using log4net;
using MTData.Common.Network;
using MTData.Common.Threading;
using MTData.Transport.Service.Map.Data.Parameters;
using MTData.Transport.Service.Map.Data.Utilities;
using MTData.Transport.Service.Map.Interface;

#endregion

namespace MTData.Transport.Service.Map.Data.Broker
{
    public class MapBrokerTCP : IMapBroker
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MapBrokerTCP));
        private readonly object _connectionsSync = new object();
        private readonly object _requestQueueSync = new object();

        private readonly object _tcpSync = new object();
        private readonly object _threadIdSync = new object();
        private Dictionary<int, NetworkStream> _connections;
        private string _hostName;
        private int _port;
        private int _processingTimeoutMS;
        private Thread _queueProcessingThread;
        private Queue<MapServerQueueItem> _requestQueue;
        private IPAddress[] _serverAddress;
        private IAsyncResult _tcpConn;
        private TCPServer _tcpServer;
        private bool _threadRunning;
        private int _uniqueID;
        private Dictionary<ulong, IMapServer> _serverList;

        public MapBrokerTCP()
        {
            CreateObject("localhost", 9901, 20000, null);
        }

        public MapBrokerTCP(string hostName, int port)
        {
            CreateObject(hostName, port, 20000, null);
        }

        public MapBrokerTCP(string hostName, int port, int processingTimeoutMS)
        {
            CreateObject(hostName, port, processingTimeoutMS, null);
        }

        public MapBrokerTCP(string hostName, int port, int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)
        {
            CreateObject(hostName, port, processingTimeoutMS, serverList);
        }

        public int ProcessingTimeoutMs
        {
            get { return _processingTimeoutMS; }
            set { _processingTimeoutMS = value; }
        }

        public Dictionary<ulong, IMapServer> ServiceList
        {
            set
            {
                _serverList = value;
            }
        }

        public void ProcessRequest(IMapDataRequest request, int threadId)
        {
            try
            {
                // You need to establish a TCP connection to process request through this object.
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(IMapDataRequest request, int threadId)", ex);
                throw new System.Exception(GetType().FullName + ".ProcessRequest(IMapDataRequest request, int threadId)", ex);
            }
        }

        public string HostName
        {
            get { return _hostName; }
            set
            {
                if (_hostName != value)
                {
                    _hostName = value;
                    _serverAddress = Dns.GetHostAddresses(_hostName);
                    foreach (IPAddress address in _serverAddress)
                    {
                        if (address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            _serverAddress = new IPAddress[1];
                            _serverAddress[0] = address;
                            break;
                        }
                    }
                    if (_tcpServer != null)
                    {
                        _tcpServer.Stop();
                        _tcpServer = null;
                    }
                    _tcpServer = new TCPServer(_serverAddress[0].ToString(), _port, "MapDataBrokerService", IncommingConnection);
                    _tcpServer.Start();
                }
            }
        }

        public int Port
        {
            get { return _port; }
            set
            {
                if (_port != value)
                {
                    _port = value;
                    _serverAddress = Dns.GetHostAddresses(_hostName);
                    foreach (IPAddress address in _serverAddress)
                    {
                        if (address.AddressFamily == AddressFamily.InterNetwork)
                        {
                            _serverAddress = new IPAddress[1];
                            _serverAddress[0] = address;
                            break;
                        }
                    }
                    if (_tcpServer != null)
                    {
                        _tcpServer.Stop();
                        _tcpServer = null;
                    }
                    _tcpServer = new TCPServer(_serverAddress[0].ToString(), _port, "MapDataBrokerService", IncommingConnection);
                    _tcpServer.Start();
                }
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            _threadRunning = false;
            if (_tcpServer != null)
            {
                lock (_tcpSync)
                {
                    _tcpServer.Stop();
                    _tcpServer = null;
                }
            }
        }

        #endregion

        /// <summary>
        ///   The format of the XML string is
        ///   <code>
        ///     <MapBroker type ="TCP">
        ///       <ServerName>MTData-AD</ServerName>
        ///       <Port>9901</Port>
        ///       <RequestTimeoutMS>20000</RequestTimeoutMS>
        ///     </MapBroker>
        ///   </code>
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "configContext"></param>
        /// <param name = "section"></param>
        /// <returns></returns>
        public static MapBrokerTCP CreateFromXML(XmlNode node, Dictionary<ulong, IMapServer> serverList)
        {
            MapBrokerTCP ms = null;
            try
            {
                string hostname = node.SelectSingleNode("ServerName").InnerText.Trim();
                int port = Convert.ToInt32(node.SelectSingleNode("Port").InnerText.Trim());
                int requestTimeout = Convert.ToInt32(node.SelectSingleNode("RequestTimeoutMS").InnerText.Trim());
                ms = new MapBrokerTCP(hostname, port, requestTimeout, serverList);
            }
            catch (Exception ex)
            {
                throw new Exception("MTData.Transport.Service.Map.Data.Broker.MapBrokerTCP.CreateFromXML(XmlNode node)", ex);
            }
            return ms;
        }
        private void CreateObject(string hostName, int port, int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)
        {
            try
            {
                _connections = new Dictionary<int, NetworkStream>();
                _requestQueue = new Queue<MapServerQueueItem>();
                _queueProcessingThread = new Thread(QueueProcessingThread);
                _queueProcessingThread.Name = "Map Server Queue Processing";
                _threadRunning = true;
                _queueProcessingThread.Start();
                _hostName = hostName;
                _port = port;
                _processingTimeoutMS = processingTimeoutMS;
                _serverAddress = Dns.GetHostAddresses(hostName);
                foreach (IPAddress address in _serverAddress)
                {
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        _serverAddress = new IPAddress[1];
                        _serverAddress[0] = address;
                        break;
                    }
                }

                _serverList = serverList;

                _tcpServer = new TCPServer(_serverAddress[0].ToString(), port, "MapDataBrokerService",
                                           IncommingConnection);
                _tcpServer.Start();
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".CreateObject(string hostName, int port, int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)", ex);
                throw new Exception(GetType().FullName + ".CreateObject(string hostName, int port, int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)", ex);
            }
        }

        private void IncommingConnection(TcpListener listener)
        {
            TcpClient tcpRequest = null;
            try
            {
                lock (_tcpSync)
                    tcpRequest = listener.AcceptTcpClient();

                if (tcpRequest != null)
                {
                    int threadId = 0;
                    lock (_threadIdSync)
                    {
                        _uniqueID++;
                        threadId = _uniqueID;
                        if (_uniqueID == int.MaxValue)
                            _uniqueID = 0;
                    }
                    lock (_connectionsSync)
                    {
                        if (!_connections.ContainsKey(threadId))
                            _connections.Add(threadId, tcpRequest.GetStream());
                    }
                    EnhancedThread handleThread = new EnhancedThread(RecieveDataAndQueueRequestForProcessing, threadId);
                    handleThread.Name = "Client TCP Connection Handler";
                    handleThread.Start();
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".IncommingConnection(IAsyncResult ar)", ex);
            }
        }

        private object RecieveDataAndQueueRequestForProcessing(EnhancedThread sender, object data)
        {
            int threadId = 0;
            //NetworkStream nsRequest = null;
            PacketFormatter formatter = null;
            byte[] buffer = null;
            object request = null;

            try
            {
                threadId = (int)data;
                formatter = new PacketFormatter();
                buffer = new byte[1024];
                while (request == null)
                {
                    try
                    {
                        while (_connections[threadId].DataAvailable)
                        {
                            try
                            {
                                int bytes = _connections[threadId].Read(buffer, 0, 1024);
                                request = formatter.Read(buffer, bytes);
                                if (request != null)
                                {
                                    MapServerQueueItem qItem = new MapServerQueueItem(request, threadId, DateTime.Now.Ticks);
                                    lock (_requestQueueSync)
                                        _requestQueue.Enqueue(qItem);
                                    break;
                                }
                            }
                            catch (Exception exDataThread)
                            {
                                _log.Error(GetType().FullName + "._threadHandler() - DataAvailable Error", exDataThread);
                                Thread.Sleep(1000);
                            }
                        }
                    }
                    catch (Exception exThread)
                    {
                        _log.Error(GetType().FullName + "._threadHandler() - Main Loop", exThread);
                        Thread.Sleep(1000);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".RecieveDataAndQueueRequestForProcessing(EnhancedThread sender, object data)", ex);
                throw new Exception(GetType().FullName + ".RecieveDataAndQueueRequestForProcessing(EnhancedThread sender, object data)", ex);
            }
            return null;
        }

        private void QueueProcessingThread()
        {
            MapServerQueueItem item = null;
            bool assignedServer = true;
            try
            {
                while (_threadRunning)
                {
                    try
                    {
                        if (assignedServer)
                        {
                            item = null;
                            lock (_requestQueueSync)
                            {
                                if (_requestQueue.Count > 0)
                                    item = _requestQueue.Dequeue();
                            }
                        }
                        if (_threadRunning && item != null)
                        {
                            assignedServer = false;
                            foreach (ulong key in _serverList.Keys)
                            {
                                if (_serverList[key].Available)
                                {
                                    _serverList[key].ProcessingRequest = true;
                                    item.AssignedServer = key;
                                    EnhancedThread handleThread = new EnhancedThread(ProcessRequest, item);
                                    handleThread.Name = "Process request " + item.ThreadId;
                                    handleThread.Start();
                                    assignedServer = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Thread.Sleep(50);
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error(GetType().FullName + ".QueueProcessingThread() - Main Loop", ex);
                    }
                }
            }
            catch (Exception exThread)
            {
                _log.Error(GetType().FullName + ".QueueProcessingThread()", exThread);
            }
        }

        private object ProcessRequest(EnhancedThread sender, object data)
        {
            //NetworkStream nsRequest = null;
            object response = null;
            TcpClient mapServer = null;
            NetworkStream stream = null;
            PacketFormatter formatter = null;
            byte[] buffer = null;
            int count = 0;
            ulong assignedServer = 0;
            try
            {
                bool processed = false;
                MapServerQueueItem item = data as MapServerQueueItem;
                assignedServer = item.AssignedServer;
                if (_serverList.ContainsKey(item.AssignedServer) && _connections.ContainsKey(item.ThreadId))
                {
                    try
                    {
                        response = null;
                        mapServer = _serverList[item.AssignedServer].OpenConnection();
                        stream = mapServer.GetStream();
                        formatter = new PacketFormatter();
                        formatter.Write(stream, item.Request);
                    }
                    catch (Exception exProcess)
                    {
                        _log.Error(GetType().FullName + ".ProcessRequest(object request, NetworkStream nsClient) - Processing Request.", exProcess);
                    }
                    try
                    {
                        count = 0;
                        buffer = new byte[1024];
                        while (response == null)
                        {
                            int bytes = stream.Read(buffer, 0, 1024);
                            response = formatter.Read(buffer, bytes);
                            if (count++ == 2000)
                                break;
                        }
                        formatter.Write(_connections[item.ThreadId], response);
                        processed = true;
                    }
                    catch (Exception exProcess)
                    {
                        _log.Error(GetType().FullName + ".ProcessRequest(object request, NetworkStream nsClient) - Processing Response.", exProcess);
                    }
                    if (!processed)
                    {
                        item.AssignedServer = 0;
                        item.ErrorCount++;
                        lock (_requestQueueSync)
                            _requestQueue.Enqueue(item);
                    }
                    else
                    {
                        lock (_connectionsSync)
                        {
                            _connections[item.ThreadId].Close();
                            _connections[item.ThreadId] = null;
                            _connections.Remove(item.ThreadId);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(EnhancedThread sender, object data) - Connection To Map Server Failed.", ex);
                throw new Exception(GetType().FullName + ".ProcessRequest(EnhancedThread sender, object data) - Connection To Map Server Failed.", ex);
            }
            finally
            {
                if (assignedServer != 0)
                {
                    if (_serverList.ContainsKey(assignedServer))
                    {
                        _serverList[assignedServer].CloseConnection(ref mapServer);
                        _serverList[assignedServer].ProcessingRequest = false;
                    }
                }
            }
            return null;
        }
    }
}

#region

using System;
using System.Net;
using System.Net.Sockets;
using log4net;
using MTData.Transport.Service.Map.Interface;

#endregion

namespace MTData.Transport.Service.Map.Data.Broker
{
    public class MapServer : IDisposable, IMapServer
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapServer));
        private readonly int _port;

        private readonly IPAddress _serverAddress;
        private readonly IPAddress[] _serverAddresses;
        private readonly int _timeoutMSec;
        private bool _available;
        private bool _disposing;
        private bool _processingRequest;

        public MapServer(string hostName, int port, int timeoutMSec)
        {
            TcpClient client = null;
            try
            {
                _serverAddresses = Dns.GetHostAddresses(hostName);
                foreach (IPAddress address in _serverAddresses)
                {
                    if (address.AddressFamily == AddressFamily.InterNetwork)
                    {
                        _serverAddress = address;
                        break;
                    }
                }
                _port = port;
                _timeoutMSec = timeoutMSec;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapServer(string hostName = '" + hostName + "', int port = " + port + ", int timeoutMSec = " + timeoutMSec + ")", ex);
            }
            try
            {
                client = OpenConnection();
                _available = true;
            }
            catch (Exception ex)
            {
                _available = false;
                _log.Error(GetType().FullName + ".MapServer(string hostName = '" + hostName + "', int port = " + port + ", int timeoutMSec = " + timeoutMSec + ")", ex);
            }
            finally
            {
                // Close the current connection, we have verified the server and the object is now ready for use.
                CloseConnection(ref client);
            }
        }

        public bool Available
        {
            get { return _available && !_processingRequest; }
        }

        public bool ProcessingRequest
        {
            get { return _processingRequest; }
            set { _processingRequest = value; }
        }

        public ulong Key
        {
            get
            {
                // The key is an 8 byte unlong (4 bytes IPv4 address and 4 bytes port number)
                byte[] data = new byte[8];
                byte[] port = BitConverter.GetBytes(_port);
                byte[] address = _serverAddress.GetAddressBytes();
                data[0] = address[0];
                data[1] = address[1];
                data[2] = address[2];
                data[3] = address[3];
                data[4] = port[0];
                data[5] = port[1];
                data[6] = port[2];
                data[7] = port[3];
                return BitConverter.ToUInt64(data, 0);
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            _disposing = true;
        }

        #endregion

        public bool TryReconnect()
        {
            bool ret = false;
            try
            {
                TcpClient client = null;
                // Try and open the connection again
                client = OpenConnection();
                // Close the current connection, we have verified the server and the object is now ready for use.
                CloseConnection(ref client);
                _available = true;
            }
            catch (Exception)
            {
                _available = false;
            }
            return ret;
        }

        public TcpClient OpenConnection()
        {
            TcpClient client = null;
            try
            {
                // Try to connect to the server before the time out period expires
                if (_serverAddress != null)
                {
                    // Create a new connection
                    client = new TcpClient(AddressFamily.InterNetwork);
                    client.Connect(_serverAddress, _port);
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".OpenConnection(ref TcpClient client)", ex);
                CloseConnection(ref client);
                throw new Exception(GetType().FullName + ".OpenConnection(ref TcpClient client)", ex);
            }
            return client;
        }

        public void CloseConnection(ref TcpClient client)
        {
            try
            {
                // If we have a connection
                if (client != null)
                {
                    // If the connection has an active client that is open
                    if (client.Client != null && client.Client.Connected)
                        // Close the client socket
                        client.Client.Close();
                    // Close the TCP client object
                    client.Close();
                    // Dispose the TCP client object
                    client = null;
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".CloseConnection(ref TcpClient client)", ex);
                throw new Exception(GetType().FullName + ".CloseConnection(ref TcpClient client)", ex);
            }
        }
    }
}
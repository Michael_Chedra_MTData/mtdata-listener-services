#region

using System;
using log4net;

#endregion

namespace MTData.Transport.Service.Map.Data.Broker
{
    public class MapServerQueueItem : IDisposable
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof (MapServers));
        private readonly long _startTicks;
        private readonly int _threadId;
        private ulong _assignedServer;
        private int _errorCount;
        private object _request;

        public MapServerQueueItem(object request, int threadId, long startTicks)
        {
            try
            {
                _threadId = threadId;
                _request = request;
                _startTicks = startTicks;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".MapServerQueueItem(ref object request, int threadId, long startTicks)", ex);
            }
        }

        public int ThreadId
        {
            get { return _threadId; }
        }

        public object Request
        {
            get { return _request; }
            set { _request = value; }
        }

        public long CurrentWaitTimeMs
        {
            get { return Convert.ToInt64((DateTime.Now.Ticks - _startTicks)/TimeSpan.TicksPerMillisecond); }
        }

        public int ErrorCount
        {
            get { return _errorCount; }
            set { _errorCount = value; }
        }

        public ulong AssignedServer
        {
            get { return _assignedServer; }
            set { _assignedServer = value; }
        }

        #region IDisposable Members

        public void Dispose()
        {
            try
            {
                _request = null;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".Dispose()", ex);
            }
        }

        #endregion
    }
}
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using log4net;
using MTData.Common.Threading;
using MTData.Transport.Service.Map.Data.Parameters;
using MTData.Transport.Service.Map.Data.Utilities;
using MTData.Transport.Service.Map.Interface;

namespace MTData.Transport.Service.Map.Data.Broker
{
    public class MapBroker : IMapBroker
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(MapBroker));
        private readonly object _connectionsSync = new object();
        private readonly object _requestQueueSync = new object();

        #region Events
        public delegate void ResultsAvailableEvent(int requestIndex, object oData);
        public event ResultsAvailableEvent eResultsAvailable;
        #endregion

        private int _processingTimeoutMS;
        private Thread _queueProcessingThread;
        private Queue<MapServerQueueItem> _requestQueue;
        private bool _threadRunning;
        private int _uniqueID;
        private Dictionary<ulong, IMapServer> _serverList;
        private IPAddress[] _serverAddress;
        
        public MapBroker()
        {
            CreateObject(20000, null);
        }

        public MapBroker(int processingTimeoutMS)
        {
            CreateObject(processingTimeoutMS, null);
        }

        public MapBroker(int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)
        {
            CreateObject(processingTimeoutMS, serverList);
        }

        public int ProcessingTimeoutMs
        {
            get { return _processingTimeoutMS; }
            set { _processingTimeoutMS = value; }
        }

        public Dictionary<ulong, IMapServer> ServiceList
        {
            set
            {
                _serverList = value;
            }
        }

        #region IDisposable Members

        public void Dispose()
        {
            _threadRunning = false;
        }

        #endregion

        /// <summary>
        ///   The format of the XML string is
        ///   <code>
        ///     <MapBroker type ="TCP">
        ///       <ServerName>MTData-AD</ServerName>
        ///       <Port>9901</Port>
        ///       <RequestTimeoutMS>20000</RequestTimeoutMS>
        ///     </MapBroker>
        ///   </code>
        /// </summary>
        /// <param name = "parent"></param>
        /// <param name = "configContext"></param>
        /// <param name = "section"></param>
        /// <returns></returns>
        public static MapBroker CreateFromXML(XmlNode node, Dictionary<ulong, IMapServer> serverList)
        {
            MapBroker ms = null;
            try
            {
                int requestTimeout = Convert.ToInt32(node.SelectSingleNode("RequestTimeoutMS").InnerText.Trim());
                ms = new MapBroker(requestTimeout, serverList);
            }
            catch (Exception ex)
            {
                throw new Exception("MTData.Transport.Service.Map.Data.Broker.MapBrokerTCP.CreateFromXML(XmlNode node)", ex);
            }
            return ms;
        }
        private void CreateObject(int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)
        {
            try
            {
                _requestQueue = new Queue<MapServerQueueItem>();
                _queueProcessingThread = new Thread(QueueProcessingThread);
                _queueProcessingThread.Name = "Map Server Queue Processing";
                _threadRunning = true;
                _queueProcessingThread.Start();
                _processingTimeoutMS = processingTimeoutMS;
                _serverList = serverList;
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".CreateObject(string hostName, int port, int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)", ex);
                throw new Exception(GetType().FullName + ".CreateObject(string hostName, int port, int processingTimeoutMS, Dictionary<ulong, IMapServer> serverList)", ex);
            }
        }

        public void ProcessRequest(IMapDataRequest request, int threadId)
        {
            try
            {
                MapServerQueueItem qItem = new MapServerQueueItem(request, threadId, DateTime.Now.Ticks);
                lock (_requestQueueSync)
                    _requestQueue.Enqueue(qItem);
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(IMapDataRequest qItem)", ex);
            }
        }

        private void QueueProcessingThread()
        {
            MapServerQueueItem item = null;
            bool assignedServer = true;
            try
            {
                while (_threadRunning)
                {
                    try
                    {
                        if (assignedServer)
                        {
                            item = null;
                            lock (_requestQueueSync)
                            {
                                if (_requestQueue.Count > 0)
                                    item = _requestQueue.Dequeue();
                            }
                        }
                        if (_threadRunning && item != null)
                        {
                            assignedServer = false;
                            foreach (ulong key in _serverList.Keys)
                            {
                                if (_serverList[key].Available)
                                {
                                    _serverList[key].ProcessingRequest = true;
                                    item.AssignedServer = key;
                                    EnhancedThread handleThread = new EnhancedThread(ProcessRequest, item);
                                    handleThread.Name = "Process request " + item.ThreadId;
                                    handleThread.Start();
                                    assignedServer = true;
                                    break;
                                }
                            }
                        }
                        else
                        {
                            Thread.Sleep(50);
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error(GetType().FullName + ".QueueProcessingThread() - Main Loop", ex);
                    }
                }
            }
            catch (Exception exThread)
            {
                _log.Error(GetType().FullName + ".QueueProcessingThread()", exThread);
            }
        }

        private object ProcessRequest(EnhancedThread sender, object data)
        {
            //NetworkStream nsRequest = null;
            object response = null;
            TcpClient mapServer = null;
            NetworkStream stream = null;
            PacketFormatter formatter = null;
            byte[] buffer = null;
            int count = 0;
            ulong assignedServer = 0;
            try
            {
                bool processed = false;
                MapServerQueueItem item = data as MapServerQueueItem;
                assignedServer = item.AssignedServer;
                if (_serverList.ContainsKey(item.AssignedServer))
                {
                    try
                    {
                        response = null;
                        mapServer = _serverList[item.AssignedServer].OpenConnection();
                        stream = mapServer.GetStream();
                        formatter = new PacketFormatter();
                        formatter.Write(stream, item.Request);
                    }
                    catch (Exception exProcess)
                    {
                        _log.Error(GetType().FullName + ".ProcessRequest(object request, NetworkStream nsClient) - Processing Request.", exProcess);
                    }
                    try
                    {
                        count = 0;
                        buffer = new byte[1024];
                        while (response == null)
                        {
                            int bytes = stream.Read(buffer, 0, 1024);
                            response = formatter.Read(buffer, bytes);
                            if (count++ == 2000)
                                break;
                        }
                        if (eResultsAvailable != null)
                            eResultsAvailable(item.ThreadId, response);
                        processed = true;
                    }
                    catch (Exception exProcess)
                    {
                        _log.Error(GetType().FullName + ".ProcessRequest(object request, NetworkStream nsClient) - Processing Response.", exProcess);
                    }
                    if (!processed)
                    {
                        item.AssignedServer = 0;
                        item.ErrorCount++;
                        lock (_requestQueueSync)
                            _requestQueue.Enqueue(item);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(GetType().FullName + ".ProcessRequest(EnhancedThread sender, object data) - Connection To Map Server Failed.", ex);
                throw new Exception(GetType().FullName + ".ProcessRequest(EnhancedThread sender, object data) - Connection To Map Server Failed.", ex);
            }
            finally
            {
                if (assignedServer != 0)
                {
                    if (_serverList.ContainsKey(assignedServer))
                    {
                        _serverList[assignedServer].CloseConnection(ref mapServer);
                        _serverList[assignedServer].ProcessingRequest = false;
                    }
                }
            }
            return null;
        }
    }
}

using System;
using System.Collections;
using System.Data;
using MTData.Common.Mathematics;
using MTData.Common.Utilities;
using MTData.Common.Plugin;
using MTData.Common.Plugin.Configuration;
using MTData.Transport.Service.Route.Data;

namespace MTData.Transport.Plugins.WebService.Route
{
    public class RouteWebServicePluginFactory : IPluginFactory
    {
        public IPlugin Create(PluginDefinition definition)
        {
            return new RouteWebServicePlugin(definition);
        }
    }

    public class RouteWebServicePlugin : IPlugin, IPluginDataProvider
    {
        private IPluginHost _pluginHost;
        private PluginDefinition _pluginDefinition;
        private double _redundantPointDistance = 4.0 / 100000.0;

        public const string MethodRoute2GetById = "Route2GetById";
        public const string MethodRoute2GetByScheduleId = "Route2GetByScheduleId";
        public const string MethodRoute2FindByFleetId = "Route2FindByFleetId";
        public const string MethodRoute2FindAllByFleetId = "Route2FindAllByFleetId";
        public const string MethodRoute2AndScheduleFindByFleetId = "Route2AndScheduleFindByFleetId";
        public const string MethodRoute2AndScheduleFindAllByFleetId = "Route2AndScheduleFindAllByFleetId";
        public const string MethodRoute2Update = "Route2Update";
        public const string MethodRoute2Simplify = "Route2Simplify";
        public const string MethodRoute2GetSchedulesByRouteId = "Route2GetSchedulesByRouteId";
        public const string MethodRoute2GetScheduleById = "Route2GetScheduleById";
        public const string MethodRoute2CheckPointsByRouteId = "Route2CheckPointsByRouteId";


        public RouteWebServicePlugin(PluginDefinition pluginDefinition)
        {
            _pluginDefinition = pluginDefinition;
        }

        public PluginMethodResult GetPluginData(string dataSource, object[] parameters, long auditUserID = 0)
        {
            PluginMethodResult result = new PluginMethodResult();

            switch (dataSource)
            {
                case MethodRoute2GetById:
                    result.DataSet = Route2GetById((int)parameters[0]);
                    break;

                case MethodRoute2FindByFleetId:
                    result.DataSet = Route2FindByFleetId(parameters, false);
                    break;

                case MethodRoute2AndScheduleFindByFleetId:
                    result.DataSet = Route2AndScheduleFindByFleetId(parameters, false);
                    break;

                case MethodRoute2AndScheduleFindAllByFleetId:
                    result.DataSet = Route2AndScheduleFindByFleetId(parameters, true);
                    break;

                case MethodRoute2FindAllByFleetId:
                    result.DataSet = Route2FindByFleetId(parameters, true);
                    break;

                case MethodRoute2GetSchedulesByRouteId:
                    result.DataSet = Route2GetSchedulesByRouteId((int)parameters[0]);
                    break;

                case MethodRoute2GetScheduleById:
                    result.DataSet = Route2GetScheduleById((int)parameters[0]);
                    break;

                case MethodRoute2GetByScheduleId:
                    result.DataSet = Route2GetByScheduleId((int)parameters[0]);
                    break;

                case MethodRoute2CheckPointsByRouteId:
                    result.DataSet = Route2CheckPointsByRouteId((int)parameters[0]);
                    break;

                default:
                    throw new NotImplementedException(dataSource);
            }

            return result;
        }

        public PluginMethodResult UpdatePluginData(string dataSource, object[] parameters, DataSet updates, long auditUserID = 0)
        {
            PluginMethodResult result = new PluginMethodResult();

            switch (dataSource)
            {
                case MethodRoute2Update:
                    result.DataSet = Route2Update(updates.Convert<DataSetRoute2>());
                    break;

                case MethodRoute2Simplify:
                    result.DataSet = Route2Simplify(updates.Convert<DataSetRoute2>());
                    break;

                default:
                    throw new NotImplementedException(dataSource);
            }

            return result;
        }


        private DataSetRoute2 Route2Simplify(DataSetRoute2 dataSetRoute2)
        {
            DataSetRoute2.T_Route2PointDataTable table = dataSetRoute2.T_Route2Point;
            int[] redundantIndices = PolyLine2D.GetRedundantPointIndices(new Route2PointPoint2DProvider(table), _redundantPointDistance);

            for (int i = redundantIndices.Length - 1; i > 0; i--)
            {
                // Cannot Remove CheckPoint Points
                DataSetRoute2.T_Route2PointRow pointRow = table[redundantIndices[i]];
                if (pointRow.GetT_Route2CheckPointRows().Length == 0)
                    pointRow.Delete();
            }
            return dataSetRoute2;
        }

        public void PluginOnChange(string datasource, object[] parameters, DataSet data)
        {
            if (PluginDataChanged != null)
                PluginDataChanged(datasource, parameters, data);
        }

        public event PluginDataChangedDelegate PluginDataChanged;

        private string prpDSN
        {
            get { return (string)_pluginHost.ContextProvider["DSN"]; }
        }

        public DataSetRoute2 Route2GetById(int route2Id)
        {
            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.GetRoute2(route2Id);
        }

        private DataSetRoute2 Route2GetByScheduleId(int route2ScheduleId)
        {
            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.GetRoute2BySchedule(route2ScheduleId);
        }

        private DataSetRoute2 Route2CheckPointsByRouteId(int route2Id)
        {
            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.GetRoute2CheckPointsByRouteId(route2Id);
        }


        public DataSetRoute2 Route2FindByFleetId(object[] parameters, bool includeRoutesInError)
        {
            int[] fleetIds = new int[parameters.Length];
            parameters.CopyTo(fleetIds, 0);

            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.FindRoute2ByFleet(fleetIds, includeRoutesInError);
        }

        public DataSetRoute2 Route2AndScheduleFindByFleetId(object[] parameters, bool includeRoutesInError)
        {
            int[] fleetIds = new int[parameters.Length];
            parameters.CopyTo(fleetIds, 0);

            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.FindRoute2AndSchedulesByFleet(fleetIds, includeRoutesInError);
        }

        private DataSet Route2GetSchedulesByRouteId(int route2Id)
        {
            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.GetRouteSchedulesByRouteId(route2Id);
        }

        private DataSet Route2GetScheduleById(int routeScheduleId)
        {
            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.GetRouteScheduleById(routeScheduleId);
        }

        public DataSetRoute2 Route2Update(DataSetRoute2 ds)
        {
            // Any Added or Modified Points need to be Simplified before Saving ..
            DataTable pointChanges = ds.T_Route2Point.GetChanges(DataRowState.Added | DataRowState.Modified);
            if ((pointChanges != null) && (pointChanges.Rows.Count > 0))
                ds = Route2Simplify(ds);

            using (Route2Biz biz = new Route2Biz(prpDSN))
                return biz.Route2Update(ds);
        }

        public string Name
        {
            get { return "Route WebService Plugin"; }
        }

        public string Description
        {
            get { return "WebService Plugin for Routes"; }
        }

        public void Initialise(IPluginHost host)
        {
            _pluginHost = host;

            Hashtable registeredDataSources = (Hashtable)host.ContextProvider["DATASOURCEMAP"];
            registeredDataSources[MethodRoute2GetById] = this;
            registeredDataSources[MethodRoute2FindByFleetId] = this;
            registeredDataSources[MethodRoute2FindAllByFleetId] = this;
            registeredDataSources[MethodRoute2AndScheduleFindByFleetId] = this;
            registeredDataSources[MethodRoute2AndScheduleFindAllByFleetId] = this;
            registeredDataSources[MethodRoute2Update] = this;
            registeredDataSources[MethodRoute2Simplify] = this;
            registeredDataSources[MethodRoute2GetSchedulesByRouteId] = this;
            registeredDataSources[MethodRoute2GetScheduleById] = this;
            registeredDataSources[MethodRoute2GetByScheduleId] = this;
        }

        public void Finalise(IPluginHost host)
        {
            Hashtable registeredDataSources = (Hashtable)host.ContextProvider["DATASOURCEMAP"];
            registeredDataSources[MethodRoute2GetById] = null;
            registeredDataSources[MethodRoute2FindByFleetId] = null;
            registeredDataSources[MethodRoute2FindAllByFleetId] = null;
            registeredDataSources[MethodRoute2AndScheduleFindByFleetId] = null;
            registeredDataSources[MethodRoute2AndScheduleFindAllByFleetId] = null;
            registeredDataSources[MethodRoute2Update] = null;
            registeredDataSources[MethodRoute2Simplify] = null;
            registeredDataSources[MethodRoute2GetSchedulesByRouteId] = null;
            registeredDataSources[MethodRoute2GetScheduleById] = null;
            registeredDataSources[MethodRoute2GetByScheduleId] = null;
        }
    }
}

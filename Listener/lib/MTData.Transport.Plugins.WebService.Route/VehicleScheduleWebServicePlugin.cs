using System;
using System.Collections;
using System.Data;
using System.Net;
using System.Net.Sockets;
using MTData.Common.Plugin;
using MTData.Common.Plugin.Configuration;
using MTData.Common.Utilities;
using MTData.Transport.Service.Route.Data;


namespace MTData.Transport.Plugins.WebService.Route
{
    public class VehicleScheduleWebServicePluginFactory : IPluginFactory
    {
        public IPlugin Create(PluginDefinition definition)
        {
            return new VehicleScheduleWebServicePlugin(definition);
        }
    }

    public class VehicleScheduleWebServicePlugin : IPlugin, IExtendedPluginDataProvider
    {
        private IPluginHost _pluginHost;
        private PluginDefinition _pluginDefinition;
        private UdpClient _udpClient;
        private string _listenerAddress;
        private int _listenerPort;

        public const string MethodVehicleScheduleGetById = "VehicleScheduleGetById";
        public const string MethodVehicleScheduleFind = "VehicleScheduleFind";
        public const string MethodVehicleScheduleUpdate = "VehicleScheduleUpdate";
        public const string MethodVehicleScheduleUpdateFromUnit = "VehicleScheduleUpdateFromUnit";


        public const string MethodUpdateRouteScheduleDataSet = "UpdateRouteScheduleDataSet";
        public const string MethodGetRouteScheduleDataSet = "GetRouteScheduleDataSet";

        public const string MethodGetActiveRouteStatus = "GetActiveRouteStatus";
        public const string MethodGetActiveRouteStatusByUser = "GetActiveRouteStatusByUser";

        public const string MethodDriverScheduleGetById = "DriverScheduleGetById";
        public const string MethodDriverScheduleFind = "DriverScheduleFind";
        public const string MethodDriverScheduleUpdate = "DriverScheduleUpdate";


        public VehicleScheduleWebServicePlugin(PluginDefinition pluginDefinition)
        {
            _pluginDefinition = pluginDefinition;
        }

        public PluginMethodResult GetPluginData(string dataSource, object[] parameters, long auditUserID)
        {
            switch (dataSource)
            {
                case MethodVehicleScheduleGetById:
                    return VehicleScheduleGetById((int)parameters[0]);

                case MethodVehicleScheduleFind:
                    return VehicleScheduleFind(parameters);

                case MethodGetRouteScheduleDataSet:
                    return GetRouteScheduleDataSet((int)parameters[0]);

                case MethodGetActiveRouteStatus:
                    if (parameters.Length == 2) //Fleet Only
                        return GetActiveRouteStatus((int)parameters[0], (bool)parameters[1], null);
                    else //Fleet Vehicle
                        return GetActiveRouteStatus((int)parameters[0], (bool)parameters[1], (int)parameters[2]);
                case MethodGetActiveRouteStatusByUser:
                    return GetActiveRouteStatusByUser((int)parameters[0], (bool)parameters[1]);


                case MethodDriverScheduleGetById:
                    return DriverScheduleGetById((int)parameters[0]);

                case MethodDriverScheduleFind:
                    return DriverScheduleFind(parameters);



            }

            throw new NotImplementedException(dataSource);
        }

        private PluginMethodResult DriverScheduleFind(object[] parameters)
        {
            int fleetCount = (int)parameters[0];
            int[] fleetIds = new int[fleetCount];
            for (int i = 0; i < fleetCount; i++)
                fleetIds[i] = (int)parameters[1 + i];
            DateTime utcFromDateInclusive = (DateTime)parameters[1 + fleetCount + 0];
            DateTime utcToDateExclusive = (DateTime)parameters[1 + fleetCount + 1];
            bool includeFatigue = false;
            if (parameters.Length > (1 + fleetCount + 2))
            {
                includeFatigue = (bool)parameters[1 + fleetCount + 2];
            }

            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.FindDriverSchedule(fleetIds, utcFromDateInclusive, utcToDateExclusive, includeFatigue);
        }

        private PluginMethodResult DriverScheduleGetById(int id)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.GetDriverScheduleGetById(id);
        }



        public PluginMethodResult UpdatePluginData(string dataSource, object[] parameters, DataSet updates, long auditUserID)
        {
            switch (dataSource)
            {
                case MethodVehicleScheduleUpdate:
                    return VehicleScheduleUpdate((int)auditUserID, updates.Convert<DataSetVehicleSchedule>());

                case MethodVehicleScheduleUpdateFromUnit:
                    return VehicleScheduleUpdateFromUnit((int)auditUserID, updates.Convert<DataSetVehicleSchedule>());

                case MethodUpdateRouteScheduleDataSet:
                    return UpdateRouteScheduleDataSet(updates.Convert<DataSetRouteSchedule>(), (int)parameters[0]);


                case MethodDriverScheduleUpdate:
                    return DriverScheduleUpdate((int)auditUserID, updates.Convert<DataSetDriverRouteAllocation>());
            }

            throw new NotImplementedException(dataSource);
        }

        private PluginMethodResult DriverScheduleUpdate(int userId, DataSetDriverRouteAllocation ds)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.RouteToDriverAllocationUpdate(ds, userId);
        }

        public void PluginOnChange(string datasource, object[] parameters, DataSet data)
        {
            if (PluginDataChanged != null)
                PluginDataChanged(datasource, parameters, data);
        }

        public event PluginDataChangedDelegate PluginDataChanged;

        private string prpDSN
        {
            get { return (string)_pluginHost.ContextProvider["DSN"]; }
        }

        public PluginMethodResult VehicleScheduleGetById(int route2Id)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.GetVehicleSchedule(route2Id);
        }

        private PluginMethodResult VehicleScheduleFind(object[] parameters)
        {
            int fleetCount = (int)parameters[0];
            int[] fleetIds = new int[fleetCount];
            for (int i = 0; i < fleetCount; i++)
                fleetIds[i] = (int)parameters[1 + i];
            DateTime utcFromDateInclusive = (DateTime)parameters[1 + fleetCount + 0];
            DateTime utcToDateExclusive = (DateTime)parameters[1 + fleetCount + 1];

            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.FindVehicleSchedule(fleetIds, utcFromDateInclusive, utcToDateExclusive);
        }

        private PluginMethodResult GetRouteScheduleDataSet(int userID)
        {

            using (RouteScheduleBiz biz = new RouteScheduleBiz(prpDSN))
                return biz.GetRouteScheduleDataSet(userID);
        }

        private PluginMethodResult UpdateRouteScheduleDataSet(DataSetRouteSchedule dataSetRouteSchedule, int userID)
        {
            using (RouteScheduleBiz biz = new RouteScheduleBiz(prpDSN))
            {
                return biz.UpdateRouteScheduleDataSet(dataSetRouteSchedule, userID);
            }

        }

        public PluginMethodResult VehicleScheduleUpdate(int userId, DataSetVehicleSchedule ds)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.VehicleScheduleUpdate(ds, userId);
        }

        public PluginMethodResult VehicleScheduleUpdateFromUnit(int userId, DataSetVehicleSchedule ds)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
            {
                //update the database
                PluginMethodResult result = biz.VehicleScheduleUpdate(ds, userId);

                lock (_udpClient)
                {
                    //send message to listener to update all clients
                    int fleetId = ds.T_VehicleSchedule[0].FleetID;
                    string str = string.Format("ClientBroadcast,{0},DATAMODEL:CUSTOM:ROUTE2VEHICLESCHEDULEUPDATE:0,Update", fleetId);
                    byte[] data = System.Text.Encoding.ASCII.GetBytes(str);
                    _udpClient.Send(data, data.Length, _listenerAddress, _listenerPort);

                    //send message to listener to update all units
                    data = System.Text.Encoding.ASCII.GetBytes("ConfigCheck,0,Route,0");
                    _udpClient.Send(data, data.Length, _listenerAddress, _listenerPort);
                }
                return result;
            }
        }


        private PluginMethodResult GetActiveRouteStatus(int fleetID, bool activeRowOnly, int? vehicle)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.GetActiveRouteStatus(fleetID, activeRowOnly, vehicle);
        }

        private PluginMethodResult GetActiveRouteStatusByUser(int userID, bool activeRowOnly)
        {
            using (VehicleScheduleBiz biz = new VehicleScheduleBiz(prpDSN))
                return biz.GetActiveRouteStatusByUser(userID, activeRowOnly);
        }

        public string Name
        {
            get { return "Vehicle Schedule WebService Plugin"; }
        }

        public string Description
        {
            get { return "WebService Plugin for Vehicle Schedules"; }
        }

        public void Initialise(IPluginHost host)
        {
            _pluginHost = host;

            Hashtable registeredDataSources = (Hashtable)host.ContextProvider["DATASOURCEMAP"];
            registeredDataSources[MethodVehicleScheduleUpdate] = this;
            registeredDataSources[MethodVehicleScheduleFind] = this;
            registeredDataSources[MethodVehicleScheduleGetById] = this;
            registeredDataSources[MethodVehicleScheduleUpdateFromUnit] = this;
            registeredDataSources[MethodUpdateRouteScheduleDataSet] = this;
            registeredDataSources[MethodGetRouteScheduleDataSet] = this;
            registeredDataSources[MethodGetActiveRouteStatus] = this;
            registeredDataSources[MethodGetActiveRouteStatusByUser] = this;

            registeredDataSources[MethodDriverScheduleUpdate] = this;
            registeredDataSources[MethodDriverScheduleFind] = this;
            registeredDataSources[MethodDriverScheduleGetById] = this;


            _udpClient = new UdpClient();
            _listenerAddress = System.Configuration.ConfigurationManager.AppSettings["ListenerAddress"];
            _listenerPort = int.Parse(System.Configuration.ConfigurationManager.AppSettings["ListenerPort"]);
        }

        public void Finalise(IPluginHost host)
        {
            Hashtable registeredDataSources = (Hashtable)host.ContextProvider["DATASOURCEMAP"];
            registeredDataSources[MethodVehicleScheduleUpdate] = null;
            registeredDataSources[MethodVehicleScheduleFind] = null;
            registeredDataSources[MethodVehicleScheduleGetById] = null;
            registeredDataSources[MethodVehicleScheduleUpdateFromUnit] = null;
            registeredDataSources[MethodUpdateRouteScheduleDataSet] = null;
            registeredDataSources[MethodGetRouteScheduleDataSet] = null;
            registeredDataSources[MethodGetActiveRouteStatus] = null;
            registeredDataSources[MethodGetActiveRouteStatusByUser] = null;

            registeredDataSources[MethodDriverScheduleUpdate] = null;
            registeredDataSources[MethodDriverScheduleFind] = null;
            registeredDataSources[MethodDriverScheduleGetById] = null;

            _udpClient.Close();
            _udpClient = null;
        }

    }
}

using System;
using MTData.Common.Interface;

namespace MTData.Common.ContextProvider
{
    /// <summary>
    /// This class provides a ContextProvider that raises
    /// an event for items requested.
    /// It can provide callback functionality for any object.
    /// </summary>
    public class ContextProviderCallback : IContextProvider
    {
        private object _syncRoot = new object();
        private object _owner = null;

        public class ContextProviderCallbackArgs
        {
            private ContextProviderCallback _provider;
            private object _result = null;
            private string _path = null;
            private object _owner = null;
            private bool _contains = false;

            public ContextProviderCallbackArgs(ContextProviderCallback provider, object owner, string path, object initialResult)
            {
                _provider = provider;
                _result = initialResult;
                _owner = owner;
                _path = path;
            }

            public ContextProviderCallback Provider
            {
                get
                {
                    return _provider;
                }
            }

            public object Owner
            {
                get
                {
                    return _owner;
                }
            }

            public string Path
            {
                get
                {
                    return _path;
                }
            }

            public object Result
            {
                get
                {
                    return _result;
                }
                set
                {
                    _result = value;
                }
            }

            public bool Contains
            {
                get { return _contains; }
                set { _contains = value; }
            }

        }

        public delegate void ContextProviderCallbackDelegate(object sender, ContextProviderCallbackArgs args);

        public event ContextProviderCallbackDelegate GetCallback;
        public event ContextProviderCallbackDelegate SetCallback;
        public event ContextProviderCallbackDelegate ContainsCallback;

        public ContextProviderCallback(object owner)
        {
            _owner = owner;
        }

        #region IContextProvider Members

        public object SyncRoot
        {
            get
            {
                return _syncRoot;
            }
        }

        public object this[string path]
        {
            get
            {
                if (GetCallback != null)
                {
                    ContextProviderCallbackArgs args = new ContextProviderCallbackArgs(this, _owner, path, null);
                    GetCallback(this, args);
                    return args.Result;
                }
                else
                    return null;
            }
            set
            {
                if (SetCallback != null)
                {
                    ContextProviderCallbackArgs args = new ContextProviderCallbackArgs(this, _owner, path, value);
                    SetCallback(this, args);
                }
            }
        }

        public bool Contains(string path)
        {
            if (ContainsCallback != null)
            {
                ContextProviderCallbackArgs args = new ContextProviderCallbackArgs(this, _owner, path, null);
                ContainsCallback(this, args);
                return args.Contains;
            }
            else
                return false;
        }

        public void RegisterChange(string path)
        {
            if (Change != null)
                Change(this, path);
        }

        public event ContextProviderChangedDelegate Change;

        #endregion
    }
}

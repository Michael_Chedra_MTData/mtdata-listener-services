using System;
using System.Xml;
using System.Configuration;

namespace MTData.Common.ContextProvider
{
	/// <summary>
	/// This class will return a standard context provider, having populated it with details
	/// from a config file.
	/// </summary>
	public class ContextProviderConfigSectionHandler : IConfigurationSectionHandler
	{
		public ContextProviderConfigSectionHandler()
		{

		}
		#region IConfigurationSectionHandler Members

		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			ContextProviderStandard result = new ContextProviderStandard();

			XmlNodeList list = section.SelectNodes("add");
			foreach(XmlNode node in list)
			{
				XmlNode keyNode = node.Attributes.GetNamedItem("key");
				if (keyNode != null)
				{
					XmlNode valueNode = node.Attributes.GetNamedItem("value");
					if (valueNode != null)
					{
						result.Register(keyNode.Value, valueNode.Value);
					}
				}
			}

			return result;
		}

		#endregion
	}
}

using System;
using MTData.Common.Interface;

namespace MTData.Common.Template
{
	/// <summary>
	/// This class will use a template, and process the fields in it, returning a string.
	/// </summary>
	public class TemplateProcessor
	{
		ITemplateDescriptor _template = null;
		IFormatProvider		_formatProvider = null;

		public TemplateProcessor(ITemplateDescriptor template)
		{
			_template = template;
		}

		public TemplateProcessor(IFormatProvider formatProvider)
		{
			_formatProvider = formatProvider;
		}

		public ITemplateDescriptor Template
		{
			get { return _template; }
			set { _template = value; }
		}

		/// <summary>
		/// Evaluate the template by extracting data from the context provider.
		/// </summary>
		/// <param name="provider"></param>
		/// <returns></returns>
		public string Evaluate(IContextProvider provider)
		{
			if (_template == null)
				return string.Empty;

			object[] parameters = new object[_template.FieldCount];
			for(int loop = 0; loop < _template.FieldCount; loop++)
			{
				if (provider.Contains(_template[loop]))
				{
					try
					{
						parameters[loop] = provider[_template[loop]];
					}
					catch
					{
						parameters[loop] = "";
					}
				}
				else
					parameters[loop] = "";
			}
			return string.Format(_formatProvider, _template.FormattingString, parameters);
		}
	}
}

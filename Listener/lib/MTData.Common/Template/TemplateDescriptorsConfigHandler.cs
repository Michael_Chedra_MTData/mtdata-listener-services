using System;
using System.Xml;

namespace MTData.Common.Template
{
	/// <summary>
	/// This class will load up all of the descirptors from the config file into a new list.
	/// </summary>
	public class TemplateDescriptorsConfigHandler : System.Configuration.IConfigurationSectionHandler
	{
		public TemplateDescriptorsConfigHandler()
		{
			
		}
		#region IConfigurationSectionHandler Members

		/// <summary>
		/// The format of the descriptors in the app config will be the following..
		/// <code>
		///		<DisplayTemplates>
		///			<Template name="TruckLiveIcon" value="Fleet&#x9;&#x9;: {%VehicleReport.FleetID%}&#xD;&#xA;Unit&#x9;&#x9;: {%VehicleReport.VehicleID%}&#xD;&#xA;Registration&#9;: {%VehicleReport.VehicleRegistration%}&#xD;&#xA;Driver&#9;&#x9;: {%VehicleReport.DriverName%}&#xD;&#xA;Location&#x9;&#x9;: {%VehicleReport.Location%}&#xD;&#xA;Speed/Max&#x9;: {%VehicleReport.Speed%} / {%VehicleReport.MaxSpeed%} Kmh&#xD;&#xA;Time&#9;&#x9;: {%VehicleReport.DeviceDate%} {%VehicleReport.DeviceTime%}&#xD;&#xA;Reason&#9;&#x9;: {%VehicleReport.ReasonName%}"/>
		///			<Template name="TruckWaypointIcon" value="Waypoint : {%Waypoint.Name%}"/>
		///		</DisplayTemplates>
		/// </code>
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="configContext"></param>
		/// <param name="section"></param>
		/// <returns></returns>
		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			TemplateDescriptors descriptors = new TemplateDescriptors();

			XmlNodeList list = section.SelectNodes("Template");
			if ((list != null) && (list.Count > 0))
			{
				foreach(XmlNode node in list)
				{
					XmlNode temp = node.Attributes.GetNamedItem("name");
					if (temp != null)
					{
						string name = temp.Value;
						temp = node.Attributes.GetNamedItem("value");
						if (temp != null)
							descriptors.Add(name, new TemplateStringParser(temp.Value));
					}
				}
			
			}
			return descriptors;
		}

		#endregion
	}
}

using System;
using System.Collections;
using MTData.Common.Interface;

namespace MTData.Common.Template
{
	/// <summary>
	/// This class provides an easy mechanism for storing templates, and 
	/// referencing them by name.
	/// </summary>
	public class TemplateDescriptors
	{
		private Hashtable _map = new Hashtable();

		public TemplateDescriptors()
		{
			
		}

		/// <summary>
		/// Add a descriptor to the map
		/// </summary>
		/// <param name="name"></param>
		/// <param name="descriptor"></param>
		public void Add(string name, ITemplateDescriptor descriptor)
		{
			_map.Add(name, descriptor);
		}

		/// <summary>
		/// Remove a descriptor from the map
		/// </summary>
		/// <param name="name"></param>
		public void Remove(string name)
		{
			_map.Remove(name);
		}

		/// <summary>
		/// This is an accessor for the descriptors.
		/// </summary>
		public ITemplateDescriptor this[string name]
		{
			get{ return (ITemplateDescriptor)_map[name]; }
		}
	}
}

using System;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Common.Database
{
	/// <summary>
	/// Summary description for cSingleDBConnection.
	/// </summary>
	public class SingleDBConnection
	{
		public SingleDBConnection()
		{
		}

		public static object ExecuteSingleValue(string sDSN, string sSQL, string sColName)
		{
			bool bConnectionFailed = false;
			object oRet = null;

			oRet = ExecuteSingleValue(sDSN, sSQL, sColName, ref bConnectionFailed);
			return oRet;
		}

		public static object ExecuteSingleValue(string sDSN, string sSQL, string sColName, ref bool bConnectionFailed)
		{
			object oRet = null;
			try
			{
				System.Data.DataSet oDS = ExecuteDS(sDSN, sSQL, ref bConnectionFailed);
				if (oDS.Tables[0].Rows.Count > 0)
					oRet = oDS.Tables[0].Rows[0][sColName];
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return oRet;
		}

		public static DataRow[] ExecuteDRows(string sDSN, string sSQL)
		{
			System.Data.DataRow[] oRet = null;
			bool bConnectionFailed = false;

			oRet = ExecuteDRows(sDSN, sSQL, ref bConnectionFailed);
			return oRet;
		}

		public static DataRow[] ExecuteDRows(string sDSN, string sSQL, ref bool bConnectionFailed)
		{
			System.Data.DataRow[] oRet = null;
			try
			{
				System.Data.DataSet oDS = ExecuteDS(sDSN, sSQL, ref bConnectionFailed);
				oRet = new DataRow[oDS.Tables[0].Rows.Count];
				for (int X = 0; X < oDS.Tables[0].Rows.Count; X++)
				{
					oRet[X] = oDS.Tables[0].Rows[X];
				}
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return oRet;
		}

		public static DataTable ExecuteDT(string sDSN, string sSQL)
		{
			System.Data.DataTable oRet = null;
			bool bConnectionFailed = false;
			try
			{
				oRet = ExecuteDT(sDSN, sSQL, ref bConnectionFailed);
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return oRet;
		}

		public static DataTable ExecuteDT(string sDSN, string sSQL, ref bool bConnectionFailed)
		{
			System.Data.DataTable oRet = null;
			try
			{
				System.Data.DataSet oDS = ExecuteDS(sDSN, sSQL, ref bConnectionFailed);
				oRet = oDS.Tables[0];
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return oRet;
		}

		public static DataSet ExecuteDS(string sDSN, string sSQL)
		{
			bool bConnectedFailed = false;
			DataSet oRawData = null;

			oRawData = ExecuteDS(sDSN, sSQL, ref bConnectedFailed);

			return oRawData;
		}
		public static DataSet ExecuteDS(string sDSN, string sSQL, ref bool bConnectionFailed)
		{
			DataSet oRawData = null;
			try
			{
                using (SqlConnection objSQLConn = new SqlConnection(sDSN))
                using (SqlCommand objSQLCmd = objSQLConn.CreateCommand())
                using (SqlDataAdapter objSQLDA = new SqlDataAdapter(objSQLCmd))
                {
                    objSQLConn.Open();
                    objSQLCmd.CommandText = sSQL;
                    objSQLCmd.CommandTimeout = 3600;
					oRawData = new System.Data.DataSet();
                    objSQLDA.Fill(oRawData);
				}
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return oRawData;
		}

		public static int ExecuteNonQuery(string sDSN, string sSQL, ref bool bConnectionFailed)
		{
			int iRet = 0;
			try
			{
                using (SqlConnection objSQLConn = new SqlConnection(sDSN))
                using (SqlCommand objSQLCmd = objSQLConn.CreateCommand())
                {
                    objSQLConn.Open();
                    objSQLCmd.CommandText = sSQL;
                    objSQLCmd.CommandTimeout = 3600;
                    iRet = objSQLCmd.ExecuteNonQuery();
				}
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return iRet;
		}

	}
}

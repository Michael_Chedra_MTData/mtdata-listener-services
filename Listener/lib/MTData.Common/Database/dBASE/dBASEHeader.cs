using System;
using System.Text;

namespace MTData.Common.Database.dBASE
{
	/// <summary>
	/// Summary description for dBaseHeader.
	/// </summary>
	public class dBASEHeader
	{
		public int Version = 0;
		public DateTime LastModified = DateTime.MinValue;
		public int RecordCount = 0;
		public int HeaderLength = 0;
		public int RecordLength = 0;
		public int IncompleteTransaction = 0;
		public int EncryptionFlag = 0;
		public int MDXFlag = 0;
		public int DriverID = 0;

		public dBASEHeader()
		{
			
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append("\r\nVersion:");
			builder.Append(Version);
			builder.Append("\r\nDate:");
			builder.Append(LastModified.ToString("YYYY/MM/DD"));
			builder.Append("\r\nRecord Count = ");
			builder.Append(RecordCount);
			builder.Append("\r\nHeader Bytes = ");
			builder.Append(HeaderLength);
			builder.Append("\r\nRecord Bytes = ");
			builder.Append(RecordLength);
			builder.Append("\r\nIncomplete dBase IV transaction = ");
			builder.Append(IncompleteTransaction);
			builder.Append("\r\ndBase IV Encryption Flag = ");
			builder.Append(EncryptionFlag);
				
			builder.Append("\r\nProduction MDX Flag = ");
			builder.Append(MDXFlag);
			builder.Append("\r\nLanguage Driver ID = ");
			builder.Append(DriverID);

			return builder.ToString();
		}

	}
}

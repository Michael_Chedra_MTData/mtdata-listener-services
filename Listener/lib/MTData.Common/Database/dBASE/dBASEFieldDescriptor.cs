using System;
using System.Collections;
using System.Text;

namespace MTData.Common.Database.dBASE
{
	public class dBASEFields
	{
		private ArrayList _fieldList = new ArrayList();
		private Hashtable _fieldIndex = new Hashtable();

		public dBASEFields()
		{
		}

		public void AddFieldDefinition(dBASEFieldDescriptor fieldDefinition)
		{
			fieldDefinition.Index = _fieldList.Add(fieldDefinition);
			_fieldIndex.Add(fieldDefinition.Name, fieldDefinition);
		}

		public int Count
		{
			get
			{
				return _fieldList.Count;
			}
		}

		public bool FieldExists(string name)
		{
			return _fieldIndex.Contains(name);
		}

		public dBASEFieldDescriptor GetField(int index)
		{
			return (dBASEFieldDescriptor)_fieldList[index];
		}

		public dBASEFieldDescriptor GetField(string fieldName)
		{
			return (dBASEFieldDescriptor)_fieldIndex[fieldName];
		}

		public dBASEFieldDescriptor this[int index]
		{
			get
			{
				return GetField(index);
			}
		}

		public dBASEFieldDescriptor this[string fieldName]
		{
			get
			{
				return GetField(fieldName);
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();

			builder.Append("\r\n=============== Fields ====================");
			for(int loop = 0; loop < _fieldList.Count; loop++)
			{
				builder.Append("\r\n=============== Field ====================\r\n");
				dBASEFieldDescriptor field = (dBASEFieldDescriptor)_fieldList[loop];
				builder.Append(field.ToString());

			}
			return builder.ToString();
		}

	}

	/// <summary>
	/// Summary description for FieldDescriptor.
	/// </summary>
	public class dBASEFieldDescriptor
	{
		public string Name = "";
		
		public System.Data.DbType FieldType = System.Data.DbType.String;

		public int Length = 0;
		public int DecimalCount= 0;
		public int WorkAreaID = 0;
		public int SetFieldsFlag = 0;
		public int NextautoIncrement = 0;
		public int IndexFieldFlag = 0;

		public int Index = 0;

        public dBASEFieldDescriptor()
		{
			
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append("\r\nName=");
			builder.Append(Name);
			builder.Append("\r\nFieldType=");
			builder.Append(FieldType);
			builder.Append("\r\nLength=");
			builder.Append(Length);
			builder.Append("\r\nDecimalCount=");
			builder.Append(DecimalCount);
			builder.Append("\r\nWorkAreaID=");
			builder.Append(WorkAreaID);
			builder.Append("\r\nSetFieldsFlag=");
			builder.Append(SetFieldsFlag);
			builder.Append("\r\nNextautoIncrement=");
			builder.Append(NextautoIncrement);
			builder.Append("\r\nIndexFieldFlag=");
			builder.Append(IndexFieldFlag);

			return builder.ToString();
		}

	}
}

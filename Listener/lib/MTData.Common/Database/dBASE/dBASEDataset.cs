using System;
using System.IO;
using System.Text;
using System.Collections;

namespace MTData.Common.Database.dBASE
{
	/// <summary>
	/// Summary description for dBaseFile.
	/// </summary>
	public class dBASEDataSet
	{
		public string FileName = "";
		public dBASEHeader Header = new dBASEHeader();

		private dBASEFields _fields = null;

		private dBASERecords _records = null;

		public dBASEDataSet()
		{
			_fields = new dBASEFields();
			_records = new dBASERecords(_fields);
		}

		public dBASEFields Fields
		{
			get
			{
				return _fields;
			}
			set
			{
				_fields = value;
				if (_records != null)
					_records = new dBASERecords(_fields);
			}
		}

		public dBASERecords Records
		{
			get
			{
				return _records;
			}
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(string.Format("=============== {0} ====================\r\n", Path.GetFileName(FileName)));
			builder.Append(Header.ToString());
			builder.Append(Fields.ToString());
			builder.Append(Records.ToString());

			return builder.ToString();
		}

	}
}

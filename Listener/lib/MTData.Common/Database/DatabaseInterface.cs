using System;
using System.Threading;
//
using System.Data.SqlClient;
using log4net;
using MTData.Common.Threading;

namespace MTData.Common.Database
{
	/// <summary>
	/// Summary description for DatabaseInterface.
	/// </summary>
	public class DatabaseInterface
	{
		#region Private Vars
        private const string sClassName = "MTData.Database.DatabaseInterface.";
        private static ILog _log = LogManager.GetLogger(typeof(DatabaseInterface));

		private bool bAutoConnect = false;
		private bool bConnected = false;
		private string sConnectString = "";
		private SqlConnection objSQLConn;
		private EnhancedThread _thread;
		private object SyncRoot = null;
		#endregion

		#region Public Properties
		public System.Data.ConnectionState State
		{
			get
			{
				System.Data.ConnectionState oState = System.Data.ConnectionState.Broken;
				if(objSQLConn != null)
				{
					try
					{
						oState = objSQLConn.State;
					}
					catch(System.Exception ex)
					{
                        _log.Error(sClassName + "State (Get)", ex);
					}
				}
				return oState;
			}
		}

		public bool Connected
		{
			get
			{
				return bConnected;
			}
		}

		public System.Threading.ThreadState ThreadState
		{
			get
			{
				System.Threading.ThreadState oState = System.Threading.ThreadState.Unstarted;

				if(_thread != null)
				{
					oState = _thread.State;
				}
				return oState;
			}
		}
		#endregion

		#region Constructor / Destructor
        public DatabaseInterface(SqlConnection conn)
        {
            this.SyncRoot = new object();
            this.objSQLConn = conn;
            this.bConnected = this.objSQLConn.State == System.Data.ConnectionState.Open;
        }

		public DatabaseInterface(string sDSN, bool bTryReconnect)
		{
			SyncRoot = new object();
			bConnected = false;
			bAutoConnect = false;
			sConnectString = sDSN;

			if(bTryReconnect)
			{
				try
				{
					_thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
					_thread.Name = "Database Connection Thread";
					bAutoConnect = true;
					_thread.Start();
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "DatabaseInterface(string sDSN = '" + sDSN+ "', bool bTryReconnect)", ex);
				}
			}
			else
			{
				try
				{
					objSQLConn = new System.Data.SqlClient.SqlConnection();
					objSQLConn.ConnectionString = sConnectString;
					objSQLConn.Open();		
					bConnected = true;
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "DatabaseInterface(string sDSN = '" + sDSN + "', bool bTryReconnect)", ex);
					bConnected = false;
				}
			}
		}


		public void Dispose()
		{

			bAutoConnect = false;
			bConnected = false;

			if(_thread != null)
			{
				try
				{
					_thread.Stop();
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "Dispose()", ex);
				}
				finally
				{
					_thread = null;
				}
			}

			if(objSQLConn != null)
			{
				try
				{
					objSQLConn.Close();
					objSQLConn.Dispose();
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "Dispose()", ex);
				}
				finally
				{
					objSQLConn = null;
				}

			}
		}
		#endregion

		#region Start / Stop Connection Thread
		public void Start()
		{
			// Close any current objects
			Dispose();

			bConnected = false;
			bAutoConnect = false;

			try
			{
				_thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
				_thread.Name = "Database Connection Thread";
				bAutoConnect = true;
				_thread.Start();
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "Start()", ex);
			}

		}

		public void Stop()
		{
			try
			{
				bAutoConnect = false;
				if(_thread != null)  _thread.Stop();
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "Stop()", ex);
			}
			finally
			{
				// Clean up any objects.
				Dispose();
			}
		}
		#endregion

		#region Select Data / Execute Command (Thread-Safe)
		public System.Data.DataRowCollection ExecuteSelect(string SQLCmd)
		{
			System.Data.DataRowCollection dRet = null;
			lock(this.SyncRoot)
			{
				try
				{
					dRet = ExecuteSelect(SQLCmd, 0);
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "ExecuteSelect(string SQLCmd = '" + SQLCmd + ")", ex);
					dRet = null;
				}
			}
			return dRet;
		}

		public System.Data.DataRowCollection ExecuteSelect(string SQLCmd, int iTable)
		{
			System.Data.DataSet objRS = null;
			System.Data.DataRowCollection dRet = null;
			try
			{
				lock(this.SyncRoot)
				{
					// Get the data as a DataSet and extract the DataRow[] from table 0
					objRS = ExecuteSelectDS(SQLCmd);

					if(objRS != null)
						if(objRS.Tables[iTable] != null)
							dRet = objRS.Tables[iTable].Rows;
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ExecuteSelect(string SQLCmd = '" + SQLCmd + ", int iTable = " + iTable + ")", ex);
				dRet = null;
			}
			return dRet;
		}

        public System.Data.DataSet ExecuteSelectDS(string SQLCmd)
        {
            System.Data.SqlClient.SqlCommand objSQLCmd = objSQLConn.CreateCommand();
            objSQLCmd.CommandText = SQLCmd;
            objSQLCmd.CommandTimeout = 3600;

            return this.ExecuteSelectDS(objSQLCmd);
        }

        public System.Data.DataSet ExecuteSelectDS(System.Data.SqlClient.SqlCommand SQLCmd)
		{
			System.Data.DataSet objRS = null;
			System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
			//System.Data.SqlClient.SqlCommand objSQLCmd = null;

            try
			{
				lock(this.SyncRoot)
				{
					// Create the objects
					objRS = new System.Data.DataSet();
					objSQLDA = new System.Data.SqlClient.SqlDataAdapter();

                    if(objSQLConn != null && bConnected)
					{
						//objSQLCmd = objSQLConn.CreateCommand();
						//objSQLCmd.CommandText = SQLCmd;
						//objSQLCmd.CommandTimeout = 3600;

						// Create the data adapter
						//objSQLDA.SelectCommand = objSQLCmd;
                        if (SQLCmd.Connection == null)
                        {
                            SQLCmd.Connection = objSQLConn;
                        }

                        if (objSQLConn.State != System.Data.ConnectionState.Open)
                        {
                            objSQLConn.Open();
                        }

                        objSQLDA.SelectCommand = SQLCmd;

						// Fill the DataSet
						objSQLDA.Fill(objRS);

						// Clean up
						//objSQLCmd.Dispose();
						//objSQLCmd = null;
						objSQLDA.Dispose();
						objSQLDA = null;
					}
					else
					{
						objRS = null;
					}
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ExecuteSelectDS(string SQLCmd = '" + SQLCmd + ")", ex);
				if(bAutoConnect)
				{
                    _log.Info(sClassName + "ExecuteSelectDS(string SQLCmd = '" + SQLCmd + ") - Restarting Connection");
					objSQLConn = null;
				}
			}
			return objRS;
		}

		public System.Data.SqlClient.SqlCommand GetCommand()
		{
			System.Data.SqlClient.SqlCommand oCmd = null;
			lock(this.SyncRoot)
			{
				oCmd = objSQLConn.CreateCommand();
			}
			return oCmd;
		}

		public int ExecuteNonQuery(string SQLCmd)
		{
			int iRet = 0;
			try
			{
				if(objSQLConn != null && bConnected)
				{
					lock(SyncRoot)
					{					
						SqlCommand oCmd = objSQLConn.CreateCommand();
						oCmd.CommandTimeout = 3600;
						oCmd.CommandText = SQLCmd;
						iRet = oCmd.ExecuteNonQuery();
					}
				}
			}
			catch(System.Data.SqlClient.SqlException exSQLCmd)
			{
                _log.Error(sClassName + "ExecuteNonQuery(string SQLCmd = '" + SQLCmd + ")", exSQLCmd);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ExecuteNonQuery(string SQLCmd = '" + SQLCmd + ")", ex);
				if(bAutoConnect)
				{
                    _log.Info(sClassName + "ExecuteNonQuery(string SQLCmd = '" + SQLCmd + ") - Restarting Connection");
					objSQLConn = null;
				}
			}
			return iRet;
		}
		#endregion

		#region The auto connection ThreadHandler
		private object ThreadHandler(EnhancedThread sender, object data)
		{
            
			_log.Info("DB Connection Thread Started");
			try
			{
				while(bAutoConnect)
				{
					if(objSQLConn != null)
					{
						// No problems, sleep for a while.
						Thread.Sleep(500);
					}
					else
					{
						bConnected = false;
						try
						{
							// Try to connect to the database.
							lock(SyncRoot)
							{
								objSQLConn = new System.Data.SqlClient.SqlConnection();
								objSQLConn.ConnectionString = sConnectString;
								objSQLConn.Open();		
								bConnected = true;
							}
							// Connection established, let the thread loop around and sleep whilst the objSQLConn object is not null.
							// If an error occurs elsewhere in this class, set objSQLConn = null and the re-connect thread will kick in.
						}
						catch(System.Data.SqlClient.SqlException exSQLCmd)
						{
                            _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data) - Connect Error : DSN = '" + sConnectString + "'", exSQLCmd);
						}
						catch(System.Exception ex)
						{
                            _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data)", ex);
                            _log.Info(sClassName + "ThreadHandler(EnhancedThread sender, object data) - Sleeping for 60 secs, then another connection will be attempted.");
							objSQLConn = null;
							Thread.Sleep(60000);
						}
					}
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data)", ex);
			}
			return null;
		}
		#endregion
	}
}

﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using log4net;

namespace MTData.Common.Database
{
    public class CheckDatabaseVersion
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CheckDatabaseVersion));
        private readonly string dsn;

        public int Major { get; set; }
        public int Minor { get; set; }
        public int Build { get; set; }
        public int Branch { get; set; }

        public CheckDatabaseVersion(string dsn)
        {
            this.dsn = dsn;
        }

        public bool CheckDatabaseRequiredVersion(int requiredMajor, int requiredMinor, int requiredBranch, int requiredBuild, out DatabaseVersionErrorMessage errorMessage)
        {
            var sql = string.Empty;
            var databaseCorrectVersion = false;
            errorMessage = null;

            try
            {
                sql = $"exec IsDatabaseRequiredVersion {requiredMajor}, {requiredMinor}, {requiredBranch}, {requiredBuild}";

                var data = new DataSet();
                using (var oConn = new SqlConnection(dsn))
                using (var oCmd = new SqlCommand(sql, oConn))
                using (var oDa = new SqlDataAdapter(oCmd))
                {
                    oDa.Fill(data);
                }
                if (data.Tables.Count >= 2)
                {
                    if (data.Tables[0].Rows.Count > 0)
                    {
                        databaseCorrectVersion = Convert.ToBoolean(data.Tables[0].Rows[0]["RequiredVersion"]);
                    }
                    if (data.Tables[1].Rows.Count > 0)
                    {
                        Major = Convert.ToInt32(data.Tables[1].Rows[0]["Major"]);
                        Minor = Convert.ToInt32(data.Tables[1].Rows[0]["Minor"]);
                        Build = Convert.ToInt32(data.Tables[1].Rows[0]["Build"]);
                        Branch = Convert.ToInt32(data.Tables[1].Rows[0]["Branch"]);
                    }

                    if (databaseCorrectVersion)
                    {
                        log.ErrorFormat("Database '{0}' version is OK", dsn);
                        return true;
                    }

                    log.Error("**************************************************************************");
                    log.ErrorFormat("Database '{0}' version is too old", dsn);
                    log.ErrorFormat("Database required version {0}.{1}.{2}.{3}", requiredMajor, requiredMinor, requiredBranch, requiredBuild);
                    log.ErrorFormat("Database current version {0}.{1}.{2}.{3}", Major, Minor, Branch, Build);
                    log.Error("**************************************************************************");

                    errorMessage = BuildDatabaseMismatchErrorMessage(requiredMajor, requiredMinor, requiredBranch,
                        requiredBuild, Major, Minor, Branch, Build);
                    return false;
                }
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error checking database version on db '{0}', sql '{1}' - {2}", dsn, sql, ex.Message);
            }
            return false;
        }

        private DatabaseVersionErrorMessage BuildDatabaseMismatchErrorMessage(int requiredMajor, int requiredMinor, int requiredBranch,
            int requiredBuild, int major, int minor, int branch, int build)
        {
            try
            {
                var machineName = Environment.MachineName;
                var entryAssembly = Assembly.GetEntryAssembly();

                var processName = entryAssembly != null ? 
                    System.IO.Path.GetFileNameWithoutExtension(entryAssembly.Location) : 
                    AppDomain.CurrentDomain.FriendlyName;

                var subject = machineName + " running " + processName + " connecting to wrong database version";
                var body = new System.Text.StringBuilder();

                body.AppendLine($"Database '{dsn}' version is too old");
                body.AppendLine($"Database required version {requiredMajor}.{requiredMinor}.{requiredBranch}.{requiredBuild}");
                body.AppendLine($"Database current version {major}.{minor}.{branch}.{build}");

                body.AppendLine($"Machine: {machineName}");
                body.AppendLine($"Process: {processName} ");
                body.AppendLine($"IPAddress: {GetIPAddress()}");

                return new DatabaseVersionErrorMessage(machineName, subject, body.ToString());
            }
            catch (Exception ex)
            {
                log.ErrorFormat("Error sending database version mismatch message: {0}", ex.Message);
            }

            return null;
        }

        private string GetIPAddress()
        {
            System.Net.IPHostEntry host = System.Net.Dns.GetHostEntry(System.Net.Dns.GetHostName());
            foreach (System.Net.IPAddress ip in host.AddressList)
            {
                if (ip.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                {
                    return ip.ToString();
                }

            }
            return "?";
        }
    }
}
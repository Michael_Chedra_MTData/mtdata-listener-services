using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Xml;
using System.Xml.Serialization;
using System.Configuration;
using System.Reflection;

namespace MTData.Common.Database
{
	/// <summary>
	/// This class will allow the easy management of sotred procedure definitions
	/// and their retrieval into valid SqlCommands
	/// </summary>
	public class DatabaseDefinition
	{
		private Hashtable _commands = new Hashtable();

		/// <summary>
		/// Default cosntructor
		/// </summary>
		public DatabaseDefinition()
		{
			
		}

		/// <summary>
		/// Register a command definition.
		/// </summary>
		/// <param name="key"></param>
		/// <param name="command"></param>
		public void RegisterCommand(string key, CommandDefinition command)
		{
			_commands.Add(key, command);
		}

		/// <summary>
		/// Register a command definition.
		/// </summary>
		/// <param name="command"></param>
		public void RegisterCommand(CommandDefinition command)
		{
			_commands.Add(command.Name, command);
		}

		/// <summary>
		/// This procedure will ensure that a single typeMap will be added ot the existing list.
		/// </summary>
		/// <param name="commandKey"></param>
		/// <param name="typeMap"></param>
		public void RegisterCommandTypeMap(string commandKey, TypeMapDefinition typeMap)
		{
			CommandDefinition command = GetCommand(commandKey);
			if (command != null)
			{

				if (command.TypeMaps != null)
				{
					ArrayList newList = new ArrayList();
					for(int loop = 0; loop < command.TypeMaps.Length; loop++)
						newList.Add(command.TypeMaps[loop]);

					newList.Add(typeMap);

					command.TypeMaps = (TypeMapDefinition[])newList.ToArray(typeof(TypeMapDefinition));
				}
				else
					command.TypeMaps = new TypeMapDefinition[]{typeMap};
			}
		}

		/// <summary>
		/// Sets the typemaps for a specified command.
		/// </summary>
		/// <param name="commandKey"></param>
		/// <param name="typeMaps"></param>
		public void RegisterCommandTypeMaps(string commandKey, TypeMapDefinition[] typeMaps)
		{
			CommandDefinition command = GetCommand(commandKey);
			if (command != null)
			{

				if (command.TypeMaps != null)
				{
					ArrayList newList = new ArrayList();
					for(int loop = 0; loop < command.TypeMaps.Length; loop++)
						newList.Add(command.TypeMaps[loop]);

					for(int loop = 0; loop < typeMaps.Length; loop++)
						newList.Add(typeMaps[loop]);

					command.TypeMaps = (TypeMapDefinition[])newList.ToArray(typeof(TypeMapDefinition));
				}
				else
					command.TypeMaps = typeMaps;
			}
		}

		/// <summary>
		/// Return a SqlCommand created from the cached definition.
		/// </summary>
		/// <param name="commandKey"></param>
		/// <returns></returns>
		public CommandDefinition GetCommand(string commandKey)
		{
			CommandDefinition command = null;
			if (_commands.Contains(commandKey))
			{
				command = (CommandDefinition)_commands[commandKey];
			}
			return command;
		}

		/// <summary>
		/// This class will create a SqlCommand given the command definition specified.
		/// </summary>
		/// <param name="definition"></param>
		/// <returns></returns>
		public SqlCommand GetSqlCommand(CommandDefinition definition)
		{
			SqlCommand command = null;
			if (definition != null)
			{
				command = new SqlCommand(definition.StoredProcedure);
				command.CommandType = CommandType.StoredProcedure;
				
				for(int loop = 0; loop < ((definition.Parameters != null)?definition.Parameters.Length:0); loop++)
				{
					FieldDefinition field = definition.Parameters[loop];
					if (field.DefaultValue != null)
					{
						SqlParameter param = command.Parameters.Add(field.Name, field.Type);
						param.Value = field.DefaultValue;
					}					
					else
						command.Parameters.Add(field.Name, field.Type);

				}
			}
			return command;
		}

		public SqlCommand GetSqlCommand(string key)
		{
			return GetSqlCommand(GetCommand(key));
		}

		#region Command Definition

		/// <summary>
		/// This class defines the items that describe a Command.
		/// It also defines the Xml Attributes used to store and retrieve it from an xml definition.
		/// </summary>
		[XmlRoot("Command")]
		public class CommandDefinition
		{
			private string _name = null;
			private string _storedProcedure = null;
			private FieldDefinition[] _parameters = null;
			private TypeMapDefinition[] _typeMaps = null;
			private Hashtable _typeHashMap = new Hashtable();

			/// <summary>
			/// Default constructor
			/// </summary>
			public CommandDefinition()
			{
			
			}
			
			/// <summary>
			/// Constructor allowing the soecification of the name
			/// </summary>
			/// <param name="name"></param>
			/// <param name="storedProcedure"></param>
			public CommandDefinition(string name, string storedProcedure) : this()
			{
				_name = name;
				_storedProcedure = storedProcedure;
			}

			/// <summary>
			/// Constructor allowing the soecification of the name and parameters
			/// </summary>
			/// <param name="name"></param>
			/// <param name="storedProcedure"></param>
			/// <param name="parameters"></param>
			public CommandDefinition(string name, string storedProcedure, FieldDefinition[] parameters) : this(name, storedProcedure)
			{
				_parameters = parameters;
			}

			/// <summary>
			/// Constructor allowing the soecification of the name, parameters and result columns
			/// </summary>
			/// <param name="name"></param>
			/// <param name="storedProcedure"></param>
			/// <param name="parameters"></param>
			public CommandDefinition(string name, string storedProcedure, FieldDefinition[] parameters, TypeMapDefinition[] typeMaps) : this(name, storedProcedure, parameters)
			{
				_typeMaps = typeMaps;
				PopulateTypeHashMap();
			}

			/// <summary>
			/// Create a map for quick identification of the type mapping.
			/// </summary>
			private void PopulateTypeHashMap()
			{
				_typeHashMap.Clear();
				foreach(TypeMapDefinition mapDefn in _typeMaps)
					_typeHashMap.Add(mapDefn.LinkedTypeName, mapDefn);
				
			}

			/// <summary>
			/// Property accessor for the name of the command.
			/// </summary>
			[XmlAttribute(DataType="string", AttributeName="Name")]
			public string Name 
			{
				get{ return _name; }
				set{ _name = value;}
			}

			/// <summary>
			/// Property accessor for the name of the stored procedure.
			/// </summary>
			[XmlAttribute(DataType="string", AttributeName="StoredProcedure")]
			public string StoredProcedure
			{
				get{ return _storedProcedure; }
				set{ _storedProcedure = value;}
			}

			/// <summary>
			/// Property accessor for the Parameters array
			/// </summary>
			[XmlElement("Parameters")]
			public FieldDefinition[] Parameters
			{
				get{ return _parameters; }
				set{ _parameters = value;}
			}

			/// <summary>
			/// This is an array of the type mappings for the command.
			/// </summary>
			[XmlElement("TypeMaps")]
			public TypeMapDefinition[] TypeMaps
			{
				get{ return _typeMaps; }
				set
				{
					_typeMaps = value;
					PopulateTypeHashMap();
				}
			}

			/// <summary>
			/// Allow easy access to the type mappings.
			/// </summary>
			/// <param name="type"></param>
			/// <returns></returns>
			public TypeMapDefinition GetTypeMapForType(string type)
			{
				if (_typeHashMap.Contains(type))
					return (TypeMapDefinition)_typeHashMap[type];
				else
					return null;
			}

			/// <summary>
			/// Returns the type map definition based on the type.
			/// </summary>
			/// <param name="type"></param>
			/// <returns></returns>
			public TypeMapDefinition GetTypeMapForType(Type type)
			{
				return GetTypeMapForType(type.FullName);
			}
		}

		#endregion

		#region Field Definition

		/// <summary>
		/// This class defines each field within a parameter list/column list.
		/// It provides name and type.
		/// </summary>
		[XmlRoot("Field")]
		public class FieldDefinition
		{
			private string _name = null;
			private SqlDbType _type = SqlDbType.Int;
			private object _defaultValue = null;

			/// <summary>
			/// Default constructor
			/// </summary>
			public FieldDefinition()
			{

			}

			/// <summary>
			/// Constructor allowing the specification of name and type
			/// </summary>
			/// <param name="name"></param>
			/// <param name="type"></param>
			public FieldDefinition(string name, SqlDbType type)
			{
				_name = name;
				_type = type;
			}

			/// <summary>
			/// This allows a default value to be specified for the parameter.
			/// </summary>
			/// <param name="name"></param>
			/// <param name="type"></param>
			/// <param name="defaultValue"></param>
			public FieldDefinition(string name, SqlDbType type, object defaultValue) : this(name, type)
			{
				_defaultValue = defaultValue;
			}

			/// <summary>
			/// Property accessor to the field name
			/// </summary>
			[XmlAttribute(DataType="string", AttributeName="Name")]
			public string Name
			{
				get{ return _name; }
				set { _name = value;}
			}

			/// <summary>
			/// Property accessor to the Type field.
			/// </summary>
			[XmlAttribute(AttributeName="Type")]
			public SqlDbType Type
			{
				get{ return _type; }
				set{ _type = value; }
			}

			/// <summary>
			/// This is the default value for the field.
			/// </summary>
			[XmlAttribute(AttributeName="DefaultValue")]
			public object DefaultValue
			{
				get{ return _defaultValue; }
				set{ _defaultValue = value; }
			}

		}

		#endregion

		#region TypeMap definition

		/// <summary>
		/// This class will allow a type to be matched to a stored procedure defintiion.
		/// This will ensure that the mapping of PRoperties to Fields will be ok.
		/// </summary>
		public class TypeMapDefinition
		{
			private string _linkedName = null;
			private Type _linkedType = null;
			private TypeFieldMapDefinition[] _parameterMapping = null;
			private TypeFieldMapDefinition[] _resultMapping = null;

			private Hashtable _parameterHashMap = new Hashtable();
			private Hashtable _resultHashMap = new Hashtable();

			/// <summary>
			/// Prepare the class for use by setting the properties.
			/// </summary>
			/// <param name="type"></param>
			/// <param name="parameterMapping"></param>
			/// <param name="resultMapping"></param>
			public TypeMapDefinition(string linkedType, TypeFieldMapDefinition[] parameterMapping, TypeFieldMapDefinition[] resultMapping)
			{	
				_linkedName = linkedType;
				_linkedType = null;
				_parameterMapping = parameterMapping;
				_resultMapping = resultMapping;
			}

			/// <summary>
			/// Register a specific type against the command.
			/// </summary>
			/// <param name="linkedType"></param>
			/// <param name="parameterMapping"></param>
			/// <param name="resultMapping"></param>
			public TypeMapDefinition(Type linkedType, TypeFieldMapDefinition[] parameterMapping, TypeFieldMapDefinition[] resultMapping)
			{	
				_linkedName = linkedType.FullName;
				_linkedType = linkedType;
				_parameterMapping = parameterMapping;
				_resultMapping = resultMapping;

				PropertyInfo[] properties = _linkedType.GetProperties();
				PopulateHashMap(_parameterHashMap, properties, _parameterMapping);
				PopulateHashMap(_resultHashMap, properties, _resultMapping);

			}

			/// <summary>
			/// Take the properties of the type, and create a map of them for use later.
			/// </summary>
			private void PopulateHashMap(Hashtable hashMap, PropertyInfo[] properties, TypeFieldMapDefinition[] fieldMap)
			{
				hashMap.Clear();
				if ((fieldMap != null) && (fieldMap.Length > 0))
				{
					foreach(PropertyInfo property in properties)
					{
						for(int fieldLoop = 0; fieldLoop < fieldMap.Length; fieldLoop++)
						{
							if (fieldMap[fieldLoop].PropertyName == property.Name)
							{
								hashMap.Add(property, fieldMap[fieldLoop]);
								break;
							}
						}
					}
				}
			}

			/// <summary>
			/// Set the actual type..
			/// </summary>
			/// <param name="type"></param>
			public Type LinkType
			{
				get{ return _linkedType; }
				set			
				{
					if (value.FullName != _linkedName)
						throw new ApplicationException(string.Format("Linked Type '{0}' does not match named type definition '{1}'", value.FullName, _linkedName));

					_linkedType = value;
					PropertyInfo[] properties = value.GetProperties();
					PopulateHashMap(_parameterHashMap, properties, _parameterMapping);
					PopulateHashMap(_resultHashMap, properties, _resultMapping);
				}
			}

			/// <summary>
			/// This is the type that the field map belongs to
			/// </summary>
			public string LinkedTypeName
			{
				get{ return _linkedName; }
				set{ _linkedName = value;}
			}

			/// <summary>
			/// This is the mapping of properties to the relevant procedure parameters
			/// </summary>
			public TypeFieldMapDefinition[] ParameterMapping
			{
				get{ return _parameterMapping; }
				set
				{ 
					_parameterMapping = value;
					if (_linkedType != null)
						PopulateHashMap(_parameterHashMap, _linkedType.GetProperties(), _parameterMapping);
				}
			}

			/// <summary>
			/// This is the mapping of the procedure result fields to the relevant properties.
			/// </summary>
			public TypeFieldMapDefinition[] ResultMapping
			{
				get{ return _resultMapping; }
				set
				{ 
					_resultMapping = value;
					if (_linkedType != null)
						PopulateHashMap(_resultHashMap, _linkedType.GetProperties(), _resultMapping);
				}
			}

			/// <summary>
			/// Returns a list of the properties matched up
			/// </summary>
			/// <returns></returns>
			public ICollection GetParameterProperties()
			{
				return _parameterHashMap.Keys;
			}

			/// <summary>
			/// Return the list of matched properties for the Result.
			/// </summary>
			/// <returns></returns>
			public ICollection GetResultProperties()
			{
				return _resultHashMap.Keys;
			}

			/// <summary>
			/// Return the appropriate Field Map for the property
			/// </summary>
			/// <param name="property"></param>
			/// <returns></returns>
			public TypeFieldMapDefinition GetFieldMapForParameter(PropertyInfo property)
			{
				if (_parameterHashMap.Contains(property))
					return (TypeFieldMapDefinition)_parameterHashMap[property];
				else
					return null;
			}

			/// <summary>
			/// This will return the appropriate map for the result property.
			/// </summary>
			/// <param name="property"></param>
			/// <returns></returns>
			public TypeFieldMapDefinition GetFieldMapForResult(PropertyInfo property)
			{
				if (_resultHashMap.Contains(property))
					return (TypeFieldMapDefinition)_resultHashMap[property];
				else
					return null;
			}
		}

		#endregion

		#region TypeFieldMap Defintiion

		/// <summary>
		/// This defines a mapping between a property and a command field (key parameter, or result column)
		/// </summary>
		public class TypeFieldMapDefinition
		{
			private string _propertyName = "";
			private string _commandField = "";
			private object _defaultValue = null;

			/// <summary>
			/// Prepare the instance for use.
			/// </summary>
			/// <param name="propertyName"></param>
			/// <param name="commandField"></param>
			/// <param name="defaultValue"></param>
			public TypeFieldMapDefinition(string propertyName, string commandField, object defaultValue)
			{
				_propertyName = propertyName;
				_commandField = commandField;
				_defaultValue = defaultValue;
			}

			/// <summary>
			/// Prepare the class without a default value.
			/// </summary>
			/// <param name="propertyName"></param>
			/// <param name="commandField"></param>
			public TypeFieldMapDefinition(string propertyName, string commandField) : this(propertyName, commandField, null)
			{

			}

			/// <summary>
			/// This is the name of the property
			/// </summary>
			public string PropertyName
			{
				get{ return _propertyName;}
				set{ _propertyName = value;}
			}

			/// <summary>
			/// This is the name of the command column
			/// </summary>
			public string CommandField
			{
				get{ return _commandField;}
				set{ _commandField = value;}
			}

			/// <summary>
			/// This is the default value to be used if the reader value is DBNull
			/// </summary>
			public object DefaultValue
			{
				get{ return _defaultValue;}
				set{ _defaultValue = value;}
			}

		}

		#endregion


		#region ConfigurationReader

		/// <summary>
		/// This class is a factory class for reading a database definition from a configuration file.
		/// </summary>
		public class ConfigurationReader : IConfigurationSectionHandler
		{
			public ConfigurationReader()
			{
			}

			#region IConfigurationSectionHandler Members

			/// <summary>
			/// This method will be passed a configuration node, and will process it returning the
			/// appropriate items.
			/// </summary>
			/// <param name="parent"></param>
			/// <param name="configContext"></param>
			/// <param name="section"></param>
			/// <returns></returns>
			public object Create(object parent, object configContext, XmlNode section)
			{
				DatabaseDefinition definition = new DatabaseDefinition();

				XmlNodeList nodes = section.SelectNodes("//Commands/Command");
				for(int loop = 0; loop < nodes.Count; loop++)
				{
					XmlNodeReader reader = new XmlNodeReader(nodes[loop]);
					XmlSerializer serializer = new XmlSerializer(typeof(CommandDefinition));
					CommandDefinition command = (CommandDefinition)serializer.Deserialize(reader);
					definition.RegisterCommand(command.Name, command);
				}

				return definition;
			}

			#endregion
		}

		#endregion
	}
}

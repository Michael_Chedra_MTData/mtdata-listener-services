﻿//-----------------------------------------------------------------------
// <copyright file="DatabaseVersionErrorMessage.cs" company="Mobile Tracking and Data Pty Ltd">
//     Copyright (c) Mobile Tracking and Data Pty Ltd. All rights reserved.
// </copyright>

namespace MTData.Common.Database
{
    /// <summary>
    /// Provide information regarding database version error.
    /// </summary>
    public class DatabaseVersionErrorMessage
    {
        #region Constructors

        /// <summary>
        /// Initializes a new instance of the <see cref="DatabaseVersionErrorMessage" /> class.
        /// </summary>
        /// <param name="machineName">Machine name of the server</param>
        /// <param name="subject">Subject of the error message.</param>
        /// <param name="message">Error message.</param>
        public DatabaseVersionErrorMessage(string machineName, string subject, string message)
        {
            this.MachineName = machineName;
            this.Subject = subject;
            this.Message = message;
        }

        #endregion

        #region Properties

        /// <summary>
        /// Gets or sets machine name of the server.
        /// </summary>
        public string MachineName { get; set; }

        /// <summary>
        /// Gets or sets subject of the error message.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets Error message.
        /// </summary>
        public string Message { get; set; }

        #endregion
    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Collections;

namespace MTData.Common.Database
{
    /// <summary>
    /// This class will provide methods for accessing a database through a database definition.
    /// </summary>
    public class DatabaseAccess : IDisposable
    {
        private string _connectionString = null;

        private SqlConnection _connection = null;
        private SqlTransaction _activeTransaction = null;
        private Hashtable _defaultValues = new Hashtable();

        private int _commandTimeout = 30;
        private int _connectionTimeout = 30;

        /// <summary>
        /// Prepare the class for use.
        /// </summary>
        /// <param name="connectionString"></param>
        public DatabaseAccess(string connectionString) : this(connectionString, 30, 30)
        {

        }

        public DatabaseAccess(string connectionString, int connectionTimeout, int commandTimeout)
        {
            _connectionString = connectionString;
            _connectionTimeout = connectionTimeout;
            _commandTimeout = commandTimeout;

            _defaultValues.Add(typeof(DateTime), DateTime.MinValue);
            _defaultValues.Add(typeof(double), 0);
            _defaultValues.Add(typeof(string), null);
            _defaultValues.Add(typeof(int), 0);
            _defaultValues.Add(typeof(long), 0);
        }

        /// <summary>
        /// Return a connection that is instantiated and open.
        /// NOTE : this is for single threaded operation, as the
        /// one connection is used.
        /// </summary>
        /// <returns></returns>
        private SqlConnection GetActiveConnection()
        {
            if (_connection == null)
                _connection = new SqlConnection(_connectionString);

            if (_connection.State == ConnectionState.Closed)
            {
                //_connection.ConnectionTimeout = _connectionTimeout;
                _connection.Open();
            }

            return _connection;
        }

        /// <summary>
        /// Close the active connection.
        /// </summary>
        private void CloseActiveConnection()
        {
            if (_connection != null)
            {
                if (_activeTransaction != null)
                    RollbackTransaction();

                _connection.Close();
                _connection = null;
            }
        }

        /// <summary>
        /// This method will allow the external world to veriufy that a connection can be made.
        /// </summary>
        public void VerifyConnection()
        {
            GetActiveConnection();
        }

        /// <summary>
        /// Returns the currently active transaction
        /// </summary>
        /// <returns></returns>
        private SqlTransaction GetActiveTransaction()
        {
            return _activeTransaction;
        }

        /// <summary>
        /// Begin a new transaction.
        /// </summary>
        public void BeginTransaction()
        {
            if (_activeTransaction != null)
                throw new ApplicationException("BeginTransaction : A transaction is currently active for this connection");

            _activeTransaction = GetActiveConnection().BeginTransaction();
        }

        /// <summary>
        /// Commit the currently active transaction
        /// </summary>
        public void CommitTransaction()
        {
            if (_activeTransaction == null)
                throw new ApplicationException("CommitTransaction : A transaction is not currently active for this connection");

            _activeTransaction.Commit();
            _activeTransaction = null;
        }

        /// <summary>
        /// Rollback the currently active transaction.
        /// </summary>
        public void RollbackTransaction()
        {
            if (_activeTransaction == null)
                throw new ApplicationException("RollbackTransaction : A transaction is not currently active for this connection");

            try
            {
                _activeTransaction.Rollback();
            }
            catch (Exception ex)
            {
                if (!ex.Message.StartsWith("This SqlTransaction has completed; it is no longer usable."))
                    throw ex;
            }
            _activeTransaction = null;
        }

        #region Persistence By Custom Attribute Decoration

        /// <summary>
        /// Populate the object passed in with the details from the sotrewd procedure specified.
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="keyObject"></param>
        /// <param name="definition"></param>
        /// <param name="commandKey"></param>
        public void ProcessObjectByAttribute(object keyObject, object dataObject, DatabaseDefinition definition, string commandKey)
        {
            //	Retrieve the command.
            SqlCommand command = definition.GetSqlCommand(commandKey);
            if (command == null)
                throw new ApplicationException(string.Format("The command '{0}' does not exist within the Database definition provided", commandKey));

            //	Process the Key fields
            Type type = keyObject.GetType();
            PropertyInfo[] properties = type.GetProperties();
            for (int propertyLoop = 0; propertyLoop < properties.Length; propertyLoop++)
            {
                FieldMapKeyAttribute[] keyAttributes = (FieldMapKeyAttribute[])properties[propertyLoop].GetCustomAttributes(typeof(FieldMapKeyAttribute), true);
                if (keyAttributes != null)
                {
                    for (int attributeLoop = 0; attributeLoop < keyAttributes.Length; attributeLoop++)
                    {
                        if (keyAttributes[attributeLoop].CommandKey == commandKey)
                        {
                            command.Parameters[keyAttributes[attributeLoop].TargetField].Value = properties[propertyLoop].GetValue(keyObject, null);
                            break;
                        }
                    }
                }
            }

            //	Set the connection details.
            command.Connection = GetActiveConnection();
            command.Transaction = GetActiveTransaction();

            if (dataObject != null)
            {
                type = dataObject.GetType();
                properties = type.GetProperties();

                //	Execute the command
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    //	Populate the fields.
                    if (!reader.Read())
                        throw new ApplicationException(string.Format("The stored procedure '{0}' did not return a record'", commandKey));

                    for (int propertyLoop = 0; propertyLoop < properties.Length; propertyLoop++)
                    {
                        FieldMapSourceAttribute[] sourceAttributes = (FieldMapSourceAttribute[])properties[propertyLoop].GetCustomAttributes(typeof(FieldMapSourceAttribute), true);
                        if (sourceAttributes != null)
                        {
                            for (int attributeLoop = 0; attributeLoop < sourceAttributes.Length; attributeLoop++)
                            {
                                if (sourceAttributes[attributeLoop].CommandKey == commandKey)
                                {
                                    object currentValue = reader[sourceAttributes[attributeLoop].SourceField];
                                    if (currentValue == DBNull.Value)
                                    {
                                        //	if the default value is null, leave it at the pre-defined default for the property
                                        if (sourceAttributes[attributeLoop].DefaultValue != null)
                                            properties[propertyLoop].SetValue(dataObject, sourceAttributes[attributeLoop].DefaultValue, null);

                                    }
                                    else
                                        properties[propertyLoop].SetValue(dataObject, currentValue, null);

                                }
                            }
                        }
                    }
                }
                finally
                {
                    reader.Close();
                }
            }
            else
            {
                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// Create a list of objects of the specified type from the command specified.
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="definition"></param>
        /// <param name="commandKey"></param>
        /// <returns></returns>
        public object[] ProcessObjectListByAttribute(Type itemType, object keyObject, DatabaseDefinition definition, string commandKey)
        {
            ArrayList resultList = new ArrayList();

            //	Retrieve the command.
            SqlCommand command = definition.GetSqlCommand(commandKey);

            //	Process the Key fields
            PropertyInfo[] properties = keyObject.GetType().GetProperties();
            for (int propertyLoop = 0; propertyLoop < properties.Length; propertyLoop++)
            {
                FieldMapKeyAttribute[] keyAttributes = (FieldMapKeyAttribute[])properties[propertyLoop].GetCustomAttributes(typeof(FieldMapKeyAttribute), true);
                if (keyAttributes != null)
                {
                    for (int attributeLoop = 0; attributeLoop < keyAttributes.Length; attributeLoop++)
                    {
                        if (keyAttributes[attributeLoop].CommandKey == commandKey)
                        {
                            command.Parameters[keyAttributes[attributeLoop].TargetField].Value = properties[propertyLoop].GetValue(keyObject, null);
                            break;
                        }
                    }
                }
            }

            //	Set the connection details.
            command.Connection = GetActiveConnection();
            command.Transaction = GetActiveTransaction();

            //	Execute the command
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                properties = itemType.GetProperties();

                while (reader.Read())
                {
                    object newObject = itemType.Assembly.CreateInstance(itemType.FullName);
                    resultList.Add(newObject);

                    for (int propertyLoop = 0; propertyLoop < properties.Length; propertyLoop++)
                    {
                        FieldMapSourceAttribute[] sourceAttributes = (FieldMapSourceAttribute[])properties[propertyLoop].GetCustomAttributes(typeof(FieldMapSourceAttribute), true);
                        if (sourceAttributes != null)
                        {
                            for (int attributeLoop = 0; attributeLoop < sourceAttributes.Length; attributeLoop++)
                            {
                                if (sourceAttributes[attributeLoop].CommandKey == commandKey)
                                {
                                    object currentValue = reader[sourceAttributes[attributeLoop].SourceField];
                                    if (currentValue == DBNull.Value)
                                    {
                                        //	Leave at pre-defined default values..
                                        if (sourceAttributes[attributeLoop].DefaultValue != null)
                                            properties[propertyLoop].SetValue(newObject, sourceAttributes[attributeLoop].DefaultValue, null);
                                    }
                                    else
                                        properties[propertyLoop].SetValue(newObject, currentValue, null);
                                }
                            }
                        }
                    }
                }
            }
            finally
            {
                reader.Close();
            }
            return (object[])resultList.ToArray(itemType);
        }

        #region Field Map Key Attribute

        /// <summary>
        /// This attribute allows a field to be attached to a specific stored procedure
        /// key field, to ensure that it will be used only with the correct sotred proc.
        /// Multiple attributes can be specified for the different stored procs in the
        /// DatabaseDefinition.
        /// </summary>
        [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
        public class FieldMapKeyAttribute : Attribute
        {
            private string _commandKey = "";
            private string _targetField = "";

            /// <summary>
            /// This constructor allow sthe specification of details relating to the use
            /// of the field in sotred procedures.
            /// </summary>
            /// <param name="commandKey"></param>
            /// <param name="targetField"></param>
            public FieldMapKeyAttribute(string commandKey, string targetField)
            {
                _commandKey = commandKey;
                _targetField = targetField;
            }

            /// <summary>
            /// This is the key for the stored procedure within the database deifnition.
            /// </summary>
            public string CommandKey
            {
                get { return _commandKey; }
                set { _commandKey = value; }
            }

            /// <summary>
            /// This property allows access ot the name of the ley field in the procedure in question.
            /// </summary>
            public string TargetField
            {
                get { return _targetField; }
                set { _targetField = value; }
            }
        }

        #endregion

        #region Field map Source Attribute

        /// <summary>
        /// This attribute identifies the field map from a sotred procedure output and 
        /// a property of a data object
        /// </summary>
        [AttributeUsage(AttributeTargets.Property, Inherited = true, AllowMultiple = true)]
        public class FieldMapSourceAttribute : Attribute
        {
            private string _commandKey = "";
            private string _sourceField = "";
            private object _defaultValue = null;

            /// <summary>
            /// This identifies the source field in the resultset that identifies the value in
            /// the attributed property.
            /// </summary>
            /// <param name="commandKey">The key for the command being executed</param>
            /// <param name="sourceField">The field from this command that holds the data</param>
            public FieldMapSourceAttribute(string commandKey, string sourceField)
            {
                _commandKey = commandKey;
                _sourceField = sourceField;
            }

            /// <summary>
            /// This identifies the command, field, and the default value to be assigned if 
            /// the result of the reader field enquiry is null.
            /// </summary>
            /// <param name="commandKey"></param>
            /// <param name="sourceField"></param>
            /// <param name="defaultValue"></param>
            public FieldMapSourceAttribute(string commandKey, string sourceField, object defaultValue) : this(commandKey, sourceField)
            {
                _defaultValue = defaultValue;
            }

            /// <summary>
            /// This is the key for the stored procedure within the database deifnition.
            /// </summary>
            public string CommandKey
            {
                get { return _commandKey; }
                set { _commandKey = value; }
            }

            /// <summary>
            /// This is the sourcefield from the stored procedure.
            /// </summary>
            public string SourceField
            {
                get { return _sourceField; }
                set { _sourceField = value; }
            }

            /// <summary>
            /// This is the default value if the value from the stored procedure.
            /// </summary>
            public object DefaultValue
            {
                get { return _defaultValue; }
                set { _defaultValue = value; }
            }

        }


        #endregion

        #endregion

        #region Persistence By Property Name matching Type matching in Command Definition

        /// <summary>
        /// Populate the object passed in with the details from the sotrewd procedure specified.
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="keyObject"></param>
        /// <param name="definition"></param>
        /// <param name="commandKey"></param>
        /// <returns>True if a record was found</returns>
        public bool ProcessObject(object keyObject, object dataObject, DatabaseDefinition definition, string commandKey)
        {
            return ProcessObject(keyObject, dataObject, definition, commandKey, false);
        }

        /// <summary>
        /// This method will allow the system to run without returning a record, and without throwing an exception.
        /// </summary>
        /// <param name="keyObject"></param>
        /// <param name="dataObject"></param>
        /// <param name="definition"></param>
        /// <param name="commandKey"></param>
        /// <param name="supportsNoResult"></param>
        /// <returns></returns>
        public bool ProcessObject(object keyObject, object dataObject, DatabaseDefinition definition, string commandKey, bool supportsNoResult)
        {
            bool result = false;

            //	Retrieve the command.
            DatabaseDefinition.CommandDefinition commandDefinition = definition.GetCommand(commandKey);
            if (commandDefinition == null)
                throw new ApplicationException(string.Format("The command '{0}' does not exist within the Database definition provided", commandKey));

            TypeMapProcessor processor = PrepareTypeMapProcessor(commandDefinition,
                (keyObject != null) ? keyObject.GetType() : null,
                (dataObject != null) ? dataObject.GetType() : null);

            using (SqlCommand command = definition.GetSqlCommand(commandDefinition))
            {
                processor.PopulateKeys(keyObject, command);

                //	Set the connection details.
                command.Connection = GetActiveConnection();
                command.Transaction = GetActiveTransaction();
                command.CommandTimeout = _commandTimeout;

                if (dataObject != null)
                {

                    //	Execute the command
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        //	Populate the fields.
                        if (reader.Read())
                        {
                            processor.PopulateObject(dataObject, reader);
                            result = true;
                        }
                        else
                            if (!supportsNoResult)
                            throw new ApplicationException(string.Format("The stored procedure '{0}' did not return a record'", commandKey));
                    }
                    finally
                    {
                        reader.Close();
                    }
                }
                else
                {
                    command.ExecuteNonQuery();
                }
            }
            return result;
        }

        /// <summary>
        /// Create a list of objects of the specified type from the command specified.
        /// </summary>
        /// <param name="dataObject"></param>
        /// <param name="definition"></param>
        /// <param name="commandKey"></param>
        /// <returns></returns>
        public object[] ProcessObjectList(Type itemType, object keyObject, DatabaseDefinition definition, string commandKey)
        {
            ArrayList resultList = new ArrayList();

            //	Retrieve the command.
            DatabaseDefinition.CommandDefinition commandDefinition = definition.GetCommand(commandKey);
            if (commandDefinition == null)
                throw new ApplicationException(string.Format("The command '{0}' does not exist within the Database definition provided", commandKey));

            TypeMapProcessor processor = PrepareTypeMapProcessor(commandDefinition,
                (keyObject != null) ? keyObject.GetType() : null,
                itemType);

            using (SqlCommand command = definition.GetSqlCommand(commandDefinition))
            {
                if (keyObject != null)
                    processor.PopulateKeys(keyObject, command);

                //	Set the connection details.
                command.Connection = GetActiveConnection();
                command.Transaction = GetActiveTransaction();

                //	Execute the command
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        object newObject = itemType.Assembly.CreateInstance(itemType.FullName);
                        resultList.Add(newObject);

                        processor.PopulateObject(newObject, reader);
                    }
                }

                return (object[])resultList.ToArray(itemType);
            }
        }

        /// <summary>
        /// PRepare the processor for use given the command definition and the objects passed in.
        /// </summary>
        /// <param name="definition"></param>
        /// <param name="keyObject"></param>
        /// <param name="dataObject"></param>
        /// <returns></returns>
        private TypeMapProcessor PrepareTypeMapProcessor(DatabaseDefinition.CommandDefinition definition, Type keyObjectType, Type dataObjectType)
        {
            DatabaseDefinition.TypeMapDefinition keyMap = null;
            DatabaseDefinition.TypeMapDefinition resultMap = null;

            if (keyObjectType != null)
            {
                keyMap = definition.GetTypeMapForType(keyObjectType);
                //if (keyMap == null)
                //	throw new ApplicationException(string.Format("The command '{0}' does not have a type map definition for this Key Object Type : {1}", definition.Name, keyObjectType.FullName));
            }

            if (dataObjectType != null)
            {
                resultMap = definition.GetTypeMapForType(dataObjectType);
                //if (resultMap == null)
                //	throw new ApplicationException(string.Format("The command '{0}' does not have a type map definition for this Data Object Type : {1}", definition.Name, dataObjectType.FullName));
            }

            return new TypeMapProcessor(keyMap, resultMap, _defaultValues);
        }

        /// <summary>
        /// This class will process an object based on the type defintion
        /// </summary>
        public class TypeMapProcessor
        {
            private DatabaseDefinition.TypeMapDefinition _parameterType = null;
            private DatabaseDefinition.TypeMapDefinition _resultType = null;
            private Hashtable _defaultValues = null;

            /// <summary>
            /// Preapre the processor for action
            /// </summary>
            /// <param name="parameterMap"></param>
            /// <param name="resultMap"></param>
            public TypeMapProcessor(DatabaseDefinition.TypeMapDefinition parameterType, DatabaseDefinition.TypeMapDefinition resultType, Hashtable defaultValues)
            {
                _parameterType = parameterType;
                _resultType = resultType;
                _defaultValues = defaultValues;
            }

            /// <summary>
            /// This method will perform the transfer from the SqlDataReader to the 
            /// data object specified.
            /// </summary>
            /// <param name="dataObject"></param>
            /// <param name="reader"></param>
            public void PopulateObject(object dataObject, SqlDataReader reader)
            {
                Type dataType = dataObject.GetType();

                //	get field names..
                Hashtable fields = new Hashtable();
                for (int loop = 0; loop < reader.FieldCount; loop++)
                    fields.Add(reader.GetName(loop), loop);

                IEnumerable properties = null;
                if (_resultType != null)
                {
                    if (dataType.FullName != _resultType.LinkedTypeName)
                        throw new ApplicationException(string.Format("The data Object passed in does not match the definition : dataObject - {0} : definition - {1}", dataType.FullName, _resultType.LinkedTypeName));

                    if (_resultType.LinkType == null)
                        _resultType.LinkType = dataType;
                    properties = _resultType.GetResultProperties();
                }
                else
                    properties = dataType.GetProperties();

                //	for each property name, check the map.. for the field value..
                foreach (PropertyInfo property in properties)
                {
                    DatabaseDefinition.TypeFieldMapDefinition map = null;

                    if (_resultType != null)
                        map = _resultType.GetFieldMapForResult(property);

                    //	If the current reader value is null, then either set it to a default value, or 
                    //	leave it at what it was.
                    object currentValue = null;
                    bool skipProperty = true;
                    if (map == null)
                    {
                        //	find if the property exists in the source reader..
                        if (fields.Contains(property.Name))
                        {
                            currentValue = reader[(int)fields[property.Name]];
                            skipProperty = false;
                        }
                    }
                    else
                    {
                        currentValue = reader[map.CommandField];
                        skipProperty = false;
                    }

                    if (!skipProperty)
                    {
                        if (currentValue == DBNull.Value)
                        {
                            //	Leave at pre-defined default values..
                            if ((map != null) && (map.DefaultValue != null))
                                currentValue = map.DefaultValue;
                            else
                            {
                                if ((_defaultValues != null) && (_defaultValues.Contains(property.PropertyType)))
                                    currentValue = _defaultValues[property.PropertyType];
                                else
                                    skipProperty = true;
                            }
                        }
                    }
                    if (!skipProperty)
                    {
                        try
                        {
                            //	Certain field mappings need modification..
                            if ((property.PropertyType == typeof(System.Double)) &&
                                (currentValue is System.Decimal))
                            {
                                property.SetValue(dataObject, Convert.ToDouble(currentValue), null);
                            }
                            else
                                property.SetValue(dataObject, currentValue, null);
                        }
                        catch (System.ArgumentException ex)
                        {
                            throw new ApplicationException(string.Format("PopulateObject:Error converting from [{0}] for property '{1}' : {2}", currentValue, property.Name, ex.Message));
                        }

                    }
                }
            }

            /// <summary>
            /// This procedure will populate the procedure parameters from the keyObject passed in.
            /// </summary>
            /// <param name="keyObject"></param>
            /// <param name="command"></param>
            public void PopulateKeys(object parameterObject, SqlCommand command)
            {
                Type parameterType = parameterObject.GetType();

                IEnumerable properties = null;

                DatabaseDefinition.TypeMapDefinition parameterMap = _parameterType;
                if (parameterMap != null)
                {
                    if (parameterType.FullName != parameterMap.LinkedTypeName)
                        throw new ApplicationException(string.Format("The parameter Object passed in does not match the definition : parameterObject - {0} : definition - {1}", parameterType.FullName, _parameterType.LinkedTypeName));

                    if (parameterMap.LinkType == null)
                        parameterMap.LinkType = parameterType;

                    properties = _parameterType.GetParameterProperties();
                }
                else
                    properties = parameterType.GetProperties();


                //	Go through the Parameters, and set their values in to the command
                foreach (PropertyInfo property in properties)
                {
                    DatabaseDefinition.TypeFieldMapDefinition map = null;
                    if (parameterMap != null)
                        map = (DatabaseDefinition.TypeFieldMapDefinition)parameterMap.GetFieldMapForParameter(property);

                    object currentValue = property.GetValue(parameterObject, null);
                    if (property.PropertyType == typeof(DateTime))
                    {
                        if (DateTime.MinValue == (DateTime)currentValue)
                            currentValue = DBNull.Value;
                    }
                    try
                    {
                        if (map == null)
                        {
                            //	Find the parameter.
                            for (int paramLoop = 0; paramLoop < command.Parameters.Count; paramLoop++)
                            {
                                if (command.Parameters[paramLoop].ParameterName == "@" + property.Name)
                                {
                                    command.Parameters[paramLoop].Value = currentValue;
                                    break;
                                }
                            }
                        }
                        else
                            command.Parameters[map.CommandField].Value = currentValue;
                    }
                    catch (System.ArgumentException ex)
                    {
                        throw new ApplicationException(string.Format("PopulateKeys:Error converting from [{0}] for property '{1}' : {2}", currentValue, property.Name, ex.Message));
                    }
                }
            }
        }
        #endregion

        #region IDisposable Members

        /// <summary>
        /// Clean up any connections that need closing.
        /// </summary>
        public void Dispose()
        {
            CloseActiveConnection();
        }

        #endregion
    }
}

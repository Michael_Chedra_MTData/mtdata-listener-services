﻿//-----------------------------------------------------------------------
// <copyright file="EmailClient.cs" company="Mobile Tracking and Data Pty Ltd">
//     Copyright (c) Mobile Tracking and Data Pty Ltd. All rights reserved.
// </copyright>

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Email client interface for sending emails.
    /// </summary>
    public interface IEmailClient
    {
        /// <summary>
        /// Sends email message.
        /// </summary>
        /// <param name="toEmailAddress">To email address.</param>
        /// <param name="fromEmailAddress">From email address.</param>
        /// <param name="fromDisplayName">From display name.</param>
        /// <param name="subject">Subject of the email.</param>
        /// <param name="body">Body text of the email.</param>
        /// <returns>On success return true, otherwise false.</returns>
        bool SendEmail(string toEmailAddress, string fromEmailAddress, string fromDisplayName, string subject, string body);
    }
}

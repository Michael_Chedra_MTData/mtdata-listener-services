using System;
using System.Xml;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace MTData.Common.Utilities
{
    public enum RuleConditionProperties
    {
        Speed,
        MaximumSpeed,
        IgnitionActive,
        Input1Active,
        Input2Active,
        IOBoxInput1Active,
        IOBoxInput2Active,
        Output1Active,
        Output2Active,
        CoolantTemperature,
        OilTemp,
        OilPressure,
        GForceFront,
        GForceBack,
        GForceLateral,
        GForceZAxis,
        CANVehicleSpeed,
        SetPointID,
        OverSpeedBreachDuration,
        PreTripIncorrect,
        PostTripIncorrect,
        AreDefect,
        IsStopWork,
        DSSSubtype,
        FatiguePeriod,
        FatigueBreakLength,
        IapVehicleCategory,
        IapNoOfAxles,
        IapTotalMass
    }

    public enum RuleConditionOperators
    {
        Equals,
        NotEquals,
        LessThan,
        LessThanAndEquals,
        GreaterThan,
        GreaterThanAndEquals,
    }

    /// <summary>
    /// Summary description for AlertRuleManager.
    /// </summary>
    public class AlertRuleManager
    {
        //private string PreConfiguredAlertRules = "PreConfiguredAlertRules.xml";
        private XmlDocument doc;
        public static int FleetSettingTemplateType = 1;
        public static int FleetSettingRuleType = 2;

        private static List<TagDetail> tagList;

        public const string DescriptionTag = "{Description}";
        public const string FleetTag = "{Fleet}";
        public const string GroupTag = "{Group}";
        public const string VehicleTag = "{Vehicle}";
        public const string OperatorTag = "{Operator}";
        public const string ThresholdTag = "{Threshold}";
        public const string VehiclePhoneTag = "{VehiclePhone}";
        public const string TypeTag = "{Type}";
        public const string DateTag = "{Date}";
        public const string TimeTag = "{Time}";
        public const string LocationTag = "{Location}";
        public const string SpeedTag = "{Speed}";
        public const string DriverTag = "{Driver}";
        public const string DriverMobileTag = "{DriverMobile}";
        public const string MassDeclarationTag = "{MassDeclaration}";
        public const string VehicleWeightLimitTag = "{VehicleWeightLimit}";
        public const string GPSDateTag = "{GPSDate}";
        public const string GPSTimeTag = "{GPSTime}";
        public const string ServerDateTag = "{ServerDate}";
        public const string ServerTimeTag = "{ServerTime}";
        public const string EcmSpeedTag = "{ECMSpeed}";
        public const string MaxSpeedTag = "{MaxSpeed}";
        public const string DriverPointRuleTag = "{DriverPointRule}";
        public const string LatitudeTag = "{Latitude}";
        public const string LongitudeTag = "{Longitude}";
        public const string HeadingTag = "{Heading}";
        public const string GearTag = "{Gear}";
        public const string RpmTag = "{Rpm}";
        public const string SpeedLimitTag = "{SpeedLimit}";
        public const string BrakingGForceTag = "{Braking G-Force}";
        public const string AccelGForceTag = "{Accel G-Force}";
        public const string LateralGForceTag = "{Lateral G-Force}";
        public const string ZAxisGForceTag = "{Z-Axis G-Force}";
        public const string BrakeHitsTag = "{Brake Hits}";
        public const string BrakeUsageTag = "{Brake Usage}";
        public const string BreachMaxEcmSpeedTag = "{Breach Max ECM Speed}";
        public const string BreachMaxGpsSpeedTag = "{Breach Max GPS Speed}";
        public const string BreachMaxSpeedTimeTag = "{Breach Max Speed Time}";
        public const string BreachDurationTag = "{Breach Duration}";
        public const string VehicleUsageTag = "{Vehicle Usage}";
        public const string CallNumberTag = "{Call Number}";
        public const string MessageTag = "{Message}";
        public const string DefectNameTag = "{Defect Name}";
        public const string DefectStateTag = "{Defect State}";
        public const string DefectCommentTag = "{Defect Comment}";
        public const string AssetNameTag = "{Asset Name}";
        public const string IncidentTypeTag = "{Incident Type}";
        public const string IncidentDescriptionTag = "{Incident Description}";
        public const string TripIncorrectQuestionTag = "{Trip Incorrect Question}";
        public const string TripIncorrectAnswerTag = "{Trip Incorrect Answer}";
        public const string MapViewLocationDateTag = "{Map View Location Date}";
        public const string MapViewLocationHeadingTag = "{Map View Location Heading}";
        public const string NoOfDefectsTag = "{No Of Defects}";
        public const string NoOfNewDefectsTag = "{No Of New Defects}";
        public const string StopWorkTag = "{Stop Work}";
        public const string SJPTripNumberTag = "{Trip Number}";
        public const string SJPConsignorTag = "{Consignor}";
        public const string SJPConsignorAddressTag = "{Consignor Address}";
        public const string SJPConsigneeTag = "{Consignee}";
        public const string SJPConsigneeAddressTag = "{Consignee Address}";
        public const string SJPContractorNameTag = "{Contractor Name}";
        public const string SJPContractorAddressTag = "{Contractor Address}";
        public const string SJPPickupAddressTag = "{Pickup Address}";
        public const string SJPDestinationAddressTag = "{Destination Address}";
        public const string SJPCommentTag = "{Comment}";
        public const string SJPStartTag = "{Start}";
        public const string SJPEndTag = "{End}";
        public const string SJPStatusTag = "{Status}";
        public const string ExcessiveIdleThresholdTag = "{Excessive Idle Threshold}";
        public const string FuelEconomyThresholdTag = "{Fuel Economy Threshold}";
        public const string GForceSustainedThresholdTag = "{GForce Sustained Threshold}";
        public const string HighCoolantTempThresholdTag = "{High Coolant Temp Threshold}";
        public const string HighGForcesThresholdTag = "{High G-Forces Threshold}";
        public const string HighOilPressureThresholdTag = "{High Oil Pressure Threshold}";
        public const string HighOilTempThresholdTag = "{High Oil Temp Threshold}";
        public const string HighRpmThresholdTag = "{High Rpm Threshold}";
        public const string LowCoolantThresholdTag = "{Low Coolant Threshold}";
        public const string LowOilPressureThresholdTag = "{Low Oil Pressure Threshold}";
        public const string WaypointNameTag = "{WayPoint Name}";
        public const string WaypointSpeedLimitTag = "{WayPoint Speed Limit}";
        public const string WaypointTimeLimitTag = "{WayPoint Time Limit}";
        public const string DSSSubtypeTag = "{DSS Subtype}";

        public const string MDVREventReason = "{MDVR Event Reason}";
        public const string MDVREventDate = "{MDVR Event Date}";

        public const string MDVRRequestType = "{MDVR Request Type}";
        public const string MDVRRequestDate = "{MDVR Request Date}";
        public const string MDVRRequestBy = "{MDVR Request By}";
        public const string MDVRRequestContent = "{MDVR Requested Content}";
        public const string MDVRRequestDateRange = "{MDVR Requested Range}";

        public const string MDVRStatusErrorCode = "{MDVR Error Code}";
        public const string MDVRStatusConnected = "{MDVR Connected}";
        public const string MDVRStatusRecording = "{MDVR Recording}";
        public const string MDVRStatusHdd = "{MDVR Hard Disk State}";
        public const string MDVRStatusChannelConnected = "{MDVR Channel Connected}";
        public const string MDVRStatusChannelRecording = "{MDVR Channel Recording}";
        public const string MDVRStatusHddSize = "{MDVR Hard Disk Size}";
        public const string MDVRStatusHddFree = "{MDVR Hard Disk Free}";

        public const string GazettedSpeedLimit = "{Gazetted Speed Limit}";

        public const string HtmlSectionSpeedZone = "{HtmlSection_SpeedZone}";
        public const string HtmlSectionLocation = "{HtmlSection_Location}";
        public const string HtmlSectionForces = "{HtmlSection_Forces}";
        public const string HtmlSectionMapView = "{HtmlSection_MapView}";
        public const string HtmlSectionCustom = "{HtmlSection_Custom}";

        public const string HtmlPartialChartForceLateral = "{HtmlPartial_ChartForceLateral}";
        public const string HtmlPartialChartForceStraight = "{HtmlPartial_ChartForceStraight}";
        public const string HtmlPartialChartForceVertical = "{HtmlPartial_ChartForceVertical}";
        public const string HtmlPartialChartSpeed = "{HtmlPartial_ChartSpeed}";
        public const string HtmlPartialChartAltitude = "{HtmlPartial_ChartAltitude}";
        public const string HtmlPartialCustom = "{HtmlPartial_Custom}";
        public const string HtmlPartialLocationSummary = "{HtmlPartial_LocationSummary}";
        public const string HtmlPartialOverviewMap = "{HtmlPartial_OverviewMap}";
        public const string HtmlPartialOverviewSatellite = "{HtmlPartial_OverviewSatellite}";
        public const string HtmlPartialOverviewStreetview = "{HtmlPartial_OverviewStreetview}";
        public const string HtmlPartialSpeedSummary = "{HtmlPartial_SpeedSummary}";
        public const string HtmlPartialSpeedZones = "{HtmlPartial_SpeedZones}";

        public const string HtmlChartForceLateral = "{Html_ChartForceLateral}";
        public const string HtmlChartForceStraight = "{Html_ChartForceStraight}";
        public const string HtmlChartForceVertical = "{Html_ChartForceVertical}";
        public const string HtmlChartSpeed = "{Html_ChartSpeed}";
        public const string HtmlChartAltitude = "{Html_ChartAltitude}";
        public const string HtmlCustom = "{Html_Custom}";
        public const string HtmlOverviewMapTag = "{Html_OverviewMap}";
        public const string HtmlOverviewSatellite = "{Html_OverviewSatellite}";
        public const string HtmlOverviewStreetview = "{Html_OverviewStreetview}";
        public const string HtmlSpeedZones = "{Html_SpeedZones}";

        public const string FatiguePeriodType = "{PeriodType}";
        public const string FatigueBreakDueTime = "{BreakDueTime}";
        public const string FatigueBreakDueIn = "{BreakDueIn}";
        public const string FatigueBreakLength = "{BreakLength}";

        public const string IapCommentCode = "{CommentCode}";
        public const string IapComment = "{Comment}";
        public const string IapVehicleCategory = "{VehicleCategory}";
        public const string IapNoOfAxles = "{NoOfAxles}";
        public const string IapTotalMass = "{TotalMass}";

        public AlertRuleManager(byte[] compressedXml)
        {
            lock (_docSyncRoot)
            {
                this.doc = this.GunzipXmlDocument(compressedXml);
            }
        }

        public AlertRuleManager(XmlDocument doc)
        {
            lock (_docSyncRoot)
            {
                this.doc = doc;
            }
        }

        public AlertRuleManager()
        {
        }

        public static List<TagDetail> TagDetailList
        {
            get
            {
                if (tagList == null)
                {
                    LoadTagDetail();
                }

                return tagList;
            }
        }

        public static Hashtable TagList
        {
            get
            {
                Hashtable tags = new Hashtable();

                if (tagList == null)
                {
                    LoadTagDetail();
                }

                foreach (TagDetail item in tagList)
                {
                    tags.Add(item.Tag, item.Description);
                }

                return tags;
            }
        }

        /// <summary>
        /// Check whether the tag can be used for the supplied tag type and reason id.
        /// </summary>
        /// <param name="tag">Tag placeholder to replace</param>
        /// <param name="tagType">The type of tag in use</param>
        /// <param name="reasonId">The reason id of the event</param>
        /// <returns>True if the tag can be replaced and false if it cannot be replaced</returns>
        public static bool IsValidTag(string tag, TagDetail.TagTypes tagType, int reasonId)
        {
            var filter = TagDetailList.Where(x => x.Tag.Equals(tag)
                                                  && x.TagType == tagType
                                                  && (!x.IncludedThresholds.Any() || x.IncludedThresholds.Contains(reasonId))
                                                  && (!x.ExcludedThresholds.Any() || !x.ExcludedThresholds.Contains(reasonId)));

            return filter.Any();
        }

        /// <summary>
        /// Define the tags that are publicly exposed
        /// </summary>
        /// <returns>Return the tag name, description, title label and whether it should be selected by default. The items are returned in the order they should be displayed.</returns>
        private static void LoadTagDetail()
        {
            tagList = new List<TagDetail>();

            // 32 = DSS Fatigue, 288 = DSS Distraction, 544 = DSS Overspeed, 800 = DSS Acceleration, 1056 = DSS Operator, 1312 = DSS FOV Exception
            List<int> dss = new List<int> { 32, 288, 544, 800, 1056, 1312 };

            // 26405 = MDVR Event
            List<int> dvrEvent = new List<int> { 26405 };

            // 29221 = MDVR Request
            List<int> dvrRequest = new List<int> { 29221 };

            // 26917 = MDVR Status, 27173 = MDVR Error, 27429 = MDVR Error - Hard Disk Status, 27685 = MDVR Error - Hard Disk Recording, 27941 = MDVR Error - Camera Connection, 28197 = MDVR Error - Connection, 
            // 28453 = MDVR Error - Data Usage Limit, 28709 = MDVR Error - Upload Attempt Limit, 28965 = MDVR Error - Incompatible Firmware
            List<int> dvrStatus = new List<int> { 26917, 27173, 27429, 27685, 27941, 28197, 28453, 28709, 28965 };

            // Join all the server generated reasons into those that should be removed from normal tracking events
            List<int> serverGenerated = dss.Concat(dvrEvent).Concat(dvrRequest).Concat(dvrStatus).ToList();

            // Define the selected items first in order of precedence and grouping
            tagList.Add(new TagDetail(DescriptionTag, "The name of the reason", "Description", true, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(FleetTag, "The fleet's name", "Fleet", true, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(GroupTag, "The group's name", "Group", true, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(VehicleTag, "The vehicle's name", "", true, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(DriverTag, "The driver's name", "Driver", true, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(DriverTag, "The driver's name", "Driver", true, TagDetail.TagTypes.Logistics));
            tagList.Add(new TagDetail(DriverTag, "The driver's name", "Driver", true, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(DateTag, "The date", "Date", true, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(TimeTag, "The time", "Time", true, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(LocationTag, "The place where the event occurred", "Location", true, TagDetail.TagTypes.Tracking));
            tagList.Add(new TagDetail(SpeedTag, "Speed in km/h", "Speed", true, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(MaxSpeedTag, "Max speed in km/h", "Max Speed", true, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(SpeedLimitTag, "The speed limit in km/h where the event occurred", "Speed Limit", true, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(GazettedSpeedLimit, "The gazetted speed limit in km/h where the event occurred", "Gazetted Speed Limit", true, TagDetail.TagTypes.Tracking, null, serverGenerated));

            // Define the non selected items order of precedence and grouping
            // Threshold values should match the int values of the reason e.g TagType of Logistics and threshold is 25 = IncidentRaised
            tagList.Add(new TagDetail(VehiclePhoneTag, "The vehicle's phone number", "Vehicle Phone Number", false, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(LatitudeTag, "The vehicle's latitude", "Latitude", false, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(LongitudeTag, "The vehicle's longitude", "Longitude", false, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(EcmSpeedTag, "ECM speed in km/h", "ECM Speed", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(GearTag, "The vehicle's gear", "Gear", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(RpmTag, "The vehicle's rpm", "RPM", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(MassDeclarationTag, "The vehicle's mass declaration (tonnes)", "Mass Declaration", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(VehicleWeightLimitTag, "The vehicle's weight limit (tonnes)", "Weight Limit", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(HeadingTag, "The vehicle's direction of travel", "Heading", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(DriverMobileTag, "The driver's mobile number", "Driver Mobile Number", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(DriverPointRuleTag, "Driver point rule", "Driver Point Rule", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(GPSDateTag, "The gps date", "GPS Date", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(GPSDateTag, "The gps date", "GPS Date", false, TagDetail.TagTypes.Logistics));
            tagList.Add(new TagDetail(GPSDateTag, "The gps date", "GPS Date", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(GPSTimeTag, "The gps time", "GPS Time", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(GPSTimeTag, "The gps time", "GPS Time", false, TagDetail.TagTypes.Logistics));
            tagList.Add(new TagDetail(GPSTimeTag, "The gps time", "GPS Time", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(ServerDateTag, "The server date", "Server Date", false, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(ServerTimeTag, "The server time", "Server Time", false, TagDetail.TagTypes.All));
            tagList.Add(new TagDetail(BrakingGForceTag, "The vehicle's braking G-Force", "Braking G-Force", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(AccelGForceTag, "The vehicle's accelerating G-Force", "Accel G-Force", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(LateralGForceTag, "The vehicle's lateral G-Force", "Lateral G-Force", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(ZAxisGForceTag, "The vehicle's vertical G-Force", "Vertical G-Force", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(BrakeHitsTag, "The number of brake applications", "Brake Hits", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(BrakeUsageTag, "The number of seconds the brake has been applied", "Brake Usage", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(BreachMaxEcmSpeedTag, "Breach max ecm speed in km/h", "Breach Max ECM Speed", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(BreachMaxGpsSpeedTag, "Breach max gps speed in km/h", "Breach Max GPS Speed", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(BreachMaxSpeedTimeTag, "Breach max speed time", "Breach Max Speed Time", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(BreachDurationTag, "Breach duration in seconds", "Breach Duration", false, TagDetail.TagTypes.Tracking, null, serverGenerated));

            // 3 = VehicleUsageOn, 9 = VehicleUsageOff (Threshold values are derived from the NonJobUpdateType enumeration)
            tagList.Add(new TagDetail(VehicleUsageTag, "The vehicle usage selected", "Vehicle Usage", false, TagDetail.TagTypes.Logistics, new List<int> { 3, 9 }));

            // 4 = PhoneCallIncoming, 13 = PhoneCallOutgoing, 14 = PhoneCallOutgoing3rdParty, 15 = PhoneCallSmsSent, 16 = PhoneCallSmsReceived
            tagList.Add(new TagDetail(CallNumberTag, "The phone number call to/from", "Call Number", false, TagDetail.TagTypes.Logistics, new List<int> { 4, 13, 14, 15, 16 }));

            // 20 = DefectRaised, 21 = DefectNote, 22 = DefectAssessment, 23 = DefectWorkOrder, 24 = DefectClosed
            tagList.Add(new TagDetail(MessageTag, "The message sent/received", "Message", false, TagDetail.TagTypes.Logistics, new List<int> { 20, 21, 22, 23, 24 }));
            tagList.Add(new TagDetail(DefectNameTag, "The name of the defect", "Defect Name", false, TagDetail.TagTypes.Logistics, new List<int> { 20, 21, 22, 23, 24 }));
            tagList.Add(new TagDetail(DefectStateTag, "The defect's current state", "Defect State", false, TagDetail.TagTypes.Logistics, new List<int> { 20, 21, 22, 23, 24 }));
            tagList.Add(new TagDetail(DefectCommentTag, "The defect's comment", "Defect Comment", false, TagDetail.TagTypes.Logistics, new List<int> { 20, 21, 22, 23, 24 }));
            tagList.Add(new TagDetail(AssetNameTag, "The Asset name", "Asset Name", false, TagDetail.TagTypes.Logistics, new List<int> { 20, 21, 22, 23, 24 }));

            // 25 = IncidentRaised, 26 = IncidentNote, 27 = IncidentClosed
            tagList.Add(new TagDetail(IncidentTypeTag, "The name of the incident", "Incident Type", false, TagDetail.TagTypes.Logistics, new List<int> { 25, 26, 27 }));
            tagList.Add(new TagDetail(IncidentDescriptionTag, "The incident's description", "Incident Description", false, TagDetail.TagTypes.Logistics, new List<int> { 25, 26, 27 }));

            // 6 = PreTrip, 7 = PostTrip, 29 = Inspection
            tagList.Add(new TagDetail(TripIncorrectQuestionTag, "The question the driver got wrong", "Trip Incorrect Question", false, TagDetail.TagTypes.Logistics, new List<int> { 6, 7, 29 }));
            tagList.Add(new TagDetail(TripIncorrectAnswerTag, "The answer the driver gave", "Trip Incorrect Answer", false, TagDetail.TagTypes.Logistics, new List<int> { 6, 7, 29 }));

            // 29 = Inspection
            tagList.Add(new TagDetail(NoOfDefectsTag, "The inspection's number of defects", "No Of Defects", false, TagDetail.TagTypes.Logistics, new List<int> { 29 }));
            tagList.Add(new TagDetail(NoOfNewDefectsTag, "The inspection's number of new defects", "No Of New Defects", false, TagDetail.TagTypes.Logistics, new List<int> { 29 }));
            tagList.Add(new TagDetail(StopWorkTag, "Any stop work", "Stop Work", false, TagDetail.TagTypes.Logistics, new List<int> { 29 }));

            tagList.Add(new TagDetail(SJPTripNumberTag, "The SJP's trip number", "SJP Trip Number", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPConsignorTag, "The SJP's consignor name", "SJP Consignor", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPConsignorAddressTag, "The SJP's consignor address", "SJP Consignor Address", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPConsigneeTag, "The SJP's consignee name", "SJP Consignee", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPConsigneeAddressTag, "The SJP's consignee address", "SJP Consignee Address", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPContractorNameTag, "The SJP's contractor name", "SJP Contractor Name", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPContractorAddressTag, "The SJP's contractor address", "SJP Contractor Address", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPPickupAddressTag, "The SJP's pickup address", "SJP Pickup Address", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPDestinationAddressTag, "The SJP's destination address", "SJP Destination Address", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPCommentTag, "The SJP's comment", "SJP Comment", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPStartTag, "The SJP's start date time", "SJP Start", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPEndTag, "The SJP's end", "SJP End", false, TagDetail.TagTypes.Sjp));
            tagList.Add(new TagDetail(SJPStatusTag, "The SJP's status", "SJP Status", false, TagDetail.TagTypes.Sjp));

            tagList.Add(new TagDetail(ExcessiveIdleThresholdTag, "Threshold value for excessive idle alert", "Excessive Idle Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(FuelEconomyThresholdTag, "Threshold value for fuel economy alert", "Fuel Economy Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(GForceSustainedThresholdTag, "Threshold value for sustained g-force", "G-Force Sustained Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(HighCoolantTempThresholdTag, "Threshold value for high coolant temperature", "High Coolant Temp Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(HighGForcesThresholdTag, "Threshold value for high g-force", "High G-Force Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(HighOilPressureThresholdTag, "Threshold value for high oil pressure", "High Oil Pressure Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(HighOilTempThresholdTag, "Threshold value for high oil temperature", "High Oil Temp Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(HighRpmThresholdTag, "Threshold value for high rpm", "High RPM Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(LowCoolantThresholdTag, "Threshold value for low coolant", "Low Coolant Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(LowOilPressureThresholdTag, "Threshold value for low oil pressure", "Low Oil Pressure Threshold", false, TagDetail.TagTypes.Tracking, null, serverGenerated));

            tagList.Add(new TagDetail(WaypointNameTag, "Waypoint name", "Waypoint Name", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(WaypointSpeedLimitTag, "Threshold value for waypoint speed limit", "Waypoint Speed Limit", false, TagDetail.TagTypes.Tracking, null, serverGenerated));
            tagList.Add(new TagDetail(WaypointTimeLimitTag, "Threshold value for waypoint time limit", "Waypoint Time Limit", false, TagDetail.TagTypes.Tracking, null, serverGenerated));

            // 288 = DSS Distraction
            tagList.Add(new TagDetail(DSSSubtypeTag, "DSS subtype", "DSS Subtype", false, TagDetail.TagTypes.Tracking, new List<int> { 288 }));

            // 26405 = MDVR Event
            tagList.Add(new TagDetail(MDVREventReason, "MDVR event reason", "MDVR Event Reason", false, TagDetail.TagTypes.Tracking, dvrEvent));
            tagList.Add(new TagDetail(MDVREventDate, "MDVR event date", "MDVR Event Date", false, TagDetail.TagTypes.Tracking, dvrEvent));

            // 29221 = MDVR Request
            tagList.Add(new TagDetail(MDVRRequestType, "MDVR request type", "MDVR Request Type", false, TagDetail.TagTypes.Tracking, dvrRequest));
            tagList.Add(new TagDetail(MDVRRequestDate, "MDVR request date", "MDVR Request Date", false, TagDetail.TagTypes.Tracking, dvrRequest));
            tagList.Add(new TagDetail(MDVRRequestBy, "MDVR request by", "MDVR Request By", false, TagDetail.TagTypes.Tracking, dvrRequest));
            tagList.Add(new TagDetail(MDVRRequestContent, "MDVR requested content", "MDVR Requested Content", false, TagDetail.TagTypes.Tracking, dvrRequest));
            tagList.Add(new TagDetail(MDVRRequestDateRange, "MDVR requested date range", "MDVR Requested Date Range", false, TagDetail.TagTypes.Tracking, dvrRequest));

            // 26917 = MDVR Status, 27173 = MDVR Error, 27429 = MDVR Error - Hard Disk Status, 27685 = MDVR Error - Hard Disk Recording, 27941 = MDVR Error - Camera Connection, 28197 = MDVR Error - Connection,
            // 28453 = MDVR Error - Data Usage Limit, 28709 = MDVR Error - Upload Attempt Limit, 28965 = MDVR Error - Incompatible Firmware
            tagList.Add(new TagDetail(MDVRStatusErrorCode, "MDVR error code", "MDVR Error Code", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusConnected, "MDVR connected", "MDVR Connected", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusRecording, "MDVR recording", "MDVR Recording", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusHdd, "MDVR hard disk state", "MDVR Hard Disk State", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusChannelConnected, "MDVR channel connected", "MDVR Channel Connected", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusChannelRecording, "MDVR channel recording", "MDVR Channel Recording", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusHddSize, "MDVR hard disk size", "MDVR Hard Disk Size", false, TagDetail.TagTypes.Tracking, dvrStatus));
            tagList.Add(new TagDetail(MDVRStatusHddFree, "MDVR hard disk free", "MDVR Hard Disk Free", false, TagDetail.TagTypes.Tracking, dvrStatus));

            tagList.Add(new TagDetail(FatigueBreakDueTime, "Fatigue break due time", "Fatigue Break Due Time", false, TagDetail.TagTypes.Logistics, new List<int> { 33 }));
            tagList.Add(new TagDetail(FatigueBreakDueIn, "Fatigue break due In", "Fatigue Break Due In", false, TagDetail.TagTypes.Logistics, new List<int> { 33 }));
            tagList.Add(new TagDetail(FatigueBreakLength, "Fatigue break length", "Fatigue Break Length", false, TagDetail.TagTypes.Logistics, new List<int> { 33 }));
            tagList.Add(new TagDetail(FatiguePeriodType, "Fatigue period type", "Fatigue Period Type", false, TagDetail.TagTypes.Logistics, new List<int> { 33 }));

            tagList.Add(new TagDetail(IapCommentCode, "IAP comment code", "IAP Comment Code", false, TagDetail.TagTypes.Logistics, new List<int> { 34 }));
            tagList.Add(new TagDetail(IapComment, "IAP comment", "IAP Comment", false, TagDetail.TagTypes.Logistics, new List<int> { 34 }));

            tagList.Add(new TagDetail(IapVehicleCategory, "IAP vehicle category", "IAP Vehicle Category", false, TagDetail.TagTypes.Logistics, new List<int> { 35 }));
            tagList.Add(new TagDetail(IapNoOfAxles, "IAP no of axles", "IAP No Of Axles", false, TagDetail.TagTypes.Logistics, new List<int> { 35 }));
            tagList.Add(new TagDetail(IapTotalMass, "IAP total mass", "IAP total mass", false, TagDetail.TagTypes.Logistics, new List<int> { 35 }));



            // Removed from the public list of tags. Contains internal information that shouldn't be included in the email
            //tagList.Add(new TagDetail(OperatorTag, "Operation used to match value (EG: > < ==", "Operator", false));
            //tagList.Add(new TagDetail(ThresholdTag, "Value rule is matching on", "Threshold", false));
            //tagList.Add(new TagDetail(TypeTag, "Rule type/description", "Rule Type", false));
        }

        public void SaveDoc(int fleetId)
        {
            string path = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetModules()[0].FullyQualifiedName);
            lock (_docSyncRoot)
            {
                doc.Save(path + "\\" + fleetId + ".xml");
            }
        }

        public void LoadPreconfiguredRules()
        {
            try
            {
            }
            catch (Exception ex) { throw new Exception("Failed to load Preconfigured Rules " + ex.Message); }
        }

        public byte[] CompressedXmlDocumentRules
        {
            get
            {
                byte[] bData = null;
                lock (_docSyncRoot)
                {
                    bData = this.GzipXmlDocument(this.doc);
                }
                return bData;
            }
        }

        public XmlDocument XmlDocumentRules
        {
            get
            {
                XmlDocument oRet = null;
                lock (_docSyncRoot)
                {
                    oRet = doc;
                }
                return oRet;
            }
        }

        public XmlNode SelectSingleNode(string xPath)
        {
            XmlNode oRet = null;
            if (doc != null)
                lock (_docSyncRoot)
                    oRet = doc.SelectSingleNode(xPath);
            return oRet;
        }

        public XmlNodeList SelectNodes(string xPath)
        {
            XmlNodeList oRet = null;
            if (doc != null)
                lock (_docSyncRoot)
                    oRet = doc.SelectNodes(xPath);
            return oRet;
        }

        public XmlNode ImportNode(XmlNode oXmlNode, bool bDeep)
        {

            XmlNode oRet = null;
            if (doc != null)
                lock (_docSyncRoot)
                    oRet = doc.ImportNode(oXmlNode, bDeep);
            return oRet;
        }

        //		public void AddRule(XmlNode n)
        //		{
        //			XmlNode settings = this.doc .SelectSingleNode("//./Settings");
        //		
        //			this.doc.ImportNode (n, true);
        //
        //			//this.doc .AppendChild (this.doc .ImportNode(n, true));
        //		}

        public ArrayList Templates
        {
            get
            {
                ArrayList templates = new ArrayList();

                XmlNodeList list = this.SelectNodes("//./Template");

                foreach (XmlNode n in list)
                {
                    string fn = n.SelectSingleNode("Name").InnerText;
                    if (fn.Equals("FleetID"))
                        continue;
                    Rule r = new Rule(n);
                    if (r.IsLink())
                        continue;
                    templates.Add(r);
                }

                return templates;
            }
        }

        public List<Rule> TemplatesList
        {
            get
            {
                List<Rule> templates = new List<Rule>();

                XmlNodeList list = this.SelectNodes("//./Template");
                foreach (XmlNode n in list)
                {
                    string fn = n.SelectSingleNode("Name").InnerText;
                    if (fn.Equals("FleetID"))
                        continue;
                    Rule r = new Rule(n);
                    if (r.IsLink())
                        continue;
                    templates.Add(r);
                }
                return templates;
            }
        }

        public string GetNextID()
        {
            int id = 0;
            XmlNodeList list = this.SelectNodes("//./Template");
            foreach (XmlNode n in list)
            {
                try
                {
                    int newid = int.Parse(n.SelectSingleNode("ID").InnerText);
                    if (newid > id)
                        id = newid;
                }
                catch
                { // id is a fixed string in this case
                }
            }
            int rid = (id + 1);
            return rid.ToString();
        }

        public void AddNewRule(Rule r)
        {
            Rule existing = this.GetRuleByID(r.ID);
            if (existing == null) // new rule
            {
                XmlNode settingNode = this.SelectSingleNode("//./Settings");
                try
                {
                    int.Parse(r.ID);
                    r.ID = this.GetNextID();
                }
                catch { } //Leave id as the fixed string
                settingNode.AppendChild(settingNode.OwnerDocument.ImportNode(r.Thresholds.Clone(), true));
            }
            else // existing update rule
            {
                existing.Thresholds.InnerXml = this.ImportNode(r.Thresholds.Clone(), true).InnerXml;
            }
        }

        public Rule GetRuleByID(string id)
        {
            XmlNode nextRule = this.SelectSingleNode("//./Template[ID='" + id + "']");
            if (nextRule == null)
                return null;
            return new Rule(nextRule);
        }

        public void DeleteRuleByID(string id)
        {
            XmlNode settingNode = this.SelectSingleNode("//./Settings");
            settingNode.RemoveChild(this.SelectSingleNode("//./Settings/Template[ID='" + id + "']"));
        }

        public void DeleteUnusedChainLinks()
        {
            XmlNodeList list = this.SelectNodes("//./Template[@ChainLink='true']");
            foreach (XmlNode n in list)
            {
                Rule r = new Rule(n);
                XmlNodeList linkedToList = this.SelectNodes("//./Template/Linked[To='" + r.ID + "']");
                if (linkedToList == null || linkedToList.Count == 0)
                    DeleteRuleByID(r.ID);
            }
        }

        public void DeleteAllTemplates()
        {
            XmlNodeList list = this.SelectNodes("//./Template");
            foreach (XmlNode n in list)
            {
                try
                {
                    doc.DocumentElement.RemoveChild(n);
                }
                catch (Exception)
                {
                }
            }
        }

        private object _docSyncRoot = new object();
        public ArrayList Rules
        {
            get
            {
                ArrayList rules = new ArrayList();
                lock (_docSyncRoot)
                {
                    XmlNodeList list = this.SelectNodes("//./Template");
                    foreach (XmlNode n in list)
                    {
                        string fn = n.SelectSingleNode("Name").InnerText;
                        if (fn.Equals("FleetID"))
                            continue;
                        Rule r = new Rule(n);
                        rules.Add(r);
                    }
                }
                return rules;
            }
        }

        public List<Rule> RulesList
        {
            get
            {
                List<Rule> rules = new List<Rule>();
                lock (_docSyncRoot)
                {
                    XmlNodeList list = this.SelectNodes("//./Template");
                    foreach (XmlNode n in list)
                    {
                        string fn = n.SelectSingleNode("Name").InnerText;
                        if (fn.Equals("FleetID"))
                            continue;

                        rules.Add(new Rule(n));
                    }
                }

                return rules;
            }
        }

        public ArrayList GetFleets()
        {
            try
            {
                XmlNodeList xList = this.SelectNodes("//./Thresholds");
                XmlNode fleetNode = this.SelectSingleNode("//.//Thresholds[Name='FleetID']");
                string fleets = fleetNode.SelectSingleNode("//./Threshold").InnerText;
                char[] sep = new char[1];
                sep[0] = ',';
                return new ArrayList(fleets.Split(sep));
            }
            catch (Exception ex)
            {
                throw new Exception("Failed to get fleets for xml document " + ex.Message);
            }
        }

        public class Rule
        {
            public XmlNode Thresholds;

            public Rule(XmlNode node)
            {
                Thresholds = node;
            }

            public Rule()
            {
                string xml =
@"<Template>
    <ID></ID>
    <FleetID></FleetID>
    <IsActive></IsActive>
    <CustomDescription></CustomDescription>
    <CustomBodyText></CustomBodyText>
    <CustomName></CustomName>
    <Name></Name>
    <Description></Description>
    <Threshold></Threshold>
    <Operator></Operator>
    <Actions></Actions>
    <Linked>
            <To>-1</To>
            <ANDOR>AND</ANDOR>
    </Linked>
</Template>";

                // Note: Each Email will be a member of Template as  <Email value="address@a.com" />
                // Note: Each Condition will be a member of Template as  <Condition Property="" Operator="" Value=""> />
                XmlDocument doc = new XmlDocument();
                doc.LoadXml(xml);
                XmlNode newNode = doc.DocumentElement;

                this.Thresholds = newNode;
            }

            public string ID
            {
                get { return Thresholds.SelectSingleNode("ID").InnerText; }
                set { Thresholds.SelectSingleNode("ID").InnerText = value; }
            }
            public int FleetID
            {
                get { return int.Parse(Thresholds.SelectSingleNode("FleetID").InnerText); }
                set { Thresholds.SelectSingleNode("FleetID").InnerText = value.ToString(); }
            }

            public bool IsActive
            {
                get
                {
                    var node = Thresholds.SelectSingleNode("IsActive");
                    if (node == null)
                    {
                        return true;
                    }

                    bool nodeValue;
                    return !bool.TryParse(node.InnerText, out nodeValue) || nodeValue;
                }
                set
                {
                    SetNodeValue("IsActive", value.ToString());
                }
            }

            public int? GroupID
            {
                get
                {
                    XmlNode node = Thresholds.SelectSingleNode("GroupID");
                    if (node == null)
                    {
                        return null;
                    }
                    int nodeValue = 0;
                    int.TryParse(node.InnerText, out nodeValue);
                    return nodeValue > 0 ? (int?)nodeValue : null;
                }
                set
                {
                    SetNodeValue("GroupID", value.ToString());
                }
            }

            public string CustomDescription
            {
                get { return Thresholds.SelectSingleNode("CustomDescription").InnerText; }
                set { Thresholds.SelectSingleNode("CustomDescription").InnerText = value; }
            }
            public string CustomBodyText
            {
                get { return Thresholds.SelectSingleNode("CustomBodyText").InnerText; }
                set { Thresholds.SelectSingleNode("CustomBodyText").InnerText = value; }
            }
            public string CustomName
            {
                get { return Thresholds.SelectSingleNode("CustomName").InnerText; }
                set { Thresholds.SelectSingleNode("CustomName").InnerText = value; }
            }
            public string Name
            {
                get { return Thresholds.SelectSingleNode("Name").InnerText; }
                set { Thresholds.SelectSingleNode("Name").InnerText = value; }
            }
            public string Description
            {
                get { return Thresholds.SelectSingleNode("Description").InnerText; }
                set { Thresholds.SelectSingleNode("Description").InnerText = value; }
            }
            public string Operator
            {
                get { return Thresholds.SelectSingleNode("Operator").InnerText; }
                set { Thresholds.SelectSingleNode("Operator").InnerText = value; }
            }
            public string Actions
            {
                get { return Thresholds.SelectSingleNode("Actions").InnerText; }
                set { Thresholds.SelectSingleNode("Actions").InnerText = value; }
            }
            public int Threshold
            {
                get { return int.Parse(Thresholds.SelectSingleNode("Threshold").InnerText); }
                set { Thresholds.SelectSingleNode("Threshold").InnerText = value.ToString(); }
            }

            public bool HtmlEmail
            {
                get
                {
                    bool value = false;
                    XmlNode node = Thresholds.SelectSingleNode("HtmlEmail");

                    if (node != null)
                    {
                        bool.TryParse(node.InnerText, out value);
                    }

                    return value;
                }
                set
                {
                    SetNodeValue("HtmlEmail", value.ToString());
                }
            }

            public int HtmlTemplateId
            {
                get
                {
                    int value = 0;
                    XmlNode node = Thresholds.SelectSingleNode("HtmlTemplateId");

                    if (node != null)
                    {
                        int.TryParse(node.InnerText, out value);
                    }

                    return value;
                }
                set
                {
                    SetNodeValue("HtmlTemplateId", value.ToString());
                }
            }

            public string LinkToID
            {
                get { return Thresholds.SelectSingleNode("Linked/To").InnerText; }
                set { Thresholds.SelectSingleNode("Linked/To").InnerText = value; }
            }

            public string ANDOR
            {
                get { return Thresholds.SelectSingleNode("Linked/ANDOR").InnerText; }
                set { Thresholds.SelectSingleNode("Linked/ANDOR").InnerText = value.ToString(); }
            }

            public bool IsLinkIDFixed()
            {
                try
                {
                    int.Parse(LinkToID);
                    return false;
                }
                catch
                {
                    return (LinkToID.Trim().Length > 0);
                }
            }

            public bool IsLink()
            {
                //XmlNodeList list = this.Thresholds.SelectNodes("[@ChainLink]");

                if (this.Thresholds.Attributes.Count > 0)
                {
                    return (this.Thresholds.Attributes["ChainLink"].Value.ToLower().Equals("true"));
                }
                return false;
            }

            public void SetAsLink(bool link)
            {
                XmlAttribute attr = null;
                if (this.Thresholds.Attributes.Count > 0 && this.Thresholds.Attributes["ChainLink"] != null)
                {
                    attr = this.Thresholds.Attributes["ChainLink"];
                    attr.Value = link.ToString();
                }
                else
                {
                    attr = this.Thresholds.OwnerDocument.CreateAttribute("ChainLink");
                    attr.Value = link.ToString();
                    this.Thresholds.Attributes.Append(attr);
                }
            }

            public void DeleteAllEmails()
            {
                XmlNodeList list = this.Thresholds.SelectNodes("Email");
                for (int i = 0; i < list.Count; i++)
                {
                    XmlNode n = (XmlNode)list[i];
                    n.ParentNode.RemoveChild(n);
                }
            }

            public void DeleteEmail(string email)
            {
                XmlNodeList list = this.Thresholds.SelectNodes("Email[@value='" + email + "']");
                for (int i = 0; i < list.Count; i++)
                {
                    XmlNode n = (XmlNode)list[i];
                    n.ParentNode.RemoveChild(n);
                }
            }

            public void AddEmail(string email)
            {
                XmlNodeList list = this.Thresholds.SelectNodes("Email[@value='']");
                XmlNode n = null;
                if (list.Count > 0)
                    n = (XmlNode)list[0];
                else
                    n = this.Thresholds.OwnerDocument.CreateNode(XmlNodeType.Element, "Email", "");
                XmlAttribute attr = n.OwnerDocument.CreateAttribute("value");
                attr.Value = email;
                n.Attributes.Append(attr);
                XmlNode newNode = n.Clone();
                newNode.Attributes.GetNamedItem("value").Value = email;
                this.Thresholds.AppendChild(newNode);
            }

            public List<string> Emails
            {
                get
                {
                    List<string> a = new List<string>();
                    foreach (XmlNode n in EmailNodes)
                    {
                        a.Add(n.Attributes.GetNamedItem("value").Value);
                    }
                    return a;
                }
            }

            public string EmailsStringList
            {
                get
                {
                    return string.Join(",", Emails);
                }
            }

            public XmlNodeList EmailNodes
            {
                get
                {
                    return Thresholds.SelectNodes("Email");
                }
            }

            public override string ToString()
            {
                return this.Description;
            }

            public XmlNodeList ConditionNodes
            {
                get
                {
                    return Thresholds.SelectNodes("Condition");
                }
            }
            public ReadOnlyCollection<RuleCondition> Conditions
            {
                get
                {
                    List<RuleCondition> a = new List<RuleCondition>();
                    foreach (XmlNode n in ConditionNodes)
                    {
                        a.Add(new RuleCondition(n));
                    }
                    return a.AsReadOnly();
                }
            }
            public void DeleteCondition(RuleCondition condition)
            {
                ReadOnlyCollection<RuleCondition> conditions = Conditions;
                foreach (RuleCondition c in conditions)
                {
                    if (c.Equals(condition))
                    {
                        c.DeleteFromParentNode();
                    }
                }
            }

            public RuleCondition AddCondition(RuleConditionProperties property, RuleConditionOperators conditionOperator, object conditionValue)
            {
                //create an new node
                XmlNode n = this.Thresholds.OwnerDocument.CreateNode(XmlNodeType.Element, "Condition", "");
                XmlAttribute attr = n.OwnerDocument.CreateAttribute("Property");
                attr.Value = property.ToString();
                n.Attributes.Append(attr);
                attr = n.OwnerDocument.CreateAttribute("Operator");
                attr.Value = conditionOperator.ToString();
                n.Attributes.Append(attr);
                attr = n.OwnerDocument.CreateAttribute("Value");
                attr.Value = conditionValue.ToString();
                n.Attributes.Append(attr);

                this.Thresholds.AppendChild(n);
                return new RuleCondition(n);
            }

            private void SetNodeValue(string name, string value)
            {
                XmlNode node = Thresholds.SelectSingleNode(name);

                if (node != null)
                {
                    node.InnerText = value;
                }
                else
                {
                    XmlNode newNode = Thresholds.OwnerDocument.CreateNode(XmlNodeType.Element, name, "");
                    newNode.InnerText = value;

                    Thresholds.AppendChild(newNode);
                }
            }
        }

        public XmlDocument GunzipXmlDocument(byte[] b)
        {
            MemoryStream ms = new MemoryStream(b);
            ICSharpCode.SharpZipLib.GZip.GZipInputStream gis = new ICSharpCode.SharpZipLib.GZip.GZipInputStream(ms);
            MemoryStream unzippedMs = new MemoryStream();
            XmlDocument doc = new XmlDocument();
            doc.Load(gis);
            return doc;
        }

        public byte[] GzipXmlDocument(XmlDocument ds)
        {
            MemoryStream ms = new MemoryStream();
            ICSharpCode.SharpZipLib.GZip.GZipOutputStream zipout = new ICSharpCode.SharpZipLib.GZip.GZipOutputStream(ms);
            ds.Save(zipout);//.WriteXml(zipout, XmlWriteMode.WriteSchema);
            zipout.Finish();
            ms.Seek(0, SeekOrigin.Begin);
            return ms.ToArray();
        }

        public class RuleCondition
        {
            private XmlNode _conditionNode = null;

            public RuleCondition(XmlNode node)
            {
                _conditionNode = node;
            }

            public RuleConditionProperties ConditionProperty
            {
                get { return (RuleConditionProperties)Enum.Parse(typeof(RuleConditionProperties), _conditionNode.Attributes["Property"].Value); }
                set { _conditionNode.Attributes["Property"].Value = value.ToString(); }
            }
            public RuleConditionOperators Operator
            {
                get { return (RuleConditionOperators)Enum.Parse(typeof(RuleConditionOperators), _conditionNode.Attributes["Operator"].Value); }
                set { _conditionNode.Attributes["Operator"].Value = value.ToString(); }
            }

            public object PropertyValue
            {
                get
                {
                    if (ConditionProperty == RuleConditionProperties.DSSSubtype)
                    {
                        return _conditionNode.Attributes["Value"].Value;
                    }

                    return double.Parse(_conditionNode.Attributes["Value"].Value);
                }
                set { _conditionNode.Attributes["Value"].Value = value.ToString(); }
            }

            public Type PropertyType
            {
                get
                {
                    if (ConditionProperty == RuleConditionProperties.DSSSubtype)
                    {
                        return typeof(String);
                    }

                    return typeof(double);
                }
            }

            public override string ToString()
            {
                string oper = "=";
                switch (Operator)
                {
                    case RuleConditionOperators.NotEquals:
                        oper = "!=";
                        break;
                    case RuleConditionOperators.LessThan:
                        oper = "<";
                        break;
                    case RuleConditionOperators.LessThanAndEquals:
                        oper = "<=";
                        break;
                    case RuleConditionOperators.GreaterThan:
                        oper = ">";
                        break;
                    case RuleConditionOperators.GreaterThanAndEquals:
                        oper = ">=";
                        break;
                }

                return string.Format("{0} {1} {2}", ConditionProperty, oper, PropertyValue);
            }

            public bool Equals(RuleCondition condition)
            {
                if (condition == null)
                {
                    return false;
                }

                if (ConditionProperty == condition.ConditionProperty && Operator == condition.Operator && PropertyValue.Equals(condition.PropertyValue))
                {
                    return true;
                }

                return false;
            }

            public void DeleteFromParentNode()
            {
                _conditionNode.ParentNode.RemoveChild(_conditionNode);
            }
        }

        public class TagDetail
        {
            public enum TagTypes
            {
                Tracking,
                Logistics,
                All,
                Sjp
            }

            public TagDetail(string tag, string description, string title, bool selected, TagTypes tagType, List<int> includedThresholds = null, List<int> excludedThresholds = null)
            {
                if (string.IsNullOrWhiteSpace(title))
                {
                    title = tag.Replace("{", "").Replace("}", "");
                }

                Tag = tag;
                Description = description;
                Title = title;
                Selected = selected;
                TagType = tagType;
                IncludedThresholds = includedThresholds ?? new List<int>();
                ExcludedThresholds = excludedThresholds ?? new List<int>();
            }

            public string Tag { get; set; }

            public string Description { get; set; }

            public string Title { get; set; }

            public bool Selected { get; set; }

            public TagTypes TagType { get; set; }

            public List<int> IncludedThresholds { get; set; }

            public List<int> ExcludedThresholds { get; set; }
        }

        public class RuleConditionDetail
        {
            public enum ConditionTypes
            {
                Tracking,
                Logistics,
                Sjp
            }

            /// <summary>
            /// Return the list of rule condition properties that are allowed for the supplied condition type
            /// </summary>
            public static List<RuleConditionProperties> GetConditionsByType(ConditionTypes conditionType)
            {
                var list = ((RuleConditionProperties[])Enum.GetValues(typeof(RuleConditionProperties))).ToList();
                var logisticItems = new[]
                {
                    RuleConditionProperties.PreTripIncorrect,
                    RuleConditionProperties.PostTripIncorrect,
                    RuleConditionProperties.AreDefect,
                    RuleConditionProperties.IsStopWork,
                    RuleConditionProperties.FatiguePeriod,
                    RuleConditionProperties.FatigueBreakLength,
                    RuleConditionProperties.IapVehicleCategory,
                    RuleConditionProperties.IapNoOfAxles,
                    RuleConditionProperties.IapTotalMass
                };

                // Filter the list of condition properties by the condition type
                switch (conditionType)
                {
                    case ConditionTypes.Tracking:
                        return list.Where(x => !logisticItems.Contains(x)).ToList();
                    case ConditionTypes.Logistics:
                        return list.Where(x => logisticItems.Contains(x)).ToList();
                }

                return null;
            }
        }
    }
}

using System;

namespace MTData.Common.Utilities
{
	/// <summary>
	/// Summary description for cDateUtility.
	/// </summary>
	public class DateUtility
	{
		public DateUtility()
		{

		}
		public static string GetDateInFormat(DateTime oDateTime, string sFormat)
		{
			string sRet = "";
			string sTemp = "";
			int iHours = 0;
			string sDay = "";
			string sMonth = "";
			string sYear = "";
			string sHour = "";
			string sTwelveHour = "";
			string sMinute = "";
			string sSecond = "";
			string sAMPM = "";

			iHours = oDateTime.Hour;
			if (iHours >= 12)
			{
				sAMPM = "PM";
				if (iHours > 12)
					iHours = iHours - 12;
				sTwelveHour = Convert.ToString(iHours);
			}
			else
			{
				sAMPM = "AM";
				if (iHours == 0)
					iHours = 12;
				sTwelveHour = Convert.ToString(iHours);
			}

			sDay = oDateTime.Day.ToString().PadLeft(2, '0');
			sMonth = oDateTime.Month.ToString().PadLeft(2, '0');
			sYear = oDateTime.Year.ToString().PadLeft(4, '0');
			sHour = oDateTime.Hour.ToString().PadLeft(2, '0');
			sMinute = oDateTime.Minute.ToString().PadLeft(2, '0');
			sSecond = oDateTime.Second.ToString().PadLeft(2, '0');

			sTemp = sFormat.ToUpper();

			switch(sTemp)
			{
				case "HH:MM":
					sRet = sHour + ":" + sMinute;
					break;
				case "HH:MM:SS":
					sRet = sHour + ":" + sMinute + ":" + sSecond;
					break;
				case "HH:MM TT":
					sRet = sTwelveHour + ":" + sMinute + " " + sAMPM;
					break;
				case "HH:MM:SS TT":
					sRet = sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM;
					break;
				case "DD/MM/YYYY":
					sRet = sDay + "/" + sMonth + "/" + sYear;
					break;
				case "MM/DD/YYYY":
					sRet = sMonth + "/" + sDay + "/" + sYear;
					break;
				case "YYYY/MM/DD":
					sRet = sYear + "/" + sMonth + "/" + sDay;
					break;
				case "YYYY/DD/MM":
					sRet = sYear + "/" + sDay + "/" + sMonth;
					break;
				case "MM/YYYY/DD":
					sRet = sMonth + "/" + sYear + "/" + sDay;
					break;
				case "DD/MM/YYYY HH:MM":
					sRet = sDay + "/" + sMonth + "/" + sYear + " " + sHour + ":" + sMinute;
					break;
				case "MM/DD/YYYY HH:MM":
					sRet = sMonth + "/" + sDay + "/" + sYear + " " + sHour + ":" + sMinute;
					break;
				case "YYYY/MM/DD HH:MM":
					sRet = sYear + "/" + sMonth + "/" + sDay + " " + sHour + ":" + sMinute;
					break;
				case "YYYY/DD/MM HH:MM":
					sRet = sYear + "/" + sDay + "/" + sMonth + " " + sHour + ":" + sMinute;
					break;
				case "MM/YYYY/DD HH:MM":
					sRet = sMonth + "/" + sYear + "/" + sDay + " " + sHour + ":" + sMinute;
					break;
				case "DD/MM/YYYY HH:MM:SS":
					sRet = sDay + "/" + sMonth + "/" + sYear + " " + sHour + ":" + sMinute + ":" + sSecond;
					break;
				case "MM/DD/YYYY HH:MM:SS":
					sRet = sMonth + "/" + sDay + "/" + sYear + " " + sHour + ":" + sMinute + ":" + sSecond;
					break;
				case "YYYY/MM/DD HH:MM:SS":
					sRet = sYear + "/" + sMonth + "/" + sDay + " " + sHour + ":" + sMinute + ":" + sSecond;
					break;
				case "YYYY/DD/MM HH:MM:SS":
					sRet = sYear + "/" + sDay + "/" + sMonth + " " + sHour + ":" + sMinute + ":" + sSecond;
					break;
				case "MM/YYYY/DD HH:MM:SS":
					sRet = sMonth + "/" + sYear + "/" + sDay + " " + sHour + ":" + sMinute + ":" + sSecond;
					break;
				case "DD/MM/YYYY HH:MM TT":
					sRet = sDay + "/" + sMonth + "/" + sYear + " " + sTwelveHour + ":" + sMinute + " " + sAMPM;
					break;
				case "MM/DD/YYYY HH:MM TT":
					sRet = sMonth + "/" + sDay + "/" + sYear + " " + sTwelveHour + ":" + sMinute + " " + sAMPM;
					break;
				case "YYYY/MM/DD HH:MM TT":
					sRet = sYear + "/" + sMonth + "/" + sDay + " " + sTwelveHour + ":" + sMinute + " " + sAMPM;
					break;
				case "YYYY/DD/MM HH:MM TT":
					sRet = sYear + "/" + sDay + "/" + sMonth + " " + sTwelveHour + ":" + sMinute + " " + sAMPM;
					break;
				case "MM/YYYY/DD HH:MM TT":
					sRet = sMonth + "/" + sYear + "/" + sDay + " " + sTwelveHour + ":" + sMinute + " " + sAMPM;
					break;
				case "DD/MM/YYYY HH:MM:SS TT":
					sRet = sDay + "/" + sMonth + "/" + sYear + " " + sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM;
					break;
				case "MM/DD/YYYY HH:MM:SS TT":
					sRet = sMonth + "/" + sDay + "/" + sYear + " " + sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM;
					break;
				case "YYYY/MM/DD HH:MM:SS TT":
					sRet = sYear + "/" + sMonth + "/" + sDay + " " + sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM;
					break;
				case "YYYY/DD/MM HH:MM:SS TT":
					sRet = sYear + "/" + sDay + "/" + sMonth + " " + sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM;
					break;
				case "MM/YYYY/DD HH:MM:SS TT":
					sRet = sMonth + "/" + sYear + "/" + sDay + " " + sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM;
					break;
				case "HH:MM DD/MM/YYYY":
					sRet = sHour + ":" + sMinute + " " + sDay + "/" + sMonth + "/" + sYear;
					break;
				case "HH:MM MM/DD/YYYY":
					sRet = sHour + ":" + sMinute + " " + sMonth + "/" + sDay + "/" + sYear;
					break;
				case "HH:MM YYYY/MM/DD":
					sRet = sHour + ":" + sMinute + " " + sYear + "/" + sMonth + "/" + sDay;
					break;
				case "HH:MM YYYY/DD/MM":
					sRet = sHour + ":" + sMinute + " " + sYear + "/" + sDay + "/" + sMonth;
					break;
				case "HH:MM MM/YYYY/DD":
					sRet = sHour + ":" + sMinute + " " + sMonth + "/" + sYear + "/" + sDay;
					break;
				case "HH:MM:SS DD/MM/YYYY":
					sRet = sHour + ":" + sMinute + ":" + sSecond + " " + sDay + "/" + sMonth + "/" + sYear;
					break;
				case "HH:MM:SS MM/DD/YYYY":
					sRet = sHour + ":" + sMinute + ":" + sSecond + " " + sMonth + "/" + sDay + "/" + sYear;
					break;
				case "HH:MM:SS YYYY/MM/DD":
					sRet = sHour + ":" + sMinute + ":" + sSecond + " " + sYear + "/" + sMonth + "/" + sDay;
					break;
				case "HH:MM:SS YYYY/DD/MM":
					sRet = sHour + ":" + sMinute + ":" + sSecond + " " + sYear + "/" + sDay + "/" + sMonth;
					break;
				case "HH:MM:SS MM/YYYY/DD":
					sRet = sHour + ":" + sMinute + ":" + sSecond + " " + sMonth + "/" + sYear + "/" + sDay;
					break;
				case "HH:MM TT DD/MM/YYYY":
					sRet = sTwelveHour + ":" + sMinute + " " + sAMPM + " " + sDay + "/" + sMonth + "/" + sYear;
					break;
				case "HH:MM TT MM/DD/YYYY":
					sRet = sTwelveHour + ":" + sMinute + " " + sAMPM + " " + sMonth + "/" + sDay + "/" + sYear;
					break;
				case "HH:MM TT YYYY/MM/DD":
					sRet = sTwelveHour + ":" + sMinute + " " + sAMPM + " " + sYear + "/" + sMonth + "/" + sDay;
					break;
				case "HH:MM TT YYYY/DD/MM":
					sRet = sTwelveHour + ":" + sMinute + " " + sAMPM + " " + sYear + "/" + sDay + "/" + sMonth;
					break;
				case "HH:MM TT MM/YYYY/DD":
					sRet = sTwelveHour + ":" + sMinute + " " + sAMPM + " " + sMonth + "/" + sYear + "/" + sDay;
					break;
				case "HH:MM:SS TT DD/MM/YYYY":
					sRet = sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM + " " + sDay + "/" + sMonth + "/" + sYear;
					break;
				case "HH:MM:SS TT MM/DD/YYYY":
					sRet = sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM + " " + sMonth + "/" + sDay + "/" + sYear;
					break;
				case "HH:MM:SS TT YYYY/MM/DD":
					sRet = sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM + " " + sYear + "/" + sMonth + "/" + sDay;
					break;
				case "HH:MM:SS TT YYYY/DD/MM":
					sRet = sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM + " " + sYear + "/" + sDay + "/" + sMonth;
					break;
				case "HH:MM:SS TT MM/YYYY/DD":
					sRet = sTwelveHour + ":" + sMinute + ":" + sSecond + " " + sAMPM + " " + sMonth + "/" + sYear + "/" + sDay;
					break;
				default:
					sRet = sDay + "/" + sMonth + "/" + sYear + " " + sHour + ":" + sMinute + ":" + sSecond;
					break;
			}
			return sRet;
		}
	}
}

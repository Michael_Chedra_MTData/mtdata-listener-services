using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Common.Utilities.TreeView
{
    public static class TreeStateManager
    {
        public static LinkedList<TreeNodeInfo<T>> GetTree<T, TKey>(IEnumerable<T> flatList, Func<T, TKey> IdSelector, Func<T, TKey> ParentKeySelector, int level = 0, TKey rootKey = default(TKey), TreeNodeInfo<T> parentNode = null) where T : class
        {
            var q = flatList.Where(n => EqualityComparer<TKey>.Default.Equals(ParentKeySelector(n), rootKey)).Select(n =>
            {
                var treeNodeInfo = new TreeNodeInfo<T>()
                {
                    Value = n,
                    Level = level,
                    Parent = parentNode,
                };

                treeNodeInfo.Children = GetTree(flatList, IdSelector, ParentKeySelector, level + 1, IdSelector(n), treeNodeInfo);
                return treeNodeInfo;
            });

            return new LinkedList<TreeNodeInfo<T>>(q);
        }

        public static IEnumerable<TreeNodeInfo<T>> TraverseTree<T>(IEnumerable<TreeNodeInfo<T>> tree) where T : class
        {
            if (tree != null)
            {
                foreach (var node in tree)
                {
                    yield return node;

                    if (node.Children.Any())
                    {
                        foreach (var child in TraverseTree(node.Children))
                        {
                            yield return child;
                        }
                    }
                }
            }
        }

    }
}
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Common.Utilities.TreeView
{
    public class TreeNodeInfo<T>
        where T : class
    {
        public T Value { get; set; }
        public int Level { get; set; }
        public bool HasChildren { get { return Children == null ? false : Children.Any(); } }
        public TreeNodeInfo<T> Parent { get; set; }
        public LinkedList<TreeNodeInfo<T>> Children { get; set; }

        public override string ToString()
        {
            return string.Format("Level {0}, ChildrenCount{1}, {2}", Level, Children == null ? 0 : Children.Count, Value.ToString());
        }
    }
}
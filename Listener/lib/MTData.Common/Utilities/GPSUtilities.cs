using System;
using Math = System.Math;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// This class provides a number of functions for manipulating GPS data
    /// </summary>
    public class GPSUtilities
    {
        public static int WGS84 = 23;

        public GPSUtilities()
        {

        }

        public class Ellipsoid
        {
            public int id;
            public string ellipsoidName;
            public double EquatorialRadius;
            public double eccentricitySquared;

            public Ellipsoid() { }
            public Ellipsoid(int Id, string name, double radius, double ecc)
            {
                id = Id;
                ellipsoidName = name;
                EquatorialRadius = radius; eccentricitySquared = ecc;
            }
        };


        static Ellipsoid[] ellipsoid = 
		{//  id, Ellipsoid name, Equatorial Radius, square of eccentricity	
			new Ellipsoid( -1, "Placeholder", 0, 0),//placeholder only, To allow array indices to match id numbers
			new Ellipsoid( 1, "Airy", 6377563, 0.00667054),
			new Ellipsoid( 2, "Australian National", 6378160, 0.006694542),
			new Ellipsoid( 3, "Bessel 1841", 6377397, 0.006674372),
			new Ellipsoid( 4, "Bessel 1841 (Nambia) ", 6377484, 0.006674372),
			new Ellipsoid( 5, "Clarke 1866", 6378206, 0.006768658),
			new Ellipsoid( 6, "Clarke 1880", 6378249, 0.006803511),
			new Ellipsoid( 7, "Everest", 6377276, 0.006637847),
			new Ellipsoid( 8, "Fischer 1960 (Mercury) ", 6378166, 0.006693422),
			new Ellipsoid( 9, "Fischer 1968", 6378150, 0.006693422),
			new Ellipsoid( 10, "GRS 1967", 6378160, 0.006694605),
			new Ellipsoid( 11, "GRS 1980", 6378137, 0.00669438),
			new Ellipsoid( 12, "Helmert 1906", 6378200, 0.006693422),
			new Ellipsoid( 13, "Hough", 6378270, 0.00672267),
			new Ellipsoid( 14, "International", 6378388, 0.00672267),
			new Ellipsoid( 15, "Krassovsky", 6378245, 0.006693422),
			new Ellipsoid( 16, "Modified Airy", 6377340, 0.00667054),
			new Ellipsoid( 17, "Modified Everest", 6377304, 0.006637847),
			new Ellipsoid( 18, "Modified Fischer 1960", 6378155, 0.006693422),
			new Ellipsoid( 19, "South American 1969", 6378160, 0.006694542),
			new Ellipsoid( 20, "WGS 60", 6378165, 0.006693422),
			new Ellipsoid( 21, "WGS 66", 6378145, 0.006694542),
			new Ellipsoid( 22, "WGS-72", 6378135, 0.006694318),
			new Ellipsoid( 23, "WGS-84", 6378137, 0.00669438)
		};

        public static void ConvertUTMtoLatLong(int ReferenceEllipsoid, double UTMNorthing,
                                        double UTMEasting, string UTMZone, out double Lat, out double Long)
        {
            int WGS84 = 23;
            ReferenceEllipsoid = WGS84;
            const double rad2deg = 180.0 / Math.PI;

            //double Lat, Long;
            double k0 = 0.9996;
            double a = ellipsoid[ReferenceEllipsoid].EquatorialRadius;
            double eccSquared = ellipsoid[ReferenceEllipsoid].eccentricitySquared;
            double eccPrimeSquared;
            double e1 = (1 - Math.Sqrt(1 - eccSquared)) / (1 + Math.Sqrt(1 - eccSquared));
            double N1, T1, C1, R1, D, M;
            double LongOrigin;
            double mu, phi1, phi1Rad;
            double x, y;

            ulong ZoneNumber;
            string ZoneLetter = "";

            x = UTMEasting - 500000.0; //remove 500,000 meter offset for longitude
            y = UTMNorthing;

            string zoneNum = "";
            foreach (Char c in UTMZone)
            {
                if (Char.IsNumber(c))
                    zoneNum += c.ToString();
                if (Char.IsNumber(c))
                    ZoneLetter += c.ToString();
            }
            ZoneNumber = Convert.ToUInt64(zoneNum);
            if (!ZoneLetter.Equals("N"))
            {
                y -= 10000000.0;//remove 10,000,000 meter offset used for southern hemisphere
            }

            LongOrigin = (ZoneNumber - 1) * 6 - 180 + 3;  //+3 puts origin in middle of zone

            eccPrimeSquared = (eccSquared) / (1 - eccSquared);

            M = y / k0;
            mu = M / (a * (1 - eccSquared / 4 - 3 * eccSquared * eccSquared / 64 - 5 * eccSquared * eccSquared * eccSquared / 256));


            phi1Rad = mu + (3 * e1 / 2 - 27 * e1 * e1 * e1 / 32) * Math.Sin(2 * mu)
                + (21 * e1 * e1 / 16 - 55 * e1 * e1 * e1 * e1 / 32) * Math.Sin(4 * mu)
                + (151 * e1 * e1 * e1 / 96) * Math.Sin(6 * mu);
            phi1 = phi1Rad * rad2deg;

            N1 = a / Math.Sqrt(1 - eccSquared * Math.Sin(phi1Rad) * Math.Sin(phi1Rad));
            T1 = Math.Tan(phi1Rad) * Math.Tan(phi1Rad);
            C1 = eccPrimeSquared * Math.Cos(phi1Rad) * Math.Cos(phi1Rad);
            R1 = a * (1 - eccSquared) / Math.Pow(1 - eccSquared * Math.Sin(phi1Rad) * Math.Sin(phi1Rad), 1.5);
            D = x / (N1 * k0);

            Lat = phi1Rad - (N1 * Math.Tan(phi1Rad) / R1) * (D * D / 2 - (5 + 3 * T1 + 10 * C1 - 4 * C1 * C1 - 9 * eccPrimeSquared) * D * D * D * D / 24
                + (61 + 90 * T1 + 298 * C1 + 45 * T1 * T1 - 252 * eccPrimeSquared - 3 * C1 * C1) * D * D * D * D * D * D / 720);
            Lat = Lat * rad2deg;

            Long = (D - (1 + 2 * T1 + C1) * D * D * D / 6 + (5 - 2 * C1 + 28 * T1 - 3 * C1 * C1 + 8 * eccPrimeSquared + 24 * T1 * T1)
                * D * D * D * D * D / 120) / System.Math.Cos(phi1Rad);
            Long = LongOrigin + Long * rad2deg;
            //return Lat.ToString ()+" "+Long.ToString ();
        }

        /// <summary>
        /// Convert a transmission uint latlong to a double.
        /// </summary>
        /// <param name="latlong"></param>
        /// <returns></returns>
        public static double ConvertToDouble(uint latlong)
        {
            uint temp = 0;

            double result = 0;

            if (latlong >= 0x10000000)
            {
                temp = 0 - latlong;
                result = Convert.ToDouble(temp) / 600000 * -1;
            }
            else
            {
                temp = latlong;
                result = Convert.ToDouble(temp) / 600000;
            }
            return result;
        }

        /// <summary>
        /// Convert a standard LatLong double to an unsigned int for transmission.
        /// </summary>
        /// <param name="latlong"></param>
        /// <returns></returns>
        public static uint ConvertToUInt32(double latlong)
        {
            uint result = 0;

            if (latlong < 0)
            {
                latlong = (0 - latlong) * 600000;
                result = 0 - Convert.ToUInt32(latlong);
            }
            else
            {
                latlong = latlong * 600000;
                result = Convert.ToUInt32(latlong);
            }

            return result;
        }





    }
}

using System;

namespace MTData.Common.Utilities
{
	/// <summary>
	/// This class will allow byte data to be written to the strema in an ordered fashion,
	/// returning a byte array at the end of it.
	/// </summary>
	public class ByteStreamWriter
	{
		public const int BUFFER_INCREMENT = 1024;
		
		private byte[] _buffer = null;
		private int _nextWrite = 0;

		/// <summary>
		/// Prepare the stream with a default size
		/// </summary>
		public ByteStreamWriter() : this(BUFFER_INCREMENT)
		{

		}
		
		/// <summary>
		/// Prepare the stream with an initial size
		/// </summary>
		/// <param name="initialSize"></param>
		public ByteStreamWriter(int initialSize)
		{
			_buffer = new byte[initialSize];
			_nextWrite = 0;
		}

		/// <summary>
		/// Write a single byte to the stream.
		/// </summary>
		/// <param name="value"></param>
		public void WriteByte(byte value)
		{
			EnsureSize(_nextWrite+1);
			_buffer[_nextWrite++] = value;
		}

		/// <summary>
		/// Write a byte array to the buffer.
		/// </summary>
		/// <param name="value"></param>
		public void WriteByteArray(byte[] value)
		{
			EnsureSize(_nextWrite + value.Length);
			Array.Copy(value, 0, _buffer, _nextWrite, value.Length);
			_nextWrite += value.Length;
		}

		/// <summary>
		/// This will write a repeating byte value to the stream
		/// It allows block spare sections to be written easily
		/// </summary>
		/// <param name="value"></param>
		/// <param name="length"></param>
		public void WriteRepeatingByte(byte value, int length)
		{
			EnsureSize(_nextWrite + length);
			for(int loop = _nextWrite; loop < _nextWrite + length; loop++)
				_buffer[loop] = value;
			_nextWrite += length;
		}

		/// <summary>
		/// Write a 4 byte integer lsb, to msb
		/// </summary>
		/// <param name="value"></param>
		public void Write4ByteInteger(int value)
		{
			EnsureSize(_nextWrite + 4);
			byte[] temp = BitConverter.GetBytes(value);
			_buffer[_nextWrite++] = (temp.Length > 0)?temp[0]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 1)?temp[1]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 2)?temp[2]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 3)?temp[3]:(byte)0;
		}

		/// <summary>
		/// This method will write a 4 byte unsigned integer.
		/// </summary>
		/// <param name="value"></param>
		public void Write4ByteUnsignedInteger(uint value)
		{
			EnsureSize(_nextWrite + 4);
			byte[] temp = BitConverter.GetBytes(value);
			_buffer[_nextWrite++] = (temp.Length > 0)?temp[0]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 1)?temp[1]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 2)?temp[2]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 3)?temp[3]:(byte)0;
		}

		/// <summary>
		/// This method will write an integer to the stream as a 2 byte number.
		/// </summary>
		/// <param name="value"></param>
		public void Write2ByteInteger(int value)
		{
			EnsureSize(_nextWrite + 2);
			byte[] temp = BitConverter.GetBytes(value);
			_buffer[_nextWrite++] = (temp.Length > 0)?temp[0]:(byte)0;
			_buffer[_nextWrite++] = (temp.Length > 1)?temp[1]:(byte)0;
		}

		/// <summary>
		/// This method will write a string value to the buffer.
		/// The output is fixed length, and the string is padded
		/// with the supplied character.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="length"></param>
		/// <param name="padding"></param>
		public void WriteASCIIStringFixedLength(string value, int length, byte padding)
		{
			if (value == null)
				value = "";

			EnsureSize(_nextWrite + length);

			byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(value);
			
			int byteCount = (bytes.Length < length)?bytes.Length:length;
	
			for(int loop = 0; loop < byteCount; loop++)
				_buffer[_nextWrite++] = bytes[loop];

			if (byteCount < length)
				for(int loop = byteCount; loop < length; loop++)
					_buffer[_nextWrite++] = padding;
		}

		/// <summary>
		/// Write out a string to the stream, given a max length, and a delimiter character.
		/// If MaxLength = -1, then there is no maximum size restriction
		/// </summary>
		/// <param name="value"></param>
		/// <param name="maxLength"></param>
		/// <param name="delimiter"></param>
		/// <returns>Number of bytes written including delimiter</returns>
		public int WriteASCIIStringDelimited(string value, int maxLength, byte delimiter)
		{
			int byteCount = WriteASCIIString(value, maxLength);
			_buffer[_nextWrite++] = delimiter;
			return byteCount + 1;
		}

		/// <summary>
		/// Write out a variable length string to the byte array.
		/// </summary>
		/// <param name="value"></param>
		/// <param name="maxLength"></param>
		/// <returns></returns>
		public int WriteASCIIString(string value, int maxLength)
		{
			if (value == null)
				value = "";

			byte[] bytes = System.Text.ASCIIEncoding.ASCII.GetBytes(value);

			int byteCount;

			if (maxLength == -1)
				byteCount = bytes.Length;
			else
				byteCount = (bytes.Length < maxLength)?bytes.Length:maxLength;

			//	enlarge to cater for stirng and delimiter
			EnsureSize(_nextWrite + byteCount + 1);

			for(int loop = 0; loop < byteCount; loop++)
				_buffer[_nextWrite++] = bytes[loop];

			return byteCount;
		}

		/// <summary>
		/// This method will return a fixed length byte array with the correct byte values.
		/// </summary>
		/// <returns></returns>
		public byte[] ToByteArray()
		{
			byte[] result = new byte[_nextWrite];
			Array.Copy(_buffer, 0, result, 0, _nextWrite);
			return result;
		}

		/// <summary>
		///	Ensure the buffer can handle the size it needs.
		/// </summary>
		/// <param name="requiredSize"></param>
		private void EnsureSize(int requiredSize)
		{
			int newSize = _buffer.Length;
			while (requiredSize > newSize)
				newSize += BUFFER_INCREMENT;

			if (newSize != _buffer.Length)
			{
				byte[] temp = new byte[newSize];
				Array.Copy(_buffer, 0, temp, 0, _buffer.Length);
				_buffer = temp;
			}
		}
	}
}

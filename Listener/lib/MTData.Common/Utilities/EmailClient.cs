﻿namespace MTData.Common.Utilities
{
    using System;
    using System.Linq;
    using System.ServiceModel;
    using System.Text;
    using Interface;
    using log4net;

    /// <summary>
    /// Defines the <see cref="EmailClient" />
    /// </summary>
    public class EmailClient : IEmailClient
    {
        /// <summary>
        /// Logger instance for this class.
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger(typeof(EmailClient));

        /// <summary>
        /// Service bindings.
        /// </summary>
        private readonly BasicHttpBinding serviceBinding;

        /// <summary>
        /// Service endpoint address
        /// </summary>
        private readonly EndpointAddress serviceEndpoint;

        /// <summary>
        /// Initializes a new instance of the <see cref = "EmailClient"/> class.
        /// </summary>
        /// <param name = "emailServiceUrl">Email service URL</param>
        public EmailClient(string emailServiceUrl)
        {
            this.serviceBinding = new BasicHttpBinding { TransferMode = TransferMode.Buffered, TextEncoding = Encoding.UTF8 };
            this.serviceEndpoint = new EndpointAddress(emailServiceUrl);
        }

        /// <summary>
        /// Sends email message.
        /// </summary>
        /// <param name = "toEmailAddress">To email address.</param>
        /// <param name = "fromEmailAddress">From email address.</param>
        /// <param name = "fromDisplayName">From display name.</param>
        /// <param name = "subject">Subject of the email.</param>
        /// <param name = "body">Body text of the email.</param>
        /// <returns>On success return true, otherwise false.</returns>
        public bool SendEmail(string toEmailAddress, string fromEmailAddress, string fromDisplayName, string subject, string body)
        {
            var emailMessage = new EmailMessage { From = fromEmailAddress, FromDisplayName = fromDisplayName, Subject = subject, BodyPlain = body, To = new[] { toEmailAddress } };
            try
            {
                this.SendEmail(emailMessage);
                return true;
            }
            catch (FaultException<SmtpFailedRecipientsFault> smtpFailedRecipientsFault)
            {
                var failedRecipients = smtpFailedRecipientsFault.Detail.FailedRecipients.Select(x => x.Address);
                this.logger?.ErrorFormat("Failed to send email ({0})to the following recipients: {1}", emailMessage.Subject, string.Join(",", failedRecipients));
            }
            catch (FaultException<SmtpFault> smtpFault)
            {
                this.logger?.Error("An error occurred while sending email. " + smtpFault.Message);
            }
            catch (Exception ex)
            {
                this.logger?.Error("An error occurred while sending email. " + ex.Message);
            }

            return false;
        }

        /// <summary>
        /// Sends email message.
        /// </summary>
        /// <param name = "message">Email message to send</param>
        public void SendEmail(EmailMessage message)
        {
            using (var myChannelFactory = new ChannelFactory<IEmailService>(this.serviceBinding, this.serviceEndpoint))
            {
                IEmailService proxy = null;
                try
                {
                    proxy = myChannelFactory.CreateChannel(this.serviceEndpoint);
                    ((IClientChannel)proxy).Open();
                    proxy.SendEmail(message);
                }
                finally
                {
                    if (((IClientChannel)proxy).State != CommunicationState.Faulted ||
                        ((IClientChannel)proxy).State == CommunicationState.Opened)
                    {
                        ((IClientChannel)proxy).Close();
                    }
                }
            }
        }
    }
}

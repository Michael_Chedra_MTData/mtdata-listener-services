using System;
using System.Threading;
using System.Collections;
using System.Net.Sockets;

namespace MTData.Common.Utilities
{
    public class ReportQueueItem
    {
        #region Event Interface
        public delegate void ServerMessageEvent(string sData, int iListIndex);
        public event ServerMessageEvent eServerMessage;

        public delegate void ServerConnectedEvent(bool bData, int iListIndex);
        public event ServerConnectedEvent eServerConnected;

        public delegate void ServerCompleteEvent(string sURL, string sLocalPath, int iListIndex);
        public event ServerCompleteEvent eServerComplete;

        public delegate void ServerErrorEvent(string sMsg);
        public event ServerErrorEvent eServerError;
        #endregion

        #region Private Vars
        private bool bServerTimesout = false;
        private bool bWaitForNextResponse = true;
        private bool bThreadActive = false;
        private int iListIndex = 0;
        private string sData = "";
        private string sFilename = "";
        private Thread tProcessReport = null;
        private ReportsCircBuffer oCircBuffer = null;
        private string sReportingAddress = "";
        private string sServerURL = "";
        private int iReportingPort = 0;
        private int iReportingRecieveTimeout = 0;
        private int iReportingSendTimeout = 0;
        private int iNoPositionRetry = 0;

        #endregion

        #region Public Properties
        public bool IsRunning
        {
            get
            {
                return bThreadActive;
            }
            set
            {
                bThreadActive = false;
            }
        }
        public string ReportsURL
        {
            get
            {
                return sServerURL;
            }
            set
            {
                sServerURL = value;
            }
        }
        #endregion

        #region Constructor / Destructor
        public ReportQueueItem(int iIndex, string sReportCommand, string sLocalPath, bool ServerTimesout, string ServerAddress, int ServerPort, int SendTimeout, int RecieveTimeout, string ServerURL, int NoPositionRetry)
        {

            iListIndex = iIndex;
            sData = sReportCommand;
            sFilename = sLocalPath;
            bServerTimesout = ServerTimesout;
            sReportingAddress = ServerAddress;
            sServerURL = ServerURL;
            iReportingPort = ServerPort;
            iReportingRecieveTimeout = SendTimeout;
            iReportingSendTimeout = RecieveTimeout;
            iNoPositionRetry = NoPositionRetry;
        }

        public void Dispose()
        {
            #region Close the circular buffer
            if (oCircBuffer != null)
            {
                oCircBuffer.StopThread();
                oCircBuffer.eServerMessage -= new ReportsCircBuffer.ServerMessageEvent(oCircBuffer_eServerMessage);
                oCircBuffer.eServerComplete -= new ReportsCircBuffer.ServerCompleteEvent(oCircBuffer_eServerComplete);
                oCircBuffer.ConsoleEvent -= new ReportsCircBuffer.WriteToConsoleEvent(oCircBuffer_ConsoleEvent);
                oCircBuffer.Dispose();
            }
            oCircBuffer = null;
            #endregion

            bWaitForNextResponse = false;
            bThreadActive = false;
        }

        #endregion

        #region Event Handlers
        private void oCircBuffer_eServerMessage(byte[] bData)
        {
            // This event is fired when the circular buffer finds a complete packet.
            string responseData = "";

            if (bData == null) return;

            if (bData.Length <= 2) return;

            if (eServerMessage == null) return;

            responseData = System.Text.ASCIIEncoding.ASCII.GetString(bData);
            eServerMessage(responseData, iListIndex);
        }

        private void oCircBuffer_eServerComplete(byte[] bData)
        {
            if (eServerComplete != null)
            {
                string sUrl = System.Text.ASCIIEncoding.ASCII.GetString(bData);
                sUrl = sUrl.Replace("\0", "");
                eServerComplete(sUrl, sFilename, iListIndex);
            }
        }
        #endregion

        #region Thread Start + Thead Routine
        public void ProcessReport()
        {
            ThreadStart oThreadStart = new ThreadStart(this.Connect);
            tProcessReport = new Thread(oThreadStart);
            tProcessReport.Name = "Process Reports Thead.";
            bThreadActive = true;
            bWaitForNextResponse = true;
            tProcessReport.Start();	// and start the retry system
        }

        private void Connect()
        {
            // NOTE : the corresponding server code is located at "MTData.Report.Service cJobQueue.NewConnection(TcpClient oTCP)"

            #region Local Vars
            byte[] data = new byte[1];
            int iQueuePort = 0;
            Int32 bytes = 0;
            string sBreak = Convert.ToString((char)0x01);
            TcpClient client = null;
            NetworkStream stream = null;
            #endregion

            #region Create the circular buffer to handle incomming data
            oCircBuffer = new ReportsCircBuffer();
            oCircBuffer.eServerMessage += new ReportsCircBuffer.ServerMessageEvent(oCircBuffer_eServerMessage);
            oCircBuffer.eServerComplete += new ReportsCircBuffer.ServerCompleteEvent(oCircBuffer_eServerComplete);
            oCircBuffer.ConsoleEvent += new ReportsCircBuffer.WriteToConsoleEvent(oCircBuffer_ConsoleEvent);
            oCircBuffer.StartThread();
            #endregion

            #region Establish a connection and get the 'working' port from the server

            while (iQueuePort < 100 && bThreadActive)
            {
                // Check that the server gave us a valid port to connect to
                // Since all the object are closed, here is a good place to exit.
                iQueuePort = GetReportPort();
                if (iQueuePort < 100)
                {
                    int iRetrySeconds = iNoPositionRetry / 1000;
                    while (iRetrySeconds > 0 && bThreadActive)
                    {
                        if (eServerMessage != null)
                            eServerMessage(string.Format("No positions are available in the reporting queue at the moment.  Will retry in {0} seconds.", iRetrySeconds), iListIndex);
                        Thread.Sleep(5000);
                        iRetrySeconds -= 5;
                    }
                }
            }
            Thread.Sleep(1000);
            #endregion

            #region Establish a connection to the working port
            if (iQueuePort >= 100 && bThreadActive)
            {
                try
                {
                    // Connect on the queue port
                    client = new TcpClient(sReportingAddress, iQueuePort);
                    if (bServerTimesout)
                    {
                        client.ReceiveTimeout = iReportingRecieveTimeout;
                        client.SendTimeout = iReportingSendTimeout;
                    }
                    stream = client.GetStream();
                    bWaitForNextResponse = true;
                    if (eServerConnected != null)
                        eServerConnected(true, iListIndex);
                }
                catch (Exception ex1)
                {
                    oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex1.Message + "\n" + ex1.Source);
                    return;
                }
            }
            #endregion

            try
            {
                while (bWaitForNextResponse && bThreadActive)
                {
                    #region While thread is active, get a packet from the server and add it to the circular buffer
                    // Buffer to store the response bytes.
                    data = new Byte[1500];
                    try
                    {
                        int iConnectionTimeout = 0;
                        while (stream != null && bWaitForNextResponse && bThreadActive)
                        {
                            if (!stream.CanRead)
                                break;

                            if (stream.DataAvailable)
                            {
                                bytes = stream.Read(data, 0, data.Length);
                                iConnectionTimeout = 0;
                                break;
                            }
                            else
                            {
                                iConnectionTimeout++;
                                if (iConnectionTimeout > 200)
                                {
                                    try
                                    {
                                        stream.WriteByte(0x01);
                                        iConnectionTimeout = 0;
                                    }
                                    catch
                                    {
                                        bWaitForNextResponse = false;
                                        eServerMessage("Connection to Reports Service Lost", iListIndex);
                                    }
                                }
                            }

                            Thread.Sleep(100);
                        }
                    }
                    catch (System.Exception exRead)
                    {
                        oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + exRead.Message + "\n" + exRead.Source);
                        return;
                    }

                    if (bytes > 0 && bThreadActive)
                    {
                        oCircBuffer.AddBytes(data);
                        bytes = 0;
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex.Message + "\n" + ex.Source);
            }

            // Let the app know the server is disconnected.
            if (eServerConnected != null)
                eServerConnected(false, iListIndex);

            #region Clean Up Objects
            // Close everything.
            try
            {
                stream.Close();
            }
            catch (System.Exception ex01)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex01.Message + "\n" + ex01.Source);
            }
            try
            {
                stream = null;
            }
            catch (System.Exception ex02)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex02.Message + "\n" + ex02.Source);
            }
            try
            {
                client.Close();
            }
            catch (System.Exception ex03)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex03.Message + "\n" + ex03.Source);
            }
            try
            {
                client = null;
            }
            catch (System.Exception ex04)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex04.Message + "\n" + ex04.Source);
            }

            try
            {
                if (oCircBuffer != null)
                {
                    // Clean up the circular buffer.
                    oCircBuffer.StopThread();
                    oCircBuffer.Dispose();
                    oCircBuffer.eServerMessage -= new ReportsCircBuffer.ServerMessageEvent(oCircBuffer_eServerMessage);
                    oCircBuffer.eServerComplete -= new ReportsCircBuffer.ServerCompleteEvent(oCircBuffer_eServerComplete);
                    oCircBuffer.ConsoleEvent -= new ReportsCircBuffer.WriteToConsoleEvent(oCircBuffer_ConsoleEvent);
                }
                oCircBuffer = null;
            }
            catch (System.Exception ex05)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex05.Message + "\n" + ex05.Source);
            }

            #endregion

            bThreadActive = false;
            // Kill off the thread.
            try
            {
                tProcessReport.Abort();
            }
            catch (System.Exception)
            {
                // Just ignore the abort error.
            }
        }
        #endregion

        private void oCircBuffer_ConsoleEvent(string sMsg)
        {
            if (eServerError != null)
                eServerError(sMsg);
        }

        /// <summary>
        /// Establish a connection and get the 'working' port from the server
        /// </summary>
        private int GetReportPort()
        {
            byte[] data = new byte[1];
            Int32 bytes = 0;
            TcpClient client = null;
            NetworkStream stream = null;
            int iQueuePort = 0;

            try
            {
                // Setup the connection
                client = new TcpClient(sReportingAddress, iReportingPort);
                stream = client.GetStream();
                if (bServerTimesout)
                {
                    client.ReceiveTimeout = iReportingRecieveTimeout;
                    client.SendTimeout = iReportingSendTimeout;
                }

                // Translate the passed message into ASCII and store it as a Byte array.
                string[] sConvertToBytes = sData.Split("-".ToCharArray());
                data = new byte[sConvertToBytes.Length];
                for (int X = 0; X < sConvertToBytes.Length; X++)
                {
                    data[X] = ((byte[])BitConverter.GetBytes(Convert.ToInt32(sConvertToBytes[X], 16)))[0];
                }
                // Send the message to the connected TcpServer. 
                stream.Write(data, 0, data.Length);

                // Receive the TcpServer.response.
                bWaitForNextResponse = true;

                int iConnectionTimeout = 0;
                while (stream != null && bWaitForNextResponse)
                {
                    if (!stream.CanRead)
                        break;

                    if (stream.DataAvailable)
                    {
                        bytes = stream.Read(data, 0, data.Length);
                        break;
                    }
                    Thread.Sleep(100);
                    iConnectionTimeout++;
                    if (iConnectionTimeout > 200)
                    {
                        data = null;
                        break;
                    }
                }

                // The server will send a new port number to connect to.
                if (data == null)
                {
                    eServerMessage("Server is not accepting connections.  If the problem persists, please contact technical support.", iListIndex);
                    Thread.Sleep(100);
                    // Close everything.
                    try
                    {
                        if (stream != null)
                            stream.Close();
                        stream = null;

                        client.Close();
                        client = null;
                    }
                    catch (System.Exception ex01)
                    {
                        oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex01.Message + "\n" + ex01.Source);
                    }
                    bThreadActive = false;
                    // Kill off the thread.
                    try
                    {
                        tProcessReport.Abort();
                    }
                    catch (System.Exception)
                    {
                        // Just ignore the abort error.
                    }
                    return iQueuePort;
                }
                if (bWaitForNextResponse)
                    iQueuePort = System.BitConverter.ToInt32(data, 0);
                else
                    iQueuePort = 0;
            }
            catch (System.Exception ex04)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex04.Message + "\n\nSource : " + ex04.Source);
            }

            // Close everything.
            try
            {
                if (stream != null)
                    stream.Close();
                stream = null;

                client.Close();
                client = null;
            }
            catch (System.Exception ex01)
            {
                oCircBuffer_ConsoleEvent("cReportInterface.Connect()\nError : " + ex01.Message + "\n" + ex01.Source);
            }
            return iQueuePort;
        }
    }
}

using System;
using System.Data;
using System.Collections;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for cTimeZone.
    /// </summary>
    public class TimeZone
    {
        public int ID = 0;
        private ArrayList oDLS = null;
        public int iNormalOffset = 0;
        private string _timeZoneName = "";

        public string TimeZoneName
        {
            get
            {
                return _timeZoneName;
            }
        }

        public TimeZone(int TimeZoneID, string sTimeZoneName, int NormalOffset, ArrayList dayLightSavings, bool bShowOffsetInName)
        {
            try
            {
                ID = TimeZoneID;
                iNormalOffset = NormalOffset;
                if (bShowOffsetInName)
                {
                    if (iNormalOffset > 0)
                        _timeZoneName = sTimeZoneName + " (" + Convert.ToString((Convert.ToDouble(iNormalOffset) / Convert.ToDouble(60)) * Convert.ToDouble(-1)) + ")";
                    else
                        _timeZoneName = sTimeZoneName + " (+" + Convert.ToString((Convert.ToDouble(iNormalOffset) / Convert.ToDouble(60)) * Convert.ToDouble(-1)) + ")";
                }
                else
                    _timeZoneName = sTimeZoneName;
                oDLS = dayLightSavings;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public TimeZone(int TimeZoneID, string sTimeZoneName, int NormalOffset, DataTable DLSTable, bool bShowOffsetInName)
        {
            DateTime dtDate = DateTime.MinValue;
            double[] dOADates = null;
            // dOADates[0] = Start Date
            // dOADates[1] = End Date
            // dOADates[2] = Offset

            try
            {
                ID = TimeZoneID;
                iNormalOffset = NormalOffset;
                if (bShowOffsetInName)
                {
                    if (iNormalOffset > 0)
                        _timeZoneName = sTimeZoneName + " (" + Convert.ToString((Convert.ToDouble(iNormalOffset) / Convert.ToDouble(60)) * Convert.ToDouble(-1)) + ")";
                    else
                        _timeZoneName = sTimeZoneName + " (+" + Convert.ToString((Convert.ToDouble(iNormalOffset) / Convert.ToDouble(60)) * Convert.ToDouble(-1)) + ")";
                }
                else
                    _timeZoneName = sTimeZoneName;
                oDLS = new ArrayList();
                if (DLSTable.Rows.Count > 0)
                {
                    for (int X = 0; X < DLSTable.Rows.Count; X++)
                    {
                        dOADates = new double[3];
                        dtDate = Convert.ToDateTime(DLSTable.Rows[X]["StartDS"]);
                        dOADates[0] = dtDate.ToOADate();
                        dtDate = Convert.ToDateTime(DLSTable.Rows[X]["EndDS"]);
                        dOADates[1] = dtDate.ToOADate();
                        dOADates[2] = Convert.ToDouble(DLSTable.Rows[X]["Offset"]);
                        if (dOADates[0] < dOADates[1])
                            oDLS.Add(dOADates);
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public DateTime GetLocalTime(DateTime dtUTCDateTime)
        {
            bool bDLSFound = false;
            double dCurrentDate = 0;
            int iOffset = 0;
            double[] dOADates = null;
            DateTime dtResult = dtUTCDateTime;
            try
            {
                if (oDLS != null)
                {
                    if (oDLS.Count > 0)
                    {
                        dCurrentDate = dtUTCDateTime.ToOADate();
                        for (int X = 0; X < oDLS.Count; X++)
                        {
                            dOADates = (double[])oDLS[X];
                            // dOADates[0] = Start Date
                            // dOADates[1] = End Date
                            // dOADates[2] = Offset

                            if (dOADates[0] <= dCurrentDate && dOADates[1] >= dCurrentDate)
                            {
                                iOffset = Convert.ToInt32(dOADates[2]);
                                dtResult = dtUTCDateTime.AddMinutes(-1 * iOffset);
                                bDLSFound = true;
                            }
                        }
                    }
                }
                if (!bDLSFound)
                {
                    dtResult = dtUTCDateTime.AddMinutes(-1 * iNormalOffset);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtResult;
        }

        public DateTime GetUniversalTime(DateTime dtUTCDateTime)
        {
            bool bDLSFound = false;
            double dCurrentDate = 0;
            int iOffset = 0;
            double[] dOADates = null;
            DateTime dtResult = dtUTCDateTime;
            try
            {
                if (oDLS != null)
                {
                    if (oDLS.Count > 0)
                    {
                        dCurrentDate = dtUTCDateTime.AddMinutes(this.iNormalOffset).ToOADate();
                        for (int X = 0; X < oDLS.Count; X++)
                        {
                            dOADates = (double[])oDLS[X];
                            // dOADates[0] = Start Date
                            // dOADates[1] = End Date
                            // dOADates[2] = Offset

                            if (dOADates[0] <= dCurrentDate && dOADates[1] >= dCurrentDate)
                            {
                                iOffset = Convert.ToInt32(dOADates[2]);
                                dtResult = dtUTCDateTime.AddMinutes(iOffset);
                                bDLSFound = true;
                            }
                        }
                    }
                }
                if (!bDLSFound)
                {
                    dtResult = dtUTCDateTime.AddMinutes(iNormalOffset);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtResult;
        }
        public int GetOffsetAtTime(DateTime dtUTCDateTime)
        {
            int iRet = 0;
            bool bDLSFound = false;
            double dCurrentDate = 0;
            double[] dOADates = null;
            DateTime dtResult = dtUTCDateTime;
            try
            {
                if (oDLS != null)
                {
                    if (oDLS.Count > 0)
                    {
                        dCurrentDate = dtUTCDateTime.ToOADate();
                        for (int X = 0; X < oDLS.Count; X++)
                        {
                            dOADates = (double[])oDLS[X];
                            // dOADates[0] = Start Date
                            // dOADates[1] = End Date
                            // dOADates[2] = Offset

                            if (dOADates[0] <= dCurrentDate && dOADates[1] >= dCurrentDate)
                            {
                                iRet = Convert.ToInt32(dOADates[2]);
                                bDLSFound = true;
                            }
                        }
                    }
                }
                if (!bDLSFound)
                {
                    iRet = iNormalOffset;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }

    }
}

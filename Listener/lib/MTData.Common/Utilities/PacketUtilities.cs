using System;
using System.IO;
using System.Text;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// This class has functions to append various data types to a byte[].
    /// </summary>
    public class PacketUtilities
    {
        #region Time Stamping

        public static byte[] GetTimeStampDMY()
        {
            DateTime oDate = System.DateTime.Now;
            return GetTimeStampDMY(oDate);
        }

        public static byte[] GetTimeStampDMY(DateTime oData)
        {
            string sDate = oData.Day + "//" + oData.Month + "//" + oData.Year + " " + oData.Hour + ":" + oData.Minute + ":" + oData.Second + "." + oData.Millisecond;
            return System.Text.ASCIIEncoding.ASCII.GetBytes(sDate);
        }

        #endregion
        #region AppendToPacket Functions
        public static byte[] AppendToPacket(byte[] bPacket, bool bData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(bData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, byte[] bAddThis)
        {
            int X = 0;
            int iCounter = 0;

            // If the data to be appended to is null
            if (bPacket == null)
            {
                // If the data to be appended is null as well, return null, else return the data to be appended.
                if (bAddThis == null)
                    return null;
                else
                    return bAddThis;
            }
            else
            {
                // If the data to be added is null, then return the data unchanged.
                if (bAddThis == null)
                    return bPacket;
            }

            byte[] bOutput = new byte[bPacket.Length + bAddThis.Length];


            if (bPacket == null)
            {
                if (bAddThis == null)
                    return null;
                else
                    return bAddThis;
            }
            else
            {
                if (bAddThis == null)
                    return bPacket;
            }

            iCounter = bPacket.Length;

            try
            {
                for (X = 0; X < bPacket.Length; X++)
                {
                    bOutput[X] = bPacket[X];
                }

            }
            catch (System.Exception ex1)
            {
                Console.Write(ex1);
            }

            try
            {
                // iCounter = next avaliable position in the byte array
                for (X = 0; X < bAddThis.Length; X++)
                {
                    bOutput[iCounter] = bAddThis[X];
                    iCounter++;
                }
            }
            catch (System.Exception ex2)
            {
                Console.Write(ex2);
            }

            return bOutput;
        }

        public static byte[] AppendToPacket(byte[] bPacket, byte bAddThis)
        {
            byte[] bOutput = new byte[bPacket.Length + 1];
            for (int X = 0; X < bPacket.Length; X++)
            {
                bOutput[X] = bPacket[X];
            }

            bOutput[bPacket.Length] = bAddThis;
            return bOutput;
        }

        public static byte[] AppendToPacket(byte[] bPacket, sbyte bData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes((byte)bData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, char cData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(cData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }

        public static byte[] AppendToPacket(byte[] bPacket, decimal dcData)
        {
            double dData = Convert.ToDouble(dcData);
            byte[] bAddThis = System.BitConverter.GetBytes(dData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, double dData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(dData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, float dData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(dData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }

        public static byte[] AppendToPacket(byte[] bPacket, int iData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(iData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, long lData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(lData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, short iData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(iData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, string sAddThis)
        {
            byte[] bOutput = null;
            if (sAddThis != "")
            {
                byte[] bAddThis = System.Text.Encoding.ASCII.GetBytes(sAddThis);
                bOutput = AppendToPacket(bPacket, bAddThis);
            }
            else
                bOutput = bPacket;
            return bOutput;
        }

        public static byte[] AppendToPacket(byte[] bPacket, byte bDelimiter, string sAddThis)
        {
            byte[] bOutput = null;
            if (sAddThis != "")
            {
                byte[] bAddThis = System.Text.Encoding.ASCII.GetBytes(sAddThis);
                bOutput = AppendToPacket(bPacket, bAddThis);
            }
            else
                bOutput = bPacket;
            bOutput = AppendToPacket(bOutput, bDelimiter);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, uint iData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(iData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, ulong lData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(lData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }
        public static byte[] AppendToPacket(byte[] bPacket, ushort iData)
        {
            byte[] bAddThis = System.BitConverter.GetBytes(iData);
            byte[] bOutput = AppendToPacket(bPacket, bAddThis);
            return bOutput;
        }

        #endregion
        #region WriteToPacketAtPos
        private static void WriteBytesToArray(ref byte[] bPacket, ref int iPos, ref byte[] bAddThis)
        {
            byte[] bRemaining = null;
            int iRemPos = 0;

            if (bPacket.Length > iPos + bAddThis.Length)
            {
                for (int X = 0; X < bAddThis.Length; X++)
                {
                    bPacket[iPos++] = bAddThis[X];
                }
            }
            else
            {
                for (int X = 0; X < bAddThis.Length; X++)
                {
                    if (bPacket.Length > iPos)
                        bPacket[iPos++] = bAddThis[X];
                    else
                    {
                        bRemaining = new byte[bAddThis.Length - X];
                        for (int Y = X; Y < bAddThis.Length; Y++)
                            bRemaining[iRemPos] = bAddThis[Y];
                        bPacket = AppendToPacket(bPacket, bRemaining);
                        iPos += bRemaining.Length;
                    }
                }
            }
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, bool oValue)
        {
            byte[] bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, byte[] oValue)
        {
            WriteBytesToArray(ref bPacket, ref iPos, ref oValue);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, byte oValue)
        {
            byte[] bData = new byte[1];
            bData[0] = oValue;
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, sbyte oValue)
        {
            byte[] bData = new byte[1];
            bData[0] = (byte)oValue;
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, char oValue)
        {
            byte[] bData = new byte[1];
            bData[0] = (byte)oValue;
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, decimal oValue)
        {
            double dValue = Convert.ToDouble(oValue);
            byte[] bData = new byte[8];
            if (dValue != 0)
                bData = System.BitConverter.GetBytes(dValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, double oValue)
        {
            byte[] bData = new byte[8];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, float oValue)
        {
            byte[] bData = new byte[4];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, int oValue)
        {
            byte[] bData = new byte[4];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, long oValue)
        {
            byte[] bData = new byte[8];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, short oValue)
        {
            byte[] bData = new byte[2];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, string oValue)
        {
            if (oValue.Length > 0)
            {
                byte[] bData = System.Text.Encoding.ASCII.GetBytes(oValue);
                WriteBytesToArray(ref bPacket, ref iPos, ref bData);
            }
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, byte bDelimiter, string oValue)
        {
            byte[] bData = null;
            if (oValue.Length > 0)
            {
                bData = System.Text.Encoding.ASCII.GetBytes(oValue);
                WriteBytesToArray(ref bPacket, ref iPos, ref bData);
            }
            bData = new byte[1];
            bData[0] = bDelimiter;
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, uint oValue)
        {
            byte[] bData = new byte[4];
            bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, ulong oValue)
        {
            byte[] bData = new byte[8];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        public static void WriteToPacketAtPos(ref byte[] bPacket, ref int iPos, ushort oValue)
        {
            byte[] bData = new byte[2];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            WriteBytesToArray(ref bPacket, ref iPos, ref bData);
        }
        #endregion
        #region ReadFromPacketAtPos
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref bool oRet)
        {
            oRet = BitConverter.ToBoolean(bPacket, iPos++);
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, int iLength, ref byte[] oRet)
        {
            oRet = new byte[iLength];
            int iTemp = 0;
            for (int X = iPos; X < iPos + iLength; X++)
            {
                oRet[iTemp++] = bPacket[X];
            }
            iPos += iLength;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref byte oRet)
        {
            oRet = bPacket[iPos++];
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref sbyte oRet)
        {
            oRet = (sbyte)bPacket[iPos++];
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref char oRet)
        {
            oRet = (char)bPacket[iPos++];
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref decimal oRet)
        {
            oRet = Convert.ToDecimal(BitConverter.ToDouble(bPacket, iPos));
            iPos += 8;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref double oRet)
        {
            oRet = BitConverter.ToDouble(bPacket, iPos);
            iPos += 8;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref float oRet)
        {
            oRet = BitConverter.ToSingle(bPacket, iPos);
            iPos += 4;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref int oRet)
        {
            oRet = BitConverter.ToInt32(bPacket, iPos);
            iPos += 4;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref long oRet)
        {
            oRet = BitConverter.ToInt64(bPacket, iPos);
            iPos += 8;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref short oRet)
        {
            oRet = BitConverter.ToInt16(bPacket, iPos);
            iPos += 2;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, byte EOS, ref string oRet)
        {
            byte bTest = (byte)0x00;

            while (iPos < bPacket.Length)
            {
                bTest = bPacket[iPos++];
                if (bTest != EOS)
                    oRet += (char)bTest;
                else
                    break;
            }
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, int iLength, ref string oRet)
        {
            byte[] bTest = new byte[iLength];

            for (int i = 0; i < iLength; i++)
            {
                bTest[i] = bPacket[iPos++];
                if (iPos >= bPacket.Length)
                    break;
            }
            oRet = System.Text.ASCIIEncoding.ASCII.GetString(bTest);
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref uint oRet)
        {
            oRet = BitConverter.ToUInt32(bPacket, iPos);
            iPos += 4;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref ulong oRet)
        {
            oRet = BitConverter.ToUInt64(bPacket, iPos);
            iPos += 8;
        }
        public static void ReadFromPacketAtPos(byte[] bPacket, ref int iPos, ref ushort oRet)
        {
            oRet = BitConverter.ToUInt16(bPacket, iPos);
            iPos += 2;
        }
        #endregion
        #region ConvertToAscii Functions
        public static string ConvertToAscii(byte[] oData)
        {
            return ConvertToAscii(oData, true);
        }

        public static string ConvertToAscii(string oData)
        {
            byte[] bPacket = System.Text.ASCIIEncoding.ASCII.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(char[] oData)
        {
            byte[] bPacket = System.Text.ASCIIEncoding.ASCII.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(DateTime oData)
        {
            string sDate = oData.Day + "//" + oData.Month + "//" + oData.Year + " " + oData.Hour + ":" + oData.Minute + ":" + oData.Second + "." + oData.Millisecond;
            byte[] bPacket = System.Text.ASCIIEncoding.ASCII.GetBytes(sDate);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(char oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(bool oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(short oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(int oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(long oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(ushort oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(uint oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(ulong oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(double oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(float oData)
        {
            byte[] bPacket = BitConverter.GetBytes(oData);
            return ConvertToAscii(bPacket, true);
        }

        public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
        {
            StringBuilder sb = new StringBuilder();
            int X = 0;
            //			byte bLower = 0x21;	// 33
            //			byte bUpper = 0x7E;  // 126
            byte bTest = 0x00;  // null
            byte[] bTestArray = new byte[1];

            if (bPacket.Length > 0)
            {
                try
                {
                    for (X = 0; X < bPacket.Length; X++)
                    {
                        bTest = bPacket[X];
                        bTestArray = new byte[1];
                        bTestArray[0] = bTest;
                        if (bTest >= 33 && bTest <= 126)  // If the character is in printable range
                            if (bAddSpacing)
                            {
                                if (sb.Length > 0)
                                    sb.Append(" - ");
                                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(bTestArray));
                            }
                            else
                            {
                                sb.Append(System.Text.ASCIIEncoding.ASCII.GetString(bTestArray));
                            }
                        else  // Show it as a number
                            if (bAddSpacing)
                            {
                                if (sb.Length > 0)
                                    sb.Append(" - ");
                                sb.AppendFormat("[{0}]", bTest);
                            }
                            else
                            {
                                sb.AppendFormat("[{0}]", System.Text.ASCIIEncoding.ASCII.GetString(bTestArray));
                            }
                    }

                }
                catch (System.Exception ex)
                {
                    Console.Write(ex.Message + "\n");
                }
            }
            return sb.ToString();
        }

        public static byte[] ConvertAsciiStringToByteArray(string sData)
        {
            byte[] bData = null;
            string[] sDataSplit = null;

            try
            {
                while (sData.IndexOf("- - -") >= 0)
                {
                    sData = sData.Replace(" - - - ", " - [45] - ");
                }
                sDataSplit = sData.Split("-".ToCharArray());
                bData = new byte[sDataSplit.Length];

                for (int X = 0; X < sDataSplit.Length; X++)
                {
                    if (sDataSplit[X].IndexOf("[") <= 1 && sDataSplit[X].IndexOf("]") > 1)
                    {
                        #region This is an byte value
                        sDataSplit[X] = sDataSplit[X].Replace("[", "");
                        sDataSplit[X] = sDataSplit[X].Replace("]", "");
                        sDataSplit[X] = sDataSplit[X].Trim();
                        bData[X] = (byte)Convert.ToUInt16(sDataSplit[X]);
                        #endregion
                    }
                    else
                    {
                        #region This is an ascii char value
                        sDataSplit[X] = sDataSplit[X].Trim();
                        bData[X] = (byte)(sDataSplit[X].ToCharArray()[0]);
                        #endregion
                    }
                }
            }
            catch (System.Exception)
            {
                bData = null;
            }
            return bData;
        }
        #endregion
        #region ByteBuffer Manipulation Functions

        /// <summary>
        /// Given a byte buffer, write the required value into it, at the specified position.
        /// </summary>
        /// <param name="buffer">byte array for buffer.. needs to be guaranteed size</param>
        /// <param name="newValue">integer value to be converted to bytes</param>
        /// <param name="index">Index into the buffer to start writing</param>
        /// <returns>Updated Index value</returns>
        public static int Write4ByteNumberToBuffer(byte[] buffer, int newValue, int index)
        {
            byte[] temp = BitConverter.GetBytes(newValue);
            buffer[index++] = (temp.Length > 0) ? temp[0] : (byte)0;
            buffer[index++] = (temp.Length > 1) ? temp[1] : (byte)0;
            buffer[index++] = (temp.Length > 2) ? temp[2] : (byte)0;
            buffer[index++] = (temp.Length > 3) ? temp[3] : (byte)0;

            return index;
        }

        /// <summary>
        /// Given a byte buffer, write the required value into it, at the specified position.
        /// </summary>
        /// <param name="buffer">byte array for buffer.. needs to be guaranteed size</param>
        /// <param name="newValue">integer value to be converted to bytes</param>
        /// <param name="index">Index into the buffer to start writing</param>
        /// <returns>Updated index value</returns>
        public static int Write2ByteNumberToBuffer(byte[] buffer, int newValue, int index)
        {
            byte[] temp = BitConverter.GetBytes(newValue);
            buffer[index++] = (temp.Length > 0) ? temp[0] : (byte)0;
            buffer[index++] = (temp.Length > 1) ? temp[1] : (byte)0;

            return index;
        }

        /// <summary>
        /// This procedure will retrieve 4 bytes of data, and convert it into a valid integer.
        /// This integer will be stored LSB,...MSB
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static int Read4ByteNumberFromBuffer(byte[] buffer, int index)
        {
            int result = 0;
            result = buffer[index + 3];
            result = result << 8;
            result += buffer[index + 2];
            result = result << 8;
            result += buffer[index + 1];
            result = result << 8;
            result += buffer[index];
            return result;
        }

        /// <summary>
        /// This procedure will retrieve 2 bytes of data, and convert it into a valid integer.
        /// This integer will be stored LSB,...MSB
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="index"></param>
        /// <returns></returns>
        public static int Read2ByteNumberFromBuffer(byte[] buffer, int index)
        {
            int result = 0;
            result += buffer[index + 1];
            result = result << 8;
            result += buffer[index];
            return result;
        }

        #endregion
        #region WriteToStream
        public static void WriteToStream(Stream oStream, bool oValue)
        {
            byte[] bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, byte[] oValue)
        {
            if (oValue != null)
                oStream.Write(oValue, 0, oValue.Length);
        }
        public static void WriteToStream(Stream oStream, byte oValue)
        {
            oStream.WriteByte(oValue);
        }
        public static void WriteToStream(Stream oStream, sbyte oValue)
        {
            byte[] bData = new byte[1];
            bData[0] = (byte)oValue;
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, char oValue)
        {
            byte[] bData = new byte[1];
            bData[0] = (byte)oValue;
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, decimal oValue)
        {
            double dValue = Convert.ToDouble(oValue);
            byte[] bData = new byte[8];
            if (dValue != 0)
                bData = System.BitConverter.GetBytes(dValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, double oValue)
        {
            byte[] bData = new byte[8];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, float oValue)
        {
            byte[] bData = new byte[4];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, int oValue)
        {
            byte[] bData = new byte[4];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, long oValue)
        {
            byte[] bData = new byte[8];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, short oValue)
        {
            byte[] bData = new byte[2];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, string oValue)
        {
            if (oValue.Length > 0)
            {
                byte[] bData = System.Text.Encoding.ASCII.GetBytes(oValue);
                oStream.Write(bData, 0, bData.Length);
            }
        }
        public static void WriteToStream(Stream oStream, byte bDelimter, string oValue)
        {
            if (oValue.Length > 0)
            {
                byte[] bData = System.Text.Encoding.ASCII.GetBytes(oValue);
                oStream.Write(bData, 0, bData.Length);
            }
            oStream.WriteByte(bDelimter);
        }
        public static void WriteToStream(Stream oStream, uint oValue)
        {
            byte[] bData = new byte[4];
            bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, ulong oValue)
        {
            byte[] bData = new byte[8];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStream(Stream oStream, ushort oValue)
        {
            byte[] bData = new byte[2];
            if (oValue != 0)
                bData = System.BitConverter.GetBytes(oValue);
            oStream.Write(bData, 0, bData.Length);
        }
        public static void WriteToStreamNullable(Stream oStream, string oValue)
        {
            if (oValue == null)
            {
                PacketUtilities.WriteToStream(oStream, (int)-1);
            }
            else
            {
                PacketUtilities.WriteToStream(oStream, oValue.Length);
                if (oValue.Length > 0)
                {
                    byte[] bData = System.Text.Encoding.ASCII.GetBytes(oValue);
                    oStream.Write(bData, 0, bData.Length);
                }
            }
        }
        #endregion
        #region ReadFromStream
        public static void ReadFromStream(Stream oStream, ref bool oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[1];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToBoolean(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            if (iRet == 0)
                throw new System.Exception("Failed to read from stream.");
        }
        public static void ReadFromStream(Stream oStream, int iLength, ref byte[] oRet)
        {
            int iRet = 0;
            oRet = new byte[iLength];
            try
            {
                iRet = oStream.Read(oRet, 0, oRet.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            if (iRet == 0)
                throw new System.Exception("Failed to read from stream.");
        }
        public static void ReadFromStream(Stream oStream, ref byte oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[1];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = bData[0];
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref sbyte oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[1];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = (sbyte)bData[0];
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref char oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[1];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = (char)bData[0];
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref decimal oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[8];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = Convert.ToDecimal(BitConverter.ToDouble(bData, 0));
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref double oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[8];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToDouble(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref float oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[4];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToSingle(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref int oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[4];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToInt32(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref long oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[8];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToInt64(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref short oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[2];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToInt16(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, byte EOS, ref string oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[1];
            try
            {
                while (oStream.Position < oStream.Length)
                {
                    iRet = oStream.Read(bData, 0, bData.Length);
                    if (iRet == 0)
                        throw new System.Exception("Failed to read from stream.");
                    if (bData[0] != EOS)
                        oRet += (char)bData[0];
                    else
                        break;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public static void ReadFromStream(Stream oStream, int iLength, ref string oRet)
        {
            byte[] bData = new byte[1];
            int iRet = 0;
            try
            {
                for (int i = 0; i < iLength; i++)
                {
                    iRet = oStream.Read(bData, 0, bData.Length);
                    if (iRet == 0)
                        throw new System.Exception("Failed to read from stream.");
                    oRet += (char)bData[0];
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref uint oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[4];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToUInt32(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref ulong oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[8];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToUInt64(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStream(Stream oStream, ref ushort oRet)
        {
            int iRet = 0;
            byte[] bData = new byte[2];
            try
            {
                iRet = oStream.Read(bData, 0, bData.Length);
                if (iRet == 0)
                    throw new System.Exception("Failed to read from stream.");
                oRet = BitConverter.ToUInt16(bData, 0);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }
        public static void ReadFromStreamNullable(Stream oStream, ref string oRet)
        {
            //first 4 bytes are lenght
            int length = 0;
            ReadFromStream(oStream, ref length);
            if (length > 0)
            {
                ReadFromStream(oStream, length, ref oRet);
            }
        }
        #endregion
        #region MCC Wrapping
        public static byte[] CreateMCCPacket(int iUnitIDLength, uint iMobileID, char cMessageType, byte bSequence, byte bPacketNumber, byte bPacketTotal, byte[] bDataPayload)
        {
            byte[] bSendPacket = null;
            byte[] bConvert = null;
            ulong iChecksum = 0;
            MemoryStream oMS = null;

            // Packet format = [SOP][MessageType][Mobile ID][Sequence Number][Payload Length][Packet Number][Packet Total][Spare][Data][Checksum][EOT]
            // [SOP] = 0x01
            // [MessageType] = 1 Byte Ascii - The type of data being transmitted
            // [Mobile ID] = 2 Bytes ushort - Mobile ID
            // [Sequence Number] = 1 Byte  - Look for an acknowledgement with this number
            // [Payload Length] = 2 Bytes ushort  - Number of bytes in the payload
            // [Packet Number] = 1 Byte - Number of this packet in data transmission
            // [Packet Total] = 1 Byte - Total number of this packets in data transmission
            // [Spare] = 1 Byte - Spare
            // [Data] = Var Length - Payload data
            // [Checksum] = 2 Byte - Sub of bytes up to this point
            // [EOT] = 0x04

            oMS = new MemoryStream();
            WriteToStream(oMS, (byte)0x01);							// SOP
            WriteToStream(oMS, cMessageType);						// Message Type
            bConvert = BitConverter.GetBytes(iMobileID);
            for (int X = iUnitIDLength - 1; X >= 0; X--)					// Mobile ID
            {
                WriteToStream(oMS, bConvert[X]);
            }
            WriteToStream(oMS, bSequence);								// Sequence Number
            bConvert = BitConverter.GetBytes(bDataPayload.Length);
            WriteToStream(oMS, bConvert[1]);							// Payload Length
            WriteToStream(oMS, bConvert[0]);
            WriteToStream(oMS, bPacketNumber);						// Packet Number
            WriteToStream(oMS, bPacketTotal);							// Packet Total
            WriteToStream(oMS, (byte)0x00);							// Spare
            if (bDataPayload == null)												// Data
                WriteToStream(oMS, (byte)0x00);
            else
                WriteToStream(oMS, bDataPayload);
            WriteToStream(oMS, (byte)0x00);							// Checksum
            WriteToStream(oMS, (byte)0x00);
            WriteToStream(oMS, (byte)0x04);							// EOT
            bSendPacket = oMS.ToArray();

            // Populate the check sum value
            for (int X = 0; X < bSendPacket.Length - 3; X++)
            {
                iChecksum += (ulong)bSendPacket[X];
            }
            bSendPacket[bSendPacket.Length - 3] = ((byte[])BitConverter.GetBytes(iChecksum))[1];
            bSendPacket[bSendPacket.Length - 2] = ((byte[])BitConverter.GetBytes(iChecksum))[0];

            return bSendPacket;
        }

        public static byte[] CreateToMCCPacketPayload(uint iMobileID, uint iJobID, byte[] bDataPayload)
        {
            byte[] bSendPacket = null;
            byte[] bIntegerBuffer = null;
            int iLen = 0;
            MemoryStream oMS = null;

            // Packet format = [SOP][Length][Mobile ID][Sep][Job ID][Sep][Data][EOP]
            // [SOP] = 0x0A
            // [Length] = 4 Bytes
            // [Mobile ID] = 4 Bytes
            // [Sep] = 0x0B
            // [Job ID] = 4 Bytes
            // [Sep] = 0x0B
            // [Data] = Var length data
            // [EOP] = 0x0C
            // Length includes [Unit ID][Sep][Job ID][Sep][Data]

            oMS = new MemoryStream();
            WriteToStream(oMS, (byte)0x0A);							// SOP
            WriteToStream(oMS, (uint)0);									// Length
            WriteToStream(oMS, iMobileID);								// Mobile ID
            WriteToStream(oMS, (byte)0x0B);							// Sep
            WriteToStream(oMS, iJobID);									// Job ID
            WriteToStream(oMS, (byte)0x0B);							// Sep
            if (bDataPayload == null)												// Data
                WriteToStream(oMS, (byte)0x00);
            else
                WriteToStream(oMS, bDataPayload);
            WriteToStream(oMS, (byte)0x0C);							// EOP
            bSendPacket = oMS.ToArray();

            // Populate the length field.
            iLen = bSendPacket.Length - 6;
            bIntegerBuffer = BitConverter.GetBytes(iLen);
            bSendPacket[1] = bIntegerBuffer[0];
            bSendPacket[2] = bIntegerBuffer[1];
            bSendPacket[3] = bIntegerBuffer[2];
            bSendPacket[4] = bIntegerBuffer[3];
            return bSendPacket;
        }

        public static string UnwrapMCCPacket(byte[] bData, int iUnitIDLength, char cMsgType, byte bSequence, uint iMobileID, byte bPacketNumber, byte bPacketTotal, uint iJobID, byte[] bPayload)
        {
            string sError = "";
            byte bTemp = (byte)0x00;
            MemoryStream oMS = null;
            ushort iLength = 0;
            ushort iChecksum = 0;
            ushort iChecksumCalc = 0;
            byte[] bConvert = null;
            byte[] bMCCPayload = null;
            int iCheckPos = 0;
            ulong lChecksum = 0;


            // Packet format = [SOP][MessageType][Mobile ID][Sequence Number][Payload Length][Packet Number][Packet Total][Spare][Data][Checksum][EOT]
            // [SOP] = 0x01
            // [MessageType] = 1 Byte Ascii - The type of data being transmitted
            // [Mobile ID] = 2 Bytes ushort - Mobile ID
            // [Sequence Number] = 1 Byte  - Look for an acknowledgement with this number
            // [Payload Length] = 2 Bytes ushort  - Number of bytes in the payload
            // [Packet Number] = 1 Byte - Number of this packet in data transmission
            // [Packet Total] = 1 Byte - Total number of this packets in data transmission
            // [Spare] = 1 Byte - Spare
            // [Data] = Var Length - Payload data (min 16 bytes)
            // [Checksum] = 2 Byte - Sub of bytes up to this point
            // [EOT] = 0x04

            try
            {
                #region Decode the packet data
                if (bData.Length < (26 + iUnitIDLength))
                {
                    sError = "Invalid MCC Packet : Payload is too short";
                    iJobID = 0;
                    iMobileID = 0;
                    bPayload = null;
                }
                else
                {
                    oMS = new MemoryStream(bData, 0, bData.Length, false, false);
                    ReadFromStream(oMS, ref bTemp);																										// SOP
                    if (bTemp != (byte)0x01)
                    {
                        sError = "Invalid MCC Packet : SOP was not found";
                        iJobID = 0;
                        iMobileID = 0;
                        bPayload = null;
                    }
                    else
                    {
                        ReadFromStream(oMS, ref cMsgType);																								// MessageType
                        bConvert = new byte[4];
                        for (int X = iUnitIDLength - 1; X >= 0; X--)
                        {
                            ReadFromStream(oMS, ref bTemp);
                            bConvert[X] = bTemp;
                        }
                        iMobileID = BitConverter.ToUInt32(bConvert, 0);																			// Mobile ID
                        ReadFromStream(oMS, ref bSequence);																							// Sequence Number
                        bConvert = new byte[2];																														// Packet Length
                        for (int X = 1; X >= 0; X--)
                        {
                            ReadFromStream(oMS, ref bTemp);
                            bConvert[X] = bTemp;
                        }
                        iLength = BitConverter.ToUInt16(bConvert, 0);
                        ReadFromStream(oMS, ref bPacketNumber);																					// Packet Number
                        ReadFromStream(oMS, ref bPacketTotal);																							// Total Packets
                        ReadFromStream(oMS, ref bTemp);																									// Spare
                        ReadFromStream(oMS, (int)iLength, ref bMCCPayload);																	// MCC Payload
                        iCheckPos = Convert.ToInt32(oMS.Position);
                        bConvert = new byte[2];																														// Checksum
                        for (int X = 1; X >= 0; X--)
                        {
                            ReadFromStream(oMS, ref bTemp);
                            bConvert[X] = bTemp;
                        }
                        iChecksum = BitConverter.ToUInt16(bConvert, 0);
                        ReadFromStream(oMS, ref bTemp);																									// EOT
                        if (bTemp != (byte)0x04)
                        {
                            sError = "Invalid MCC Packet : EOT was not found";
                            iJobID = 0;
                            iMobileID = 0;
                            bPayload = null;
                        }

                        if (iChecksum > 0)
                        {
                            for (int X = 0; X < iCheckPos; X++)
                            {
                                lChecksum += (ulong)bData[X];
                            }
                            bConvert = new byte[2];
                            bConvert[0] = ((byte[])BitConverter.GetBytes(iChecksum))[1];
                            bConvert[1] = ((byte[])BitConverter.GetBytes(iChecksum))[0];
                            iChecksumCalc = BitConverter.ToUInt16(bConvert, 0);
                            if (iChecksumCalc != iChecksum)
                            {
                                sError = "Invalid MCC Packet : Checksum was not correct";
                                iJobID = 0;
                                iMobileID = 0;
                                bPayload = null;
                            }
                        }
                    }
                }
                #endregion
                #region Decode the MCC Payload
                if (sError == "" && bMCCPayload != null)
                {
                    sError = UnwrapMCCPayloadPacket(bMCCPayload, iMobileID, iJobID, bPayload);
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                sError = "Invalid MCC Packet : " + ex.Message;
                iJobID = 0;
                iMobileID = 0;
                bPayload = null;
            }
            return sError;
        }
        public static string UnwrapMCCPayloadPacket(byte[] bData, uint iMobileID, uint iJobID, byte[] bPayload)
        {
            string sError = "";
            uint iPacketLength = 0;
            byte bTemp = (byte)0x00;
            MemoryStream oMS = null;

            // Packet format = [SOP][Length][Mobile ID][Sep][Job ID][Sep][Data][EOP]
            // [SOP] = 0x0A
            // [Length] = 4 Bytes
            // [Mobile ID] = 4 Bytes
            // [Sep] = 0x0B
            // [Job ID] = 4 Bytes
            // [Sep] = 0x0B
            // [Data] = Var length data
            // [EOP] = 0x0C
            // Length includes [Unit ID][Sep][Job ID][Sep][Data]

            try
            {
                #region Decode the packet data
                if (bData.Length < 16)
                {
                    sError = "Invalid Packet Payload : Payload is too short";
                    iJobID = 0;
                    iMobileID = 0;
                    bPayload = null;
                }
                else
                {
                    oMS = new MemoryStream(bData, 0, bData.Length, false, false);
                    ReadFromStream(oMS, ref bTemp);																										// SOP
                    if (bTemp != (byte)0x0A)
                    {
                        sError = "Invalid Packet Payload : SOP was not found";
                        iJobID = 0;
                        iMobileID = 0;
                        bPayload = null;
                    }
                    else
                    {
                        ReadFromStream(oMS, ref iPacketLength);																						// Length
                        if (Convert.ToUInt32(oMS.Length - (long)6) != iPacketLength)
                        {
                            sError = "Invalid Packet Payload : Packet Length was not correct";
                            iJobID = 0;
                            iMobileID = 0;
                            bPayload = null;
                        }
                        else
                        {
                            ReadFromStream(oMS, ref iMobileID);																							// Mobile ID
                            ReadFromStream(oMS, ref bTemp);																								// Sep
                            if (bTemp != (byte)0x0B)
                            {
                                sError = "Invalid Packet Payload : Unit ID Seperator was not found";
                                iJobID = 0;
                                iMobileID = 0;
                                bPayload = null;
                            }
                            else
                            {
                                ReadFromStream(oMS, ref iJobID);																							// Mobile ID
                                ReadFromStream(oMS, ref bTemp);																							// Sep
                                if (bTemp != (byte)0x0B)
                                {
                                    sError = "Invalid Packet Payload : Job ID Seperator was not found";
                                    iJobID = 0;
                                    iMobileID = 0;
                                    bPayload = null;
                                }
                                else
                                {
                                    ReadFromStream(oMS, (int)iPacketLength, ref bPayload);													// Payload
                                    ReadFromStream(oMS, ref bTemp);																						// EOP
                                    if (bTemp != (byte)0x0C)
                                    {
                                        sError = "Invalid Packet Payload : EOP was not found";
                                        iJobID = 0;
                                        iMobileID = 0;
                                        bPayload = null;
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                sError = "Invalid Packet Payload : " + ex.Message;
                iJobID = 0;
                iMobileID = 0;
                bPayload = null;
            }
            return sError;
        }
        public static void UnwrapFromMCC(byte[] bData, ref int iMobileID, ref int iJobID, ref byte[] bPayload, ref byte[] bLeftOver)
        {
            // Packet format = [SOP][Length 1234][Unit ID][Sep][Job ID][Sep][Data][EOP]
            // [SOP] = 0x0A
            // [Sep] = 0x0B
            // [EOP] = 0x0C

            MemoryStream oMS = null;
            byte bTemp = (byte)0x00;
            byte[] baTemp = null;
            int iTemp = 0;
            int iLength = 0;
            int iPos = 0;

            bPayload = null;
            iMobileID = -1;
            iJobID = -1;

            if (bLeftOver != null)
            {
                baTemp = new byte[bLeftOver.Length + bData.Length];
                for (int X = 0; X < bLeftOver.Length; X++)
                    baTemp[iPos++] = bLeftOver[X];
                for (int X = 0; X < bData.Length; X++)
                    baTemp[iPos++] = bData[X];

                oMS = new MemoryStream(baTemp, 0, baTemp.Length, false, false);
                bLeftOver = null;
                baTemp = null;
                bData = oMS.ToArray();
            }

            oMS = new MemoryStream(bData, 0, bData.Length, false, false);

            ReadFromStream(oMS, ref bTemp);
            if (bTemp == (byte)0x0A)
            {
                ReadFromStream(oMS, ref iLength);
                if (iLength + 5 < Convert.ToInt32(oMS.Length))
                {
                    if (bData[iLength + 5] == (byte)0x0C)
                    {
                        ReadFromStream(oMS, ref iTemp);
                        iMobileID = iTemp;
                        ReadFromStream(oMS, ref bTemp);
                        if (bTemp == (byte)0x0B)
                        {
                            ReadFromStream(oMS, ref iTemp);
                            iJobID = iTemp;
                            ReadFromStream(oMS, ref bTemp);
                            if (bTemp == (byte)0x0B)
                            {
                                ReadFromStream(oMS, iLength - 10, ref baTemp);
                            }
                            bPayload = baTemp;
                            ReadFromStream(oMS, ref bTemp); // EOP Char
                            if (bTemp != (byte)0x0C)
                            {
                                bPayload = null;
                            }
                            if (oMS.Position < bData.Length)
                            {
                                iLength = bData.Length - Convert.ToInt32(oMS.Position);
                                ReadFromStream(oMS, iLength, ref baTemp);
                                bLeftOver = baTemp;
                            }
                        }
                    }
                }
                else
                {
                    bLeftOver = bData;
                    bPayload = null;
                }
            }
        }
        #endregion
    }
}

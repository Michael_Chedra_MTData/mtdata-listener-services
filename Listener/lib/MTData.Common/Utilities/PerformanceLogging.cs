using System;
using System.IO;
using System.Text;
using System.Collections;

///DUPLICATE?????
namespace MTData.Common.Utilities
{
    /// <summary>
    /// This will contain the information regarding each performance entry in the stack
    /// </summary>
    [Obsolete]
    public class PerformanceLogEntry
    {
        private string _typeName;
        private string _commandName;
        private long _startTicks;
        private long _endTicks;

        #region Construction

        public PerformanceLogEntry(string commandName, string typeName)
        {
            this._commandName = commandName;
            this._typeName = typeName;
        }

        #endregion

        #region Methods

        public void Start()
        {
            this._startTicks = DateTime.Now.Ticks;
        }

        public void Stop()
        {
            this._endTicks = DateTime.Now.Ticks;
        }
        #endregion

        #region Properties

        public string TypeName
        {
            get
            {
                return this._typeName;
            }
        }

        public string CommandName
        {
            get
            {
                return this._commandName;
            }

        }

        public long DurationTicks
        {
            get
            {
                return (this._endTicks - this._startTicks);
            }
        }

        #endregion
    }

    /// <summary>
    /// This class will allow the logging of actions, and the recording of time taken to perform them.
    /// </summary>
    [Obsolete]
    public class PerformanceLogging
    {
        private static Stack _stack = new Stack();
        private static ArrayList _output = new ArrayList();

        private static string _fileName = @"C:\TrackingPerformance.log";

        private static bool _suspend = GetInitialSuspend();

        public PerformanceLogging()
        {

        }

        private static bool GetInitialSuspend()
        {
            return false;
        }

        private static string GetFileName()
        {
            //			if (_fileName == "")
            //			{
            //				Config config = new Config();
            //				_fileName = config.GetStringValue("PerformanceLogging/Path");
            //			}
            return _fileName;
        }

        public static void Push(System.Type type, string command)
        {
            Push(type.Name, command);
        }

        public static void Push(string typeName, string command)
        {
            if (!_suspend)
            {
                PerformanceLogEntry entry = new PerformanceLogEntry(command, typeName);

                StringBuilder builder = new StringBuilder();
                for (int loop = 0; loop < _stack.Count; loop++)
                    builder.Append("\t");
                builder.Append(typeName);
                builder.Append(" : ");
                builder.Append(command);
                lock (_output.SyncRoot)
                {
                    _output.Add(builder.ToString());
                    entry.Start();
                    _stack.Push(entry);
                }
            }
        }

        public static void Pop(System.Type type, string command)
        {
            Pop(type.Name, command);
        }

        public static void Pop(string typeName, string command)
        {
            if (!_suspend)
            {
                lock (_output.SyncRoot)
                {
                    PerformanceLogEntry entry = (PerformanceLogEntry)_stack.Pop();
                    if ((entry.CommandName != command) || (entry.TypeName != typeName))
                        throw new Exception(string.Format("Performance Log Entry Mismatch. Log Entry {1}[{2}] does not match {3}[{4}]", entry.CommandName, entry.TypeName, command, typeName));

                    entry.Stop();

                    StringBuilder builder = new StringBuilder();
                    for (int loop = 0; loop < _stack.Count; loop++)
                        builder.Append("\t");
                    builder.Append(typeName);
                    builder.Append(" : ");
                    builder.Append(command);
                    builder.Append(" : Duration ");
                    builder.Append(entry.DurationTicks.ToString());
                    builder.Append(" (100ns)");
                    _output.Add(builder.ToString());

                    if (_stack.Count == 0)
                    {
                        StreamWriter writer = new StreamWriter(GetFileName(), true, System.Text.Encoding.Unicode);
                        try
                        {
                            foreach (string line in _output)
                                writer.WriteLine(line);

                            _output.Clear();
                        }
                        finally
                        {
                            writer.Close();
                        }
                    }
                }
            }
        }

        public static void PushMessage(System.Type type, string message)
        {
            if (!_suspend)
            {
                StringBuilder builder = new StringBuilder();
                for (int loop = 0; loop < _stack.Count; loop++)
                    builder.Append("\t");
                builder.Append(type.Name);
                builder.Append(" : ");
                builder.Append(message);
                lock (_output.SyncRoot)
                {
                    _output.Add("=========================================================================");
                    _output.Add(builder.ToString());
                    _output.Add("=========================================================================");
                }
            }
        }

        public static bool Suspend(bool suspend)
        {
            bool result = _suspend;
            _suspend = suspend;
            return result;
        }
    }
}

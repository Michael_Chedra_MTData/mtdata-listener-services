using System;
using System.Collections;
using System.IO;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for FileUtilities.
    /// </summary>
    [Obsolete]
    public class FileUtilities
    {
        public FileUtilities()
        {
        }

        public static ArrayList ReadFileToArrayOfLines(string sFilePath)
        {
            ArrayList oRet = null;
            FileStream oFS = null;
            byte[] bData = null;
            byte[] bEOL = null;
            byte[] bExtractData = null;
            byte bLookingFor = (byte)0x00;
            int iEOLPos = 0;
            int iStartPos = 0;
            int iPos = 0;
            int iExtractPos = 0;

            try
            {
                oRet = new ArrayList();
                if (File.Exists(sFilePath))
                {
                    oFS = new FileStream(sFilePath, FileMode.Open);
                    bData = new byte[Convert.ToInt32(oFS.Length)];
                    oFS.Read(bData, 0, bData.Length);
                    oFS.Close();
                    oFS = null;
                }
                if (bData != null)
                {
                    bEOL = System.Text.ASCIIEncoding.ASCII.GetBytes("\r\n");
                    bLookingFor = bEOL[iEOLPos];
                    iStartPos = 0;

                    for (iPos = 0; iPos < bData.Length; iPos++)
                    {
                        if (bData[iPos] == bLookingFor)
                        {
                            if (iEOLPos == (bEOL.Length - 1))
                            {
                                iExtractPos = 0;
                                bExtractData = new byte[(iPos - (bEOL.Length - 1)) - iStartPos];
                                for (int X = iStartPos; X < (iPos - (bEOL.Length - 1)); X++)
                                    bExtractData[iExtractPos++] = bData[X];
                                oRet.Add(System.Text.ASCIIEncoding.ASCII.GetString(bExtractData, 0, bExtractData.Length));
                                iEOLPos = 0;
                                iStartPos = iPos + 1;
                            }
                            else
                                iEOLPos++;
                            bLookingFor = bEOL[iEOLPos];
                        }
                    }
                    if (iStartPos < iPos)
                    {
                        bExtractData = new byte[(iPos - bEOL.Length) - iStartPos];
                        for (int X = iStartPos; X < (iPos - bEOL.Length); X++)
                            bExtractData[iExtractPos++] = bData[X];
                        oRet.Add(System.Text.ASCIIEncoding.ASCII.GetString(bExtractData, 0, bExtractData.Length));
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return oRet;
        }

    }
}

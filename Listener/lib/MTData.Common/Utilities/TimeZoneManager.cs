using System;
using System.Collections;
using System.Data;
using System.Linq;

namespace MTData.Common.Utilities
{
    public class TimeZoneManager
    {
        public delegate void TimeZoneDatasetChangedDelegate();
        public event TimeZoneDatasetChangedDelegate TimeZoneDatasetChanged;
        public delegate void TimeZoneToDriverChangedDelegate(int iFleetID, int iDriverID);
        public event TimeZoneToDriverChangedDelegate TimeZoneToDriverChanged;
        public delegate void TimeZoneToFleetChangedDelegate(int iFleetID);
        public event TimeZoneToFleetChangedDelegate TimeZoneToFleetChanged;
        public delegate void TimeZoneToVehicleChangedDelegate(int iFleetID, int iVehicleID);
        public event TimeZoneToVehicleChangedDelegate TimeZoneToVehicleChanged;
        public delegate void TimeZoneToUserChangedDelegate(int iUserID);
        public event TimeZoneToUserChangedDelegate TimeZoneToUserChanged;
        public delegate void TimeZoneToTrailerChangedDelegate(int iFleetID, int iTrailerID);
        public event TimeZoneToTrailerChangedDelegate TimeZoneToTrailerChanged;

        private Hashtable oTimeZones = null;
        private Hashtable oTimeZoneToVehicle = null;
        private Hashtable oTimeZoneToFleet = null;
        private Hashtable oTimeZoneToDriver = null;
        private Hashtable oTimeZoneToTrailer = null;
        private Hashtable oTimeZoneToUser = null;

        public TimeZoneManager()
        {
            oTimeZones = Hashtable.Synchronized(new Hashtable());
            oTimeZoneToVehicle = Hashtable.Synchronized(new Hashtable());
            oTimeZoneToFleet = Hashtable.Synchronized(new Hashtable());
            oTimeZoneToDriver = Hashtable.Synchronized(new Hashtable());
            oTimeZoneToTrailer = Hashtable.Synchronized(new Hashtable());
            oTimeZoneToUser = Hashtable.Synchronized(new Hashtable());
            // dsTimeZones
            // Table[0] = T_Timezones
            // Table[1] = T_TimezoneDaylightSaving
            // Table[2] = T_TimezoneToVehicle
            // Table[3] = T_TimezoneToFleetDefault
            // Table[4] = T_TimezoneToDriver
            // Table[5] = T_TimezoneToTrailer
            // Table[6] = T_TimezoneToUser

        }

        public TimeZoneManager(Hashtable tzList, Hashtable tzToVehicle, Hashtable tzToFleet, Hashtable tzToDriver, Hashtable tzToTrailer, Hashtable tzToUser)
        {
            oTimeZones = tzList;
            oTimeZoneToVehicle = tzToVehicle;
            oTimeZoneToFleet = tzToFleet;
            oTimeZoneToDriver = tzToDriver;
            oTimeZoneToTrailer = tzToTrailer;
            oTimeZoneToUser = tzToUser;
        }

        public TimeZoneManager(bool bShowOffsetInName, DataSet dsTimeZones, string sErrMsg)
        {
            DataTable dtTimezones = null;
            DataTable dtTimezoneDaylightSaving = null;
            DataTable dtTimezoneToVehicle = null;
            DataTable dtTimezoneToFleetDefault = null;
            DataTable dtTimezoneToDriver = null;
            DataTable dtTimezoneToTrailer = null;
            DataTable dtTimezoneToUser = null;

            // dsTimeZones
            // Table[0] = T_Timezones
            // Table[1] = T_TimezoneDaylightSaving
            // Table[2] = T_TimezoneToVehicle
            // Table[3] = T_TimezoneToFleetDefault
            // Table[4] = T_TimezoneToDriver
            // Table[5] = T_TimezoneToTrailer
            // Table[6] = T_TimezoneToUser

            if (dsTimeZones != null)
            {
                if (dsTimeZones.Tables.Count > 0) dtTimezones = dsTimeZones.Tables[0];
                if (dsTimeZones.Tables.Count > 1) dtTimezoneDaylightSaving = dsTimeZones.Tables[1];
                if (dsTimeZones.Tables.Count > 2) dtTimezoneToVehicle = dsTimeZones.Tables[2];
                if (dsTimeZones.Tables.Count > 3) dtTimezoneToFleetDefault = dsTimeZones.Tables[3];
                if (dsTimeZones.Tables.Count > 4) dtTimezoneToDriver = dsTimeZones.Tables[4];
                if (dsTimeZones.Tables.Count > 5) dtTimezoneToTrailer = dsTimeZones.Tables[5];
                if (dsTimeZones.Tables.Count > 6) dtTimezoneToUser = dsTimeZones.Tables[6];
            }
            GetTimeZoneObjects(bShowOffsetInName, dtTimezones, dtTimezoneDaylightSaving, dtTimezoneToVehicle, dtTimezoneToFleetDefault, dtTimezoneToDriver, dtTimezoneToTrailer, dtTimezoneToUser, sErrMsg);
        }
        public TimeZoneManager(bool bShowOffsetInName, DataTable dtTimezones, DataTable dtTimezoneDaylightSaving, DataTable dtTimezoneToVehicle, DataTable dtTimezoneToFleetDefault, DataTable dtTimezoneToDriver, DataTable dtTimezoneToTrailer, DataTable dtTimezoneToUser, string sErrMsg)
        {
            GetTimeZoneObjects(bShowOffsetInName, dtTimezones, dtTimezoneDaylightSaving, dtTimezoneToVehicle, dtTimezoneToFleetDefault, dtTimezoneToDriver, dtTimezoneToTrailer, dtTimezoneToUser, sErrMsg);
        }

        public ulong GetTimeZoneKey(int iFleetID, int iObjectID)
        {
            byte[] bFleetID = new byte[4];
            byte[] bObjectID = new byte[4];
            byte[] bData = new byte[8];

            try
            {
                bFleetID = BitConverter.GetBytes(iFleetID);
                bObjectID = BitConverter.GetBytes(iObjectID);
                bData[0] = bFleetID[0];
                bData[1] = bFleetID[1];
                bData[2] = bFleetID[2];
                bData[3] = bFleetID[3];
                bData[4] = bObjectID[0];
                bData[5] = bObjectID[1];
                bData[6] = bObjectID[2];
                bData[7] = bObjectID[3];
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return BitConverter.ToUInt64(bData, 0);
        }
        public void GetTimeZoneObjects(bool bShowOffsetInName, DataTable dtTimezones, DataTable dtTimezoneDaylightSaving, DataTable dtTimezoneToVehicle, DataTable dtTimezoneToFleetDefault, DataTable dtTimezoneToDriver, DataTable dtTimezoneToTrailer, DataTable dtTimezoneToUser, string sErrMsg)
        {
            DataTable dtDLS = null;
            DataRow[] drDLS = null;
            int iTimeZoneID = 0;
            int iOffset = 0;
            int iFleetID = 0;
            int iObjectID = 0;
            ulong iHashKey = 0;
            TimeZone oTimeZone = null;
            string sZoneName = "";

            try
            {
                oTimeZones = Hashtable.Synchronized(new Hashtable());
                oTimeZoneToVehicle = Hashtable.Synchronized(new Hashtable());
                oTimeZoneToFleet = Hashtable.Synchronized(new Hashtable());
                oTimeZoneToDriver = Hashtable.Synchronized(new Hashtable());
                oTimeZoneToTrailer = Hashtable.Synchronized(new Hashtable());
                oTimeZoneToUser = Hashtable.Synchronized(new Hashtable());

                if (dtTimezones != null)
                {
                    if (dtTimezones != null)
                    {
                        for (int X = 0; X < dtTimezones.Rows.Count; X++)
                        {
                            #region For Each Time Zone
                            try
                            {
                                iTimeZoneID = Convert.ToInt32(dtTimezones.Rows[X]["ID"]);
                                iOffset = Convert.ToInt32(dtTimezones.Rows[X]["Offset"]);
                                sZoneName = Convert.ToString(dtTimezones.Rows[X]["Name"]);
                                dtDLS = null;
                                if (dtTimezoneDaylightSaving != null)
                                {
                                    dtDLS = dtTimezoneDaylightSaving.Clone();
                                    drDLS = dtTimezoneDaylightSaving.Select("TimeZoneID = " + Convert.ToInt32(iTimeZoneID));
                                    if (drDLS.Length > 0)
                                    {
                                        #region For each day light savings time period
                                        for (int iRow = 0; iRow < drDLS.Length; iRow++)
                                        {
                                            try
                                            {
                                                DataRow row = dtDLS.NewRow();
                                                for (int iCol = 0; iCol < dtDLS.Columns.Count; iCol++)
                                                {
                                                    row[iCol] = drDLS[iRow][iCol];
                                                }
                                                dtDLS.Rows.Add(row);
                                                dtDLS.AcceptChanges();
                                            }
                                            catch (System.Exception exLoadDLS)
                                            {
                                                sErrMsg += "Error loading DLS : " + exLoadDLS.Message + "\r\n";
                                            }
                                        }
                                        #endregion
                                    }
                                }
                                oTimeZone = new TimeZone(iTimeZoneID, sZoneName, iOffset, dtDLS, bShowOffsetInName);
                                lock (oTimeZones.SyncRoot)
                                {
                                    oTimeZones.Add(iTimeZoneID, oTimeZone);
                                }
                            }
                            catch (System.Exception exLoadTable)
                            {
                                sErrMsg += "Error loading Time Zones : " + exLoadTable.Message + "\r\n";
                            }
                            #endregion
                        }
                    }
                    if (dtTimezoneToVehicle != null)
                    {
                        for (int X = 0; X < dtTimezoneToVehicle.Rows.Count; X++)
                        {
                            #region For Each Time Zone to Vehicle Entry
                            try
                            {
                                iFleetID = Convert.ToInt32(dtTimezoneToVehicle.Rows[X]["FleetID"]);
                                iObjectID = Convert.ToInt32(dtTimezoneToVehicle.Rows[X]["VehicleID"]);
                                iTimeZoneID = Convert.ToInt32(dtTimezoneToVehicle.Rows[X]["TimeZoneID"]);
                                iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                                lock (oTimeZoneToVehicle.SyncRoot)
                                    if (!oTimeZoneToVehicle.ContainsKey(iHashKey))
                                        oTimeZoneToVehicle.Add(iHashKey, iTimeZoneID);
                            }
                            catch (System.Exception exLoadTable)
                            {
                                sErrMsg += "Error loading Time Zone to Vehilce entries : " + exLoadTable.Message + "\r\n";
                            }
                            #endregion
                        }
                    }
                    if (dtTimezoneToFleetDefault != null)
                    {
                        for (int X = 0; X < dtTimezoneToFleetDefault.Rows.Count; X++)
                        {
                            #region For Each Time Zone to Fleet Entry
                            try
                            {
                                iFleetID = Convert.ToInt32(dtTimezoneToFleetDefault.Rows[X]["FleetID"]);
                                iTimeZoneID = Convert.ToInt32(dtTimezoneToFleetDefault.Rows[X]["TimeZoneID"]);
                                iHashKey = GetTimeZoneKey(iFleetID, 0);
                                lock (oTimeZoneToFleet.SyncRoot)
                                    if (!oTimeZoneToFleet.ContainsKey(iHashKey))
                                        oTimeZoneToFleet.Add(iHashKey, iTimeZoneID);
                            }
                            catch (System.Exception exLoadTable)
                            {
                                sErrMsg += "Error loading Time Zone to Fleet entries : " + exLoadTable.Message + "\r\n";
                            }
                            #endregion
                        }
                    }
                    if (dtTimezoneToDriver != null)
                    {
                        for (int X = 0; X < dtTimezoneToDriver.Rows.Count; X++)
                        {
                            #region For Each Time Zone to Driver Entry
                            try
                            {
                                iFleetID = Convert.ToInt32(dtTimezoneToDriver.Rows[X]["FleetID"]);
                                iObjectID = Convert.ToInt32(dtTimezoneToDriver.Rows[X]["DriverID"]);
                                iTimeZoneID = Convert.ToInt32(dtTimezoneToDriver.Rows[X]["TimeZoneID"]);
                                iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                                lock (oTimeZoneToDriver.SyncRoot)
                                    if (!oTimeZoneToDriver.ContainsKey(iHashKey))
                                        oTimeZoneToDriver.Add(iHashKey, iTimeZoneID);
                            }
                            catch (System.Exception exLoadTable)
                            {
                                sErrMsg += "Error loading Time Zone to Driver entries : " + exLoadTable.Message + "\r\n";
                            }
                            #endregion
                        }
                    }
                    if (dtTimezoneToTrailer != null)
                    {
                        for (int X = 0; X < dtTimezoneToTrailer.Rows.Count; X++)
                        {
                            #region For Each Time Zone to Trailer Entry
                            try
                            {
                                iFleetID = Convert.ToInt32(dtTimezoneToTrailer.Rows[X]["FleetID"]);
                                iObjectID = Convert.ToInt32(dtTimezoneToTrailer.Rows[X]["TrailerID"]);
                                iTimeZoneID = Convert.ToInt32(dtTimezoneToTrailer.Rows[X]["TimeZoneID"]);
                                iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                                lock (oTimeZoneToTrailer.SyncRoot)
                                    if (!oTimeZoneToTrailer.ContainsKey(iHashKey))
                                        oTimeZoneToTrailer.Add(iHashKey, iTimeZoneID);
                            }
                            catch (System.Exception exLoadTable)
                            {
                                sErrMsg += "Error loading Time Zone to Trailer entries : " + exLoadTable.Message + "\r\n";
                            }
                            #endregion
                        }
                    }
                    if (dtTimezoneToUser != null)
                    {
                        for (int X = 0; X < dtTimezoneToUser.Rows.Count; X++)
                        {
                            #region For Each Time Zone to User Entry
                            try
                            {
                                iFleetID = 0;
                                iObjectID = Convert.ToInt32(dtTimezoneToUser.Rows[X]["UserID"]);
                                iTimeZoneID = Convert.ToInt32(dtTimezoneToUser.Rows[X]["TimeZoneID"]);
                                iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                                lock (oTimeZoneToUser.SyncRoot)
                                    if (!oTimeZoneToUser.ContainsKey(iHashKey))
                                        oTimeZoneToUser.Add(iHashKey, iTimeZoneID);
                            }
                            catch (System.Exception exLoadTable)
                            {
                                sErrMsg += "Error loading Time Zone to User entries : " + exLoadTable.Message + "\r\n";
                            }
                            #endregion
                        }
                    }
                }
                if (TimeZoneDatasetChanged != null)
                    TimeZoneDatasetChanged();
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
        }

        public DateTime GetLocalTimeForDriver(int iFleetID, int iDriverID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iDriverID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToDriver.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToDriver[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetLocalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetLocalTimeForFleet(int iFleetID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, 0);
                lock (oTimeZoneToFleet.SyncRoot)
                {
                    if (oTimeZoneToFleet.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetLocalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetLocalTimeForVehicle(int iFleetID, int iVehicleID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iVehicleID);
                lock (oTimeZoneToVehicle.SyncRoot)
                {
                    if (oTimeZoneToVehicle.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToVehicle[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetLocalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetLocalTimeForTrailer(int iFleetID, int iTrailerID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iTrailerID);
                lock (oTimeZoneToTrailer.SyncRoot)
                {
                    if (oTimeZoneToTrailer.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToTrailer[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetLocalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetLocalTimeForUser(int iUserID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(0, iUserID);
                lock (oTimeZoneToUser.SyncRoot)
                {
                    if (oTimeZoneToUser.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToUser[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetLocalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetLocalTimeForTimeZone(int iTimeZone, DateTime dtUTCTime)
        {
            DateTime dtRet = dtUTCTime;
            TimeZone oTimeZone = null;
            try
            {
                lock (oTimeZones.SyncRoot)
                {
                    if (oTimeZones.ContainsKey(iTimeZone))
                        oTimeZone = (TimeZone)oTimeZones[iTimeZone];
                }
                if (oTimeZone != null)
                {
                    dtRet = oTimeZone.GetLocalTime(dtUTCTime);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetUniversalTimeForDriver(int iFleetID, int iDriverID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iDriverID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToDriver.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToDriver[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetUniversalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetUniversalTimeForFleet(int iFleetID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, 0);
                lock (oTimeZoneToFleet.SyncRoot)
                {
                    if (oTimeZoneToFleet.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetUniversalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetUniversalTimeForVehicle(int iFleetID, int iVehicleID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iVehicleID);
                lock (oTimeZoneToVehicle.SyncRoot)
                {
                    if (oTimeZoneToVehicle.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToVehicle[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetUniversalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetUniversalTimeForTrailer(int iFleetID, int iTrailerID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iTrailerID);
                lock (oTimeZoneToTrailer.SyncRoot)
                {
                    if (oTimeZoneToTrailer.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToTrailer[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetUniversalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetUniversalTimeForUser(int iUserID, DateTime dtUTCTime)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            DateTime dtRet = dtUTCTime;
            try
            {
                iHashKey = GetTimeZoneKey(0, iUserID);
                lock (oTimeZoneToUser.SyncRoot)
                {
                    if (oTimeZoneToUser.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToUser[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                dtRet = GetUniversalTimeForTimeZone(iTimeZoneID, dtUTCTime);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }
        public DateTime GetUniversalTimeForTimeZone(int iTimeZone, DateTime dtUTCTime)
        {
            DateTime dtRet = dtUTCTime;
            TimeZone oTimeZone = null;
            try
            {
                lock (oTimeZones.SyncRoot)
                {
                    if (oTimeZones.ContainsKey(iTimeZone))
                        oTimeZone = (TimeZone)oTimeZones[iTimeZone];
                }
                if (oTimeZone != null)
                {
                    dtRet = oTimeZone.GetUniversalTime(dtUTCTime);
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return dtRet;
        }

        public DateTime GetUniversalTimeForTimeZone(string timeZoneName, DateTime dtUTCTime)
        {
            if (string.IsNullOrWhiteSpace(timeZoneName))
            {
                throw new ArgumentNullException("Timezone name is null");
            }

            var q = from n in oTimeZones.Values.OfType<TimeZone>()
                    where n.TimeZoneName.StartsWith(timeZoneName)
                    select n;

            return q.First().GetUniversalTime(dtUTCTime);
        }

        public string GetTimeZoneNameForDriver(int iFleetID, int iDriverID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            string sRet = "";
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iDriverID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToDriver.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToDriver[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                sRet = GetTimeZoneName(iTimeZoneID);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return sRet;
        }
        public string GetTimeZoneNameForFleet(int iFleetID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            string sRet = "";
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, 0);
                lock (oTimeZoneToFleet.SyncRoot)
                {
                    if (oTimeZoneToFleet.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                sRet = GetTimeZoneName(iTimeZoneID);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return sRet;
        }

        public string GetTimeZoneNameForVehicle(int iFleetID, int iVehicleID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            string sRet = "";
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iVehicleID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToVehicle.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToVehicle[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                sRet = GetTimeZoneName(iTimeZoneID);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return sRet;
        }

        public string GetTimeZoneNameForTrailer(int iFleetID, int iTrailerID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            string sRet = "";
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iTrailerID);
                lock (oTimeZoneToTrailer.SyncRoot)
                {
                    if (oTimeZoneToTrailer.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToTrailer[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                sRet = GetTimeZoneName(iTimeZoneID);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return sRet;
        }
        public string GetTimeZoneNameForUser(int iUserID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            string sRet = "";
            try
            {
                iHashKey = GetTimeZoneKey(0, iUserID);
                lock (oTimeZoneToUser.SyncRoot)
                {
                    if (oTimeZoneToUser.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToUser[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                sRet = GetTimeZoneName(iTimeZoneID);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return sRet;
        }
        public string GetTimeZoneName(int iTimeZone)
        {
            string sRet = "";
            TimeZone oTimeZone = null;
            try
            {
                lock (oTimeZones.SyncRoot)
                {
                    if (oTimeZones.ContainsKey(iTimeZone))
                        oTimeZone = (TimeZone)oTimeZones[iTimeZone];
                }
                if (oTimeZone != null)
                {
                    sRet = oTimeZone.TimeZoneName;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return sRet;
        }

        public int GetTimeZoneID(string timezoneName)
        {
            if (string.IsNullOrWhiteSpace(timezoneName))
            {
                throw new ArgumentNullException("Timezone name is null");
            }

            var q = from n in oTimeZones.Values.OfType<TimeZone>()
                    where n.TimeZoneName.StartsWith(timezoneName)
                    select n.ID;

            return q.First();
        }

        public int GetTimeZoneIDForDriver(int iFleetID, int iDriverID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iDriverID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToDriver.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToDriver[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iTimeZoneID;
        }
        public int GetTimeZoneIDForFleet(int iFleetID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, 0);
                lock (oTimeZoneToFleet.SyncRoot)
                {
                    if (oTimeZoneToFleet.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                    }
                    if (iTimeZoneID == -1)
                    {
                        iHashKey = GetTimeZoneKey(0, 0);
                        lock (oTimeZoneToFleet.SyncRoot)
                        {
                            if (oTimeZoneToFleet.ContainsKey(iHashKey))
                            {
                                iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                            }
                        }
                    }
                    if (iTimeZoneID == -1)
                    {
                        iTimeZoneID = 5;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iTimeZoneID;
        }

        public int GetTimeZoneIDForVehicle(int iFleetID, int iVehicleID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iVehicleID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToVehicle.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToVehicle[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iTimeZoneID;
        }

        public int GetTimeZoneIDForTrailer(int iFleetID, int iTrailerID)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iTrailerID);
                lock (oTimeZoneToTrailer.SyncRoot)
                {
                    if (oTimeZoneToTrailer.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToTrailer[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iTimeZoneID;
        }
        public int GetTimeZoneIDForUser(int iUserID, bool useDefaultIfNonExisting = true)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            try
            {
                iHashKey = GetTimeZoneKey(0, iUserID);
                lock (oTimeZoneToUser.SyncRoot)
                {
                    if (oTimeZoneToUser.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToUser[iHashKey];
                    }
                }

                if (useDefaultIfNonExisting)
                {
                    if (iTimeZoneID == -1)
                    {
                        iHashKey = GetTimeZoneKey(0, 0);
                        lock (oTimeZoneToFleet.SyncRoot)
                        {
                            if (oTimeZoneToFleet.ContainsKey(iHashKey))
                            {
                                iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                            }
                        }
                    }
                    if (iTimeZoneID == -1)
                    {
                        iTimeZoneID = 5;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iTimeZoneID;
        }

        public bool UpdateTimeZoneForDriver(int iFleetID, int iDriverID, int iTimeZoneID)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iDriverID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToDriver.ContainsKey(iHashKey))
                    {
                        oTimeZoneToDriver.Remove(iHashKey);
                    }
                    oTimeZoneToDriver.Add(iHashKey, iTimeZoneID);
                    if (TimeZoneToDriverChanged != null)
                        TimeZoneToDriverChanged(iFleetID, iDriverID);
                    bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }
        public bool UpdateTimeZoneForFleet(int iFleetID, int iTimeZoneID)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, 0);
                lock (oTimeZoneToFleet.SyncRoot)
                {
                    if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        oTimeZoneToFleet.Remove(iHashKey);
                    oTimeZoneToFleet.Add(iHashKey, iTimeZoneID);
                    if (TimeZoneToFleetChanged != null)
                        TimeZoneToFleetChanged(iFleetID);
                    bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }
        public bool UpdateTimeZoneForVehicle(int iFleetID, int iVehicleID, int iTimeZoneID)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iVehicleID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToVehicle.ContainsKey(iHashKey))
                        oTimeZoneToVehicle.Remove(iHashKey);
                    oTimeZoneToVehicle.Add(iHashKey, iTimeZoneID);
                    if (TimeZoneToVehicleChanged != null)
                        TimeZoneToVehicleChanged(iFleetID, iVehicleID);
                    bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }

        public bool UpdateTimeZoneForTrailer(int iFleetID, int iTrailerID, int iTimeZoneID)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iTrailerID);
                lock (oTimeZoneToTrailer.SyncRoot)
                {
                    if (oTimeZoneToTrailer.ContainsKey(iHashKey))
                        oTimeZoneToTrailer.Remove(iHashKey);
                    oTimeZoneToTrailer.Add(iHashKey, iTimeZoneID);
                    if (TimeZoneToTrailerChanged != null)
                        TimeZoneToTrailerChanged(iFleetID, iTrailerID);
                    bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }
        public bool UpdateTimeZoneForUser(int iUserID, int iTimeZoneID)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            try
            {
                iHashKey = GetTimeZoneKey(0, iUserID);
                lock (oTimeZoneToUser.SyncRoot)
                {
                    if (oTimeZoneToUser.ContainsKey(iHashKey))
                        oTimeZoneToUser.Remove(iHashKey);
                    oTimeZoneToUser.Add(iHashKey, iTimeZoneID);
                    if (TimeZoneToUserChanged != null)
                        TimeZoneToUserChanged(iUserID);
                    bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }

        public bool UpdateTimeZoneTableForTimeZones(DataTable dtTimezones, DataTable dtTimezoneDaylightSaving, bool bShowOffsetInName)
        {
            bool bRet = false;
            DataTable dtDLS = null;
            DataRow[] drDLS = null;
            int iTimeZoneID = 0;
            int iOffset = 0;
            TimeZone oTimeZone = null;
            string sZoneName = "";

            try
            {
                oTimeZones = Hashtable.Synchronized(new Hashtable());
                if (dtTimezones != null)
                {
                    for (int X = 0; X < dtTimezones.Rows.Count; X++)
                    {
                        #region For Each Time Zone
                        iTimeZoneID = Convert.ToInt32(dtTimezones.Rows[X]["ID"]);
                        iOffset = Convert.ToInt32(dtTimezones.Rows[X]["Offset"]);
                        sZoneName = Convert.ToString(dtTimezones.Rows[X]["Name"]);
                        dtDLS = null;
                        if (dtTimezoneDaylightSaving != null)
                        {
                            dtDLS = dtTimezoneDaylightSaving.Clone();
                            drDLS = dtTimezoneDaylightSaving.Select("TimeZoneID = " + Convert.ToInt32(iTimeZoneID));
                            if (drDLS.Length > 0)
                            {
                                #region For each day light savings time period
                                for (int iRow = 0; iRow < drDLS.Length; iRow++)
                                {
                                    DataRow row = dtDLS.NewRow();
                                    for (int iCol = 0; iCol < dtDLS.Columns.Count; iCol++)
                                    {
                                        row[iCol] = drDLS[iRow][iCol];
                                    }
                                    dtDLS.Rows.Add(row);
                                    dtDLS.AcceptChanges();
                                }
                                #endregion
                            }
                        }
                        oTimeZone = new TimeZone(iTimeZoneID, sZoneName, iOffset, dtDLS, bShowOffsetInName);
                        lock (oTimeZones.SyncRoot)
                        {
                            oTimeZones.Add(iTimeZoneID, oTimeZone);
                        }
                        #endregion
                    }
                }
                bRet = true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }

        public bool UpdateTimeZoneTableForDriver(DataTable dtTimezoneToDriver)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            int iFleetID = 0;
            int iObjectID = 0;
            int iTimeZoneID = 0;
            try
            {
                oTimeZoneToDriver = Hashtable.Synchronized(new Hashtable());
                if (dtTimezoneToDriver != null)
                {
                    for (int X = 0; X < dtTimezoneToDriver.Rows.Count; X++)
                    {
                        #region For Each Time Zone to Driver Entry
                        iFleetID = Convert.ToInt32(dtTimezoneToDriver.Rows[X]["FleetID"]);
                        iObjectID = Convert.ToInt32(dtTimezoneToDriver.Rows[X]["DriverID"]);
                        iTimeZoneID = Convert.ToInt32(dtTimezoneToDriver.Rows[X]["TimeZoneID"]);
                        iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                        lock (oTimeZoneToDriver.SyncRoot)
                            if (!oTimeZoneToDriver.ContainsKey(iHashKey))
                                oTimeZoneToDriver.Add(iHashKey, iTimeZoneID);
                        #endregion
                    }
                }
                bRet = true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }
        public bool UpdateTimeZoneTableForFleet(DataTable dtTimezoneToFleetDefault)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            int iFleetID = 0;
            int iTimeZoneID = 0;
            try
            {
                oTimeZoneToFleet = Hashtable.Synchronized(new Hashtable());
                if (dtTimezoneToFleetDefault != null)
                {
                    for (int X = 0; X < dtTimezoneToFleetDefault.Rows.Count; X++)
                    {
                        #region For Each Time Zone to Fleet Entry
                        iFleetID = Convert.ToInt32(dtTimezoneToFleetDefault.Rows[X]["FleetID"]);
                        iTimeZoneID = Convert.ToInt32(dtTimezoneToFleetDefault.Rows[X]["TimeZoneID"]);
                        iHashKey = GetTimeZoneKey(iFleetID, 0);
                        lock (oTimeZoneToFleet.SyncRoot)
                            if (!oTimeZoneToFleet.ContainsKey(iHashKey))
                                oTimeZoneToFleet.Add(iHashKey, iTimeZoneID);
                        #endregion
                    }
                }
                bRet = true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }
        public bool UpdateTimeZoneTableForVehicle(DataTable dtTimezoneToVehicle)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            int iFleetID = 0;
            int iObjectID = 0;
            int iTimeZoneID = 0;
            try
            {
                oTimeZoneToVehicle = Hashtable.Synchronized(new Hashtable());
                if (dtTimezoneToVehicle != null)
                {
                    for (int X = 0; X < dtTimezoneToVehicle.Rows.Count; X++)
                    {
                        #region For Each Time Zone to Vehicle Entry
                        iFleetID = Convert.ToInt32(dtTimezoneToVehicle.Rows[X]["FleetID"]);
                        iObjectID = Convert.ToInt32(dtTimezoneToVehicle.Rows[X]["VehicleID"]);
                        iTimeZoneID = Convert.ToInt32(dtTimezoneToVehicle.Rows[X]["TimeZoneID"]);
                        iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                        lock (oTimeZoneToVehicle.SyncRoot)
                            if (!oTimeZoneToVehicle.ContainsKey(iHashKey))
                                oTimeZoneToVehicle.Add(iHashKey, iTimeZoneID);
                        #endregion
                    }
                }
                bRet = true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }

        public bool UpdateTimeZoneTableForTrailer(DataTable dtTimezoneToTrailer)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            int iFleetID = 0;
            int iObjectID = 0;
            int iTimeZoneID = 0;
            try
            {
                oTimeZoneToTrailer = Hashtable.Synchronized(new Hashtable());
                if (dtTimezoneToTrailer != null)
                {
                    for (int X = 0; X < dtTimezoneToTrailer.Rows.Count; X++)
                    {
                        #region For Each Time Zone to Trailer Entry
                        iFleetID = Convert.ToInt32(dtTimezoneToTrailer.Rows[X]["FleetID"]);
                        iObjectID = Convert.ToInt32(dtTimezoneToTrailer.Rows[X]["TrailerID"]);
                        iTimeZoneID = Convert.ToInt32(dtTimezoneToTrailer.Rows[X]["TimeZoneID"]);
                        iHashKey = GetTimeZoneKey(iFleetID, iObjectID);
                        lock (oTimeZoneToTrailer.SyncRoot)
                            if (!oTimeZoneToTrailer.ContainsKey(iHashKey))
                                oTimeZoneToTrailer.Add(iHashKey, iTimeZoneID);
                        #endregion
                    }
                }
                bRet = true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }
        public bool UpdateTimeZoneTableForUser(DataTable dtTimezoneToUser)
        {
            bool bRet = false;
            ulong iHashKey = 0;
            int iObjectID = 0;
            int iTimeZoneID = 0;
            try
            {
                oTimeZoneToUser = Hashtable.Synchronized(new Hashtable());
                if (dtTimezoneToUser != null)
                {
                    for (int X = 0; X < dtTimezoneToUser.Rows.Count; X++)
                    {
                        #region For Each Time Zone to User Entry
                        iObjectID = Convert.ToInt32(dtTimezoneToUser.Rows[X]["UserID"]);
                        iTimeZoneID = Convert.ToInt32(dtTimezoneToUser.Rows[X]["TimeZoneID"]);
                        iHashKey = GetTimeZoneKey(0, iObjectID);
                        lock (oTimeZoneToUser.SyncRoot)
                            if (!oTimeZoneToUser.ContainsKey(iHashKey))
                                oTimeZoneToUser.Add(iHashKey, iTimeZoneID);
                        #endregion
                    }
                }
                bRet = true;
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return bRet;
        }

        public int[] GetTimeZoneOffsetsForDriver(int iFleetID, int iDriverID, DateTime dtStartDate, DateTime dtEndDate)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            int[] iRet = null;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iDriverID);
                lock (oTimeZoneToDriver.SyncRoot)
                {
                    if (oTimeZoneToDriver.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToDriver[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                iRet = GetTimeZoneOffsets(iTimeZoneID, dtStartDate, dtEndDate);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }

        public int[] GetTimeZoneOffsetsForFleet(int iFleetID, DateTime dtStartDate, DateTime dtEndDate)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            int[] iRet = null;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, 0);
                lock (oTimeZoneToFleet.SyncRoot)
                {
                    if (oTimeZoneToFleet.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                iRet = GetTimeZoneOffsets(iTimeZoneID, dtStartDate, dtEndDate);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }

        public int[] GetTimeZoneOffsetsForVehicle(int iFleetID, int iVehicleID, DateTime dtStartDate, DateTime dtEndDate)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            int[] iRet = null;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iVehicleID);
                lock (oTimeZoneToVehicle.SyncRoot)
                {
                    if (oTimeZoneToVehicle.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToVehicle[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                iRet = GetTimeZoneOffsets(iTimeZoneID, dtStartDate, dtEndDate);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }

        public int[] GetTimeZoneOffsetsForTrailer(int iFleetID, int iTrailerID, DateTime dtStartDate, DateTime dtEndDate)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            int[] iRet = null;
            try
            {
                iHashKey = GetTimeZoneKey(iFleetID, iTrailerID);
                lock (oTimeZoneToTrailer.SyncRoot)
                {
                    if (oTimeZoneToTrailer.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToTrailer[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(iFleetID, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                iRet = GetTimeZoneOffsets(iTimeZoneID, dtStartDate, dtEndDate);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }

        public int[] GetTimeZoneOffsetsForUser(int iUserID, DateTime dtStartDate, DateTime dtEndDate)
        {
            ulong iHashKey = 0;
            int iTimeZoneID = -1;
            int[] iRet = null;
            try
            {
                iHashKey = GetTimeZoneKey(0, iUserID);
                lock (oTimeZoneToUser.SyncRoot)
                {
                    if (oTimeZoneToUser.ContainsKey(iHashKey))
                    {
                        iTimeZoneID = (int)oTimeZoneToUser[iHashKey];
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iHashKey = GetTimeZoneKey(0, 0);
                    lock (oTimeZoneToFleet.SyncRoot)
                    {
                        if (oTimeZoneToFleet.ContainsKey(iHashKey))
                        {
                            iTimeZoneID = (int)oTimeZoneToFleet[iHashKey];
                        }
                    }
                }
                if (iTimeZoneID == -1)
                {
                    iTimeZoneID = 5;
                }
                iRet = GetTimeZoneOffsets(iTimeZoneID, dtStartDate, dtEndDate);
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }

        public int GetOffsetAtTime(int iTimeZone, DateTime dtUTCDateTime)
        {
            int timeZoneUTCOffset = 0;
            TimeZone oTimeZone = null;

            lock (oTimeZones.SyncRoot)
            {
                if (oTimeZones.ContainsKey(iTimeZone))
                {
                    oTimeZone = (TimeZone)oTimeZones[iTimeZone];
                    timeZoneUTCOffset = oTimeZone.GetOffsetAtTime(dtUTCDateTime);
                }
            }

            return timeZoneUTCOffset;
        }

        public int[] GetTimeZoneOffsets(int iTimeZone, DateTime dtStartDate, DateTime dtEndDate)
        {
            TimeZone oTimeZone = null;
            int[] iRet = null;
            int iStartOffset = 0;
            int iEndOffset = 0;
            try
            {
                lock (oTimeZones.SyncRoot)
                {
                    if (oTimeZones.ContainsKey(iTimeZone))
                        oTimeZone = (TimeZone)oTimeZones[iTimeZone];
                }
                if (oTimeZone != null)
                {
                    iStartOffset = oTimeZone.GetOffsetAtTime(dtStartDate);
                    iEndOffset = oTimeZone.GetOffsetAtTime(dtEndDate);
                    if (iStartOffset == iEndOffset)
                    {
                        iRet = new int[1];
                        iRet[0] = iStartOffset;
                    }
                    else
                    {
                        iRet = new int[2];
                        iRet[0] = iStartOffset;
                        iRet[0] = iEndOffset;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw ex;
            }
            return iRet;
        }
    }
}

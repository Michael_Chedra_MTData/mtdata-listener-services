﻿using System;
using System.ComponentModel;
using System.Linq;

namespace MTData.Common.Utilities
{
    public static class EnumHelper
    {
        public static string GetEnumDescription(this Enum value)
        {
            DescriptionAttribute attribute = value.GetType()
                .GetField(value.ToString())
                .GetCustomAttributes(typeof(DescriptionAttribute), false)
                .SingleOrDefault() as DescriptionAttribute;
            return attribute == null ? value.ToString() : attribute.Description;
        }
    }
}

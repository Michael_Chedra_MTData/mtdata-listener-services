﻿//-----------------------------------------------------------------------
// <copyright file="EmailHelper.cs" company="Mobile Tracking and Data Pty Ltd">
//     Copyright (c) Mobile Tracking and Data Pty Ltd. All rights reserved.
// </copyright>

namespace MTData.Common.Utilities
{
    using System;
    using System.Configuration;
    using System.Diagnostics;
    using Common.Database;
    using log4net;

    /// <summary>
    ///  Helper class for sending database version mismatch email message.
    /// </summary>
    public class EmailHelper : IEmailHelper
    {
        /// <summary>
        /// Logger for logging 
        /// </summary>
        private readonly ILog logger = LogManager.GetLogger(typeof(EmailHelper));

        /// <summary>
        /// Email client to relay email message.
        /// </summary>
        private readonly IEmailClient emailClient;

        /// <summary>
        /// Initializes a new instance of the <see cref="EmailHelper" /> class.
        /// </summary>
        /// <param name="emailClient">Email client to relay email message.</param>
        public EmailHelper(IEmailClient emailClient)
        {
            this.emailClient = emailClient;
        }

        /// <summary>
        /// Send database version error email.
        /// </summary>
        /// <param name="errorMessage">Database mismatch version error message.</param>
        /// <returns>Returns true on success, otherwise return false.</returns>
        public bool SendDatabaseVersionErrorEmail(DatabaseVersionErrorMessage errorMessage)
        {
#if DEBUG
            return true;
#elif !DEBUG
            string emailTo;
            try
            {
                emailTo = ConfigurationManager.AppSettings["DBMismatchEmailTo"];
                if (string.IsNullOrEmpty(emailTo) || !this.IsEmailAddress(emailTo))
                {
                    emailTo = "support@mtdata.com.au";
                }
            }
            catch
            {
                emailTo = "support@mtdata.com.au";
            }

            var from = errorMessage.MachineName + "@mtdata.com.au";
            var fromDisplayName = errorMessage.MachineName + "@mtdata.com.au";
            var subject = errorMessage.Subject;
            var bodyPlain = errorMessage.Message;
            return this.SendEmail(emailTo, from, fromDisplayName, subject, bodyPlain, false);
#endif
        }

        /// <summary>
        /// Sends email message.
        /// </summary>
        /// <param name="toEmailAddress">To email address.</param>
        /// <param name="fromEmailAddress">From email address.</param>
        /// <param name="fromDisplayName">From display name.</param>
        /// <param name="subject">Subject of the email.</param>
        /// <param name="body">Body text of the email.</param>
        /// <param name="writeToEventLog">Flag indicating if the email should be logged to event log.</param>
        /// <returns>On success return true, otherwise false.</returns>
        public bool SendEmail(string toEmailAddress, string fromEmailAddress, string fromDisplayName, string subject, string body, bool writeToEventLog)
        {
            // If the config has an item writeToEventLog, override the parameter
            if (ConfigurationManager.AppSettings["SMTPLogSendsToSystemLog"] != null && ConfigurationManager.AppSettings["SMTPLogSendsToSystemLog"].Trim().ToUpper() == "TRUE")
            {
                writeToEventLog = true;
            }

            var retValue = false;
            try
            {
                retValue = this.emailClient.SendEmail(toEmailAddress, fromEmailAddress, fromDisplayName, subject, body);
                var message = $"Email Sent TO < {toEmailAddress}  > MSG <  {body}  >";
                if (writeToEventLog)
                {
                    var el = new EventLog { Source = "Email Sent" };
                    el.WriteEntry(message, EventLogEntryType.Information);
                    el.Close();
                }
            }
            catch (Exception e)
            {
                if (writeToEventLog)
                {
                    this.logger.Error("Error in ProcessEmail", e);

                    // write to event viewer log
                    var el = new EventLog();
                    var message = "Error in SendMail " + e;
                    el.Source = "Error in SendMail";
                    el.WriteEntry(message, EventLogEntryType.Error);
                    el.Close();
                }
            }

            return retValue;
        }

        /// <summary>
        /// Validate email address.
        /// </summary>
        /// <param name="address">Email address</param>
        /// <returns>Returns true if valid email address, otherwise returns false.</returns>
        public bool IsEmailAddress(string address)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(address);
                return addr.Address == address;
            }
            catch
            {
                return false;
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MTData.Common.Utilities.Schedule
{
    [Flags]
    [Serializable]
    public enum MulityDayOfWeek
    {
        None = 0,
        Sunday = 1,
        Monday = 2,
        Tuesday = 4,
        Wednesday = 8,
        Thursday = 16,
        Friday = 32,
        Saturday = 64
    }

    public enum Position
    {
        First = 0,
        Second,
        Third,
        Fourth,
        Last
    }

    public enum Day
    {
        Sunday,
        Monday,
        Tuesday,
        Wednesday,
        Thursday,
        Friday,
        Saturday,
        Day,
        WeekDay,
        WeekEndDay
    }

    public enum RecurrenceType
    {
        [Description("Daily")]
        Daily = 0,
        [Description("Weekly")]
        Weekly = 1,
        [Description("Monthly")]
        MonthlyByDay = 2,
        [Description("Monthly")]
        MonthlyByPosition = 3
    }

    public enum Recurrence
    {
        Daily = 0,
        Weekly = 1,
        Monthly = 2
    }
}

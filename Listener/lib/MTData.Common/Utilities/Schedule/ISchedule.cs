using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Common.Utilities.Schedule
{

    /// <summary>
    /// this interface details the schedule of a scheduledRoute
    /// </summary>
    public interface ISchedule
    {
        /// <summary>
        /// the description of schedule
        /// </summary>
        string Description { get; }

        /// <summary>
        /// validate the schedule
        /// </summary>
        /// <returns>null if the schedule is valid, otherwise returns an error message</returns>
        string ValidateSchedule();

        /// <summary>
        /// get the next time this schedule is to be run in UTC after the given time
        /// </summary>
        DateTime NextScheduledDate(DateTime dateTimeUTC);

        /// <summary>
        /// get the start date of this schedule
        /// </summary>
        DateTime StartDate { get;}

        /// <summary>
        /// get the end date of this schedule
        /// </summary>
        DateTime? EndDate { get;}

        TimeSpan EngineRunTime { get;}
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Common.Utilities.Schedule
{
    [Serializable]
    public class DayMonthlySchedule : AbstractSchedule
    {
        #region private fields
        /// <summary>
        /// repeat the schedule in x number of months
        /// </summary>
        private int _repeatInXMonths;

        /// <summary>
        /// the day of the month to run the Route on
        /// </summary>
        private int _dayOfMonth;
        #endregion

        public DayMonthlySchedule()
            : base()
        {
            _repeatInXMonths = 1;
            _dayOfMonth = 1;
        }
        public DayMonthlySchedule(ISchedule schedule)
            : base(schedule)
        {
            _repeatInXMonths = 1;
            _dayOfMonth = 1;
            if (schedule is DayMonthlySchedule)
            {
                _repeatInXMonths = ((DayMonthlySchedule)schedule).RepeatInXMonths;
                _dayOfMonth = ((DayMonthlySchedule)schedule).DayOfMonth;
            }
        }

        public int RepeatInXMonths
        {
            get { return _repeatInXMonths; }
            set { _repeatInXMonths = value; }
        }
        public int DayOfMonth
        {
            get { return _dayOfMonth; }
            set { _dayOfMonth = value; }
        }
        #region ISchedule Members

        [System.Xml.Serialization.XmlIgnore]
        public override string Description
        {
            get
            {
                string error = ValidateSchedule();
                if (error != null)
                {
                    return error;
                }

                if (_repeatInXMonths <= 0)
                {
                    return string.Format("Route will be scheduled once on {0}", GetFirstScheduledTime().ToString("dd/MM/yyyy"));
                }

                string des = string.Format("Route will be scheduled every month on day {0}", _dayOfMonth);
                if (_repeatInXMonths > 1)
                {
                    des = string.Format("Route will be scheduled every {0} months on day {1}", _repeatInXMonths, _dayOfMonth);
                }
                if (EndDate.HasValue)
                    des = string.Format("{0}. Route will be scheduled between {1:dd/MM/yyyy} and {2:dd/MM/yyyy}.", des, StartDate.Date, EndDate.Value.Date);
                else
                    des = string.Format("{0}. Route will be scheduled from {1:dd/MM/yyyy}.", des, StartDate.Date);
                return des;
            }
        }

        public override string ValidateSchedule()
        {
            string error = base.ValidateSchedule();
            if (error != null)
            {
                return error;
            }
            if (_dayOfMonth <= 0 || _dayOfMonth >= 32)
            {
                return "Day of month must be between 1 and 31";
            }
            return null;
        }
        public override DateTime NextScheduledDate(DateTime dateTimeUTC)
        {
            if (dateTimeUTC < StartDate || _repeatInXMonths <= 0)
            {
                return GetFirstScheduledTime();
            }
            if (dateTimeUTC > EndDate)
            {
                return GetLastNextDate();
            }

            DateTime next = CreateDefaultDate(dateTimeUTC);
            if (next < dateTimeUTC)
            {
                next = next.AddMonths(1);
            }

            if (_repeatInXMonths > 1)
            {
                DateTime firstSchedule = GetFirstScheduledTime();
                //calculate number of months between first and next schedules
                int months = 0;
                if (firstSchedule.Month > next.Month)
                {
                    months = 12 - firstSchedule.Month + next.Month;
                    months += 12 * (next.Year - firstSchedule.Year + 1);
                }
                else
                {
                    months = next.Month - firstSchedule.Month;
                    months += 12 * (next.Year - firstSchedule.Year);
                }

                int nextMonth = (months % _repeatInXMonths);
                if (nextMonth > 0)
                {
                    nextMonth = _repeatInXMonths - nextMonth;
                    next = CreateDefaultDate(next.AddMonths(nextMonth));
                }
            }
            if (!EndDate.HasValue)
            {
                return next;
            }
            if (next < EndDate)
            {
                return next;
            }
            return GetLastNextDate();
        }

        #endregion

        private DateTime GetFirstScheduledTime()
        {
            DateTime next = CreateDefaultDate(StartDate);
            if (next < StartDate)
            {
                next = next.AddMonths(1);
            }
            return next;
        }

        private DateTime GetLastNextDate()
        {
            DateTime next = CreateDefaultDate((EndDate.HasValue ? EndDate.Value : DateTime.MaxValue));
            if (next > EndDate)
            {
                next = next.AddMonths(-1);
            }

            if (_repeatInXMonths > 1)
            {
                DateTime firstSchedule = GetFirstScheduledTime();
                //calculate number of months between first and next schedules
                int months = 0;
                if (firstSchedule.Month > next.Month)
                {
                    months = 12 - firstSchedule.Month + next.Month;
                    months += 12 * (next.Year - firstSchedule.Year + 1);
                }
                else
                {
                    months = next.Month - firstSchedule.Month;
                    months += 12 * (next.Year - firstSchedule.Year);
                }

                int nextMonth = (months % _repeatInXMonths) * -1;
                next = next.AddMonths(nextMonth);
            }
            return next;
        }

        /// <summary>
        /// depening on the month, create the defualt date i.e if _dayOfMonth = 31 set to last day of months with less days
        /// </summary>
        /// <param name="time"></param>
        /// <returns></returns>
        private DateTime CreateDefaultDate(DateTime time)
        {
            if (_dayOfMonth <= 28)
            {
                return new DateTime(time.Year, time.Month, _dayOfMonth);//, StartDate.Hour, StartDate.Minute, StartDate.Second);
            }
            DateTime next = new DateTime(time.Year, time.Month + 1, 1);//, StartDate.Hour, StartDate.Minute, StartDate.Second);
            next = next.AddDays(-1);
            while (next.Day > _dayOfMonth)
            {
                next = next.AddDays(-1);
            }
            return next;
        }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Common.Utilities.Schedule
{
    //public enum Position
    //{
    //    First = 0,
    //    Second,
    //    Third,
    //    Fourth,
    //    Last
    //}

    //public enum Day
    //{
    //    Sunday,
    //    Monday,
    //    Tuesday,
    //    Wednesday,
    //    Thursday,
    //    Friday,
    //    Saturday,
    //    Day,
    //    WeekDay,
    //    WeekEndDay
    //}

    [Serializable]
    public class RelativeMonthlySchedule : AbstractSchedule
    {
        #region private fields
        /// <summary>
        /// repeat the schedule in x number of months
        /// </summary>
        private int _repeatInXMonths;

        /// <summary>
        /// the position in the month to run the Route on
        /// </summary>
        private Position _position;
        /// <summary>
        /// the day in the month to run the Route on
        /// </summary>
        private Day _day;
        #endregion

        public RelativeMonthlySchedule()
            : base()
        {
            _repeatInXMonths = 1;
            _day = Day.Monday;
            _position = Position.First;
        }
        public RelativeMonthlySchedule(ISchedule schedule)
            : base(schedule)
        {
            _repeatInXMonths = 1;
            _day = Day.Monday;
            _position = Position.First;
            if (schedule is RelativeMonthlySchedule)
            {
                _repeatInXMonths = ((RelativeMonthlySchedule)schedule).RepeatInXMonths;
                _day = ((RelativeMonthlySchedule)schedule).Day;
                _position = ((RelativeMonthlySchedule)schedule).Position;
            }
        }

        public int RepeatInXMonths
        {
            get { return _repeatInXMonths; }
            set { _repeatInXMonths = value; }
        }
        public Day Day
        {
            get { return _day; }
            set { _day = value; }
        }
        public Position Position
        {
            get { return _position; }
            set { _position = value; }
        }
        #region ISchedule Members

        [System.Xml.Serialization.XmlIgnore]
        public override string Description
        {
            get
            {
                string error = ValidateSchedule();
                if (error != null)
                {
                    return error;
                }

                if (_repeatInXMonths <= 0)
                {
                    return string.Format("Route will be scheduled once on {0}", GetFirstScheduledTime().ToString("dd/MM/yyyy"));
                }

                string des = string.Format("Route will be scheduled every {0} {1} of every month", _position, _day);
                if (_repeatInXMonths > 1)
                {
                    des = string.Format("Route will be scheduled every {0} {1} of every {2} months", _position, _day, _repeatInXMonths);
                }
                if (EndDate.HasValue)
                    des = string.Format("{0}. Route will be scheduled between {1:dd/MM/yyyy} and {2:dd/MM/yyyy}.", des, StartDate.Date, EndDate.Value.Date);
                else
                    des = string.Format("{0}. Route will be scheduled from {1:dd/MM/yyyy}.", des, StartDate.Date);
                return des;
            }
        }

        public override DateTime NextScheduledDate(DateTime dateTimeUTC)
        {
            if (dateTimeUTC < StartDate || _repeatInXMonths <= 0)
            {
                return GetFirstScheduledTime();
            }
            if (dateTimeUTC > EndDate)
            {
                return GetLastNextDate();
            }

            DateTime next = GetScheduleDateForMonth(dateTimeUTC);
            if (next < dateTimeUTC)
            {
                next = GetScheduleDateForMonth(dateTimeUTC.AddMonths(1));
            }

            if (_repeatInXMonths > 1)
            {
                DateTime firstSchedule = GetFirstScheduledTime();
                //calculate number of months between first and next schedules
                int months = 0;
                if (firstSchedule.Month > next.Month)
                {
                    months = 12 - firstSchedule.Month + next.Month;
                    months += 12 * (next.Year - firstSchedule.Year + 1);
                }
                else
                {
                    months = next.Month - firstSchedule.Month;
                    months += 12 * (next.Year - firstSchedule.Year);
                }

                int nextMonth = (months % _repeatInXMonths);
                if (nextMonth > 0)
                {
                    nextMonth = _repeatInXMonths - nextMonth;
                    next = GetScheduleDateForMonth(next.AddMonths(nextMonth));
                }
            }
            if (!EndDate.HasValue)
            {
                return next;
            }

            if (next < EndDate.Value)
            {
                return next;
            }
            return GetLastNextDate();
        }

        #endregion

        private DateTime GetScheduleDateForMonth(DateTime date)
        {
            DateTime time = new DateTime(date.Year, date.Month, 1);//, StartDate.Hour, StartDate.Minute, StartDate.Second);
            switch (_position)
            {
                case Position.First:
                    while (!CorrectDay(time))
                    {
                        time = time.AddDays(1);
                    }
                    break;
                case Position.Second:
                    for (int i = 0; i < 2; i++)
                    {
                        while (!CorrectDay(time))
                        {
                            time = time.AddDays(1);
                        }
                        if (i < 1)
                        {
                            time = time.AddDays(1);
                        }
                    }
                    break;
                case Position.Third:
                    for (int i = 0; i < 3; i++)
                    {
                        while (!CorrectDay(time))
                        {
                            time = time.AddDays(1);
                        }
                        if (i < 2)
                        {
                            time = time.AddDays(1);
                        }
                    }
                    break;
                case Position.Fourth:
                    for (int i = 0; i < 4; i++)
                    {
                        while (!CorrectDay(time))
                        {
                            time = time.AddDays(1);
                        }
                        if (i < 3)
                        {
                            time = time.AddDays(1);
                        }
                    }
                    break;
                case Position.Last:
                    time = time.AddMonths(1);
                    time = time.AddDays(-1);
                    while (!CorrectDay(time))
                    {
                        time = time.AddDays(-1);
                    }
                    break;
            }
            return time;
        }

        private bool CorrectDay(DateTime time)
        {
            switch (_day)
            {
                case Day.Monday:
                    if (time.DayOfWeek == DayOfWeek.Monday)
                    {
                        return true;
                    }
                    break;
                case Day.Tuesday:
                    if (time.DayOfWeek == DayOfWeek.Tuesday)
                    {
                        return true;
                    }
                    break;
                case Day.Wednesday:
                    if (time.DayOfWeek == DayOfWeek.Wednesday)
                    {
                        return true;
                    }
                    break;
                case Day.Thursday:
                    if (time.DayOfWeek == DayOfWeek.Thursday)
                    {
                        return true;
                    }
                    break;
                case Day.Friday:
                    if (time.DayOfWeek == DayOfWeek.Friday)
                    {
                        return true;
                    }
                    break;
                case Day.Saturday:
                    if (time.DayOfWeek == DayOfWeek.Saturday)
                    {
                        return true;
                    }
                    break;
                case Day.Sunday:
                    if (time.DayOfWeek == DayOfWeek.Sunday)
                    {
                        return true;
                    }
                    break;
                case Day.WeekDay:
                    if (time.DayOfWeek == DayOfWeek.Monday ||
                        time.DayOfWeek == DayOfWeek.Tuesday ||
                        time.DayOfWeek == DayOfWeek.Wednesday ||
                        time.DayOfWeek == DayOfWeek.Thursday ||
                        time.DayOfWeek == DayOfWeek.Friday)
                    {
                        return true;
                    }
                    break;
                case Day.WeekEndDay:
                    if (time.DayOfWeek == DayOfWeek.Saturday || time.DayOfWeek == DayOfWeek.Sunday)
                    {
                        return true;
                    }
                    break;
                case Day.Day:
                    return true;
            }
            return false;
        }

        private DateTime GetFirstScheduledTime()
        {
            DateTime next = GetScheduleDateForMonth(StartDate);
            if (next < StartDate)
            {
                next = GetScheduleDateForMonth(StartDate.AddMonths(1));
            }
            return next;
        }

        private DateTime GetLastNextDate()
        {
            DateTime next = GetScheduleDateForMonth((EndDate.HasValue ? EndDate.Value : DateTime.MaxValue));
            if (next > EndDate)
            {
                next = GetScheduleDateForMonth((EndDate.HasValue ? EndDate.Value : DateTime.MaxValue).AddMonths(-1));
            }

            if (_repeatInXMonths > 1)
            {
                DateTime firstSchedule = GetFirstScheduledTime();
                //calculate number of months between first and next schedules
                int months = 0;
                if (firstSchedule.Month > next.Month)
                {
                    months = 12 - firstSchedule.Month + next.Month;
                    months += 12 * (next.Year - firstSchedule.Year + 1);
                }
                else
                {
                    months = next.Month - firstSchedule.Month;
                    months += 12 * (next.Year - firstSchedule.Year);
                }

                int nextMonth = (months % _repeatInXMonths) * -1;
                next = GetScheduleDateForMonth(next.AddMonths(nextMonth));
            }
            return next;
        }
    }
}

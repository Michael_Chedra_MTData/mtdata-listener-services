using System;
using System.Threading;
using System.Collections;
using log4net;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for cQueueItems.
    /// </summary>
    public class QueueItems
    {
        #region Events
        public delegate void ItemToProcessEvent(object oData);
        public event ItemToProcessEvent eNewItemToProcess;

        public event WriteToConsoleEvent eConsoleEvent;
        public delegate void WriteToConsoleEvent(string sMsg);
        #endregion

        #region Private Vars
        private ILog _logger = LogManager.GetLogger(typeof(QueueItems));
        private bool bThreadActive = false;
        private int iMaxQueueLength = 100;
        private int iMinPeriod = 500;
        private Queue oQueue = null;
        private Thread tProcessQueue = null;
        private string _name = "cQueueItems";
        private AutoResetEvent _waitHandle;

        #endregion

        #region Constructor / Destructor
        public QueueItems(int MaxQueueLength, int PaketRateInms, string name)
            : this(MaxQueueLength, PaketRateInms)
        {
            _name = name;
        }

        public QueueItems(int MaxQueueLength, int PaketRateInms)
        {
            oQueue = Queue.Synchronized(new Queue());
            tProcessQueue = new Thread(new ThreadStart(ProcessQueue));
            tProcessQueue.Name = "cQueueItems.ProcessQueue";

            iMaxQueueLength = MaxQueueLength;
            iMinPeriod = PaketRateInms;
            _waitHandle = new AutoResetEvent(false);
        }

        public void Dispose()
        {
            bThreadActive = false;
            _waitHandle.Set();

            if (oQueue != null)
            {
                oQueue.Clear();
                oQueue = null;
            }
        }
        #endregion

        #region Start and Stop Procedures

        public void Start()
        {
            bThreadActive = true;
            tProcessQueue.Start();
        }

        public void Stop()
        {
            if (!bThreadActive) return;

            bThreadActive = false;
            _waitHandle.Set();
            Thread.Sleep(100);
            oQueue.Clear();
        }
        #endregion

        #region Thread loop
        public void ProcessQueue()
        {
            object[] oData = null;

            _logger.InfoFormat("ProcessQueue() [%s] - Starting", _name);

            try
            {
                while (bThreadActive)
                {

                    try
                    {
                        oData = null;
                        if (oQueue.Count > 0)
                        {

                            lock (oQueue.SyncRoot)
                            {
                                oData = new object[oQueue.Count];
                                for (int loop = 0; loop < oQueue.Count; loop++)
                                    oData[loop] = oQueue.Dequeue();
                            }
                            if ((oData != null) && (oData.Length > 0) && (eNewItemToProcess != null))
                            {
                                for (int loop = 0; loop < oData.Length; loop++)
                                {
                                    if (oData[loop] != null)
                                    {
                                        try
                                        {
                                            eNewItemToProcess(oData[loop]);
                                        }
                                        catch (System.Exception ex)
                                        {
                                            WriteToErrorConsole(ex);
                                        }
                                    }
                                }
                            }
                        }
                        else
                            _waitHandle.WaitOne();

                    }
                    catch (System.Exception ex)
                    {
                        WriteToErrorConsole(ex);
                    }
                }
            }
            finally
            {
                _logger.InfoFormat("ProcessQueue() [%s] - Stopping", _name);
            }
            try
            {
                tProcessQueue.Abort();
            }
            catch (System.Exception)
            {
                // Ignore the abort error.
            }
        }
        #endregion

        #region Public functions

        private bool _ditching = false;
        public void EnqueueItem(object oData)
        {
            try
            {
                if (oQueue != null & bThreadActive)
                {
                    lock (oQueue.SyncRoot)
                    {
                        oQueue.Enqueue(oData);
                    }
                }
                if (oQueue.Count > iMaxQueueLength)
                {
                    if (!_ditching)
                        WriteToConsole("Ditching object, to many objects queued. Limit = " + Convert.ToString(iMaxQueueLength));

                    _ditching = true;
                    lock (oQueue.SyncRoot)
                    {
                        while (oQueue.Count > iMaxQueueLength)
                            oQueue.Dequeue();
                    }
                }
                _waitHandle.Set();
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        public void ClearQueue()
        {
            try
            {
                if (oQueue != null)
                {
                    lock (oQueue.SyncRoot)
                    {
                        oQueue.Clear();
                    }
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }
        #endregion
        #region Log Event Interface Support
        private void WriteToConsole(string sMsg)
        {
            if (eConsoleEvent != null)
                eConsoleEvent(sMsg);
            else
                Console.WriteLine(sMsg);
        }

        private void WriteToErrorConsole(System.Exception ex)
        {
            string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
            WriteToConsole(sMsg);
        }
        #endregion
    }
}

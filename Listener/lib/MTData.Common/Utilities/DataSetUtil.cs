﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Common.Utilities
{
    public static class DataSetUtil
    {
        public static T ConvertTyped<T>(DataSet dataSet) where T : DataSet, new()
        {
            if (dataSet == null)
                return null;
            else
            {
                T result = new T();
                result.EnforceConstraints = false;
                result.Merge(dataSet, true);
                return result;
            }
        }

        public static T Convert<T>(this DataSet dataSet) where T : DataSet, new()
        {
            if (dataSet == null)
                return null;
            else
            {
                T result = new T();
                result.EnforceConstraints = false;
                result.Merge(dataSet, true);
                return result;
            }
        }

    }
}

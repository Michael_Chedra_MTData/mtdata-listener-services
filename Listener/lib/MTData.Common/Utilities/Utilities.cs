using System;
using System.Configuration;
using System.Reflection;
using System.Text;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for Utilities.
    /// </summary>
    public static partial class Util
    {

        ///// <summary>
        ///// This method will take a byte array and output it in the format
        ///// [xxx][yyy][zzz]
        ///// Where xxx, yyy, and zzz are decimal representations of each byte in the array.
        ///// </summary>
        ///// <param name="bytes"></param>
        ///// <returns></returns>
        //public static string EncodeByteArrayForLogging(byte[] bytes)
        //{
        //    if (bytes == null)
        //        return "<NULL>";
        //    else
        //        return EncodeByteArrayForLogging(bytes, bytes.Length);
        //}

        ///// <summary>
        ///// This method will take a byte array and output it in the format
        ///// [xxx][yyy][zzz]
        ///// Where xxx, yyy, and zzz are decimal representations of each byte in the array.
        ///// </summary>
        ///// <param name="bytes"></param>
        ///// <param name="packetLength"></param>
        ///// <returns></returns>
        //public static string EncodeByteArrayForLogging(byte[] bytes, int packetLength)
        //{
        //    if (bytes == null)
        //        return "<NULL>";
        //    else
        //        return EncodeByteArrayForLogging(bytes, 0, packetLength);
        //}

        /// <summary>
        /// This will pass along the details for the relevant method.
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="startPos"></param>
        /// <param name="packetLength"></param>
        /// <returns></returns>
        public static string EncodeByteArrayForLogging(byte[] bytes, int startPos, int packetLength)
        {
            if (bytes == null)
                return "<NULL>";
            else
                return EncodeByteArrayAsDecimal(bytes, startPos, packetLength);
        }

        public static string EncodeByteArrayAsDecimal(byte[] bytes, int startPos, int packetLength)
        {
            StringBuilder builder = new StringBuilder();

            int endPos = startPos + packetLength;
            for (int loop = startPos; loop < endPos; loop++)
            {
                builder.Append("[");
                int representation = Convert.ToInt16(bytes[loop]);
                if (representation < 100)
                    builder.Append(" ");
                if (representation < 10)
                    builder.Append(" ");
                builder.Append(representation);
                builder.Append("]");
            }
            return builder.ToString();
        }

        #region ConvertTo Ascii
        /// <summary>
        /// This method will convert a byte array into a semi ascii based
        /// representation. If the field holds a byte that can be displayed 
        /// as ASCII, then it will be, otherwise it is displayed as decimal.
        /// </summary>
        /// <param name="bPacket"></param>
        /// <returns></returns>
        public static string EncodeByteArrayToAscii(byte[] bPacket)
        {
            return EncodeByteArrayToAscii(bPacket, true);
        }

        /// <summary>
        /// This method will convert a byte array into a semi ascii based
        /// representation. If the field holds a byte that can be displayed 
        /// as ASCII, then it will be, otherwise it is displayed as decimal.
        /// </summary>
        /// <param name="bPacket"></param>
        /// <returns></returns>
        public static string EncodeByteArrayToAscii(byte[] bPacket, bool bAddSpacing)
        {
            StringBuilder sRet = new StringBuilder();
            int X = 0;
            //			byte bLower = 0x21;	// 33
            //			byte bUpper = 0x7E;  // 126
            byte bTest = 0x00;  // null
            byte[] bTestArray = new byte[1];

            if (bPacket.Length > 0)
            {
                try
                {
                    for (X = 0; X < bPacket.Length; X++)
                    {
                        bTest = bPacket[X];
                        bTestArray = new byte[1];
                        bTestArray[0] = bTest;
                        if (bTest >= 33 && bTest <= 126)  // If the character is in printable range
                        {
                            if ((bAddSpacing) && (sRet.Length > 0))
                                sRet.Append(" - ");
                            sRet.Append(System.Text.ASCIIEncoding.ASCII.GetString(bTestArray));
                        }
                        else  // Show it as a number
                        {
                            if ((bAddSpacing) && (sRet.Length > 0))
                                sRet.Append(" - ");

                            sRet.Append("[" + Convert.ToString(bTest) + "]");
                        }
                    }

                }
                catch (System.Exception ex)
                {
                    Console.Write(ex.Message + "\n");
                }
            }
            return sRet.ToString();
        }
        #endregion

        #region Encoding into Hex String

        /// <summary>
        /// This method will take a byte array and output a string of hex, comma separated.
        /// </summary>
        /// <param name="bytes"></param>
        /// <returns></returns>
        public static string EncodeByteArrayAsHex(byte[] bytes)
        {
            if (bytes == null)
                return "<NULL>";
            else
                return EncodeByteArrayAsHex(bytes, bytes.Length);
        }

        /// <summary>
        /// This method will take a byte array and output a string of hex, comma separated.
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="packetLength"></param>
        /// <returns></returns>
        public static string EncodeByteArrayAsHex(byte[] bytes, int packetLength)
        {
            if (bytes == null)
                return "<NULL>";
            else
                return EncodeByteArrayAsHex(bytes, 0, packetLength);
        }

        /// <summary>
        /// This method will take a byte array and output a string of hex, comma separated.
        /// </summary>
        /// <param name="bytes"></param>
        /// <param name="startPos"></param>
        /// <param name="packetLength"></param>
        /// <returns></returns>
        public static string EncodeByteArrayAsHex(byte[] bytes, int startPos, int packetLength)
        {
            StringBuilder builder = new StringBuilder();

            int endPos = startPos + packetLength;
            for (int loop = startPos; loop < endPos; loop++)
            {
                if (loop > startPos)
                    builder.Append(",");
                builder.Append(string.Format("{0:x2}", Convert.ToInt32(bytes[loop])));
            }
            return builder.ToString();
        }

        #endregion

        #region Byte Summation Utilities

        /// <summary>
        /// This method will calculate the summation of all byte values in an array.
        /// </summary>
        /// <param name="array"></param>
        /// <returns></returns>
        public static int SumOfBytes(byte[] array)
        {
            int total = 0;

            foreach (byte b in array)
            {
                total += Convert.ToInt32(b);
            }
            return total;
        }

        #endregion

        #region Date and Time Functions
        public static int GetDayOfWeekasInt(System.DateTime oDate)
        {
            int iRet = 0;
            switch (oDate.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    iRet = 1;
                    break;
                case System.DayOfWeek.Monday:
                    iRet = 2;
                    break;
                case System.DayOfWeek.Tuesday:
                    iRet = 3;
                    break;
                case System.DayOfWeek.Wednesday:
                    iRet = 4;
                    break;
                case System.DayOfWeek.Thursday:
                    iRet = 5;
                    break;
                case System.DayOfWeek.Friday:
                    iRet = 6;
                    break;
                case System.DayOfWeek.Saturday:
                    iRet = 7;
                    break;
                default:
                    break;
            }
            return iRet;
        }

        public static int GetDayOfWeekUTCasInt(System.DateTime oLocalDate)
        {
            int iRet = 0;
            System.DateTime oDate = oLocalDate.ToUniversalTime();
            switch (oDate.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    iRet = 1;
                    break;
                case System.DayOfWeek.Monday:
                    iRet = 2;
                    break;
                case System.DayOfWeek.Tuesday:
                    iRet = 3;
                    break;
                case System.DayOfWeek.Wednesday:
                    iRet = 4;
                    break;
                case System.DayOfWeek.Thursday:
                    iRet = 5;
                    break;
                case System.DayOfWeek.Friday:
                    iRet = 6;
                    break;
                case System.DayOfWeek.Saturday:
                    iRet = 7;
                    break;
                default:
                    break;
            }
            return iRet;
        }

        public static int GetDayOfWeekLocalasInt(System.DateTime oUTCDate)
        {
            int iRet = 0;
            System.DateTime oDate = oUTCDate.ToLocalTime();
            switch (oDate.DayOfWeek)
            {
                case System.DayOfWeek.Sunday:
                    iRet = 1;
                    break;
                case System.DayOfWeek.Monday:
                    iRet = 2;
                    break;
                case System.DayOfWeek.Tuesday:
                    iRet = 3;
                    break;
                case System.DayOfWeek.Wednesday:
                    iRet = 4;
                    break;
                case System.DayOfWeek.Thursday:
                    iRet = 5;
                    break;
                case System.DayOfWeek.Friday:
                    iRet = 6;
                    break;
                case System.DayOfWeek.Saturday:
                    iRet = 7;
                    break;
                default:
                    break;
            }
            return iRet;
        }

        public static string ToReadableString(this TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}",
                span.Duration().Days > 0 ? string.Format("{0:0} Day{1}, ", span.Days, span.Days == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Hours > 0 ? string.Format("{0:0} Hour{1}, ", span.Hours, span.Hours == 1 ? String.Empty : "s") : string.Empty,
                span.Duration().Minutes > 0 ? string.Format("{0:0} Minute{1}, ", span.Minutes, span.Minutes == 1 ? String.Empty : "s") : string.Empty);

            if (formatted.EndsWith(", ")) formatted = formatted.Substring(0, formatted.Length - 2);

            if (string.IsNullOrEmpty(formatted)) formatted = "0 Mins";

            return formatted;
        }

        #endregion

        #region ServiceName
        private static string _serviceName = null;
        private static string _displayName = null;

        public static string ServiceName
        {
            get
            {
                if (_serviceName == null)
                {
                    _serviceName = ConfigurationManager.AppSettings["ServiceName"];
                }
                return _serviceName;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (_displayName == null)
                {
                    _displayName = ConfigurationManager.AppSettings["DisplayName"];
                }
                return _displayName;
            }
        }
        #endregion
    }
}

using System;
using System.Threading;
using System.Collections;
using System.Net.Sockets;

namespace MTData.Common.Utilities
{
    public class ReportInterface
    {
        #region Events
        public delegate void ServerMessageEvent(string sData, int iListIndex);
        public event ServerMessageEvent eServerMessage;

        //public delegate void ServerConnectedEvent(bool bData, int iListIndex);
        //public event ServerConnectedEvent eServerConnected;

        public delegate void ServerCompleteEvent(string sURL, string sLocalPath, int iListIndex);
        public event ServerCompleteEvent eServerComplete;

        public delegate void ServerErrorEvent(string sMsg);
        public event ServerErrorEvent eServerError;
        #endregion

        #region Variables
        #region Private Variables
        private Queue oSendQueue = null;
        private bool bThreadActive = false;
        private bool bSkip = false;
        private Thread tProcessSendQueue = null;
        private string sReportsURL = "";
        #endregion
        #endregion

        public ReportInterface()
        {
            oSendQueue = new Queue();
            ThreadStart oThreadStart = new ThreadStart(this.HandleSendQueue);
            tProcessSendQueue = new Thread(oThreadStart);
            tProcessSendQueue.Name = "Process Reports Circular Buffer Thead.";
            bThreadActive = true;
            tProcessSendQueue.Start();	// and start the retry system
        }

        public void Dispose()
        {
            if (bThreadActive)
            {
                bThreadActive = false;
                Thread.Sleep(1000);
            }
            if (oSendQueue != null)
            {
                try
                {
                    oSendQueue.Clear();
                    oSendQueue = null;
                }
                catch (System.Exception)
                {
                }
            }
        }


        public ReportQueueItem GetExcelReport
        {
            set
            {
                lock (oSendQueue)
                {
                    oSendQueue.Enqueue(value);
                }
            }
        }

        public void TimeoutCurrentItem()
        {
            bSkip = true;
        }

        private void HandleSendQueue()
        {
            ReportQueueItem oQueueItem = null;

            bThreadActive = true;

            while (bThreadActive)
            {
                try
                {
                    oQueueItem = null;

                    lock (oSendQueue)
                    {
                        if (oSendQueue.Count > 0)
                            oQueueItem = (ReportQueueItem)oSendQueue.Dequeue();
                    }

                    if (oQueueItem != null)
                    {
                        // Connect to the server and generate the Excel file, retireve it and open it.
                        oQueueItem.eServerComplete += new ReportQueueItem.ServerCompleteEvent(oQueueItem_eServerComplete);
                        oQueueItem.eServerMessage += new ReportQueueItem.ServerMessageEvent(oQueueItem_eServerMessage);
                        oQueueItem.eServerConnected += new ReportQueueItem.ServerConnectedEvent(oQueueItem_eServerConnected);
                        sReportsURL = oQueueItem.ReportsURL;
                        // Start the server connection...
                        oQueueItem.ProcessReport();

                        bSkip = false;
                        while (bThreadActive && oQueueItem.IsRunning && !bSkip)
                        {
                            // Wait for the report to complete
                            Thread.Sleep(500);
                        }
                        bSkip = false;

                        // Wait for the report to clean up and pass any messages.
                        Thread.Sleep(500);

                        oQueueItem.eServerComplete -= new ReportQueueItem.ServerCompleteEvent(oQueueItem_eServerComplete);
                        oQueueItem.eServerMessage -= new ReportQueueItem.ServerMessageEvent(oQueueItem_eServerMessage);
                        oQueueItem.eServerConnected -= new ReportQueueItem.ServerConnectedEvent(oQueueItem_eServerConnected);
                        oQueueItem.Dispose();
                        oQueueItem = null;

                        // Clean up the dequeued items memory.
                    }
                    else
                    {
                        // Nothing is queued at the moment, just sleep for 1/2 a second.
                        Thread.Sleep(500);
                    }
                }
                catch (System.Exception ex)
                {
                    WriteToConsole("cReportInterface.HandleSendQueue()\nError : " + ex.Message + "\n" + ex.Source);
                }
            }

            try
            {
                tProcessSendQueue.Abort();
            }
            catch (System.Exception)
            {
                // Just ignore the abort error.
            }
        }

        private void oQueueItem_eServerMessage(string sMessage, int iListIndex)
        {
            // This event is fired when Queue item object finds a server message packet..
            if (sMessage.Length <= 2) return;

            if (eServerMessage != null)
                eServerMessage(sMessage, iListIndex);
        }

        private void oQueueItem_eServerComplete(string sUrl, string sLocalPath, int iListIndex)
        {
            // This event is fired when Queue item object finds a server complete packet..
            if (eServerComplete != null)
            {
                eServerComplete(sUrl, sLocalPath, iListIndex);
            }
            bSkip = true;
        }

        private void oQueueItem_eServerConnected(bool bData, int iListIndex)
        {
            // This event is fired when Queue item object connects / disconnects from the server
        }

        public void SkipCurrentItem()
        {
            bSkip = true;
        }

        private void WriteToConsole(string sMsg)
        {
            if (eServerError != null)
                eServerError(sMsg);
        }

    }
}

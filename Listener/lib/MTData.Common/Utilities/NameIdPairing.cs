using System;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for NameIdPairing.
    /// </summary>
    public class NameIdPairing
    {
        private int _id = -1;
        private string _name = "";
        private string _tag = "";

        public NameIdPairing(int id, string name, string tag)
        {
            this._id = id;
            this._name = name;
            this._tag = tag;
        }

        public NameIdPairing(int id, string name)
        {
            this._id = id;
            this._name = name;
            this._tag = "";
        }

        /// <summary>
        /// By overriding the ToString, any list will display the name 
        /// when items of this type are added to it.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return this._name;
        }

        /// <summary>
        /// The Name is the string representation of this object.
        /// </summary>
        public string Name
        {
            get
            {
                return this._name;
            }
            set
            {
                this._name = value;
            }
        }

        /// <summary>
        /// A Tag is bit odf string data attached to the object., it CustomerID is the ID, CustomerName is the name, CustomerPhonenumber can be the tag.
        /// </summary>
        public string Tag
        {
            get
            {
                return this._tag;
            }
            set
            {
                this._tag = value;
            }
        }

        /// <summary>
        /// The ID is the identifier of this object.
        /// </summary>
        public int Id
        {
            get
            {
                return this._id;
            }
            set
            {
                this._id = value;
            }
        }
    }
}

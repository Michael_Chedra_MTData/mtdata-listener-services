using System;
using System.Threading;

namespace MTData.Common.Utilities
{
    public class ReportsCircBuffer
    {
        #region Events
        public delegate void ServerMessageEvent(byte[] bData);
        public event ServerMessageEvent eServerMessage;
        public delegate void ServerCompleteEvent(byte[] bData);
        public event ServerCompleteEvent eServerComplete;
        public event WriteToConsoleEvent ConsoleEvent;
        public delegate void WriteToConsoleEvent(string sMsg);
        #endregion

        #region Variables
        #region Private Variables
        private byte[] bCircBuffer = null;
        private int iCBStartPoint = 0;
        private int iCBEndPoint = -1;
        private Thread tMonitorThread = null;
        private bool bMonitorThreadActive = false;
        #endregion
        #endregion

        public ReportsCircBuffer()
        {
            bCircBuffer = new byte[10000];
            iCBStartPoint = 0;
            iCBEndPoint = -1;
            tMonitorThread = new Thread(new ThreadStart(MonitorBuffer));
            tMonitorThread.Name = "cReportsCircBuffer.MonitorIncommingBuffer";
            bMonitorThreadActive = false;
        }

        public void Dispose()
        {
            bMonitorThreadActive = false;
            bCircBuffer = null;
            iCBStartPoint = 0;
            iCBEndPoint = 0;
        }

        public void StartThread()
        {
            bMonitorThreadActive = true;
            tMonitorThread.Start();
        }

        public void StopThread()
        {
            bMonitorThreadActive = false;
        }

        public void AddBytes(byte[] bData)
        {
            int X = 0;
            if (iCBEndPoint == -1) iCBEndPoint = 0;
            try
            {
                for (X = 0; X < bData.Length; X++)
                {
                    bCircBuffer[iCBEndPoint] = bData[X];

                    iCBEndPoint++;

                    if (iCBEndPoint >= bCircBuffer.Length)
                        iCBEndPoint = 0;
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }


        public void MonitorBuffer()
        {
            int iState = 0;
            int X = 0;
            int Y = 0;
            int iSOP = -1;
            int iLen = -1;

            bool bBufferReset = false;
            int iStartPoint = -1;
            int iEndPoint = -1;

            byte[] bPayload = null;
            byte[] bExamineData = null;
            byte[] bConvert = new byte[4];

            while (bMonitorThreadActive)
            {
                bBufferReset = false;
                if (iCBStartPoint != iCBEndPoint)
                {
                    try
                    {
                        #region Get the bytes from bCircBuffer between iCBStartPoint and iCBEndPoint (wrapped)
                        iStartPoint = (iCBStartPoint >= 0) ? iCBStartPoint : 0;
                        iEndPoint = iCBEndPoint;

                        if (iEndPoint < iStartPoint)
                        {
                            // Create the bytes to examine of length = Start point to end of buffer + start of buffer to end point
                            if (iEndPoint > 0)
                            {
                                bExamineData = new byte[(bCircBuffer.Length - iStartPoint) + iEndPoint];
                            }
                            else
                            {
                                bExamineData = new byte[(bCircBuffer.Length - iStartPoint) + 1];
                            }

                            Y = 0;
                            // Get the bytes from start point to the end of the buffer
                            for (X = iStartPoint; X < bCircBuffer.Length; X++)
                            {
                                bExamineData[Y++] = bCircBuffer[X];
                            }

                            if (iEndPoint > 0)
                            {
                                // Get the bytes from the start of the buffer to the end point
                                for (X = 0; X < iEndPoint; X++)
                                {
                                    bExamineData[Y++] = bCircBuffer[X];
                                }
                            }
                        }
                        else
                        {
                            // Create the bytes to examine of length = End point - Start Point
                            bExamineData = new byte[iEndPoint - iStartPoint];

                            Y = 0;
                            // Get the bytes from start point to the end of the buffer
                            for (X = iStartPoint; X < iEndPoint; X++)
                            {
                                bExamineData[Y++] = bCircBuffer[X];
                            }

                        }
                        #endregion
                    }
                    catch (System.Exception ex)
                    {
                        WriteToErrorConsole(ex);
                        WriteToConsole("Resetting..");

                        bCircBuffer = null;

                        bCircBuffer = new byte[1000000];
                        iCBStartPoint = 0;
                        iCBEndPoint = 0;
                        bBufferReset = true;
                    }

                    if (!bBufferReset)
                    {
                        try
                        {
                            #region Check if there is a complete packet
                            // Packet format = [SOP][Length][CmdType][Data][EOP]
                            // [SOP] = 0x0A
                            // [Length] = 4 Bytes
                            // [CmdType] = 1 Byte
                            // [Data] = Var Length String
                            // [EOP] = 0x0C
                            // Length includes [CmdType][Data]

                            // Reset the state machine	
                            iState = 0;

                            for (X = 0; X < bExamineData.Length; X++)
                            {
                                switch (iState)
                                {
                                    case 0: // Find the start of packet marker

                                        // Packet format = [SOP][Length][Unit ID][Sep][Job ID][Sep][Data][EOP]
                                        //                                 ^
                                        //							Byte 0

                                        if (bExamineData[X] == (byte)0x0A)
                                        {
                                            iState = 1;
                                            iSOP = X;
                                        }
                                        break;
                                    case 1: // Extract the packet length
                                        // Packet format = [SOP][Length][CmdType][Data][EOP]
                                        //                                             ^
                                        //									Bytes 1 - 4

                                        if (bExamineData.Length > X + 4)
                                        {
                                            try
                                            {
                                                // Convert the length from bytes
                                                bConvert = new byte[4];
                                                bConvert[0] = bExamineData[X++];
                                                bConvert[1] = bExamineData[X++];
                                                bConvert[2] = bExamineData[X++];
                                                bConvert[3] = bExamineData[X++];
                                                iLen = BitConverter.ToInt32(bConvert, 0);
                                            }
                                            catch (System.Exception ex01)
                                            {
                                                WriteToErrorConsole(ex01);
                                                iState = 0;
                                            }


                                            if (iLen > 2000)
                                            {
                                                // The length is too large to be a real packet, move back to the character
                                                // after the one where we thought we had found a start of packet (SOP)
                                                X = iSOP + 1;
                                                iSOP = -1;
                                                iState = 0;
                                            }
                                            else
                                            {
                                                // Check for the end of packet marker.
                                                // Packet format = [SOP][Length][CmdType][Data][EOP]
                                                //																	 		 		 ^
                                                //															Byte at pos [SOP] + 4 + [Length]

                                                if (X + iLen > bExamineData.Length)
                                                {
                                                    // The packet isn't long enough to be complete yet.
                                                    Thread.Sleep(100);
                                                    continue;
                                                }
                                                else
                                                {
                                                    try
                                                    {
                                                        if (bExamineData[X + iLen] == (byte)0x0C)
                                                        {
                                                            int iBytesRcvd = (X + iLen) - iSOP;
                                                            bPayload = new byte[iBytesRcvd + 1];
                                                            int Z = 0;

                                                            byte bCmd = bExamineData[X];

                                                            for (Y = X + 1; Y <= X + iLen - 1; Y++)
                                                            {
                                                                bPayload[Z++] = bExamineData[Y];
                                                            }

                                                            if (bCmd == (byte)0x01)
                                                            {
                                                                eServerMessage(bPayload);
                                                            }

                                                            if (bCmd == (byte)0x02)
                                                            {
                                                                eServerComplete(bPayload);
                                                            }

                                                            // Move the start pointer.
                                                            iCBStartPoint = iStartPoint + iBytesRcvd;
                                                            if (iCBStartPoint >= bCircBuffer.Length)
                                                                iCBStartPoint = iCBStartPoint - bCircBuffer.Length;

                                                            iSOP = -1;
                                                            iState = 0;
                                                            X += iLen;  // move to the end of the packet just found.
                                                        }
                                                        else
                                                        {
                                                            // The end of packet wasn't found, move back to the character
                                                            // after the one where we thought we had found a start of packet (SOP)
                                                            X = iSOP + 1;
                                                            iState = 0;
                                                        }
                                                    }
                                                    catch (System.Exception ex02)
                                                    {
                                                        WriteToErrorConsole(ex02);
                                                        iState = 0;
                                                    }

                                                }
                                            }
                                        }
                                        else
                                        {
                                            // The packet isn't long enough to be complete yet.
                                            continue;
                                        }
                                        break;
                                }
                                bConvert = null;
                                bPayload = null;
                            }
                            if (iState == 0)
                            {
                                // No Start of packet was found, therefore we don't need to look at these bytes again.
                                iCBStartPoint = iEndPoint;
                            }
                            #endregion
                        }
                        catch (System.Exception ex1)
                        {
                            WriteToErrorConsole(ex1);
                            WriteToConsole("Resetting..");

                            bCircBuffer = null;

                            bCircBuffer = new byte[1000000];
                            iCBStartPoint = 0;
                            iCBEndPoint = 0;
                        }
                    }
                }
                else
                    Thread.Sleep(100);
            }

            try
            {
                tMonitorThread.Abort();
            }
            catch (System.Exception)
            {
            }
        }

        private void WriteToConsole(string strMsg)
        {
            if (ConsoleEvent != null)
                ConsoleEvent(strMsg);
        }

        private void WriteToErrorConsole(System.Exception ex)
        {
            string sMsg = "";
            sMsg = "Error Message : \n      Message : " + ex.Message + "\n      Source : " + ex.Source + "\n      Stack : " + ex.StackTrace;
            WriteToConsole(sMsg);
        }
    }
}

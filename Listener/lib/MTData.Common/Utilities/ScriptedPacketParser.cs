using System;
using System.Xml;
using System.Collections;

namespace MTData.Common.Utilities
{
	/// <summary>
	/// This is the type of field that can be read by the scripter.
	/// </summary>
	public enum ScriptedPacketParserFieldType : int
	{
		Byte,
		UInt,
		Int,
		Ascii
	}

	/// <summary>
	/// This is the field entry when parsed.
	/// </summary>
	public class ScriptedPacketParserField
	{
		public string Name;
		public ScriptedPacketParserFieldType Type;
		public int Length;
		public byte Delimiter;
		public bool Delimited = false;
		public object Value;

		public ScriptedPacketParserField(string name, ScriptedPacketParserFieldType type, int length, byte delimiter, bool delimited)
		{
			Name = name;
			Type = type;
			Length = length;
			Delimiter = delimiter;
			Delimited = delimited;
		}
	}

	/// <summary>
	/// This class will use a script to parse the packets concerned.
	/// The output will be a hashtable with all of the relevant values.
	/// </summary>
	public class ScriptedPacketParser
	{
		XmlNode _scriptNode = null;

		public ScriptedPacketParser(XmlNode scriptNode)
		{
			_scriptNode = scriptNode;
		}

		/// <summary>
		/// Decode a packet given the packet structure and data
		/// </summary>
		/// <param name="packetBuffer"></param>
		/// <returns></returns>
		public Hashtable DecodePacket(byte[] packetBuffer)
		{
			return DecodePacket(new ByteStreamReader(packetBuffer));

		}

		/// <summary>
		/// Decode the byte array, and if specified, only return the values in the hashtable, not the full field definitions.
		/// </summary>
		/// <param name="packetBuffer"></param>
		/// <param name="valuesOnly"></param>
		/// <returns></returns>
		public Hashtable DecodePacket(byte[] packetBuffer, bool valuesOnly)
		{
			return DecodePacket(new ByteStreamReader(packetBuffer), valuesOnly);

		}

		/// <summary>
		/// Decode the packet from the provided bytestream, and return the full field definitions.
		/// </summary>
		/// <param name="reader"></param>
		/// <returns></returns>
		public Hashtable DecodePacket(ByteStreamReader reader)
		{
			return DecodePacket(reader, false);
		}

		/// <summary>
		/// Decode the packet passed in.
		/// </summary>
		/// <param name="reader"></param>
		/// <param name="valuesOnly">Indicates that in the result hashtable, we only want values, not field defs.</param>
		/// <returns></returns>
		public Hashtable DecodePacket(ByteStreamReader reader, bool valuesOnly)
		{
			Hashtable data = new Hashtable();

			DecodeChildNodes(data, _scriptNode, reader, null);

			if (valuesOnly)
			{
				Hashtable result = new Hashtable();

				foreach(string key in data.Keys)
					result.Add(key, ((ScriptedPacketParserField)data[key]).Value);

				return result;
			}
			else
				return data;
		}

		/// <summary>
		/// Encode a packet given the packet's structure
		/// </summary>
		/// <param name="fieldValues"></param>
		/// <returns></returns>
		public byte[] EncodePacket(Hashtable fieldValues)
		{
			ByteStreamWriter writer = new ByteStreamWriter();
			EncodePacket(fieldValues, writer);
			return writer.ToByteArray();
		}

		/// <summary>
		/// Write to a byte stream writer
		/// </summary>
		/// <param name="fieldValues"></param>
		/// <param name="writer"></param>
		public void EncodePacket(Hashtable fieldValues, ByteStreamWriter writer)
		{
			Hashtable data = new Hashtable();
			EncodeChildNodes(data, fieldValues, _scriptNode, writer, null);
		}

		/// <summary>
		/// Type of fields allowed
		/// </summary>
		private enum ParseNodeType : int
		{
			Null,
			Field,
			Switch,
			Case,
			Default
		}

		/// <summary>
		/// Get an integer attribute value.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		private int GetIntegerAttribute(XmlNode node, string attributeName, int defaultValue)
		{
			XmlAttribute attribute = (XmlAttribute)node.Attributes.GetNamedItem(attributeName);
			if (attribute == null)
				return defaultValue;
			else
				return Int32.Parse(attribute.Value);
		}

		/// <summary>
		/// Return a string attribute value
		/// </summary>
		/// <param name="node"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		private string GetStringAttribute(XmlNode node, string attributeName, string defaultValue)
		{
			XmlAttribute attribute = (XmlAttribute)node.Attributes.GetNamedItem(attributeName);
			if (attribute == null)
				return defaultValue;
			else
				return attribute.Value;
		}

		/// <summary>
		/// Return a byte value form an integer field.
		/// </summary>
		/// <param name="node"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		private byte GetByteAttribute(XmlNode node, string attributeName, byte defaultValue)
		{
			XmlAttribute attribute = (XmlAttribute)node.Attributes.GetNamedItem(attributeName);
			if (attribute == null)
				return defaultValue;
			else
				return Convert.ToByte(Int32.Parse(attribute.Value) % 256);
		}

		/// <summary>
		/// Return a booleran attribute value
		/// </summary>
		/// <param name="node"></param>
		/// <param name="attributeName"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		private bool GetBooleanAttribute(XmlNode node, string attributeName, bool defaultValue)
		{
			XmlAttribute attribute = (XmlAttribute)node.Attributes.GetNamedItem(attributeName);
			if (attribute == null)
				return defaultValue;
			else
				return (attribute.Value.ToLower() == "true");
		}

		/// <summary>
		/// Process the child script nodes for the current node.
		/// </summary>
		/// <param name="data"></param>
		/// <param name="parentNode"></param>
		/// <param name="reader"></param>
		/// <param name="contextField">Switch field of parent switch.</param>
		private void DecodeChildNodes(Hashtable data, XmlNode parentNode, ByteStreamReader reader, ScriptedPacketParserField contextField)
		{
			bool caseMatched = false;

			for(int loop = 0; loop < parentNode.ChildNodes.Count; loop++)
			{
				XmlNode currentNode = parentNode.ChildNodes[loop];

				if (currentNode.NodeType == XmlNodeType.Element)
				{
					ParseNodeType nodeType = (ParseNodeType)Enum.Parse(typeof(ParseNodeType), currentNode.Name, false);

					switch(nodeType)
					{
						case ParseNodeType.Null :
						{
							//	Read off a number of bytes and throw them away..
							int length = GetIntegerAttribute(currentNode, "Length", 0);
							if (length > 0)
								reader.ReadBytes(length);
							break;
						}
						case ParseNodeType.Field :
						{
							string name = GetStringAttribute(currentNode, "Name", "");
							if ((name != null) && (name != ""))
							{
								ScriptedPacketParserField field = new ScriptedPacketParserField(
									name, 
									(ScriptedPacketParserFieldType)Enum.Parse(typeof(ScriptedPacketParserFieldType), GetStringAttribute(currentNode, "Type", "")), 
									GetIntegerAttribute(currentNode, "Length", 0), 
									GetByteAttribute(currentNode, "Delimiter", (byte)0x00), 
									GetBooleanAttribute(currentNode, "Delimited", false));

								switch (field.Type)							
								{
									case ScriptedPacketParserFieldType.Byte :
									{
										if (field.Length > 1)
											field.Value = reader.ReadBytes(field.Length);
										else
											field.Value = reader.ReadByte();
										break;
									}
									case ScriptedPacketParserFieldType.Int :
									{
										if (field.Length == 2)
											field.Value = reader.Read2ByteInteger();
										if (field.Length == 4)
											field.Value = reader.Read4ByteInteger();
										break;
									}
									case ScriptedPacketParserFieldType.UInt :
									{
										if (field.Length == 2)
											field.Value = Convert.ToUInt32(reader.Read2ByteInteger());
										if (field.Length == 4)
											field.Value = reader.Read4ByteUnsignedInteger();
										break;
									}
									case ScriptedPacketParserFieldType.Ascii :
									{
										if (field.Delimited)
											field.Value = reader.ReadASCIIStringDelimited(field.Length, field.Delimiter);
										else
											field.Value = reader.ReadASCIIStringFixedLength(field.Length, field.Delimiter);
										break;
									}
								}

								data.Add(field.Name, field);
							}
							break;
						}
						case ParseNodeType.Switch :
						{
							//	Based on the value in another field, process the nodes below it.
							string name = GetStringAttribute(currentNode, "Name", "");
							if ((name != null) && (name != ""))
							{
								ScriptedPacketParserField field = (ScriptedPacketParserField)data[name];
								DecodeChildNodes(data, currentNode, reader, field);
							}
							break;
						}
						case ParseNodeType.Case :
						{
							//	Base on the value in a parent field, do each of the entries after it.
							string matchValue = GetStringAttribute(currentNode, "Value", "");
						
							bool matched = false;
							switch (contextField.Type)
							{
								case ScriptedPacketParserFieldType.Byte :
								{
									if (contextField.Length == 1)
										matched = (byte)contextField.Value == Convert.ToByte(Convert.ToInt32(matchValue));
									break;
								}
								case ScriptedPacketParserFieldType.Int : 
								{
									matched = (int)contextField.Value == Int32.Parse(matchValue);
									break;
								}
								case ScriptedPacketParserFieldType.UInt : 
								{
									matched = (uint)contextField.Value == UInt32.Parse(matchValue);
									break;
								}
								case ScriptedPacketParserFieldType.Ascii : 
								{
									matched = (string)contextField.Value == matchValue;
									break;
								}
							}
							if (matched)
							{
								//	process child nodes
								caseMatched = true;
								DecodeChildNodes(data, currentNode, reader, null);
							}
							break;
						}
						case ParseNodeType.Default :
						{
							if (!caseMatched)
							{
								DecodeChildNodes(data, currentNode, reader, null);
							}
							break;
						}
					}
				}
			}
		}

		/// <summary>
		/// Write the required details to the writer...
		/// </summary>
		/// <param name="data"></param>
		/// <param name="fieldValues"></param>
		/// <param name="parentNode"></param>
		/// <param name="writer"></param>
		/// <param name="contextField"></param>
		private void EncodeChildNodes(Hashtable data, Hashtable fieldValues, XmlNode parentNode, ByteStreamWriter writer, ScriptedPacketParserField contextField)
		{
			bool caseMatched = false;

			for(int loop = 0; loop < parentNode.ChildNodes.Count; loop++)
			{
				XmlNode currentNode = parentNode.ChildNodes[loop];

				if (currentNode.NodeType == XmlNodeType.Element)
				{
					ParseNodeType nodeType = (ParseNodeType)Enum.Parse(typeof(ParseNodeType), currentNode.Name, false);

					switch(nodeType)
					{
						case ParseNodeType.Null :
						{
							//	Read off a number of bytes and throw them away..
							int length = GetIntegerAttribute(currentNode, "Length", 0);
							if (length > 0)
								writer.WriteRepeatingByte((byte)0x00,length);
							break;
						}
						case ParseNodeType.Field :
						{
							string name = GetStringAttribute(currentNode, "Name", "");
							if ((name != null) && (name != ""))
							{
								ScriptedPacketParserField field = new ScriptedPacketParserField(
									name, 
									(ScriptedPacketParserFieldType)Enum.Parse(typeof(ScriptedPacketParserFieldType), GetStringAttribute(currentNode, "Type", "")), 
									GetIntegerAttribute(currentNode, "Length", 0), 
									GetByteAttribute(currentNode, "Delimiter", (byte)0x00), 
									GetBooleanAttribute(currentNode, "Delimited", false));

								string xmlValue = GetStringAttribute(currentNode, "Value", "");
								field.Value = xmlValue;
								if (xmlValue.StartsWith("@"))
									field.Value = fieldValues[xmlValue.Substring(1, xmlValue.Length - 1)];

								switch (field.Type)							
								{
									case ScriptedPacketParserFieldType.Byte :
									{
										if (field.Length > 1)
										{
											field.Value = (byte[])field.Value;
											writer.WriteByteArray((byte[])field.Value);
										}
										else
										{
											field.Value = Convert.ToByte(field.Value);
											writer.WriteByte((byte)field.Value);
										}
										break;
									}
									case ScriptedPacketParserFieldType.Int :
									{
										field.Value = Convert.ToInt32(field.Value);
										if (field.Length == 2)
											writer.Write2ByteInteger((int)field.Value);
										if (field.Length == 4)
											writer.Write4ByteInteger((int)field.Value);
										break;
									}
									case ScriptedPacketParserFieldType.UInt :
									{
										field.Value = Convert.ToUInt32(field.Value);
										if (field.Length == 2)
											writer.Write2ByteInteger(Convert.ToInt32(field.Value));
										if (field.Length == 4)
											writer.Write4ByteUnsignedInteger((uint)field.Value);
										break;
									}
									case ScriptedPacketParserFieldType.Ascii :
									{
										field.Value = Convert.ToString(field.Value);
										if (field.Delimited)
											writer.WriteASCIIStringDelimited((string)field.Value, field.Length, field.Delimiter);
										else
											writer.WriteASCIIStringFixedLength((string)field.Value,field.Length, field.Delimiter);
										break;
									}
								}

								data.Add(field.Name, field);
							}
							break;
						}
						case ParseNodeType.Switch :
						{
							//	Based on the value in another field, process the nodes below it.
							string name = GetStringAttribute(currentNode, "Name", "");
							if ((name != null) && (name != ""))
							{
								ScriptedPacketParserField field = (ScriptedPacketParserField)data[name];
								EncodeChildNodes(data, fieldValues, currentNode, writer, field);
							}
							break;
						}
						case ParseNodeType.Case :
						{
							//	Base on the value in a parent field, do each of the entries after it.
							string matchValue = GetStringAttribute(currentNode, "Value", "");
						
							bool matched = false;
							switch (contextField.Type)
							{
								case ScriptedPacketParserFieldType.Byte :
								{
									if (contextField.Length == 1)
										matched = (byte)contextField.Value == Convert.ToByte(Convert.ToInt32(matchValue));
									break;
								}
								case ScriptedPacketParserFieldType.Int : 
								{
									matched = (int)contextField.Value == Int32.Parse(matchValue);
									break;
								}
								case ScriptedPacketParserFieldType.UInt : 
								{
									matched = (uint)contextField.Value == UInt32.Parse(matchValue);
									break;
								}
								case ScriptedPacketParserFieldType.Ascii : 
								{
									matched = (string)contextField.Value == matchValue;
									break;
								}
							}
							if (matched)
							{
								//	process child nodes
								caseMatched = true;
								EncodeChildNodes(data, fieldValues, currentNode, writer, null);
							}
							break;
						}
						case ParseNodeType.Default :
						{
							if (!caseMatched)
							{
								EncodeChildNodes(data, fieldValues, currentNode, writer, null);
							}
							break;
						}
					}
				}
			}
		}

	}
}

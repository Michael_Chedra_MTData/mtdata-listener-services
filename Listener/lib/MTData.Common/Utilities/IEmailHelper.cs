﻿//-----------------------------------------------------------------------
// <copyright file="IEmailHelper.cs" company="Mobile Tracking and Data Pty Ltd">
//     Copyright (c) Mobile Tracking and Data Pty Ltd. All rights reserved.
// </copyright>

namespace MTData.Common.Utilities
{
    using Common.Database;

    /// <summary>
    /// Email helper interface for sending emails.
    /// </summary>
    public interface IEmailHelper
    {
        /// <summary>
        /// Send database version error email.
        /// </summary>
        /// <param name="errorMessage">Database mismatch version error message.</param>
        /// <returns>Returns true on success, otherwise return false.</returns>
        bool SendDatabaseVersionErrorEmail(DatabaseVersionErrorMessage errorMessage);

        /// <summary>
        /// Sends email message.
        /// </summary>
        /// <param name="toEmailAddress">To email address.</param>
        /// <param name="fromEmailAddress">From email address.</param>
        /// <param name="fromDisplayName">From display name.</param>
        /// <param name="subject">Subject of the email.</param>
        /// <param name="body">Body text of the email.</param>
        /// <param name="writeToEventLog">Flag indicating if the email should be logged to event log.</param>
        /// <returns>On success return true, otherwise false.</returns>
        bool SendEmail(string toEmailAddress, string fromEmailAddress, string fromDisplayName, string subject, string body, bool writeToEventLog);
    }
}

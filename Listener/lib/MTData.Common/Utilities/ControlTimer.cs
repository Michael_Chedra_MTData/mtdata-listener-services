using System;
using System.Collections;
using System.Threading;
using MTData.Common.Threading;

namespace MTData.Common.Utilities
{
	/// <summary>
	/// This class provides a mechanism for registering time delays, and methods to be called
	/// and this class will trigger those methods when the time is right.
	/// There are two ways for this to operate. It can call the methods synchronously,
	/// which can result in delays for other methods if one of the registered methods takes
	/// a long time, or it can call the methods asynchronously, relying on the threadpool
	/// to execute the methods concerned. It depends on the requirement of the individual method.
	/// </summary>
	public class ControlTimer
	{
		#region ControlTimerCallbackEntry

		/// <summary>
		/// This class is the callback registration item.
		/// </summary>
		public class ControlTimerCallbackEntry
		{
			private ulong _key = 0;
			private DateTime _timeout = DateTime.MinValue;
			private object _data = null;
			private ControlTimerEventDelegate _callback = null;
			private bool _processAsync = true;

			public ControlTimerCallbackEntry(DateTime timeout, object data, ControlTimerEventDelegate callback, bool processAsync)
			{
				_key = 0;
				_timeout = timeout;
				_data = data;
				_callback = callback;
				_processAsync = processAsync;
			}

			public ulong Key
			{
				get{ return _key; }
				set{ _key = value; }
			}

			public DateTime Timeout
			{
				get{ return _timeout; }
			}

			public object Data
			{
				get{ return _data; }
			}

			public ControlTimerEventDelegate Callback
			{
				get{ return _callback; }
			}

			public bool ProcessAsync
			{
				get{ return _processAsync; }
			}

		}

		#endregion

		#region ControlTimerCallbackEntryComparer
		
		/// <summary>
		/// This class will compare the TimerCallback entries, and sort them appropriately
		/// </summary>
		private class ControlTimerCallbackEntryComparer : IComparer
		{
			/// <summary>
			/// Prepare the class.
			/// </summary>
			public ControlTimerCallbackEntryComparer()
			{
			}

			#region IComparer Members

			/// <summary>
			/// Compare the timeouts of the entries.
			/// </summary>
			/// <param name="x"></param>
			/// <param name="y"></param>
			/// <returns></returns>
			public int Compare(object x, object y)
			{
				if ((x is ControlTimerCallbackEntry) && 
					(y is ControlTimerCallbackEntry))
				{
					ControlTimerCallbackEntry xx = (ControlTimerCallbackEntry)x;
					ControlTimerCallbackEntry yy = (ControlTimerCallbackEntry)y;
					
					if (xx.Timeout < yy.Timeout)
						return -1;
					else if (xx.Timeout == yy.Timeout)
						return 0;
					else
						return 1;
				}
				else
					return 0;
			}

			#endregion
		}

		#endregion

		/// <summary>
		/// This is the format that methods must fulfill to be controled by this class.
		/// </summary>
		public delegate void ControlTimerEventDelegate(ControlTimer sender, object data);

		/// <summary>
		/// This delegate defines a method for passing exceptions back to the main reporting mechanism
		/// </summary>
		public delegate void ControlTimerExceptionDelegate(ControlTimer sender, Exception ex);

		/// <summary>
		/// Event for passing exceptions back to main reporting mechanism
		/// </summary>
		public event ControlTimerExceptionDelegate ExceptionFound;

		/// <summary>
		/// This is the list of entries. It will be sorted by the Comparer above.
		/// </summary>
		private ArrayList _list = ArrayList.Synchronized(new ArrayList());

		/// <summary>
		/// This is the key source for the returned key value.
		/// </summary>
		private ulong _nextKey = 0;

		/// <summary>
		/// This reset event will control interaction between the thread and all lists.
		/// </summary>
		private AutoResetEvent _autoEvent = new AutoResetEvent(false);

		/// <summary>
		/// This method will control the handling of events.
		/// </summary>
		private EnhancedThread _thread = null;

		/// <summary>
		/// Prepare the class for use.
		/// </summary>
		public ControlTimer()
		{
			_thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(EnhancedThreadHandler), this);
		}

		/// <summary>
		/// Cancel a registered callback.
		/// </summary>
		/// <param name="key"></param>
		public void CancelCallback(ulong key)
		{
			lock(_list.SyncRoot)
			{
				int index = -1;
				for(int loop = 0; loop < _list.Count; loop++)
				{
					if (((ControlTimerCallbackEntry)_list[loop]).Key == key)
					{
						index = loop;
						break;
					}
				}

				if (index >= 0)
				{
					_list.RemoveAt(index);
					_list.Sort(new ControlTimerCallbackEntryComparer());
					_autoEvent.Set();
				}
			}
		}

		/// <summary>
		/// Start the control timer.
		/// </summary>
		public void Start()
		{
			_thread.Start();
		}

		/// <summary>
		/// Stop the control timer
		/// </summary>
		public void Stop()
		{
			lock(_list.SyncRoot)
				_list.Clear();

			_thread.Stop();
			_autoEvent.Set();
		}

		/// <summary>
		/// Register a call back function.
		/// </summary>
		/// <param name="entry"></param>
		/// <returns></returns>
		public ulong RegisterCallback(ControlTimerCallbackEntry entry)
		{
			lock(_list.SyncRoot)
			{
				entry.Key = _nextKey++;
				if (_nextKey == ulong.MaxValue)
					_nextKey = 0;

				_list.Add(entry);
				_list.Sort(new ControlTimerCallbackEntryComparer());
				_autoEvent.Set();

			}
			return entry.Key;
		}

		/// <summary>
		/// This method will process the handling of the callbacks.
		/// </summary>
		private object EnhancedThreadHandler(EnhancedThread sender, object data)
		{
			ArrayList buffer = new ArrayList();
			while(!sender.Stopping)
			{
				DateTime timeOfNextUnprocessed = DateTime.MinValue;
				lock(_list.SyncRoot)
				{
					buffer.Clear();
					if (_list.Count > 0)
					{
						DateTime now = DateTime.Now;
						for(int loop = 0; loop < _list.Count; loop++)
						{
							ControlTimerCallbackEntry entry = (ControlTimerCallbackEntry)_list[loop];
							if (entry.Timeout <= now)
								buffer.Add(entry);
							else
							{
								//	we have come far enough in the list.. 
								timeOfNextUnprocessed = entry.Timeout;
								break;
							}
						}

						for(int loop = 0; loop < buffer.Count; loop++)
							_list.RemoveAt(0);
					}
				}

				if (buffer.Count > 0)
				{
					for(int loop = 0; loop < buffer.Count; loop++)
					{
						ControlTimerCallbackEntry entry = (ControlTimerCallbackEntry)buffer[loop];
						try
						{
							if (entry.ProcessAsync)
								entry.Callback.BeginInvoke(this, entry.Data, null, null);
							else
								entry.Callback(this, entry.Data);
						}
						catch(Exception ex)
						{
							if (ExceptionFound != null)
								ExceptionFound(this, ex);
						}
					}
				}

				if (timeOfNextUnprocessed != DateTime.MinValue)
				{
					DateTime now = DateTime.Now;
					if (now < timeOfNextUnprocessed)
						_autoEvent.WaitOne(timeOfNextUnprocessed - now, true);
				}
				else
					_autoEvent.WaitOne();
			}
			return null;
		}
	}
}

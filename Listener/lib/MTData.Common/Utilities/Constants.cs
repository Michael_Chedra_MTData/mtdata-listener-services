using System;

namespace MTData.Common.Utilities
{
	public class Constants
	{
		public const string PersistInsert = "Insert";
		public const string PersistUpdate = "Update";
		public const string PersistDelete = "Delete";
	}
}

using System;
using System.Collections;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for cReporting.
    /// </summary>
    public class Reporting
    {
        public Reporting()
        {
        }

        public static byte[] WriteReportServerRequest(int iUserID, int iFleet, int iVehicle, int iTrailerID, string sReportCommand, string sFilename, DateTime dtDateFrom, DateTime dtDateTo, ArrayList oReasonIDs, int iWPGroupID, ArrayList oWPIDs, decimal dVariable, decimal dVariable2, bool bSearchAllHours, bool bHeadersOverPages, bool bDailySummaries)
        {
            int iTemperatureFormat = -1;
            int iSpeedFormat = -1;
            int iDistanceFormat = -1;
            int iVolumeFormat = -1;
            int iDriverID = -1;
            bool bUseVehicleTimeZone = false;
            int iTimeZone = 0;
            try
            {
                return WriteReportServerRequest(0, iUserID, iFleet, iVehicle, iTrailerID, iDriverID, sReportCommand, sFilename, dtDateFrom, dtDateTo, oReasonIDs, iWPGroupID, oWPIDs, dVariable, dVariable2, bSearchAllHours, bHeadersOverPages, bDailySummaries, iTemperatureFormat, iSpeedFormat, iDistanceFormat, iVolumeFormat, bUseVehicleTimeZone, iTimeZone);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public static byte[] WriteReportServerRequest(int iUserID, int iFleet, int iVehicle, int iTrailerID, string sReportCommand, string sFilename, DateTime dtDateFrom, DateTime dtDateTo, ArrayList oReasonIDs, int iWPGroupID, ArrayList oWPIDs, decimal dVariable, decimal dVariable2, bool bSearchAllHours, bool bHeadersOverPages, bool bDailySummaries, int iTemperatureFormat, int iSpeedFormat, int iDistanceFormat, int iVolumeFormat)
        {
            int iDriverID = -1;
            bool bUseVehicleTimeZone = false;
            int iTimeZone = 0;
            try
            {
                return WriteReportServerRequest(1, iUserID, iFleet, iVehicle, iTrailerID, iDriverID, sReportCommand, sFilename, dtDateFrom, dtDateTo, oReasonIDs, iWPGroupID, oWPIDs, dVariable, dVariable2, bSearchAllHours, bHeadersOverPages, bDailySummaries, iTemperatureFormat, iSpeedFormat, iDistanceFormat, iVolumeFormat, bUseVehicleTimeZone, iTimeZone);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public static byte[] WriteReportServerRequest(int iUserID, int iFleet, int iVehicle, int iTrailerID, int iDriverID, string sReportCommand, string sFilename, DateTime dtDateFrom, DateTime dtDateTo, ArrayList oReasonIDs, int iWPGroupID, ArrayList oWPIDs, decimal dVariable, decimal dVariable2, bool bSearchAllHours, bool bHeadersOverPages, bool bDailySummaries, int iTemperatureFormat, int iSpeedFormat, int iDistanceFormat, int iVolumeFormat)
        {
            bool bUseVehicleTimeZone = false;
            int iTimeZone = 0;
            try
            {
                return WriteReportServerRequest(2, iUserID, iFleet, iVehicle, iTrailerID, iDriverID, sReportCommand, sFilename, dtDateFrom, dtDateTo, oReasonIDs, iWPGroupID, oWPIDs, dVariable, dVariable2, bSearchAllHours, bHeadersOverPages, bDailySummaries, iTemperatureFormat, iSpeedFormat, iDistanceFormat, iVolumeFormat, bUseVehicleTimeZone, iTimeZone);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public static byte[] WriteReportServerRequest(int iUserID, int iFleet, int iVehicle, int iTrailerID, int iDriverID, string sReportCommand, string sFilename, DateTime dtDateFrom, DateTime dtDateTo, ArrayList oReasonIDs, int iWPGroupID, ArrayList oWPIDs, decimal dVariable, decimal dVariable2, bool bSearchAllHours, bool bHeadersOverPages, bool bDailySummaries, int iTemperatureFormat, int iSpeedFormat, int iDistanceFormat, int iVolumeFormat, bool bUseVehicleTimeZone, int iTimeZone)
        {
            try
            {
                return WriteReportServerRequest(3, iUserID, iFleet, iVehicle, iTrailerID, iDriverID, sReportCommand, sFilename, dtDateFrom, dtDateTo, oReasonIDs, iWPGroupID, oWPIDs, dVariable, dVariable2, bSearchAllHours, bHeadersOverPages, bDailySummaries, iTemperatureFormat, iSpeedFormat, iDistanceFormat, iVolumeFormat, bUseVehicleTimeZone, iTimeZone);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public static byte[] WriteReportServerRequest(int iRequestVersion, int iUserID, int iFleet, int iVehicle, int iTrailerID, int iDriverID, string sReportCommand, string sFilename, DateTime dtDateFrom, DateTime dtDateTo, ArrayList oReasonIDs, int iWPGroupID, ArrayList oWPIDs, decimal dVariable, decimal dVariable2, bool bSearchAllHours, bool bHeadersOverPages, bool bDailySummaries, int iTemperatureFormat, int iSpeedFormat, int iDistanceFormat, int iVolumeFormat, bool bUseVehicleTimeZone, int iTimeZone)
        {
            byte[] bData = null;
            byte[] bConvert = null;
            byte[] bIDs = null;
            byte[] bReportName = null;
            byte[] bFromDate = null;
            byte[] bToDate = null;
            byte[] bReasonIDs = null;
            byte[] bWPIDs = null;
            byte[] bVariables = null;
            byte[] bReportFlags = null;
            byte[] bTimeZone = null;


            try
            {
                switch (iRequestVersion)
                {
                    case 0:
                        #region Build v0 report request bytes
                        #region Get the fleet and unit IDs - bIDs = [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                        bIDs = BitConverter.GetBytes(iUserID);											// User ID (4 Bytes)
                        bConvert = BitConverter.GetBytes(iFleet);
                        bIDs = PacketUtilities.AppendToPacket(bIDs, bConvert[0]);			// Fleet ID (1 Byte)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iVehicle);					// VehicleID (4 Bytes)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iTrailerID);				// Trailer ID (4 Bytes)
                        #endregion
                        #region Get the Report Name - bReportName = [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bReportName = System.Text.ASCIIEncoding.ASCII.GetBytes(sReportCommand);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, System.Text.ASCIIEncoding.ASCII.GetBytes(sFilename));
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        #endregion
                        #region Get the to and from dates - bFromDate and bToDate - Format = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        #region Get the from date - bFromDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateFrom.Day);
                        bFromDate = new byte[1];
                        bFromDate[0] = bConvert[0];																			// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Month);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Year);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Year (2 Bytes)
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateFrom.Hour);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Minute);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Second);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Second (1 Byte)
                        #endregion
                        #region Get the to date - bToDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateTo.Day);
                        bToDate = new byte[1];
                        bToDate[0] = bConvert[0];																				// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Month);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Year);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Year (2 Bytes)
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateTo.Hour);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Minute);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Second);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Second (1 Byte)
                        #endregion
                        #endregion
                        #region Get the Reason IDs -  bReasonIDs = [Num Of Reasons (2)]([ReasonID (4)])
                        bReasonIDs = new byte[2];
                        bReasonIDs[0] = (byte)0x00;
                        bReasonIDs[1] = (byte)0x00;
                        if (oReasonIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oReasonIDs.Count);
                            bReasonIDs[0] = bConvert[0];
                            bReasonIDs[1] = bConvert[1];
                            for (int iReason = 0; iReason < oReasonIDs.Count; iReason++)
                            {
                                bConvert = BitConverter.GetBytes((int)oReasonIDs[iReason]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[0]);	// Reason ID (4 Bytes)
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[1]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[2]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[3]);
                            }
                        }
                        #endregion
                        #region Get the  WP IDs - bWPIDs = [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bWPIDs = new byte[2];
                        bWPIDs[0] = (byte)0x00;
                        bWPIDs[1] = (byte)0x00;
                        if (oWPIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oWPIDs.Count);
                            bWPIDs[0] = bConvert[0];																			// Num of WP IDs
                            bWPIDs[1] = bConvert[1];
                            bConvert = BitConverter.GetBytes(iWPGroupID);										// WP Group ID
                            bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert);
                            for (int iWP = 0; iWP < oWPIDs.Count; iWP++)
                            {
                                bConvert = BitConverter.GetBytes((int)oWPIDs[iWP]);
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[0]);	// WP ID (2 Bytes)
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[1]);
                            }
                        }
                        #endregion
                        #region Get the variable values - bVariables = [Variable 1 (4)][Variable 2 (4)]
                        bVariables = new byte[8];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable));
                        bVariables[0] = bConvert[0];
                        bVariables[1] = bConvert[1];
                        bVariables[2] = bConvert[2];
                        bVariables[3] = bConvert[3];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable2));
                        bVariables[4] = bConvert[0];
                        bVariables[5] = bConvert[1];
                        bVariables[6] = bConvert[2];
                        bVariables[7] = bConvert[3];
                        #endregion
                        #region Get the report flags - bReportFlags = [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)]
                        bReportFlags = new byte[3];
                        if (bSearchAllHours)
                            bReportFlags[0] = (byte)0x01;
                        if (bHeadersOverPages)
                            bReportFlags[1] = (byte)0x01;
                        if (bDailySummaries)
                            bReportFlags[2] = (byte)0x01;
                        #endregion

                        bData = new byte[3];
                        bData[0] = (byte)0x01;		// [SOP (1)]
                        bData[1] = (byte)0x00;		// [Length (2)]
                        bData[2] = (byte)0x00;
                        bData = PacketUtilities.AppendToPacket(bData, bIDs);					// [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportName);	// [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bFromDate);		// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bToDate);			// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bReasonIDs);		// [Num Of Reasons (2)]([ReasonID (2)])
                        bData = PacketUtilities.AppendToPacket(bData, bWPIDs);			// [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bData = PacketUtilities.AppendToPacket(bData, bVariables);		// [Variable 1 (4)][Variable 2 (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportFlags);	// [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)]
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x01);		// [EOP (1)]
                        // Set the length bytes
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(bData.Length));
                        bData[1] = bConvert[0];		// [Length (2)]
                        bData[2] = bConvert[1];
                        #endregion
                        break;
                    case 1:
                        #region Build v1 report request bytes
                        #region Get the fleet and unit IDs - bIDs = [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                        bIDs = BitConverter.GetBytes(iUserID);											// User ID (4 Bytes)
                        bConvert = BitConverter.GetBytes(iFleet);
                        bIDs = PacketUtilities.AppendToPacket(bIDs, bConvert[0]);			// Fleet ID (1 Byte)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iVehicle);					// VehicleID (4 Bytes)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iTrailerID);				// Trailer ID (4 Bytes)
                        #endregion
                        #region Get the Report Name - bReportName = [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bReportName = System.Text.ASCIIEncoding.ASCII.GetBytes(sReportCommand);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, System.Text.ASCIIEncoding.ASCII.GetBytes(sFilename));
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        #endregion
                        #region Get the to and from dates - bFromDate and bToDate - Format = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        #region Get the from date - bFromDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateFrom.Day);
                        bFromDate = new byte[1];
                        bFromDate[0] = bConvert[0];																			// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Month);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Year);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Year (2 Bytes)
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateFrom.Hour);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Minute);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Second);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Second (1 Byte)
                        #endregion
                        #region Get the to date - bToDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateTo.Day);
                        bToDate = new byte[1];
                        bToDate[0] = bConvert[0];																				// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Month);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Year);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Year (2 Bytes)
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateTo.Hour);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Minute);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Second);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Second (1 Byte)
                        #endregion
                        #endregion
                        #region Get the Reason IDs -  bReasonIDs = [Num Of Reasons (2)]([ReasonID (4)])
                        bReasonIDs = new byte[2];
                        bReasonIDs[0] = (byte)0x00;
                        bReasonIDs[1] = (byte)0x00;
                        if (oReasonIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oReasonIDs.Count);
                            bReasonIDs[0] = bConvert[0];
                            bReasonIDs[1] = bConvert[1];
                            for (int iReason = 0; iReason < oReasonIDs.Count; iReason++)
                            {
                                bConvert = BitConverter.GetBytes((int)oReasonIDs[iReason]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[0]);	// Reason ID (4 Bytes)
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[1]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[2]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[3]);
                            }
                        }
                        #endregion
                        #region Get the  WP IDs - bWPIDs = [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bWPIDs = new byte[2];
                        bWPIDs[0] = (byte)0x00;
                        bWPIDs[1] = (byte)0x00;
                        if (oWPIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oWPIDs.Count);
                            bWPIDs[0] = bConvert[0];																			// Num of WP IDs
                            bWPIDs[1] = bConvert[1];
                            bConvert = BitConverter.GetBytes(iWPGroupID);										// WP Group ID
                            bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert);
                            for (int iWP = 0; iWP < oWPIDs.Count; iWP++)
                            {
                                bConvert = BitConverter.GetBytes((int)oWPIDs[iWP]);
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[0]);	// WP ID (2 Bytes)
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[1]);
                            }
                        }
                        #endregion
                        #region Get the variable values - bVariables = [Variable 1 (4)][Variable 2 (4)]
                        bVariables = new byte[8];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable));
                        bVariables[0] = bConvert[0];
                        bVariables[1] = bConvert[1];
                        bVariables[2] = bConvert[2];
                        bVariables[3] = bConvert[3];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable2));
                        bVariables[4] = bConvert[0];
                        bVariables[5] = bConvert[1];
                        bVariables[6] = bConvert[2];
                        bVariables[7] = bConvert[3];
                        #endregion
                        #region Get the report flags - bReportFlags = [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)][Temperature Format (1)][Speed Format (1)][Distance Format (1)][Volume Format (1)]
                        bReportFlags = new byte[7];
                        if (bSearchAllHours)
                            bReportFlags[0] = (byte)0x01;
                        if (bHeadersOverPages)
                            bReportFlags[1] = (byte)0x01;
                        if (bDailySummaries)
                            bReportFlags[2] = (byte)0x01;
                        if (iTemperatureFormat == 1)
                            bReportFlags[3] = (byte)0x01;
                        if (iSpeedFormat == 1)
                            bReportFlags[4] = (byte)0x01;
                        if (iDistanceFormat == 1)
                            bReportFlags[5] = (byte)0x01;
                        if (iVolumeFormat == 1)
                            bReportFlags[6] = (byte)0x01;
                        #endregion

                        bData = new byte[3];
                        bData[0] = (byte)0x02;		// [SOP (1)]
                        bData[1] = (byte)0x00;		// [Length (2)]
                        bData[2] = (byte)0x00;
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x01);		// [Report Request Protocol Version]
                        bData = PacketUtilities.AppendToPacket(bData, bIDs);					// [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportName);	// [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bFromDate);		// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bToDate);			// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bReasonIDs);		// [Num Of Reasons (2)]([ReasonID (4)])
                        bData = PacketUtilities.AppendToPacket(bData, bWPIDs);			// [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bData = PacketUtilities.AppendToPacket(bData, bVariables);		// [Variable 1 (4)][Variable 2 (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportFlags);	// [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)][Temperature Format (1)][Speed Format (1)][Distance Format (1)][Volume Format (1)]
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x01);		// [EOP (1)]
                        // Set the length bytes
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(bData.Length));
                        bData[1] = bConvert[0];		// [Length (2)]
                        bData[2] = bConvert[1];
                        #endregion
                        break;
                    case 2:
                        #region Build v2 report request bytes
                        #region Get the fleet and unit IDs - bIDs = [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                        bIDs = BitConverter.GetBytes(iUserID);											// User ID (4 Bytes)
                        bConvert = BitConverter.GetBytes(iFleet);
                        bIDs = PacketUtilities.AppendToPacket(bIDs, bConvert[0]);			// Fleet ID (1 Byte)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iVehicle);					// VehicleID (4 Bytes)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iTrailerID);				// Trailer ID (4 Bytes)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iDriverID);				// Driver ID (4 Bytes)
                        #endregion
                        #region Get the Report Name - bReportName = [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bReportName = System.Text.ASCIIEncoding.ASCII.GetBytes(sReportCommand);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, System.Text.ASCIIEncoding.ASCII.GetBytes(sFilename));
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        #endregion
                        #region Get the to and from dates - bFromDate and bToDate - Format = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        #region Get the from date - bFromDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateFrom.Day);
                        bFromDate = new byte[1];
                        bFromDate[0] = bConvert[0];																			// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Month);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Year);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Year (2 Bytes)
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateFrom.Hour);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Minute);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Second);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Second (1 Byte)
                        #endregion
                        #region Get the to date - bToDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateTo.Day);
                        bToDate = new byte[1];
                        bToDate[0] = bConvert[0];																				// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Month);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Year);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Year (2 Bytes)
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateTo.Hour);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Minute);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Second);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Second (1 Byte)
                        #endregion
                        #endregion
                        #region Get the Reason IDs -  bReasonIDs = [Num Of Reasons (2)]([ReasonID (4)])
                        bReasonIDs = new byte[2];
                        bReasonIDs[0] = (byte)0x00;
                        bReasonIDs[1] = (byte)0x00;
                        if (oReasonIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oReasonIDs.Count);
                            bReasonIDs[0] = bConvert[0];
                            bReasonIDs[1] = bConvert[1];
                            for (int iReason = 0; iReason < oReasonIDs.Count; iReason++)
                            {
                                bConvert = BitConverter.GetBytes((int)oReasonIDs[iReason]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[0]);	// Reason ID (4 Bytes)
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[1]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[2]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[3]);
                            }
                        }
                        #endregion
                        #region Get the  WP IDs - bWPIDs = [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bWPIDs = new byte[2];
                        bWPIDs[0] = (byte)0x00;
                        bWPIDs[1] = (byte)0x00;
                        if (oWPIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oWPIDs.Count);
                            bWPIDs[0] = bConvert[0];																			// Num of WP IDs
                            bWPIDs[1] = bConvert[1];
                            bConvert = BitConverter.GetBytes(iWPGroupID);										// WP Group ID
                            bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert);
                            for (int iWP = 0; iWP < oWPIDs.Count; iWP++)
                            {
                                bConvert = BitConverter.GetBytes((int)oWPIDs[iWP]);
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[0]);	// WP ID (2 Bytes)
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[1]);
                            }
                        }
                        #endregion
                        #region Get the variable values - bVariables = [Variable 1 (4)][Variable 2 (4)]
                        bVariables = new byte[8];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable));
                        bVariables[0] = bConvert[0];
                        bVariables[1] = bConvert[1];
                        bVariables[2] = bConvert[2];
                        bVariables[3] = bConvert[3];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable2));
                        bVariables[4] = bConvert[0];
                        bVariables[5] = bConvert[1];
                        bVariables[6] = bConvert[2];
                        bVariables[7] = bConvert[3];
                        #endregion
                        #region Get the report flags - bReportFlags = [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)]
                        bReportFlags = new byte[7];
                        if (bSearchAllHours)
                            bReportFlags[0] = (byte)0x01;
                        if (bHeadersOverPages)
                            bReportFlags[1] = (byte)0x01;
                        if (bDailySummaries)
                            bReportFlags[2] = (byte)0x01;
                        if (iTemperatureFormat == 1)
                            bReportFlags[3] = (byte)0x01;
                        if (iSpeedFormat == 1)
                            bReportFlags[4] = (byte)0x01;
                        if (iDistanceFormat == 1)
                            bReportFlags[5] = (byte)0x01;
                        if (iVolumeFormat == 1)
                            bReportFlags[6] = (byte)0x01;
                        #endregion

                        bData = new byte[3];
                        bData[0] = (byte)0x02;		// [SOP (1)]
                        bData[1] = (byte)0x00;		// [Length (2)]
                        bData[2] = (byte)0x00;
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x02);		// [Report Request Protocol Version]
                        bData = PacketUtilities.AppendToPacket(bData, bIDs);					// [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)][DriverID (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportName);	// [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bFromDate);		// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bToDate);			// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bReasonIDs);		// [Num Of Reasons (2)]([ReasonID (4)])
                        bData = PacketUtilities.AppendToPacket(bData, bWPIDs);			// [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bData = PacketUtilities.AppendToPacket(bData, bVariables);		// [Variable 1 (4)][Variable 2 (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportFlags);	// [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)][Temperature Format (1)][Speed Format (1)][Distance Format (1)][Volume Format (1)]
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x01);		// [EOP (1)]
                        // Set the length bytes
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(bData.Length));
                        bData[1] = bConvert[0];		// [Length (2)]
                        bData[2] = bConvert[1];
                        #endregion
                        break;
                    case 3:
                        #region Build v3 report request bytes
                        #region Get the fleet and unit IDs - bIDs = [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)][DriverID (4)]
                        bIDs = BitConverter.GetBytes(iUserID);											// User ID (4 Bytes)
                        bConvert = BitConverter.GetBytes(iFleet);
                        bIDs = PacketUtilities.AppendToPacket(bIDs, bConvert[0]);			// Fleet ID (1 Byte)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iVehicle);					// VehicleID (4 Bytes)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iTrailerID);				// Trailer ID (4 Bytes)
                        bIDs = PacketUtilities.AppendToPacket(bIDs, iDriverID);				// Driver ID (4 Bytes)
                        #endregion
                        #region Get the Report Name - bReportName = [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bReportName = System.Text.ASCIIEncoding.ASCII.GetBytes(sReportCommand);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        bReportName = PacketUtilities.AppendToPacket(bReportName, System.Text.ASCIIEncoding.ASCII.GetBytes(sFilename));
                        bReportName = PacketUtilities.AppendToPacket(bReportName, (byte)0x05);
                        #endregion
                        #region Get the to and from dates - bFromDate and bToDate - Format = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        #region Get the from date - bFromDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateFrom.Day);
                        bFromDate = new byte[1];
                        bFromDate[0] = bConvert[0];																			// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Month);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Year);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Year (2 Bytes)
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateFrom.Hour);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Minute);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateFrom.Second);
                        bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Second (1 Byte)
                        #endregion
                        #region Get the to date - bToDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bConvert = BitConverter.GetBytes(dtDateTo.Day);
                        bToDate = new byte[1];
                        bToDate[0] = bConvert[0];																				// Day (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Month);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Month (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Year);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Year (2 Bytes)
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[1]);
                        bConvert = BitConverter.GetBytes(dtDateTo.Hour);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Hour (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Minute);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Minute (1 Byte)
                        bConvert = BitConverter.GetBytes(dtDateTo.Second);
                        bToDate = PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Second (1 Byte)
                        #endregion
                        #endregion
                        #region Get the Reason IDs -  bReasonIDs = [Num Of Reasons (2)]([ReasonID (4)])
                        bReasonIDs = new byte[2];
                        bReasonIDs[0] = (byte)0x00;
                        bReasonIDs[1] = (byte)0x00;
                        if (oReasonIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oReasonIDs.Count);
                            bReasonIDs[0] = bConvert[0];
                            bReasonIDs[1] = bConvert[1];
                            for (int iReason = 0; iReason < oReasonIDs.Count; iReason++)
                            {
                                bConvert = BitConverter.GetBytes((int)oReasonIDs[iReason]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[0]);	// Reason ID (4 Bytes)
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[1]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[2]);
                                bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[3]);
                            }
                        }
                        #endregion
                        #region Get the  WP IDs - bWPIDs = [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bWPIDs = new byte[2];
                        bWPIDs[0] = (byte)0x00;
                        bWPIDs[1] = (byte)0x00;
                        if (oWPIDs.Count > 0)
                        {
                            bConvert = BitConverter.GetBytes(oWPIDs.Count);
                            bWPIDs[0] = bConvert[0];																			// Num of WP IDs
                            bWPIDs[1] = bConvert[1];
                            bConvert = BitConverter.GetBytes(iWPGroupID);										// WP Group ID
                            bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert);
                            for (int iWP = 0; iWP < oWPIDs.Count; iWP++)
                            {
                                bConvert = BitConverter.GetBytes((int)oWPIDs[iWP]);
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[0]);	// WP ID (2 Bytes)
                                bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[1]);
                            }
                        }
                        #endregion
                        #region Get the variable values - bVariables = [Variable 1 (4)][Variable 2 (4)]
                        bVariables = new byte[8];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable));
                        bVariables[0] = bConvert[0];
                        bVariables[1] = bConvert[1];
                        bVariables[2] = bConvert[2];
                        bVariables[3] = bConvert[3];
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable2));
                        bVariables[4] = bConvert[0];
                        bVariables[5] = bConvert[1];
                        bVariables[6] = bConvert[2];
                        bVariables[7] = bConvert[3];
                        #endregion
                        #region Get the report flags - bReportFlags = [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)][Temperature Format (1)][Speed Format (1)][Distance Format (1)][Volume Format (1)]
                        bReportFlags = new byte[7];
                        if (bSearchAllHours)
                            bReportFlags[0] = (byte)0x01;
                        if (bHeadersOverPages)
                            bReportFlags[1] = (byte)0x01;
                        if (bDailySummaries)
                            bReportFlags[2] = (byte)0x01;
                        if (iTemperatureFormat == 1)
                            bReportFlags[3] = (byte)0x01;
                        if (iSpeedFormat == 1)
                            bReportFlags[4] = (byte)0x01;
                        if (iDistanceFormat == 1)
                            bReportFlags[5] = (byte)0x01;
                        if (iVolumeFormat == 1)
                            bReportFlags[6] = (byte)0x01;
                        #endregion
                        #region Get the time zone selection info - bTimeZone = [UseVehicleTimeZone (1)][TimeZoneID (4)]
                        bTimeZone = new byte[5];
                        if (bUseVehicleTimeZone)
                            bTimeZone[0] = (byte)0x01;
                        else
                            bTimeZone[0] = (byte)0x00;
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(iTimeZone));
                        bTimeZone[1] = bConvert[0];
                        bTimeZone[2] = bConvert[1];
                        bTimeZone[3] = bConvert[2];
                        bTimeZone[4] = bConvert[3];
                        #endregion

                        bData = new byte[3];
                        bData[0] = (byte)0x02;		// [SOP (1)]
                        bData[1] = (byte)0x00;		// [Length (2)]
                        bData[2] = (byte)0x00;
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x03);		// [Report Request Protocol Version]
                        bData = PacketUtilities.AppendToPacket(bData, bIDs);					// [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)][DriverID (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportName);	// [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bFromDate);		// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bToDate);			// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bReasonIDs);		// [Num Of Reasons (2)]([ReasonID (4)])
                        bData = PacketUtilities.AppendToPacket(bData, bWPIDs);			// [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
                        bData = PacketUtilities.AppendToPacket(bData, bVariables);		// [Variable 1 (4)][Variable 2 (4)]
                        bData = PacketUtilities.AppendToPacket(bData, bReportFlags);	// [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)][Temperature Format (1)][Speed Format (1)][Distance Format (1)][Volume Format (1)]
                        bData = PacketUtilities.AppendToPacket(bData, bTimeZone);		// [UseVehicleTimeZone (1)][TimeZoneID (4)]
                        bData = PacketUtilities.AppendToPacket(bData, (byte)0x01);		// [EOP (1)]
                        // Set the length bytes
                        bConvert = BitConverter.GetBytes(Convert.ToInt32(bData.Length));
                        bData[1] = bConvert[0];		// [Length (2)]
                        bData[2] = bConvert[1];
                        #endregion
                        break;
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return bData;
        }

        public static byte[] ReadReportServerRequest(byte[] bParsed, ref int iUserID, ref int iFleet, ref int iVehicle, ref int iTrailerID, ref int iDriverID, ref string sReportName, ref string sReportCommand, ref string sFilename, ref DateTime dtDateFrom, ref DateTime dtDateTo, ref ArrayList oReasonIDs, ref int iWPGroupID, ref ArrayList oWPIDs, ref decimal dVariable, ref decimal dVariable2, ref bool bSearchAllHours, ref bool bHeadersOverPages, ref bool bDailySummaries, ref int iTemperatureFormat, ref int iSpeedFormat, ref int iDistanceFormat, ref int iVolumeFormat)
        {
            bool bTimeZonesEnabled = false;
            bool bUseVehicleTimeZone = false;
            int iTimeZoneID = 0;
            try
            {
                return ReadReportServerRequest(bParsed, ref iUserID, ref iFleet, ref iVehicle, ref iTrailerID, ref iDriverID, ref sReportName, ref sReportCommand, ref sFilename, ref dtDateFrom, ref dtDateTo, ref oReasonIDs, ref iWPGroupID, ref oWPIDs, ref dVariable, ref dVariable2, ref bSearchAllHours, ref bHeadersOverPages, ref bDailySummaries, ref iTemperatureFormat, ref iSpeedFormat, ref iDistanceFormat, ref iVolumeFormat, ref bTimeZonesEnabled, ref bUseVehicleTimeZone, ref iTimeZoneID);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public static byte[] ReadReportServerRequest(byte[] bParsed, ref int iUserID, ref int iFleet, ref int iVehicle, ref int iTrailerID, ref int iDriverID, ref string sReportName, ref string sReportCommand, ref string sFilename, ref DateTime dtDateFrom, ref DateTime dtDateTo, ref ArrayList oReasonIDs, ref int iWPGroupID, ref ArrayList oWPIDs, ref decimal dVariable, ref decimal dVariable2, ref bool bSearchAllHours, ref bool bHeadersOverPages, ref bool bDailySummaries, ref int iTemperatureFormat, ref int iSpeedFormat, ref int iDistanceFormat, ref int iVolumeFormat, ref bool bTimeZonesEnabled, ref bool bUseVehicleTimeZone, ref int iTimeZoneID)
        {
            bool bCheckVersion = false;
            int iProtocolVersion = 0;
            byte[] bData = null;
            byte[] bConvert = null;
            byte bTemp = (byte)0x00;
            int iPos = 0;
            int iPacketLength = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            int iItemCount = 0;
            int iItemID = 0;

            // Report Request.
            // [SOP (1)][Length (2)]
            // [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
            // [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
            // [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
            // [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
            // [Num Of Reasons (2)]([ReasonID (2)])
            // [Num Of WP IDs (2)]([WP ID (2)])
            // [Variable 1 (4)][Variable 2 (4)]
            // [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)]
            // [EOP (1)]

            try
            {
                if (oReasonIDs == null)
                    oReasonIDs = new ArrayList();
                if (oWPIDs == null)
                    oWPIDs = new ArrayList();
                bTimeZonesEnabled = false;
                #region Parse the data
                #region Check the packet looks valid
                if (bParsed == null)
                {
                    throw (new System.Exception("Invalid Request."));
                }
                if (bParsed[iPos] != (byte)0x01)	// SOP = 0x01 = Version 0 of protocol, protocol version bytes are not included in the packet
                {
                    if (bParsed[iPos] != (byte)0x02) // SOP = 0x02 = Version 1+ proctocol, protocol version bytes are included in the packet.
                    {
                        throw (new System.Exception("Invalid Request, no SOP was found."));
                    }
                    else
                    {
                        bCheckVersion = true;
                    }
                }
                else
                {
                    iProtocolVersion = 0;
                    bCheckVersion = false;
                }
                iPos++;
                if (bParsed.Length < 4)
                {
                    throw (new System.Exception("Invalid Request, packet is too short."));
                }
                bConvert = new byte[4];
                bConvert[0] = bParsed[iPos++];
                bConvert[1] = bParsed[iPos++];
                iPacketLength = BitConverter.ToInt32(bConvert, 0);
                if (bParsed.Length < iPacketLength)
                {
                    throw (new System.Exception("Invalid Request, packet is too short."));
                }
                if (bParsed[iPacketLength - 1] != (byte)0x01)
                {
                    throw (new System.Exception("Invalid Request, EOP is not in the correct location."));
                }

                #region If the protocol version is included in the packet, then read it from the next byte
                if (bCheckVersion)
                {
                    bTemp = bParsed[iPos++];
                    iProtocolVersion = (int)bTemp;
                }
                #endregion
                bData = new byte[iPacketLength];
                for (int X = 0; X < iPacketLength; X++)
                    bData[X] = bParsed[X];

                #endregion
                if (iProtocolVersion == 0)
                {
                    #region Protocol v0 Report Request
                    #region The ID Values [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iUserID = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iFleet = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iVehicle = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iTrailerID = BitConverter.ToInt32(bConvert, 0);
                    // Driver is not in  this version of the protocol
                    iDriverID = -1;
                    #endregion
                    #region Read [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                    sReportCommand = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sReportCommand += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read report name."));
                    }
                    sFilename = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sFilename += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read file name."));
                    }
                    #endregion
                    #region Read the From Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateFrom = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the To Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateTo = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the reason IDs - [Num Of Reasons (2)]([ReasonID (2)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// Reason Count
                    if (iItemCount > 0)
                    {
                        for (int X = 0; X < iItemCount; X++)
                        {
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                //cGlobals.SendMsg("Error : Invalid Request, failed to read reason ID (" + Convert.ToString(X) + ".", ref networkStream);
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// Reason ID
                            oReasonIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read reasons."));
                    }
                    #endregion
                    #region Read the WP IDs - [Num Of WP IDs (2)]([WP ID (2)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// WP ID Count
                    if (iItemCount > 0)
                    {
                        bConvert = new byte[4];
                        bConvert[0] = bParsed[iPos++];
                        bConvert[1] = bParsed[iPos++];
                        bConvert[2] = bParsed[iPos++];
                        bConvert[3] = bParsed[iPos++];
                        iWPGroupID = BitConverter.ToInt32(bConvert, 0);		// WP Group ID
                        for (int X = 0; X < iItemCount; X++)
                        {
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// WP ID
                            oWPIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read WP IDs."));
                    }
                    #endregion
                    #region Read the variable values
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 1
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable2 = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 2
                    #endregion
                    #region Read the flags
                    if (bParsed[iPos++] == (byte)0x01)
                        bSearchAllHours = true;
                    else
                        bSearchAllHours = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bHeadersOverPages = true;
                    else
                        bHeadersOverPages = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bDailySummaries = true;
                    else
                        bDailySummaries = false;
                    iTemperatureFormat = 1;
                    #endregion
                    #region Resolve the report name
                    switch (sReportCommand)
                    {
                        #region Driver Reports
                        case "TripReportExtended":
                            sReportName = "Trip Report";
                            break;
                        case "TripReportByVehicleExtended":
                            sReportName = "Trip Report By Vehicle";
                            break;
                        case "DriverPerformanceReport":
                            sReportName = "Performance Report";
                            break;
                        case "StationaryPeriodReport":
                            sReportName = "Stationary Period Report";
                            break;
                        case "ExceptionsReport":
                            if (iVehicle > 0)
                                sReportName = "Vehicle Exception Report";
                            else
                                sReportName = "Fleet Exception Report";
                            break;
                        case "ExceptionsGraphReport":
                            sReportName = "Exception Graph";
                            break;
                        case "NoOverSpeedsGraphReport":
                            sReportName = "OverSpeed Graph";
                            break;
                        case "DistanceAndSpeedVsTime":
                            sReportName = "Speed Graph";
                            break;
                        case "UtilizationReport":
                            sReportName = "Utilization Report";
                            break;
                        case "FatigueManagement":
                            sReportName = "Fatigue Management By Vehicle";
                            break;
                        #endregion
                        #region Vehicle Reports
                        case "EngineReport":
                            sReportName = "ECM Report";
                            break;
                        case "VehicleDetailedReport":
                            sReportName = "Vehicle Details Report";
                            break;
                        case "AvgFuelGraphReport":
                            sReportName = "Fuel Economy Graph";
                            break;
                        case "LineHaulReport":
                            sReportName = "Line Haul Report";
                            break;
                        case "LocationReport":
                            sReportName = "Location Report";
                            break;
                        case "UsageReportByVehicle":
                            sReportName = "Usage Report By Vehicle";
                            break;
                        case "OutofCityReport":
                            sReportName = "Out of City Report";
                            break;
                        #endregion
                        #region Way Point Reports
                        case "CustomerReport":
                            sReportName = "Way Point Report";
                            break;
                        case "CustomerExceptionsReport":
                            sReportName = "Way Point Exception Report";
                            break;
                        case "DockTimeReport":
                            sReportName = "Dock Time Report";
                            break;
                        case "DockTimeExceptionGraph":
                            sReportName = "Customer Exception Graph";
                            break;
                        #endregion
                        #region Concrete Reports
                        case "ConcreteTripReport":
                            sReportName = "Concrete Trip Report";
                            break;
                        case "ConcreteStatusReport":
                            sReportName = "Concrete Status Report";
                            break;
                        #endregion
                        #region Trailer Reports
                        case "TrailerLocationReport":
                            sReportName = "Trailer Location Report";
                            break;
                        case "TrailerStationaryReport":
                            sReportName = "Trailer Stationary Report";
                            break;
                        case "TrailerEfficiencyReport":
                            sReportName = "Trailer Utilization Report";
                            break;
                        case "TrailerLineHaulReport":
                            sReportName = "Trailer Line Haul Report";
                            break;
                        #endregion
                        default:
                            sReportName = "";
                            break;
                    }
                    #endregion
                    #endregion
                }
                else if (iProtocolVersion == 1)
                {
                    #region Protocol v1 Report Request
                    #region The ID Values [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iUserID = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iFleet = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iVehicle = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iTrailerID = BitConverter.ToInt32(bConvert, 0);
                    // Driver is not in  this version of the protocol
                    iDriverID = -1;
                    #endregion
                    #region Read [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                    sReportCommand = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sReportCommand += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read report name."));
                    }
                    sFilename = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sFilename += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read file name."));
                    }
                    #endregion
                    #region Read the From Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateFrom = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the To Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateTo = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the reason IDs - [Num Of Reasons (2)]([ReasonID (4)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// Reason Count
                    if (iItemCount > 0)
                    {
                        for (int X = 0; X < iItemCount; X++)
                        {
                            #region Read the reason ID
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[2] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[3] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// Reason ID
                            #endregion
                            oReasonIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read reasons."));
                    }
                    #endregion
                    #region Read the WP IDs - [Num Of WP IDs (2)]([WP ID (2)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// WP ID Count
                    if (iItemCount > 0)
                    {
                        bConvert = new byte[4];
                        bConvert[0] = bParsed[iPos++];
                        bConvert[1] = bParsed[iPos++];
                        bConvert[2] = bParsed[iPos++];
                        bConvert[3] = bParsed[iPos++];
                        iWPGroupID = BitConverter.ToInt32(bConvert, 0);		// WP Group ID
                        for (int X = 0; X < iItemCount; X++)
                        {
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// WP ID
                            oWPIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read WP IDs."));
                    }
                    #endregion
                    #region Read the variable values
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 1
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable2 = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 2
                    #endregion
                    #region Read the flags
                    if (bParsed[iPos++] == (byte)0x01)
                        bSearchAllHours = true;
                    else
                        bSearchAllHours = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bHeadersOverPages = true;
                    else
                        bHeadersOverPages = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bDailySummaries = true;
                    else
                        bDailySummaries = false;

                    iTemperatureFormat = (int)bParsed[iPos++];
                    iSpeedFormat = (int)bParsed[iPos++];
                    iDistanceFormat = (int)bParsed[iPos++];
                    iVolumeFormat = (int)bParsed[iPos++];
                    #endregion
                    #region Resolve the report name
                    switch (sReportCommand)
                    {
                        #region Driver Reports
                        case "TripReportExtended":
                            sReportName = "Trip Report";
                            break;
                        case "TripReportByVehicleExtended":
                            sReportName = "Trip Report By Vehicle";
                            break;
                        case "DriverPerformanceReport":
                            sReportName = "Performance Report";
                            break;
                        case "StationaryPeriodReport":
                            sReportName = "Stationary Period Report";
                            break;
                        case "ExceptionsReport":
                            if (iVehicle > 0)
                                sReportName = "Vehicle Exception Report";
                            else
                                sReportName = "Fleet Exception Report";
                            break;
                        case "ExceptionsGraphReport":
                            sReportName = "Exception Graph";
                            break;
                        case "NoOverSpeedsGraphReport":
                            sReportName = "OverSpeed Graph";
                            break;
                        case "DistanceAndSpeedVsTime":
                            sReportName = "Speed Graph";
                            break;
                        case "UtilizationReport":
                            sReportName = "Utilization Report";
                            break;
                        case "FatigueManagement":
                            sReportName = "Fatigue Management By Vehicle";
                            break;
                        case "FatigueManagementDriver":
                            sReportName = "Fatigue Management Driver Report";
                            break;
                        #endregion
                        #region Vehicle Reports
                        case "EngineReport":
                            sReportName = "ECM Report";
                            break;
                        case "VehicleDetailedReport":
                            sReportName = "Vehicle Details Report";
                            break;
                        case "AvgFuelGraphReport":
                            sReportName = "Fuel Economy Graph";
                            break;
                        case "LineHaulReport":
                            sReportName = "Line Haul Report";
                            break;
                        case "LocationReport":
                            sReportName = "Location Report";
                            break;
                        case "UsageReportByVehicle":
                            sReportName = "Usage Report By Vehicle";
                            break;
                        case "OutofCityReport":
                            sReportName = "Out of City Report";
                            break;
                        #endregion
                        #region Way Point Reports
                        case "CustomerReport":
                            sReportName = "Way Point Report";
                            break;
                        case "CustomerExceptionsReport":
                            sReportName = "Way Point Exception Report";
                            break;
                        case "DockTimeReport":
                            sReportName = "Dock Time Report";
                            break;
                        case "DockTimeExceptionGraph":
                            sReportName = "Customer Exception Graph";
                            break;
                        #endregion
                        #region Concrete Reports
                        case "ConcreteTripReport":
                            sReportName = "Concrete Trip Report";
                            break;
                        case "ConcreteStatusReport":
                            sReportName = "Concrete Status Report";
                            break;
                        #endregion
                        #region Trailer Reports
                        case "TrailerLocationReport":
                            sReportName = "Trailer Location Report";
                            break;
                        case "TrailerStationaryReport":
                            sReportName = "Trailer Stationary Report";
                            break;
                        case "TrailerEfficiencyReport":
                            sReportName = "Trailer Utilization Report";
                            break;
                        case "TrailerLineHaulReport":
                            sReportName = "Trailer Line Haul Report";
                            break;
                        #endregion
                        #region Refrigeration Report
                        case "RefrigerationDetailedReport":
                            sReportName = "Refrigeration Detailed Report";
                            break;
                        case "RefrigerationTemperatureGraph":
                            sReportName = "Refrigeration Temperature Graph";
                            break;
                        case "RefrigerationExceptionReport":
                            sReportName = "Refrigeration Exception Report";
                            break;
                        #endregion
                        #region Unit Reports
                        case "UnitHealthReport":
                            sReportName = "Unit Health Report";
                            break;
                        case "UnitAssetReport":
                            sReportName = "Unit Asset Report";
                            break;
                        case "UnitDataUsageReport":
                            sReportName = "Unit Data Usage Report";
                            break;
                        #endregion
                        default:
                            sReportName = sReportCommand;
                            break;
                    }
                    #endregion
                    #endregion
                }
                else if (iProtocolVersion == 2)
                {
                    #region Protocol v2 Report Request (Added Driver ID field)
                    #region The ID Values [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)][Driver ID (4)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iUserID = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iFleet = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iVehicle = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iTrailerID = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iDriverID = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    #region Read [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                    sReportCommand = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sReportCommand += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read report name."));
                    }
                    sFilename = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sFilename += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read file name."));
                    }
                    #endregion
                    #region Read the From Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateFrom = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the To Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateTo = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the reason IDs - [Num Of Reasons (2)]([ReasonID (4)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// Reason Count
                    if (iItemCount > 0)
                    {
                        for (int X = 0; X < iItemCount; X++)
                        {
                            #region Read the reason ID
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[2] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[3] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// Reason ID
                            #endregion
                            oReasonIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read reasons."));
                    }
                    #endregion
                    #region Read the WP IDs - [Num Of WP IDs (2)]([WP ID (2)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// WP ID Count
                    if (iItemCount > 0)
                    {
                        bConvert = new byte[4];
                        bConvert[0] = bParsed[iPos++];
                        bConvert[1] = bParsed[iPos++];
                        bConvert[2] = bParsed[iPos++];
                        bConvert[3] = bParsed[iPos++];
                        iWPGroupID = BitConverter.ToInt32(bConvert, 0);		// WP Group ID
                        for (int X = 0; X < iItemCount; X++)
                        {
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// WP ID
                            oWPIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read WP IDs."));
                    }
                    #endregion
                    #region Read the variable values
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 1
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable2 = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 2
                    #endregion
                    #region Read the flags
                    if (bParsed[iPos++] == (byte)0x01)
                        bSearchAllHours = true;
                    else
                        bSearchAllHours = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bHeadersOverPages = true;
                    else
                        bHeadersOverPages = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bDailySummaries = true;
                    else
                        bDailySummaries = false;

                    iTemperatureFormat = (int)bParsed[iPos++];
                    iSpeedFormat = (int)bParsed[iPos++];
                    iDistanceFormat = (int)bParsed[iPos++];
                    iVolumeFormat = (int)bParsed[iPos++];
                    #endregion
                    #region Resolve the report name
                    switch (sReportCommand)
                    {
                        #region Driver Reports
                        case "TripReportExtended":
                            sReportName = "Trip Report";
                            break;
                        case "TripReportByVehicleExtended":
                            sReportName = "Trip Report By Vehicle";
                            break;
                        case "DriverPerformanceReport":
                            sReportName = "Performance Report";
                            break;
                        case "StationaryPeriodReport":
                            sReportName = "Stationary Period Report";
                            break;
                        case "ExceptionsReport":
                            if (iVehicle > 0)
                                sReportName = "Vehicle Exception Report";
                            else
                                sReportName = "Fleet Exception Report";
                            break;
                        case "ExceptionsGraphReport":
                            sReportName = "Exception Graph";
                            break;
                        case "NoOverSpeedsGraphReport":
                            sReportName = "OverSpeed Graph";
                            break;
                        case "DistanceAndSpeedVsTime":
                            sReportName = "Speed Graph";
                            break;
                        case "UtilizationReport":
                            sReportName = "Utilization Report";
                            break;
                        case "FatigueManagementDriver":
                            sReportName = "Fatigue Management Driver Report";
                            break;
                        case "FatigueManagement":
                            sReportName = "Fatigue Management By Vehicle";
                            break;
                        #endregion
                        #region Vehicle Reports
                        case "EngineReport":
                            sReportName = "ECM Report";
                            break;
                        case "VehicleDetailedReport":
                            sReportName = "Vehicle Details Report";
                            break;
                        case "AvgFuelGraphReport":
                            sReportName = "Fuel Economy Graph";
                            break;
                        case "LineHaulReport":
                            sReportName = "Line Haul Report";
                            break;
                        case "LocationReport":
                            sReportName = "Location Report";
                            break;
                        case "UsageReportByVehicle":
                            sReportName = "Usage Report By Vehicle";
                            break;
                        case "OutofCityReport":
                            sReportName = "Out of City Report";
                            break;
                        #endregion
                        #region Way Point Reports
                        case "CustomerReport":
                            sReportName = "Way Point Report";
                            break;
                        case "CustomerExceptionsReport":
                            sReportName = "Way Point Exception Report";
                            break;
                        case "DockTimeReport":
                            sReportName = "Dock Time Report";
                            break;
                        case "DockTimeExceptionGraph":
                            sReportName = "Customer Exception Graph";
                            break;
                        #endregion
                        #region Concrete Reports
                        case "ConcreteTripReport":
                            sReportName = "Concrete Trip Report";
                            break;
                        case "ConcreteStatusReport":
                            sReportName = "Concrete Status Report";
                            break;
                        #endregion
                        #region Trailer Reports
                        case "TrailerLocationReport":
                            sReportName = "Trailer Location Report";
                            break;
                        case "TrailerStationaryReport":
                            sReportName = "Trailer Stationary Report";
                            break;
                        case "TrailerEfficiencyReport":
                            sReportName = "Trailer Utilization Report";
                            break;
                        case "TrailerLineHaulReport":
                            sReportName = "Trailer Line Haul Report";
                            break;
                        #endregion
                        #region Refrigeration Report
                        case "RefrigerationDetailedReport":
                            sReportName = "Refrigeration Detailed Report";
                            break;
                        case "RefrigerationTemperatureGraph":
                            sReportName = "Refrigeration Temperature Graph";
                            break;
                        case "RefrigerationExceptionReport":
                            sReportName = "Refrigeration Exception Report";
                            break;
                        #endregion
                        #region Unit Reports
                        case "UnitHealthReport":
                            sReportName = "Unit Health Report";
                            break;
                        case "UnitAssetReport":
                            sReportName = "Unit Asset Report";
                            break;
                        case "UnitDataUsageReport":
                            sReportName = "Unit Data Usage Report";
                            break;
                        #endregion
                        default:
                            sReportName = sReportCommand;
                            break;
                    }
                    #endregion
                    #endregion
                }
                else if (iProtocolVersion == 3)
                {
                    #region Protocol v3 Report Request (Added Time Zone Info fields)
                    bTimeZonesEnabled = true;
                    #region The ID Values [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)][Driver ID (4)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iUserID = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iFleet = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iVehicle = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iTrailerID = BitConverter.ToInt32(bConvert, 0);
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    iDriverID = BitConverter.ToInt32(bConvert, 0);
                    #endregion
                    #region Read [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
                    sReportCommand = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sReportCommand += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read report name."));
                    }
                    sFilename = "";
                    while (iPos < bParsed.Length)
                    {
                        bTemp = bParsed[iPos++];
                        if (bTemp == (byte)0x05)
                            break;
                        else
                            sFilename += Convert.ToString((char)bTemp);
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read file name."));
                    }
                    #endregion
                    #region Read the From Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateFrom = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the To Date - [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iDay = BitConverter.ToInt32(bConvert, 0);		// Day
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMonth = BitConverter.ToInt32(bConvert, 0);	// Month
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iYear = BitConverter.ToInt32(bConvert, 0);		// Year
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iHour = BitConverter.ToInt32(bConvert, 0);		// Hour
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iMinute = BitConverter.ToInt32(bConvert, 0);	// Minute
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    iSecond = BitConverter.ToInt32(bConvert, 0);	// Second
                    dtDateTo = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                    #endregion
                    #region Read the reason IDs - [Num Of Reasons (2)]([ReasonID (4)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// Reason Count
                    if (iItemCount > 0)
                    {
                        for (int X = 0; X < iItemCount; X++)
                        {
                            #region Read the reason ID
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[2] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            bConvert[3] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read reason ID.(" + Convert.ToString(X) + ")."));
                            }
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// Reason ID
                            #endregion
                            oReasonIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read reasons."));
                    }
                    #endregion
                    #region Read the WP IDs - [Num Of WP IDs (2)]([WP ID (2)])
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    iItemCount = BitConverter.ToInt32(bConvert, 0);		// WP ID Count
                    if (iItemCount > 0)
                    {
                        bConvert = new byte[4];
                        bConvert[0] = bParsed[iPos++];
                        bConvert[1] = bParsed[iPos++];
                        bConvert[2] = bParsed[iPos++];
                        bConvert[3] = bParsed[iPos++];
                        iWPGroupID = BitConverter.ToInt32(bConvert, 0);		// WP Group ID
                        for (int X = 0; X < iItemCount; X++)
                        {
                            bConvert = new byte[4];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[0] = bParsed[iPos++];
                            if (iPos >= bParsed.Length)
                            {
                                throw (new System.Exception("Invalid Request, failed to read WP ID (" + Convert.ToString(X) + ")."));
                            }
                            bConvert[1] = bParsed[iPos++];
                            iItemID = BitConverter.ToInt32(bConvert, 0);		// WP ID
                            oWPIDs.Add(iItemID);
                        }
                    }
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read WP IDs."));
                    }
                    #endregion
                    #region Read the variable values
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 1
                    bConvert = new byte[4];
                    bConvert[0] = bParsed[iPos++];
                    bConvert[1] = bParsed[iPos++];
                    bConvert[2] = bParsed[iPos++];
                    bConvert[3] = bParsed[iPos++];
                    dVariable2 = Convert.ToDecimal(BitConverter.ToInt32(bConvert, 0));		// Variable 2
                    #endregion
                    #region Read the flags
                    if (bParsed[iPos++] == (byte)0x01)
                        bSearchAllHours = true;
                    else
                        bSearchAllHours = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bHeadersOverPages = true;
                    else
                        bHeadersOverPages = false;
                    if (bParsed[iPos++] == (byte)0x01)
                        bDailySummaries = true;
                    else
                        bDailySummaries = false;

                    iTemperatureFormat = (int)bParsed[iPos++];
                    iSpeedFormat = (int)bParsed[iPos++];
                    iDistanceFormat = (int)bParsed[iPos++];
                    iVolumeFormat = (int)bParsed[iPos++];
                    #endregion
                    #region Time Zone Info
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read time zone flag. Potition (" + Convert.ToString(iPos) + ")"));
                    }
                    if (bParsed[iPos++] == (byte)0x01)
                        bUseVehicleTimeZone = true;
                    else
                        bUseVehicleTimeZone = false;
                    bConvert = new byte[4];
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read time zone ID. Potition (" + Convert.ToString(iPos) + ")"));
                    }
                    bConvert[0] = bParsed[iPos++];
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read time zone ID. Potition (" + Convert.ToString(iPos) + ")"));
                    }
                    bConvert[1] = bParsed[iPos++];
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read time zone ID. Potition (" + Convert.ToString(iPos) + ")"));
                    }
                    bConvert[2] = bParsed[iPos++];
                    if (iPos >= bParsed.Length)
                    {
                        throw (new System.Exception("Invalid Request, failed to read time zone ID. Potition (" + Convert.ToString(iPos) + ")"));
                    }
                    bConvert[3] = bParsed[iPos++];
                    iTimeZoneID = BitConverter.ToInt32(bConvert, 0);
                    #endregion

                    #region Resolve the report name
                    switch (sReportCommand)
                    {
                        #region Driver Reports
                        case "TripReportExtended":
                            sReportName = "Trip Report";
                            break;
                        case "TripReportByVehicleExtended":
                            sReportName = "Trip Report By Vehicle";
                            break;
                        case "DriverPerformanceReport":
                            sReportName = "Performance Report";
                            break;
                        case "StationaryPeriodReport":
                            sReportName = "Stationary Period Report";
                            break;
                        case "ExceptionsReport":
                            if (iVehicle > 0)
                                sReportName = "Vehicle Exception Report";
                            else
                                sReportName = "Fleet Exception Report";
                            break;
                        case "ExceptionsGraphReport":
                            sReportName = "Exception Graph";
                            break;
                        case "NoOverSpeedsGraphReport":
                            sReportName = "OverSpeed Graph";
                            break;
                        case "DistanceAndSpeedVsTime":
                            sReportName = "Speed Graph";
                            break;
                        case "UtilizationReport":
                            sReportName = "Utilization Report";
                            break;
                        case "FatigueManagementDriver":
                            sReportName = "Fatigue Management Driver Report";
                            break;
                        case "FatigueManagement":
                            sReportName = "Fatigue Management By Vehicle";
                            break;
                        #endregion
                        #region Vehicle Reports
                        case "EngineReport":
                            sReportName = "ECM Report";
                            break;
                        case "VehicleDetailedReport":
                            sReportName = "Vehicle Details Report";
                            break;
                        case "AvgFuelGraphReport":
                            sReportName = "Fuel Economy Graph";
                            break;
                        case "LineHaulReport":
                            sReportName = "Line Haul Report";
                            break;
                        case "LocationReport":
                            sReportName = "Location Report";
                            break;
                        case "UsageReportByVehicle":
                            sReportName = "Usage Report By Vehicle";
                            break;
                        case "OutofCityReport":
                            sReportName = "Out of City Report";
                            break;
                        #endregion
                        #region Way Point Reports
                        case "CustomerReport":
                            sReportName = "Way Point Report";
                            break;
                        case "CustomerExceptionsReport":
                            sReportName = "Way Point Exception Report";
                            break;
                        case "DockTimeReport":
                            sReportName = "Dock Time Report";
                            break;
                        case "DockTimeExceptionGraph":
                            sReportName = "Customer Exception Graph";
                            break;
                        #endregion
                        #region Concrete Reports
                        case "ConcreteTripReport":
                            sReportName = "Concrete Trip Report";
                            break;
                        case "ConcreteStatusReport":
                            sReportName = "Concrete Status Report";
                            break;
                        #endregion
                        #region Trailer Reports
                        case "TrailerLocationReport":
                            sReportName = "Trailer Location Report";
                            break;
                        case "TrailerStationaryReport":
                            sReportName = "Trailer Stationary Report";
                            break;
                        case "TrailerEfficiencyReport":
                            sReportName = "Trailer Utilization Report";
                            break;
                        case "TrailerLineHaulReport":
                            sReportName = "Trailer Line Haul Report";
                            break;
                        #endregion
                        #region Refrigeration Report
                        case "RefrigerationDetailedReport":
                            sReportName = "Refrigeration Detailed Report";
                            break;
                        case "RefrigerationTemperatureGraph":
                            sReportName = "Refrigeration Temperature Graph";
                            break;
                        case "RefrigerationExceptionReport":
                            sReportName = "Refrigeration Exception Report";
                            break;
                        #endregion
                        #region Unit Reports
                        case "UnitHealthReport":
                            sReportName = "Unit Health Report";
                            break;
                        case "UnitAssetReport":
                            sReportName = "Unit Asset Report";
                            break;
                        case "UnitDataUsageReport":
                            sReportName = "Unit Data Usage Report";
                            break;
                        #endregion
                        default:
                            sReportName = sReportCommand;
                            break;
                    }
                    #endregion
                    #endregion
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return bData;
        }

        #region Create v3 request backup
        //			// Version 
        //			// [SOP (1)][Length (2)]
        //			// [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
        //			// [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
        //			// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
        //			// [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
        //			// [Num Of Reasons (2)]([ReasonID (2)])
        //			// [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
        //			// [Variable 1 (4)][Variable 2 (4)]
        //			// [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)]
        //			// [EOP (1)]
        //
        //			#region Get the fleet and unit IDs - bIDs = [UserID (4)][FleetID (1)][VehicleID (4)][TrailerID (4)]
        //			bIDs = BitConverter.GetBytes(iUserID);											// User ID (4 Bytes)
        //			bConvert = BitConverter.GetBytes(iFleet);
        //			bIDs = PacketUtilities.AppendToPacket(bIDs, bConvert[0]);			// Fleet ID (1 Byte)
        //			bIDs = PacketUtilities.AppendToPacket(bIDs, iVehicle);					// VehicleID (4 Bytes)
        //			bIDs = PacketUtilities.AppendToPacket(bIDs, iTrailerID);				// Trailer ID (4 Bytes)
        //			bIDs = PacketUtilities.AppendToPacket(bIDs, iDriverID);				// Driver ID (4 Bytes)
        //			#endregion
        //			#region Get the Report Name - bReportName = [ReportName (X)][End Of Report Name (0x05) (1)][Filename (X)][End of Filename (0x05) (1)]
        //			bReportName = System.Text.ASCIIEncoding.ASCII.GetBytes(sReportCommand);
        //			bReportName = PacketUtilities.AppendToPacket(bReportName, (byte) 0x05);
        //			bReportName = PacketUtilities.AppendToPacket(bReportName, System.Text.ASCIIEncoding.ASCII.GetBytes(sFilename));
        //			bReportName = PacketUtilities.AppendToPacket(bReportName, (byte) 0x05);
        //			#endregion
        //			#region Get the to and from dates - bFromDate and bToDate - Format = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
        //			#region Get the from date - bFromDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
        //			bConvert= BitConverter.GetBytes(dtDateFrom.Day);
        //			bFromDate= new byte[1];
        //			bFromDate[0] = bConvert[0];																			// Day (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateFrom.Month);
        //			bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Month (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateFrom.Year);
        //			bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Year (2 Bytes)
        //			bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[1]);
        //			bConvert = BitConverter.GetBytes(dtDateFrom.Hour);
        //			bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Hour (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateFrom.Minute);
        //			bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Minute (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateFrom.Second);
        //			bFromDate = PacketUtilities.AppendToPacket(bFromDate, bConvert[0]);	// Second (1 Byte)
        //			#endregion
        //			#region Get the to date - bToDate = [Day (1)][Month (1)][Year (2)][Hour (1)][Minute (1)][Second (1)]
        //			bConvert= BitConverter.GetBytes(dtDateTo.Day);
        //			bToDate= new byte[1];
        //			bToDate[0] = bConvert[0];																				// Day (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateTo.Month);
        //			bToDate= PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Month (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateTo.Year);
        //			bToDate= PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Year (2 Bytes)
        //			bToDate= PacketUtilities.AppendToPacket(bToDate, bConvert[1]);
        //			bConvert = BitConverter.GetBytes(dtDateTo.Hour);
        //			bToDate= PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Hour (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateTo.Minute);
        //			bToDate= PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Minute (1 Byte)
        //			bConvert = BitConverter.GetBytes(dtDateTo.Second);
        //			bToDate= PacketUtilities.AppendToPacket(bToDate, bConvert[0]);			// Second (1 Byte)
        //			#endregion
        //			#endregion
        //			#region Get the Reason IDs -  bReasonIDs = [Num Of Reasons (2)]([ReasonID (4)])
        //			bReasonIDs = new byte[2];
        //			bReasonIDs[0] = (byte) 0x00;
        //			bReasonIDs[1] = (byte) 0x00;
        //			if(oReasonIDs.Count > 0)
        //			{
        //				bConvert = BitConverter.GetBytes(oReasonIDs.Count);
        //				bReasonIDs[0] = bConvert[0]; 
        //				bReasonIDs[1] = bConvert[1];
        //				for(int iReason = 0; iReason < oReasonIDs.Count; iReason++)
        //				{
        //					bConvert = BitConverter.GetBytes((int) oReasonIDs[iReason]);
        //					bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[0]);	// Reason ID (4 Bytes)
        //					bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[1]);	
        //					bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[2]);
        //					bReasonIDs = PacketUtilities.AppendToPacket(bReasonIDs, bConvert[3]);
        //				}
        //			}
        //			#endregion
        //			#region Get the  WP IDs - bWPIDs = [Num Of WP IDs (2)][WP Group ID (4)]([WP ID (2)])
        //			bWPIDs = new byte[2];
        //			bWPIDs[0] = (byte) 0x00;
        //			bWPIDs[1] = (byte) 0x00;
        //			if(oWPIDs.Count > 0)
        //			{
        //				bConvert = BitConverter.GetBytes(oWPIDs.Count);
        //				bWPIDs[0] = bConvert[0];																			// Num of WP IDs
        //				bWPIDs[1] = bConvert[1];
        //				bConvert = BitConverter.GetBytes(iWPGroupID);										// WP Group ID
        //				bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert);
        //				for(int iWP = 0; iWP < oWPIDs.Count; iWP++)
        //				{
        //					bConvert = BitConverter.GetBytes((int) oWPIDs[iWP]);
        //					bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[0]);	// WP ID (2 Bytes)
        //					bWPIDs = PacketUtilities.AppendToPacket(bWPIDs, bConvert[1]);	
        //				}
        //			}
        //			#endregion
        //			#region Get the variable values - bVariables = [Variable 1 (4)][Variable 2 (4)]
        //			bVariables = new byte[8];
        //			bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable));
        //			bVariables[0] = bConvert[0];
        //			bVariables[1] = bConvert[1];
        //			bVariables[2] = bConvert[2];
        //			bVariables[3] = bConvert[3];
        //			bConvert = BitConverter.GetBytes(Convert.ToInt32(dVariable2));
        //			bVariables[4] = bConvert[0];
        //			bVariables[5] = bConvert[1];
        //			bVariables[6] = bConvert[2];
        //			bVariables[7] = bConvert[3];
        //			#endregion
        //			#region Get the report flags - bReportFlags = [SearchAllHours (1)][HeadersOverPages (1)][DailySummaries (1)]
        //			bReportFlags = new byte[7];
        //			if(bSearchAllHours)
        //				bReportFlags[0] = (byte) 0x01;
        //			if(bHeadersOverPages)
        //				bReportFlags[1] = (byte) 0x01;
        //			if(bDailySummaries)
        //				bReportFlags[2] = (byte) 0x01;
        //			if(iTemperatureFormat == 1)
        //				bReportFlags[3] = (byte) 0x01;				
        //			if(iSpeedFormat == 1)
        //				bReportFlags[4] = (byte) 0x01;
        //			if(iDistanceFormat == 1)
        //				bReportFlags[5] = (byte) 0x01;
        //			if(iVolumeFormat == 1)
        //				bReportFlags[6] = (byte) 0x01;
        //			#endregion
        //			#region Get the time zone selection info - bTimeZone = [UseVehicleTimeZone (1)][TimeZoneID (4)]
        //			bTimeZone = new byte[5];
        //			if(bUseVehicleTimeZone)
        //				bTimeZone[0] = (byte) 0x01;
        //			else
        //				bTimeZone[0] = (byte) 0x00;
        //			bConvert = BitConverter.GetBytes(Convert.ToInt32(iTimeZone));
        //			bTimeZone[1] = bConvert[0];
        //			bTimeZone[2] = bConvert[1];
        //			bTimeZone[3] = bConvert[2];
        //			bTimeZone[4] = bConvert[3];
        //			#endregion
        #endregion
    }
}

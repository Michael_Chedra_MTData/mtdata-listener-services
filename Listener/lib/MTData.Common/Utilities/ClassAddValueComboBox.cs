using System;

namespace MTData.Common.Utilities
{
	/// <summary>
	/// Summary description for ClassAddValueComboBox.
	/// </summary>
	public class ClassAddValueComboBox
	{
		public ClassAddValueComboBox()
		{

		}

		private  int _ID;
		public int ID
		{
			get
			{
				return _ID;
			}
		}

		private  System.Int64 _IDs;
		public System.Int64 IDs
		{
			get
			{
				return _IDs;
			}
		}

		private string _Name;
		public string Name
		{
			get
			{
				return _Name;
			}
		}

		public ClassAddValueComboBox(int iID, string sName) 
		{
			_ID = iID;
			_Name = sName;    
		}

		public ClassAddValueComboBox(System.Int64 iID, string sName) 
		{
			_IDs = iID;
			_Name = sName;    
		}
	}
}

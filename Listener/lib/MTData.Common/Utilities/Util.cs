using System;
using System.Text.RegularExpressions;
using System.IO;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
//using System.Windows.Forms;
using System.Data;
using System.Drawing;
using System.Text;
using System.Security.Cryptography;
using System.Xml;
using ICSharpCode.SharpZipLib.Zip;
using ICSharpCode.SharpZipLib.GZip;
using ICSharpCode.SharpZipLib.Tar;
using log4net;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Summary description for Utilities.
    /// </summary>
    public static partial class Util
    {

        private static ILog _log = LogManager.GetLogger(typeof(Util));
        #region GetLocalTimeDifference
        public static string GetLocalTimeDifference(string sError)
        {
            int iRet = 0;
            try
            {
                System.DateTime oTime = System.DateTime.Now;
                System.DateTime oGMTTime = oTime.ToUniversalTime();
                System.TimeSpan oSpan = oTime.Subtract(oGMTTime);
                iRet = oSpan.Hours;
            }
            catch (System.Exception ex)
            {
                iRet = 0;
                sError += "Error : " + ex.Message + "\r\nSource : " + ex.Source + "\r\nTrace : " + ex.StackTrace;
            }
            return Convert.ToString(iRet);
        }
        #endregion

        public static string EncodeByteArrayForLogging(byte[] bytes)
        {
            if (bytes == null)
                return "<NULL>";
            else
                return EncodeByteArrayForLogging(bytes, bytes.Length);
        }
        public static string EncodeByteArrayForLogging(byte[] bytes, int packetLength)
        {
            StringBuilder builder = new StringBuilder();

            for (int loop = 0; loop < packetLength; loop++)
            {
                builder.Append("[");
                int representation = Convert.ToInt16(bytes[loop]);
                if (representation < 100)
                    builder.Append(" ");
                if (representation < 10)
                    builder.Append(" ");
                builder.Append(representation);
                builder.Append("]");
            }
            return builder.ToString();
        }
        static public string GetMd5Sum(string str)
        {
            // First we need to convert the string into bytes, which
            // means using a text encoder.
            Encoder enc = System.Text.Encoding.Unicode.GetEncoder();

            // Create a buffer large enough to hold the string
            byte[] unicodeText = new byte[str.Length * 2];
            enc.GetBytes(str.ToCharArray(), 0, str.Length, unicodeText, 0, true);

            // Now that we have a byte array we can ask the CSP to hash it
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] result = md5.ComputeHash(unicodeText);

            // Build the final string by converting each byte
            // into hex and appending it to a StringBuilder
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < result.Length; i++)
            {
                sb.Append(result[i].ToString("X2"));
            }

            // And return it
            return sb.ToString();
        }

        public static double GetMilesFromMiles(double dMi)
        {
            string s = Convert.ToString(dMi);
            char seps = '.';
            string[] sArr = s.Split(seps);
            return Convert.ToDouble(sArr[0]);
        }
        public static double GetMilesFractionFromMiles(double dMi)
        {
            dMi = System.Math.Round(dMi, 2);
            string s = Convert.ToString(dMi);
            char seps = '.';
            if (s == "0")
                s = "0.00";
            string[] sArr = s.Split(seps);
            if (sArr[1].Length == 1)
                sArr[1] = sArr[1] + "0";
            return Convert.ToDouble(sArr[1]);
        }
        public static int GetKilometersFromKilometerMeter(int iMet)
        {
            return Convert.ToInt32(System.Math.Ceiling(Convert.ToDouble(iMet) / Convert.ToDouble(1000)));
        }
        public static int GetMetersFromKilometerMeter(int iMet)
        {
            //return Convert.ToInt32(iMet - System.Math.Ceiling(Convert.ToDouble(iMet) / Convert.ToDouble(1000)) * 1000);
            return iMet % 1000;
        }
        public static int GetMinutesFromMinuteSecond(int iSec)
        {
            return Convert.ToInt32(System.Math.Ceiling(Convert.ToDouble(iSec) / Convert.ToDouble(60)));
        }
        public static int GetSecondsFromMinuteSecond(int iSec)
        {
            return Convert.ToInt32(iSec - (System.Math.Ceiling(Convert.ToDouble(iSec) / Convert.ToDouble(60)) * 60));
        }
        public static string BinLatLongToDegMin(decimal bin)
        {
            // Expecting a number like:
            //-37.49863
            // Must be converted as follows:
            // => -37 degrees
            // .49863 * 60 = 29.9178 
            // => 29.9178 minutes


            int degrees = (int)Decimal.Truncate(bin);
            decimal minssecs = bin - degrees;

            minssecs = Math.Abs(minssecs * 60);
            decimal minutes = Decimal.Round(minssecs, 5);

            string sign = "";
            if (bin < 0 && degrees == 0)
                sign = "-";
            return sign + degrees + "�" + minutes.ToString("00.00000") + "'";
        }
        public static decimal DegMinToBinLatLong(string degmin)
        {
            decimal retVal = 0;
            // We are expecting a string of the form:
            // (-)ddd�mm.sss
            // The only thing is the user may not have entered the degrees sign.
            // We will also accept an extra dot or a d:
            // i.e. -37.936.344 or -37d346.345 etc	
            try
            {
                char[] seps = { '.', 'd' };
                string[] sections = degmin.Split(seps);
                switch (sections.Length)
                {
                    case 0:
                        retVal = 0;
                        break;
                    case 1:
                        retVal = Convert.ToDecimal(degmin);
                        break;
                    case 2:
                        // i.e. 37.05 - that's OK
                        // take the 05 and divide it by 60:
                        retVal = (Convert.ToDecimal(sections[1]) / 60);
                        retVal += (Convert.ToDecimal(sections[0]));
                        break;
                    case 3:
                        // i.e. 37d05.756 - that's OK too
                        retVal = (Convert.ToDecimal(sections[1] + sections[2]) / 60);
                        retVal += (Convert.ToDecimal(sections[0]));
                        break;
                }
            }
            catch (Exception)
            {

                retVal = 0;
            }
            return retVal;
        }
        public static double DegMinToBinLatLongStjepan(string degmin)
        {
            double retVal = 0;
            // We are expecting a string of the form:
            // (-)ddd�mm.sss
            // The only thing is the user may not have entered the degrees sign.
            // We will also accept an extra dot or a d:
            // i.e. -37.936.344 or -37d346.345 etc	
            try
            {
                char[] seps = { '.', '�', '\'', 'd' };
                string[] sections = degmin.Split(seps);
                switch (sections.Length)
                {
                    case 0:
                        retVal = 0;
                        break;
                    case 1:
                        retVal = Convert.ToDouble(degmin);
                        break;
                    case 2:
                        // i.e. 37.05 - that's OK
                        // take the 05 and divide it by 60:
                        retVal = (Convert.ToDouble(sections[1]) / 60);
                        retVal += (Convert.ToDouble(sections[0]));
                        break;
                    case 3:
                        // i.e. 37d05.756 - that's OK too						
                        retVal = (Convert.ToDouble(sections[1] + sections[2]) / 60);
                        retVal += (Convert.ToDouble(sections[0]));
                        break;
                    case 4:
                        if (sections[0].StartsWith("-"))
                        {
                            retVal = (Convert.ToDouble(sections[1] + "." + sections[2]) / 60);
                            retVal -= (Convert.ToDouble(sections[0]));
                            retVal *= -1;
                        }
                        else
                        {
                            retVal = (Convert.ToDouble(sections[1] + "." + sections[2]) / 60);
                            retVal += (Convert.ToDouble(sections[0]));
                        }
                        break;
                }
            }
            catch (Exception)
            {

                retVal = 0;
            }
            return retVal;
        }
        public static decimal DegMinToBinLatLongEx(string degmin)
        {
            decimal retVal = 0;
            // We are expecting a string of the form:
            // (-)ddd�mm.sss
            // The only thing is the user may not have entered the degrees sign.
            // We will also accept an extra dot or a d:
            // i.e. -37.936.344 or -37d346.345 etc	
            try
            {
                //char[] seps = {'.', 'd', (char)176};
                string[] sections = degmin.TrimEnd('\'').Split('.', 'd', (char)176);
                switch (sections.Length)
                {
                    case 0:
                        retVal = 0;
                        break;
                    case 1:
                        if (Convert.ToDecimal(degmin) > 0)
                            retVal = Convert.ToDecimal(degmin);
                        else
                        {
                            retVal -= (Convert.ToDecimal(degmin));
                            retVal = -retVal;
                        }
                        break;
                    case 2:
                        // i.e. 37.05 - that's OK
                        // take the 05 and divide it by 60:
                        retVal = (Convert.ToDecimal(sections[1]) / 60);
                        if (Convert.ToDecimal(sections[0]) > 0)
                            retVal += (Convert.ToDecimal(sections[0]));
                        else
                        {
                            retVal -= (Convert.ToDecimal(sections[0]));
                            retVal = -retVal;
                        }
                        break;
                    case 3:
                        // i.e. 37d05.756 - that's OK too
                        retVal = (Convert.ToDecimal(sections[1] + "." + sections[2]) / 60);
                        if (Convert.ToDecimal(sections[0]) > 0)
                            retVal += (Convert.ToDecimal(sections[0]));
                        else
                        {
                            retVal -= (Convert.ToDecimal(sections[0]));
                            retVal = -retVal;
                        }
                        break;
                }
            }
            catch (Exception)
            {
                retVal = 0;
            }
            return retVal;
        }


        /// <summary>
        /// This holds a cache of the descriptive headings.
        /// </summary>
        private static string[] _degreeHeadingCache = new string[] { "N", "NE", "E", "SE", "S", "SW", "W", "NW", "N" };
        /// <summary>
        /// This method provides a descriptive indication of the direction of travel.
        /// </summary>
        /// <param name="iDeg"></param>
        /// <returns></returns>
        public static string GetDescriptiveHeadingForDegree(int iDeg)
        {
            if ((iDeg < 0) || (iDeg >= 360))
                return "N/A";
            else
            {
                int degreeOffset = Convert.ToInt32(Convert.ToInt32(((iDeg + 22) / 45)));
                return _degreeHeadingCache[degreeOffset];
            }
        }

        public static string GetDescriptiveHeadingForDegree(double dCourse)
        {
            string sRet = "";

            if (dCourse <= 22.5)
                sRet = "N";
            if (dCourse > 22.5 && dCourse <= 67.5)
                sRet = "NE";
            if (dCourse > 67.5 && dCourse <= 112.5)
                sRet = "E";
            if (dCourse > 112.5 && dCourse <= 157.5)
                sRet = "SE";
            if (dCourse > 157.5 && dCourse <= 202.5)
                sRet = "S";
            if (dCourse > 202.5 && dCourse <= 247.5)
                sRet = "SW";
            if (dCourse > 247.5 && dCourse <= 292.5)
                sRet = "W";
            if (dCourse > 292.5 && dCourse <= 337.5)
                sRet = "NW";
            if (dCourse > 337.5)
                sRet = "N";

            return sRet;
        }

        public static string GetDirectionFromUnitToPoint(double dUnitLat, double dUnitLong, double dPositionLat, double dPositionLong)
        {
            double dDegToRad = Math.PI / 180;
            double dRadToDeg = 180.0 / Math.PI;

            return GetDirectionFromUnitToPoint(dUnitLat, dUnitLong, dPositionLat, dPositionLong, dDegToRad, dRadToDeg);
        }

        public static string GetDirectionFromUnitToPoint(double dUnitLat, double dUnitLong, double dPositionLat, double dPositionLong, double dDegToRad, double dRadToDeg)
        {
            double dCourse = 0;
            double dLat = 0;
            double dLon = 0;
            double dLat1 = 0;
            double dLon1 = 0;
            double dLat2 = 0;
            double dLon2 = 0;
            double distanceNorth = 0;
            double distanceEast = 0;
            string sRet = "";

            dLat1 = DegreesToRadians(dUnitLat, dDegToRad);
            dLon1 = DegreesToRadians(dUnitLong, dDegToRad);
            dLat2 = DegreesToRadians(dPositionLat, dDegToRad);
            dLon2 = DegreesToRadians(dPositionLong, dDegToRad);
            dLat = dLat2 - dLat1;
            dLon = dLon2 - dLon1;
            distanceNorth = dLat;
            distanceEast = dLon * Math.Cos(dLat1);
            dCourse = Math.Atan2(distanceEast, distanceNorth) % (2 * Math.PI);
            dCourse = RadiansToDegrees(dCourse, dRadToDeg);
            sRet = GetDescriptiveHeadingForDegree(dCourse);

            return sRet;
        }

        public static double DegreesToRadians(double degrees)
        {
            return DegreesToRadians(degrees, Math.PI / 180.0);
        }
        public static double DegreesToRadians(double degrees, double dDegToRad)
        {
            return degrees * dDegToRad;
        }
        public static double RadiansToDegrees(double radians)
        {
            return RadiansToDegrees(radians, 180.0 / Math.PI);
        }
        public static double RadiansToDegrees(double radians, double dRadToDeg)
        {
            return radians * dRadToDeg;
        }
        public static double RadiansToNauticalMiles(double radians)
        {
            return RadiansToNauticalMiles(radians, 180 / Math.PI);
        }
        public static double RadiansToNauticalMiles(double radians, double dRadToDeg)
        {
            // There are 60 nautical miles for each degree
            return radians * 60 * dRadToDeg;
        }
        public static double RadiansToMeters(double radians)
        {
            // there are 1852 meters in a nautical mile
            return 1852 * RadiansToNauticalMiles(radians, 180 / Math.PI);
        }
        public static double RadiansToMeters(double radians, double dRadToDeg)
        {
            // there are 1852 meters in a nautical mile
            return 1852 * RadiansToNauticalMiles(radians, dRadToDeg);
        }

        public static double CalculateHorizontalDistance(double dLonA, double dLonB, double dLatB)
        {
            return CalculateHorizontalDistance(dLonA, dLonB, dLatB, (180.0 / Math.PI));
        }
        public static double CalculateHorizontalDistance(double dLonA, double dLonB, double dLatB, double dRadToDeg)
        {
            return 1.61 * 69.1 * Math.Abs((Math.Abs(dLonA) - Math.Abs(dLonB))) * Math.Cos(Math.Abs(dLatB) / dRadToDeg);
        }
        public static double CalculateVerticalDistance(double dLatA, double dLatB)
        {
            return 1.61 * 69.1 * Math.Abs((Math.Abs(dLatA) - Math.Abs(dLatB)));
        }
        public static double CalculateDistance(double dLatA, double dLonA, double dLatB, double dLonB)
        {
            return CalculateDistance(dLatA, dLonA, dLatB, dLonB, 180.0 / Math.PI);
        }
        public static double CalculateDistance(double dLatA, double dLonA, double dLatB, double dLonB, double dRadToDeg)
        {
            double fx = 69.1 * Math.Abs((Math.Abs(dLatA) - Math.Abs(dLatB)));
            double fy = 69.1 * Math.Abs((Math.Abs(dLonA) - Math.Abs(dLonB))) * Math.Cos(Math.Abs(dLatB) / dRadToDeg);
            return 1.61 * Math.Sqrt(fx * fx + fy * fy);
        }
        public static double CalculateDistanceSquared(double dLatA, double dLonA, double dLatB, double dLonB)
        {
            return CalculateDistanceSquared(dLatA, dLonA, dLatB, dLonB, Math.PI / 180);
        }
        public static double CalculateDistanceSquared(double dLatA, double dLonA, double dLatB, double dLonB, double dDegToRad)
        {
            // This function is useful for comparing relative distances, without the over head of determining the sqrt value.
            return Math.Pow(dLatA - dLatB, 2) + Math.Pow((dLonA - dLonB) * Math.Cos(dLatB * dDegToRad), 2);
        }

        // Convert the database native units (nautical miles) to
        // local units (km) and round off:
        private const int ROUNDING_LEVEL = 0;	// i.e. nearest 100 metres
        public static string ConvertToKilometreString(string sNauticalMiles)
        {
            if ((sNauticalMiles != null) && !(sNauticalMiles.Equals("")))
            {
                float fNauticalMiles = Convert.ToSingle(sNauticalMiles);
                return (Decimal.Round((Decimal)(((float)1.852) * fNauticalMiles),
                    ROUNDING_LEVEL)).ToString();
            }
            return "";
        }
        public static string ConvertFromAustralianToAmericanDate(string sDate)
        {
            System.IFormatProvider format = new System.Globalization.CultureInfo("en-AU");
            System.DateTime myDateTime = System.DateTime.Parse(sDate, format);
            return myDateTime.Month.ToString() + "/" + myDateTime.Day.ToString() + "/" + myDateTime.Year.ToString();
        }
        public static string ConvertFromAustralianToAmericanDateTime(string sDateTime)
        {
            System.IFormatProvider format = new System.Globalization.CultureInfo("en-AU");
            System.DateTime myDateTime = System.DateTime.Parse(sDateTime, format);
            return myDateTime.Month.ToString() + "/" + myDateTime.Day.ToString() + "/" + myDateTime.Year.ToString() + " " + myDateTime.Hour.ToString() + ":" + myDateTime.Minute.ToString() + ":" + myDateTime.Second.ToString();
        }
        public static string ConvertFromAmericanToAustralianDate(string sDate)
        {
            System.IFormatProvider format = new System.Globalization.CultureInfo("en-US");
            System.DateTime myDateTime = System.DateTime.Parse(sDate, format);
            return myDateTime.Day.ToString() + "/" + myDateTime.Month.ToString() + "/" + myDateTime.Year.ToString();

        }
        public static string ConvertFromAmericanToAustralianDateTime(string sDateTime)
        {
            System.IFormatProvider format = new System.Globalization.CultureInfo("en-US");
            System.DateTime myDateTime = System.DateTime.Parse(sDateTime, format);
            return myDateTime.Day.ToString() + "/" + myDateTime.Month.ToString() + "/" + myDateTime.Year.ToString() + " " + myDateTime.Hour.ToString() + ":" + myDateTime.Minute.ToString() + ":" + myDateTime.Second.ToString();

        }
        public static double ConfigScreenFromNMtoKPH(double dNauticalMiles)
        {
            return dNauticalMiles * 1.852;
        }
        public static double ConfigScreenFromNMtoMPH(double dNauticalMiles)
        {

            return dNauticalMiles * 1.15078;
        }
        public static double ConfigScreenFromKPHtoNM(double dKPH)
        {
            return dKPH * 0.53996;
        }
        public static double ConfigScreenFromMPHtoNM(double dMPH)
        {

            return dMPH * 0.86895;
        }
        public static double ConfigScreenFromMItoKM(double MI)
        {
            return MI * 1.60930;
        }
        public static double ConfigScreenFromKMtoMI(double KM)
        {
            return KM * 0.62140;
        }
        public static double FromMPHtoKPH(double mph)
        {
            return mph * 1.60930;
        }
        public static double FromKPHtoMPH(double kph)
        {
            return kph * 0.62140;
        }
        public static int FromNMtoKPH(string sNauticalMiles)
        {
            if (sNauticalMiles != "")
                //return RoundToMultipleOfFive(Convert.ToInt32(sNauticalMiles) * 1.852);
                return Convert.ToInt32(Convert.ToDouble(sNauticalMiles) * 1.852);
            else
                return 0;
        }
        public static int FromNMtoMPH(string sNauticalMiles)
        {
            if (sNauticalMiles != "")
                return Convert.ToInt32(Convert.ToDouble(sNauticalMiles) * 1.15078);
            else
                return 0;
        }
        public static int FromKPHtoNM(decimal iKilometresPerHour)
        {
            return Convert.ToInt32(System.Math.Ceiling((double)iKilometresPerHour / 1.852));
        }
        public static bool IsInteger(string strNumber)
        {
            try
            {
                int i = Convert.ToInt32(strNumber);
                return true;
            }
            catch (FormatException)
            {
                return false;
            }
        }
        public static bool IsNumber(String strNumber)
        {
            Regex objNotNumberPattern = new Regex("[^0-9.-]");
            Regex objTwoDotPattern = new Regex("[0-9]*[.][0-9]*[.][0-9]*");
            Regex objTwoMinusPattern = new Regex("[0-9]*[-][0-9]*[-][0-9]*");
            String strValidRealPattern = "^([-]|[.]|[-.]|[0-9])[0-9]*[.]*[0-9]+$";
            String strValidIntegerPattern = "^([-]|[0-9])[0-9]*$";
            Regex objNumberPattern = new Regex("(" + strValidRealPattern + ")|(" + strValidIntegerPattern + ")");

            return !objNotNumberPattern.IsMatch(strNumber) &&
                !objTwoDotPattern.IsMatch(strNumber) &&
                !objTwoMinusPattern.IsMatch(strNumber) &&
                objNumberPattern.IsMatch(strNumber);
        }
        public static bool IsValidDate(int year, int month, int day, int hour, int minute, int second)
        {
            if (year < 1950 || year > 2100)
                return false;

            if (month < 1 || month > 12)
                return false;


            if (day < 1)
                return false;

            if (month == 2)
            {
                if (DateTime.IsLeapYear(year))
                {
                    if (day > 29)
                        return false;
                }
                else
                {
                    if (day > 29)
                        return false;
                }
            }

            if (month == 2 || month == 4 || month == 6 || month == 9 || month == 11)
            {
                if (day > 30)
                    return false;
            }
            else
            {
                if (day > 31)
                    return false;
            }


            if (hour < 0 || hour > 23)
                return false;

            if (minute < 0 || minute > 59)
                return false;

            if (second < 0 || second > 59)
                return false;

            return true;
        }
        public static bool IsDate(object inValue)
        {
            bool bValid;
            try
            {
                DateTime myDT = DateTime.Parse(Convert.ToString(inValue));
                bValid = true;
            }
            catch
            {
                bValid = false;
            }
            return bValid;
        }
        public static void DeletePointFile()
        {
            string sFileName = "pointfile.txt";
            FileInfo fi = new FileInfo(@sFileName);
            fi.Delete();
        }
        public static void WriteToAnyFile(string sFileName, string sLine)
        {
            // if file doesn't exist then create log file and put line in it.

            FileStream fs = new FileStream(@sFileName, FileMode.Append, FileAccess.Write, FileShare.Read);
            StreamWriter sw = new StreamWriter(fs);

            string sLineToFile = sLine;
            sw.WriteLine(sLineToFile);
            sw.Close();
            fs.Close();
        }
        public static void WriteToPointFile(string sLine)
        {
            // if file doesn't exist then create log file and put line in it.
            string sFileName = "pointfile.txt";
            FileStream fs = new FileStream(@sFileName, FileMode.Append, FileAccess.Write, FileShare.Read);
            StreamWriter sw = new StreamWriter(fs);

            string sLineToFile = sLine;
            sw.WriteLine(sLineToFile);
            sw.Close();
            fs.Close();
        }

        public static Int16 GetDayFromDayOfWeek(string dayOfWeek)
        {
            var mp = new Dictionary<string, int>
            {
                { "Sunday", 0 },
                { "Monday", 1 },
                { "Tuesday", 2 },
                { "Wednesday", 3 },
                { "Thursday", 4 },
                { "Friday", 5 },
                { "Saturday", 6 }
            };

            int returnValue = -1;
            mp.TryGetValue(dayOfWeek, out returnValue);
            return (short)returnValue;
        }


        public static bool FileExist(string sFullPathName)
        {
            bool bRet = false;
            FileInfo fi = new FileInfo(sFullPathName);
            if (fi.Exists)
            {
                bRet = true;
            }
            return bRet;
        }
        public static string GetLocalHostName()
        {
            return System.Net.Dns.GetHostName();
        }
        public static string GetLocalIPAddress()
        {
            // Getting Ip address of local machine.
            // First get the host name of local machine.
            string strHostName = System.Net.Dns.GetHostName();

            // Using host name, get the IP address list..
            IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
            IPAddress[] addr = ipEntry.AddressList;

            return addr[0].ToString();
        }
        public static string CutStringFromEnd(string sInputString, int iFromPositionToEnd)
        {
            string sRet = sInputString;
            if (sInputString.Length > iFromPositionToEnd)
                sRet = sInputString.Substring(0, iFromPositionToEnd) + ".";
            return sRet;
        }
        public static bool CheckStartingString(string sSource, string sCompareString)
        {
            bool bRet = false;

            sSource = sSource.ToUpper();
            sCompareString = sCompareString.ToUpper();

            char[] seps = { ',' };
            string[] sections = sCompareString.Split(seps);
            for (int i = 0; i < sections.Length; i++)
            {
                if (sSource.StartsWith(sections[i].Trim()))
                {
                    bRet = true;
                    break;
                }
            }
            return bRet;
        }
        /*
                public static string GetStreetNameFromLatLong(double dLatitude, double dLongitude)
                {
                    MapViewRoadMapControl mvrmp = new MapViewRoadMapControl();
                    return mvrmp.GetStreetNameFromLatLong(dLatitude, dLongitude);
                }

		
                public static string GetStreetNameFromLatLongMapPoint(double dLatitude, double dLongitude)
                {
                    MapViewMapPointControl mvmp = new MapViewMapPointControl();
                    string sRet = mvmp.GetStreetNameFromLatLong(dLatitude, dLongitude);
                    mvmp.SetSaveOnClose();
                    return sRet;
                }
                */
        public static int RoundToMultipleOfFive(double dNum)
        {
            double dNumLower = Math.Floor(dNum / 5) * 5;
            double dNumUpper = Math.Ceiling(dNum / 5) * 5;
            if ((dNum - dNumLower) < (dNumUpper - dNum))
                return Convert.ToInt32(dNumLower);
            else
                return Convert.ToInt32(dNumUpper);
        }
        public static void CreateFile(string sFileName)
        {
            try
            {
                StreamWriter SW;
                SW = File.CreateText(sFileName);
                SW.WriteLine("");
                SW.Close();
            }
            catch (Exception ex)
            {
                _log.Error("Util.cs - CreateFile. Can't create file " + sFileName + "\n" + ex.Message);
            }
        }
        #region XML Conversion
        public static DataSet XMLToDataSet(string sXMLData)
        {
            StringReader oXMLStream = null;
            XmlTextReader oXMLReader = null;
            DataSet dsRet = null;
            try
            {
                dsRet = new DataSet();
                oXMLStream = new StringReader(sXMLData);
                oXMLReader = new XmlTextReader(oXMLStream);
                dsRet.ReadXml(oXMLReader);
                if (oXMLStream != null)
                    oXMLStream.Close();
                oXMLStream = null;
                if (oXMLReader != null)
                    oXMLReader.Close();
                oXMLReader = null;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("DataSet XMLToDataSet(string sXMLData = '" + sXMLData + "') - " + ex.Message);
                dsRet = null;
            }
            return dsRet;
        }
        public static string DataSetToXML(DataSet dsData)
        {
            System.IO.MemoryStream oXMLStream = null;
            System.Xml.XmlTextWriter oXMLWriter = null;
            System.Text.UnicodeEncoding utf = null;
            byte[] bData = null;
            int iStreamLength = 0;
            string sRet = "";

            try
            {
                oXMLStream = new System.IO.MemoryStream();
                oXMLWriter = new XmlTextWriter(oXMLStream, System.Text.Encoding.Unicode);
                dsData.WriteXml(oXMLWriter);
                iStreamLength = Convert.ToInt32(oXMLStream.Length);
                bData = new byte[iStreamLength];
                oXMLStream.Seek(0, System.IO.SeekOrigin.Begin);
                oXMLStream.Read(bData, 0, iStreamLength);
                utf = new System.Text.UnicodeEncoding();
                sRet = utf.GetString(bData).Trim();
                oXMLWriter.Close();
                oXMLWriter = null;
                oXMLStream = null;
            }
            catch (System.Exception ex)
            {
                Console.WriteLine("DataSetToXML(DataSet dsData) - " + ex.Message);
                if (oXMLWriter != null)
                {
                    oXMLWriter.Close();
                }
                oXMLWriter = null;
                oXMLStream = null;
                sRet = "";
            }
            return sRet;
        }
        #endregion
        #region AddColumn Functions
        /// <summary>
        /// This method will add a column to the given table
        /// </summary>
        /// <param name="dtTable">The DataTable to add the column to</param>
        /// <param name="sColName">The name of the new column</param>
        /// <returns></returns>
        public static void AddColumn(DataTable dtTable, string sColName)
        {
            AddColumn(dtTable, sColName, "System.String", sColName);
        }
        /// <summary>
        /// This method will add a column to the given table
        /// </summary>
        /// <param name="dtTable">The DataTable to add the column to</param>
        /// <param name="sColName">The name of the new column</param>
        /// <param name="sDataType">The System.Type of this column i.e. System.String</param>
        /// <returns></returns>
        public static void AddColumn(DataTable dtTable, string sColName, string sDataType)
        {
            AddColumn(dtTable, sColName, "System.String", sColName);
        }
        /// <summary>
        /// This method will add a column to the given table
        /// </summary>
        /// <param name="dtTable">The DataTable to add the column to</param>
        /// <param name="sColName">The name of the new column</param>
        /// <param name="sDataType">The System.Type of this column i.e. System.String</param>
        /// <param name="sCaptionName">The caption text for this column</param>
        /// <returns></returns>
        public static void AddColumn(DataTable dtTable, string sColName, string sDataType, string sCaptionName)
        {
            DataColumn oColumn = null;

            oColumn = new DataColumn();
            oColumn.DataType = System.Type.GetType(sDataType);
            oColumn.AllowDBNull = false;
            oColumn.Caption = sCaptionName;
            oColumn.ColumnName = sColName;
            dtTable.Columns.Add(oColumn);
        }
        #endregion
        /// <summary>
        /// This method will convert a DBNull value to the default value, for use when setting listvalues etc..
        /// </summary>
        /// <param name="fieldValue">Value of field</param>
        /// <param name="defaultValue">Default Value to use if fieldValue is DBNULL</param>
        /// <returns></returns>
        public static string DBFieldToString(object fieldValue, string defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToString(fieldValue);
        }
        /// <summary>
        /// This method will convert a DBNull value to the default value, for use when setting listvalues etc..
        /// </summary>
        /// <param name="fieldValue">Value of field</param>
        /// <param name="defaultValue">Default Value to use if fieldValue is DBNULL</param>
        /// <returns></returns>
        public static short DBFieldToInt16(object fieldValue, short defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToInt16(fieldValue);
        }
        /// <summary>
        /// This method will convert a DBNull value to the default value, for use when setting listvalues etc..
        /// </summary>
        /// <param name="fieldValue">Value of field</param>
        /// <param name="defaultValue">Default Value to use if fieldValue is DBNULL</param>
        /// <returns></returns>
        public static int DBFieldToInt32(object fieldValue, int defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToInt32(fieldValue);
        }
        /// <summary>
        /// This method will convert a DBNull value to the default value, for use when setting listvalues etc..
        /// </summary>
        /// <param name="fieldValue">Value of field</param>
        /// <param name="defaultValue">Default Value to use if fieldValue is DBNULL</param>
        /// <returns></returns>
        public static long DBFieldToLong(object fieldValue, long defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToInt64(fieldValue);
        }
        /// <summary>
        /// This method will check to see if the row value is null prior to converting it.
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static decimal DBFieldToDecimal(object fieldValue, decimal defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToDecimal(fieldValue);
        }
        /// <summary>
        /// This method takes the lowest 32 bits of an Int64 from a fieldvalue and returns it.
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int DBFieldToLower32Bits(object fieldValue, int defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToInt32(Convert.ToInt64(fieldValue) & 0xFFFFFFFF);
        }
        /// <summary>
        /// This method takes the upper 32 bits of an Int64 from a fieldvalue, shifts it 32 bits, and returns it as an int32
        /// </summary>
        /// <param name="fieldValue"></param>
        /// <param name="defaultValue"></param>
        /// <returns></returns>
        public static int DBFieldToUpper32Bits(object fieldValue, int defaultValue)
        {
            if (fieldValue == DBNull.Value)
                return defaultValue;
            else
                return Convert.ToInt32(Convert.ToInt64(fieldValue) >> 32);
        }
        /// <summary>
        /// Checks if the specified text is positive integer
        /// </summary>
        /// <param name="strNumber"></param>
        /// <returns></returns>
        public static bool IsIntegerPositive(string strNumber)
        {
            bool bRet = false;
            Regex objNotNumberPattern = new Regex("[^0-9]");

            if (strNumber.Length <= 9)
            {
                bRet = !objNotNumberPattern.IsMatch(strNumber);

                if (strNumber == "")
                    bRet = false;
            }

            return bRet;
        }
        /// <summary>
        /// Retrieves UTC string
        /// </summary>
        /// <param name="tLocalDate"></param>
        /// <returns></returns>
        public static string GetUniversalDateTimeStr(DateTime tLocalDate)
        {
            DateTime tResultDateTime;
            string sLocalDate = "";

            try
            {
                // Convert Local time (at 12am) to Universal Time
                sLocalDate = tLocalDate.ToString("yyyy-MM-dd");
                tResultDateTime = DateTime.Parse(sLocalDate + " 00:00");
                tResultDateTime = tResultDateTime.ToUniversalTime();

                // Return String Result
                return tResultDateTime.ToString("yyyy-MM-dd HH:mm:ss");
            }
            catch
            {
                // Return Input as string
                return tLocalDate.ToString("yyyy-MM-dd");
            }
        }
        /// <summary>
        /// Retrieves file content as String
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="Error"></param>
        /// <returns></returns>
        public static String GetFileContent(String FileName, out bool Error)
        {
            String TextContent = "";

            FileInfo fInfo = new FileInfo(FileName);

            FileStream fStream = null;
            StreamReader sReader = null;

            Error = false;

            try
            {
                fStream = fInfo.OpenRead();
                sReader = new StreamReader(fStream);

                TextContent = sReader.ReadToEnd();


            }
            catch (System.IO.IOException)
            {
                Error = true;
            }
            finally
            {
                if (fStream != null)
                {
                    fStream.Close();
                }

                if (sReader != null)
                {
                    sReader.Close();
                }

            }

            return TextContent;
        }
        /// <summary>
        /// Checks if file contains specified text.
        /// </summary>
        /// <param name="FileName"></param>
        /// <param name="SearchForText"></param>
        /// <param name="CaseSensitive"></param>
        /// <returns></returns>
        public static bool FileContainsText(String FileName, String SearchForText, bool CaseSensitive)
        {
            bool Result = (SearchForText.Length == 0);

            if (!Result)
            {
                bool Error;
                String TextContent = GetFileContent(FileName, out Error);

                if (!Error)
                {
                    if (!CaseSensitive)
                    {
                        TextContent = TextContent.ToUpper();
                        SearchForText = SearchForText.ToUpper();
                    }

                    Result = (TextContent.IndexOf(SearchForText) != -1);
                }
            }

            return Result;
        }
        /// <summary>
        /// Pre-Process the stirng to strip out any trailing blanks between the text and an EOL character..
        /// </summary>
        /// <param name="strDescLines"></param>
        /// <param name="iLineLength"></param>
        /// <returns></returns>
        public static string WordWrapString(string strDescLines, int iLineLength)
        {
            //	Pre-Process the stirng to strip out any trailing blanks between the text and an EOL character..
            char[] cDesc = strDescLines.ToCharArray(0, strDescLines.Length);

            StringBuilder builder = new StringBuilder();
            bool lastValidCharEOL = true;

            for (int loop = cDesc.Length - 1; loop >= 0; loop--)
            {
                switch (cDesc[loop])
                {
                    case '\r':
                    case '\n':
                        lastValidCharEOL = true;
                        builder.Insert(0, cDesc[loop]);
                        break;
                    case ' ':
                        if (!lastValidCharEOL)
                            builder.Insert(0, cDesc[loop]);
                        break;
                    default:
                        lastValidCharEOL = false;
                        builder.Insert(0, cDesc[loop]);
                        break;
                }
            }

            string temp = builder.ToString();

            cDesc = temp.ToCharArray(0, temp.Length);
            string sResult = "";
            string sLine = "";
            int iCurrentPos = 0;

            if (cDesc.Length > 0)
            {
                for (int X = 0; X < cDesc.Length; X++)
                {
                    // For each char in the array
                    if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                    {
                        if (iCurrentPos > 0)
                        {
                            if (sLine.Length < iLineLength)
                                sResult += sLine + "\n";
                            else
                                sResult += sLine;

                            sLine = "";
                            iCurrentPos = 0;
                        }
                        else
                        {
                            sResult += "\n";
                            sLine = "";
                            iCurrentPos = 0;
                        }
                    }
                    else
                    {
                        if (iCurrentPos >= iLineLength)
                        {
                            int iLastSpace = sLine.LastIndexOf(' ');
                            if (iLastSpace > 0)
                            {
                                sResult += sLine.Substring(0, iLastSpace) + "\n";
                                sLine = sLine.Substring(iLastSpace + 1).Trim();
                                iCurrentPos = sLine.Length;
                            }
                            else
                            {
                                if (sLine.Length < iLineLength)
                                    sResult += sLine + "\n";
                                else
                                    sResult += sLine;
                                sLine = "";
                                iCurrentPos = 0;
                            }
                            if ((cDesc[X] == '\r') || (cDesc[X] == '\n'))
                            {
                                if (iCurrentPos > 0)
                                {
                                    if (sLine.Length < iLineLength)
                                        sResult += sLine + "\n";
                                    else
                                        sResult += sLine;
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                                else
                                {
                                    sResult += "\n";
                                    sLine = "";
                                    iCurrentPos = 0;
                                }
                            }
                            else
                            {
                                if (iCurrentPos == 0 && cDesc[X] == ' ')
                                {
                                    // Don't add white space to the front of the line.
                                }
                                else
                                {
                                    sLine += cDesc[X];
                                    iCurrentPos++;
                                }
                            }
                        }
                        else
                        {
                            if (iCurrentPos == 0 && cDesc[X] == ' ')
                            {
                                // Don't add white space to the front of the line.

                            }
                            else
                            {
                                sLine += cDesc[X];
                                iCurrentPos++;
                            }
                        }
                    }
                }
            }

            if (sLine.Length > 0)
                sResult += sLine;

            return sResult;
        }

        #region Zip Files/Directories
        public static void ZipFiles(string inputFolderPath, string outputPathAndFile, string password)
        {
            try
            {
                ArrayList ar = GenerateFileList(inputFolderPath); // generate file list
                int TrimLength = (Directory.GetParent(inputFolderPath)).ToString().Length;
                // find number of chars to remove     // from orginal file path
                TrimLength += 1; //remove '\'
                FileStream ostream;
                byte[] obuffer;
                string outPath = outputPathAndFile;
                ZipOutputStream oZipStream = new ZipOutputStream(File.Create(outPath)); // create zip stream
                if (password != null && password != String.Empty)
                    oZipStream.Password = password;
                oZipStream.SetLevel(9); // maximum compression
                ZipEntry oZipEntry;
                foreach (string Fil in ar) // for each file, generate a zipentry
                {
                    oZipEntry = new ZipEntry(Fil.Remove(0, TrimLength));
                    oZipStream.PutNextEntry(oZipEntry);

                    if (!Fil.EndsWith(@"/")) // if a file ends with '/' its a directory
                    {
                        ostream = File.OpenRead(Fil);
                        obuffer = new byte[ostream.Length];
                        ostream.Read(obuffer, 0, obuffer.Length);
                        oZipStream.Write(obuffer, 0, obuffer.Length);
                        ostream.Close();
                        ostream.Dispose();
                        ostream = null;
                        GC.Collect();
                    }
                }
                oZipStream.Finish();
                oZipStream.Close();
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }


        private static ArrayList GenerateFileList(string Dir)
        {
            ArrayList fils = new ArrayList();
            bool Empty = true;
            foreach (string file in Directory.GetFiles(Dir)) // add each file in directory
            {
                fils.Add(file);
                Empty = false;
            }

            if (Empty)
            {
                if (Directory.GetDirectories(Dir).Length == 0)
                // if directory is completely empty, add it
                {
                    fils.Add(Dir + @"/");
                }
            }

            foreach (string dirs in Directory.GetDirectories(Dir)) // recursive
            {
                foreach (object obj in GenerateFileList(dirs))
                {
                    fils.Add(obj);
                }
            }
            return fils; // return file list
        }


        public static void UnZipFiles(string zipPathAndFile, string outputFolder, string password, bool deleteZipFile)
        {
            ZipInputStream s = new ZipInputStream(File.OpenRead(zipPathAndFile));
            if (password != null && password != String.Empty)
                s.Password = password;
            ZipEntry theEntry;
            string tmpEntry = String.Empty;
            while ((theEntry = s.GetNextEntry()) != null)
            {
                string directoryName = outputFolder;
                string fileName = Path.GetFileName(theEntry.Name);
                // create directory 
                if (directoryName != "")
                {
                    Directory.CreateDirectory(directoryName);
                }
                if (fileName != String.Empty)
                {
                    if (theEntry.Name.IndexOf(".ini") < 0)
                    {
                        string fullPath = directoryName + "\\" + theEntry.Name;
                        fullPath = fullPath.Replace("\\ ", "\\");
                        string fullDirPath = Path.GetDirectoryName(fullPath);
                        if (!Directory.Exists(fullDirPath)) Directory.CreateDirectory(fullDirPath);
                        FileStream streamWriter = File.Create(fullPath);
                        int size = 2048;
                        byte[] data = new byte[2048];
                        while (true)
                        {
                            size = s.Read(data, 0, data.Length);
                            if (size > 0)
                            {
                                streamWriter.Write(data, 0, size);
                            }
                            else
                            {
                                break;
                            }
                        }
                        streamWriter.Close();
                    }
                }
            }
            s.Close();
            if (deleteZipFile)
                File.Delete(zipPathAndFile);
        }

        /// <summary>
        /// convert a tgz fiel to a zip file
        /// </summary>
        /// <param name="tgzFile">stream representing the tgz</param>
        /// <param name="zipFile">stream reprenenting the zip file</param>
        public static void ConvertTgzToZip(Stream tgzFile, Stream zipFile)
        {
            GZipInputStream gZipIn = new GZipInputStream(tgzFile);
            TarInputStream tarIn = new TarInputStream(gZipIn);
            ZipOutputStream zipOut = new ZipOutputStream(zipFile);
            TarEntry tarEntry;
            while ((tarEntry = tarIn.GetNextEntry()) != null)
            {
                ZipEntry zipEntry = new ZipEntry(tarEntry.Name);
                zipOut.PutNextEntry(zipEntry);
                if (!tarEntry.IsDirectory)
                {
                    using (MemoryStream fileStream = new MemoryStream())
                    {
                        tarIn.CopyEntryContents(zipOut);
                    }
                }
            }
            tarIn.Close();
            zipOut.Finish();
            zipOut.Close();
        }
        #endregion
    }
}

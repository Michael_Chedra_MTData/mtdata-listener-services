using System;
using System.IO;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// Thos class provides a number of methods for retrieving details from a Sql Server Isntance
    /// </summary>
    public class SqlUtilities : IDisposable
    {
        private string _connectionString;
        private SqlConnection _connection;
        private bool disposed = false;
        public SqlUtilities(string connectionString)
        {
            _connectionString = connectionString;
            _connection = new SqlConnection(connectionString);
            _connection.InfoMessage += new SqlInfoMessageEventHandler(_connection_InfoMessage);
        }

        public void Connect()
        {
            _connection.Open();
        }

        public void Close()
        {
            _connection.Close();
        }

        public string[] ExportTable(string tableName)
        {
            return null;
        }

        public string[] ListItemsOfType(string type)
        {
            ArrayList list = new ArrayList();
            string strSelect;

            if (type == "U")            // For tables, ignore dt*
                strSelect = "select name from sysobjects where type='U' and name not like 'dt%' ORDER BY name";
            else if (type == "P")       // For procedures, include functions, and ignore dt*
                strSelect = "select name from sysobjects where (type='P' or type='FN' or type='IF' or type='TF') and name not like 'dt%' ORDER BY name";
            else
                strSelect = string.Format("select name from sysobjects where type='{0}' ORDER BY name", type);

            using (SqlCommand command = new SqlCommand(strSelect, _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    list.Add(Convert.ToString(reader["name"]));
                }
            }

            return (string[])list.ToArray(typeof(string));
        }

        public string[] ListProcedures()
        {
            return ListItemsOfType("P");
        }

        public string[] ListTriggers()
        {
            return ListItemsOfType("TR");
        }

        public string[] ListTables()
        {
            return ListItemsOfType("U");
        }

        public string[] ListDefaults()
        {
            return ListItemsOfType("D");
        }

        public string[] ListViews()
        {
            return ListItemsOfType("V");
        }

        public string[] ListForeignKeys()
        {
            return ListItemsOfType("F");
        }


        public string[] ExportProcedureToArray(string procedureName)
        {
            ArrayList list = new ArrayList();

            using (SqlCommand command = new SqlCommand(string.Format("sp_helptext {0}", procedureName), _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    list.Add(Convert.ToString(reader["Text"]));
                }
            }

            return (string[])list.ToArray(typeof(string));
        }

        public void ExportProcedureToStream(string procedureName, StreamWriter writer)
        {
            using (SqlCommand command = new SqlCommand(string.Format("sp_helptext {0}", procedureName), _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    //	We need to trim out end-of-lines without a return
                    //	\n that do not have \r\n
                    string line = Convert.ToString(reader["Text"]);
                    line = line.Replace("\r\n", "XXPODXX");
                    line = line.Replace("\n", "XXPODXX");
                    line = line.Replace("XXPODXX", "\r\n");
                    writer.Write(line);
                }
            }
        }

        public void ExportDefaultToStream(string procedureName, string sTableName, string sColName, StreamWriter writer)
        {
            using (SqlCommand command = new SqlCommand(string.Format("sp_helptext {0}", procedureName), _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    //	We need to trim out end-of-lines without a return
                    //	\n that do not have \r\n
                    string line = Convert.ToString(reader["Text"]);
                    line = "ALTER TABLE " + sTableName + " ADD CONSTRAINT " + procedureName + " DEFAULT " + line + " FOR " + sColName;
                    writer.WriteLine(line);
                }
            }

        }

        public void ExportViewToStream(string procedureName, StreamWriter writer)
        {
            using (SqlCommand command = new SqlCommand(string.Format("sp_helptext {0}", procedureName), _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    string line = Convert.ToString(reader["Text"]);
                    line = line.Replace("\r\n", "XXPODXX");
                    line = line.Replace("\n", "XXPODXX");
                    line = line.Replace("XXPODXX", "\r\n");
                    writer.Write(line);
                }
            }
        }

        public void ExportForeignKeyToStream(string procedureName, StreamWriter writer)
        {
            string sSQL = "";

            sSQL = "SELECT 'ALTER TABLE [dbo].[' + lt.Name + '] WITH CHECK ADD CONSTRAINT [' + so.Name + '] FOREIGN KEY ([' + lcol.Name + ']) REFERENCES [dbo].[' + rt.Name + '] ([' + rcol.Name + '])' As Text ";
            sSQL += "FROM (((((sysobjects so INNER JOIN sysforeignkeys fk on fk.constid = so.ID) ";
            sSQL += "INNER JOIN syscolumns lcol on lcol.ID = fk.fkeyid AND lcol.colid = fk.fkey) ";
            sSQL += "INNER JOIN syscolumns rcol on rcol.ID = fk.rkeyid AND rcol.colid = fk.rkey) ";
            sSQL += "INNER JOIN sysobjects lt on lt.ID = fk.fkeyid) ";
            sSQL += "INNER JOIN sysobjects rt on rt.ID = fk.rkeyid) ";
            sSQL += "WHERE so.Name = '" + procedureName + "' AND so.Type = 'F' ";

            using (SqlCommand command = new SqlCommand(sSQL, _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                try
                {
                    while (reader.Read())
                    {
                        string line = Convert.ToString(reader["Text"]);
                        writer.WriteLine(line);
                    }
                }
                catch (System.Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }
        }

        public string GetTableNameForDefault(string sDefaultName)
        {
            string sRet = "";
            string sSQLCommand = "SELECT Name FROM sysobjects where id = (SELECT top 1 parent_obj FROM sysobjects WHERE Name = '" + sDefaultName + "' AND Type = 'D')";

            using (SqlCommand oCmd = new SqlCommand(sSQLCommand, _connection))
            using (SqlDataAdapter oDA = new SqlDataAdapter(oCmd))
            using (DataSet dsRet = new DataSet())
            {
                oCmd.CommandTimeout = 0;
                oDA.Fill(dsRet);

                if (dsRet.Tables.Count > 0)
                {
                    if (dsRet.Tables[0].Rows.Count > 0)
                    {
                        if (dsRet.Tables[0].Rows[0]["Name"] != System.DBNull.Value)
                            sRet = Convert.ToString(dsRet.Tables[0].Rows[0]["Name"]);
                    }
                }
            }

            return sRet;
        }

        public string GetColumnForDefault(string sDefaultName)
        {
            string sRet = "";
            string sSQLCommand = "select Name from syscolumns WHERE ID = (SELECT top 1 parent_obj FROM sysobjects WHERE Name = '" + sDefaultName + "' AND Type = 'D') AND cdefault = (SELECT top 1 ID FROM sysobjects WHERE Name = '" + sDefaultName + "' AND Type = 'D')";

            using (SqlCommand oCmd = new SqlCommand(sSQLCommand, _connection))
            using (SqlDataAdapter oDA = new SqlDataAdapter(oCmd))
            using (DataSet dsRet = new DataSet())
            {
                oCmd.CommandTimeout = 0;
                oDA.Fill(dsRet);

                if (dsRet.Tables.Count > 0)
                {
                    if (dsRet.Tables[0].Rows.Count > 0)
                    {
                        if (dsRet.Tables[0].Rows[0]["Name"] != System.DBNull.Value)
                            sRet = Convert.ToString(dsRet.Tables[0].Rows[0]["Name"]);
                    }
                }
            }
            return sRet;
        }

        public string GetTableNameForForeignKey(string sDefaultName)
        {
            string sRet = "";
            string sSQLCommand = "";


            sSQLCommand = "SELECT Name FROM sysobjects where id = (SELECT top 1 parent_obj FROM sysobjects WHERE Name = '" + sDefaultName + "' AND Type = 'F')";
            using (SqlCommand oCmd = new SqlCommand(sSQLCommand, _connection))
            using (SqlDataAdapter oDA = new SqlDataAdapter(oCmd))
            using (DataSet dsRet = new DataSet())
            {
                #region Get the dataset
                oCmd.CommandTimeout = 0;
                oDA.Fill(dsRet);

                #endregion

                if (dsRet != null)
                {
                    if (dsRet.Tables.Count > 0)
                    {
                        if (dsRet.Tables[0].Rows.Count > 0)
                        {
                            if (dsRet.Tables[0].Rows[0]["Name"] != System.DBNull.Value)
                                sRet = Convert.ToString(dsRet.Tables[0].Rows[0]["Name"]);
                        }
                    }
                }

                return sRet;
            }
        }

        public void ExcuteSelectDS(string sSQLCommand, StreamWriter writer)
        {

            int iTable = 0;
            int iColumn = 0;
            int iRow = 0;
            string sColNames = "";

            #region Get the dataset
            using (SqlCommand oCmd = new SqlCommand(sSQLCommand, _connection))
            using (SqlDataAdapter oDA = new SqlDataAdapter(oCmd))
            using (DataSet dsRet = new DataSet())
            {
                oCmd.CommandTimeout = 0;
                sConsoleMessages = "";
                oDA.Fill(dsRet);

                #endregion

                if (sConsoleMessages.Length > 0)
                {
                    writer.WriteLine("Console : ");
                    writer.WriteLine(sConsoleMessages);
                }

                if (dsRet != null)
                {
                    for (iTable = 0; iTable < dsRet.Tables.Count; iTable++)
                    {
                        sColNames = "";
                        for (iColumn = 0; iColumn < dsRet.Tables[iTable].Columns.Count; iColumn++)
                        {
                            sColNames += ", " + dsRet.Tables[iTable].Columns[iColumn].ColumnName;
                        }
                        if (sColNames.Length > 0)
                            sColNames = sColNames.Substring(2, sColNames.Length - 2);

                        writer.WriteLine("Result Set : " + Convert.ToString(iTable));
                        writer.WriteLine("");
                        writer.WriteLine(sColNames);

                        for (iRow = 0; iRow < dsRet.Tables[iTable].Rows.Count; iRow++)
                        {
                            sColNames = "";
                            for (iColumn = 0; iColumn < dsRet.Tables[iTable].Columns.Count; iColumn++)
                            {
                                if (dsRet.Tables[iTable].Rows[iRow][iColumn] != System.DBNull.Value)
                                    sColNames += ", " + Convert.ToString(dsRet.Tables[iTable].Rows[iRow][iColumn]);
                                else
                                    sColNames += ", null";
                            }
                            if (sColNames.Length > 0)
                                sColNames = sColNames.Substring(2, sColNames.Length - 2);
                            writer.WriteLine(sColNames);
                        }
                    }

                }
            }
        }

        private string sConsoleMessages = "";
        public void ExecuteScalar(string sSQLCommand, StreamWriter writer)
        {

            string sRet = "";
            object oRet = null;

            #region Get the dataset

            using (SqlCommand oCmd = new SqlCommand(sSQLCommand, _connection))
            {
                oCmd.CommandTimeout = 0;
                sConsoleMessages = "";
                oRet = oCmd.ExecuteScalar();
                sRet = Convert.ToString(oRet);
            }
            #endregion
            if (sConsoleMessages.Length > 0)
            {
                writer.WriteLine("Console : ");
                writer.WriteLine(sConsoleMessages);
            }
            writer.WriteLine("Result : " + sRet);
        }

        public void ExecuteNonQuery(string sSQLCommand, StreamWriter writer)
        {

            int iRet = 0;

            #region Get the dataset

            using (SqlCommand oCmd = new SqlCommand(sSQLCommand, _connection))
            {
                oCmd.CommandTimeout = 0;
                sConsoleMessages = "";
                iRet = oCmd.ExecuteNonQuery();

                #endregion
                if (sConsoleMessages.Length > 0)
                {
                    writer.WriteLine("Console : ");
                    writer.WriteLine(sConsoleMessages);
                }
                writer.WriteLine("Result : " + Convert.ToString(iRet));
                writer.WriteLine("");
            }
        }

        public void ExportTableToStream(string tableName, StreamWriter writer)
        {
            using (SqlCommand command = new SqlCommand(string.Format("sp_columns {0}", tableName), _connection))
            using (SqlDataReader reader = command.ExecuteReader())
            {
                bool headerWritten = false;
                while (reader.Read())
                {
                    if (!headerWritten)
                    {
                        writer.WriteLine("CREATE TABLE [{0}] (", tableName);
                        headerWritten = true;
                    }
                    else
                        writer.WriteLine(",");

                    writer.Write("\t[");
                    writer.Write(reader["COLUMN_NAME"]);
                    writer.Write("] [");
                    string typename = Convert.ToString(reader["TYPE_NAME"]);
                    writer.Write(typename);
                    writer.Write("] ");

                    if (typename == "bigint identity")
                        writer.Write("(1,1) ");

                    if (typename == "varchar")
                        writer.Write("({0}) COLLATE SQL_Latin1_General_CP1_CI_AS ", Convert.ToInt32(reader["LENGTH"]));

                    if (Convert.ToInt32(reader["NULLABLE"]) == 1)
                        writer.Write("NULL ");
                    else
                        writer.Write("NOT NULL ");

                }
                if (headerWritten)
                {
                    writer.WriteLine("");
                    writer.WriteLine(") ON [PRIMARY]");
                }
            }
        }

        private void _connection_InfoMessage(object sender, SqlInfoMessageEventArgs e)
        {
            sConsoleMessages += e.Message + "\r\n";
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                try { _connection?.Dispose(); } catch { }
            }

            disposed = true;
        }
    }
}

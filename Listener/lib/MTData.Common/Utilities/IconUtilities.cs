using System;
using System.IO;
using System.Text;
using System.Drawing;

namespace MTData.Common.Utilities
{
    /// <summary>
    /// This class provides mechanisms for loading and modifying icon files without the
    /// intervention of the imaging library. This is much faster than regular manipulation.
    /// NOTE : This class is not a complete utility yet. A clean new icon cannot be manufactured
    /// at this time. It works based on manipulation of an existing icon.
    /// </summary>
    public class IconUtilities
    {
        /// <summary>
        /// Instantiate the class with default values.
        /// </summary>
        public IconUtilities()
        {
        }

        /// <summary>
        /// Instantiate the class, and load up the specified icon file.
        /// </summary>
        /// <param name="filename"></param>
        public IconUtilities(string filename)
            : this()
        {
            Load(filename);
        }

        /// <summary>
        /// Instantiate the class and load up the icon from the stream specified.
        /// </summary>
        /// <param name="stream"></param>
        public IconUtilities(Stream stream)
            : this()
        {
            Load(stream);
        }

        /// <summary>
        /// Load up the icon file.
        /// </summary>
        /// <param name="fileName"></param>
        public void Load(string fileName)
        {
            FileStream reader = new FileStream(fileName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
            try
            {
                Load(reader);
            }
            finally
            {
                reader.Close();
            }
        }

        /// <summary>
        /// Load the icon file form the stream specified.
        /// </summary>
        /// <param name="stream"></param>
        public void Load(Stream stream)
        {
            ReadHeader(stream, _header);

            _directory = new IconDirectory[_header.IconCount];

            ReadIconDirectories(stream, _directory);

            _bitmapHeaders = new BitmapHeaderInfo[_header.IconCount];
            ReadBitmaps(stream, _bitmapHeaders, _directory);
        }

        /// <summary>
        /// Saves the icon file to the filename specified
        /// </summary>
        /// <param name="fileName"></param>
        public void Save(string fileName)
        {
            FileStream writer = new FileStream(fileName, FileMode.Create);
            try
            {
                Save(writer);
            }
            finally
            {
                writer.Close();
            }
        }

        /// <summary>
        /// SAves the icon to the stream specified.
        /// </summary>
        /// <param name="stream"></param>
        public void Save(Stream stream)
        {
            WriteHeader(stream, _header);
            WriteIconDirectories(stream, _directory);
            WriteBitmaps(stream, _bitmapHeaders, _directory);
        }

        #region Icon Header

        /// <summary>
        /// This class denotes the contents of the Icon Header
        /// </summary>
        public class IconHeader
        {
            public int Reserved;
            public int Type;
            public int IconCount;
        }

        /// <summary>
        /// This holds the icon header in memory
        /// </summary>
        private IconHeader _header = new IconHeader();

        /// <summary>
        /// The icon header constists of the following..
        ///		byte[2] reserved (always 0)
        ///		byte[2] type	 {always 1 for icons}
        ///		byte[2] count	 {total number of icon pics in file}
        /// </summary>
        /// <param name="stream"></param>
        private void ReadHeader(Stream stream, IconHeader header)
        {
            byte[] buffer = new Byte[6];
            if (stream.Read(buffer, 0, 6) != 6)
                throw new Exception("Not enough bytes in stream for header");

            int index = 0;
            header.Reserved = ReadWordLSBMSB(buffer, index);
            index += 2;

            header.Type = ReadWordLSBMSB(buffer, index);
            index += 2;

            header.IconCount = ReadWordLSBMSB(buffer, index);

        }

        /// <summary>
        /// Write out the header for hte icon file.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="header"></param>
        private void WriteHeader(Stream stream, IconHeader header)
        {
            byte[] buffer = new Byte[6];

            int index = 0;
            WriteWordLSBMSB(buffer, index, header.Reserved);
            index += 2;

            WriteWordLSBMSB(buffer, index, header.Type);
            index += 2;

            WriteWordLSBMSB(buffer, index, header.IconCount);

            stream.Write(buffer, 0, buffer.Length);
        }

        #endregion

        #region Icon Directory

        /// <summary>
        /// This class denotes an entry in the icon directory, which is a list of the 
        /// icon details at the top of the file.
        /// </summary>
        public class IconDirectory
        {
            public int Width;
            public int Height;
            public int ColorCount;
            public byte Reserved;
            public int Planes;
            public int BitCount;
            public long BytesInResource; // Including pallette data XOR, AND and Bitmap Info
            public long ImageOffset;	 // Offset from beginning of file.
        }

        /// <summary>
        /// This is a list of the icon directories.
        /// </summary>
        private IconDirectory[] _directory = null;

        /// <summary>
        /// Loads the icon directories form the stream provided.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="directory"></param>
        private void ReadIconDirectories(Stream stream, IconDirectory[] directory)
        {
            byte[] buffer = new byte[16];

            for (int loop = 0; loop < _header.IconCount; loop++)
            {
                directory[loop] = new IconDirectory();

                if (stream.Read(buffer, 0, 16) != 16)
                    throw new Exception("Insufficient data to retrieve Icon Directory");

                int index = 0;
                directory[loop].Width = Convert.ToInt32(buffer[index++]);
                directory[loop].Height = Convert.ToInt32(buffer[index++]);
                directory[loop].ColorCount = Convert.ToInt32(buffer[index++]);
                directory[loop].Reserved = buffer[index++];
                directory[loop].Planes = ReadWordLSBMSB(buffer, index);
                index += 2;
                directory[loop].BitCount = ReadWordLSBMSB(buffer, index);
                index += 2;
                directory[loop].BytesInResource = ReadDWordLSBMSB(buffer, index);
                index += 4;
                directory[loop].ImageOffset = ReadDWordLSBMSB(buffer, index);
            }
        }

        /// <summary>
        /// Takes the existing icon directories and writes them out to the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="directory"></param>
        private void WriteIconDirectories(Stream stream, IconDirectory[] directory)
        {

            for (int loop = 0; loop < directory.Length; loop++)
            {
                byte[] buffer = new byte[16];

                int index = 0;
                buffer[index++] = Convert.ToByte(directory[loop].Width);
                buffer[index++] = Convert.ToByte(directory[loop].Height);
                buffer[index++] = Convert.ToByte(directory[loop].ColorCount);
                buffer[index++] = directory[loop].Reserved;
                WriteWordLSBMSB(buffer, index, directory[loop].Planes);
                index += 2;
                WriteWordLSBMSB(buffer, index, directory[loop].BitCount);
                index += 2;
                WriteDWordLSBMSB(buffer, index, directory[loop].BytesInResource);
                index += 4;
                WriteDWordLSBMSB(buffer, index, directory[loop].ImageOffset);

                stream.Write(buffer, 0, buffer.Length);
            }
        }

        #endregion

        #region Bitmap Header Details

        /// <summary>
        /// This class defines the contents of the bitmap itself within the icon file.
        /// </summary>
        public class BitmapHeaderInfo
        {
            public long Size;
            public long Width;
            public long Height;
            public int Planes;
            public int BitCount;	//  {number color bits 4 = 16 colors, 8 = 256 pixel is a byte}
            public long Compression;
            public long SizeImage;
            public long XPelsPerMeter;
            public long YPelsPerMeter;
            public long ColorUsed;
            public long ColorImportant;

            public long[] Colors = null;

            public int[,] XORMask = null;
            public bool[,] ANDMask = null;
        }

        /// <summary>
        /// This is a list of bitmap contents.
        /// </summary>
        private BitmapHeaderInfo[] _bitmapHeaders = null;

        /// <summary>
        /// This reads all of the bitmap data from the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="headerInfo"></param>
        /// <param name="directories"></param>
        private void ReadBitmaps(Stream stream, BitmapHeaderInfo[] headerInfo, IconDirectory[] directories)
        {
            for (int loop = 0; loop < headerInfo.Length; loop++)
            {
                headerInfo[loop] = new BitmapHeaderInfo();
                ReadBitmapHeader(stream, headerInfo[loop]);

                if (headerInfo[loop].BitCount <= 8)
                    ReadBitmapPallette(stream, headerInfo[loop], directories[loop].ColorCount);

                ReadBitmapXORMask(stream, headerInfo[loop], directories[loop].Width, directories[loop].Height);
                ReadBitmapANDMask(stream, headerInfo[loop], directories[loop].Width, directories[loop].Height);
            }

        }

        /// <summary>
        /// This writes out the bitmap data from the stream
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="headerInfo"></param>
        /// <param name="directories"></param>
        private void WriteBitmaps(Stream stream, BitmapHeaderInfo[] headerInfo, IconDirectory[] directories)
        {
            for (int loop = 0; loop < headerInfo.Length; loop++)
            {
                WriteBitmapHeader(stream, headerInfo[loop]);

                if (headerInfo[loop].BitCount <= 8)
                    WriteBitmapPallette(stream, headerInfo[loop], directories[loop].ColorCount);

                WriteBitmapXORMask(stream, headerInfo[loop], directories[loop].Width, directories[loop].Height);
                WriteBitmapANDMask(stream, headerInfo[loop], directories[loop].Width, directories[loop].Height);
            }


        }

        /// <summary>
        /// This reads the bitmap header record identifying what is available in the data that follows it.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="headerInfo"></param>
        private void ReadBitmapHeader(Stream stream, BitmapHeaderInfo headerInfo)
        {
            byte[] sizebuffer = new byte[4];
            if (stream.Read(sizebuffer, 0, 4) != 4)
                throw new Exception("Insufficient data to load SizeBuffer of BitmapInfoHeader");

            headerInfo.Size = ReadDWordLSBMSB(sizebuffer, 0);

            byte[] buffer = new byte[headerInfo.Size - 4];
            if (stream.Read(buffer, 0, buffer.Length) != buffer.Length)
                throw new Exception("Insufficient data to load BitmapInfoHeader");

            int index = 0;
            headerInfo.Width = ReadDWordLSBMSB(buffer, index);
            index += 4;
            headerInfo.Height = ReadDWordLSBMSB(buffer, index);
            index += 4;
            headerInfo.Planes = ReadWordLSBMSB(buffer, index);
            index += 2;
            headerInfo.BitCount = ReadWordLSBMSB(buffer, index);
            index += 2;
            headerInfo.Compression = ReadDWordLSBMSB(buffer, index);
            index += 4;

            headerInfo.SizeImage = ReadDWordLSBMSB(buffer, index);
            index += 4;
            headerInfo.XPelsPerMeter = ReadDWordLSBMSB(buffer, index);
            index += 4;
            headerInfo.YPelsPerMeter = ReadDWordLSBMSB(buffer, index);
            index += 4;

            headerInfo.ColorUsed = ReadDWordLSBMSB(buffer, index);
            index += 4;
            headerInfo.ColorImportant = ReadDWordLSBMSB(buffer, index);
            index += 4;

        }

        /// <summary>
        /// This method writes out the bitmap header to a stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="headerInfo"></param>
        private void WriteBitmapHeader(Stream stream, BitmapHeaderInfo headerInfo)
        {
            byte[] buffer = new byte[headerInfo.Size];

            int index = 0;
            WriteDWordLSBMSB(buffer, index, headerInfo.Size);
            index += 4;

            WriteDWordLSBMSB(buffer, index, headerInfo.Width);
            index += 4;
            WriteDWordLSBMSB(buffer, index, headerInfo.Height);
            index += 4;
            WriteWordLSBMSB(buffer, index, headerInfo.Planes);
            index += 2;
            WriteWordLSBMSB(buffer, index, headerInfo.BitCount);
            index += 2;
            WriteDWordLSBMSB(buffer, index, headerInfo.Compression);
            index += 4;

            WriteDWordLSBMSB(buffer, index, headerInfo.SizeImage);
            index += 4;
            WriteDWordLSBMSB(buffer, index, headerInfo.XPelsPerMeter);
            index += 4;
            WriteDWordLSBMSB(buffer, index, headerInfo.YPelsPerMeter);
            index += 4;

            WriteDWordLSBMSB(buffer, index, headerInfo.ColorUsed);
            index += 4;
            WriteDWordLSBMSB(buffer, index, headerInfo.ColorImportant);
            index += 4;
            stream.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// This method reads the pallette for a bitmap from the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        /// <param name="colorCount"></param>
        private void ReadBitmapPallette(Stream stream, BitmapHeaderInfo info, int colorCount)
        {
            //	The pallette will consist of a number of colors...
            info.Colors = new long[colorCount];

            byte[] buffer = new byte[4];

            for (int loop = 0; loop < info.Colors.Length; loop++)
            {
                if (stream.Read(buffer, 0, 4) != 4)
                    throw new Exception("Insufficient data to read pallette");

                info.Colors[loop] = ReadDWordLSBMSB(buffer, 0);
            }
        }

        /// <summary>
        /// This method writes out the bitmap pallette
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        /// <param name="colorCount"></param>
        private void WriteBitmapPallette(Stream stream, BitmapHeaderInfo info, int colorCount)
        {

            //	The pallette will consist of a number of colors...
            byte[] buffer = new byte[4 * colorCount];

            for (int loop = 0; loop < info.Colors.Length; loop++)
            {
                WriteDWordLSBMSB(buffer, loop * 4, info.Colors[loop]);
            }
            stream.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// This method reads the XOR bitmask that identifies positions within the pallette for
        /// each color specified in the mask.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void ReadBitmapXORMask(Stream stream, BitmapHeaderInfo info, int width, int height)
        {
            //	depending on bitcount, if it is 4, we use a nibble per offset into the pallete
            //						   if it is 8, we use a byte per offset into the pallette
            info.XORMask = new int[width, height];

            bool splitByte = (info.BitCount == 4);
            int byteWidth = (splitByte ? width / 2 : width);

            byte[] buffer = new byte[byteWidth * height];
            if (stream.Read(buffer, 0, buffer.Length) != buffer.Length)
                throw new Exception("Insufficient bytes to laod XOR mask");

            int xIndex = 0;
            int yIndex = 0;

            for (int loop = 0; loop < buffer.Length; loop++)
            {
                if (splitByte)
                {
                    info.XORMask[xIndex++, yIndex] = Convert.ToInt32((buffer[loop] >> 4));
                    info.XORMask[xIndex++, yIndex] = Convert.ToInt32((buffer[loop] & (byte)0x0F));
                }
                else
                {
                    info.XORMask[xIndex++, yIndex] = Convert.ToInt32(buffer[loop]);
                }

                if (xIndex == width)
                {
                    xIndex = 0;
                    yIndex++;
                }
            }
        }

        /// <summary>
        /// This takes the XOR mask and writes it out to the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void WriteBitmapXORMask(Stream stream, BitmapHeaderInfo info, int width, int height)
        {
            //	depending on bitcount, if it is 4, we use a nibble per offset into the pallete
            //						   if it is 8, we use a byte per offset into the pallette
            bool splitByte = (info.BitCount == 4);
            int byteWidth = (splitByte ? width / 2 : width);

            byte[] buffer = new byte[byteWidth * height];

            int xIndex = 0;
            int yIndex = 0;

            for (int loop = 0; loop < buffer.Length; loop++)
            {
                if (splitByte)
                {
                    buffer[loop] = Convert.ToByte((int)(info.XORMask[xIndex++, yIndex] << 4) + (int)info.XORMask[xIndex++, yIndex]);
                }
                else
                {
                    buffer[loop] = Convert.ToByte(info.XORMask[xIndex++, yIndex]);
                }

                if (xIndex == width)
                {
                    xIndex = 0;
                    yIndex++;
                }
            }
            stream.Write(buffer, 0, buffer.Length);
        }

        /// <summary>
        /// This reads the AND mask which is a boolean mask identifying 8 pixels per byte, with either
        /// a black or a white (0 or 1) to idnetify whether it is painted or not.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void ReadBitmapANDMask(Stream stream, BitmapHeaderInfo info, int width, int height)
        {
            //	This mask is bit per pixel..
            info.ANDMask = new bool[width, height];

            byte[] buffer = new byte[(width / 8) * height];
            if (stream.Read(buffer, 0, buffer.Length) != buffer.Length)
                throw new Exception("Insufficient bytes to laod AND mask");

            int xIndex = 0;
            int yIndex = 0;

            for (int loop = 0; loop < buffer.Length; loop++)
            {
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x80) == (byte)0x80);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x40) == (byte)0x40);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x20) == (byte)0x20);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x10) == (byte)0x10);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x08) == (byte)0x08);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x04) == (byte)0x04);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x02) == (byte)0x02);
                info.ANDMask[xIndex++, yIndex] = ((buffer[loop] & (byte)0x01) == (byte)0x01);

                if (xIndex == width)
                {
                    xIndex = 0;
                    yIndex++;
                }
            }
        }

        /// <summary>
        /// Writes out the AND mask to the stream.
        /// </summary>
        /// <param name="stream"></param>
        /// <param name="info"></param>
        /// <param name="width"></param>
        /// <param name="height"></param>
        private void WriteBitmapANDMask(Stream stream, BitmapHeaderInfo info, int width, int height)
        {
            //	This mask is bit per pixel..

            byte[] buffer = new byte[(width / 8) * height];

            int xIndex = 0;
            int yIndex = 0;

            for (int loop = 0; loop < buffer.Length; loop++)
            {
                buffer[loop] = 0;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x80 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x40 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x20 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x10 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x08 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x04 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x02 : (byte)0x00;
                buffer[loop] |= (info.ANDMask[xIndex++, yIndex]) ? (byte)0x01 : (byte)0x00;

                if (xIndex == width)
                {
                    xIndex = 0;
                    yIndex++;
                }
            }
            stream.Write(buffer, 0, buffer.Length);
        }

        #endregion

        #region Utility Functions

        /// <summary>
        /// Read in 4 bytes and format them based on LSB first, MSB last.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private long ReadDWordLSBMSB(byte[] buffer, int offset)
        {
            long result = 0;
            result = Convert.ToInt64(buffer[offset + 3]);
            result = (result << 8) + Convert.ToInt64(buffer[offset + 2]);
            result = (result << 8) + Convert.ToInt64(buffer[offset + 1]);
            result = (result << 8) + Convert.ToInt64(buffer[offset]);

            return result;
        }

        /// <summary>
        /// Write out 4 bytes and format them based on LSB first, MSB last.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="newValue"></param>
        private void WriteDWordLSBMSB(byte[] buffer, int offset, long newValue)
        {
            long temp = newValue;

            buffer[offset] = Convert.ToByte(temp & (long)0x000000FF);
            temp = (temp >> 8);
            buffer[offset + 1] = Convert.ToByte(temp & (long)0x000000FF);
            temp = (temp >> 8);
            buffer[offset + 2] = Convert.ToByte(temp & (long)0x000000FF);
            temp = (temp >> 8);
            buffer[offset + 3] = Convert.ToByte(temp & (long)0x000000FF);
        }

        /// <summary>
        /// Read in 2 bytes and format them based on LSB first, MSB last.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <returns></returns>
        private int ReadWordLSBMSB(byte[] buffer, int offset)
        {
            int result = Convert.ToInt32(buffer[offset + 1]);
            result = (result << 8) + Convert.ToInt32(buffer[offset]);
            return result;
        }

        /// <summary>
        /// Write out 2 bytes and format them based on LSB first, MSB last.
        /// </summary>
        /// <param name="buffer"></param>
        /// <param name="offset"></param>
        /// <param name="newValue"></param>
        private void WriteWordLSBMSB(byte[] buffer, int offset, int newValue)
        {
            buffer[offset] = Convert.ToByte(newValue & (int)0x000000FF);
            buffer[offset + 1] = Convert.ToByte((int)(newValue >> 8) & (int)0x000000FF);

        }

        #endregion

        #region properties

        /// <summary>
        /// Access to the header
        /// </summary>
        public IconHeader Header
        {
            get { return _header; }
        }

        /// <summary>
        /// Access to the directory
        /// </summary>
        public IconDirectory[] Directory
        {
            get { return _directory; }
        }

        /// <summary>
        /// Access to the bitmap information
        /// </summary>
        public BitmapHeaderInfo[] Info
        {
            get { return _bitmapHeaders; }
        }

        #endregion

        /// <summary>
        /// Takes the values read from a stream, and formats them for logical display on screen.
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();

            builder.Append("Header\r\n");
            builder.Append("  IconCount : ");
            builder.Append(_header.IconCount);
            builder.Append("\r\n");

            for (int loop = 0; loop < _header.IconCount; loop++)
            {
                builder.Append("Icon : ");
                builder.Append(loop);
                builder.Append("\r\n");

                builder.Append("  Size (w/h): ");
                builder.Append(_directory[loop].Width);
                builder.Append(",");
                builder.Append(_directory[loop].Height);

                builder.Append("  Colors :\r\n");
                for (int colorLoop = 0; colorLoop < _bitmapHeaders[loop].Colors.Length; colorLoop++)
                {
                    builder.Append("    ");
                    builder.Append(_bitmapHeaders[loop].Colors[colorLoop].ToString("X8"));
                    builder.Append("\r\n");
                }
            }
            return builder.ToString();
        }

        /// <summary>
        /// Load the icon stream, and parse it until you get to the color concerned, 
        /// and then write out the modified pallette
        /// </summary>
        /// <param name="sourceFile"></param>
        /// <param name="targetFile"></param>
        /// <param name="sourceColor"></param>
        /// <param name="targetColor"></param>
        public static void ChangeFilePalette(string sourceFile, string targetFile, Color sourceColor, Color targetColor)
        {
            long colorMatch = (sourceColor.ToArgb() & (long)0x00FFFFFF);
            long targetMatch = (targetColor.ToArgb() & (long)0x00FFFFFF);

            IconUtilities source = new IconUtilities();
            source.Load(sourceFile);
            for (int loop = 0; loop < source._bitmapHeaders.Length; loop++)
            {
                BitmapHeaderInfo header = source._bitmapHeaders[loop];

                for (int colorLoop = 0; colorLoop < header.Colors.Length; colorLoop++)
                {
                    if (header.Colors[colorLoop] == colorMatch)
                    {
                        header.Colors[colorLoop] = targetMatch;
                        break;
                    }
                }
            }

            source.Save(targetFile);

        }
    }
}

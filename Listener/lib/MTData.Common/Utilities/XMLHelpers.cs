﻿using System;
using System.Text;
using System.Xml;

namespace MTData.Common.Utilities
{
    public class XMLHelpers
    {
        #region Constructor

        #endregion

        #region Public Events/Delegates

        #endregion

        #region Private Fields

        #endregion

        #region Public Properties

        #endregion

        #region Methods
        public static string TabifyXML(XmlDocument doc)
        {
            return BeautifyXML(doc, "\t");
        }

        public static string BeautifyXMLCustomCharacter(XmlDocument doc, string customCharacter)
        {
            return BeautifyXML(doc, customCharacter);
        }

        public static XmlElement CreateElement(XmlDocument ownerDocument, string elementName)
        {
            return ownerDocument.CreateElement(elementName);
        }

        public static XmlElement CreateElementWithInnerText(XmlDocument ownerDocument, string elementName, object innerText)
        {
            XmlElement newElement = CreateElement(ownerDocument, elementName);

            if (innerText is bool)
            {
                newElement.InnerText = Boolean.Parse(innerText.ToString()).ToString();
            }
            else
            {
                newElement.InnerText = innerText.ToString();
            }

            return newElement;
        }

        public static void AddAttribute(XmlElement elementToUse, string name, object value)
        {
            if (value is bool)
            {
                elementToUse.SetAttribute(name, Boolean.Parse(value.ToString()).ToString());
            }
            else
            {
                elementToUse.SetAttribute(name, value.ToString());
            }
        }

        private static string BeautifyXML(XmlDocument doc, string characterToUse)
        {
            StringBuilder sb = new StringBuilder();
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.Indent = true;
            settings.IndentChars = characterToUse;
            settings.NewLineChars = "\r\n";
            settings.NewLineHandling = NewLineHandling.Replace;

            using (XmlWriter writer = XmlWriter.Create(sb, settings))
            {
                doc.Save(writer);
            }

            return sb.ToString();
        }
        public static string GetNodeWithVersion(System.Data.DataRow row, int tabDepth)
        {
            string nodeName = Convert.ToString(row["XmlTag"]);
            string version = Convert.ToString(row["DataSegmentVersion"]);
            string xml = string.Format("<{0} version=\"{1}\">", nodeName, version);

            for (int i = 0; i < tabDepth; i++)
            {
                xml = "\t" + xml;
            }
            return xml;
        }

        public static string GetNodeWithVersionAndType(System.Data.DataRow row, string itemType, int tabDepth)
        {
            string nodeName = Convert.ToString(row["XmlTag"]);
            string version = Convert.ToString(row["DataSegmentVersion"]);
            string xml = string.Format("<{0} type=\"{2}\" version=\"{1}\">", nodeName, version, itemType);

            for (int i = 0; i < tabDepth; i++)
            {
                xml = "\t" + xml;
            }
            return xml;
        }

        public static string GetEndNode(System.Data.DataRow row, int tabDepth)
        {
            string nodeName = Convert.ToString(row["XmlTag"]);
            string xml = string.Format("</{0}>", nodeName);

            for (int i = 0; i < tabDepth; i++)
            {
                xml = "\t" + xml;
            }
            return xml;
        }

        public static string GetNodeForColumn(System.Data.DataRow row, string columnName, int tabDepth)
        {
            if (columnName.EndsWith("ID"))
            {
                columnName = columnName.Substring(0, columnName.Length - 2) + "Id";
            }
            string xmlNode = string.Empty;
            if (row.Table.Columns.Contains(columnName))
            {
                object value = row[columnName];
                if (value != null && value != DBNull.Value)
                {
                    if (value is byte[])
                    {
                        xmlNode = BitConverter.ToString((byte[])value);
                    }
                    else if (value is DateTime)
                    {
                        xmlNode = Convert.ToDateTime(value).ToString("yyyy-MM-ddTHH:mm:ss");
                    }
                    else
                    {

                        xmlNode = Convert.ToString(value);
                        xmlNode = ReplaceXmlSpecialCharacters(xmlNode);
                    }
                    xmlNode = string.Format("<{0}>{1}</{0}>\n", columnName, xmlNode);
                    for (int X = 0; X < tabDepth; X++)
                        xmlNode = "\t" + xmlNode;
                }
            }
            return xmlNode;
        }

        public static string GetNodeForValue(string value, string columnName, int tabDepth)
        {
            if (columnName.EndsWith("ID"))
            {
                columnName = columnName.Substring(0, columnName.Length - 2) + "Id";
            }
            string xmlNode = string.Empty;
            if (value != null)
            {
                xmlNode = ReplaceXmlSpecialCharacters(value);
                xmlNode = string.Format("<{0}>{1}</{0}>\n", columnName, xmlNode);
                for (int X = 0; X < tabDepth; X++)
                    xmlNode = "\t" + xmlNode;
            }
            return xmlNode;
        }

        public static string ReplaceXmlSpecialCharacters(string xmlNode)
        {
            if (xmlNode.Length > 0)
            {
                xmlNode = xmlNode.Replace("&", "&amp;");
                xmlNode = xmlNode.Replace("<", "&lt;");
                xmlNode = xmlNode.Replace(">", "&gt;");
                xmlNode = xmlNode.Replace("\"", "&quot;");
                xmlNode = xmlNode.Replace("'", "&apos;");
            }
            return xmlNode;
        }

        public static string GetStringFromObject(object item)
        {
            string value = "";
            if (item != null && item != System.DBNull.Value)
            {
                if (item is byte[])
                {
                    value = BitConverter.ToString((byte[])item);
                }
                else if (item is DateTime)
                {
                    value = Convert.ToDateTime(item).ToString("yyyy-MM-ddTHH:mm:ss");
                }
                else
                {
                    value = Convert.ToString(item);
                }
            }
            return value;
        }

        public static XmlElement GetXMLElement(string sXML)
        {
            try
            {
                XmlTextReader reader = new XmlTextReader(new System.IO.StringReader(sXML));
                reader.WhitespaceHandling = WhitespaceHandling.None;
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);
                reader.Close();
                return doc.DocumentElement;
            }
            catch (Exception ex)
            {
                throw (new Exception(ex.Message));
            }
        }
        #endregion

        #region Override Methods

        #endregion
    }
}

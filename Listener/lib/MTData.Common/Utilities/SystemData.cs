using System;
using System.Diagnostics;
using System.IO;
using System.Management;
using System.Collections;

namespace MTData.Common.Utilities
{
	public class SystemData
	{
		#region Local Vars
		PerformanceCounter _memoryCounter = new PerformanceCounter();	
		PerformanceCounter _cpuCounter = new PerformanceCounter();	
		PerformanceCounter _diskReadCounter = new PerformanceCounter();	
		PerformanceCounter _diskWriteCounter = new PerformanceCounter();	
		string[] _instanceNames;
		PerformanceCounter[] _netRecvCounters;	
		PerformanceCounter[] _netSentCounters;	
		#endregion
		#region Delegates
		public delegate void OnLogicalDiskProc(string s);
		#endregion
		#region Enums
		public enum DiskData {ReadAndWrite, Read, Write};
		public enum NetData {ReceivedAndSent, Received, Sent};
		public enum Unit {B, KB, MB, GB, ER}
		#endregion
		#region Constructor
		public SystemData()
		{
            try
            {
                PerformanceCounterCategory cat = new PerformanceCounterCategory("Network Interface");
                _instanceNames = cat.GetInstanceNames();
            }
            catch (System.Exception)
            {
                _instanceNames = new string[0];
            }
            if (_instanceNames != null && _instanceNames.Length > 0)
            {
                try
                {
                    _netRecvCounters = new PerformanceCounter[_instanceNames.Length];
                    for (int i = 0; i < _instanceNames.Length; i++)
                        _netRecvCounters[i] = new PerformanceCounter();

                    _netSentCounters = new PerformanceCounter[_instanceNames.Length];
                    for (int i = 0; i < _instanceNames.Length; i++)
                        _netSentCounters[i] = new PerformanceCounter();
                }
                catch (System.Exception)
                {
                    _netRecvCounters = new PerformanceCounter[0];
                    _netSentCounters = new PerformanceCounter[0];
                }
            }
            else
            {
                _netRecvCounters = new PerformanceCounter[0];
                _netSentCounters = new PerformanceCounter[0];
            }
		}
		#endregion
		#region Public Methods
		/// <summary>
		/// GetProcessorData()
		/// This function returns a CPU uages % value.
		/// </summary>
		public double GetProcessorData()
		{
			double dRet = 0;
			try
			{
				dRet = GetCounterValue(_cpuCounter, "Processor", "% Processor Time", "_Total");
			}
			catch(System.Exception)
			{
				dRet = 0;
			}
			return dRet;
		}

		/// <summary>
		/// GetMemoryVData()
		/// This function returns a double array of virtual memory usage values
		/// Result[0] = Commited bytes in use
		/// Result[1] = Commited bytes
		/// Result[2] = Commit Limit (Total Available)
		/// </summary>
		public double[] GetMemoryVData()
		{
			double[] dRet = new double[3];
			double dTemp = 0;

			try
			{				
				dTemp = GetCounterValue(_memoryCounter, "Memory", "% Committed Bytes In Use", null);				
				dRet[0] = dTemp;
				dTemp = GetCounterValue(_memoryCounter, "Memory", "Committed Bytes", null);
				dRet[1] = dTemp;
				dTemp = GetCounterValue(_memoryCounter, "Memory", "Commit Limit", null);
				dRet[2] = dTemp;
			}
			catch(System.Exception)
			{
				dRet = new double[3];
			}
			return dRet;
		}

		/// <summary>
		/// GetMemoryPData()
		/// This function returns a double array of physical memory usage values
		/// Result[0] = Bytes in use
		/// Result[1] = Bytes free
		/// Result[2] = Commit Limit (Total Available)
		/// </summary>
		public double[]  GetMemoryPData()
		{
			double[] dRet = new double[3];
			ArrayList oResults = null;

			try
			{
				dRet[1] = GetCounterValue(_memoryCounter, "Memory", "Available Bytes", null);
				oResults = QueryComputerSystem("totalphysicalmemory", "");
				if(oResults.Count > 0)
				{
					dRet[2] = Convert.ToDouble(oResults[0]);
					dRet[0] = dRet[2] - dRet[1];
				}
				else
				{
					dRet[2] = dRet[1];
					dRet[0] = dRet[1];
				}

			}
			catch(System.Exception)
			{
				dRet = new double[3];
			}
			return dRet;
		}

		/// <summary>
		/// GetDiskData()
		/// This function returns a double array of disk read/write bytes/sec values
		/// Result[0] = Disk Read Bytes/sec
		/// Result[1] = Disk Write Bytes/sec
		/// </summary>
		public double[] GetDiskData()
		{
			double[] dRet = new double[2];
			try
			{
				dRet[0] = GetCounterValue(_diskReadCounter, "PhysicalDisk", "Disk Read Bytes/sec", "_Total");
				dRet[1] = GetCounterValue(_diskWriteCounter, "PhysicalDisk", "Disk Write Bytes/sec", "_Total");
			}
			catch(System.Exception)
			{
				dRet = new double[2];
			}
			return dRet;
		}
		/// <summary>
		/// GetNetData()
		/// This function returns a double array of network read/write bytes/sec values.  The returned values are the sum of all network devices on the machine.
		/// Result[0] = Bytes Received/sec
		/// Result[1] = Bytes Sent/sec
		/// </summary>
		public double[] GetNetData()
		{
			double[] dRet = new double[2];

			try
			{
                if (_instanceNames != null && _instanceNames.Length != 0) 
				{
					for(int X =0 ; X < _instanceNames.Length; X++)
					{
						dRet[0] += GetCounterValue(_netRecvCounters[X], "Network Interface", "Bytes Received/sec", _instanceNames[X]);
						dRet[1] += GetCounterValue(_netSentCounters[X], "Network Interface", "Bytes Sent/sec", _instanceNames[X]);
					}
				}
			}
			catch(System.Exception)
			{
				dRet = new double[2];
			}
			return dRet;
		}

		/// <summary>
		/// FormatBytes(double dBytes)
		/// This function returns a string converting the number of bytes in to the most appropraite format (B, KB, MB, GB, ER)
		/// Result[0] = Bytes Received/sec
		/// Result[1] = Bytes Sent/sec
		/// </summary>
		public string FormatBytes(double dBytes)
		{
			int iUnit = 0;
			string sRet = "";

			try
			{
				while (dBytes>1024) 
				{
					dBytes /= 1024;
					++iUnit;
				}
				sRet = ((int)dBytes).ToString() +" ";
				sRet = sRet +((Unit)iUnit).ToString();
			}
			catch(System.Exception)
			{
				sRet = Convert.ToString(dBytes) + " B";
			}
			return sRet;
		}

		#region General Win32 Value queries
		public ArrayList QueryWin32(string sField, string sClass, string sWhere)
		{
			ArrayList oResults = new ArrayList();
			ManagementObjectSearcher objCS = null;
			try
			{
				if(sWhere != "")
					objCS = new ManagementObjectSearcher("SELECT * FROM " + sClass + " WHERE " + sWhere);
				else
					objCS = new ManagementObjectSearcher("SELECT * FROM " + sClass);

				foreach ( ManagementObject objMgmt in objCS.Get() )
				{
					oResults.Add(objMgmt[sField]);
				}
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		public ArrayList Query1394Controller(string sField, string sWhere)
		{
			//		class Win32_1394Controller : CIM_Controller
			//		{
			//			uint16 Availability;
			//			string Caption;
			//			uint32 ConfigManagerErrorCode;
			//			boolean ConfigManagerUserConfig;
			//			string CreationClassName;
			//			string Description;
			//			string DeviceID;
			//			boolean ErrorCleared;
			//			string ErrorDescription;
			//			datetime InstallDate;
			//			uint32 LastErrorCode;
			//			string Manufacturer;
			//			uint32 MaxNumberControlled;
			//			string Name;
			//			string PNPDeviceID;
			//			uint16 PowerManagementCapabilities[];
			//			boolean PowerManagementSupported;
			//			uint16 ProtocolSupported;
			//			string Status;
			//			uint16 StatusInfo;
			//			string SystemCreationClassName;
			//			string SystemName;
			//			datetime TimeOfLastReset;
			//		};

			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_1394Controller", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		public ArrayList Query1394ControllerDevice(string sField, string sWhere)
		{
//			class Win32_1394ControllerDevice : CIM_ControlledBy
//			{
//				uint16 AccessState;
//				Win32_1394Controller ref Antecedent;
//				CIM_LogicalDevice ref Dependent;
//				uint32 NegotiatedDataWidth;
//				uint64 NegotiatedSpeed;
//				uint32 NumberOfHardResets;
//				uint32 NumberOfSoftResets;
//			};

			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_1394ControllerDevice", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		public ArrayList QueryAccount(string sField, string sWhere)
		{
//			class Win32_Account : CIM_LogicalElement
//			{
//				string Caption;
//				string Description;
//				string Domain;
//				datetime InstallDate;
//				boolean LocalAccount;
//				string Name;
//				string SID;
//				uint8 SIDType;
//				string Status;
//			};

			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_Account", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		public ArrayList QueryAccountSID(string sField, string sWhere)
		{
//			class Win32_AccountSID
//			{
//				Win32_Account ref Element;
//				Win32_SID ref Setting;
//			};

			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_AccountSID", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryACE(string sField, string sWhere)
		{
//			class Win32_ACE : __ACE
//			{
//				uint32 AccessMask;
//				uint32 AceFlags;
//				uint32 AceType;
//				string GuidInheritedObjectType;
//				string GuidObjectType;
//				Win32_Trustee Trustee;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_ACE", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryActionCheck(string sField, string sWhere)
		{
//			class Win32_ActionCheck
//			{
//				CIM_Action Action;
//				CIM_Check Check;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_ActionCheck", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryActiveRoute(string sField, string sWhere)
		{
			//			class Win32_ActiveRoute : CIM_LogicalIdentity
			//			{
			//				Win32_IP4PersistedRouteTable ref SameElement;
			//				Win32_IP4RouteTable ref SystemElement;
			//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_ActiveRoute", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		public ArrayList QueryAllocatedResource(string sField, string sWhere)
		{
//			class Win32_AllocatedResource : CIM_Dependency
//			{
//				CIM_SystemResource ref Antecedent;
//				CIM_LogicalDevice ref Dependent;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_AllocatedResource", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryApplicationCommandLine(string sField, string sWhere)
		{
//			class Win32_ApplicationCommandLine : CIM_ServiceAccessBySAP
//			{
//				Win32_ApplicationService Antecedent;
//				Win32_CommandLineAccess Dependent;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_ApplicationCommandLine", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryApplicationService(string sField, string sWhere)
		{
//			class Win32_ApplicationService : CIM_Service
//			{
//				string Caption;
//				string CreationClassName;
//				string Description;
//				datetime InstallDate;
//				string Name;
//				boolean Started;
//				string StartMode;
//				string Status;
//				string SystemCreationClassName;
//				string SystemName;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_ApplicationService", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryAssociatedBattery(string sField, string sWhere)
		{
//			class Win32_AssociatedBattery : CIM_AssociatedBattery
//			{
//				CIM_Battery ref Antecedent;
//				CIM_LogicalDevice ref Dependent;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_AssociatedBattery", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryAssociatedProcessorMemory(string sField, string sWhere)
		{
//			class Win32_AssociatedProcessorMemory : CIM_AssociatedProcessorMemory
//			{
//				Win32_CacheMemory ref Antecedent;
//				uint32 BusSpeed;
//				Win32_Processor ref Dependent;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_AssociatedProcessorMemory", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryAutochkSetting(string sField, string sWhere)
		{
//			class Win32_AutochkSetting : CIM_Setting
//			{
//				string Caption;
//				string Description;
//				string SettingID;
//				uint32 UserInputDelay;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_AutochkSetting", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryBaseBoard(string sField, string sWhere)
		{
//			class Win32_BaseBoard : CIM_Card
//			{
//				string Caption;
//				string ConfigOptions[];
//				string CreationClassName;
//				real32 Depth;
//				string Description;
//				real32 Height;
//				boolean HostingBoard;
//				boolean HotSwappable;
//				datetime InstallDate;
//				string Manufacturer;
//				string Model;
//				string Name;
//				string OtherIdentifyingInfo;
//				string PartNumber;
//				boolean PoweredOn;
//				string Product;
//				boolean Removable;
//				boolean Replaceable;
//				string RequirementsDescription;
//				boolean RequiresDaughterBoard;
//				string SerialNumber;
//				string SKU;
//				string SlotLayout;
//				boolean SpecialRequirements;
//				string Status;
//				string Tag;
//				string Version;
//				real32 Weight;
//				real32 Width;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_BaseBoard", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		public ArrayList QueryBaseService(string sField, string sWhere)
		{
//			class Win32_BaseService : CIM_Service
//			{
//				boolean AcceptPause;
//				boolean AcceptStop;
//				string Caption;
//				string CreationClassName;
//				string Description;
//				boolean DesktopInteract;
//				string DisplayName;
//				string ErrorControl;
//				uint32 ExitCode;
//				datetime InstallDate;
//				string Name;
//				string PathName;
//				uint32 ServiceSpecificExitCode;
//				uint8 ServiceType;
//				boolean Started;
//				string StartMode;
//				string StartName;
//				string State;
//				string Status;
//				string SystemCreationClassName;
//				string SystemName;
//				uint32 TagId;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_BaseService", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryBattery(string sField, string sWhere)
		{
//			class Win32_Battery : CIM_Battery
//			{
//				uint16 Availability;
//				uint32 BatteryRechargeTime;
//				uint16 BatteryStatus;
//				string Caption;
//				uint16 Chemistry;
//				uint32 ConfigManagerErrorCode;
//				boolean ConfigManagerUserConfig;
//				string CreationClassName;
//				string Description;
//				uint32 DesignCapacity;
//				uint64 DesignVoltage;
//				string DeviceID;
//				boolean ErrorCleared;
//				string ErrorDescription;
//				uint16 EstimatedChargeRemaining;
//				uint32 EstimatedRunTime;
//				uint32 ExpectedBatteryLife;
//				uint32 ExpectedLife;
//				uint32 FullChargeCapacity;
//				datetime InstallDate;
//				uint32 LastErrorCode;
//				uint32 MaxRechargeTime;
//				string Name;
//				string PNPDeviceID;
//				uint16 PowerManagementCapabilities[];
//				boolean PowerManagementSupported;
//				string SmartBatteryVersion;
//				string Status;
//				uint16 StatusInfo;
//				string SystemCreationClassName;
//				string SystemName;
//				uint32 TimeOnBattery;
//				uint32 TimeToFullCharge;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_Battery", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryBinary(string sField, string sWhere)
		{
//			class Win32_Binary : Win32_MSIResource
//			{
//				string Caption;
//				string Data;
//				string Description;
//				string Name;
//				string ProductCode;
//				string SettingID;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_Binary", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryBindImageAction(string sField, string sWhere)
		{
//			class Win32_BindImageAction : CIM_Action
//			{
//				string ActionID;
//				string Caption;
//				string Description;
//				uint16 Direction;
//				string File;
//				string Name;
//				string Path;
//				string SoftwareElementID;
//				uint16 SoftwareElementState;
//				uint16 TargetOperatingSystem;
//				string Version;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_BindImageAction", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		
		public ArrayList QueryComputerSystem(string sField, string sWhere)
		{
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_ComputerSystem", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}
		public ArrayList QueryOperatingSystem(string sField, string sWhere)
		{
//			class Win32_OperatingSystem : CIM_OperatingSystem
//			{
//				string BootDevice;
//				string BuildNumber;
//				string BuildType;
//				string Caption;
//				string CodeSet;
//				string CountryCode;
//				string CreationClassName;
//				string CSCreationClassName;
//				string CSDVersion;
//				string CSName;
//				sint16 CurrentTimeZone;
//				boolean DataExecutionPrevention_Available;
//				boolean DataExecutionPrevention_32BitApplications;
//				boolean DataExecutionPrevention_Drivers;
//				uint8 DataExecutionPrevention_SupportPolicy;
//				boolean Debug;
//				string Description;
//				boolean Distributed;
//				uint32 EncryptionLevel;
//				uint8 ForegroundApplicationBoost;
//				uint64 FreePhysicalMemory;
//				uint64 FreeSpaceInPagingFiles;
//				uint64 FreeVirtualMemory;
//				datetime InstallDate;
//				uint32 LargeSystemCache;
//				datetime LastBootUpTime;
//				datetime LocalDateTime;
//				string Locale;
//				string Manufacturer;
//				uint32 MaxNumberOfProcesses;
//				uint64 MaxProcessMemorySize;
//				string MUILanguages[];
//				string Name;
//				uint32 NumberOfLicensedUsers;
//				uint32 NumberOfProcesses;
//				uint32 NumberOfUsers;
//				uint32 OperatingSystemSKU;
//				string Organization;
//				string OSArchitecture;
//				uint32 OSLanguage;
//				uint32 OSProductSuite;
//				uint16 OSType;
//				string OtherTypeDescription;
//				Boolean PAEEnabled;
//				string PlusProductID;
//				string PlusVersionNumber;
//				boolean Primary;
//				uint32 ProductType;
//				uint8 QuantumLength;
//				uint8 QuantumType;
//				string RegisteredUser;
//				string SerialNumber;
//				uint16 ServicePackMajorVersion;
//				uint16 ServicePackMinorVersion;
//				uint64 SizeStoredInPagingFiles;
//				string Status;
//				uint32 SuiteMask;
//				string SystemDevice;
//				string SystemDirectory;
//				string SystemDrive;
//				uint64 TotalSwapSpaceSize;
//				uint64 TotalVirtualMemorySize;
//				uint64 TotalVisibleMemorySize;
//				string Version;
//				string WindowsDirectory;
//			};
			ArrayList oResults = new ArrayList();
			try
			{
				oResults = QueryWin32(sField, "Win32_OperatingSystem", sWhere);
			}
			catch(System.Exception)
			{
				oResults = new ArrayList();
			}
			return oResults;
		}

		#endregion

		public string QueryEnvironment(string type)
		{
            string sRet = "";
            try
            {
                sRet = Environment.ExpandEnvironmentVariables(type);
            }
            catch (System.Exception)
            {
                sRet = "";
            }
            return sRet;
		}

		/// <summary>
		/// LogicalDisk()
		/// This function returns an array, with each element in the array being a system drive.
		/// Each element of the array is an array object containing 3 entries, drive letter (string), free space in bytes (double) and total space in bytes.
		/// (ArrayList) Return Array
		///	|- (ArrayList) Return Array[0] 
		///	|       |- (string) Drive Letter (i.e. C:)
        ///	|       |- (ulong) Free space in bytes
        ///	|       |- (ulong) Total space in bytes
        ///	|       |- (ulong) Space Used
		///	|- (ArrayList) Return Array[1] 
		///	|       |- (string) Drive Letter (i.e. D:)
		///	|       |- (ulong) Free space in bytes
		///	|       |- (ulong) Total space in bytes
        ///	|       |- (ulong) Space Used
		/// </summary>

		public ArrayList LogicalDisk()
		{
			ArrayList oRet = new ArrayList();
			ArrayList oItem = null;

			try
			{
                System.IO.DriveInfo[] oDrives = System.IO.DriveInfo.GetDrives();
                foreach (System.IO.DriveInfo oDrive in oDrives)
                {
                    if (oDrive.DriveType == DriveType.Fixed)
                    {
                        oItem = new ArrayList();
                        oItem.Add(oDrive.Name.Replace("\\", ""));
                        oItem.Add(oDrive.AvailableFreeSpace);
                        oItem.Add(oDrive.TotalSize);
                        oItem.Add(oDrive.TotalSize - oDrive.AvailableFreeSpace);
                        oRet.Add(oItem);
                    }
                }
			}
			catch(System.Exception)
			{
			}
			return oRet;
		}
		#endregion
		#region Private Methods
		private double GetCounterValue(PerformanceCounter pc, string categoryName, string counterName, string instanceName)
		{
            double dRet = 0;
            try
            {
                pc.CategoryName = categoryName;
                pc.CounterName = counterName;
                pc.InstanceName = instanceName;
                dRet = pc.NextValue();
            }
            catch (System.Exception)
            {
                dRet = 0;
            }
            return dRet;
		}
		#endregion
	}
}

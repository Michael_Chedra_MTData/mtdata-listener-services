using System;
using MTData.Common.ContextProvider;
using MTData.Common.Interface;

namespace MTData.Common.DataAccess
{
    /// <summary>
    /// This is a class that allows access to the shared details accross a business logic layer, such as 
    /// provider implementations and settings.
    /// </summary>
    public class BusinessApplication
    {
        private BusinessApplication()
        {

        }

        private static Configuration.ProviderConfiguration _providerConfiguration = null;

        /// <summary>
        /// This property allows access to the provider configuration for the system.
        /// </summary>
        public static Configuration.ProviderConfiguration ProviderConfiguration
        {
            get
            {
                if (_providerConfiguration == null)
                    lock (typeof(BusinessApplication))
                    {
                        if (_providerConfiguration == null)
                        {
                            string providerPath = System.Configuration.ConfigurationManager.AppSettings["ProviderPath"];
                            if ((providerPath != null) && (providerPath.Trim().Length > 0))
                                _providerConfiguration = (Configuration.ProviderConfiguration)System.Configuration.ConfigurationManager.GetSection(providerPath);
                            else
                                _providerConfiguration = new Configuration.ProviderConfiguration();
                        }
                    }

                return _providerConfiguration;
            }
        }

        private static ContextProviderStandard _contextProvider = null;

        public static void RegisterContextInformation(string name, object value)
        {
            ContextProviderStandard contextProvider = GetContextProvider();
            contextProvider.Register(name, value);
        }

        private static ContextProviderStandard GetContextProvider()
        {
            if (_contextProvider == null)
            {
                lock (typeof(BusinessApplication))
                {
                    if (_contextProvider == null)
                    {
                        _contextProvider = new ContextProviderStandard();

                        string settingsPath = System.Configuration.ConfigurationManager.AppSettings["SettingsPath"];
                        if ((settingsPath != null) && (settingsPath.Trim().Length > 0))
                            _contextProvider.Register("Settings", (IContextProvider)System.Configuration.ConfigurationManager.GetSection(settingsPath));
                        else
                            _contextProvider.Register("Settings", new ContextProviderStandard());
                    }
                }
            }
            return _contextProvider;
        }

        public static IContextProvider ContextProvider
        {
            get
            {
                return GetContextProvider();
            }
        }
    }
}

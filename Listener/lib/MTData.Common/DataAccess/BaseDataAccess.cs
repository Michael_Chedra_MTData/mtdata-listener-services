using System;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Common.DataAccess
{
    /// <summary>
    /// Summary description for BaseDataAccess.
    /// </summary>
    public class BaseDataAccess : IDisposable
    {
        private SqlConnection _connection;
        private SqlTransaction _transaction;
        private bool disposed = false;

        public BaseDataAccess()
        {
        }

        public SqlConnection Connection
        {
            get { return _connection; }
            set { _connection = value; }
        }

        public SqlTransaction Transaction
        {
            get { return _transaction; }
            set { _transaction = value; }
        }

        protected SqlCommand ConfigureStoredProc(SqlCommand command)
        {
            command.CommandType = CommandType.StoredProcedure;
            command.Connection = _connection;
            command.Transaction = _transaction;
            return command;
        }

        protected SqlCommand ConfigureText(SqlCommand command)
        {
            command.CommandType = CommandType.Text;
            command.Connection = _connection;
            command.Transaction = _transaction;
            return command;
        }

        public void Dispose()
        {
            // Dispose of unmanaged resources.
            Dispose(true);
            // Suppress finalization.
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                try { _transaction?.Dispose(); } catch { }
                try { _connection?.Dispose(); } catch { }
            }

            disposed = true;
        }
    }
}

using System;
using System.Xml;
using System.Configuration;

namespace MTData.Common.DataAccess.Configuration
{
	/// <summary>
	/// This class will load the required details for the active provider.
	/// </summary>
	public class ProviderHandler : IConfigurationSectionHandler
	{
		public ProviderHandler()
		{
			
		}

		#region IConfigurationSectionHandler Members

		/// <summary>
		/// This method will load and parse the relevant details for the active provider
		/// </summary>
		/// <param name="parent"></param>
		/// <param name="configContext"></param>
		/// <param name="section"></param>
		/// <returns></returns>
		public object Create(object parent, object configContext, System.Xml.XmlNode section)
		{
			ProviderConfiguration configuration = new ProviderConfiguration();

			XmlNodeList providerNodes = section.SelectNodes("Provider");
			foreach(XmlNode providerNode in providerNodes)
			{
				Type interfaceType = null;

				XmlNode interfaceNode = providerNode.Attributes.GetNamedItem("interface");
				if (interfaceNode != null)
					interfaceType = Type.GetType(interfaceNode.Value);

				Type implementationType = null;

				XmlNode implementationNode = providerNode.Attributes.GetNamedItem("type");
				if (implementationNode != null)
					implementationType = Type.GetType(implementationNode.Value);
	
				if ((interfaceType != null) && (implementationType != null))
					configuration.AddImplementation(interfaceType, implementationType);
			}

			return configuration;
		}

		#endregion
	}
}

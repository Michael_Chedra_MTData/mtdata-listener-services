using System;
using System.Collections;

namespace MTData.Common.DataAccess.Configuration
{
	/// <summary>
	/// This class will contain the details describing the configured provider.
	/// </summary>
	public class ProviderConfiguration
	{
		#region Override Link

		/// <summary>
		/// Internal hashtable object
		/// </summary>
		private class InterfaceImplementation
		{
			public InterfaceImplementation(Type interfaceType, Type implementationType)
			{
				_interfaceType = interfaceType;
				_implementationType = implementationType;
			}

			private Type _interfaceType; 
			private Type _implementationType;

			public Type InterfaceType
			{
				get
				{
					return _interfaceType;
				}
				set
				{
					_interfaceType = value; 
				}
			}

			public Type ImplementationType
			{
				get
				{
					return _implementationType;
				}
				set
				{
					_implementationType = value;
				}
			}
		}

		#endregion

		private Hashtable _interfaceOverrides = new Hashtable();

		public ProviderConfiguration()
		{
			
		}

		public void AddImplementation(Type interfaceType, Type implementationType)
		{
			InterfaceImplementation link = (InterfaceImplementation)_interfaceOverrides[interfaceType];
			if (link == null)
				_interfaceOverrides.Add(interfaceType, new InterfaceImplementation(interfaceType, implementationType));
			else
				link.ImplementationType = implementationType;
		}

		/// <summary>
		/// REturn the implementation available
		/// </summary>
		/// <param name="interfaceType"></param>
		/// <returns></returns>
		public Type GetImplementation(Type interfaceType)
		{
			InterfaceImplementation link = (InterfaceImplementation)_interfaceOverrides[interfaceType];
			if (link != null)
				return link.ImplementationType;
			else
				return null;
		}
	}
}

using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Common.DataAccess
{
	/// <summary>
	/// A simple Sql Data Adapter that does not require all Insert, Update & Delete commands to be set in order to work
	/// The .NET SqlDataAdapter will throw a exception if it comes across a Deleted row but does not have an DeleteCommand configured.
	/// The normal work around for this is to use the GetChanges(DataRowState.Added | DataRowState.Modified) method to return
	/// a subset of the changes required, however this is problematic since changes made during an Insert are then not reflected back to the
	/// original DataTable, only to the temporary GetChanges() DataTable.
	/// The OptimizedSqlDataAdapter allows all Delete operations on 1 or more relationed tables (Child/Parent) to be performed
	/// followed by all Insert/Update operations on the same Table instances.
	/// </summary>
	public class OptimizedSqlDataAdapter 
	{
        //public interface IObjectProvider
        //{
        //    object Provide(DataRow row, string columnName);
        //}

		public class StdObjectProvider //: IObjectProvider
		{
			public object Value;

			public StdObjectProvider(object value)
			{
				Value = value;
			}

			public object Provide(DataRow row, string columnName)
			{
				return Value;
			}
		}

		public class ColumnConverter
		{
			protected DataColumn	_column;

			public ColumnConverter(DataColumn column)
			{
				_column		= column;
			}

			public ColumnConverter()
			{
			}

			public virtual void Load(DataRow row, int ordinal, SqlDataReader reader)
			{
				object value = ConvertTo(reader.GetValue(ordinal));
				if (_column.ReadOnly)
				{
					_column.ReadOnly	= false;
					row[_column]		= value;
					_column.ReadOnly	= true;
				}
				else
					row[_column]		= value;
			}

			public virtual object ConvertTo(object value)
			{
				return value;
			}

			public virtual object ConvertFrom(object value)
			{
				return value;
			}
		}

		public class ColumnConverterFactory
		{
			public virtual ColumnConverter CreateForColumn(DataColumn column)
			{
				return new ColumnConverter(column);
			}

			public virtual ColumnConverter Create()
			{
				return new ColumnConverter();
			}
		}

		public class DateTimeToDoubleConverter : ColumnConverter
		{
			public DateTimeToDoubleConverter(DataColumn column) : base(column)
			{
			}

			public DateTimeToDoubleConverter()
			{
			}

			public override object ConvertTo(object value)
			{
				if (value == DBNull.Value)
					return value;
				return Convert.ToDateTime(value).ToOADate();
			}

			public override object ConvertFrom(object value)
			{
				if (value == DBNull.Value)
					return value;

				return DateTime.FromOADate(Convert.ToDouble(value));
			}
		}

		public class DateTimeToDoubleConverterFactory : ColumnConverterFactory
		{
			public override ColumnConverter CreateForColumn(DataColumn column)
			{
				return new DateTimeToDoubleConverter(column);
			}

			public override ColumnConverter Create()
			{
				return new DateTimeToDoubleConverter();
			}

        }

		private static ColumnConverterFactory	DefaultColumnConverterFactory = new ColumnConverterFactory();

		private SqlCommand			_insertCommand;
		private ColumnConverter[]	_insertCommandColumnConverters;
		private ColumnConverter[]	_insertUpdateCommandColumnConverters;

		private SqlCommand			_updateCommand;
		private ColumnConverter[]	_updateCommandColumnConverters;
		
		private SqlCommand			_deleteCommand;
		private ColumnConverter[]	_deleteCommandColumnConverters;
		
		private SqlCommand			_selectCommand;

		private Hashtable			_overrideObjectProviders = new Hashtable();
		private Hashtable			_columnConverterFactories = new Hashtable();

		public OptimizedSqlDataAdapter()
		{
		}

		public OptimizedSqlDataAdapter(SqlCommand selectCommand)
		{
			_selectCommand = selectCommand;
		}

        public void AddParameterOverride(string parameterName, StdObjectProvider objectProvider)
		{
			_overrideObjectProviders[parameterName] = objectProvider;
		}

		public void AddParameterOverride(string parameterName, object value)
		{
			AddParameterOverride(parameterName, new StdObjectProvider(value));
		}

		public void AddColumnLoaderFactory(string columnName, ColumnConverterFactory columnConverterFactory)
		{
			_columnConverterFactories.Add(columnName, columnConverterFactory);
		}

		public SqlCommand InsertCommand
		{
			get { return _insertCommand; }
			set
			{
				_insertCommand					= value;
				_insertCommandColumnConverters	= CreateColumnConverters(_insertCommand);
			}
		}

		public SqlCommand UpdateCommand
		{
			get { return _updateCommand; }
			set
			{
				_updateCommand = value;
				_updateCommandColumnConverters	= CreateColumnConverters(_updateCommand);
			}
		}

		public SqlCommand DeleteCommand
		{
			get { return _deleteCommand; }
			set
			{
				_deleteCommand = value;
				_deleteCommandColumnConverters	= CreateColumnConverters(_deleteCommand);
			}
		}
	
		public int Update(DataTable changes)
		{
			int affected = 0;
			foreach(DataRow row in changes.Rows)
			{
				if (row.RowState == DataRowState.Deleted) 
					affected += _deleteRow(row);
				else if (row.RowState == DataRowState.Added)
					affected += _insertRow(row);
				else if (row.RowState == DataRowState.Modified)
					affected += _updateRow(row);
			}
			return affected;
		}

		public int Update(DataRow row)
		{
			int affected = 0;
			if (row.RowState == DataRowState.Deleted) 
				affected += _deleteRow(row);
			else if (row.RowState == DataRowState.Added)
				affected += _insertRow(row);
			else if (row.RowState == DataRowState.Modified)
				affected += _updateRow(row);
			return affected;
		}

		private int _updateRow(DataRow row)
		{
			if (_updateCommand == null)
				return 0;

			PrepareCommand(_updateCommand, row, DataRowVersion.Current, _overrideObjectProviders, _updateCommandColumnConverters);

			return _updateCommand.ExecuteNonQuery();
		}

		public static void PrepareCommand(SqlCommand cmd, DataRow row, DataRowVersion version)
		{
			PrepareCommand(cmd, row, version, null, null);			
		}

		public static void PrepareCommand(SqlCommand cmd, DataRow row, DataRowVersion version, Hashtable overrideParameterValues, ColumnConverter[] columnConverters)
		{
			for(int i = 0 ; i < cmd.Parameters.Count ; i++)
			{
				SqlParameter parameter			= cmd.Parameters[i];
				object value					= null;
				
				if (overrideParameterValues != null)
				{
                    StdObjectProvider objectProvider = (StdObjectProvider)overrideParameterValues[parameter.ParameterName];
					if (objectProvider != null)
						value = objectProvider.Provide(row, parameter.SourceColumn);
				}

				if (value == null)
					value = row[parameter.SourceColumn, version];
				
				if (columnConverters != null)
					value = columnConverters[i].ConvertFrom(value);

				parameter.Value = value;					
			}
		}

		private int _insertRow(DataRow row)
		{
			if (_insertCommand == null)
				return 0;
			
			PrepareCommand(_insertCommand, row, DataRowVersion.Current, _overrideObjectProviders, _insertCommandColumnConverters);

			SqlDataReader reader = _insertCommand.ExecuteReader();
			try
			{
				// Create the Column Converters once for the 1st record inserted ..
				if (_insertUpdateCommandColumnConverters == null)
					_insertUpdateCommandColumnConverters = CreateColumnConverters(reader, row.Table.Columns);

				if (reader.Read())
				{
					for(int i = 0 ; i < reader.FieldCount ; i++)
					{
						ColumnConverter columnConverter = _insertUpdateCommandColumnConverters[i];
						if (columnConverter != null)
							columnConverter.Load(row, i, reader);
					}
				}
			}
			finally
			{
				reader.Close();
			}
			return 1;
		}

		private int _deleteRow(DataRow row)
		{
			if (_deleteCommand == null)
				return 0;
			
			PrepareCommand(_deleteCommand, row, DataRowVersion.Original, _overrideObjectProviders, _deleteCommandColumnConverters);

			return _deleteCommand.ExecuteNonQuery();
		}

		public int Fill(DataTable dataTable)
		{
			SqlDataReader reader = _selectCommand.ExecuteReader();

			try
			{
				dataTable.BeginLoadData();
				try
				{
					return _Fill(reader, dataTable);
				}
				finally
				{
					dataTable.EndLoadData();
				}
			}
			finally
			{
				reader.Close();
			}
		}

		private int _Fill(SqlDataReader reader, DataTable dataTable)
		{
			int affectedRecords					= 0;
			ColumnConverter[] columnConverters	= CreateColumnConverters(reader, dataTable.Columns);

			while (reader.Read())
			{
				DataRow row = dataTable.NewRow();
				for(int i = 0 ; i < reader.FieldCount ; i++)
				{
					ColumnConverter columnConverter = columnConverters[i];
					if (columnConverter != null)
						columnConverter.Load(row, i, reader);
				}
				dataTable.Rows.Add(row);
				row.AcceptChanges();
				affectedRecords++;
			}

			return affectedRecords;
		}

		private ColumnConverter[] CreateColumnConverters(SqlDataReader reader, DataColumnCollection columns)
		{
			// Prepare ColumnLoaders and use any preconfigured ColumnLoaders
			ColumnConverter[] columnConverters = new ColumnConverter[reader.FieldCount];
			for(int i = 0 ; i < columnConverters.Length ; i++)
			{
				ColumnConverterFactory columnConverterFactory = null;
				string readerColumnName					= reader.GetName(i);
				DataColumn column						= columns[readerColumnName];
				if (column == null)
					continue;

				if (_columnConverterFactories != null)
					columnConverterFactory = (ColumnConverterFactory) _columnConverterFactories[readerColumnName];

				if (columnConverterFactory == null)
					columnConverterFactory = DefaultColumnConverterFactory;

				if (columnConverterFactory != null)
					columnConverters[i] = columnConverterFactory.CreateForColumn(column);
			}
			return columnConverters;
		}

		private ColumnConverter[] CreateColumnConverters(SqlCommand command)
		{
			if (command == null)
				return null;

			ColumnConverter[] columnConverters = new ColumnConverter[command.Parameters.Count];
			for(int i = 0 ; i < columnConverters.Length ; i++)
			{
				SqlParameter parameter							= command.Parameters[i];
				ColumnConverterFactory columnConverterFactory	= (ColumnConverterFactory) _columnConverterFactories[parameter.SourceColumn];
				if (columnConverterFactory == null)
					columnConverterFactory = DefaultColumnConverterFactory;
				columnConverters[i] = columnConverterFactory.Create();
			}

			return columnConverters;
		}
	}
}

using System;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Common.DataAccess
{
    /// <summary>
    /// Summary description for BaseBusinessLogic.
    /// </summary>
    public class BaseBusinessLogic : IDisposable
    {
        #region Connection Management

        protected SqlConnection _connection;
        protected string _dsn;
        protected long _auditUserID;
        private bool disposed = false;

        public BaseBusinessLogic(string dsn)
        {
            _dsn = dsn;
        }

        public string DSN
        {
            get { return _dsn; }
            set { _dsn = value; }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        // Protected implementation of Dispose pattern.
        protected virtual void Dispose(bool disposing)
        {
            if (disposed)
                return;

            if (disposing)
            {
                try { _connection?.Dispose(); } catch { }
            }

            disposed = true;
        }

        protected BaseDataAccess _Open(BaseDataAccess DAL, Int64 auditUserID = 0)
        {
            if (_connection == null)
                _connection = new SqlConnection(_dsn);

            if (_connection.State == ConnectionState.Closed)
                _connection.Open();

            DAL.Connection = _connection;

            SetContextInfo(_connection, auditUserID);

            return DAL;
        }

        /// <summary>
        /// Attempts to set context_info to the current connection if the user is 
        /// logged in to our application.
        /// </summary>
        private static void SetContextInfo(SqlConnection dbcon, Int64 auditUserID)
        {
            if (auditUserID > 0 && dbcon != null && dbcon.State == ConnectionState.Open)
            {
                using (SqlCommand command = new SqlCommand(String.Format("SET CONTEXT_INFO 0x{0:X}", auditUserID), dbcon))
                {
                    command.CommandType = CommandType.Text;
                    command.ExecuteNonQuery();
                }
            }
        }


        #endregion

        #region Provider Overrides

        /// <summary>
        /// Return the implementation of the requested interface type
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <param name="defaultType"></param>
        /// <returns></returns>
        protected Type GetDALType(Type interfaceType, Type defaultType)
        {
            Type type = BusinessApplication.ProviderConfiguration.GetImplementation(interfaceType);
            if (type == null)
                type = defaultType;
            return type;
        }

        /// <summary>
        /// REturn a configured instance of the DAL type taking any overrides into consideration
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <param name="defaultType"></param>
        /// <returns></returns>
        protected object GetDALInstance(Type interfaceType, Type defaultType)
        {
            return GetDALInstance(interfaceType, defaultType, -1);
        }

        /// <summary>
        /// REturn a configured instance of the DAL type taking any overrides into consideration
        /// </summary>
        /// <param name="interfaceType"></param>
        /// <param name="defaultType"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        protected object GetDALInstance(Type interfaceType, Type defaultType, long auditUserID)
        {
            Type type = GetDALType(interfaceType, defaultType);
            return GetDALInstance(type, auditUserID);
        }

        /// <summary>
        /// REturn a configured instance of the DAL class
        /// </summary>
        /// <param name="type"></param>
        /// <returns></returns>
        protected object GetDALInstance(Type type)
        {
            return GetDALInstance(type, -1);
        }

        /// <summary>
        /// REturn a configured instance of the DAL class
        /// </summary>
        /// <param name="type"></param>
        /// <param name="userID"></param>
        /// <returns></returns>
        protected object GetDALInstance(Type type, long auditUserID)
        {
            object instance = Activator.CreateInstance(type);
            if (instance is MTData.Common.Interface.DataAccess.IAssignableDSN)
                ((MTData.Common.Interface.DataAccess.IAssignableDSN)instance).DSN = _dsn;

            if ((auditUserID != -1) && (instance is MTData.Common.Interface.DataAccess.IAssignableUserID))
                ((MTData.Common.Interface.DataAccess.IAssignableUserID)instance).UserID = (int)auditUserID;

            if (instance is MTData.Common.Interface.DataAccess.IContextConfigurableProvider)
                ((MTData.Common.Interface.DataAccess.IContextConfigurableProvider)instance).Configure(BusinessApplication.ContextProvider);

            _auditUserID = auditUserID;

            return instance;
        }

        /// <summary>
        /// Release the resources fo the DAL instance
        /// </summary>
        /// <param name="instance"></param>
        protected void ReleaseDALInstance(object instance)
        {
            if (instance is IDisposable)
                ((IDisposable)instance).Dispose();
        }

        #endregion
    }
}

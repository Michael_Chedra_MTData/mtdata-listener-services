using log4net;
using MTData.Common.Threading;
using System.Threading;
using MTData.Common.Queues;

namespace MTData.Common.Network
{
	/// <summary>
	/// Summary description for UDPConnection.
	/// </summary>
	public class UDPConnection
	{
        private const string sClassName = "MTData.Network.UDPConnection.";
        private static ILog _log = LogManager.GetLogger(typeof(UDPConnection));

		#region Private Vars
		private bool bRunning = false;
		private object SyncRoot = null;
		private string _toAddress = "";
		private int _toPort = 0;
		private int _listenPort = 0;
		private UDPSender _Sender = null;
		private UDPListener _Listener = null;
		private StandardQueue _toUDPQ = null;
		private StandardQueue _fromUDPQ = null;
		private EnhancedThread _SendThread = null;
		#endregion

		public UDPConnection(string toAddress, int toPort, int listenPort, StandardQueue toUDPQ, StandardQueue fromUDPQ)
		{
			try
			{
				SyncRoot = new object();
				_toAddress = toAddress;
				_toPort = toPort;
				_listenPort = listenPort;
				_toUDPQ = toUDPQ;
				_fromUDPQ = fromUDPQ;
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "UDPConnection(string toAddress, int toPort, int listenPort, StandardQueue toUDPQ, StandardQueue fromUDPQ)", ex);
			}
		}

		#region IsRunning / Start / Stop / Dispose functions
		public bool IsRunning
		{
			get
			{
				bool bRet = false;
				try
				{
					lock(this.SyncRoot)
						bRet = bRunning;
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "IsRunning (Get)", ex);
				}
				return bRet;
			}
		}

		public void Start()
		{
			bool bStarted = false;
			try
			{
				// Clean up any existing objects
				Stop();
				if(_toPort == _listenPort && _toUDPQ != null && _fromUDPQ != null)
				{
					// If we are sending to the same port we are listening on..
					if (_toAddress != "" && _toPort > 0 && _toUDPQ != null && _listenPort > 0 && _fromUDPQ != null)
					{
						_Listener = new UDPListener(_listenPort, _fromUDPQ, _toUDPQ);
						bStarted = true;
					}

				}
				else
				{
					// If we are sending and recieving on different ports
					// If the parameters have been supplied, create the sender object and thread
					if (_toAddress != "" && _toPort > 0 && _toUDPQ != null)
					{
						_Sender = new UDPSender(_toAddress, _toPort);
						_SendThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
						_SendThread.Name = "UDP Client Sender Thread";
						_SendThread.Start();
						bStarted = true;
					}
					// If the parameters have been supplied, create the UDPListener object
					if (_listenPort > 0 && _fromUDPQ != null)
					{
						_Listener = new UDPListener(_listenPort, _fromUDPQ);
						bStarted = true;
					}
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "Start()", ex);
			}
			// If either the sender or listener was started
			if (bStarted)
			{
				lock(this.SyncRoot)
					bRunning = true;
			}
		}

		public void Stop()
		{
			try
			{
				// If the send thread is running, stop it
				if (_SendThread != null)
				{
					_SendThread.Stop();
					_SendThread = null;
				}
				// If the sender object exists, end it
				if(_Sender != null)
				{
					_Sender.Dispose();
					_Sender = null;
				}
				// If the listener object exists, end it
				if (_Listener != null)
				{
					_Listener.Stop();
					_Listener = null;
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "Stop()", ex);
			}

			// Nothing is now running.
			lock(this.SyncRoot)
				bRunning = false;
		}

		public void Dispose()
		{
			Stop();
			SyncRoot = null;
		}
		#endregion

		#region Send Thread Handler
		private object ThreadHandler(EnhancedThread sender, object oData)
		{
			byte[] bData = null;
			try
			{
				while(!sender.Stopping)
				{
					lock(_toUDPQ.SyncRoot)
					{
						if (_toUDPQ.Count > 0)
							bData = (byte[]) _toUDPQ.Dequeue();
						else
							bData = null;
					}

					if (bData == null)
						Thread.Sleep(100);	
					else
					{
						_Sender.SendMsg(bData);
					}
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object oData)", ex);
			}
			return null;
		}
		#endregion
	}
}

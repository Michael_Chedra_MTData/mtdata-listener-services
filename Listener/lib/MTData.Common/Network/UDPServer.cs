using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;


namespace MTData.Common.Network
{
    public class UDPServer
    {
        private const string sClassName = "MTData.Network.UDPServer.";
        private static ILog _log = LogManager.GetLogger(typeof(UDPServer));
        private bool bThreadRunning = false;
		private bool bSendThreadRunning = false;
		private EnhancedThread _listener;
		private EnhancedThread _sender;
		private QueueInterface _fromRemoteIPQ = null;
		private QueueInterface _toRemoteIPQ = null;
		private ArrayList oClients = null;
        private ArrayList oClientLiveUpdateList = null;
        private Hashtable oClientToFleetRegistration = null;
		private int iPort = 0;
		private UdpClient _udpClient = null;

		public UDPServer(int Port, QueueInterface fromRemoteIPQ)
		{
			try
			{
                CreateObject(Port, fromRemoteIPQ, null, null, null, null);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ)", ex);
            }
		}
		public UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)
		{
			try
			{
                CreateObject(Port, fromRemoteIPQ, toRemoteIPQ, null, null, null);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
            }
		}
        public UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList)
        {
            try
            {
                CreateObject(Port, fromRemoteIPQ, toRemoteIPQ, clientList, null, null);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList)", ex);
                throw new System.Exception(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList)", ex);
            }
        }
        public UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList)
        {
            try
            {
                CreateObject(Port, fromRemoteIPQ, toRemoteIPQ, clientList, clientLiveUpdateList, null);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList)", ex);
                throw new System.Exception(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList)", ex);
            }
        }
        public UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList, Hashtable clientToFleetRegistration)
        {
            try
            {
                CreateObject(Port, fromRemoteIPQ, toRemoteIPQ, clientList, clientLiveUpdateList, clientToFleetRegistration);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList, Hashtable clientToFleetRegistration)", ex);
                throw new System.Exception(sClassName + "UDPServer(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList, Hashtable clientToFleetRegistration)", ex);
            }
        }

        private void CreateObject(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList, Hashtable clientToFleetRegistration)
		{
			try
			{
                if (clientList == null)
                    oClients = ArrayList.Synchronized(new ArrayList());
                else
                    oClients = clientList;
                if (clientLiveUpdateList == null)
                    oClientLiveUpdateList = ArrayList.Synchronized(new ArrayList());
                else
                    oClientLiveUpdateList = clientLiveUpdateList;
                if (clientToFleetRegistration == null)
                    oClientToFleetRegistration = Hashtable.Synchronized(new Hashtable());
                else
                    oClientToFleetRegistration = clientToFleetRegistration;

				iPort = Port;
				_udpClient = new UdpClient(iPort);
				_fromRemoteIPQ = fromRemoteIPQ;
				_toRemoteIPQ = toRemoteIPQ;
				if(_toRemoteIPQ != null)
				{
					_sender  = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(SendThreadHandler), _toRemoteIPQ);
					_sender.Name = "UDP.Sender.Thread";
					bSendThreadRunning = true;
					_sender.Start();
				}
				if(_fromRemoteIPQ != null)
				{
					_log.Info("Starting UDP listener on "  + Convert.ToString(Port) + ".");
					IPAddress oLocal = ((IPAddress[]) Dns.GetHostEntry("localhost").AddressList)[0];

					_listener = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReadThreadHandler), _fromRemoteIPQ);
					_listener.Name = "UDP.Listener.Thread";
					bThreadRunning = true;
					_listener.Start();
				}

				_log.Info("Listening for UDP on port "  + Convert.ToString(Port) + ".");
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "CreateObject(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList, Hashtable clientToFleetRegistration)", ex);
                throw new System.Exception(sClassName + "CreateObject(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ, ArrayList clientList, ArrayList clientLiveUpdateList, Hashtable clientToFleetRegistration)", ex);
			}
		}
		public void Stop()
		{
			bThreadRunning = false;
			bSendThreadRunning = false;
		}
		public object SendThreadHandler(EnhancedThread sender, object data)
		{
			QueueInterface outboundQ = (QueueInterface)data;
			byte[] bSendData = null;
			int iRet = 0;
			ArrayList oRemoveAt = new ArrayList();
            IPEndPoint oEP = null;

			while(bSendThreadRunning)
			{
				if (_udpClient != null)
				{
					lock(outboundQ.SyncRoot)
					{
						if (outboundQ.Count > 0)
							bSendData = (byte[]) outboundQ.Dequeue();
					}
                    if (bSendData != null)
                    {
                        try
                        {
                            #region Send the message to all the clients listed in oClientLiveUpdateList, record IPEndPoint of failed transmissions to oRemoveAt
                            oRemoveAt = new ArrayList();
                            lock (oClientLiveUpdateList.SyncRoot)
                            {
                                if (oClientLiveUpdateList.Count > 0)
                                {
                                    for (int X = 0; X < oClientLiveUpdateList.Count; X++)
                                    {
                                        try
                                        {
                                            oEP = (IPEndPoint)oClientLiveUpdateList[X];
                                            lock (_udpClient)
                                            {
                                                iRet = _udpClient.Send(bSendData, bSendData.Length, oEP);
                                            }
                                        }
                                        catch (System.Exception)
                                        {
                                            oRemoveAt.Add(oEP);
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region If there where any failed transmissions
                            if (oRemoveAt.Count > 0)
                            {
                                for (int X = oRemoveAt.Count - 1; X >= 0; X--)
                                {
                                    oEP = (IPEndPoint)oRemoveAt[X];
                                    lock (oClientLiveUpdateList.SyncRoot)
                                    {
                                        for (int Y = oClientLiveUpdateList.Count - 1; Y >= 0; Y--)
                                        {
                                            if(oEP.Equals((IPEndPoint) oClientLiveUpdateList[Y]))
                                            {
                                                oClientLiveUpdateList.RemoveAt(Y);
                                                break;
                                            }
                                        }
                                    }
                                    lock (oClients.SyncRoot)
                                    {
                                        for (int Y = oClients.Count - 1; Y >= 0; Y--)
                                        {
                                            if (oEP.Equals((IPEndPoint)oClients[Y]))
                                            {
                                                oClients.RemoveAt(Y);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            _log.Info(String.Format("UDP Send Thread Handler Error : {0}, [{1}]", ex.Message, ex.StackTrace));
                        }
                        bSendData = null;
                    }
                    else
                        Thread.Sleep(100);
				}
			}		

			_log.Info("Stopped sending on UDP on port "  + Convert.ToString(iPort) + ".");
			return null;
		}
		public object ReadThreadHandler(EnhancedThread sender, object data)
		{
			bool bAlreadyInList = false;
			byte[] receivedBytes = null;
			QueueInterface inboundQ = (QueueInterface)data;
            UDPServer_Message oQueueMsg = null;

			while(bThreadRunning)
			{
				if (_udpClient != null)
				{
					IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
					try
					{
						receivedBytes = _udpClient.Receive(ref remoteIP);
						bAlreadyInList = false;
						lock(oClients.SyncRoot)
						{
							for (int X = 0; X < oClients.Count; X++)
							{
								if(((IPEndPoint)oClients[X]).Equals(remoteIP))
								{
									bAlreadyInList = true;
									break;
								}
							}
							if(!bAlreadyInList)
								oClients.Add(remoteIP);
						}
                        
                        oQueueMsg = new UDPServer_Message(remoteIP, receivedBytes);
						lock(inboundQ.SyncRoot)
                            inboundQ.Enqueue(oQueueMsg);
					}
					catch(SocketException sockEx)
					{
						if (!sockEx.Message.StartsWith("A blocking operation was interrupted by a call to WSACancelBlockingCall"))
							_log.Info(String.Format("UDP Listener Error : {0}, [{1}]", sockEx.Message, sockEx.StackTrace));
					}
					catch(Exception ex)
					{
						_log.Info(String.Format("UDP Listener Error : {0}, [{1}]", ex.Message, ex.StackTrace));
					}
				}
			}		

			_log.Info("Stopped listening for UDP on port "  + Convert.ToString(iPort) + ".");
			return null;
		}
        public void RemoveClients(IPEndPoint[] oRemoveClients)
        {
            IPEndPoint oEP = null;
            IPEndPoint oRemoveEP = null;
            try
            {
                if (oRemoveClients != null)
                {
                    if (oRemoveClients.Length > 0)
                    {
                        for (int Y = 0; Y < oRemoveClients.Length; Y++)
                        {
                            oRemoveEP = (IPEndPoint)oRemoveClients[Y];
                            #region Remove the end point from the list of clients recieving live updates
                            lock (oClientLiveUpdateList.SyncRoot)
                            {

                                for (int X = oClientLiveUpdateList.Count - 1; X >= 0; X--)
                                {
                                    oEP = (IPEndPoint)oClientLiveUpdateList[X];
                                    if (oEP.Equals(oRemoveEP))
                                    {
                                        _log.Info("Removing client live update registration " + oEP.Address.ToString() + ":" + Convert.ToString(oEP.Port) + ".");
                                        oClientLiveUpdateList.RemoveAt(X);
                                        break;
                                    }
                                }
                            }
                            #endregion
                            #region Removed the client from the list of client connections
                            lock (oClients.SyncRoot)
                            {

                                for (int X = oClients.Count - 1; X >= 0; X--)
                                {
                                    oEP = (IPEndPoint)oClients[X];
                                    if (oEP.Equals(oRemoveEP))
                                    {
                                        oClients.RemoveAt(X);
                                        break;
                                    }
                                }
                            }
                            #endregion
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RemoveClients(IPEndPoint[] oRemoveClients)", ex);
                throw new System.Exception(sClassName + "RemoveClients(IPEndPoint[] oRemoveClients)", ex);
            }
        }

        /// <summary>
        /// Send data direct to a given endpoint
        /// </summary>
        /// <param name="data">the data to send</param>
        /// <param name="endPoint">the end point to send the data to</param>
        public void SendDirect(byte[] data, IPEndPoint endPoint)
        {
            try
            {
                if (_udpClient != null && data != null)
                {
                    lock (_udpClient)
                    {
                        _udpClient.Send(data, data.Length, endPoint);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "SendDirect(byte[] data, IPEndPoint endPoint)", ex);
                throw new System.Exception(sClassName + "SendDirect(byte[] data, IPEndPoint endPoint)", ex);
            }
        }
        
    }
}

using System;
using System.Threading;

namespace MTData.Network
{
	/// <summary>
	/// Summary description for EnhancedThread.
	/// </summary>
	public class EnhancedThread
	{
		public delegate object EnhancedThreadStart(EnhancedThread sender, object data);

		private object SyncRoot = new object();
		private object _data = null;
		private object _result = null;
		private bool _stopping = false;
		private bool _running = false;
		private event EnhancedThreadStart _threadStart;
		private Thread _internalThread;

		public EnhancedThread(EnhancedThreadStart threadStart, object data)
		{
			_threadStart += threadStart;
			_data = data;
			_internalThread = new Thread(new System.Threading.ThreadStart(ThreadHandler));
		}

		public string Name
		{
			get
			{
				return _internalThread.Name;
			}
			set
			{
				_internalThread.Name = value;
			}
		}

		public void Start()
		{
			lock(SyncRoot)
			{
				if ((!_running) && (!_stopping))
				{
					_running = true;
					_stopping = false;
					_internalThread.Start();
				}
			}
		}

		public void Stop()
		{
			lock(SyncRoot)
				if (_running)
					_stopping = true;
		}

		public void Abort()
		{
			lock(SyncRoot)
			{
				if (_running)
					_internalThread.Abort();
				_stopping = true;
			}
		}

		private void ThreadHandler()
		{
			object internalResult = null;
			try
			{
				if (_threadStart != null)
					internalResult = _threadStart(this, _data);
			}
			finally
			{
				lock(SyncRoot)
				{
					_result = internalResult;
					_running = false;
					_stopping = false;
				}
			}
		}

		public object Result
		{
			get
			{
				lock(SyncRoot)
					return _result;
			}
		}

		public bool Running
		{
			get
			{
				lock(SyncRoot)
					return _running;
			}
		}

		public bool Stopping
		{
			get
			{
				lock(SyncRoot)
					return _stopping;
			}
		}
	}
}

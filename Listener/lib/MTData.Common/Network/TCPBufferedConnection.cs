using System.Net.Sockets;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Queues;

namespace MTData.Common.Network
{
	/// <summary>
	/// Summary description for TCPBufferedConnection.
	/// </summary>
	public class TCPBufferedConnection
	{
        private const string sClassName = "MTData.Network.TCPBufferedConnection.";
        private static ILog _log = LogManager.GetLogger(typeof(TCPBufferedConnection));
		#region Public Interface
		// Close Event
		public delegate void ConnectionClosedHandler(TCPConnection sender);
		#endregion

		#region Private Vars
		// Event Delegate
		private event ConnectionClosedHandler _connectionClosed;
		// Private Vars
		private TCPConnection _client = null;
		private QueueInterface _fromRemoteIPQ = null;
		private StandardQueue _fromRemoteIPRawQ = null;
		private QueueInterface _toRemoteIPQ = null;
		private CircBuffer _cbuffer = null;
		private string _description = "";
		private int _iFixedPacketLength = 0;
		#endregion

		#region Constructor / Close
		// Standard Constructor
		private void StandardConstructor(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize,
			string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose, byte SOP, byte EOP, int iLengthBytes, bool bFixedLength, 
			int iFixedPacketLength, byte[] bLookForTheseBytes, int iAdditionalHeaderBytes)
		{
			// Setup the local vars
			_fromRemoteIPQ = fromRemoteIPQ;
			_toRemoteIPQ = toRemoteIPQ;
			_connectionClosed += connectionClosed;
			_description = description;
			_fromRemoteIPRawQ = new StandardQueue();
			_iFixedPacketLength = iFixedPacketLength;

			// Create the new TCP connection object
			_client = new TCPConnection(client, _description, _fromRemoteIPRawQ, _toRemoteIPQ, new TCPConnection.ConnectionClosedHandler(client_connectionclosed), packetSize, packetWaitPeriod, sleepWaitPeriod, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose);
			_cbuffer = new CircBuffer(_description, bufferSize, _client, _fromRemoteIPQ, SOP, EOP, iLengthBytes, bFixedLength, iFixedPacketLength, bLookForTheseBytes, iAdditionalHeaderBytes);
		}

		#region Constructor overides
		// This is a MTData default connection i.e. packet = [0x02][Length 1][Length 2][Data][0x03]
		public TCPBufferedConnection(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize)
		{
			StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, bufferSize, "", false, false, (byte) 0x02, (byte) 0x03, 0, false, 0, null, 0);
		}
		// This is a MTData default connection i.e. packet = [0x02][Length 1][Length 2][Data][0x03]
		// however the TCP connection looks for a disconnect string to disconnect from the other end.
		public TCPBufferedConnection(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize,
			string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose)
		{
			StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, bufferSize, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose, (byte) 0x02, (byte) 0x03, 0, false, 0, null, 0);
		}
		// If bFixed length is true
		// then we are looinking for a packet iLengthBytes long with a SOP and EOP marker
		// else we are looking for a packet with SOP, iLengthBytes bytes of length, the data then EOP - i.e. [SOP][Length Bytes][Data][EOP]
		public TCPBufferedConnection(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize,
			string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose, byte SOP, byte EOP, int iLengthBytes, bool bFixedLength)
		{
			StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, bufferSize, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose, SOP, EOP, iLengthBytes, bFixedLength, 0, null, 0);
		}
		// Same as above with iAdditionalHeaderBytes additional bytes of header not included in the length value.
		// i.e. [SOP][Length Bytes][Header][Data][EOP]
		public TCPBufferedConnection(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize,
			string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose, byte SOP, byte EOP, int iLengthBytes, bool bFixedLength, int iAdditionalHeaderBytes)
		{
			StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, bufferSize, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose, SOP, EOP, iLengthBytes, bFixedLength, 0, null, iAdditionalHeaderBytes);
		}
		// For looking for a fixed number of bytes at a time
		public TCPBufferedConnection(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize,
			string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose, int iFixedPacketLength)
		{
			StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, bufferSize, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose, (byte) 0x00, (byte) 0x00, 0, true, iFixedPacketLength, null, 0);
		}
		// For looking for specific bytes
		public TCPBufferedConnection(TcpClient client, 	string description, 	QueueInterface fromRemoteIPQ,  QueueInterface toRemoteIPQ, 
			ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod, int bufferSize,
			string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose, byte[] bLookForTheseBytes)
		{
			StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, bufferSize, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose, (byte) 0x00, (byte) 0x00, 0, false, 0, bLookForTheseBytes, 0);
		}
		#endregion

		public void Close()
		{
			_client.Close();
			_cbuffer.Stop();
		}

		public void client_connectionclosed(TCPConnection sender)
		{
			if (_connectionClosed != null)
				_connectionClosed(sender);
		}		
		#endregion

		#region Public Properties
		public QueueInterface fromRemoteIPQ
		{
			get
			{
				return _fromRemoteIPQ;
			}
		}

		public QueueInterface toRemoteIPQ
		{
			get
			{
				return _toRemoteIPQ;
			}
		}

		public int HashCode
		{
			get
			{
				return _client.HashCode;
			}
		}

		public bool Active
		{
			get
			{
				return _client.Active;
			}
		}

		public int Port
		{
			set 
			{
				_client.Port = value;
			}
			get
			{
				return _client.Port;
			}
		}
		#endregion

	}
}

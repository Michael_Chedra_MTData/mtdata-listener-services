using System;
using System.Net;
using System.Text;

namespace MTData.Common.Network
{
    public class UDPServer_Message
    {
        public IPEndPoint EndPoint = null;
        public byte[] Data = null;
        public object Tag = null;

        public UDPServer_Message(IPEndPoint oEndPoint, byte[] bData)
        {
            Data = bData;
            EndPoint = oEndPoint;
        }
        public UDPServer_Message(IPEndPoint oEndPoint, byte[] bData, object oTag)
        {
            Data = bData;
            EndPoint = oEndPoint;
            Tag = oTag;
        }
    }
}

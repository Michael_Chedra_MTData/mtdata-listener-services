using System;
using System.Collections;

namespace MTData.Network
{
	/// <summary>
	/// This class will allow a number of other logging interfaces
	/// to be combined into one.
	/// </summary>
	public class LoggingGroup : LoggingInterface
	{
		private ArrayList _loggers = null;

		public LoggingGroup()
		{
			_loggers = ArrayList.Synchronized(new ArrayList());	
		}

		public void AddLogger(LoggingInterface logger)
		{
			lock(_loggers.SyncRoot)
				if (!_loggers.Contains(logger))
					_loggers.Add(logger);
		}

		public void RemoveLogger(LoggingInterface logger)
		{
			lock(_loggers.SyncRoot)
				if (_loggers.Contains(logger))
					_loggers.Remove(logger);
		}

		#region LoggingInterface Members

		public void LogInformation(string message)
		{
			lock(_loggers.SyncRoot)
			{
				for(int loop = 0; loop < _loggers.Count; loop++)
					try
					{
						((LoggingInterface)_loggers[loop]).LogInformation(message);
					}
					catch(Exception ex)
					{
						Console.WriteLine("Error Logging item : {0}", message);
						Console.WriteLine("Exception : {0} : {1}", ex.Message, ex.StackTrace);
					}
			}
		}

		public void LogWarning(string message)
		{
			lock(_loggers.SyncRoot)
			{
				for(int loop = 0; loop < _loggers.Count; loop++)
					try
					{
						((LoggingInterface)_loggers[loop]).LogWarning(message);
					}
					catch(Exception ex)
					{
						Console.WriteLine("Error Logging item : {0}", message);
						Console.WriteLine("Exception : {0} : {1}", ex.Message, ex.StackTrace);
					}
			}
		}

		public void LogError(string message, Exception ex)
		{
			lock(_loggers.SyncRoot)
			{
				for(int loop = 0; loop < _loggers.Count; loop++)
					try
					{
						((LoggingInterface)_loggers[loop]).LogError(message, ex);
					}
					catch(Exception)
					{
						Console.WriteLine("Error Logging item : {0}", message);
						Console.WriteLine("Exception : {0} : {1}", ex.Message, ex.StackTrace);
					}
			}
		}

		#endregion
	}
}

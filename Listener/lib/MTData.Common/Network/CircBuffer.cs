using System;
using System.Threading;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;

namespace MTData.Common.Network
{
    /// <summary>
    /// This reader will read bytes form a queue, determine the packet boundary,
    /// and then traqnslate them prior to passing them on to the target queue
    /// </summary>
    public class CircBuffer
    {
        private const string sClassName = "MTData.Network.cCircBuffer.";
        private static ILog _log = LogManager.GetLogger(typeof(CircBuffer));

        // Optional Event interface
        public delegate void CompletePacketFound();
        public event CompletePacketFound _completePacketFound;

        private int _bufferSize;
        private int _iLengthBytes = 0;
        private int _iHeaderBytes = 0;										// The count of bytes in the header after the length bytes
        private byte _bSOP = (byte)0x00;
        private byte _bEOP = (byte)0x00;
        private byte[] _bLookForTheseBytes = null;
        private bool _bLookForFPL = false;
        private bool _bLookForSpecificBytes = false;
        private int _iFixedPacketLength = 0;
        private QueueInterface _queOutbound;
        private EnhancedThread _thread;
        private string sConnectionName = "Client";
        private TCPConnection interfaceConnection = null;

        // Full constrictor.
        private void StandardConstructor(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound,
            byte SOP, byte EOP, int iLengthBytes, bool LookForFPL, int iFixedPacketLength, byte[] bLookForTheseBytes, int iAddtionalHeaderBytes)
        {
            sConnectionName = ConnectionName;
            interfaceConnection = interfaceTCPConn;
            _bufferSize = bufferSize;
            _queOutbound = queOutbound;

            _bSOP = SOP;
            _bEOP = EOP;
            _iLengthBytes = iLengthBytes;

            _bLookForFPL = LookForFPL;
            _iFixedPacketLength = iFixedPacketLength;
            _iHeaderBytes = iAddtionalHeaderBytes;

            if (bLookForTheseBytes != null)
            {
                _bLookForTheseBytes = bLookForTheseBytes;
                _bLookForFPL = false;
                _bLookForSpecificBytes = true;
            }

            _thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
            _thread.Name = "Buffer Thread for " + sConnectionName;
            _thread.Start();
        }

        #region Constructor overrides
        public CircBuffer(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound,
            byte SOP, byte EOP, int iLengthBytes, bool LookForFPL, int iFixedPacketLength, byte[] bLookForTheseBytes, int iAdditionalHeaderBytes)
        {
            StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, SOP, EOP, iLengthBytes, LookForFPL, iFixedPacketLength, bLookForTheseBytes, iAdditionalHeaderBytes);
        }

        // Normal MTData packet
        public CircBuffer(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound)
        {
            StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, (byte)0x02, (byte)0x03, 2, false, 0, null, 0);
        }

        public CircBuffer(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound,
            byte SOP, byte EOP, int iLengthBytes, bool bFixedLength)
        {
            if (bFixedLength)
                // A packet with a fixed number of bytes and fixed SOP/EOP
                StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, SOP, EOP, 0, bFixedLength, iLengthBytes, null, 0);
            else
                // A packet with SOP, EOP and the number of length bytes (iLengthBytes).
                StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, SOP, EOP, iLengthBytes, bFixedLength, 0, null, 0);
        }

        public CircBuffer(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound,
            byte SOP, byte EOP, int iLengthBytes, int iAdditionalHeaderBytes)
        {
            // A packet with a fixed number of bytes and fixed SOP/EOP with iAdditionalHeaderBytes bytes of header (not included in the length value).
            StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, SOP, EOP, 0, false, iLengthBytes, null, iAdditionalHeaderBytes);
        }

        // A packet with a fixed number of bytes and no fixed SOP/EOP
        public CircBuffer(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound,
            int iFixedPacketLength)
        {
            StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, (byte)0x00, (byte)0x00, 0, true, iFixedPacketLength, null, 0);
        }

        // Look for a specific set of bytes
        public CircBuffer(
            string ConnectionName,
            int bufferSize,
            TCPConnection interfaceTCPConn,
            QueueInterface queOutbound,
            byte[] bLookForTheseBytes)
        {
            StandardConstructor(ConnectionName, bufferSize, interfaceTCPConn, queOutbound, (byte)0x00, (byte)0x00, 0, false, 0, bLookForTheseBytes, 0);
        }
        #endregion

        private enum PacketState
        {
            SeekingStart,
            ReadingFirstLength,
            ReadingSecondLength,
            ReadingThirdLength,
            ReadingForthLength,
            ReadingData
        }

        /// <summary>
        /// Start Reading form the inbound queue.. determine the start point of the packet,
        /// 
        /// Convert the packet to the MCC format, ad place it in the outbound queue.. job done.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadHandler(EnhancedThread sender, object data)
        {
            int bufferNextInsert = 0;
            int packetLength = 0;
            int iStartPoint = 0;
            byte[] packetLengthBuffer = new byte[4];
            bool bContiune = false;
            PacketState packetState = PacketState.SeekingStart;

            byte[] buffer = new byte[_bufferSize];
            _log.Info("Circular buffer for " + sConnectionName + " started.");
            try
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        byte[] entry = null;

                        lock (interfaceConnection.fromRemoteIPQ.SyncRoot)
                            if (interfaceConnection.fromRemoteIPQ.Count > 0)
                                entry = (byte[])interfaceConnection.fromRemoteIPQ.Dequeue();

                        //	Append piece of packet to 
                        if (entry == null)
                        {
                            Thread.Sleep(10);
                        }
                        else
                        {
                            _log.Info("Circular buffer for " + sConnectionName + " : Processing Bytes : " + MTData.Common.Utilities.Util.EncodeByteArrayForLogging(entry));

                            for (int loop = 0; loop < entry.Length; loop++)
                            {
                                switch (packetState)
                                {
                                    case PacketState.SeekingStart:
                                        {
                                            //	Keep looking for the start of the packet..
                                            if (_bLookForFPL)
                                            {
                                                #region Look for fixed length packet start
                                                bContiune = false;
                                                if (_bSOP != (byte)0x00 && _bEOP != (byte)0x00)
                                                {
                                                    if (entry[loop] == _bSOP)
                                                    {
                                                        bContiune = true;
                                                    }
                                                }
                                                else
                                                    bContiune = true;

                                                if (bContiune)
                                                {
                                                    #region If we found a start of packet
                                                    bufferNextInsert = 0;
                                                    buffer[bufferNextInsert++] = entry[loop];
                                                    packetLength = _iFixedPacketLength;
                                                    packetState = PacketState.ReadingData;
                                                    iStartPoint = loop;
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                if (_bLookForSpecificBytes)
                                                {
                                                    #region Look for a match to the first byte of the specific string of bytes we are trying to match.
                                                    if (entry[loop] == _bLookForTheseBytes[0])
                                                    {
                                                        bufferNextInsert = 0;
                                                        buffer[bufferNextInsert++] = entry[loop];
                                                        packetState = PacketState.ReadingData;
                                                        iStartPoint = loop;
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Looking for a SOP marker
                                                    if (entry[loop] == _bSOP)
                                                    {
                                                        bufferNextInsert = 0;
                                                        buffer[bufferNextInsert++] = entry[loop];
                                                        packetState = PacketState.ReadingFirstLength;
                                                        packetLength = 0;
                                                        iStartPoint = loop;
                                                    }
                                                    #endregion
                                                }
                                            }
                                            break;
                                        }
                                    case PacketState.ReadingFirstLength:
                                        {
                                            #region Read the first byte of the length
                                            if (_iLengthBytes > 1)
                                            {
                                                packetLengthBuffer[0] = entry[loop];
                                                buffer[bufferNextInsert++] = entry[loop];
                                                packetState = PacketState.ReadingSecondLength;
                                            }
                                            else
                                            {
                                                packetLengthBuffer[0] = entry[loop];
                                                buffer[bufferNextInsert++] = entry[loop];

                                                //	Add 4 to packet length to include the start byte and 3 length bytes + any aditional header bytes (_iHeaderBytes) and the EOP byte at the end of the data.
                                                packetLength = BitConverter.ToInt32(packetLengthBuffer, 0) + 3 + _iHeaderBytes;
                                                if (packetLength > 0)
                                                    packetState = PacketState.ReadingData;
                                                else
                                                {
                                                    packetState = PacketState.SeekingStart;
                                                    loop = iStartPoint + 1;
                                                }

                                            }
                                            #endregion
                                            break;
                                        }
                                    case PacketState.ReadingSecondLength:
                                        {
                                            #region Read the second  byte of the length
                                            if (_iLengthBytes > 2)
                                            {
                                                packetLengthBuffer[1] = entry[loop];
                                                buffer[bufferNextInsert++] = entry[loop];
                                                packetState = PacketState.ReadingThirdLength;
                                            }
                                            else
                                            {
                                                packetLengthBuffer[1] = entry[loop];
                                                buffer[bufferNextInsert++] = entry[loop];

                                                //	Add 4 to packet length to include the start byte and 2 length bytes + any aditional header bytes (_iHeaderBytes) and the EOP byte at the end of the data.
                                                packetLength = BitConverter.ToInt32(packetLengthBuffer, 0) + 4 + _iHeaderBytes;
                                                if (packetLength > _bufferSize)
                                                {
                                                    // This is too big to be a real packet.
                                                    packetState = PacketState.SeekingStart;
                                                    loop = iStartPoint + 1;
                                                }
                                                else
                                                {
                                                    if (packetLength > 0)
                                                        packetState = PacketState.ReadingData;
                                                    else
                                                    {
                                                        packetState = PacketState.SeekingStart;
                                                        loop = iStartPoint + 1;
                                                    }
                                                }
                                            }
                                            #endregion
                                            break;
                                        }
                                    case PacketState.ReadingThirdLength:
                                        #region Read the third byte of the length
                                        if (_iLengthBytes > 3)
                                        {
                                            packetLengthBuffer[2] = entry[loop];
                                            buffer[bufferNextInsert++] = entry[loop];
                                            packetState = PacketState.ReadingForthLength;
                                        }
                                        else
                                        {
                                            packetLengthBuffer[2] = entry[loop];
                                            buffer[bufferNextInsert++] = entry[loop];

                                            //	Add 4 to packet length to include the start byte and 3 length bytes + any aditional header bytes (_iHeaderBytes) and the EOP byte at the end of the data.
                                            packetLength = BitConverter.ToInt32(packetLengthBuffer, 0) + 5 + _iHeaderBytes;
                                            if (packetLength > _bufferSize)
                                            {
                                                // This is too big to be a real packet.
                                                packetState = PacketState.SeekingStart;
                                                loop = iStartPoint + 1;
                                            }
                                            else
                                            {
                                                if (packetLength > 0)
                                                    packetState = PacketState.ReadingData;
                                                else
                                                {
                                                    packetState = PacketState.SeekingStart;
                                                    loop = iStartPoint + 1;
                                                }
                                            }
                                        }
                                        #endregion
                                        break;
                                    case PacketState.ReadingForthLength:
                                        #region Read the forth  byte of the length
                                        packetLengthBuffer[3] = entry[loop];
                                        buffer[bufferNextInsert++] = entry[loop];

                                        //	Add 4 to packet length to include the start byte and 4 length bytes + any aditional header bytes (_iHeaderBytes) and the EOP byte at the end of the data.
                                        packetLength = BitConverter.ToInt32(packetLengthBuffer, 0) + 6 + _iHeaderBytes;
                                        if (packetLength > _bufferSize)
                                        {
                                            // This is too big to be a real packet.
                                            packetState = PacketState.SeekingStart;
                                            loop = iStartPoint + 1;
                                        }
                                        else
                                        {
                                            if (packetLength > 0)
                                                packetState = PacketState.ReadingData;
                                            else
                                            {
                                                packetState = PacketState.SeekingStart;
                                                loop = iStartPoint + 1;
                                            }
                                        }
                                        #endregion
                                        break;
                                    case PacketState.ReadingData:
                                        {

                                            buffer[bufferNextInsert++] = entry[loop];
                                            if (_bLookForFPL)
                                            {
                                                #region Looking for a fixed length packet
                                                bContiune = false;
                                                if (_bSOP != (byte)0x00 && _bEOP != (byte)0x00)
                                                {
                                                    if (entry[loop] == _bEOP)
                                                    {
                                                        bContiune = true;
                                                    }
                                                }
                                                else
                                                    bContiune = true;

                                                // If we are at the EOP
                                                if (bContiune)
                                                {
                                                    #region If we are looking for a fixed number of bytes in each packet.
                                                    byte[] bQueueData = new byte[packetLength];
                                                    for (int iY = 0; iY < packetLength; iY++)
                                                        bQueueData[iY] = buffer[iY];

                                                    //Console.Write("Found a packet - "  + Utilities.EncodeByteArrayForLogging(bQueueData) + "\n");

                                                    // Enqueue the complete packet
                                                    lock (_queOutbound.SyncRoot)
                                                        _queOutbound.Enqueue(bQueueData);

                                                    // If the event interface is hooked up..
                                                    if (_completePacketFound != null)
                                                        _completePacketFound();

                                                    //  switch back to seeking start
                                                    packetState = PacketState.SeekingStart;
                                                    #endregion
                                                }
                                                else
                                                {
                                                    // The EOP marker was not found.
                                                    if (entry[loop] != _bEOP)
                                                        _log.Info("No EOP found on incomming packet on " + sConnectionName + " connection.");

                                                    //  switch back to seeking start
                                                    packetState = PacketState.SeekingStart;
                                                    loop = iStartPoint + 1;
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                if (_bLookForSpecificBytes)
                                                {
                                                    #region Looking for a specific set of bytes
                                                    if (entry[loop] == _bLookForTheseBytes[bufferNextInsert])
                                                    {
                                                        buffer[bufferNextInsert++] = entry[loop];
                                                        if (bufferNextInsert == _bLookForTheseBytes.Length - 1)
                                                        {
                                                            #region We have found a matching packet
                                                            byte[] bQueueData = new byte[bufferNextInsert];
                                                            for (int iY = 0; iY < bufferNextInsert; iY++)
                                                                bQueueData[iY] = buffer[iY];

                                                            //Console.Write("Found a packet - "  + Utilities.EncodeByteArrayForLogging(bQueueData) + "\n");

                                                            // Enqueue the complete packet
                                                            lock (_queOutbound.SyncRoot)
                                                                _queOutbound.Enqueue(bQueueData);

                                                            // If the event interface is hooked up..
                                                            if (_completePacketFound != null)
                                                                _completePacketFound();

                                                            //  switch back to seeking start
                                                            packetState = PacketState.SeekingStart;
                                                            #endregion
                                                        }
                                                    }
                                                    else
                                                    {
                                                        #region Start looking again
                                                        packetState = PacketState.SeekingStart;
                                                        loop = iStartPoint + 1;
                                                        #endregion
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region Reading until packet length is reached
                                                    //  if the end point is found.. convert the buffer..
                                                    if (bufferNextInsert == packetLength - 1)
                                                    {
                                                        if (entry[loop] != _bEOP)
                                                        {
                                                            // The EOP marker was not found.
                                                            _log.Info("No EOP found on incomming packet on " + sConnectionName + " connection.");
                                                            //  switch back to seeking start
                                                            packetState = PacketState.SeekingStart;
                                                            loop = iStartPoint + 1;
                                                        }
                                                        else
                                                        {
                                                            byte[] bQueueData = new byte[packetLength];
                                                            for (int iY = 0; iY < packetLength; iY++)
                                                                bQueueData[iY] = buffer[iY];

                                                            // Enqueu the complete packet
                                                            lock (_queOutbound.SyncRoot)
                                                                _queOutbound.Enqueue(bQueueData);

                                                            // If the event interface is hooked up..
                                                            if (_completePacketFound != null)
                                                                _completePacketFound();

                                                            //  switch back to seeking start
                                                            packetState = PacketState.SeekingStart;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                            break;
                                        }
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data)", ex);
                    }

                }
            }
            finally
            {
                _log.Info("Circular buffer for " + sConnectionName + " stopped");
            }
            return null;
        }

        /// <summary>
        /// Take a standard integer and convert it into a 4 byte array
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private byte[] ConvertIntto4ByteArray(int newValue)
        {
            byte[] temp = BitConverter.GetBytes(newValue);
            byte[] result = new byte[4];
            result[0] = (temp.Length > 0) ? temp[0] : (byte)0;
            result[1] = (temp.Length > 1) ? temp[1] : (byte)0;
            result[2] = (temp.Length > 2) ? temp[2] : (byte)0;
            result[3] = (temp.Length > 3) ? temp[3] : (byte)0;
            return result;
        }

        public void Stop()
        {
            _thread.Stop();
        }
    }
}


using System;
using System.Net.Sockets;
using System.Collections;


namespace MTData.Network
{
	/// <summary>
	/// Summary description for TCPLogger.
	/// </summary>
	public class TCPLogger : LoggingInterface
	{
		private TCPServer _server = null;
		private Hashtable _connections = null;
		private int _waitPeriod = 100;

		public class ConnectionEntry
		{
			private TCPConnection _oConnection = null;
			private QueueInterface _qOutbound = null;
			private QueueInterface _qInbound = null;


			public TCPConnection Connection
			{
				get
				{
					return _oConnection;
				}
			}
			public QueueInterface Outbound
			{
				get
				{
					return _qOutbound;
				}
			}

			public QueueInterface Inbound
			{
				get
				{
					return _qInbound;
				}			
			}

			public ConnectionEntry(TCPConnection connection, QueueInterface qOutbound)
			{
				_qOutbound = qOutbound;
				_qInbound = null;
				_oConnection = connection;
			}

			public ConnectionEntry(TCPConnection connection, QueueInterface qOutbound, QueueInterface qInbound)
			{
				_qOutbound = qOutbound;
				_qInbound = qInbound;
				_oConnection = connection;
			}
		}

		public TCPLogger(int portNumber, int waitPeriod)
		{
			_waitPeriod = waitPeriod;
			_connections = Hashtable.Synchronized(new Hashtable());

			TCPServer.InboundConnectionHandler handler = new MTData.Network.TCPServer.InboundConnectionHandler(HandleInboundConnection);
			_server = new TCPServer("127.0.0.1", portNumber, "Logging Comms", handler);
			_server.Start();
			
		}

		public bool Active
		{
			get
			{
				if (_server != null)
					return _server.Listening;
				else
					return false;
			}
		}

		public void Stop()
		{
			_server.Stop();

			ConnectionEntry[] connections = null;
			lock(_connections.SyncRoot)
			{
				connections = new ConnectionEntry[_connections.Count];
				_connections.Values.CopyTo(connections, 0);
			}

			for(int loop = 0; loop < connections.Length; loop++)
				connections[loop].Connection.Close();

		}

		public void HandleInboundConnection(TcpListener listener)
		{
			TcpClient client = listener.AcceptTcpClient();

			QueueInterface outBound = new StandardQueue(20);

			TCPConnection connection = new TCPConnection(client, "LogConnection", outBound, null, new TCPConnection.ConnectionClosedHandler(ConnectionClosed), 40, 5, 100);
			
			lock(_connections.SyncRoot)
				_connections.Add(connection, new ConnectionEntry(connection, outBound));
		}

		/// <summary>
		/// Event handler to cater for the closure of a carry connection
		/// </summary>
		/// <param name="sender"></param>
		public void ConnectionClosed(TCPConnection sender)
		{
			lock(_connections.SyncRoot)
				if (_connections.Contains(sender))
					_connections.Remove(sender);
		}

		private void WriteString(string message)
		{
			ConnectionEntry[] connections = null;
			lock(_connections.SyncRoot)
			{
				connections = new ConnectionEntry[_connections.Count];
				_connections.Values.CopyTo(connections, 0);
			}

			foreach(ConnectionEntry entry in connections)
				try
				{
					lock(entry.Outbound.SyncRoot)
						entry.Outbound.Enqueue(System.Text.UTF8Encoding.UTF8.GetBytes(message + "\r\n"));
				}
				catch(Exception ex)
				{
					Console.WriteLine(String.Format("Could not add Log Entry to TCPOutQ : {0} : Stack : {1}", ex.Message, ex.StackTrace));
				}
		}

		#region LoggingInterface Members

		public void LogInformation(string message)
		{
			if (message.StartsWith("EMAIL")) return;

			WriteString(String.Format("{0:dd/MM/yyyy HH:mm:ss} Info  : {1}", DateTime.Now, message));
		}

		public void LogWarning(string message)
		{
			if (message.StartsWith("EMAIL")) return;

			WriteString(String.Format("{0:dd/MM/yyyy HH:mm:ss} Warn  : {1}", DateTime.Now, message));
		}

		public void LogError(string message, Exception ex)
		{
			if (message.StartsWith("EMAIL")) return;

			WriteString(String.Format("{0:dd/MM/yyyy HH:mm:ss} Error : {1} : Error : {2} Stack : {3}\r\n", DateTime.Now, message, ex.Message, ex.StackTrace));
		}

		#endregion
	}
}

using System;
using System.Threading;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;
using MTData.Common.Utilities;

namespace MTData.Common.Network
{
    /// <summary>
    /// Summary description for cSerialCircBuffer.
    /// </summary>
    public class SerialCircBuffer
    {
        private static ILog _log = LogManager.GetLogger(typeof(SerialCircBuffer));

        /// <summary>
        /// This reader will read bytes form a queue, determine the packet boundary,
        /// and then traqnslate them prior to passing them on to the target queue
        /// </summary>
        public delegate void CompletePacketFound();
        public event CompletePacketFound _completePacketFound;

        public object SyncRoot = null;
        private int _bufferSize;
        private QueueInterface _queOutbound;
        private QueueInterface _queInbound;
        private EnhancedThread _thread;
        private string sConnectionName = "Client";
        private byte bSOP = (byte)0x00;
        private byte bEOP = (byte)0x00;
        private bool bLookForPacketLength = true;
        private bool bLookForFixedLengthPacket = true;
        private byte[] bLookForTheseBytes = null;
        private int iFixedPacketLength = 0;


        private void CreateCircBuffer(
            string ConnectionName,
            int bufferSize,
            QueueInterface queInbound,
            QueueInterface queOutbound,
            byte SOP, byte EOP, bool LookForPacketLen, bool UsedFixedLength, int PacketFixedLength, byte[] LookForTheseBytes)
        {
            #region Set up Common Parameters
            SyncRoot = new object();
            sConnectionName = ConnectionName;
            _bufferSize = bufferSize;
            _queInbound = queInbound;
            _queOutbound = queOutbound;
            bLookForFixedLengthPacket = UsedFixedLength;
            bLookForTheseBytes = LookForTheseBytes;
            #endregion

            #region Select the type of processing to perform on the buffer
            if (bLookForTheseBytes != null)
            {
                bSOP = (byte)0x00; ;
                bEOP = (byte)0x00; ;
                bLookForPacketLength = false;
                bLookForFixedLengthPacket = false;
                iFixedPacketLength = 0;
            }
            else
            {
                if (!bLookForFixedLengthPacket)
                {
                    bSOP = SOP;
                    bEOP = EOP;
                    bLookForPacketLength = LookForPacketLen;
                    iFixedPacketLength = 0;
                }
                else
                {
                    bSOP = (byte)0x00; ;
                    bEOP = (byte)0x00; ;
                    bLookForPacketLength = false;
                    iFixedPacketLength = PacketFixedLength;
                }
            }
            #endregion

            #region Start the buffer processing thread.
            _thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), null);
            _thread.Name = "Buffer Thread for " + sConnectionName;
            _thread.Start();
            #endregion
        }

        #region Constructors
        public SerialCircBuffer(
            string ConnectionName,
            int bufferSize,
            QueueInterface queInbound,
            QueueInterface queOutbound)
        {
            // This is for the standard generic MTData interface, packets SOP = 0x02, EOP = 0x02, 2 bytes length
            // [0x02][Len1][Len2][Data 0]...[Data Length][0x03]
            CreateCircBuffer(ConnectionName, bufferSize, queInbound, queOutbound, (byte)0x02, (byte)0x03, true, false, 0, null);
        }

        public SerialCircBuffer(
            string ConnectionName,
            int bufferSize,
            QueueInterface queInbound,
            QueueInterface queOutbound,
            byte SOP, byte EOP)
        {
            // This is for the standard packets with alternate SOP and EOP bytes, the packet still has 2 bytes length
            // [SOP][Len1][Len2][Data 0]...[Data Length][EOP]
            CreateCircBuffer(ConnectionName, bufferSize, queInbound, queOutbound, SOP, EOP, true, false, 0, null);
        }

        public SerialCircBuffer(
            string ConnectionName,
            int bufferSize,
            QueueInterface queInbound,
            QueueInterface queOutbound,
            byte SOP, byte EOP, bool LookForPacketLen)
        {
            // This is for the packets with alternate SOP and EOP bytes, and optionally (LookForPacketLen) 2 bytes length
            // LookForPacketLen = true then [SOP][Len1][Len2][Data 0]...[Data Length][EOP]
            // else [SOP][Data 0]...[Data X][EOP]

            CreateCircBuffer(ConnectionName, bufferSize, queInbound, queOutbound, SOP, EOP, LookForPacketLen, false, 0, null);
        }

        public SerialCircBuffer(
            string ConnectionName,
            int bufferSize,
            QueueInterface queInbound,
            QueueInterface queOutbound,
            int iFixedPacketLength)
        {
            // This is for the packets with a fixed length
            // [Byte 0]...[Byte Length]
            CreateCircBuffer(ConnectionName, bufferSize, queInbound, queOutbound, (byte)0x00, (byte)0x00, false, true, iFixedPacketLength, null);
        }

        public SerialCircBuffer(
            string ConnectionName,
            int bufferSize,
            QueueInterface queInbound,
            QueueInterface queOutbound,
            byte[] LookForTheseBytes)
        {
            // This is for when you are looking for a specific set of bytes (LookForTheseBytes) in the stream
            CreateCircBuffer(ConnectionName, bufferSize, queInbound, queOutbound, (byte)0x00, (byte)0x00, false, false, 0, LookForTheseBytes);
        }
        #endregion

        private bool LookForPacketLength
        {
            // This variable needs to be syncronised so that the way packets are parsed can be 
            // change whilst the object is running.
            // This is useful in the 3030 tester where the test code sends through packet lengths but the real code doesn't.
            get { return bLookForPacketLength; }
            set
            {
                lock (SyncRoot)
                {
                    bLookForPacketLength = value;
                }
            }
        }

        public void LookForSOP_EOPPacketMarkers(bool LookForPacketLength, byte SOP, byte EOP)
        {
            // This variable needs to be syncronised so that the way packets are parsed can be 
            // change whilst the object is running.
            lock (SyncRoot)
            {
                bSOP = SOP;
                bEOP = EOP;
                bLookForPacketLength = LookForPacketLength;

                bLookForFixedLengthPacket = false;
                iFixedPacketLength = 0;
            }
        }

        private void LookForFixedLengthPacket(int FixedPacketLength)
        {
            // This variable needs to be syncronised so that the way packets are parsed can be 
            // change whilst the object is running.
            if (FixedPacketLength > 0)
            {
                lock (SyncRoot)
                {
                    bLookForFixedLengthPacket = true;
                    iFixedPacketLength = FixedPacketLength;

                    bSOP = (byte)0x00; ;
                    bEOP = (byte)0x00; ;
                    bLookForPacketLength = false;
                }
            }
        }

        private enum PacketState
        {
            SeekingStart,
            ReadingFirstLength,
            ReadingSecondLength,
            ReadingData
        }

        /// <summary>
        /// Start Reading form the inbound queue.. determine the start point of the packet,
        /// 
        /// Convert the packet to the MCC format, ad place it in the outbound queue.. job done.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadHandler(EnhancedThread sender, object data)
        {
            int bufferNextInsert = 0;
            int iStartPoint = 0;
            int packetLength = 0;
            byte[] packetLengthBuffer = new byte[4];
            PacketState packetState = PacketState.SeekingStart;
            bool bLookForEOP = true;
            bool bLookForFPL = true;
            byte[] buffer = new byte[_bufferSize];

            _log.Info("Circular buffer for " + sConnectionName + " started.");
            try
            {
                while (!sender.Stopping)
                {
                    #region  Check to see if the processing method has changed.
                    lock (this.SyncRoot)
                    {
                        bLookForEOP = bLookForPacketLength;
                        bLookForFPL = bLookForFixedLengthPacket;
                    }
                    #endregion

                    try
                    {
                        #region Check if there is any new data to process.
                        byte[] entry = null;

                        lock (_queInbound)
                        {
                            if (_queInbound.Count > 0)
                                entry = (byte[])_queInbound.Dequeue();
                            else
                                entry = null;
                        }
                        #endregion

                        //	Append piece of packet to 
                        if (entry == null)
                        {
                            Thread.Sleep(10);
                        }
                        else
                        {
                            _log.Info("Circular buffer for " + sConnectionName + " : Processing Bytes : " + MTData.Common.Utilities.Util.EncodeByteArrayForLogging(entry) + "\n");

                            for (int loop = 0; loop < entry.Length; loop++)
                            {
                                switch (packetState)
                                {
                                    case PacketState.SeekingStart:
                                        #region Looking for SOP
                                        //	Keep looking for the start of the packet..
                                        if (bLookForFPL)
                                        {
                                            #region If we are looking for a fixed length packet
                                            bufferNextInsert = 0;
                                            buffer[bufferNextInsert++] = entry[loop];
                                            packetLength = iFixedPacketLength;
                                            packetState = PacketState.ReadingData;
                                            iStartPoint = loop;
                                            #endregion
                                        }
                                        else
                                        {
                                            if (bLookForTheseBytes != null)
                                            {
                                                #region Looking for a specific set of bytes
                                                if (entry[loop] == bLookForTheseBytes[0])
                                                {
                                                    bufferNextInsert = 0;
                                                    buffer[bufferNextInsert++] = entry[loop];
                                                    packetState = PacketState.ReadingData;
                                                    iStartPoint = loop;
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Looking for a SOP marker
                                                if (entry[loop] == bSOP)
                                                {
                                                    bufferNextInsert = 0;
                                                    buffer[bufferNextInsert++] = entry[loop];
                                                    iStartPoint = loop;

                                                    if (bLookForEOP)
                                                    {
                                                        packetState = PacketState.ReadingFirstLength;
                                                        packetLength = 0;
                                                    }
                                                    else
                                                    {
                                                        packetState = PacketState.ReadingData;
                                                    }
                                                }
                                                #endregion
                                            }
                                        }
                                        #endregion
                                        break;
                                    case PacketState.ReadingFirstLength:
                                        #region Looking for the first byte of length
                                        packetLengthBuffer[0] = entry[loop];
                                        buffer[bufferNextInsert++] = entry[loop];
                                        packetState = PacketState.ReadingSecondLength;
                                        #endregion
                                        break;
                                    case PacketState.ReadingSecondLength:
                                        #region Looking for the seconf byte of length
                                        if (entry[loop] != (byte)0x00)
                                        {
                                            if (entry[loop] == bSOP)
                                            {
                                                bufferNextInsert = 0;
                                                buffer[bufferNextInsert++] = entry[loop];
                                                packetState = PacketState.ReadingFirstLength;
                                                packetLength = 0;
                                            }
                                            else
                                            {
                                                packetState = PacketState.SeekingStart;
                                                loop = iStartPoint + 1;
                                            }
                                        }
                                        else
                                        {
                                            packetLengthBuffer[1] = entry[loop];
                                            buffer[bufferNextInsert++] = entry[loop];

                                            //	Add 4 to packet length to include the start byte and 2 length bytes and the EOP byte at the end of the data.
                                            packetLength = BitConverter.ToInt32(packetLengthBuffer, 0) + 4;
                                            if (packetLength > 44)
                                            {
                                                _log.Info("Packet size of " + packetLength + " is too large");

                                                // This is too big to be a real packet.
                                                packetState = PacketState.SeekingStart;
                                                loop = iStartPoint + 1;
                                            }
                                            else
                                            {
                                                Console.Write("Looking for a packet of length " + Convert.ToString(packetLength) + " bytes." + "\n");
                                                if (packetLength > 0)
                                                    packetState = PacketState.ReadingData;
                                                else
                                                {
                                                    packetState = PacketState.SeekingStart;
                                                    loop = iStartPoint + 1;
                                                }
                                            }
                                        }
                                        #endregion
                                        break;
                                    case PacketState.ReadingData:
                                        #region Append data to the array and attempt to find the EOP
                                        if (bLookForTheseBytes != null)
                                        {
                                            #region Check to see if the next byte matches the next byte we are looking for
                                            if (entry[loop] == bLookForTheseBytes[bufferNextInsert])
                                            {
                                                buffer[bufferNextInsert++] = entry[loop];

                                                if (bufferNextInsert == bLookForTheseBytes.Length - 1)
                                                {
                                                    #region We have found a matching packet
                                                    byte[] bQueueData = new byte[bufferNextInsert];
                                                    for (int iY = 0; iY < bufferNextInsert; iY++)
                                                        bQueueData[iY] = buffer[iY];

                                                    Console.Write("Found a packet - " + MTData.Common.Utilities.Util.EncodeByteArrayForLogging(bQueueData) + "\n");

                                                    // Enqueu the complete packet
                                                    lock (_queOutbound.SyncRoot)
                                                        _queOutbound.Enqueue(bQueueData);

                                                    if (_completePacketFound != null)
                                                        _completePacketFound();

                                                    //  switch back to seeking start
                                                    packetState = PacketState.SeekingStart;
                                                    #endregion
                                                }
                                            }
                                            else
                                            {
                                                #region Start looking again
                                                packetState = PacketState.SeekingStart;
                                                loop = iStartPoint + 1;
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            buffer[bufferNextInsert++] = entry[loop];
                                            if (bLookForFPL)
                                            {
                                                #region If we are looking for a fixed number of bytes in each packet.
                                                if (bufferNextInsert == packetLength)
                                                {
                                                    byte[] bQueueData = new byte[packetLength];
                                                    for (int iY = 0; iY < packetLength; iY++)
                                                        bQueueData[iY] = buffer[iY];

                                                    Console.Write("Found a packet - " + MTData.Common.Utilities.Util.EncodeByteArrayForLogging(bQueueData) + "\n");

                                                    // Enqueu the complete packet
                                                    lock (_queOutbound.SyncRoot)
                                                        _queOutbound.Enqueue(bQueueData);

                                                    if (_completePacketFound != null)
                                                        _completePacketFound();

                                                    //  switch back to seeking start
                                                    packetState = PacketState.SeekingStart;
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                if (bLookForEOP)
                                                {
                                                    #region If we know the length of the packet and we are looking for an EOP
                                                    //  if the end point is found.. convert the buffer..
                                                    if (bufferNextInsert == packetLength)
                                                    {
                                                        if (entry[loop] != bEOP)
                                                            _log.Info("No EOP found on incomming packet on " + sConnectionName + " connection." + "\n");

                                                        byte[] bQueueData = new byte[packetLength];
                                                        for (int iY = 0; iY < packetLength; iY++)
                                                            bQueueData[iY] = buffer[iY];

                                                        Console.Write("Found a packet - " + MTData.Common.Utilities.Util.EncodeByteArrayForLogging(bQueueData) + "\n");

                                                        // Enqueu the complete packet
                                                        lock (_queOutbound.SyncRoot)
                                                            _queOutbound.Enqueue(bQueueData);

                                                        if (_completePacketFound != null)
                                                            _completePacketFound();

                                                        //  switch back to seeking start
                                                        packetState = PacketState.SeekingStart;
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    #region If we don't know the lenght but are looking for an EOP marker
                                                    if (entry[loop] == bEOP)
                                                    {
                                                        packetLength = bufferNextInsert;
                                                        byte[] bQueueData = new byte[packetLength];
                                                        for (int iY = 0; iY < packetLength; iY++)
                                                            bQueueData[iY] = buffer[iY];

                                                        Console.Write("Found a packet - " + MTData.Common.Utilities.Util.EncodeByteArrayForLogging(bQueueData) + "\n");

                                                        // Enqueue the complete packet
                                                        lock (_queOutbound.SyncRoot)
                                                            _queOutbound.Enqueue(bQueueData);

                                                        // If something has hooked up to be notified when a packet arrives, send the notification.
                                                        if (_completePacketFound != null)
                                                            _completePacketFound();

                                                        //  switch back to seeking start
                                                        packetState = PacketState.SeekingStart;
                                                    }
                                                    if (loop > 2000)
                                                    {
                                                        _log.Info("Recieved " + Convert.ToString(loop) + " bytes without recieving an EOP" + "\n");

                                                        packetState = PacketState.SeekingStart;
                                                        loop = iStartPoint + 1;
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                        #endregion
                                        break;
                                }
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error("MTData.Network.cSerialCircBuffer.ThreadHandler(EnhancedThread sender, object data)", ex);
                    }

                }
            }
            finally
            {
                _log.Info("Circular buffer for " + sConnectionName + " stopped" + "\n");
            }
            return null;
        }

        /// <summary>
        /// Take a standard integer and convert it into a 4 byte array
        /// </summary>
        /// <param name="newValue"></param>
        /// <returns></returns>
        private byte[] ConvertIntto4ByteArray(int newValue)
        {
            byte[] temp = BitConverter.GetBytes(newValue);
            byte[] result = new byte[4];
            result[0] = (temp.Length > 0) ? temp[0] : (byte)0;
            result[1] = (temp.Length > 1) ? temp[1] : (byte)0;
            result[2] = (temp.Length > 2) ? temp[2] : (byte)0;
            result[3] = (temp.Length > 3) ? temp[3] : (byte)0;
            return result;
        }

        public void Stop()
        {
            _thread.Stop();
        }
    }
}

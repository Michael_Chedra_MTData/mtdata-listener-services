using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;

namespace MTData.Common.Network
{
    public class LogisticsServerHandler
    {
        private const string sClassName = "MTData.Network.LogisticsServerHandler.";
        private static ILog _log = LogManager.GetLogger(typeof(LogisticsServerHandler));

        #region Events
        // If nothing is heard for 120 seconds  
        public delegate void CommsTimeoutEvent();
        public event CommsTimeoutEvent eCommsTimeout;
        // If there is an error
        public delegate void CommsErrorEvent(string sMsg, System.Exception ex);
        public event CommsErrorEvent eCommsError;
        // Log send bytes
        public delegate void CommsLogSendEvent(byte[] bData);
        public event CommsLogSendEvent eCommsSendData;
        // Log Recv bytes
        public delegate void CommsLogRecvEvent(byte[] bData);
        public event CommsLogRecvEvent eCommsRecvData;
        #endregion
        #region Private Vars
        private bool _logIncommingComms;
        private bool _logOutgoingComm;
        /// <summary>
        /// To indicate if the UDP client is currently connected.
        /// </summary>
        private bool _connected;
        /// <summary>
        /// This is used for synchronisation
        /// </summary>
        private object _syncRoot = new object();
        /// <summary>
        /// UDP client for listener communications
        /// </summary>
        private UdpClient _udpClient = null;
        /// <summary>
        /// This is the Q that messages will be received on from the listener.
        /// This class only adds items to this q.
        /// </summary>
        private QueueInterface _fromListenerQ = null;
        /// <summary>
        /// This is the queue that messages will be sent to the listneer on
        /// This class is responsible for sending these packets to the listener.
        /// </summary>
        private QueueInterface _toListenerQ = null;
        /// <summary>
        /// This is the ip address of the listener.
        /// </summary>
        private string _liveUpdateAddress = "";
        /// <summary>
        /// This is the portnumber that the listener is listening on.
        /// </summary>
        private int _liveUpdatePortNumber = 0;
        /// <summary>
        /// This is the portnumber that the UDP will bind to locally.
        /// </summary>
        private int _iListenerLocalBindPort = 0;
        /// <summary>
        /// This is the thread that will process the messages from the listener.
        /// </summary>
        private EnhancedThread _threadListener = null;
        /// <summary>
        /// This is the thread that will process the queue to the listener.
        /// </summary>
        private EnhancedThread _threadSender = null;
        /// <summary>
        /// This is the thread that will check for a Comms Timeout
        /// </summary>
        private EnhancedThread _threadCommsTimeout = null;
        /// <summary>
        /// This is the thread that will send a ping periodically
        /// </summary>
        private EnhancedThread _threadServerPing = null;
        /// <summary>
        /// This is the date and time of the last listener communication
        /// </summary>
        private DateTime _lastListenerCommunication = DateTime.Now;
        /// <summary>
        /// This is the remote endpoint for the listener.
        /// </summary>
        private IPEndPoint _remoteEndPoint = null;
        /// <summary>
        /// If no data form listener for longer than this period, a reconnect will be initiated.
        /// </summary>
        private int _listenerTimeoutSeconds = 120;
        /// <summary>
        /// The number of seconds between pings.
        /// </summary>
        private int _pingTimer = 30;
        /// <summary>
        /// A flag to cause the ping thread to send an ping in 200ms time.
        /// </summary>
        private bool _pingNow;

        private byte[] _loginPacket;
        private byte[] _pingPacket;
        #endregion
        public byte[] LoginPacket
        {
            get
            {
                return _loginPacket;
            }
            set
            {
                _loginPacket = value;
            }
        }
        public bool LogIncommingPackets
        {
            get
            {
                return _logIncommingComms;
            }
            set
            {
                _logIncommingComms = value;
            }
        }
        public bool LogOutgoingPackets
        {
            get
            {
                return _logOutgoingComm;
            }
            set
            {
                _logOutgoingComm = value;
            }
        }
        public LogisticsServerHandler(
            string liveUpdateAddress,
            int liveUpdatePortNumber,
            int iListenerLocalBindPort,
            QueueInterface toLogisticsInterfaceQ,
            QueueInterface fromLogisticsInterfaceQ,
            int connectionTimeout,
            int pingTimer,
            byte[] loginPacket,
            byte[] pingPacket,
            bool logIncomming,
            bool logOutgoing)
        {
            _logIncommingComms = logIncomming;
            _logOutgoingComm = logOutgoing;
            _pingTimer = pingTimer;
            _loginPacket = loginPacket;
            _pingPacket = pingPacket;
            _liveUpdateAddress = liveUpdateAddress;
            _liveUpdatePortNumber = liveUpdatePortNumber;
            _iListenerLocalBindPort = iListenerLocalBindPort;
            _listenerTimeoutSeconds = connectionTimeout;
            _toListenerQ = toLogisticsInterfaceQ;
            _fromListenerQ = fromLogisticsInterfaceQ;
            System.Net.IPAddress ip = null;
            try
            {
                ip = System.Net.IPAddress.Parse(liveUpdateAddress);
                _remoteEndPoint = new IPEndPoint(ip, liveUpdatePortNumber);
            }
            catch
            {
                //not an IP address, try DNS lookup
                IPHostEntry ipHostInfo = Dns.GetHostEntry(liveUpdateAddress);
                string temp = liveUpdateAddress;
                liveUpdateAddress = "";
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ip = ipHostInfo.AddressList[X];
                        break;
                    }
                }
                if (ip != null)
                {
                    _remoteEndPoint = new IPEndPoint(ip, liveUpdatePortNumber);
                }
            }
            if (ip == null)
                throw (new Exception("Failed to connect to parse logistics address"));
        }

        /// <summary>
        /// Start the instance of the class listening for inbound communications from the listener, 
        /// and messages on the toListenerQ.
        /// Send a Connect message to the listener.
        /// </summary>
        public bool Start()
        {
            bool bRet = false;
            try
            {
                OpenUDPClient();
                if (_connected && _udpClient != null)
                    _toListenerQ.Enqueue(_loginPacket);
                else
                    throw (new Exception("Failed to connect to logistics service."));

                _threadSender = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadSenderHandler), null);
                _threadSender.Start();
                _threadListener = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadListenerHandler), null);
                _threadListener.Start();
                _threadServerPing = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadPingHandler), null);
                _threadServerPing.Start();

                if (_listenerTimeoutSeconds > 0)
                {
                    _threadCommsTimeout = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadCommsTimeoutHandler), null);
                    _threadCommsTimeout.Start();
                }
                lock (_toListenerQ.SyncRoot)
                {
                    _toListenerQ.Enqueue(_loginPacket);
                }
                bRet = true;
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "Start()", ex);
                if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".Start()", ex, null, null);
                Stop();
                throw (new Exception(ex.Message));
            }
            return bRet;
        }

        /// <summary>
        /// Stop the instance from listening, with a disconnect message to the listener.
        /// </summary>
        public void Stop()
        {
            try
            {
                int timeout = 100;
                #region Stop the ping thread
                try
                {
                    if (_threadServerPing != null)
                    {
                        if (_threadServerPing.Running)
                            _threadServerPing.Stop();
                        // Wait 1 second for the thread to terminate
                        timeout = 100;
                        while (_threadServerPing.Running && timeout >= 0)
                        {
                            timeout--;
                            Thread.Sleep(10);
                        }
                        _threadServerPing = null;
                    }
                }
                catch (System.Exception)
                {
                }
                #endregion
                #region Stop the comms timeout thread
                try
                {
                    if (_threadCommsTimeout != null)
                    {
                        if (_threadCommsTimeout.Running)
                            _threadCommsTimeout.Stop();
                        // Wait 1 second for the thread to terminate
                        timeout = 100;
                        while (_threadCommsTimeout.Running && timeout >= 0)
                        {
                            timeout--;
                            Thread.Sleep(10);
                        }
                        _threadCommsTimeout = null;
                    }
                }
                catch (System.Exception)
                {
                }
                #endregion
                #region Stop the recv thread
                try
                {
                    if (_threadListener != null)
                    {
                        if (_threadListener.Running)
                            _threadListener.Stop();
                        // Wait 1 second for the thread to terminate
                        timeout = 100;
                        while (_threadListener.Running && timeout >= 0)
                        {
                            timeout--;
                            Thread.Sleep(10);
                        }
                        _threadListener = null;
                    }
                }
                catch (System.Exception)
                {
                }
                #endregion
                #region Stop the sender thread
                try
                {
                    if (_threadSender != null)
                    {
                        if (_threadSender.Running)
                            _threadSender.Stop();
                        // Wait 1 second for the thread to terminate
                        timeout = 100;
                        while (_threadSender.Running && timeout >= 0)
                        {
                            timeout--;
                            Thread.Sleep(10);
                        }
                        _threadSender = null;
                    }
                }
                catch (System.Exception)
                {
                }
                #endregion
                CloseUDPClient();
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
                if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".Stop()", ex, null, null);
                Stop();
            }
        }

        /// <summary>
        /// This method will prepare the UdpClient for interaction with the listener.
        /// </summary>
        private void OpenUDPClient()
        {
            int portToUse = 0;

            lock (_syncRoot)
            {

                try
                {
                    // this wont work as easily as I thought
                    if (_iListenerLocalBindPort == 0)
                        portToUse = _liveUpdatePortNumber + 10;
                    else
                        portToUse = _iListenerLocalBindPort;
                    _log.Info("Connecting to Logistics Interface on " + _liveUpdateAddress + ":" + _liveUpdatePortNumber + " (using local port " + portToUse + ")");
                    int attemptCount = 0;
                    while (attemptCount < 5)
                    {
                        // Bind the new socket to the port.
                        try
                        {
                            _udpClient = new UdpClient(portToUse);
                            _connected = true;
                            break;
                        }
                        catch (Exception exOpen)
                        {
                            _log.Error(sClassName + "OpenUDPClient() - Failed to open connection to " + _liveUpdateAddress + ":" + _liveUpdatePortNumber + " (using local port " + portToUse + ")", exOpen);
                            if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".OpenUDPClient() - Failed to open connection to " + _liveUpdateAddress + ":" + _liveUpdatePortNumber + " (using local port " + portToUse + ")", exOpen, null, null);
                            //	Try another 10 on the port..
                            attemptCount++;
                            portToUse += 10;
                            _udpClient = null;
                            _connected = false;
                        }
                    }
                    if (!_connected)
                    {
                        _log.Info(this.GetType().FullName + ".OpenUDPClient() - Failed to open connection to " + _liveUpdateAddress + ":" + _liveUpdatePortNumber + " after 5 attempts");
                    }
                }
                catch (Exception ex)
                {
                    _udpClient = null;
                    _log.Error(sClassName + "OpenUDPClient()", ex);
                    if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".OpenUDPClient()", ex, null, null);
                }
            }
        }
        /// <summary>
        /// This method will close the udp client.
        /// </summary>
        private void CloseUDPClient()
        {
            try
            {
                lock (_syncRoot)
                {
                    if (_udpClient != null)
                    {
                        _log.Info("Closing Logistics Interface on " + _liveUpdateAddress + ":" + _liveUpdatePortNumber);
                        _udpClient.Close();
                        _udpClient = null;
                    }
                }
            }
            catch (Exception)
            {
            }
            finally
            {
                _connected = false;
            }

        }

        #region Threads
        /// <summary>
        /// This method will handle all communications from the listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadListenerHandler(EnhancedThread sender, object data)
        {
            bool sucessRecv = true;
            if (_udpClient != null)
            {
                while (!sender.Stopping)
                {
                    if (!sender.Stopping && _connected)
                    {
                        try
                        {
                            #region Try and receive data on the UDP client object
                            IPEndPoint remoteEndPoint = _remoteEndPoint;
                            byte[] dataIn = _udpClient.Receive(ref remoteEndPoint);
                            sucessRecv = true;
                            if (dataIn != null)
                            {
                                if (_logIncommingComms)
                                    _log.Info("Incomming Logistics Server Data : " + System.Text.Encoding.ASCII.GetString(dataIn));
                                _lastListenerCommunication = DateTime.Now;
                                _fromListenerQ.Enqueue(dataIn);
                                if (eCommsRecvData != null) eCommsRecvData.BeginInvoke(dataIn, null, null);
                            }
                            #endregion
                        }
                        catch (Exception ex)
                        {
                            if (!sender.Stopping)
                            {
                                // Set the connected state straight away to stop the send thread.
                                _connected = false;

                                if (sucessRecv)
                                    sucessRecv = false;
                                else
                                {
                                    // This is a repeat issue, sleep for 10 and try again

                                }
                                // Close and dispose the current UDP client object
                                CloseUDPClient();
                                // Open a new udp client object
                                OpenUDPClient();
                                if (eCommsError != null)
                                {
                                    if (_connected)
                                    {
                                        _log.Error(sClassName + "ThreadListenerHandler(EnhancedThread sender, object data) - Connection failed, reconnect sucessful.", ex);
                                        if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".ThreadListenerHandler(EnhancedThread sender, object data) - Connection failed, reconnect sucessful.", ex, null, null);
                                    }
                                    else
                                    {
                                        _log.Error(sClassName + "ThreadListenerHandler(EnhancedThread sender, object data) - Connection failed, reconnect failed.", ex);
                                        if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".ThreadListenerHandler(EnhancedThread sender, object data) - Connection failed, reconnect failed.", ex, null, null);
                                        Thread.Sleep(100);
                                    }
                                }
                                _pingNow = true;
                            }
                        }
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// This method will handle all communications from the listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadSenderHandler(EnhancedThread sender, object data)
        {
            byte[] bSendData = null;
            if (_udpClient != null)
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        if (!_connected || _udpClient == null)
                            Thread.Sleep(50);
                        else
                        {
                            #region If we are connected, see if there is some data to send. If there is, send it else sleep for 100
                            if (bSendData == null)
                            {
                                #region Get the next bit of data to send if the last was sent successfully
                                lock (_toListenerQ.SyncRoot)
                                {
                                    if (_toListenerQ.Count > 0)
                                    {
                                        bSendData = (byte[])_toListenerQ.Dequeue();
                                    }
                                }
                                #endregion
                            }
                            if (!sender.Stopping && bSendData != null)
                            {
                                #region Send the data to the server, handle any exceptions
                                try
                                {
                                    if (_logOutgoingComm)
                                        _log.Info("To Logistics Interface Data : " + System.Text.Encoding.ASCII.GetString(bSendData));
                                    //	Send the data to the listener.
                                    _udpClient.Send(bSendData, bSendData.Length, _remoteEndPoint);
                                    if (eCommsSendData != null) eCommsSendData.BeginInvoke(bSendData, null, null);
                                    // Pause after send - Comms failure will occure on the recv thread.
                                    Thread.Sleep(100);
                                    if (_connected)
                                        bSendData = null;
                                }
                                catch (Exception exSend)
                                {
                                    // Trigger and error on the receive thread.
                                    _udpClient = null;
                                    _log.Error(sClassName + "ThreadSenderHandler(EnhancedThread sender, object data)", exSend);
                                    if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".ThreadSenderHandler(EnhancedThread sender, object data)", exSend, null, null);
                                }
                                #endregion
                            }
                            else
                            {
                                #region There is nothing to send at the moment, take a break.
                                if (!sender.Stopping) Thread.Sleep(100);
                                #endregion
                            }
                            #endregion
                        }
                    }
                    catch (Exception ex)
                    {
                        _log.Error(sClassName + "ThreadSenderHandler(EnhancedThread sender, object data)", ex);
                        if (eCommsError != null) eCommsError.BeginInvoke(this.GetType().FullName + ".ThreadSenderHandler(EnhancedThread sender, object data)", ex, null, null);
                        bSendData = null;
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// This method will handle a check for a  communications timeout.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadCommsTimeoutHandler(EnhancedThread sender, object data)
        {
            int iRetryCounter = _listenerTimeoutSeconds;
            _lastListenerCommunication = DateTime.Now;
            if (_udpClient != null)
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        TimeSpan gap = DateTime.Now - _lastListenerCommunication;
                        if (gap.TotalSeconds > iRetryCounter)
                        {
                            if (!_connected)
                            {
                                iRetryCounter = _listenerTimeoutSeconds;
                                _lastListenerCommunication = DateTime.Now;
                            }
                            else
                            {
                                if (iRetryCounter <= 0)
                                {
                                    _log.Info(this.GetType().FullName + ".ThreadCommsTimeoutHandler(EnhancedThread sender, object data) - No comms for 120 seconds, sending re-connect to server.");
                                    if (eCommsTimeout != null)
                                        eCommsTimeout();
                                    lock (_toListenerQ.SyncRoot)
                                    {
                                        _toListenerQ.Enqueue(_loginPacket);
                                    }
                                    iRetryCounter = _listenerTimeoutSeconds;
                                }
                                else
                                    iRetryCounter--;
                            }
                        }
                        #region Sleep for 1 seconds
                        int sleepTime = 10;
                        while (!sender.Stopping && sleepTime > 0)
                        {
                            sleepTime--;
                            Thread.Sleep(100);
                        }
                        #endregion
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "ThreadCommsTimeoutHandler(EnhancedThread sender, object data)", ex);
                    }
                }
            }
            return null;
        }
        /// <summary>
        /// This method will handle pings to the server.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadPingHandler(EnhancedThread sender, object data)
        {
            if (_udpClient != null && _pingPacket != null)
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        #region Sleep for 30 seconds
                        int sleepTime = _pingTimer * 10;
                        while (!sender.Stopping && sleepTime > 0)
                        {
                            if (_pingNow)
                                sleepTime = 0;
                            else
                            {
                                sleepTime--;
                                Thread.Sleep(100);
                            }
                        }
                        // Wait for 200ms before sending if _ping now is set.
                        if (!sender.Stopping && _pingNow)
                        {
                            _pingNow = false;
                            Thread.Sleep(200);
                        }
                        #endregion
                        if (!sender.Stopping)
                        {
                            lock (_toListenerQ.SyncRoot)
                            {
                                _toListenerQ.Enqueue(_pingPacket);
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "ThreadPingHandler(EnhancedThread sender, object data)", ex);
                    }
                }
            }
            return null;
        }

        #endregion
    }
}

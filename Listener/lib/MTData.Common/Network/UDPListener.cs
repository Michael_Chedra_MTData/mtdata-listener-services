using System;
using System.Net;
using System.Net.Sockets;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;

namespace MTData.Common.Network
{
	/// <summary>
	/// Summary description for UDPListener.
	/// </summary>
	public class UDPListener
	{
        private const string sClassName = "MTData.Network.UDPListener.";
        private static ILog _log = LogManager.GetLogger(typeof(UDPListener));
		private bool bThreadRunning = false;
		private bool bSendThreadRunning = false;
		private EnhancedThread _listener;
		private EnhancedThread _sender;
		private QueueInterface _fromRemoteIPQ = null;
		private QueueInterface _toRemoteIPQ = null;
		private int iPort = 0;
		private UdpClient _udpClient = null;

		public UDPListener(int Port, QueueInterface fromRemoteIPQ)
		{
			try
			{
				CreateObject(Port, fromRemoteIPQ, null);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "UDPListener(int Port = " + Port + ", QueueInterface fromRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "UDPListener(int Port = " + Port + ", QueueInterface fromRemoteIPQ)", ex);
			}
		}

		public UDPListener(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)
		{
			try
			{
				CreateObject(Port, fromRemoteIPQ, toRemoteIPQ);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "UDPListener(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "UDPListener(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
			}
		}

		private void CreateObject(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)
		{
			try
			{
				if(_toRemoteIPQ != null)
				{
					_sender  = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(SendThreadHandler), _toRemoteIPQ);
					_sender.Name = "UDP.Sender.Thread";
					bSendThreadRunning = true;
					_sender.Start();
				}
				else
				{
                    _log.Info("Starting UDP listener on " + Convert.ToString(Port) + ".");
					iPort = Port;
					_fromRemoteIPQ = fromRemoteIPQ;
					_toRemoteIPQ = toRemoteIPQ;
					IPAddress oLocal = ((IPAddress[]) Dns.GetHostEntry("localhost").AddressList)[0];
					_udpClient = new UdpClient(iPort);

					_listener = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReadThreadHandler), _fromRemoteIPQ);
					_listener.Name = "UDP.Listener.Thread";
					bThreadRunning = true;
					_listener.Start();
				}

                _log.Info("Listening for UDP on port " + Convert.ToString(Port) + ".");
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "CreateObject(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "CreateObject(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
			}
		}
		
		public void Stop()
		{
			bThreadRunning = false;
			bSendThreadRunning = false;
		}

		
		public object SendThreadHandler(EnhancedThread sender, object data)
		{
			QueueInterface outboundQ = (QueueInterface)data;
			byte[] bSendData = null;
			int iRet = 0;

			while(bSendThreadRunning)
			{
				if (_udpClient != null)
				{
					lock(outboundQ.SyncRoot)
					{
						if (outboundQ.Count > 0)
							bSendData = (byte[]) outboundQ.Dequeue();
					}
					if (bSendData != null)
					{					
						try
						{
							iRet = _udpClient.Send(bSendData, bSendData.Length);
						}
						catch(Exception ex)
						{
                            _log.Info(String.Format("UDP Send Thread Handler Error : {0}, [{1}]", ex.Message, ex.StackTrace));
						}
						bSendData = null;
					}
				}
			}

            _log.Info("Stopped sending on UDP on port " + Convert.ToString(iPort) + ".");
			return null;
		}

		public object ReadThreadHandler(EnhancedThread sender, object data)
		{
			QueueInterface inboundQ = (QueueInterface)data;

			while(bThreadRunning)
			{
				if (_udpClient != null)
				{
					IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
					try
					{
						byte[] receivedBytes = _udpClient.Receive(ref remoteIP);

						lock(inboundQ.SyncRoot)
							inboundQ.Enqueue(receivedBytes);
					}
					catch(SocketException sockEx)
					{
						if (!sockEx.Message.StartsWith("A blocking operation was interrupted by a call to WSACancelBlockingCall"))
                            _log.Info(String.Format("UDP Listener Error : {0}, [{1}]", sockEx.Message, sockEx.StackTrace));
					}
					catch(Exception ex)
					{
                        _log.Info(String.Format("UDP Listener Error : {0}, [{1}]", ex.Message, ex.StackTrace));
					}
				}
			}

            _log.Info("Stopped listening for UDP on port " + Convert.ToString(iPort) + ".");
			return null;
		}

	}
}

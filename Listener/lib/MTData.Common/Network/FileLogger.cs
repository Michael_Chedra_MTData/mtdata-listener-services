using System;
using System.IO;
using System.Threading;
using MTData.Common.Threading;

namespace MTData.Network
{
	/// <summary>
	/// This file logger will ensure that all log messages are written to file.
	/// </summary>
	public class FileLogger : LoggingInterface
	{
		private string _directory;
		private string _prefixFormat;
		private string _name;
		private string _extension;
		private QueueInterface _outBound;
		private EnhancedThread _thread = null;

		private class QueueEntry
		{
			public string Message;
			public DateTime Occurrance;
 
			public QueueEntry(string message)
			{
				Message = message;
				Occurrance = DateTime.Now;
			}
		}

		public bool Active
		{
			get
			{
				if (_thread != null)
					return _thread.Running;
				else
					return false;
			}
		}

		public FileLogger(string directory, string prefixFormat, string name, string extension)
		{
			_directory = directory.Trim();
			if (_directory.Substring(_directory.Length - 1, 1) != "\\")
				_directory += "\\";

			SetPrefixFormat(prefixFormat);
			_name = name;
			_extension = extension;

			_outBound = new StandardQueue();
		
			EnhancedThread.EnhancedThreadStart handler = new EnhancedThread.EnhancedThreadStart(ThreadHandler);

			_thread = new EnhancedThread(handler, null);
			_thread.Start();
		}

		public void Stop()
		{
			_thread.Stop();
		}

		private object ThreadHandler(EnhancedThread sender, object data)
		{
			try
			{
				if (!System.IO.Directory.Exists(_directory))
					System.IO.Directory.CreateDirectory(_directory);

				int count = 0;
				QueueEntry entry = null;
				while (!sender.Stopping)
				{
					lock(_outBound.SyncRoot)
						count = _outBound.Count;

					
					if(count > 0)
					{
						System.IO.StreamWriter file = null;
						string currentFile = "";
						try
						{
							lock(_outBound.SyncRoot)
								entry = (QueueEntry)_outBound.Dequeue();
							count--;
							string fileName = Path.Combine(_directory, GetFileName(entry.Occurrance));
							if (fileName != currentFile)
							{
								if (file != null)
									file.Close();
								try
								{
									file = System.IO.File.AppendText(fileName);
									currentFile = fileName;
								}
								catch(Exception ex)
								{
									file = null;
									Console.WriteLine("Could not open Log file for append : FileName {0}\r\n{1}\r\nStack : {1}\r\n", fileName, ex.Message, ex.StackTrace);
								}
							}

							if (file != null)
								try
								{
									file.Write(entry.Message);
								}
								catch(Exception ex)
								{
									Console.WriteLine("Could not write log entry : FileName {0}\r\n{1}\r\nStack : {1}\r\n", fileName, ex.Message, ex.StackTrace);
								}
					
						}
						finally
						{
							if (file != null)
								file.Close();
							file = null;
						}
					}
					Thread.Sleep(100);
				}
			}
			catch(Exception ex)
			{
				Console.WriteLine("Could not prepare file logging functionality : {0}\r\nStack : {1}\r\n", ex.Message, ex.StackTrace);
			}
			return null;
		}

		private void SetPrefixFormat(string prefixFormat)
		{
			switch (prefixFormat)
			{
				case "DMY":
					_prefixFormat = "{0:dd-MM-yyyy} - {1}.{2}";
					break;
				case "DYM":
					_prefixFormat = "{0:dd-yyyy-MM} - {1}.{2}";
					break;
				case "MDY":
					_prefixFormat = "{0:MM-dd-yyyy} - {1}.{2}";
					break;
				case "MYD":
					_prefixFormat = "{0:MM-yyyy-dd} - {1}.{2}";
					break;
				case "YMD":
					_prefixFormat = "{0:yyyy-MM-dd} - {1}.{2}";
					break;
				case "YDM":
					_prefixFormat = "{0:yyyy-dd-MM} - {1}.{2}";
					break;
				default:
					_prefixFormat = "{0:dd-MM-yyyy} - {1}.{2}";
					break;
			}

		}

		private string GetFileName(DateTime occurrance)
		{
			return string.Format(_prefixFormat, occurrance, _name, _extension);
		}

		private void WriteString(string message)
		{
			if (_thread.Running)
				lock(_outBound.SyncRoot)
					_outBound.Enqueue(new QueueEntry(message + "\r\n"));
		}

		#region LoggingInterface Members

		public void LogInformation(string message)
		{
			WriteString(String.Format("{0:dd/MM/yyyy HH:mm:ss} Info  : {1}", DateTime.Now, message));
		}

		public void LogWarning(string message)
		{
			WriteString(String.Format("{0:dd/MM/yyyy HH:mm:ss} Warn  : {1}", DateTime.Now, message));
		}

		public void LogError(string message, Exception ex)
		{
			WriteString(String.Format("{0:dd/MM/yyyy HH:mm:ss} Error : {1} : Error : {2} Stack : {3}\r\n", DateTime.Now, message, ex.Message, ex.StackTrace));
		}

		#endregion
	}
}

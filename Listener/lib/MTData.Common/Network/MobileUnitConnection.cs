using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Common.Queues;
using MTData.Common.Threading;

namespace MTData.Common.Network
{
    // A delegate type for hooking up change errors on the connection.
    public delegate void ErrorEventHandler(object sender, byte type);


    public class MobileUnitConnection
    {
        #region Private Vars
        private const string sClassName = "MTData.Network.MobileUnitConnection.";
        private static ILog _log = LogManager.GetLogger(typeof(MobileUnitConnection));
        private bool bRunning = false;
        private object SyncRoot = null;
        private string _toAddress = "";
        private int _toPort = 0;
        private StandardQueue _toUDPQ = null;
        private StandardQueue _fromUDPQ = null;
        private EnhancedThread _SendThread = null;
        private EnhancedThread _RecvThread = null;
        private Socket _client = null;
        private ManualResetEvent _connectDone = null;
        private ManualResetEvent _sendDone = null;

        public event ErrorEventHandler ErrorDetected;


        #endregion

        public MobileUnitConnection(string toAddress, int toPort, StandardQueue toUDPQ, StandardQueue fromUDPQ)
        {
            try
            {
                SyncRoot = new object();
                _toAddress = toAddress;
                _toPort = toPort;
                _toUDPQ = toUDPQ;
                _fromUDPQ = fromUDPQ;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "MobileUnitConnection(string toAddress, int toPort, StandardQueue toUDPQ, StandardQueue fromUDPQ, LoggingInterface oLogging)", ex);
            }
        }

        #region Call backs
        private void objConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket oTempClient = (Socket)ar.AsyncState;
                // Complete the connection.
                oTempClient.EndConnect(ar);
                // Signal that the connection has been made.
                _connectDone.Set();
            }
            catch (SocketException sex)
            {
                _log.Error(sClassName + "objConnectCallback(IAsyncResult ar)  - ErrorCode <" + Convert.ToString(sex.ErrorCode) + "> NativeErrorCode <" + Convert.ToString(sex.NativeErrorCode) + "> Message <" + sex.Message + "> Source <" + sex.Source + "> StackTrace <" + sex.StackTrace + ">", sex);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "objConnectCallback(IAsyncResult ar)", ex);
            }
        }

        private void objSendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket oTempClient = (Socket)ar.AsyncState;
                // Complete sending the data to the remote device.
                int bytesSent = oTempClient.EndSend(ar);
                // Signal that all bytes have been sent.
                _sendDone.Set();
            }
            catch (SocketException sex)
            {
                _log.Error(sClassName + "objSendCallback(IAsyncResult ar)  - ErrorCode <" + Convert.ToString(sex.ErrorCode) + "> NativeErrorCode <" + Convert.ToString(sex.NativeErrorCode) + "> Message <" + sex.Message + "> Source <" + sex.Source + "> StackTrace <" + sex.StackTrace + ">", sex);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "objSendCallback(IAsyncResult ar)", ex);
            }
        }
        #endregion

        #region IsRunning / Start / Stop / Dispose functions
        public bool IsRunning
        {
            get
            {
                bool bRet = false;
                try
                {
                    lock (this.SyncRoot)
                        bRet = bRunning;
                }
                catch (System.Exception ex)
                {
                    _log.Error("Returning the running status", ex);
                }
                return bRet;
            }
        }

        private void CreateClientObject()
        {
            try
            {
                _connectDone = new ManualResetEvent(false);
                _sendDone = new ManualResetEvent(false);
                IPHostEntry ipHostInfo = Dns.GetHostEntry(_toAddress); //"host.contoso.com"
                IPAddress ipAddress = null;
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[X];
                        break;
                    }
                }
                IPEndPoint remoteEP = new IPEndPoint(ipAddress, _toPort);
                // Create a TCP/IP socket.
                _client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                // Connect to the remote endpoint.
                _client.BeginConnect(_toAddress, _toPort, new AsyncCallback(objConnectCallback), _client);
                _connectDone.WaitOne();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "CreateClientObject()", ex);
            }
        }

        public void Start()
        {
            try
            {
                // Clean up any existing objects
                Stop();
                // Create a new connection
                if (_client == null)
                    CreateClientObject();

                _SendThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(SendThreadHandler), null);
                _SendThread.Start();
                _RecvThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(RecvThreadHandler), null);
                _RecvThread.Start();

                lock (this.SyncRoot)
                    bRunning = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Start()", ex);
                Stop();
            }
        }

        public void Stop()
        {
            try
            {
                if (_SendThread != null)
                {
                    _SendThread.Stop();
                    Thread.Sleep(100);
                    _SendThread = null;
                }
                if (_RecvThread != null)
                {
                    _RecvThread.Stop();
                    Thread.Sleep(100);
                    _RecvThread = null;
                }
                if (_client != null)
                {
                    try
                    {
                        _client.Shutdown(SocketShutdown.Both);
                    }
                    finally
                    {
                        if (_client != null)
                        {
                            _client.Close();
                            _client.Dispose();
                            _client = null;
                        }
                    }
                }
                lock (this.SyncRoot)
                    bRunning = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }
        }

        public void Dispose()
        {
            Stop();
            SyncRoot = null;
        }
        #endregion

        #region Thread Handlers -SendThreadHandler and RecvThreadHandler
        private object RecvThreadHandler(EnhancedThread sender, object oData)
        {
            while (!sender.Stopping)
            {
                if (_client != null)
                {
                    IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
                    try
                    {
                        byte[] receivedBytes = new byte[8192];
                        int iRcvdBytes = _client.Receive(receivedBytes, 0, 8192, SocketFlags.None);
                        byte[] bData = new byte[iRcvdBytes];
                        for (int X = 0; X < iRcvdBytes; X++)
                            bData[X] = receivedBytes[X];
                        lock (_fromUDPQ.SyncRoot)
                            _fromUDPQ.Enqueue(bData);
                    }
                    catch (SocketException sex)
                    {
                        if (!sender.Stopping)
                        {
                            _log.Error(sClassName + "RecvThreadHandler(EnhancedThread sender, object oData) - ErrorCode <" + Convert.ToString(sex.ErrorCode) + "> NativeErrorCode <" + Convert.ToString(sex.NativeErrorCode) + "> Message <" + sex.Message + "> Source <" + sex.Source + "> StackTrace <" + sex.StackTrace + ">", sex);
                            Thread.Sleep(2000);		//if errored here then hold thread for 2 seconds so not to overload the logging
                            if (ErrorDetected != null)
                                ErrorDetected(this, 1);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (!sender.Stopping)
                        {
                            _log.Error(sClassName + "RecvThreadHandler(EnhancedThread sender, object oData)", ex);
                            Thread.Sleep(2000);		//if errored here then hold thread for 2 seconds so not to overload the logging
                            if (ErrorDetected != null)
                                ErrorDetected(this, 2);
                        }
                    }
                }
            }
            return null;
        }
        private object SendThreadHandler(EnhancedThread sender, object oData)
        {
            byte[] bData = null;
            try
            {
                while (!sender.Stopping)
                {
                    lock (_toUDPQ.SyncRoot)
                    {
                        if (_toUDPQ.Count > 0)
                            bData = (byte[])_toUDPQ.Dequeue();
                        else
                            bData = null;
                    }

                    if (bData == null)
                        Thread.Sleep(100);
                    else
                    {
                        // Begin sending the data to the remote device.
                        _client.BeginSend(bData, 0, bData.Length, 0, new AsyncCallback(objSendCallback), _client);
                        _sendDone.WaitOne();
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error("Stopping UDP Connection object", ex);
                if (ErrorDetected != null)
                {
                    ErrorDetected(this, 3);
                }
            }
            return null;
        }
        #endregion
    }
}

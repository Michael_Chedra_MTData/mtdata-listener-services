using System;
using System.Collections;

namespace MTData.Common.Network
{
	/// <summary>
	/// Standard implementation of the queue interface.
	/// </summary>
	public class StandardQueue : QueueInterface
	{
		private Queue _queue = null;
		private int _maxEntries = 300;

		public StandardQueue(int maxEntries)
		{
			_maxEntries = maxEntries;
			_queue = Queue.Synchronized(new Queue());
		}

		public StandardQueue() : this(300)
		{
		}

		#region QueueInterface Members

		public object SyncRoot
		{
			get
			{
				return _queue.SyncRoot;
			}
		}

		/// <summary>
		/// Enqueue the data. If the queue gets above a certain size,
		/// it will start dropping off entries as they will be out of date.
		/// </summary>
		/// <param name="entry"></param>
		public void Enqueue(object entry)
		{
			if ((_maxEntries > 0) && (_queue.Count >= _maxEntries))
				_queue.Dequeue();

			_queue.Enqueue(entry);
		}

		/// <summary>
		/// Dequeue the next entry in the queue.
		/// </summary>
		/// <returns></returns>
		public object Dequeue()
		{
			return _queue.Dequeue();
		}

		/// <summary>
		/// Only dequeue if they match
		/// </summary>
		/// <param name="entry"></param>
		/// <returns></returns>
		public object DequeueEx(object entry)
		{
			if (_queue.Peek() == entry)
				return _queue.Dequeue();
			else
				return null;
		}

		/// <summary>
		/// Return the item at the head of the queue.
		/// </summary>
		/// <returns></returns>
		public object Peek()
		{
			return _queue.Peek();
		}

		/// <summary>
		/// Return the queue count
		/// </summary>
		public int Count
		{
			get
			{
				return _queue.Count;
			}
		}

		#endregion
	}
}

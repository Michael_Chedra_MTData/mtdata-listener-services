using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;

namespace MTData.Common.Network
{
    public class ServerHandler
    {
        private const string sClassName = "MTData.Network.ServerHandler.";
        private static ILog _log = LogManager.GetLogger(typeof(ServerHandler));

        #region Events
        public delegate void CommsTimeoutEvent();
        public event CommsTimeoutEvent eCommsTimeout;
        #endregion
        #region Private Vars
        /// <summary>
        /// This is used for synchronisation
        /// </summary>
        private object _syncRoot = new object();
        /// <summary>
        /// UDP client for listener communications
        /// </summary>
        private UdpClient _udpClient = null;
        /// <summary>
        /// A boolean to indicate if the connection is currently active
        /// </summary>
        private bool _currentlyConnected = false;
        /// <summary>
        /// This is the Q that messages will be received on from the listener.
        /// This class only adds items to this q.
        /// </summary>
        private QueueInterface _fromListenerQ = null;
        /// <summary>
        /// This is the queue that messages will be sent to the listneer on
        /// This class is responsible for sending these packets to the listener.
        /// </summary>
        private QueueInterface _toListenerQ = null;
        /// <summary>
        /// This is the address of the listener (can be ip or host name).
        /// </summary>
        private string _liveUpdateAddress = "";
        /// <summary>
        /// This is the portnumber that the listener is listening on.
        /// </summary>
        private int _liveUpdatePortNumber = 0;
        /// <summary>
        /// This is the portnumber that the UDP will bind to locally.
        /// </summary>
        private int _iListenerLocalBindPort = 0;

        /// <summary>
        /// This is the thread that will process the messages from the listener.
        /// </summary>
        private EnhancedThread _threadListener = null;
        /// <summary>
        /// This is the thread that will process the queue to the listener.
        /// </summary>
        private EnhancedThread _threadSender = null;
        /// <summary>
        /// This is the thread that will check for a Comms Timeout
        /// </summary>
        private EnhancedThread _threadCommsTimeout = null;
        /// <summary>
        /// This is the date and time of the last listener communication
        /// </summary>
        private DateTime _lastListenerCommunication = DateTime.MinValue;
        /// <summary>
        /// This is the remote endpoint for the listener.
        /// </summary>
        private IPEndPoint _remoteEndPoint = null;
        /// <summary>
        /// If no data form listener for longer than this period, a reconnect will be initiated.
        /// </summary>
        private int _listenerTimeoutSeconds = 120;
        /// <summary>
        /// Should incomming data from the server be logged?
        /// </summary>
        private bool _bLogIncommingComms = false;
        /// <summary>
        /// Should outgoing data to the server be logged?
        /// </summary>
        private bool _bLogOutgoingComm = false;
        /// <summary>
        /// The connection string to send when connecting to the listener
        /// </summary>
        private string _sConnectionString;
        /// <summary>
        /// Default connection string
        /// </summary>
        private string _sDefaultConnectionString = "[Connection],[Version],[Fleets]";
        /// <summary>
        /// connection to the listener is for live updates only
        /// </summary>
        private string _sLiveUpdatesConnectionString = "ConnectV";
        /// <summary>
        /// connection to the listener is for live updates and 'cached' historic updates
        /// </summary>
        private string _sWithHistoryConnectionString = "ConnectWithHistoryV";
        /// <summary>
        /// connection to the listener is for non tracking updates only
        /// </summary>
        private string _sNonTrackingConnectionString = "ConnectNonTrackingV";
        /// <summary>
        /// the current connection type, false = live updates only, true for cached updates
        /// </summary>
        private bool _bConnectWithHistory = true;
        /// <summary>
        /// connect for non tracking only
        /// </summary>
        private bool _connectNonTracking = false;
        /// <summary>
        /// the fleets part of the connection string
        /// </summary>
        private string _sFleets;
        /// <summary>
        /// the version part of the connection string
        /// </summary>
        private string _sVersion;
        #endregion
        #region Public Properties
        /// <summary>
        /// A flag to indicate if the UDP connection is currently active
        /// </summary>
        public bool CurrentlyConnected
        {
            get
            {
                return _currentlyConnected;
            }
        }
        #endregion
        /// <summary>
        /// This will prepare the connection to the listener and listen on the udp port specified.
        /// </summary>
        /// <param name="liveUpdateAddress"></param>
        /// <param name="liveUpdatePortNumber"></param>
        public ServerHandler(
            string liveUpdateAddress,
            int liveUpdatePortNumber,
            int iListenerLocalBindPort,
            QueueInterface toListenerQ,
            QueueInterface fromListenerQ,
            int ListenerTimeoutSeconds,
            string sRegisterForFleets,
            string sConnectVersion,
            bool bLogIncommingComms,
            bool bLogOutgoingComms)
        {
            _bLogIncommingComms = bLogIncommingComms;
            _bLogOutgoingComm = bLogOutgoingComms;
            _liveUpdateAddress = liveUpdateAddress;
            _liveUpdatePortNumber = liveUpdatePortNumber;
            _iListenerLocalBindPort = iListenerLocalBindPort;
            _listenerTimeoutSeconds = ListenerTimeoutSeconds;
            _toListenerQ = toListenerQ;
            _fromListenerQ = fromListenerQ;

            _sFleets = sRegisterForFleets;
            _sVersion = sConnectVersion;
            CreateConnectionString();
        }

        /// <summary>
        /// get and set the registered fleets
        /// </summary>
        public string RegisteredFleets
        {
            get { return _sFleets; }
            set
            {
                _sFleets = value;
                CreateConnectionString();
                lock (_toListenerQ.SyncRoot)
                {
                    _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("Disconnect"));
                    _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes(_sConnectionString));
                }
            }
        }

        public bool ConnectionWithHistory
        {
            get { return _bConnectWithHistory; }
            set
            {
                _bConnectWithHistory = value;
                CreateConnectionString();
                lock (_toListenerQ.SyncRoot)
                {
                    _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("Disconnect"));
                    _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes(_sConnectionString));
                }
            }
        }

        public bool ConnectionNonTracking
        {
            get { return _connectNonTracking; }
            set
            {
                _connectNonTracking = value;
                CreateConnectionString();
                lock (_toListenerQ.SyncRoot)
                {
                    _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("Disconnect"));
                    _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes(_sConnectionString));
                }
            }
        }

        /// <summary>
        /// Start the instance of the class listening for inbound communications from the listener, 
        /// and messages on the toListenerQ.
        /// Send a Connect message to the listener.
        /// </summary>
        public void Start()
        {
            OpenUDPClient();
            _lastListenerCommunication = System.DateTime.Now;
            if (_udpClient == null)
                throw new Exception("Could not open Udp Client connection to the listener");
            _threadSender = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadSenderHandler), null);
            _threadSender.Start();
            _threadListener = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadListenerHandler), null);
            _threadListener.Start();
            if (_listenerTimeoutSeconds > 0)
            {
                _threadCommsTimeout = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadCommsTimeoutHandler), null);
                _threadCommsTimeout.Start();
            }

            lock (_toListenerQ.SyncRoot)
            {
                _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("Disconnect"));
            }
            Thread.Sleep(500);
            lock (_toListenerQ.SyncRoot)
            {
                _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes(_sConnectionString));
            }

        }

        /// <summary>
        /// Stop the instance from listening, with a disconnect message to the listener.
        /// </summary>
        public void Stop()
        {
            _log.Info("Stopping Server Connection Thread.");

            lock (_toListenerQ.SyncRoot)
            {
                _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("Disconnect"));
            }
            Thread.Sleep(500);

            try
            {
                if (_threadCommsTimeout != null)
                {
                    _threadCommsTimeout.Stop();
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (_threadListener != null)
                {
                    _threadListener.Stop();
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (_threadSender != null)
                {
                    _threadSender.Stop();
                    _threadSender = null;
                }
            }
            catch (System.Exception)
            {
            }
            CloseUDPClient();
        }

        /// <summary>
        /// This method will prepare the UdpClient for interaction with the listener.
        /// </summary>
        private void OpenUDPClient()
        {
            int portToUse = 0;


            IPAddress localAddress = ((IPAddress[])Dns.GetHostEntry("localhost").AddressList)[0];
            try
            {
                portToUse = _iListenerLocalBindPort;
                _log.Info("Connecting to Listener on " + _liveUpdateAddress + ":" + _liveUpdatePortNumber + " (using local port " + portToUse + ")");

                int attemptCount = 0;
                bool success = false;
                while (attemptCount < 5)
                {
                    // Bind the new socket to the port.
                    try
                    {
                        lock (_syncRoot)
                        { 
                            if (_udpClient != null)
                            {
                                try
                                {
                                    _udpClient.Close();
                                }
                                catch { }
                                finally
                                {
                                    _udpClient = null;
                                }
                            }
                            _udpClient = new UdpClient(portToUse);
                            if (portToUse == 0)
                            {
                                _log.InfoFormat("Local Bind Port {0}", ((IPEndPoint)_udpClient.Client.LocalEndPoint).Port);
                            }
                        }
                        success = true;
                        break;
                    }
                    catch (Exception ex)
                    {
                        _udpClient = null;
                        if (attemptCount == 4)
                            _log.Info(String.Format("ServerHandler:Socket Bind Error Port {0} - Bind Failure: {1} <{2}>", portToUse, ex.Message, ex.StackTrace));
                        else
                            _log.Info(String.Format("ServerHandler:Socket Bind Error Port {0} - Attempting port {1} : {2} <{3}>", portToUse, portToUse + 10, ex.Message, ex.StackTrace));
                        attemptCount++;
                        //set the port to use to zero, to let the udpClient use any free port
                        portToUse = 0;
                    }
                }
                if (!success)
                {
                    _udpClient = null;
                    _log.Info("ServerHandler:Socket Bind Error - Proceeding with open Port Number");
                    _currentlyConnected = false;
                }
                else
                    _currentlyConnected = true;
            }
            catch (Exception ex)
            {
                _udpClient = null;
                _log.Info(String.Format("ServerHandler:Socket Creation Error : {0} <{1}>", ex.Message, ex.StackTrace));
            }
        }


        /// <summary>
        /// This method will close the udp client.
        /// </summary>
        private void CloseUDPClient()
        {

            lock (_syncRoot)
            {
                if (_udpClient != null)
                    _udpClient.Close();
            }
            _currentlyConnected = false;
            _log.Info("Server Connection Closed.");
        }

        private bool _reconnectActive = false;

        private object ReconnectClient(EnhancedThread sender, object data)
        {
            if (_reconnectActive)
                return null;
            try
            {
                _reconnectActive = true;
                _log.Info("Reconnecting Client");
                CloseUDPClient();
                OpenUDPClient();
                CreateConnectionString();
                lock (_toListenerQ.SyncRoot)
                {
                    if (_toListenerQ.Count == 0)
                        _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes(_sConnectionString));
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "ServerHandler:ReconnectClient", ex);
            }
            finally
            {
                _reconnectActive = false;
            }
            return null;
        }

        /// <summary>
        /// create the connection string
        /// </summary>
        private void CreateConnectionString()
        {
            if (_bConnectWithHistory)
            {
                _sConnectionString = _sDefaultConnectionString.Replace("[Connection]", _sWithHistoryConnectionString);
            }
            else if (_connectNonTracking)
            {
                _sConnectionString = _sDefaultConnectionString.Replace("[Connection]", _sNonTrackingConnectionString);
            }
            else
            {
                _sConnectionString = _sDefaultConnectionString.Replace("[Connection]", _sLiveUpdatesConnectionString);
            }
            _sConnectionString = _sConnectionString.Replace("[Version]", _sVersion);
            _sConnectionString = _sConnectionString.Replace("[Fleets]", _sFleets);
        }

        private IPEndPoint CreateRemoteEndPoint()
        {
            IPEndPoint endPoint = null;
            try
            {
                System.Net.IPAddress ip = System.Net.IPAddress.Parse(_liveUpdateAddress);
                endPoint = new IPEndPoint(ip, _liveUpdatePortNumber);
            }
            catch
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(_liveUpdateAddress); //"host.contoso.com"
                IPAddress ipAddress = null;
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[X];
                        break;
                    }
                }
                if (ipAddress != null)
                {
                    endPoint = new IPEndPoint(ipAddress, _liveUpdatePortNumber);
                }
            }
            return endPoint;
        }

        #region Threads
        /// <summary>
        /// This method will handle all communications from the listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadListenerHandler(EnhancedThread sender, object data)
        {
            string stringData = "";
            bool inRecovery = false;
            if (_udpClient != null)
            {
                while (!sender.Stopping)
                {
                    if (_currentlyConnected && !_reconnectActive)
                    {
                        try
                        {
                            if (_remoteEndPoint == null)
                            {
                                _remoteEndPoint = CreateRemoteEndPoint();
                            }
                            IPEndPoint remoteEndPoint = _remoteEndPoint;
                            byte[] dataIn = _udpClient.Receive(ref remoteEndPoint);
                            if (inRecovery)
                            {
                                inRecovery = false;
                                _log.Info("Connection To Server Re-Established.");
                            }
                            if (dataIn != null)
                            {
                                if (_bLogIncommingComms)
                                {
                                    stringData = System.Text.Encoding.ASCII.GetString(dataIn);
                                    _log.Info("Incomming Server Data : " + stringData);
                                }
                                _lastListenerCommunication = DateTime.Now;

                                if (System.Text.ASCIIEncoding.ASCII.GetString(dataIn, 0, 4).ToUpper() == "TEST")
                                {
                                    lock (_toListenerQ.SyncRoot)
                                    {
                                        _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("ALIVE"));
                                    }
                                }
                                else
                                    _fromListenerQ.Enqueue(dataIn);
                            }
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.IndexOf("A blocking operation was interrupted by a call to WSACancelBlockingCall") < 0
                                && ex.Message.IndexOf("Cannot access a disposed object.") < 0
                                && ex.Message.IndexOf("An existing connection was forcibly closed by the remote host") < 0)
                                _log.Error(sClassName + "ServerHandler:ThreadListener", ex);
                            if (!inRecovery)
                            {
                                _log.Info("Connection Closed by Server");
                            }
                            if (!sender.Stopping)
                            {
                                _currentlyConnected = false;
                                // Make a 5 second pause between attempts
                                if (inRecovery)
                                {
                                    int iRecoveryAttemptTimer = 0;
                                    while (!sender.Stopping && iRecoveryAttemptTimer < 10)
                                    {
                                        iRecoveryAttemptTimer++;
                                        Thread.Sleep(500);
                                    }
                                }
                                else
                                {
                                    inRecovery = true;
                                    //reset the remote end point
                                    _remoteEndPoint = null;
                                }
                                if (!sender.Stopping)
                                {
                                    EnhancedThread _threadReconnect = null;
                                    _threadReconnect = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReconnectClient), null);
                                    _threadReconnect.Start();
                                }
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            return null;
        }

        /// <summary>
        /// This method will handle all communications from the listener.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadSenderHandler(EnhancedThread sender, object data)
        {
            byte[] bSendData = null;
            string stringData = "";
            if (_udpClient != null)
            {
                while (!sender.Stopping)
                {
                    if (_currentlyConnected)
                    {
                        try
                        {
                            if (_udpClient != null)
                            {
                                lock (_toListenerQ.SyncRoot)
                                {
                                    if (_toListenerQ.Count > 0)
                                    {
                                        bSendData = (byte[])_toListenerQ.Dequeue();
                                    }
                                }
                            }
                            if (bSendData != null)
                            {
                                if (_bLogOutgoingComm)
                                {
                                    stringData = System.Text.Encoding.ASCII.GetString(bSendData);
                                    _log.Info("Outgoing Server Data : " + stringData);
                                }
                                //	Send the data to the listener.
                                _udpClient.Send(bSendData, bSendData.Length, _liveUpdateAddress, _liveUpdatePortNumber);
                                bSendData = null;
                            }
                            Thread.Sleep(100);
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message.IndexOf("A blocking operation was interrupted by a call to WSACancelBlockingCall") < 0)
                                _log.Error(sClassName + "ServerHandler:ThreadListener", ex);
                            bSendData = null;
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            return null;
        }


        /// <summary>
        /// This method will handle a check for a  communications timeout.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        private object ThreadCommsTimeoutHandler(EnhancedThread sender, object data)
        {
            if (_udpClient != null)
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        if (!_currentlyConnected)
                            _lastListenerCommunication = DateTime.Now;
                        else
                        {
                            TimeSpan gap = DateTime.Now - _lastListenerCommunication;
                            if (gap.TotalSeconds > _listenerTimeoutSeconds)
                            {
                                _log.Warn(string.Format("SenderHandler:No communication from Listener in {0} seconds. Attempting Reconnect", _listenerTimeoutSeconds));
                                if (eCommsTimeout != null)
                                    eCommsTimeout();

                                if (!sender.Stopping)
                                {
                                    _currentlyConnected = false;
                                    EnhancedThread _threadReconnect = null;
                                    _threadReconnect = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReconnectClient), null);
                                    _threadReconnect.Start();
                                    _lastListenerCommunication = DateTime.Now;
                                }
                            }
                            else if ((int)gap.TotalSeconds % 30 == 0 && gap.TotalSeconds > 1)
                            {
								//not heard anything for 30 seconds, send a ping to check comms still open
                                lock (_toListenerQ.SyncRoot)
                                {
                                    if (_toListenerQ.Count == 0)
                                        _toListenerQ.Enqueue(System.Text.Encoding.ASCII.GetBytes("PING"));
                                }
                            }
                        }
                        Thread.Sleep(1000);
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "ThreadCommsTimeoutHandler(EnhancedThread sender, object data)", ex);
                    }
                }
            }
            return null;
        }
        #endregion
    }
}

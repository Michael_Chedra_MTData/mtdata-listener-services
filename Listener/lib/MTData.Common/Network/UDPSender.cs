using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Text;
using log4net;


namespace MTData.Common.Network
{

    public delegate void UDPSenderDelegate(string message);

    /// <summary>
    /// Summary description for UDPSender.
    /// </summary>
    public class UDPSender
    {
        private const string sClassName = "MTData.Network.UDPSender.";
        private static ILog _log = LogManager.GetLogger(typeof(UDPSender));

        #region Non-static Functions
        private ManualResetEvent _connectDone = null;
        private ManualResetEvent _sendDone = null;
        private Socket _client = null;
        private string _sRemoteHost = "";
        private int _iRemotePort = 0;

        public string IPAddress { get { return _sRemoteHost; } }
        public int Port { get { return _iRemotePort; } }

        public UDPSender(string remoteHost, int port)
        {
            try
            {
                _sRemoteHost = remoteHost;
                _iRemotePort = port;
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "UDPSender(string remoteHost = '" + remoteHost + "', int port = " + port + ")", ex);
                throw new System.Exception(sClassName + "UDPSender(string remoteHost = '" + remoteHost + "', int port = " + port + ")", ex);
            }
        }

        public void SendMsg(byte[] byteData)
        {
            try
            {
                if (_client == null)
                    CreateClientObject();

                // Begin sending the data to the remote device.
                _client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(objSendCallback), _client);
                _sendDone.WaitOne();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendMsg(byte[] byteData) - Sending UDP message to " + _sRemoteHost + ":" + Convert.ToString(_iRemotePort), ex);
            }
        }

        public void SendMsg(string sMsg)
        {
            try
            {
                if (_client == null)
                    CreateClientObject();

                // Convert the string data to byte data using ASCII encoding.
                byte[] byteData = Encoding.ASCII.GetBytes(sMsg);

                // Begin sending the data to the remote device.
                _client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(objSendCallback), _client);
                _sendDone.WaitOne();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendMsg(string sMsg) - Sending UDP message to " + _sRemoteHost + ":" + Convert.ToString(_iRemotePort), ex);
            }
        }

        public void Dispose()
        {
            try
            {
                _client.Shutdown(SocketShutdown.Both);
                _client.Close();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Dispose()", ex);
            }
        }

        private void CreateClientObject()
        {
            IPHostEntry ipHostInfo = null;
            IPAddress ipAddress = null;
            IPEndPoint remoteEP = null;
            byte[] bData = null;

            try
            {
                _connectDone = new ManualResetEvent(false);
                _sendDone = new ManualResetEvent(false);
                ipHostInfo = Dns.GetHostEntry(_sRemoteHost);
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[X];
                        break;
                    }
                }
                bData = ipAddress.GetAddressBytes();

                remoteEP = new IPEndPoint(ipAddress, _iRemotePort);
                _client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                _client.BeginConnect(_sRemoteHost, _iRemotePort, new AsyncCallback(objConnectCallback), _client);
                _connectDone.WaitOne();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "CreateClientObject()", ex);
            }
        }

        private void objConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket oTempClient = (Socket)ar.AsyncState;
                // Complete the connection.
                oTempClient.EndConnect(ar);
                // Signal that the connection has been made.
                _connectDone.Set();
            }
            catch (SocketException sex)
            {
                _log.Error(sClassName + "objConnectCallback(IAsyncResult ar) - Error in ConnectCallback (SocketException) ErrorCode <" + Convert.ToString(sex.ErrorCode) + "> NativeErrorCode <" + Convert.ToString(sex.NativeErrorCode) + "> Message <" + sex.Message + "> Source <" + sex.Source + "> StackTrace <" + sex.StackTrace + ">");
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "objConnectCallback(IAsyncResult ar)", ex);
            }
        }

        private void objSendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket oTempClient = (Socket)ar.AsyncState;
                // Complete sending the data to the remote device.
                int bytesSent = oTempClient.EndSend(ar);
                // Signal that all bytes have been sent.
                _sendDone.Set();
            }
            catch (SocketException ex)
            {
                _log.Error(sClassName + "objSendCallback(IAsyncResult ar) - Error in SendCallback", ex);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "objSendCallback(IAsyncResult ar)", ex);
            }
        }
        #endregion
        #region Static Functions
        private static ManualResetEvent connectDone = new ManualResetEvent(false);
        private static ManualResetEvent sendDone = new ManualResetEvent(false);

        public static void UDPClientSendMessage(IPEndPoint remoteEP, string message)
        {
            try
            {
                // Create a socket.
                Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
                // Connect to the remote endpoint.
                client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                connectDone.WaitOne();
                // Send test data to the remote device.
                Send(client, message);
                sendDone.WaitOne();
                // Release the socket.
                client.Shutdown(SocketShutdown.Both);
                client.Close();
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "UDPClientSendMessage(IPEndPoint remoteEP, string message)", ex);
            }
        }

        public static void UDPClientSendMessage(string remoteHost, int port, string message)
        {
            // Connect to a remote device.
            try
            {
                // Establish the remote endpoint for the socket.
                // The name of the 
                // remote device is "host.contoso.com".
                IPHostEntry ipHostInfo = Dns.GetHostEntry(remoteHost); //"host.contoso.com"
                IPAddress ipAddress = null;
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[X];
                        break;
                    }
                }
                if (ipAddress != null)
                {
                    IPEndPoint remoteEP = new IPEndPoint(ipAddress, port);

                    // Create a TCP/IP socket.
                    Socket client = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

                    // Connect to the remote endpoint.
                    client.BeginConnect(remoteEP, new AsyncCallback(ConnectCallback), client);
                    connectDone.WaitOne();

                    // Send test data to the remote device.
                    Send(client, message); //"This is a test<EOF>");
                    sendDone.WaitOne();

                    // Release the socket.
                    client.Shutdown(SocketShutdown.Both);
                    client.Close();
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "UDPClientSendMessage(string remoteHost, int port, string message)", ex);
            }
        }

        private static void ConnectCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;

                // Complete the connection.
                client.EndConnect(ar);

                // Signal that the connection has been made.
                connectDone.Set();
            }
            catch (SocketException sex)
            {
                _log.Info("Error in ConnectCallback (SocketException) ErrorCode <" + Convert.ToString(sex.ErrorCode) + "> NativeErrorCode <" + Convert.ToString(sex.NativeErrorCode) + "> Message <" + sex.Message + "> Source <" + sex.Source + "> StackTrace <" + sex.StackTrace + ">");
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "ConnectCallback(IAsyncResult ar)", ex);
            }
        }

        private static void Send(Socket client, String data)
        {
            // Convert the string data to byte data using ASCII encoding.
            byte[] byteData = Encoding.ASCII.GetBytes(data);

            // Begin sending the data to the remote device.
            client.BeginSend(byteData, 0, byteData.Length, 0, new AsyncCallback(SendCallback), client);
        }

        private static void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket client = (Socket)ar.AsyncState;
                // Complete sending the data to the remote device.
                int bytesSent = client.EndSend(ar);
                // Signal that all bytes have been sent.
                sendDone.Set();
            }
            catch (SocketException sex)
            {
                _log.Info("Error in SendCallback (SocketException) ErrorCode <" + Convert.ToString(sex.ErrorCode) + "> NativeErrorCode <" + Convert.ToString(sex.NativeErrorCode) + "> Message <" + sex.Message + "> Source <" + sex.Source + "> StackTrace <" + sex.StackTrace + ">");
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "SendCallback(IAsyncResult ar)", ex);
            }
        }
        #endregion
    }
}
using System;

namespace MTData.Network
{
	/// <summary>
	/// Summary description for LoggingInterface.
	/// </summary>
	public interface LoggingInterface
	{
		void LogInformation(string message);
		void LogWarning(string message);
		void LogError(string message, Exception ex);
	}
}

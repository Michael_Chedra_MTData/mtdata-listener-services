using log4net;
using MTData.Common.Threading;
using System;
using System.Threading;
using System.Net.Sockets;
using MTData.Common.Interface;

namespace MTData.Common.Network
{
    /// <summary>
    /// This class acts as a wrapper to a TCP connection by providing synchronised queues for inbound and outbound communication.
    /// The connection will raise an event ( ConnectionClosedHandler) when the connection is closed.
    /// On closing, the closs will send a disconnect message (sDisconnectMsg) if the flag (_bSendDisconnectMsg) is set to true
    /// </summary>
    public class TCPConnection
    {
        private const string sClassName = "MTData.Network.TCPConnection.";
        private static ILog _log = LogManager.GetLogger(typeof(TCPConnection));

        /// <summary>
        /// This is the types of format available for logging.
        /// </summary>
        public enum LoggingFormat
        {
            Hex,
            Decimal
        }

        #region Public Interface
        // Close Event
        public delegate void ConnectionClosedHandler(TCPConnection sender);
        public delegate void ConnectionTCPMessageSent(TCPConnection sender);
        #endregion

        #region Private Vars
        // Event Delegate
        private event ConnectionClosedHandler _connectionClosed;
        public event ConnectionTCPMessageSent eMsgSent;
        // Private Vars
        private bool _bLogData = true;
        private TcpClient _client = null;
        private EnhancedThread _reader = null;
        private QueueInterface _fromRemoteIPQ = null;
        private QueueInterface _toRemoteIPQ = null;
        private string _description = "";
        private int _packetWaitPeriod = 10;
        private int _sleepWaitPeriod = 50;
        private int iPort = 0;
        private byte[] disconnectMsg = null;
        private bool _bSendDisconnectMsg = false;
        private bool _bCheckForDisconnectMsg = false;
        private LoggingFormat _loggingFormat = LoggingFormat.Decimal;
        private string _sRemoteAddress = "";
        #endregion

        #region Constructor / Close
        public void StandardConstructor(TcpClient client, string description, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ,
            ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod,
            string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose)
        {

            #region Setup Disconnect Options.
            // Check if a disconnect message was provided
            if (DisconnectMsg.Length > 0)
            {
                // If it was, then setup the client to send it on close.
                disconnectMsg = System.Text.ASCIIEncoding.ASCII.GetBytes(DisconnectMsg);
                _bSendDisconnectMsg = SendDisconnectMsgOnClose;
                // Set the flag to check if the client should monitor incomming data for a disconnect message
                _bCheckForDisconnectMsg = CheckForDisconnectMsg;
            }
            else
            {
                // If no disconnect string is specified, then non can be send on disconnect and there is nothing to monitor for in the incomming data.
                disconnectMsg = null;
                _bSendDisconnectMsg = false;
                _bCheckForDisconnectMsg = false;
            }
            #endregion

            // Create a standard TCP connection client.
            CreateClient(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod);
        }

        #region Constructor overides
        public TCPConnection(TcpClient client, string description, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ,
            ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod)
        {
            StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, "", false, false);
        }

        public TCPConnection(TcpClient client, string description, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ,
            ConnectionClosedHandler connectionClosed, int packetSize, int packetWaitPeriod, int sleepWaitPeriod,
            string DisconnectMsg, bool CheckForDisconnectMsg, bool SendDisconnectMsgOnClose)
        {
            StandardConstructor(client, description, fromRemoteIPQ, toRemoteIPQ, connectionClosed, packetSize, packetWaitPeriod, sleepWaitPeriod, DisconnectMsg, CheckForDisconnectMsg, SendDisconnectMsgOnClose);
        }
        #endregion

        private void CreateClient(TcpClient client,
            string description,
            QueueInterface fromRemoteIPQ,
            QueueInterface toRemoteIPQ,
            ConnectionClosedHandler connectionClosed,
            int packetSize,
            int packetWaitPeriod,
            int sleepWaitPeriod)
        {
            _packetWaitPeriod = packetWaitPeriod;
            _sleepWaitPeriod = sleepWaitPeriod;

            _description = description;
            if (connectionClosed != null)
                _connectionClosed += connectionClosed;
            _fromRemoteIPQ = fromRemoteIPQ;
            _toRemoteIPQ = toRemoteIPQ;

            _client = client;
            _reader = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ThreadHandler), packetSize);
            _reader.Name = "TCPConnection:" + description;
            _reader.Start();
        }

        public void Close()
        {
            // Close is used to stop a client connection.
            if (_bSendDisconnectMsg && disconnectMsg != null)
            {
                // Enqueue the disconnect message.
                lock (_toRemoteIPQ.SyncRoot)
                {
                    _toRemoteIPQ.Enqueue(disconnectMsg);
                }
                // Wait for the disconnect to be sent.
                Thread.Sleep(500);
            }
            // Close the reader.
            _reader.Stop();

            // close tcp client connection
            if (_client != null)
                _client.Close();
            _client = null;

        }
        #endregion

        #region Public Properties
        public QueueInterface fromRemoteIPQ
        {
            get
            {
                return _fromRemoteIPQ;
            }
        }

        public QueueInterface toRemoteIPQ
        {
            get
            {
                return _toRemoteIPQ;
            }
        }

        public int HashCode
        {
            get
            {
                int iRet = 0;
                if (_client != null)
                    iRet = _client.GetHashCode();
                return iRet;
            }
        }

        public bool Active
        {
            get
            {
                bool bRet = false;
                if (_reader.State == ThreadState.Background || _reader.State == ThreadState.Running
                    || _reader.State == ThreadState.WaitSleepJoin)
                    bRet = true;
                return bRet;
            }
        }

        public int Port
        {
            set
            {
                if (this.Active)
                    iPort = value;
            }
            get
            {
                int iRet = 0;
                if (this.Active)
                    iRet = iPort;
                return iRet;
            }
        }

        public bool LogData
        {
            set
            {
                _bLogData = value;
            }
            get
            {
                return _bLogData;
            }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public string RemoteAddress
        {
            get { return _sRemoteAddress; }
            set { _sRemoteAddress = value; }
        }
        /// <summary>
        /// This is the format with which to log the output data.
        /// </summary>
        public LoggingFormat LogFormat
        {
            get { return _loggingFormat; }
            set { _loggingFormat = value; }
        }

        #endregion

        #region Internal Methods

        /// <summary>
        /// Encode the output buffer in the preferred format.
        /// The default is decimal format.
        /// </summary>
        /// <param name="buffer"></param>
        /// <returns></returns>
        private string EncodeBufferForLogging(byte[] buffer)
        {
            if (_loggingFormat == LoggingFormat.Decimal)
                return MTData.Common.Utilities.Util.EncodeByteArrayAsDecimal(buffer, 0, buffer.Length);
            else
                return MTData.Common.Utilities.Util.EncodeByteArrayAsHex(buffer, 0, buffer.Length);
        }

        #endregion

        #region Thread Handler
        private object ThreadHandler(EnhancedThread sender, object data)
        {
            bool bDisconnected = false;
            int packetSize = (int)data;
            byte[] readBuffer = new byte[packetSize];
            byte[] writeBuffer = null;

            NetworkStream stream = _client.GetStream();
            try
            {
                if (_bLogData)
                    _log.Info(string.Format("{0} : Connected", _description));

                #region Main Loop
                while (!sender.Stopping && !bDisconnected)
                {
                    bool NOP = true;

                    if (_fromRemoteIPQ != null)
                    {
                        #region Handle the read operations for any pending data
                        if (stream.DataAvailable)
                        {
                            try
                            {
                                NOP = false;
                                int readCount = stream.Read(readBuffer, 0, packetSize);
                                if (readCount > 0)
                                {
                                    byte[] bytesRead = new byte[readCount];
                                    for (int loop = 0; loop < readCount; loop++)
                                        bytesRead[loop] = readBuffer[loop];

                                    if (_bLogData)
                                        _log.Info(string.Format("{0} : Reading {1} bytes : {2}", _description, readCount, EncodeBufferForLogging(bytesRead)));

                                    bDisconnected = false;
                                    if (_bCheckForDisconnectMsg)
                                    {
                                        if (disconnectMsg != null && bytesRead != null)
                                        {
                                            if (bytesRead.Length == disconnectMsg.Length)
                                            {
                                                for (int X = 0; X < disconnectMsg.Length; X++)
                                                {
                                                    // If this byte is not the same as the corresponding byte is the disconnect message
                                                    if (bytesRead[X] != disconnectMsg[X]) break;

                                                    // If this is the last byte and this and all previous match the disconnect message
                                                    if (X == disconnectMsg.Length - 1)
                                                    {
                                                        _log.Info("Disconnect message recieved");
                                                        // We just recieved a disconnect message from the other end.
                                                        bDisconnected = true;
                                                    }
                                                }
                                            }
                                        }
                                    }

                                    // If we weren't just send a disconnect message
                                    if (!bDisconnected)
                                    {
                                        lock (_fromRemoteIPQ.SyncRoot)
                                            _fromRemoteIPQ.Enqueue(bytesRead);
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data)", ex);
                            }
                        }
                        #endregion
                    }

                    if (_toRemoteIPQ != null && !bDisconnected)
                    {
                        if (_client == null)
                        {
                            Console.WriteLine("Client has disconnected.");
                        }
                        #region Handle any pending write operations
                        lock (_toRemoteIPQ.SyncRoot)
                        {
                            if (_toRemoteIPQ.Count > 0)
                                writeBuffer = (byte[])_toRemoteIPQ.Peek();
                            else
                                writeBuffer = null;
                        }

                        try
                        {

                            if (writeBuffer != null)
                            {
                                NOP = false;
                                if (_bLogData)
                                    _log.Info(string.Format("{0} : Writing {1} bytes : {2}", _description, writeBuffer.Length, EncodeBufferForLogging(writeBuffer)));
                                stream.Write(writeBuffer, 0, writeBuffer.Length);
                                //stream.Flush();
                                lock (_toRemoteIPQ.SyncRoot)
                                    _toRemoteIPQ.DequeueEx(writeBuffer);

                                if (eMsgSent != null)
                                    eMsgSent(this);
                            }
                        }
                        catch (System.IO.IOException ioEx)
                        {
                            _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data)", ioEx);
                            sender.Stop();
                        }
                        catch (Exception ex)
                        {
                            _log.Error(sClassName + "ThreadHandler(EnhancedThread sender, object data)", ex);
                        }
                        #endregion
                    }

                    if (!bDisconnected)
                    {
                        #region Sleep
                        if (NOP)
                            Thread.Sleep(_sleepWaitPeriod);
                        else
                            if (_packetWaitPeriod > 0)
                                Thread.Sleep(_packetWaitPeriod);
                        #endregion
                    }
                }
                #endregion
            }
            finally
            {
                stream.Close();
            }
            if (_bLogData)
                _log.Info(string.Format("{0} : Closing Connection", _description));

            if (_client != null)
                _client.Close();
            if (_connectionClosed != null)
                _connectionClosed(this);
            return null;
        }
        #endregion

    }
}

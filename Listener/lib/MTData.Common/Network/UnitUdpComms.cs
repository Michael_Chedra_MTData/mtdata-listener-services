﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;
using MTData.Common.Threading;
using MTData.Common.Queues;

namespace MTData.Common.Network
{
    public class UnitUdpComms
    {
        private readonly ILog _log = LogManager.GetLogger(typeof(UnitUdpComms));
        private readonly IPEndPoint _remoteAddress;
        private UdpClient _udpClient;
        private EnhancedThread _sendThread;
        private readonly StandardQueue _toUdpQ;
        private readonly StandardQueue _fromUdpQ;

        private bool _bThreadRunning;

        public UnitUdpComms(string remoteIp, int remotePort, StandardQueue toUdpq, StandardQueue fromUdpq)
		{
            _remoteAddress = CreateRemoteEndPoint(remoteIp, remotePort);
			_toUdpQ = toUdpq;
			_fromUdpQ = fromUdpq;
        }

        public void Start()
		{
			try
			{
                _udpClient = new UdpClient(0);
                _log.DebugFormat("Local Bind Port {0}", ((IPEndPoint)_udpClient.Client.LocalEndPoint).Port);

			    // start the sending thread
			    _sendThread = new EnhancedThread(SendThreadHandler, null)
			    {
			        Name = "UDP.Sender.Thread"
			    };

			    _log.Info("Starting the _sender in UnitUdpComms");
			    _sendThread.Start();

                // Start receiving
                _udpClient.BeginReceive(Receive, null);

                _bThreadRunning = true;
            }
			catch(Exception ex)
			{
				_log.Error(ex);
			}
		}

		public void Stop()
		{
            try
            {
                _sendThread.Stop();
                _udpClient.Close();
                _udpClient = null;
            }
            catch (Exception exp)
            {
                _log.Error("Error stopping", exp);
            }
        }

        private object SendThreadHandler(EnhancedThread sender, object oData)
        {
            while (!sender.Stopping)
            {
                try
                {
                    if (_udpClient == null)
                    {
                        _log.Info("_udpClient is null");
                        Thread.Sleep(10000);
                        continue;
                    }

                    byte[] data = null;
                    lock (_toUdpQ.SyncRoot)
                    {
                        if (_toUdpQ.Count > 0)
                        {
                            data = (byte[])_toUdpQ.Dequeue();
                        }
                    }

                    if (data != null)
                    {
                        _udpClient.Send(data, data.Length, _remoteAddress);
                    }
                    else
                    {
                        Thread.Sleep(1);
                    }
                }
                catch (Exception exp)
                {
                    _log.Error(exp);

                    Thread.Sleep(100);
                }
            }

            return null;
        }

        private void Receive(IAsyncResult res)
        {
            if (!_bThreadRunning)
            {
                return;
            }

            try
            {
                var remoteIp = new IPEndPoint(IPAddress.Any, 0);

                var data = _udpClient.EndReceive(res, ref remoteIp);

                lock (_fromUdpQ.SyncRoot)
                {
                    _fromUdpQ.Enqueue(data);
                }

                // Continue
                _udpClient.BeginReceive(Receive, null);
            }
            catch (Exception exp)
            {
                _log.Error(exp);
            }
        }

        private IPEndPoint CreateRemoteEndPoint(string remoteIP, int remotePort)
        {
            IPEndPoint endPoint = null;
            try
            {
                IPAddress ip = IPAddress.Parse(remoteIP);
                endPoint = new IPEndPoint(ip, remotePort);
            }
            catch
            {
                IPHostEntry ipHostInfo = Dns.GetHostEntry(remoteIP);
                IPAddress ipAddress = null;
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ipAddress = ipHostInfo.AddressList[X];
                        break;
                    }
                }
                if (ipAddress != null)
                {
                    endPoint = new IPEndPoint(ipAddress, remotePort);
                }
            }
            return endPoint;
        }



    }
}

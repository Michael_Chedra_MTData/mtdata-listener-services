﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using log4net;

namespace MTData.Common.Network
{
    public class TCPClientAsync
    {
        private static ILog _log = LogManager.GetLogger(typeof(TCPClientAsync));
        private IPEndPoint _ep;
        private Socket _tcpClient;
        private byte[] _buffer;
        private int _bufferSize;
        private int _connectionTimeout;
        private bool _listenerRunning;
        private bool _listenerStopped;
        private Thread _threadListener;
        private bool _senderRunning;
        private bool _senderStopped;
        private Thread _threadSender;

        /// <summary>
        /// Data received on the TCP server is queued here as TCPAsyncData object (ConnectionID/Data)
        /// </summary>
        public bool Connected { get; set; }
        /// <summary>
        /// Data received on the TCP server is queued here as TCPAsyncData object (ConnectionID/Data)
        /// </summary>
        public Queue<TCPAsyncData> RecvDataQ { get; set; }
        /// <summary>
        /// Data to send to a connection should be added to this queue (ConnectionID/Data)
        /// if connection ID = -1, the data will be sent to the first connected connection.
        /// if the connection ID is not present, the data will be requeued on UnSentByteDataQ for the contorolling
        /// application to deal with.
        /// </summary>
        public Queue<TCPAsyncData> SendDataQ { get; set; }
        /// <summary>
        /// Default constructor
        /// </summary>
        public TCPClientAsync()
        {
            RecvDataQ = new Queue<TCPAsyncData>();
            SendDataQ = new Queue<TCPAsyncData>();
            Connected = false;
        }
        /// <summary>
        /// Start a new TCP connection to bindAddress:inPort
        /// </summary>
        /// <param name="bindAddress"></param>
        /// <param name="inPort"></param>
        /// <param name="maxPendingConns"></param>
        /// <param name="bufferSize"></param>
        public void StartConnection(string bindAddress, int port, int bufferSize = 4096, int connectionTimeout = 500)
        {
            try
            {
                _bufferSize = bufferSize;
                _connectionTimeout = connectionTimeout;
                System.Net.IPAddress ip = TCPSeverAsync.GetIPFromString(bindAddress);
                if (ip == null)
                    throw new System.Exception("Unable to determine bind IP from provided address '" + bindAddress + "'");
                _ep = TCPSeverAsync.GetEndpoint(bindAddress, port);
                // Try and connect, if not then try again when there is some data to send.
                try
                {
                    _tcpClient = GetConnection(_bufferSize, _connectionTimeout);
                    Connected = _tcpClient.Connected;
                }
                catch(System.Exception exConnect)
                {
                    _log.Error(this.GetType().FullName + string.Format(".StartConnection(string bindAddress = {0}, int port = {1}, int bufferSize = {2}, int connectionTimeout = {3})", bindAddress, port, bufferSize, connectionTimeout), exConnect);
                }
                _listenerRunning = true;
                _threadListener = new Thread(new ThreadStart(IncommingDataThread));
                _threadListener.Start();

                _senderRunning = true;
                _threadSender = new Thread(new ThreadStart(SendDataThread));
                _threadSender.Start();
            }
            catch (System.Exception ex)
            {
                Connected = false;
                _log.Error(this.GetType().FullName + string.Format(".StartConnection(string bindAddress = {0}, int port = {1}, int bufferSize = {2}, int connectionTimeout = {3})", bindAddress, port, bufferSize, connectionTimeout), ex);
            }
        }
        /// <summary>
        /// Close the current connection
        /// </summary>
        public void CloseConnection()
        {
            try
            {
                if (this._listenerRunning)
                {
                    // Stop the thread, wait up to 2 seconds for it to terminate.
                    this._listenerRunning = false;

                    int X = 0;

                    while (!this._listenerStopped && X < 20)
                    {
                        Thread.Sleep(100);
                        X++;
                    }
                }
                if (this._senderRunning)
                {
                    // Stop the thread, wait up to 2 seconds for it to terminate.
                    this._senderRunning = false;

                    int X = 0;

                    while (!this._senderStopped && X < 20)
                    {
                        Thread.Sleep(100);
                        X++;
                    }
                }
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (this._tcpClient != null)
                {
                    this._tcpClient.Shutdown(SocketShutdown.Both);
                    this._tcpClient.Close();
                }

                this._tcpClient = null;
            }
            catch (System.Exception)
            {
            }
            finally
            {
                this.Connected = false;
                this._tcpClient = null;
            }
        }
        /// <summary>
        /// Restart the current connection, using the same endpoint as last time.
        /// </summary>
        /// <returns></returns>
        public bool Reconnect()
        {
            bool ret = false;
            try
            {
                if (_listenerRunning || _senderRunning || _tcpClient != null)
                {
                    try
                    {
                        if (_tcpClient != null)
                        {
                            _tcpClient.Shutdown(SocketShutdown.Both);
                            _tcpClient.Close();
                        }
                        _tcpClient = null;
                    }
                    catch (System.Exception)
                    {
                    }
                }
                try
                {
                    _tcpClient = GetConnection(_bufferSize, _connectionTimeout);
                }
                catch(Exception exReconnect)
                {
                    _log.Error(this.GetType().FullName + ".Reconnect()", exReconnect);
                }
                if (_tcpClient != null)
                    ret = _tcpClient.Connected;
            }
            catch (System.Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + ".Reconnect()", ex);
            }
            return ret;
        }
        /// <summary>
        /// Thread to handle incomming data
        /// </summary>
        private void IncommingDataThread()
        {
            try
            {
                _listenerStopped = false;
                IAsyncResult recvCallback = null;
                // Main loop
                while (_listenerRunning)
                {
                    // If we have been disconnected, we will reconnect on next send
                    if (_tcpClient != null)
                    {
                        if (recvCallback == null)
                            recvCallback = _tcpClient.BeginReceive(_buffer, 0, _buffer.Length, 0, null, null);
                        // Wait for 100 ms for incoming data, then check if the thread is still running.
                        if (recvCallback.AsyncWaitHandle.WaitOne(100, true))
                        {
                            // We got a result before the time out
                            int bytesRead = _tcpClient.EndReceive(recvCallback);
                            recvCallback = _tcpClient.BeginReceive(_buffer, 0, _buffer.Length, 0, null, null);
                            if (bytesRead > 0)
                            {
                                // Read the bytes and create a new TCPAsyncData
                                TCPAsyncData data = new TCPAsyncData();
                                data.Buffer = new byte[bytesRead];
                                for (int X = 0; X < bytesRead; X++)
                                    data.Buffer[X] = _buffer[X];
                                // Queue the TCPAsyncData for processing.
                                RecvDataQ.Enqueue(data);
                            }
                            //else
                            //{
                            //    // We received a result of zero bytes
                            //    CloseConnection();
                            //}
                        }
                    }
                    else
                        Thread.Sleep(100);
                }
            }
            catch (System.Exception ex)
            {
                if (ex.Message == "An existing connection was forcibly closed by the remote host")
                {
                    _log.Debug("Connection was forcibly closed by the remote host");
                    this.Connected = false;
                }
                else
                {
                    _log.Error(this.GetType().FullName + ".IncommingDataThread()", ex);
                }
            }
            finally
            {
                _listenerStopped = true;
            }
        }
        /// <summary>
        /// If there is something to send
        ///     1. If we are not connected, try to reconnect, if reconnect fails wait 5sec before trying again
        ///     2. Send the data to the remote server, if successful, dequeue from the send data queue.
        /// </summary>
        private void SendDataThread()
        {
            try
            {
                _senderStopped = false;
                TCPAsyncData sendData = null;
                // Main loop
                while (_senderRunning)
                {
                    bool sentSomething = false;
                    // Check to see if there is something to send
                    if (sendData == null && SendDataQ.Count > 0)
                    {
                        sendData = SendDataQ.Peek();

                        // If we have been disconnected, we will reconnect on next send
                        if (sendData != null)
                        {
                            // If we are running and the _tcpclient is not connected
                            while (_senderRunning && (_tcpClient == null || _tcpClient.Connected == false))
                            {
                                // Attempt a reconnect
                                if (!Reconnect())
                                {
                                    // If we are still failed, wait for 5 seconds
                                    int X = 0;
                                    while (_senderRunning && X < 50)
                                    {
                                        Thread.Sleep(100);
                                        X++;
                                    }
                                }
                            }

                            // Basic check to make sure we are connected
                            if (_senderRunning && _tcpClient != null && _tcpClient.Poll(1000, SelectMode.SelectWrite) && _tcpClient.Connected)
                            {
                                try
                                {
                                    // If we are connected, send the data, if success, then dequeue from the send Q
                                    this.Send(sendData.Buffer);
                                    //_tcpClient.Send(sendData.Buffer);

                                    // Set the value to null for the next loop aroung
                                    SendDataQ.Dequeue();
                                    sendData = null;
                                    sentSomething = true;
                                }
                                catch (System.Exception exSend)
                                {
                                    _log.Error(this.GetType().FullName + ".IncommingDataThread()", exSend);
                                    sendData = null;
                                    _tcpClient = null;
                                    // We need to add this one back onto the queue so that we do not loose the data
                                }

                            }
                        }
                    }

                    if (!sentSomething)
                    {
                        // Nothing to send and we didn't send anything.
                        Thread.Sleep(100);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".IncommingDataThread()", ex);
            }
            finally
            {
                _senderStopped = true;
            }
        }
        /// <summary>
        /// A method to async send data with a send timeout
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        private bool Send(byte[] data)
        {
            try
            {
                if (_tcpClient == null || !_tcpClient.Connected)
                    Reconnect();

                if (_tcpClient != null && _tcpClient.Connected)
                {
                    // Set a send wait flag
                    _tcpClient.BeginSend(data, 0, data.Length, 0, new AsyncCallback(SendCallback), _tcpClient);
                    return true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Send(byte[] data)", ex);
            }
            return false;
        }
        /// <summary>
        /// This method is called when the send of data is complete
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the socket from the state object.
                Socket handler = (Socket)ar.AsyncState;
                // Complete sending the data to the remote device.
                int bytesSent = handler.EndSend(ar);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendCallback(IAsyncResult ar)", ex);
            }
        }
        /// <summary>
        /// This method establishes a connection to the address specified in the IPEndpoint member var _ep
        /// with a connection timeout
        /// </summary>
        /// <param name="bufferSize"></param>
        /// <param name="connectionTimeout"></param>
        /// <returns></returns>
        private Socket GetConnection(int bufferSize = 4096, int connectionTimeout = 500)
        {
            Socket tcpClient = null;
            try
            {
                _buffer = new byte[bufferSize];
                // Create a new client
                tcpClient = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                //tcpClient.SetSocketOption(SocketOptionLevel.Tcp, SocketOptionName.KeepAlive, true);
                // Send a request to connect and continue on
                var result = tcpClient.BeginConnect(_ep, null, null);
                // Wait for connectionTimeout ms for the async call to return
                if (result.AsyncWaitHandle.WaitOne(connectionTimeout, true))
                {
                    // We didn't timeout so we are connected
                    tcpClient.EndConnect(result);
                }
                else
                {
                    // We timed out, end the async call and close the connection
                    // The next line will throw an exception if we failed to connect
                    tcpClient.EndConnect(result);
                    tcpClient = null;
                }
            }
            catch (System.Exception)
            {
                //throw new System.Exception(this.GetType().FullName + string.Format(".GetConnection(int bufferSize = {0}, int connectionTimeout = {1})", bufferSize, connectionTimeout), ex);
            }
            return tcpClient;
        }
    }
}

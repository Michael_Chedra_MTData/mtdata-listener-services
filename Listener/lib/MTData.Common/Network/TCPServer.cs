using log4net;
using MTData.Common.Threading;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace MTData.Common.Network
{
	/// <summary>
	/// Summary description for Class1.
	/// </summary>
	public class TCPServer
	{
        private const string sClassName = "MTData.Network.TCPServer.";
        private static ILog _log = LogManager.GetLogger(typeof(TCPServer));
        public delegate void InboundConnectionHandler(TcpListener listener);

        private string _description = "";
		private object SyncRoot = new object();
		private IPEndPoint _localIPEndPoint = null;
		private string _IPDescription = "";
		private EnhancedThread _listener;

		private event InboundConnectionHandler _inboundConnection;

		public TCPServer(
			string localBindIP,
			int portNumber, 
			string description,
			InboundConnectionHandler inboundConnectionHandler)
		{
			if ((localBindIP != null) && (localBindIP.Trim() != ""))
			{
				_localIPEndPoint = new IPEndPoint(System.Net.IPAddress.Parse(localBindIP), portNumber);
				_IPDescription = _localIPEndPoint.ToString();
			}
			else
			{
				_localIPEndPoint = new IPEndPoint(System.Net.IPAddress.Any, portNumber);
				_IPDescription = portNumber.ToString();
			}

		
			_description = description;
			if (inboundConnectionHandler != null)
				_inboundConnection += inboundConnectionHandler;
			_listener = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ListenHandler), null);
			_listener.Name = "TCPServer:" + description;
			_listener.Start();
		}

		public void Start()
		{
			_listener.Start();
		}

		public void Stop()
		{
			_listener.Stop();
		}

		public bool Listening
		{
			get
			{
				return _listener.Running;
			}
		}

		private object ListenHandler(EnhancedThread sender, object data)
		{
            _log.Info(string.Format("{0} : binding to port {1}", _description, _IPDescription));

			bool started = false;
			if (_inboundConnection != null)
			{
				TcpListener listener = new TcpListener(_localIPEndPoint);
				try
				{
					listener.Start();
					_log.Info(string.Format("{0} : listening to port {1}", _description, _IPDescription));
					started = true;
					while (!sender.Stopping)
					{
						if (listener.Pending())
						{
							_log.Info(string.Format("{0} : inbound connection on port {1}", _description, _IPDescription));
							if (_inboundConnection != null)
								_inboundConnection(listener);
						}
						else
							Thread.Sleep(10);
					}
				}
				finally
				{
					if (started)
						listener.Stop();
					listener = null;
				}
			}
			_log.Info(string.Format("{0} : releasing port {1}", _description, _IPDescription));
			return null;
		}
	}
}

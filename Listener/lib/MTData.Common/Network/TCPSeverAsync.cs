﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using log4net;
using System.Collections.Generic;

namespace MTData.Common.Network
{
    /// <summary>
    /// For holding the data buffer and the connection ID that sent it.
    /// </summary>
    [Serializable]
    public class TCPAsyncData
    {
        /// <summary>
        /// Connection ID
        /// </summary>
        public uint ConnectionID { get; set; }
        /// <summary>
        /// The recieve buffer.
        /// </summary>
        public byte[] Buffer { get; set; }
    }

    /// <summary>
    /// For holding the TCP connection and buffer to pass with async calls
    /// </summary>
    public class TCPAsyncState : TCPAsyncData
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="bufferSize"></param>
        public TCPAsyncState(int bufferSize)
        {
            Buffer = new byte[bufferSize];
        }
        /// <summary>
        /// The clients TCP socket
        /// </summary>
        public Socket TCPSocket { get; set; }
        public void Close()
        {
            try
            {
                if(TCPSocket != null)
                {
                    TCPSocket.Close();
                    TCPSocket.Dispose();
                }
            }
            catch(System.Exception)
            {
            }
            finally
            {
                TCPSocket = null;
            }
        }
    }

    /// <summary>
    /// An async TCP server that provides for multiple connections
    /// </summary>
    public class TCPSeverAsync
    {
        private static ILog _log = LogManager.GetLogger(typeof(TCPSeverAsync));
        private Socket _listener;
        private int _bufferSize;
        private bool _listenerRunning;
        private bool _listenerStopped;
        private Thread _threadListener;
        private bool _sendRunning;
        private bool _sendStopped;
        private bool _synchronised;
        private object _syncSendQ = new object();
        private object _syncRecvQ = new object();
        private object _syncUnSentQ = new object();
        private Queue<TCPAsyncData> _recvDataQ;
        private Queue<TCPAsyncData> _sendDataQ;
        private Queue<TCPAsyncData> _unSentByteDataQ;
        private Thread _threadSender;
        // connectionId = 0 is broadcast
        private uint connectionId = 1;
        private Dictionary<uint, TCPAsyncState> _connections;
        /// <summary>
        /// Call back for the accept connections thread
        /// </summary>
        private ManualResetEvent waitForTCPConnection = new ManualResetEvent(false);
        /// <summary>
        /// This property is used to set if the TCP server uses local queue locking to make the object thread safe.
        /// </summary>
        public bool Synchronised { get { return _synchronised; } set { _synchronised = value; } }
        /// <summary>
        /// The number of clients connected to the server.
        /// </summary>
        public int ConnectionCount
        {
            get
            {
                if (_connections != null)
                    return _connections.Count;
                return 0;
            }
        }
        /// <summary>
        /// Data received on the TCP server is queued here as TCPAsyncData object (ConnectionID/Data)
        /// If the object is set to Synchronised = true, then direct access to the queues is restricted
        /// </summary>
        public Queue<TCPAsyncData> RecvDataQ 
        {
            get
            {
                if (_synchronised) return null;
                else return _recvDataQ;
            }
            set
            {
                if (!_synchronised)
                    _recvDataQ = value;
            }
        }
        /// <summary>
        /// Data to send to a connection should be added to this queue (ConnectionID/Data)
        /// if connection ID = -1, the data will be sent to the first connected connection.
        /// if the connection ID is not present, the data will be requeued on UnSentByteDataQ for the contorolling
        /// application to deal with.
        /// If the object is set to Synchronised = true, then direct access to the queues is restricted
        /// </summary>
        public Queue<TCPAsyncData> SendDataQ
        {
            get
            {
                if (_synchronised) return null;
                else return _sendDataQ;
            }
            set
            {
                if (!_synchronised)
                    _sendDataQ = value;
            }
        }
        /// <summary>
        /// If data is queued to be sent to a connection ID that is no longer active, the data object
        /// will be requeued on this queue for the controlling service to deal with.
        /// If the object is set to Synchronised = true, then direct access to the queues is restricted
        /// </summary>
        public Queue<TCPAsyncData> UnSentByteDataQ
        {
            get
            {
                if (_synchronised) return null;
                else return _unSentByteDataQ;
            }
            set
            {
                if (!_synchronised)
                    _unSentByteDataQ = value;
            }
        }

        /// <summary>
        /// Default constructor
        /// </summary>
        public TCPSeverAsync()
        {
            _recvDataQ = new Queue<TCPAsyncData>();
            _sendDataQ = new Queue<TCPAsyncData>();
            _unSentByteDataQ = new Queue<TCPAsyncData>();
            _connections = new Dictionary<uint, TCPAsyncState>();
        }
        /// <summary>
        /// Start a new TCP listener on the bindAddress:inPort
        /// </summary>
        /// <param name="bindAddress"></param>
        /// <param name="inPort"></param>
        /// <param name="maxPendingConns"></param>
        /// <param name="bufferSize"></param>
        public void StartServer(string bindAddress, int inPort, int maxPendingConns = 100, int bufferSize = 4096)
        {
            try
            {
                if (_listenerRunning)
                {
                    StopServer();
                }

                _bufferSize = bufferSize;
                //byte[] bytes = new Byte[bufferSize];
                System.Net.IPAddress ip = GetIPFromString(bindAddress);
                if (ip == null)
                {
                    throw new System.Exception("Unable to determine bind IP from provided address '" + bindAddress + "'");
                }

                IPEndPoint ep = GetEndpoint(bindAddress, inPort);
                _listener = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
                _listener.Bind(ep);
                _listener.Listen(maxPendingConns);

                _listenerRunning = true;
                _threadListener = new Thread(AcceptConnections);
                _threadListener.Start();

                _sendRunning = true;
                _threadSender = new Thread(SendDataToConnections);
                _threadSender.Start();

            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + string.Format(".StartServer(string bindAddress = {0}, int inPort = {1})", bindAddress, inPort), ex);
                throw new System.Exception(this.GetType().FullName + string.Format(".StartServer(string bindAddress = {0}, int inPort = {1})", bindAddress, inPort), ex);
            }
        }
        /// <summary>
        /// Disconnect all connected clients and stop the processing threads.
        /// </summary>
        public void StopServer()
        {
            try
            {
                CloseConnection(0);
                if (_sendRunning)
                {
                    // Stop the thread, wait up to 2 seconds for it to terminate.
                    _sendRunning = false;
                    int X = 0;
                    while (!_sendStopped && X < 20)
                    {
                        Thread.Sleep(100);
                        X++;
                    }
                }

                if (_listenerRunning)
                {
                    // Stop the thread, wait up to 2 seconds for it to terminate.
                    _listenerRunning = false;
                    // Set the call back so the AcceptConnections will loop again
                    waitForTCPConnection.Set();
                    int X = 0;
                    while (!_listenerStopped && X < 20)
                    {
                        Thread.Sleep(100);
                        X++;
                    }
                }

                _listener.Close();
                _listener = null;
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".StopServer()", ex);
                throw new System.Exception(this.GetType().FullName + ".StopServer()", ex);
            }
        }
        /// <summary>
        /// This method will close one selected client, connectionId = 0 means close all clients
        /// </summary>
        /// <param name="connectionId"></param>
        public void CloseConnection(uint connectionId)
        {
            try
            {
                if (_connections.ContainsKey(connectionId))
                {
                    try
                    {
                        // If we have the key in the collection
                        _connections[connectionId].TCPSocket.Shutdown(SocketShutdown.Both);
                        _connections[connectionId].TCPSocket.Close();
                    }
                    catch(System.Exception exDisconnect)
                    {
                        _log.Error(this.GetType().FullName + ".CloseConnection(uint connectionId) - Disconnect error", exDisconnect);
                    }
                    _connections.Remove(connectionId);
                }
                else if (connectionId == 0 && _connections.Keys.Count > 0)
                {
                    // Send to all the connections
                    uint[] keys = new uint[_connections.Keys.Count];
                    _connections.Keys.CopyTo(keys, 0);
                    foreach (uint key in keys)
                    {
                        try
                        {
                            // If we have the key in the collection
                            _connections[key].TCPSocket.Shutdown(SocketShutdown.Both);
                            _connections[key].TCPSocket.Close();
                        }
                        catch (System.Exception exDisconnect)
                        {
                            _log.Error(this.GetType().FullName + ".CloseConnection(uint connectionId) - Disconnect error", exDisconnect);
                        }
                        try
                        {
                            _connections.Remove(key);
                        }
                        catch (System.Exception exDisconnect)
                        {
                            _log.Error(this.GetType().FullName + ".CloseConnection(uint connectionId) - Disconnect error", exDisconnect);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".CloseConnection(uint connectionId)", ex);
            }
        }
        /// <summary>
        /// This is the thread for accepting new TCP connections
        /// </summary>
        private void AcceptConnections()
        {
            try
            {
                _listenerStopped = false;
                // Main loop
                while (_listenerRunning)
                {
                    // Reset the call back
                    waitForTCPConnection.Reset();
                    // Set a delegate to Fire AcceptCallback when a new connection arrives
                    _listener.BeginAccept(new AsyncCallback(AcceptCallback), _listener);
                    // Wait here until a new connection had been established and handled by AcceptCallback, then loop back and wait for another connection.
                    waitForTCPConnection.WaitOne();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AcceptConnections()", ex);
                throw new System.Exception(this.GetType().FullName + ".AcceptConnections()", ex);
            }
            finally
            {
                _listenerStopped = true;
            }
        }
        /// <summary>
        /// This thread will dequeue TCPAsyncData from the _sendDataQ.  
        /// If there is a connection matching TCPAsyncData.ConnectionID the message will be sent on the connection
        /// If the ConnectionID is zero, the message will be sent to all connections
        /// If the ConnectionID is not zero and there is no connection in the list with the matching ID, the message will be requeued on UnSentByteDataQ
        /// </summary>
        private void SendDataToConnections()
        {
            try
            {
                _sendStopped = false;
                // Main loop
                while (_sendRunning)
                {
                    TCPAsyncData data = null;
                    // If we are operating in synchonised mode, then lock the queue and then check for send data
                    if(_synchronised)
                    {
                        lock (_syncSendQ)
                        {
                            if (_connections.Keys.Count > 0 && _sendDataQ.Count > 0)
                            {
                                data = _sendDataQ.Dequeue();
                            }
                        }
                    }
                    else
                    {
                        // If we are not operating in synchonised mode, then simply dequeue
                        if (_connections.Keys.Count > 0 && _sendDataQ.Count > 0)
                        {
                            data = _sendDataQ.Dequeue();
                        }
                    }
                    // If there is someone to send to and a message to send.
                    if (data != null)
                    {
                        // Get the connection ID to send to
                        uint connId = data.ConnectionID;
                        bool toUnsentData = true;
                        try
                        {
                            if (_connections.ContainsKey(connId))
                            {
                                // If we have the key in the collection
                                _connections[connId].TCPSocket.BeginSend(data.Buffer, 0, data.Buffer.Length, 0, new AsyncCallback(SendCallback), _connections[connId].TCPSocket);
                                toUnsentData = false;
                            }
                            else if (connId == 0 && _connections.Keys.Count > 0)
                            {
                                // Send to all the connections
                                foreach (uint key in _connections.Keys)
                                    _connections[key].TCPSocket.BeginSend(data.Buffer, 0, data.Buffer.Length, 0, new AsyncCallback(SendCallback), _connections[key].TCPSocket);
                                toUnsentData = false;
                            }
                        }
                        catch (System.Exception exSend)
                        {
                            if (exSend.Message.Contains("An existing connection was forcibly closed by the remote host"))
                            {
                                _log.Error("Send Failed to connection ID " + connId + ": Connection closed on remote end.");
                            }
                            else
                                _log.Error(this.GetType().FullName + ".SendDataToConnections()", exSend);
                            toUnsentData = true;
                        }

                        // If the message wasn't a braodacast and it wasn't sent correctly, return it.
                        if(toUnsentData)
                        {
                            // No match, requeue the message for the higher level service to reprocess.
                            if (_synchronised)
                                lock(_syncUnSentQ)
                                    _unSentByteDataQ.Enqueue(data);
                            else
                                _unSentByteDataQ.Enqueue(data);
                        }
                    }
                    else
                        Thread.Sleep(50);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendDataToConnections()", ex);
                throw new System.Exception(this.GetType().FullName + ".SendDataToConnections()", ex);
            }
            finally
            {
                _sendStopped = true;
            }
        }
        /// <summary>
        /// Accept a new connection and hook up the ReadCallback events
        /// </summary>
        /// <param name="ar"></param>
        private void AcceptCallback(IAsyncResult ar) 
        {
            try
            {
                // Set the call back so the AcceptConnections thread will loop and accept new connections
                waitForTCPConnection.Set();

                // Get the socket and end the async call
                Socket listener = (Socket) ar.AsyncState;
                Socket handler = listener.EndAccept(ar);

                // Create the state object.
                TCPAsyncState tcpConn = new TCPAsyncState(_bufferSize);
                tcpConn.TCPSocket = handler;
                if (connectionId == uint.MaxValue - 1)
                    // Set to 1 because connectionId = 0 is broadcast
                    connectionId = 1;
                tcpConn.ConnectionID = connectionId++;

                // Add the new connections to the connections list.
                if (_connections.ContainsKey(tcpConn.ConnectionID))
                    _connections.Remove(tcpConn.ConnectionID);
                _connections.Add(tcpConn.ConnectionID, tcpConn);

                // Set the delegate to process data recieved on the socket
                handler.BeginReceive(tcpConn.Buffer, 0, tcpConn.Buffer.Length, 0, new AsyncCallback(ReadCallback), tcpConn);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AcceptCallback(IAsyncResult ar)", ex);
            }
        }
        /// <summary>
        /// Read the data received on a connection and queue it for processing.
        /// If the unit sends an empty packet, close the connection.
        /// </summary>
        /// <param name="ar"></param>
        private void ReadCallback(IAsyncResult ar)
        {
            try
            {
                // Retrieve the state object and the handler socket
                // from the asynchronous state object.
                TCPAsyncState tcpConn = (TCPAsyncState)ar.AsyncState;
                Socket handler = tcpConn.TCPSocket;
                // Get the number of bytes received
                int bytesRead = handler.EndReceive(ar);
                if (bytesRead > 0)
                {
                    // Read the bytes and create a new TCPAsyncData
                    TCPAsyncData data = new TCPAsyncData();
                    data.ConnectionID = tcpConn.ConnectionID;
                    data.Buffer = new byte[bytesRead];
                    for (int X = 0; X < bytesRead; X++)
                        data.Buffer[X] = tcpConn.Buffer[X];
                    handler.BeginReceive(tcpConn.Buffer, 0, tcpConn.Buffer.Length, 0, new AsyncCallback(ReadCallback), tcpConn);
                    // Queue the TCPAsyncData for processing.
                    // If we are operating in synchonised mode, lock the recieve queue object before enquing the data
                    if (_synchronised)
                    {
                        lock(_syncRecvQ)
                            _recvDataQ.Enqueue(data);
                    }
                    else
                    {
                        // If we are not operating in synchronised mode, simply enqueue the data.
                        _recvDataQ.Enqueue(data);
                    }
                }
                else
                {
                    // This is a close connection
                    tcpConn.Close();
                    // Remove the connection from the list
                    if (_connections.ContainsKey(tcpConn.ConnectionID))
                        _connections.Remove(tcpConn.ConnectionID);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ReadCallback(IAsyncResult ar)", ex);
            }
        }
        /// <summary>
        /// This call back is fired after data is sent to a TCP connection.
        /// </summary>
        /// <param name="ar"></param>
        private void SendCallback(IAsyncResult ar) 
        {
            try 
            {
                // Retrieve the socket from the state object.
                Socket tcpConn = (Socket) ar.AsyncState;
                // Complete sending the data to the remote device.
                int bytesSent = tcpConn.EndSend(ar);
            } 
            catch (Exception ex) 
            {
                _log.Error(this.GetType().FullName + ".SendCallback(IAsyncResult ar) ", ex);
            }
        }
        /// <summary>
        /// This method will resolve and IP address from a host name or IP address string.
        /// </summary>
        /// <param name="bindAddress"></param>
        /// <returns></returns>
        public static IPAddress GetIPFromString(string bindAddress)
        {
            System.Net.IPAddress ip = null;
            try
            {
                ip = System.Net.IPAddress.Parse(bindAddress);
            }
            catch
            {
                //not an IP address, try DNS lookup
                IPHostEntry ipHostInfo = Dns.GetHostEntry(bindAddress);
                for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                {
                    if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                    {
                        ip = ipHostInfo.AddressList[X];
                        break;
                    }
                }
            }
            return ip;
        }
        /// <summary>
        /// This method will return a IPEndpoint from a host name or IP address string and a port number.
        /// </summary>
        /// <param name="bindAddress"></param>
        /// <param name="inPort"></param>
        /// <returns></returns>
        public static IPEndPoint GetEndpoint(string bindAddress, int inPort)
        {
            IPEndPoint ep = null;
            try
            {

                byte[] bytes = new Byte[1024];
                System.Net.IPAddress ip = GetIPFromString(bindAddress);
                if (ip == null)
                    throw new System.Exception("Unable to determine bind IP from provided address '" + bindAddress + "'");
                ep = new IPEndPoint(ip, inPort);
            }
            catch (System.Exception ex)
            {
                _log.Error(string.Format("MTData.Common.Network.TCPServerAsync.GetEndpoint(string bindAddress = {0}, int inPort = {1})", bindAddress, inPort), ex);
                throw new System.Exception(string.Format("MTData.Common.Network.TCPServerAsync.GetEndpoint(string bindAddress = {0}, int inPort = {1})", bindAddress, inPort), ex);
            }
            return ep;
        }
        /// <summary>
        /// This method enqueues data for sending in a thread safe manor
        /// </summary>
        /// <param name="data"></param>
        public void SafeQueueForSend(TCPAsyncData data)
        {
            try
            {
                lock (_syncSendQ)
                    _sendDataQ.Enqueue(data);
            }
            catch(System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SafeQueueForSend(TCPAsyncData data)", ex);
                throw new System.Exception(this.GetType().FullName + ".SafeQueueForSend(TCPAsyncData data)", ex);
            }
        }
        /// <summary>
        /// This method checks if there has been any data received in a thread safe manor
        /// </summary>
        /// <param name="data"></param>
        public TCPAsyncData SafeReadFromConnections()
        {
            TCPAsyncData ret = null;
            try
            {
                lock(_syncRecvQ)
                {
                    if (_recvDataQ.Count > 0)
                        ret = _recvDataQ.Dequeue();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ReadFromConnections()", ex);
                throw new System.Exception(this.GetType().FullName + ".ReadFromConnections()", ex);
            }
            return ret;
        }
        /// <summary>
        /// This method checks if there is any unsent data in a thread safe manor
        /// </summary>
        /// <param name="data"></param>
        public TCPAsyncData SafeReadUnSentData()
        {
            TCPAsyncData ret = null;
            try
            {
                lock (_syncUnSentQ)
                {
                    if (_unSentByteDataQ.Count > 0)
                        ret = _unSentByteDataQ.Dequeue();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SafeReadUnSentData()", ex);
                throw new System.Exception(this.GetType().FullName + ".SafeReadUnSentData()", ex);
            }
            return ret;
        }
    }
}

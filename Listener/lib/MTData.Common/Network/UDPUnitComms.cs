using System;
using System.Net;
using System.Net.Sockets;
using log4net;

using System.Threading;
using MTData.Common.Threading;
using MTData.Common.Queues;

namespace MTData.Common.Network
{
	public class UDPUnitComms
	{
		#region Private Vars
		private const string sClassName = "MTData.Network.UDPUnitComms.";
        private static ILog _log = LogManager.GetLogger(typeof(UDPUnitComms));
		private bool bStopping = false;
		private string _sLocalBindIP = "";
		private int _iLocalBindPort = 0;
		private int _iReconnectTime = 0;
		private UdpClient _oUDPClient = null;
		private EnhancedThread _SendThread = null;
		private EnhancedThread _ReadThread = null;
		private StandardQueue _toUDPQ = null;
		private StandardQueue _fromUDPQ = null;
		#endregion

		#region Public Properties
		string LocalBindIP
		{
			get{return _sLocalBindIP;}
			set{_sLocalBindIP = value;}
		}
		int LocalBindPort
		{
			get{return _iLocalBindPort;}
			set{_iLocalBindPort = value;}
		}
		bool Stopping
		{
			get{return bStopping;}
		}
		#endregion

		public UDPUnitComms(string sLocalBindIP, int iLocalBindPort, int iReconnectTime, StandardQueue toUDPQ, StandardQueue fromUDPQ)
		{
			try
			{
				#region Create the UDP object
				_iReconnectTime = iReconnectTime;
				_sLocalBindIP = sLocalBindIP;
				_iLocalBindPort = iLocalBindPort;
				#endregion
				#region Create the comms queues
				_toUDPQ = toUDPQ;
				_fromUDPQ = fromUDPQ;
				#endregion
				Start();
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "UDPUnitComms(string sLocalBindIP, int iLocalBindPort)", ex);
			}
		}

		private void Start()
		{
			int iTimeout = 0;
			try
			{
				bStopping = true;
				#region Stop the recieve thread
				if(_ReadThread != null)
				{
					try
					{
						_ReadThread.Stop();
						iTimeout = 0;
						while(_ReadThread.Stopping)
						{
							Thread.Sleep(100);
							iTimeout += 100;
							if (iTimeout > 10000)
								break;
						}
						_ReadThread = null;
					}
					catch(System.Exception)
					{
					}
				}
				#endregion
				#region Stop the send thread
				if(_SendThread != null)
				{
					try
					{
						_SendThread.Stop();
						iTimeout = 0;
						while(_SendThread.Stopping)
						{
							Thread.Sleep(100);
							iTimeout += 100;
							if (iTimeout > 10000)
								break;
						}
						_SendThread = null;
					}
					catch(System.Exception)
					{
					}
				}
				#endregion
				#region Stop the UDP Client
				if (_oUDPClient != null)
				{
					try
					{
						_oUDPClient.Close();
					}
					catch(System.Exception)
					{
					}
					try
					{
						_oUDPClient = null;
					}
					catch(System.Exception)
					{
					}
				}
				#endregion
				bStopping = false;
				#region Create the new UDP object
				_oUDPClient = new UdpClient( _iLocalBindPort);
				#endregion
				#region Create the send thread
				_SendThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(SendThreadHandler), null);
				_SendThread.Name = "UDP Client Sender Thread";
				_SendThread.Start();
				#endregion
				#region Create the read thread
				_ReadThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReadThreadHandler), null);
				_ReadThread.Name = "UDP Client Reader Thread";
				_ReadThread.Start();
				#endregion	
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "Start()", ex);
			}
		}

		private void Stop()
		{
			int iTimeout = 0;
			try
			{
				bStopping = true;
				#region Stop the UDP Client
				if (_oUDPClient != null)
				{
					try
					{
						_oUDPClient.Close();
					}
					catch(System.Exception)
					{
					}
					try
					{
						_oUDPClient = null;
					}
					catch(System.Exception)
					{
					}
				}
				#endregion
				#region Stop the recieve thread
				if(_ReadThread != null)
				{
					try
					{
						_ReadThread.Stop();
						iTimeout = 0;
						while(_ReadThread.Stopping)
						{
							Thread.Sleep(100);
							iTimeout += 100;
							if (iTimeout > 10000)
								break;
						}
						_ReadThread = null;
					}
					catch(System.Exception)
					{
					}
				}
				#endregion
				#region Stop the send thread
				if(_SendThread != null)
				{
					try
					{
						_SendThread.Stop();
						iTimeout = 0;
						while(_SendThread.Stopping)
						{
							Thread.Sleep(100);
							iTimeout += 100;
							if (iTimeout > 10000)
								break;
						}
						_SendThread = null;
					}
					catch(System.Exception)
					{
					}
				}
				#endregion
				bStopping = false;
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "Stop()", ex);
			}
		}

		private object SendThreadHandler(EnhancedThread sender, object oData)
		{
			object[] oSendData = null;
			byte[] bData = null;
			IPEndPoint oIPEndPoint = null;
			int iTimeout = 0;
			try
			{
				while(!sender.Stopping)
				{
					if(_oUDPClient != null)
					{
						try
						{
							#region Poll the _toUDPQ and send the data
							#region Check for new data on the queue
							lock(_toUDPQ.SyncRoot)
							{
								if (_toUDPQ.Count > 0)
									oSendData = (object[]) _toUDPQ.Dequeue();
								else
									oSendData = null;
							}
							#endregion
							if (oSendData == null)
								Thread.Sleep(100);	
							else
							{
								#region Get the destination address and bytes to send
								oIPEndPoint = (IPEndPoint) oSendData[0];
								bData = (byte[]) oSendData[1];
                                IPHostEntry oServerIP = Dns.GetHostEntry(oIPEndPoint.Address);
								#endregion
								#region Send the data
                                _oUDPClient.Send(bData, bData.Length, oServerIP.HostName, oIPEndPoint.Port);
								#endregion
							}
							#endregion
						}
						catch(System.Exception ex)
						{
							_log.Error(sClassName + "SendThreadHandler(EnhancedThread sender, object oData)", ex);
						}
					}
					else
					{
						try
						{
							#region Reconnect the UDP object
							if (!sender.Stopping && !bStopping)
							{
								_log.Info("UDP object is set to null - Trying to recreate connection");
								while (_oUDPClient == null && !sender.Stopping)
								{
									try
									{
										_oUDPClient = new UdpClient(_sLocalBindIP, _iLocalBindPort);
									}
									catch(System.Exception exConn)
									{
										_log.Error(sClassName + "SendThreadHandler(EnhancedThread sender, object oData) - Failed to recreate connection.  Will try again in " + Convert.ToString(_iReconnectTime) + "ms.", exConn);
										_oUDPClient = null;
										iTimeout = 0;
									}
									if (_oUDPClient == null && !sender.Stopping)
									{
										while (iTimeout < _iReconnectTime && _oUDPClient == null && !sender.Stopping)
										{
											Thread.Sleep(100);
											iTimeout += 100;
										}
									}
								}
							}
							#endregion
						}
						catch(System.Exception ex)
						{
							_log.Error(sClassName + "SendThreadHandler(EnhancedThread sender, object oData) - Stopped UDP Connection Send Thread", ex);
							break;
						}
					}
				}
			}
			catch(System.Exception exOverall)
			{
				_log.Error(sClassName + "SendThreadHandler(EnhancedThread sender, object oData) - Stopping UDP Connection Send Thread", exOverall);
			}

			_log.Info("Stopped UDP Connection Send Thread.");
			return null;
		}

		public object ReadThreadHandler(EnhancedThread sender, object data)
		{
			object[] oRcvdData = null;
			byte[] bData = null;
			IPEndPoint remoteIP = null;

			try
			{
				while(!sender.Stopping)
				{
					if (_oUDPClient != null)
					{
						try
						{
							#region Wait for incomming data
							remoteIP = new IPEndPoint(IPAddress.Any, 0);
							bData = _oUDPClient.Receive(ref remoteIP);
							oRcvdData = new object[2];
							oRcvdData[0] = remoteIP;
							oRcvdData[1] = bData;
							lock(_fromUDPQ.SyncRoot)
								_fromUDPQ.Enqueue(oRcvdData);
							#endregion
						}
						catch(SocketException sockEx)
						{
							#region Handle Socket Exception
							if (!sockEx.Message.StartsWith("A blocking operation was interrupted by a call to WSACancelBlockingCall"))
							{
								_log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data) - Socket Exception", sockEx);
								_oUDPClient = null;
								Thread.Sleep(100);
							}
							#endregion
						}
						catch(Exception ex)
						{
							#region Handle General Exception
							_log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data)", ex);
							_oUDPClient = null;
							Thread.Sleep(100);
							#endregion
						}
					}
					else
					{
						try
						{
							#region If the UDP object is null, wait for the thread to end or the UPD object to be recreated.
							if(!sender.Stopping && !bStopping)
								Thread.Sleep(100);
							#endregion
						}
						catch(Exception ex)
						{
							_log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data)", ex);
							break;
						}
					}		
				}
			}
			catch(System.Exception exOverall)
			{
				_log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data) - Stopping UDP Connection Recieve Thread", exOverall);
			}
			_log.Info("Stopped UDP Connection Send Thread.");
			return null;
		}
	}
}

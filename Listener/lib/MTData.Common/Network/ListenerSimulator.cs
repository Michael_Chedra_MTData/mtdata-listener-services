using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using log4net;
using MTData.Common.Interface;
using MTData.Common.Threading;

namespace MTData.Common.Network
{
    /// <summary>
    /// Summary description for cListenerSimulator.
    /// </summary>
    public class ListenerSimulator
    {
        private const string sClassName = "MTData.Network.cListenerSimulator.";
        private static ILog _log = LogManager.GetLogger(typeof(ListenerSimulator));

        private bool bThreadRunning = false;
        private bool bSendThreadRunning = false;
        private EnhancedThread _listener;
        private EnhancedThread _sender;
        private QueueInterface _fromRemoteIPQ = null;
        private QueueInterface _toRemoteIPQ = null;
        private ArrayList oClients = null;
        private int iPort = 0;
        private UdpClient _udpClient = null;

        public ListenerSimulator(int Port, QueueInterface fromRemoteIPQ)
        {
            try
            {
                CreateObject(Port, fromRemoteIPQ, null);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "cListenerSimulator(int Port = " + Port + ", QueueInterface fromRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "cListenerSimulator(int Port = " + Port + ", QueueInterface fromRemoteIPQ)", ex);
            }
        }

        public ListenerSimulator(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)
        {
            try
            {
                CreateObject(Port, fromRemoteIPQ, toRemoteIPQ);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "cListenerSimulator(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "cListenerSimulator(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
            }
        }

        private void CreateObject(int Port, QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)
        {
            try
            {
                oClients = ArrayList.Synchronized(new ArrayList());
                iPort = Port;
                _udpClient = new UdpClient(iPort);
                _fromRemoteIPQ = fromRemoteIPQ;
                _toRemoteIPQ = toRemoteIPQ;
                if (_toRemoteIPQ != null)
                {
                    _sender = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(SendThreadHandler), _toRemoteIPQ);
                    _sender.Name = "UDP.Sender.Thread";
                    bSendThreadRunning = true;
                    _sender.Start();
                }
                if (_fromRemoteIPQ != null)
                {
                    _log.Info("Starting UDP listener on " + Convert.ToString(Port) + ".");
                    IPAddress oLocal = ((IPAddress[])Dns.GetHostEntry("localhost").AddressList)[0];

                    _listener = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ReadThreadHandler), _fromRemoteIPQ);
                    _listener.Name = "UDP.Listener.Thread";
                    bThreadRunning = true;
                    _listener.Start();
                }

                _log.Info("Listening for UDP on port " + Convert.ToString(Port) + ".");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "CreateObject(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
                throw new System.Exception(sClassName + "CreateObject(int Port = " + Port + ", QueueInterface fromRemoteIPQ, QueueInterface toRemoteIPQ)", ex);
            }
        }

        public void Stop()
        {
            try
            {
                bThreadRunning = false;
                bSendThreadRunning = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
                throw new System.Exception(sClassName + "Stop()", ex);
            }
        }


        public object SendThreadHandler(EnhancedThread sender, object data)
        {
            QueueInterface outboundQ = (QueueInterface)data;
            byte[] bSendData = null;
            int iRet = 0;
            ArrayList oRemoveAt = new ArrayList();

            try
            {
                while (bSendThreadRunning)
                {
                    if (_udpClient != null)
                    {
                        lock (outboundQ.SyncRoot)
                        {
                            if (outboundQ.Count > 0)
                                bSendData = (byte[])outboundQ.Dequeue();
                        }
                        if (bSendData != null)
                        {
                            try
                            {
                                lock (oClients.SyncRoot)
                                {
                                    if (oClients.Count > 0)
                                    {
                                        for (int X = 0; X < oClients.Count; X++)
                                        {
                                            try
                                            {
                                                IPEndPoint oEP = (IPEndPoint)oClients[X];
                                                iRet = _udpClient.Send(bSendData, bSendData.Length, oEP);
                                            }
                                            catch (System.Exception)
                                            {
                                                oRemoveAt.Add(X);
                                            }
                                        }
                                        if (oRemoveAt.Count > 0)
                                        {
                                            for (int X = oRemoveAt.Count - 1; X >= 0; X--)
                                            {
                                                oClients.RemoveAt((int)oRemoveAt[X]);
                                            }
                                            oRemoveAt = new ArrayList();
                                        }
                                    }
                                }
                            }
                            catch (Exception ex)
                            {
                                _log.Error(sClassName + "SendThreadHandler(EnhancedThread sender, object data) - Loop", ex);
                            }
                            bSendData = null;
                        }
                    }
                }

            }
            catch (Exception exMain)
            {
                _log.Error(sClassName + "SendThreadHandler(EnhancedThread sender, object data) - Thread Error", exMain);
            }
            finally
            {
                _log.Info("Stopped sending on UDP on port " + Convert.ToString(iPort) + ".");
            }
            return null;
        }

        public object ReadThreadHandler(EnhancedThread sender, object data)
        {
            bool bAlreadyInList = false;
            byte[] receivedBytes = null;
            QueueInterface inboundQ = (QueueInterface)data;

            try
            {
                while (bThreadRunning)
                {
                    if (_udpClient != null)
                    {
                        IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);
                        try
                        {
                            receivedBytes = _udpClient.Receive(ref remoteIP);
                            bAlreadyInList = false;
                            lock (oClients.SyncRoot)
                            {
                                for (int X = 0; X < oClients.Count; X++)
                                {
                                    if (((IPEndPoint)oClients[X]).Equals(remoteIP))
                                    {
                                        bAlreadyInList = true;
                                        break;
                                    }
                                }
                                if (!bAlreadyInList)
                                    oClients.Add(remoteIP);
                            }
                            lock (inboundQ.SyncRoot)
                                inboundQ.Enqueue(receivedBytes);
                        }
                        catch (SocketException sockEx)
                        {
                            if (!sockEx.Message.StartsWith("A blocking operation was interrupted by a call to WSACancelBlockingCall"))
                                _log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data) - Socket Exception : ", sockEx);
                        }
                        catch (Exception ex)
                        {
                            _log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data) - Loop Error : ", ex);
                        }
                    }
                }
            }
            catch (Exception exMain)
            {
                _log.Error(sClassName + "ReadThreadHandler(EnhancedThread sender, object data) - Thread Error", exMain);
            }
            finally
            {
                _log.Info("Stopped listening for UDP on port " + Convert.ToString(iPort) + ".");
            }
            return null;
        }
    }
}

using System;
using System.Text;
using System.Collections;
using System.Runtime.CompilerServices;

namespace MTData.Common.Performance
{
	/// <summary>
	/// This list will provide access to a number of methods for logging
	/// and recording class instance information
	/// </summary>
	public class ClassInstanceList
	{
		private static ClassInstanceList _instance = null;

		/// <summary>
		/// Holds the list of class information
		/// </summary>
		private Hashtable _classMap = new Hashtable();

		/// <summary>
		/// PRepare the list for use
		/// </summary>
		private ClassInstanceList()
		{
			
		}

		/// <summary>
		/// Return a singleton instance of the class, for use anywhere.
		/// </summary>
		/// <returns></returns>
		[System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.Synchronized)]
		public static ClassInstanceList GetInstance()
		{
			if (_instance == null)
				_instance = new ClassInstanceList();
			return _instance;
		}

		/// <summary>
		/// Synchronise the retrieval of instance data, to ensure that 
		/// only one instance of each item is added to the list.
		/// </summary>
		/// <param name="className">Name of class to retrieve</param>
		/// <returns>Instance of a ClassInstanceData structure holding the class details</returns>
		[System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.Synchronized)]
		public ClassInstanceData GetClassInstanceData(string className)
		{
			if (!this._classMap.Contains(className))
			{
				ClassInstanceData data = new ClassInstanceData(className);
				this._classMap.Add(className, data);
				return data;
			}
			else
			{
				return (ClassInstanceData)this._classMap[className];
			}
		}

		/// <summary>
		/// Add a reference fo rthe class name specified.
		/// </summary>
		/// <param name="className">Name of class to increment reference count for</param>
		public void AddReference(string className)
		{
			ClassInstanceData data = GetClassInstanceData(className);
			data.AddRef();
		}

		/// <summary>
		/// Release a reference, and reduce the instance count.
		/// </summary>
		/// <param name="className">Name of class to reduce instance count for</param>
		public void ReleaseReference(string className)
		{
			ClassInstanceData data = GetClassInstanceData(className);
			data.Release();
		}

		/// <summary>
		/// Format the output to show the list of instances, and the equivalent instance counts.
		/// </summary>
		/// <returns>String list of class names and counts</returns>
		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			//	output each of the instance counts
			foreach(ClassInstanceData instanceData in this._classMap.Values)
			{
				builder.Append(instanceData.ToString());
				builder.Append("\r\n");
			}
			return builder.ToString();
		}

	}
}

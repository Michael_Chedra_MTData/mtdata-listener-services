using System;
using System.Runtime.CompilerServices;

namespace MTData.Common.Performance
{
	/// <summary>
	/// This class contains information relating to the 
	/// number of instances of a class.
	/// </summary>
	public class ClassInstanceData
	{
		private string _className;
		private int _instanceCount = 0;

		public ClassInstanceData(string className)
		{
			this._className = className;
			this._instanceCount = 0;
		}

		public string ClassName
		{
			get
			{
				return this._className;
			}
		}

		public int InstanceCount
		{
			get
			{
				return this._instanceCount;
			}
		}

		[System.Runtime.CompilerServices.MethodImpl(MethodImplOptions.Synchronized)]
		private int UpdateReferenceCount(int adjustment)
		{
			this._instanceCount += adjustment;
			return this._instanceCount;
		}

		public int AddRef()
		{
			return UpdateReferenceCount(+1);
		}

		public int Release()
		{
			return UpdateReferenceCount(-1);
		}

		public override string ToString()
		{
			return string.Format("{0} : {1}", this._className, this._instanceCount);
		}

	}
}

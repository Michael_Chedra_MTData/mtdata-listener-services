﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Common.Config
{
    public class ConvertSignatureConfig
    {
        public bool LogSignatureData = false;
        public int MaxHeight = 0;
        public int MaxWidth = 0;
        public int MaxVectorHeight = 0;
        public int MaxVectorWidth = 0;
        public string PicturePath = "";
        public double ResizeFactor = 1;
        public bool IncreaseContrast = true;
        public uint ContrastTolerance = 1;
        public bool MakeSignatureTransparent = false;
        public Color SignatureBackgroundColor = Color.White;
        public bool SaveImageFile = false;
        public string DefaultBackgroundGif = "";
    }

}

using System;
using System.Drawing;
using System.Configuration;


namespace MTData.Common.Config
{
    /// <summary>
    /// Summary description for GenConfigReader.
    /// </summary>
    public class GenConfigReader
    {
        public bool DebugMode = false;
        public bool SendDisconnectMsgOnClose = false;
        public bool CheckForDisconnectMsg = false;

        public string DisconnectMessage = "";
        public string InterfaceBindIP = "0.0.0.0";
        public int InterfacePort = 5009;
        public int HeartbeatSendPeriod = 0;
        public int HeartbeatTimeoutPeriod = 0;

        public string MCCBindIP = "0.0.0.0";
        public int MCCPort = 5019;
        public string SecondaryMCCBindIP = "0.0.0.0";
        public int SecondaryMCCPort = 5021;

        public string ListenerBindIP = "0.0.0.0";
        public int ListenerPort = 0;
        public int ListenerRSUPort = 0;
        public string ListenerVersion = "6.1";
        public int ListenerLocalBindPort = 0;
        public int ListenerRemoteStatePort = 0;
        public int ListenerLiveUpdatePort = 0;
        public int ListenerRemoteSendPort = 0;
        public int ListenerCommsTimeout = 0;
        public bool ListenerConnectWithHistory = false;
        public string RegisterForUpdatesFromFleets = "";
        public string LiveUpdateVersion = "";
        public bool bLogListenerIncommingPacketData = false;
        public bool bLogListenerOutgoingPacketData = false;
        public int ClientListenerPort = 0;

        public int LoggingPort = -1;
        public int PacketSize = 60;
        public int TranslationBufferSize = 4096;
        public int CommunicationBufferSize = -1;
        public int PacketWaitPeriod = 10;
        public int SleepWaitPeriod = 100;
        public int MaxQueueSize = 300;

        public string LogFileDirectory = @"C:\MTDataLogs";
        public string LogFilePrefixFormat = "YMD";
        public string LogFileName = "MTData.MDT.Interface";
        public string LogFileExtension = "log";
        public string DownloadLogFileName = "MTData.MDT.Interface.Downloads";
        public bool LogToMCCData = false;
        public bool LogFromMCCData = false;
        public bool LogToListenerData = false;
        public bool LogFromListenerData = false;
        public bool LogToCustomerData = false;
        public bool LogFromCustomerData = false;
        public bool LogLiveUpdateData = false;
        public bool LogSQLData = false;
        public string TranslatorType = "5040";
        public string DSN = "";
        public string TransportAppDSN = "";
        public string db = "";
        public string transportDB = "";
        public int DatabasePollRate = 0;
        public string RouteURLWebService = "";

        public bool bMessageSendRecievedConfirmationViaEmail = false;
        public string MessageConfirmationEmailServer = "";
        public string MessageConfirmationEmailFrom = "";
        public string MessageConfirmationEmailTo = "";

        // Com Port Settings
        public bool ComPortEnabled = false;
        public int ComPortNumber = 0;
        public int ComPortBitRate = 96000;
        public string ComPortParity = "n";
        public int ComPortDataBits = 8;
        public int ComPortStopBits = 1;
        public bool ComPortUseFlowContol = false;
        public bool ComPortReceiveEnabled = true;

        // Reporting Server Settings 
        public string ReportServerReportsFolder = "";
        public string ReportServerTemplatePath = "";
        public string ReportServerHTTPAddress = "";
        public string ReportServerSignaturesFolder = "";
        public string ReportServerLogoRight = "";
        public string ReportServerLogoLeft = "";
        public bool ReportServerLogReportingProcessResults = false;
        public bool ReportServerKillExcelAfterUse = false;
        public string ReportServerExcelPath = "";
        public bool ReportServerExcelVisible = false;
        public bool ReportServerShowStreetNames = false;
        public bool ReportServerAvoidOrientation = false;
        public bool ReportServerAvoidPaperSize = false;
        public bool ReportServerAvoidMargins = false;

        // Report Server Com settings
        public string ReportServerListenIP = "";
        public int ReportServerListenPort = 0;
        public int ReportServerComPortStart = 0;
        public int ReportServerComPortFinish = 0;
        public bool ReportServerTCPTimeout = false;
        public int ReportServerTCPSendTimeout = 0;
        public int ReportServerTCPReceiveTimeout = 0;
        public int ReportServerPacketSize = 0;
        public int ReportServerPacketWaitPeriod = 0;
        public int ReportServerSleepWaitPeriod = 0;
        public int ReportServerBufferSize = 0;
        public bool ReportServerSendDisconnectOnClose = false;
        public byte ReportServerSOP = (byte)0x02;
        public byte ReportServerEOP = (byte)0x03;
        public int ReportServerLengthBytes = 0;
        public bool ReportServerFixedLength = false;
        public int ReportServerAdditionalHeaderBytes = 0;
        //	Load up the config for the signature information
        public ConvertSignatureConfig SignatureConfig = new ConvertSignatureConfig();
        // Email Logging Settings
        public string EmailToAddress = "";
        public string EmailFromAddress = "";
        public string EmailFromAddressDisplayName = "";
        public string EmailSubject = "";
        public int EmailSendInterval = 0;
        public int EmailSendWarningsInterval = 0;
        // MOT File Settings
        public ushort AutoLogoffXMinsAfterIgnOff = 0;
        public string FirmwareImagePath = "";
        public int UnitSoftwareVersion = 0;
        public int DownloadSegementSize = 0;
        public bool CheckGPS = false;
        public byte FirmwareVersionMajor = (byte)0x00;
        public byte FirmwareVersionMinor = (byte)0x00;
        public bool ForceFirmwareDownload = false;
        // WinCE File Settings
        public string WinCEFilePath = "";
        public byte WinCEVersionMajor = (byte)0x00;
        public byte WinCEVersionMinor = (byte)0x00;
        public bool ForceWinCEDownload = false;
        public int WinCEDownloadSegementSize = 0;
        // CPLD File Settings
        public string CPLDFilePath = "";
        public byte CPLDVersionMajor = (byte)0x00;
        public byte CPLDVersionMinor = (byte)0x00;
        public bool ForceCPLDDownload = false;
        public int CPLDDownloadSegementSize = 0;
        // 5010 File Settings
        public string Firmware5010FilePath = "";
        public byte Firmware5010VersionMajor = (byte)0x00;
        public byte Firmware5010VersionMinor = (byte)0x00;
        public bool Force5010Download = false;
        public int Firmware5010DownloadSegementSize = 0;
        //update Units xml path
        public string UpdateUnitsXML = string.Empty;
        public int MinutesSinceLastForceDownload;

        public GenConfigReader()
        {
            string configPrefix = ReadStringFromConfig("ActiveConfiguration", "(local)");
            Initialise(configPrefix);
        }

        public GenConfigReader(string configPrefix)
        {
            Initialise(configPrefix);
        }

        private void Initialise(string configPrefix)
        {
            // Load the IP/Port of the interface to pass to
            InterfaceBindIP = ReadStringFromConfig(configPrefix + "InterfaceBindIP", "0.0.0.0");
            InterfacePort = ReadIntFromConfig(configPrefix + "InterfaceBindPort", 0);
            HeartbeatSendPeriod = ReadIntFromConfig(configPrefix + "HeartbeatSendPeriod", 0);
            HeartbeatTimeoutPeriod = ReadIntFromConfig(configPrefix + "HeartbeatTimeoutPeriod", 0);
            // Load the IP/Port of the MCC server
            MCCBindIP = ReadStringFromConfig(configPrefix + "MCCBindIP", "0.0.0.0");
            MCCPort = ReadIntFromConfig(configPrefix + "MCCBindPort", 0);
            SecondaryMCCBindIP = ReadStringFromConfig(configPrefix + "SecondaryMCCBindIP", "0.0.0.0");
            SecondaryMCCPort = ReadIntFromConfig(configPrefix + "SecondaryMCCPort", 0);
            // Listener config settings
            ListenerBindIP = ReadStringFromConfig(configPrefix + "ListenerBindIP", "0.0.0.0");
            ListenerPort = ReadIntFromConfig(configPrefix + "ListenerBindPort", 0);
            ListenerRSUPort = ReadIntFromConfig(configPrefix + "ListenerRSUPort", 0); 
            ListenerRemoteStatePort = ReadIntFromConfig(configPrefix + "ListenerRemoteStatePort", 0);
            ListenerLiveUpdatePort = ReadIntFromConfig(configPrefix + "ListenerLiveUpdatePort", 0);
            ListenerRemoteSendPort = ReadIntFromConfig(configPrefix + "ListenerRemoteSendPort", 0);
            ListenerCommsTimeout = ReadIntFromConfig(configPrefix + "ListenerCommsTimeout", 0);
            RegisterForUpdatesFromFleets = ReadStringFromConfig(configPrefix + "RegisterForUpdatesFromFleets", "");
            LiveUpdateVersion = ReadStringFromConfig(configPrefix + "LiveUpdateVersion", "");
            ListenerVersion = ReadStringFromConfig(configPrefix + "LiveUpdateVersion", "");
            ListenerLocalBindPort = ReadIntFromConfig(configPrefix + "ListenerLocalBindPort", 0);
            bLogListenerIncommingPacketData = ReadBooleanFromConfig(configPrefix + "LogListenerIncommingPacketData", false);
            bLogListenerOutgoingPacketData = ReadBooleanFromConfig(configPrefix + "LogListenerOutgoingPacketData", false);
            ClientListenerPort = ReadIntFromConfig(configPrefix + "ClientListenerPort", 0);
            ListenerConnectWithHistory = ReadBooleanFromConfig(configPrefix + "ListenerConnectWithHistory", false);
            // Load the UDP Monitor options
            LoggingPort = -1;
            if (ReadBooleanFromConfig(configPrefix + "MonitorEnabled", true))
                LoggingPort = ReadIntFromConfig(configPrefix + "MonitorBindPort", 15001);

            // Com Port Settings
            ComPortEnabled = ReadBooleanFromConfig(configPrefix + "ComPortEnabled", false);
            ComPortNumber = ReadIntFromConfig(configPrefix + "ComPortNumber", 1);
            ComPortBitRate = ReadIntFromConfig(configPrefix + "ComPortBitRate", 9600);
            ComPortDataBits = ReadIntFromConfig(configPrefix + "ComPortDataBits", 8);
            ComPortParity = ReadStringFromConfig(configPrefix + "ComPortParity", "n");
            ComPortStopBits = ReadIntFromConfig(configPrefix + "ComPortStopBits", 1);
            ComPortUseFlowContol = ReadBooleanFromConfig(configPrefix + "ComPortUseFlowContol", false);
            ComPortReceiveEnabled = ReadBooleanFromConfig(configPrefix + "ComPortReceiveEnabled", false);

            // Load the Packet / Processing options
            TranslatorType = ReadStringFromConfig(configPrefix + "TranslatorType", "5040");
            PacketSize = ReadIntFromConfig(configPrefix + "PacketSize", 1024);
            TranslationBufferSize = ReadIntFromConfig(configPrefix + "TranslationBufferSize", 4096);
            CommunicationBufferSize = ReadIntFromConfig(configPrefix + "CommunicationBufferSize", -1);
            PacketWaitPeriod = ReadIntFromConfig(configPrefix + "PacketWaitPeriod", 10);
            SleepWaitPeriod = ReadIntFromConfig(configPrefix + "SleepWaitPeriod", 100);
            MaxQueueSize = ReadIntFromConfig(configPrefix + "MaxQueueSize", 300);

            // Load the Disconnect Options.
            SendDisconnectMsgOnClose = ReadBooleanFromConfig(configPrefix + "SendDisconnectMsgOnClose", false);
            CheckForDisconnectMsg = ReadBooleanFromConfig(configPrefix + "CheckForDisconnectMsg", false);
            DisconnectMessage = ReadStringFromConfig(configPrefix + "DisconnectMessage", "");

            // Load the Database options
            DSN = ReadStringFromConfig(configPrefix + "DSN", "");
            TransportAppDSN = ReadStringFromConfig(configPrefix + "TransportDSN", "");
            db = ReadStringFromConfig(configPrefix + "db", "");
            transportDB = ReadStringFromConfig(configPrefix + "transportDB", "");
            DatabasePollRate = ReadIntFromConfig(configPrefix + "DatabasePollRate", 60);
            RouteURLWebService = ReadStringFromConfig(configPrefix + "RouteURLWebService", "");

            // Load the Logging options
            LogFileDirectory = ReadStringFromConfig(configPrefix + "LogFileDir", "c:\\MTDataLogs\\");
            LogFilePrefixFormat = ReadStringFromConfig(configPrefix + "LogFileNamePrefixFormat", "YMD");
            LogFileName = ReadStringFromConfig(configPrefix + "LogFileName", "MTData.MDT.Interface");
            LogFileExtension = ReadStringFromConfig(configPrefix + "LogFileExtension", "log");
            DownloadLogFileName = ReadStringFromConfig(configPrefix + "DownloadLogFileName", "MTData.MDT.Interface.Downloads");
            LogToMCCData = ReadBooleanFromConfig(configPrefix + "LogToMCCData", false);
            LogFromMCCData = ReadBooleanFromConfig(configPrefix + "LogFromMCCData", false);
            LogToListenerData = ReadBooleanFromConfig(configPrefix + "LogToListenerData", false);
            LogFromListenerData = ReadBooleanFromConfig(configPrefix + "LogFromListenerData", false);
            LogToCustomerData = ReadBooleanFromConfig(configPrefix + "LogToCustomerData", false);
            LogFromCustomerData = ReadBooleanFromConfig(configPrefix + "LogFromCustomerData", false);
            LogLiveUpdateData = ReadBooleanFromConfig(configPrefix + "LogLiveUpdateData", false);
            LogSQLData = ReadBooleanFromConfig(configPrefix + "LogSQLData", false);
            // Load the MDT Options
            try
            {
                AutoLogoffXMinsAfterIgnOff = (ushort)ReadIntFromConfig(configPrefix + "AutoLogoffXMinsAfterIgnOff", 0);
            }
            catch (System.Exception)
            {
                AutoLogoffXMinsAfterIgnOff = 0;
            }
            FirmwareImagePath = ReadStringFromConfig(configPrefix + "FirmwareImagePath", "");
            UnitSoftwareVersion = ReadIntFromConfig(configPrefix + "UnitSoftwareVersion", 0);
            DownloadSegementSize = ReadIntFromConfig(configPrefix + "DownloadSegementSize", 0);
            CheckGPS = ReadBooleanFromConfig(configPrefix + "CheckGPS", false);

            // Service debug mode flag
            DebugMode = ReadBooleanFromConfig(configPrefix + "DebugMode", false);

            // Load the Signature options
            SignatureConfig.LogSignatureData = ReadBooleanFromConfig(configPrefix + "LogSignatureData", false);
            SignatureConfig.MaxHeight = ReadIntFromConfig(configPrefix + "MaxImageHeight", 80);
            SignatureConfig.MaxWidth = ReadIntFromConfig(configPrefix + "MaxImageWidth", 300);
            SignatureConfig.MaxVectorHeight = ReadIntFromConfig(configPrefix + "MaxVectorImageHeight", 80);
            SignatureConfig.MaxVectorWidth = ReadIntFromConfig(configPrefix + "MaxVectorImageWidth", 300);
            SignatureConfig.PicturePath = ReadStringFromConfig(configPrefix + "PicturePath", "c:\\MTDataLogs\\sigtemp\\");
            SignatureConfig.ResizeFactor = ReadDoubleFromConfig(configPrefix + "ResizeFactor", 0.5);
            SignatureConfig.SignatureBackgroundColor = ReadColorFromConfig(configPrefix + "SignatureBackgroundColor", Color.White);
            SignatureConfig.IncreaseContrast = ReadBooleanFromConfig(configPrefix + "IncreaseContrast", true);
            SignatureConfig.ContrastTolerance = (uint)ReadIntFromConfig(configPrefix + "ContrastTolerance", 100000);
            SignatureConfig.MakeSignatureTransparent = ReadBooleanFromConfig(configPrefix + "MakeSignatureTransparent", false);
            SignatureConfig.SaveImageFile = ReadBooleanFromConfig(configPrefix + "SaveImageFile", true);
            string signature = ConfigurationManager.AppSettings["UseBackgroundGIFforSignatures"];
            if (signature == null)
            {
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                path = System.IO.Path.GetDirectoryName(path);
                signature = path + "\\Back.gif";
            }
            else if (!signature.Contains(":"))
            {
                string path = System.Reflection.Assembly.GetExecutingAssembly().Location;
                path = System.IO.Path.GetDirectoryName(path);
                signature = path + "\\" + signature;
            }
            SignatureConfig.DefaultBackgroundGif = signature;

            // Message confirmation email settings
            bMessageSendRecievedConfirmationViaEmail = ReadBooleanFromConfig(configPrefix + "SendRecievedConfirmationViaEmail", false);
            MessageConfirmationEmailFrom = ReadStringFromConfig(configPrefix + "MessageEmailFrom", "");
            MessageConfirmationEmailTo = ReadStringFromConfig(configPrefix + "MessageEmailTo", "");

            // Reporting Settings 
            ReportServerReportsFolder = ReadStringFromConfig(configPrefix + "ReportServerReportsFolder", "");
            ReportServerTemplatePath = ReadStringFromConfig(configPrefix + "ReportServerTemplatePath", "");
            ReportServerHTTPAddress = ReadStringFromConfig(configPrefix + "ReportServerHTTPAddress", "");
            ReportServerSignaturesFolder = ReadStringFromConfig(configPrefix + "ReportServerSignaturesFolder", "");
            ReportServerLogoRight = ReadStringFromConfig(configPrefix + "ReportServerLogoRight", "");
            ReportServerLogoLeft = ReadStringFromConfig(configPrefix + "ReportServerLogoLeft", "");
            ReportServerLogReportingProcessResults = ReadBooleanFromConfig(configPrefix + "ReportServerLogReportingProcessResults", false);
            ReportServerKillExcelAfterUse = ReadBooleanFromConfig(configPrefix + "ReportServerKillExcelAfterUse", false);
            ReportServerExcelPath = ReadStringFromConfig(configPrefix + "ReportServerExcelPath", "");
            ReportServerExcelVisible = ReadBooleanFromConfig(configPrefix + "ReportServerExcelVisible", false);
            ReportServerShowStreetNames = ReadBooleanFromConfig(configPrefix + "ReportServerShowStreetNames", false);
            ReportServerAvoidOrientation = ReadBooleanFromConfig(configPrefix + "ReportServerAvoidOrientation", false);
            ReportServerAvoidPaperSize = ReadBooleanFromConfig(configPrefix + "ReportServerAvoidPaperSize", false);
            ReportServerAvoidMargins = ReadBooleanFromConfig(configPrefix + "ReportServerAvoidMargins", false);

            // Report server comms settings
            ReportServerListenIP = ReadStringFromConfig(configPrefix + "ReportServerListenIP", "");
            ReportServerListenPort = ReadIntFromConfig(configPrefix + "ReportServerListenPort", 0);
            ReportServerComPortStart = ReadIntFromConfig(configPrefix + "ReportServerComPortStart", 0);
            ReportServerComPortFinish = ReadIntFromConfig(configPrefix + "ReportServerComPortFinish", 0);
            ReportServerTCPTimeout = ReadBooleanFromConfig(configPrefix + "ReportServerTCPTimeout", false);
            ReportServerTCPSendTimeout = ReadIntFromConfig(configPrefix + "ReportServerTCPSendTimeout", 0);
            ReportServerTCPReceiveTimeout = ReadIntFromConfig(configPrefix + "ReportServerTCPReceiveTimeout", 0);
            ReportServerPacketSize = ReadIntFromConfig(configPrefix + "ReportServerPacketSize", 0);
            ReportServerPacketWaitPeriod = ReadIntFromConfig(configPrefix + "ReportServerPacketWaitPeriod", 0);
            ReportServerSleepWaitPeriod = ReadIntFromConfig(configPrefix + "ReportServerSleepWaitPeriod", 0);
            ReportServerBufferSize = ReadIntFromConfig(configPrefix + "ReportServerBufferSize", 0);
            ReportServerSendDisconnectOnClose = ReadBooleanFromConfig(configPrefix + "ReportServerSendDisconnectOnClose", false);
            ReportServerSOP = (byte)ReadIntFromConfig(configPrefix + "ReportServerSOP", 0);
            ReportServerEOP = (byte)ReadIntFromConfig(configPrefix + "ReportServerEOP", 0);
            ReportServerLengthBytes = ReadIntFromConfig(configPrefix + "ReportServerLengthBytes", 0);
            ReportServerFixedLength = ReadBooleanFromConfig(configPrefix + "ReportServerFixedLength", false);
            ReportServerAdditionalHeaderBytes = ReadIntFromConfig(configPrefix + "ReportServerAdditionalHeaderBytes", 0);

            // Email Settings
            EmailToAddress = ReadStringFromConfig(configPrefix + "EmailTargetAddress", "");
            EmailFromAddress = ReadStringFromConfig(configPrefix + "EmailSourceAddress", "");
            EmailFromAddressDisplayName = ReadStringFromConfig(configPrefix + "EmailSourceAddressDisplayName", "");
            EmailSubject = ReadStringFromConfig(configPrefix + "EmailSubject", "");
            EmailSendInterval = ReadIntFromConfig(configPrefix + "EmailSendErrorsEveryXSeconds", 0);
            EmailSendWarningsInterval = ReadIntFromConfig(configPrefix + "EmailSendWarningsEveryXSeconds", 0);

            // Mot File Downloads
            UpdateUnitsXML = ReadStringFromConfig(configPrefix + "UpdateUnitsXML", "UpdateUnits.xml");
            FirmwareVersionMajor = ReadByteFromConfig(configPrefix + "FirmwareVersionMSB", (byte)0x00);
            FirmwareVersionMinor = ReadByteFromConfig(configPrefix + "FirmwareVersionLSB", (byte)0x00);
            ForceFirmwareDownload = ReadBooleanFromConfig(configPrefix + "ForceFirmwareDownload", false);
            // WinCE File Downloads
            WinCEFilePath = ReadStringFromConfig(configPrefix + "WinCEFilePath", "");
            WinCEVersionMajor = ReadByteFromConfig(configPrefix + "WinCEVersionMSB", (byte)0x00);
            WinCEVersionMinor = ReadByteFromConfig(configPrefix + "WinCEVersionLSB", (byte)0x00);
            ForceWinCEDownload = ReadBooleanFromConfig(configPrefix + "ForceWinCEDownload", false);
            WinCEDownloadSegementSize = ReadIntFromConfig(configPrefix + "WinCEDownloadSegementSize", 1024);
            // CPLD File Downloads
            CPLDFilePath = ReadStringFromConfig(configPrefix + "CPLDFilePath", "");
            CPLDVersionMajor = ReadByteFromConfig(configPrefix + "CPLDVersionMSB", (byte)0x00);
            CPLDVersionMinor = ReadByteFromConfig(configPrefix + "CPLDVersionLSB", (byte)0x00);
            ForceCPLDDownload = ReadBooleanFromConfig(configPrefix + "ForceCPLDDownload", false);
            CPLDDownloadSegementSize = ReadIntFromConfig(configPrefix + "CPLDDownloadSegementSize", 1024);
            // 5010 File Downloads
            Firmware5010FilePath = ReadStringFromConfig(configPrefix + "Firmware5010FilePath", "");
            Firmware5010VersionMajor = ReadByteFromConfig(configPrefix + "Firmware5010VersionMSB", (byte)0x00);
            Firmware5010VersionMinor = ReadByteFromConfig(configPrefix + "Firmware5010VersionLSB", (byte)0x00);
            Force5010Download = ReadBooleanFromConfig(configPrefix + "Force5010Download", false);
            Firmware5010DownloadSegementSize = ReadIntFromConfig(configPrefix + "Firmware5010DownloadSegementSize", 1024);
            MinutesSinceLastForceDownload = ReadIntFromConfig(configPrefix + "MinutesSinceLastForceDownload", 60);
        }

        #region Supporting Functions

        private string ReadStringFromConfig(string name, string defaultValue)
        {
            string configValue = ConfigurationManager.AppSettings[name];
            if (configValue == null)
                return defaultValue;
            else
                return configValue;
        }

        private int ReadIntFromConfig(string name, int defaultValue)
        {
            string configValue = ConfigurationManager.AppSettings[name];
            if ((configValue == null) || (configValue == ""))
                return defaultValue;
            else
                return Convert.ToInt32(configValue);
        }

        private double ReadDoubleFromConfig(string name, double defaultValue)
        {
            string configValue = ConfigurationManager.AppSettings[name];
            if ((configValue == null) || (configValue == ""))
                return defaultValue;
            else
                return Convert.ToDouble(configValue);
        }

        private byte ReadByteFromConfig(string name, byte bDefault)
        {
            byte bResult = (byte)0x00;
            try
            {
                bResult = (byte)Convert.ToInt32(ConfigurationManager.AppSettings[name]);
            }
            catch (System.Exception)
            {
                bResult = (byte)bDefault;
            }
            return bResult;
        }

        private bool ReadBooleanFromConfig(string name, bool defaultValue)
        {
            string configValue = ConfigurationManager.AppSettings[name];
            if ((configValue == null) || (configValue == ""))
                return defaultValue;
            else
                return (configValue.ToLower() == "true");
        }

        private Color ReadColorFromConfig(string name, Color defaultValue)
        {
            Color result = defaultValue;
            string configValue = ConfigurationManager.AppSettings[name];
            if ((configValue != null) && (configValue != ""))
            {
                bool tryName = false;
                try
                {
                    int colorRGB = Convert.ToInt32(configValue);
                    result = Color.FromArgb(colorRGB);
                }
                catch (Exception)
                {
                    tryName = true;
                }
                if (tryName)
                {
                    try
                    {
                        result = Color.FromName(configValue);
                    }
                    catch (Exception)
                    {
                        result = defaultValue;
                    }
                }
            }
            return result;
        }

        #endregion

    }
}

using System;
using System.Threading;

namespace MTData.Common.Threading
{
	/// <summary>
	/// This enhanced thread will allow a more resilient management of active threads.
	/// It does this by passing a reference to the calling thread into the handler method,
	/// allowing the handler method to always identify who is running it.
	/// </summary>
	public class EnhancedThread
	{
		public delegate object EnhancedThreadStart(EnhancedThread sender, object data);

		private object SyncRoot = new object();
		private object _data = null;
		private object _result = null;
		private bool _stopping = false;
		private bool _running = false;
		private event EnhancedThreadStart _threadStart;
		private Thread _internalThread;
        
		public EnhancedThread(EnhancedThreadStart threadStart, object data) : this(threadStart, data, true)
		{
		
		}

		public EnhancedThread(EnhancedThreadStart threadStart, object data, bool isBackGround)
		{
			_threadStart += threadStart;
			_data = data;
			_internalThread = new Thread(new System.Threading.ThreadStart(ThreadHandler));
			_internalThread.IsBackground = isBackGround;
		}

		/// <summary>
		/// This is the object (object data) that is passed to the thread procedure when the thread is started.
		/// </summary>
		public object DataObject
		{
			get
			{
				return _data;
			}
			set
			{
				_data = value;
			}
		}

		public bool IsBackGround
		{
			get{ return _internalThread.IsBackground; }
			set{ _internalThread.IsBackground = value; }
		}

		/// <summary>
		/// This is the name used by the thread when writing to the console
		/// </summary>
		public string Name
		{
			get
			{
				return _internalThread.Name;
			}
			set
			{
				_internalThread.Name = value;
			}
		}

		/// <summary>
		/// Start the thread executing. 
		/// </summary>
		public void Start()
		{
			lock(SyncRoot)
			{
				if ((!_running) && (!_stopping))
				{
					_running = true;
					_stopping = false;
                    try
                    {
                        _internalThread.Start();
                    }
                    catch (Exception)
                    {
                        // stop fking bugging me
                    }
				}
			}
		}

		/// <summary>
		/// Stop the thread executing.
		/// </summary>
		public void Stop()
		{
			lock(SyncRoot)
				if (_running)
					_stopping = true;
		}

		/// <summary>
		/// Abort the thread, by forcing a halt.
		/// </summary>
		public void Abort()
		{
			lock(SyncRoot)
			{
				if (_running)
					_internalThread.Abort();
				_stopping = true;
			}
		}

		/// <summary>
		/// This is the standard thread handler which will call the handler passed to this class.
		/// </summary>
		private void ThreadHandler()
		{
			object internalResult = null;
			try
			{
				if (_threadStart != null)
					internalResult = _threadStart(this, _data);
			}
			finally
			{
				lock(SyncRoot)
				{
					_result = internalResult;
					_running = false;
					_stopping = false;
				}
			}
		}

		/// <summary>
		/// This is a property allowing access to the result property of the thread.
		/// </summary>
		public object Result
		{
			get
			{
				lock(SyncRoot)
					return _result;
			}
		}

		public System.Threading.ThreadState State
		{
			get
			{
				System.Threading.ThreadState oState = System.Threading.ThreadState.Unstarted;
				if(_internalThread != null)
				{
					lock(SyncRoot)
						oState = _internalThread.ThreadState;
				}
				return oState;
			}
		}

		/// <summary>
		/// This indicates whether the thread is active or not.
		/// </summary>
		public bool Running
		{
			get
			{
				lock(SyncRoot)
					return _running;
			}
		}

		/// <summary>
		/// This indicates whether the thread is attempting to stop or not.
		/// Running will be true until such time as the thread is stopped.
		/// Stopping will be true once Stop is called, and will continue 
		/// to remain true until Running is false.
		/// </summary>
		public bool Stopping
		{
			get
			{
				lock(SyncRoot)
					return _stopping;
			}
		}
	}
}

﻿namespace MTData.Common.Structs
{
    public struct FleetVehicle
    {
        public readonly int fleetId, vehicleId;

        public FleetVehicle(int fleetId, int vehicleId)
        {
            this.fleetId = fleetId;
            this.vehicleId = vehicleId;
        }
    }
}

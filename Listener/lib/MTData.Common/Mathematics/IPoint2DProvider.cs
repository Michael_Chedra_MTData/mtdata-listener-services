using System;

namespace MTData.Common.Mathematics
{
	/// <summary>
	/// Summary description for IPoint2DProvider.
	/// </summary>
	public interface IPoint2DProvider
	{
		Point2D this[int index] { get; }
		int Count { get; }
	}

	public interface IIsCloseProvider
	{
		bool IsClose(Point2D a, Point2D b);
	}
}

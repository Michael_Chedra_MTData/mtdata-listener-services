using System;
using System.Drawing;

namespace MTData.Common.Mathematics
{
	/// <summary>
	/// 2D cartesian Point representation
	/// </summary>
	public class Point2D
	{
		/// <summary>
		/// X & Y components of Point
		/// </summary>
		public double X;
		public double Y;

		public Point2D()
		{
		}

		public Point2D(double x, double y)
		{
			X = x;
			Y = y;
		}

		public Point2D(Point2D copy)
		{
			X = copy.X;
			Y = copy.Y;
		}

		public Point2D(System.Drawing.Point pt)
		{
			X = pt.X;
			Y = pt.Y;
		}

		public Point2D(PointF pt)
		{
			X = pt.X;
			Y = pt.Y;
		}

		/// <summary>
		/// Create an Array of Point2D's from an Array of Point's
		/// </summary>
		/// <param name="points">Point array to create from</param>
		/// <returns>Array of Point2D</returns>
		public static Point2D[] CreateFrom(Point[] points)
		{
			Point2D[] result = new Point2D[points.Length];
			for(int i = 0 ; i < result.Length ; i++)
				result[i] = new Point2D(points[i]);
			return result;
		}

		/// <summary>
		/// Create an Array of Point2D's from an Array of PointF's
		/// </summary>
		/// <param name="points">PointF array to create from</param>
		/// <returns>Array of Point2D</returns>
		public static Point2D[] CreateFrom(PointF[] points)
		{
			Point2D[] result = new Point2D[points.Length];
			for(int i = 0 ; i < result.Length ; i++)
				result[i] = new Point2D(points[i]);
			return result;
		}

		/// <summary>
		/// Equality test against another Point2D
		/// </summary>
		/// <param name="obj">Object to compare against</param>
		/// <returns>True if compare is a Point2D with exact same X & Y components</returns>
		public override bool Equals(object obj)
		{
			Point2D compare = obj as Point2D;
			if (compare == null)
				return false;

			return (compare.X == X) && (compare.Y == Y);
		}

		/// <summary>
		/// Get the HashCode of this Point2D
		/// </summary>
		/// <returns>Hashcode of this Point2D</returns>
		public override int GetHashCode()
		{
			return X.GetHashCode() + Y.GetHashCode();
		}

		/// <summary>
		/// Convert to a Vector2D
		/// </summary>
		/// <returns>A Vector2D with X & Y components equal to this X & Y component</returns>
		public Vector2D ToVector2D()
		{
			return new Vector2D(X, Y);
		}

		/// <summary>
		/// Create a Vector2D v such that v + a = b
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static Vector2D operator-(Point2D a, Point2D b)
		{
			return new Vector2D(a.X - b.X, a.Y - b.Y);
		}

		/// <summary>
		/// Create a Point2D p such that a - p = b
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static Point2D operator+(Point2D a, Vector2D b)
		{
			return new Point2D(a.X + b.X, a.Y + b.Y);
		}

		/// <summary>
		/// Create a Point2D p such that a + p = b
		/// </summary>
		/// <param name="a"></param>
		/// <param name="b"></param>
		/// <returns></returns>
		public static Point2D operator-(Point2D a, Vector2D b)
		{
			return new Point2D(a.X - b.X, a.Y - b.Y);
		}

		/// <summary>
		/// Move this Point to the intersection of the infinite line segments ptA + vA and ptB + vB.  Returns tA and tB
		/// as the 'time' value for ptA and ptB respectively where the intersection occurs.  Ie if they intersect at ptA then 
		/// tA = 0, if they intersect at ptB then tB = 0.
		/// </summary>
		/// <param name="ptA">Line segment A origin</param>
		/// <param name="vA">Line segment A direction</param>
		/// <param name="ptB">Line segment B origin</param>
		/// <param name="vB">Line segment B direction</param>
		/// <param name="tA">Time relative to Line segment A of intersection</param>
		/// <param name="tB">Time relative to Line segment B of intersection</param>
		/// <returns>True if the intersection occurs on both Line's finite segments</returns>
		public bool IntersectAt(Point2D ptA, Vector2D vA, Point2D ptB, Vector2D vB, out double tA, out double tB)
		{
			tA = 0;
			tB = 0;
			double d = vA.Y * vB.X - vA.X * vB.Y;
			if (d == 0.0)							// Parallel lines never intersect
				return false;
			
			tA = ((ptB.Y - ptA.Y) * vB.X + (ptA.X - ptB.X) * vB.Y) / d;
			tB = 0;
			if (vB.X != 0)
				tB = (tA * vA.X + ptA.X - ptB.X) / vB.X;
			else
				tB = (tA * vA.Y + ptA.Y - ptB.Y) / vB.Y;

			X = ptA.X + tA * vA.X;
			Y = ptA.Y + tA * vA.Y;

			return ((tA >= 0) && (tA <= 1.0) && (tB >= 0) && (tB <= 1.0));
		}

		/// <summary>
		/// Get string representation
		/// </summary>
		/// <returns>X, Y format</returns>
		public override string ToString()
		{
			return string.Format("{0},{1}", X, Y);
		}

		/// <summary>
		/// Convert to a Point
		/// </summary>
		/// <returns></returns>
		public Point ToPoint()
		{
			return new Point((int)X, (int)Y);
		}

		/// <summary>
		/// Convert to a PointF
		/// </summary>
		/// <returns></returns>
		public PointF ToPointF()
		{
			return new PointF((float)X, (float)Y);
		}
	}
}

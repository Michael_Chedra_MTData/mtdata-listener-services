using System;
using System.Collections;

namespace MTData.Common.Formatters
{
	/// <summary>
	/// Summary description for FriendlyFormatter.
	/// </summary>
	public class FriendlyFormatter : IFormatProvider, ICustomFormatter
	{
		private static Hashtable _formatReplacers = new Hashtable();

		public static void AddFormatReplacer(string key, string replace)
		{
			_formatReplacers[key] = replace;
		}

		public object GetFormat(Type formatType)
		{
			if (formatType == typeof(ICustomFormatter))
				return this;
			else
				return null;
		}

		public string Format(string format, object arg, IFormatProvider formatProvider)
		{
			try
			{
				switch(format)
				{
					case "distance":
					{
						Int64 metres = Convert.ToInt64(arg);

						if (metres < 100)
							return string.Format("{0} Metres", metres);
						else
							return string.Format("{0:0.0} Kms", metres / 1000M);
					}

					case "seconds_duration":
					{
						Int64 seconds = Convert.ToInt64(arg);
						DateTime dt = new DateTime(seconds * TimeSpan.TicksPerSecond);
				
						if (seconds < 60)
							return string.Format("{0:%s} Seconds", dt);	
						else if (seconds < 2 * 60)
							return string.Format("1 Minute {0:%s} Seconds", dt);	
						else if (seconds < 60 * 60)
							return string.Format("{0:%m} Minutes {0:%s} Seconds", dt);	
						else if (seconds < 2 * 60 * 60)
							return string.Format("1 Hour {0:%m} Minutes {0:%s} Seconds", dt);	
						else 
							return string.Format("{0:%H} Hours {0:%m} Minutes {0:%s} Seconds", dt);	
					}

					case "minutes_duration":
					{
						DateTime dt;
						if (!(arg is DateTime))
						{
							Int64 minutes = Convert.ToInt64(arg);
							if (minutes < 0)
								minutes = 0;
							dt = new DateTime(minutes * TimeSpan.TicksPerMinute);
						}
						else
							dt = (DateTime)arg;

						if (dt.Ticks < 60 * TimeSpan.TicksPerMinute)
							return string.Format("{0:%m} Minutes", dt);	
						else if (dt.Ticks < 2 * TimeSpan.TicksPerHour)
							return string.Format("1 Hour {0:%m} Minutes", dt);	
						else if (dt.Ticks < TimeSpan.TicksPerDay)
							return string.Format("{0:%H} Hours {0:%m} Minutes", dt);	
						else if (dt.Ticks < 2 * TimeSpan.TicksPerDay)
							return string.Format("1 Day {0:%H} Hours {0:%m} Minutes", dt);	
						else 
							return string.Format("{0:%d} Days {0:%H} Hours {0:%m} Minutes", dt);	
					}

					default:
					{
						foreach(string key in _formatReplacers.Keys)
							if (format == key)
								format = (string) _formatReplacers[key];
					
						IFormattable formattable = arg as IFormattable;
						if (formattable != null)
							return formattable.ToString(format, formatProvider);
						else if (arg != null)
							return arg.ToString();
						else
							return string.Empty;
					}
				}			
			}
			catch
			{
				return string.Empty;
			}
		}
	}	
}

using System;
using System.IO;
using System.Reflection;
using MTData.Common.Interface;

namespace MTData.Common.Loader
{
    /// <summary>
    /// This class will load up instance of classes, given the typename.
    /// When loaded form a configuration node, the supplied instance will be 
    /// automatically configured, if it supports the IConfigurable interface.
    /// </summary>
    public class Loader : ILoader
    {
        /// <summary>
        /// Prepare the class for use.
        /// </summary>
        public Loader()
        {

        }

        private const string ERROR_TYPENODE_NOTFOUND = "Could not find 'type' attribute in supplied Xml Node";
        private const string ERROR_TYPENAME_INVALID = "The type name supplied is invalid";
        private const string ERROR_TYPE_NOTFOUND = "The type '{0}' could not be found";
        private const string ERROR_TYPE_NOTINSTANTIATED = "The type '{0}' could not be instantiated";
        private const string ERROR_TYPE_NOTCONFIGURED = "The type '{0}' could not be configured";

        /// <summary>
        /// Load and instantiate a class instance based on an xml node supplied.
        /// There are two methods used to configure the type to be loaded.
        ///		type Attribute, specifying the full typename and assembly reference
        ///		assembly and class Attributes, which can be combined to provide the typename
        /// </summary>
        /// <param name="configNode"></param>
        /// <returns></returns>
        public object Create(object context, System.Xml.XmlNode configNode)
        {
            string typeName = null;
            System.Xml.XmlNode typeNode = configNode.Attributes.GetNamedItem("type");
            if (typeNode == null)
            {
                typeNode = configNode.Attributes.GetNamedItem("assembly");
                if (typeNode != null)
                {
                    typeName = typeNode.Value;
                    typeNode = configNode.Attributes.GetNamedItem("class");
                    if (typeNode != null)
                        typeName = string.Format("{0},{1}", typeNode.Value, typeName);
                    else
                        typeName = null;
                }
            }
            else
                typeName = typeNode.Value;

            if (typeName == null)
                throw new LoaderException(ERROR_TYPENODE_NOTFOUND);
            else
                return Create(context, typeName, configNode);
        }

        /// <summary>
        /// Create an instance of the typename supplied.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="typeName"></param>
        /// <returns></returns>
        public object Create(object context, string typeName)
        {
            return Create(context, typeName, null);
        }

        /// <summary>
        /// Create an instance of the typename supplied, and configure it from the configNode
        /// </summary>
        /// <param name="context"></param>
        /// <param name="typeName"></param>
        /// <param name="configNode"></param>
        /// <returns></returns>
        public object Create(object context, string typeName, System.Xml.XmlNode configNode)
        {
            if ((typeName == null) || (typeName == ""))
                throw new LoaderException(ERROR_TYPENAME_INVALID);

            Type type = Type.GetType(typeName);

            //	If a codebase has been set, try accessing the assembly through that..
            if ((type == null) && (_codeBase != null))
            {
                string[] splits = typeName.Split(',');
                if (splits.Length >= 2)
                {
                    string assemblyName = Path.Combine(_codeBase, splits[1] + ".dll");
                    Assembly assembly = Assembly.LoadFrom(assemblyName);
                    type = assembly.GetType(splits[0]);
                }
            }

            if (type == null)
                throw new LoaderException(string.Format(ERROR_TYPE_NOTFOUND, typeName));

            return Create(context, type, configNode);
        }

        /// <summary>
        /// This method will create an instance of a givne type, and configure it.
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="configNode"></param>
        /// <returns></returns>
        public object Create(object context, Type type, System.Xml.XmlNode configNode)
        {
            return Create(context, type, configNode, type.Name);
        }

        /// <summary>
        /// This method creates an instance of the specified type and configures it with the 
        /// subnode with the name specified in the configNodeName parameter
        /// </summary>
        /// <param name="context"></param>
        /// <param name="type"></param>
        /// <param name="configNode"></param>
        /// <param name="configNodeName"></param>
        /// <returns></returns>
        public object Create(object context, Type type, System.Xml.XmlNode configNode, string configNodeName)
        {
            object instance = null;
            try
            {
                instance = Activator.CreateInstance(type);
                if (instance == null)
                    throw new LoaderException(string.Format(ERROR_TYPE_NOTINSTANTIATED, type.Name));
            }
            catch (Exception ex)
            {
                throw new LoaderException(string.Format(ERROR_TYPE_NOTINSTANTIATED, type.Name), ex);
            }

            try
            {
                if ((instance is ILoaderConfigurable) ||
                    (instance is IConfigurable))
                {
                    System.Xml.XmlNode typeConfigNode = null;
                    if (configNode != null)
                        typeConfigNode = configNode.SelectSingleNode(configNodeName);

                    if (instance is ILoaderConfigurable)
                        ((ILoaderConfigurable)instance).Configure(this, context, typeConfigNode);
                    else
                        ((IConfigurable)instance).Configure(context, typeConfigNode);
                }
            }
            catch (Exception ex)
            {
                throw new LoaderException(string.Format(ERROR_TYPE_NOTCONFIGURED, type.Name), ex);
            }

            return instance;
        }

        private string _codeBase = null;

        /// <summary>
        /// This is the base path from which an assembly should be loaded if it is not found through standard mechanisms.
        /// </summary>
        public string CodeBase
        {
            get { return _codeBase; }
            set { _codeBase = value; }
        }
    }
}

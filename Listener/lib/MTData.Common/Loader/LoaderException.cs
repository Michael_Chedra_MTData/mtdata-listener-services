using System;

namespace MTData.Common.Loader
{
    /// <summary>
    /// This exception will be raised whenever an error occurs within the Loader.
    /// </summary>
    public class LoaderException : Exception
    {
        /// <summary>
        /// Simple constructor for specifying a message
        /// </summary>
        /// <param name="message"></param>
        public LoaderException(string message)
            : base(message)
        {

        }

        /// <summary>
        /// Simple constructor for creating an instance wrapped around another exception.
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public LoaderException(string message, Exception innerException)
            : base(message, innerException)
        {

        }
    }
}

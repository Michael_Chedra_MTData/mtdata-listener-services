using System;
using System.Threading;
using MTData.Common.Interface;
using MTData.Common.Threading;

namespace MTData.Common.Queues
{
	/// <summary>
	/// This class will provide a thread for reading from an inbound queue.
	/// Once items are avaialble in the queue, it will read them and raise an
	/// event idicating this. This can be re-used anywhere to avoid re-writing 
	/// code to perform this functionality.
	/// </summary>
	public class QueueReaderThread
	{
		#region Enumerations

		/// <summary>
		/// This is the states that the thread can enter.
		/// </summary>
		public enum ReaderThreadState
		{
			Initialised,
			Starting,
			Started,
			Stopping,
			Stopped
		}

		#endregion

		#region Delegate Definitions

		/// <summary>
		/// This delegate identifies how thread state changes are logged
		/// </summary>
		public delegate void ThreadStateChangedDelegate(object sender, ReaderThreadState newState);

		/// <summary>
		/// This delegate identifies the event raised when data is found
		/// </summary>
		public delegate void DataFoundDelegate(object sender, object data);
		
		/// <summary>
		/// This delegate defines the behaviour if an exception is raised.
		/// </summary>
		public delegate void ThreadExceptionDelegate(object sender, Exception ex);

		#endregion

		#region Instance Data

		/// <summary>
		/// This is the thread that will read from the Queue.
		/// Events will be raised on this thread, and will be blocking.
		/// </summary>
		private EnhancedThread _thread = null;

		/// <summary>
		/// This si the queue that the thread will read from.
		/// </summary>
		private QueueInterface _queue = null;

		/// <summary>
		/// This is the number if milliseconds to pause between events.
		/// </summary>
		private int _interEventPause = 0;

		/// <summary>
		/// This is the number of milliseconds to pause if no items are in the queue.
		/// </summary>
		private int _noopPause = 0;

		#endregion

		#region Events

		/// <summary>
		/// This is raised when the state changes.
		/// </summary>
		public event ThreadStateChangedDelegate ThreadStateChanged;

		/// <summary>
		/// This event is raised when data is dequeued.
		/// </summary>
		public event DataFoundDelegate DataFound;

		/// <summary>
		/// This event is raised whenever there is a thread exception that has not been 
		/// handled elsewhere.
		/// </summary>
		public event ThreadExceptionDelegate ThreadException;

		#endregion

		#region Internal Methods

		/// <summary>
		/// Prepare the listener for use.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="queue"></param>
		public QueueReaderThread(string name, QueueInterface queue)
		{
			_queue = queue;

			_thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(EnhancedThreadHandler), _queue);
			_thread.Name = name;
		}

		/// <summary>
		/// This constructor allows delays to be specified between successive events being raised.
		/// </summary>
		/// <param name="name"></param>
		/// <param name="queue"></param>
		/// <param name="interEventPause"></param>
		/// <param name="noopPause"></param>
		public QueueReaderThread(string name, QueueInterface queue, int interEventPause, int noopPause) : this(name, queue)
		{
			_interEventPause = interEventPause;
			_noopPause = noopPause;
		}

		/// <summary>
		/// Notify listeners of the change in state.
		/// </summary>
		/// <param name="state"></param>
		private void OnThreadStateChanged(ReaderThreadState state)
		{
			if (ThreadStateChanged != null)
				ThreadStateChanged(this, state);
		}

		/// <summary>
		/// This method will pass on an exception if someone is listening.
		/// </summary>
		/// <param name="ex"></param>
		private void OnThreadException(Exception ex)
		{
			if (ThreadException != null)
				ThreadException(this, ex);
		}

		/// <summary>
		/// this thread handler will read form the queue, and raise an event for each
		/// item retrieved form the queue.
		/// </summary>
		/// <param name="sender">this is the enhanced thread instance</param>
		/// <param name="data">This is the inbound queue</param>
		/// <returns>no result from this thread.</returns>
		private object EnhancedThreadHandler(EnhancedThread sender, object data)
		{
			OnThreadStateChanged(ReaderThreadState.Started);
			try
			{
				QueueInterface inboundQ = (QueueInterface)data;
			
				while(!sender.Stopping)
				{
					try
					{
						int itemCount = 0;
						lock(inboundQ.SyncRoot)
							itemCount = inboundQ.Count;

						if (itemCount > 0)
						{
							for(int loop = 0; loop < itemCount; loop++)
							{
								object queueData = null;
								lock(inboundQ.SyncRoot)
									queueData = inboundQ.Dequeue();

								try
								{
									if(DataFound != null)
										DataFound(this, queueData);
								}
								catch(Exception dataEx)
								{
									OnThreadException(dataEx);
								}

								if ((_interEventPause > 0) && (loop < itemCount))
									Thread.Sleep(_interEventPause);
							}
						}
						else
							if (_noopPause > 0)
								Thread.Sleep(_noopPause);
					}
					catch(Exception ex)
					{
						OnThreadException(ex);
					}
				}
			}
			finally
			{
				OnThreadStateChanged(ReaderThreadState.Stopped);
			}
			return null;
		}

		#endregion

		#region Public Methods and Properties

		/// <summary>
		/// Start the listener
		/// </summary>
		public void Start()
		{
			OnThreadStateChanged(ReaderThreadState.Starting);
			_thread.Start();
		}

		/// <summary>
		/// Stop the listener.
		/// </summary>
		public void Stop()
		{
			OnThreadStateChanged(ReaderThreadState.Stopping);
			_thread.Stop();
		}

		/// <summary>
		/// Indicates whther the thread is currently running or not.
		/// </summary>
		public bool Running
		{
			get{ return _thread.Running; }
		}

		/// <summary>
		/// Indicates whether the loop is stopping or not.
		/// </summary>
		public bool Stopping
		{
			get{ return _thread.Stopping; }
		}

		#endregion

	}
}

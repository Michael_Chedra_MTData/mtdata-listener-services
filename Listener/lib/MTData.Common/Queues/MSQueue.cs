using System;
using System.Messaging;
using System.Text;
using System.Xml;
using System.Xml.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using MTData.Common.Utilities;

namespace MTData.Common.Queues
{
    /// <summary>
    /// Queue is the implementation of the MSMQ functionality.
    /// </summary>
    public class MSQueue
    {
        #region Variables

        private string _queueName;
        private MessageQueue _queue;
        private static object _syncRoot = new object();
        private int iQueueCount = 0;
        private int iMaxQueueSize = 0;
        #endregion

        #region Properties

        public MessageQueue Q
        {
            get { return this._queue; }
        }

        public string QName
        {
            get { return this._queueName; }
        }

        public int QSize
        {
            get { return iQueueCount; }
        }
        public int MaxSize
        {
            get { return iMaxQueueSize; }
        }
        #endregion

        #region Constructor

        public MSQueue(string QueueName)
        {
            CreateQueue(QueueName, -1, new System.Messaging.BinaryMessageFormatter());
        }

        public MSQueue(string QueueName, int iMaxQueue)
        {
            CreateQueue(QueueName, iMaxQueue, new System.Messaging.BinaryMessageFormatter());
        }

        public MSQueue(string QueueName, int iMaxQueue, IMessageFormatter formatter)
        {
            CreateQueue(QueueName, iMaxQueue, formatter);
        }

        private void CreateQueue(string QueueName, int iMaxQueue, IMessageFormatter formatter)
        {
            //System.Messaging.Message[] oMessages = null;

            this._queueName = QueueName;
            if (!this._queueName.StartsWith("FormatName:direct=OS:"))
                this._queueName = string.Concat("FormatName:direct=OS:", this._queueName);
            this._queue = new MessageQueue(this._queueName);
            this._queue.Formatter = formatter;
            this._queue.MessageReadPropertyFilter.SetAll();
            iMaxQueueSize = iMaxQueue;
            ReCount();
        }
        private int GetMessageCount(MessageQueue q)
        {
            int count = 0;
            Cursor cursor = q.CreateCursor();

            Message m = PeekWithoutTimeout(q, cursor, PeekAction.Current);
            {
                count = 1;
                while ((m = PeekWithoutTimeout(q, cursor, PeekAction.Next)) != null)
                {
                    count++;
                }
            }
            return count;
        }
        private Message PeekWithoutTimeout(MessageQueue q, Cursor cursor, PeekAction action)
        {
            Message ret = null;
            try
            {
                ret = q.Peek(new TimeSpan(1), cursor, action);
            }
            catch (MessageQueueException mqe)
            {
                if (!mqe.Message.ToLower().Contains("timeout"))
                {
                    throw;
                }
            }
            return ret;
        }
        #endregion

        #region Functions
        public bool InsertMessageToQueue(object SendMessage, System.Messaging.MessagePriority priority)
        {
            return InsertMessageToQueue(SendMessage, priority, false);
        }

        public bool InsertMessageToQueue(object SendMessage, System.Messaging.MessagePriority priority, bool bSetMessageAsRecoverable)
        {
            bool fRet = false;

            try
            {
                System.Messaging.Message msg = new System.Messaging.Message(SendMessage, this._queue.Formatter);
                msg.Priority = priority;
                msg.Recoverable = bSetMessageAsRecoverable;
                lock (_syncRoot)
                {
                    if (iMaxQueueSize > 0)
                    {
                        if (iQueueCount < iMaxQueueSize)
                        {
                            this._queue.Send(msg);
                            iQueueCount++;
                            fRet = true;
                        }
                        else
                            fRet = false;
                    }
                    else
                    {
                        if (this.Q.CanWrite)
                        {
                            this._queue.Send(msg);
                            fRet = true;
                        }
                        else
                            fRet = false;
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return fRet;
        }

        public object PeekBody()
        {
            object oRet = null;
            System.Messaging.Message message = null;

            try
            {
                message = this.Q.Peek(new TimeSpan(0, 0, 0, 0, 100));
            }
            catch (System.Exception)
            {
                message = null;
            }
            if (message != null)
                oRet = message.Body;

            return oRet;
        }
        public object ReadBody()
        {
            object oRet = null;
            System.Messaging.Message message = null;

            try
            {
                message = this.Q.Receive(new TimeSpan(0, 0, 0, 0, 100));
                if (iQueueCount > 0)
                {
                    iQueueCount--;
                }
            }
            catch (System.Exception)
            {
                message = null;
            }
            if (message != null)
                oRet = message.Body;

            return oRet;
        }

        public void Delete()
        {
            if (this._queueName.Length <= 0)
                throw new ApplicationException("Must specify a MSMQ Queue name to delete.");
            else
                System.Messaging.MessageQueue.Delete(this._queueName);
        }

        /// <summary>
        /// re count the message's on the queue
        /// </summary>
        public int ReCount()
        {
            try
            {
                iQueueCount = GetMessageCount(this._queue);
            }
            catch (System.Exception)
            {
                iQueueCount = 0;
            }
            return iQueueCount;
        }
        #endregion
        #region Queue SQL Command Functions
        public byte[] ConvertSQLCommandToByteArray(SqlCommand oCmd)
        {
            byte[] bRet = null;
            MemoryStream stream = null;
            DateTime dtTemp = DateTime.MinValue;
            Guid oGUID = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0); ;

            try
            {
                #region Create a new stream and writer and write the commands details

                stream = new MemoryStream();
                PacketUtilities.WriteToStream(stream, (byte)0x01);	// Start of SQL Command
                PacketUtilities.WriteToStream(stream, oCmd.CommandText);
                PacketUtilities.WriteToStream(stream, (byte)0x0A);
                if (oCmd.CommandType == System.Data.CommandType.StoredProcedure)
                    PacketUtilities.WriteToStream(stream, (byte)0x01);
                else if (oCmd.CommandType == System.Data.CommandType.TableDirect)
                    PacketUtilities.WriteToStream(stream, (byte)0x02);
                else if (oCmd.CommandType == System.Data.CommandType.Text)
                    PacketUtilities.WriteToStream(stream, (byte)0x03);
                PacketUtilities.WriteToStream(stream, oCmd.CommandTimeout);
                PacketUtilities.WriteToStream(stream, oCmd.Parameters.Count);
                #endregion
                #region For each parameter, write the parameter details.
                for (int X = 0; X < oCmd.Parameters.Count; X++)
                {
                    string sTypeName = oCmd.Parameters[X].SqlDbType.ToString();
                    PacketUtilities.WriteToStream(stream, Convert.ToInt64(oCmd.Parameters[X].ParameterName));
                    PacketUtilities.WriteToStream(stream, (byte)0x0A);
                    PacketUtilities.WriteToStream(stream, sTypeName);
                    PacketUtilities.WriteToStream(stream, (byte)0x0A);
                    PacketUtilities.WriteToStream(stream, oCmd.Parameters[X].Precision);
                    PacketUtilities.WriteToStream(stream, oCmd.Parameters[X].Scale);
                    PacketUtilities.WriteToStream(stream, oCmd.Parameters[X].Size);
                    if (oCmd.Parameters[X].Direction == System.Data.ParameterDirection.Input)
                        PacketUtilities.WriteToStream(stream, (byte)0x01);
                    else if (oCmd.Parameters[X].Direction == System.Data.ParameterDirection.InputOutput)
                        PacketUtilities.WriteToStream(stream, (byte)0x02);
                    else if (oCmd.Parameters[X].Direction == System.Data.ParameterDirection.Output)
                        PacketUtilities.WriteToStream(stream, (byte)0x03);
                    else if (oCmd.Parameters[X].Direction == System.Data.ParameterDirection.ReturnValue)
                        PacketUtilities.WriteToStream(stream, (byte)0x04);

                    switch (sTypeName)
                    {
                        #region BigInt
                        case "BigInt":
                            PacketUtilities.WriteToStream(stream, Convert.ToInt64(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Binary
                        case "Binary":
                            PacketUtilities.WriteToStream(stream, ((byte[])oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, (byte[])oCmd.Parameters[X].Value);
                            break;
                        #endregion
                        #region Bit
                        case "Bit":
                            PacketUtilities.WriteToStream(stream, Convert.ToByte(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Char
                        case "Char":
                            PacketUtilities.WriteToStream(stream, Convert.ToChar(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region DateTime
                        case "DateTime":
                            dtTemp = (DateTime)oCmd.Parameters[X].Value;
                            PacketUtilities.WriteToStream(stream, dtTemp.ToOADate());
                            break;
                        #endregion
                        #region Decimal
                        case "Decimal":
                            PacketUtilities.WriteToStream(stream, Convert.ToDecimal(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Float
                        case "Float":
                            PacketUtilities.WriteToStream(stream, Convert.ToDouble(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Image
                        case "Image":
                            PacketUtilities.WriteToStream(stream, ((byte[])oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, (byte[])oCmd.Parameters[X].Value);
                            break;
                        #endregion
                        #region Int
                        case "Int":
                            PacketUtilities.WriteToStream(stream, Convert.ToInt32(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Money
                        case "Money":
                            PacketUtilities.WriteToStream(stream, Convert.ToDecimal(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region NChar
                        case "NChar":
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region NText
                        case "NText":
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region NVarChar
                        case "NVarChar":
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Real
                        case "Real":
                            PacketUtilities.WriteToStream(stream, Convert.ToDouble(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region SmallDateTime
                        case "SmallDateTime":
                            dtTemp = (DateTime)oCmd.Parameters[X].Value;
                            PacketUtilities.WriteToStream(stream, dtTemp.ToOADate());
                            break;
                        #endregion
                        #region SmallInt
                        case "SmallInt":
                            PacketUtilities.WriteToStream(stream, Convert.ToInt16(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region SmallMoney
                        case "SmallMoney":
                            PacketUtilities.WriteToStream(stream, Convert.ToDecimal(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Text
                        case "Text":
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region Timestamp
                        case "Timestamp":
                            dtTemp = (DateTime)oCmd.Parameters[X].Value;
                            PacketUtilities.WriteToStream(stream, dtTemp.ToOADate());
                            break;
                        #endregion
                        #region TinyInt
                        case "TinyInt":
                            PacketUtilities.WriteToStream(stream, (byte)oCmd.Parameters[X].Value);
                            break;
                        #endregion
                        #region UniqueIdentifier
                        case "UniqueIdentifier":
                            oGUID = (Guid)oCmd.Parameters[X].Value;
                            PacketUtilities.WriteToStream(stream, oGUID.ToByteArray().Length);
                            PacketUtilities.WriteToStream(stream, (byte[])oGUID.ToByteArray());
                            break;
                        #endregion
                        #region VarBinary
                        case "VarBinary":
                            PacketUtilities.WriteToStream(stream, ((byte[])oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, (byte[])oCmd.Parameters[X].Value);
                            break;
                        #endregion
                        #region VarChar
                        case "VarChar":
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value).Length);
                            PacketUtilities.WriteToStream(stream, Convert.ToString(oCmd.Parameters[X].Value));
                            break;
                        #endregion
                        #region default
                        default:
                            PacketUtilities.WriteToStream(stream, (byte)0x00);
                            break;
                        #endregion
                    }
                }
                #endregion
                #region Write the stream out to a byte array and close/dispose the writer and stream objects
                bRet = stream.ToArray();
                stream.Close();
                stream = null;
                #endregion
            }
            catch (System.Exception ex)
            {
                stream = null;
                bRet = null;
                throw (ex);
            }
            return bRet;
        }
        public SqlCommand ConvertByteArrayToSQLCommand(byte[] bData)
        {
            #region Local Vars
            MemoryStream stream = null;
            SqlCommand oRet = null;
            SqlParameter oParam = null;
            byte bTemp = (byte)0x00;
            string sCommandText = "";
            int iCommandTimeout = 0;
            int iParameterCount = 0;
            string sParameterName = "";
            string sParameterType = "";
            byte bPrecision = (byte)0x00;
            byte bScale = (byte)0x00;
            int iSize = 0;
            byte bDirection = (byte)0x00;
            uint uiValue = 0;
            int iValue = 0;
            ushort isValue = 0;
            byte[] baTemp = null;
            char cTemp = ' ';
            double dTemp = 0;
            decimal dcTemp = 0;
            string sTemp = "";
            Guid oGUID = new Guid(0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0);
            ParameterDirection oDirection = ParameterDirection.Input;
            #endregion

            try
            {
                #region Read the SQL command from the byte array
                stream = new MemoryStream(bData);
                PacketUtilities.ReadFromStream(stream, ref bTemp);
                if (bTemp == (byte)0x01)
                {
                    PacketUtilities.ReadFromStream(stream, (byte)0x0A, ref sCommandText);
                    if (sCommandText != "")
                    {
                        oRet = new SqlCommand(sCommandText);
                        PacketUtilities.ReadFromStream(stream, ref bTemp);
                        if (bTemp == (byte)0x01)
                            oRet.CommandType = System.Data.CommandType.StoredProcedure;
                        else if (bTemp == (byte)0x02)
                            oRet.CommandType = System.Data.CommandType.TableDirect;
                        else
                            oRet.CommandType = System.Data.CommandType.Text;
                        PacketUtilities.ReadFromStream(stream, ref iCommandTimeout);
                        oRet.CommandTimeout = iCommandTimeout;
                        PacketUtilities.ReadFromStream(stream, ref iParameterCount);
                        for (int X = 0; X < iParameterCount; X++)
                        {
                            PacketUtilities.ReadFromStream(stream, (byte)0x0A, ref sParameterName);
                            PacketUtilities.ReadFromStream(stream, (byte)0x0A, ref sParameterType);
                            PacketUtilities.ReadFromStream(stream, ref bPrecision);
                            PacketUtilities.ReadFromStream(stream, ref bScale);
                            PacketUtilities.ReadFromStream(stream, ref iSize);
                            PacketUtilities.ReadFromStream(stream, ref bDirection);
                            if (bDirection == (byte)0x01)
                                oDirection = System.Data.ParameterDirection.Input;
                            else if (bDirection == (byte)0x02)
                                oDirection = System.Data.ParameterDirection.InputOutput;
                            else if (bDirection == (byte)0x03)
                                oDirection = System.Data.ParameterDirection.Output;
                            else
                                oDirection = System.Data.ParameterDirection.ReturnValue;

                            switch (sParameterType)
                            {
                                #region BigInt
                                case "BigInt":
                                    PacketUtilities.ReadFromStream(stream, ref uiValue);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.BigInt);
                                    oParam.Direction = oDirection;
                                    oParam.Value = uiValue;
                                    break;
                                #endregion
                                #region Binary
                                case "Binary":
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    baTemp = new byte[iValue];
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref baTemp);														// Byte Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Binary, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = baTemp;
                                    break;
                                #endregion
                                #region Bit
                                case "Bit":
                                    PacketUtilities.ReadFromStream(stream, ref bTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Bit);
                                    oParam.Direction = oDirection;
                                    oParam.Value = bTemp;
                                    break;
                                #endregion
                                #region Char
                                case "Char":
                                    PacketUtilities.ReadFromStream(stream, ref cTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Char);
                                    oParam.Direction = oDirection;
                                    oParam.Value = cTemp;
                                    break;
                                #endregion
                                #region DateTime
                                case "DateTime":
                                    PacketUtilities.ReadFromStream(stream, ref dTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.DateTime);
                                    oParam.Direction = oDirection;
                                    oParam.Value = DateTime.FromOADate(dTemp);
                                    break;
                                #endregion
                                #region Decimal
                                case "Decimal":
                                    PacketUtilities.ReadFromStream(stream, ref dcTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Decimal);
                                    oParam.Precision = bPrecision;
                                    oParam.Scale = bScale;
                                    oParam.Direction = oDirection;
                                    oParam.Value = dcTemp;
                                    break;
                                #endregion
                                #region Float
                                case "Float":
                                    PacketUtilities.ReadFromStream(stream, ref dTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Float);
                                    oParam.Precision = bPrecision;
                                    oParam.Scale = bScale;
                                    oParam.Direction = oDirection;
                                    oParam.Value = dTemp;
                                    break;
                                #endregion
                                #region Image
                                case "Image":
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    baTemp = new byte[iValue];
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref baTemp);														// Byte Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Image, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = baTemp;
                                    break;
                                #endregion
                                #region Int
                                case "Int":
                                    PacketUtilities.ReadFromStream(stream, ref iValue);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Int);
                                    oParam.Direction = oDirection;
                                    oParam.Value = iValue;
                                    break;
                                #endregion
                                #region Money
                                case "Money":
                                    PacketUtilities.ReadFromStream(stream, ref dTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Money);
                                    oParam.Precision = bPrecision;
                                    oParam.Scale = bScale;
                                    oParam.Direction = oDirection;
                                    oParam.Value = dTemp;
                                    break;
                                #endregion
                                #region NChar
                                case "NChar":
                                    sTemp = "";
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref sTemp);														// String Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.NChar, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = sTemp;
                                    break;
                                #endregion
                                #region NText
                                case "NText":
                                    sTemp = "";
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref sTemp);														// String Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.NText, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = sTemp;
                                    break;
                                #endregion
                                #region NVarChar
                                case "NVarChar":
                                    sTemp = "";
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref sTemp);														// String Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.NVarChar, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = sTemp;
                                    break;
                                #endregion
                                #region Real
                                case "Real":
                                    PacketUtilities.ReadFromStream(stream, ref dTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Real);
                                    oParam.Precision = bPrecision;
                                    oParam.Scale = bScale;
                                    oParam.Direction = oDirection;
                                    oParam.Value = dTemp;
                                    break;
                                #endregion
                                #region SmallDateTime
                                case "SmallDateTime":
                                    PacketUtilities.ReadFromStream(stream, ref dTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.SmallDateTime);
                                    oParam.Direction = oDirection;
                                    oParam.Value = DateTime.FromOADate(dTemp);
                                    break;
                                #endregion
                                #region SmallInt
                                case "SmallInt":
                                    PacketUtilities.ReadFromStream(stream, ref isValue);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.SmallInt);
                                    oParam.Direction = oDirection;
                                    oParam.Value = isValue;
                                    break;
                                #endregion
                                #region SmallMoney
                                case "SmallMoney":
                                    PacketUtilities.ReadFromStream(stream, ref dcTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.SmallMoney);
                                    oParam.Precision = bPrecision;
                                    oParam.Scale = bScale;
                                    oParam.Direction = oDirection;
                                    oParam.Value = dcTemp;
                                    break;
                                #endregion
                                #region Text
                                case "Text":
                                    sTemp = "";
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref sTemp);														// String Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Text, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = sTemp;
                                    break;
                                #endregion
                                #region Timestamp
                                case "Timestamp":
                                    PacketUtilities.ReadFromStream(stream, ref dTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.Timestamp);
                                    oParam.Direction = oDirection;
                                    oParam.Value = DateTime.FromOADate(dTemp);
                                    break;
                                #endregion
                                #region TinyInt
                                case "TinyInt":
                                    PacketUtilities.ReadFromStream(stream, ref bTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.TinyInt);
                                    oParam.Direction = oDirection;
                                    oParam.Value = bTemp;
                                    break;
                                #endregion
                                #region UniqueIdentifier
                                case "UniqueIdentifier":
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    baTemp = new byte[iValue];
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref baTemp);														// Byte Data
                                    oGUID = new Guid(baTemp);
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.UniqueIdentifier);
                                    oParam.Direction = oDirection;
                                    oParam.Value = oGUID;
                                    break;
                                #endregion
                                #region VarBinary
                                case "VarBinary":
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    baTemp = new byte[iValue];
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref baTemp);														// Byte Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.VarBinary, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = baTemp;
                                    break;
                                #endregion
                                #region VarChar
                                case "VarChar":
                                    sTemp = "";
                                    PacketUtilities.ReadFromStream(stream, ref iValue);																			// Length
                                    PacketUtilities.ReadFromStream(stream, (int)iValue, ref sTemp);														// String Data
                                    oParam = oRet.Parameters.Add(sParameterName, SqlDbType.VarChar, iValue);
                                    oParam.Direction = oDirection;
                                    oParam.Value = sTemp;
                                    break;
                                #endregion
                                #region default
                                default:
                                    ((SqlParameter)oRet.Parameters.Add(sParameterName, SqlDbType.Variant)).Value = 0;
                                    break;
                                #endregion
                            }
                        }
                    }
                }
                #endregion
                #region Close/dispose the stream object
                stream.Close();
                stream = null;
                #endregion
            }
            catch (System.Exception ex)
            {
                stream = null;
                oRet = null;
                throw (ex);
            }
            return oRet;
        }
        #endregion
    }
}

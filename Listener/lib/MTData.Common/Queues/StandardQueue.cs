using System;
using System.Collections;
using MTData.Common.Interface;

//Use the name "Queues" to avoid conflict with System.Collection.Queue
namespace MTData.Common.Queues
{
	/// <summary>
	/// Standard implementation of the queue interface.
	/// </summary>
	public class StandardQueue : QueueInterface
	{
		private Queue _queue = null;
		private int _maxEntries = 300;
        private int _messageEnqueueCount = 0;
        private int _messageDequeueCount = 0;
        private object _statsSync = new object();

		public StandardQueue(int maxEntries)
		{
			_maxEntries = maxEntries;
			_queue = Queue.Synchronized(new System.Collections.Queue());
		}

		public StandardQueue() : this(400000)
		{
		}

		#region QueueInterface Members

		public object SyncRoot
		{
			get
			{
				return _queue.SyncRoot;
			}
		}

		/// <summary>
		/// Enqueue the data. If the queue gets above a certain size,
		/// it will start dropping off entries as they will be out of date.
		/// </summary>
		/// <param name="entry"></param>
		public void Enqueue(object entry)
		{
            if ((_maxEntries > 0) && (_queue.Count >= _maxEntries))
            {
                throw new Exception("Queue has reached max size, unable to add entry to the queue.");
            }

			_queue.Enqueue(entry);

            lock (_statsSync)
            {
                _messageEnqueueCount++;
            }
        }

		/// <summary>
		/// Dequeue the next entry in the queue.
		/// </summary>
		/// <returns></returns>
		public object Dequeue()
		{
            lock (_statsSync)
            {
                _messageDequeueCount++;
            }
            return _queue.Dequeue();
		}

		/// <summary>
		/// Only dequeue if they match
		/// </summary>
		/// <param name="entry"></param>
		/// <returns></returns>
		public object DequeueEx(object entry)
		{
            if (_queue.Peek() == entry)
            {
                lock (_statsSync)
                {
                    _messageDequeueCount++;
                }
                return _queue.Dequeue();
            }
            else
                return null;
		}

		/// <summary>
		/// Return the item at the head of the queue.
		/// </summary>
		/// <returns></returns>
		public object Peek()
		{
			return _queue.Peek();
		}

		/// <summary>
		/// Return the queue count
		/// </summary>
		public int Count
		{
			get
			{
				return _queue.Count;
			}
		}

		public void Clear()
		{
			lock(_queue.SyncRoot)
				_queue.Clear();
		}

        public int StatsCountAndReset
        {
            get
            {
                int count = 0;
                lock (_statsSync)
                {
                    count = _messageEnqueueCount;
                    _messageEnqueueCount = 0;
                }
                return count;
            }
        }

        public int StatsDeQueueCountAndReset
        {
            get
            {
                int count = 0;
                lock (_statsSync)
                {
                    count = _messageDequeueCount;
                    _messageDequeueCount = 0;
                }
                return count;

            }
        }
        #endregion
    }
}

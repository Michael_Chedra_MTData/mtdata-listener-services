using System;
using System.IO;
using System.Text;
using System.Security.Cryptography;

namespace MTData.Common.Crypto
{
    public class TextEncryption
    {
        SymmetricAlgorithm alg = new TripleDESCryptoServiceProvider();

        public TextEncryption()
        {
        }


        public string EncryptBase64(string sAnyString)
        {
            string strTo = System.Convert.ToBase64String(Encoding.Unicode.GetBytes(sAnyString.ToCharArray()));
            return strTo;
        }

        public string DecryptBase64(string sAnyString)
        {
            string strFrom = Encoding.Unicode.GetString(System.Convert.FromBase64String(sAnyString));
            return strFrom;
        }

        public string EncryptSaltBase64(string username, string password)
        {
            // generate a 64-bit salt using a secure PRNG
            byte[] salt = new byte[64 / 8];
            salt = Convert.FromBase64String("2CvptTvNQmc=");
            byte[] convertedToBytes = Encoding.UTF8.GetBytes((salt + password + username.ToLower()));
            HashAlgorithm hashType = new SHA512Managed();
            byte[] hashBytes = hashType.ComputeHash(convertedToBytes);

            return Convert.ToBase64String(hashBytes);
        }

        private byte[] PerformCryptoOperation(ICryptoTransform op, byte[] input)
        {
            // Create a buffer to hold the output
            MemoryStream msOutput = new MemoryStream();

            // Create a crypto stream that will transform anything
            // we write into it storing the results in the buffer 
            // we created
            CryptoStream encStream = new CryptoStream(msOutput, op, CryptoStreamMode.Write);

            // Write the input through to the output buffer. 
            encStream.Write(input, 0, input.Length);

            // Important - the CryptoStream caches
            encStream.Close();

            // Turn it back into a byte array
            return msOutput.ToArray();
        }

        private string RandomKey()
        {
            //			// We'd like secure random numbers, please
            //			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider(); 
            //
            //			// Key size is in bits
            //			byte[] keybytes = new byte[alg.KeySize / 8]; 
            //			rng.GetBytes(keybytes);
            //
            //			// Display it in base64
            //			return Convert.ToBase64String(keybytes); 
            return "B+72yv+IVIkhoYwRdXPBYdtOvZrqImcO";
        }

        private string RandomIV()
        {
            //			// We'd like secure random numbers, please
            //			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider(); 
            //
            //			// Block size is in bits - IV must match block size
            //			byte[] ivbytes = new byte[alg.BlockSize / 8]; 
            //			rng.GetBytes(ivbytes);
            //
            //			// Display it in base64
            //			return Convert.ToBase64String(ivbytes); 
            return "6pYPR/3JOy0=";
        }

        public string Encrypt(string splaintext)
        {
            // Create an encryptor based on the key and IV specified in the UI
            ICryptoTransform enc = alg.CreateEncryptor(
                Convert.FromBase64String(RandomKey()),
                Convert.FromBase64String(RandomIV())
                );

            // Turn the input from a string into an array of bytes
            System.Text.ASCIIEncoding ascii = new System.Text.ASCIIEncoding();
            byte[] plaintext = ascii.GetBytes(splaintext);

            // Encrypt the input
            byte[] cyphertext = PerformCryptoOperation(enc, plaintext);

            // Display the resulting byte array as base64

            return Convert.ToBase64String(cyphertext);
        }

        public string Decrypt(string ciphertext)
        {
            // Create a dencryptor based on the key and IV specified in the UI
            ICryptoTransform dec = alg.CreateDecryptor(
                Convert.FromBase64String(RandomKey()),
                Convert.FromBase64String(RandomIV())
                );

            // Get the cyphertext as an array of bytes
            byte[] cyphertext = Convert.FromBase64String(ciphertext);

            // Decrypt it
            byte[] plaintext = PerformCryptoOperation(dec, cyphertext);

            // It's an array of bytes, but we want a string
            System.Text.ASCIIEncoding ascii = new System.Text.ASCIIEncoding();

            return ascii.GetString(plaintext);
        }
    }
}

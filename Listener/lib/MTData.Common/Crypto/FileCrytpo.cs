using System;
using System.IO;
using System.Security.Cryptography;

namespace MTData.Common.Crypto
{
	/// <summary>
	/// This class will use standard encryption algorithms to encrypt and decrypt 
	/// files.
	/// </summary>
	public class FileCrytpo
	{
		private byte[] _cryptoKey = null;

		public FileCrytpo(byte[] cryptoKey)
		{
			_cryptoKey = cryptoKey;
		}
		
		public void Encrypt(Stream sourceStream, Stream targetStream)
		{
			long fileSize = sourceStream.Length;
			int bytesRead = 0;

			byte[] salt = new byte[] {0xF0, 0xF1, 0xF2, 0xF3, 0xF4, 0xF5, 0xF6, 0xF7, 0x18, 0x19, 0x1a, 0x1b, 0x1c, 0x1d, 0x1e, 0x1f};
				
			Rijndael alg = Rijndael.Create();
			alg.BlockSize = 128;
			alg.Padding = PaddingMode.PKCS7;
			alg.KeySize = 128;
			alg.Mode = CipherMode.ECB;
			
			byte[] IV = alg.IV;

			byte[] buffer = new byte[alg.BlockSize];

			targetStream.Write(IV, 0, IV.Length);
			targetStream.Write(salt, 0, salt.Length);
				
			HashAlgorithm hasher = SHA256.Create();
			CryptoStream streamOut = new CryptoStream(targetStream, alg.CreateEncryptor(_cryptoKey, IV), CryptoStreamMode.Write);
			CryptoStream hashOut = new CryptoStream(Stream.Null, hasher, CryptoStreamMode.Write);

			BinaryWriter writer = new BinaryWriter(targetStream);
			writer.Write(fileSize);
			writer.Write((byte)0x7E);

			bytesRead = sourceStream.Read(buffer, 0, alg.BlockSize);
			while(bytesRead > 0)
			{
				streamOut.Write(buffer, 0, bytesRead);
				hashOut.Write(buffer, 0, bytesRead);
				bytesRead = sourceStream.Read(buffer, 0, alg.BlockSize);
			}

			hashOut.Flush();
			hashOut.Close();

			byte[] hash = hasher.Hash;
			streamOut.Write(hash, 0, hash.Length);

			streamOut.Flush();
			streamOut.Close();
		}

		public void Encrypt(string sourceFile, string targetFile)
		{
			FileStream target = null;
			FileStream source = File.OpenRead(sourceFile);
			try
			{
				string path = Path.GetDirectoryName(targetFile);
				if ((path != null) && (path != "") && (!Directory.Exists(path)))
					Directory.CreateDirectory(path);

				target = File.OpenWrite(targetFile);
		
				Encrypt(source, target);
			}
			finally
			{
				source.Close();
				if (target != null)
					target.Close();
			}
		}

		public long Decrypt(Stream sourceStream, Stream targetStream)
		{
			long fileSize = 0;
			long fileRead = 0;

			byte[] salt = new byte[16];
				
			Rijndael alg = Rijndael.Create();
			alg.BlockSize = 128;
			alg.Padding = PaddingMode.PKCS7;
			alg.KeySize = 128;
			alg.Mode = CipherMode.ECB;

			byte[] IV = new byte[16];

			sourceStream.Read(IV, 0, 16);
			sourceStream.Read(salt, 0, 16);
				
			HashAlgorithm hasher = SHA256.Create();
			CryptoStream hashOut = null;
			CryptoStream streamIn = new CryptoStream(sourceStream, alg.CreateDecryptor(_cryptoKey, IV), CryptoStreamMode.Read);
			try
			{
				hashOut = new CryptoStream(Stream.Null, hasher, CryptoStreamMode.Write);

				int bytesRead = 0;
				byte[] buffer = new byte[alg.BlockSize];

				BinaryReader reader = new BinaryReader(sourceStream);
				fileSize = reader.ReadInt64();
				if (reader.ReadByte() != (byte)0x7E)
					throw new Exception("Crypto:Invalid Stream Format character");

				fileRead = fileSize;
				bytesRead = streamIn.Read(buffer, 0, Convert.ToInt32((fileRead < alg.BlockSize)?fileRead:alg.BlockSize));
				while((bytesRead > 0) && (fileRead > 0))
				{
					if (fileRead < alg.BlockSize)
					{
						targetStream.Write(buffer, 0, Convert.ToInt32(fileRead));
						hashOut.Write(buffer, 0, Convert.ToInt32(fileRead));
						fileRead  = 0;
					}
					else
					{
						targetStream.Write(buffer, 0, bytesRead);
						hashOut.Write(buffer, 0, bytesRead);
						fileRead -= bytesRead;
					}
					if (fileRead == 0)
						break;
					bytesRead = streamIn.Read(buffer, 0, Convert.ToInt32((fileRead < alg.BlockSize)?fileRead:alg.BlockSize));
				}

				hashOut.Flush();
				hashOut.Close();
				hashOut = null;

				byte[] hash = hasher.Hash;

				byte[] oldHash = new byte[hasher.HashSize/8];
				streamIn.Read(oldHash, 0, oldHash.Length);

				if (oldHash.Length != hash.Length) 
					throw new Exception("Crypto:Hash length different");

				for(int loop = 0; loop < oldHash.Length; loop++)
					if (oldHash[loop] != hash[loop])
						throw new Exception("Crypto:Hash different");
			}
			finally
			{
				if (hashOut != null)
					hashOut.Close();
				streamIn.Close();
			}

			return fileRead;
		}

		public long Decrypt(string sourceFile, string targetFile)
		{
			long fileSize = 0;
			FileStream target = null;
			FileStream source = File.OpenRead(sourceFile);
			try
			{
				string path = Path.GetDirectoryName(targetFile);
				if ((path != null) && (path != "") && (!Directory.Exists(path)))
					Directory.CreateDirectory(path);

				target = File.OpenWrite(targetFile);

				fileSize = Decrypt(source, target);
			}
			finally
			{
				if (target != null)
				{
					target.Flush();
					target.Close();
				}

				source.Close();
			}
			return fileSize;
		}

		public static byte[] ExtractKeyBytesFromString(string key, int keyLength)
		{
			byte[] ascii = System.Text.ASCIIEncoding.ASCII.GetBytes(key);

			byte[] result = new byte[keyLength];
			for(int loop = 0; loop < keyLength; loop++)
				result[loop] = (byte)0x53;

			for(int loop = 0; loop < ascii.Length; loop++)
				result[loop % keyLength] ^= (byte)ascii[loop];
			return result;
		}
	}
}

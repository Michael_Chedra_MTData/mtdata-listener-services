﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;

namespace MTData.Common.ExtensionMethod
{
    public static class XmlExtension
    {
        public static XmlNode ToXmlNode(this string xml)
        {
            using (StringReader sr = new StringReader(xml))
            {
                XmlTextReader reader = new XmlTextReader(sr);
                reader.WhitespaceHandling = WhitespaceHandling.None;
                XmlDocument doc = new XmlDocument();
                doc.Load(reader);
                reader.Close();
                return doc.DocumentElement;
            }
        }
    }
}

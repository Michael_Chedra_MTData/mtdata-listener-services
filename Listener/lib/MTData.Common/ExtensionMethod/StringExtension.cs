﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MTData.Common.ExtensionMethod
{
    public static class StringExtension
    {
        public static string ToYesNoString(this bool? value)
        {
            return value.HasValue ? ToYesNoString(value.Value) : null;
        }

        public static string ToYesNoString(this bool value)
        {
            return value ? "Yes" : "No";
        }
    }
}

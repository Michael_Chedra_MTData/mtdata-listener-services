﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MTData.Common.ExtensionMethod
{
    public static class SqlExtensions
    {
        public static string GetLogString(this SqlCommand command)
        {
            if (command == null)
                return null;

            try
            {
                StringBuilder sb = new StringBuilder();
                if (command.CommandType == CommandType.StoredProcedure)
                {
                    sb.AppendFormat("exec {0}", command.CommandText);
                    if (command.Parameters.Count > 0)
                    {
                        sb.Append(" ");
                        sb.Append(string.Join(", ", command.Parameters.OfType<SqlParameter>().Select(p => GetStoredProcParamterDisplayString(p))));
                    }

                }
                else if (command.CommandType == CommandType.Text)
                {
                    sb.Append(command.CommandText);
                    if (command.Parameters.Count > 0)
                    {
                        command.Parameters.OfType<SqlParameter>().ToList().ForEach(p => sb.Replace(p.ParameterName, GetParamterValueDisplayString(p)));
                    }
                }
                else
                {
                    sb.Append(command.CommandText);
                }

                return sb.ToString();
            }
            catch 
            {
                StringBuilder sb = new StringBuilder();

                foreach (SqlParameter p in command.Parameters)
                {
                    sb.AppendFormat("{0} = {1}, ", p.ParameterName, p.Value.ToString());
                }

                return sb.ToString();
            }
        }


        private static string GetParamterValueDisplayString(SqlParameter p)
        {
            string format;
            object value = p.Value;

            if (value is DateTime)
            {
                format = "'{0:yyyy-MM-dd HH:mm:ss}'";
            }
            else if (value is string)
            {
                if (value != null)
                {
                    value = value.ToString().Replace("'", "''");
                }

                format = "'{0}'";
            }
            else if (value is DBNull)
            {
                format = "NULL";
            }
            else if (value is bool)
            {
                format = (bool)value ? "1" : "0";
            }
            else if (value is DataTable)
            {
                DataTable dt = (DataTable)value;
                List<string> rows = new List<string>();
                foreach (DataRow row in dt.Rows)
                {
                    IEnumerable<string> fields = row.ItemArray.Select(field => field.ToString());
                    rows.Add("(" + string.Join(",", fields) + ")");
                }
                format = string.Join(",", rows);
            }
            else
            {
                format = "{0}";
            }

            return string.Format(format, value);
        }

        private static string GetStoredProcParamterDisplayString(SqlParameter p)
        {
            string value = GetParamterValueDisplayString(p);

            return string.Format("{0} = {1}", p.ParameterName, value);
        }

        public static string ToSqlString(this int? integer)
        {
            return integer.HasValue ? integer.Value.ToString() : "NULL";
        }
    }


}

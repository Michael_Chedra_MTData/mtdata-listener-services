﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace MTData.Common.ExtensionMethod
{
    public static class DataTableExtension
    {
        /// <summary>
        /// Copies all rows from a source table into a destination table 
        /// </summary>
        /// <param name="destTable"></param>
        /// <param name="sourceTable"></param>
        /// <remarks>
        /// Source and destination tables must have the same schema.
        /// </remarks>
        public static void ImportTable(this DataTable destTable, DataTable sourceTable)
        {
            foreach (DataRow row in sourceTable.Rows)
            {
                destTable.ImportRow(row);
            }
        }
    }
}

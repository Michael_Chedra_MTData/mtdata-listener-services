﻿using System.Collections.Generic;
using System.Text;
using System.Linq;

namespace MTData.Common.ExtensionMethod
{
    public static class IEnumerableExtension
    {
        public static HashSet<T> ToHashSet<T>(this IEnumerable<T> source)
        {
            return new HashSet<T>(source);
        }

        /// <summary>
        /// Returns a string of comma seperated values.
        /// </summary>
        public static string ToCSV<T>(this IEnumerable<T> source, string seperator = ", ")
        {
            return string.Join(seperator, source.Select(x => System.Convert.ToString(x)));
        }
    }
}

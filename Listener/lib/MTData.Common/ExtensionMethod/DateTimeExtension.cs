﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Common.ExtensionMethod
{
    public static class DateTimeExtension
    {
        /// <summary>
        /// Convert a date time to Date Key
        /// </summary>
        /// <param name="dt">DateTime to convert</param>
        /// <returns>Date key</returns>
        public static int ToDateKey(this DateTime dt)
        {
            return (dt.Year * 32768) + (dt.Month * 2048) + (dt.Day * 64) + (dt.Hour * 2) + (dt.Minute >= 30 ? 1 : 0);
        }

        /// <summary>
        /// Determines whether the DateTime is between the specified DateTime range.
        /// </summary>
        /// <param name="dt">The DateTime to compare</param>
        /// <param name="from">From date time</param>
        /// <param name="to">To date time</param>
        /// <returns>true if the DateTime is between the specified DateTime range; otherwise, false.</returns>
        public static bool Between(this DateTime dt, DateTime from, DateTime to)
        {
            if (dt >= from && dt <= to)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        /// <summary>
        /// Rounds a TimeSpan to the nearest minute.
        /// </summary>
        /// <param name="input">The TimeSpan to round</param>
        /// <returns>The rounded TimeSpan</returns>
        /// <remarks>Will round up if exactly half way between minutes (30s)</remarks>
        public static TimeSpan RoundToNearestMinute(this TimeSpan input)
        {
            if (input < TimeSpan.Zero)
            {
                return -RoundToNearestMinute(-input);
            }
            int minutes = (int)input.TotalMinutes;
            if (input.Seconds >= 30)
            {
                minutes++;
            }
            return TimeSpan.FromMinutes(minutes);
        }

        /// <summary>
        /// Formats
        /// </summary>
        public static string ToSQLString(this DateTime datetime)
        {
            return datetime.ToString("MM/dd/yyyy HH:mm:ss.fff");
        }
    }
}

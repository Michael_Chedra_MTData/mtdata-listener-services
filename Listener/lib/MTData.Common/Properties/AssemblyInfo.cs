using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
// General Information about an assembly is controlled through the following 
// set of attributes. Change these attribute values to modify the information
// associated with an assembly.
[assembly: AssemblyTitle("MTData.Common")]
[assembly: AssemblyDescription("MTData Common Library")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mobile Tracking and Data")]
[assembly: AssemblyProduct("MTData.Common")]
[assembly: AssemblyCopyright("Copyright © 2013")]
[assembly: AssemblyTrademark("MTData")]
[assembly: AssemblyCulture("")]
// Setting ComVisible to false makes the types in this assembly not visible 
// to COM components.  If you need to access a type in this assembly from 
// COM, set the ComVisible attribute to true on that type.
[assembly: ComVisible(false)]
// The following GUID is for the ID of the typelib if this project is exposed to COM
[assembly: Guid("6732307c-7720-486b-a658-2a3c25c6415a")]
// Version information for an assembly consists of the following four values:
//
//      Major Version
//      Minor Version 
//      Build Number
//      Revision
//
// You can specify all the values or you can default the Build and Revision Numbers 
// by using the '*' as shown below:
[assembly: AssemblyVersion("8.2.0.0")]
[assembly: AssemblyFileVersion("8.2.0.0")]

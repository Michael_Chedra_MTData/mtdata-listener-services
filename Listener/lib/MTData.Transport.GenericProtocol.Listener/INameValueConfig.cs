﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace MTData.Transport.GenericProtocol.Listener
{
    public interface INameValueConfig
    {
        void ParseDataset(DataSet data);
    }
}

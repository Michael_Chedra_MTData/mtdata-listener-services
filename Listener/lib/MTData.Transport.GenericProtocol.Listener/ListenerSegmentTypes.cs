﻿using System;
using System.Collections.Generic;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// the segemnt types for the listener interface
    /// </summary>
    public enum ListenerSegmentTypes
    {
        TrackingSegment = SegmentTypes.ListenerSegments + 1,
        Gps,
        Mass,
        RequestHardwareConfig,
        HardwareConfig,
        KeepAlive,
        RequestReportOnDemand,
        RequestRules,
        Rules,
        RequiredRules,
        RequestLocationFiles,
        LocationFiles,
        RequestFeature,
        RequestAccidentBuffer,
        RequestExtendedHistory,
        UpdateIAPDataBlocks,
        IAPDataBlocksUpdated,
        HardwareConfigNameValuePair,
        RequestDefaultHardwareConfig,
        RequestDvr,
        RequestDvrConfig,
        DvrConfig,
        RequestCCSConfig,
        CCSConfig,
        DefaultHardwareConfig,
        UpdateHardwareConfig,
        RequestOverSpeedConfig,
        SendOverSpeedConfig,
        UnitDetails,
        AssetPacket,
        RequestWaypoints,
        Waypoints
    }
}

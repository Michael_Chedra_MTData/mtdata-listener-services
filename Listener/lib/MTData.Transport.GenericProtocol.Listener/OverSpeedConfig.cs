using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class OverSpeedConfig : IPacketEncode
    {
        /// <summary>
        /// The version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        #region Constructor

        public OverSpeedConfig()
        {
        }

        public OverSpeedConfig(bool active, float speedLimit, byte tolerance, int flags)
        {
            Active = active;
            SpeedLimit = speedLimit;
            Tolerance = tolerance;
            Flags = flags;
        }

        #endregion

        #region Properties

        public bool Active { get; set; }

        public float SpeedLimit { get; set; }

        public byte Tolerance { get; set; }

        public int Flags { get; set; }

        #endregion

        #region IPacketEncode members

        public void Encode(MemoryStream stream)
        {
            // Write the data to a local stream
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.WriteBool(local, Active);
                BaseLayer.WriteFloat(local, SpeedLimit);
                local.WriteByte(Tolerance);
                BaseLayer.Write4ByteInteger(local, Flags);

                // Write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);

                // Write the length of the data
                byte[] bytes = local.ToArray();
                BaseLayer.WriteMoreFlag(stream, bytes.Length);

                // Now write the data
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            Active = BaseLayer.ReadBool(data, ref index);
            SpeedLimit = BaseLayer.ReadFloat(data, ref index);
            Tolerance = data[index++];
            Flags = BaseLayer.Read4ByteInteger(data, ref index);

            // Skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        #endregion
    }
}
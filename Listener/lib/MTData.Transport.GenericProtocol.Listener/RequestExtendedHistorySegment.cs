﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class RequestExtendedHistorySegment : SegmentLayer
    {
        #region private fields
        private string _uploadId;
        private DateTime _startTime;
        private DateTime _endTime;
        private string _url;
        private int _fileType;
        #endregion

        #region properties
        public string UploadId
        {
            get { return _uploadId; }
            set { _uploadId = value; }
        }
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
        public int FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }
        #endregion

        #region constructor
        public RequestExtendedHistorySegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestExtendedHistory;
        }
        public RequestExtendedHistorySegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestExtendedHistory)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestExtendedHistory, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestExtendedHistory;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fileType);
                WriteString(stream, _uploadId, 0x12);
                WriteDateTime(stream, _startTime);
                WriteDateTime(stream, _endTime);
                WriteString(stream, _url, 0x12);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fileType = Read4ByteInteger(Data, ref index);
            _uploadId = ReadString(Data, ref index, 0x12);
            _startTime = ReadDateTime(Data, ref index);
            _endTime = ReadDateTime(Data, ref index);
            _url = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

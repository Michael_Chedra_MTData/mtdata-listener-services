﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Listener
{
    public enum SerialConnections
    {
        None = 0,
        Unit1035,
        SatelliteInternal,
        SatelliteExternal,
        Duress,
        J1939,
        RefridgerationTTech,
        SwipeCard,
        GpsSimulator,
        Console,
        Thermoking,
        Tramanco,
        IMS,
        BarCodeReader,
        Printer
    }
    public enum UsbConnections
    {
        None = 0,
        Wifi = 0x1001,
        Debug = 0x1002,
    }
    public enum EthernetConnections
    {
        None = 0,
        M7 = 0x10001,
        M7R = 0x10002,
    }
    public enum Features
    {
        None = 0,
        InternalGForce = 0x01,
        DeadReckoning = 0x02,
    }


    /// <summary>
    /// this segment contains the tarcking information sent with all other listener segments from the unit
    /// </summary>
    public class HardwareConfigSegment : SegmentLayer
    {
        #region private fields
        private int _configId;
        private int _configVersion;
        private string _name;
        private SerialConnections _serialA;
        private SerialConnections _serialAShort;
        private SerialConnections _serialALong;
        private SerialConnections _serialB;
        private SerialConnections _serialBShort;
        private SerialConnections _serialBLong;
        private UsbConnections _usb;
        private EthernetConnections _ethernet;
        private Features _feature;
        #endregion

        #region properties
        public int ConfigId
        {
            get { return _configId; }
            set { _configId = value; }
        }
        public int ConfigVersion
        {
            get { return _configVersion; }
            set { _configVersion = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public SerialConnections SerialA
        {
            get { return _serialA; }
            set { _serialA = value; }
        }
        public SerialConnections SerialAShort
        {
            get { return _serialAShort; }
            set { _serialAShort = value; }
        }
        public SerialConnections SerialALong
        {
            get { return _serialALong; }
            set { _serialALong = value; }
        }
        public SerialConnections SerialB
        {
            get { return _serialB; }
            set { _serialB = value; }
        }
        public SerialConnections SerialBShort
        {
            get { return _serialBShort; }
            set { _serialBShort = value; }
        }
        public SerialConnections SerialBLong
        {
            get { return _serialBLong; }
            set { _serialBLong = value; }
        }
        public UsbConnections Usb
        {
            get { return _usb; }
            set { _usb = value; }
        }
        public EthernetConnections Ethernet
        {
            get { return _ethernet; }
            set { _ethernet = value; }
        }
        public Features Feature
        {
            get { return _feature; }
            set { _feature = value; }
        }
        #endregion

        #region constructor
        public HardwareConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.HardwareConfig;
        }
        public HardwareConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.HardwareConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.HardwareConfig, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.HardwareConfig;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _configId);
                Write4ByteInteger(stream, _configVersion);
                WriteString(stream, _name, 0x12);
                Write4ByteInteger(stream, (int)_serialA);
                Write4ByteInteger(stream, (int)_serialAShort);
                Write4ByteInteger(stream, (int)_serialALong);
                Write4ByteInteger(stream, (int)_serialB);
                Write4ByteInteger(stream, (int)_serialBShort);
                Write4ByteInteger(stream, (int)_serialBLong);
                Write4ByteInteger(stream, (int)_usb);
                Write4ByteInteger(stream, (int)_ethernet);
                Write4ByteInteger(stream, (int)_feature);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _configId = Read4ByteInteger(Data, ref index);
            _configVersion = Read4ByteInteger(Data, ref index);
            _name = ReadString(Data, ref index, 0x12);
            _serialA = (SerialConnections)Read4ByteInteger(Data, ref index);
            _serialAShort = (SerialConnections)Read4ByteInteger(Data, ref index);
            _serialALong = (SerialConnections)Read4ByteInteger(Data, ref index);
            _serialB = (SerialConnections)Read4ByteInteger(Data, ref index);
            _serialBShort = (SerialConnections)Read4ByteInteger(Data, ref index);
            _serialBLong = (SerialConnections)Read4ByteInteger(Data, ref index);
            _usb = (UsbConnections)Read4ByteInteger(Data, ref index);
            _ethernet = (EthernetConnections)Read4ByteInteger(Data, ref index);
            _feature = (Features)Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// keep alive segment sent by unit
    /// </summary>
    public class KeepAliveSegment : SegmentLayer
    {
        #region private fields
        private int _fleet;
        private int _vehicle;
        #endregion

        #region properties
        public int Fleet
        {
            get { return _fleet; }
            set { _fleet = value; }
        }
        public int Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
        #endregion

        #region constructor
        public KeepAliveSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.KeepAlive;
        }
        public KeepAliveSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.KeepAlive)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.KeepAlive, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.KeepAlive;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleet);
                Write4ByteInteger(stream, _vehicle);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleet = Read4ByteInteger(Data, ref index);
            _vehicle = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

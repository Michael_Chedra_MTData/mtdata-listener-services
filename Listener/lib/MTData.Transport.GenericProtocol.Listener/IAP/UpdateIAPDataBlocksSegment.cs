﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.IAP
{
    public class UpdateIAPDataBlocksSegment : SegmentLayer
    {
        #region private fields
        private int  _updateId;
        private uint _postionId;
        private uint _speedId;
        private uint _blockId;
        #endregion

        #region properties
        public int UpdateId
        {
            get { return _updateId; }
            set { _updateId = value; }
        }
        public uint PostionId
        {
            get { return _postionId; }
            set { _postionId = value; }
        }
        public uint SpeedId
        {
            get { return _speedId; }
            set { _speedId = value; }
        }
        public uint BlockId
        {
            get { return _blockId; }
            set { _blockId = value; }
        }
        #endregion

        #region constructor
        public UpdateIAPDataBlocksSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.UpdateIAPDataBlocks;
        }
        public UpdateIAPDataBlocksSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.UpdateIAPDataBlocks)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.UpdateIAPDataBlocks, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.UpdateIAPDataBlocks;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _updateId);
                Write4ByteUInteger(stream, _postionId);
                Write4ByteUInteger(stream, _speedId);
                Write4ByteUInteger(stream, _blockId);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _updateId = Read4ByteInteger(Data, ref index);
            _postionId = Read4ByteUInteger(Data, ref index);
            _speedId = Read4ByteUInteger(Data, ref index);
            _blockId = Read4ByteUInteger(Data, ref index);
        }
        #endregion
    }
}

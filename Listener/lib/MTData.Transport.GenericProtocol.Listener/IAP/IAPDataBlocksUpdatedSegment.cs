﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.IAP
{
    public class IAPDataBlocksUpdatedSegment : SegmentLayer
    {
        #region private fields
        private int  _updateId;
        #endregion

        #region properties
        public int UpdateId
        {
            get { return _updateId; }
            set { _updateId = value; }
        }
        #endregion

        #region constructor
        public IAPDataBlocksUpdatedSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.IAPDataBlocksUpdated;
        }
        public IAPDataBlocksUpdatedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.IAPDataBlocksUpdated)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.IAPDataBlocksUpdated, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.IAPDataBlocksUpdated;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _updateId);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _updateId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class RequestCCSConfigSegment : SegmentLayer
    {
        #region private fields
        private int _fleetID;
        private int _vehicleID;
        #endregion

        #region properties
        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public int VehicleID
        {
            get { return _vehicleID; }
            set { _vehicleID = value; }
        }
        #endregion

        #region constructor
        public RequestCCSConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestCCSConfig;
        }
        public RequestCCSConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestCCSConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestCCSConfig, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestCCSConfig;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetID);
                Write4ByteInteger(stream, _vehicleID);

                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetID = Read4ByteInteger(Data, ref index);
            _vehicleID = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

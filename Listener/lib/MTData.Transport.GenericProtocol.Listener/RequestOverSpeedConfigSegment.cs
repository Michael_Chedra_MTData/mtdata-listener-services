﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// RequestOverSpeedConfigSegment
    /// </summary>
    public class RequestOverSpeedConfigSegment : SegmentLayer
    {
        #region Constructor

        public RequestOverSpeedConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestOverSpeedConfig;
        }

        public RequestOverSpeedConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestOverSpeedConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestOverSpeedConfig, segment.Type));
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestOverSpeedConfig;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The id of the vehicle config the unit is running
        /// </summary>
        public int ConfigId { get; set; }

        /// <summary>
        /// The major version of the config
        /// </summary>
        public int ConfigVersionMajor { get; set; }

        /// <summary>
        /// The minor version of the config
        /// </summary>
        public int ConfigVersionMinor { get; set; }

        /// <summary>
        /// The id of the waypoint group the unit is running
        /// </summary>
        public int WaypointGroupId { get; set; }

        /// <summary>
        /// The major version of the waypoint group
        /// </summary>
        public int WaypointGroupVersionMajor { get; set; }

        /// <summary>
        /// The minor version of the waypoint group
        /// </summary>
        public int WaypointGroupVersionMinor { get; set; }

        #endregion

        #region Override Methods

        public override byte[] GetBytes()
        {
            // Set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, ConfigId);
                Write4ByteInteger(stream, ConfigVersionMajor);
                Write4ByteInteger(stream, ConfigVersionMinor);
                Write4ByteInteger(stream, WaypointGroupId);
                Write4ByteInteger(stream, WaypointGroupVersionMajor);
                Write4ByteInteger(stream, WaypointGroupVersionMinor);
                Data = stream.ToArray();
                stream.Close();
            }

            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region Private Methods

        private void DecodeData()
        {
            int index = 0;

            ConfigId = Read4ByteInteger(Data, ref index);
            ConfigVersionMajor = Read4ByteInteger(Data, ref index);
            ConfigVersionMinor = Read4ByteInteger(Data, ref index);
            WaypointGroupId = Read4ByteInteger(Data, ref index);
            WaypointGroupVersionMajor = Read4ByteInteger(Data, ref index);
            WaypointGroupVersionMinor = Read4ByteInteger(Data, ref index);
        }

        #endregion
    }
}
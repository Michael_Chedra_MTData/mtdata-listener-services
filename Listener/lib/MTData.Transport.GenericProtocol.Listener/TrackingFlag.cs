﻿using System;
using System.Collections.Generic;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// Tracking Flag types
    /// </summary>
    public enum TrackingFlagTypes
    {
        StatusFlags,
        InputFlags,
        OutputFlags,
        MdtStates,
        ConcreteStates,
        WayPointStates,
        RouteStates,
        PacketState,
        Refrigeration
    }

    public enum StatusFlags
    {
        LoggedIn = 0x01,
        OnBreak = 0x02
    }
    public enum MdtStates
    {
        MdtState1 = 0x01,
        MdtState2 = 0x02,
        MdtState3 = 0x04,
        MdtState4 = 0x08,
        MdtState5 = 0x10,
        MdtState6 = 0x20,
        MdtState7 = 0x40,
        MdtState8 = 0x80
    }

    /// <summary>
    /// A tracking flag
    /// </summary>
    public class TrackingFlag : IPacketEncode
    {
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private TrackingFlagTypes _flagType;
        private uint _value;

        public TrackingFlag() { }
        public TrackingFlag(TrackingFlagTypes flagType, uint value)
        {
            _flagType = flagType;
            _value = value;
        }

        public TrackingFlagTypes TrackingFlagType { get { return _flagType; } set { _flagType = value; } }
        public uint Value { get { return _value; } set { _value = value; } }

        #region IPacketEncode members

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            System.IO.MemoryStream local = new System.IO.MemoryStream();
            local.WriteByte((byte)(_flagType));
            BaseLayer.Write4ByteUInteger(local, _value);

            //write version number
            BaseLayer.WriteMoreFlag(stream, _versionNumber);
            //write the length
            byte[] bytes = local.ToArray();
            BaseLayer.WriteMoreFlag(stream, bytes.Length);
            //now write the data
            stream.Write(bytes, 0, bytes.Length);
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _flagType = (TrackingFlagTypes)data[index++];
            _value = BaseLayer.Read4ByteUInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        #endregion
    }

}

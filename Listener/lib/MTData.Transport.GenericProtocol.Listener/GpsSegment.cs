﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// TRacking GPS segemnt
    /// </summary>
    public class GpsSegment : SegmentLayer
    {
        #region private fields
        private DateTime _gpsTime;
        private int _deviceTimer;
        private double _latitude;
        private double _longitude;
        private float _speed;
        private float _maxSpeed;
        private int _heading;
        private int _altitude;
        private float _hdop;
        private float _vdop;
        #endregion

        #region properties
        public DateTime GpsTime
        {
            get { return _gpsTime; }
            set { _gpsTime = value; }
        }
        public int DeviceTimer
        {
            get { return _deviceTimer; }
            set { _deviceTimer = value; }
        }
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public float Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        public float MaxSpeed
        {
            get { return _maxSpeed; }
            set { _maxSpeed = value; }
        }
        public int Heading
        {
            get { return _heading; }
            set { _heading = value; }
        }
        public int Altitude
        {
            get { return _altitude; }
            set { _altitude = value; }
        }
        public float Hdop
        {
            get { return _hdop; }
            set { _hdop = value; }
        }
        public float Vdop
        {
            get { return _vdop; }
            set { _vdop = value; }
        }
        #endregion

        #region constructor
        public GpsSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.Gps;
        }
        public GpsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.Gps)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.Gps, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.Gps;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                WriteDateTime(stream, _gpsTime);
                Write2ByteInteger(stream, _deviceTimer);
                WriteCoordinate(stream, _latitude);
                WriteCoordinate(stream, _longitude);
                WriteFloat(stream, _speed);
                WriteFloat(stream, _maxSpeed);
                Write2ByteInteger(stream, _heading);
                Write2ByteInteger(stream, _altitude);
                WriteFloat(stream, _hdop);
                WriteFloat(stream, _vdop);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _gpsTime = ReadDateTime(Data, ref index);
            _deviceTimer = Read2ByteInteger(Data, ref index);
            _latitude = ReadCoordinate(Data, ref index);
            _longitude = ReadCoordinate(Data, ref index);
            _speed = ReadFloat(Data, ref index);
            _maxSpeed = ReadFloat(Data, ref index);
            _heading = Read2ByteInteger(Data, ref index);
            _altitude = Read2ByteInteger(Data, ref index);
            _hdop = ReadFloat(Data, ref index);
            _vdop = ReadFloat(Data, ref index);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.InternalRequests
{
    public enum FeatureType
    {
        ReportOnDemand,
        AccidentBuffer,
        ExtendedHistory,
        DvrRequest,
        DvrConfig
    }

    /// <summary>
    /// request feature segment sent by transport web serive 
    /// </summary>
    public class RequestFeatureSegment : SegmentLayer
    {
        #region private fields
        private int _fleet;
        private int _vehicle;
        private FeatureType _feature;
        private int _featureId;
        #endregion

        #region properties
        public int Fleet
        {
            get { return _fleet; }
            set { _fleet = value; }
        }
        public int Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
        public FeatureType Feature
        {
            get { return _feature; }
            set { _feature = value; }
        }

        public int FeatureId
        {
            get { return _featureId; }
            set { _featureId = value; }
        }
        #endregion

        #region constructor
        public RequestFeatureSegment()
        {
            Version = 2;
            Type = (int)ListenerSegmentTypes.RequestFeature;
        }
        public RequestFeatureSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestFeature)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestFeature, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestFeature;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleet);
                Write4ByteInteger(stream, _vehicle);
                stream.WriteByte((byte)_feature);

                if (Version == 2)
                {
                    Write4ByteInteger(stream, _featureId);
                }

                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleet = Read4ByteInteger(Data, ref index);
            _vehicle = Read4ByteInteger(Data, ref index);
            _feature = (FeatureType)Data[index++];

            if (Version == 2)
            {
                _featureId = Read4ByteInteger(Data, ref index);
            }
        }
        #endregion
    }
}

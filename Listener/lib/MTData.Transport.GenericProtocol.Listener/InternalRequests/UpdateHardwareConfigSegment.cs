﻿namespace MTData.Transport.GenericProtocol.Listener.InternalRequests
{
    using System;
    using System.Collections.Generic;
    using System.IO;

    /// <summary>
    /// The update hardware config segment class.
    /// </summary>
    public class UpdateHardwareConfigSegment : SegmentLayer
    {
        #region Constructor
        /// <summary>
        /// Initialises a new instance of the <see cref="UpdateHardwareConfigSegment"/> class.
        /// </summary>
        public UpdateHardwareConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.UpdateHardwareConfig;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="UpdateHardwareConfigSegment"/> class.
        /// </summary>
        /// <param name="segment">The segment layer.</param>
        /// <exception cref="Exception">If segment type is not of <see cref="ListenerSegmentTypes.UpdateHardwareConfig"/></exception>
        public UpdateHardwareConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.UpdateHardwareConfig)
            {
                throw new Exception($"Can only create a segment of type {ListenerSegmentTypes.UpdateHardwareConfig}, not {segment.Type}");
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.UpdateHardwareConfig;
            Data = segment.Data;
            DecodeData();
        }
        #endregion Constructor

        #region Properties
        /// <summary>
        /// Gets or sets the fleet id.
        /// </summary>
        public int FleetId { get; set; }

        /// <summary>
        /// Gets or sets the list of vehicles identifiers.
        /// </summary>
        public List<int> VehicleIds { get; set; }
        #endregion Properties

        #region Override Methods
        /// <summary>
        /// Get the segment encoded bytes.
        /// </summary>
        /// <returns>The encode bytes.</returns>
        public override byte[] GetBytes()
        {
            // set the data
            using (var stream = new MemoryStream())
            {
                Write4ByteInteger(stream, FleetId);
                Write4ByteInteger(stream, VehicleIds.Count);
                foreach (var vehicleId in VehicleIds)
                {
                    Write4ByteInteger(stream, vehicleId);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        /// <summary>
        /// Decode segment payload.
        /// </summary>
        /// <param name="payload">The segment payload.</param>
        /// <returns>The index of decoded data.</returns>
        public override int Decode(byte[] payload)
        {
            var read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion Override Methods

        #region Private Methods
        /// <summary>
        /// Decode segment.
        /// </summary>
        private void DecodeData()
        {
            var index = 0;
            FleetId = Read4ByteInteger(Data, ref index);
            VehicleIds = new List<int>();
            var count = Read4ByteInteger(Data, ref index);
            for (var i = 0; i < count; i++)
            {
                VehicleIds.Add(Read4ByteInteger(Data, ref index));
            }
        }
        #endregion Private Methods
    }
}

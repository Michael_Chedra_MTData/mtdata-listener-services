﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class HardwareConfigNameValueSegment : SegmentLayer, INameValueConfig
    {
        private int _configId;
        private int _configVersion;
        private string _name;
        private NameValueConfigBuilder builder = new NameValueConfigBuilder();

        public int ConfigId
        {
            get { return this._configId; }
            set { this._configId = value; }
        }

        public int ConfigVersion
        {
            get { return this._configVersion; }
            set { this._configVersion = value; }
        }

        public string Name
        {
            get { return this._name; }
            set { this._name = value; }
        }

        public NameValueConfigBuilder Builder
        {
            get { return this.builder; }
        }

        public HardwareConfigNameValueSegment()
        {
            base.Version = 1;
            base.Type = (int)ListenerSegmentTypes.HardwareConfigNameValuePair;
        }

        public HardwareConfigNameValueSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.HardwareConfigNameValuePair)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.HardwareConfigNameValuePair, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.HardwareConfigNameValuePair;
            Data = segment.Data;
            DecodeData();
        }

        void INameValueConfig.ParseDataset(System.Data.DataSet data)
        {
            this._configId = Convert.ToInt32(data.Tables[0].Rows[0]["ID"]);
            this._configVersion = Convert.ToInt32(data.Tables[0].Rows[0]["VersionNumber"]);
            this._name = Convert.ToString(data.Tables[0].Rows[0]["Name"]);
            this.builder.BuildFromDataRows(data.Tables[0].Rows);
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;
            this._configId = SegmentLayer.Read4ByteInteger(Data, ref index);
            this._configVersion = SegmentLayer.Read4ByteInteger(Data, ref index);
            this._name = SegmentLayer.ReadString(Data, ref index, 0x12);

            this.builder.DecodeData(Data, ref index);
        }

        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _configId);
                Write4ByteInteger(stream, _configVersion);
                WriteString(stream, _name, 0x12);
                this.builder.GetBytes(stream);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }
    }
}

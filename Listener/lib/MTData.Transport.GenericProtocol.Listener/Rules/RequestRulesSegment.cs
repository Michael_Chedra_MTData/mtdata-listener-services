﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Listener.Rules
{
    public class RequestRulesSegment : SegmentLayer
    {
        #region private fields
        private int _fleet;
        private int _vehicle;
        private List<ListId> _ruleIds;
        #endregion

        #region properties
        public int Fleet
        {
            get { return _fleet; }
            set { _fleet = value; }
        }
        public int Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
        public List<ListId> RuleIds
        {
            get { return _ruleIds; }
            set { _ruleIds = value; }
        }
        #endregion

        #region constructor
        public RequestRulesSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestRules;
            _ruleIds = new List<ListId>();
        }
        public RequestRulesSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestRules)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestRules, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestRules;
            _ruleIds = new List<ListId>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleet);
                Write4ByteInteger(stream, _vehicle);
                Write2ByteInteger(stream, _ruleIds.Count);
                foreach (ListId id in _ruleIds)
                {
                    id.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleet = Read4ByteInteger(Data, ref index);
            _vehicle = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            for (int i = 0; i < count; i++)
            {
                ListId id = new ListId();
                id.Decode(Data, ref index);
                _ruleIds.Add(id);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Listener.Rules
{
    public class LocationFilesSegment : SegmentLayer
    {
        #region private fields
        private List<LocationFile> _locationFiles;
        #endregion

        #region properties
        public List<LocationFile> LocationFiles
        {
            get { return _locationFiles; }
            set { _locationFiles = value; }
        }
        #endregion

        #region constructor
        public LocationFilesSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.LocationFiles;
            _locationFiles = new List<LocationFile>();
        }
        public LocationFilesSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.LocationFiles)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.LocationFiles, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.LocationFiles;
            _locationFiles = new List<LocationFile>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write2ByteInteger(stream, _locationFiles.Count);
                foreach (LocationFile file in _locationFiles)
                {
                    file.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            int count = Read2ByteInteger(Data, ref index);
            _locationFiles.Clear();
            for (int i = 0; i < count; i++)
            {
                LocationFile fiel = new LocationFile();
                fiel.Decode(Data, ref index);
                _locationFiles.Add(fiel);
            }
        }
        #endregion
    }
}

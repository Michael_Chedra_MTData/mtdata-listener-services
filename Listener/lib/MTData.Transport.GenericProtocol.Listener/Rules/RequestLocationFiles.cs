﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Listener.Rules
{
    public class RequestLocationFiles : SegmentLayer
    {
        #region private fields
        private int _fleet;
        private int _vehicle;
        private List<ListId> _locationIds;
        private List<bool> _latestVersions;
        #endregion

        #region properties
        public int Fleet
        {
            get { return _fleet; }
            set { _fleet = value; }
        }
        public int Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
        public List<ListId> LocationIds
        {
            get { return _locationIds; }
            set { _locationIds = value; }
        }
        public List<bool> LatestVersions
        {
            get { return _latestVersions; }
            set { _latestVersions = value; }
        }
        #endregion

        #region constructor
        public RequestLocationFiles()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestLocationFiles;
            _locationIds = new List<ListId>();
            _latestVersions = new List<bool>();
        }
        public RequestLocationFiles(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestLocationFiles)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestLocationFiles, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestLocationFiles;
            _locationIds = new List<ListId>();
            _latestVersions = new List<bool>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleet);
                Write4ByteInteger(stream, _vehicle);
                Write2ByteInteger(stream, _locationIds.Count);
                for (int i = 0; i < _locationIds.Count; i++ )
                {
                    _locationIds[i].Encode(stream);
                    if (i < _latestVersions.Count)
                    {
                        WriteBool(stream, _latestVersions[i]);
                    }
                    else
                    {
                        WriteBool(stream, false);
                    }

                }
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleet = Read4ByteInteger(Data, ref index);
            _vehicle = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _locationIds.Clear();
            _latestVersions.Clear();
            for (int i = 0; i < count; i++)
            {
                ListId id = new ListId();
                id.Decode(Data, ref index);
                _locationIds.Add(id);
                _latestVersions.Add(ReadBool(Data, ref index));
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.Rules
{
    public class Rule : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1; 

        private int _ruleId;
        private string _condition;
        private List<Action> _actions;
        #endregion

        #region constructor
        public Rule()
        {
            _actions = new List<Action>();
        }
        #endregion

        #region properties
        public int RuleId { get { return _ruleId; } set { _ruleId = value; } }
        public string Condition { get { return _condition; } set { _condition = value; } }
        public List<Action> Actions { get { return _actions; } set { _actions = value; } }
        #endregion

        #region IPacketEncode members
        /// <summary>
        /// encode the list into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _ruleId);
                BaseLayer.WriteString(local, _condition, 0x12);
                BaseLayer.Write2ByteInteger(local, _actions.Count);
                foreach (Action a in _actions)
                {
                    a.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            } 
        }

        /// <summary>
        /// decode the list from a byte[]
        /// </summary>
        /// <param name="Data">byte[] containing the list parameters to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _ruleId = BaseLayer.Read4ByteInteger(data, ref index);
            _condition = BaseLayer.ReadString(data, ref index, 0x12);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _actions = new List<Action>();
            for (int i = 0; i < count; i++)
            {
                Action a = new Action();
                a.Decode(data, ref index);
                _actions.Add(a);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

        }
        #endregion

    }
}

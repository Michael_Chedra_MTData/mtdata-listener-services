﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.Rules
{

    public class Action : IPacketEncode
    {
        public static int ActionFlagInverted = 1;
    
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1; 

        private string _arguments;
        #endregion

        #region constructor
        public Action() { }
        #endregion

        #region properties
        public string Arguments { get { return _arguments; } set { _arguments = value; } }
        #endregion

        #region IPacketEncode members
        /// <summary>
        /// encode the list into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, _arguments, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            } 
        }

        /// <summary>
        /// decode the list from a byte[]
        /// </summary>
        /// <param name="Data">byte[] containing the list parameters to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _arguments = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.Rules
{
    public class RulesSeqment : SegmentLayer
    {
        #region private fields
        private int _ruleSetId;
        private int _ruleSetVersion;
        private List<Rule> _rules;
        #endregion

        #region properties
        public int RuleSetId
        {
            get { return _ruleSetId; }
            set { _ruleSetId = value; }
        }
        public int RuleSetVersion
        {
            get { return _ruleSetVersion; }
            set { _ruleSetVersion = value; }
        }
        public List<Rule> Rules { get { return _rules; } set { _rules = value; } }
        #endregion

        #region constructor
        public RulesSeqment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.Rules;
            _rules = new List<Rule>();
        }
        public RulesSeqment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.Rules)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.Rules, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.Rules;
            _rules = new List<Rule>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _ruleSetId);
                Write4ByteInteger(stream, _ruleSetVersion);
                BaseLayer.Write2ByteInteger(stream, _rules.Count);
                foreach (Rule r in _rules)
                {
                    r.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _ruleSetId = Read4ByteInteger(Data, ref index);
            _ruleSetVersion = Read4ByteInteger(Data, ref index);
            int count = BaseLayer.Read2ByteInteger(Data, ref index);
            _rules = new List<Rule>();
            for (int i = 0; i < count; i++)
            {
                Rule r = new Rule();
                r.Decode(Data, ref index);
                _rules.Add(r);
            }
        }
        #endregion
    }
}

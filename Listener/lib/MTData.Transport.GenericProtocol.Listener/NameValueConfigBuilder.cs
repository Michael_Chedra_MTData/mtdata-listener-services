﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Data;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class NameValueConfigBuilder
    {
        private List<NVGroupType> _groupList = new List<NVGroupType>();

        public int ValueCount
        {
            get { return this._groupList.Count; }
        }

        public List<NVGroupType> Values
        {
            get { return this._groupList; }
        }

        public void BuildFromDataRows(DataRowCollection data)
        {
            foreach (DataRow row in data)
            {
                NVGroupType tmpType = null;

                tmpType = this._groupList.Find(item => item.GroupID == Convert.ToInt32(row["HWConfigValueGroupTypeID"]));

                if (tmpType == null)
                {
                    tmpType = new NVGroupType(Convert.ToInt32(row["HWConfigValueGroupTypeID"]));

                    this._groupList.Add(tmpType);
                }

                tmpType.Attributes.Add(new NVAttribute(row));
            }
        }

        public void DecodeData(byte[] Data, ref int index)
        {
            int valueCount = SegmentLayer.Read4ByteInteger(Data, ref index);

            for (int i = 0; i < valueCount; i++)
            {
                NVGroupType tmpGroup = new NVGroupType(SegmentLayer.Read4ByteInteger(Data, ref index));

                tmpGroup.DecodeData(Data, ref index);

                this._groupList.Add(tmpGroup);
            }
        }

        public void GetBytes(MemoryStream stream)
        {
            SegmentLayer.Write4ByteInteger(stream, this.ValueCount);

            foreach (NVGroupType nvg in this._groupList)
            {
                SegmentLayer.Write4ByteInteger(stream, nvg.GroupID);
                SegmentLayer.Write2ByteInteger(stream, nvg.Attributes.Count);

                foreach (NVAttribute att in nvg.Attributes)
                {
                    att.GetBytes(stream);
                }
            }
        }
    }

    public class NVGroupType
    {
        private int _groupID;
        private List<NVAttribute> _attributeList = new List<NVAttribute>();

        public NVGroupType(int groupID)
        {
            this._groupID = groupID;
        }

        public int GroupID
        {
            get { return this._groupID; }
        }

        public List<NVAttribute> Attributes
        {
            get { return this._attributeList; }
        }

        public void DecodeData(byte[] Data, ref int index)
        {
            int attributeCount = SegmentLayer.Read2ByteInteger(Data, ref index);

            for (int i = 0; i < attributeCount; i++)
            {
                NVAttribute tmpAttribute = new NVAttribute();

                tmpAttribute.DecodeData(Data, ref index);

                this._attributeList.Add(tmpAttribute);
            }
        }
    }

    public class NVAttribute
    {
        private int _attributeTypeID;
        private int _attributeDataTypeID;
        private string _attributeValue;

        public int AttributeTypeID
        {
            get { return this._attributeTypeID; }
            set { this._attributeTypeID = value; }
        }

        /// <summary>
        /// Id's 1, 2, 3, 11, 12 currently not implemented
        /// </summary>
        public int AttributeDataTypeID
        {
            get { return this._attributeDataTypeID; }
            set
            {
                if(value ==1 || value ==2 || value ==3 || value ==11 || value ==12)
                {
                    throw new NotImplementedException();
                }
                else
                {
                    this._attributeDataTypeID = value; 
                }
            }
        }

        public string AttributeValue
        {
            get { return this._attributeValue; }
            set { this._attributeValue = value; }
        }

        public void DecodeData(byte[] Data, ref int index)
        {
            this._attributeTypeID = SegmentLayer.Read4ByteInteger(Data, ref index);
            this._attributeDataTypeID = SegmentLayer.Read4ByteInteger(Data, ref index);

            switch (this._attributeDataTypeID)
            {
                case 1: // Bit
                    //break;
                case 2: // Nibble
                    //break;
                case 3: // Char
                    this._attributeValue = Data[index].ToString();
                    index++;
                    break;
                case 4: // String
                    this._attributeValue = SegmentLayer.ReadString(Data, ref index, 0x12);
                    break;
                case 5: // Short
                //Write2ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 6: // UShort
                    this._attributeValue = SegmentLayer.Read2ByteInteger(Data, ref index).ToString();
                    break;
                case 7: // Int
                //Write4ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 8: // UInt
                //Write4ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 9: // Long
                //Write4ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 10: // ULong
                    this._attributeValue = SegmentLayer.Read4ByteInteger(Data, ref index).ToString();
                    break;
                case 11: // LongLong
                    throw new NotImplementedException();
                    //Write8ByteInteger(stream, int.Parse(att.AttributeValue));
                    break;
                case 12: // ULongLong
                    throw new NotImplementedException();
                    //Write8ByteInteger(stream, int.Parse(att.AttributeValue));
                    break;
                default:
                    break;
            }
        }

        public void GetBytes(MemoryStream stream)
        {
            SegmentLayer.Write4ByteInteger(stream, this._attributeTypeID);
            SegmentLayer.Write4ByteInteger(stream, this._attributeDataTypeID);
            /*
             TypeID Name        Alt Name    Byte    Bit
             1	    Bit	        Bit	        0	    1
             2	    Nibble	    Nibble	    0	    4
             3	    Char	    Char	    1	    0
             4	    String	    String	    9999	0       If sending string after type must be string length in bytes
             5	    Short	    Int16	    2	    0
             6	    UShort	    UInt16	    2	    0
             7	    Int	        Int32	    4	    0
             8	    UInt	    UInt32	    4	    0
             9	    Long	    Int32	    4	    0
             10	    ULong	    UInt32	    4	    0
             11	    LongLong	Int64	    8	    0
             12	    ULongLong	UInt64	    8	    0
            */
            switch (this._attributeDataTypeID)
            {
                case 1: // Bit
                    //break;
                case 2: // Nibble
                    //break;
                case 3: // Char
                    byte byteToUse;

                  byte.TryParse(this._attributeValue, out byteToUse);

                    stream.WriteByte(byteToUse);
                    break;
                case 4: // String
                    SegmentLayer.WriteString(stream, this._attributeValue, 0x12);
                    break;
                case 5: // Short
                //Write2ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 6: // UShort
                    SegmentLayer.Write2ByteInteger(stream, int.Parse(this._attributeValue));
                    break;
                case 7: // Int
                //Write4ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 8: // UInt
                //Write4ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 9: // Long
                //Write4ByteInteger(stream, int.Parse(att.AttributeValue));
                //break;
                case 10: // ULong
                    SegmentLayer.Write4ByteInteger(stream, int.Parse(this._attributeValue));
                    break;
                case 11: // LongLong
                    throw new NotImplementedException();
                    //Write8ByteInteger(stream, int.Parse(att.AttributeValue));
                    break;
                case 12: // ULongLong
                    throw new NotImplementedException();
                    //Write8ByteInteger(stream, int.Parse(att.AttributeValue));
                    break;
                default:
                    break;
            }
        }

        public NVAttribute()
        {
        }

        public NVAttribute(DataRow row)
        {
            this._attributeTypeID = Convert.ToInt32(row["VehicleHWConfigAttributeTypeID"]);
            this._attributeDataTypeID = Convert.ToInt32(row["VehicleHWConfigAttributeDataTypeID"]);
            this._attributeValue = Convert.ToString(row["Value"]);
        }
    }
}

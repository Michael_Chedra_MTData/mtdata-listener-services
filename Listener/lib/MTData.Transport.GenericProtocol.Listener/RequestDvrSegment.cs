﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class RequestDvrSegment : SegmentLayer
    {
        #region private fields

        private string _uploadId;
        private string _url;

        #endregion

        #region properties

        public string UploadId
        {
            get { return _uploadId; }
            set { _uploadId = value; }
        }

        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }

        public int RequestType { get; set; }
        public int ContentType { get; set; }
        public int ResponseChannel { get; set; }
        public int? AlarmId { get; set; }
        public DateTime? DateStart { get; set; }
        public DateTime? DateEnd { get; set; }
        public int Duration { get; set; }

        #endregion

        #region constructor

        public RequestDvrSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestDvr;
        }
        public RequestDvrSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestDvr)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestDvr, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestDvr;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region override methods

        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                WriteString(stream, _uploadId, 0x12);

                Write4ByteInteger(stream, RequestType);
                Write4ByteInteger(stream, ContentType);
                Write4ByteInteger(stream, ResponseChannel);

                Write4ByteInteger(stream, AlarmId ?? 0);
                WriteDateTime(stream, DateStart ?? DateTime.MinValue);
                WriteDateTime(stream, DateEnd ?? DateTime.MaxValue);
                Write4ByteInteger(stream, Duration);

                WriteString(stream, _url, 0x12);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region private methods

        private void DecodeData()
        {
            int index = 0;
            _uploadId = ReadString(Data, ref index, 0x12);

            RequestType = Read4ByteInteger(Data, ref index);
            ContentType = Read4ByteInteger(Data, ref index);
            ResponseChannel = Read4ByteInteger(Data, ref index);

            AlarmId = Read4ByteInteger(Data, ref index);
            DateStart = ReadDateTime(Data, ref index);
            DateEnd = ReadDateTime(Data, ref index);
            Duration = Read4ByteInteger(Data, ref index);

            _url = ReadString(Data, ref index, 0x12);
        }

        #endregion
    }
}

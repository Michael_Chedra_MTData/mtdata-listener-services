﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// RequestHardwareConfigSegment
    /// </summary>
    public class RequestHardwareConfigSegment : SegmentLayer
    {
        #region properties

        public int Fleet { get; set; }
        public int Vehicle { get; set; }
        public int ConfigId { get; set; }
        public int ConfigVersion { get; set; }

        #endregion

        #region constructor

        public RequestHardwareConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestHardwareConfig;
        }

        public RequestHardwareConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestHardwareConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestHardwareConfig, segment.Type));
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestHardwareConfig;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region override methods

        public override byte[] GetBytes()
        {
            // Set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, Fleet);
                Write4ByteInteger(stream, Vehicle);
                Write4ByteInteger(stream, ConfigId);
                Write4ByteInteger(stream, ConfigVersion);
                Data = stream.ToArray();
                stream.Close();
            }

            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region private methods

        private void DecodeData()
        {
            int index = 0;
            Fleet = Read4ByteInteger(Data, ref index);
            Vehicle = Read4ByteInteger(Data, ref index);
            ConfigId = Read4ByteInteger(Data, ref index);
            ConfigVersion = Read4ByteInteger(Data, ref index);
        }

        #endregion
    }
}
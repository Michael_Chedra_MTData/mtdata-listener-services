﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class RequestReportOnDemandSegment : SegmentLayer
    {
        #region private fields
        #endregion

        #region properties
        #endregion

        #region constructor
        public RequestReportOnDemandSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestReportOnDemand;
        }
        public RequestReportOnDemandSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestReportOnDemand)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestReportOnDemand, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestReportOnDemand;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class RequestAccidentBufferSegment : SegmentLayer
    {
        #region private fields
        private string _uploadId;
        private DateTime _accidentTime;
        private string _url;
        private int _fileType;
        #endregion

        #region properties
        public string UploadId
        {
            get { return _uploadId; }
            set { _uploadId = value; }
        }
        public DateTime AccidentTime
        {
            get { return _accidentTime; }
            set { _accidentTime = value; }
        }
        public string Url
        {
            get { return _url; }
            set { _url = value; }
        }
        public int FileType
        {
            get { return _fileType; }
            set { _fileType = value; }
        }
        #endregion

        #region constructor
        public RequestAccidentBufferSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestAccidentBuffer;
        }
        public RequestAccidentBufferSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestAccidentBuffer)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestAccidentBuffer, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestAccidentBuffer;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fileType);
                WriteString(stream, _uploadId, 0x12);
                WriteDateTime(stream, _accidentTime);
                WriteString(stream, _url, 0x12);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fileType = Read4ByteInteger(Data, ref index);
            _uploadId = ReadString(Data, ref index, 0x12);
            _accidentTime = ReadDateTime(Data, ref index);
            _url = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

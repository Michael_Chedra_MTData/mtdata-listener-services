﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class CCSConfigSegment : SegmentLayer
    {
        #region private fields
        private int _fleetID;
        private int _vehicleID;

        private int _FCTimeBeforeEventInSec;
        private int _FCTimeAfterEventInSec;
        private int _FCSampleRateInSec;
        private int _HBSampleRateInSec;
        private int _CustomerRef;
        private string _FileUploadUrl;
        #endregion

        #region properties
        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public int VehicleID
        {
            get { return _vehicleID; }
            set { _vehicleID = value; }
        }

        public int FCTimeBeforeEventInSec
        {
            get { return _FCTimeBeforeEventInSec; }
            set { _FCTimeBeforeEventInSec = value; }
        }

        public int FCTimeAfterEventInSec
        {
            get { return _FCTimeAfterEventInSec; }
            set { _FCTimeAfterEventInSec = value; }
        }

        public int FCSampleRateInSec
        {
            get { return _FCSampleRateInSec; }
            set { _FCSampleRateInSec = value; }
        }

        public int HBSampleRateInSec
        {
            get { return _HBSampleRateInSec; }
            set { _HBSampleRateInSec = value; }
        }

        public int CustomerRef
        {
            get { return _CustomerRef; }
            set { _CustomerRef = value; }
        }

        public string FileUploadUrl
        {
            get { return _FileUploadUrl; }
            set { _FileUploadUrl = value; }
        }
        #endregion

        #region constructor
        public CCSConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.CCSConfig;
        }
        public CCSConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.CCSConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.CCSConfig, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.CCSConfig;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetID);
                Write4ByteInteger(stream, _vehicleID);
                Write4ByteInteger(stream, _FCTimeBeforeEventInSec);
                Write4ByteInteger(stream, _FCTimeAfterEventInSec);
                Write4ByteInteger(stream, _FCSampleRateInSec);
                Write4ByteInteger(stream, _HBSampleRateInSec);
                Write4ByteInteger(stream, _CustomerRef);
                WriteString(stream, _FileUploadUrl, 0x12);

                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetID = Read4ByteInteger(Data, ref index);
            _vehicleID = Read4ByteInteger(Data, ref index);
            _FCTimeBeforeEventInSec = Read4ByteInteger(Data, ref index);
            _FCTimeAfterEventInSec = Read4ByteInteger(Data, ref index);
            _FCSampleRateInSec = Read4ByteInteger(Data, ref index);
            _HBSampleRateInSec = Read4ByteInteger(Data, ref index);
            _CustomerRef = Read4ByteInteger(Data, ref index);
            _FileUploadUrl = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// RequestDvrConfigSegment
    /// </summary>
    public class RequestDvrConfigSegment : SegmentLayer
    {
        #region properties

        public int Fleet { get; set; }

        public int Vehicle { get; set; }

        public int ConfigNo { get; set; }

        public int ConfigMajorVersion { get; set; }

        public int ConfigMinorVersion { get; set; }

        #endregion

        #region constructor

        public RequestDvrConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestDvrConfig;
        }

        public RequestDvrConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestDvrConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestDvrConfig, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestDvrConfig;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region override methods

        public override byte[] GetBytes()
        {
            // Set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, Fleet);
                Write4ByteInteger(stream, Vehicle);
                Write4ByteInteger(stream, ConfigNo);
                Write4ByteInteger(stream, ConfigMajorVersion);
                Write4ByteInteger(stream, ConfigMinorVersion);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region private methods

        private void DecodeData()
        {
            int index = 0;
            Fleet = Read4ByteInteger(Data, ref index);
            Vehicle = Read4ByteInteger(Data, ref index);
            ConfigNo = Read4ByteInteger(Data, ref index);
            ConfigMajorVersion = Read4ByteInteger(Data, ref index);
            ConfigMinorVersion = Read4ByteInteger(Data, ref index);
        }

        #endregion
    }
}
﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    public class SendOverSpeedConfigSegment : SegmentLayer
    {
        #region Constructor

        public SendOverSpeedConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.SendOverSpeedConfig;

            GazettedZone1 = new OverSpeedConfig();
            GazettedZone2 = new OverSpeedConfig();
            GazettedZone3 = new OverSpeedConfig();
            GazettedZone4 = new OverSpeedConfig();
            WaypointZone1 = new OverSpeedConfig();
            WaypointZone2 = new OverSpeedConfig();
            WaypointZone3 = new OverSpeedConfig();
            WaypointZone4 = new OverSpeedConfig();
        }

        public SendOverSpeedConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.SendOverSpeedConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.SendOverSpeedConfig, segment.Type));
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.SendOverSpeedConfig;

            GazettedZone1 = new OverSpeedConfig();
            GazettedZone2 = new OverSpeedConfig();
            GazettedZone3 = new OverSpeedConfig();
            GazettedZone4 = new OverSpeedConfig();
            WaypointZone1 = new OverSpeedConfig();
            WaypointZone2 = new OverSpeedConfig();
            WaypointZone3 = new OverSpeedConfig();
            WaypointZone4 = new OverSpeedConfig();

            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The id of the vehicle config the unit is running
        /// </summary>
        public int ConfigId { get; set; }

        /// <summary>
        /// The major version of the config
        /// </summary>
        public int ConfigVersionMajor { get; set; }

        /// <summary>
        /// The minor version of the config
        /// </summary>
        public int ConfigVersionMinor { get; set; }

        /// <summary>
        /// The gazetted overspeed zone 1 config
        /// </summary>
        public OverSpeedConfig GazettedZone1 { get; set; }

        /// <summary>
        /// The gazetted overspeed zone 2 config
        /// </summary>
        public OverSpeedConfig GazettedZone2 { get; set; }

        /// <summary>
        /// The gazetted overspeed zone 3 config
        /// </summary>
        public OverSpeedConfig GazettedZone3 { get; set; }

        /// <summary>
        /// The gazetted overspeed zone 4 config
        /// </summary>
        public OverSpeedConfig GazettedZone4 { get; set; }

        /// <summary>
        /// The id of the waypoint group the unit is running
        /// </summary>
        public int WaypointGroupId { get; set; }

        /// <summary>
        /// The major version of the waypoint group
        /// </summary>
        public int WaypointGroupVersionMajor { get; set; }

        /// <summary>
        /// The minor version of the waypoint group
        /// </summary>
        public int WaypointGroupVersionMinor { get; set; }

        /// <summary>
        /// The waypoint overspeed zone 1 config
        /// </summary>
        public OverSpeedConfig WaypointZone1 { get; set; }

        /// <summary>
        /// The waypoint overspeed zone 2 config
        /// </summary>
        public OverSpeedConfig WaypointZone2 { get; set; }

        /// <summary>
        /// The waypoint overspeed zone 3 config
        /// </summary>
        public OverSpeedConfig WaypointZone3 { get; set; }

        /// <summary>
        /// The waypoint overspeed zone 4 config
        /// </summary>
        public OverSpeedConfig WaypointZone4 { get; set; }

        #endregion

        #region Override Methods

        public override byte[] GetBytes()
        {
            // Set the data
            using (MemoryStream stream = new MemoryStream())
            {
                // Write the configuration data
                Write4ByteInteger(stream, ConfigId);
                Write4ByteInteger(stream, ConfigVersionMajor);
                Write4ByteInteger(stream, ConfigVersionMinor);
                GazettedZone1.Encode(stream);
                GazettedZone2.Encode(stream);
                GazettedZone3.Encode(stream);
                GazettedZone4.Encode(stream);

                // Write the waypoint data
                Write4ByteInteger(stream, WaypointGroupId);
                Write4ByteInteger(stream, WaypointGroupVersionMajor);
                Write4ByteInteger(stream, WaypointGroupVersionMinor);
                WaypointZone1.Encode(stream);
                WaypointZone2.Encode(stream);
                WaypointZone3.Encode(stream);
                WaypointZone4.Encode(stream);

                Data = stream.ToArray();
                stream.Close();
            }

            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);

            DecodeData();

            return read;
        }

        #endregion

        #region Private Methods

        private void DecodeData()
        {
            int index = 0;

            // Read the configuration data
            ConfigId = Read4ByteInteger(Data, ref index);
            ConfigVersionMajor = Read4ByteInteger(Data, ref index);
            ConfigVersionMinor = Read4ByteInteger(Data, ref index);
            GazettedZone1.Decode(Data, ref index);
            GazettedZone2.Decode(Data, ref index);
            GazettedZone3.Decode(Data, ref index);
            GazettedZone4.Decode(Data, ref index);

            // Read the waypoint data
            WaypointGroupId = Read4ByteInteger(Data, ref index);
            WaypointGroupVersionMajor = Read4ByteInteger(Data, ref index);
            WaypointGroupVersionMinor = Read4ByteInteger(Data, ref index);
            WaypointZone1.Decode(Data, ref index);
            WaypointZone2.Decode(Data, ref index);
            WaypointZone3.Decode(Data, ref index);
            WaypointZone4.Decode(Data, ref index);
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace MTData.Transport.GenericProtocol.Listener
{
    /// <summary>
    /// DVR config event
    /// </summary>
    [DataContract]
    public class DvrConfigEvent
    {
        [DataMember]
        public int ReasonId { get; set; }

        [DataMember]
        public int AutoImageUpload { get; set; }

        [DataMember]
        public int AutoLowResVideoUpload { get; set; }

        [DataMember]
        public int AutoLowResVideoDuration { get; set; }
    }

    /// <summary>
    /// DVR config content storage in hours
    /// </summary>
    [DataContract]
    public class DvrConfigContentStorage
    {
        [DataMember]
        public int Image { get; set; }

        [DataMember]
        public int VideoLowRes { get; set; }

        [DataMember]
        public int VideoHighRes { get; set; }
    }

    /// <summary>
    /// DVR config detail
    /// </summary>
    [DataContract]
    public class DvrConfigDetail
    {
        [DataMember]
        public int ConfigNo { get; set; }

        [DataMember]
        public int ConfigVersionMajor { get; set; }

        [DataMember]
        public int ConfigVersionMinor { get; set; }

        [DataMember]
        public string Url { get; set; }

        [DataMember]
        public string UrlStream { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public bool? SendHealthStatus { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public int? HealthStatusUtcOffset { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public DvrConfigContentStorage ContentStorageHours { get; set; }

        [DataMember(EmitDefaultValue = false)]
        public List<DvrConfigEvent> Events { get; set; }
    }

    /// <summary>
    /// This segment contains the dvr config information sent to the unit
    /// </summary>
    public class DvrConfigSegment : SegmentLayer
    {
        #region properties

        public DvrConfigDetail Detail { get; set; }

        #endregion

        #region constructor

        public DvrConfigSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.DvrConfig;
            Detail = new DvrConfigDetail();
        }

        public DvrConfigSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.DvrConfig)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.DvrConfig, segment.Type));
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.DvrConfig;
            Detail = new DvrConfigDetail();
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region override methods

        public string GetText()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer js = new DataContractJsonSerializer(typeof(DvrConfigDetail));
                js.WriteObject(stream, Detail);

                stream.Position = 0;
                StreamReader sr = new StreamReader(stream);

                return sr.ReadToEnd();
            }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(typeof(DvrConfigDetail));

                serializer.WriteObject(stream, Detail);

                Data = stream.ToArray();
                stream.Close();
            }

            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region private methods

        private void DecodeData()
        {
            using (MemoryStream stream = new MemoryStream(Data))
            {
                DataContractJsonSerializer serializer = new DataContractJsonSerializer(Detail.GetType());

                Detail = serializer.ReadObject(stream) as DvrConfigDetail;

                stream.Close();
            }
        }

        #endregion
    }
}
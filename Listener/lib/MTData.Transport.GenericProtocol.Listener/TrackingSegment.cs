﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Listener
{
    public enum ReasonCodes
    {
        StatusReport,
        MassDeclaration
    }


    /// <summary>
    /// this segment contains the tarcking information sent with all other listener segments from the unit
    /// </summary>
    public class TrackingSegment : SegmentLayer
    {
        #region private fields
        private int _fleet;
        private int _vehicle;
        private string _serialNumber;
        private ReasonCodes _reasonCode;
        private List<TrackingFlag> _trackingFlags;
        #endregion

        #region properties
        public int Fleet
        {
            get { return _fleet; }
            set { _fleet = value; }
        }
        public int Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
        public string SerialNumber
        {
            get { return _serialNumber; }
            set { _serialNumber = value; }
        }
        public ReasonCodes ReasonCode
        {
            get { return _reasonCode; }
            set { _reasonCode = value; }
        }
        public List<TrackingFlag> TrackingFlags
        {
            get { return _trackingFlags; }
            set { _trackingFlags = value; }
        }
        #endregion

        #region constructor
        public TrackingSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.TrackingSegment;
            _trackingFlags = new List<TrackingFlag>();
        }
        public TrackingSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.TrackingSegment)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.TrackingSegment, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.TrackingSegment;
            _trackingFlags = new List<TrackingFlag>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleet);
                Write4ByteInteger(stream, _vehicle);
                WriteString(stream, _serialNumber, 0x12);
                Write4ByteInteger(stream, (int)_reasonCode);
                Write2ByteInteger(stream, _trackingFlags.Count);
                foreach (TrackingFlag flag in _trackingFlags)
                {
                    flag.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleet = Read4ByteInteger(Data, ref index);
            _vehicle = Read4ByteInteger(Data, ref index);
            _serialNumber = ReadString(Data, ref index, 0x12);
            _reasonCode = (ReasonCodes)Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _trackingFlags.Clear();
            for (int i = 0; i < count; i++)
            {
                TrackingFlag flag = new TrackingFlag();
                flag.Decode(Data, ref index);
                _trackingFlags.Add(flag);
            }
        }
        #endregion
    }
}

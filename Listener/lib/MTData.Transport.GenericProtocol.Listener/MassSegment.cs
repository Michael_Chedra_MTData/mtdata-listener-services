﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener
{
    #region enum declarations
    /// <summary>
    /// the scales type
    /// </summary>
    public enum ScaleTypes
    {
        Manual = 0, // Manual declaration (driver input)
        LoadSMART   // declaration via LoadSMART scales
    }

    /// <summary>
    /// the state the scales are in for this declaration
    /// </summary>
    public enum ScaleStates
    {
        OK = 0,     //the scales are stable, a perfect read
        UNSTABLE_WEIGHT //the scales are reporting the weight is unstable 
    }

    /// <summary>
    /// the vehicle section types for an axle group
    /// </summary>
    public enum SectionTypes
    {
        NO_SECTION = 0,
        PRIME_1_1,
        PRIME_1_2,
        RIGID_1_1,
        RIGID_1_2,
        RIGID_1_3,
        RIGID_2_2,
        RIGID_2_3,
        PIG_1,
        PIG_2,
        PIG_3,
        DOG_1_1,
        DOG_1_2,
        DOG_2_2,
        SEMI_1,
        SEMI_2,
        SEMI_3,
        SEMI_2_5TH_WHEEL,
        SEMI_3_5TH_WHEEL,
        DOLLY_2,
        DOLLY_3,
        Unknown,
        SEMI_4_5TH_WHEEL,
		SEMI_4,
		PRIME_2_2,
		DOG_2_3,
		DOG_3_3,
		DOG_3_4,
		DOLLY_1,
        PRIME_1_1_COMBINED,
        PRIME_1_2_COMBINED,
        PRIME_2_2_COMBINED,
        PRIME_2_3,
        PRIME_2_3_COMBINED
    }
    #endregion

    public class MassSegment : SegmentLayer
    {
        #region private fields
        private ScaleTypes _scalesType;
        private ScaleStates _scalesState;
        /// <summary>
        /// total mass in kg
        /// </summary>
        private int _totalMass;
        private List<MassAxle> _axles;
        private string _ticketId;
        private int _legId;
        #endregion

        #region properties
        public ScaleTypes ScalesType
        {
            get { return _scalesType; }
            set { _scalesType = value; }
        }
        public ScaleStates ScalesState
        {
            get { return _scalesState; }
            set { _scalesState = value; }
        }
        public int TotalMass
        {
            get { return _totalMass; }
            set { _totalMass = value; }
        }
        public List<MassAxle> Axles
        {
            get { return _axles; }
            set { _axles = value; }
        }
        public string TicketId
        {
            get { return _ticketId; }
            set { _ticketId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        #endregion

        #region constructor
        public MassSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.Mass;
            _axles = new List<MassAxle>();
        }
        public MassSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.Mass)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.Mass, segment.Type));
            }
            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.Mass;
            _axles = new List<MassAxle>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)(_scalesType));
                stream.WriteByte((byte)(_scalesState));
                Write4ByteInteger(stream, _totalMass);
                Write2ByteInteger(stream, _axles.Count);
                foreach (MassAxle axle in _axles)
                {
                    axle.Encode(stream);
                }
                WriteString(stream, _ticketId, 0x12);
                Write4ByteInteger(stream, _legId);
                Data = stream.ToArray();
                stream.Close();
            }
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _scalesType = (ScaleTypes)Data[index++];
            _scalesState = (ScaleStates)Data[index++];
            _totalMass = Read4ByteInteger(Data, ref index);
            _axles.Clear();
            int count = Read2ByteInteger(Data, ref index);
            for (int i = 0; i < count; i++)
            {
                MassAxle axle = new MassAxle();
                axle.Decode(Data, ref index);
                _axles.Add(axle);
            }
            _ticketId = ReadString(Data, ref index, 0x12);
            _legId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }

    /// <summary>
    /// A mass decalation for an axle
    /// </summary>
    public class MassAxle : IPacketEncode
    {
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private SectionTypes _axleType;
        /// <summary>
        /// mass in KG
        /// </summary>
        private int _mass;

        public MassAxle() { }
        public MassAxle(SectionTypes axleType, int mass)
        {
            _axleType = axleType;
            _mass = mass;
        }

        public SectionTypes AxleType { get { return _axleType; } set { _axleType = value; } }
        /// <summary>
        /// mass in kg
        /// </summary>
        public int Mass { get { return _mass; } set { _mass = value; } }

        #region IPacketEncode members

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (MemoryStream local = new MemoryStream())
            {
                local.WriteByte((byte)(_axleType));
                BaseLayer.Write4ByteInteger(local, _mass);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                byte[] bytes = local.ToArray();
                BaseLayer.WriteMoreFlag(stream, bytes.Length);
                //now write the data
                stream.Write(bytes, 0, bytes.Length);
            }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _axleType = (SectionTypes)data[index++];
            _mass = BaseLayer.Read2ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.Waypoints
{
    public class WaypointsSegment : SegmentLayer
    {
        #region Constructor

        public WaypointsSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.Waypoints;
            Waypoints = new List<Waypoint>();
        }

        public WaypointsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.Waypoints)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.Waypoints, segment.Type));
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.Waypoints;

            Waypoints = new List<Waypoint>();

            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The id of the vehicle config the unit is running
        /// </summary>
        public bool IsUpToDate { get; set; }

        /// <summary>
        /// The SetpointNumber of the waypoint group
        /// </summary>
        public int WaypointGroupNumber { get; set; }
        /// <summary>
        /// The major version of the waypoint group
        /// </summary>
        public byte WaypointGroupVersionMajor { get; set; }

        /// <summary>
        /// The minor version of the waypoint group
        /// </summary>
        public byte WaypointGroupVersionMinor { get; set; }
        
        public List<Waypoint> Waypoints { get; set; }

        #endregion

        #region Override Methods

        public override byte[] GetBytes()
        {
            // Set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, WaypointGroupNumber);
                stream.WriteByte((byte)WaypointGroupVersionMajor);
                stream.WriteByte((byte)WaypointGroupVersionMinor);
                WriteBool(stream, IsUpToDate);
                if (!IsUpToDate)
                {
                    Write4ByteInteger(stream, Waypoints.Count);
                    foreach(var wayPoint in Waypoints)
                    {
                        wayPoint.Encode(stream);
                    }
                }
                Data = stream.ToArray();
                stream.Close();
            }

            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion


        #region Private Methods

        private void DecodeData()
        {
            int index = 0;

            WaypointGroupNumber = Read4ByteInteger(Data, ref index);
            WaypointGroupVersionMajor = Data[index++];
            WaypointGroupVersionMinor = Data[index++];
            IsUpToDate = ReadBool(Data, ref index);

            if (!IsUpToDate)
            {
                var count = Read4ByteInteger(Data, ref index);
                for(var i = 0; i < count; i++)
                {
                    var waypoint = new Waypoint();
                    waypoint.Decode(Data, ref index);
                    Waypoints.Add(waypoint);
                }
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Listener.Waypoints
{
    public enum GeofenceType : byte
    {
        Rectangle,
        Circle,
        Polygon
    }

    public class NodePoint : IPacketEncode
    {
        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public void Decode(byte[] data, ref int index)
        {
            Latitude = BaseLayer.ReadCoordinate(data, ref index);
            Latitude = BaseLayer.ReadCoordinate(data, ref index);
        }

        public void Encode(MemoryStream stream)
        {
            // Set the data
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.WriteCoordinate(local, Latitude);
                BaseLayer.WriteCoordinate(local, Longitude);
                BaseLayer.CopyStream(local, stream);
            }
        }
    }

    public class Waypoint : IPacketEncode
    {
        /// <summary>
        /// The version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        #region Constructor

        public Waypoint()
        {
            NodePoints = new List<NodePoint>();
        }

        #endregion

        #region Properties

        public int SetPointID { get; set; }

        public int Flags { get; set; }

        public byte DurationTime { get; set; }

        public float Speed { get; set; }

        public double Latitude { get; set; }

        public double Longitude { get; set; }

        public int XTolerance { get; set; }

        public int YTolerance { get; set; }

        public GeofenceType GeofenceType { get; set; }

        public int Radius { get; set; }

        public List<NodePoint> NodePoints { get; set; }

        #endregion

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            SetPointID = BaseLayer.Read4ByteInteger(data, ref index);
            Flags = BaseLayer.Read2ByteInteger(data, ref index);
            DurationTime = data[index++];
            Speed = BaseLayer.ReadFloat(data, ref index);
            Latitude = BaseLayer.ReadCoordinate(data, ref index);
            Longitude = BaseLayer.ReadCoordinate(data, ref index);
            XTolerance = BaseLayer.Read4ByteInteger(data, ref index);
            YTolerance = BaseLayer.Read4ByteInteger(data, ref index);
            GeofenceType = (GeofenceType)data[index++];
            if(GeofenceType == GeofenceType.Circle)
            {
                Radius = BaseLayer.Read2ByteInteger(data, ref index);
            }
            else if(GeofenceType == GeofenceType.Polygon)
            {
                var count = BaseLayer.Read2ByteInteger(data, ref index);
                for (int i = 0; i < count; i++)
                {
                    var nodePoint = new NodePoint();
                    nodePoint.Decode(data, ref index);
                    NodePoints.Add(nodePoint);
                }
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, SetPointID);
                BaseLayer.Write2ByteInteger(local, Flags);
                local.WriteByte((byte)DurationTime);
                BaseLayer.WriteFloat(local, Speed);
                BaseLayer.WriteCoordinate(local, Latitude);
                BaseLayer.WriteCoordinate(local, Longitude);
                BaseLayer.Write4ByteInteger(local, XTolerance);
                BaseLayer.Write4ByteInteger(local, YTolerance);
                local.WriteByte((byte)GeofenceType);
                if(GeofenceType == GeofenceType.Circle)
                {
                    BaseLayer.Write2ByteInteger(local, Radius);
                }
                else if(GeofenceType == GeofenceType.Polygon)
                {
                    BaseLayer.Write2ByteInteger(local, NodePoints.Count);
                    foreach(var nodePoint in NodePoints)
                    {
                        nodePoint.Encode(local);
                    }
                }
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data

                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener.Waypoint
{
    public class RequestWaypointsSegment : SegmentLayer
    {
        #region Constructor

        public RequestWaypointsSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.RequestWaypoints;
        }

        public RequestWaypointsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.RequestWaypoints)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.RequestWaypoints, segment.Type));
            }

            Version = segment.Version;
            Type = (int)ListenerSegmentTypes.RequestWaypoints;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Properties

        /// <summary>
        /// The id of the fleet
        /// </summary>
        public int FleetId { get; set; }

        /// <summary>
        /// The id of the vehicle
        /// </summary>
        public int VehicleId { get; set; }

        /// <summary>
        /// The id of the waypoint group the unit is running
        /// </summary>
        public int WaypointGroupNumber { get; set; }

        /// <summary>
        /// The major version of the waypoint group
        /// </summary>
        public byte WaypointGroupVersionMajor { get; set; }

        /// <summary>
        /// The minor version of the waypoint group
        /// </summary>
        public byte WaypointGroupVersionMinor { get; set; }

        #endregion

        #region Override Methods

        public override byte[] GetBytes()
        {
            // Set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, FleetId);
                Write4ByteInteger(stream, VehicleId);
                Write4ByteInteger(stream, WaypointGroupNumber);
                stream.WriteByte((byte)WaypointGroupVersionMajor);
                stream.WriteByte((byte)WaypointGroupVersionMinor);
                Data = stream.ToArray();
                stream.Close();
            }

            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region Private Methods

        private void DecodeData()
        {
            int index = 0;

            FleetId = Read4ByteInteger(Data, ref index);
            VehicleId = Read4ByteInteger(Data, ref index);
            WaypointGroupNumber = Read4ByteInteger(Data, ref index);
            WaypointGroupVersionMajor = Data[index++];
            WaypointGroupVersionMinor = Data[index++];
        }

        #endregion

    }
}

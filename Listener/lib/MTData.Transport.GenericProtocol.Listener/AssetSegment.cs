﻿using System;
using System.IO;

namespace MTData.Transport.GenericProtocol.Listener
{
    public enum AssetType
    {
        SatModem = 1,
        VIN
    }

    public class AssetSegment : SegmentLayer
    {
        #region Private Variables
        
        private AssetType assetType;

        private string assetData;

        #endregion

        #region Constructors

        public AssetSegment()
        {
            Version = 1;
            Type = (int)ListenerSegmentTypes.AssetPacket;
        }

        public AssetSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)ListenerSegmentTypes.AssetPacket)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", ListenerSegmentTypes.AssetPacket, segment.Type));
            }
            Version = segment.Version;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Public Properties

        public AssetType AssetType
        {
            get { return assetType; }
            set { assetType = value; }
        }

        // AssetType = 1: IMEI number, AssetType = 2: VIN number
        public string AssetData
        {
            get { return assetData; }
            set { assetData = value; }
        }

        #endregion

        #region Override Methods

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream memoryStream = new MemoryStream())
            {
                Write2ByteInteger(memoryStream, (int)assetType);
                WriteString(memoryStream, assetData);
                Data = memoryStream.ToArray();
                memoryStream.Close();
            }

            return base.GetBytes();
        }

        #endregion

        #region Private Methods

        private void DecodeData()
        {
            int index = 0;
            assetType = (AssetType)Read2ByteInteger(Data, ref index);
            assetData = ReadString(Data, ref index, 0x0);
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using System.Management.Instrumentation;

namespace MTData.Common.WMI
{
    [InstrumentationClass(InstrumentationType.Instance)]
    public class MTDataMonitor : Instance
    {
        #region Private Variables

        private static readonly Dictionary<string, MTDataMonitor> _instances = new Dictionary<string, MTDataMonitor>();

        private readonly string _serviceName = "ListenerService";

        #endregion

        #region Properties

        public string ServiceName
        {
            get { return _serviceName; }
        }

        public int PacketsReceived { get; set; }
        public int CurrentMemoryQueueSize { get; set; }
        public int MaxMemoryQueueSize { get; set; }
        public int PacketsThrownAway { get; set; }
        public int PacketsProcessed { get; set; }
        public int PacketsSent { get; set; }

        public int TransactionsAddedToMsmq { get; set; }
        public int TransactionsRemovedFromMsmq { get; set; }
        public int TransactionsMaxQueueSize { get; set; }
        public double AvTransactionQueueTimeTicks { get; set; }
        public double AvTransactionSaveTimeTicks { get; set; }

        public int ServerLoadLevel { get; set; }

        #endregion

        #region Constructors

        /// <summary>
        /// The default Constructor.
        /// </summary>
        private MTDataMonitor(string serviceName)
            : base()
        {
            this._serviceName = serviceName;
        }

        #endregion

        #region Public Methods

        public static MTDataMonitor GetInstance(string serviceName)
        {
            try
            {
                if (!_instances.ContainsKey(serviceName))
                {
                    _instances.Add(serviceName, new MTDataMonitor(serviceName));
                }

                return _instances[serviceName];
            }
            catch (Exception)
            {
                return null;
            }

        }

        #endregion

    }
}

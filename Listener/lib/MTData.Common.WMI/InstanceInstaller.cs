using System;
using System.Collections.Generic;
using System.Text;
using System.Management.Instrumentation;

[assembly: Instrumented("root/MTData")]

namespace MTData.Common.WMI
{
    [System.ComponentModel.RunInstaller(true)]
    public class InstanceInstaller : DefaultManagementProjectInstaller
    {
    }
}

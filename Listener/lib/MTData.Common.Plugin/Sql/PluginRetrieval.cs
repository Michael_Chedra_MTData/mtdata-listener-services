using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using MTData.Common.Plugin.Configuration;

namespace MTData.Common.Plugin.Sql
{
    /// <summary>
    /// This class will provide capabilities to retrieve plugins form the database.
    /// </summary>
    public class PluginRetrieval : IDisposable
    {
        private SqlConnection _connection = null;

        public PluginRetrieval(string connectionString)
        {
            _connection = new SqlConnection(connectionString);
        }

        /// <summary>
        /// Open the connection
        /// </summary>
        public void Open()
        {
            _connection.Open();
        }

        /// <summary>
        /// Close the connection
        /// </summary>
        public void Close()
        {
            if (_connection.State != ConnectionState.Closed)
                _connection.Close();
        }

        /// <summary>
        /// Connection to the underlying plugin database
        /// </summary>
        protected SqlConnection Connection
        {
            get { return _connection; }
        }

        private SqlTransaction _transaction = null;
        protected SqlTransaction Transaction
        {
            get { return _transaction; }
        }

        public void BeginTransaction()
        {
            if (_transaction != null)
                throw new Exception("Transaction already active");

            _transaction = _connection.BeginTransaction();
        }

        public void CommitTransaction()
        {
            if (_transaction == null)
                throw new Exception("No Transaction active");

            _transaction.Commit();
            _transaction = null;
        }

        public void RollbackTransaction()
        {
            if (_transaction == null)
                throw new Exception("No Transaction active");
            _transaction.Rollback();
            _transaction = null;
        }

        protected SqlCommand CreateCommand(string sqlText)
        {
            if (_transaction != null)
                return new SqlCommand(sqlText, _connection, _transaction);
            else
                return new SqlCommand(sqlText, _connection);
        }
        /// <summary>
        /// Populate the plugin reader from the Sql Data Reader
        /// </summary>
        /// <param name="reader"></param>
        /// <returns></returns>
        protected Configuration.PluginDefinition CreatePluginDefinitionFromReader(SqlDataReader reader)
        {
            Configuration.PluginDefinition definition = new Configuration.PluginDefinition();
            definition.ModuleID = (int)reader["ModuleID"];
            definition.PluginID = (int)reader["PluginID"];
            definition.PluginVersionMajor = (int)reader["PluginVersionMajor"];
            definition.PluginVersionMinor = (int)reader["PluginVersionMinor"];
            definition.PluginType = (int)reader["PluginType"];
            definition.State = (int)reader["State"];
            definition.BuildNumber = (int)reader["BuildNumber"];
            definition.CreationDateUTC = (DateTime)reader["CreationDateUTC"];
            definition.ModifiedDateUTC = (DateTime)reader["ModifiedDateUTC"];
            definition.Name = (string)reader["Name"];
            definition.TypeName = (string)reader["TypeName"];
            definition.SecurityRole = (string)reader["SecurityRole"];
            definition.PluginParent = (string)reader["PluginParent"];
            object configData = reader["ConfigData"];
            if (configData is System.DBNull)
                definition.ConfigData = null;
            else
                definition.ConfigData = (byte[])configData;

            definition.Assembly = CreateAssemblyFromReader(reader, false);
            return definition;
        }

        public Configuration.PluginDefinition[] GetInstalledPlugins(int pluginType, int userID, string pluginParent, int pluginVersionMajor, int pluginVersionMinor)
        {
            Configuration.PluginDefinition[] result = null;

            using (SqlCommand command = CreateCommand("usp_PluginDefinitionsGet"))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = command.Parameters.Add("@PluginType", SqlDbType.Int);
                parameter.Value = pluginType;
                parameter = command.Parameters.Add("@UserID", SqlDbType.Int);
                parameter.Value = userID;
                parameter = command.Parameters.Add("@PluginParent", SqlDbType.VarChar);
                parameter.Value = pluginParent;
                parameter = command.Parameters.Add("@PluginVersionMajor", SqlDbType.Int);
                parameter.Value = pluginVersionMajor;
                parameter = command.Parameters.Add("@PluginVersionMinor", SqlDbType.Int);
                parameter.Value = pluginVersionMinor;

                ArrayList list = new ArrayList();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                        list.Add(CreatePluginDefinitionFromReader(reader));
                }

                result = (Configuration.PluginDefinition[])list.ToArray(typeof(Configuration.PluginDefinition));
            }

            return result;
        }

        /// <summary>
        /// Return a list of all installed plugins
        /// </summary>
        /// <returns></returns>
        public Configuration.PluginDefinition[] GetInstalledPlugins()
        {
            Configuration.PluginDefinition[] result = null;

            using (SqlCommand command = CreateCommand("usp_PluginDefinitionsGetAll"))
            {
                command.CommandType = CommandType.StoredProcedure;
                ArrayList list = new ArrayList();

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                        list.Add(CreatePluginDefinitionFromReader(reader));
                }

                result = (Configuration.PluginDefinition[])list.ToArray(typeof(Configuration.PluginDefinition));
            }

            return result;
        }

        /// <summary>
        /// Populate an assembly object fomr the data in the reader.
        /// </summary>
        /// <param name="reader"></param>
        /// <param name="readAssemblyData"></param>
        /// <returns></returns>
        protected Configuration.PluginAssembly CreateAssemblyFromReader(SqlDataReader reader, bool readAssemblyData)
        {
            Configuration.PluginAssembly assembly = new Configuration.PluginAssembly();
            assembly.ID = (int)reader["AssemblyID"];
            assembly.AssemblyName = (string)reader["AssemblyName"];
            assembly.AssemblyDateUTC = (DateTime)reader["AssemblyDateUTC"];
            assembly.VersionMajor = (int)reader["AssemblyVersionMajor"];
            assembly.VersionMinor = (int)reader["AssemblyVersionMinor"];

            if (readAssemblyData)
                assembly.AssemblyData = (byte[])reader["Assembly"];

            return assembly;
        }

        public Configuration.PluginAssembly GetInstalledAssembly(int assemblyID, int versionMajor, int versionMinor)
        {
            Configuration.PluginAssembly result = null;
            using (SqlCommand command = CreateCommand("usp_PluginAssemblyGet"))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = command.Parameters.Add("@AssemblyID", SqlDbType.Int);
                parameter.Value = assemblyID;
                parameter = command.Parameters.Add("@AssemblyVersionMajor", SqlDbType.Int);
                parameter.Value = versionMajor;
                parameter = command.Parameters.Add("@AssemblyVersionMinor", SqlDbType.Int);
                parameter.Value = versionMinor;

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                        result = CreateAssemblyFromReader(reader, true);
                }
            }
            return result;
        }

        /// <summary>
        /// REturn a list of the installed assemblies.
        /// </summary>
        /// <returns></returns>
        public Configuration.PluginAssembly[] GetInstalledAssemblies()
        {
            Configuration.PluginAssembly[] result = null;
            using (SqlCommand command = CreateCommand("usp_PluginAssembliesGet"))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlDataReader reader = command.ExecuteReader();
                try
                {
                    ArrayList list = new ArrayList();
                    while (reader.Read())
                    {
                        PluginAssembly assembly = CreateAssemblyFromReader(reader, false);
                        list.Add(assembly);
                    }
                    result = (Configuration.PluginAssembly[])list.ToArray(typeof(Configuration.PluginAssembly));
                }
                finally
                {
                    reader.Close();
                }
            }

            return result;
        }

        private Configuration.ModuleDefinition CreateModuleDefinitionFromReader(SqlDataReader reader)
        {
            Configuration.ModuleDefinition result = new MTData.Common.Plugin.Configuration.ModuleDefinition();
            result.ModuleID = (int)reader["ID"];
            result.Name = (string)reader["Name"];
            result.Description = (string)reader["Description"];
            return result;
        }

        /// <summary>
        /// Return a list of the modules installed.
        /// </summary>
        /// <returns></returns>
        public Configuration.ModuleDefinition[] GetInstalledModules()
        {
            Configuration.ModuleDefinition[] result = null;
            using (SqlCommand command = CreateCommand("usp_PluginModulesGet"))
            {
                command.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    ArrayList list = new ArrayList();
                    while (reader.Read())
                        list.Add(CreateModuleDefinitionFromReader(reader));
                    result = (Configuration.ModuleDefinition[])list.ToArray(typeof(Configuration.ModuleDefinition));
                }
            }

            return result;
        }

        private Configuration.PluginType CreatePluginTypeFromReader(SqlDataReader reader)
        {
            Configuration.PluginType result = new MTData.Common.Plugin.Configuration.PluginType();
            result.ID = (int)reader["PluginType"];
            result.Name = (string)reader["Name"];
            return result;
        }

        /// <summary>
        /// REturn a list of the available plugin types.
        /// </summary>
        /// <returns></returns>
        public Configuration.PluginType[] GetInstalledPluginTypes()
        {
            Configuration.PluginType[] result = null;
            using (SqlCommand command = CreateCommand("usp_PluginTypesGet"))
            {
                command.CommandType = CommandType.StoredProcedure;
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    ArrayList list = new ArrayList();
                    while (reader.Read())
                        list.Add(CreatePluginTypeFromReader(reader));
                    result = (Configuration.PluginType[])list.ToArray(typeof(Configuration.PluginType));
                }
            }

            return result;
        }

        /// <summary>
        /// Return a list of the assemblies that a specific assembly depends on
        /// </summary>
        /// <returns></returns>
        public Configuration.PluginAssembly[] GetAssemblyDependancies(int assemblyID, int versionMajor, int versionMinor, bool deep)
        {
            Hashtable dependancies = new Hashtable();
            AppendUniqueDependancies(assemblyID, versionMajor, versionMinor, deep, dependancies);

            PluginAssembly[] result = new PluginAssembly[dependancies.Count];
            dependancies.Values.CopyTo(result, 0);

            return result;
        }

        public void AppendUniqueDependancies(int assemblyID, int versionMajor, int versionMinor, bool deep, Hashtable dependancies)
        {
            using (SqlCommand command = CreateCommand("usp_PluginAssemblyDependanciesGet"))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = command.Parameters.Add("@AssemblyID", SqlDbType.Int);
                parameter.Value = assemblyID;
                parameter = command.Parameters.Add("@AssemblyVersionMajor", SqlDbType.Int);
                parameter.Value = versionMajor;
                parameter = command.Parameters.Add("@AssemblyVersionMinor", SqlDbType.Int);
                parameter.Value = versionMinor;

                Hashtable thisTier = new Hashtable();
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        PluginAssembly assembly = CreateAssemblyFromReader(reader, false);
                        string key = string.Format("{0}.{1}.{2}", assembly.ID, assembly.VersionMajor, assembly.VersionMinor);
                        if (!dependancies.ContainsKey(key) && !thisTier.ContainsKey(key))
                        {
                            thisTier.Add(key, assembly);
                            dependancies.Add(key, assembly);
                        }
                    }
                }

                if (deep)
                {
                    foreach (PluginAssembly assembly in thisTier.Values)
                        AppendUniqueDependancies(assembly.ID, assembly.VersionMajor, assembly.VersionMinor, deep, dependancies);
                }
            }

        }

        #region IDisposable Support
        private bool disposedValue = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    try { _transaction?.Dispose(); } catch { }
                    try { _connection?.Dispose(); } catch { }
                }

                disposedValue = true;
            }
        }

        void IDisposable.Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
        #endregion

    }
}

using System;
using System.Data;
using System.Data.SqlClient;
using MTData.Common.Plugin.Configuration;

namespace MTData.Common.Plugin.Sql
{
    /// <summary>
    /// given the connection string, and a list of assemblies
    /// and plugins, this class will install them.
    /// </summary>
    public class PluginInstaller : PluginRetrieval
    {

        public PluginInstaller(string connectionString) : base(connectionString)
        {

        }

        /// <summary>
        /// This will install the assembly
        /// </summary>
        /// <param name="assembly"></param>
        public void InstallAssembly(Configuration.PluginAssembly assembly)
        {
            using (SqlCommand command = base.CreateCommand("usp_PluginAssemblyUpdate"))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = command.Parameters.Add("@AssemblyID", SqlDbType.Int);
                parameter.Value = assembly.ID;
                parameter = command.Parameters.Add("@AssemblyVersionMajor", SqlDbType.Int);
                parameter.Value = assembly.VersionMajor;

                parameter = command.Parameters.Add("@AssemblyVersionMinor", SqlDbType.Int);
                parameter.Value = assembly.VersionMinor;
                parameter = command.Parameters.Add("@AssemblyName", SqlDbType.VarChar);
                parameter.Value = assembly.AssemblyName;
                parameter = command.Parameters.Add("@AssemblyDateUTC", SqlDbType.DateTime);
                parameter.Value = assembly.AssemblyDateUTC;
                parameter = command.Parameters.Add("@Assembly", SqlDbType.Image);
                parameter.Value = assembly.AssemblyData;

                command.ExecuteNonQuery();
            }
        }

        public void InstallAssemblyDependancies(PluginAssembly assembly)
        {
            RemoveAssemblyDependancies(assembly);

            foreach (PluginAssembly dependancy in assembly.Dependancies)
                InstallAssemblyDependancy(assembly, dependancy);
        }

        public void InstallAssemblyDependancy(PluginAssembly assembly, PluginAssembly dependantOn)
        {
            using (SqlCommand command = base.CreateCommand("usp_PluginAssemblyDependancyInsert"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@AssemblyID", SqlDbType.Int).Value = assembly.ID;
                command.Parameters.Add("@AssemblyVersionMajor", SqlDbType.Int).Value = assembly.VersionMajor;
                command.Parameters.Add("@AssemblyVersionMinor", SqlDbType.Int).Value = assembly.VersionMinor;

                command.Parameters.Add("@DependantOnAssemblyID", SqlDbType.Int).Value = dependantOn.ID;
                command.Parameters.Add("@DependantOnAssemblyVersionMajor", SqlDbType.Int).Value = dependantOn.VersionMajor;
                command.Parameters.Add("@DependantOnAssemblyVersionMinor", SqlDbType.Int).Value = dependantOn.VersionMinor;

                command.ExecuteNonQuery();
            }
        }

        public void RemoveAssemblyDependancies(PluginAssembly assembly)
        {
            using (SqlCommand command = base.CreateCommand("usp_PluginAssemblyDependanciesDelete"))
            {
                command.CommandType = CommandType.StoredProcedure;
                command.Parameters.Add("@AssemblyID", SqlDbType.Int).Value = assembly.ID;
                command.Parameters.Add("@AssemblyVersionMajor", SqlDbType.Int).Value = assembly.VersionMajor;
                command.Parameters.Add("@AssemblyVersionMinor", SqlDbType.Int).Value = assembly.VersionMinor;

                command.ExecuteNonQuery();
            }
        }

        /// <summary>
        /// This method will install a plugin definition.
        /// If the plugin assembly is not already installed, it will install one.
        /// </summary>
        /// <param name="plugin"></param>
        public void InstallPlugin(Configuration.PluginDefinition plugin)
        {
            using (SqlCommand command = CreateCommand("usp_PluginDefinitionUpdate"))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter parameter = command.Parameters.Add("@ModuleID", SqlDbType.Int);
                parameter.Value = plugin.ModuleID;
                parameter = command.Parameters.Add("@PluginID", SqlDbType.Int);
                parameter.Value = plugin.PluginID;
                parameter = command.Parameters.Add("@PluginVersionMajor", SqlDbType.Int);
                parameter.Value = plugin.PluginVersionMajor;
                parameter = command.Parameters.Add("@PluginVersionMinor", SqlDbType.Int);
                parameter.Value = plugin.PluginVersionMinor;
                parameter = command.Parameters.Add("@PluginType", SqlDbType.Int);
                parameter.Value = plugin.PluginType;
                parameter = command.Parameters.Add("@State", SqlDbType.Int);
                parameter.Value = plugin.State;
                parameter = command.Parameters.Add("@BuildNumber", SqlDbType.Int);
                parameter.Value = plugin.BuildNumber;

                parameter = command.Parameters.Add("@Name", SqlDbType.VarChar);
                parameter.Value = plugin.Name;
                parameter = command.Parameters.Add("@TypeName", SqlDbType.VarChar);
                parameter.Value = plugin.TypeName;
                parameter = command.Parameters.Add("@SecurityRole", SqlDbType.VarChar);
                parameter.Value = plugin.SecurityRole;
                parameter = command.Parameters.Add("@PluginParent", SqlDbType.VarChar);
                parameter.Value = plugin.PluginParent;

                parameter = command.Parameters.Add("@AssemblyID", SqlDbType.Int);
                parameter.Value = plugin.Assembly.ID;
                parameter = command.Parameters.Add("@AssemblyVersionMajor", SqlDbType.Int);
                parameter.Value = plugin.Assembly.VersionMajor;
                parameter = command.Parameters.Add("@AssemblyVersionMinor", SqlDbType.Int);
                parameter.Value = plugin.Assembly.VersionMinor;

                parameter = command.Parameters.Add("@ConfigData", SqlDbType.VarBinary);
                parameter.Value = plugin.ConfigData;

                command.ExecuteNonQuery();
            }
        }
    }
}

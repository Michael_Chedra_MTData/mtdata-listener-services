using System;
using System.Collections;
using System.Collections.Generic;
using log4net;
using MTData.Common.Interface;

namespace MTData.Common.Plugin
{
    /// <summary>
    /// This is a basic implementation for the PluginHost.
    /// </summary>
    public class PluginManager : IPluginHost, IDisposable
    {
        private const string sClassName = "MTData.Common.Plugin.PluginManager.";
        private static ILog _log = LogManager.GetLogger(typeof(PluginManager));

        private enum PluginState
        {
            Defined,
            VerifyFailed,
            Verified,
            Loaded,
            Initialised
        }

        private class PluginReference
        {
            private Configuration.PluginDefinition _definition;
            private PluginState _state;
            private IPlugin _instance;

            public PluginReference(Configuration.PluginDefinition definition)
            {
                _definition = definition;
                _state = PluginState.Defined;
                _instance = null;
            }

            /// <summary>
            /// Verify that the plugin assembly is available.
            /// </summary>
            public void Verify(Configuration.IPluginConfigProvider configProvider, string targetAssemblyDirectory)
            {
                if (_state == PluginState.Defined)
                {
                    //	Verify that the plugin exists, and if it does not, download the required assembly.
                    if (configProvider.GetPluginAssembly(_definition, targetAssemblyDirectory, ""))
                        _state = PluginState.Verified;
                    else
                        _state = PluginState.VerifyFailed;
                }
            }

            public void Verify(Configuration.IPluginConfigProvider configProvider, string targetAssemblyDirectory, string serverCacheName)
            {
                if (_state == PluginState.Defined)
                {
                    //	Verify that the plugin exists, and if it does not, download the required assembly.
                    if (configProvider.GetPluginAssembly(_definition, targetAssemblyDirectory, serverCacheName))
                        _state = PluginState.Verified;
                    else
                        _state = PluginState.VerifyFailed;
                }
            }

            public bool Verified
            {
                get { return ((_state != PluginState.VerifyFailed) && (_state != PluginState.Defined)); }
            }

            public bool Instantiated
            {
                get { return ((_state == PluginState.Loaded) || (_state == PluginState.Initialised)); }
            }

            /// <summary>
            /// Instantiate the plugin and Initialise it
            /// </summary>
            /// <param name="host"></param>
            /// <param name="loader"></param>
            /// <param name="targetAssemblyDirectory"></param>
            public void Initialise(IPluginHost host, Loader.Loader loader, string targetAssemblyDirectory)
            {
                if (_state == PluginState.Verified)
                {
                    if (string.IsNullOrEmpty(loader.CodeBase))
                    {
                        loader.CodeBase = targetAssemblyDirectory;
                    }
                    IPluginFactory factory = (IPluginFactory)loader.Create(targetAssemblyDirectory, _definition.TypeName);

                    _instance = factory.Create(_definition);

                    _state = PluginState.Loaded;
                    _instance.Initialise(host);
                    _state = PluginState.Initialised;
                }
            }

            /// <summary>
            /// Finalise the plugin and dispose of it
            /// </summary>
            /// <param name="host"></param>
            public void Finalise(IPluginHost host)
            {
                if (_state == PluginState.Initialised)
                {
                    _instance.Finalise(host);
                    _state = PluginState.Loaded;
                }

                if (_state == PluginState.Loaded)
                {
                    if (_instance is IDisposable)
                        ((IDisposable)_instance).Dispose();
                    _state = PluginState.Verified;
                }
            }

            public string Key { get { return string.Format("{0}:{1}", ModuleID, PluginID); } }

            public int ModuleID { get { return _definition.ModuleID; } }
            public int PluginID { get { return _definition.PluginID; } }
            public Configuration.PluginDefinition Definition { get { return _definition; } }
            public PluginState State { get { return _state; } }
            public IPlugin Instance { get { return _instance; } }
        }

        private MTData.Common.Loader.Loader _loader = new MTData.Common.Loader.Loader();

        private Configuration.IPluginConfigProvider _configProvider = null;
        private IContextProvider _contextProvider = null;
        private Configuration.IPluginDataProvider _dataProvider = null;
        private Configuration.IPluginSecurityProvider _securityProvider = null;
        private int _pluginType;
        private int _versionMajor;
        private int _versionMinor;
        private string _managerName;
        private string _targetAssemblyDirectory = null;
        private string _serverCacheName;

        private readonly Dictionary<string, PluginReference> _pluginMap = new Dictionary<string, PluginReference>();

        private bool _initialised = false;

        public PluginManager(
            int pluginType,
            int versionMajor,
            int versionMinor,
            string managerName,
            Configuration.IPluginConfigProvider configProvider,
            IContextProvider contextProvider,
            Configuration.IPluginDataProvider dataProvider,
            Configuration.IPluginSecurityProvider securityProvider,
            string targetAssemblyDirectory,
            string serverCacheName)
        {
            _configProvider = configProvider;
            _contextProvider = contextProvider;
            _dataProvider = dataProvider;
            _securityProvider = securityProvider;
            _pluginType = pluginType;
            _versionMajor = versionMajor;
            _versionMinor = versionMinor;
            _managerName = managerName;
            _targetAssemblyDirectory = targetAssemblyDirectory;
            _serverCacheName = serverCacheName;
        }

        public PluginManager(
            int pluginType,
            int versionMajor,
            int versionMinor,
            string managerName,
            Configuration.IPluginConfigProvider configProvider,
            IContextProvider contextProvider,
            Configuration.IPluginDataProvider dataProvider,
            Configuration.IPluginSecurityProvider securityProvider,
            string targetAssemblyDirectory)
        {
            _configProvider = configProvider;
            _contextProvider = contextProvider;
            _dataProvider = dataProvider;
            _securityProvider = securityProvider;
            _pluginType = pluginType;
            _versionMajor = versionMajor;
            _versionMinor = versionMinor;
            _managerName = managerName;
            _targetAssemblyDirectory = targetAssemblyDirectory;
            _serverCacheName = "";
        }


        /// <summary>
        /// Load and populate the plugins.
        /// </summary>
        public void LoadPlugins()
        {
            if (!_initialised)
            {
                PopulatePluginReferences();
                VerifyPluginAssemblies();
                InstantiatePlugins();
                _initialised = true;
            }
        }

        /// <summary>
        /// This will take the list of plugins and insitialise a list of references for them.
        /// </summary>
        private void PopulatePluginReferences()
        {
            Configuration.PluginDefinition[] definitions = _configProvider.GetRegisteredPlugins(_pluginType, _versionMajor, _versionMinor, _managerName);
            foreach (Configuration.PluginDefinition definition in definitions)
            {
                PluginReference reference = new PluginReference(definition);
                if (!_pluginMap.ContainsKey(reference.Key))
                _pluginMap.Add(reference.Key, reference);
            }
        }

        /// <summary>
        /// Verify the plugin assemblies, and ensure that they are present and available for use.
        /// </summary>
        private void VerifyPluginAssemblies()
        {
            foreach (PluginReference reference in _pluginMap.Values)
            {
                try
                {
                    reference.Verify(_configProvider, _targetAssemblyDirectory, _serverCacheName);
                }
                catch (Exception ex)
                {
                    _log.Error(sClassName + "VerifyPluginAssemblies()", ex);
                }
            }
        }

        /// <summary>
        /// Instantiate all of the plugins that have been verified.
        /// </summary>
        private void InstantiatePlugins()
        {
            foreach (PluginReference reference in _pluginMap.Values)
            {
                if (reference.Verified)
                    try
                    {
                        reference.Initialise(this, _loader, _targetAssemblyDirectory);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(sClassName + string.Format("InstantiatePlugins() - Parmeters Manager : {0} Module ID : {1} Plugin ID : {2}", _managerName, reference.ModuleID, reference.PluginID), ex);
                    }
            }
        }

        #region IPluginHost Members

        public void RegisterFormType(string name, Type formType)
        {
            //	not yet implemented.
        }

        public Configuration.IPluginDataProvider DataProvider
        {
            get
            {
                return _dataProvider;
            }
        }

        public IContextProvider ContextProvider
        {
            get
            {
                return _contextProvider;
            }
        }

        public int CurrentUserID
        {
            get
            {
                return _securityProvider.CurrentUserID;
            }
        }

        public bool UserHasRole(string roleName)
        {
            return _securityProvider.UserHasRole(roleName);
        }

        #endregion

        #region IDisposable Members

        public void Dispose()
        {
            foreach (PluginReference reference in _pluginMap.Values)
            {
                if (reference.Instantiated)
                    try
                    {
                        reference.Finalise(this);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(sClassName + string.Format("Dispose() - Parmeters Manager : {0} Module ID : {1} Plugin ID : {2}", _managerName, reference.ModuleID, reference.PluginID), ex);
                    }
            }
        }

        #endregion
    }
}

using System;

namespace MTData.Common.Plugin
{
	/// <summary>
	/// this interface identifies the methods used to create a 
	/// configured plugin instance
	/// </summary>
	public interface IPluginFactory
	{
		IPlugin Create(Configuration.PluginDefinition definition);
	}
}

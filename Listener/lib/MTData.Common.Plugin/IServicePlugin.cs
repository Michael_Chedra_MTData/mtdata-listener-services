﻿using System;
using System.Collections.Generic;
using MTData.Common.Plugin.Service;

namespace MTData.Common.Plugin
{
    /// <summary>
    /// This is the interface that is implemented by Service plugin objects.
    /// This interface allows the objects to be created from XML configuration 
    /// and provides communications between the plugin services
    /// 
    /// Created By : Andrew Downs 13/11/2013
    /// </summary>
    public interface IServicePlugin : IDisposable
    {
        /// <summary>
        /// This is the ID that is allocated to the plugin by the ServicePluginLoader at runtime
        /// </summary>
        int PluginID { get; set; }
        /// <summary>
        /// This property is used to determine if the plugin needs to process messages from other plugins.
        /// </summary>
        bool RequiresMessageFeed { get; set; }
        /// <summary>
        /// Initilaise this object from XML
        /// </summary>
        /// <param name="System.Xml.XmlNode section">The objects XML definition</param>
        object Create(System.Xml.XmlNode section);
        /// <summary>
        /// Stop any threads and cleanup any resources assosiated with this object
        /// </summary>
        void Dispose();
        /// <summary>
        /// Start the threads for this object
        /// </summary>
        void Start();
        /// <summary>
        /// Stop the threads for this object
        /// </summary>
        void Stop();
        /// <summary>
        /// The method on the object to process messages from other plugins
        /// </summary>
        /// <param name="object Message">The object to be processed by this plugin</param>
        SendMessageReponce ProcessMessage<T>(T message);
        /// <summary>
        /// This method will check if the SendMessageDelegate delegate has been set on this object, if so then send the message to the other plugins and wait for a reply.
        /// The delegate will call ServicePluginLoader.SendMessage(int pluginId, object message) 
        /// This will then call the ProcessMessage function on each of the inherited objects and return a list of responces
        /// </summary>
        /// <param name="int pluginId">The ID of the plugin sending the message</param>
        /// <param name="object Message">The object to be sent to the other plugins</param>
        List<SendMessageReponce> SendMessage<T>(int pluginId, T message);
        /// <summary>
        /// This property is the delegate to enqueue a message for processing by other plugins
        /// This delegate should be called when no responce is required for the message.
        /// </summary>
        ServicePluginLoader.AsyncMessageSendDelegate AsyncMessageSendDel { get; set; }
        /// <summary>
        /// This property is the delegate to receive and enqueue messages (objects) from other plugins.
        /// This delegate will async call the ProcessMessage(object message) on the inheriting object
        /// </summary>
        ServicePluginLoader.AsyncMessageReceiveDelegate AsyncMessageReceive { get; set; }
        /// <summary>
        /// This delegate will call ServicePluginLoader.SendMessage(int pluginId, object message) 
        /// This will then call the ProcessMessage function on each of the inherited objects and return a list of responces
        /// </summary>
        ServicePluginLoader.SendMessageDelegate SendMessageDel { get; set; }
    }
}

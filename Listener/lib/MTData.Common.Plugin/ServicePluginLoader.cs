﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;
using System.IO;
using System.Reflection;
using log4net;
using MTData.Common.Plugin.Service;

namespace MTData.Common.Plugin
{
    /// <summary>
    /// This class creates ServicePlugin object from XML configuration in the app.config file.
    /// This class provides communication services between service plugins and the parent (creator) object.
    /// 
    /// Created By : Andrew Downs 13/11/2013
    /// </summary>
    public class ServicePluginLoader : IConfigurationSectionHandler
    {
        // Logging Interface
        private static ILog _log = LogManager.GetLogger(typeof(ServicePluginLoader));
        private List<IServicePlugin> _pluginList;
        private AsyncMessageSendDelegate _asyncMsgSendDel;
        private SendMessageDelegate _sendMsgDel;
        /// <summary>
        /// This delegate allows plugins to send messages to other plugins
        /// </summary>
        public delegate void AsyncMessageSendDelegate(int pluginId, object message);
        /// <summary>
        /// This delegate allows plugins to async recieve messages from other plugins
        /// </summary>
        public delegate void AsyncMessageReceiveDelegate(object message);
        /// <summary>
        /// This delegate allows plugins to async recieve messages from other plugins
        /// </summary>
        public delegate List<SendMessageReponce> SendMessageDelegate(int pluginId, object message);
        /// <summary>
        /// Initilaise this object and load the ServicePlugin object from the XML
        /// </summary>
        /// <param name="object parent">The object making the create call</param>
        /// <param name="object configContext">The configuration context</param>
        /// <param name="XmlNode section">The objects XML definition</param>
        public object Create(object parent, object configContext, XmlNode section)
        {
            string assemblyName = "";
            string typeName = "";
            string requiresMessageFeed = "";
            bool messageFeed = false;
            try
            {
                // Clean up if this is being reloaded.
                this.Dispose();
                // Get a list of plugin defs from the app.config file.
                XmlNodeList plugins = null;
                _pluginList = new List<IServicePlugin>();
                _asyncMsgSendDel = new AsyncMessageSendDelegate(AsyncSendMessage);
                _sendMsgDel = new SendMessageDelegate(SendMessage);
                plugins = section.SelectNodes("Plugin");
                // For each definition
                foreach (XmlNode pluginXml in plugins)
                {
                    try
                    {
                        // Get the .dll or .exe to load the class from and the name of the class to load.
                        typeName = "";
                        assemblyName = "";
                        requiresMessageFeed = "";
                        typeName = pluginXml.Attributes["typeName"].Value;
                        assemblyName = pluginXml.Attributes["assemblyName"].Value;
                        requiresMessageFeed = pluginXml.Attributes["requiresMessageFeed"].Value.ToUpper();
                        if (requiresMessageFeed == "TRUE")
                            messageFeed = true;
                        else
                            messageFeed = false;

                        if (string.IsNullOrEmpty(typeName) || string.IsNullOrEmpty(typeName))
                        {
                            _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section) - Failed to read ServicePlugin configuration, typeName = " + (string.IsNullOrEmpty(typeName) ? "null" : typeName) + ", assemblyName = " + (string.IsNullOrEmpty(assemblyName) ? "null" : assemblyName));
                        }
                        else
                        {
                            // Try and load the specified assembly
                            string assemblyPath = Path.Combine(Environment.CurrentDirectory, assemblyName);
                            // Try and load the specified assembly
                            Assembly assembly = Assembly.LoadFrom(assemblyPath);
                            // Try and create an object of the specified type
                            IServicePlugin pluginObject = (IServicePlugin)assembly.CreateInstance(typeName);
                            if (pluginObject == null)
                            {
                                // If we couldn't create the class instance for some reason.
                                _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section) - Failed to create ServicePlugin from XML, typeName = " + (string.IsNullOrEmpty(typeName) ? "null" : typeName) + ", assemblyName = " + (string.IsNullOrEmpty(assemblyName) ? "null" : assemblyName));
                            }
                            else
                            {
                                try
                                {
                                    // Pass the XML data to the object
                                    pluginObject = (IServicePlugin)pluginObject.Create(pluginXml);
                                    // Allow the plugin to queue messages for other plugins
                                    pluginObject.AsyncMessageSendDel = _asyncMsgSendDel;
                                    // Allow the plugin to send requests to other plugins
                                    pluginObject.SendMessageDel = _sendMsgDel;
                                    // Determines if the plugin is listening for messages
                                    pluginObject.RequiresMessageFeed = messageFeed;
                                }
                                catch (System.Exception exPluginCreateFailure)
                                {
                                    _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section) - Failed to create ServicePlugin, typeName = " + typeName + ", assemblyName = " + assemblyName + "\r\nXML = " + (pluginXml == null ? "null" : pluginXml.OuterXml), exPluginCreateFailure);
                                    pluginObject = null;
                                }
                                // If the object has been created sucessfully, then add it to the list of plugin.
                                if (pluginObject != null)
                                {
                                    // Allocate the plugin a unique ID
                                    pluginObject.PluginID = _pluginList.Count;
                                    // Add the plugin to the list of plugins
                                    _pluginList.Add(pluginObject);
                                }
                            }
                        }
                    }
                    catch (System.Exception exPluginLoadFailure)
                    {
                        _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section) - Failed to load ServicePlugin, typeName = " + (string.IsNullOrEmpty(typeName) ? "null" : typeName) + ", assemblyName = " + (string.IsNullOrEmpty(assemblyName) ? "null" : assemblyName) + "\r\nXML = " + (pluginXml == null ? "null" : pluginXml.OuterXml), exPluginLoadFailure);
                    }
                }

                if (_pluginList != null)
                {
                    foreach (IServicePlugin plugin in _pluginList)
                    {
                        plugin.Start();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }

        /// <summary>
        /// Stop any threads and cleanup any resources assosiated with this object
        /// </summary>
        public void Dispose()
        {
            try
            {
                // If we has a plugin list
                if (_pluginList != null)
                {
                    // Dispose each plugin in the list
                    foreach (IServicePlugin plugin in _pluginList)
                        plugin.Dispose();
                    // Set the plugin list to null.
                    _pluginList = null;
                }
                _asyncMsgSendDel = null;
                _sendMsgDel = null;
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Dispose()", ex);
            }
        }
        /// <summary>
        /// This method call is passed as a delegate to IServicePlugin objects.  The objects
        /// call this delgate to pass a message to other plugins
        /// This method will call GEServiceAbstract.AsyncReceiveMessage to queue the message for processing.
        /// The thread within the GEServiceAbstract will then call ProcessMessage on the inheriting object.
        /// The response from the ProcessMessage call (if any) is thrown away
        /// </summary>
        /// <param name="int pluginId">The ID of the plugin that sent the message</param>
        /// <param name="object message">The message to pass to the other plugins</param>
        private void AsyncSendMessage(int pluginId, object message)
        {
            try
            {
                // If we have plugins loaded
                if (_pluginList != null)
                    // For each plugin, by ID
                    for (int id = 0; id < _pluginList.Count; id++)
                        // If this isn't this plugin that sent the message and it is interested in receiving messages
                        if (id != pluginId && _pluginList[id].AsyncMessageReceive != null)
                            // Call the delegate to queue the message for processing by the plugin.
                            _pluginList[id].AsyncMessageReceive.BeginInvoke(message, AsyncMessageReceivedCallBack, _pluginList[id].AsyncMessageReceive);

            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncSendMessage(int pluginId, object message)", ex);
            }
        }

        /// <summary>
        /// This method is called when the _pluginList[id].AsyncMessageReceive.BeginInvoke is completed.
        /// This function cleans up memory for the async call delegate.
        /// </summary>
        /// <param name="IAsyncResult ar">This is result of the delegate run.</param>
        private void AsyncMessageReceivedCallBack(IAsyncResult ar)
        {
            try
            {
                (ar.AsyncState as AsyncMessageReceiveDelegate).EndInvoke(ar);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncMessageReceivedCallBack(IAsyncResult ar)", ex);
            }
        }
        /// <summary>
        /// This method will check if the SendMessageDelegate delegate has been set on this object, if so then send the message to the other plugins and wait for a reply.
        /// The delegate will call ServicePluginLoader.SendMessage(int pluginId, object message) 
        /// This will then call the ProcessMessage function on each of the inherited objects and return a list of responces
        /// </summary>
        /// <param name="int pluginId">The ID of the plugin sending the message</param>
        /// <param name="object Message">The object to be sent to the other plugins</param>
        private List<SendMessageReponce> SendMessage(int pluginId, object message)
        {
            List<SendMessageReponce> ret = new List<SendMessageReponce>();
            try
            {
                // If we have plugins loaded
                if (_pluginList != null)
                    // For each plugin, by ID
                    for (int id = 0; id < _pluginList.Count; id++)
                        // If this isn't this plugin that sent the message and it is interested in receiving messages
                        if (id != pluginId && _pluginList[id].AsyncMessageReceive != null)
                        {
                            // Call the delegate to queue the message for processing by the plugin.
                            SendMessageReponce resp = _pluginList[id].ProcessMessage(message);
                            if (resp != null)
                                ret.Add(resp);
                        }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendMessage(int pluginId, object message)", ex);
            }
            return ret;
        }
    }
}

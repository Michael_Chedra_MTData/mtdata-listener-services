using System;

namespace MTData.Common.Plugin
{
	/// <summary>
	/// This interface allow the plugin to be intialised.
	/// All plugins must implement this interface.
	/// </summary>
	public interface IPlugin
	{
		string Name {get;}
		string Description {get;}

		void Initialise(IPluginHost host);
		void Finalise(IPluginHost host);
	}
}

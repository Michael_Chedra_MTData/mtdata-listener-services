using System;
using MTData.Common.Interface;

namespace MTData.Common.Plugin
{
	/// <summary>
	/// This interface identifies an action host.
	/// The action host can also support other interfaces.
	/// </summary>
	public interface IPluginHost
	{
		#region Form Manager

		/// <summary>
		/// Allows a form type to be put in the form manager, or to override an
		/// existing default entry
		/// </summary>
		/// <param name="name"></param>
		/// <param name="formType"></param>
		void RegisterFormType(string name, Type formType);
		
		#endregion

		#region PluginDataProvider

		/// <summary>
		/// Allows access to the standard provider for Plugin Datasets.
		/// </summary>
		Configuration.IPluginDataProvider DataProvider {get;}

		#endregion

		#region Host information

		/// <summary>
		/// This property makes available any of the items that are
		/// required for the plugin to perform it's task
		/// </summary>
		IContextProvider ContextProvider{ get;}
		
		#endregion

		#region User Security

		/// <summary>
		/// Returns the current userid
		/// </summary>
		int CurrentUserID {get;}

		/// <summary>
		/// Returns whether the current userid has the specified role.
		/// </summary>
		/// <param name="role"></param>
		/// <returns></returns>
		bool UserHasRole(string roleName);

		#endregion
	}
}

using System;

namespace MTData.Common.Plugin
{
	/// <summary>
	/// This is the type of trigger for an action
	/// </summary>
	public enum ActionTriggerType : int
	{
		Click,
		Checked
	}
}

﻿using System;

namespace MTData.Common.Plugin.Service
{
    /// <summary>
    /// This class provides a responce from as plugin to a SendMessage request
    /// 
    /// Created By : Andrew Downs 13/11/2013
    /// </summary>
    public class SendMessageReponce
    {
        public object Responce;
        public string ErrorMsg;
    }
}

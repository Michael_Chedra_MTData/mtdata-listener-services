﻿using System;
using System.Collections.Generic;
using System.Threading;
using log4net;

namespace MTData.Common.Plugin.Service
{
    /// <summary>
    /// This is the base class that should be inherited by ServicePlugin objects.
    /// This abstract layer provides PluginID, AsyncMessageSend, AsyncMessageReceive and SendMessage functionality
    /// 
    /// Created By : Andrew Downs 17/12/2013
    /// </summary>
    public abstract class ServicePlugin : IServicePlugin
    {
        // Logging Interface
        private static ILog _log = LogManager.GetLogger(typeof(ServicePlugin));
        // Plugin variables
        private ServicePluginLoader.AsyncMessageSendDelegate _asyncMsgSendDel;
        private ServicePluginLoader.AsyncMessageReceiveDelegate _asyncMsgRecvDel;
        private ServicePluginLoader.SendMessageDelegate _sendMsgDel;
        // Message Queue variables
        private object _recvMsgQueueSync;
        private Queue<object> _recvMsgQueue;
        private int _pluginID;
        private bool _recvPluginMsgs;
        // Message Processing Thread
        private Thread _processMsgQueueThread;
        private bool _processMsgQueueThreadStopping;

        public ServicePlugin()
        {
            _recvMsgQueueSync = new object();
            _recvMsgQueue = new Queue<object>();
        }
        /// <summary>
        /// This property is used to determine if the plugin needs to process messages from other plugins.
        /// </summary>
        public bool RequiresMessageFeed
        {
            get
            {
                return _recvPluginMsgs;
            }
            set
            {
                if (value != _recvPluginMsgs)
                {
                    if (value)
                    {
                        if (_asyncMsgRecvDel == null)
                        {
                            _asyncMsgRecvDel = new ServicePluginLoader.AsyncMessageReceiveDelegate(this.AsyncReceiveMessage);
                        }
                    }
                    else
                        _asyncMsgRecvDel = null;
                }
                _recvPluginMsgs = value;
            }
        }
        /// <summary>
        /// This property is a unique ID allocate by the ServicePluginLoader.
        /// </summary>
        public int PluginID
        {
            get
            {
                return _pluginID;
            }
            set
            {
                _pluginID = value;
            }
        }
        /// <summary>
        /// This property is the delegate to enqueue a message for processing by other plugins
        /// This delegate should be called when no responce is required for the message.
        /// </summary>
        public ServicePluginLoader.AsyncMessageSendDelegate AsyncMessageSendDel
        {
            get
            {
                return _asyncMsgSendDel;
            }
            set
            {
                _asyncMsgSendDel = value;
            }
        }
        /// <summary>
        /// This property is the delegate to receive and enqueue messages (objects) from other plugins.
        /// This delegate will async call the ProcessMessage(object message) on the inheriting object
        /// </summary>
        public ServicePluginLoader.AsyncMessageReceiveDelegate AsyncMessageReceive
        {
            get
            {
                return _asyncMsgRecvDel;
            }
            set
            {
                _asyncMsgRecvDel = value;
            }
        }
        /// <summary>
        /// This property is the delegate to send a message to the other plugins and wait for an responce
        /// This delegate should only be used when a responce is required for the message.
        /// </summary>
        public ServicePluginLoader.SendMessageDelegate SendMessageDel
        {
            get
            {
                return _sendMsgDel;
            }
            set
            {
                _sendMsgDel = value;
            }
        }
        
        /// <summary>
        /// This property is used for queuing messages for processing by the plugin
        /// </summary>
        public Queue<object> RecvMessageQueue
        {
            get
            {
                return _recvMsgQueue;
            }
            set
            {
                _recvMsgQueue = value;
            }
        }
        /// <summary>
        /// This method will check if the SendMessageDelegate delegate has been set on this object, if so then send the message to the other plugins and wait for a reply.
        /// The delegate will call ServicePluginLoader.SendMessage(int pluginId, object message) 
        /// This will then call the ProcessMessage function on each of the inherited objects and return a list of responces
        /// </summary>
        /// <param name="int pluginId">The ID of the plugin sending the message</param>
        /// <param name="object Message">The object to be sent to the other plugins</param>
        public List<SendMessageReponce> SendMessage<T>(int pluginId, T message)
        {
            List<SendMessageReponce> ret = new List<SendMessageReponce>();
            try
            {
                if (_sendMsgDel != null)
                    ret = _sendMsgDel(pluginId, message);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendMessage(int pluginId, object message)", ex);
            }
            return ret;
        }
        /// <summary>
        /// This method will check if the AsyncMessageSendDelegate delegate has been set on this object, if so then send the message to the other plugins.
        /// The delegate will call ServicePluginLoader.AsyncSendMessage(int pluginId, object message) 
        /// This will then call the ProcessMessage function on each of the inherited objects.
        /// This call will not wait for replies from the other plugins
        /// </summary>
        /// <param name="int pluginId">The ID of the plugin sending the message</param>
        /// <param name="object Message">The object to be sent to the other plugins</param>
        public void AsyncSendMessage(object Message)
        {
            try
            {
                if (_asyncMsgSendDel != null)
                    _asyncMsgSendDel.BeginInvoke(_pluginID, Message, null, null);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncSendMessage(object Message)", ex);
            }
        }

        /// <summary>
        /// When another plugin is a passing a message it is recieved on this interface via this.AsyncMessageReceive
        /// </summary>
        /// <param name="object Message">The object being sent to this plugin</param>
        private void AsyncReceiveMessage(object message)
        {
            try
            {
                lock (_recvMsgQueueSync)
                    _recvMsgQueue.Enqueue(message);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncReceiveMessage(object message)", ex);
            }
        }
        /// <summary>
        /// Initilaise this object from XML
        /// </summary>
        /// <param name="System.Xml.XmlNode section">The objects XML definition</param>
        public virtual object Create(System.Xml.XmlNode section)
        {
            return null;
        }
        /// <summary>
        /// Stop any threads and cleanup any resources assosiated with this object
        /// </summary>
        public virtual void Dispose()
        {
            try
            {
                RequiresMessageFeed = false;
                if (_recvMsgQueue != null)
                {
                    lock (_recvMsgQueueSync)
                    {
                        _recvMsgQueue.Clear();
                        _recvMsgQueue = null;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Dispose()", ex);
            }
        }
        /// <summary>
        /// Start the threads for this object
        /// </summary>
        public virtual void Start()
        {
            try
            {
                _processMsgQueueThreadStopping = false;
                _processMsgQueueThread = new Thread(new ThreadStart(ProcessMessageQueue));
                _processMsgQueueThread.Name = "Process Message Queue";
                _processMsgQueueThread.Start();
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Start()", ex);
            }
        }
        /// <summary>
        /// Stop the threads for this object
        /// </summary>
        public virtual void Stop()
        {
            try
            {
                // If the message processing thread is running, stop it.
                if (_processMsgQueueThread != null)
                {
                    // Setting this var to true will cause thread to drop out on the next loop
                    _processMsgQueueThreadStopping = true;
                    // Wait up to 1 second for the thread to end
                    int timeOut = 0;
                    while (_processMsgQueueThreadStopping && timeOut < 100)
                    {
                        Thread.Sleep(10);
                        timeOut++;
                    }
                    // Set thread to null.
                    _processMsgQueueThread = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Stop()", ex);
            }
        }
        /// <summary>
        /// Process a message from another plugin
        /// </summary>
        /// <param name="object message">The message from another plugin</param>
        public virtual SendMessageReponce ProcessMessage<T>(T message)
        {
            // This function must be overriden in the inheriting object.
            return null;
        }
        /// <summary>
        /// This is the thread code for processing the queue of messages from other plugins, this method will call
        /// the overridden ProcessMessage function on the inherited object for each item in the queue.
        /// </summary>
        private void ProcessMessageQueue()
        {
            try
            {
                // _processMsgQueueThreadStopping is used to stop the thread on the next loop
                while (!_processMsgQueueThreadStopping)
                {
                    object data = null;
                    lock (_recvMsgQueueSync)
                    {
                        if (_recvMsgQueue.Count > 0)
                            data = _recvMsgQueue.Dequeue();
                    }
                    if (data == null)
                        Thread.Sleep(100);
                    else
                    {
                        SendMessageReponce resp = this.ProcessMessage(data);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Stop()", ex);
            }
        }
    }
}

using System;

namespace MTData.Common.Plugin.Configuration
{
	/// <summary>
	/// This interface will be implemented by any Security settings provider that
	/// wants to provide security authentication to a plugin throught the plugin host 
	/// interface.
	/// </summary>
	public interface IPluginSecurityProvider
	{
		/// <summary>
		/// This is the id of the currently logged in user
		/// </summary>
		int CurrentUserID {get; }

		/// <summary>
		/// This method will return true if the user has the appropriate security level.
		/// </summary>
		/// <param name="roleName"></param>
		/// <returns></returns>
		bool UserHasRole(string roleName);
	}
}

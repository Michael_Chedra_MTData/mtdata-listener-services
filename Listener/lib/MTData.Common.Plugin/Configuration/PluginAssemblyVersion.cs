﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Common.Plugin.Configuration
{
    [Serializable]
    public class PluginAssemblyVersion
    {
        private int _id;
        private int _versionMajor;
        private int _versionMinor;

        public PluginAssemblyVersion(int id, int versionMajor, int versionMinor)
        {
            _id = id;
            _versionMajor = versionMajor;
            _versionMinor = versionMinor;
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int VersionMajor
        {
            get { return _versionMajor; }
            set { _versionMajor = value; }
        }

        public int VersionMinor
        {
            get { return _versionMinor; }
            set { _versionMinor = value; }
        }
    }
}

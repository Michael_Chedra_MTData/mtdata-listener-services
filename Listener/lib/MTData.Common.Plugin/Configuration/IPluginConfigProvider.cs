using System;

namespace MTData.Common.Plugin.Configuration
{
	/// <summary>
	/// This interface identifies the methods used to retrieve the list
	/// of available plugins, as well as methods to facilitiate their 
	/// download, preparation, and implementation
	/// </summary>
	public interface IPluginConfigProvider
	{
		/// <summary>
		/// Retrieves a list of the plugins available for the specified parentName
		/// of the specified type.
		/// </summary>
		/// <param name="pluginType"></param>
		/// <param name="parentName"></param>
		/// <returns></returns>
		PluginDefinition[] GetRegisteredPlugins(int pluginType, int versionMajor, int versionMinor, string parentName);

		/// <summary>
		/// This will check the plugin assembly, and download it if neccessary.
		/// </summary>
		/// <param name="definition"></param>
		/// <returns></returns>
		bool GetPluginAssembly(PluginDefinition definition, string targetAssemblyDirectory);
        bool GetPluginAssembly(PluginDefinition definition, string targetAssemblyDirectory, string serverCacheName);
	}
}

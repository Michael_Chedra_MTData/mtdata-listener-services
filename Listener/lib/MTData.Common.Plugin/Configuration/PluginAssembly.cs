﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MTData.Common.Plugin.Configuration
{

    /// <summary>
    /// This is a definition of the assembly
    /// </summary>
    [Serializable]
    public class PluginAssembly
    {
        private int _id = 0;
        private string _codeBase = null;
        private string _assemblyName = null;
        private int _versionMajor = 0;
        private int _versionMinor = 0;
        private DateTime _assemblyDateUTC = DateTime.MinValue;
        private byte[] _assemblyData = null;
        private PluginAssembly[] _dependancies = new PluginAssembly[0];

        public PluginAssembly()
        {

        }

        public PluginAssembly(int id, string codeBase, string assemblyName, int versionMajor, int versionMinor, DateTime assemblyDateUTC, byte[] assemblyData)
        {
            _id = id;
            _versionMajor = versionMajor;
            _versionMinor = versionMinor;

            _codeBase = codeBase;
            _assemblyName = assemblyName;
            _assemblyDateUTC = assemblyDateUTC;
            _assemblyData = assemblyData;
        }

        public int ID { get { return _id; } set { _id = value; } }
        public int VersionMajor { get { return _versionMajor; } set { _versionMajor = value; } }
        public int VersionMinor { get { return _versionMinor; } set { _versionMinor = value; } }
        public string CodeBase { get { return _codeBase; } set { _codeBase = value; } }
        public string AssemblyName { get { return _assemblyName; } set { _assemblyName = value; } }
        public DateTime AssemblyDateUTC { get { return _assemblyDateUTC; } set { _assemblyDateUTC = value; } }
        public byte[] AssemblyData { get { return _assemblyData; } set { _assemblyData = value; } }
        public PluginAssembly[] Dependancies { get { return _dependancies; } set { _dependancies = value; } }

        public override string ToString()
        {
            return string.Format("{0} ({1}.{2}) {3}", _id, _versionMajor, _versionMinor, _assemblyName);
        }

        public override bool Equals(object obj)
        {
            PluginAssembly compare = obj as PluginAssembly;
            if (compare == null)
                return false;

            return (compare.ID == ID)
                && (compare.VersionMajor == VersionMajor)
                && (compare.VersionMinor == VersionMinor)
                && (compare.AssemblyName == AssemblyName)
                && (compare.AssemblyDateUTC == AssemblyDateUTC);
        }

        public override int GetHashCode()
        {
            return ID.GetHashCode() + VersionMajor.GetHashCode() + VersionMinor.GetHashCode() + (AssemblyName != null ? AssemblyName.GetHashCode() : 0) + AssemblyDateUTC.GetHashCode();
        }

    }
}

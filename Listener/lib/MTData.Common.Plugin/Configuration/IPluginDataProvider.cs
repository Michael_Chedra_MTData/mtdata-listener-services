using System;
using System.Data;

namespace MTData.Common.Plugin.Configuration
{
	public delegate void PluginDataChangedDelegate(string datasource, object[] paremeters, DataSet data);

	/// <summary>
	///	This interface identifies methdos that will allow a 
	///	plugin to interract with the standard database 
	///	through the plugin host
	/// </summary>
	public interface IPluginDataProvider
	{
		/// <summary>
		///	allows access to methods in the standard data provider
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		PluginMethodResult GetPluginData(string dataSource, object[] parameters, long auditUserID = 0);

		/// <summary>
		/// Allows access to the standard data provider
		/// </summary>
		/// <param name="dataSource"></param>
		/// <param name="parameters"></param>
		/// <param name="updates"></param>
		/// <returns></returns>
		PluginMethodResult UpdatePluginData(string dataSource, object[] parameters, DataSet updates, long auditUserID = 0);

        /// <summary>
        /// Allows user to fire the PluginDataChanged event to pass messages to other plugins.
        /// </summary>
        /// <param name="dataSource"></param>
        /// <param name="parameters"></param>
        /// <param name="data"></param>
        /// <returns></returns>
	    //void PluginOnChange(string datasource, object[] parameters, DataSet data);

		/// <summary>
		/// Fired when ever there is a broadcast of data required from the plugin.
		/// </summary>
		event PluginDataChangedDelegate PluginDataChanged;
	}
}

using System;
using System.Collections;

namespace MTData.Common.Plugin.Configuration
{
    /// <summary>
    /// This class provides a definitiion of the plugin.
    /// </summary>
    [Serializable]
    public class PluginDefinition
    {
        private int _moduleID = -1;
        private int _pluginID = -1;
        private int _pluginVersionMajor = 0;
        private int _pluginVersionMinor = 0;
        private int _buildNumber = 0;
        private int _pluginType = -1;
        private int _state = 0;

        private DateTime _creationDateUTC = DateTime.MinValue;
        private DateTime _modifiedDateUTC = DateTime.MinValue;

        private string _name = null;
        private string _typeName = null;
        private string _securityRole = null;
        private string _pluginParent = null;

        private PluginAssembly _assembly = null;
        private byte[] _configData = null;

        public PluginDefinition()
        {

        }

        public PluginDefinition(int moduleID, int pluginID, int pluginVersionMajor, int pluginVersionMinor, int buildNumber, int pluginType, int state, DateTime creationDateUTC, DateTime modifiedDateUTC, string name, string typeName, string securityRole, PluginAssembly assembly, byte[] configData)
        {
            _moduleID = moduleID;
            _pluginID = pluginID;
            _pluginVersionMajor = pluginVersionMajor;
            _pluginVersionMinor = pluginVersionMinor;
            _buildNumber = buildNumber;
            _state = state;

            _creationDateUTC = creationDateUTC;
            _modifiedDateUTC = modifiedDateUTC;

            _pluginType = pluginType;
            _name = name;
            _typeName = typeName;
            _securityRole = securityRole;
            _assembly = assembly;
            _configData = configData;
        }

        public int ModuleID { get { return _moduleID; } set { _moduleID = value; } }
        public int PluginID { get { return _pluginID; } set { _pluginID = value; } }
        public int PluginVersionMajor { get { return _pluginVersionMajor; } set { _pluginVersionMajor = value; } }
        public int PluginVersionMinor { get { return _pluginVersionMinor; } set { _pluginVersionMinor = value; } }
        public int BuildNumber { get { return _buildNumber; } set { _buildNumber = value; } }
        public int PluginType { get { return _pluginType; } set { _pluginType = value; } }
        public int State { get { return _state; } set { _state = value; } }

        public DateTime CreationDateUTC { get { return _creationDateUTC; } set { _creationDateUTC = value; } }
        public DateTime ModifiedDateUTC { get { return _modifiedDateUTC; } set { _modifiedDateUTC = value; } }

        public string Name { get { return _name; } set { _name = value; } }
        public string TypeName { get { return _typeName; } set { _typeName = value; } }
        public string SecurityRole { get { return _securityRole; } set { _securityRole = value; } }
        public string PluginParent { get { return _pluginParent; } set { _pluginParent = value; } }

        public PluginAssembly Assembly { get { return _assembly; } set { _assembly = value; } }
        public byte[] ConfigData { get { return _configData; } set { _configData = value; } }

    }
}

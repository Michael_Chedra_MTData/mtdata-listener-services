﻿namespace MTData.Common.Plugin.Configuration
{
    public interface IPluginObjectProvider
    {
        object GetPluginObject(string dataSet, object[] parameters);
    }
}

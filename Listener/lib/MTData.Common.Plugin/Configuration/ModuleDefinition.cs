using System;

namespace MTData.Common.Plugin.Configuration
{
	/// <summary>
	/// This class identifies a module.
	/// </summary>
	public class ModuleDefinition
	{
		private int _moduleID = 0;
		private string _name = null;
		private string _description = null;

		public ModuleDefinition()
		{
			
		}

		public ModuleDefinition(int moduleID, string name, string description)
		{
			_moduleID = moduleID;
			_name = name;
			_description = description;
		}

		public int ModuleID {get{ return _moduleID; } set{ _moduleID = value; }}
		public string Name { get{ return _name; } set{ _name = value;}} 
		public string Description { get{ return _description; } set{ _description = value; }}

		public override string ToString()
		{
			return _name;
		}

	}
}

using System;
using System.Collections;
using System.Data;

namespace MTData.Common.Plugin.Configuration
{
	/// <summary>
	/// Summary description for PluginMethodResult.
	/// </summary>
	[Serializable]
	public class PluginMethodResult
	{
		[Serializable]
		public class RowResult
		{
			private object[]	_rowIdentifier;
			private int			_messageId;
			private string		_message;

			public RowResult()
			{
			}

			public RowResult(object rowIdentifier, int messageId, string message)
			{
				_rowIdentifier	= new object[] { rowIdentifier };
				_messageId		= messageId;
				_message		= message;
			}

			public RowResult(object[] rowIdentifier, int messageId, string message)
			{
				_rowIdentifier	= rowIdentifier;
				_messageId		= messageId;
				_message		= message;
			}

			public object[] RowIdentifier
			{
				get { return _rowIdentifier; }
				set { _rowIdentifier = value; }
			}

			public int MessageId
			{
				get { return _messageId; }
				set { _messageId = value; }
			}

			public string Message
			{
				get { return _message; }
				set { _message = value; }
			}
		}

		private DataSet		_dataSet;
		private ArrayList	_information;
		private ArrayList	_warnings;
		private ArrayList	_errors;

		public PluginMethodResult()
		{
		}

		public DataSet DataSet
		{
			get { return _dataSet; }
			set { _dataSet = value; }
		}

		public RowResult[] Information
		{
			get
			{
				if (_information == null)
					return null;
				return (RowResult[]) _information.ToArray(typeof(RowResult));
			}
			set
			{
				throw new NotSupportedException("Use AddInformation method");
			}
		}

		public void AddInformation(RowResult information)
		{
			if (_information == null)
				_information = new ArrayList();
			_information.Add(information);
		}

		public void AddInformation(string information)
		{
			if (_information == null)
				_information = new ArrayList();
			_information.Add(new RowResult(null, 0, information));
		}

		public RowResult[] Warnings
		{
			get
			{
				if (_warnings == null)
					return null;
				return (RowResult[]) _warnings.ToArray(typeof(RowResult));
			}
			set
			{
				throw new NotSupportedException("Use AddWarning method");
			}
		}
			
		public void AddWarning(RowResult warning)
		{
			if (_warnings == null)
				_warnings = new ArrayList();
			_warnings.Add(warning);
		}

		public void AddWarning(string warning)
		{
			if (_warnings == null)
				_warnings = new ArrayList();
			_warnings.Add(new RowResult(null, 0, warning));
		}

		public RowResult[] Errors
		{
			get
			{
				if (_errors == null)
					return null;
				return (RowResult[]) _errors.ToArray(typeof(RowResult));
			}
			set
			{
				throw new NotSupportedException("Use AddError method");
			}
		}

		public void AddError(RowResult error)
		{
			if (_errors == null)
				_errors = new ArrayList();
			_errors.Add(error);
		}

		public void AddError(string error)
		{
			if (_errors == null)
				_errors = new ArrayList();
			_errors.Add(new RowResult(null, 0, error));
		}
	}
}

using System;

namespace MTData.Common.Plugin.Configuration
{
	/// <summary>
	/// This interface will provide the details to install 
	/// a plugin into the system.
	/// </summary>
	public interface IPluginInstallationProvider
	{
		/// <summary>
		/// Allow the update of a user role 
		/// </summary>
		/// <param name="role"></param>
		void SetUserRoleDefinition(string role);

		/// <summary>
		/// Store off the plugin information.
		/// </summary>
		/// <param name="definition"></param>
		void SetPluginDefinition(PluginDefinition definition);
	}
}

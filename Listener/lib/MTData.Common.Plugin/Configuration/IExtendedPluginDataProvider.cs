using System;
using System.Data;

namespace MTData.Common.Plugin.Configuration
{
	/// <summary>
	/// Specialized version of IPluginDataProvider that allows data to be manipulated in context of a specific User Id
	/// </summary>
	public interface IExtendedPluginDataProvider
	{
		/// <summary>
		/// get data in a specific user context from a data provider
		/// </summary>
		/// <param name="userId"></param>
		/// <param name="dataSource"></param>
		/// <param name="parameters"></param>
		/// <returns></returns>
		PluginMethodResult GetPluginData(string dataSource, object[] parameters, long auditUserID);

        /// <summary>
        /// update data in a specific user context to a data provider
        /// </summary>
        /// <param name="auditUserID"></param>
        /// <param name="dataSource"></param>
        /// <param name="parameters"></param>
        /// <param name="updates"></param>
        /// <returns></returns>
        PluginMethodResult UpdatePluginData(string dataSource, object[] parameters, DataSet updates, long auditUserID);
	}

}

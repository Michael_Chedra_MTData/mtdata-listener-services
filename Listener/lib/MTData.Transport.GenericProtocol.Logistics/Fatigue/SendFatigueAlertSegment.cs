﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Fatigue
{
    public class SendFatigueAlertSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendFatigueAlert;

        public List<FatigueAlert> Alerts { get; set; } = new List<FatigueAlert>();

        public SendFatigueAlertSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SendFatigueAlertSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, Alerts?.Count ?? 0);

                if (Alerts != null)
                {
                    foreach (var a in Alerts)
                    {
                        a.Encode(stream);
                    }
                }

                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);
            var alerts = new List<FatigueAlert>();
            FatigueAlert a;

            for (int i = 0; i < count; i++)
            {
                a = new FatigueAlert();
                a.Decode(Data, ref index);
                alerts.Add(a);
            }

            this.Alerts = alerts;
        }

    }
}

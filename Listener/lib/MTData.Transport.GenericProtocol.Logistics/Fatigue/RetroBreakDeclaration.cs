﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Fatigue
{
    [System.Diagnostics.DebuggerDisplay("StartTime: {StartTime.ToString()}, EndTime: {EndTime.ToString()}, BreakType: {BreakType}")]
    public class RetroBreakDeclaration : IPacketEncode
    {
        public int VersionNumber { get; } = 1;

        public DateTime StartTime { get; set; }

        public DateTime EndTime { get; set; }

        public int Duration { get; set; }

        public BreakType BreakType { get; set; }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            Duration = BaseLayer.Read4ByteInteger(data, ref index);
            StartTime = BaseLayer.ReadDateTime(data, ref index);
            EndTime = BaseLayer.ReadDateTime(data, ref index);
            BreakType = (BreakType)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, Duration);
                BaseLayer.WriteDateTime(local, StartTime);
                BaseLayer.WriteDateTime(local, EndTime);
                local.WriteByte((byte)BreakType);

                //write version number
                BaseLayer.WriteMoreFlag(stream, VersionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }

        }
    }

}

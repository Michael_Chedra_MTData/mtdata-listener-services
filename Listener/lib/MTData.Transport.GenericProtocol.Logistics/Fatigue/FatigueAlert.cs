﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Fatigue
{
    [System.Diagnostics.DebuggerDisplay("PeriodTypeID: {PeriodTypeID}, BreakDueTime: {BreakDueTime.ToString()}, BreakLength: {BreakLength}, AlertListItemID: {AlertListItemID}, AlertListVersion: {AlertListVersion}")]
    public class FatigueAlert : IPacketEncode
    {
        public int VersionNumber { get; } = 1;

        public int AlertListItemID { get; set; }

        public int AlertListVersion { get; set; }

        public DateTime BreakDueTime { get; set; }

        public int BreakLength { get; set; }

        public int PeriodTypeID { get; set; }


        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            AlertListItemID = BaseLayer.Read4ByteInteger(data, ref index);
            AlertListVersion = BaseLayer.Read4ByteInteger(data, ref index);
            BreakDueTime = BaseLayer.ReadDateTime(data, ref index);
            BreakLength = BaseLayer.ReadMoreFlag(data, ref index);
            PeriodTypeID = BaseLayer.ReadMoreFlag(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, AlertListItemID);
                BaseLayer.Write4ByteInteger(local, AlertListVersion);
                BaseLayer.WriteDateTime(local, BreakDueTime);
                BaseLayer.WriteMoreFlag(local, BreakLength);
                BaseLayer.WriteMoreFlag(local, PeriodTypeID);

                //write version number
                BaseLayer.WriteMoreFlag(stream, VersionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// a login packet from a unit
    /// </summary>
    public class LoginSegment : SegmentLayer
    {
        #region private fields
        private int _driverId;
        private int _pin;
        private string _serial;
        private VersionInfo _winCEShell;
        private int _hardwareType;
        private VersionInfo _hardwareVersion;
        private int _odometer;
        private int _totalFuel;
        private int _palletsCount;
        private ListId _fleetPhoneBook;
        private ListId _driverPhoneBook;
        private ListId _divisionsList;
        private ListId _delayReasons;
        private ListId _rejectReasons;
        private ListId _pallets;
        private ListId _trailers;
        private ListId _vehicleUsages;
        private ListId _predefinedMessages;
        private ListId _preTripQuestions;
        private ListId _postTripQuestions;
        private ListId _pickupQuestions;
        private ListId _deliveryQuestions;
        private ListId _fuelStations;
        private ListId _fleetAddressBook;
        private ListId _driverAddressBook;
        private ListId _fleetEmailAddressBook;
        private ListId _driverEmailAddressBook;
        private ListId _jobTemplates;
        private ListId _massSchemes;
        private ListId _defectTemplate;
        private ListId _incientTemplate;
        private bool _terminalLogin;
        private VersionInfo _shellConfigVersion;
        private bool _useTripTemplate;
        private ListId _preTripTemplate;
        private ListId _postTripTemplate;
        private List<ListId> _cachedLists;
        private int _lastAssociationId;
        private ListId _fuelTypes;
        #endregion

        #region properties
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public int Pin
        {
            get { return _pin; }
            set { _pin = value; }
        }
        public string Serial
        {
            get { return _serial; }
            set { _serial = value; }
        }
        public VersionInfo WinCEShell
        {
            get { return _winCEShell; }
            set { _winCEShell = value; }
        }
        public int HardwareType
        {
            get { return _hardwareType; }
            set { _hardwareType = value; }
        }
        public VersionInfo HardwareVersion
        {
            get { return _hardwareVersion; }
            set { _hardwareVersion = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        public int PalletsCount
        {
            get { return _palletsCount; }
            set { _palletsCount = value; }
        }
        public ListId FleetPhoneBook
        {
            get { return _fleetPhoneBook; }
            set { _fleetPhoneBook = value; }
        }
        public ListId DriverPhoneBook
        {
            get { return _driverPhoneBook; }
            set { _driverPhoneBook = value; }
        }
        public ListId DivisionsList
        {
            get { return _divisionsList; }
            set { _divisionsList = value; }
        }
        public ListId DelayReasons
        {
            get { return _delayReasons; }
            set { _delayReasons = value; }
        }
        public ListId RejectReasons
        {
            get { return _rejectReasons; }
            set { _rejectReasons = value; }
        }
        public ListId Pallets
        {
            get { return _pallets; }
            set { _pallets = value; }
        }
        public ListId Trailers
        {
            get { return _trailers; }
            set { _trailers = value; }
        }
        public ListId VehicleUsages
        {
            get { return _vehicleUsages; }
            set { _vehicleUsages = value; }
        }
        public ListId PredefinedMessages
        {
            get { return _predefinedMessages; }
            set { _predefinedMessages = value; }
        }
        public ListId PreTripQuestions
        {
            get { return _preTripQuestions; }
            set { _preTripQuestions = value; }
        }
        public ListId PostTripQuestions
        {
            get { return _postTripQuestions; }
            set { _postTripQuestions = value; }
        }
        public ListId PickupQuestions
        {
            get { return _pickupQuestions; }
            set { _pickupQuestions = value; }
        }
        public ListId DeliveryQuestions
        {
            get { return _deliveryQuestions; }
            set { _deliveryQuestions = value; }
        }
        public ListId FuelStations
        {
            get { return _fuelStations; }
            set { _fuelStations = value; }
        }
        public ListId FleetAddressBook
        {
            get { return _fleetAddressBook; }
            set { _fleetAddressBook = value; }
        }
        public ListId DriverAddressBook
        {
            get { return _driverAddressBook; }
            set { _driverAddressBook = value; }
        }
        public ListId FleetEmailAddressBook
        {
            get { return _fleetEmailAddressBook; }
            set { _fleetEmailAddressBook = value; }
        }
        public ListId DriverEmailAddressBook
        {
            get { return _driverEmailAddressBook; }
            set { _driverEmailAddressBook = value; }
        }
        public ListId JobTemplates
        {
            get { return _jobTemplates; }
            set { _jobTemplates = value; }
        }
        public ListId MassSchemes
        {
            get { return _massSchemes; }
            set { _massSchemes = value; }
        }
        public bool TerminalLogin
        {
            get { return _terminalLogin; }
            set { _terminalLogin = value; }
        }
        public VersionInfo ShellConfigVersion
        {
            get { return _shellConfigVersion; }
            set { _shellConfigVersion = value; }
        }

        public ListId DefectTemplate
        {
            get { return _defectTemplate; }
            set { _defectTemplate = value; }
        }
        
        public ListId IncidentTemplate
        {
            get { return _incientTemplate; }
            set { _incientTemplate = value; }
        }

        public bool UseTripTemplate
        {
            get { return _useTripTemplate; }
            set { _useTripTemplate = value; }
        }

        public ListId PreTripTemplate
        {
            get { return _preTripTemplate; }
            set { _preTripTemplate = value; }
        }

        public ListId PostTripTemplate
        {
            get { return _postTripTemplate; }
            set { _postTripTemplate = value; }
        }

        /// <summary>
        /// Lists cached on Talon
        /// </summary>
        public List<ListId> CachedLists
        {
            get { return _cachedLists; }
            set { _cachedLists = value; }
        }

        /// <summary>
        /// The last association TerminalCreationID generated on this unit
        /// </summary>
        public int LastAssociationId
        {
            get { return _lastAssociationId; }
            set { _lastAssociationId = value; }
        }

        /// <summary>
        /// The last association FuelTypeListId generated on this unit
        /// </summary>
        public ListId FuelTypes
        {
            get { return _fuelTypes; }
            set { _fuelTypes = value; }
        }

        #endregion

        #region constructor
        public LoginSegment()
        {
            Version = 13;
            Type = (int)LogisticsSegmentTypes.Login;
            InitialiseObjects();
        }
        public LoginSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.Login)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.Login, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.Login;
            InitialiseObjects();
            Data = segment.Data;
            DecodeData();
        }
        private void InitialiseObjects()
        {
            _winCEShell = new VersionInfo();
            _hardwareVersion = new VersionInfo();
            _divisionsList = new ListId();
            _delayReasons = new ListId();
            _rejectReasons = new ListId();
            _pallets = new ListId();
            _trailers = new ListId();
            _vehicleUsages = new ListId();
            _predefinedMessages = new ListId();
            _postTripQuestions = new ListId();
            _pickupQuestions = new ListId();
            _deliveryQuestions = new ListId();
            _fuelStations = new ListId();
            _preTripQuestions = new ListId();
            _fleetAddressBook = new ListId();
            _driverAddressBook = new ListId();
            _fleetEmailAddressBook = new ListId();
            _driverEmailAddressBook = new ListId();
            _fleetPhoneBook = new ListId();
            _driverPhoneBook = new ListId();
            _jobTemplates = new ListId();
            _massSchemes = new ListId();
            _shellConfigVersion = new VersionInfo();
            _defectTemplate = new ListId();
            _incientTemplate = new ListId();
            _useTripTemplate = false;
            _preTripTemplate = new ListId();
            _postTripTemplate = new ListId();
            _cachedLists = new List<ListId>();
            _fuelTypes = new ListId();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _driverId);
                Write2ByteInteger(stream, _pin);
                if (Version < 4)
                {
                    int serial = 0;
                    try
                    {
                        serial = Convert.ToInt32(_serial);
                    }
                    catch { }
                    Write4ByteInteger(stream, serial);
                }
                else
                {
                    WriteString(stream, _serial, 0x12);
                }
                _winCEShell.Encode(stream);
                Write4ByteInteger(stream, _hardwareType);
                _hardwareVersion.Encode(stream);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                Write2ByteInteger(stream, _palletsCount);
                if (Version == 1)
                {
                    Write4ByteInteger(stream, _fleetPhoneBook.Version);
                    Write4ByteInteger(stream, _driverPhoneBook.Version);
                }
                else
                {
                    _fleetPhoneBook.Encode(stream);
                    _driverPhoneBook.Encode(stream);
                }

                _divisionsList.Encode(stream);
                _delayReasons.Encode(stream);
                _rejectReasons.Encode(stream);
                _pallets.Encode(stream);
                _trailers.Encode(stream);
                _vehicleUsages.Encode(stream);
                _predefinedMessages.Encode(stream);
                _preTripQuestions.Encode(stream);
                _postTripQuestions.Encode(stream);
                _pickupQuestions.Encode(stream);
                _deliveryQuestions.Encode(stream);
                _fuelStations.Encode(stream);

                if (Version == 1)
                {
                    Write4ByteInteger(stream, _fleetAddressBook.Version);
                    Write4ByteInteger(stream, _driverAddressBook.Version);
                    Write4ByteInteger(stream, _fleetEmailAddressBook.Version);
                    Write4ByteInteger(stream, _driverEmailAddressBook.Version);
                }
                else
                {
                    _fleetAddressBook.Encode(stream);
                    _driverAddressBook.Encode(stream);
                    _fleetEmailAddressBook.Encode(stream);
                    _driverEmailAddressBook.Encode(stream);
                }

                if (Version >= 3)
                {
                    _jobTemplates.Encode(stream);
                    if (Version >= 5)
                    {
                        WriteBool(stream, _terminalLogin);
                    }
                }

                if (Version >= 7)
                {
                    _shellConfigVersion.Encode(stream);
                }

                if (Version >= 8)
                {
                    Write4ByteInteger(stream, _shellConfigVersion.Major); //Write 4 bytes version number
                    Write4ByteInteger(stream, _shellConfigVersion.Minor);
                }
                if (Version >= 9)
                {
                    _massSchemes.Encode(stream);
                }
                // Added version 10
                _defectTemplate.Encode(stream);
                _incientTemplate.Encode(stream);

                // Added version 11
                WriteBool(stream, _useTripTemplate);
                _preTripTemplate.Encode(stream);
                _postTripTemplate.Encode(stream);
                if (_cachedLists != null)
                {
                    WriteMoreFlag(stream, _cachedLists.Count);
                    foreach (var listId in _cachedLists)
                    {
                        listId.Encode(stream);
                    }
                }
                else
                {
                    WriteMoreFlag(stream, 0);
                }
                // Added version 12
                Write4ByteInteger(stream, _lastAssociationId);

                // Added version 13
                _fuelTypes.Encode(stream);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;

            _driverId = Read4ByteInteger(Data, ref index);
            _pin = Read2ByteInteger(Data, ref index);
            if (Version < 4)
            {
                _serial = Read4ByteInteger(Data, ref index).ToString();
            }
            else
            {
                _serial = ReadString(Data, ref index, 0x12);
            }
            _winCEShell.Decode(Data, ref index);
            _hardwareType = Read4ByteInteger(Data, ref index);
            _hardwareVersion.Decode(Data, ref index);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            _palletsCount = Read2ByteInteger(Data, ref index);
            if (Version == 1)
            {
                _fleetPhoneBook.Id = 0;
                _driverPhoneBook.Id = 0;
                _fleetPhoneBook.Version = Read4ByteInteger(Data, ref index);
                _driverPhoneBook.Version = Read4ByteInteger(Data, ref index);
            }
            else
            {
                _fleetPhoneBook.Decode(Data, ref index);
                _driverPhoneBook.Decode(Data, ref index);
            }

            _divisionsList.Decode(Data, ref index);
            _delayReasons.Decode(Data, ref index);
            _rejectReasons.Decode(Data, ref index);
            _pallets.Decode(Data, ref index);
            _trailers.Decode(Data, ref index);
            _vehicleUsages.Decode(Data, ref index);
            _predefinedMessages.Decode(Data, ref index);
            _preTripQuestions.Decode(Data, ref index);
            _postTripQuestions.Decode(Data, ref index);
            _pickupQuestions.Decode(Data, ref index);
            _deliveryQuestions.Decode(Data, ref index);
            _fuelStations.Decode(Data, ref index);

            if (Version == 1)
            {
                _fleetAddressBook.Id = 0;
                _driverAddressBook.Id = 0;
                _fleetEmailAddressBook.Id = 0;
                _driverEmailAddressBook.Id = 0;
                _fleetAddressBook.Version = Read4ByteInteger(Data, ref index);
                _driverAddressBook.Version = Read4ByteInteger(Data, ref index);
                _fleetEmailAddressBook.Version = Read4ByteInteger(Data, ref index);
                _driverEmailAddressBook.Version = Read4ByteInteger(Data, ref index);
            }
            else
            {
                _fleetAddressBook.Decode(Data, ref index);
                _driverAddressBook.Decode(Data, ref index);
                _fleetEmailAddressBook.Decode(Data, ref index);
                _driverEmailAddressBook.Decode(Data, ref index);
            }

            _terminalLogin = true;
            if (Version >= 3)
            {
                _jobTemplates.Decode(Data, ref index);
                if (Version >= 5)
                {
                    _terminalLogin = ReadBool(Data, ref index);
                }
            }
            if (Version >= 7)
            {
                _shellConfigVersion.Decode(Data, ref index);
            }
            else
            {
                _shellConfigVersion = new VersionInfo(-1, -1);
            }

            if (Version >= 8)
            {
                if (_shellConfigVersion == null)
                {
                    _shellConfigVersion = new VersionInfo();
                }
                _shellConfigVersion.Major = Read4ByteInteger(Data, ref index);
                _shellConfigVersion.Minor = Read4ByteInteger(Data, ref index);
            }

            if (Version >= 9)
            {
                _massSchemes.Decode(Data, ref index);
            }

            if (Version >= 10)
            {
                _defectTemplate.Decode(Data, ref index);
                _incientTemplate.Decode(Data, ref index);
            }

            if (Version >= 11)
            {
                _useTripTemplate = ReadBool(Data, ref index);
                _preTripTemplate.Decode(Data, ref index);
                _postTripTemplate.Decode(Data, ref index);
                int count = ReadMoreFlag(Data, ref index);
                _cachedLists = new List<ListId>();
                for (int i = 0; i < count; i++)
                {
                    var listId = new ListId();
                    listId.Decode(Data, ref index);
                    _cachedLists.Add(listId);
                }
            }
            else
            {
                _cachedLists = new List<ListId>();
            }
            if (Version >= 12)
            {
                _lastAssociationId = Read4ByteInteger(Data, ref index);
            }
            else
            {
                _lastAssociationId = 0;
            }

            if (Version >= 13)
            {
                _fuelTypes.Decode(Data, ref index);
            }
        }
        #endregion
    }
}

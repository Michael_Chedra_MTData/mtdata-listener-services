using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// this segment represents an 5080 asset packet
    /// </summary>
    public class Asset5080Segment : SegmentLayer
    {
        #region private fields
        private VersionInfo _winCEShell;
        private string _customerBuild;
        private VersionInfo _metroviewDll;
        private VersionInfo _mapData;
        private string _mapDataName;
        private VersionInfo _shellExe;
        private VersionInfo _apiDll;
        private VersionInfo _coreDll;
        private VersionInfo _coreUiDll;
        private VersionInfo _guiDll;
        private VersionInfo _iapDll;
        private VersionInfo _jobsDll;
        private VersionInfo _mapDll;
        private VersionInfo _preTripDll;
        private VersionInfo _resourcesDll;
        private VersionInfo _screenDll;
        private VersionInfo _routeDll;
        private VersionInfo _webServiceDll;
        private VersionInfo _sysDll;
        private VersionInfo _phoneDll;
        private VersionInfo _downloadUpdatesDll;
        private VersionInfo _paltfromCeDll;
        private VersionInfo _windowsOS;
        private VersionInfo _shellConfig;
        private string _shellConfigName;
        private string _windowsBuild;
        private VersionInfo _windowsBuildVersion;

        private int _storageCardCapacity;
        private int _storageCardFreeSpace;
        private int _mccQueue;
        private int _battery;
        private string _androidDeviceID;
        #endregion

        #region properties
        public VersionInfo WinCEShell
        {
            get { return _winCEShell; }
            set { _winCEShell = value; }
        }
        public string CustomerBuild
        {
            get { return _customerBuild; }
            set { _customerBuild = value; }
        }
        public VersionInfo MetroviewDll
        {
            get { return _metroviewDll; }
            set { _metroviewDll = value; }
        }
        public VersionInfo MapData
        {
            get { return _mapData; }
            set { _mapData = value; }
        }
        public string MapDataName
        {
            get { return _mapDataName; }
            set { _mapDataName = value; }
        }
        public VersionInfo ShellExe
        {
            get { return _shellExe; }
            set { _shellExe = value; }
        }
        public VersionInfo ApiDll
        {
            get { return _apiDll; }
            set { _apiDll = value; }
        }
        public VersionInfo CoreDll
        {
            get { return _coreDll; }
            set { _coreDll = value; }
        }
        public VersionInfo CoreUiDll
        {
            get { return _coreUiDll; }
            set { _coreUiDll = value; }
        }
        public VersionInfo GuiDll
        {
            get { return _guiDll; }
            set { _guiDll = value; }
        }
        public VersionInfo IapDll
        {
            get { return _iapDll; }
            set { _iapDll = value; }
        }
        public VersionInfo JobsDll
        {
            get { return _jobsDll; }
            set { _jobsDll = value; }
        }
        public VersionInfo MapDll
        {
            get { return _mapDll; }
            set { _mapDll = value; }
        }
        public VersionInfo PreTripDll
        {
            get { return _preTripDll; }
            set { _preTripDll = value; }
        }
        public VersionInfo ResourcesDll
        {
            get { return _resourcesDll; }
            set { _resourcesDll = value; }
        }
        public VersionInfo ScreenDll
        {
            get { return _screenDll; }
            set { _screenDll = value; }
        }
        public VersionInfo RouteDll
        {
            get { return _routeDll; }
            set { _routeDll = value; }
        }
        public VersionInfo WebServiceDll
        {
            get { return _webServiceDll; }
            set { _webServiceDll = value; }
        }
        public VersionInfo SysDll
        {
            get { return _sysDll; }
            set { _sysDll = value; }
        }
        public VersionInfo PhoneDll
        {
            get { return _phoneDll; }
            set { _phoneDll = value; }
        }
        public VersionInfo DownloadUpdatesDll
        {
            get { return _downloadUpdatesDll; }
            set { _downloadUpdatesDll = value; }
        }
        public VersionInfo PaltfromCeDll
        {
            get { return _paltfromCeDll; }
            set { _paltfromCeDll = value; }
        }
        public VersionInfo WindowsOS
        {
            get { return _windowsOS; }
            set { _windowsOS = value; }
        }
        public int StorageCardCapacity
        {
            get { return _storageCardCapacity; }
            set { _storageCardCapacity = value; }
        }
        public int StorageCardFreeSpace
        {
            get { return _storageCardFreeSpace; }
            set { _storageCardFreeSpace = value; }
        }
        public int MccQueue
        {
            get { return _mccQueue; }
            set { _mccQueue = value; }
        }
        public int Battery
        {
            get { return _battery; }
            set { _battery = value; }
        }
        public VersionInfo ShellConfig
        {
            get { return _shellConfig; }
            set { _shellConfig = value; }
        }
        public string ShellConfigName
        {
            get { return _shellConfigName; }
            set { _shellConfigName = value; }
        }

        public string WindowsBuild
        {
            get { return _windowsBuild; }
            set { _windowsBuild = value; }
        }

        public VersionInfo WindowsBuildVersion
        {
            get { return _windowsBuildVersion; }
            set { _windowsBuildVersion = value; }
        }

        public string AndroidDeviceID
        {
            get { return _androidDeviceID; }
            set { _androidDeviceID = value; }
        }

        #endregion

        #region constructor
        public Asset5080Segment()
        {
            Version = 4;
            Type = (int)LogisticsSegmentTypes.Asset5080;
            InitialiseVersionInfos();
        }
        public Asset5080Segment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.Asset5080)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.Asset5080, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.Asset5080;
            InitialiseVersionInfos();
            Data = segment.Data;
            DecodeData();
        }

        private void InitialiseVersionInfos()
        {
            _winCEShell = new VersionInfo();
            _metroviewDll = new VersionInfo();
            _mapData = new VersionInfo();
            _shellExe = new VersionInfo();
            _apiDll = new VersionInfo();
            _coreDll = new VersionInfo();
            _coreUiDll = new VersionInfo();
            _guiDll = new VersionInfo();
            _iapDll = new VersionInfo();
            _jobsDll = new VersionInfo();
            _mapDll = new VersionInfo();
            _preTripDll = new VersionInfo();
            _resourcesDll = new VersionInfo();
            _screenDll = new VersionInfo();
            _routeDll = new VersionInfo();
            _webServiceDll = new VersionInfo();
            _sysDll = new VersionInfo();
            _phoneDll = new VersionInfo();
            _downloadUpdatesDll = new VersionInfo();
            _paltfromCeDll = new VersionInfo();
            _windowsOS = new VersionInfo();
            _shellConfig = new VersionInfo();
            _windowsBuildVersion = new VersionInfo();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {

                _winCEShell.Encode(stream);
                WriteString(stream, _customerBuild, 0x12);
                _metroviewDll.Encode(stream);
                _mapData.Encode(stream);
                WriteString(stream, _mapDataName, 0x12);
                _shellExe.Encode(stream);
                _apiDll.Encode(stream);
                _coreDll.Encode(stream);
                _coreUiDll.Encode(stream);
                _guiDll.Encode(stream);
                _iapDll.Encode(stream);
                _jobsDll.Encode(stream);
                _mapDll.Encode(stream);
                _preTripDll.Encode(stream);
                _resourcesDll.Encode(stream);
                _screenDll.Encode(stream);
                _routeDll.Encode(stream);
                _webServiceDll.Encode(stream);
                _sysDll.Encode(stream);
                _phoneDll.Encode(stream);
                _downloadUpdatesDll.Encode(stream);
                _paltfromCeDll.Encode(stream);
                _windowsOS.Encode(stream);
                Write4ByteInteger(stream, _storageCardCapacity);
                Write4ByteInteger(stream, _storageCardFreeSpace);
                Write2ByteInteger(stream, _mccQueue);
                Write4ByteInteger(stream, _battery);
                Write4ByteInteger(stream, _shellConfig.Major); //Write 4 bytes.  VersionInfo.Encode only write 2 bytes
                Write4ByteInteger(stream, _shellConfig.Minor); //Write 4 bytes.  VersionInfo.Encode only write 2 bytes
                WriteString(stream, _shellConfigName, 0x12);
                if (Version >= 3)
                {

                    Write4ByteInteger(stream, _windowsBuildVersion.Major); //Write 4 bytes.  VersionInfo.Encode only write 2 bytes
                    Write4ByteInteger(stream, _windowsBuildVersion.Minor); //Write 4 bytes.  VersionInfo.Encode only write 2 bytes
                    WriteString(stream, _windowsBuild, 0x12);
                }
                if (Version >= 4)
                {
                    WriteString(stream, _androidDeviceID, 0x12);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;

            _winCEShell.Decode(Data, ref index);
            _customerBuild = ReadString(Data, ref index, 0x12);
            _metroviewDll.Decode(Data, ref index);
            _mapData.Decode(Data, ref index);
            _mapDataName = ReadString(Data, ref index, 0x12);
            _shellExe.Decode(Data, ref index);
            _apiDll.Decode(Data, ref index);
            _coreDll.Decode(Data, ref index);
            _coreUiDll.Decode(Data, ref index);
            _guiDll.Decode(Data, ref index);
            _iapDll.Decode(Data, ref index);
            _jobsDll.Decode(Data, ref index);
            _mapDll.Decode(Data, ref index);
            _preTripDll.Decode(Data, ref index);
            _resourcesDll.Decode(Data, ref index);
            _screenDll.Decode(Data, ref index);
            _routeDll.Decode(Data, ref index);
            _webServiceDll.Decode(Data, ref index);
            _sysDll.Decode(Data, ref index);
            _phoneDll.Decode(Data, ref index);
            _downloadUpdatesDll.Decode(Data, ref index);
            _paltfromCeDll.Decode(Data, ref index);
            _windowsOS.Decode(Data, ref index);
            _storageCardCapacity = Read4ByteInteger(Data, ref index);
            _storageCardFreeSpace = Read4ByteInteger(Data, ref index);
            _mccQueue = Read2ByteInteger(Data, ref index);
            _battery = Read4ByteInteger(Data, ref index);
            if (Version >= 2)
            {
                if (_shellConfig == null)
                {
                    _shellConfig = new VersionInfo();
                }
                _shellConfig.Major = Read4ByteInteger(Data, ref index); //Read 4 bytes.  VersionInfo.Encode only read 2 bytes
                _shellConfig.Minor = Read4ByteInteger(Data, ref index); //Read 4 bytes.  VersionInfo.Encode only read 2 bytes
                this._shellConfigName = ReadString(Data, ref index, 0x12);

            }
            else
            {
                _shellConfig = new VersionInfo(-1, -1);
            }

            if (Version >= 3)
            {
                if (_windowsBuildVersion == null)
                {
                    _windowsBuildVersion = new VersionInfo();
                }
                _windowsBuildVersion.Major = Read4ByteInteger(Data, ref index); //Read 4 bytes.  VersionInfo.Encode only read 2 bytes
                _windowsBuildVersion.Minor = Read4ByteInteger(Data, ref index); //Read 4 bytes.  VersionInfo.Encode only read 2 bytes

                this._windowsBuild = ReadString(Data, ref index, 0x12);
            }

            if (Version >= 4)
            {
                this._androidDeviceID = ReadString(Data, ref index, 0x12);
            }
        }
        #endregion
    }
}

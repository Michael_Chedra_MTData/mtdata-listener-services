using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// class to hold the version info of a Talon 3rp Part App
    /// </summary>
    public class Talon3rdPartyAppVersionInfo : IPacketEncode
    {
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        public string DisplayName { get; set; }
        public string PackageName { get; set; }
        public int VersionCode { get; set; }
        public string VersionName { get; set; }

        public Talon3rdPartyAppVersionInfo() { }

        #region IPacketEncode members

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, DisplayName, 0x12);
                BaseLayer.WriteString(local, PackageName, 0x12);
                BaseLayer.Write4ByteInteger(local, VersionCode);
                BaseLayer.WriteString(local, VersionName, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            DisplayName = BaseLayer.ReadString(data, ref index, 0x12);
            PackageName = BaseLayer.ReadString(data, ref index, 0x12);
            VersionCode = BaseLayer.Read4ByteInteger(data, ref index);
            VersionName = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        #endregion
    }
}

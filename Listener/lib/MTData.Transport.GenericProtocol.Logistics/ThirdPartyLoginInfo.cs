﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class ThirdPartyLoginInfo : IPacketEncode
    {
        private int _versionNumber = 1;

        private DateTime _loginTime;

        private FatigueWorkType _fatigueWorkType;

        public DateTime LoginTime
        {
            get { return _loginTime; }
            set { _loginTime = value; }
        }

        public FatigueWorkType FatigueWorkType
        {
            get { return _fatigueWorkType; }
            set { _fatigueWorkType = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            LoginTime = BaseLayer.ReadDateTime(data, ref index);
            FatigueWorkType = (FatigueWorkType)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteDateTime(local, LoginTime);
                local.WriteByte((byte)FatigueWorkType);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// enum for different phone call report types
    /// </summary>
    public enum PhoneCallTypes
    {
        None = -1,
        IncomingCall=0,
        OutgoingCall,
        SMSSent,
        SMSReceived,
        Outgoing3rdParty
    }

    public class PhoneCallReportSegment : SegmentLayer
    {
        #region private fields
        private PhoneCallTypes _phoneCallType;
        private int _callTime;
        private string _phoneNumber;
        private string _simCardNumber;
        private string _smsMessage;
        #endregion

        #region properties
        public PhoneCallTypes PhoneCallType
        {
            get { return _phoneCallType; }
            set { _phoneCallType = value; }
        }
        public int CallTime
        {
            get { return _callTime; }
            set { _callTime = value; }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }
        public string SimCardNumber
        {
            get { return _simCardNumber; }
            set { _simCardNumber = value; }
        }
        public string SmsMessage
        {
            get { return _smsMessage; }
            set { _smsMessage = value; }
        }
        #endregion

        #region constructor
        public PhoneCallReportSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.PhoneCallReport;
        }
        public PhoneCallReportSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.PhoneCallReport)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.PhoneCallReport, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.PhoneCallReport;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_phoneCallType);
                Write2ByteInteger(stream, _callTime);
                WriteString(stream, _phoneNumber, 0x12);
                WriteString(stream, _simCardNumber, 0x12);
                if (Version >= 2)
                {
                    WriteString(stream, _smsMessage, 0x12);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _phoneCallType = (PhoneCallTypes)Data[index++];
            _callTime = Read2ByteInteger(Data, ref index);
            _phoneNumber = ReadString(Data, ref index, 0x12);
            _simCardNumber = ReadString(Data, ref index, 0x12);
            if (Version >= 2)
            {
                _smsMessage = ReadString(Data, ref index, 0x12);
            }
        }
        #endregion

    }
}

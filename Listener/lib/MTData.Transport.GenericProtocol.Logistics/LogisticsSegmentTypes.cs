using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// the segemnt types for the logisitcs interface
    /// </summary>
    public enum LogisticsSegmentTypes
    {
        LogisticsHeader = SegmentTypes.LogisticsSegments + 1,
        KeepAlive,
        Asset5080,
        Login,
        Logout,
        TripAnswers,
        RequestList,
        DriverBreak,
        VehicleUsage,
        PhoneCallReport,//160
        FuelReport,
        JobItemReceived,
        JobRead,
        JobEmailAlert,
        JobArriveSite,
        JobLegComplete,
        JobLegActionStatusChanged,
        MessageReceived,
        MessageRead,
        MessageReply,//170
        MessageSendPreDefined,
        MessageSend,
        MessageSendEmail,
        AddressNew,
        AddressDelete,
        TrailerRequest,
        PreDefinedFunctionResult,

        SendLoginResponse,
        SendForceLogout,
        SendList,//180
        CreateJob,
        CancelJob,
        AddLeg,
        DeleteLeg,
        AddAction,
        DeleteAction,
        SendMessage,
        SendTrailer,
        PreDefinedFunction,
        SendVersioNumbers,//190

        JobLegActionComplete,
        UnitCreateLeg,
        SendCreatedLegId,
        JobStartTransaction,
        JobEndTransaction,
        JobActionStatusChanged,
        JobActionComplete,
        JobStatusChanged,
        RequestJobHistory,
        SendJobHistory,//200
        RequestJobHistoryDetailed,
        SendJobHistoryDetailed,
        UnitCreateJobAction,
        SendCreatedJobActionId,
        UnitCreateJob,
        ErrorMessage,
        UnitCreateLegAction,
        SendCreatedLegActionId,
        LegStatusChanged,
        UpdateLegInfo,//210
        UpdateLegAction,
        CreateContainerRequestJob,
        SendMultiList,
        StartAssociation,
        EndAssociation,
        [Obsolete]
        AssociationError, // No longer used
        [Obsolete]
        AssociationChange, // No longer used
        [Obsolete]
        AssociationMessageReceived, // No longer used
        SendDocumentManifest,

        SafeJourneyPlan,//220
        SafeJourneyDeclaration,
        SafeJourneySimpleEvent,
        SafeJourneySimpleEventBase,
        SendFleetTrailers,
        SafeJourneyEventAck,

        Defects,
        UnitCreatedDefect,
        UnitCreatedDefectAck,
        DefectNote,
        [Obsolete]
        DefectRefresh, // No longer used now included inside EventRefresh 230
        DefectsReqExtraData,
        DefectsExtraData,

        Incidents,
        UnitCreatedIncident,
        UnitCreatedIncidentAck,
        IncidentNote,
        EventRefresh,
        TripTemplateAnswer,
        RequestListAck,
        SendInspection, //240
        RequestDefectsForAsset,
        SendDefectsForAsset,
        AssociationWarning,
        AssociationSync,
        AssociationRefresh,
        EditDriverPhonebook,
        DriverBreakEvent,
        IncidentReqExtraData,
        IncidentExtraData,
        RequestIncidentsForAsset,
        SendIncidentsForAsset,
        SendDocumentAck,
        SendFatigueAlert,
        RequestDocumentManifest,
        IapCommentEvent,
        IapMassDeclarationEvent,
    }
}

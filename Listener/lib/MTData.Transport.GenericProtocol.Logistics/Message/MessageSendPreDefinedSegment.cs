using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    /// <summary>
    /// a predefined message has been sent by the unit
    /// </summary>
    public class MessageSendPreDefinedSegment : SegmentLayer
    {
        #region private fields
        private ListId _predefinedListId;
        private int _messageId;
        #endregion

        #region properties
        public ListId PredefinedListId
        {
            get { return _predefinedListId; }
            set { _predefinedListId = value; }
        }
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        #endregion

        #region constructor
        public MessageSendPreDefinedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.MessageSendPreDefined;
            _predefinedListId = new ListId();
        }
        public MessageSendPreDefinedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.MessageSendPreDefined)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.MessageSendPreDefined, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.MessageSendPreDefined;
            _predefinedListId = new ListId();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                _predefinedListId.Encode(stream);
                Write4ByteInteger(stream, _messageId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _predefinedListId.Decode(Data, ref index);
            _messageId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

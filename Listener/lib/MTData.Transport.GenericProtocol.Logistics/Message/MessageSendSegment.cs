using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    /// <summary>
    /// the unit is sending a new message
    /// </summary>
    public class MessageSendSegment : SegmentLayer
    {
        #region private fields
        private string _message;

        private int _sjpId;
        #endregion

        #region properties
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public int SjpId
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }

        public List<MessageAttachment> Attachments { get; set; } = new List<MessageAttachment>();

        #endregion

        #region constructor
        public MessageSendSegment()
        {
            Version = 3;
            Type = (int)LogisticsSegmentTypes.MessageSend;
        }
        public MessageSendSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.MessageSend)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.MessageSend, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.MessageSend;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                // Versoin 1
                WriteString(stream, _message, 0x12);

                // Version 2
                Write4ByteInteger(stream, _sjpId);

                // Version 3
                int attachmentCount = Attachments?.Count ?? 0;
                WriteMoreFlag(stream, attachmentCount);
                if (attachmentCount > 0)
                {
                    foreach (var a in Attachments)
                    {
                        a.Encode(stream);
                    }
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _message = ReadString(Data, ref index, 0x12);

            if (Version >= 2)
            {
                _sjpId = Read4ByteInteger(Data, ref index);
            }

            if (Version >= 3)
            {
                int attachmentCount = ReadMoreFlag(Data, ref index);

                var attachments = new List<MessageAttachment>(attachmentCount);

                for (int i = 0; i < attachmentCount; i++)
                {
                    MessageAttachment a = new MessageAttachment();
                    a.Decode(Data, ref index);
                    attachments.Add(a);
                }

                Attachments = attachments;
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    /// <summary>
    /// unit is sending an email message
    /// </summary>
    public class MessageSendEmailSegment : SegmentLayer
    {
        #region private fields
        private string _message;
        private string _emailAddress;
        #endregion

        #region properties
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        public string EmailAddress
        {
            get { return _emailAddress; }
            set { _emailAddress = value; }
        }
        #endregion

        #region constructor
        public MessageSendEmailSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.MessageSendEmail;
        }
        public MessageSendEmailSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.MessageSendEmail)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.MessageSendEmail, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.MessageSendEmail;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                WriteString(stream, _message, 0x12);
                WriteString(stream, _emailAddress, 0x12);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _message = ReadString(Data, ref index, 0x12);
            _emailAddress = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

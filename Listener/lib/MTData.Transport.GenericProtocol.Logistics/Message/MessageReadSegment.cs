using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    /// <summary>
    /// the unit has read a message
    /// </summary>
    public class MessageReadSegment : SegmentLayer
    {
        #region private fields
        private int _messageId;
        #endregion

        #region properties
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        #endregion

        #region constructor
        public MessageReadSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.MessageRead;
        }
        public MessageReadSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.MessageRead)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.MessageRead, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.MessageRead;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _messageId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _messageId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    [Serializable]
    public class MessageAttachment : IPacketEncode
    {
        public int VersionNumber { get; } = 1;

        public MessageAttachmentType Type { get; set; }

        public int AttachmentID { get; set; }

        public string AttachmentName { get; set; }

        public byte[] Attachment { get; set; } = new byte[0];

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            Type = (MessageAttachmentType)data[index++];
            AttachmentID = BaseLayer.Read4ByteInteger(data, ref index);
            AttachmentName = BaseLayer.ReadString(data, ref index, 0x12);

            int attachementLength = BaseLayer.ReadMoreFlag(data, ref index);
            Attachment = new byte[attachementLength];
            Array.Copy(data, index, Attachment, 0, attachementLength);
            index += attachementLength;


            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {

                local.WriteByte((byte)Type);
                BaseLayer.Write4ByteInteger(local, AttachmentID);
                BaseLayer.WriteString(local, AttachmentName, 0x12);

                BaseLayer.WriteMoreFlag(local, Attachment?.Length ?? 0);
                if ((Attachment?.Length ?? 0) > 0)
                {
                    local.Write(Attachment, 0, Attachment.Length);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, VersionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }

        }
    }
}

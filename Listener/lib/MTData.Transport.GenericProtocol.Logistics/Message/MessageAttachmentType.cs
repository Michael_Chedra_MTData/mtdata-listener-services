﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    public enum MessageAttachmentType : byte
    {
        Unknown = 0,
        PDF,
        PNG,
        JPG

    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    /// <summary>
    /// the unit has replied to a message
    /// </summary>
    public class MessageReplySegment : SegmentLayer
    {
        #region private fields
        private int _messageId;
        private string _message;
        #endregion

        #region properties
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public List<MessageAttachment> Attachments { get; set; } = new List<MessageAttachment>();
        #endregion

        #region constructor
        public MessageReplySegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.MessageReply;
        }
        public MessageReplySegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.MessageReply)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.MessageReply, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.MessageReply;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                // Version 1
                Write4ByteInteger(stream, _messageId);
                WriteString(stream, _message, 0x12);

                // Version 2
                int attachmentCount = Attachments?.Count ?? 0;
                WriteMoreFlag(stream, attachmentCount);
                if (attachmentCount > 0)
                {
                    foreach (var a in Attachments)
                    {
                        a.Encode(stream);
                    }
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _messageId = Read4ByteInteger(Data, ref index);
            _message = ReadString(Data, ref index, 0x12);

            if (Version >= 2)
            {
                int attachmentCount = ReadMoreFlag(Data, ref index);

                var attachments = new List<MessageAttachment>(attachmentCount);

                for (int i = 0; i < attachmentCount; i++)
                {
                    MessageAttachment a = new MessageAttachment();
                    a.Decode(Data, ref index);
                    attachments.Add(a);
                }

                Attachments = attachments;
            }

        }
        #endregion
    }
}

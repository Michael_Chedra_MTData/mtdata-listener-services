using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Message
{
    /// <summary>
    /// send a new message to the unit
    /// </summary>
    public class SendMessageToUnitSegment : SegmentLayer
    {
        #region private fields
        private int _messageId;
        private string _message;
        private bool _replyRequired;
        private bool _hasAddressDetails;
        private string _name;
        private string _address;
        private string _phoneNumber;
        private double _latitude;
        private double _longitude;
        private DateTime _expiryDate;
        private int _sjpId;
        #endregion

        #region properties
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        public bool ReplyRequired
        {
            get { return _replyRequired; }
            set { _replyRequired = value; }
        }
        public string Name
        {
            get { return _name; }
            set
            {
                _name = value;
                _hasAddressDetails = true;
            }
        }
        public string Address
        {
            get { return _address; }
            set
            {
                _address = value;
                _hasAddressDetails = true;
            }
        }
        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set
            {
                _phoneNumber = value;
                _hasAddressDetails = true;
            }
        }
        public double Latitude
        {
            get { return _latitude; }
            set
            {
                _latitude = value;
                _hasAddressDetails = true;
            }
        }
        public double Longitude
        {
            get { return _longitude; }
            set
            {
                _longitude = value;
                _hasAddressDetails = true;
            }
        }

        public DateTime ExpiryDate
        {
            get { return _expiryDate; }
            set { _expiryDate = value; }
        }

        public int SjpId
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }

        public List<MessageAttachment> Attachments { get; set; } = new List<MessageAttachment>();
        #endregion

        #region constructor
        public SendMessageToUnitSegment()
        {
            Version = 4;
            Type = (int)LogisticsSegmentTypes.SendMessage;
        }
        public SendMessageToUnitSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendMessage)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendMessage, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendMessage;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _messageId);
                WriteString(stream, _message, 0x12);
                WriteBool(stream, _replyRequired);
                WriteBool(stream, _hasAddressDetails);

                if (_hasAddressDetails)
                {
                    WriteString(stream, _name, 0x12);
                    WriteString(stream, _address, 0x12);
                    WriteString(stream, _phoneNumber, 0x12);
                    WriteCoordinate(stream, _latitude);
                    WriteCoordinate(stream, _longitude);
                }

                WriteDateTime(stream, ExpiryDate);

                // Version 3
                Write4ByteInteger(stream, SjpId);

                // Version 4
                int attachmentCount = Attachments?.Count ?? 0;
                WriteMoreFlag(stream, attachmentCount);
                if (attachmentCount > 0)
                {
                    foreach (var a in Attachments)
                    {
                        a.Encode(stream);
                    }
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _messageId = Read4ByteInteger(Data, ref index);
            _message = ReadString(Data, ref index, 0x12);
            _replyRequired = ReadBool(Data, ref index);
            _hasAddressDetails = ReadBool(Data, ref index);
            if (_hasAddressDetails)
            {
                _name = ReadString(Data, ref index, 0x12);
                _address = ReadString(Data, ref index, 0x12);
                _phoneNumber = ReadString(Data, ref index, 0x12);
                _latitude = ReadCoordinate(Data, ref index);
                _longitude = ReadCoordinate(Data, ref index);
            }
            else
            {
                _name = string.Empty;
                _address = string.Empty;
                _phoneNumber = string.Empty;
                _latitude = 0;
                _longitude = 0;
            }

            ExpiryDate = DateTime.MinValue;
            if (Version >= 2)
            {
                ExpiryDate = ReadDateTime(Data, ref index);
            }

            if (Version >= 3)
            {
                SjpId = Read4ByteInteger(Data, ref index);
            }

            if (Version >= 4)
            {
                int attachmentCount = ReadMoreFlag(Data, ref index);

                var attachments = new List<MessageAttachment>(attachmentCount);

                for (int i = 0; i < attachmentCount; i++)
                {
                    MessageAttachment a = new MessageAttachment();
                    a.Decode(Data, ref index);
                    attachments.Add(a);
                }

                Attachments = attachments;
            }
        }
        #endregion
    }
}

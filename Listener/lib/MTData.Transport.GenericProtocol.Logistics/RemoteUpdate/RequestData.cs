﻿using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Jobs;

namespace MTData.Transport.GenericProtocol.Logistics.RemoteUpdate
{
    [Serializable]
    public class RequestData
    {
        // Header Info
        public int FleetID { get; set; }
        public int VehicleID { get; set; }
        public int DriverID { get; set; }
        public float Latitude { get; set; }
        public float Longitude { get; set; }
        public double GPSTimeToOADate { get; set; }
        public short Speed { get; set; }
        public short Heading { get; set; }
        public int DeviceTimer { get; set; }

        // Job Info
        public int DispatchID { get; set; }
        public int JobUpdateType { get; set; }
        public int JobEventID { get; set; }
        public int JobActionID { get; set; }
        public JobStatus JobStatus { get; set; }
        public JobActionStatus JobActionStatus { get; set; }
        public JobActionTypes JobActionType { get; set; }
        
        // Leg Info
        public int LegID { get; set; }
        public int LegActionID { get; set; }
        public LegStatus LegStatus { get; set; }
        public LegActionStatus LegActionStatus { get; set; }
        public LegActionTypes LegActionType { get; set; }
        public int LegEventID { get; set; }

        // Other
        public EMailFlag EMailFlag { get; set; }
    }
}

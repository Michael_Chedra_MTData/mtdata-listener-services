﻿
using System.ComponentModel;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public enum EventType
    {
        Login = 0,
        Logoff = 1,
        [Description("Start Break")]
        BreakOn = 2,
        [Description("Start Vehicle Usage")]
        VehicleUsageOn = 3,
        [Description("PhoneCall - Incoming")]
        PhoneCallIncoming = 4,
        Fuel = 5,
        [Description("Pre Trip")]
        PreTrip = 6,
        [Description("Post Trip")]
        PostTrip = 7,
        [Description("End Break")]
        BreakOff = 8,
        [Description("End Vehicle Usage")]
        VehicleUsageOff = 9,
        [Description("Message Sent")]
        MessageSent = 10,
        [Description("Message Received")]
        MessageReceived = 11,
        [Description("Message Read By Driver")]
        MessageReadByDriver = 12,
        [Description("PhoneCall - Outgoing")]
        PhoneCallOutgoing = 13,
        [Description("PhoneCall - Outgoing 3rd Party")]
        PhoneCallOutgoing3rdParty = 14,
        [Description("PhoneCall - SMS Sent")]
        PhoneCallSmsSent = 15,
        [Description("PhoneCall - SMS Received")]
        PhoneCallSmsReceived = 16,
        [Description("Predefined Function")]
        PredefinedFunction = 17,
        [Description("Start Work")]
        WorkOn = 18,
        [Description("End Work")]
        WorkOff = 19,
        [Description("Defect Raised")]
        DefectRaised = 20,
        [Description("Defect Note")]
        DefectNote = 21,
        [Description("Defect Assessment")]
        DefectAssessment = 22,
        [Description("Defect Work Order")]
        DefectWorkOrder = 23,
        [Description("Defect Closed")]
        DefectClosed = 24,
        [Description("Incident Raised")]
        IncidentRaised = 25,
        [Description("Incident Note")]
        IncidentNote = 26,
        [Description("Incident Closed")]
        IncidentClosed = 27,
        [Description("Association")]
        Association = 28,
        [Description("Inspection")]
        Inspection = 29,
        [Description("Break Missed")]
        BreakMissed = 30,
        [Description("Break Finished Early")]
        BreakFinishedEarly = 31,
        [Description("Document Acknowledge")]
        DocumentAcknowledge = 32,
        [Description("Fatigue Alert")]
        FatigueAlert = 33,
        [Description("IAP Comment")]
        IapComment = 34,
        [Description("IAP Mass Declaration")]
        IapMassDeclaration = 35,
    }


}

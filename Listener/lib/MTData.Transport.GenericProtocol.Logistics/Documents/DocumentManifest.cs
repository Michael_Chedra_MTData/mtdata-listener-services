﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Documents
{
    public class DocumentManifest : IPacketEncode
    {
        #region Private Fields

        private int _versionNumber = 3;
        private int _libraryId;
        private string _libraryName;
        private List<DocumentManifestFile> _files;
        private bool _privateLibrary;
        private DocumentAckState _ackState;

        #endregion

        #region Constructors

        public DocumentManifest()
        {
            _files = new List<DocumentManifestFile>();
        }

        public DocumentManifest(int libraryId, string libraryName, List<DocumentManifestFile> files, bool privateLibrary)
        {
            _libraryId = libraryId;
            _libraryName = libraryName;
            _files = files;
            _privateLibrary = privateLibrary;
        }

        public DocumentManifest(DocumentManifest documentManifest)
        {
            _libraryId = documentManifest._libraryId;
            _libraryName = documentManifest._libraryName;
            _files = documentManifest._files;
            _privateLibrary = documentManifest._privateLibrary;
        }

        #endregion

        #region Public Properties

        public int LibraryId { get { return _libraryId; } set { _libraryId = value; } }

        public string LibraryName { get { return _libraryName; } set { _libraryName = value; } }

        public List<DocumentManifestFile> Files { get { return _files; } set { _files = value; } }

        public bool PrivateLibrary { get { return _privateLibrary; } set { _privateLibrary = value; } }

        public DocumentAckState AckState { get { return _ackState; } set { _ackState = value; } }

        #endregion

        #region IPacketEncodeMembers

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            this._libraryId = BaseLayer.Read4ByteInteger(data, ref index);
            this._libraryName = BaseLayer.ReadString(data, ref index, 0x12);
            this._files.Clear();
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for(int i = 0; i < count; i++)
            {
                DocumentManifestFile file = new DocumentManifestFile();
                file.Decode(data, ref index);
                this._files.Add(file);
            }

            if (versionNumber > 1)
            {
                _privateLibrary = BaseLayer.ReadBool(data, ref index);
            }

            if (versionNumber > 2)
            {
                this._ackState = (DocumentAckState)data[index++];
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            using(System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _libraryId);
                BaseLayer.WriteString(local, _libraryName, 0x12);
                BaseLayer.Write2ByteInteger(local, _files.Count);

                foreach (DocumentManifestFile file in _files)
                {
                    file.Encode(local);
                }

                BaseLayer.WriteBool(local, _privateLibrary);

                //Version 3
                local.WriteByte((byte)_ackState);

                //write version number
                BaseLayer.WriteMoreFlag(stream, this._versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }            
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Documents
{
    [Flags]
    public enum DocumentAckState : byte
    {
        /// <summary>
        /// None
        /// </summary>
        None = 0x00,
        /// <summary>
        /// Document requires read ack
        /// </summary>
        ReadAckRequired = 0x01,
        /// <summary>
        /// Driver has read the document
        /// </summary>
        ReadAcknowledged = 0x02,
    }
}

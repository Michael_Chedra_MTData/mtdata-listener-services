﻿namespace MTData.Transport.GenericProtocol.Logistics.Documents
{
    using System;

    /// <summary>
    /// Defines the <see cref="RequestDocumentManifestSegment" />
    /// </summary>
    public class RequestDocumentManifestSegment : SegmentLayer
    {
        /// <summary>
        /// Defines the SegmentType
        /// </summary>
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.RequestDocumentManifest;

        /// <summary>
        /// Initialises a new instance of the <see cref="RequestDocumentManifestSegment"/> class.
        /// </summary>
        public RequestDocumentManifestSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="RequestDocumentManifestSegment"/> class.
        /// </summary>
        /// <param name="segment">The <see cref="SegmentLayer"/></param>
        public RequestDocumentManifestSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception($"Can only create a segment of type {SegmentType}, not {segment.Type}");
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
        }
    }
}

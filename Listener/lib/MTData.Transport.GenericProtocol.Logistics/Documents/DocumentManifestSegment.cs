﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Documents
{
    public class DocumentManifestSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendDocumentManifest;

        #region Private Fields

        private List<DocumentManifest> _manifests = new List<DocumentManifest>();
        private string _serviceUrl;
        private string _token;

        #endregion

        #region Constructors

        public DocumentManifestSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public DocumentManifestSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Public Properties

        public List<DocumentManifest> Manifests
        {
            get { return _manifests; }
            set { _manifests = value; }
        }

        public string ServiceUrl
        {
            get { return _serviceUrl; }
            set { _serviceUrl = value; }
        }

        public string Token
        {
            get { return _token; }
            set { _token = value; }
        }

        #endregion

        #region Override Methods

        public override byte[] GetBytes()
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                WriteString(stream, _serviceUrl, 0x12);
                WriteString(stream, _token, 0x12);
                Write2ByteInteger(stream, _manifests.Count);
                foreach (DocumentManifest documentManifest in _manifests)
                {
                    documentManifest.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region Private Methods

        private void DecodeData()
        {
            int index = 0;
            _serviceUrl = ReadString(Data, ref index, 0x12);
            _token = ReadString(Data, ref index, 0x12);
            int count = Read2ByteInteger(Data, ref index);
            _manifests.Clear();
            for (int i = 0; i < count; i++)
            {
                DocumentManifest manifest = new DocumentManifest();
                manifest.Decode(Data, ref index);
                _manifests.Add(manifest);
            }
        }

        #endregion
    }
}

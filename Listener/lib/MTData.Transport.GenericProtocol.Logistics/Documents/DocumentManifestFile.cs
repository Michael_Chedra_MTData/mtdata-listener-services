﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Documents
{
    public class DocumentManifestFile : IPacketEncode
    {
        #region Private Fields

        private int _versionNumber = 2;
        private int _fileId;
        private string _displayName;
        private string _fileExtension;
        private DocumentAckState _ackState;

        #endregion

        #region Constructors

        public DocumentManifestFile() { }

        public DocumentManifestFile(int fileId, string displayName, string fileExtension)
        {
            _fileId = fileId;
            _displayName = displayName;
            _fileExtension = fileExtension;
        }

        public DocumentManifestFile(DocumentManifestFile file)
        {
            _fileId = file._fileId;
            _displayName = file._displayName;
            _fileExtension = file._fileExtension;
        }

        #endregion

        #region Public Properties

        public int FileId { get { return _fileId; } set { _fileId = value; } }

        public string DisplayName { get { return _displayName; } set { _displayName = value; } }

        public string FileExtension { get { return _fileExtension; } set { _fileExtension = value; } }

        public string FileName { get { return string.Format("{0}{1}", _fileId, _fileExtension); } }

        public DocumentAckState AckState { get { return _ackState; } set { _ackState = value; } }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            this._fileId = BaseLayer.Read4ByteInteger(data, ref index);
            this._displayName = BaseLayer.ReadString(data, ref index, 0x12);
            this._fileExtension = BaseLayer.ReadString(data, ref index, 0x12);

            if (versionNumber >= 2)
            {
                this._ackState = (DocumentAckState)data[index++];
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _fileId);
                BaseLayer.WriteString(local, _displayName, 0x12);
                BaseLayer.WriteString(local, _fileExtension, 0x12);
                local.WriteByte((byte)_ackState);

                //write version number
                BaseLayer.WriteMoreFlag(stream, this._versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

﻿using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Documents
{
    public class SendDocumentAckSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendDocumentAck;

        public int LibraryID { get; set; }

        public int FileID { get; set; }

        public AnsweredQuestions Answers { get; set; }

        public SendDocumentAckSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SendDocumentAckSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        #region Override Methods

        public override byte[] GetBytes()
        {
            using (System.IO.MemoryStream stream = new System.IO.MemoryStream())
            {
                int fieldsFlags = (Answers != null ? 1 << 0 : 0);

                Write4ByteInteger(stream, LibraryID);
                Write4ByteInteger(stream, FileID);
                WriteMoreFlag(stream, fieldsFlags);

                if (Answers != null)
                {
                    Answers.Encode(stream);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        #endregion

        #region Private Methods

        private void DecodeData()
        {
            int index = 0;

            LibraryID = Read4ByteInteger(Data, ref index);
            FileID = Read4ByteInteger(Data, ref index);

            int fieldsFlags = ReadMoreFlag(Data, ref index);

            if ((fieldsFlags & (1 << 0)) != 0)
            {
                Answers = new AnsweredQuestions();
                Answers.Decode(Data, ref index);
            }
            else
            {
                Answers = null;
            }
        }

        #endregion

    }
}

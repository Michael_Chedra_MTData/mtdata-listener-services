using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class DriverBreakSegment : SegmentLayer
    {
        #region private fields
        private bool _startBreak;
        private int _odometer;
        private int _totalFuel;
        private byte _breakType;
        #endregion

        #region properties
        public bool StartBreak
        {
            get { return _startBreak; }
            set { _startBreak = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }

        public BreakType BreakType
        {
            get { return (BreakType)_breakType; }
            set { _breakType = (byte)value; }
        }
        #endregion

        #region constructor
        public DriverBreakSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.DriverBreak;
        }
        public DriverBreakSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.DriverBreak)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.DriverBreak, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.DriverBreak;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                WriteBool(stream, _startBreak);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);

                if (Version >= 2)
                {
                    stream.WriteByte(_breakType);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _startBreak = ReadBool(Data, ref index);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);

            if (Version >= 2)
            {
                _breakType = Data[index++];
            }
            else
            {
                _breakType = 0;
            }
        }
        #endregion
    }
}

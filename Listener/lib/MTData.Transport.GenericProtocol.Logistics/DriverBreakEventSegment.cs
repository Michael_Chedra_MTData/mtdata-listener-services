using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class DriverBreakEventSegment : SegmentLayer
    {
        #region private fields
        private byte _breakType;
        #endregion

        #region properties
        public BreakEventType BreakType
        {
            get { return (BreakEventType)_breakType; }
            set { _breakType = (byte)value; }
        }
        #endregion

        #region constructor
        public DriverBreakEventSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.DriverBreakEvent;
        }
        public DriverBreakEventSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.DriverBreakEvent)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.DriverBreakEvent, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.DriverBreakEvent;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte(_breakType);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _breakType = Data[index++];
        }
        #endregion
    }
}

﻿using MTData.Transport.GenericProtocol;
using MTData.Transport.GenericProtocol.Logistics;
using MTData.Transport.GenericProtocol.Logistics.DefectReport;
using MTData.Transport.GenericProtocol.Logistics.IncidentReport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class EventRefreshSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.EventRefresh;

        private List<DefectItem> _defectItems = new List<DefectItem>();
        private List<IncidentItem> _incidentItems = new List<IncidentItem>();

        public EventRefreshSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public EventRefreshSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public IEnumerable<DefectItem> DefectItems
        {
            get { return _defectItems; }
        }

        public IEnumerable<IncidentItem> IncidentItems
        {
            get { return _incidentItems; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, _defectItems.Count);
                foreach (DefectItem d in _defectItems)
                {
                    d.Encode(stream);
                }
                WriteMoreFlag(stream, _incidentItems.Count);
                foreach (IncidentItem i in _incidentItems)
                {
                    i.Encode(stream);
                }
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        public void AddDefectRefresh(DefectItem defectItem)
        {
            // If this object contains old refresh data for the same defect that has not yet been
            // sent then it should be removed and replaced with the new data.
            // This assumes that this method will be invoked in the order that 
            // events occur. 
            //_defectItems.RemoveAll(x => x.DefectID == defectItem.DefectID); // No linq support in Compact Framework 
            for (int i = _defectItems.Count - 1; i >= 0; i--)
            {
                if (_defectItems[i].DefectID == defectItem.DefectID)
                {
                    _defectItems.RemoveAt(i);
                }
            }
            _defectItems.Add(defectItem);
        }

        public void AddIncidentRefresh(IncidentItem incidentItem)
        {
            // If this object contains old refresh data for the same incident that has not yet been
            // sent then it should be removed and replaced with the new data.
            // This assumes that this method will be invoked in the order that 
            // events occur. 
            //_incidentItems.RemoveAll(x => x.IncidentID == incidentItem.IncidentID); // No linq support in Compact Framework 
            for (int i = _incidentItems.Count - 1; i >= 0; i--)
            {
                if (_incidentItems[i].IncidentID == incidentItem.IncidentID)
                {
                    _incidentItems.RemoveAt(i);
                }
            }
            _incidentItems.Add(incidentItem);
        }

        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);
            List<DefectItem> defectItems = new List<DefectItem>(count);
            for (int i = 0; i < count; i++)
            {
                DefectItem d = new DefectItem();
                d.Decode(Data, ref index);
                defectItems.Add(d);
            }
            _defectItems = defectItems;
            count = ReadMoreFlag(Data, ref index);
            List<IncidentItem> incidentItems = new List<IncidentItem>(count);
            for (int j = 0; j < count; j++)
            {
                IncidentItem i = new IncidentItem();
                i.Decode(Data, ref index);
                incidentItems.Add(i);
            }
            _incidentItems = incidentItems;
        }

    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// this segment contains the logistics header information sent with all other logistics segments from the unit
    /// </summary>
    public class LogisticsHeaderSegment : SegmentLayer
    {
        #region private fields
        private int _fleet;
        private int _vehicle;
        private int _driverId;
        private double _latitude;
        private double _longitude;
        private DateTime _gpsTime;
        private int _speed;
        private int _heading;
        private int _deviceTimer;
        private int _ecmOdometer;
        private int _gpsOdometer;
        private int _totalFuel;
        private bool _manualOdo;
        #endregion

        #region properties
        public int Fleet
        {
            get { return _fleet; }
            set { _fleet = value; }
        }
        public int Vehicle
        {
            get { return _vehicle; }
            set { _vehicle = value; }
        }
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public DateTime GpsTime
        {
            get { return _gpsTime; }
            set { _gpsTime = value; }
        }
        public int Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        public int Heading
        {
            get { return _heading; }
            set { _heading = value; }
        }
        public int DeviceTimer
        {
            get { return _deviceTimer; }
            set { _deviceTimer = value; }
        }
        public DateTime DeviceTime
        {
            get { return _gpsTime == DateTime.MinValue ? DateTime.UtcNow : _gpsTime.AddSeconds(_deviceTimer); }
        }
        public int ECMOdometer
        {
            get { return _ecmOdometer; }
            set { _ecmOdometer = value; }
        }

        public int GPSOdometer
        {
            get { return _gpsOdometer; }
            set { _gpsOdometer = value; }
        }

        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }

        public bool ManualOdo
        {
            get { return _manualOdo; }
            set { _manualOdo = value; }
        }
        #endregion

        #region constructor
        public LogisticsHeaderSegment()
        {
            Version = 4;
            Type = (int)LogisticsSegmentTypes.LogisticsHeader;
        }
        public LogisticsHeaderSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.LogisticsHeader)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.LogisticsHeader, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.LogisticsHeader;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleet);
                Write4ByteInteger(stream, _vehicle);
                Write4ByteInteger(stream, _driverId);
                WriteCoordinate(stream, _latitude);
                WriteCoordinate(stream, _longitude);
                WriteDateTime(stream, _gpsTime);
                stream.WriteByte((byte)_speed);
                Write2ByteInteger(stream, _heading);
                // Version 1
                // Write2ByteInteger(stream, _deviceTimer);
                // Version 2
                Write4ByteInteger(stream, _deviceTimer);
                // Version 3
                Write4ByteInteger(stream, _ecmOdometer);
                Write4ByteInteger(stream, _gpsOdometer);
                Write4ByteInteger(stream, _totalFuel);
				// Version 4
                WriteBool(stream, _manualOdo);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleet = Read4ByteInteger(Data, ref index);
            _vehicle = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _latitude = ReadCoordinate(Data, ref index);
            _longitude = ReadCoordinate(Data, ref index);
            _gpsTime = ReadDateTime(Data, ref index);
            _speed = (int)Data[index];
            index++;
            _heading = Read2ByteInteger(Data, ref index);
            if (Version == 1)
            {
                _deviceTimer = Read2ByteInteger(Data, ref index);
            }
            else
            {
                _deviceTimer = Read4ByteInteger(Data, ref index);
            }
            if (Version >= 3)
            {
                _ecmOdometer = Read4ByteInteger(Data, ref index);
                _gpsOdometer = Read4ByteInteger(Data, ref index);
                _totalFuel = Read4ByteInteger(Data, ref index);
            }
            if (Version >= 4)
            {
                _manualOdo = ReadBool(Data, ref index);
            }
        }
        #endregion
    }
}

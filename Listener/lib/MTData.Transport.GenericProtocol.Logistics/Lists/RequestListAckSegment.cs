using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class RequestListAckSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.RequestListAck;

        private ListId _listId = new ListId();

        public ListId ListId
        {
            get { return _listId; }
            set { _listId = value; }
        }

        public RequestListAckSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public RequestListAckSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                _listId.Encode(stream);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            //1st byte is the list type
            int index = 0;
            _listId = new ListId();
            _listId.Decode(Data, ref index);

        }
        #endregion



    }
}
﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class PoiList : List
    {
        private int _versionNumber = 1;
        private ListId _categoryListId;
        private int _categoryListItemId;
        private int _categoryListItemUniqueKey;

        public PoiList() : base(ListTypes.Poi)
        {
            _categoryListId = new ListId();
        }
        
        public ListId CategoryListId
        {
            get { return _categoryListId; }
            set { _categoryListId = value; }
        }

        public int CategoryListItemId
        {
            get { return _categoryListItemId; }
            set { _categoryListItemId = value; }
        }

        public int CategoryListItemUniqueKey
        {
            get { return _categoryListItemUniqueKey; }
            set { _categoryListItemUniqueKey = value; }
        }

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _categoryListId = new ListId();
            _categoryListId.Decode(data, ref index);

            _categoryListItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _categoryListItemUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _categoryListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _categoryListItemId);
                BaseLayer.Write4ByteInteger(local, _categoryListItemUniqueKey);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

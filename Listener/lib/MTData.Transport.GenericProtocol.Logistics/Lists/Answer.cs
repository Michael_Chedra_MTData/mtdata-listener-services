using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// an answered question
    /// </summary>
    public class Answer
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;

        private int _questionId;
        private string _answer;
        private byte[] _photo = new byte[0];
        private AnsweredQuestions _conditionalAnsweredQuestions;
        #endregion

        #region properties
        public int QuestionId
        {
            get { return _questionId; }
            set { _questionId = value; }
        }
        public string QuestionAnswer
        {
            get { return _answer; }
            set { _answer = value; }
        }

        /// <summary>
        /// Some yes no questions point to a list of additional questions to be answered
        /// depending on the answer to this question.
        /// </summary>
        public AnsweredQuestions ConditionalAnsweredQuestions
        {
            get { return _conditionalAnsweredQuestions; }
            set { _conditionalAnsweredQuestions = value; }
        }

        /// <summary>
        /// The photo property is only applicable for photo questions
        /// </summary>
        public byte[] Photo
        {
            get { return _photo; }
            set { _photo = value; }
        }
        #endregion

        #region Constructor
        public Answer() { }
        public Answer(int questionId, string answer)
        {
            _questionId = questionId;
            _answer = answer;
        }
        public Answer(Answer answer)
        {
            _questionId = answer._questionId;
            _answer = answer._answer;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _questionId = BaseLayer.Read4ByteInteger(data, ref index);
            _answer = BaseLayer.ReadString(data, ref index, 0x12);

            if (versionNumber >= 2)
            {
                int count = BaseLayer.Read2ByteInteger(data, ref index);
                _photo = new byte[count];
                Array.Copy(data, index, _photo, 0, count);
                index += count;

                bool linkAnswersPresent = BaseLayer.ReadBool(data, ref index);
                if (linkAnswersPresent)
                {
                    _conditionalAnsweredQuestions = new AnsweredQuestions();
                    _conditionalAnsweredQuestions.Decode(data, ref index);
                }
            }
            else
            {
                _photo = null;
                _conditionalAnsweredQuestions = null;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _questionId);
                BaseLayer.WriteString(local, _answer, 0x12);

                //write photo
                if (_photo != null)
                {
                    BaseLayer.Write2ByteInteger(local, _photo.Length);
                    if (_photo.Length > 0)
                    {
                        local.Write(_photo, 0, _photo.Length);
                    }
                }
                else
                {
                    BaseLayer.Write2ByteInteger(local, 0);
                }
                // If this answer references a conditional list of answered questions 
                // then these also need to be encoded
                // This will be recursively called if the child list also contains conditional lists
                if (_conditionalAnsweredQuestions != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _conditionalAnsweredQuestions.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public enum TripTemplateTypes
    {
        Questions = 0,
        TrackableAssetDeclaration,
        Inspection,
        Signature,
        Incident,
        NonDrivingWork = 5,
    }

    /// <summary>
    /// a question Item
    /// </summary>
    public class TripTemplateItem : ListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;

        /// <summary>
        /// the trip type
        /// </summary>
        private TripTemplateTypes _tripTemplateType;
        /// <summary>
        /// is item list id
        /// </summary>
        private ListId _itemListId;
        private ListId _yesIncidentList;
        private ListId _noIncidentList;
        private int? _yesIncidentUniqueKey;
        private int? _noIncidentUniqueKey;
        #endregion

        #region properties
        /// <summary>
        /// the trip template type
        /// </summary>
        public TripTemplateTypes TripTemplateType
        {
            get { return _tripTemplateType; }
            set { _tripTemplateType = value; }
        }
        /// <summary>
        /// The item List id
        /// </summary>
        public ListId ItemListId
        {
            get { return _itemListId; }
            set { _itemListId = value; }
        }
        public ListId YesIncidentList
        {
            get { return _yesIncidentList; }
            set { _yesIncidentList = value; }
        }
        public ListId NoIncidentList
        {
            get { return _noIncidentList; }
            set { _noIncidentList = value; }
        }
        public int? YesIncidentUniqueKey
        {
            get { return _yesIncidentUniqueKey; }
            set { _yesIncidentUniqueKey = value; }
        }
        public int? NoIncidentUniqueKey
        {
            get { return _noIncidentUniqueKey; }
            set { _noIncidentUniqueKey = value; }
        }
        #endregion

        #region constructor
        public TripTemplateItem()
        {
            _itemListId = new ListId();
        }
        public TripTemplateItem(int id, string name, TripTemplateTypes tripTemplateType)
            : base(id, name)
        {
            _itemListId = new ListId(-1, -1);
            _tripTemplateType = tripTemplateType;
        }
        public TripTemplateItem(int id, string name, TripTemplateTypes tripTemplateType, ListId itemListId)
            : base(id, name)
        {
            _itemListId = itemListId;
            _tripTemplateType = tripTemplateType;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _tripTemplateType = (TripTemplateTypes)data[index++];

            _itemListId = new ListId();
            _itemListId.Decode(data, ref index);

            _yesIncidentList = null;
            _noIncidentList = null;
            _yesIncidentUniqueKey = null;
            _noIncidentUniqueKey = null;
            if (versionNumber >= 2)
            {
                if (_tripTemplateType == TripTemplateTypes.Incident)
                {
                    if (BaseLayer.ReadBool(data, ref index))
                    {
                        _yesIncidentList = new ListId();
                        _yesIncidentList.Decode(data, ref index);
                    }
                    if (BaseLayer.ReadBool(data, ref index))
                    {
                        _noIncidentList = new ListId();
                        _noIncidentList.Decode(data, ref index);
                    }
                    _yesIncidentUniqueKey = BaseLayer.Read4ByteNullableInteger(data, ref index);
                    _noIncidentUniqueKey = BaseLayer.Read4ByteNullableInteger(data, ref index);
                }
            }
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                local.WriteByte((byte)_tripTemplateType);
                if (_itemListId == null)
                {
                    _itemListId = new ListId(-1, -1);
                }
                _itemListId.Encode(local);

                if (_tripTemplateType == TripTemplateTypes.Incident)
                {
                    if (_yesIncidentList != null)
                    {
                        BaseLayer.WriteBool(local, true);
                        _yesIncidentList.Encode(local);
                    }
                    else
                    {
                        BaseLayer.WriteBool(local, false);
                    }
                    if (_noIncidentList != null)
                    {
                        BaseLayer.WriteBool(local, true);
                        _noIncidentList.Encode(local);
                    }
                    else
                    {
                        BaseLayer.WriteBool(local, false);
                    }
                    BaseLayer.Write4ByteNullableInteger(local, _yesIncidentUniqueKey);
                    BaseLayer.Write4ByteNullableInteger(local, _noIncidentUniqueKey);
                }
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

    }
}

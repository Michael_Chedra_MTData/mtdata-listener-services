using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class PersonItem : AddressItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private string _firstName;
        private string _lastName;
        #endregion

        #region properties
        public string FirstName
        {
            get { return _firstName; }
            set { _firstName = value; }
        }
        public string LastName
        {
            get { return _lastName; }
            set { _lastName = value; }
        }
        #endregion

        #region Constructor
        public PersonItem() { }
        public PersonItem(int id, string name, string address, string note, double latitude, double longitude, int radius, string firstName, string lastName, string externalRef)
            : base(id, name, address, note, latitude, longitude, radius)
        {
            _firstName = firstName;
            _lastName = lastName;
            ExternalRef = externalRef;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _firstName = BaseLayer.ReadString(data, ref index, 0x12);
            _lastName = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _firstName, 0x12);
                BaseLayer.WriteString(local, _lastName, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

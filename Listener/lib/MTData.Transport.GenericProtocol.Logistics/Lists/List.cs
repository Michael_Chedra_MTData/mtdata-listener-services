using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class List : IList
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;

        private ListId _id;
        private ListTypes _listType;
        private List<IListItem> _listItems;
        #endregion

        #region constructor
        public List() : this(ListTypes.Unknown) { }
        public List(ListTypes type)
        {
            _listItems = new List<IListItem>();
            _id = new ListId();
            _listType = type;
        }
        #endregion

        #region IList Members

        public ListId Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public ListTypes ListType
        {
            get { return _listType; }
            set { _listType = value; }
        }

        public List<IListItem> ListItems
        {
            get { return _listItems; }
        }

        public IListItem this[string name]
        {
            get
            {
                foreach (IListItem item in _listItems)
                {
                    if (item.Name.Equals(name))
                    {
                        return item;
                    }
                }
                throw new Exception("No List item with name " + name);
            }
        }

        public IListItem this[int itemId]
        {
            get
            {
                foreach (IListItem item in _listItems)
                {
                    if (item.ItemId == itemId)
                    {
                        return item;
                    }
                }
                throw new Exception("No List item with id " + itemId);
            }
        }
        #endregion

        #region IPacketEncode members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _listType = (ListTypes)data[index++];
            _id.Decode(data, ref index);
            _listItems.Clear();
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                IListItem item = CreateNewItem();
                if ((versionNumber == 1 && _listType == ListTypes.Parts) || (versionNumber <= 2 && _listType == ListTypes.PalletTypes))
                {
                    ListItem oldItem = new ListItem();
                    oldItem.Decode(data, ref index);
                    ((ListItem)item).ExternalRef = oldItem.ExternalRef;
                    ((ListItem)item).ItemId = oldItem.ItemId;
                    ((ListItem)item).Name = oldItem.Name;
                }
                else
                {
                    item.Decode(data, ref index);
                }
                _listItems.Add(item);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_listType);
                _id.Encode(local);
                BaseLayer.Write2ByteInteger(local, _listItems.Count);
                foreach (IListItem item in _listItems)
                {
                    item.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return _listItems.GetEnumerator();
        }

        #endregion

        #region private methods
        private IListItem CreateNewItem()
        {
            switch (_listType)
            {
                case ListTypes.FleetAddressBook:
                case ListTypes.DriverAddressBook:
                case ListTypes.Customers:
                    return new AddressItem();
                case ListTypes.FuelStations:
                    return new FuelStationItem();
                case ListTypes.FleetPhoneBook:
                case ListTypes.DriverPhoneBook:
                    return new PhoneItem();
                case ListTypes.Questions:
                    return new QuestionItem();
                case ListTypes.Trailers:
                    return new TrailerItem();
                case ListTypes.TransferStations:
                    return new TransferStationItem();
                case ListTypes.Person:
                    return new PersonItem();
                case ListTypes.PassengerStop:
                    return new PassengerStopItem();
                case ListTypes.Organisation:
                    return new OrganisationItem();
                case ListTypes.JobTemplate:
                    return new JobTemplateItem();
                case ListTypes.Parts:
                    return new PartItem();
                case ListTypes.PalletTypes:
                    return new PalletTypeItem();
                case ListTypes.MassScheme:
                    return new MassSchemeItem();
                case ListTypes.Products:
                    return new ProductItem();
                case ListTypes.Poi:
                    return new PoiListItem();
                case ListTypes.Category:
                    return new CategoryItem();
                case ListTypes.DefectTemplate:
                    return new DefectTemplateItem();
                case ListTypes.DefectType:
                    return new DefectTypeItem();
                case ListTypes.IncidentTemplate:
                    return new IncidentTemplateItem();
                case ListTypes.TrackableAsset:
                    return new TrackableAssetListItem();
                case ListTypes.VehicleUsages:
                    return new VehicleUsageItem();
                case ListTypes.PreTripTemplate:
                case ListTypes.PostTripTemplate:
                    return new TripTemplateItem();
                case ListTypes.DefectSeverity:
                    return new DefectSeverityItem();
                case ListTypes.FatigueAlert:
                    return new FatigueAlertItem();
                default:
                    return new ListItem();
            }
        }
        #endregion

    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class ListDefaultList : IPacketEncode
    {
        #region Private Members
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private ListFleetDefaultsMultiTypes _type;
        private List<ListId> _lists;

        #endregion

        #region Constructor

        public ListDefaultList()
        {
            _type = ListFleetDefaultsMultiTypes.RuleSets;
            _lists = new List<ListId>();
        }

        public ListDefaultList(ListFleetDefaultsMultiTypes type)
        {
            _type = type;
            _lists = new List<ListId>();
        }

        #endregion

        #region Public Properties

        public ListFleetDefaultsMultiTypes Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public List<ListId> Lists
        {
            get { return _lists; }
            set { _lists = value; }
        }

        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;
            _type = (ListFleetDefaultsMultiTypes)data[index++];
            _lists.Clear();
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                ListId list = new ListId();
                list.Decode(data, ref index);                
                _lists.Add(list);
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_type);
                BaseLayer.Write2ByteInteger(local, _lists.Count);
                foreach (ListId listId in _lists)
                {
                    listId.Encode(local);
                }
                
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);                
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class ListItem : IListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;

        private int _itemId;
        private string _name;
        private string _externalRef;
        private int _uniqueKey;
        #endregion

        #region Constructor
        public ListItem() { }
        public ListItem(int id, string name)
        {
            _itemId = id;
            _name = name;
        }
        public ListItem(int id, string name, string externalRef)
        {
            _itemId = id;
            _name = name;
            _externalRef = externalRef;
        }
        public ListItem(int id, string name, string externalRef, int uniqueKey)
        {
            _itemId = id;
            _name = name;
            _externalRef = externalRef;
            _uniqueKey = uniqueKey;
        }
        #endregion

        #region IListItem Members

        public int ItemId
        {
            get { return _itemId; }
            set { _itemId = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string ExternalRef
        {
            get { return _externalRef; }
            set { _externalRef = value; }
        }

        public int UniqueKey
        {
            get { return _uniqueKey; }
            set { _uniqueKey = value; }
        }
        #endregion

        #region IPacketEncode members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _itemId = BaseLayer.Read4ByteInteger(data, ref index);
            _name = BaseLayer.ReadString(data, ref index, 0x12);
            _externalRef = null;
            if (versionNumber >= 2)
            {
                _externalRef = BaseLayer.ReadString(data, ref index, 0x12);
            }
            if (versionNumber >= 3)
            {
                _uniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            }
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _itemId);
                BaseLayer.WriteString(local, _name, 0x12);
                BaseLayer.WriteString(local, _externalRef, 0x12);
                BaseLayer.Write4ByteInteger(local, _uniqueKey);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

        #region IComparable<ListItem> Members

        public int CompareTo(IListItem other)
        {
            return Name.CompareTo(other.Name);
        }

        #endregion
    }
}

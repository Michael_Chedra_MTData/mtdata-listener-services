using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// enum for trackable asset list types
    /// </summary>
    public enum TrackableAssetListTypes
    {
        Undefined = -1,
        Generic = 0,
        Bin,
        Container,
        Trailer
    }

    /// <summary>
    /// class represents a trackable asset list
    /// </summary>
    public class TrackableAssetList : List
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;

        private TrackableAssetListTypes _trackableAssetType;
        private int? _defectTemplateListId;
        private int? _defectTemplateUniqueKey;
        #endregion

        #region properties
        public TrackableAssetListTypes TrackableAssetType
        {
            get { return _trackableAssetType; }
            set { _trackableAssetType = value; }
        }

        public int? DefectTemplateListId
        {
            get { return _defectTemplateListId; }
            set { _defectTemplateListId = value; }
        }

        public int? DefectTemplateUniqueKey
        {
            get { return _defectTemplateUniqueKey; }
            set { _defectTemplateUniqueKey = value; }
        }
        #endregion

        #region Constructor
        public TrackableAssetList ()
            : base(ListTypes.TrackableAsset)
        {
            _trackableAssetType = TrackableAssetListTypes.Generic;
        }
        #endregion

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _trackableAssetType = (TrackableAssetListTypes)data[index++];

            int fieldsFlags = 0;
            if (versionNumber >= 2)
            {
                fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
                _defectTemplateListId = ((fieldsFlags & (1 << 0)) != 0) ? (int?)BaseLayer.Read4ByteInteger(data, ref index) : null;
                _defectTemplateUniqueKey = ((fieldsFlags & (1 << 1)) != 0) ? (int?)BaseLayer.Read4ByteInteger(data, ref index) : null;
            }
            else
            {
                _defectTemplateListId = null;
                _defectTemplateUniqueKey = null;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {

                base.Encode(local);
                local.WriteByte((byte)_trackableAssetType);

                // Added version 2
                int fieldsFlags = (_defectTemplateListId.HasValue ? 1 : 0)
                    | (_defectTemplateUniqueKey.HasValue ? 1 << 1 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                if (_defectTemplateListId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _defectTemplateListId.Value);
                }
                if (_defectTemplateUniqueKey.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _defectTemplateUniqueKey.Value);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class TripAnswerSegment : SegmentLayer
    {
        #region private fields
        private bool _preTrip;
        private AnsweredQuestions _answers;
        #endregion

        #region properties
        public bool PreTrip
        {
            get { return _preTrip; }
            set { _preTrip = value; }
        }
        public AnsweredQuestions Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }
        #endregion

        #region constructor
        public TripAnswerSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.TripAnswers;
            _answers = new AnsweredQuestions();
        }
        public TripAnswerSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.TripAnswers)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.TripAnswers, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.TripAnswers;
            _answers = new AnsweredQuestions();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {

                WriteBool(stream, _preTrip);
                _answers.Encode(stream);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _preTrip = ReadBool(Data, ref index);
            _answers.Decode(Data, ref index);
        }
        #endregion
    }
}

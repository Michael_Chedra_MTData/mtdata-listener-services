﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class DefectTemplateItem : ListItem
    {
        private int _versionNumber = 2;

        private int _defectTypeListID;

        private int _questionListId;

        private int _inspectionQuestionListId;

        public int DefectTypeListID
        {
            get { return _defectTypeListID; }
            set { _defectTypeListID = value; }
        }

        public int QuestionListId
        {
            get { return _questionListId; }
            set { _questionListId = value; }
        }

        public int InspectionQuestionListId
        {
            get { return _inspectionQuestionListId; }
            set { _inspectionQuestionListId = value; }
        }


        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            base.Decode(data, ref index);

            DefectTypeListID = BaseLayer.Read4ByteInteger(data, ref index);

            QuestionListId = BaseLayer.Read4ByteInteger(data, ref index);

            if(versionNumber >= 2)
            {
                InspectionQuestionListId = BaseLayer.Read4ByteInteger(data, ref index);
            }

            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                BaseLayer.Write4ByteInteger(local, DefectTypeListID);
                BaseLayer.Write4ByteInteger(local, QuestionListId);

                if (_versionNumber >= 2)
                {
                    BaseLayer.Write4ByteInteger(local, InspectionQuestionListId);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

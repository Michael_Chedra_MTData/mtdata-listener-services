﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{

#if !NETCF
    [System.Diagnostics.DebuggerDisplay("Action: {Action} Id: {ItemId} Name: {Name} Phone: {PhoneNumber}")]
#endif

    public class EditPhoneItem : PhoneItem
    {
        public enum PhoneEditAction : byte
        {
            None = 0,
            Insert,
            Update,
            Delete
        }

        private int _versionNumber = 1;

        private PhoneEditAction _action;

        public PhoneEditAction Action
        {
            get { return _action; }
            set { _action = value; }
        }

        public EditPhoneItem()
        {

        }

        public EditPhoneItem(PhoneEditAction action, PhoneItem item)
        {
            Action = action;
            ItemId = item.ItemId;
            Name = item.Name;
            PhoneNumber = item.PhoneNumber;
            ExternalRef = item.ExternalRef;
            UniqueKey = item.UniqueKey;
        }

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _action = (PhoneEditAction)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                local.WriteByte((byte)_action);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }


}

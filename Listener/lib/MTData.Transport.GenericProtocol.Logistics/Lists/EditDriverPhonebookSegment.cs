﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class EditDriverPhonebookSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.EditDriverPhonebook;

        private List<EditPhoneItem> _phonebookItems = new List<EditPhoneItem>();

        public List<EditPhoneItem> PhonebookItems
        {
            get { return _phonebookItems; }
            set { _phonebookItems = value; }
        }

        public EditDriverPhonebookSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }


        public EditDriverPhonebookSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, PhonebookItems.Count);
                foreach (EditPhoneItem item in PhonebookItems)
                {
                    item.Encode(stream);
                }


                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);

            List<EditPhoneItem> items = new List<EditPhoneItem>(count);

            for (int i = 0; i < count; i++)
            {
                EditPhoneItem item = new EditPhoneItem();
                item.Decode(Data, ref index);
                items.Add(item);
            }

            _phonebookItems = items;
        }
    }
}

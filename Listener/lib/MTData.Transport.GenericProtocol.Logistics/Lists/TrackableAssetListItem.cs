using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// class represents a trackable asset list
    /// </summary>
    public class TrackableAssetListItem : ListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 4;

        private int? _defectTemplateListId;
        private int? _defectTemplateUniqueKey;
        #endregion

        #region properties
        public int? DefectTemplateListId
        {
            get { return _defectTemplateListId; }
            set { _defectTemplateListId = value; }
        }

        public int? DefectTemplateUniqueKey
        {
            get { return _defectTemplateUniqueKey; }
            set { _defectTemplateUniqueKey = value; }
        }
        #endregion

        #region Constructor
        public TrackableAssetListItem()
        {
        }
        #endregion

        public override void Decode(byte[] data, ref int index)
        {
            int originalStart = index;
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            if (versionNumber < 4)
            {
                // This class is created from version 4.
                // Prior to version 4 only the base ListItem was encoded, so we need to revert the index and avoid reading the version and length twice.
                index = originalStart;
                base.Decode(data, ref index);
            }
            else
            {
                int startIndex = index;
                base.Decode(data, ref index);

                int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
                _defectTemplateListId = ((fieldsFlags & (1 << 0)) != 0) ? (int?)BaseLayer.Read4ByteInteger(data, ref index) : null;
                _defectTemplateUniqueKey = ((fieldsFlags & (1 << 1)) != 0) ? (int?)BaseLayer.Read4ByteInteger(data, ref index) : null;

                //skip any unread bytes
                int readBytes = index - startIndex;
                if (length > readBytes)
                {
                    index += length - readBytes;
                }
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {

                base.Encode(local);

                int fieldsFlags = (_defectTemplateListId.HasValue ? 1 : 0)
                    | (_defectTemplateUniqueKey.HasValue ? 1 << 1 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                if (_defectTemplateListId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _defectTemplateListId.Value);
                }
                if (_defectTemplateUniqueKey.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _defectTemplateUniqueKey.Value);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

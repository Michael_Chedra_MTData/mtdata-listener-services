using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// a trailer list item
    /// </summary>
    public class TrailerItem : ListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private byte[] _esn;
        #endregion

        #region properties
        public byte[] TrailerEsn
        {
            get { return _esn; }
            set { _esn = value; }
        }
        #endregion

        #region constructor
        public TrailerItem() { }
        public TrailerItem(int id, string name, byte[] esn) : base(id, name)
        {
            _esn = esn;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            base.Decode(data, ref index);
            _esn = new byte[6];
            Array.Copy(data, index, _esn, 0, 6);
            index += 6;

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                for (int i = 0; i < 6; i++)
                {
                    if (_esn == null || _esn.Length <= i)
                    {
                        local.WriteByte(0x00);
                    }
                    else
                    {
                        local.WriteByte(_esn[i]);
                    }
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// enum of cost types
    /// </summary>
    public enum CostType
    {
        Part = 0,
        Labour
    }

    public class PartItem : ListItem
    {
         #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private string _partCode;
        private string _description;
        private CostType _costType;
        private float _unitCost;
        private float _unitPrice;
        private float _taxRate;
        private ListId _questionList;
        #endregion

        #region properties
        public string PartCode
        {
            get { return _partCode; }
            set { _partCode = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public CostType PartCostType
        {
            get { return _costType; }
            set { _costType = value; }
        }
        public float UnitCost
        {
            get { return _unitCost; }
            set { _unitCost = value; }
        }
        public float UnitPrice
        {
            get { return _unitPrice; }
            set { _unitPrice = value; }
        }
        public float TaxRate
        {
            get { return _taxRate; }
            set { _taxRate = value; }
        }
        public ListId QuestionList
        {
            get { return _questionList; }
            set { _questionList = value; }
        }
        #endregion

        #region Constructor
        public PartItem() 
        {
            _questionList = new ListId();
        }
        public PartItem(int id, string name, string partCode, string description, CostType costType, float unitCost, float unitPrice, float taxRate, ListId questionlist)
            : base(id, name)
        {
            _partCode = partCode;
            _description = description;
            _costType = costType;
            _unitCost = unitCost;
            _unitPrice = unitPrice;
            _taxRate = taxRate;
            _questionList = questionlist;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _partCode = BaseLayer.ReadString(data, ref index, 0x12);
            _description = BaseLayer.ReadString(data, ref index, 0x12);
            _costType = (CostType)data[index++];
            int value = BaseLayer.Read4ByteInteger(data, ref index);
            _unitCost = value / 1000f;
            value = BaseLayer.Read4ByteInteger(data, ref index);
            _unitPrice = value / 1000f;
            value = BaseLayer.Read4ByteInteger(data, ref index);
            _taxRate = value / 1000f;
            _questionList.Decode(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _partCode, 0x12);
                BaseLayer.WriteString(local, _description, 0x12);
                local.WriteByte((byte)_costType);
                int value = (int)((_unitCost * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                value = (int)((_unitPrice * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                value = (int)((_taxRate * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                _questionList.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

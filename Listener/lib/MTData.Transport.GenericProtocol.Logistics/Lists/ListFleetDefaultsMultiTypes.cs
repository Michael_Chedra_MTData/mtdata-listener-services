﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public enum ListFleetDefaultsMultiTypes
    {
        Unknown = -1,
        RuleSets = 0,
        Poi = 1,
        TrackableAsset = 2,
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public enum CategoryTypes
    {
        Undefined = -1,
        Farm = 0,
        Hazard = 1,
        LevelCrossing = 2,
        Mine = 3,
        RestStop = 4,
        FuelStation = 5
    }

    public static class CategoryTypesHelper
    {
        public static string CategoryTypesToString(CategoryTypes categoryType)
        {
            switch(categoryType)
            {
                case CategoryTypes.FuelStation:
                    return "Fuel Station";
                case CategoryTypes.LevelCrossing:
                    return "Level Crossing";
                case CategoryTypes.RestStop:
                    return "Rest Stop";
                default:
                    return categoryType.ToString();
            }
        }
    }

    public class CategoryItem: ListItem
    {
        private int _version = 1;
        private CategoryTypes _categoryType;
        private bool _alert;
        private string _alertTextToSpeech;
        private int _alertDuration;
        private bool _alertAutoDismiss;
        private bool _massDeclaration;
        private bool _fuelStation;
        private bool _phoneCall;

        public CategoryItem()
        {
            _categoryType = CategoryTypes.Undefined;
        }

        public CategoryItem(int id, string name, CategoryTypes categoryType, bool alert, string alertTextToSpeech, int alertDuration, bool alertAutoDismiss, bool massDeclaration, bool fuelStation, bool phoneCall)
            : base(id,name)
        {
            _categoryType = categoryType;
            _alert = alert;
            _alertTextToSpeech = alertTextToSpeech;
            _alertDuration = alertDuration;
            _alertAutoDismiss = alertAutoDismiss;
            _massDeclaration = massDeclaration;
            _fuelStation = fuelStation;
            _phoneCall = phoneCall;
        }

        public CategoryTypes CategoryType
        {
            get { return _categoryType; }
            set { _categoryType = value; }
        }

        public bool Alert
        {
            get { return _alert; }
            set { _alert = value; }
        }

        public string AlertTextToSpeech
        {
            get { return _alertTextToSpeech; }
            set { _alertTextToSpeech = value; }
        }

        public int AlertDuration
        {
            get { return _alertDuration; }
            set { _alertDuration = value; }
        }

        public bool AlertAutoDismiss
        {
            get { return _alertAutoDismiss; }
            set { _alertAutoDismiss = value; }
        }

        public bool MassDeclaration
        {
            get { return _massDeclaration; }
            set { _massDeclaration = value; }
        }

        public bool FuelStation
        {
            get { return _fuelStation; }
            set { _fuelStation = value; }
        }

        public bool PhoneCall
        {
            get { return _phoneCall; }
            set { _phoneCall = value; }
        }

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _categoryType = (CategoryTypes)data[index++];
            _alert = BaseLayer.ReadBool(data, ref index);
            _alertTextToSpeech = BaseLayer.ReadString(data, ref index, 0x12);
            _alertDuration = BaseLayer.Read4ByteInteger(data, ref index);
            _alertAutoDismiss = BaseLayer.ReadBool(data, ref index);
            _massDeclaration = BaseLayer.ReadBool(data, ref index);
            _fuelStation = BaseLayer.ReadBool(data, ref index);
            _phoneCall = BaseLayer.ReadBool(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);                
                local.WriteByte((byte)_categoryType);
                BaseLayer.WriteBool(local, _alert);
                BaseLayer.WriteString(local, _alertTextToSpeech, 0x12);
                BaseLayer.Write4ByteInteger(local, _alertDuration);
                BaseLayer.WriteBool(local, _alertAutoDismiss);
                BaseLayer.WriteBool(local, _massDeclaration);
                BaseLayer.WriteBool(local, _fuelStation);
                BaseLayer.WriteBool(local, _phoneCall);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _version);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

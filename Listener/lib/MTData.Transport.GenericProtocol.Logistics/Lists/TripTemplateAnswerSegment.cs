using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class TripTemplateAnswerSegment : SegmentLayer
    {
        #region private fields
        private bool _preTrip;
        private List<TripTemplateItemAnswer> _answers;
        #endregion

        #region properties
        public bool PreTrip
        {
            get { return _preTrip; }
            set { _preTrip = value; }                           
        }
        public List<TripTemplateItemAnswer> Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }
        #endregion

        #region constructor
        public TripTemplateAnswerSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.TripTemplateAnswer;
            _answers = new List<TripTemplateItemAnswer>();
        }
        public TripTemplateAnswerSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.TripTemplateAnswer)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.TripTemplateAnswer, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.TripTemplateAnswer;
            _answers = new List<TripTemplateItemAnswer>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {

                WriteBool(stream, _preTrip);
                Write2ByteInteger(stream, _answers.Count);
                foreach(TripTemplateItemAnswer answer in _answers)
                {
                    answer.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _preTrip = ReadBool(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            for (int i=0; i<count; i++)
            {
                TripTemplateItemAnswer item = new TripTemplateItemAnswer();
                item.Decode(Data, ref index);
                _answers.Add(item);
            }
        }
        #endregion
    }
}

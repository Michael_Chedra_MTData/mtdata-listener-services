using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// enum for list types
    /// </summary>
    public enum ListTypes
    {
        Unknown = 0,
        Division,
        DelayReasons,
        RejectReasons,
        PredefinedMessages,
        PalletTypes,
        Trailers,
        VehicleUsages,
        FuelStations,
        Questions,
        FleetPhoneBook,
        DriverPhoneBook,
        FleetAddressBook,
        DriverAddressBook,
        FleetEmailAddressBook,
        DriverEmailAddressBook,
        DriverTypes,
        LoadTypes,
        LoginTypes,
        TransferStations,
        TagLocations,
        Customers,
        ActualProblem,
        ActionTaken,
        TowingService,
        Parts,
        Person,
        PassengerStop,
        Organisation,
        JobTemplate,
        JobList,
        MassScheme,
        RuleSets,
        Products,
        Category,
        Poi,
        VehicleProfile,
        TrackableAsset,
        WaypointCategory,
        SjpTemplate,
        DefectTemplate,
        DefectType,
        IncidentTemplate,
        DefectSeverity,
        PreTripTemplate,
        PostTripTemplate,
        FuelTypes,
        FatigueAlert,
        CostCentre,
        AssetBookingLocations
    }

    /// <summary>
    /// interface for a list
    /// </summary>
    public interface IList : IEnumerable, IPacketEncode
    {
        /// <summary>
        /// the list id and version
        /// </summary>
        ListId Id { get; }
        /// <summary>
        /// the lsit type
        /// </summary>
        ListTypes ListType { get; }
        /// <summary>
        /// list items
        /// </summary>
        List<IListItem> ListItems { get; }

        /// <summary>
        /// get the list item that has the given name, if none found an exception is thrown
        /// </summary>
        /// <param name="name">the name of the item to find</param>
        /// <returns>the found item</returns>
        /// <exception cref="Exception">if no item of name is in the list</exception>
        IListItem this[string name] { get; }

        /// <summary>
        /// get the list item that has the given item id, if none found an exception is thrown
        /// </summary>
        /// <param name="itemId">the id of the item to find</param>
        /// <returns>the found item</returns>
        /// <exception cref="Exception">if no item with id is in the list</exception>
        IListItem this[int itemId] { get; }
    }
}

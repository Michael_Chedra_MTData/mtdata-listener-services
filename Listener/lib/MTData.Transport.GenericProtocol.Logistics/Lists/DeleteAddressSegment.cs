using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class DeleteAddressSegment : SegmentLayer
    {
        #region private fields
        private int _addressId;
        #endregion

        #region properties
        public int AddressId
        {
            get { return _addressId; }
            set { _addressId = value; }
        }
        #endregion

        #region constructor
        public DeleteAddressSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AddressDelete;
        }
        public DeleteAddressSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AddressDelete)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AddressDelete, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AddressDelete;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _addressId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _addressId = Read4ByteInteger(Data, ref index);
        }
        #endregion

    }
}

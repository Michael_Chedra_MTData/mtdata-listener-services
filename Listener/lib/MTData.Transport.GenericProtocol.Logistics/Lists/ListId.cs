using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// class to hold the list id and version
    /// </summary>
    [System.Diagnostics.DebuggerDisplay("Id: {Id}, Version: {Version}")]

    public class ListId : IPacketEncode
    {
        #region private fields
        private int _listId;
        private int _version;
        #endregion

        #region constructor
        public ListId() { }
        public ListId(int listId, int version)
        {
            _listId = listId;
            _version = version;
        }
        public ListId(ListId listId)
        {
            _listId = listId._listId;
            _version = listId._version;
        }
        #endregion

        #region properties
        public int Id { get { return _listId; } set { _listId = value; } }
        public int Version { get { return _version; } set { _version = value; } }
        #endregion

        #region IPacketEncode members
        /// <summary>
        /// encode the list into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        public void Encode(System.IO.MemoryStream stream)
        {
            BaseLayer.Write4ByteInteger(stream, _listId);
            BaseLayer.Write4ByteInteger(stream, _version);
        }

        /// <summary>
        /// decode the list from a byte[]
        /// </summary>
        /// <param name="Data">byte[] containing the list parameters to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        public void Decode(byte[] data, ref int index)
        {
            _listId = BaseLayer.Read4ByteInteger(data, ref index);
            _version = BaseLayer.Read4ByteInteger(data, ref index);
        }
        #endregion

        public override bool Equals(object obj)
        {
            ListId other = obj as ListId;
            if (other != null)
            {
                if (_listId == other._listId && _version == other._version)
                {
                    return true;
                }
            }
            return false;
        }

        public override int GetHashCode()
        {
            int hash = 17;
            hash = hash * 23 + _listId.GetHashCode();
            hash = hash * 23 + _version.GetHashCode();
            return hash;
        }
    }
}

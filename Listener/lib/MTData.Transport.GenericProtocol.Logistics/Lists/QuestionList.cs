using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// enum for question list types
    /// </summary>
    public enum QuestionListTypes
    {
        Undefined = -1,
        Generic = 0,
        PreTrip,
        PostTrip,
        Pickup,
        Delivery,
        Inspection
    }

    /// <summary>
    /// class represents a question list
    /// </summary>
    public class QuestionList : List
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private bool _signatureRequired;
        private QuestionListTypes _questionListType;
        #endregion

        #region properties
        public bool SignatureRequired
        {
            get { return _signatureRequired; }
            set { _signatureRequired = value; }
        }
        public QuestionListTypes QuestionListType
        {
            get { return _questionListType; }
            set { _questionListType = value; }
        }
        #endregion

        #region Constructor
        public QuestionList()
            : base(ListTypes.Questions)
        {
            _questionListType = QuestionListTypes.Generic;
        }
        public QuestionList(bool signatureRequired)
            : base(ListTypes.Questions)
        {
            _signatureRequired = signatureRequired;
            _questionListType = QuestionListTypes.Generic;
        }
        #endregion

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _signatureRequired = BaseLayer.ReadBool(data, ref index);
            _questionListType = (QuestionListTypes)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {

                base.Encode(local);
                BaseLayer.WriteBool(local, _signatureRequired);
                local.WriteByte((byte)_questionListType);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class FatigueAlertItem : ListItem
    {
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        public int DurationBeforeSec { get; set; }

        public bool DisplayAlert { get; set; }

        public string DisplayText { get; set; }

        public bool PlayAudio { get; set; }

        public string AudioText { get; set; }

        public int AutoDismiss { get; set; }

        public List<int> PeriodTypes { get; set; } = new List<int>();

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            DurationBeforeSec = BaseLayer.Read4ByteInteger(data, ref index);
            DisplayAlert = BaseLayer.ReadBool(data, ref index);
            DisplayText = BaseLayer.ReadString(data, ref index, 0x12);
            PlayAudio = BaseLayer.ReadBool(data, ref index);
            AudioText = BaseLayer.ReadString(data, ref index, 0x12);
            AutoDismiss = BaseLayer.Read4ByteInteger(data, ref index);

            int count = BaseLayer.ReadMoreFlag(data, ref index);
            var periodTypes = new List<int>(count);
            int ptid;

            for (int i = 0; i < count; i++)
            {
                ptid = BaseLayer.ReadMoreFlag(data, ref index);
                periodTypes.Add(ptid);
            }

            this.PeriodTypes = periodTypes;

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                base.Encode(local);

                BaseLayer.Write4ByteInteger(local, DurationBeforeSec);
                BaseLayer.WriteBool(local, DisplayAlert);
                BaseLayer.WriteString(local, DisplayText, 0x12);

                BaseLayer.WriteBool(local, PlayAudio);
                BaseLayer.WriteString(local, AudioText, 0x12);
                BaseLayer.Write4ByteInteger(local, AutoDismiss);

                BaseLayer.WriteMoreFlag(local, PeriodTypes?.Count ?? 0);

                if (PeriodTypes != null)
                {
                    foreach (var ptid in PeriodTypes)
                    {
                        BaseLayer.WriteMoreFlag(local, ptid);
                    }
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

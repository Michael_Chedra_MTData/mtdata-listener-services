﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class DefectTypeItem : ListItem
    {
        private int _versionNumber = 3;

        private int _parentItemUniqueID;
        private int _severityItemUniqueID;
        private bool? _inspectable;

        public int ParentItemUniqueID
        {
            get { return _parentItemUniqueID; }
            set { _parentItemUniqueID = value; }
        }

        public int SeverityItemUniqueID
        {
            get { return _severityItemUniqueID; }
            set { _severityItemUniqueID = value; }
        }

        public bool? Inspectable
        {
            get { return _inspectable; }
            set { _inspectable = value; }
        }

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            base.Decode(data, ref index);

            _parentItemUniqueID = BaseLayer.Read4ByteInteger(data, ref index);

            if (versionNumber > 1)
            {
                _severityItemUniqueID = BaseLayer.Read4ByteInteger(data, ref index);
            }

            if (versionNumber >= 3)
            {
                _inspectable = BaseLayer.ReadNullableBool(data, ref index);
            }

            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write4ByteInteger(local, _parentItemUniqueID);

                if (_versionNumber > 1)
                {
                    BaseLayer.Write4ByteInteger(local, _severityItemUniqueID);
                }
                
                if (_versionNumber >= 3)
                {
                    BaseLayer.WriteNullableBool(local, _inspectable);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

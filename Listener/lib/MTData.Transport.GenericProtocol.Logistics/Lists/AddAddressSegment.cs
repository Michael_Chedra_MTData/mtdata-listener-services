using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class AddAddressSegment : SegmentLayer
    {
        #region private fields
        private AddressItem _addressItem;
        #endregion

        #region properties
        public AddressItem AddressItem
        {
            get { return _addressItem; }
            set { _addressItem = value; }
        }
        #endregion

        #region constructor
        public AddAddressSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AddressNew;
            _addressItem = new AddressItem();
        }
        public AddAddressSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AddressNew)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AddressNew, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AddressNew;
            _addressItem = new AddressItem();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                _addressItem.Encode(stream);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _addressItem.Decode(Data, ref index);
        }
        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class AddressItem : ListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;

        private string _address;
        private string _note;
        private Location _location;
        #endregion

        #region properties
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }
        public double Latitude
        {
            get { return _location.CentrePoint.Latitude; }
            set { _location.CentrePoint.Latitude = value; }
        }
        public double Longitude
        {
            get { return _location.CentrePoint.Longitude; }
            set { _location.CentrePoint.Longitude = value; }
        }
        public int Radius
        {
            get { return _location.Radius; }
            set { _location.Radius = value; }
        }
        public Location Location
        {
            get { return _location; }
            set { _location = value; }
        }

        #endregion

        #region Constructor
        public AddressItem()
        {
            _location = new Location();
        }
        public AddressItem(int id, string name, string address, string note, double latitude, double longitude, int radius) 
            : base (id, name)
        {
            _address = address;
            _note = note;
            _location = new Location(LocationTypes.Circle);
            _location.CentrePoint = new LocationPoint(latitude, longitude);
            _location.Radius = radius;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _address = BaseLayer.ReadString(data, ref index, 0x12);
            _note = BaseLayer.ReadString(data, ref index, 0x12);
            double latitude = BaseLayer.ReadCoordinate(data, ref index);
            double longitude = BaseLayer.ReadCoordinate(data, ref index);
            int radius = 0;
            if (versionNumber >= 2)
            {
                radius = BaseLayer.Read4ByteInteger(data, ref index);
            }

            if (versionNumber >= 3)
            {
                _location = new Location();
                _location.Decode(data, ref index);
            }
            else
            {
                _location = new Location(LocationTypes.Circle);
                _location.CentrePoint = new LocationPoint(latitude, longitude);
                _location.Radius = radius;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _address, 0x12);
                BaseLayer.WriteString(local, _note, 0x12);
                //leave these 3 fields for backwards compatabilty
                BaseLayer.WriteCoordinate(local, _location.CentrePoint.Latitude);
                BaseLayer.WriteCoordinate(local, _location.CentrePoint.Longitude);
                BaseLayer.Write4ByteInteger(local, _location.Radius);
                _location.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

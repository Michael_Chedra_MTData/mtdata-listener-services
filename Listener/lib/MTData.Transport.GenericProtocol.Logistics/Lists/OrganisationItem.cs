using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class OrganisationItem : AddressItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private List<ListId> _personLists;
        #endregion

        #region properties
        public List<ListId> PersonList
        {
            get { return _personLists; }
            set { _personLists = value; }
        }
        #endregion

        #region Constructor
        public OrganisationItem()
        {
            _personLists = new List<ListId>();
        }
        public OrganisationItem(int id, string name, string address, string note, double latitude, double longitude, int radius, List<ListId> personList)
            : base(id, name, address, note, latitude, longitude, radius)
        {
            _personLists = personList;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                ListId id = new ListId();
                id.Decode(data, ref index);
                _personLists.Add(id);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write2ByteInteger(local, _personLists.Count);
                foreach (ListId id in _personLists)
                {
                    id.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.TrackableAsset;
using MTData.Transport.GenericProtocol.Logistics.DefectReport;
using MTData.Transport.GenericProtocol.Logistics.IncidentReport;
using MTData.Transport.GenericProtocol.Logistics.Fatigue;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{

    /// <summary>
    /// class represents an answered trip template item
    /// </summary>
    public class TripTemplateItemAnswer : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;

        private int _templateItemId;
        private TripTemplateTypes _itemType;
        private AnsweredQuestions _questionAnswers;
        private Association _association;
        private Inspection _inspection;
        private byte[] _signature;
        private UnitCreatedIncident _incident;
        private bool? _incidentYesAnswer;
        private bool? _retroBreakYesAnswer;
        private RetroBreakDeclaration _retroBreak;
        #endregion

        #region properties
        public int TemplateItemId
        {
            get { return _templateItemId; }
            set { _templateItemId = value; }
        }
        public TripTemplateTypes ItemType
        {
            get { return _itemType; }
            set { _itemType = value; }
        }
        public AnsweredQuestions QuestionAnswers
        {
            get { return _questionAnswers; }
            set { _questionAnswers = value; }
        }
        public Association Association
        {
            get { return _association; }
            set { _association = value; }
        }
        public Inspection Inspection
        {
            get { return _inspection; }
            set { _inspection = value; }
        }
        public byte[] Signature
        {
            get { return _signature; }
            set { _signature = value; }
        }
        /// <summary>
        /// If question type is incident, then this is the incident created by Driver
        /// </summary>
        public UnitCreatedIncident Incident
        {
            get { return _incident; }
            set { _incident = value; }
        }
        /// <summary>
        /// If question type is incident, then if the driver answered yes value is true
        /// </summary>
        public bool? IncidentYesAnswer
        {
            get { return _incidentYesAnswer; }
            set { _incidentYesAnswer = value; }
        }

        public bool? RetroBreakYesAnswer
        {
            get { return _retroBreakYesAnswer; }
            set { _retroBreakYesAnswer = value; }
        }
        public RetroBreakDeclaration RetroBreak
        {
            get { return _retroBreak; }
            set { _retroBreak = value; }
        }

        #endregion

        #region Constructor
        public TripTemplateItemAnswer()
        {
            _questionAnswers = new AnsweredQuestions();
            _association = new Association();
            _inspection = new Inspection();
            _retroBreak = new RetroBreakDeclaration();
        }
        public TripTemplateItemAnswer(TripTemplateItem item)
        {
            _itemType = item.TripTemplateType;
            _templateItemId = item.ItemId;
            _questionAnswers = new AnsweredQuestions();
            _association = new Association();
            _inspection = new Inspection();
            _retroBreak = new RetroBreakDeclaration();
            if (_itemType == TripTemplateTypes.Questions)
            {
                _questionAnswers.QuestionListId = item.ItemListId;
            }
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _templateItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _itemType = (TripTemplateTypes)data[index++];

            switch (_itemType)
            {
                case TripTemplateTypes.Questions:
                    _questionAnswers.Decode(data, ref index);
                    break;
                case TripTemplateTypes.TrackableAssetDeclaration:
                    _association.Decode(data, ref index);
                    break;
                case TripTemplateTypes.Inspection:
                    _inspection.Decode(data, ref index);
                    break;
                case TripTemplateTypes.Signature:
                    int count = BaseLayer.Read2ByteInteger(data, ref index);
                    _signature = new byte[count];
                    Array.Copy(data, index, _signature, 0, count);
                    index += count;
                    break;
                case TripTemplateTypes.Incident:
                    if (versionNumber >= 2)
                    {
                        _incidentYesAnswer = BaseLayer.ReadNullableBool(data, ref index);
                        if (BaseLayer.ReadBool(data, ref index))
                        {
                            _incident = new UnitCreatedIncident();
                            _incident.Decode(data, ref index);
                        }
                    }
                    break;
                case TripTemplateTypes.NonDrivingWork:
                    if (versionNumber >= 3)
                    {
                        RetroBreakYesAnswer = BaseLayer.ReadNullableBool(data, ref index);
                        if (BaseLayer.ReadBool(data, ref index))
                        {
                            RetroBreak = new RetroBreakDeclaration();
                            RetroBreak.Decode(data, ref index);
                        }
                    }
                    break;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _templateItemId);
                local.WriteByte((byte)_itemType);
                switch (_itemType)
                {
                    case TripTemplateTypes.Questions:
                        _questionAnswers.Encode(local);
                        break;
                    case TripTemplateTypes.TrackableAssetDeclaration:
                        _association.Encode(local);
                        break;
                    case TripTemplateTypes.Inspection:
                        _inspection.Encode(local);
                        break;
                    case TripTemplateTypes.Signature:
                        if (_signature != null)
                        {
                            BaseLayer.Write2ByteInteger(local, _signature.Length);
                            if (_signature.Length > 0)
                            {
                                local.Write(_signature, 0, _signature.Length);
                            }
                        }
                        else
                        {
                            BaseLayer.Write2ByteInteger(local, 0);
                        }

                        break;
                    case TripTemplateTypes.Incident:
                        BaseLayer.WriteNullableBool(local, _incidentYesAnswer);
                        if (_incident != null)
                        {
                            BaseLayer.WriteBool(local, true);
                            _incident.Encode(local);
                        }
                        else
                        {
                            BaseLayer.WriteBool(local, false);
                        }
                        break;
                    case TripTemplateTypes.NonDrivingWork:
                        BaseLayer.WriteNullableBool(local, RetroBreakYesAnswer);
                        if (RetroBreak != null)
                        {
                            BaseLayer.WriteBool(local, true);
                            RetroBreak.Encode(local);
                        }
                        else
                        {
                            BaseLayer.WriteBool(local, false);
                        }
                        break;
                }
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion

    }
}

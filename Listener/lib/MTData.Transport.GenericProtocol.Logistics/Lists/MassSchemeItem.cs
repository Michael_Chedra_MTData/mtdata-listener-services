﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// A mass scheme item
    /// </summary>
    public class MassSchemeItem : ListItem
    {
        /// <summary>
        /// No Weight Limit set
        /// </summary>
        public static readonly int NO_LIMIT_SET = -1;
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private string _state;
        private string _scheme;
        private int _totalLimit;
        private List<MassVehicleSection> _sections;
        #endregion

        #region properties
        /// <summary>
        /// the state
        /// </summary>
        public string State
        {
            get { return _state; }
            set { _state = value; }
        }
        /// <summary>
        /// the scheme
        /// </summary>
        public string Scheme
        {
            get { return _scheme; }
            set { _scheme = value; }
        }

        /// <summary>
        /// is the total limit set
        /// </summary>
        public int TotalLimit
        {
            get { return _totalLimit; }
            set { _totalLimit = value; }
        }

        /// <summary>
        /// the vehicle sections
        /// </summary>
        public List<MassVehicleSection> VehicleSections
        {
            get { return _sections; }
            set { _sections = value; }
        }
        #endregion

        #region constructor
        public MassSchemeItem()
        {
            _totalLimit = NO_LIMIT_SET;
            _sections = new List<MassVehicleSection>();
        }
        public MassSchemeItem(int id, string name, string state, string scheme, int totalLimit)
            : base(id, name)
        {
            _state = state;
            _scheme = scheme;
            _totalLimit = totalLimit;
            _sections = new List<MassVehicleSection>();
        }


        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _state = BaseLayer.ReadString(data, ref index, 0x12);
            _scheme = BaseLayer.ReadString(data, ref index, 0x12);
            _totalLimit = BaseLayer.Read4ByteInteger(data, ref index);
            int count = (int)data[index++];
            for (int i = 0; i < count; i++)
            {
                MassVehicleSection section = new MassVehicleSection();
                section.Decode(data, ref index);
                _sections.Add(section);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _state, 0x12);
                BaseLayer.WriteString(local, _scheme, 0x12);
                BaseLayer.Write4ByteInteger(local, _totalLimit);
                local.WriteByte((byte)_sections.Count);
                for (int i = 0; i < _sections.Count; i++)
                {
                    _sections[i].Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

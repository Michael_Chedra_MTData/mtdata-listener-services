using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// class represents an answered question list
    /// </summary>
    public class AnsweredQuestions : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private ListId _questionListId;
        private byte[] _signature;
        private List<Answer> _answers;
        #endregion

        #region properties
        public ListId QuestionListId
        {
            get { return _questionListId; }
            set { _questionListId = value; }
        }
        public byte[] Signature
        {
            get { return _signature; }
            set { _signature = value; }
        }
        public List<Answer> Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }
        #endregion

        #region Constructor
        public AnsweredQuestions()
        {
            _questionListId = new ListId();
            _answers = new List<Answer>();
            _signature = new byte[0];
        }
        public AnsweredQuestions(AnsweredQuestions answers)
        {
            if (answers._questionListId != null)
            {
                _questionListId = new ListId(answers._questionListId);
            }
            if (answers._answers != null)
            {
                _answers = new List<Answer>();
                foreach (Answer a in answers._answers)
                {
                    _answers.Add(new Answer(a));
                }
            }
            if (answers._signature != null)
            {
                _signature = new byte[answers._signature.Length];
                Array.Copy(answers._signature, _signature, answers._signature.Length);
            }
            else
            {
                _signature = new byte[0];
            }
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _questionListId.Decode(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _answers.Clear();
            for (int i = 0; i < count; i++)
            {
                Answer a = new Answer();
                a.Decode(data, ref index);
                _answers.Add(a);
            }
            count = BaseLayer.Read2ByteInteger(data, ref index);
            _signature = new byte[count];
            Array.Copy(data, index, _signature, 0, count);
            index += count;

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                _questionListId.Encode(local);
                BaseLayer.Write2ByteInteger(local, _answers.Count);
                foreach (Answer a in _answers)
                {
                    a.Encode(local);
                }
                if (_signature != null)
                {
                    BaseLayer.Write2ByteInteger(local, _signature.Length);
                    if (_signature.Length > 0)
                    {
                        local.Write(_signature, 0, _signature.Length);
                    }
                }
                else
                {
                    BaseLayer.Write2ByteInteger(local, 0);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion

        #region public methods
        /// <summary>
        /// clear all answers and signature
        /// </summary>
        public void ClearAnswers()
        {
            _answers.Clear();
            _signature = null;
        }

        public void ClearAnswer(int questionId)
        {
            List<Answer> answers = GetMultiAnswers(questionId);
            foreach (Answer a in answers)
            {
                _answers.Remove(a);
            }
        }

        /// <summary>
        /// set the answer to a question. If the question is already answered it will replace the answer
        /// </summary>
        /// <param name="answer">the answer to a question</param>
        public void SetAnswer(Answer answer)
        {
            //check if the answer already exists
            Answer a = GetAnswer(answer.QuestionId);

            if (a != null)
            {
                int index = _answers.IndexOf(a);

                ClearAnswer(answer.QuestionId);
                _answers.Insert(index, answer);
            }
            else
            {
                _answers.Add(answer);
            }
        }

        public void AddMultiAnswer(Answer answer)
        {
            _answers.Add(answer);
        }

        /// <summary>
        /// Get the answer to a question
        /// </summary>
        /// <param name="questionId">the question id</param>
        /// <returns>the answer or null if the question is not answered</returns>
        public Answer GetAnswer(int questionId)
        {
            foreach (Answer a in _answers)
            {
                if (a.QuestionId == questionId)
                {
                    return a;
                }
            }
            return null;
        }

        public List<Answer> GetMultiAnswers(int questionId)
        {
            List<Answer> answers = new List<Answer>();
            foreach (Answer a in _answers)
            {
                if (a.QuestionId == questionId)
                {
                    answers.Add(a);
                }
            }
            return answers;
        }

        public void RemoveMultiAnswer(Answer answer)
        {
            _answers.Remove(answer);
        }

        #endregion
    }
}

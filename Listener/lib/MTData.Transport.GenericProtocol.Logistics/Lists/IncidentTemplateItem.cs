﻿using MTData.Transport.GenericProtocol.Logistics.IncidentReport;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class IncidentTemplateItem : ListItem
    {
        private int _versionNumber = 2;

        private int _questionListId;

        private string _instructions;

        private AssociationType _associationType;

        public int QuestionListId
        {
            get { return _questionListId; }
            set { _questionListId = value; }
        }

        public string Instructions
        {
            get { return _instructions; }
            set { _instructions = value; }
        }

        public AssociationType AssociationType
        {
            get { return _associationType; }
            set { _associationType = value; }
        }

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            base.Decode(data, ref index);

            QuestionListId = BaseLayer.Read4ByteInteger(data, ref index);

            Instructions = BaseLayer.ReadString(data, ref index, 0x12);

            if (versionNumber >= 2)
            {
                _associationType = (AssociationType)BaseLayer.ReadMoreFlag(data, ref index);
            }
            else
            {
                _associationType = AssociationType.Vehicle;
            }

            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                base.Encode(local);

                BaseLayer.Write4ByteInteger(local, QuestionListId);
                BaseLayer.WriteString(local, Instructions, 0x12);
                // Version 2
                BaseLayer.WriteMoreFlag(local, (int)_associationType);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

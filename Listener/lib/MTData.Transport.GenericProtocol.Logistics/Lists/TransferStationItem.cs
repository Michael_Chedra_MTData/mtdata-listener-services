using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class TransferStationItem : AddressItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;

        private bool _contaminatedAllowed;
        private string _materials;
        private ListId _wasteTypeListId;
        private bool _wasteTypeSelectionMandatory;
        private ListId _materialListId;
        #endregion

        #region properties
        public bool ContaminatedAllowed
        {
            get { return _contaminatedAllowed; }
            set { _contaminatedAllowed = value; }
        }
        public string Materials
        {
            get { return _materials; }
            set { _materials = value; }
        }
        public ListId WasteTypeListId
        {
            get { return _wasteTypeListId; }
            set { _wasteTypeListId = value; }
        }
        public bool WasteTypeSelectionMandatory
        {
            get { return _wasteTypeSelectionMandatory; }
            set { _wasteTypeSelectionMandatory = value; }
        }
        public ListId MaterialListId
        {
            get { return _materialListId; }
            set { _materialListId = value; }
        }

        #endregion

        #region Constructor
        public TransferStationItem()
        {
            _wasteTypeListId = new ListId();
            _materialListId = new ListId();
        }
        public TransferStationItem(int id, string name, string address, string note, double latitude, double longitude, int radius,
            bool contaminatedAllowed, string externalRef, string materials) 
            : base (id, name, address, note, latitude, longitude,radius)
        {
            _contaminatedAllowed = contaminatedAllowed;
            ExternalRef = externalRef;
            _materials = materials;
            _wasteTypeListId = new ListId();
            _materialListId = new ListId();
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _contaminatedAllowed = BaseLayer.ReadBool(data, ref index);
            if (versionNumber == 1)
            {
                //read the old external ref
                ExternalRef = BaseLayer.ReadString(data, ref index, 0x12);
            }
            else
            {
                //ignore the external ref as now read in base, its still in packet for old versions
                BaseLayer.ReadString(data, ref index, 0x12);
            }
            _materials = BaseLayer.ReadString(data, ref index, 0x12);

            _wasteTypeListId = new ListId();
            _materialListId = new ListId();
            _wasteTypeSelectionMandatory = false;
            if (versionNumber >= 3)
            {
                _wasteTypeListId.Decode(data, ref index);
                _wasteTypeSelectionMandatory = BaseLayer.ReadBool(data, ref index);
                _materialListId.Decode(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _contaminatedAllowed);
                //version 1 has external ref field here, have to leave it in so old units can still read it
                BaseLayer.WriteString(local, ExternalRef, 0x12);
                BaseLayer.WriteString(local, _materials, 0x12);
                _wasteTypeListId.Encode(local);
                BaseLayer.WriteBool(local, _wasteTypeSelectionMandatory);
                _materialListId.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

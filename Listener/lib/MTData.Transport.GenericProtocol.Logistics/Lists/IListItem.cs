using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// interface for a list item
    /// </summary>
    public interface IListItem: IPacketEncode, IComparable<IListItem>
    {
        /// <summary>
        /// the items id
        /// </summary>
        int ItemId { get; }
        /// <summary>
        /// the items name (descriptive text)
        /// </summary>
        string Name { get; }
        /// <summary>
        /// the items external ref
        /// </summary>
        string ExternalRef { get; }
        /// <summary>
        /// the unique key of the list item
        /// </summary>
        int UniqueKey { get; }
    }
}

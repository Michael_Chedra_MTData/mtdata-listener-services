﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class VehicleUsageItem : ListItem
    {
        private int _versionNumber = 4;

        private FatigueWorkType _fatigueWorkType;

        public FatigueWorkType FatigueWorkType
        {
            get { return _fatigueWorkType; }
            set { _fatigueWorkType = value; }
        }

        public override void Decode(byte[] data, ref int index)
        {
            int originalStart = index;
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex;

            if (versionNumber < 4)
            {
                // This class is created from version 4.
                // Prior to version 4 only the base ListItem was encoded, so we need to revert the index and avoid reading the version and length twice.
                index = originalStart;
                startIndex = index;
                base.Decode(data, ref index);
            }
            else
            {
                startIndex = index;
                base.Decode(data, ref index);

                _fatigueWorkType = (FatigueWorkType)data[index++];
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {

                base.Encode(local);

                local.WriteByte((byte)_fatigueWorkType);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// a send list segment
    /// </summary>
    public class SendListSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendList;

        #region private fields
        private IList _list;
        private bool _defaultList;
        /// <summary>
        /// the fleet or driver that requested this list
        /// </summary>
        private int _ownerId;
        #endregion

        #region properties
        public IList List
        {
            get { return _list; }
            set { _list = value; }
        }
        public bool DefaultList
        {
            get { return _defaultList; }
            set { _defaultList = value; }
        }
        /// <summary>
        /// the fleet or driver that requested this list
        /// </summary>
        public int OwnerId
        {
            get { return _ownerId; }
            set { _ownerId = value; }
        }
        #endregion

        #region constructor
        public SendListSegment()
        {
            Version = 3;
            Type = (int)SegmentType;
            _defaultList = true;
            _ownerId = -1;
        }
        public SendListSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendList;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_list.ListType);
                _list.Encode(stream);
                if (Version > 1)
                {
                    WriteBool(stream, _defaultList);
                }
                if (Version > 2)
                {
                    Write4ByteInteger(stream, _ownerId);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            //1st byte is the list type
            int index = 0;
            ListTypes t = (ListTypes)Data[index++];
            switch (t)
            {
                case ListTypes.Questions:
                    _list = new QuestionList();
                    break;
                case ListTypes.Division:
                case ListTypes.DelayReasons:
                case ListTypes.RejectReasons:
                case ListTypes.PredefinedMessages:
                case ListTypes.PalletTypes:
                case ListTypes.Trailers:
                case ListTypes.VehicleUsages:
                case ListTypes.FuelStations:
                case ListTypes.FleetPhoneBook:
                case ListTypes.DriverPhoneBook:
                case ListTypes.FleetAddressBook:
                case ListTypes.DriverAddressBook:
                case ListTypes.FleetEmailAddressBook:
                case ListTypes.DriverEmailAddressBook:
                case ListTypes.TransferStations:
                case ListTypes.DriverTypes:
                case ListTypes.LoginTypes:
                case ListTypes.LoadTypes:
                case ListTypes.TagLocations:
                case ListTypes.Unknown:
                case ListTypes.Customers:
                case ListTypes.ActionTaken:
                case ListTypes.ActualProblem:
                case ListTypes.TowingService:
                case ListTypes.Parts:
                case ListTypes.Person:
                case ListTypes.PassengerStop:
                case ListTypes.Organisation:
                case ListTypes.JobTemplate:
                case ListTypes.JobList:
                case ListTypes.MassScheme:
                case ListTypes.Products:
                case ListTypes.Category:
                case ListTypes.WaypointCategory:
                case ListTypes.DefectTemplate:
                case ListTypes.IncidentTemplate:
                case ListTypes.PreTripTemplate:
                case ListTypes.PostTripTemplate:
                case ListTypes.DefectSeverity:
                case ListTypes.FuelTypes:
                case ListTypes.FatigueAlert:
                    _list = new List();
                    break;
                case ListTypes.Poi:
                    _list = new PoiList();
                    break;
                case ListTypes.TrackableAsset:
                    _list = new TrackableAssetList();
                    break;
                case ListTypes.DefectType:
                    _list = new DefectTypeList();
                    break;
                default:
                    throw new Exception(string.Format("Unknown list type {0}", t));
            }
            if (_list != null)
            {
                _list.Decode(Data, ref index);
            }

            if (Version > 1)
            {
                _defaultList = ReadBool(Data, ref index);
            }
            else
            {
                //true as we only had defualt lists before
                _defaultList = true;
            }
            if (Version > 2)
            {
                _ownerId = Read4ByteInteger(Data, ref index);
            }
            else
            {
                //-1 as we didn't have owner's before
                _ownerId = -1;
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Jobs;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Delivery;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    /// <summary>
    /// a job template item
    /// </summary>
    public class JobTemplateItem : ListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 6;

        private int _jobTemplateId;
        private JobTypes _jobType;
        private List<ListId> _listIds;
        private List<int> _jobFieldIds;
        private PalletJobTemplateInfo _palletJobTemplateInfo;
        private GenericJobTemplaterInfo _genericInfo;
        private DeliveryJobTemplateInfo _deliveryJobTemplateInfo;
        #endregion

        #region properties
        public int JobTemplateId
        {
            get { return _jobTemplateId; }
            set { _jobTemplateId = value; }
        }
        public JobTypes JobType
        {
            get { return _jobType; }
            set { _jobType = value; }
        }
        public List<ListId> ListIds
        {
            get { return _listIds; }
            set { _listIds = value; }
        }
        public List<int> JobFieldIds
        {
            get { return _jobFieldIds; }
            set { _jobFieldIds = value; }
        }
        public PalletJobTemplateInfo PalletJobTemplateInfo
        {
            get { return _palletJobTemplateInfo; }
            set { _palletJobTemplateInfo = value; }
        }
        public GenericJobTemplaterInfo GenericInfo
        {
            get { return _genericInfo; }
            set { _genericInfo = value; }
        }
        public DeliveryJobTemplateInfo DeliveryJobTemplateInfo
        {
            get { return _deliveryJobTemplateInfo; }
            set { _deliveryJobTemplateInfo = value; }
        }

        #endregion

        #region constructor
        public JobTemplateItem() 
        {
            _listIds = new List<ListId>();
            _jobFieldIds = new List<int>();
            _genericInfo = new GenericJobTemplaterInfo();
        }
        public JobTemplateItem(int id, string name, int jobTemplate, JobTypes jobType) : base(id, name)
        {
            _jobTemplateId = jobTemplate;
            _jobType = jobType;
            _listIds = new List<ListId>();
            _jobFieldIds = new List<int>();
            _genericInfo = new GenericJobTemplaterInfo();
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            base.Decode(data, ref index);
            _jobTemplateId = BaseLayer.Read4ByteInteger(data, ref index);
            _jobType = (JobTypes)data[index++];
            _listIds.Clear();
            _jobFieldIds.Clear();
            if (versionNumber >= 2)
            {
                int count = (int)data[index++];
                for (int i = 0; i < count; i++)
                {
                    ListId id = new ListId();
                    id.Decode(data, ref index);
                    _listIds.Add(id);
                    if (versionNumber >= 3)
                    {
                        _jobFieldIds.Add(BaseLayer.Read2ByteInteger(data, ref index));
                    }
                    else
                    {
                        _jobFieldIds.Add(-1);
                    }
                }
            }
            _palletJobTemplateInfo = null;
            if (versionNumber >= 4 && _jobType == JobTypes.Pallets)
            {
                _palletJobTemplateInfo = new Lists.PalletJobTemplateInfo();
                _palletJobTemplateInfo.Decode(data, ref index);
            }

            if (versionNumber >= 5)
            {
                _genericInfo.Decode(data, ref index);
            }

            if (versionNumber >= 6 && _jobType == JobTypes.Delivery)
            {
                _deliveryJobTemplateInfo = new DeliveryJobTemplateInfo();
                _deliveryJobTemplateInfo.Decode(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write4ByteInteger(local, _jobTemplateId);
                local.WriteByte((byte)_jobType);
                if (_versionNumber >= 2)
                {
                    local.WriteByte((byte)_listIds.Count);
                    for (int i = 0; i < _listIds.Count; i++)
                    {
                        _listIds[i].Encode(local);
                        if (_versionNumber >= 3)
                        {
                            BaseLayer.Write2ByteInteger(local, _jobFieldIds[i]);
                        }
                    }
                }
                if (_versionNumber >= 4 && _jobType == JobTypes.Pallets && _palletJobTemplateInfo != null)
                {
                    _palletJobTemplateInfo.Encode(local);
                }
                if (_versionNumber >= 5)
                {
                    _genericInfo.Encode(local);
                }
                if (_versionNumber >= 6 && _jobType == JobTypes.Delivery)
                {
                    if (_deliveryJobTemplateInfo != null)
                    {
                        _deliveryJobTemplateInfo = new DeliveryJobTemplateInfo();
                    }
                    _deliveryJobTemplateInfo.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion 
    }

    public class PalletJobTemplateInfo : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 4;
        private string _loadType;
        private bool _showTimes;
        private bool _pickupAutoComplete;
        private bool _pickupSkipStart;
        private bool _pickupSkipPalletQuestions;
        private bool _pickupDelayReasonRequired;
        private bool _pickupDocRequired;
        private bool _pickupPodRequired;
        private bool _deliveryAutoComplete;
        private bool _deliverySkipStart;
        private bool _deliverySkipPalletQuestions;
        private bool _deliverySkipReturnQuestions;
        private bool _deliveryDelayReasonRequired;
        private bool _deliveryDocRequired;
        private bool _deliveryPodRequired;
        private bool _createRejectitemsLeg;
        private bool _singlePickupDeliveryLeg;
        private int _atSite;
        private bool _selectDivision;
        private bool _driverAdhocItems;
        private bool _trackableAssetLegActionsUsed;
        private bool _palletTransferUsed;
        private int _palletTransferClientListId;
        private int _palletTransferClientUniqueKey;
        #endregion 

        #region public properties
        public string LoadType
        {
            get { return _loadType; }
            set { _loadType = value; }
        }
        public bool ShowTimes
        {
            get { return _showTimes; }
            set { _showTimes = value; }
        }
        public bool PickupAutoComplete
        {
            get { return _pickupAutoComplete; }
            set { _pickupAutoComplete = value; }
        }
        public bool PickupSkipStart
        {
            get { return _pickupSkipStart; }
            set { _pickupSkipStart = value; }
        }
        public bool PickupSkipPalletQuestions
        {
            get { return _pickupSkipPalletQuestions; }
            set { _pickupSkipPalletQuestions= value; }
        }
        public bool PickupDelayReasonRequired
        {
            get { return _pickupDelayReasonRequired; }
            set { _pickupDelayReasonRequired = value; }
        }
        public bool PickupDocRequired
        {
            get { return _pickupDocRequired; }
            set { _pickupDocRequired = value; }
        }
        public bool PickupPodRequired
        {
            get { return _pickupPodRequired; }
            set { _pickupPodRequired = value; }
        }
        public bool DeliveryAutoComplete
        {
            get { return _deliveryAutoComplete; }
            set { _deliveryAutoComplete = value; }
        }
        public bool DeliverySkipStart
        {
            get { return _deliverySkipStart; }
            set { _deliverySkipStart = value; }
        }
        public bool DeliverySkipPalletQuestions
        {
            get { return _deliverySkipPalletQuestions; }
            set { _deliverySkipPalletQuestions = value; }
        }
        public bool DeliverySkipReturnQuestions
        {
            get { return _deliverySkipReturnQuestions; }
            set { _deliverySkipReturnQuestions = value; }
        }
        public bool DeliveryDelayReasonRequired
        {
            get { return _deliveryDelayReasonRequired; }
            set { _deliveryDelayReasonRequired = value; }
        }
        public bool DeliveryDocRequired
        {
            get { return _deliveryDocRequired; }
            set { _deliveryDocRequired = value; }
        }
        public bool DeliveryPodRequired
        {
            get { return _deliveryPodRequired; }
            set { _deliveryPodRequired = value; }
        }
        public bool CreateRejectitemsLeg
        {
            get { return _createRejectitemsLeg; }
            set { _createRejectitemsLeg = value; }
        }
        public bool SinglePickupDeliveryLeg
        {
            get { return _singlePickupDeliveryLeg; }
            set { _singlePickupDeliveryLeg = value; }
        }
        public int AtSite
        {
            get { return _atSite; }
            set { _atSite= value; }
        }
        public bool SelectDivision
        {
            get { return _selectDivision; }
            set { _selectDivision = value; }
        }

        public bool DriverAdhocItems
        {
            get { return _driverAdhocItems; }
            set { _driverAdhocItems = value; }
        }
        public bool TrackableAssetLegActionsUsed
        {
            get { return _trackableAssetLegActionsUsed; }
            set { _trackableAssetLegActionsUsed = value; }
        }
        public bool PalletTransferUsed
        {
            get { return _palletTransferUsed; }
            set { _palletTransferUsed = value; }
        }
        public int PalletTransferClientListId
        {
            get { return _palletTransferClientListId; }
            set { _palletTransferClientListId = value; }
        }
        public int PalletTransferClientUniqueKey
        {
            get { return _palletTransferClientUniqueKey; }
            set { _palletTransferClientUniqueKey = value; }
        }
        #endregion

        #region constructor
        public PalletJobTemplateInfo() { }
        #endregion

        #region IPacketEncode members
        /// <summary>
        /// encode the list into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, _loadType, 0x12);
                BaseLayer.WriteBool(local, _showTimes);
                BaseLayer.WriteBool(local, _pickupAutoComplete);
                BaseLayer.WriteBool(local, _pickupSkipStart);
                BaseLayer.WriteBool(local, _pickupSkipPalletQuestions);
                BaseLayer.WriteBool(local, _pickupDelayReasonRequired);
                BaseLayer.WriteBool(local, _pickupDocRequired);
                BaseLayer.WriteBool(local, _pickupPodRequired);
                BaseLayer.WriteBool(local, _deliveryAutoComplete);
                BaseLayer.WriteBool(local, _deliverySkipStart);
                BaseLayer.WriteBool(local, _deliverySkipPalletQuestions);
                BaseLayer.WriteBool(local, _deliverySkipReturnQuestions);
                BaseLayer.WriteBool(local, _deliveryDelayReasonRequired);
                BaseLayer.WriteBool(local, _deliveryDocRequired);
                BaseLayer.WriteBool(local, _deliveryPodRequired);
                BaseLayer.WriteBool(local, _createRejectitemsLeg);
                BaseLayer.WriteBool(local, _singlePickupDeliveryLeg);
                BaseLayer.Write4ByteInteger(local, _atSite);
                BaseLayer.WriteBool(local, _selectDivision);
                BaseLayer.WriteBool(local, _driverAdhocItems);
                BaseLayer.WriteBool(local, _trackableAssetLegActionsUsed);
                BaseLayer.WriteBool(local, _palletTransferUsed);
                BaseLayer.Write4ByteInteger(local, _palletTransferClientListId);
                BaseLayer.Write4ByteInteger(local, _palletTransferClientUniqueKey);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        /// <summary>
        /// decode the list from a byte[]
        /// </summary>
        /// <param name="Data">byte[] containing the list parameters to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _loadType = BaseLayer.ReadString(data, ref index, 0x12);
            _showTimes = BaseLayer.ReadBool(data, ref index);
            _pickupAutoComplete = BaseLayer.ReadBool(data, ref index);
            _pickupSkipStart = BaseLayer.ReadBool(data, ref index);
            _pickupSkipPalletQuestions = BaseLayer.ReadBool(data, ref index);
            _pickupDelayReasonRequired = BaseLayer.ReadBool(data, ref index);
            _pickupDocRequired = BaseLayer.ReadBool(data, ref index);
            _pickupPodRequired = BaseLayer.ReadBool(data, ref index);
            _deliveryAutoComplete = BaseLayer.ReadBool(data, ref index);
            _deliverySkipStart = BaseLayer.ReadBool(data, ref index);
            _deliverySkipPalletQuestions = BaseLayer.ReadBool(data, ref index);
            _deliverySkipReturnQuestions = BaseLayer.ReadBool(data, ref index);
            _deliveryDelayReasonRequired = BaseLayer.ReadBool(data, ref index);
            _deliveryDocRequired = BaseLayer.ReadBool(data, ref index);
            _deliveryPodRequired = BaseLayer.ReadBool(data, ref index);
            _createRejectitemsLeg = BaseLayer.ReadBool(data, ref index);
            _singlePickupDeliveryLeg = BaseLayer.ReadBool(data, ref index);
            _atSite = BaseLayer.Read4ByteInteger(data, ref index);
            if (versionNumber >= 2)
            {
                _selectDivision = BaseLayer.ReadBool(data, ref index);
            }
            else
            {
                _selectDivision = false;
            }

            if (versionNumber >= 3)
            {
                _driverAdhocItems = BaseLayer.ReadBool(data, ref index);
            }
            _trackableAssetLegActionsUsed = false;
            _palletTransferUsed = false;
            _palletTransferClientListId = -1;
            _palletTransferClientUniqueKey = -1;
            if (versionNumber >= 4)
            {
                _trackableAssetLegActionsUsed = BaseLayer.ReadBool(data, ref index);
                _palletTransferUsed = BaseLayer.ReadBool(data, ref index);
                _palletTransferClientListId = BaseLayer.Read4ByteInteger(data, ref index);
                _palletTransferClientUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        #endregion
    }

    public enum RefRequiredEnum
    {
        Required,
        Optional,
        Hidden
    }

    public class GenericJobTemplaterInfo : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 1;
        private ListId _titleListId;
        private RefRequiredEnum _refRequired;
        private RefRequiredEnum _refIntRequired;
        private RefRequiredEnum _docketNumberRequired;
        #endregion 

        #region public properties
        public ListId TitleListId
        {
            get { return _titleListId; }
            set { _titleListId = value; }
        }
        public RefRequiredEnum RefRequired
        {
            get { return _refRequired; }
            set { _refRequired = value; }
        }
        public RefRequiredEnum RefIntRequired
        {
            get { return _refIntRequired; }
            set { _refIntRequired = value; }
        }
        public RefRequiredEnum DocketNumberRequired
        {
            get { return _docketNumberRequired; }
            set { _docketNumberRequired = value; }
        }
        #endregion

        #region constructor
        public GenericJobTemplaterInfo() 
        {
            _titleListId = new ListId(-1,-1);
            _refRequired = RefRequiredEnum.Optional;
            _refIntRequired = RefRequiredEnum.Optional;
            _docketNumberRequired = RefRequiredEnum.Optional;
        }
        #endregion

        #region IPacketEncode members
        /// <summary>
        /// encode the list into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                _titleListId.Encode(local);
                local.WriteByte((byte)_refRequired);
                local.WriteByte((byte)_refIntRequired);
                local.WriteByte((byte)_docketNumberRequired);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        /// <summary>
        /// decode the list from a byte[]
        /// </summary>
        /// <param name="Data">byte[] containing the list parameters to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _titleListId.Decode(data, ref index);
            _refRequired = (RefRequiredEnum)data[index++];
            _refIntRequired = (RefRequiredEnum)data[index++];
            _docketNumberRequired = (RefRequiredEnum)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        #endregion
    }

    public class DeliveryJobTemplateInfo : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 1;
        private MassDeclarationRequired _massDecRequired;
        private bool _totalWeightRequired;
        private bool _inspectAssetOnPickup;
        private bool _inspectAssetOnDelivery;
        private bool _driverAdhocItems;
        private bool _trackableAssetLegActionsUsed;
        #endregion 

        #region public properties
        public MassDeclarationRequired MassDecRequired
        {
            get { return _massDecRequired; }
            set { _massDecRequired = value; }
        }
        public bool TotalWeightRequired
        {
            get { return _totalWeightRequired; }
            set { _totalWeightRequired = value; }
        }
        public bool InspectAssetOnPickup
        {
            get { return _inspectAssetOnPickup; }
            set { _inspectAssetOnPickup = value; }
        }
        public bool InspectAssetOnDrop
        {
            get { return _inspectAssetOnDelivery; }
            set { _inspectAssetOnDelivery = value; }
        }

        public bool DriverAdhocItems
        {
            get { return _driverAdhocItems; }
            set { _driverAdhocItems = value; }
        }
        public bool TrackableAssetLegActionsUsed
        {
            get { return _trackableAssetLegActionsUsed; }
            set { _trackableAssetLegActionsUsed = value; }
        }
        #endregion

        #region constructor
        public DeliveryJobTemplateInfo() { }
        #endregion

        #region IPacketEncode members
        /// <summary>
        /// encode the list into the memory stream
        /// </summary>
        /// <param name="stream"></param>
        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_massDecRequired);
                BaseLayer.WriteBool(local, _totalWeightRequired);
                BaseLayer.WriteBool(local, _inspectAssetOnPickup);
                BaseLayer.WriteBool(local, _inspectAssetOnDelivery);
                BaseLayer.WriteBool(local, _driverAdhocItems);
                BaseLayer.WriteBool(local, _trackableAssetLegActionsUsed);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        /// <summary>
        /// decode the list from a byte[]
        /// </summary>
        /// <param name="Data">byte[] containing the list parameters to decode</param>
        /// <param name="index">the starting place in the byte[] to read, on return gives the index of the next byte in the array</param>
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _massDecRequired = (MassDeclarationRequired)data[index++];
            _totalWeightRequired = BaseLayer.ReadBool(data, ref index);
            _inspectAssetOnPickup = BaseLayer.ReadBool(data, ref index);
            _inspectAssetOnDelivery = BaseLayer.ReadBool(data, ref index);
            _driverAdhocItems = BaseLayer.ReadBool(data, ref index);
            _trackableAssetLegActionsUsed = BaseLayer.ReadBool(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class RequestListSegment : SegmentLayer
    {
        #region private fields
        private ListId _id;
        private ListTypes _listType;
        private QuestionListTypes _questionListType;
        private TrackableAssetListTypes _trackableAssetType;
        private int _currentVersion;
        #endregion

        #region properties
        public ListId Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public ListTypes ListType
        {
            get { return _listType; }
            set { _listType = value; }
        }
        public QuestionListTypes QuestionListType
        {
            get { return _questionListType; }
            set { _questionListType = value; }
        }
        public TrackableAssetListTypes TrackableAssetType
        {
            get { return _trackableAssetType; }
            set { _trackableAssetType = value; }
        }

        public int CurrentVersion
        {
            get { return _currentVersion; }
            set { _currentVersion = value; }
        }
        #endregion

        #region constructor
        public RequestListSegment()
        {
            Version = 4;
            Type = (int)LogisticsSegmentTypes.RequestList;
            _id = new ListId();
        }
        public RequestListSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.RequestList)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.RequestList, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.RequestList;
            _id = new ListId();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                _id.Encode(stream);
                stream.WriteByte((byte)_listType);
                if (_listType == ListTypes.Questions)
                {
                    stream.WriteByte((byte)_questionListType);
                }
                else if (_listType == ListTypes.TrackableAsset)
                {
                    stream.WriteByte((byte)_trackableAssetType);
                }
                if (Version >= 4)
                {
                    Write4ByteInteger(stream, _currentVersion);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            //1st byte is the list type
            int index = 0;
            _id.Decode(Data, ref index);
            _listType = (ListTypes)Data[index++];
            if (_listType == ListTypes.Questions)
            {
                _questionListType = (QuestionListTypes)Data[index++];
            }
            else
            {
                _questionListType = QuestionListTypes.Generic;
            }

            if (_listType == ListTypes.TrackableAsset && Version >= 3)
            {
                _trackableAssetType = (TrackableAssetListTypes)Data[index++];
            }
            else
            {
                _trackableAssetType = TrackableAssetListTypes.Undefined;
            }

            if (Version >= 4)
            {
                _currentVersion = Read4ByteInteger(Data, ref index);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class DefaultMultiListSegment: SegmentLayer
    {
        #region Private Members
        
        private ListDefaultList _defaultMultiList;

        #endregion

        #region Constructor

        public DefaultMultiListSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendMultiList;
            _defaultMultiList = new ListDefaultList();
        }

        public DefaultMultiListSegment(SegmentLayer segment)
        {
            if(segment.Type != (int)LogisticsSegmentTypes.SendMultiList)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendMultiList, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendMultiList;            
            Data = segment.Data;
            DecodeData();
        }

        #endregion

        #region Public Properties

        public ListDefaultList DefaultMultiList
        {
            get { return _defaultMultiList; }
            set { _defaultMultiList = value; }
        }

        #endregion

        #region Override methods

        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                _defaultMultiList.Encode(stream);                
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region Private Methods
        private void DecodeData()
        {
            int index = 0;
            _defaultMultiList = new ListDefaultList();
            _defaultMultiList.Decode(Data, ref index);                
        }
        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public enum QuestionTypes
    {
        YesNo = 0,
        Numeric,
        Text,
        Time,
        List,
        TimeLength,
        Quantity,
        BarCode,
        Photo
    }

    /// <summary>
    /// a question Item
    /// </summary>
    public class QuestionItem : ListItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 7;

        /// <summary>
        /// the question type
        /// </summary>
        private QuestionTypes _questionType;
        /// <summary>
        /// is the answer validated
        /// </summary>
        private bool _isAnswerChecked;
        /// <summary>
        /// the correct answer only used if _isAnswerValidated = true
        /// </summary>
        private string _correctAnswer;
        /// <summary>
        /// the message to display when user answer's question wrong only used if _isAnswerValidated = true
        /// </summary>
        private string _incorrectAnswerMessage;
        /// <summary>
        /// the list id only used if _questionType = List
        /// </summary>
        private ListId _listId;
        private string _shortName;
        private bool _multiSelect;
        private string _defaultValue;
        private bool _mandatory;
        private bool _checkList;
        private ListId _yesQuestionList;
        private ListId _noQuestionList;
        #endregion

        #region properties
        /// <summary>
        /// the question type
        /// </summary>
        public QuestionTypes QuestionType
        {
            get { return _questionType; }
            set { _questionType = value; }
        }
        /// <summary>
        /// is the answer validated
        /// </summary>
        public bool IsAnswerChecked
        {
            get { return _isAnswerChecked; }
            set { _isAnswerChecked = value; }
        }

        /// <summary>
        /// the correct answer only used if IsAnswerValidated = true
        /// </summary>
        public string CorrectAnswer
        {
            get { return _correctAnswer; }
            set { _correctAnswer = value; }
        }

        /// <summary>
        /// the message to display when user answer's question wrong only used if IsAnswerValidated = true
        /// </summary>
        public string IncorrectAnswerMessage
        {
            get { return _incorrectAnswerMessage; }
            set { _incorrectAnswerMessage = value; }
        }

        /// <summary>
        /// the list id only used if QuestionType = List
        /// </summary>
        public ListId ListId
        {
            get { return _listId; }
            set { _listId = value; }
        }

        public string ShortName
        {
            get { return _shortName; }
            set { _shortName = value; }
        }

        public bool MultiSelect
        {
            get { return _multiSelect; }
            set { _multiSelect = value; }
        }

        public string DefaultValue
        {
            get { return _defaultValue; }
            set { _defaultValue = value; }
        }

        public ListId YesQuestionList
        {
            get { return _yesQuestionList; }
            set { _yesQuestionList = value; }
        }

        public ListId NoQuestionList
        {
            get { return _noQuestionList; }
            set { _noQuestionList = value; }
        }

        public bool IsMandatory
        {
            get { return _mandatory; }
            set { _mandatory = value; }
        }

        public bool IsCheckList
        {
            get { return _checkList; }
            set { _checkList = value; }
        }
        #endregion

        #region constructor
        public QuestionItem()
        {
            _listId = new ListId();
            _mandatory = true;
        }
        public QuestionItem(int id, string name, QuestionTypes questionType)
            : base(id, name)
        {
            _listId = new ListId();
            _questionType = questionType;
            _mandatory = true;
        }
        public QuestionItem(int id, string name, QuestionTypes questionType, string shortName)
            : base(id, name)
        {
            _listId = new ListId();
            _questionType = questionType;
            _shortName = shortName;
            _mandatory = true;
        }
        public QuestionItem(int id, string name, QuestionTypes questionType, string correctAnswer, string incorrectAnswerMessage)
            : base(id, name)
        {
            _listId = new ListId();
            _questionType = questionType;
            _isAnswerChecked = true;
            _correctAnswer = correctAnswer;
            _incorrectAnswerMessage = incorrectAnswerMessage;
            _mandatory = true;
        }
        public QuestionItem(int id, string name, QuestionTypes questionType, string correctAnswer, string incorrectAnswerMessage, string shortName)
            : base(id, name)
        {
            _listId = new ListId();
            _questionType = questionType;
            _isAnswerChecked = true;
            _correctAnswer = correctAnswer;
            _incorrectAnswerMessage = incorrectAnswerMessage;
            _shortName = shortName;
            _mandatory = true;
        }

        public QuestionItem(int id, string name, QuestionTypes questionType, string shortName, bool multiSelect)
            : base(id, name)
        {
            _listId = new ListId();
            _questionType = questionType;
            _shortName = shortName;
            _multiSelect = multiSelect;
            _mandatory = true;
        }


        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _questionType = (QuestionTypes)data[index++];
            _isAnswerChecked = BaseLayer.ReadBool(data, ref index);
            if (_isAnswerChecked)
            {
                _correctAnswer = BaseLayer.ReadString(data, ref index, 0x12);
                _incorrectAnswerMessage = BaseLayer.ReadString(data, ref index, 0x12);
            }
            else
            {
                _correctAnswer = string.Empty;
                _incorrectAnswerMessage = string.Empty;
            }

            _listId = new ListId();
            if (_questionType == QuestionTypes.List)
            {
                _listId.Decode(data, ref index);
            }

            if (versionNumber >= 2)
            {
                _shortName = BaseLayer.ReadString(data, ref index, 0x12);
            }
            else
            {
                _shortName = string.Empty;
            }

            _multiSelect = false;
            _defaultValue = string.Empty;

            if (versionNumber >= 3)
            {
                switch (_questionType)
                {
                    case QuestionTypes.List:
                        _multiSelect = BaseLayer.ReadBool(data, ref index);
                        break;
                    case QuestionTypes.Quantity:
                    case QuestionTypes.TimeLength:
                        _defaultValue = BaseLayer.ReadString(data, ref index, 0x12);
                        break;
                    default:
                        break;
                }
            }

            _mandatory = true;
            if (versionNumber >= 4)
            {
                _mandatory = BaseLayer.ReadBool(data, ref index);
            }

            if (versionNumber >= 5)
            {
                _checkList = BaseLayer.ReadBool(data, ref index);
            }
            if (versionNumber >= 6)
            {
                if (QuestionType == QuestionTypes.YesNo)
                {
                    if(BaseLayer.ReadBool(data, ref index))
                    {
                        _yesQuestionList = new ListId();
                        _yesQuestionList.Decode(data, ref index);
                    }
                    if(BaseLayer.ReadBool(data, ref index))
                    {
                        _noQuestionList = new ListId();
                        _noQuestionList.Decode(data, ref index);
                    }
                }
            }

            if (versionNumber >= 7)
            {
                if (_questionType == QuestionTypes.Numeric)
                    _defaultValue = BaseLayer.ReadString(data, ref index, 0x12);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                local.WriteByte((byte)_questionType);
                BaseLayer.WriteBool(local, _isAnswerChecked);
                if (_isAnswerChecked)
                {
                    BaseLayer.WriteString(local, _correctAnswer, 0x12);
                    BaseLayer.WriteString(local, _incorrectAnswerMessage, 0x12);
                }

                if (_questionType == QuestionTypes.List)
                {
                    if (_listId == null)
                    {
                        _listId = new ListId();
                    }
                    _listId.Encode(local);
                }
                if (_versionNumber >= 2)
                {
                    BaseLayer.WriteString(local, _shortName, 0x12);
                }

                if (_versionNumber >= 3)
                {
                    switch (_questionType)
                    {
                        case QuestionTypes.List:
                            BaseLayer.WriteBool(local, _multiSelect);
                            break;
                        case QuestionTypes.Quantity:
                        case QuestionTypes.TimeLength:
                            BaseLayer.WriteString(local, _defaultValue, 0x12);
                            break;
                        default:
                            break;
                    }
                }
                if (_versionNumber >= 4)
                {
                    BaseLayer.WriteBool(local, _mandatory);
                }
                if (_versionNumber >= 5)
                {
                    BaseLayer.WriteBool(local, _checkList);
                }
                if (_versionNumber >= 6)
                {
                    if (QuestionType == QuestionTypes.YesNo)
                    {
                        // Encode any conditional yes no lists
                        // Encode conditional yes branch
                        if (_yesQuestionList != null)
                        {
                            BaseLayer.WriteBool(local, true);
                            _yesQuestionList.Encode(local);
                        }
                        else
                        {
                            BaseLayer.WriteBool(local, false);
                        }
                        // Encode conditional no branch
                        if (_noQuestionList != null)
                        {
                            BaseLayer.WriteBool(local, true);
                            _noQuestionList.Encode(local);
                        }
                        else
                        {
                            BaseLayer.WriteBool(local, false);
                        }
                    }
                }

                if(_versionNumber >= 7)
                {
                    if(_questionType == QuestionTypes.Numeric)
                        BaseLayer.WriteString(local, _defaultValue, 0x12);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

        #region public methods
        /// <summary>
        /// check if the given answer is the correct answer of this question
        /// </summary>
        /// <param name="answer"></param>
        /// <returns></returns>
        public bool CheckAnswerCorrect(string answer)
        {
            if (!_isAnswerChecked || (answer != null && _correctAnswer.ToLower().Equals(answer.Trim().ToLower())))
            {
                return true;
            }
            return false;
        }
        #endregion
    }
}

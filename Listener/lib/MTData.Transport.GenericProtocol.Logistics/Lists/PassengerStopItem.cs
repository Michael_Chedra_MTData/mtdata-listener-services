using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class PassengerStopItem : AddressItem
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private int _sequence;
        #endregion

        #region properties
        public int Sequence
        {
            get { return _sequence; }
            set { _sequence = value; }
        }
        #endregion

        #region Constructor
        public PassengerStopItem() { }
        public PassengerStopItem(int id, string name, string address, string note, double latitude, double longitude, int radius, int sequence)
            : base(id, name, address, note, latitude, longitude, radius)
        {
            _sequence = sequence;
        }
        #endregion

        #region override base class methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _sequence = BaseLayer.Read4ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write4ByteInteger(local, _sequence);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

    }
}

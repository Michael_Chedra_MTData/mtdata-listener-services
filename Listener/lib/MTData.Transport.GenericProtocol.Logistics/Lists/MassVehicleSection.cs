﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public enum MassVehicleSectionTypes
    {
        NO_SECTION,
        PRIME_1_1,
        PRIME_1_2,
        RIGID_1_1,
        RIGID_1_2,
        RIGID_1_3,
        RIGID_2_2,
        RIGID_2_3,
        PIG_1,
        PIG_2,
        PIG_3,
        DOG_1_1,
        DOG_1_2,
        DOG_2_2,
        SEMI_1,
        SEMI_2,
        SEMI_3,
        SEMI_2_5TH_WHEEL,
        SEMI_3_5TH_WHEEL,
        DOLLY_2,
        DOLLY_3,
        UNKNOWN,
        SEMI_4_5TH_WHEEL,
        SEMI_4,
        PRIME_2_2,
        DOG_2_3,
        DOG_3_3,
        DOG_3_4,
        DOLLY_1,
        PRIME_1_1_COMBINED,
        PRIME_1_2_COMBINED,
        PRIME_2_2_COMBINED,
        PRIME_2_3,
        PRIME_2_3_COMBINED
    }

    /// <summary>
    /// a Mass vehicle section
    /// </summary>
    public class MassVehicleSection : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private int _sequence;
        private MassVehicleSectionTypes _sectionType;
        private int _frontAxleWeightLimit;
        private int _rearAxleWeightLimit;
        #endregion

        #region properties
        public int Sequence
        {
            get { return _sequence; }
            set { _sequence = value; }
        }
        public MassVehicleSectionTypes SectionType
        {
            get { return _sectionType; }
            set { _sectionType = value; }
        }
        public int FrontAxleWeightLimit
        {
            get { return _frontAxleWeightLimit; }
            set { _frontAxleWeightLimit = value; }
        }
        public int RearAxleWeightLimit
        {
            get { return _rearAxleWeightLimit; }
            set { _rearAxleWeightLimit = value; }
        }
        #endregion

        #region Constructor
        public MassVehicleSection() 
        {
            _sectionType = MassVehicleSectionTypes.NO_SECTION;
            _frontAxleWeightLimit = MassSchemeItem.NO_LIMIT_SET;
            _rearAxleWeightLimit = MassSchemeItem.NO_LIMIT_SET;
        }
        public MassVehicleSection(int sequence, MassVehicleSectionTypes sectionType, int frontLimit, int rearLimit)
        {
            _sequence = sequence;
            _sectionType = sectionType;
            _frontAxleWeightLimit = frontLimit;
            _rearAxleWeightLimit = rearLimit;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _sequence = BaseLayer.Read4ByteInteger(data, ref index);
            _sectionType = (MassVehicleSectionTypes)data[index++];
            _frontAxleWeightLimit = BaseLayer.Read4ByteInteger(data, ref index);
            _rearAxleWeightLimit = BaseLayer.Read4ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _sequence);
                local.WriteByte((byte)_sectionType);
                BaseLayer.Write4ByteInteger(local, _frontAxleWeightLimit);
                BaseLayer.Write4ByteInteger(local, _rearAxleWeightLimit);
                
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion

       
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Lists
{
    public class DefectSeverityItem : ListItem
    {
        private int _versionNumber = 1;

        private byte _severityLevel;
        private bool _stopWork;
        private string _instructions;

        public byte SeverityLevel
        {
            get { return _severityLevel; }
            set { _severityLevel = value; }
        }

        public bool StopWork
        {
            get { return _stopWork; }
            set { _stopWork = value; }
        }

        public string Instructions
        {
            get { return _instructions; }
            set { _instructions = value; }
        }

        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            base.Decode(data, ref index);

            _severityLevel = data[index++];
            _stopWork = BaseLayer.ReadBool(data, ref index);
            _instructions = BaseLayer.ReadString(data, ref index);

            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                local.WriteByte(_severityLevel);
                BaseLayer.WriteBool(local, _stopWork);
                BaseLayer.WriteString(local, _instructions);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// a force logout packet from base
    /// </summary>
    public class SendForceLogoutSegment : SegmentLayer
    {
        #region private fields
        private int _driverUserId;
        private int _fleetId;
        private int _vehicleId;
        private string _message;
        private bool _fromBase;
        #endregion

        #region properties
        public int DriverUserId
        {
            get { return _driverUserId; }
            set { _driverUserId = value; }
        }

        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }

        public bool FromBase
        {
            get { return _fromBase; }
            set { _fromBase = value; }
        }
        #endregion

        #region constructor
        public SendForceLogoutSegment()
        {
            Version = 3;
            Type = (int)LogisticsSegmentTypes.SendForceLogout;
        }
        public SendForceLogoutSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendForceLogout)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendForceLogout, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendForceLogout;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                if (Version >= 2)
                {
                    Write4ByteInteger(stream, _driverUserId);
                }

                if(Version >= 3)
                {
                    Write4ByteInteger(stream, _fleetId);
                    Write4ByteInteger(stream, _vehicleId);
                    WriteString(stream, _message, 0x12);
                    WriteBool(stream, _fromBase);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            _driverUserId = 0;
            int index = 0;
            if (Version >= 2)
            {
                _driverUserId = Read4ByteInteger(Data, ref index);
            }

            if(Version >= 3)
            {
                _fleetId = Read4ByteInteger(Data, ref index);
                _vehicleId = Read4ByteInteger(Data, ref index);
                _message = ReadString(Data, ref index, 0x12);
                _fromBase = ReadBool(Data, ref index);
            }
        }
        #endregion

    }
}

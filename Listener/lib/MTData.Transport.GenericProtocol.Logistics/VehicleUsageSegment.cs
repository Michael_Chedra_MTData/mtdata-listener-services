using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class VehicleUsageSegment : SegmentLayer
    {
        #region private fields
        private bool _startVehicleUsage;
        private int _vehicleUsageId;
        private ListId _vehicleUsageListId;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public bool StartVehicleUsage
        {
            get { return _startVehicleUsage; }
            set { _startVehicleUsage = value; }
        }
        public int VehicleUsageId
        {
            get { return _vehicleUsageId; }
            set { _vehicleUsageId = value; }
        }
        public ListId VehicleUsageListId
        {
            get { return _vehicleUsageListId; }
            set { _vehicleUsageListId = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public VehicleUsageSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.VehicleUsage;
            _vehicleUsageListId = new ListId();
        }
        public VehicleUsageSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.VehicleUsage)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.VehicleUsage, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.VehicleUsage;
            _vehicleUsageListId = new ListId();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                WriteBool(stream, _startVehicleUsage);
                Write4ByteInteger(stream, _vehicleUsageId);
                _vehicleUsageListId.Encode(stream);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _startVehicleUsage = ReadBool(Data, ref index);
            _vehicleUsageId = Read4ByteInteger(Data, ref index);
            _vehicleUsageListId.Decode(Data, ref index);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

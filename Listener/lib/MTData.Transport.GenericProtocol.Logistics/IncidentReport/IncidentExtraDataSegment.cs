using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.DefectReport;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class IncidentExtraDataSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.IncidentExtraData;

        private int _incidentId;

        private List<DisplayAnswer> _answers = new List<DisplayAnswer>();

        public IncidentExtraDataSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public IncidentExtraDataSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public int IncidentId
        {
            get { return _incidentId; }
            set { _incidentId = value; }
        }

        public List<DisplayAnswer> Answers
        {
            get { return _answers; }
            set { _answers = value ?? new List<DisplayAnswer>(); }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _incidentId);
                WriteMoreFlag(stream, _answers.Count);
                foreach (DisplayAnswer d in _answers)
                {
                    d.Encode(stream);
                }
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;

            _incidentId = Read4ByteInteger(Data, ref index);
            int count = ReadMoreFlag(Data, ref index);
            _answers = new List<DisplayAnswer>();
            for (int i = 0; i < count; i++)
            {
                DisplayAnswer a = new DisplayAnswer();
                a.Decode(Data, ref index);
                _answers.Add(a);
            }
            index += count;
        }
    }
}
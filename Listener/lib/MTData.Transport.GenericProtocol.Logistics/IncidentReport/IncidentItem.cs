﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class IncidentItem : IPacketEncode
    {
        private const int _versionNumber = 4;

        private int _incidentID;

        private int _terminalCreationID;

        private IncidentStatus _status;

        private string _incidentType;

        private DateTime _reportTime;

        private List<IncidentLog> _logs = new List<IncidentLog>();

        private string _description;

        private DateTime _incidentTime;

        private int _driverID;
        private int? _slaveFleetId;
        private int? _slaveVehicleId;
        private int? _assetUniqueKey;

        public int IncidentID
        {
            get { return _incidentID; }
            set { _incidentID = value; }
        }

        public int TerminalCreationID
        {
            get { return _terminalCreationID; }
            set { _terminalCreationID = value; }
        }

        public IncidentStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string IncidentType
        {
            get { return _incidentType; }
            set { _incidentType = value; }
        }

        public DateTime ReportTime
        {
            get { return _reportTime; }
            set { _reportTime = value; }
        }

        public List<IncidentLog> Logs
        {
            get { return _logs; }
            set { _logs = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public DateTime IncidentTime
        {
            get { return _incidentTime; }
            set { _incidentTime = value; }
        }

        /// <summary>
        /// References the driver that can view this incident.
        /// If the value is less than or equal to zero then this is a vehicle
        /// association (as opposed to a vehicle association) therefore all drivers can view.
        /// </summary>
        public int DriverID
        {
            get { return _driverID; }
            set { _driverID = value; }
        }

        public int? SlaveFleetId
        {
            get { return _slaveFleetId; }
            set { _slaveFleetId = value; }
        }
        public int? SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }
        public int? AssetUniqueKey
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _incidentID = BaseLayer.Read4ByteInteger(data, ref index);
            _terminalCreationID = BaseLayer.Read4ByteInteger(data, ref index);
            _status = (IncidentStatus)data[index++];
            _incidentType = BaseLayer.ReadString(data, ref index, 0x12);
            _reportTime = BaseLayer.ReadDateTime(data, ref index);

            int count = BaseLayer.ReadMoreFlag(data, ref index);
            List<IncidentLog> logs = new List<IncidentLog>(count);
            for (int i = 0; i < count; i++)
            {
                IncidentLog l = new IncidentLog();
                l.Decode(data, ref index);
                logs.Add(l);
            }
            _logs = logs;
            if (versionNumber >= 2)
            {
                _description = BaseLayer.ReadString(data, ref index);
                _incidentTime = BaseLayer.ReadDateTime(data, ref index);
            }
            if (versionNumber >= 3)
            {
                _driverID = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _driverID = 0;
            }
            _slaveFleetId = null;
            _slaveVehicleId = null;
            _assetUniqueKey = null;
            if (_versionNumber >= 4)
            {
                _slaveFleetId = BaseLayer.Read4ByteNullableInteger(data, ref index);
                _slaveVehicleId = BaseLayer.Read4ByteNullableInteger(data, ref index);
                _assetUniqueKey = BaseLayer.Read4ByteNullableInteger(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _incidentID);
                BaseLayer.Write4ByteInteger(local, _terminalCreationID);
                local.WriteByte((byte)_status);
                BaseLayer.WriteString(local, _incidentType, 0x12);
                BaseLayer.WriteDateTime(local, _reportTime);
                BaseLayer.WriteMoreFlag(local, _logs.Count);
                foreach (IncidentLog log in _logs)
                {
                    log.Encode(local);
                }
                // Version 2
                BaseLayer.WriteString(local, _description);
                BaseLayer.WriteDateTime(local, _incidentTime);
                // Version 3
                BaseLayer.Write4ByteInteger(local, _driverID);
                //version 4
                BaseLayer.Write4ByteNullableInteger(local, _slaveFleetId);
                BaseLayer.Write4ByteNullableInteger(local, _slaveVehicleId);
                BaseLayer.Write4ByteNullableInteger(local, _assetUniqueKey);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

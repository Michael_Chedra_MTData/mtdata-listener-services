﻿namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public enum AssociationType : byte
    {
        Vehicle,
        Driver,
        Asset
    }
}

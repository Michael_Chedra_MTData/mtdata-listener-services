﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public enum IncidentStatus : byte
    {
        Open,
        Closed
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class IncidentNoteSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.IncidentNote;

        private int _incidentId;
        private string _note;

        /// <summary>
        /// The ID of the incident that this note is associated with. If the incident ID is not yet known then the creation ID multiplied by -1 should be used instead.
        /// </summary>
        public int IncidentId
        {
            get { return _incidentId; }
            set { _incidentId = value; }
        }

        public string Note
        {
            get { return _note; }
            set { _note = value; }
        }

        public IncidentNoteSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public IncidentNoteSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _incidentId);
                WriteString(stream, _note, 0x12);
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;
            _incidentId = Read4ByteInteger(Data, ref index);
            _note = ReadString(Data, ref index, 0x12);
        }
    }
}

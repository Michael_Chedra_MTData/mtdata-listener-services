﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class UnitCreatedIncidentAckSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.UnitCreatedIncidentAck;

        private int _incidentId;

        private int _unitCreatedId;

        public int IncidentId
        {
            get { return _incidentId; }
            set { _incidentId = value; }
        }

        public int UnitCreatedId
        {
            get { return _unitCreatedId; }
            set { _unitCreatedId = value; }
        }
        public UnitCreatedIncidentAckSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public UnitCreatedIncidentAckSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }


        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _incidentId);
                Write4ByteInteger(stream, _unitCreatedId);
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            _incidentId = Read4ByteInteger(Data, ref index);
            _unitCreatedId = Read4ByteInteger(Data, ref index);
        }

    }
}

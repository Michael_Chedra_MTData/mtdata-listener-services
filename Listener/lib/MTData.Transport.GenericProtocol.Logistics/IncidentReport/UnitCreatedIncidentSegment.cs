﻿using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class UnitCreatedIncidentSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.UnitCreatedIncident;

        private UnitCreatedIncident _incident;

        public UnitCreatedIncident Incident
        {
            get { return _incident; }
            set { _incident = value; }
        }


        public UnitCreatedIncidentSegment()
        {
            Version = 2;
            Type = (int)SegmentType;
        }

        public UnitCreatedIncidentSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }


        public override byte[] GetBytes()
        {
            Data = _incident.EncodePayload();
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;
            _incident = new UnitCreatedIncident();
            _incident.DecodePayload(Data, ref index, Version);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class IncidentsForAssetSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendIncidentsForAsset;

        private List<IncidentItem> _incidentItems = new List<IncidentItem>();
        private int _assetUniqueKey;
        private int _slaveFleetId;
        private int _slaveVehicleId;

        public int AssetUniqueKey
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }
        public int SlaveFleetId
        {
            get { return _slaveFleetId; }
            set { _slaveFleetId = value; }
        }
        public int SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }


        public IncidentsForAssetSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
            _assetUniqueKey = -1;
            _slaveFleetId = -1;
            _slaveVehicleId = -1;
        }

        public IncidentsForAssetSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public List<IncidentItem> IncidentItems
        {
            get { return _incidentItems; }
            set { _incidentItems = value ?? new List<IncidentItem>(); }
        }

        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _assetUniqueKey);
                Write4ByteInteger(stream, _slaveFleetId);
                Write4ByteInteger(stream, _slaveVehicleId);
                WriteMoreFlag(stream, _incidentItems.Count);
                foreach (IncidentItem i in _incidentItems)
                {
                    i.Encode(stream);
                }

                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;

            _assetUniqueKey = Read4ByteInteger(Data, ref index);
            _slaveFleetId = Read4ByteInteger(Data, ref index);
            _slaveVehicleId = Read4ByteInteger(Data, ref index);
            int count = ReadMoreFlag(Data, ref index);
            List<IncidentItem> incidentItems = new List<IncidentItem>(count);
            for (int i = 0; i < count; i++)
            {
                IncidentItem incident = new IncidentItem();
                incident.Decode(Data, ref index);
                incidentItems.Add(incident);
            }
            _incidentItems = incidentItems;
        }
    }
}

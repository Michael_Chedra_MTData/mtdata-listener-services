﻿using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class UnitCreatedIncident : IPacketEncode
    {
        private int _version = 2;
        private AnsweredQuestions _answers;
        private int _templateListItemId;
        private int _unitCreatedId;
        private DateTime _incidentTime;
        private string _description;
        private int _fleetId;
        private int _vehicleId;
        private int _assetListItemId;

        public AnsweredQuestions Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }

        public int TemplateListItemId
        {
            get { return _templateListItemId; }
            set { _templateListItemId = value; }
        }

        public int UnitCreatedId
        {
            get { return _unitCreatedId; }
            set { _unitCreatedId = value; }
        }

        public DateTime IncidentTime
        {
            get { return _incidentTime; }
            set { _incidentTime = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public int AssetListItemId
        {
            get { return _assetListItemId; }
            set { _assetListItemId = value; }
        }

        public UnitCreatedIncident()
        {
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            DecodePayload(data, ref index, versionNumber);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

        }
        public void DecodePayload(byte[] data, ref int index, int versionNumber)
        {
            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _answers = new AnsweredQuestions();
                _answers.Decode(data, ref index);
            }
            else
            {
                _answers = null;
            }
            _templateListItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _unitCreatedId = BaseLayer.Read4ByteInteger(data, ref index);
            _incidentTime = BaseLayer.ReadDateTime(data, ref index);
            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _description = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _description = null;
            }

            if (versionNumber >= 2)
            {
                _fleetId = BaseLayer.Read4ByteInteger(data, ref index);
                _vehicleId = BaseLayer.Read4ByteInteger(data, ref index);
                _assetListItemId = BaseLayer.Read4ByteInteger(data, ref index);
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write version number
            BaseLayer.WriteMoreFlag(stream, _version);
            byte[] data = EncodePayload();
            //write the length
            BaseLayer.WriteMoreFlag(stream, data.Length);
            //now write the data
            stream.Write(data, 0, data.Length);
        }


        public byte[] EncodePayload()
        {
            using (MemoryStream local = new MemoryStream())
            {
                int fieldsFlags = (_answers != null ? 1 : 0)
                    | (_description != null ? 1 << 1 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);

                if (_answers != null)
                {
                    _answers.Encode(local);
                }
                BaseLayer.Write4ByteInteger(local, _templateListItemId);
                BaseLayer.Write4ByteInteger(local, _unitCreatedId);
                BaseLayer.WriteDateTime(local, _incidentTime);
                if (_description != null)
                {
                    BaseLayer.WriteString(local, _description);
                }

                // Version 2
                BaseLayer.Write4ByteInteger(local, _fleetId);
                BaseLayer.Write4ByteInteger(local, _vehicleId);
                BaseLayer.Write4ByteInteger(local, _assetListItemId);

                return local.ToArray();
            }
        }
    }
}

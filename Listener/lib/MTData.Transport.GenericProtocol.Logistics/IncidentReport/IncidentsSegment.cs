﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class IncidentsSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.Incidents;

        private List<IncidentItem> _incidentItems = new List<IncidentItem>();

        public IncidentsSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public IncidentsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public List<IncidentItem> IncidentItems
        {
            get { return _incidentItems; }
            set { _incidentItems = value ?? new List<IncidentItem>(); }
        }

        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, _incidentItems.Count);
                foreach (IncidentItem d in _incidentItems)
                {
                    d.Encode(stream);
                }

                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);
            List<IncidentItem> incidentItems = new List<IncidentItem>(count);
            for (int i = 0; i < count; i++)
            {
                IncidentItem d = new IncidentItem();
                d.Decode(Data, ref index);
                incidentItems.Add(d);
            }
            _incidentItems = incidentItems;
        }
    }

}

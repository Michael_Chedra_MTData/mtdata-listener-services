using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.IncidentReport
{
    public class IncidentReqExtraDataSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.IncidentReqExtraData;

        private int _incidentId = 0;

        /// <summary>
        /// The ID of the incident that the request corresponds to. 
        /// If the incdient ID is not yet known then the creation ID multiplied by -1 should be used instead.
        /// </summary>
        public int IncidentId
        {
            get { return _incidentId; }
            set { _incidentId = value; }
        }

        public IncidentReqExtraDataSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public IncidentReqExtraDataSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _incidentId);
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;
            _incidentId = Read4ByteInteger(Data, ref index);
        }
    }
}
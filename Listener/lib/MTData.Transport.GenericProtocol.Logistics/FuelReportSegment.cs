using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class FuelReportSegment : SegmentLayer
    {
        #region private fields
        private int _fuelTypeListId;
        private int _fuelTypeListUniqueKey;
        private int _fuelStationId;
        private ListId _fuelStationListId;
        private float _pricePerLitre;
        private float _numberOfLitres;
        private float _totalCost;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public int FuelTypeListId
        {
            get { return _fuelTypeListId; }
            set { _fuelTypeListId = value; }
        }
        public int FuelTypeListUniqueKey
        {
            get { return _fuelTypeListUniqueKey; }
            set { _fuelTypeListUniqueKey = value; }
        }
        public int FuelStationId
        {
            get { return _fuelStationId; }
            set { _fuelStationId = value; }
        }
        public ListId FuelStationListId
        {
            get { return _fuelStationListId; }
            set { _fuelStationListId = value; }
        }
        public float PricePerLitre
        {
            get { return _pricePerLitre; }
            set { _pricePerLitre = value; }
        }
        public float NumberOfLitres
        {
            get { return _numberOfLitres; }
            set { _numberOfLitres = value; }
        }
        public float TotalCost
        {
            get { return _totalCost; }
            set { _totalCost = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public FuelReportSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.FuelReport;
            _fuelStationListId = new ListId();
        }
        public FuelReportSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.FuelReport)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.FuelReport, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.FuelReport;
            _fuelStationListId = new ListId();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fuelStationId);
                _fuelStationListId.Encode(stream);
                int value = (int)((_pricePerLitre * 1000) + 0.5);
                Write4ByteInteger(stream, value);
                value = (int)((_numberOfLitres * 100) + 0.5);
                Write4ByteInteger(stream, value);
                value = (int)((_totalCost * 100) + 0.5);
                Write4ByteInteger(stream, value);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                if (Version >= 2)
                {
                    Write4ByteInteger(stream, _fuelTypeListId);
                    Write4ByteInteger(stream, _fuelTypeListUniqueKey);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fuelStationId = Read4ByteInteger(Data, ref index);
            _fuelStationListId.Decode(Data, ref index);
            int value = Read4ByteInteger(Data, ref index);
            _pricePerLitre = value / 1000f;
            value = Read4ByteInteger(Data, ref index);
            _numberOfLitres = value / 100f;
            value = Read4ByteInteger(Data, ref index);
            _totalCost = value / 100f;
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            if (Version >= 2)
            {
                _fuelTypeListId = Read4ByteInteger(Data, ref index);
                _fuelTypeListUniqueKey = Read4ByteInteger(Data, ref index);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// a logout packet
    /// </summary>
    public class LogoutSegment : SegmentLayer
    {
        #region private fields
        private int _odometer;
        private int _totalFuel;
        private string _serial;
        private int _palletsCount;
        private bool _forcedLogoutFromBase;
        #endregion

        #region properties
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        public string Serial
        {
            get { return _serial; }
            set { _serial = value; }
        }
        public int PalletsCount
        {
            get { return _palletsCount; }
            set { _palletsCount = value; }
        }
        public bool ForcedLogoutFromBase
        {
            get { return _forcedLogoutFromBase; }
            set { _forcedLogoutFromBase = value; }
        }
        #endregion

        #region constructor
        public LogoutSegment()
        {
            Version = 3;
            Type = (int)LogisticsSegmentTypes.Logout;
        }
        public LogoutSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.Logout)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.Logout, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.Logout;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                if (Version < 2)
                {
                    int serial = 0;
                    try
                    {
                        serial = Convert.ToInt32(_serial);
                    }
                    catch { }
                    Write4ByteInteger(stream, serial);
                }
                else
                {
                    WriteString(stream, _serial, 0x12);
                }                
                Write2ByteInteger(stream, _palletsCount);
                if (Version >= 3)
                {
                    WriteBool(stream, _forcedLogoutFromBase);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            if (Version < 2)
            {
                _serial = Read4ByteInteger(Data, ref index).ToString();
            }
            else
            {
                _serial = ReadString(Data, ref index, 0x12);
            }
            _palletsCount = Read2ByteInteger(Data, ref index);
            if (Version >= 3)
            {
                _forcedLogoutFromBase = ReadBool(Data, ref index);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public class StartAssociationSegment : SegmentLayer
    {
        #region private fields
        private Association _association;
        #endregion

        #region properties
        public int AssetUniqueId
        {
            get { return _association.AssetUniqueId; }
            set { _association.AssetUniqueId = value; }
        }
        public int AssetListId
        {
            get { return _association.AssetListId; }
            set { _association.AssetListId = value; }
        }

        public AssociationTypes AssociationType
        {
            get { return _association.AssociationType; }
            set { _association.AssociationType = value; }
        }

        public Association Association
        {
            get { return _association; }
            set { _association = value; }
        }
        #endregion

        #region constructor
        public StartAssociationSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.StartAssociation;
            _association = new Association();
        }
        public StartAssociationSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.StartAssociation)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.StartAssociation, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.StartAssociation;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                //set the data
                _association.Encode(stream);
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _association = new Association();
            if (Version <= 1)
            {
                _association.DecodeHeaderData(Data, ref index); 
            }
            else if (Version >= 2) // Version 2 includes association packetEncode version number
            {
                _association.Decode(Data, ref index);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    /// <summary>
    /// This segment is used to send a warning to a driver when an association is
    /// made with an asset that is currently associated with another vehicle.
    /// </summary>
    public class AssociationWarningSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.AssociationWarning;
        private int _terminalCreationId;
        private int _vehicleId;
        private int _fleetId;
        private string _vehicleName;

        /// <summary>
        /// The ID (generated by Talon) of the offending association that triggered this warning.
        /// </summary>
        public int TerminalCreationId
        {
            get { return _terminalCreationId; }
            set { _terminalCreationId = value; }
        }

        /// <summary>
        /// The ID of the vehicle that the association was previously with.
        /// </summary>
        public int VehicleId 
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        /// <summary>
        /// The ID of the fleet that the association was previously with.
        /// </summary>
        public int FleetId 
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }

        /// <summary>
        /// The name of the vehicle that the association was previously with.
        /// </summary>
        public string VehicleName 
        {
            get { return _vehicleName; }
            set { _vehicleName = value; }
        }

        #region constructor
        public AssociationWarningSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }
        public AssociationWarningSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _terminalCreationId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _fleetId);
                WriteString(stream, _vehicleName, 0x12);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _terminalCreationId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleName = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    /// <summary>
    /// Segment sent to Talon to syncronise associations. 
    /// This segment is sent when a driver logs in and also when an association is 
    /// added or removed from a vehicle and the change was not made on the vehicle.
    /// </summary>
    public class AssociationSyncSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.AssociationSync;

        private List<AssociationSyncItem> _addedAssociations = new List<AssociationSyncItem>();
        private List<AssociationSyncItem> _removedAssociations = new List<AssociationSyncItem>();

        public AssociationSyncSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public AssociationSyncSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        /// <summary>
        /// Includes assets that have been associated with the vehicle.
        /// </summary>
        public List<AssociationSyncItem> AddedAssociations
        {
            get { return _addedAssociations; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _addedAssociations = value;
            }
        }

        /// <summary>
        /// Inludes associations that caused an asset to be removed from the vehicle. 
        /// For example this could include an association of an asset that was previously associated with this vehicle.
        /// This allows Talon to present a detailed warning to the driver when an association is removed.
        /// For example "Trailer A was moved to Vehicle B at 10:05am by pauld"
        /// </summary>
        public List<AssociationSyncItem> RemovedAssociations
        {
            get { return _removedAssociations; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _removedAssociations = value;
            }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, _addedAssociations.Count);
                foreach (AssociationSyncItem a in _addedAssociations)
                {
                    a.Encode(stream);
                }
                WriteMoreFlag(stream, _removedAssociations.Count);
                foreach (AssociationSyncItem a in _removedAssociations)
                {
                    a.Encode(stream);
                }

                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);
            _addedAssociations = new List<AssociationSyncItem>(count);
            for (int i = 0; i < count; i++)
            {
                AssociationSyncItem a = new AssociationSyncItem();
                a.Decode(Data, ref index);
                _addedAssociations.Add(a);
            }
            count = ReadMoreFlag(Data, ref index);
            _removedAssociations = new List<AssociationSyncItem>(count);
            for (int i = 0; i < count; i++)
            {
                AssociationSyncItem a = new AssociationSyncItem();
                a.Decode(Data, ref index);
                _removedAssociations.Add(a);
            }
        }
    }
}

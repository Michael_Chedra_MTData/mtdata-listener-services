﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public class AssociationErrorSegment : SegmentLayer
    {
        #region private fields
        private int _assetUniqueId;
        private int _assetListId;
        private DateTime _gpsTime;
        private string _errorMessage;
        #endregion

        #region properties
        public int AssetUniqueId
        {
            get { return _assetUniqueId; }
            set { _assetUniqueId = value; }
        }
        public int AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        }

        public DateTime GpsTime
        {
            get { return _gpsTime; }
            set { _gpsTime = value; }
        }
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        #endregion

        #region constructor
        public AssociationErrorSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AssociationError;
        }
        public AssociationErrorSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AssociationError)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AssociationError, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AssociationError;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _assetUniqueId);
                Write4ByteInteger(stream, _assetListId);
                WriteDateTime(stream, _gpsTime);
                WriteString(stream, _errorMessage, 0x12);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _assetUniqueId = Read4ByteInteger(Data, ref index);
            _assetListId = Read4ByteInteger(Data, ref index);
            _gpsTime = ReadDateTime(Data, ref index);
            _errorMessage = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public class AssociationMessageReceivedSegment : SegmentLayer
    {
        #region private fields
        private int _messageId;
        #endregion

        #region properties
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        #endregion

        #region constructor
        public AssociationMessageReceivedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AssociationMessageReceived;
        }
        public AssociationMessageReceivedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AssociationMessageReceived)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AssociationMessageReceived, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AssociationMessageReceived;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _messageId);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _messageId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

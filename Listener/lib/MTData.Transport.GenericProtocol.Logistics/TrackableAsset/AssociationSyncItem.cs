﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public class AssociationSyncItem : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 2;
        private int _associationId;
        private int? _terminalCreationId;
        private AssociationTypes _associationType;
        private int? _assetUniqueKey;
        private int? _assetListId;
        private string _assetName;
        private int? _slaveFleetId;
        private int? _slaveVehicleId;
        private int? _masterFleetId;
        private int? _masterVehicleId;
        private string _userName;
        private string _driverName;
        private DateTime _associationTime;
        // The following are required when sending down details of an association being removed
        // This makes it possible for Talon to display where an asset was moved. 
        
        private string _masterVehicleName;
        private bool _isLocationAssociation;
        #endregion

        #region properties
        public int AssociationId 
        {
            get { return _associationId; }
            set { _associationId = value; }
        }
        public int? TerminalCreationId 
        {
            get { return _terminalCreationId; }
            set { _terminalCreationId = value; }
        }

        public AssociationTypes AssociationType
        {
            get { return _associationType; }
            set { _associationType = value; }
        }

        public int? AssetUniqueKey 
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }

        public int? AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        }

        public string AssetName 
        {
            get { return _assetName; }
            set { _assetName = value; }
        }

        public int? SlaveFleetId
        {
            get { return _slaveFleetId; }
            set { _slaveFleetId = value; } // In version 1 the property set was incorrect.
        }

        public int? SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }

        public int? MasterFleetId
        {
            get { return _masterFleetId; }
            set { _masterFleetId = value; }
        }

        public int? MasterVehicleId
        {
            get { return _masterVehicleId; }
            set { _masterVehicleId = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        
        public string DriverName
        {
            get { return _driverName; }
            set { _driverName = value; }
        } 

        public DateTime AssociationTime
        {
            get { return _associationTime; }
            set { _associationTime = value; }
        }

        public string MasterVehicleName
        {
            get { return _masterVehicleName; }
            set { _masterVehicleName = value; }
        }

        public bool IsLocationAssociation
        {
            get { return _isLocationAssociation; }
            set { _isLocationAssociation = value; }
        }
        #endregion

        #region constructor
        public AssociationSyncItem()
        {
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            DecodeData(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            byte[] data = GetBytes();
            //write version number
            BaseLayer.WriteMoreFlag(stream, _versionNumber);
            //write the length
            BaseLayer.WriteMoreFlag(stream, data.Length);
            //now write the data
            stream.Write(data, 0, data.Length);
        }
        #endregion

        private byte[] GetBytes()
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                int fieldsFlags = (_terminalCreationId != null ? 1 : 0)
                    | (_assetUniqueKey.HasValue ? 1 << 1 : 0)
                    | (_assetListId.HasValue ? 1 << 2 : 0)
                    | (_assetName != null ? 1 << 3 : 0)
                    | (_slaveFleetId.HasValue ? 1 << 4 : 0)
                    | (_slaveVehicleId.HasValue ? 1 << 5 : 0)
                    | (_masterFleetId.HasValue ? 1 << 6 : 0)
                    | (_masterVehicleId.HasValue ? 1 << 7 : 0)
                    | (_userName != null ? 1 << 8 : 0)
                    | (_driverName != null ? 1 << 9 : 0)
                    | (_masterVehicleName != null ? 1 << 10 : 0)
                    | (_isLocationAssociation ? 1 << 11 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                BaseLayer.Write4ByteInteger(local, _associationId);
                if (_terminalCreationId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _terminalCreationId.Value);
                }
                local.WriteByte((byte)_associationType);
                if (_assetUniqueKey.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _assetUniqueKey.Value);
                }
                if (_assetListId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _assetListId.Value);
                }
                if (_assetName != null)
                {
                    BaseLayer.WriteString(local, _assetName);
                }
                if (_slaveFleetId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _slaveFleetId.Value);
                }
                if (_slaveVehicleId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _slaveVehicleId.Value);
                }
                if (_masterFleetId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _masterFleetId.Value);
                }
                if (_masterVehicleId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _masterVehicleId.Value);
                }
                if (_userName != null)
                {
                    BaseLayer.WriteString(local, _userName);
                }
                if (_driverName != null)
                {
                    BaseLayer.WriteString(local, _driverName);
                }
                BaseLayer.WriteDateTime(local, _associationTime);
                if (_masterVehicleName != null)
                {
                    BaseLayer.WriteString(local, _masterVehicleName);
                }
                return local.ToArray();
            }
        }

        private void DecodeData(byte[] data, ref int index)
        {

            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
            _associationId = BaseLayer.Read4ByteInteger(data, ref index);
            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _terminalCreationId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _terminalCreationId = null;
            }
            _associationType = (AssociationTypes)data[index++];
            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _assetUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _assetUniqueKey = null;
            }
            if ((fieldsFlags & (1 << 2)) != 0)
            {
                _assetListId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _assetListId = null;
            }
            if ((fieldsFlags & (1 << 3)) != 0)
            {
                _assetName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _assetName = null;
            }
            if ((fieldsFlags & (1 << 4)) != 0)
            {
                _slaveFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _slaveFleetId = null;
            }
            if ((fieldsFlags & (1 << 5)) != 0)
            {
                _slaveVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _slaveVehicleId = null;
            }
            if ((fieldsFlags & (1 << 6)) != 0)
            {
                _masterFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _masterFleetId = null;
            }
            if ((fieldsFlags & (1 << 7)) != 0)
            {
                _masterVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _masterVehicleId = null;
            }
            if ((fieldsFlags & (1 << 8)) != 0)
            {
                _userName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _userName = null;
            }
            if ((fieldsFlags & (1 << 9)) != 0)
            {
                _driverName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _driverName = null;
            }
            _associationTime = BaseLayer.ReadDateTime(data, ref index);
            if ((fieldsFlags & (1 << 10)) != 0)
            {
                _masterVehicleName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _masterVehicleName = null;
            }
            _isLocationAssociation = ((fieldsFlags & (1 << 11)) != 0);
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    /// <summary>
    /// Segment sent to Talon to refresh all associations. 
    /// This segment is sent when a driver logs in.
    /// </summary>
    public class AssociationRefreshSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.AssociationRefresh;

        private int _lastAssociationId;
        private List<AssociationSyncItem> _currentAssociations = new List<AssociationSyncItem>();

        public AssociationRefreshSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public AssociationRefreshSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        /// <summary>
        /// Includes all assets currently associated with the vehicle.
        /// </summary>
        public List<AssociationSyncItem> CurrentAssociations
        {
            get { return _currentAssociations; }
            set
            {
                if (value == null)
                {
                    throw new ArgumentNullException("value");
                }
                _currentAssociations = value;
            }
        }

        /// <summary>
        /// The terminalCreatedId of the last association event that the server was
        /// aware of at the time this refresh segment was created. 
        /// </summary>
        public int LastAssociationId
        {
            get { return _lastAssociationId; }
            set { _lastAssociationId = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, _currentAssociations.Count);
                foreach (AssociationSyncItem a in _currentAssociations)
                {
                    a.Encode(stream);
                }
                Write4ByteInteger(stream, _lastAssociationId);

                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);
            _currentAssociations = new List<AssociationSyncItem>(count);
            for (int i = 0; i < count; i++)
            {
                AssociationSyncItem a = new AssociationSyncItem();
                a.Decode(Data, ref index);
                _currentAssociations.Add(a);
            }
            _lastAssociationId = Read4ByteInteger(Data, ref index);

        }

    }

}

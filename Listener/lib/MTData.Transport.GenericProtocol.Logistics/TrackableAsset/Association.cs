﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public class Association : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 2;
        private int _assetUniqueId;
        private int _assetListId;
        private AssociationTypes _associationType;
        private int _slaveVehicleId;
        private int _terminalCreationId;
        private int _dispatchId;
        private int _legActionId;
        #endregion

        #region properties
        public int AssetUniqueId
        {
            get { return _assetUniqueId; }
            set { _assetUniqueId = value; }
        }
        public int AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        }

        public AssociationTypes AssociationType
        {
            get { return _associationType; }
            set { _associationType = value; }
        }

        public int SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }
        public int TerminalCreationId
        {
            get { return _terminalCreationId; }
            set { _terminalCreationId = value; }
        }
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public int LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }
        #endregion

        #region constructor
        public Association()
        {
            _dispatchId = -1;
            _legActionId = -1;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            // For backwards version number and lengther are written after header data
            DecodeHeaderData(data, ref index);

            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            DecodeData(data, ref index, versionNumber);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void DecodeHeaderData(byte[] data, ref int index)
        {
            _assetUniqueId = BaseLayer.Read4ByteInteger(data, ref index);
            _assetListId = BaseLayer.Read4ByteInteger(data, ref index);
            _associationType = (AssociationTypes)data[index++];
        }

        private void EncodeHeaderData(Stream stream)
        {
            BaseLayer.Write4ByteInteger(stream, _assetUniqueId);
            BaseLayer.Write4ByteInteger(stream, _assetListId);
            stream.WriteByte((byte)_associationType);
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            byte[] data = GetBytes();
            // Write Header Data - always has a length of 9 bytes
            EncodeHeaderData(stream);
            //write version number
            BaseLayer.WriteMoreFlag(stream, _versionNumber);
            //write the length
            BaseLayer.WriteMoreFlag(stream, data.Length);
            //now write the data
            stream.Write(data, 0, data.Length);
        }
        #endregion

        private byte[] GetBytes()
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _slaveVehicleId);
                BaseLayer.Write4ByteInteger(local, _terminalCreationId);
                BaseLayer.Write4ByteInteger(local, _dispatchId);
                BaseLayer.Write4ByteInteger(local, _legActionId);
                return local.ToArray();
            }
        }

        private void DecodeData(byte[] data, ref int index, int versionNumber)
        {
            _slaveVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            _terminalCreationId = BaseLayer.Read4ByteInteger(data, ref index);
            _dispatchId = -1;
            _legActionId = -1;
            if (versionNumber > 1)
            {
                _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
                _legActionId = BaseLayer.Read4ByteInteger(data, ref index);
            }
        }
    }
}
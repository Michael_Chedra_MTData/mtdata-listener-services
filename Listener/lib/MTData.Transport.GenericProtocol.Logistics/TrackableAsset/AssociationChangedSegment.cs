﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public class AssociationChangedSegment : SegmentLayer
    {
        #region private fields
        private int _assetUniqueId;
        private int _assetListId;
        private DateTime _startTime;
        private DateTime _endTime;
        private int _messageId;
        private string _message;
        #endregion

        #region properties
        public int AssetUniqueId
        {
            get { return _assetUniqueId; }
            set { _assetUniqueId = value; }
        }
        public int AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        }

        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        #endregion

        #region constructor
        public AssociationChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AssociationChange;
        }
        public AssociationChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AssociationChange)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AssociationChange, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AssociationChange;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _assetUniqueId);
                Write4ByteInteger(stream, _assetListId);
                WriteDateTime(stream, _startTime);
                WriteDateTime(stream, _endTime);
                Write4ByteInteger(stream, _messageId);
                WriteString(stream, _message, 0x12);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _assetUniqueId = Read4ByteInteger(Data, ref index);
            _assetListId = Read4ByteInteger(Data, ref index);
            _startTime = ReadDateTime(Data, ref index);
            _endTime = ReadDateTime(Data, ref index);
            _messageId = Read4ByteInteger(Data, ref index);
            _message = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

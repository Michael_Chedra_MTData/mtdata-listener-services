﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.TrackableAsset
{
    public enum AssociationTypes
    {
        // Every association is either a vehicle or location association therefore this must be stored seperate.
        // To determine if an association is a location association check if the MasterVehicleId is null.
        [Obsolete("A location type association is now specified by setting the MasterFleetId and MasterVehicleId to null ")]
        Location = 0,
        Manual = 1,
        Driver = 2,
        Job = 3,
        Auto = 4,
        DriverRedeclare = 5
    }
}

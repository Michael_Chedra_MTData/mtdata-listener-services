using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// class to hold the version info of an object (major and minor numbers)
    /// </summary>
    public class VersionInfo : IPacketEncode
    {
        private int _major;
        private int _minor;

        public VersionInfo() { }
        public VersionInfo(int major, int minor)
        {
            _major = major;
            _minor = minor;
        }

        public int Major { get { return _major; } set { _major = value; } }
        public int Minor { get { return _minor; } set { _minor = value; } }

        #region IPacketEncode members

        public void Encode(System.IO.MemoryStream stream)
        {
            stream.WriteByte((byte)(_major));
            stream.WriteByte((byte)(_minor));
        }

        public void Decode(byte[] data, ref int index)
        {
            _major = (int)data[index++];
            _minor = (int)data[index++];
        }
        #endregion
    }
}

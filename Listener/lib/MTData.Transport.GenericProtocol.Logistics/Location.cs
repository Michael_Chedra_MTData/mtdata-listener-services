using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// the location types
    /// </summary>
    public enum LocationTypes
    {
        Rectangle,
        Circle,
        Polygon
    }

    /// <summary>
    /// A Location
    /// </summary>
    public class Location : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private LocationTypes _locationType;
        private LocationPoint _centrePoint;
        private int _radius;
        private int _northSouthDistance;
        private int _eastWestDistance;
        private List<LocationPoint> _points;
        #endregion

        #region public properties
        /// <summary>
        /// the location type
        /// </summary>
        public LocationTypes LocationType { get { return _locationType; } }
        /// <summary>
        /// the centre point of this location
        /// </summary>
        public LocationPoint CentrePoint { get { return _centrePoint; } set { _centrePoint = value; } }
        /// <summary>
        /// the radius of the location (only used for circles)
        /// </summary>
        public int Radius 
        {
            get 
            {
                switch (_locationType)
                {
                    case LocationTypes.Circle:
                        return _radius;
                    case LocationTypes.Rectangle:
                        if (_northSouthDistance > _eastWestDistance)
                        {
                            return _northSouthDistance;
                        }
                        return _eastWestDistance;
                    case LocationTypes.Polygon:
                        return CalculateRadiusForPolygon();
                }
                return 0;
            } 
            set { _radius = value; } 
        }
        /// <summary>
        /// the north/South distance of the location (only used for rectangle)
        /// </summary>
        public int NorthSouthDistance { get { return _northSouthDistance; } set { _northSouthDistance = value; } }
        /// <summary>
        /// the east/west distance of the location (only used for rectangle)
        /// </summary>
        public int EastWestDistance { get { return _eastWestDistance; } set { _eastWestDistance = value; } }
        /// <summary>
        /// the points of the location (only used for polygon)
        /// </summary>
        public List<LocationPoint> Points { get { return _points; } set { _points = value; } }
        #endregion

        #region constructor
        public Location()
        {
            _locationType = LocationTypes.Circle;
            _centrePoint = new LocationPoint();
            _points = new List<LocationPoint>();
        }
        public Location(LocationTypes type)
        {
            _locationType = type;
            _centrePoint = new LocationPoint();
            _points = new List<LocationPoint>();
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _locationType = (LocationTypes)data[index++];
            _centrePoint.Latitude = BaseLayer.ReadCoordinate(data, ref index);
            _centrePoint.Longitude = BaseLayer.ReadCoordinate(data, ref index);
            _points.Clear();
            switch (_locationType)
            {
                case LocationTypes.Circle:
                    _radius = BaseLayer.Read2ByteInteger(data, ref index);
                    break;
                case LocationTypes.Rectangle:
                    _northSouthDistance = BaseLayer.Read2ByteInteger(data, ref index);
                    _eastWestDistance = BaseLayer.Read2ByteInteger(data, ref index);
                    break;
                case LocationTypes.Polygon:
                    int count = BaseLayer.Read2ByteInteger(data, ref index);
                    for (int i=0; i<count; i++)
                    {
                        LocationPoint point = new LocationPoint();
                        point.Latitude = BaseLayer.ReadCoordinate(data, ref index);
                        point.Longitude = BaseLayer.ReadCoordinate(data, ref index);
                        _points.Add(point);
                    }
                    if(versionNumber >= 2)
                    {
                        _northSouthDistance = BaseLayer.Read2ByteInteger(data, ref index);
                        _eastWestDistance = BaseLayer.Read2ByteInteger(data, ref index);
                    }
                    break;
            }
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_locationType);
                BaseLayer.WriteCoordinate(local, _centrePoint.Latitude);
                BaseLayer.WriteCoordinate(local, _centrePoint.Longitude);
                switch (_locationType)
                {
                    case LocationTypes.Circle:
                        BaseLayer.Write2ByteInteger(local, _radius);
                        break;
                    case LocationTypes.Rectangle:
                        BaseLayer.Write2ByteInteger(local, _northSouthDistance);
                        BaseLayer.Write2ByteInteger(local, _eastWestDistance);
                        break;
                    case LocationTypes.Polygon:
                        BaseLayer.Write2ByteInteger(local, _points.Count);
                        foreach (LocationPoint p in _points)
                        {
                            BaseLayer.WriteCoordinate(local, p.Latitude);
                            BaseLayer.WriteCoordinate(local, p.Longitude);
                        }
                        BaseLayer.Write2ByteInteger(local, _northSouthDistance);
                        BaseLayer.Write2ByteInteger(local, _eastWestDistance);
                        break;
                }


                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion

        #region public methods
        public bool IsInArea(LocationPoint point)
        {
            return IsInArea(point.Latitude, point.Longitude);
        }
        public bool IsInArea(double latitude, double longitude)
        {
            switch (_locationType)
            {
                case LocationTypes.Circle:
                    return IsInCircle(latitude, longitude);
                case LocationTypes.Rectangle:
                    return IsInRectangle(latitude, longitude);
                case LocationTypes.Polygon:
                    if (_northSouthDistance > 0 && _eastWestDistance > 0)
                    {
                        if (IsInRectangle(latitude, longitude))
                        {
                            return IsInPolygon(latitude, longitude);
                        }

                        return false;
                    }
                    else
                    {
                        return IsInPolygon(latitude, longitude);
                    }
            }
            return false;
        }
        #endregion

        #region private methods
        private bool IsInCircle(double latitude, double longitude)
        {
            double distance = DistanceBetweenPoints(_centrePoint.Latitude, _centrePoint.Longitude, latitude, longitude);
            if (distance < _radius)
            {
                return true;
            }
            return false;
        }

        private bool IsInRectangle(double latitude, double longitude)
        {
            double toleranceLat = DistanceLatitude(_northSouthDistance);
            double toleranceLong = DistanceLongitude(_eastWestDistance, _centrePoint.Latitude);

            double topLeftLat = _centrePoint.Latitude + toleranceLat;
            double bottomRightLat = _centrePoint.Latitude - toleranceLat;
            double topLeftLong = _centrePoint.Longitude - toleranceLong;
            double bottomRightLong = _centrePoint.Longitude + toleranceLong;

            if (latitude < topLeftLat && latitude > bottomRightLat &&
               longitude > topLeftLong && longitude < bottomRightLong)
            {
                return true;
            }
            return false;
        }

        private bool IsInPolygon(double latitude, double longitude)
        {
            LocationPoint lastPoint = _points[_points.Count - 1];
            bool inside = false;
            foreach (LocationPoint p in _points)
            {
                if ((p.Latitude <= latitude && latitude < lastPoint.Latitude) || (lastPoint.Latitude <= latitude && latitude < p.Latitude))
                {
                    if (longitude < (lastPoint.Longitude - p.Longitude) * (latitude - p.Latitude) / (lastPoint.Latitude - p.Latitude) + p.Longitude)
                    {
                        inside = !inside;
                    }
                }

                //set last point to this one
                lastPoint = p;
            }

            return inside;
        }

        private int CalculateRadiusForPolygon()
        {
            double radius = 0;
            foreach (LocationPoint p in _points)
            {
                double r = DistanceBetweenPoints(p.Latitude, p.Longitude, _centrePoint.Latitude, _centrePoint.Longitude);
                if (r > radius)
                {
                    radius = r;
                }
            }
            return (int)(radius + 0.5);
        }

        /// <summary>
        /// calculate the distance in m between 2 points
        /// </summary>
        /// <param name="lat1">latitude of point 1</param>
        /// <param name="lon1">longitude of point 1</param>
        /// <param name="lat2">latitude of point 2</param>
        /// <param name="lon2">longitude of point 2</param>
        /// <returns>the distance in m between the 2 points</returns>
        private double DistanceBetweenPoints(double lat1, double lon1, double lat2, double lon2)
        {
            //convert the degree values to radians before calculation
            lat1 = ToRadians(lat1);
            lon1 = ToRadians(lon1);
            lat2 = ToRadians(lat2);
            lon2 = ToRadians(lon2);

            double dlon = lon2 - lon1;
            double dlat = lat2 - lat1;

            double a = Math.Pow(Math.Sin(dlat / 2), 2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Pow(Math.Sin(dlon / 2), 2);
            double c = 2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a));

            return 6367000.0 * c;
        }
        /// <summary>
        /// Convert a coordinate from degress to radians
        /// </summary>
        /// <param name="degree">the coordinate in degrees</param>
        /// <returns>the coordinate in radians</returns>
        private double ToRadians(double degree)
        {
            return (degree * Math.PI) / 180.0;
        }
        /// <summary>
        /// Convert a coordinate from radians to degrees
        /// </summary>
        /// <param name="degree">the coordinate in radians</param>
        /// <returns>the coordinate in degress</returns>
        private double ToDegrees(double degree)
        {
            return (degree * 180) / Math.PI;
        }
        /// <summary>
        /// convert a distance in meters to degress latitude
        /// </summary>
        /// <param name="distanceMetres">the distance in metres</param>
        /// <returns>the distance in degress latitude</returns>
        private double DistanceLatitude(double distanceMetres)
        {
            // Convert to a rectangle expressed in lat/long coordinates:
            // 1 degree of Lat = 1852m. To express in minutes, divide by 60.
            // Hence ToleranceLat(mins) = (ToleranceY / 1852) / 60 
            // or just ToleranceY / (1852 * 60) => ToleranceY / 111120
            return distanceMetres / (double)111120;
        }
        /// <summary>
        /// convert a distance in meters to degress longitude
        /// </summary>
        /// <param name="distanceMetres">the distance in metres</param>
        /// <returns>the latitude at point</returns>
        private double DistanceLongitude(double distanceMetres, double latitude)
        {
            // In the X direction we have to use the cosine:
            // ToleranceLong(mins) = ToleranceX / cos(Lat) * 111120
            // Lat has to be in radians = lat(degs) * PI / 180;
            return distanceMetres / (Math.Cos(ToRadians(latitude)) * 111120);
        }
        #endregion
    }

    /// <summary>
    /// A Location Point
    /// </summary>
    public class LocationPoint
    {
        private double _latitude;
        private double _longitude;

        public double Latitude { get { return _latitude; } set { _latitude = value; } }
        public double Longitude { get { return _longitude; } set { _longitude = value; } }

        public LocationPoint()
        {
        }
        public LocationPoint(double latitude, double longitude)
        {
            _latitude = latitude;
            _longitude = longitude;
        }
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public enum InspectionResults : byte
    {
        None,
        Yes,
        No,
        N_A,
    }
    public class InspectionItem : IPacketEncode
    {
        private const int _versionNumber = 1;

        private int _defectTypeItemId;
        private InspectionResults _inspectionResult;
        private int _existingDefectId;
        private string _existingDefectNote;
        private UnitCreatedDefect _newDefect;

        public int DefectTypeItemId
        {
            get { return _defectTypeItemId; }
            set { _defectTypeItemId = value; }
        }

        public InspectionResults InspectionResults
        {
            get { return _inspectionResult; }
            set { _inspectionResult = value; }
        }
        public int ExistingDefectId
        {
            get { return _existingDefectId; }
            set { _existingDefectId = value; }
        }
        public string ExistingDefectNote
        {
            get { return _existingDefectNote; }
            set { _existingDefectNote = value; }
        }

        public UnitCreatedDefect NewDefect
        {
            get { return _newDefect; }
            set { _newDefect = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _defectTypeItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _inspectionResult = (InspectionResults)data[index++];
            _existingDefectId = BaseLayer.Read4ByteInteger(data, ref index);
            _existingDefectNote = BaseLayer.ReadString(data, ref index);

            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);

            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _newDefect = new UnitCreatedDefect();
                _newDefect.Decode(data, ref index);
            }
            else
            {
                _newDefect = null;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _defectTypeItemId);
                local.WriteByte((byte)_inspectionResult);
                BaseLayer.Write4ByteInteger(local, _existingDefectId);
                BaseLayer.WriteString(local, _existingDefectNote);

                int fieldsFlags = (_newDefect != null ? 1 << 0 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);

                if (_newDefect != null)
                {
                    _newDefect.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}
using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class UnitCreatedDefectSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.UnitCreatedDefect;

        private UnitCreatedDefect _defect;

        public UnitCreatedDefect Defect
        {
            get { return _defect; }
            set { _defect = value; }
        }
        public AnsweredQuestions Answers
        {
            get { return _defect.Answers; }
            set { _defect.Answers = value; }
        }

        public int TemplateListItemId
        {
            get { return _defect.TemplateListItemId; }
            set { _defect.TemplateListItemId = value; }
        }

        public int TypeListItemId
        {
            get { return _defect.TypeListItemId; }
            set { _defect.TypeListItemId = value; }
        }

        public int FleetId
        {
            get { return _defect.FleetId; }
            set { _defect.FleetId = value; }
        }

        public int VehicleId
        {
            get { return _defect.VehicleId; }
            set { _defect.VehicleId = value; }
        }

        public int AssetListItemId
        {
            get { return _defect.AssetListItemId; }
            set { _defect.AssetListItemId = value; }
        }

        public int UnitCreatedId
        {
            get { return _defect.UnitCreatedId; }
            set { _defect.UnitCreatedId = value; }
        }

        public string Comment
        {
            get { return _defect.Comment; }
            set { _defect.Comment = value; }
        }

        public UnitCreatedDefectSegment()
        {
            Version = 3;
            Type = (int)SegmentType;
            _defect = new UnitCreatedDefect();
        }

        public UnitCreatedDefectSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            _defect = new UnitCreatedDefect();
            DecodeData();
        }


        public override byte[] GetBytes()
        {
            Data = _defect.GetBytes();
            return base.GetBytes();
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;
            _defect.DecodeData(Data, ref index, Version);

        }
    }
}
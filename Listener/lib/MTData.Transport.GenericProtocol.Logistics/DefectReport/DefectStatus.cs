using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public enum DefectStatus : byte
    {
        Reported,
        Assessed,
        [Description("On Work Order")]
        OnWorkOrder,
        Closed
    }
}
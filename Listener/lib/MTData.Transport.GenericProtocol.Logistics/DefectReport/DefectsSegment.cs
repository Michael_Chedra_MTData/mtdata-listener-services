using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class DefectsSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.Defects;

        private List<DefectItem> _defectItems = new List<DefectItem>();

        public DefectsSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public DefectsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public List<DefectItem> DefectItems
        {
            get { return _defectItems; }
            set { _defectItems = value ?? new List<DefectItem>(); }
        }

        public override byte[] GetBytes()
        {

            using (MemoryStream stream = new MemoryStream())
            {
                WriteMoreFlag(stream, _defectItems.Count);
                foreach (DefectItem d in _defectItems)
                {
                    d.Encode(stream);
                }

                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;

            int count = ReadMoreFlag(Data, ref index);
            List<DefectItem> defectItems = new List<DefectItem>(count);
            for (int i = 0; i < count; i++)
            {
                DefectItem d = new DefectItem();
                d.Decode(Data, ref index);
                defectItems.Add(d);
            }
            _defectItems = defectItems;
        }
    }
}
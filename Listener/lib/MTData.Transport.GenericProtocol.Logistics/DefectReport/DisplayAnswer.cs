using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class DisplayAnswer : IPacketEncode
    {
        private const int _versionNumber = 2;

        private string _question = string.Empty;
        private QuestionTypes _questionType = QuestionTypes.YesNo;
        // All answers are stored as a string in the database. 
        // However multi-select list questions store multiple answers per question.   
        private List<string> _answer; 
        private List<byte[]> _photos = new List<byte[]>();

        public string Question
        { 
            get { return _question; }
            set { _question = value ?? string.Empty; }
        }
        
        public QuestionTypes QuestionType
        {
            get { return _questionType; }
            set { _questionType = value; }
        } 

        public List<string> Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }

        public List<byte[]> Photos
        {
            get { return _photos; }
            set { _photos = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
            _question = BaseLayer.ReadString(data, ref index);
            _questionType = (QuestionTypes)BaseLayer.ReadMoreFlag(data, ref index);
            if ((fieldsFlags & 1 << 0) != 0) 
            {
                int count = BaseLayer.ReadMoreFlag(data, ref index);
                if (count >= 0)
                {
                    _answer = new List<string>();
                    for (int i = 0; i < count; i++)
                    {
                        _answer.Add(BaseLayer.ReadString(data, ref index));
                    }
                }
            }
            else
            {
                _answer = null;
            }

            _photos = new List<byte[]>();
            if ((fieldsFlags & (1 << 1)) != 0)
            {
                int count = BaseLayer.ReadMoreFlag(data, ref index);
                byte[] p = count < 0 ? new byte[0] : new byte[count];
                Array.Copy(data, index, p, 0, count);
                index += count;
                _photos.Add(p);
            }
            if ((fieldsFlags & (1 << 2)) != 0)
            {
                int photoCount = data[index++];
                for (int i = 0; i < photoCount; i++)
                {
                    int count = BaseLayer.ReadMoreFlag(data, ref index);
                    byte[] p = count < 0 ? new byte[0] : new byte[count];
                    Array.Copy(data, index, p, 0, count);
                    index += count;
                    _photos.Add(p);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                int fieldsFlags = (_answer != null ? 1 : 0) 
                    | ((_photos != null && _photos.Count > 0) ? 1 << 1 : 0) 
                    | ((_photos != null && _photos.Count > 1) ? 1 << 2 : 0);

                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                BaseLayer.WriteString(local, _question);
                BaseLayer.WriteMoreFlag(local, (int)_questionType);
                if (_answer != null)
                {
                    BaseLayer.WriteMoreFlag(local, _answer.Count);
                    foreach (string a in _answer)
                    {
                        BaseLayer.WriteString(local, a);
                    }
                }
                if (_photos != null && _photos.Count > 0)
                {
                    for (int i=0; i<_photos.Count; i++)
                    {
                        if (i == 1)
                        {
                            local.WriteByte((byte)(_photos.Count - 1));
                        }

                        int length = _photos[i].Length;
                        BaseLayer.WriteMoreFlag(local, length);
                        if (length > 0)
                        {
                            local.Write(_photos[i], 0, length);
                        }
                    }
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}
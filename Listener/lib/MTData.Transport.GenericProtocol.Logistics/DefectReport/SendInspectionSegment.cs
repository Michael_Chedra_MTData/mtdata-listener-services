using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class SendInspectionSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendInspection;

        private Inspection _inspection = new Inspection();

        public SendInspectionSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SendInspectionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public Inspection Inspection
        {
            get { return _inspection; }
            set { _inspection = value; }
        }


        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                _inspection.Encode(stream);
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;

            Inspection inspection = new Inspection();
            inspection.Decode(Data, ref index);
            _inspection = inspection;
        }

    }
}
using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class UnitCreatedDefect : IPacketEncode
    {
        private const int _versionNumber = 3;
        private AnsweredQuestions _answers;
        private int _templateListItemId;
        private int _typeListItemId;
        private int _fleetId;
        private int _vehicleId;
        private int _assetListItemId;
        private int _unitCreatedId;
        private string _comment;
        private int? _defaultSeverityListItemId;

        public AnsweredQuestions Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }

        public int TemplateListItemId
        {
            get { return _templateListItemId; }
            set { _templateListItemId = value; }
        }

        public int TypeListItemId
        {
            get { return _typeListItemId; }
            set { _typeListItemId = value; }
        }

        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }

        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public int AssetListItemId
        {
            get { return _assetListItemId; }
            set { _assetListItemId = value; }
        }

        public int UnitCreatedId
        {
            get { return _unitCreatedId; }
            set { _unitCreatedId = value; }
        }

        public string Comment
        {
            get { return _comment; }
            set { _comment = value; }
        }

        public int? DefaultSeverityListItemId
        {
            get { return _defaultSeverityListItemId; }
            set { _defaultSeverityListItemId = value; }
        }

        public UnitCreatedDefect()
        {
            _answers = new AnsweredQuestions();
        }
        
        
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;
            DecodeData(data, ref index, versionNumber);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            byte[] data = GetBytes();

            //write version number
            BaseLayer.WriteMoreFlag(stream, _versionNumber);
            //write the length
            BaseLayer.WriteMoreFlag(stream, (int)data.Length);
            //now write the data
            stream.Write(data, 0, data.Length);
        }
        public byte[] GetBytes()
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                int fieldsFlags = (_answers != null ? 1 : 0)
                                    | (_comment != null ? 1 << 1 : 0)
                                    | (_defaultSeverityListItemId != null ? 1 << 2 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);

                if (_answers != null)
                {
                    _answers.Encode(local);
                }
                BaseLayer.Write4ByteInteger(local, _templateListItemId);
                BaseLayer.Write4ByteInteger(local, _typeListItemId);
                BaseLayer.Write4ByteInteger(local, _fleetId);
                BaseLayer.Write4ByteInteger(local, _vehicleId);
                BaseLayer.Write4ByteInteger(local, _assetListItemId);
                BaseLayer.Write4ByteInteger(local, _unitCreatedId);
                if (_comment != null)
                {
                    BaseLayer.WriteString(local, _comment);
                }
                if (_defaultSeverityListItemId != null)
                {
                    BaseLayer.Write4ByteInteger(local, _defaultSeverityListItemId.Value);
                }
                return local.ToArray();
            }
        }
        public void DecodeData(byte[] data, ref int index, int versionNumber)
        {
            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _answers = new AnsweredQuestions();
                _answers.Decode(data, ref index);
            }
            _templateListItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _typeListItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _fleetId = BaseLayer.Read4ByteInteger(data, ref index);
            _vehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            _assetListItemId = BaseLayer.Read4ByteInteger(data, ref index);
            _unitCreatedId = BaseLayer.Read4ByteInteger(data, ref index);
            if (versionNumber >= 2)
            {
                _comment = ((fieldsFlags & (1 << 1)) != 0) ? BaseLayer.ReadString(data, ref index) : null;
            }
            else
            {
                _comment = null;
            }
            if (versionNumber >= 3)
            {
                _defaultSeverityListItemId = ((fieldsFlags & (1 << 2)) != 0) ? (int?)BaseLayer.Read4ByteInteger(data, ref index) : null;
            }
            else
            {
                _defaultSeverityListItemId = null;
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    [Flags]
    public enum AssessmentResult : byte
    {
        None = 0,
        [Description("Repair At Next Service")]
        RepaireAtNextService = 1,
        [Description("Work Order Required")]
        WorkOrderRequired = 2,
        [Obsolete]
        [Description("Decommission Vehicle")]
        DecommissionVehicle = 4 // Obsolete no longer used
    }
}
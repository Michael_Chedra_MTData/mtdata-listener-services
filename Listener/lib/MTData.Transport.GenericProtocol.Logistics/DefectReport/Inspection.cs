using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class Inspection : IPacketEncode
    {
        private const int _versionNumber = 2;

        private int _inspectedAssetUniqueKey;
        private int _inspectedFleetId;
        private int _inspectedVehicleId;
        private int _defectListId;
        private int _defectListVersion;
        private List<InspectionItem> _inspectedItems;
        private AnsweredQuestions _inspectionAnswers;

        public int InspectedAssetUniqueKey
        {
            get { return _inspectedAssetUniqueKey; }
            set { _inspectedAssetUniqueKey = value; }
        }
        public int InspectedFleetId
        {
            get { return _inspectedFleetId; }
            set { _inspectedFleetId = value; }
        }
        public int InspectedVehicleId
        {
            get { return _inspectedVehicleId; }
            set { _inspectedVehicleId = value; }
        }

        public int DefectListId
        {
            get { return _defectListId; }
            set { _defectListId = value; }
        }
        public int DefectListVersion
        {
            get { return _defectListVersion; }
            set { _defectListVersion = value; }
        }
        public List<InspectionItem> InspectedItems
        {
            get { return _inspectedItems; }
            set { _inspectedItems = value; }
        }

        public AnsweredQuestions InspectionAnsweredQuestions
        {
            get { return _inspectionAnswers; }
            set { _inspectionAnswers = value; }
        }

        public Inspection()
        {
            _inspectionAnswers = new AnsweredQuestions();
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _inspectedAssetUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            _inspectedFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            _inspectedVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            _defectListId = BaseLayer.Read4ByteInteger(data, ref index);
            _defectListVersion = BaseLayer.Read4ByteInteger(data, ref index);

            int count = BaseLayer.ReadMoreFlag(data, ref index);
            _inspectedItems = new List<InspectionItem>(count);
            for (int i = 0; i < count; i++)
            {
                InspectionItem item = new InspectionItem();
                item.Decode(data, ref index);
                _inspectedItems.Add(item);
            }

            if(_versionNumber >= 2)
            {
                _inspectionAnswers.Decode(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _inspectedAssetUniqueKey);
                BaseLayer.Write4ByteInteger(local, _inspectedFleetId);
                BaseLayer.Write4ByteInteger(local, _inspectedVehicleId);
                BaseLayer.Write4ByteInteger(local, _defectListId);
                BaseLayer.Write4ByteInteger(local, _defectListVersion);

                BaseLayer.WriteMoreFlag(local, _inspectedItems.Count);
                foreach (InspectionItem item in _inspectedItems)
                {
                    item.Encode(local);
                }

                if(_versionNumber >=2)
                {
                    _inspectionAnswers.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        public bool InspectionComplete()
        {
            foreach (var item in _inspectedItems)
            {
                switch (item.InspectionResults)
                {
                    case InspectionResults.Yes:
                        if (item.ExistingDefectId <= 0 && item.NewDefect == null)
                        {
                            return false;
                        }
                        break;
                    case InspectionResults.None:
                        return false;
                }
            }
            return true;
        }
    }
}
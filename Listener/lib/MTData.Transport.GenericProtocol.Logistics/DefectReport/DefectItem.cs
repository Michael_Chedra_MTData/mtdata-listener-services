using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class DefectItem : IPacketEncode
    {
        private const int _versionNumber = 3;

        private int _defectID;

        private string _defectTypeName;

        private int _terminalCreationID;

        private DateTime? _expireTime;

        private string _asset = string.Empty;

        private string _description = string.Empty;

        private DefectStatus _status;

        private string _severity;

        private List<DefectLog> _logs = new List<DefectLog>();

        private AssessmentResult _assesmentResult;

        private DateTime? _repaireDue;

        private string _externalRef;

        private int _defectTypeUniqueKey;

        private int? _assetUniqueKey;

        private int? _fleetID;

        private int? _vehicleID;



        public int DefectID
        {
            get { return _defectID; }
            set { _defectID = value; }
        }

        public string DefectTypeName
        {
            get { return _defectTypeName; }
            set { _defectTypeName = value; }
        }
        public int TerminalCreationID
        {
            get { return _terminalCreationID; }
            set { _terminalCreationID = value; }
        }

        public DateTime? ExpireTime
        {
            get { return _expireTime; }
            set { _expireTime = value; }
        }

        public string Asset
        {
            get { return _asset; }
            set { _asset = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        public DefectStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public string Severity
        {
            get { return _severity; }
            set { _severity = value; }
        }

        public List<DefectLog> Logs
        {
            get { return _logs; }
            set { _logs = value ?? new List<DefectLog>(); }
        }

        public AssessmentResult AssesmentResult
        {
            get { return _assesmentResult; }
            set { _assesmentResult = value; }
        }

        public DateTime? RepairDue
        {
            get { return _repaireDue; }
            set { _repaireDue = value; }
        }

        public string ExternalRef
        {
            get { return _externalRef; }
            set { _externalRef = value; }
        }

        public int DefectTypeUniqueKey
        {
            get { return _defectTypeUniqueKey; }
            set { _defectTypeUniqueKey = value; }
        }

        public int? AssetUniqueKey
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }

        public int? FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public int? VehicleID
        {
            get { return _vehicleID; }
            set { _vehicleID = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _defectID = BaseLayer.Read4ByteInteger(data, ref index);
            _defectTypeName = BaseLayer.ReadString(data, ref index, 0x12);
            _terminalCreationID = BaseLayer.Read4ByteInteger(data, ref index);
            _asset = BaseLayer.ReadString(data, ref index, 0x12);
            _description = BaseLayer.ReadString(data, ref index, 0x12);
            _status = (DefectStatus)data[index++];
            index++; //Skip Severity byte in the previous version
            _assesmentResult = (AssessmentResult)data[index++];

            int count = BaseLayer.ReadMoreFlag(data, ref index);
            List<DefectLog> logs = new List<DefectLog>(count);
            for (int i = 0; i < count; i++)
            {
                DefectLog l = new DefectLog();
                l.Decode(data, ref index);
                logs.Add(l);
            }
            _logs = logs;

            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);

            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _expireTime = BaseLayer.ReadDateTime(data, ref index);
            }
            else
            {
                _expireTime = null;
            }

            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _repaireDue = BaseLayer.ReadDateTime(data, ref index);
            }
            else
            {
                _repaireDue = null;
            }
            _externalRef = BaseLayer.ReadString(data, ref index);

            if (versionNumber >= 2)
            {
                _severity = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _severity = string.Empty;
            }

            if (versionNumber >= 3)
            {
                _defectTypeUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);

                if ((fieldsFlags & (1 << 2)) != 0)
                {
                    _assetUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
                }
                else
                {
                    _assetUniqueKey = null;
                }

                if ((fieldsFlags & (1 << 3)) != 0)
                {
                    _fleetID = BaseLayer.Read4ByteInteger(data, ref index);
                }
                else
                {
                    _fleetID = null;
                }

                if ((fieldsFlags & (1 << 4)) != 0)
                {
                    _vehicleID = BaseLayer.Read4ByteInteger(data, ref index);
                }
                else
                {
                    _vehicleID = null;
                }
            }
            else
            {
                _defectTypeUniqueKey = 0;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                int fieldsFlags = (_expireTime != null ? 1 << 0 : 0)
                    | (_repaireDue != null ? 1 << 1 : 0)
                    | (_assetUniqueKey.HasValue ? 1 << 2 : 0)
                    | (_fleetID.HasValue ? 1 << 3 : 0)
                    | (_vehicleID.HasValue ? 1 << 4 : 0);

                BaseLayer.Write4ByteInteger(local, _defectID);
                BaseLayer.WriteString(local, _defectTypeName, 0x12);
                BaseLayer.Write4ByteInteger(local, _terminalCreationID);
                BaseLayer.WriteString(local, _asset, 0x12);
                BaseLayer.WriteString(local, _description, 0x12);
                local.WriteByte((byte)_status);
                local.WriteByte(0);
                local.WriteByte((byte)_assesmentResult);

                BaseLayer.WriteMoreFlag(local, _logs.Count);
                foreach (DefectLog log in _logs)
                {
                    log.Encode(local);
                }

                BaseLayer.WriteMoreFlag(local, fieldsFlags);

                if (_expireTime.HasValue)
                {
                    BaseLayer.WriteDateTime(local, _expireTime.Value);
                }

                if (_repaireDue.HasValue)
                {
                    BaseLayer.WriteDateTime(local, _repaireDue.Value);

                }
                BaseLayer.WriteString(local, _externalRef);

                BaseLayer.WriteString(local, _severity);

                //Version 3
                BaseLayer.Write4ByteInteger(local, _defectTypeUniqueKey);
                if (_assetUniqueKey.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _assetUniqueKey.Value);
                }
                if (_fleetID.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _fleetID.Value);
                }
                if (_vehicleID.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _vehicleID.Value);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}
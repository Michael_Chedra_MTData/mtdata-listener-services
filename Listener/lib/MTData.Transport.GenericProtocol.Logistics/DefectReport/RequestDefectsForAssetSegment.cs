using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class RequestDefectsForAssetSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.RequestDefectsForAsset;

        private int _assetUniqueKey;
        private int _slaveFleetId;
        private int _slaveVehicleId;

        public int AssetUniqueKey
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }
        public int SlaveFleetId
        {
            get { return _slaveFleetId; }
            set { _slaveFleetId = value; }
        }
        public int SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }

        public RequestDefectsForAssetSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
            _assetUniqueKey = -1;
            _slaveFleetId = -1;
            _slaveVehicleId = -1;
        }

        public RequestDefectsForAssetSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }


        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _assetUniqueKey);
                Write4ByteInteger(stream, _slaveFleetId);
                Write4ByteInteger(stream, _slaveVehicleId);
                Data = stream.ToArray();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        private void DecodeData()
        {
            int index = 0;
            _assetUniqueKey = Read4ByteInteger(Data, ref index);
            _slaveFleetId = Read4ByteInteger(Data, ref index);
            _slaveVehicleId = Read4ByteInteger(Data, ref index);
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.DefectReport
{
    public class DefectLog : IPacketEncode
    {
        private const int _versionNumber = 1;

        private EventType _eventType;

        private DateTime _timeStamp;

        private string _user;

        private string _description;

        public EventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public DateTime TimeStamp
        {
            get { return _timeStamp; }
            set { _timeStamp = value; }
        }
        public string User
        {
            get { return _user; }
            set { _user = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }



        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _eventType = (EventType)BaseLayer.Read2ByteInteger(data, ref index);
            _timeStamp = BaseLayer.ReadDateTime(data, ref index);
            _user = BaseLayer.ReadString(data, ref index, 0x12);
            _description = BaseLayer.ReadString(data, ref index, 0x12);


            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.Write2ByteInteger(local, (short)_eventType);
                BaseLayer.WriteDateTime(local, _timeStamp);
                BaseLayer.WriteString(local, _user, 0x12);
                BaseLayer.WriteString(local, _description, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}
using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class SendLoginResponseSegment : SegmentLayer
    {
        public enum ELoginResult
        {
            Success = 0,
            InvalidDriverId,
            InvalidPin,
            PasswordExpired
        }

        #region private fields
        private ELoginResult _loginResult;
        private bool _allowFreeDial;
        private VersionInfo _firmwareVersion;
        private bool _firmwareForceUpdate;
        private VersionInfo _necVersion;
        private bool _necForceUpdate;
        private bool _newPreTripQuestions;
        private VersionInfo _firmware4000Version;
        private bool _firmware4000ForceUpdate;
        private int _timeOffset;
        private int _driverId;
        private string _driverName;
        private string _vehicleDisplayName;
        private string _vehicleType;
        private string _webServiceUrl;
        private string _loadType;
        private VersionInfo _shellConfigVersion;
        private bool _shellConfigForceUpdate;
        private float _fuelBurnRate;
        private int _defectTemplateUniqueKey;
        private int _fuelTypeListId;
        private int _fuelTypeListUniqueKey;
        private List<Talon3rdPartyAppVersionInfo> _talon3rdPartyApps;
        private ThirdPartyLoginInfo _thirdPartyLoginInfo;
        private int _readAckQuestionID;
        private int _fatigueAlertListID;
        private string _vehicleRef;
        private string _driverRef;
        #endregion

        #region properties
        public ELoginResult LoginResult
        {
            get { return _loginResult; }
            set { _loginResult = value; }
        }
        public bool AllowFreeDial
        {
            get { return _allowFreeDial; }
            set { _allowFreeDial = value; }
        }
        public VersionInfo FirmwareVersion
        {
            get { return _firmwareVersion; }
            set { _firmwareVersion = value; }
        }
        public bool FirmwareForceUpdate
        {
            get { return _firmwareForceUpdate; }
            set { _firmwareForceUpdate = value; }
        }
        public VersionInfo NecVersion
        {
            get { return _necVersion; }
            set { _necVersion = value; }
        }
        public bool NecForceUpdate
        {
            get { return _necForceUpdate; }
            set { _necForceUpdate = value; }
        }
        public VersionInfo Firmware4000Version
        {
            get { return _firmware4000Version; }
            set { _firmware4000Version = value; }
        }
        public bool Firmware4000ForceUpdate
        {
            get { return _firmware4000ForceUpdate; }
            set { _firmware4000ForceUpdate = value; }
        }
        public bool NewPreTripQuestions
        {
            get { return _newPreTripQuestions; }
            set { _newPreTripQuestions = value; }
        }
        public int TimeOffset
        {
            get { return _timeOffset; }
            set { _timeOffset = value; }
        }
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public string DriverName
        {
            get { return _driverName; }
            set { _driverName = value; }
        }
        public string VehicleDisplayName
        {
            get { return _vehicleDisplayName; }
            set { _vehicleDisplayName = value; }
        }
        public string VehicleType
        {
            get { return _vehicleType; }
            set { _vehicleType = value; }
        }
        public string WebServiceUrl
        {
            get { return _webServiceUrl; }
            set { _webServiceUrl = value; }
        }
        public string LoadType
        {
            get { return _loadType; }
            set { _loadType = value; }
        }
        public VersionInfo ShellConfigVersion
        {
            get { return _shellConfigVersion; }
            set { _shellConfigVersion = value; }
        }
        public bool ShellConfigForceUpdate
        {
            get { return _shellConfigForceUpdate; }
            set { _shellConfigForceUpdate = value; }
        }
        public float FuelBurnRate
        {
            get { return _fuelBurnRate; }
            set { _fuelBurnRate = value; }
        }

        /// <remarks>
        /// Values less less than or equal to 0 are treated as null
        /// </remarks>
        public int DefectTemplateUniqueKey
        {
            get { return _defectTemplateUniqueKey; }
            set { _defectTemplateUniqueKey = value; }
        }

        /// <remarks>
        /// Values less less than or equal to 0 are treated as null
        /// </remarks>
        public int FuelTypeListId
        {
            get { return _fuelTypeListId; }
            set { _fuelTypeListId = value; }
        }

        /// <remarks>
        /// Values less less than or equal to 0 are treated as null
        /// </remarks>
        public int FuelTypeListUniqueKey
        {
            get { return _fuelTypeListUniqueKey; }
            set { _fuelTypeListUniqueKey = value; }
        }

        public List<Talon3rdPartyAppVersionInfo> Talon3rdPartyApps
        {
            get { return _talon3rdPartyApps; }
            set { _talon3rdPartyApps = value; }
        }

        public ThirdPartyLoginInfo ThirdPartyLoginInfo
        {
            get { return _thirdPartyLoginInfo; }
            set { _thirdPartyLoginInfo = value; }
        }

        public int ReadAckQuestionID
        {
            get { return _readAckQuestionID; }
            set { _readAckQuestionID = value; }
        }

        public int FatigueAlertListID
        {
            get { return _fatigueAlertListID; }
            set { _fatigueAlertListID = value; }
        }

        public string VehicleRef
        {
            get { return _vehicleRef; }
            set { _vehicleRef = value; }
        }
        public string DriverRef
        {
            get { return _driverRef; }
            set { _driverRef = value; }
        }
        #endregion

        #region constructor
        public SendLoginResponseSegment()
        {
            Version = 13;
            Type = (int)LogisticsSegmentTypes.SendLoginResponse;
            _firmwareVersion = new VersionInfo();
            _necVersion = new VersionInfo();
            _firmware4000Version = new VersionInfo();
            _shellConfigVersion = new VersionInfo(-1, -1);
            _talon3rdPartyApps = new List<Talon3rdPartyAppVersionInfo>();

        }
        public SendLoginResponseSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendLoginResponse)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendLoginResponse, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendLoginResponse;
            _firmwareVersion = new VersionInfo();
            _necVersion = new VersionInfo();
            _firmware4000Version = new VersionInfo();
            _shellConfigVersion = new VersionInfo(-1, -1);
            _talon3rdPartyApps = new List<Talon3rdPartyAppVersionInfo>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_loginResult);
                if (_loginResult == ELoginResult.Success)
                {
                    WriteBool(stream, _allowFreeDial);
                    _firmwareVersion.Encode(stream);
                    WriteBool(stream, _firmwareForceUpdate);
                    _necVersion.Encode(stream);
                    WriteBool(stream, _necForceUpdate);
                    WriteBool(stream, _newPreTripQuestions);
                    Write2ByteInteger(stream, _timeOffset);
                    Write4ByteInteger(stream, _driverId);
                    WriteString(stream, _driverName, 0x12);
                    WriteString(stream, _vehicleType, 0x12);
                    WriteString(stream, _webServiceUrl, 0x12);
                    WriteString(stream, _loadType, 0x12);
                    _firmware4000Version.Encode(stream);
                    WriteBool(stream, _firmware4000ForceUpdate);
                    if (Version >= 3)
                    {
                        _shellConfigVersion.Encode(stream); //Write 2 bytes version number
                        WriteBool(stream, _shellConfigForceUpdate);
                    }
                    if (Version >= 4)
                    {
                        Write4ByteInteger(stream, _shellConfigVersion.Major); //Write 4 bytes version number
                        Write4ByteInteger(stream, _shellConfigVersion.Minor);
                        WriteBool(stream, _shellConfigForceUpdate);
                    }
                    if (Version >= 5)
                    {
                        WriteString(stream, _vehicleDisplayName, 0x12);
                    }
                    if (Version >= 6)
                    {
                        WriteFloat(stream, _fuelBurnRate);
                    }
                    // Added version 7
                    Write4ByteInteger(stream, _defectTemplateUniqueKey);
                    //add version 8
                    Write2ByteInteger(stream, _talon3rdPartyApps.Count);
                    foreach (Talon3rdPartyAppVersionInfo t in _talon3rdPartyApps)
                    {
                        t.Encode(stream);
                    }

                    // Verion 9
                    int fieldsFlags = (_thirdPartyLoginInfo != null ? 1 << 0 : 0);

                    WriteMoreFlag(stream, fieldsFlags);
                    if (_thirdPartyLoginInfo != null)
                    {
                        _thirdPartyLoginInfo.Encode(stream);
                    }

                    if (Version >= 10)
                    {
                        Write4ByteInteger(stream, _fuelTypeListId); //Write 4 bytes Fuel Type List ID
                        Write4ByteInteger(stream, _fuelTypeListUniqueKey); //Write 4 bytes Fuel Type List Unique Key
                    }

                    if (Version >= 11)
                    {
                        Write4ByteInteger(stream, _readAckQuestionID);
                    }

                    if (Version >= 12)
                    {
                        Write4ByteInteger(stream, FatigueAlertListID);
                    }
                    //version 13
                    WriteString(stream, _vehicleRef, 0x12);
                    WriteString(stream, _driverRef, 0x12);

                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            _allowFreeDial = false;
            _necForceUpdate = false;
            _firmwareForceUpdate = false;
            _newPreTripQuestions = false;
            _firmware4000ForceUpdate = false;
            _firmwareVersion = new VersionInfo();
            _firmware4000Version = new VersionInfo();
            _necVersion = new VersionInfo();
            _shellConfigVersion = new VersionInfo(-1, -1);
            _driverName = null;
            _vehicleType = null;
            _webServiceUrl = null;
            _loadType = null;
            _driverId = 0;
            _timeOffset = 0;
            _fuelBurnRate = 0;
            _fuelTypeListId = 0;
            _fuelTypeListUniqueKey = 0;

            int index = 0;
            _loginResult = (ELoginResult)Data[index++];
            if (_loginResult == ELoginResult.Success)
            {
                _allowFreeDial = ReadBool(Data, ref index);
                _firmwareVersion.Decode(Data, ref index);
                _firmwareForceUpdate = ReadBool(Data, ref index);
                _necVersion.Decode(Data, ref index);
                _necForceUpdate = ReadBool(Data, ref index);
                _newPreTripQuestions = ReadBool(Data, ref index);
                _timeOffset = Read2ByteInteger(Data, ref index);
                _driverId = Read4ByteInteger(Data, ref index);
                _driverName = ReadString(Data, ref index, 0x12);
                _vehicleType = ReadString(Data, ref index, 0x12);
                _webServiceUrl = ReadString(Data, ref index, 0x12);
                _loadType = ReadString(Data, ref index, 0x12);
                if (Version >= 2)
                {
                    _firmware4000Version.Decode(Data, ref index);
                    _firmware4000ForceUpdate = ReadBool(Data, ref index);
                }
                if (Version >= 3)
                {
                    _shellConfigVersion.Decode(Data, ref index); //Read 2 bytes version
                    _shellConfigForceUpdate = ReadBool(Data, ref index);
                }
                if (Version >= 4)
                {
                    if (_shellConfigVersion == null)
                    {
                        _shellConfigVersion = new VersionInfo();
                    }
                    _shellConfigVersion.Major = Read4ByteInteger(Data, ref index); //Read 4 bytes version. 
                    _shellConfigVersion.Minor = Read4ByteInteger(Data, ref index); //Read 4 bytes version. 

                    _shellConfigForceUpdate = ReadBool(Data, ref index);
                }
                _vehicleDisplayName = "";
                if (Version >= 5)
                {
                    _vehicleDisplayName = ReadString(Data, ref index, 0x12);
                }
                if (Version >= 6)
                {
                    _fuelBurnRate = ReadFloat(Data, ref index);
                }
                if (Version >= 7)
                {
                    _defectTemplateUniqueKey = Read4ByteInteger(Data, ref index);
                }
                _talon3rdPartyApps = new List<Talon3rdPartyAppVersionInfo>();
                if (Version >= 8)
                {
                    int count = Read2ByteInteger(Data, ref index);
                    for (int i = 0; i < count; i++)
                    {
                        Talon3rdPartyAppVersionInfo t = new Talon3rdPartyAppVersionInfo();
                        t.Decode(Data, ref index);
                        _talon3rdPartyApps.Add(t);
                    }
                }

                int fieldsFlags = 0;

                if (Version >= 9)
                {
                    fieldsFlags = ReadMoreFlag(Data, ref index);

                    if ((fieldsFlags & (1 << 0)) != 0)
                    {
                        _thirdPartyLoginInfo = new ThirdPartyLoginInfo();
                        _thirdPartyLoginInfo.Decode(Data, ref index);
                    }
                    else
                    {
                        _thirdPartyLoginInfo = null;
                    }
                }

                if (Version >= 10)
                {
                    _fuelTypeListId = Read4ByteInteger(Data, ref index); //Read 4 bytes Fuel Type List ID
                    _fuelTypeListUniqueKey = Read4ByteInteger(Data, ref index); //Read 4 bytes Fuel Type List Unique Key
                }

                if (Version >= 11)
                {
                    _readAckQuestionID = Read4ByteInteger(Data, ref index);
                }

                if (Version >= 12)
                {
                    FatigueAlertListID = Read4ByteInteger(Data, ref index);
                }

                _driverRef = string.Empty;
                _vehicleRef = string.Empty;
                if (Version >= 13)
                {
                    _vehicleRef = ReadString(Data, ref index, 0x12);
                    _driverRef = ReadString(Data, ref index, 0x12);
                }
            }
        }
        #endregion
    }
}

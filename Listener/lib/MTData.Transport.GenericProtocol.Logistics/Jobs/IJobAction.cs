using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// job action types
    /// </summary>
    public enum JobActionTypes
    {
        Reject = 0,
        Accept = 1,
        RequestCancel = 2,
        AdditionalInfo = 3,
        ActivateRoute = 4,
        OnHold = 5,
        DispatchedQuestion = 6,
        EditDocketNumber = 7,
        Delay = 8,
        Alert = 9,
        Start = 10,
        Finish = 11
    }

    /// <summary>
    /// Action states
    /// </summary>
    public enum JobActionStatus
    {
        NotStarted = 0,
        InProgress = 1,
        Complete = 2
    }

    /// <summary>
    /// delegate for job action events
    /// </summary>
    /// <param name="leg"></param>
    public delegate void JobActionEvent(IJobAction jobAction);

    /// <summary>
    /// The Interface for a Job Action
    /// </summary>
    public interface IJobAction : IPacketEncode
    {
        /// <summary>
        /// the job action type
        /// </summary>
        JobActionTypes JobActionType { get; }

        /// <summary>
        /// The id of the job the job action belongs to
        /// </summary>
        int DispatchId { get; }

        /// <summary>
        /// The Action id
        /// </summary>
        int JobActionId { get; }

        /// <summary>
        /// the job action number (used for sequencing)
        /// </summary>
        int JobActionNumber { get; }

        /// <summary>
        /// the action title
        /// </summary>
        string Title { get; }
        /// <summary>
        /// the notes assoicated with the action, can be null
        /// </summary>
        string Notes { get; }

        /// <summary>
        /// the job action status
        /// </summary>
        JobActionStatus Status { get; set;}

        /// <summary>
        /// get an external reference for the action
        /// </summary>
        string ExternalReference { get; }

        /// <summary>
        /// event for when status of the leg action has changed
        /// </summary>
        event JobActionEvent OnJobActionStatusChanged;
    }
}

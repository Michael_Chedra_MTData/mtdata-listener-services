using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// packet sent by base when adding a new action to an existing job
    /// </summary>
    public class AddActionSegment : SegmentLayer
    {
        #region private fields
        private int _allocationId;
        private int _jobDispatchId;
        private ILegAction _action;
        /// <summary>
        /// dictionary containing the order of the actions for this leg
        /// key = action id, value = the leg action number (used for sequencing)
        /// </summary>
        private Dictionary<int, int> _actionOrderNumbers;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        public ILegAction Action
        {
            get { return _action; }
            set { _action = value; }
        }
        /// <summary>
        /// dictionary containing the order of the actions for this leg
        /// key = action id, value = the leg action number (used for sequencing)
        /// </summary>
        public Dictionary<int, int> ActionOrderNumbers
        {
            get { return _actionOrderNumbers; }
            set { _actionOrderNumbers = value; }
        }
        #endregion

        #region constructor
        public AddActionSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AddAction;
            _actionOrderNumbers = new Dictionary<int, int>();
        }
        public AddActionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AddAction)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AddAction, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AddAction;
            _actionOrderNumbers = new Dictionary<int, int>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                Write4ByteInteger(stream, _jobDispatchId);
                stream.WriteByte((byte)_action.LegActionType);
                _action.Encode(stream);
                Write2ByteInteger(stream, _actionOrderNumbers.Count);
                foreach (int orderId in _actionOrderNumbers.Keys)
                {
                    Write4ByteInteger(stream, orderId);
                    Write4ByteInteger(stream, _actionOrderNumbers[orderId]);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            LegActionTypes t = (LegActionTypes)Data[index++];
            switch (t)
            {
                case LegActionTypes.PalletPickup:
                    _action = new Pallets.PalletsPickupAction();
                    break;
                case LegActionTypes.PalletDelivery:
                    _action = new Pallets.PalletsDeliveryAction();
                    break;
                case LegActionTypes.PalletQuestions:
                    _action = new Pallets.PalletsQuestionAction();
                    break;
                case LegActionTypes.Questions:
                case LegActionTypes.OutOfSequence:
                    _action = new QuestionAction(t);
                    break;
                case LegActionTypes.Coupling:
                case LegActionTypes.Decoupling:
                case LegActionTypes.BinPickupDrop:
                case LegActionTypes.BinTransferStation:
                case LegActionTypes.TagLocation:
                case LegActionTypes.Mass:
                case LegActionTypes.Photo:
                case LegActionTypes.Ticket:
                case LegActionTypes.PalletTransfer:
                    _action = new LegAction();
                    ((LegAction)_action).LegActionType = t;
                    break;
                case LegActionTypes.BinPickup:
                    _action = new Bin.BinPickupAction();
                    break;
                case LegActionTypes.Recovery:
                    _action = new Recovery.RecoveryLegAction();
                    break;
                case LegActionTypes.PassengerStop:
                    _action = new Passenger.PassengerStopLegAction();
                    break;
                case LegActionTypes.Pickup:
                case LegActionTypes.Deliver:
                    _action = new Delivery.ItemsLegAction();
                    ((LegAction)_action).LegActionType = t;
                    break;
                case LegActionTypes.AddTrailer:
                case LegActionTypes.DropTrailer:
                    _action = new Trailer.TrailerLegAction();
                    ((LegAction)_action).LegActionType = t;
                    break;
                case LegActionTypes.Service:
                    _action = new Service.ServiceLegAction();
                    break;
                case LegActionTypes.Alert:
                    _action = new Container.AlertLegAction();
                    break;
                case LegActionTypes.Container:
                    _action = new Container.ContainerLegAction();
                    break;
                case LegActionTypes.ConcreteLoad:
                    _action = new Common.ProductLegAction();
                    break;
                case LegActionTypes.ConcretePour:
                    _action = new Common.ProductLegAction();
                    break;
                case LegActionTypes.AddAsset:
                case LegActionTypes.DropAsset:
                    _action = new Delivery.TrackableAssetLegAction();
                    ((LegAction)_action).LegActionType = t;
                    break;
            }
            if (_action == null)
            {
                throw new Exception(string.Format("Unknown action type {0}", t));
            }
            _action.Decode(Data, ref index);

            int count = Read2ByteInteger(Data, ref index);
            _actionOrderNumbers.Clear();
            for (int i = 0; i < count; i++)
            {
                int id = Read4ByteInteger(Data, ref index);
                int orderNumber = Read4ByteInteger(Data, ref index);
                _actionOrderNumbers.Add(id, orderNumber);
            }
        }
        #endregion
    }
}

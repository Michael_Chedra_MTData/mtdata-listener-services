using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// a questions job action
    /// </summary>
    public class AdditionalInfoJobAction : JobAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private List<QuestionItem> _questions;
        #endregion

        #region public properties
        public List<QuestionItem> Questions
        {
            get { return _questions; }
            set { _questions = value; }
        }
        
        #endregion

        #region constructor
        public AdditionalInfoJobAction()
        {
            _questions = new List<QuestionItem>();
            JobActionType = JobActionTypes.AdditionalInfo;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _questions.Clear();
            for (int i = 0; i < count; i++)
            {
                QuestionItem item = new QuestionItem();
                item.Decode(data, ref index);
                _questions.Add(item);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write2ByteInteger(local, _questions.Count);
                foreach (QuestionItem item in _questions)
                {
                    item.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

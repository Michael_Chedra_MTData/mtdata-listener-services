using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by base in response of a UnitCreateLegAction packet, returns the new leg actions id
    /// </summary>
    public class SendCreatedLegActionIdSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private int _originalLegActionId;
        private int _legActionId;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public int OriginalLegActionId
        {
            get { return _originalLegActionId; }
            set { _originalLegActionId = value; }
        }
        public int LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }
        #endregion

        #region constructor
        public SendCreatedLegActionIdSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendCreatedLegActionId;
        }
        public SendCreatedLegActionIdSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendCreatedLegActionId)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendCreatedLegActionId, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendCreatedLegActionId;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                Write4ByteInteger(stream, _originalLegActionId);
                Write4ByteInteger(stream, _legActionId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _originalLegActionId = Read4ByteInteger(Data, ref index);
            _legActionId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

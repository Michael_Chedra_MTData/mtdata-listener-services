using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class JobActionStatusChangedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _actionId;
        private JobActionTypes _actionType;
        private JobActionStatus _actionStatus;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int ActionId
        {
            get { return _actionId; }
            set { _actionId = value; }
        }
        public JobActionTypes ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }
        public JobActionStatus ActionStatus
        {
            get { return _actionStatus; }
            set { _actionStatus = value; }
        }
        #endregion

        #region constructor
        public JobActionStatusChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobActionStatusChanged;
        }
        public JobActionStatusChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobActionStatusChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobActionStatusChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobActionStatusChanged;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _actionId);
                stream.WriteByte((byte)_actionType);
                stream.WriteByte((byte)_actionStatus);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _actionId = Read4ByteInteger(Data, ref index);
            _actionType = (JobActionTypes)Data[index++];
            _actionStatus = (JobActionStatus)Data[index++];
        }
        #endregion
    }
}

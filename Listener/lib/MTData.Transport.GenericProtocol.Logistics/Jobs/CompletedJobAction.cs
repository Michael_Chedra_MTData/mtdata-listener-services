using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class CompletedJobAction : ICompletedJobAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private JobActionTypes _jobActionType;
        private int _jobActionId;
        #endregion

        #region Constructor
        public CompletedJobAction() { }
        public CompletedJobAction(JobActionTypes actionType, int actionId)
        {
            _jobActionId = actionId;
            _jobActionType = actionType;
        }
        #endregion

        #region ICompetedAction Members

        public JobActionTypes JobActionType
        {
            get { return _jobActionType; }
            set { _jobActionType = value; }
        }

        public int JobActionId
        {
            get { return _jobActionId; }
            set { _jobActionId = value; }
        }

        #endregion

        #region IPacketEncode Members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _jobActionType = (JobActionTypes)data[index++];
            _jobActionId = BaseLayer.Read4ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_jobActionType);
                BaseLayer.Write4ByteInteger(local, _jobActionId);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

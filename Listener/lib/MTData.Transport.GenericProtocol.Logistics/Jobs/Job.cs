using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Common;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class Job : IJob
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;
        private JobTypes _jobType;
        private int _dispatchId;
        private string _jobReference;
        private int _jobReferenceInt;
        private string _title;
        private List<ILeg> _legs;
        private JobStatus _status;
        private List<IJobAction> _jobActions;
        private List<ICompletedJobAction> _completedJobActions;
        private JobActionIterator _jobActionsIterator;
        /// <summary>
        /// complete the job once all legs are complete
        /// </summary>
        private bool _completeJobWhenAllLegsAreComplete;
        /// <summary>
        /// was the last state change due to a delayed job action being completed
        /// </summary>
        private bool _delayedLastStateChange;
        private string _docketNumber;
        private bool _canDriverEditDocketNumber;
        private string _notes;
        #endregion

        #region properties
        /// <summary>
        /// complete the job once all legs are complete
        /// </summary>
        protected bool CompleteJobWhenAllLegsAreComplete
        {
            get { return _completeJobWhenAllLegsAreComplete; }
            set { _completeJobWhenAllLegsAreComplete = value; }
        }
        #endregion
      
        #region constructor
        public Job()
        {
            _status = JobStatus.NotStarted;
            _legs = new List<ILeg>();
            _jobReference = string.Empty;
            _title = string.Empty;
            _jobActions = new List<IJobAction>();
            _completedJobActions = new List<ICompletedJobAction>();
            _jobActionsIterator = new JobActionIterator(this);
            _completeJobWhenAllLegsAreComplete = true;
            _delayedLastStateChange = false;
            _docketNumber = string.Empty;
            _canDriverEditDocketNumber = false;
            _notes = string.Empty;
        }
        #endregion

        #region IJob Members

        public JobTypes JobType
        {
            get { return _jobType; }
            set { _jobType = value; }
        }

        public int DispatchId
        {
            get { return _dispatchId; }
            set 
            {
                _dispatchId = value;
                foreach (Leg l in _legs)
                {
                    l.DispatchId = _dispatchId;
                }
                foreach (JobAction action in _jobActions)
                {
                    action.DispatchId = _dispatchId;
                }
            }
        }

        public string JobReference
        {
            get { return _jobReference; }
            set { _jobReference = value; }
        }

        public int JobReferenceInt
        {
            get { return _jobReferenceInt; }
            set { _jobReferenceInt = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string DocketNumber 
        {
            get { return _docketNumber; }
            set { _docketNumber = value; }
        }
        public bool CanDriverEditDocketNumber
        {
            get { return _canDriverEditDocketNumber; }
            set { _canDriverEditDocketNumber = value; }
        }
        public string JobNotes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public JobStatus Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    if (OnJobStatusChanged != null)
                    {
                        OnJobStatusChanged(this);
                    }
                }
            }
        }

        public int LegsCount
        {
            get { return _legs.Count; }
        }

        public ILeg this[int index]
        {
            get
            {
                if (index >= 0 && index < _legs.Count)
                {
                    return _legs[index];
                }
                throw new Exception(string.Format("Unknown leg with index {0}, legs list has only {1} items.", index, _legs.Count));
            }
        }

        public event JobEvent OnJobStatusChanged;

        public void AddLeg(ILeg leg)
        {
            if (_dispatchId == leg.DispatchId)
            {
                leg.OnLegStatusChanged += new LegEvent(leg_OnLegStatusChanged);
                _legs.Add(leg);
                CheckJobStatus();
            }
            else
            {
                throw new Exception("Can't add leg to job, they have different dispatch ids");
            }
        }

        public void DeleteLeg(int legId)
        {
            ILeg leg = GetLegWithId(legId);
            if (leg != null)
            {
                leg.OnLegStatusChanged -= new LegEvent(leg_OnLegStatusChanged);
                _legs.Remove(leg);
            }
            CheckJobStatus();
        }

        public ILeg GetLegWithId(int legId)
        {
            foreach (ILeg leg in _legs)
            {
                if (leg.LegId == legId)
                {
                    return leg;
                }
            }
            return null;
        }

        public void SortLegsOnLegNumber()
        {
            _legs.Sort();
        }

        public int JobActionCount
        {
            get { return _jobActions.Count; }
        }

        public void AddJobAction(IJobAction jobAction)
        {
            if (_dispatchId == jobAction.DispatchId)
            {
                jobAction.OnJobActionStatusChanged += new JobActionEvent(jobAction_OnJobActionStatusChanged);
                _jobActions.Add(jobAction);
                CheckJobStatus();
            }
            else
            {
                throw new Exception("Can't add job action to job, they have different dispatch ids");
            }
        }

        public void DeleteJobAction(int jobActionId)
        {
            IJobAction action = GetJobActionWithId(jobActionId);
            if (action != null)
            {
                action.OnJobActionStatusChanged -= new JobActionEvent(jobAction_OnJobActionStatusChanged);
                _jobActions.Remove(action);
            }
            CheckJobStatus();
        }

        public IJobAction GetJobActionWithIndex(int index)
        {
            return _jobActions[index];
        }

        public IJobAction GetJobActionWithId(int jobActionId)
        {
            foreach (IJobAction action in _jobActions)
            {
                if (action.JobActionId == jobActionId)
                {
                    return action;
                }
            }
            return null;
        }

        public IJobActions JobActions
        {
            get { return _jobActionsIterator; }
        }

        public List<ICompletedJobAction> CompletedJobActions
        {
            get { return _completedJobActions; }
        }

        public ICompletedJobAction GetCompletedJobActionWithId(int jobActionId)
        {
            foreach (ICompletedJobAction action in _completedJobActions)
            {
                if (action.JobActionId == jobActionId)
                {
                    return action;
                }
            }
            return null;
        }
        #endregion

        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return _legs.GetEnumerator();
        }

        #endregion

        #region IPacketEncode Members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _jobType = (JobTypes)data[index++];
            _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
            _jobReference = BaseLayer.ReadString(data, ref index, 0x12);
            _jobReferenceInt = BaseLayer.Read4ByteInteger(data, ref index);
            _title = BaseLayer.ReadString(data, ref index, 0x12);
            _status = (JobStatus)data[index++];
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _legs.Clear();
            for (int i = 0; i < count; i++)
            {
                LegTypes t = (LegTypes)data[index++];
                ILeg leg = null;
                switch (t)
                {
                    case LegTypes.Simple:
                    case LegTypes.ContainerPickup:
                    case LegTypes.ContainerWait:
                    case LegTypes.ContainerDelivery:
                        leg = new Leg();
                        ((Leg)leg).LegType = t;
                        break;
                    case LegTypes.PalletPickup:
                        leg = new Pallets.PalletsPickupLeg();
                        break;
                    case LegTypes.PalletDelivery:
                        leg = new Pallets.PalletsDeliveryLeg();
                        break;
                    case LegTypes.PassengerStop:
                        leg = new Passenger.PassengerStopLeg();
                        break;
                    case LegTypes.Stop:
                        leg = new Bus.StopLeg();
                        break;
                }
                if (leg == null)
                {
                    throw new Exception(string.Format("Unknown leg type {0}", t));
                }
                leg.Decode(data, ref index);
                AddLeg(leg);
            }

            if (versionNumber >= 2)
            {
                count = BaseLayer.Read2ByteInteger(data, ref index);
                _jobActions.Clear();
                for (int i = 0; i < count; i++)
                {
                    JobActionTypes t = (JobActionTypes)data[index++];
                    IJobAction action = null;
                    switch (t)
                    {
                        case JobActionTypes.Reject:
                        case JobActionTypes.Accept:
                        case JobActionTypes.DispatchedQuestion:
                        case JobActionTypes.Delay:
                        case JobActionTypes.Start:
                        case JobActionTypes.Finish:
                            action = new QuestionsJobAction();
                            break;
                        case JobActionTypes.RequestCancel:
                        case JobActionTypes.EditDocketNumber:
                            action = new JobAction();
                            break;
                        case JobActionTypes.AdditionalInfo:
                            action = new AdditionalInfoJobAction();
                            break;
                        case JobActionTypes.Alert:
                            action = new AlertJobAction();
                            break;
                    }
                    if (action == null)
                    {
                        throw new Exception(string.Format("Unknown job action type {0}", t));
                    }
                    action.Decode(data, ref index);
                    AddJobAction(action);
                }
                count = BaseLayer.Read2ByteInteger(data, ref index);
                _completedJobActions.Clear();
                for (int i = 0; i < count; i++)
                {
                    JobActionTypes t = (JobActionTypes)data[index++];
                    ICompletedJobAction action = null;
                    switch (t)
                    {
                        case JobActionTypes.Reject:
                        case JobActionTypes.Accept:
                        case JobActionTypes.DispatchedQuestion:
                        case JobActionTypes.Delay:
                        case JobActionTypes.Start:
                        case JobActionTypes.Finish:
                            action = new QuestionsCompletedJobAction();
                            break;
                        case JobActionTypes.RequestCancel:
                        case JobActionTypes.Alert:
                            action = new CompletedJobAction();
                            break;
                        case JobActionTypes.AdditionalInfo:
                            action = new AdditionalInfoJobCompletedAction();
                            break;
                        case JobActionTypes.EditDocketNumber:
                            action = new EditDocketNumberJobCompletedAction();
                            break;
                    }
                    if (action == null)
                    {
                        throw new Exception(string.Format("Unknown completed job action type {0}", t));
                    }
                    action.Decode(data, ref index);
                    _completedJobActions.Add(action);
                }

                if (versionNumber >= 3)
                {
                    _docketNumber = BaseLayer.ReadString(data, ref index, 0x12);
                    _canDriverEditDocketNumber = BaseLayer.ReadBool(data, ref index);
                    _notes = BaseLayer.ReadString(data, ref index, 0x12);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

            //check job status is correct
            if (this.GetType() == typeof(Job))
            {
                CheckJobStatus();
            }
        }

        public virtual void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_jobType);
                BaseLayer.Write4ByteInteger(local, _dispatchId);
                BaseLayer.WriteString(local, _jobReference, 0x12);
                BaseLayer.Write4ByteInteger(local, _jobReferenceInt);
                BaseLayer.WriteString(local, _title, 0x12);
                local.WriteByte((byte)_status);
                BaseLayer.Write2ByteInteger(local, _legs.Count);
                foreach (ILeg l in _legs)
                {
                    local.WriteByte((byte)l.LegType);
                    l.Encode(local);
                }

                if (_versionNumber >= 2)
                {
                    BaseLayer.Write2ByteInteger(local, _jobActions.Count);
                    foreach (IJobAction a in _jobActions)
                    {
                        local.WriteByte((byte)a.JobActionType);
                        a.Encode(local);
                    }
                    BaseLayer.Write2ByteInteger(local, _completedJobActions.Count);
                    foreach (ICompletedJobAction ca in _completedJobActions)
                    {
                        local.WriteByte((byte)ca.JobActionType);
                        ca.Encode(local);
                    }

                    if (_versionNumber >= 3)
                    {
                        BaseLayer.WriteString(local, _docketNumber, 0x12);
                        BaseLayer.WriteBool(local, _canDriverEditDocketNumber);
                        BaseLayer.WriteString(local, _notes, 0x12);
                    }
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }

        #endregion

        #region private methods
        private void leg_OnLegStatusChanged(ILeg leg)
        {
            _delayedLastStateChange = false;
            CheckJobStatus();
        }

        protected void CheckJobStatus()
        {
            //check if each leg is complete
            bool complete = true;
            bool atSite = false;
            bool atSiteCompleted = false;
            bool leftSite = false;
            bool onApproach = false;
            foreach (ILeg leg in _legs)
            {
                switch (leg.Status)
                {
                    case LegStatus.NotArrived:
                        complete = false;
                        break;
                    case LegStatus.OnApproach:
                        complete = false;
                        onApproach = true;
                        break;
                    case LegStatus.AtSite:
                    case LegStatus.InProgress:
                        complete = false;
                        atSite = true;
                        break;
                    case LegStatus.Complete:
                    case LegStatus.CompleteWithChanges:
                        atSiteCompleted = true;
                        break;
                    case LegStatus.LeftSite:
                        leftSite = true;
                        break;
                }
            }
            //check if each job action is complete
            bool rejected = false;
            bool accepted = false;
            bool started = false;
            bool finished = false;
            bool finishedRequired = false;
            bool additionalInfoRequired = false;
            foreach (IJobAction a in _jobActions)
            {
                if (a.Status == JobActionStatus.Complete)
                {
                    switch (a.JobActionType)
                    {
                        case JobActionTypes.Accept:
                            accepted = true;
                            break;
                        case JobActionTypes.Reject:
                            rejected = true;
                            break;
                        case JobActionTypes.Start:
                            started = true;
                            break;
                        case JobActionTypes.Finish:
                            finished = true;
                            break;
                    }
                }
                else if (a.JobActionType == JobActionTypes.AdditionalInfo && a.Status != JobActionStatus.Complete)
                {
                    additionalInfoRequired = true;
                }
                else if (a.JobActionType == JobActionTypes.Finish && a.Status != JobActionStatus.Complete)
                {
                    finishedRequired = true;
                }
            }

            JobStatus status = JobStatus.NotStarted;
            if (additionalInfoRequired)
            {
                status = JobStatus.NotStarted;
            }
            else if (complete && finishedRequired)
            {
                status = JobStatus.AllLegsCompleted;
            }
            else if (complete && (_completeJobWhenAllLegsAreComplete || finished))
            {
                status = JobStatus.Complete;
            }
            else if (atSite)
            {
                status = JobStatus.AtLeg;
            }
            else if (atSiteCompleted)
            {
                status = JobStatus.FinishedActionsAtLeg;
            }
            else if (leftSite || onApproach || _status == JobStatus.OnRoute)
            {
                status = JobStatus.OnRoute;
            }
            else if (accepted)
            {
                status = JobStatus.Accepted;
            }
            else if (_delayedLastStateChange)
            {
                status = JobStatus.Delayed;
            }
            else if (started)
            {
                status = JobStatus.Started;
            }

            if (rejected)
            {
                status = JobStatus.Rejected;
            }

            //if status is different, update it
            if (status != _status)
            {
                Status = status;
            }
        }

        private void jobAction_OnJobActionStatusChanged(IJobAction action)
        {
            if (action.JobActionType == JobActionTypes.Delay && action.Status == JobActionStatus.Complete)
            {
                _delayedLastStateChange = true;
            }
            else
            {
                _delayedLastStateChange = false;
            }

            CheckJobStatus();
        }
        #endregion

        private class JobActionIterator : IJobActions
        {
            private Job _job;
            public JobActionIterator(Job job) { _job = job; }
            #region IEnumerable Members

            public System.Collections.IEnumerator GetEnumerator()
            {
                return _job._jobActions.GetEnumerator();
            }

            #endregion
        }
    }

}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// email flags
    /// </summary>
    public enum EMailFlag
    {
        SendAtDistance = 0x01,
        SendOnArrival = 0x02,
        SendOnCompletation = 0x04
    }

    /// <summary>
    /// interface for a leg's location
    /// </summary>
    public interface ILegLocation : IPacketEncode
    {
        /// <summary>
        /// The location's name
        /// </summary>
        string Name { get; }
        /// <summary>
        /// the location's address
        /// </summary>
        string Address { get; }
        /// <summary>
        /// the contact name
        /// </summary>
        string ContactName { get; }
        /// <summary>
        /// the location's phone number
        /// </summary>
        string PhoneNumber { get; }

        /// <summary>
        /// the latitude of the location
        /// </summary>
        double Latitude { get; }
        /// <summary>
        /// the longitude of the location 
        /// </summary>
        double Longitude { get; }
        /// <summary>
        /// the radius of the location (in meters). Used to create a bounding box
        /// </summary>
        int Radius { get; }

        /// <summary>
        /// the location object that describes the location as either a circle, rectangle or polygon
        /// </summary>
        Location Location { get; }

        /// <summary>
        /// The email flags set for this location
        /// </summary>
        byte EmailFlags { get; }
        /// <summary>
        /// the distance in meters the EMailFlag.SendAtDistance is triggered 
        /// </summary>
        int EmailAtDistance { get; }

        bool IsEmailSendAtDistance { get; set;}
        bool IsEmailSendOnArrival { get; set;}
        bool IsEmailSendOnCompletion { get; set;}
    }
}

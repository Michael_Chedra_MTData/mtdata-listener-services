using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// enum for different sub commands on an arrive packet
    /// </summary>
    public enum ArriveTypes
    {
        /// <summary>
        /// Arrived at site (geo fenced)
        /// </summary>
        Arrive = 0,
        /// <summary>
        /// Arrived at site (manual)
        /// </summary>
        ArriveManual,
        /// <summary>
        /// Depart site
        /// </summary>
        Depart,
        /// <summary>
        /// Start actions
        /// </summary>
        StartActions
    }
    
    /// <summary>
    /// packet sent when an arrive event on a leg is triggered 
    /// </summary>
    public class JobArriveSiteSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private LegTypes _legType;
        private ArriveTypes _arriveType;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegTypes LegType
        {
            get { return _legType; }
            set { _legType = value; }
        }
        public ArriveTypes ArriveType
        {
            get { return _arriveType; }
            set { _arriveType = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public JobArriveSiteSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobArriveSite;
        }
        public JobArriveSiteSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobArriveSite)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobArriveSite, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobArriveSite;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legType);
                stream.WriteByte((byte)_arriveType);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _legType = (LegTypes)Data[index++];
            _arriveType = (ArriveTypes)Data[index++];
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class LegLocation: ILegLocation
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 4;
        private string _name;
        private string _address;
        private string _contactName;
        private string _phoneNumber;
        private Location _location;
        private byte _emailFlags;
        private int _emailAtDistance;
        #endregion

        #region constructor
        public LegLocation()
        {
            _name = string.Empty;
            _address = string.Empty;
            _phoneNumber = string.Empty;
            _contactName = string.Empty;
            _location = new Location();
        }
        #endregion

        #region ILegLocation Members

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public string ContactName
        {
            get { return _contactName; }
            set { _contactName = value; }
        }

        public string PhoneNumber
        {
            get { return _phoneNumber; }
            set { _phoneNumber = value; }
        }

        public double Latitude
        {
            get { return _location.CentrePoint.Latitude; }
            set { _location.CentrePoint.Latitude = value; }
        }

        public double Longitude
        {
            get { return _location.CentrePoint.Longitude; }
            set { _location.CentrePoint.Longitude = value; }
        }

        public int Radius
        {
            get { return _location.Radius; }
            set { _location.Radius = value; }
        }
        public Location Location 
        {
            get { return _location; }
            set { _location = value; }
        }

        public byte EmailFlags
        {
            get { return _emailFlags; }
            set { _emailFlags = value; }
        }

        public int EmailAtDistance
        {
            get { return _emailAtDistance; }
            set { _emailAtDistance = value; }
        }

        public bool IsEmailSendAtDistance
        {
            get
            {
                if ((_emailFlags & (byte)EMailFlag.SendAtDistance) != 0)
                {
                    return true;
                }
                return false;
            }
            set
            {
                SetEmailFlag(EMailFlag.SendAtDistance, value);
            }
        }
        public bool IsEmailSendOnArrival
        {
            get
            {
                if ((_emailFlags & (byte)EMailFlag.SendOnArrival) != 0)
                {
                    return true;
                }
                return false;
            }
            set
            {
                SetEmailFlag(EMailFlag.SendOnArrival, value);
            }
        }
        public bool IsEmailSendOnCompletion
        {
            get
            {
                if ((_emailFlags & (byte)EMailFlag.SendOnCompletation) != 0)
                {
                    return true;
                }
                return false;
            }
            set
            {
                SetEmailFlag(EMailFlag.SendOnCompletation, value);
            }
        }

        #endregion

        #region private methods
        private void SetEmailFlag(EMailFlag flag, bool on)
        {
            if (on)
            {
                _emailFlags = (byte)(_emailFlags | (uint)flag);
            }
            else
            {
                _emailFlags = (byte)(_emailFlags & ~(ulong)flag);
            }
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _name = BaseLayer.ReadString(data, ref index, 0x12);
            _address = BaseLayer.ReadString(data, ref index, 0x12);
            _phoneNumber = BaseLayer.ReadString(data, ref index, 0x12);

            //read the old fields
            double latitude = BaseLayer.ReadCoordinate(data, ref index);
            double longitude = BaseLayer.ReadCoordinate(data, ref index);
            int radius = BaseLayer.Read2ByteInteger(data, ref index);

            _emailFlags = (byte)data[index++];
            _emailAtDistance = BaseLayer.Read2ByteInteger(data, ref index);

            if (versionNumber >= 2)
            {
                _contactName = BaseLayer.ReadString(data, ref index, 0x12);
            }

            if (versionNumber >= 3)
            {
                _location = new Location();
                _location.Decode(data, ref index);
            }
            else
            {
                _location = new Location(LocationTypes.Circle);
                _location.CentrePoint.Latitude = latitude;
                _location.CentrePoint.Longitude = longitude;
                _location.Radius = radius;
            }

            if (versionNumber >= 4)
            {
                //email distance needs to be a int instead of a short as short is limited to 32.7km
                _emailAtDistance = BaseLayer.Read4ByteInteger(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, _name, 0x12);
                BaseLayer.WriteString(local, _address, 0x12);
                BaseLayer.WriteString(local, _phoneNumber, 0x12);
                //leave these 3 fields there for backwards compatabilty
                BaseLayer.WriteCoordinate(local, _location.CentrePoint.Latitude);
                BaseLayer.WriteCoordinate(local, _location.CentrePoint.Longitude);
                BaseLayer.Write2ByteInteger(local, _location.Radius);
                local.WriteByte(_emailFlags);
                BaseLayer.Write2ByteInteger(local, _emailAtDistance);
                BaseLayer.WriteString(local, _contactName, 0x12);
                _location.Encode(local);
                //email distance needs to be a int instead of a short as short is limited to 32.7km (for backwards compatiablity data has to be repeated)
                BaseLayer.Write4ByteInteger(local, _emailAtDistance);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Concrete
{
    public class ConcreteJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        /// <summary>
        /// the time onSite
        /// </summary>
        private int _onSiteTime;
        #endregion

        #region public properties
        /// <summary>
        /// Gets/Sets the on site time
        /// </summary>
        public int OnSiteTime
        {
            get { return this._onSiteTime; }
            set { this._onSiteTime = value; }
        }
        #endregion

        #region Constructor
        public ConcreteJob()
        {
            JobType = JobTypes.Concrete;
            CompleteJobWhenAllLegsAreComplete = true;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            this._onSiteTime = BaseLayer.Read2ByteInteger(data, ref index);
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                BaseLayer.Write2ByteInteger(local, this._onSiteTime);
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

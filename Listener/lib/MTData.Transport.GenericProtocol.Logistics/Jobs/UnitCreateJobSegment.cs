using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// used by unit to create a job on the fly from a template
    /// </summary>
    public class UnitCreateJobSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchId;
        private int _jobTemplateId;
        private ListId _jobTemplateListId;
        private string _title;
        private string _jobReference;
        private int _jobReferenceInt;
        private int _odometer;
        private int _totalFuel;
        private List<NewLegInfo> _legInfo;
        private string _docketNumber;
        private int _customerId;
        private ListId _customerListId;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public int JobTemplateId
        {
            get { return _jobTemplateId; }
            set { _jobTemplateId = value; }
        }
        public ListId JobTemplateListId
        {
            get { return _jobTemplateListId; }
            set { _jobTemplateListId = value; }
        }
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string JobReference
        {
            get { return _jobReference; }
            set { _jobReference = value; }
        }
        public int JobReferenceInt
        {
            get { return _jobReferenceInt; }
            set { _jobReferenceInt = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        public List<NewLegInfo> LegInfo
        {
            get { return _legInfo; }
            set { _legInfo = value; }
        }
        public string DocketNumber
        {
            get { return _docketNumber; }
            set { _docketNumber = value; }
        }
        public int CustomerId
        {
            get { return _customerId; }
            set { _customerId = value; }
        }
        public ListId CustomerListId
        {
            get { return _customerListId; }
            set { _customerListId = value; }
        }
        #endregion

        #region constructor
        public UnitCreateJobSegment()
        {
            Version = 5;
            Type = (int)LogisticsSegmentTypes.UnitCreateJob;
            _jobTemplateListId = new ListId();
            _legInfo = new List<NewLegInfo>();
            _customerListId = new ListId();
        }
        public UnitCreateJobSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.UnitCreateJob)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.UnitCreateJob, segment.Type));
            }
            Version = segment.Version;
            _jobTemplateListId = new ListId();
            _legInfo = new List<NewLegInfo>();
            _customerListId = new ListId();
            Type = (int)LogisticsSegmentTypes.UnitCreateJob;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchId);
                Write4ByteInteger(stream, _jobTemplateId);
                _jobTemplateListId.Encode(stream);
                WriteString(stream, _title, 0x12);
                WriteString(stream, _jobReference, 0x12);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                stream.WriteByte((byte)_legInfo.Count);
                foreach (NewLegInfo info in _legInfo)
                {
                    info.Encode(stream);
                }
                Write4ByteInteger(stream, _jobReferenceInt);
                WriteString(stream, _docketNumber, 0x12);
                Write4ByteInteger(stream, _customerId);
                _customerListId.Encode(stream);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchId = Read4ByteInteger(Data, ref index);
            _jobTemplateId = Read4ByteInteger(Data, ref index);
            _jobTemplateListId.Decode(Data, ref index);
            _title = ReadString(Data, ref index, 0x12);
            _jobReference = ReadString(Data, ref index, 0x12);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            _legInfo.Clear();
            if (Version >= 2)
            {
                int count = (int)Data[index++];
                for (int i = 0; i < count; i++)
                {
                    NewLegInfo info = new NewLegInfo();
                    info.Decode(Data, ref index);
                    _legInfo.Add(info);
                }
            }
            _jobReferenceInt = 0;
            if (Version >= 3)
            {
                _jobReferenceInt = Read4ByteInteger(Data, ref index);
            }
            _docketNumber = null;
            if (Version >= 4)
            {
                _docketNumber = ReadString(Data, ref index, 0x12);
            }
            if (Version >= 5)
            {
                _customerId = Read4ByteInteger(Data, ref index);
                _customerListId.Decode(Data, ref index);
            }
            else
            {
                _customerId = -1;
                _customerListId.Id = -1;
                _customerListId.Version = -1;
            }
        }
        #endregion


        #region NewLegInfo inner class
        public class NewLegInfo : IPacketEncode
        {
            #region private fields
            /// <summary>
            /// the version number of the decode/encode of this item
            /// </summary>
            private int _versionNumber = 1;

            private int _customerId;
            private ListId _customerListId;
            private string _customerName;
            private string _customerAddress;
            #endregion

            #region properties
            public int CustomerId
            {
                get { return _customerId; }
                set { _customerId = value; }
            }
            public ListId CustomerListId
            {
                get { return _customerListId; }
                set { _customerListId = value; }
            }
            public string CustomerName
            {
                get { return _customerName; }
                set { _customerName = value; }
            }
            public string CustomerAddress
            {
                get { return _customerAddress; }
                set { _customerAddress = value; }
            }

            #endregion

            #region constructor
            public NewLegInfo() 
            {
                _customerListId = new ListId();
            }
            public NewLegInfo(int customerId, ListId customerListId, string customerName, string customerAddress)
            {
                _customerId = customerId;
                _customerListId = customerListId;
                _customerName = customerName;
                _customerAddress = customerAddress;
            }
            #endregion

            #region IPacketEncode Members

            public void Decode(byte[] data, ref int index)
            {
                int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
                int length = BaseLayer.ReadMoreFlag(data, ref index);

                int startIndex = index;

                _customerId = BaseLayer.Read4ByteInteger(data, ref index);
                _customerListId.Decode(data, ref index);
                _customerName = BaseLayer.ReadString(data, ref index, 0x12);
                _customerAddress = BaseLayer.ReadString(data, ref index, 0x12);

                //skip any unread bytes
                int readBytes = index - startIndex;
                if (length > readBytes)
                {
                    index += length - readBytes;
                }
            }

            public void Encode(MemoryStream stream)
            {
                //write the data to a local stream
                using (System.IO.MemoryStream local = new System.IO.MemoryStream())
                {
                    BaseLayer.Write4ByteInteger(local, _customerId);
                    _customerListId.Encode(local);
                    BaseLayer.WriteString(local, _customerName, 0x12);
                    BaseLayer.WriteString(local, _customerAddress, 0x12);

                    //write version number
                    BaseLayer.WriteMoreFlag(stream, _versionNumber);
                    //write the length
                    BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                    //now write the data
                    BaseLayer.CopyStream(local, stream);
                }
            }

            #endregion
        }
        #endregion
    }
}

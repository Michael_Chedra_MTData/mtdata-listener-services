using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Service
{
    public class CostItem : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 1;
        /// <summary>
        /// the item id
        /// </summary>
        private int _itemId;
        /// <summary>
        /// the item list
        /// </summary>
        private ListId _itemList;
        /// <summary>
        /// the external ref
        /// </summary>
        private string _externalReference = "";
        /// <summary>
        /// the number of items
        /// </summary>
        private int _count;
        /// <summary>
        /// the cost of the item
        /// </summary>
        private float _totalCost;
        /// <summary>
        /// is read only
        /// </summary>
        private bool _isReadOnly;
        /// <summary>
        /// is answers read only
        /// </summary>
        private bool _isAnswerReadOnly;
        /// <summary>
        /// the id of the answers
        /// </summary>
        private int _answersId;
        /// <summary>
        /// if there are any questions associated with the cost item, this describes the answers
        /// </summary>
        private AnsweredQuestions _costItemAnswers;
        #endregion

        #region public properties
        /// <summary>
        /// the item id
        /// </summary>
        public int ItemId
        {
            get { return _itemId; }
            set { _itemId = value; }
        }
        /// <summary>
        /// the item list
        /// </summary>
        public ListId ItemList
        {
            get { return _itemList; }
            set { _itemList = value; }
        }
        /// <summary>
        /// the external reference
        /// </summary>
        public string ExternalReference
        {
            get { return _externalReference; }
            set { _externalReference = value; }
        }
        /// <summary>
        /// the number of items
        /// </summary>
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
        /// <summary>
        /// the cost of the item
        /// </summary>
        public float TotalCost
        {
            get { return _totalCost; }
            set { _totalCost = value; }
        }
        /// <summary>
        /// is the cost item read only
        /// </summary>
        public bool IsReadOnly
        {
            get { return _isReadOnly; }
            set { _isReadOnly = value; }
        }
        /// <summary>
        /// is the cost item answers read only
        /// </summary>
        public bool IsAnswerReadOnly
        {
            get { return _isAnswerReadOnly; }
            set { _isAnswerReadOnly = value; }
        }
        /// <summary>
        /// if there are any questions associated with the cost item, this describes the answers
        /// </summary>
        public AnsweredQuestions CostItemAnswers
        {
            get { return _costItemAnswers; }
            set { _costItemAnswers = value; }
        }
        public int AnswersId
        {
            get { return _answersId; }
            set { _answersId = value; }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public CostItem()
        {
            _costItemAnswers = new AnsweredQuestions();
            _itemList = new ListId();
        }

        public CostItem(CostItem item)
        {
            _answersId = item._answersId;
            _count = item._count;
            _externalReference = item._externalReference;
            _isAnswerReadOnly = item._isAnswerReadOnly;
            _isReadOnly = item._isReadOnly;
            _itemId = item._itemId;
            if (item._itemList != null)
            {
                _itemList = new ListId(item._itemList);
            }
            _totalCost = item._totalCost;
            if (item._costItemAnswers != null)
            {
                _costItemAnswers = new AnsweredQuestions(item._costItemAnswers);
            }
        }

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            _itemId = BaseLayer.Read4ByteInteger(data, ref index);
            _itemList.Decode(data, ref index);
            _externalReference = BaseLayer.ReadString(data, ref index, 0x12);
            _count = BaseLayer.Read4ByteInteger(data, ref index);
            int value = BaseLayer.Read4ByteInteger(data, ref index);
            _totalCost = value / 1000f;
            _isReadOnly = BaseLayer.ReadBool(data, ref index);
            _isAnswerReadOnly = BaseLayer.ReadBool(data, ref index);
            _answersId = BaseLayer.Read4ByteInteger(data, ref index);
            _costItemAnswers.Decode(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _itemId);
                _itemList.Encode(local);
                BaseLayer.WriteString(local, _externalReference, 0x12);
                BaseLayer.Write4ByteInteger(local, (_count));
                int value = (int)((_totalCost * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                BaseLayer.WriteBool(local, _isReadOnly);
                BaseLayer.WriteBool(local, _isAnswerReadOnly);
                BaseLayer.Write4ByteInteger(local, (_answersId));
                _costItemAnswers.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

    }
}

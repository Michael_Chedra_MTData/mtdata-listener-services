using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Service
{
    public class ServiceCompletedLegAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        /// <summary>
        /// the hours required
        /// </summary>
        private float _hours;
        /// <summary>
        /// the labour cost
        /// </summary>
        private float _labourCost;
        /// <summary>
        /// the parts cost
        /// </summary>
        private float _partCost;
        /// <summary>
        /// list of cost items
        /// </summary>
        private List<CostItem> _costItems;
        #endregion

        #region public properties
        /// <summary>
        /// the hours 
        /// </summary>
        public float Hours
        {
            get { return _hours; }
            set { _hours = value; }
        }
        /// <summary>
        /// the labour cost
        /// </summary>
        public float LabourCost
        {
            get { return _labourCost; }
            set { _labourCost = value; }
        }
        /// <summary>
        /// the parts cost
        /// </summary>
        public float PartCost
        {
            get { return _partCost; }
            set { _partCost = value; }
        }
        /// <summary>
        /// list of cost items
        /// </summary>
        public List<CostItem> CostItems
        {
            get { return _costItems; }
            set { _costItems = value; }
        }
        #endregion

        /// <summary>
        /// Constructor
        /// </summary>
        public ServiceCompletedLegAction() 
        {
            _costItems = new List<CostItem>();
            LegActionType = LegActionTypes.Service;
        }

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            int value = BaseLayer.Read4ByteInteger(data, ref index);
            _hours = value / 1000f;
            value = BaseLayer.Read4ByteInteger(data, ref index);
            _labourCost = value / 1000f;
            value = BaseLayer.Read4ByteInteger(data, ref index);
            _partCost = value / 1000f;
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                CostItem item = new CostItem();
                item.Decode(data, ref index);
                _costItems.Add(item);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                int value = (int)((_hours * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                value = (int)((_labourCost * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                value = (int)((_partCost * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                BaseLayer.Write2ByteInteger(local, _costItems.Count);
                foreach (CostItem item in _costItems)
                {
                    item.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

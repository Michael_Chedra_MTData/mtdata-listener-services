using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Service
{
    /// <summary>
    /// class represents a service job
    /// </summary>
    public class ServiceJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        
        /// <summary>
        /// a list of part lists used by the dispatcher when creating the job
        /// </summary>
        private List<ListId> _partsListForDispatch;
        /// <summary>
        /// the part lists which can be used by a driver when doing the job
        /// </summary>
        private List<ListId> _partsListForDriver;
        #endregion

        #region public properties
        /// <summary>
        /// the part lists which can be used by a dispatcher when creating the job
        /// </summary>
        public List<ListId> PartsListForDispatch
        {
            get { return _partsListForDispatch; }
            set { _partsListForDispatch = value; }
        }
        /// <summary>
        /// the part lists which can be used by a driver when doing the job
        /// </summary>
        public List<ListId> PartsListForDriver
        {
            get { return _partsListForDriver; }
            set { _partsListForDriver = value; }
        }
        #endregion

        #region Constructor
        public ServiceJob()
        {
            JobType = JobTypes.Service;
            _partsListForDispatch = new List<ListId>();
            _partsListForDriver = new List<ListId>();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _partsListForDispatch = new List<ListId>();
            _partsListForDriver = new List<ListId>();

            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                ListId id = new ListId();
                id.Decode(data, ref index);
                _partsListForDispatch.Add(id);
            }
            count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                ListId id = new ListId();
                id.Decode(data, ref index);
                _partsListForDriver.Add(id);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write2ByteInteger(local, _partsListForDispatch.Count);
                foreach (ListId id in _partsListForDispatch)
                {
                    id.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _partsListForDriver.Count);
                foreach (ListId id in _partsListForDriver)
                {
                    id.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    

    }
}

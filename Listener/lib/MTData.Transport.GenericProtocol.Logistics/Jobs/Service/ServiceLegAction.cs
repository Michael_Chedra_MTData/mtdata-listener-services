using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Service
{
    /// <summary>
    /// class represents a recovery leg action
    /// </summary>
    public class ServiceLegAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private string _requirements;
        private float _hoursRequired;
        private int _serviceJobTypeId;
        private ListId _serviceJobTypeList;
        private int _priorityId;
        private ListId _priorityList;
        private float _labourCost;
        private float _partCost;
        private bool _fixedPrice;
        private List<CostItem> _costItems;

        #endregion

        #region public properties
        /// <summary>
        /// the requirements
        /// </summary>
        public string Requirements
        {
            get { return _requirements; }
            set { _requirements = value; }
        }
        /// <summary>
        /// the hours required
        /// </summary>
        public float HoursRequired
        {
            get { return _hoursRequired; }
            set { _hoursRequired = value; }
        }
        /// <summary>
        /// the service Job Type id
        /// </summary>
        public int ServiceJobTypeId
        {
            get { return _serviceJobTypeId; }
            set { _serviceJobTypeId = value; }
        }
        /// <summary>
        /// the service Job Type List
        /// </summary>
        public ListId ServiceJobTypeList
        {
            get { return _serviceJobTypeList; }
            set { _serviceJobTypeList = value; }
        }
        /// <summary>
        /// the priority id
        /// </summary>
        public int PriorityId
        {
            get { return _priorityId; }
            set { _priorityId = value; }
        }
        /// <summary>
        /// the priority list
        /// </summary>
        public ListId PriorityList
        {
            get { return _priorityList; }
            set { _priorityList = value; }
        }
        /// <summary>
        /// the labour cost
        /// </summary>
        public float LabourCost
        {
            get { return _labourCost; }
            set { _labourCost = value; }
        }
        /// <summary>
        /// the parts cost
        /// </summary>
        public float PartCost
        {
            get { return _partCost; }
            set { _partCost = value; }
        }
        /// <summary>
        /// is the job a fixed price job
        /// </summary>
        public bool FixedPrice
        {
            get { return _fixedPrice; }
            set { _fixedPrice = value; }
        }
        /// <summary>
        /// list of cost items
        /// </summary>
        public List<CostItem> CostItems
        {
            get { return _costItems; }
            set { _costItems = value; }
        }

        #endregion

        #region constructor
        public ServiceLegAction()
        {
            LegActionType = LegActionTypes.Service;
            _requirements = string.Empty;
            _costItems = new List<CostItem>();
            _serviceJobTypeList = new ListId();
            _priorityList = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _costItems = new List<CostItem>();
            _requirements = BaseLayer.ReadString(data, ref index, 0x12);
            int value = BaseLayer.Read4ByteInteger(data, ref index);
            _hoursRequired = value / 1000f;
            _serviceJobTypeId = BaseLayer.Read4ByteInteger(data, ref index);
            _serviceJobTypeList.Decode(data, ref index);
            _priorityId = BaseLayer.Read4ByteInteger(data, ref index);
            _priorityList.Decode(data, ref index);
            value = BaseLayer.Read4ByteInteger(data, ref index);
            _labourCost = value / 1000f;
            value = BaseLayer.Read4ByteInteger(data, ref index);
            _partCost = value / 1000f;
            _fixedPrice = BaseLayer.ReadBool(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            for (int i = 0; i < count; i++)
            {
                CostItem item = new CostItem();
                item.Decode(data, ref index);
                _costItems.Add(item);
            }
            
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _requirements, 0x12);
                int value = (int)((_hoursRequired * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                BaseLayer.Write4ByteInteger(local, _serviceJobTypeId);
                _serviceJobTypeList.Encode(local);
                BaseLayer.Write4ByteInteger(local, _priorityId);
                _priorityList.Encode(local);
                value = (int)((_labourCost * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                value = (int)((_partCost * 1000) + 0.5);
                BaseLayer.Write4ByteInteger(local, value);
                BaseLayer.WriteBool(local, _fixedPrice);
                BaseLayer.Write2ByteInteger(local, _costItems.Count);
                foreach (CostItem item in _costItems)
                {
                    item.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

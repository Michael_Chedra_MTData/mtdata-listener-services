using System;
using System.Collections.Generic;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Common;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// a question action
    /// </summary>
    public class QuestionAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private ListId _questionListId;
        private QuestionGroupValidation _validation;
        #endregion

        #region public properties
        public ListId QuestionListId
        {
            get { return _questionListId; }
            set { _questionListId = value; }
        }
        public QuestionGroupValidation Validation
        {
            get { return _validation; }
            set { _validation = value; }
        }
        #endregion

        #region constructor
        public QuestionAction()
            : this(LegActionTypes.Questions)
        {
        }
        public QuestionAction(LegActionTypes actionType)
        {
            LegActionType = actionType;
            _questionListId = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _questionListId.Decode(data, ref index);
            _validation = null;
            if (versionNumber >= 2)
            {
                bool hasValidation = BaseLayer.ReadBool(data, ref index);
                if (hasValidation)
                {
                    _validation = new QuestionGroupValidation();
                    _validation.Decode(data, ref index);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _questionListId.Encode(local);
                if (_validation != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _validation.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

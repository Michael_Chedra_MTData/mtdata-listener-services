using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by base to delete an leg from a job
    /// </summary>
    public class DeleteLegsSegment : SegmentLayer
    {
        #region private fields
        private int _allocationId;
        private int _dispatchId;
        private List<int> _legIds;
        #endregion

        #region properties
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public List<int> LegIds
        {
            get { return _legIds; }
            set { _legIds = value; }
        }
        #endregion

        #region constructor
        public DeleteLegsSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.DeleteLeg;
            _legIds = new List<int>();
        }
        public DeleteLegsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.DeleteLeg)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.DeleteLeg, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.DeleteLeg;
            _legIds = new List<int>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                Write4ByteInteger(stream, _dispatchId);
                Write2ByteInteger(stream, _legIds.Count);
                for (int i = 0; i < _legIds.Count; i++)
                {
                    Write4ByteInteger(stream, _legIds[i]);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
            _dispatchId = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            for (int i = 0; i < count; i++)
            {
                _legIds.Add(Read4ByteInteger(Data, ref index));
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.History
{
    public class SendJobHistorySegment : SegmentLayer
    {
        #region private fields
        private int _driverId;
        private DateTime _startTime;
        private DateTime _endTime;
        private List<JobHistory> _jobHistory;
        #endregion

        #region properties
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }
        public List<JobHistory> JobHistory
        {
            get { return _jobHistory; }
            set { _jobHistory = value; }
        }
        #endregion

        #region constructor
        public SendJobHistorySegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendJobHistory;
            _jobHistory = new List<JobHistory>();
        }
        public SendJobHistorySegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendJobHistory)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendJobHistory, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendJobHistory;
            Data = segment.Data;
            _jobHistory = new List<JobHistory>();
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _driverId);
                WriteDateTime(stream, _startTime);
                WriteDateTime(stream, _endTime);
                Write2ByteInteger(stream, _jobHistory.Count);
                foreach (JobHistory job in _jobHistory)
                {
                    job.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _driverId = Read4ByteInteger(Data, ref index);
            _startTime = ReadDateTime(Data, ref index);
            _endTime = ReadDateTime(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _jobHistory.Clear();
            for (int i = 0; i < count; i++)
            {
                JobHistory job = new JobHistory();
                job.Decode(Data, ref index);
                _jobHistory.Add(job);
            }
        }
        #endregion

    }

}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.History
{
    public class SendJobHistoryDetailedSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchId;
        private Dictionary<string, string> _jobDetails;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public Dictionary<string, string> JobDetails
        {
            get { return _jobDetails; }
            set { _jobDetails = value; }
        }
        #endregion

        #region constructor
        public SendJobHistoryDetailedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendJobHistoryDetailed;
            _jobDetails = new Dictionary<string, string>();
        }
        public SendJobHistoryDetailedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendJobHistoryDetailed)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendJobHistoryDetailed, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendJobHistoryDetailed;
            Data = segment.Data;
            _jobDetails = new Dictionary<string, string>();
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchId);
                Write2ByteInteger(stream, _jobDetails.Count);
                foreach (string label in _jobDetails.Keys)
                {
                    WriteString(stream, label, 0x12);
                    WriteString(stream, _jobDetails[label], 0x12);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchId = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _jobDetails.Clear();
            for (int i = 0; i < count; i++)
            {
                string label = ReadString(Data, ref index, 0x12);
                string value = ReadString(Data, ref index, 0x12);
                _jobDetails.Add(label, value);
            }
        }
        #endregion

    }

}

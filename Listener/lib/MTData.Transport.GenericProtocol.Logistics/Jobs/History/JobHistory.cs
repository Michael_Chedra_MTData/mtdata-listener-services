using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.History
{
    public class JobHistory : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1; 
        private int _dispatchId;
        private string _summaryLine1;
        private string _summaryLine2;
        private JobStatus _status;
        private JobTypes _jobType;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public string SummaryLine1
        {
            get { return _summaryLine1; }
            set { _summaryLine1 = value; }
        }
        public string SummaryLine2
        {
            get { return _summaryLine2; }
            set { _summaryLine2 = value; }
        }
        public JobTypes JobType
        {
            get { return _jobType; }
            set { _jobType = value; }
        }
        public JobStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }
        #endregion

        #region constructor
        public JobHistory()
        {
            _dispatchId = -1;
            _summaryLine1 = string.Empty;
            _summaryLine2 = string.Empty;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
            _summaryLine1 = BaseLayer.ReadString(data, ref index, 0x12);
            _summaryLine2 = BaseLayer.ReadString(data, ref index, 0x12);
            _jobType = (JobTypes)data[index++];
            _status = (JobStatus)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _dispatchId);
                BaseLayer.WriteString(local, _summaryLine1, 0x12);
                BaseLayer.WriteString(local, _summaryLine2, 0x12);
                local.WriteByte((byte)_jobType);
                local.WriteByte((byte)_status);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion   
    }
}

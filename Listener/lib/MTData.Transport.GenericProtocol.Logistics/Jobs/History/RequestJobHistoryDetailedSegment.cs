using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.History
{
    public class RequestJobHistoryDetailedSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchId;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        #endregion

        #region constructor
        public RequestJobHistoryDetailedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.RequestJobHistoryDetailed;
        }
        public RequestJobHistoryDetailedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.RequestJobHistoryDetailed)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.RequestJobHistoryDetailed, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.RequestJobHistoryDetailed;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchId = Read4ByteInteger(Data, ref index);
        }
        #endregion    
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.History
{
    public class RequestJobHistorySegment : SegmentLayer
    {
        #region private fields
        private int _driverId;
        private DateTime _startTime;
        private DateTime _endTime;
        #endregion

        #region properties
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public DateTime StartTime
        {
            get { return _startTime; }
            set { _startTime = value; }
        }
        public DateTime EndTime
        {
            get { return _endTime; }
            set { _endTime = value; }
        }
        #endregion

        #region constructor
        public RequestJobHistorySegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.RequestJobHistory;
        }
        public RequestJobHistorySegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.RequestJobHistory)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.RequestJobHistory, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.RequestJobHistory;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _driverId);
                WriteDateTime(stream, _startTime);
                WriteDateTime(stream, _endTime);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _driverId = Read4ByteInteger(Data, ref index);
            _startTime = ReadDateTime(Data, ref index);
            _endTime = ReadDateTime(Data, ref index);
        }
        #endregion    
    }
}

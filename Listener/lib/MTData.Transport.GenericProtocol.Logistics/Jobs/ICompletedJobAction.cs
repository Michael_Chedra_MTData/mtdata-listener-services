using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// interface for a completed action
    /// </summary>
    public interface ICompletedJobAction : IPacketEncode
    {
        /// <summary>
        /// the Job action type
        /// </summary>
        JobActionTypes JobActionType { get; }

        /// <summary>
        /// The job Action id
        /// </summary>
        int JobActionId { get; }
    }
}

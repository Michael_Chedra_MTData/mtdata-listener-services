using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    public class PalletLoad : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 8;
        private int _palletType;
        private int _count;
        private ListId _listId;
        private int _weight;
        private string _ref;
        private string _itemUnitType;
        private string _dimensions;
        private string _notes;
        private int _totalQuantity;
        private string _totalUOM;
        private AnsweredQuestions _rejectedAnswers;
        private string _name;
        #endregion

        #region properties
        public int PalletType
        {
            get { return _palletType; }
            set { _palletType = value; }
        }
        public int Count
        {
            get { return _count; }
            set { _count = value; }
        }
        public ListId PalletListId
        {
            get { return _listId; }
            set { _listId = value; }
        }
        public int Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public string Ref
        {
            get { return _ref; }
            set { _ref = value; }
        }
        public string ItemUnitType
        {
            get { return _itemUnitType; }
            set { _itemUnitType = value; }
        }
        public string Dimensions
        {
            get { return _dimensions; }
            set { _dimensions = value; }
        }
        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }
        public int TotalQuantity
        {
            get { return _totalQuantity; }
            set { _totalQuantity = value; }
        }
        public string TotalUOM
        {
            get { return _totalUOM; }
            set { _totalUOM = value; }
        }
        public AnsweredQuestions RejectedAnswers
        {
            get { return _rejectedAnswers; }
            set { _rejectedAnswers = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        #endregion

        #region constructor
        public PalletLoad()
        {
            _listId = new ListId();
        }
        public PalletLoad(int palletType, int count, ListId palletListId)
        {
            _palletType = palletType;
            _count = count;
            _listId = palletListId;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _palletType = BaseLayer.Read4ByteInteger(data, ref index);
            _count = (int)data[index++];

            _ref = string.Empty;
            _dimensions = string.Empty;
            _notes = string.Empty;
            if (versionNumber >= 2)
            {
                _listId.Decode(data, ref index);
                if (versionNumber >= 3)
                {
                    _weight = BaseLayer.Read4ByteInteger(data, ref index);
                    _ref = BaseLayer.ReadString(data, ref index, 0x12);
                    if (versionNumber >= 4)
                    {
                        _itemUnitType = BaseLayer.ReadString(data, ref index, 0x12);
                        if (versionNumber >= 5)
                        {
                            _dimensions = BaseLayer.ReadString(data, ref index, 0x12);
                            _notes = BaseLayer.ReadString(data, ref index, 0x12);
                        }
                    }
                }
            }

            if (versionNumber >= 6)
            {
                _count = BaseLayer.Read4ByteInteger(data, ref index);
            }
            _totalQuantity = 0;
            _totalUOM = string.Empty;
            _rejectedAnswers = null;
            _name = string.Empty;
            if (versionNumber >= 7)
            {
                _totalQuantity = BaseLayer.Read4ByteInteger(data, ref index);
                _totalUOM = BaseLayer.ReadString(data, ref index, 0x12);
                bool rejected = BaseLayer.ReadBool(data, ref index);
                if (rejected)
                {
                    _rejectedAnswers = new AnsweredQuestions();
                    _rejectedAnswers.Decode(data, ref index);
                }
            }

            if (versionNumber >= 8)
            {
                _name = BaseLayer.ReadString(data, ref index, 0x12);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _palletType);
                local.WriteByte((byte)_count);
                if (_versionNumber >= 2)
                {
                    _listId.Encode(local);
                    if (_versionNumber >= 3)
                    {
                        BaseLayer.Write4ByteInteger(local, _weight);
                        BaseLayer.WriteString(local, _ref, 0x12);
                        if (_versionNumber >= 4)
                        {
                            BaseLayer.WriteString(local, _itemUnitType, 0x12);
                            if (_versionNumber >= 5)
                            {
                                BaseLayer.WriteString(local, _dimensions, 0x12);
                                BaseLayer.WriteString(local, _notes, 0x12);
                            }
                        }
                    }
                }

                if (_versionNumber >= 6)
                {
                    BaseLayer.Write4ByteInteger(local, _count);
                }
                BaseLayer.Write4ByteInteger(local, _totalQuantity);
                BaseLayer.WriteString(local, _totalUOM, 0x12);
                if (_rejectedAnswers != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _rejectedAnswers.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }

                if (_versionNumber >= 8)
                {
                    BaseLayer.WriteString(local, _name, 0x12);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

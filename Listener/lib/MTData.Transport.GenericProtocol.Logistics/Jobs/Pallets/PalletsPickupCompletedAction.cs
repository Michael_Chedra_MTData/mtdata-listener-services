using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    public class PalletsPickupCompletedAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;

        private ListId _divisionListId;
        private int _divisionId;
        private List<PalletLoad> _pallets;
        private ListId _palletListId;
        private AnsweredQuestions _rejectedAnswers;
        #endregion

        #region public properties
        public ListId DivisionListId
        {
            get { return _divisionListId; }
            set { _divisionListId = value; }
        }
        public int DivisionId
        {
            get { return _divisionId; }
            set { _divisionId = value; }
        }
        public List<PalletLoad> Pallets
        {
            get { return _pallets; }
            set { _pallets = value; }
        }
        public ListId PalletListId
        {
            get { return _palletListId; }
            set { _palletListId = value; }
        }
        public AnsweredQuestions RejectedAnswers
        {
            get { return _rejectedAnswers; }
            set { _rejectedAnswers = value; }
        }
        #endregion

        #region constructor
        public PalletsPickupCompletedAction()
        {
            LegActionType = LegActionTypes.PalletPickup;
            _pallets = new List<PalletLoad>();
            _palletListId = new ListId();
            _divisionListId = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _divisionListId.Decode(data, ref index);
            _divisionId = BaseLayer.Read4ByteInteger(data, ref index);
            _palletListId.Decode(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _pallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad pallet = new PalletLoad();
                pallet.Decode(data, ref index);
                _pallets.Add(pallet);
            }
            _rejectedAnswers = null;
            if (versionNumber >= 2)
            {
                bool rejected = BaseLayer.ReadBool(data, ref index);
                if (rejected)
                {
                    _rejectedAnswers = new AnsweredQuestions();
                    _rejectedAnswers.Decode(data, ref index);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _divisionListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _divisionId);
                _palletListId.Encode(local);
                BaseLayer.Write2ByteInteger(local, _pallets.Count);
                foreach (PalletLoad p in _pallets)
                {
                    p.Encode(local);
                }

                if (_rejectedAnswers != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _rejectedAnswers.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

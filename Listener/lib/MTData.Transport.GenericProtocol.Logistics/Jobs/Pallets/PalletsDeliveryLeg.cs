using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    /// <summary>
    /// This class represents a pallet delivery
    /// </summary>
    public class PalletsDeliveryLeg : Leg
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;
        private bool _autoComplete;
        private string _exchangePalletOrderNo;
        private bool _isReturnRejectedItemsLeg;
        private bool _skipStart;
        private bool _dangerousGoods;
        private string _extraInfoLabel;
        #endregion

        #region public properties
        public bool AutoComplete
        {
            get { return _autoComplete; }
            set { _autoComplete = value; }
        }
        public string ExchangePalletOrderNo
        {
            get { return _exchangePalletOrderNo; }
            set { _exchangePalletOrderNo = value; }
        }
        public bool IsReturnRejectedItemsLeg
        {
            get { return _isReturnRejectedItemsLeg; }
            set { _isReturnRejectedItemsLeg = value; }
        }
        public bool SkipStart
        {
            get { return _skipStart; }
            set { _skipStart = value; }
        }
        public bool DangerousGoods
        {
            get { return _dangerousGoods; }
            set { _dangerousGoods = value; }
        }
        public string ExtraInfoLabel
        {
            get { return _extraInfoLabel; }
            set { _extraInfoLabel = value; }
        }
        #endregion

        #region constructor
        public PalletsDeliveryLeg()
        {
            LegType = LegTypes.PalletDelivery;
            _exchangePalletOrderNo = string.Empty;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _autoComplete = BaseLayer.ReadBool(data, ref index);
            _exchangePalletOrderNo = BaseLayer.ReadString(data, ref index, 0x12);
            _isReturnRejectedItemsLeg = BaseLayer.ReadBool(data, ref index);
            _skipStart = false;
            _dangerousGoods = false;
            _extraInfoLabel = string.Empty;
            if (versionNumber >= 2)
            {
                _skipStart = BaseLayer.ReadBool(data, ref index);
                if (versionNumber >= 3)
                {
                    _dangerousGoods = BaseLayer.ReadBool(data, ref index);
                    _extraInfoLabel = BaseLayer.ReadString(data, ref index, 0x12);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _autoComplete);
                BaseLayer.WriteString(local, _exchangePalletOrderNo, 0x12);
                BaseLayer.WriteBool(local, _isReturnRejectedItemsLeg);
                BaseLayer.WriteBool(local, _skipStart);
                BaseLayer.WriteBool(local, _dangerousGoods);
                BaseLayer.WriteString(local, _extraInfoLabel, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion   
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    /// <summary>
    /// the pallet question action (used for pickups and deliveries)
    /// </summary>
    public class PalletsQuestionAction : QuestionAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private bool _delayReasonRequired;
        private bool _documentationRequired;
        private bool _podRequired;
        #endregion

        #region public properties
        public bool DelayReasonRequired
        {
            get { return _delayReasonRequired; }
            set { _delayReasonRequired = value; }
        }
        public bool DocumentationRequired
        {
            get { return _documentationRequired; }
            set { _documentationRequired = value; }
        }
        public bool PodRequired
        {
            get { return _podRequired; }
            set { _podRequired = value; }
        }
        #endregion

        #region constructor
        public PalletsQuestionAction()
        {
            LegActionType = LegActionTypes.PalletQuestions;
            _delayReasonRequired = false;
            _documentationRequired = false;
            _podRequired = false;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _delayReasonRequired = BaseLayer.ReadBool(data, ref index);
            _documentationRequired = BaseLayer.ReadBool(data, ref index);
            _podRequired = BaseLayer.ReadBool(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _delayReasonRequired);
                BaseLayer.WriteBool(local, _documentationRequired);
                BaseLayer.WriteBool(local, _podRequired);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion

    }
}

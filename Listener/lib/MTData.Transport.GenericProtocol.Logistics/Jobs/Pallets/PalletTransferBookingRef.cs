using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    public class PalletTransferBookingRef : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private string _bookingRef;
        private int _vendorListId;
        private int _vendorUniqueKey;
        private List<PalletTransferPallet> _pallets;
        #endregion

        #region public properties
        public string BookingRef
        {
            get { return _bookingRef; }
            set { _bookingRef = value; }
        }
        public int VendorListId
        {
            get { return _vendorListId; }
            set { _vendorListId = value; }
        }
        public int VendorUniqueKey
        {
            get { return _vendorUniqueKey; }
            set { _vendorUniqueKey = value; }
        }
        public List<PalletTransferPallet> Pallets
        {
            get { return _pallets; }
            set { _pallets = value; }
        }
        #endregion

        #region constructor
        public PalletTransferBookingRef()
        {
            _pallets = new List<PalletTransferPallet>();
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _bookingRef = BaseLayer.ReadString(data, ref index, 0x12);
            _vendorListId = BaseLayer.Read4ByteInteger(data, ref index);
            _vendorUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            int count = (int)data[index++];
            _pallets = new List<PalletTransferPallet>();
            for (int i=0; i<count; i++)
            {
                PalletTransferPallet p = new PalletTransferPallet();
                p.Decode(data, ref index);
                _pallets.Add(p);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, _bookingRef, 0x12);
                BaseLayer.Write4ByteInteger(local, _vendorListId);
                BaseLayer.Write4ByteInteger(local, _vendorUniqueKey);
                local.WriteByte((byte)_pallets.Count);
                foreach (PalletTransferPallet p in _pallets)
                {
                    p.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

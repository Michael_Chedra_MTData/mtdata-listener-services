using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    /// <summary>
    /// a pallet delivery action
    /// </summary>
    public class PalletsDeliveryAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;
        private List<PalletLoad> _pallets;
        private List<PalletLoad> _exchangePallets;
        private bool _skipPalletQuestions;
        private int _divisionId;
        private bool _skipReturnQuestions;
        private string _extraInfo;
        private int _totalWeightKg;
        private string _divisionName;
        #endregion

        #region public properties
        public List<PalletLoad> Pallets
        {
            get { return _pallets; }
            set { _pallets = value; }
        }
        public List<PalletLoad> ExchangePallets
        {
            get { return _exchangePallets; }
            set { _exchangePallets = value; }
        }
        public bool SkipPalletQuestions
        {
            get { return _skipPalletQuestions; }
            set { _skipPalletQuestions = value; }
        }
        public int DivisionId
        {
            get { return _divisionId; }
            set { _divisionId = value; }
        }
        public bool SkipReturnQuestions
        {
            get { return _skipReturnQuestions; }
            set { _skipReturnQuestions = value; }
        }
        public string ExtraInfo
        {
            get { return _extraInfo; }
            set { _extraInfo = value; }
        }
        public int TotalWeightKg
        {
            get { return _totalWeightKg; }
            set { _totalWeightKg = value; }
        }
        public string DivisionName
        {
            get { return _divisionName; }
            set { _divisionName = value; }
        }
        #endregion

        #region constructor
        public PalletsDeliveryAction()
        {
            LegActionType = LegActionTypes.PalletDelivery;
            _pallets = new List<PalletLoad>();
            _exchangePallets = new List<PalletLoad>();
            _skipPalletQuestions = false;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _skipPalletQuestions = BaseLayer.ReadBool(data, ref index);
            _divisionId = BaseLayer.Read4ByteInteger(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _pallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad p = new PalletLoad();
                p.Decode(data, ref index);
                _pallets.Add(p);
            }
            count = BaseLayer.Read2ByteInteger(data, ref index);
            _exchangePallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad p = new PalletLoad();
                p.Decode(data, ref index);
                _exchangePallets.Add(p);
            }
            _extraInfo = string.Empty;
            _skipReturnQuestions = false;
            if (versionNumber >= 2)
            {
                _extraInfo = BaseLayer.ReadString(data, ref index, 0x12);
                _skipReturnQuestions = BaseLayer.ReadBool(data, ref index);
            }
            _totalWeightKg = 0;
            _divisionName = string.Empty;
            if (versionNumber >= 3)
            {
                _totalWeightKg = BaseLayer.Read4ByteInteger(data, ref index);
                _divisionName = BaseLayer.ReadString(data, ref index, 0x12);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _skipPalletQuestions);
                BaseLayer.Write4ByteInteger(local, _divisionId);
                BaseLayer.Write2ByteInteger(local, _pallets.Count);
                foreach (PalletLoad p in _pallets)
                {
                    p.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _exchangePallets.Count);
                foreach (PalletLoad p in _exchangePallets)
                {
                    p.Encode(local);
                }
                BaseLayer.WriteString(local, _extraInfo, 0x12);
                BaseLayer.WriteBool(local, _skipReturnQuestions);
                BaseLayer.Write4ByteInteger(local, _totalWeightKg);
                BaseLayer.WriteString(local, _divisionName, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

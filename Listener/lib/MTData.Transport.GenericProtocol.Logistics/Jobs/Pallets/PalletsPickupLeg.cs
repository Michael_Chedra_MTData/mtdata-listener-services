using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    /// <summary>
    /// class that represents a pallet pickup leg
    /// </summary>
    public class PalletsPickupLeg : Leg
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;
        private bool _autoComplete;
        private bool _dangerousGoods;
        private bool _skipStart;
        private string _extraInfoLabel;
        #endregion

        #region public properties
        public bool AutoComplete
        {
            get { return _autoComplete; }
            set { _autoComplete = value; }
        }
        public bool DangerousGoods
        {
            get { return _dangerousGoods; }
            set { _dangerousGoods = value; }
        }
        public bool SkipStart
        {
            get { return _skipStart; }
            set { _skipStart = value; }
        }
        public string ExtraInfoLabel
        {
            get { return _extraInfoLabel; }
            set { _extraInfoLabel = value; }
        }
        #endregion

        #region constructor
        public PalletsPickupLeg()
        {
            LegType = LegTypes.PalletPickup;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _autoComplete = BaseLayer.ReadBool(data, ref index);
            _dangerousGoods = BaseLayer.ReadBool(data, ref index);
            _skipStart = false;
            _extraInfoLabel = string.Empty;
            if (versionNumber >= 2)
            {
                _skipStart = BaseLayer.ReadBool(data, ref index);
                if (versionNumber >= 3)
                {
                    _extraInfoLabel = BaseLayer.ReadString(data, ref index, 0x12);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _autoComplete);
                BaseLayer.WriteBool(local, _dangerousGoods);
                BaseLayer.WriteBool(local, _skipStart);
                BaseLayer.WriteString(local, _extraInfoLabel, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

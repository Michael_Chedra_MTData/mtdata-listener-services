using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    /// <summary>
    /// the class that represents a pallets job
    /// </summary>
    public class PalletsJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 9;
        /// <summary>
        /// the job's load type
        /// </summary>
        private string _loadType;
        /// <summary>
        /// list of trailers for the job
        /// </summary>
        private List<int> _trailerIds;
        /// <summary>
        /// if any items are rejected, create a new reject leg
        /// </summary>
        private bool _createRejectItemsLeg;
        /// <summary>
        /// is the leg times shown for this job
        /// </summary>
        private bool _showTimes;
        private bool _autoComplete;
        private ListId _itemsListId;
        private ListId _pickupCustomersListId;
        private ListId _pickupQuestionsId;
        private ListId _deliveryCustomersListId;
        private ListId _deliveryQuestionsId;
        private bool _driverCanCreateLegs;
        private ListId _emptyItemsListId;
        private ListId _outOfSequenceListId;
        private ListId _rejectReasonListId;
        private ListId _emptyItemsQuestionListId;
        private bool _driverAdhocItems;
        private bool _trackableAssetLegActionsEnabled;
        private bool _palletTransferUsed;
        private ListId _palletTransferCustomerListId;
        private int _palletTransferClientListId;
        private int _palletTransferClientUniqueKey;
        private ListId _palletTransferVendorsListId;
        private ListId _palletTransferAccountIdsListId;
        private bool _inspectAssetOnPickup;
        private bool _inspectAssetOnDrop;
        private bool _palletTransferRequired;
        #endregion

        #region public properties
        public string LoadType
        {
            get { return _loadType; }
            set { _loadType = value; }
        }
        public List<int> TrailerIds
        {
            get { return _trailerIds; }
            set { _trailerIds = value; }
        }
        public bool CreateRejectItemsLeg
        {
            get { return _createRejectItemsLeg; }
            set { _createRejectItemsLeg = value; }
        }
        public bool ShowTimes
        {
            get { return _showTimes; }
            set { _showTimes = value; }
        }
        public bool AutoComplete
        {
            get { return _autoComplete; }
            set 
            {
                _autoComplete = value;
                CompleteJobWhenAllLegsAreComplete = _autoComplete;
            }
        }
        public ListId ItemsListId
        {
            get { return _itemsListId; }
            set { _itemsListId = value; }
        }
        public ListId PickupCustomersListId
        {
            get { return _pickupCustomersListId; }
            set { _pickupCustomersListId = value; }
        }
        public ListId PickupQuestionsId
        {
            get { return _pickupQuestionsId; }
            set { _pickupQuestionsId = value; }
        }
        public ListId DeliveryCustomersListId
        {
            get { return _deliveryCustomersListId; }
            set { _deliveryCustomersListId = value; }
        }
        public ListId DeliveryQuestionsId
        {
            get { return _deliveryQuestionsId; }
            set { _deliveryQuestionsId = value; }
        }
        public bool DriverCanCreateLegs
        {
            get { return _driverCanCreateLegs; }
            set { _driverCanCreateLegs = value; }
        }
        public ListId EmptyItemsListId
        {
            get { return _emptyItemsListId; }
            set { _emptyItemsListId = value; }
        }
        public ListId OutOfSequenceListId
        {
            get { return _outOfSequenceListId; }
            set { _outOfSequenceListId = value; }
        }
        public ListId RejectReasonListId
        {
            get { return _rejectReasonListId; }
            set { _rejectReasonListId = value; }
        }
        public ListId EmptyItemsQuestionListId
        {
            get { return _emptyItemsQuestionListId; }
            set { _emptyItemsQuestionListId = value; }
        }

        public bool DriverAdhocItems
        {
            get { return _driverAdhocItems; }
            set { _driverAdhocItems = value; }
        }

        /// <summary>
        /// is adding tracking asset actions enabled
        /// </summary>
        public bool TrackableAssetLegActionsEnabled
        {
            get { return _trackableAssetLegActionsEnabled; }
            set { _trackableAssetLegActionsEnabled = value; }
        }
        /// <summary>
         /// is the pallet transfer module used
         /// </summary>
        public bool PalletTransferUsed
        {
            get { return _palletTransferUsed; }
            set { _palletTransferUsed = value; }
        }
        /// <summary>
        /// If Pallet Transfer is enabled then the List Id of the customer list
        /// </summary>
        public ListId PalletTransferCustomerListId
        {
            get { return _palletTransferCustomerListId; }
            set { _palletTransferCustomerListId = value; }
        }
        /// <summary>
        /// If Pallet Transfer is enabled then List Id of the list that contains the client
        /// </summary>
        public int PalletTransferClientListId
        {
            get { return _palletTransferClientListId; }
            set { _palletTransferClientListId = value; }
        }
        /// <summary>
        /// the unique of the client used within the pallet transfer module
        /// </summary>
        public int PalletTransferClientUniqueKey
        {
            get { return _palletTransferClientUniqueKey; }
            set { _palletTransferClientUniqueKey = value; }
        }
        /// <summary>
        /// If Pallet Transfer is enabled then List Id of the vendors (items)
        /// </summary>
        public ListId PalletTransferVendorsListId
        {
            get { return _palletTransferVendorsListId; }
            set { _palletTransferVendorsListId = value; }
        }
        /// <summary>
        /// If Pallet Transfer is enabled then List Id of the accounts used by the client
        /// </summary>
        public ListId PalletTransferAccountIdsListId
        {
            get { return _palletTransferAccountIdsListId; }
            set { _palletTransferAccountIdsListId = value; }
        }

        public bool InspectAssetOnPickup
        {
            get { return _inspectAssetOnPickup; }
            set { _inspectAssetOnPickup = value; }
        }
        public bool InspectAssetOnDrop
        {
            get { return _inspectAssetOnDrop; }
            set { _inspectAssetOnDrop = value; }
        }
        /// <summary>
        /// is the pallet transfer module required
        /// </summary>
        public bool PalletTransferRequired
        {
            get { return _palletTransferRequired; }
            set { _palletTransferRequired = value; }
        }
        #endregion

        #region Constructor
        public PalletsJob()
        {
            JobType = JobTypes.Pallets;
            _loadType = string.Empty;
            _trailerIds = new List<int>();
            _itemsListId = new ListId();
            _pickupCustomersListId = new ListId();
            _pickupQuestionsId = new ListId();
            _deliveryCustomersListId = new ListId();
            _deliveryQuestionsId = new ListId();
            _emptyItemsListId = new ListId();
            _outOfSequenceListId = new ListId();
            _rejectReasonListId = new ListId();
            _emptyItemsQuestionListId = new ListId();
            _palletTransferCustomerListId = new ListId();
            _palletTransferVendorsListId = new ListId();
            _palletTransferAccountIdsListId = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _loadType = BaseLayer.ReadString(data, ref index, 0x12);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _trailerIds.Clear();
            for (int i = 0; i < count; i++)
            {
                _trailerIds.Add(BaseLayer.Read4ByteInteger(data, ref index));
            }
            _createRejectItemsLeg = BaseLayer.ReadBool(data, ref index);
            _showTimes = false;
            if (versionNumber >= 2)
            {
                _showTimes = BaseLayer.ReadBool(data, ref index);
            }
            AutoComplete = true;
            _itemsListId = new ListId(-1, -1);
            _pickupCustomersListId = new ListId(-1, -1);
            _pickupQuestionsId = new ListId(-1, -1);
            _deliveryCustomersListId = new ListId(-1, -1);
            _deliveryQuestionsId = new ListId(-1, -1);
            _driverCanCreateLegs = false;
            _emptyItemsListId = new ListId(-1, -1);
            _outOfSequenceListId = new ListId(-1, -1);
            _rejectReasonListId = new ListId(-1, -1);
            _emptyItemsQuestionListId = new ListId(-1, -1);
            _palletTransferCustomerListId = new ListId(-1, -1);
            _palletTransferVendorsListId = new ListId(-1, -1);
            _palletTransferAccountIdsListId = new ListId(-1, -1);
            if (versionNumber >= 3)
            {
                AutoComplete = BaseLayer.ReadBool(data, ref index);
                _itemsListId.Decode(data, ref index);
                _pickupCustomersListId.Decode(data, ref index);
                _pickupQuestionsId.Decode(data, ref index);
                _deliveryCustomersListId.Decode(data, ref index);
                _deliveryQuestionsId.Decode(data, ref index);
                _driverCanCreateLegs = BaseLayer.ReadBool(data, ref index);
            }
            if (versionNumber >= 4)
            {
                _emptyItemsListId.Decode(data, ref index);
                _outOfSequenceListId.Decode(data, ref index);
                _rejectReasonListId.Decode(data, ref index);
            }
            if (versionNumber >= 5)
            {
                _emptyItemsQuestionListId.Decode(data, ref index);
            }

            if (versionNumber >= 6)
            {
                _driverAdhocItems = BaseLayer.ReadBool(data, ref index);
            }
            if (versionNumber >= 7)
            {
                _trackableAssetLegActionsEnabled = BaseLayer.ReadBool(data, ref index);
                _palletTransferUsed = BaseLayer.ReadBool(data, ref index);
                _palletTransferCustomerListId.Decode(data, ref index);
                _palletTransferClientListId = BaseLayer.Read4ByteInteger(data, ref index);
                _palletTransferClientUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
                _palletTransferVendorsListId.Decode(data, ref index);
                _palletTransferAccountIdsListId.Decode(data, ref index);
            }
            if (versionNumber >= 8)
            {
                _inspectAssetOnPickup = BaseLayer.ReadBool(data, ref index);
                _inspectAssetOnDrop = BaseLayer.ReadBool(data, ref index);
            }
            if (versionNumber >= 9)
            {
                _palletTransferRequired = BaseLayer.ReadBool(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _loadType, 0x12);
                BaseLayer.Write2ByteInteger(local, _trailerIds.Count);
                foreach (int t in _trailerIds)
                {
                    BaseLayer.Write4ByteInteger(local, t);
                }
                BaseLayer.WriteBool(local, _createRejectItemsLeg);
                BaseLayer.WriteBool(local, _showTimes);
                BaseLayer.WriteBool(local, _autoComplete);
                _itemsListId.Encode(local);
                _pickupCustomersListId.Encode(local);
                _pickupQuestionsId.Encode(local);
                _deliveryCustomersListId.Encode(local);
                _deliveryQuestionsId.Encode(local);
                BaseLayer.WriteBool(local, _driverCanCreateLegs);
                _emptyItemsListId.Encode(local);
                _outOfSequenceListId.Encode(local);
                _rejectReasonListId.Encode(local);
                _emptyItemsQuestionListId.Encode(local);
                BaseLayer.WriteBool(local, _driverAdhocItems);
                BaseLayer.WriteBool(local, _trackableAssetLegActionsEnabled);
                BaseLayer.WriteBool(local, _palletTransferUsed);
                _palletTransferCustomerListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _palletTransferClientListId);
                BaseLayer.Write4ByteInteger(local, _palletTransferClientUniqueKey);
                _palletTransferVendorsListId.Encode(local);
                _palletTransferAccountIdsListId.Encode(local);
                BaseLayer.WriteBool(local, _inspectAssetOnPickup);
                BaseLayer.WriteBool(local, _inspectAssetOnDrop);
                BaseLayer.WriteBool(local, _palletTransferRequired);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    public class PalletsDeliveryCompletedAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;

        private bool _isReturnRejectedItemsLeg;
        private ListId _rejectListId;
        private int _rejectReasonId;
        private ListId _divisionListId;
        private int _divisionId;
        private ListId _palletListId;
        private string _exchangedPalletsOrderNumber;
        private List<PalletLoad> _pallets;
        private List<PalletLoad> _rejectedPallets;
        private List<PalletLoad> _exchangedPallets;
        private List<PalletLoad> _returnedPallets;
        private AnsweredQuestions _rejectedAnswers;
        private AnsweredQuestions _emptyAnswers;
        #endregion

        #region public properties
        public bool IsReturnRejectedItemsLeg
        {
            get { return _isReturnRejectedItemsLeg; }
            set { _isReturnRejectedItemsLeg = value; }
        }
        public ListId RejectListId
        {
            get { return _rejectListId; }
            set { _rejectListId = value; }
        }
        public int RejectReasonId
        {
            get { return _rejectReasonId; }
            set { _rejectReasonId = value; }
        }
        public ListId DivisionListId
        {
            get { return _divisionListId; }
            set { _divisionListId = value; }
        }
        public int DivisionId
        {
            get { return _divisionId; }
            set { _divisionId = value; }
        }
        public ListId PalletListId
        {
            get { return _palletListId; }
            set { _palletListId = value; }
        }
        public List<PalletLoad> Pallets
        {
            get { return _pallets; }
            set { _pallets = value; }
        }
        public List<PalletLoad> RejectedPallets
        {
            get { return _rejectedPallets; }
            set { _rejectedPallets = value; }
        }
        public List<PalletLoad> ExchangedPallets
        {
            get { return _exchangedPallets; }
            set { _exchangedPallets = value; }
        }
        public List<PalletLoad> ReturnedPallets
        {
            get { return _returnedPallets; }
            set { _returnedPallets = value; }
        }
        public string ExchangedPalletsOrderNumber
        {
            get { return _exchangedPalletsOrderNumber; }
            set { _exchangedPalletsOrderNumber = value; }
        }
        public AnsweredQuestions RejectedAnswers
        {
            get { return _rejectedAnswers; }
            set { _rejectedAnswers = value; }
        }
        public AnsweredQuestions EmptyAnswers
        {
            get { return _emptyAnswers; }
            set { _emptyAnswers = value; }
        }
        #endregion

        #region constructor
        public PalletsDeliveryCompletedAction()
        {
            LegActionType = LegActionTypes.PalletDelivery;
            _rejectListId = new ListId();
            _divisionListId = new ListId();
            _palletListId = new ListId();
            _pallets = new List<PalletLoad>();
            _rejectedPallets = new List<PalletLoad>();
            _exchangedPallets = new List<PalletLoad>();
            _returnedPallets = new List<PalletLoad>();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            if (data[index++] == 0x01)
            {
                _isReturnRejectedItemsLeg = true;
            }
            else
            {
                _isReturnRejectedItemsLeg = false;
            }
            _rejectListId.Decode(data, ref index);
            _rejectReasonId = BaseLayer.Read4ByteInteger(data, ref index);
            _divisionListId.Decode(data, ref index);
            _divisionId = BaseLayer.Read4ByteInteger(data, ref index);
            _exchangedPalletsOrderNumber = BaseLayer.ReadString(data, ref index, 0x12);

            _palletListId.Decode(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _pallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad pallet = new PalletLoad();
                pallet.Decode(data, ref index);
                _pallets.Add(pallet);
            }

            count = BaseLayer.Read2ByteInteger(data, ref index);
            _rejectedPallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad pallet = new PalletLoad();
                pallet.Decode(data, ref index);
                _rejectedPallets.Add(pallet);
            }
            count = BaseLayer.Read2ByteInteger(data, ref index);
            _exchangedPallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad pallet = new PalletLoad();
                pallet.Decode(data, ref index);
                _exchangedPallets.Add(pallet);
            }
            count = BaseLayer.Read2ByteInteger(data, ref index);
            _returnedPallets.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad pallet = new PalletLoad();
                pallet.Decode(data, ref index);
                _returnedPallets.Add(pallet);
            }
            _rejectedAnswers = null;
            if (versionNumber >= 2)
            {
                bool rejected = BaseLayer.ReadBool(data, ref index);
                if (rejected)
                {
                    _rejectedAnswers = new AnsweredQuestions();
                    _rejectedAnswers.Decode(data, ref index);
                }
            }
            _emptyAnswers = null;
            if (versionNumber >= 3)
            {
                bool empty = BaseLayer.ReadBool(data, ref index);
                if (empty)
                {
                    _emptyAnswers = new AnsweredQuestions();
                    _emptyAnswers.Decode(data, ref index);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                if (_isReturnRejectedItemsLeg)
                {
                    local.WriteByte(0x01);
                }
                else
                {
                    local.WriteByte(0x00);
                }
                _rejectListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _rejectReasonId);
                _divisionListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _divisionId);
                BaseLayer.WriteString(local, _exchangedPalletsOrderNumber, 0x12);

                _palletListId.Encode(local);

                BaseLayer.Write2ByteInteger(local, _pallets.Count);
                foreach (PalletLoad p in _pallets)
                {
                    p.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _rejectedPallets.Count);
                foreach (PalletLoad p in _rejectedPallets)
                {
                    p.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _exchangedPallets.Count);
                foreach (PalletLoad p in _exchangedPallets)
                {
                    p.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _returnedPallets.Count);
                foreach (PalletLoad p in _returnedPallets)
                {
                    p.Encode(local);
                }

                if (_rejectedAnswers != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _rejectedAnswers.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }
                if (_emptyAnswers != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _emptyAnswers.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    public class PalletsQuestionCompletedAction : QuestionCompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private ListId _delayListId;
        private int _delayReasonId;
        private bool _hasDocumentation;
        private string _pod;
        #endregion
        
        #region properties
        public ListId DelayListId
        {
            get { return _delayListId; }
            set { _delayListId = value; }
        }
        public int DelayReasonId
        {
            get { return _delayReasonId; }
            set { _delayReasonId = value; }
        }
        public bool HasDocumentation
        {
            get { return _hasDocumentation; }
            set { _hasDocumentation = value; }
        }
        public string Pod
        {
            get { return _pod; }
            set { _pod = value; }
        }
        #endregion

        #region constructor
        public PalletsQuestionCompletedAction()
            : base()
        {
            _delayListId = new ListId();
            LegActionType = LegActionTypes.PalletQuestions;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _delayListId.Decode(data, ref index);
            _delayReasonId = BaseLayer.Read4ByteInteger(data, ref index);
            _hasDocumentation = BaseLayer.ReadBool(data, ref index);
            _pod = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _delayListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _delayReasonId);
                BaseLayer.WriteBool(local, _hasDocumentation);
                BaseLayer.WriteString(local, _pod, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets
{
    public enum PalletTransferTypes
    {
        None = -1,
        TransferOn,
        TransferOff,
        ExchangeOff,
        ExchangeOn,
        Direct,
        Dehire,
        Issue
    }

    public class PalletTransferCompletedAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private string _docketNumber;
        private PalletTransferTypes _transferType;
        private int _senderListId;
        private int _senderUniqueKey;
        private string _senderAccount;
        private int _receiverListId;
        private int _receiverUniqueKey;
        private string _receiverAccount;
        private List<PalletTransferBookingRef> _bookingRefs;
        #endregion

        #region public properties
        public string DocketNumber
        {
            get { return _docketNumber; }
            set { _docketNumber = value; }
        }
        public PalletTransferTypes TransferType
        {
            get { return _transferType; }
            set { _transferType = value; }
        }
        public int SenderListId
        {
            get { return _senderListId; }
            set { _senderListId = value; }
        }
        public int SenderUniqueKey
        {
            get { return _senderUniqueKey; }
            set { _senderUniqueKey = value; }
        }
        public string SenderAccount
        {
            get { return _senderAccount; }
            set { _senderAccount = value; }
        }
        public int ReceiverListId
        {
            get { return _receiverListId; }
            set { _receiverListId = value; }
        }
        public int ReceiverUniqueKey
        {
            get { return _receiverUniqueKey; }
            set { _receiverUniqueKey = value; }
        }
        public string ReceiverAccount
        {
            get { return _receiverAccount; }
            set { _receiverAccount = value; }
        }
        public List<PalletTransferBookingRef> BookingRefs
        {
            get { return _bookingRefs; }
            set { _bookingRefs = value; }
        }
        #endregion

        #region constructor
        public PalletTransferCompletedAction()
        {
            LegActionType = LegActionTypes.PalletTransfer;
            _bookingRefs = new List<PalletTransferBookingRef>();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _docketNumber = BaseLayer.ReadString(data, ref index, 0x12);
            _transferType = (PalletTransferTypes)data[index++];
            _senderListId = BaseLayer.Read4ByteInteger(data, ref index);
            _senderUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            _receiverListId = BaseLayer.Read4ByteInteger(data, ref index);
            _receiverUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            _senderAccount = BaseLayer.ReadString(data, ref index, 0x12);
            _receiverAccount = BaseLayer.ReadString(data, ref index, 0x12);
            int count = (int)data[index++];
            _bookingRefs = new List<PalletTransferBookingRef>();
            for (int i = 0; i < count; i++)
            {
                PalletTransferBookingRef p = new PalletTransferBookingRef();
                p.Decode(data, ref index);
                _bookingRefs.Add(p);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _docketNumber, 0x12);
                local.WriteByte((byte)_transferType);
                BaseLayer.Write4ByteInteger(local, _senderListId);
                BaseLayer.Write4ByteInteger(local, _senderUniqueKey);
                BaseLayer.Write4ByteInteger(local, _receiverListId);
                BaseLayer.Write4ByteInteger(local, _receiverUniqueKey);
                BaseLayer.WriteString(local, _senderAccount, 0x12);
                BaseLayer.WriteString(local, _receiverAccount, 0x12);
                local.WriteByte((byte)_bookingRefs.Count);
                foreach (PalletTransferBookingRef p in _bookingRefs)
                {
                    p.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

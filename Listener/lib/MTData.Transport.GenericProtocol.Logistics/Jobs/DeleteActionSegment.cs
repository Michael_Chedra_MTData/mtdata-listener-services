using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by base to delete an action from a leg
    /// </summary>
    public class DeleteActionSegment : SegmentLayer
    {
        #region private fields
        private int _allocationId;
        private int _dispatchId;
        private int _legId;
        private LegTypes _legType;
        private int _actionId;
        #endregion

        #region properties
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegTypes LegType
        {
            get { return _legType; }
            set { _legType = value; }
        }
        public int ActionId
        {
            get { return _actionId; }
            set { _actionId = value; }
        }
        #endregion

        #region constructor
        public DeleteActionSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.DeleteAction;
        }
        public DeleteActionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.DeleteAction)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.DeleteAction, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.DeleteAction;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                Write4ByteInteger(stream, _dispatchId);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legType);
                Write4ByteInteger(stream, _actionId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
            _dispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _legType = (LegTypes)Data[index++];
            _actionId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

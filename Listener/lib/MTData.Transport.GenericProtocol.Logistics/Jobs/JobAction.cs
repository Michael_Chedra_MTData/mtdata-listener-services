using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class JobAction : IJobAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private JobActionTypes _jobActionType;
        private int _dispatchId;
        private int _jobActionId;
        private int _jobActionNumber;
        private string _title;
        private string _notes;
        private JobActionStatus _status;
        private string _externalReference;
        #endregion

        #region Constructor
        public JobAction()
        {
            _status = JobActionStatus.NotStarted;
            _title = string.Empty;
            _notes = string.Empty;
            _externalReference = string.Empty;
        }
        #endregion

        #region IJobAction Members

        public JobActionTypes JobActionType
        {
            get { return _jobActionType; }
            set { _jobActionType = value; }
        }

        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }

        public int JobActionId
        {
            get { return _jobActionId; }
            set { _jobActionId = value; }
        }

        public int JobActionNumber
        {
            get { return _jobActionNumber; }
            set { _jobActionNumber = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public JobActionStatus Status
        {
            get { return _status; }
            set 
            {
                if (_status != value)
                {
                    _status = value;
                    if (OnJobActionStatusChanged != null)
                    {
                        OnJobActionStatusChanged(this);
                    }
                }
            }
        }

        public string ExternalReference 
        {
            get { return _externalReference; }
            set { _externalReference = value; }
        }

        public event JobActionEvent OnJobActionStatusChanged;
        #endregion

        #region IPacketEncode Members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _jobActionType = (JobActionTypes)data[index++];
            _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
            _jobActionId = BaseLayer.Read4ByteInteger(data, ref index);
            _jobActionNumber = BaseLayer.Read4ByteInteger(data, ref index);
            _title = BaseLayer.ReadString(data, ref index, 0x12);
            _notes = BaseLayer.ReadString(data, ref index, 0x12);
            _status = (JobActionStatus)data[index++];
            _externalReference = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_jobActionType);
                BaseLayer.Write4ByteInteger(local, _dispatchId);
                BaseLayer.Write4ByteInteger(local, _jobActionId);
                BaseLayer.Write4ByteInteger(local, _jobActionNumber);
                BaseLayer.WriteString(local, _title, 0x12);
                BaseLayer.WriteString(local, _notes, 0x12);
                local.WriteByte((byte)_status);
                BaseLayer.WriteString(local, _externalReference, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
   }
}

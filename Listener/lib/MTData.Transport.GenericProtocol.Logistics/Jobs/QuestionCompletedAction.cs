using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class QuestionCompletedAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private AnsweredQuestions _answeredQuestions;
        #endregion
        
        #region properties
        public AnsweredQuestions AnsweredQuestions
        {
            get { return _answeredQuestions; }
            set { _answeredQuestions = value; }
        }
        #endregion

        #region constructor
        public QuestionCompletedAction()
            : this(0, new AnsweredQuestions(), LegActionTypes.Questions)
        {
        }
        public QuestionCompletedAction(int actionId, AnsweredQuestions answers)
            : this(actionId, answers, LegActionTypes.Questions)
        {
        }
        public QuestionCompletedAction(LegActionTypes actionType)
            : this(0, new AnsweredQuestions(), actionType)
        {
        }
        public QuestionCompletedAction(int actionId, AnsweredQuestions answers, LegActionTypes actionType)
            : base(actionType, actionId)
        {
            _answeredQuestions = answers;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _answeredQuestions.Decode(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _answeredQuestions.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

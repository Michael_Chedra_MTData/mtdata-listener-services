using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// leg types
    /// </summary>
    public enum LegTypes
    {
        Simple = 0,
        PalletPickup = 1,
        PalletDelivery = 2,
        Service = 3,
        PassengerStop = 4,
        Stop = 5,
        ContainerPickup = 6,
        ContainerWait = 7,
        ContainerDelivery = 8
    }

    /// <summary>
    /// leg states
    /// </summary>
    public enum LegStatus
    {
        NotArrived = 0,
        OnApproach = 1,
        AtSite = 2,
        InProgress = 3,
        Complete = 4,
        CompleteWithChanges = 5,
        LeftSite = 6
    }

    /// <summary>
    /// delegate for leg events
    /// </summary>
    /// <param name="leg"></param>
    public delegate void LegEvent(ILeg leg);

    /// <summary>
    /// interface for a leg
    /// </summary>
    public interface ILeg : IEnumerable, IPacketEncode, IComparable<ILeg>
    {
        /// <summary>
        /// the Leg type
        /// </summary>
        LegTypes LegType { get; }

        /// <summary>
        /// The dispatch id of the job the leg belongs to
        /// </summary>
        int DispatchId { get; }
        
        /// <summary>
        /// The leg id
        /// </summary>
        int LegId { get; }

        /// <summary>
        /// the leg number (used for sequencing)
        /// </summary>
        int LegNumber { get; }

        /// <summary>
        /// the leg title
        /// </summary>
        string Title { get; }

        /// <summary>
        /// the leg location
        /// </summary>
        ILegLocation Location { get; }

        /// <summary>
        /// the notes assoicated with the leg, can be null
        /// </summary>
        string Notes { get; }

        /// <summary>
        /// the leg status
        /// </summary>
        LegStatus Status { get; set;}

        /// <summary>
        /// the time information for a leg
        /// </summary>
        ILegTime Time { get; }

        /// <summary>
        /// a count of the number of actions on this leg
        /// </summary>
        int ActionsCount { get; }
        /// <summary>
        /// get the action at index
        /// </summary>
        /// <param name="index">the index of the leg action</param>
        /// <returns></returns>
        ILegAction this[int index] { get; }

        /// <summary>
        /// a list of the completed actions for this leg
        /// </summary>
        List<ICompetedAction> CompletedActions { get; }
        
        /// <summary>
        /// a list of leg ids of legs that must be completed before this leg can be stared
        /// </summary>
        List<int> PreviousCompletedLegs { get; }

        /// <summary>
        /// get an external reference for the leg
        /// </summary>
        string ExternalReference { get; }

        /// <summary>
        /// the leg order number
        /// </summary>
        string OrderNumber { get; }


        /// <summary>
        /// event for when status of the leg has changed
        /// </summary>
        event LegEvent OnLegStatusChanged;

        /// <summary>
        /// add a new leg action to leg
        /// </summary>
        /// <param name="legAction"></param>
        void AddLegAction(ILegAction legAction);
        /// <summary>
        /// delete a leg action 
        /// </summary>
        /// <param name="legActionId"></param>
        void DeleteLegAction(int legActionId);
        /// <summary>
        /// get the leg action with the given leg action id
        /// </summary>
        /// <param name="legActionId"></param>
        ILegAction GetLegActionWithId(int legActionId);
        /// <summary>
        /// get the completed leg action with the given leg action id
        /// </summary>
        /// <param name="legActionId"></param>
        ICompetedAction GetCompletedLegActionWithId(int legActionId);

        /// <summary>
        /// returns true if the leg status is either Complete, CompleteWithErrors or LeftSite
        /// </summary>
        bool IsLegComplete { get; }
    }
}

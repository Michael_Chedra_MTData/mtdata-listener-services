using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// packet sent by unit when an job leg email alert is activated
    /// </summary>
    public class JobAlertSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private LegTypes _legType;
        private EMailFlag _emailFlag;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegTypes LegType
        {
            get { return _legType; }
            set { _legType = value; }
        }
        public EMailFlag EmailFlag
        {
            get { return _emailFlag; }
            set { _emailFlag = value; }
        }
        #endregion

        #region constructor
        public JobAlertSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobEmailAlert;
        }
        public JobAlertSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobEmailAlert)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobEmailAlert, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobEmailAlert;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legType);
                stream.WriteByte((byte)_emailFlag);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _legType = (LegTypes)Data[index++];
            _emailFlag = (EMailFlag)Data[index++];
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class UpdateLegInfoSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private string _title;
        private string _address;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        #endregion

        #region constructor
        public UpdateLegInfoSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.UpdateLegInfo;
        }
        public UpdateLegInfoSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.UpdateLegInfo)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.UpdateLegInfo, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.UpdateLegInfo;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                WriteString(stream, _title, 0x12);
                WriteString(stream, _address, 0x12);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _title = ReadString(Data, ref index, 0x12);
            _address = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class Leg : ILeg
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private LegTypes _legType;
        private int _dispatchId;
        private int _legId;
        private int _legNumber;
        private string _title;
        private ILegLocation _location;
        private string _notes;
        private LegStatus _status;
        private ILegTime _time;
        private List<ILegAction> _actions;
        private List<ICompetedAction> _completedActions;
        private List<int> _previousCompletedLegs;
        private string _externalReference;
        private string _orderNumber;

        /// <summary>
        /// complete the leg once all actions are complete
        /// </summary>
        private bool _completeLegWhenAllActionsAreComplete;
        #endregion

        #region properties
        /// <summary>
        /// complete the leg once all actions are complete
        /// </summary>
        protected bool CompleteLegWhenAllActionsAreComplete
        {
            get { return _completeLegWhenAllActionsAreComplete;}
            set { _completeLegWhenAllActionsAreComplete = value; }
        }
        #endregion

        #region Constructor
        public Leg()
        {
            _status = LegStatus.NotArrived;
            _actions = new List<ILegAction>();
            _completedActions = new List<ICompetedAction>();
            _title = string.Empty;
            _notes = string.Empty;
            _location = new LegLocation();
            _time = new LegTime();
            _legType = LegTypes.Simple;
            _previousCompletedLegs = new List<int>();
            _externalReference = string.Empty;
            _completeLegWhenAllActionsAreComplete = true;
        }
        #endregion

        #region ILeg Members

        public LegTypes LegType
        {
            get { return _legType; }
            set { _legType = value; }
        }

        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }

        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }

        public int LegNumber
        {
            get { return _legNumber; }
            set { _legNumber = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public ILegLocation Location
        {
            get { return _location; }
            set { _location = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public LegStatus Status
        {
            get { return _status; }
            set
            {
                if (_status != value)
                {
                    _status = value;
                    if (OnLegStatusChanged != null)
                    {
                        OnLegStatusChanged(this);
                    }
                }
            }
        }

        public ILegTime Time
        {
            get { return _time; }
            set { _time = value; }
        }

        public List<ICompetedAction> CompletedActions
        {
            get { return _completedActions; }
        }

        public List<int> PreviousCompletedLegs { get { return _previousCompletedLegs; } }

        public int ActionsCount
        {
            get { return _actions.Count; }
        }

        public ILegAction this[int index]
        {
            get 
            {
                if (index >= 0 && index < _actions.Count)
                {
                    return _actions[index];
                }
                throw new Exception(string.Format("Unknown leg action with index {0}, actions list has only {1} items.", index, _actions.Count)); 
            }
        }

        public event LegEvent OnLegStatusChanged;

        public void AddLegAction(ILegAction legAction)
        {
            if (_legId == legAction.LegId)
            {
                legAction.OnLegActionStatusChanged += new LegActionEvent(legAction_OnLegActionStatusChanged);
                _actions.Add(legAction);
            }
            else
            {
                throw new Exception("Can't add action to leg, they have different leg ids");
            }
        }

        public void DeleteLegAction(int legActionId)
        {
            ILegAction a = GetLegActionWithId(legActionId);
            if (a != null)
            {
                a.OnLegActionStatusChanged -= new LegActionEvent(legAction_OnLegActionStatusChanged);
            }
            CheckLegStatus();
        }

        public ILegAction GetLegActionWithId(int legActionId)
        {
            foreach (ILegAction a in _actions)
            {
                if (a.LegActionId == legActionId)
                {
                    return a;
                }
            }
            return null;
        }

        public ICompetedAction GetCompletedLegActionWithId(int legActionId)
        {
            foreach (ICompetedAction a in _completedActions)
            {
                if (a.LegActionId == legActionId)
                {
                    return a;
                }
            }
            return null;
        }

        public bool IsLegComplete 
        { 
            get 
            { 
                switch (_status)
                {
                    case LegStatus.Complete:
                    case LegStatus.CompleteWithChanges:
                    case LegStatus.LeftSite:
                        return true;
                }
                return false;
            }
        }

        public string ExternalReference
        {
            get { return _externalReference; }
            set { _externalReference = value; }
        }

        public string OrderNumber
        {
            get { return _orderNumber; }
            set { _orderNumber = value; }
        }
        #endregion

        #region IEnumerable Members

        public System.Collections.IEnumerator GetEnumerator()
        {
            return _actions.GetEnumerator();
        }

        #endregion

        #region IPacketEncode Members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _legType = (LegTypes)data[index++];
            _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
            _legId = BaseLayer.Read4ByteInteger(data, ref index);
            _legNumber = BaseLayer.Read4ByteInteger(data, ref index);
            _title = BaseLayer.ReadString(data, ref index, 0x12);
            _location.Decode(data, ref index);
            _notes = BaseLayer.ReadString(data, ref index, 0x12);
            _status = (LegStatus)data[index++];
            _time.Decode(data, ref index);
            _externalReference = BaseLayer.ReadString(data, ref index, 0x12);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _actions.Clear();
            for (int i = 0; i < count; i++)
            {
                LegActionTypes t = (LegActionTypes)data[index++];
                ILegAction action = null;
                switch (t)
                {
                    case LegActionTypes.PalletPickup:
                        action = new Pallets.PalletsPickupAction();
                        break;
                    case LegActionTypes.PalletDelivery:
                        action = new Pallets.PalletsDeliveryAction();
                        break;
                    case LegActionTypes.PalletQuestions:
                        action = new Pallets.PalletsQuestionAction();
                        break;
                    case LegActionTypes.Questions:
                    case LegActionTypes.OutOfSequence:
                        action = new QuestionAction(t);
                        break;
                    case LegActionTypes.BinPickup:
                        action = new Bin.BinPickupAction();
                        break;
                    case LegActionTypes.Recovery:
                        action = new Recovery.RecoveryLegAction();
                        break;
                    case LegActionTypes.PassengerStop:
                        action = new Passenger.PassengerStopLegAction();
                        break;
                    case LegActionTypes.Coupling:
                    case LegActionTypes.Decoupling:
                    case LegActionTypes.BinPickupDrop:
                    case LegActionTypes.BinTransferStation:
                    case LegActionTypes.TagLocation:
                    case LegActionTypes.Mass:
                    case LegActionTypes.Photo: 
                    case LegActionTypes.Ticket:
                    case LegActionTypes.PalletTransfer:
                        action = new LegAction();
                        ((LegAction)action).LegActionType = t;
                        break;
                    case LegActionTypes.Pickup:
                    case LegActionTypes.Deliver:
                        action = new Delivery.ItemsLegAction();
                        ((LegAction)action).LegActionType = t;
                        break;
                    case LegActionTypes.AddTrailer:
                    case LegActionTypes.DropTrailer:
                        action = new Trailer.TrailerLegAction();
                        ((LegAction)action).LegActionType = t;
                        break;
                    case LegActionTypes.Service:
                        action = new Service.ServiceLegAction();
                        break;
                    case LegActionTypes.Alert:
                        action = new Container.AlertLegAction();
                        break;
                    case LegActionTypes.Container:
                        action = new Container.ContainerLegAction();
                        break;
                    case LegActionTypes.ConcreteLoad:
                        action = new Common.ProductLegAction();
                        break;
                    case LegActionTypes.ConcretePour:
                        action = new Common.ProductLegAction();
                        break;
                    case LegActionTypes.AddAsset:
                    case LegActionTypes.DropAsset:
                        action = new Delivery.TrackableAssetLegAction();
                        ((LegAction)action).LegActionType = t;
                        break;
                }
                if (action == null)
                {
                    throw new Exception(string.Format("Unknown action type {0}", t));
                }
                action.Decode(data, ref index);
                AddLegAction(action);
            }

            count = BaseLayer.Read2ByteInteger(data, ref index);
            _completedActions.Clear();
            for (int i = 0; i < count; i++)
            {
                LegActionTypes t = (LegActionTypes)data[index++];
                ICompetedAction completedAction = null;
                switch (t)
                {
                    case LegActionTypes.PalletPickup:
                        completedAction = new Pallets.PalletsPickupCompletedAction();
                        break;
                    case LegActionTypes.PalletDelivery:
                        completedAction = new Pallets.PalletsDeliveryCompletedAction();
                        break;
                    case LegActionTypes.PalletQuestions:
                        completedAction = new Pallets.PalletsQuestionCompletedAction();
                        break;
                    case LegActionTypes.Questions:
                    case LegActionTypes.OutOfSequence:
                        completedAction = new QuestionCompletedAction(t);
                        break;
                    case LegActionTypes.BinPickup:
                        completedAction = new Bin.BinPickupCompletedAction();
                        break;
                    case LegActionTypes.BinPickupDrop:
                        completedAction = new Bin.BinDropCompletedAction();
                        break;
                    case LegActionTypes.BinTransferStation:
                        completedAction = new Bin.BinTransferStationCompletedAction();
                        break;
                    case LegActionTypes.TagLocation:
                        completedAction = new TagLocationCompletedAction();
                        break;
                    case LegActionTypes.Coupling:
                    case LegActionTypes.Decoupling:
                    case LegActionTypes.AddTrailer:
                    case LegActionTypes.DropTrailer:
                    case LegActionTypes.Alert:
                    case LegActionTypes.Container:
                        completedAction = new CompletedAction();
                        ((CompletedAction)completedAction).LegActionType = t;
                        break;
                    case LegActionTypes.Recovery:
                        completedAction = new Recovery.RecoveryCompletedLegAction();
                        break;
                    case LegActionTypes.PassengerStop:
                        completedAction = new Passenger.PassengerStopCompletedLegAction();
                        break;
                    case LegActionTypes.Pickup:
                    case LegActionTypes.Deliver:
                        completedAction = new Delivery.ItemsCompletedLegAction();
                        ((CompletedAction)completedAction).LegActionType = t;
                        break;
                    case LegActionTypes.Service:
                        completedAction = new Service.ServiceCompletedLegAction();
                        break;
                    case LegActionTypes.Mass:
                        completedAction = new Delivery.MassCompletedLegAction();
                        break;
                    case LegActionTypes.Photo:
                        completedAction = new Delivery.PhotoCompletedLegAction();
                        break;
                    case LegActionTypes.Ticket:
                        completedAction = new Bus.TicketCompletedLegAction();
                        break;
                    case LegActionTypes.ConcreteLoad:
                    case LegActionTypes.ConcretePour:
                        completedAction = new Common.ProductsCompletedLegAction();
                        break;
                    case LegActionTypes.PalletTransfer:
                        completedAction = new Pallets.PalletTransferCompletedAction();
                        break;
                    case LegActionTypes.AddAsset:
                    case LegActionTypes.DropAsset:
                        completedAction = new Delivery.TrackableAssetCompletedLegAction();
                        ((CompletedAction)completedAction).LegActionType = t;
                        break;
                }
                if (completedAction == null)
                {
                    throw new Exception(string.Format("Unknown completed action type {0}", t));
                }
                completedAction.Decode(data, ref index);
                _completedActions.Add(completedAction);
            }

            count = BaseLayer.Read2ByteInteger(data, ref index);
            _previousCompletedLegs.Clear();
            for (int i = 0; i < count; i++)
            {
                _previousCompletedLegs.Add(BaseLayer.Read4ByteInteger(data, ref index));
            }
            _orderNumber = string.Empty;
            if (versionNumber >= 2)
            {
                _orderNumber = BaseLayer.ReadString(data, ref index, 0x12);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_legType);
                BaseLayer.Write4ByteInteger(local, _dispatchId);
                BaseLayer.Write4ByteInteger(local, _legId);
                BaseLayer.Write4ByteInteger(local, _legNumber);
                BaseLayer.WriteString(local, _title, 0x12);
                _location.Encode(local);
                BaseLayer.WriteString(local, _notes, 0x12);
                local.WriteByte((byte)_status);
                _time.Encode(local);
                BaseLayer.WriteString(local, _externalReference, 0x12);
                BaseLayer.Write2ByteInteger(local, _actions.Count);
                foreach (ILegAction a in _actions)
                {
                    local.WriteByte((byte)a.LegActionType);
                    a.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _completedActions.Count);
                foreach (ICompetedAction c in _completedActions)
                {
                    local.WriteByte((byte)c.LegActionType);
                    c.Encode(local);
                }
                BaseLayer.Write2ByteInteger(local, _previousCompletedLegs.Count);
                foreach (int legId in _previousCompletedLegs)
                {
                    BaseLayer.Write4ByteInteger(local, legId);
                }
                BaseLayer.WriteString(local, _orderNumber, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }

        #endregion

        #region private methods
        private void legAction_OnLegActionStatusChanged(ILegAction legAction)
        {
            CheckLegStatus();
        }

        private void CheckLegStatus()
        {
            if (_status == LegStatus.AtSite)
            {
                foreach (ILegAction a in _actions)
                {
                    //ignore alert actions as the alert can be in progress and the leg still not started
                    if (a.Status == LegActionStatus.InProgress && a.LegActionType != LegActionTypes.Alert)
                    {
                        Status = LegStatus.InProgress;
                    }
                }
            }
            else if (_status == LegStatus.InProgress && _completeLegWhenAllActionsAreComplete)
            {
                //check if each action is complete
                bool errors = false;
                foreach (ILegAction a in _actions)
                {
                    switch (a.Status)
                    {
                        case LegActionStatus.Complete:
                            break;
                        case LegActionStatus.CompleteWithChanges:
                            errors = true;
                            break;
                        default:
                            //any other status return as not changing leg status
                            return;
                    }
                }
                //if we got here all actions are complete or complete with errors, update status
                if (errors)
                {
                    Status = LegStatus.CompleteWithChanges;
                }
                else
                {
                    Status = LegStatus.Complete;
                }
            }
        }
        #endregion

        #region IComparable<ILeg> Members

        public int CompareTo(ILeg other)
        {
            return _legNumber.CompareTo(other.LegNumber);
        }

        #endregion
    }
}

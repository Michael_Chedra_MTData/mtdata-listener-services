using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class TagLocationCompletedAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private ListId _tagLocationListId;
        private int _tagLocationId;
        private string _comments;
        #endregion

        #region public properties
        public ListId TagLocationListId
        {
            get { return _tagLocationListId; }
            set { _tagLocationListId = value; }
        }
        public int TagLocationId
        {
            get { return _tagLocationId; }
            set { _tagLocationId = value; }
        }
        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }
        #endregion

        #region constructor
        public TagLocationCompletedAction()
        {
            LegActionType = LegActionTypes.TagLocation;
            _comments = string.Empty;
            _tagLocationListId = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _tagLocationListId.Decode(data, ref index);
            _tagLocationId = BaseLayer.Read4ByteInteger(data, ref index);
            _comments = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _tagLocationListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _tagLocationId);
                BaseLayer.WriteString(local, _comments, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Delivery
{
    public class ItemsLegAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private List<PalletLoad> _items;
        private bool _skipStart;
        private bool _skipArrive;
        #endregion

        #region public properties
        public List<PalletLoad> Items
        {
            get { return _items; }
            set { _items = value; }
        }
        public bool SkipStart
        {
            get { return _skipStart; }
            set { _skipStart = value; }
        }
        public bool SkipArrive
        {
            get { return _skipArrive; }
            set { _skipArrive = value; }
        }

        #endregion

        #region constructor
        public ItemsLegAction()
        {
            _items = new List<PalletLoad>();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _items.Clear();
            for (int i = 0; i < count; i++)
            {
                PalletLoad p = new PalletLoad();
                p.Decode(data, ref index);
                _items.Add(p);
            }

            if (versionNumber >= 2)
            {
                _skipStart = BaseLayer.ReadBool(data, ref index);
                _skipArrive = BaseLayer.ReadBool(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write2ByteInteger(local, _items.Count);
                foreach (PalletLoad p in _items)
                {
                    p.Encode(local);
                }

                BaseLayer.WriteBool(local, _skipStart);
                BaseLayer.WriteBool(local, _skipArrive);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

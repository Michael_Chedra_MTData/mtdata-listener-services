using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.TrackableAsset;
using MTData.Transport.GenericProtocol.Logistics.DefectReport;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Delivery
{
    public class TrackableAssetCompletedLegAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private bool? _completedSuccessfully;
        private bool _driverCreated;
        private Inspection _inspection;
        private Association _association;
        #endregion

        #region public properties
        public bool? CompletedSuccessfully
        {
            get { return _completedSuccessfully; }
            set { _completedSuccessfully = value; }
        }
        public bool DriverCreated
        {
            get { return _driverCreated; }
            set { _driverCreated = value; }
        }
        public Inspection Inspection
        {
            get { return _inspection; }
            set { _inspection = value; }
        }
        public Association Association
        {
            get { return _association; }
            set { _association = value; }
        }
        #endregion

        #region constructor
        public TrackableAssetCompletedLegAction()
        {
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _completedSuccessfully = BaseLayer.ReadNullableBool(data, ref index);
            _driverCreated = BaseLayer.ReadBool(data, ref index);

            //read bool to indicate if inspection is present
            if (BaseLayer.ReadBool(data, ref index))
            {
                _inspection = new Inspection();
                _inspection.Decode(data, ref index);
            }
            else
            {
                _inspection = null;
            }
            if (_completedSuccessfully.HasValue && _completedSuccessfully.Value)
            {
                _association = new Association();
                _association.Decode(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteNullableBool(local, _completedSuccessfully);
                BaseLayer.WriteBool(local, _driverCreated);
                if (_inspection != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _inspection.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }
                if (_completedSuccessfully.HasValue && _completedSuccessfully.Value)
                {
                    var a = _association ?? new Association();
                    a.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

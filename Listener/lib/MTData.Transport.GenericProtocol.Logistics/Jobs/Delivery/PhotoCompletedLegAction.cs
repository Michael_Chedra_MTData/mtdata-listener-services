﻿using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Delivery
{
    public class PhotoCompletedLegAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private int _id;
        private byte[] _image;
        #endregion

        #region public properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public byte[] Image
        {
            get { return _image; }
            set { _image = value; }
        }
        #endregion

        #region constructor
        public PhotoCompletedLegAction()
        {
            LegActionType = LegActionTypes.Photo;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _id = BaseLayer.Read4ByteInteger(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _image = new byte[count];
            Array.Copy(data, index, _image, 0, count);
            index += count;

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write4ByteInteger(local, _id);
                if (_image != null)
                {
                    BaseLayer.Write2ByteInteger(local, _image.Length);
                    if (_image.Length > 0)
                    {
                        local.Write(_image, 0, _image.Length);
                    }
                }
                else
                {
                    BaseLayer.Write2ByteInteger(local, 0);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

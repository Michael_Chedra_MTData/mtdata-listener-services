using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Delivery
{
    public enum MassDeclarationRequired
    {
        No = 0,
        Optional = 1,
        OnPickupRequired = 2,
        OnDeliveryRequired = 3,
        RequiredAtEveryLeg = 4
    }

    /// <summary>
    /// class the represnets a delivery job
    /// </summary>
    public class DeliveryJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 7;
        /// <summary>
        /// is this job an auto complete job
        /// </summary>
        private bool _autoComplete;
        /// <summary>
        /// the list id/version of the pickup/deliver objects
        /// </summary>
        private ListId _itemsListId;
        /// <summary>
        /// the list id/version of the pickup customer list
        /// </summary>
        private ListId _pickupCustomersListId;
        /// <summary>
        /// the list id/version of the pickup question list
        /// </summary>
        private ListId _pickupQuestionsId;
        /// <summary>
        /// the list id/version of the pickup customer list
        /// </summary>
        private ListId _deliveryCustomersListId;
        /// <summary>
        /// the list id/version of the pickup question list
        /// </summary>
        private ListId _deliveryQuestionsId;
        private bool _driverCanCreateLegs;
        private bool _driverCanCreateActions;
        private bool _totalWeightRequired;
        private ListId _delayQuestionsId;
        private MassDeclarationRequired _massDeclarationRequired;
        private ListId _rejectQuestionsId;
        private bool _driverAdhocItems;
        private bool _trackableAssetLegActionsEnabled;
        private bool _inspectAssetOnPickup;
        private bool _inspectAssetOnDrop;
        #endregion

        #region public properties
        public bool AutoComplete
        {
            get { return _autoComplete; }
            set 
            {
                _autoComplete = value;
                CompleteJobWhenAllLegsAreComplete = _autoComplete;
            }
        }
        public ListId ItemsListId
        {
            get { return _itemsListId; }
            set { _itemsListId = value; }
        }
        public ListId PickupCustomersListId
        {
            get { return _pickupCustomersListId; }
            set { _pickupCustomersListId = value; }
        }
        public ListId PickupQuestionsId
        {
            get { return _pickupQuestionsId; }
            set { _pickupQuestionsId = value; }
        }
        public ListId DeliveryCustomersListId
        {
            get { return _deliveryCustomersListId; }
            set { _deliveryCustomersListId = value; }
        }
        public ListId DeliveryQuestionsId
        {
            get { return _deliveryQuestionsId; }
            set { _deliveryQuestionsId = value; }
        }
        public bool DriverCanCreateLegs
        {
            get { return _driverCanCreateLegs; }
            set { _driverCanCreateLegs = value; }
        }
        public bool DriverCanCreateActions
        {
            get { return _driverCanCreateActions; }
            set { _driverCanCreateActions = value; }
        }
        public bool TotalWeightRequired
        {
            get { return _totalWeightRequired; }
            set { _totalWeightRequired = value; }
        }
        public ListId DelayQuestionsId
        {
            get { return _delayQuestionsId; }
            set { _delayQuestionsId = value; }
        }
        public MassDeclarationRequired MassDecRequired
        {
            get { return _massDeclarationRequired; }
            set { _massDeclarationRequired = value; }
        }
        public ListId RejectQuestionsId
        {
            get { return _rejectQuestionsId; }
            set { _rejectQuestionsId = value; }
        }

        public bool DriverAdhocItems
        {
            get { return _driverAdhocItems; }
            set { _driverAdhocItems = value; }
        }
        public bool TrackableAssetLegActionsEnabled
        {
            get { return _trackableAssetLegActionsEnabled; }
            set { _trackableAssetLegActionsEnabled = value; }
        }
        public bool InspectAssetOnPickup
        {
            get { return _inspectAssetOnPickup; }
            set { _inspectAssetOnPickup = value; }
        }
        public bool InspectAssetOnDrop
        {
            get { return _inspectAssetOnDrop; }
            set { _inspectAssetOnDrop = value; }
        }
        #endregion

        #region Constructor
        public DeliveryJob()
        {
            JobType = JobTypes.Delivery;
            _itemsListId = new ListId();
            _pickupCustomersListId = new ListId();
            _pickupQuestionsId = new ListId();
            _deliveryCustomersListId = new ListId();
            _deliveryQuestionsId = new ListId();
            _delayQuestionsId = new ListId();
            _massDeclarationRequired = MassDeclarationRequired.No;
            _rejectQuestionsId = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            AutoComplete = BaseLayer.ReadBool(data, ref index);
            _itemsListId.Decode(data, ref index);
            _pickupCustomersListId.Decode(data, ref index);
            _pickupQuestionsId.Decode(data, ref index);
            _deliveryCustomersListId.Decode(data, ref index);
            _deliveryQuestionsId.Decode(data, ref index);
            _driverCanCreateLegs = false;
            _driverCanCreateActions = false;
            _totalWeightRequired = false;
            _delayQuestionsId = new ListId(-1, -1);
            _massDeclarationRequired = MassDeclarationRequired.No;
            _rejectQuestionsId = new ListId(-1, -1);
            if (versionNumber >= 2)
            {
                _driverCanCreateLegs = BaseLayer.ReadBool(data, ref index);
                _driverCanCreateActions = BaseLayer.ReadBool(data, ref index);
                if (versionNumber >= 3)
                {
                    _totalWeightRequired = BaseLayer.ReadBool(data, ref index);
                    _delayQuestionsId.Decode(data, ref index);
                    if (versionNumber >= 4)
                    {
                        _massDeclarationRequired = (MassDeclarationRequired)data[index++];
                        if (versionNumber >= 5)
                        {
                            _rejectQuestionsId.Decode(data, ref index);
                        }
                    }
                }
            }

            if (versionNumber >= 6)
            {
                _driverAdhocItems = BaseLayer.ReadBool(data, ref index);
            }
            if (versionNumber >= 7)
            {
                _trackableAssetLegActionsEnabled = BaseLayer.ReadBool(data, ref index);
                _inspectAssetOnPickup = BaseLayer.ReadBool(data, ref index);
                _inspectAssetOnDrop = BaseLayer.ReadBool(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _autoComplete);
                _itemsListId.Encode(local);
                _pickupCustomersListId.Encode(local);
                _pickupQuestionsId.Encode(local);
                _deliveryCustomersListId.Encode(local);
                _deliveryQuestionsId.Encode(local);
                BaseLayer.WriteBool(local, _driverCanCreateLegs);
                BaseLayer.WriteBool(local, _driverCanCreateActions);
                BaseLayer.WriteBool(local, _totalWeightRequired);
                _delayQuestionsId.Encode(local);
                local.WriteByte((byte)_massDeclarationRequired);
                _rejectQuestionsId.Encode(local);
                BaseLayer.WriteBool(local, _driverAdhocItems);
                BaseLayer.WriteBool(local, _trackableAssetLegActionsEnabled);
                BaseLayer.WriteBool(local, _inspectAssetOnPickup);
                BaseLayer.WriteBool(local, _inspectAssetOnDrop);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

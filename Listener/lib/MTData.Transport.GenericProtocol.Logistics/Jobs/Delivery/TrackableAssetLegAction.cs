
namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Delivery
{
    public class TrackableAssetLegAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private int _trackableAssetListId;
        private int _trackableAssetUniqueKey;
        private int _slaveFleetId;
        private int _slaveVehicleId;
        private string _name;
        private bool _inspectionRequired;
        #endregion

        #region public properties
        public int TrackableAssetListId
        {
            get { return _trackableAssetListId; }
            set { _trackableAssetListId = value; }
        }
        public int TrackableAssetUniqueKey
        {
            get { return _trackableAssetUniqueKey; }
            set { _trackableAssetUniqueKey = value; }
        }
        public int SlaveFleetId
        {
            get { return _slaveFleetId; }
            set { _slaveFleetId = value; }
        }
        public int SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }
        public bool InspectionRequired
        {
            get { return _inspectionRequired; }
            set { _inspectionRequired = value; }
        }

        #endregion

        #region constructor
        public TrackableAssetLegAction()
        {
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _trackableAssetListId = BaseLayer.Read4ByteInteger(data, ref index);
            _trackableAssetUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            _slaveFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            _slaveVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            _name = BaseLayer.ReadString(data, ref index, 0x12);
            _inspectionRequired = BaseLayer.ReadBool(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write4ByteInteger(local, _trackableAssetListId);
                BaseLayer.Write4ByteInteger(local, _trackableAssetUniqueKey);
                BaseLayer.Write4ByteInteger(local, _slaveFleetId);
                BaseLayer.Write4ByteInteger(local, _slaveVehicleId);
                BaseLayer.WriteString(local, _name, 0x12);
                BaseLayer.WriteBool(local, _inspectionRequired);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

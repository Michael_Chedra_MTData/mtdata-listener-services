using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by base in response of a UnitCreateLeg packet, returns the new leg and leg actions ids
    /// </summary>
    public class SendCreatedLegIdSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _originalLegId;
        private int _legId;
        private Dictionary<int, int> _legActionIds;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int OriginalLegId
        {
            get { return _originalLegId; }
            set { _originalLegId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public Dictionary<int, int> LegActionIds
        {
            get { return _legActionIds; }
            set { _legActionIds = value; }
        }
        #endregion

        #region constructor
        public SendCreatedLegIdSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendCreatedLegId;
            _legActionIds = new Dictionary<int, int>();
        }
        public SendCreatedLegIdSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendCreatedLegId)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendCreatedLegId, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendCreatedLegId;
            _legActionIds = new Dictionary<int, int>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _originalLegId);
                Write4ByteInteger(stream, _legId);
                Write2ByteInteger(stream, _legActionIds.Count);
                foreach (int originalLegActionId in _legActionIds.Keys)
                {
                    Write4ByteInteger(stream, originalLegActionId);
                    Write4ByteInteger(stream, _legActionIds[originalLegActionId]);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _originalLegId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _legActionIds.Clear();
            for (int i = 0; i < count; i++)
            {
                int originalActionId = Read4ByteInteger(Data, ref index);
                int id = Read4ByteInteger(Data, ref index);
                _legActionIds.Add(originalActionId, id);
            }
        }
        #endregion
    }
}

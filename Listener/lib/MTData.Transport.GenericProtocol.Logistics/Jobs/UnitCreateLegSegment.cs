using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// used by unit to create a leg on the fly
    /// </summary>
    public class UnitCreateLegSegment : SegmentLayer
    {
        #region private fields
        private ILeg _leg;
        private ListId _customerListId;
        private int _customerId;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public ILeg Leg
        {
            get { return _leg; }
            set { _leg = value; }
        }
        public ListId CustomerListId
        {
            get { return _customerListId; }
            set { _customerListId = value; }
        }
        public int CustomerId
        {
            get { return _customerId; }
            set { _customerId  = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public UnitCreateLegSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.UnitCreateLeg;
            _customerListId = new ListId();
        }
        public UnitCreateLegSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.UnitCreateLeg)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.UnitCreateLeg, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.UnitCreateLeg;
            _customerListId = new ListId(); 
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_leg.LegType);
                _leg.Encode(stream);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                if (Version >= 2)
                {
                    Write4ByteInteger(stream, _customerId);
                    _customerListId.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            LegTypes t = (LegTypes)Data[index++];
            switch (t)
            {
                case LegTypes.Simple:
                case LegTypes.ContainerPickup:
                case LegTypes.ContainerWait:
                case LegTypes.ContainerDelivery:
                    _leg = new Leg();
                    ((Leg)_leg).LegType = t;
                    break;
                case LegTypes.PalletPickup:
                    _leg = new Pallets.PalletsPickupLeg();
                    break;
                case LegTypes.PalletDelivery:
                    _leg = new Pallets.PalletsDeliveryLeg();
                    break;
                case LegTypes.PassengerStop:
                    _leg = new Passenger.PassengerStopLeg();
                    break;
                case LegTypes.Stop:
                    _leg = new Bus.StopLeg();
                    break;
                default:
                    throw new Exception(string.Format("Unknown leg type {0}", t));
            }
            _leg.Decode(Data, ref index);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            if (Version >= 2)
            {
                _customerId = Read4ByteInteger(Data, ref index);
                _customerListId.Decode(Data, ref index);
            }
            else
            {
                _customerId = -1;
                _customerListId = new ListId(-1, -1);
            }
        }
        #endregion

    }
}

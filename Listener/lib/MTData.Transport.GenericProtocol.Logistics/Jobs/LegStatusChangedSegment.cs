using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class LegStatusChangedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private LegStatus _legStatus;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegStatus Status
        {
            get { return _legStatus; }
            set { _legStatus = value; }
        }
        #endregion

        #region constructor
        public LegStatusChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.LegStatusChanged;
        }
        public LegStatusChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.LegStatusChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.LegStatusChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.LegStatusChanged;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legStatus);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _legStatus = (LegStatus)Data[index++];
        }
        #endregion
    }
}

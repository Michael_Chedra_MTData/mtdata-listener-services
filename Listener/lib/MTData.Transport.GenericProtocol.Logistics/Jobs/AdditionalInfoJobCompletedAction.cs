using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class AdditionalInfoJobCompletedAction : CompletedJobAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private List<Answer> _answers;
        #endregion

        #region public properties
        public List<Answer> Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }
        #endregion

        #region constructor
        public AdditionalInfoJobCompletedAction()
        {
            _answers = new List<Answer>();
            JobActionType = JobActionTypes.AdditionalInfo;
        }
        public AdditionalInfoJobCompletedAction(int actionId, List<Answer> answers)
            : base(JobActionTypes.AdditionalInfo, actionId)
        {
            _answers = answers;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _answers.Clear();
            for (int i = 0; i <count; i++)
            {
                Answer a = new Answer();
                a.Decode(data, ref index);
                _answers.Add(a);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write2ByteInteger(local, _answers.Count);
                foreach (Answer a in _answers)
                {
                    a.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class LegTime : ILegTime
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private DateTime _arriveTime;
        private DateTime _departTime;
        private int _arriveTimeTolerance;
        private int _departTimeTolerance;
        private int _deliveryWindowStart;
        private int _deliveryWindowEnd;
        #endregion

        #region constructor
        public LegTime()
        {
            _arriveTime = DateTime.MinValue;
            _departTime = DateTime.MinValue;
            _arriveTimeTolerance = -1;
            _departTimeTolerance = -1;
            _deliveryWindowStart = -1;
            _deliveryWindowEnd = -1;
        }
        #endregion

        #region ILegTime Members

        public DateTime ArriveTime
        {
            get { return _arriveTime; }
            set { _arriveTime = value; }
        }

        public DateTime DepartTime
        {
            get { return _departTime; }
            set { _departTime = value; }
        }

        public int ArriveTimeTolerance
        {
            get { return _arriveTimeTolerance; }
            set { _arriveTimeTolerance = value; }
        }

        public int DepartTimeTolerance
        {
            get { return _departTimeTolerance; }
            set { _departTimeTolerance = value; }
        }

        public int DeliveryWindowStart
        {
            get { return _deliveryWindowStart; }
            set { _deliveryWindowStart = value; }
        }
        public int DeliveryWindowEnd
        {
            get { return _deliveryWindowEnd; }
            set { _deliveryWindowEnd = value; }
        }

        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _arriveTime = BaseLayer.ReadDateTime(data, ref index);
            _departTime = BaseLayer.ReadDateTime(data, ref index);
            _arriveTimeTolerance = BaseLayer.Read2ByteInteger(data, ref index);
            _departTimeTolerance = BaseLayer.Read2ByteInteger(data, ref index);

            _deliveryWindowStart = -1;
            _deliveryWindowEnd = -1;
            if (versionNumber >= 2)
            {
                _deliveryWindowStart = BaseLayer.Read2ByteInteger(data, ref index);
                _deliveryWindowEnd = BaseLayer.Read2ByteInteger(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteDateTime(local, _arriveTime);
                BaseLayer.WriteDateTime(local, _departTime);
                BaseLayer.Write2ByteInteger(local, _arriveTimeTolerance);
                BaseLayer.Write2ByteInteger(local, _departTimeTolerance);
                BaseLayer.Write2ByteInteger(local, _deliveryWindowStart);
                BaseLayer.Write2ByteInteger(local, _deliveryWindowEnd);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

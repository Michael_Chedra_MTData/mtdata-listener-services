using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// the job types
    /// </summary>
    public enum JobTypes
    {
        /// <summary>
        /// Undefined job type
        /// </summary>
        Undefined = -1,
        /// <summary>
        ///  Pallet job type
        /// </summary>
        Pallets = 0,
        /// <summary>
        /// Service job type
        /// </summary>
        Service = 1,
        /// <summary>
        /// Simple job type
        /// </summary>
        Simple = 2,
        /// <summary>
        /// Bon job type
        /// </summary>
        Bin = 3,
        /// <summary>
        /// Bulk job type
        /// </summary>
        Bulk = 4,
        /// <summary>
        /// Recovery job type
        /// </summary>
        Recovery = 5,
        /// <summary>
        /// Passenger job type
        /// </summary>
        Passenger = 6,
        /// <summary>
        /// Delivery job type
        /// </summary>
        Delivery = 7,
        /// <summary>
        /// Trailer job type
        /// </summary>
        Trailer = 8,
        /// <summary>
        /// Container bulk job type
        /// </summary>
        ContainerBulk = 9,
        /// <summary>
        /// Container Request job type
        /// </summary>
        ContainerRequest = 10,
        /// <summary>
        /// Container sub job type
        /// </summary>
        ContainerSubJob = 11,
        /// <summary>
        /// Container job type
        /// </summary>
        Container = 12,
        /// <summary>
        /// Bus job Type
        /// </summary>
        Bus = 13,
        /// <summary>
        /// Concrete job Type
        /// </summary>
        Concrete = 14
    }

    /// <summary>
    /// Job Status
    /// </summary>
    public enum JobStatus
    {
        /// <summary>
        ///  Job not started
        /// </summary>
        NotStarted = 0,
        /// <summary>
        /// Job at leg
        /// </summary>
        AtLeg,
        /// <summary>
        /// Leg actions finished
        /// </summary>
        FinishedActionsAtLeg,
        /// <summary>
        /// En-route to leg
        /// </summary>
        OnRoute,
        /// <summary>
        /// Job complete
        /// </summary>
        Complete,
        /// <summary>
        /// New
        /// </summary>
        New,
        /// <summary>
        /// Sending from server
        /// </summary>
        Sending,
        /// <summary>
        /// Job cancelled
        /// </summary>
        Cancelled,
        /// <summary>
        /// Job rejected by driver
        /// </summary>
        Rejected,
        /// <summary>
        /// Job accepted by driver
        /// </summary>
        Accepted,
        /// <summary>
        /// Job unallocated
        /// </summary>
        UnAllocated,
        /// <summary>
        /// Job resent
        /// </summary>
        Resend,
        /// <summary>
        /// Job failed to send
        /// </summary>
        Fail,
        /// <summary>
        /// Job assigned to a driver
        /// </summary>
        AssignedToDriver,
        /// <summary>
        /// Waiting For Driver To Login
        /// </summary>
        WaitingForDriverToLogin,
        /// <summary>
        /// Job assigned to a vehicle
        /// </summary>
        AssignedToVehicle,
        /// <summary>
        /// Waiting For Vehicle To Login
        /// </summary>
        WaitingForVehicleToLogin,
        /// <summary>
        /// On Hold
        /// </summary>
        OnHold,
        /// <summary>
        /// Job is Delayed
        /// </summary>
        Delayed,
        /// <summary>
        /// Job is in Progress
        /// </summary>
        InProgress,
        /// <summary>
        /// Job is started
        /// </summary>
        Started,
        /// <summary>
        /// All legs completed, waiting for finish job action to be completed
        /// </summary>
        AllLegsCompleted
    }

    /// <summary>
    /// delegate for job events
    /// </summary>
    /// <param name="leg">An IJob</param>
    public delegate void JobEvent(IJob job);

    /// <summary>
    /// Job interface
    /// </summary>
    public interface IJob : IEnumerable, IPacketEncode
    {
        /// <summary>
        /// get the job type
        /// </summary>
        JobTypes JobType { get; }

        /// <summary>
        /// get the job dispatch id
        /// </summary>
        int DispatchId { get; }
        /// <summary>
        /// get an external job reference
        /// </summary>
        string JobReference { get; }
        /// <summary>
        /// get an external job reference number
        /// </summary>
        int JobReferenceInt { get; }
        /// <summary>
        /// get the title of the job
        /// </summary>
        string Title { get; }
        /// <summary>
        /// the docket number of the job
        /// </summary>
        string DocketNumber { get; set;}
        /// <summary>
        /// can a driver edit the docket number
        /// </summary>
        bool CanDriverEditDocketNumber { get; }
        /// <summary>
        /// get any job notes
        /// </summary>
        string JobNotes { get; }

        /// <summary>
        /// a count of the number of legs on this job
        /// </summary>
        int LegsCount { get; }
        /// <summary>
        /// get the leg at index
        /// </summary>
        /// <param name="index">the index of the leg</param>
        /// <returns></returns>
        ILeg this[int index] { get; }

        /// <summary>
        /// the current status of a job
        /// </summary>
        JobStatus Status { get; }

        /// <summary>
        /// event for when status of the job has changed
        /// </summary>
        event JobEvent OnJobStatusChanged;

        /// <summary>
        /// add a new leg to job
        /// </summary>
        /// <param name="leg"></param>
        void AddLeg(ILeg leg);
        /// <summary>
        /// delete a leg 
        /// </summary>
        /// <param name="legId"></param>
        void DeleteLeg(int legId);
        /// <summary>
        /// get the leg with the given leg id
        /// </summary>
        /// <param name="legId"></param>
        ILeg GetLegWithId(int legId);

        /// <summary>
        /// sort the legs on their legNumber
        /// </summary>
        void SortLegsOnLegNumber();

        /// <summary>
        /// a count of the number of job actions on this job
        /// </summary>
        int JobActionCount { get; }
        /// <summary>
        /// add a new job action to job
        /// </summary>
        /// <param name="jobAction"></param>
        void AddJobAction(IJobAction jobAction);
        /// <summary>
        /// delete a job action 
        /// </summary>
        /// <param name="jobActionId"></param>
        void DeleteJobAction(int jobActionId);
        /// <summary>
        /// get the job action with the index
        /// </summary>
        /// <param name="index"></param>
        IJobAction GetJobActionWithIndex(int index);
        /// <summary>
        /// get the job action with the given job action id
        /// </summary>
        /// <param name="jobActionId"></param>
        IJobAction GetJobActionWithId(int jobActionId);
        /// <summary>
        /// iterate through the job actions
        /// </summary>
        IJobActions JobActions { get; }
        /// <summary>
        /// a list of the completed job actions for this job
        /// </summary>
        List<ICompletedJobAction> CompletedJobActions { get; }
        /// <summary>
        /// get the completed job action with the given job action id
        /// </summary>
        /// <param name="jobActionId"></param>
        ICompletedJobAction GetCompletedJobActionWithId(int jobActionId);
    }

    public interface IJobActions : IEnumerable { }
}

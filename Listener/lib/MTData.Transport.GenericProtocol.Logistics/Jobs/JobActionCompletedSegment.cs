using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Bin;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by the unit when a job action is completed
    /// </summary>
    public class JobActionCompletedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private ICompletedJobAction _completedAction;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public ICompletedJobAction CompletedAction
        {
            get { return _completedAction; }
            set { _completedAction = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public JobActionCompletedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobActionComplete;
        }
        public JobActionCompletedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobActionComplete)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobActionComplete, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobActionComplete;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                stream.WriteByte((byte)_completedAction.JobActionType);
                _completedAction.Encode(stream);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            JobActionTypes t = (JobActionTypes)Data[index++];
            _completedAction = CreateNewItem(t);
            _completedAction.Decode(Data, ref index);
        }

        private ICompletedJobAction CreateNewItem(JobActionTypes t)
        {
            switch (t)
            {
                case JobActionTypes.Accept:
                case JobActionTypes.Reject:
                case JobActionTypes.DispatchedQuestion:
                case JobActionTypes.Delay:
                case JobActionTypes.Start:
                case JobActionTypes.Finish:
                    QuestionsCompletedJobAction a = new QuestionsCompletedJobAction();
                    a.JobActionType = t;
                    return a;
                case JobActionTypes.RequestCancel:
                case JobActionTypes.Alert:
                    CompletedJobAction cancelAction = new CompletedJobAction();
                    cancelAction.JobActionType = t;
                    return cancelAction;
                case JobActionTypes.AdditionalInfo:
                    return new AdditionalInfoJobCompletedAction();
                case JobActionTypes.EditDocketNumber:
                    return new EditDocketNumberJobCompletedAction();
            }
            throw new Exception(string.Format("Unknown Leg Action Type {0}", t));
        }
        #endregion
    }
}

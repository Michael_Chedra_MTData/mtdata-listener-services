﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Common
{
    public class QuestionValidation : IPacketEncode
    {
        #region Private Fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int versionNumber = 1;
        /// <summary>
        /// the question external ref
        /// </summary>
        private string _questionExtRef;
        /// <summary>
        /// the correct answer
        /// </summary>
        private string _answer;
        /// <summary>
        /// the override code
        /// </summary>
        private string _overrideCode;
        /// <summary>
        /// the error message
        /// </summary>
        private string _errorMessage;
        #endregion

        #region Public Properties
        /// <summary>
        /// the question external ref
        /// </summary>
        public string QuestionExtRef
        {
            get { return _questionExtRef; }
            set { _questionExtRef = value; }
        }
        /// <summary>
        /// the correct answer
        /// </summary>
        public string Answer
        {
            get { return _answer; }
            set { _answer = value; }
        }
        /// <summary>
        /// the override code
        /// </summary>
        public string OverrideCode
        {
            get { return _overrideCode; }
            set { _overrideCode = value; }
        }
        /// <summary>
        /// the error message
        /// </summary>
        public string ErrorMessage
        {
            get { return _errorMessage; }
            set { _errorMessage = value; }
        }
        #endregion

        #region Override Methods
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            this._questionExtRef = BaseLayer.ReadString(data, ref index, 0x12);
            this._answer = BaseLayer.ReadString(data, ref index, 0x12);
            this._overrideCode = BaseLayer.ReadString(data, ref index, 0x12);
            this._errorMessage = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, this._questionExtRef, 0x12);
                BaseLayer.WriteString(local, this._answer, 0x12);
                BaseLayer.WriteString(local, this._overrideCode, 0x12);
                BaseLayer.WriteString(local, this._errorMessage, 0x12);
                //write version number
                BaseLayer.WriteMoreFlag(stream, this.versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Common
{
    public enum QuestionGroupResultTypes
    {
        Nothing = 0,
        DriveOutput1,
        DriveOutput2
    }

    public class QuestionGroupValidation : IPacketEncode
    {
        public readonly static int DRIVE_OUTPUT_UNTIL_LEG_IS_COMPLETE = 0;

        #region Private Fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int versionNumber = 1;
        /// <summary>
        /// the result type
        /// </summary>
        private QuestionGroupResultTypes _resultType;
        /// <summary>
        /// the result time length
        /// </summary>
        private int _resultTimeLength;
        private List<QuestionValidation> _validations;
        #endregion

        #region Public Properties
        /// <summary>
        /// the result type
        /// </summary>
        public QuestionGroupResultTypes ResultType
        {
            get { return _resultType; }
            set { _resultType = value; }
        }
        /// <summary>
        /// the result time length
        /// </summary>
        public int ResultTimeLength
        {
            get { return _resultTimeLength; }
            set { _resultTimeLength = value; }
        }
        public List<QuestionValidation> Validations
        {
            get { return _validations; }
            set { _validations = value; }
        }
        #endregion

        public QuestionGroupValidation()
        {
            _validations = new List<QuestionValidation>();
        }

        #region Override Methods
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            this._resultType = (QuestionGroupResultTypes)data[index++];
            this._resultTimeLength = BaseLayer.Read4ByteInteger(data, ref index);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _validations.Clear();
            for (int i = 0; i < count; i++)
            {
                QuestionValidation q = new QuestionValidation();
                q.Decode(data, ref index);
                _validations.Add(q);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_resultType);
                BaseLayer.Write4ByteInteger(local, this._resultTimeLength);
                BaseLayer.Write2ByteInteger(local, this._validations.Count);
                foreach (QuestionValidation q in _validations)
                {
                    q.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, this.versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

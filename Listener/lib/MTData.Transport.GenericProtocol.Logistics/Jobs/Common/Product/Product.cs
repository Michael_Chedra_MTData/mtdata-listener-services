﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Common
{
    public class Product : IPacketEncode
    {
        #region Private Fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int versionNumber = 1;
        /// <summary>
        /// the item id
        /// </summary>
        private int productId;
        /// <summary>
        /// the item list id
        /// </summary>
        private int productListId;
        /// <summary>
        /// the item list version id
        /// </summary>
        private int productListVersion;
        /// <summary>
        /// the external ref
        /// </summary>
        private string externalReference = "";
        /// <summary>
        /// the number of items
        /// </summary>
        private float count;
        /// <summary>
        /// the weight of the items
        /// </summary>
        private float weight;
        /// <summary>
        /// the units of the item
        /// </summary>
        private string unitType;
        /// <summary>
        /// the dimensions of the item
        /// </summary>
        private string dimensions;
        /// <summary>
        /// the notes of the item
        /// </summary>
        private string notes;
        #endregion

        #region Constructor

        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        /// <summary>
        /// the item id
        /// </summary>
        public int ProductId
        {
            get { return productId; }
            set { productId = value; }
        }
        /// <summary>
        /// the item list id
        /// </summary>
        public int ProductListId
        {
            get { return productListId; }
            set { productListId = value; }
        }
        /// <summary>
        /// the item list version
        /// </summary>
        public int ProductListVersion
        {
            get { return productListVersion; }
            set { productListVersion = value; }
        }
        /// <summary>
        /// the external reference
        /// </summary>
        public string ExternalReference
        {
            get { return externalReference; }
            set { externalReference = value; }
        }
        /// <summary>
        /// the number of items
        /// </summary>
        public float Count
        {
            get { return count; }
            set { count = value; }
        }
        /// <summary>
        /// the weight of the items
        /// </summary>
        public float Weight
        {
            get { return weight; }
            set { weight = value; }
        }

        /// <summary>
        /// the units of the item
        /// </summary>
        public string UnitType
        {
            get { return unitType; }
            set { unitType = value; }
        }

        /// <summary>
        /// the Dimensions of the item
        /// </summary>
        public string Dimensions
        {
            get { return dimensions; }
            set { dimensions = value; }
        }
        /// <summary>
        /// the Notes of the item
        /// </summary>
        public string Notes
        {
            get { return notes; }
            set { notes = value; }
        }
        #endregion

        #region Methods

        #endregion

        #region Override Methods

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            this.productId = BaseLayer.Read4ByteInteger(data, ref index);
            this.productListId = BaseLayer.Read4ByteInteger(data, ref index);
            this.productListVersion = BaseLayer.Read4ByteInteger(data, ref index);
            this.externalReference = BaseLayer.ReadString(data, ref index, 0x12);
            this.count = BaseLayer.ReadFloat(data, ref index);
            this.weight = BaseLayer.ReadFloat(data, ref index);
            this.unitType = BaseLayer.ReadString(data, ref index, 0x12);
            this.dimensions = BaseLayer.ReadString(data, ref index, 0x12);
            this.notes = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, this.productId);
                BaseLayer.Write4ByteInteger(local, this.productListId);
                BaseLayer.Write4ByteInteger(local, this.productListVersion);
                BaseLayer.WriteString(local, this.externalReference, 0x12);
                BaseLayer.WriteFloat(local, this.count);
                BaseLayer.WriteFloat(local, this.weight);
                BaseLayer.WriteString(local, this.unitType, 0x12);
                BaseLayer.WriteString(local, this.dimensions, 0x12);
                BaseLayer.WriteString(local, this.notes, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, this.versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Common
{
    public class ProductsCompletedLegAction : CompletedAction
    {
        #region Private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private List<Product> products;
        #endregion

        #region Public Properties
        public List<Product> Products
        {
            get { return this.products; }
            set { this.products = value; }
        }
        #endregion

        #region Constructor
        public ProductsCompletedLegAction()
        {
            this.products = new List<Product>();
        }
        #endregion

        #region Override Methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            int count = BaseLayer.Read2ByteInteger(data, ref index);

            this.products.Clear();

            for (int i = 0; i < count; i++)
            {
                Product product = new Product();
                product.Decode(data, ref index);
                this.products.Add(product);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write2ByteInteger(local, this.products.Count);

                foreach (Product p in this.products)
                {
                    p.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

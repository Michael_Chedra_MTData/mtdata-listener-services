using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Common
{
    public class AlertJobAction : JobAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private JobStatus _triggerStatus;
        private JobStatus _stopStatus;
        private int _timeLimit;
        private bool _repeat;
        #endregion

        #region public properties
        public JobStatus TriggerStatus
        {
            get { return _triggerStatus; }
            set { _triggerStatus = value; }
        }
        public JobStatus StopStatus
        {
            get { return _stopStatus; }
            set { _stopStatus = value; }
        }
        public int TimeLimit
        {
            get { return _timeLimit; }
            set { _timeLimit = value; }
        }
        public bool Repeat
        {
            get { return _repeat; }
            set { _repeat = value; }
        }

        #endregion

        #region constructor
        public AlertJobAction()
        {
            JobActionType = JobActionTypes.Alert;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _triggerStatus = (JobStatus)data[index++];
            _stopStatus = (JobStatus)data[index++];
            _timeLimit = BaseLayer.Read4ByteInteger(data, ref index);
            _repeat = false;
            if (versionNumber >= 2)
            {
                _repeat = BaseLayer.ReadBool(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                local.WriteByte((byte)_triggerStatus);
                local.WriteByte((byte)_stopStatus);
                BaseLayer.Write4ByteInteger(local, _timeLimit);
                BaseLayer.WriteBool(local, _repeat);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

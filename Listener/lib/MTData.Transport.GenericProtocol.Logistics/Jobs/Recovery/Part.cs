using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Recovery
{
    /// <summary>
    /// this class represents a part
    /// </summary>
    public class Part : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 1;
        private int _id;
        private string _description;
        private float _cost;
        #endregion

        #region properties
        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public float Cost
        {
            get { return _cost; }
            set { _cost = value; }
        }
        #endregion

        #region constructor
        public Part()
        {
            _description = string.Empty;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            _id = BaseLayer.Read4ByteInteger(data, ref index);
            _description = BaseLayer.ReadString(data, ref index, 0x12);
            _cost = BaseLayer.Read4ByteInteger(data, ref index) / 100f;

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _id);
                BaseLayer.WriteString(local, _description, 0x12);
                BaseLayer.Write4ByteInteger(local, (int)(_cost * 100));

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

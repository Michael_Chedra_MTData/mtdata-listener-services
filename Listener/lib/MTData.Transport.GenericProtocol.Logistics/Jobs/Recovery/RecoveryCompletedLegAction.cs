using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Recovery
{
    /// <summary>
    /// represents a recovery completed leg action
    /// </summary>
    public class RecoveryCompletedLegAction : CompletedAction
    {
         #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private int _passengerCount;
        private float _kmsTravelled;
        private int _odometer;
        private ListId _actualProblemListId;
        private int _actualProblemId;
        private ListId _actionTakenListId;
        private int _actionTakenId;
        private string _otherInfo;
        private List<int> _towingServiceIds;
        private ListId _towingServiceListId;
        private string _towingServiceOther;
        private ListId _partsListId;
        private List<Part> _parts;
        #endregion

        #region public properties
        public int PassengerCount
        {
            get { return _passengerCount; }
            set { _passengerCount = value; }
        }
        public float KmsTravelled
        {
            get { return _kmsTravelled; }
            set { _kmsTravelled = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public ListId ActualProblemListId
        {
            get { return _actualProblemListId; }
            set { _actualProblemListId = value; }
        }
        public int ActualProblemId
        {
            get { return _actualProblemId; }
            set { _actualProblemId = value; }
        }
        public ListId ActionTakenListId
        {
            get { return _actionTakenListId; }
            set { _actionTakenListId = value; }
        }
        public int ActionTakenId
        {
            get { return _actionTakenId; }
            set { _actionTakenId = value; }
        }
        public string OtherInfo
        {
            get { return _otherInfo; }
            set { _otherInfo = value; }
        }
        public List<int> TowingServiceIds
        {
            get { return _towingServiceIds; }
            set { _towingServiceIds = value; }
        }
        public ListId TowingServiceListId
        {
            get { return _towingServiceListId; }
            set { _towingServiceListId = value; }
        }
        public string TowingServiceOther
        {
            get { return _towingServiceOther; }
            set { _towingServiceOther = value; }
        }
        public ListId PartsListId
        {
            get { return _partsListId; }
            set { _partsListId = value; }
        }
        public List<Part> Parts
        {
            get { return _parts; }
            set { _parts = value; }
        }
        #endregion

        #region constructor
        public RecoveryCompletedLegAction()
        {
            LegActionType = LegActionTypes.Recovery;
            _actualProblemListId = new ListId();
            _actionTakenListId = new ListId();
            _otherInfo = string.Empty;
            _towingServiceIds = new List<int>();
            _towingServiceListId = new ListId();
            _towingServiceOther = string.Empty;
            _partsListId = new ListId();
            _parts = new List<Part>();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _passengerCount = BaseLayer.Read4ByteInteger(data, ref index);
            _kmsTravelled = BaseLayer.Read4ByteInteger(data, ref index) / 1000f;
            _odometer = BaseLayer.Read4ByteInteger(data, ref index);
            _actualProblemListId.Decode(data, ref index); 
            _actualProblemId = BaseLayer.Read4ByteInteger(data, ref index);
            _actionTakenListId.Decode(data, ref index);
            _actionTakenId = BaseLayer.Read4ByteInteger(data, ref index);
            _otherInfo = BaseLayer.ReadString(data, ref index, 0x12);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _towingServiceIds.Clear();
            for (int i=0; i<count; i++)
            {
                _towingServiceIds.Add(BaseLayer.Read4ByteInteger(data, ref index));
            }
            _towingServiceListId.Decode(data, ref index);
            _towingServiceOther = BaseLayer.ReadString(data, ref index, 0x12);
            _partsListId.Decode(data, ref index);
            count = BaseLayer.Read2ByteInteger(data, ref index);
            _parts.Clear();
            for (int i = 0; i < count; i++)
            {
                Part p = new Part();
                p.Decode(data, ref index);
                _parts.Add(p);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                BaseLayer.Write4ByteInteger(local, _passengerCount);
                BaseLayer.Write4ByteInteger(local, (int)(_kmsTravelled * 1000));
                BaseLayer.Write4ByteInteger(local, _odometer);
                _actualProblemListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _actualProblemId);
                _actionTakenListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _actionTakenId);
                BaseLayer.WriteString(local, _otherInfo, 0x12);
                BaseLayer.Write2ByteInteger(local, _towingServiceIds.Count);
                foreach (int id in _towingServiceIds)
                {
                    BaseLayer.Write4ByteInteger(local, id);
                }
                _towingServiceListId.Encode(local);
                BaseLayer.WriteString(local, _towingServiceOther, 0x12);
                _partsListId.Encode(local);
                BaseLayer.Write2ByteInteger(local, _parts.Count);
                foreach (Part p in _parts)
                {
                    p.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

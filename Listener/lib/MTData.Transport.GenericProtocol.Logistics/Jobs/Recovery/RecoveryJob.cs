using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Recovery
{
    /// <summary>
    /// class represents a recovery job
    /// </summary>
    public class RecoveryJob : Job
    {
         #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        
        /// <summary>
        /// the list id/version of the actual problem list
        /// </summary>
        private ListId _actualProblemListId;
        /// <summary>
        /// the list id/version of the action taken list
        /// </summary>
        private ListId _actionTakenListId;
        /// <summary>
        /// the list id/version of the towing service list
        /// </summary>
        private ListId _towingServiceListId;
        /// <summary>
        /// the list id/version of the parts list
        /// </summary>
        private ListId _partsListId;
        #endregion

        #region public properties
        public ListId ActualProblemListId
        {
            get { return _actualProblemListId; }
            set { _actualProblemListId = value; }
        }
        public ListId ActionTakenListId
        {
            get { return _actionTakenListId; }
            set { _actionTakenListId = value; }
        }
        public ListId TowingServiceListId
        {
            get { return _towingServiceListId; }
            set { _towingServiceListId = value; }
        }
        public ListId PartsListId
        {
            get { return _partsListId; }
            set { _partsListId = value; }
        }
        #endregion

        #region Constructor
        public RecoveryJob()
        {
            JobType = JobTypes.Recovery;
            _actionTakenListId = new ListId();
            _actualProblemListId = new ListId();
            _partsListId = new ListId();
            _towingServiceListId = new ListId();
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _actionTakenListId.Decode(data, ref index);
            _actualProblemListId.Decode(data, ref index);
            _partsListId.Decode(data, ref index);
            _towingServiceListId.Decode(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _actionTakenListId.Encode(local);
                _actualProblemListId.Encode(local);
                _partsListId.Encode(local);
                _towingServiceListId.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    
    }
}

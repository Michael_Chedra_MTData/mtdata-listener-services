using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Recovery
{
    /// <summary>
    /// class represents a recovery leg action
    /// </summary>
    public class RecoveryLegAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private string _client;
        private string _regNo;
        private string _caller;
        private string _make;
        private string _model;
        private string _manufactureYear;
        private string _fuelType;
        private string _transmission;
        private string _color;
        private string _vin;
        private string _problemGiven;
        private string _extraLocationInfo;
        private string _additionalContactDetails;
        private bool _callOnArrival;

        #endregion

        #region public properties
        public string Client
        {
            get { return _client; }
            set { _client = value; }
        }
        public string RegNo
        {
            get { return _regNo; }
            set { _regNo = value; }
        }
        public string Caller
        {
            get { return _caller; }
            set { _caller = value; }
        }
        public string Make
        {
            get { return _make; }
            set { _make = value; }
        }
        public string Model
        {
            get { return _model; }
            set { _model = value; }
        }
        public string ManufactureYear
        {
            get { return _manufactureYear; }
            set { _manufactureYear = value; }
        }
        public string FuelType
        {
            get { return _fuelType; }
            set { _fuelType = value; }
        }
        public string Transmission
        {
            get { return _transmission; }
            set { _transmission = value; }
        }
        public string Color
        {
            get { return _color; }
            set { _color = value; }
        }
        public string Vin
        {
            get { return _vin; }
            set { _vin = value; }
        }
        public string ProblemGiven
        {
            get { return _problemGiven; }
            set { _problemGiven = value; }
        }
        public string ExtraLocationInfo
        {
            get { return _extraLocationInfo; }
            set { _extraLocationInfo = value; }
        }
        public string AdditionalContactDetails
        {
            get { return _additionalContactDetails; }
            set { _additionalContactDetails = value; }
        }
        public bool CallOnArrival
        {
            get { return _callOnArrival; }
            set { _callOnArrival = value; }
        }

        #endregion

        #region constructor
        public RecoveryLegAction()
        {
            LegActionType = LegActionTypes.Recovery;
            _client = string.Empty;
            _regNo = string.Empty;
            _caller = string.Empty;
            _make = string.Empty;
            _model = string.Empty;
            _manufactureYear = string.Empty;
            _fuelType = string.Empty;
            _transmission = string.Empty;
            _color = string.Empty;
            _vin = string.Empty;
            _problemGiven = string.Empty;
            _extraLocationInfo = string.Empty;
            _additionalContactDetails = string.Empty;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _client = BaseLayer.ReadString(data, ref index, 0x12);
            _regNo = BaseLayer.ReadString(data, ref index, 0x12);
            _caller = BaseLayer.ReadString(data, ref index, 0x12);
            _make = BaseLayer.ReadString(data, ref index, 0x12);
            _model = BaseLayer.ReadString(data, ref index, 0x12);
            _manufactureYear = BaseLayer.ReadString(data, ref index, 0x12);
            _fuelType = BaseLayer.ReadString(data, ref index, 0x12);
            _transmission = BaseLayer.ReadString(data, ref index, 0x12);
            _color = BaseLayer.ReadString(data, ref index, 0x12);
            _vin = BaseLayer.ReadString(data, ref index, 0x12);
            _problemGiven = BaseLayer.ReadString(data, ref index, 0x12);
            _extraLocationInfo = BaseLayer.ReadString(data, ref index, 0x12);
            _additionalContactDetails = BaseLayer.ReadString(data, ref index, 0x12);
            _callOnArrival = BaseLayer.ReadBool(data, ref index);
            
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _client, 0x12);
                BaseLayer.WriteString(local, _regNo, 0x12);
                BaseLayer.WriteString(local, _caller, 0x12);
                BaseLayer.WriteString(local, _make, 0x12);
                BaseLayer.WriteString(local, _model, 0x12);
                BaseLayer.WriteString(local, _manufactureYear, 0x12);
                BaseLayer.WriteString(local, _fuelType, 0x12);
                BaseLayer.WriteString(local, _transmission, 0x12);
                BaseLayer.WriteString(local, _color, 0x12);
                BaseLayer.WriteString(local, _vin, 0x12);
                BaseLayer.WriteString(local, _problemGiven, 0x12);
                BaseLayer.WriteString(local, _extraLocationInfo, 0x12);
                BaseLayer.WriteString(local, _additionalContactDetails, 0x12);
                BaseLayer.WriteBool(local, _callOnArrival);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class LegAction : ILegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private LegActionTypes _legActionType;
        private int _legId;
        private int _legActionId;
        private int _legActionNumber;
        private string _title;
        private string _notes;
        private LegActionStatus _status;
        private string _externalReference;
        private Dictionary<string, string> _customData;
        #endregion

        #region Constructor
        public LegAction()
        {
            _status = LegActionStatus.NotStarted;
            _title = string.Empty;
            _notes = string.Empty;
            _externalReference = string.Empty;
            _customData = new Dictionary<string, string>();
        }
        #endregion

        #region ILegAction Members

        public LegActionTypes LegActionType
        {
            get { return _legActionType; }
            set { _legActionType = value; }
        }

        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }

        public int LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }

        public int LegActionNumber
        {
            get { return _legActionNumber; }
            set { _legActionNumber = value; }
        }

        public string Title
        {
            get { return _title; }
            set { _title = value; }
        }

        public string Notes
        {
            get { return _notes; }
            set { _notes = value; }
        }

        public LegActionStatus Status
        {
            get { return _status; }
            set 
            {
                if (_status != value)
                {
                    _status = value;
                    if (OnLegActionStatusChanged != null)
                    {
                        OnLegActionStatusChanged(this);
                    }
                }
            }
        }

        public string ExternalReference 
        {
            get { return _externalReference; }
            set { _externalReference = value; }
        }

        public Dictionary<string, string> CustomData
        {
            get { return _customData; }
            set { _customData = value; }
        }

        public event LegActionEvent OnLegActionStatusChanged;
        #endregion

        #region IPacketEncode Members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _legActionType = (LegActionTypes)data[index++];
            _legId = BaseLayer.Read4ByteInteger(data, ref index);
            _legActionId = BaseLayer.Read4ByteInteger(data, ref index);
            _legActionNumber = BaseLayer.Read4ByteInteger(data, ref index);
            _title = BaseLayer.ReadString(data, ref index, 0x12);
            _notes = BaseLayer.ReadString(data, ref index, 0x12);
            _status = (LegActionStatus)data[index++];
            _externalReference = BaseLayer.ReadString(data, ref index, 0x12);

            _customData = new Dictionary<string, string>();
            if (versionNumber >= 2)
            {
                int count = (int)data[index++];
                for (int i = 0; i < count; i++)
                {
                    string key = BaseLayer.ReadString(data, ref index, 0x12);
                    string value = BaseLayer.ReadString(data, ref index, 0x12);
                    _customData.Add(key, value);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_legActionType);
                BaseLayer.Write4ByteInteger(local, _legId);
                BaseLayer.Write4ByteInteger(local, _legActionId);
                BaseLayer.Write4ByteInteger(local, _legActionNumber);
                BaseLayer.WriteString(local, _title, 0x12);
                BaseLayer.WriteString(local, _notes, 0x12);
                local.WriteByte((byte)_status);
                BaseLayer.WriteString(local, _externalReference, 0x12);

                if (_versionNumber >= 2)
                {
                    local.WriteByte((byte)_customData.Count);
                    foreach (string key in _customData.Keys)
                    {
                        BaseLayer.WriteString(local, key, 0x12);
                        BaseLayer.WriteString(local, _customData[key], 0x12);
                    }
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
   }
}

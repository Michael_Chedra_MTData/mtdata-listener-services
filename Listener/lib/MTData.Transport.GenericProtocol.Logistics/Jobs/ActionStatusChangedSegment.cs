using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class ActionStatusChangedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private LegTypes _legType;
        private int _actionId;
        private LegActionTypes _actionType;
        private LegActionStatus _actionStatus;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegTypes LegType
        {
            get { return _legType; }
            set { _legType = value; }
        }
        public int ActionId
        {
            get { return _actionId; }
            set { _actionId = value; }
        }
        public LegActionTypes ActionType
        {
            get { return _actionType; }
            set { _actionType = value; }
        }
        public LegActionStatus ActionStatus
        {
            get { return _actionStatus; }
            set { _actionStatus = value; }
        }
        #endregion

        #region constructor
        public ActionStatusChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobLegActionStatusChanged;
        }
        public ActionStatusChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobLegActionStatusChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobLegActionStatusChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobLegActionStatusChanged;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legType);
                Write4ByteInteger(stream, _actionId);
                stream.WriteByte((byte)_actionType);
                stream.WriteByte((byte)_actionStatus);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _legType = (LegTypes)Data[index++];
            _actionId = Read4ByteInteger(Data, ref index);
            _actionType = (LegActionTypes)Data[index++];
            _actionStatus = (LegActionStatus)Data[index++];
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Trailer
{
    /// <summary>
    /// class represents a Trailer job
    /// </summary>
    public class TrailerJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        
        /// <summary>
        /// is this job an auto complete job
        /// </summary>
        private bool _autoComplete;
        #endregion

        #region public properties
        public bool AutoComplete
        {
            get { return _autoComplete; }
            set 
            {
                _autoComplete = value;
                CompleteJobWhenAllLegsAreComplete = _autoComplete;
            }
        }
        #endregion

        #region Constructor
        public TrailerJob()
        {
            JobType = JobTypes.Trailer;
            CompleteJobWhenAllLegsAreComplete = false;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            AutoComplete = BaseLayer.ReadBool(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteBool(local, _autoComplete);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    
    }
}

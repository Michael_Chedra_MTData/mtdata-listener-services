using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// used by base to send a cancel job to a unit
    /// </summary>
    public class CancelJobSegment : SegmentLayer
    {
        #region private fields
        private int _allocationId;
        private int _dispatchId;
        private string _message;
        #endregion

        #region properties
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        #endregion

        #region constructor
        public CancelJobSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.CancelJob;
        }
        public CancelJobSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.CancelJob)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.CancelJob, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.CancelJob;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                Write4ByteInteger(stream, _dispatchId);
                if (Version >= 2)
                {
                    WriteString(stream, _message, 0x12);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
            _dispatchId = Read4ByteInteger(Data, ref index);
            if (Version >= 2)
            {
                _message = ReadString(Data, ref index, 0x12);
            }
            else
            {
                _message = string.Empty;
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// packet sent by unit to confirm it has received a new job item (job, leg or action)
    /// </summary>
    public class JobItemReceivedSegment : SegmentLayer
    {
        #region private fields
        private int _allocationId;
        #endregion

        #region properties
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        #endregion

        #region constructor
        public JobItemReceivedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobItemReceived;
        }
        public JobItemReceivedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobItemReceived)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobItemReceived, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobItemReceived;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

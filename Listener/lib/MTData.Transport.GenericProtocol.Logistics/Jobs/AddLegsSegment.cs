using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// packet sent by base when adding a new leg to an existing job
    /// </summary>
    public class AddLegsSegment : SegmentLayer
    {
        #region private fields
        private int _allocationId;
        private List<ILeg> _legs;
        /// <summary>
        /// dictionary containing the order of the legs for this job
        /// key = leg id, value = the leg number (used for sequencing)
        /// </summary>
        private Dictionary<int, int> _legOrderNumbers;
        #endregion

        #region properties
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        public List<ILeg> Legs
        {
            get { return _legs; }
            set { _legs = value; }
        }
        /// <summary>
        /// dictionary containing the order of the legs for this job
        /// key = leg id, value = the leg number (used for sequencing)
        /// </summary>
        public Dictionary<int, int> LegOrderNumbers
        {
            get { return _legOrderNumbers; }
            set { _legOrderNumbers = value; }
        }
        #endregion

        #region constructor
        public AddLegsSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.AddLeg;
            _legs = new List<ILeg>();
            _legOrderNumbers = new Dictionary<int, int>();
        }
        public AddLegsSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.AddLeg)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.AddLeg, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.AddLeg;
            _legs = new List<ILeg>();
            _legOrderNumbers = new Dictionary<int, int>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                Write2ByteInteger(stream, _legs.Count);
                foreach (ILeg leg in _legs)
                {
                    stream.WriteByte((byte)leg.LegType);
                    leg.Encode(stream);
                }
                Write2ByteInteger(stream, _legOrderNumbers.Count);
                foreach (int orderId in _legOrderNumbers.Keys)
                {
                    Write4ByteInteger(stream, orderId);
                    Write4ByteInteger(stream, _legOrderNumbers[orderId]);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _legs.Clear();
            for (int i = 0; i < count; i++)
            {
                LegTypes t = (LegTypes)Data[index++];
                ILeg leg = null;
                switch (t)
                {
                    case LegTypes.Simple:
                    case LegTypes.ContainerPickup:
                    case LegTypes.ContainerWait:
                    case LegTypes.ContainerDelivery:
                        leg = new Leg();
                        ((Leg)leg).LegType = t;
                        break;
                    case LegTypes.PalletPickup:
                        leg = new Pallets.PalletsPickupLeg();
                        break;
                    case LegTypes.PalletDelivery:
                        leg = new Pallets.PalletsDeliveryLeg();
                        break;
                    case LegTypes.PassengerStop:
                        leg = new Passenger.PassengerStopLeg();
                        break;
                    case LegTypes.Stop:
                        leg = new Bus.StopLeg();
                        break;
                }
                if (leg == null)
                {
                    throw new Exception(string.Format("Unknown action type {0}", t));
                }
                leg.Decode(Data, ref index);
                _legs.Add(leg);
            }
            count = Read2ByteInteger(Data, ref index);
            _legOrderNumbers.Clear();
            for (int i = 0; i < count; i++)
            {
                int id = Read4ByteInteger(Data, ref index);
                int orderNumber = Read4ByteInteger(Data, ref index);
                _legOrderNumbers.Add(id, orderNumber);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class JobEndTransactionSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchedId;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchedId; }
            set { _dispatchedId = value; }
        }
        #endregion

        #region constructor
        public JobEndTransactionSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobEndTransaction;
        }
        public JobEndTransactionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobEndTransaction)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobEndTransaction, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobEndTransaction;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchedId);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchedId = Read4ByteInteger(Data, ref index);
        }
        #endregion    
    }
}

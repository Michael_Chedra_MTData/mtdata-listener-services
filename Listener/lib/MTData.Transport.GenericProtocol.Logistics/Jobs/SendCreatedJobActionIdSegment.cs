using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by base in response of a UnitCreateJobAction packet, returns the new job actions id
    /// </summary>
    public class SendCreatedJobActionIdSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _originalJobActionId;
        private int _jobActionId;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int OriginalJobActionId
        {
            get { return _originalJobActionId; }
            set { _originalJobActionId = value; }
        }
        public int JobActionId
        {
            get { return _jobActionId; }
            set { _jobActionId = value; }
        }
        #endregion

        #region constructor
        public SendCreatedJobActionIdSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendCreatedJobActionId;
        }
        public SendCreatedJobActionIdSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendCreatedJobActionId)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendCreatedJobActionId, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendCreatedJobActionId;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _originalJobActionId);
                Write4ByteInteger(stream, _jobActionId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _originalJobActionId = Read4ByteInteger(Data, ref index);
            _jobActionId = Read4ByteInteger(Data, ref index);
        }
        #endregion
    }
}

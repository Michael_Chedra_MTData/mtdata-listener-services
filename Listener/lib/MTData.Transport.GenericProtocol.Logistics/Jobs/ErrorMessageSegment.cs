using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public enum LogisticsErrorCodes
    {
        CreateJobTemplateFailed=0
    }

    /// <summary>
    /// used by base to send a cancel job to a unit
    /// </summary>
    public class ErrorMessageSegment : SegmentLayer
    {
        #region private fields
        private LogisticsErrorCodes _errorCode;
        private string _message;
        private byte[] _associatedData;
        #endregion

        #region properties
        public LogisticsErrorCodes ErrorCode
        {
            get { return _errorCode; }
            set { _errorCode = value; }
        }
        public string ErrorMessage
        {
            get { return _message; }
            set { _message = value; }
        }
        public byte[] AssociatedData
        {
            get { return _associatedData; }
            set { _associatedData = value; }
        }
        #endregion

        #region constructor
        public ErrorMessageSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.ErrorMessage;
        }
        public ErrorMessageSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.ErrorMessage)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.ErrorMessage, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.ErrorMessage;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_errorCode);
                WriteString(stream, _message, 0x12);
                if (_associatedData != null && _associatedData.Length > 0)
                {
                    Write2ByteInteger(stream, _associatedData.Length);
                    stream.Write(_associatedData, 0, _associatedData.Length);
                }
                else
                {
                    Write2ByteInteger(stream, 0);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _errorCode = (LogisticsErrorCodes)Data[index++];
            _message = ReadString(Data, ref index, 0x12);
            int count = Read2ByteInteger(Data, ref index);
            _associatedData = new byte[count];
            if (count > 0)
            {
                Array.Copy(Data, index, _associatedData, 0, count);
                index += count;
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// used by unit to create a leg action on the fly
    /// </summary>
    public class UnitCreateLegActionSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchId;
        private ILegAction _legAction;
        private ICompetedAction _completedLegAction;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public ILegAction LegAction
        {
            get { return _legAction; }
            set { _legAction = value; }
        }
        public ICompetedAction CompletedLegAction
        {
            get { return _completedLegAction; }
            set { _completedLegAction = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public UnitCreateLegActionSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.UnitCreateLegAction;
        }
        public UnitCreateLegActionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.UnitCreateLegAction)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.UnitCreateLegAction, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.UnitCreateLegAction;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchId);
                stream.WriteByte((byte)_legAction.LegActionType);
                _legAction.Encode(stream);
                if (_completedLegAction != null && (_legAction.Status == LegActionStatus.Complete || _legAction.Status == LegActionStatus.CompleteWithChanges))
                {
                    _completedLegAction.Encode(stream);
                }
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchId = Read4ByteInteger(Data, ref index);
            LegActionTypes t = (LegActionTypes)Data[index++];
            switch (t)
            {
                case LegActionTypes.PalletPickup:
                    _legAction = new Pallets.PalletsPickupAction();
                    _completedLegAction = new Pallets.PalletsPickupCompletedAction();
                    break;
                case LegActionTypes.PalletDelivery:
                    _legAction = new Pallets.PalletsDeliveryAction();
                    _completedLegAction = new Pallets.PalletsDeliveryCompletedAction();
                    break;
                case LegActionTypes.PalletQuestions:
                    _legAction = new Pallets.PalletsQuestionAction();
                    _completedLegAction = new Pallets.PalletsQuestionCompletedAction();
                    break;
                case LegActionTypes.Questions:
                case LegActionTypes.OutOfSequence:
                    _legAction = new QuestionAction(t);
                    _completedLegAction = new QuestionCompletedAction(t);
                    break;
                case LegActionTypes.BinPickup:
                    _legAction = new Bin.BinPickupAction();
                    _completedLegAction = new Bin.BinPickupCompletedAction();
                    break;
                case LegActionTypes.Recovery:
                    _legAction = new Recovery.RecoveryLegAction();
                    _completedLegAction = new Recovery.RecoveryCompletedLegAction();
                    break;
                case LegActionTypes.PassengerStop:
                    _legAction = new Passenger.PassengerStopLegAction();
                    _completedLegAction = new Passenger.PassengerStopCompletedLegAction();
                    break;
                case LegActionTypes.Coupling:
                case LegActionTypes.Decoupling:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new CompletedAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.BinPickupDrop:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Bin.BinDropCompletedAction();
                    break;
                case LegActionTypes.BinTransferStation:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Bin.BinTransferStationCompletedAction();
                    break;
                case LegActionTypes.TagLocation:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new TagLocationCompletedAction();
                    break;
                case LegActionTypes.Pickup:
                case LegActionTypes.Deliver:
                    _legAction = new Delivery.ItemsLegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Delivery.ItemsCompletedLegAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.AddTrailer:
                case LegActionTypes.DropTrailer:
                    _legAction = new Trailer.TrailerLegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new CompletedAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.Service:
                    _legAction = new Service.ServiceLegAction();
                    _completedLegAction = new Service.ServiceCompletedLegAction();
                    break;
                case LegActionTypes.Mass:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Delivery.MassCompletedLegAction();
                    break;
                case LegActionTypes.Alert:
                    _legAction = new Container.AlertLegAction();
                    _completedLegAction = new CompletedAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.Container:
                    _legAction = new Container.ContainerLegAction();
                    _completedLegAction = new CompletedAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.Photo:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Delivery.PhotoCompletedLegAction();
                    break;
                case LegActionTypes.Ticket:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Bus.TicketCompletedLegAction();
                    break;
                case LegActionTypes.ConcreteLoad:
                    _legAction = new Common.ProductLegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Common.ProductsCompletedLegAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.ConcretePour:
                    _legAction = new Common.ProductLegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Common.ProductsCompletedLegAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
                case LegActionTypes.PalletTransfer:
                    _legAction = new LegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Pallets.PalletTransferCompletedAction();
                    break;
                case LegActionTypes.AddAsset:
                case LegActionTypes.DropAsset:
                    _legAction = new Delivery.TrackableAssetLegAction();
                    ((LegAction)_legAction).LegActionType = t;
                    _completedLegAction = new Delivery.TrackableAssetCompletedLegAction();
                    ((CompletedAction)_completedLegAction).LegActionType = t;
                    break;
            }
            _legAction.Decode(Data, ref index);
            if (_legAction.Status == LegActionStatus.Complete || _legAction.Status == LegActionStatus.CompleteWithChanges)
            {
                _completedLegAction.Decode(Data, ref index);
            }
            else
            {
                _completedLegAction = null;
            }
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
        }
        #endregion

    }
}

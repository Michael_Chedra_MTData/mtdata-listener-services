using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// interface for a completed action
    /// </summary>
    public interface ICompetedAction : IPacketEncode
    {
        /// <summary>
        /// the Leg action type
        /// </summary>
        LegActionTypes LegActionType { get; }

        /// <summary>
        /// The Action id
        /// </summary>
        int LegActionId { get; }
    }
}

using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// the interface for Leg Time information
    /// </summary>
    public interface ILegTime : IPacketEncode
    {
        /// <summary>
        /// the required leg arrive time 
        /// </summary>
        DateTime ArriveTime { get; }
        /// <summary>
        /// the required leg depart time 
        /// </summary>
        DateTime DepartTime { get; }
        /// <summary>
        /// a tolerance in minutes around the arrive time. If not required set as -1
        /// </summary>
        int ArriveTimeTolerance { get; }
        /// <summary>
        /// a tolerance in minutes around the depart time. If not required set as -1
        /// </summary>
        int DepartTimeTolerance { get; }
        /// <summary>
        /// the delivery window start time  (minutes since midnight or -1 if not set)
        /// </summary>
        int DeliveryWindowStart { get; }
        /// <summary>
        /// the delivery window end time  (minutes since midnight or -1 if not set)
        /// </summary>
        int DeliveryWindowEnd { get; }
    }
}

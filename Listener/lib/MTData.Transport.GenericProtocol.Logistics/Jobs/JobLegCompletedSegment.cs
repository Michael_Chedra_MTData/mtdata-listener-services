using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Bin;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by the unit when a job leg is completed
    /// </summary>
    public class JobLegCompletedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private LegTypes _legType;
        private List<ICompetedAction> _completedActions;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegTypes LegType
        {
            get { return _legType; }
            set { _legType = value; }
        }
        public List<ICompetedAction> CompletedActions
        {
            get { return _completedActions; }
            set { _completedActions = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public JobLegCompletedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobLegComplete;
            _completedActions = new List<ICompetedAction>();
        }
        public JobLegCompletedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobLegComplete)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobLegComplete, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobLegComplete;
            _completedActions = new List<ICompetedAction>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legType);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                Write2ByteInteger(stream, _completedActions.Count);
                foreach (ICompetedAction item in _completedActions)
                {
                    stream.WriteByte((byte)item.LegActionType);
                    item.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _legType = (LegTypes)Data[index++];
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            for (int i = 0; i < count; i++)
            {
                LegActionTypes t = (LegActionTypes)Data[index++];
                ICompetedAction item = CreateNewItem(t);
                item.Decode(Data, ref index);
                _completedActions.Add(item);
            }
        }

        private ICompetedAction CreateNewItem(LegActionTypes t)
        {
            switch (t)
            {
                case LegActionTypes.PalletPickup:
                    return new PalletsPickupCompletedAction();
                case LegActionTypes.PalletDelivery:
                    return new PalletsDeliveryCompletedAction();
                case LegActionTypes.Questions:
                case LegActionTypes.OutOfSequence:
                    return new QuestionCompletedAction(t);
                case LegActionTypes.PalletQuestions:
                    return new PalletsQuestionCompletedAction();
                case LegActionTypes.BinPickup:
                    return new BinPickupCompletedAction();
                case LegActionTypes.BinPickupDrop:
                    return new BinDropCompletedAction();
                case LegActionTypes.BinTransferStation:
                    return new BinTransferStationCompletedAction();
                case LegActionTypes.TagLocation:
                    return new TagLocationCompletedAction();
                case LegActionTypes.Coupling:
                case LegActionTypes.Decoupling:
                case LegActionTypes.AddTrailer:
                case LegActionTypes.DropTrailer:
                case LegActionTypes.Alert:
                case LegActionTypes.Container:
                    CompletedAction action = new CompletedAction();
                    action.LegActionType = t;
                    return action;
                case LegActionTypes.ConcreteLoad:
                case LegActionTypes.ConcretePour:
                    Common.ProductsCompletedLegAction pcla = new Common.ProductsCompletedLegAction();
                    pcla.LegActionType = t;
                    return pcla;
                case LegActionTypes.Recovery:
                    return new Recovery.RecoveryCompletedLegAction();
                case LegActionTypes.PassengerStop:
                    return new Passenger.PassengerStopCompletedLegAction();
                case LegActionTypes.Pickup:
                case LegActionTypes.Deliver:
                    Delivery.ItemsCompletedLegAction itemsAction = new Delivery.ItemsCompletedLegAction();
                    itemsAction.LegActionType = t;
                    return itemsAction;
                case LegActionTypes.Service:
                    return new Service.ServiceCompletedLegAction();
                case LegActionTypes.Mass:
                    return new Delivery.MassCompletedLegAction();
                case LegActionTypes.Photo:
                    return new Delivery.PhotoCompletedLegAction();
                case LegActionTypes.Ticket:
                    return new Bus.TicketCompletedLegAction();
                case LegActionTypes.PalletTransfer:
                    return new PalletTransferCompletedAction();
                case LegActionTypes.AddAsset:
                case LegActionTypes.DropAsset:
                    Delivery.TrackableAssetCompletedLegAction asset = new Delivery.TrackableAssetCompletedLegAction();
                    asset.LegActionType = t;
                    return asset;
            }
            throw new Exception(string.Format("Unknown Leg Action Type {0}", t));
        }
        #endregion
    }
}

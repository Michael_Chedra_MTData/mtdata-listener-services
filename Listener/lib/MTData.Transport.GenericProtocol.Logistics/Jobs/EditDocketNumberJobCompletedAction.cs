using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class EditDocketNumberJobCompletedAction : CompletedJobAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private string _oldDocketNumber;
        private string _docketNumber;
        #endregion

        #region public properties
        public string OldDocketNumber
        {
            get { return _oldDocketNumber; }
            set { _oldDocketNumber = value; }
        }
        public string DocketNumber
        {
            get { return _docketNumber; }
            set { _docketNumber = value; }
        }
        #endregion

        #region constructor
        public EditDocketNumberJobCompletedAction()
        {
            JobActionType = JobActionTypes.EditDocketNumber;
        }
        public EditDocketNumberJobCompletedAction(int actionId, string oldDocketNumber, string docketNumber)
            : base(JobActionTypes.EditDocketNumber, actionId)
        {
            _oldDocketNumber = oldDocketNumber;
            _docketNumber = docketNumber;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _oldDocketNumber = BaseLayer.ReadString(data, ref index, 0x12);
            _docketNumber = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _oldDocketNumber, 0x12);
                BaseLayer.WriteString(local, _docketNumber, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

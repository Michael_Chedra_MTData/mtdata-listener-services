using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class CompletedAction : ICompetedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private LegActionTypes _legActionType;
        private int _legActionId;
        #endregion

        #region Constructor
        public CompletedAction() { }
        public CompletedAction(LegActionTypes actionType, int actionId)
        {
            _legActionId = actionId;
            _legActionType = actionType;
        }
        #endregion

        #region ICompetedAction Members

        public LegActionTypes LegActionType
        {
            get { return _legActionType; }
            set { _legActionType = value; }
        }

        public int LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }

        #endregion

        #region IPacketEncode Members

        public virtual void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _legActionType = (LegActionTypes)data[index++];
            _legActionId = BaseLayer.Read4ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public virtual void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                local.WriteByte((byte)_legActionType);
                BaseLayer.Write4ByteInteger(local, _legActionId);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

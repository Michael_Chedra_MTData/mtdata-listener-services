using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    public class JobStatusChangedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private JobStatus _jobStatus;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public JobStatus Status
        {
            get { return _jobStatus; }
            set { _jobStatus = value; }
        }
        #endregion

        #region constructor
        public JobStatusChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobStatusChanged;
        }
        public JobStatusChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobStatusChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobStatusChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobStatusChanged;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                stream.WriteByte((byte)_jobStatus);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _jobStatus = (JobStatus)Data[index++];
        }
        #endregion
    }
}

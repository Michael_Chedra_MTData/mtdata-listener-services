using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Container
{
    /// <summary>
    /// this class represents a part
    /// </summary>
    public class ContainerCargo : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 1;
        private string _consigneeCode;
        private string _consigneeName;
        private string _description;
        private int _quantity;
        private string _units;
        private string _volume;
        private string _weight;
        private string _imoCode;
        private string _unCode;
        private string _consignmentNo;
        private string _batchNo;
        private string _poNo;
        private string _masterHousebillNumber;
        private string _housebillNumber;
        private string _marksAndNumbers;
        private string _remarks;
        private int _numberOfPallets;
        private string _palletType;
        private string _shipperInvNo;
        #endregion

        #region properties
        public string ConsigneeCode
        {
            get { return _consigneeCode; }
            set { _consigneeCode = value; }
        }
        public string ConsigneeName
        {
            get { return _consigneeName; }
            set { _consigneeName = value; }
        }
        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        public int Quantity
        {
            get { return _quantity; }
            set { _quantity = value; }
        }
        public string Units
        {
            get { return _units; }
            set { _units = value; }
        }
        public string Volume
        {
            get { return _volume; }
            set { _volume = value; }
        }
        public string Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public string ImoCode
        {
            get { return _imoCode; }
            set { _imoCode = value; }
        }
        public string UnCode
        {
            get { return _unCode; }
            set { _unCode = value; }
        }
        public string ConsignmentNo
        {
            get { return _consignmentNo; }
            set { _consignmentNo = value; }
        }
        public string BatchNo
        {
            get { return _batchNo; }
            set { _batchNo = value; }
        }
        public string PoNo
        {
            get { return _poNo; }
            set { _poNo = value; }
        }
        public string MasterHousebillNumber
        {
            get { return _masterHousebillNumber; }
            set { _masterHousebillNumber = value; }
        }
        public string HousebillNumber
        {
            get { return _housebillNumber; }
            set { _housebillNumber = value; }
        }
        public string MarksAndNumbers
        {
            get { return _marksAndNumbers; }
            set { _marksAndNumbers = value; }
        }
        public string Remarks
        {
            get { return _remarks; }
            set { _remarks = value; }
        }
        public int NumberOfPallets
        {
            get { return _numberOfPallets; }
            set { _numberOfPallets = value; }
        }
        public string PalletType
        {
            get { return _palletType; }
            set { _palletType = value; }
        }
        public string ShipperInvNo
        {
            get { return _shipperInvNo; }
            set { _shipperInvNo = value; }
        }
        #endregion

        #region constructor
        public ContainerCargo()
        {
            _consigneeCode = string.Empty;
            _consigneeName = string.Empty;
            _description = string.Empty;
            _volume = string.Empty;
            _weight = string.Empty;
            _imoCode = string.Empty;
            _unCode = string.Empty;
            _consignmentNo = string.Empty;
            _batchNo = string.Empty;
            _poNo = string.Empty;
            _masterHousebillNumber = string.Empty;
            _housebillNumber = string.Empty;
            _marksAndNumbers = string.Empty;
            _remarks = string.Empty;
            _palletType = string.Empty;
            _shipperInvNo = string.Empty;
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;

            _consigneeCode = BaseLayer.ReadString(data, ref index, 0x12);
            _consigneeName = BaseLayer.ReadString(data, ref index, 0x12);
            _description = BaseLayer.ReadString(data, ref index, 0x12);
            _quantity = BaseLayer.Read4ByteInteger(data, ref index);
            _units = BaseLayer.ReadString(data, ref index, 0x12);
            _volume = BaseLayer.ReadString(data, ref index, 0x12);
            _weight = BaseLayer.ReadString(data, ref index, 0x12);
            _imoCode = BaseLayer.ReadString(data, ref index, 0x12);
            _unCode = BaseLayer.ReadString(data, ref index, 0x12);
            _consignmentNo = BaseLayer.ReadString(data, ref index, 0x12);
            _batchNo = BaseLayer.ReadString(data, ref index, 0x12);
            _poNo = BaseLayer.ReadString(data, ref index, 0x12);
            _masterHousebillNumber = BaseLayer.ReadString(data, ref index, 0x12);
            _housebillNumber = BaseLayer.ReadString(data, ref index, 0x12);
            _marksAndNumbers = BaseLayer.ReadString(data, ref index, 0x12);
            _remarks = BaseLayer.ReadString(data, ref index, 0x12);
            _numberOfPallets = BaseLayer.Read4ByteInteger(data, ref index);
            _palletType = BaseLayer.ReadString(data, ref index, 0x12);
            _shipperInvNo = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, _consigneeCode, 0x12);
                BaseLayer.WriteString(local, _consigneeName, 0x12);
                BaseLayer.WriteString(local, _description, 0x12);
                BaseLayer.Write4ByteInteger(local, _quantity);
                BaseLayer.WriteString(local, _units, 0x12);
                BaseLayer.WriteString(local, _volume, 0x12);
                BaseLayer.WriteString(local, _weight, 0x12);
                BaseLayer.WriteString(local, _imoCode, 0x12);
                BaseLayer.WriteString(local, _unCode, 0x12);
                BaseLayer.WriteString(local, _consignmentNo, 0x12);
                BaseLayer.WriteString(local, _batchNo, 0x12);
                BaseLayer.WriteString(local, _poNo, 0x12);
                BaseLayer.WriteString(local, _masterHousebillNumber, 0x12);
                BaseLayer.WriteString(local, _housebillNumber, 0x12);
                BaseLayer.WriteString(local, _marksAndNumbers, 0x12);
                BaseLayer.WriteString(local, _remarks, 0x12);
                BaseLayer.Write4ByteInteger(local, _numberOfPallets);
                BaseLayer.WriteString(local, _palletType, 0x12);
                BaseLayer.WriteString(local, _shipperInvNo, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

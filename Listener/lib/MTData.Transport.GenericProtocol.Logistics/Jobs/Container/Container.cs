using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Container
{
    /// <summary>
    /// class representing a container
    /// </summary>
    public class Container : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private string _customer;
        private string _consignee;
        private string _customerRef;
        private string _release;
        private string _isoCode;
        private string _typeSize;
        private float _weight;
        private bool _podRequired;
        private string _movementCode;
        private string _sealNo;
        private string _cargoType;
        private string _shipmentType;
        private string _timeSlot;
        private string _vessel;
        private string _vesselId;
        private string _voyage;
        private string _shippingAgent;
        private List<string> _containerRefs;
        private ContainerCargo _cargo;
        #endregion

        #region public properties
        public string Customer
        {
            get { return _customer; }
            set { _customer = value; }
        }
        public string Consignee
        {
            get { return _consignee; }
            set { _consignee = value; }
        }
        public string CustomerRef
        {
            get { return _customerRef; }
            set { _customerRef = value; }
        }
        public string Release
        {
            get { return _release; }
            set { _release = value; }
        }
        public string IsoCode
        {
            get { return _isoCode; }
            set { _isoCode = value; }
        }
        public string TypeSize
        {
            get { return _typeSize; }
            set { _typeSize = value; }
        }
        public float Weight
        {
            get { return _weight; }
            set { _weight = value; }
        }
        public bool PodRequired
        {
            get { return _podRequired; }
            set { _podRequired = value; }
        }
        public string MovementCode
        {
            get { return _movementCode; }
            set { _movementCode = value; }
        }
        public string SealNo
        {
            get { return _sealNo; }
            set { _sealNo = value; }
        }
        public string CargoType
        {
            get { return _cargoType; }
            set { _cargoType = value; }
        }
        public string ShipmentType
        {
            get { return _shipmentType; }
            set { _shipmentType = value; }
        }
        public string TimeSlot
        {
            get { return _timeSlot; }
            set { _timeSlot = value; }
        }
        public string Vessel
        {
            get { return _vessel; }
            set { _vessel = value; }
        }
        public string VesselId
        {
            get { return _vesselId; }
            set { _vesselId = value; }
        }
        public string Voyage
        {
            get { return _voyage; }
            set { _voyage = value; }
        }
        public string ShippingAgent
        {
            get { return _shippingAgent; }
            set { _shippingAgent = value; }
        }
        public List<string> ContainerRefs
        {
            get { return _containerRefs; }
            set { _containerRefs = value; }
        }
        public ContainerCargo Cargo
        {
            get { return _cargo; }
            set { _cargo = value; }
        }
        #endregion

        #region Constructor
        public Container()
        {
            _containerRefs = new List<string>();
            _cargo = new ContainerCargo();
        }
        #endregion

        #region IPacketEncode Members
        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _customer = BaseLayer.ReadString(data, ref index, 0x12);
            _consignee = BaseLayer.ReadString(data, ref index, 0x12);
            _customerRef = BaseLayer.ReadString(data, ref index, 0x12);
            _release = BaseLayer.ReadString(data, ref index, 0x12);
            _isoCode = BaseLayer.ReadString(data, ref index, 0x12);
            _typeSize = BaseLayer.ReadString(data, ref index, 0x12);
            _weight = BaseLayer.ReadFloat(data, ref index);
            _podRequired = BaseLayer.ReadBool(data, ref index);
            _movementCode = BaseLayer.ReadString(data, ref index, 0x12);
            _sealNo = BaseLayer.ReadString(data, ref index, 0x12);
            _cargoType = BaseLayer.ReadString(data, ref index, 0x12);
            _shipmentType = BaseLayer.ReadString(data, ref index, 0x12);
            _timeSlot = BaseLayer.ReadString(data, ref index, 0x12);
            _vessel = BaseLayer.ReadString(data, ref index, 0x12);
            _vesselId = BaseLayer.ReadString(data, ref index, 0x12);
            _voyage = BaseLayer.ReadString(data, ref index, 0x12);
            _shippingAgent = BaseLayer.ReadString(data, ref index, 0x12);
            int count = BaseLayer.Read2ByteInteger(data, ref index);
            _containerRefs.Clear();
            for (int i = 0; i < count; i++)
            {
                _containerRefs.Add(BaseLayer.ReadString(data, ref index, 0x12));
            }
            _cargo.Decode(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.WriteString(local, _customer, 0x12);
                BaseLayer.WriteString(local, _consignee, 0x12);
                BaseLayer.WriteString(local, _customerRef, 0x12);
                BaseLayer.WriteString(local, _release, 0x12);
                BaseLayer.WriteString(local, _isoCode, 0x12);
                BaseLayer.WriteString(local, _typeSize, 0x12);
                BaseLayer.WriteFloat(local, _weight);
                BaseLayer.WriteBool(local, _podRequired);
                BaseLayer.WriteString(local, _movementCode, 0x12);
                BaseLayer.WriteString(local, _sealNo, 0x12);
                BaseLayer.WriteString(local, _cargoType, 0x12);
                BaseLayer.WriteString(local, _shipmentType, 0x12);
                BaseLayer.WriteString(local, _timeSlot, 0x12);
                BaseLayer.WriteString(local, _vessel, 0x12);
                BaseLayer.WriteString(local, _vesselId, 0x12);
                BaseLayer.WriteString(local, _voyage, 0x12);
                BaseLayer.WriteString(local, _shippingAgent, 0x12);
                BaseLayer.Write2ByteInteger(local, _containerRefs.Count);
                foreach (string contRef in _containerRefs)
                {
                    BaseLayer.WriteString(local, contRef, 0x12);
                }
                _cargo.Encode(local);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion 
    }
}

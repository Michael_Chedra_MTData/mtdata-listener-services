using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Container
{
    /// <summary>
    /// class representing a container job
    /// </summary>
    public class ContainerJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 2;
        private string _groupId;
        private int _containerCount;
        private Container _containerInfo;
        private bool _editable;
        #endregion

        #region public properties
        public string GroupId
        {
            get { return _groupId; }
            set { _groupId = value; }
        }
        public int ContainerCount
        {
            get { return _containerCount; }
            set { _containerCount = value; }
        }
        public Container ContainerInfo
        {
            get { return _containerInfo; }
            set { _containerInfo = value; }
        }
        public bool Editable
        {
            get { return _editable; }
            set { _editable = value; }
        }
        #endregion

        #region Constructor
        public ContainerJob(JobTypes jobType)
        {
            JobType = jobType;
            if (JobType == JobTypes.ContainerBulk)
            {
                CompleteJobWhenAllLegsAreComplete = false;
            }
            else
            {
                CompleteJobWhenAllLegsAreComplete = true;
            }
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _groupId = BaseLayer.ReadString(data, ref index, 0x12);
            _containerCount = BaseLayer.Read4ByteInteger(data, ref index);
            bool containerInfroExists = BaseLayer.ReadBool(data, ref index);
            if (containerInfroExists)
            {
                _containerInfo = new Container();
                _containerInfo.Decode(data, ref index);
            }
            else
            {
                _containerInfo = null;
            }
            _editable = false;
            if (versionNumber >= 2)
            {
                _editable = BaseLayer.ReadBool(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.WriteString(local, _groupId, 0x12);
                BaseLayer.Write4ByteInteger(local, _containerCount);
                if (_containerInfo != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _containerInfo.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }
                BaseLayer.WriteBool(local, _editable);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion 
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Container
{
    /// <summary>
    /// create a new container request job
    /// </summary>
    public class CreateContainerRequestJobSegment : SegmentLayer
    {
        #region private fields
        private int _customerId;
        private ListId _customerListId;
        private List<string> _containers;
        #endregion

        #region public properties
        public int CustomerId { get { return _customerId; } set { _customerId = value; } }
        public ListId CustomerListId { get { return _customerListId; } set { _customerListId = value; } }
        public List<string> Containers { get { return _containers; } set { _containers = value; } }
        #endregion

        #region constructor
        public CreateContainerRequestJobSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.CreateContainerRequestJob;
            _customerListId = new ListId();
            _containers = new List<string>();
        }
        public CreateContainerRequestJobSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.CreateContainerRequestJob)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.CreateContainerRequestJob, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.CreateContainerRequestJob;
            Data = segment.Data;
            _customerListId = new ListId();
            _containers = new List<string>(); 
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _customerId);
                _customerListId.Encode(stream);
                Write2ByteInteger(stream, _containers.Count);
                foreach (string container in _containers)
                {
                    WriteString(stream, container, 0x12);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _customerId = Read4ByteInteger(Data, ref index);
            _customerListId.Decode(Data, ref index);
            int count = Read2ByteInteger(Data, ref index);
            _containers.Clear();
            for (int i = 0; i < count; i++)
            {
                _containers.Add(ReadString(Data, ref index, 0x12));
            }
        }
        #endregion

    }
}

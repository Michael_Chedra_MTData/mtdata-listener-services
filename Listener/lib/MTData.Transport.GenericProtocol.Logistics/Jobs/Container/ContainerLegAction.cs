using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Container
{
    public class ContainerLegAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private Container _containerInfo;
        #endregion

        #region public properties
        public Container ContainerInfo
        {
            get { return _containerInfo; }
            set { _containerInfo = value; }
        }
        #endregion

        #region constructor
        public ContainerLegAction()
        {
            LegActionType = LegActionTypes.Container;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            bool containerInfroExists = BaseLayer.ReadBool(data, ref index);
            if (containerInfroExists)
            {
                _containerInfo = new Container();
                _containerInfo.Decode(data, ref index);
            }
            else
            {
                _containerInfo = null;
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                if (_containerInfo != null)
                {
                    BaseLayer.WriteBool(local, true);
                    _containerInfo.Encode(local);
                }
                else
                {
                    BaseLayer.WriteBool(local, false);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

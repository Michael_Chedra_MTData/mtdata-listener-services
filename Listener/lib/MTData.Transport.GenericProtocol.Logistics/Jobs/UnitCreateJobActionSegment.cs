using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Common;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// used by unit to create a job action on the fly
    /// </summary>
    public class UnitCreateJobActionSegment : SegmentLayer
    {
        #region private fields
        private IJobAction _jobAction;
        private ICompletedJobAction _completedJobAction;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public IJobAction JobAction
        {
            get { return _jobAction; }
            set { _jobAction = value; }
        }
        public ICompletedJobAction CompletedJobAction
        {
            get { return _completedJobAction; }
            set { _completedJobAction = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public UnitCreateJobActionSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.UnitCreateJobAction;
        }
        public UnitCreateJobActionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.UnitCreateJobAction)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.UnitCreateJobAction, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.UnitCreateJobAction;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_jobAction.JobActionType);
                _jobAction.Encode(stream);
                if (_completedJobAction != null && _jobAction.Status == JobActionStatus.Complete)
                {
                    _completedJobAction.Encode(stream);
                }
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            JobActionTypes t = (JobActionTypes)Data[index++];
            switch (t)
            {
                case JobActionTypes.Accept:
                case JobActionTypes.Reject:
                case JobActionTypes.DispatchedQuestion:
                case JobActionTypes.Delay:
                case JobActionTypes.Start:
                case JobActionTypes.Finish:
                    _jobAction = new QuestionsJobAction();
                    _completedJobAction = new QuestionsCompletedJobAction();
                    break;
                case JobActionTypes.RequestCancel:
                    _jobAction = new JobAction();
                    _completedJobAction = new CompletedJobAction();
                    break;
                case JobActionTypes.AdditionalInfo:
                    _jobAction = new AdditionalInfoJobAction();
                    _completedJobAction = new AdditionalInfoJobCompletedAction();
                    break;
                case JobActionTypes.EditDocketNumber:
                    _jobAction = new JobAction();
                    _completedJobAction = new EditDocketNumberJobCompletedAction();
                    break;
                case JobActionTypes.Alert:
                    _jobAction = new AlertJobAction();
                    _completedJobAction = new CompletedJobAction();
                    break;
                default:
                    throw new Exception(string.Format("Unknown job action type {0}", t));
            }
            _jobAction.Decode(Data, ref index);
            if (_jobAction.Status == JobActionStatus.Complete)
            {
                _completedJobAction.Decode(Data, ref index);
            }
            else
            {
                _completedJobAction = null;
            }
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
        }
        #endregion

    }
}

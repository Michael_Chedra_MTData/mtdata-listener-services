﻿using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Bus
{
    /// <summary>
    /// enum of ticket types
    /// </summary>
    public enum TicketTypes
    {
        /// <summary>
        /// unkownn ticket type
        /// </summary>
        Unknown = -1,
        /// <summary>
        /// a single trip ticket
        /// </summary>
        Single = 0,
        /// <summary>
        /// a multi trip ticket with no expiry date
        /// </summary>
        Multi,
        /// <summary>
        /// a multi trip ticket with an expiry date
        /// </summary>
        Period
    }


    /// <summary>
    /// represents a ticket completed leg action
    /// </summary>
    public class TicketCompletedLegAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private ListId _ticketListId;
        private int _ticketId;
        private float _fare;
        private TicketTypes _ticketType;

        #endregion

        #region public properties
        public ListId TicketListId
        {
            get { return _ticketListId; }
            set { _ticketListId = value; }
        }
        public int TicketId
        {
            get { return _ticketId; }
            set { _ticketId = value; }
        }
        /// <summary>
        /// fare in cents
        /// </summary>
        public float Fare
        {
            get { return _fare; }
            set { _fare = value; }
        }
        public TicketTypes TicketType
        {
            get { return _ticketType; }
            set { _ticketType = value; }
        }
       #endregion

        #region constructor
        public TicketCompletedLegAction()
        {
            LegActionType = LegActionTypes.Ticket;
            _ticketListId = new ListId();
            _ticketType = TicketTypes.Unknown;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _ticketListId.Decode(data, ref index); 
            _ticketId = BaseLayer.Read4ByteInteger(data, ref index);
            _fare = BaseLayer.ReadFloat(data, ref index);
            _ticketType = (TicketTypes)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                _ticketListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _ticketId);
                BaseLayer.WriteFloat(local, _fare);
                local.WriteByte((byte)_ticketType);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

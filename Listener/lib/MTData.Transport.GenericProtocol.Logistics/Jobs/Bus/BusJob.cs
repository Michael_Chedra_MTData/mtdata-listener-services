using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Bus
{
    /// <summary>
    /// class represents a Bus job
    /// </summary>
    public class BusJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        
        /// <summary>
        /// the list id/version of the stop list
        /// </summary>
        private ListId _stopListId;
        /// <summary>
        /// the list id/version of the tickets list
        /// </summary>
        private ListId _ticketListId;
        private bool _createManualStops;
        private int _minDistanceBetweenStops;
        private int _defaultRadius;
        private string _singleTicketHeader;
        #endregion

        #region public properties
        public ListId StopListId
        {
            get { return _stopListId; }
            set { _stopListId = value; }
        }

        public ListId TicketListId
        {
            get { return _ticketListId; }
            set { _ticketListId = value; }
        }
        public bool CreateManualStops
        {
            get { return _createManualStops; }
            set { _createManualStops = value; }
        }
        public int MinDistanceBetweenStops
        {
            get { return _minDistanceBetweenStops; }
            set { _minDistanceBetweenStops = value; }
        }
        public int DefaultRadiusForStop
        {
            get { return _defaultRadius; }
            set { _defaultRadius = value; }
        }
        public string SingleTicketHeader
        {
            get { return _singleTicketHeader; }
            set { _singleTicketHeader = value; }
        }
        #endregion

        #region Constructor
        public BusJob()
        {
            JobType = JobTypes.Bus;
            _stopListId = new ListId();
            _ticketListId = new ListId();
            _singleTicketHeader = string.Empty;
            CompleteJobWhenAllLegsAreComplete = false;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _stopListId.Decode(data, ref index);
            _ticketListId.Decode(data, ref index);
            _createManualStops = BaseLayer.ReadBool(data, ref index);
            _minDistanceBetweenStops = BaseLayer.Read4ByteInteger(data, ref index);
            _defaultRadius = BaseLayer.Read4ByteInteger(data, ref index);
            _singleTicketHeader = BaseLayer.ReadString(data, ref index, 0x12);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }

            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _stopListId.Encode(local);
                _ticketListId.Encode(local);
                BaseLayer.WriteBool(local, _createManualStops);
                BaseLayer.Write4ByteInteger(local, _minDistanceBetweenStops);
                BaseLayer.Write4ByteInteger(local, _defaultRadius);
                BaseLayer.WriteString(local, _singleTicketHeader, 0x12);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    
    }
}

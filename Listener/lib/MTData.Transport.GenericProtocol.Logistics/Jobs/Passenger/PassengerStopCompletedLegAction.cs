using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Passenger
{
    /// <summary>
    /// represents a passenger stop completed leg action
    /// </summary>
    public class PassengerStopCompletedLegAction : CompletedAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;

        private ListId _passengerStopListId;
        private int _passengerStopId;
        private ListId _passengerListId;
        private int _passengerId;
        private ListId _organisationListId;
        private int _organisationId;
        private string _autoExternalRef;
        private string _manualExternalRef;
        private string _manualOrganisationName;
        private string _manualPassengerName;
        private PassengerStopAction _passengerStopAction;
        private bool _isValidPassengerForStopList;
        private bool _isValidPassengerForStop;

        #endregion

        #region public properties
        public ListId PassengerStopListId
        {
            get { return _passengerStopListId; }
            set { _passengerStopListId = value; }
        }
        public int PassengerStopId
        {
            get { return _passengerStopId; }
            set { _passengerStopId = value; }
        }
        public ListId PassengerListId
        {
            get { return _passengerListId; }
            set { _passengerListId = value; }
        }
        public int PassengerId
        {
            get { return _passengerId; }
            set { _passengerId = value; }
        }
        public ListId OrganisationListId
        {
            get { return _organisationListId; }
            set { _organisationListId = value; }
        }
        public int OrganisationId
        {
            get { return _organisationId; }
            set { _organisationId = value; }
        }
        public string AutoExternalRef
        {
            get { return _autoExternalRef; }
            set { _autoExternalRef = value; }
        }
        public string ManualExternalRef
        {
            get { return _manualExternalRef; }
            set { _manualExternalRef = value; }
        }
        public string ManualOrganisationName
        {
            get { return _manualOrganisationName; }
            set { _manualOrganisationName = value; }
        }
        public string ManualPassengerName
        {
            get { return _manualPassengerName; }
            set { _manualPassengerName = value; }
        }
        public PassengerStopAction PassengerStopAction
        {
            get { return _passengerStopAction; }
            set { _passengerStopAction = value; }
        }
        public bool IsValidPassengerForStopList
        {
            get { return _isValidPassengerForStopList; }
            set { _isValidPassengerForStopList = value; }
        }
        public bool IsValidPassengerForStop
        {
            get { return _isValidPassengerForStop; }
            set { _isValidPassengerForStop = value; }
        }
        
       #endregion

        #region constructor
        public PassengerStopCompletedLegAction()
        {
            LegActionType = LegActionTypes.PassengerStop;
            _passengerStopListId = new ListId();
            _passengerListId = new ListId();
            _organisationListId = new ListId();
            _autoExternalRef = string.Empty;
            _manualExternalRef =  string.Empty;
            _manualOrganisationName = string.Empty;
            _manualPassengerName = string.Empty;
            _passengerStopAction = PassengerStopAction.Unknown;
            _isValidPassengerForStopList = true;
            _isValidPassengerForStop = true;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);

            _passengerStopListId.Decode(data, ref index); 
            _passengerStopId = BaseLayer.Read4ByteInteger(data, ref index);
            _passengerListId.Decode(data, ref index);
            _passengerId = BaseLayer.Read4ByteInteger(data, ref index);
            _organisationListId.Decode(data, ref index);
            _organisationId = BaseLayer.Read4ByteInteger(data, ref index);
            _autoExternalRef = BaseLayer.ReadString(data, ref index, 0x12);
            _manualExternalRef = BaseLayer.ReadString(data, ref index, 0x12);
            _manualOrganisationName = BaseLayer.ReadString(data, ref index, 0x12);
            _manualPassengerName = BaseLayer.ReadString(data, ref index, 0x12);
            _passengerStopAction = (PassengerStopAction)data[index++];
            _isValidPassengerForStopList = BaseLayer.ReadBool(data, ref index);
            _isValidPassengerForStop = BaseLayer.ReadBool(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);

                _passengerStopListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _passengerStopId);
                _passengerListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _passengerId);
                _organisationListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _organisationId);
                BaseLayer.WriteString(local, _autoExternalRef, 0x12);
                BaseLayer.WriteString(local, _manualExternalRef, 0x12);
                BaseLayer.WriteString(local, _manualOrganisationName, 0x12);
                BaseLayer.WriteString(local, _manualPassengerName, 0x12);
                local.WriteByte((byte)_passengerStopAction);
                BaseLayer.WriteBool(local, _isValidPassengerForStopList);
                BaseLayer.WriteBool(local, _isValidPassengerForStop);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Passenger
{
    /// <summary>
    /// class represents a Passenger job
    /// </summary>
    public class PassengerJob : Job
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 3;
        
        /// <summary>
        /// the list id/version of the passenger stop list
        /// </summary>
        private ListId _passengerStopListId;
        /// <summary>
        /// the list id/version of the passenger list
        /// </summary>
        private List<PassengerListItem> _passengerLists;
        private bool _createManualStops;
        private int _minDistanceBetweenStops;
        private int _defaultRadius;
        private bool _passengerManifestJob;
        #endregion

        #region public properties
        public ListId PassengerStopListId
        {
            get { return _passengerStopListId; }
            set { _passengerStopListId = value; }
        }

        public List<PassengerListItem> PassengerLists
        {
            get { return _passengerLists; }
            set { _passengerLists = value; }
        }
        public bool CreateManualStops
        {
            get { return _createManualStops; }
            set { _createManualStops = value; }
        }
        public int MinDistanceBetweenStops
        {
            get { return _minDistanceBetweenStops; }
            set { _minDistanceBetweenStops = value; }
        }
        public int DefaultRadiusForStop
        {
            get { return _defaultRadius; }
            set { _defaultRadius = value; }
        }

        public bool PassengerManifestJob
        {
            get { return _passengerManifestJob; }
            set { _passengerManifestJob = value; }
        }
        #endregion

        #region Constructor
        public PassengerJob()
        {
            JobType = JobTypes.Passenger;
            _passengerStopListId = new ListId();
            _passengerLists = new List<PassengerListItem>();
            CompleteJobWhenAllLegsAreComplete = false;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _passengerStopListId.Decode(data, ref index);

            int passengerListCount = BaseLayer.Read2ByteInteger(data, ref index);
            _passengerLists.Clear();
            for (int i = 0; i < passengerListCount; i++)
            {
                PassengerListItem passengerlist = new PassengerListItem();
                passengerlist.Decode(data, ref index);
                _passengerLists.Add(passengerlist);
            }
            _createManualStops = BaseLayer.ReadBool(data, ref index);
            _minDistanceBetweenStops = BaseLayer.Read4ByteInteger(data, ref index);
            _defaultRadius = 50;
            if (versionNumber >= 2)
            {
                _defaultRadius = BaseLayer.Read4ByteInteger(data, ref index);
            }

            if (versionNumber >= 3)
            {
                _passengerManifestJob = BaseLayer.ReadBool(data, ref index);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
            CheckJobStatus();
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _passengerStopListId.Encode(local);

                BaseLayer.Write2ByteInteger(local, _passengerLists.Count);
                foreach (PassengerListItem passengerlist in _passengerLists)
                {
                    passengerlist.Encode(local);
                }
                BaseLayer.WriteBool(local, _createManualStops);
                BaseLayer.Write4ByteInteger(local, _minDistanceBetweenStops);
                BaseLayer.Write4ByteInteger(local, _defaultRadius);
                BaseLayer.WriteBool(local, _passengerManifestJob);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);

            }
        }
        #endregion
    
    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Passenger
{
    public class PassengerListItem : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private ListId _passengerListId;
        private ListId _organisationListId;
        private int _organisationId;
        #endregion
        #region public properties
        public ListId PassengerListId
        {
            get { return _passengerListId; }
            set { _passengerListId = value; }
        }

        public ListId OrganisationListId
        {
            get { return _organisationListId; }
            set { _organisationListId = value; }
        }
        public int OrganisationId
        {
            get { return _organisationId; }
            set { _organisationId = value; }
        }
        #endregion

        #region constructor
        public PassengerListItem()
        {
            _organisationListId = new ListId();
            _passengerListId = new ListId();
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _passengerListId.Decode(data, ref index);
            _organisationListId.Decode(data, ref index);
            _organisationId = BaseLayer.Read4ByteInteger(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                _passengerListId.Encode(local);
                _organisationListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _organisationId);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }

        #endregion
    }
}

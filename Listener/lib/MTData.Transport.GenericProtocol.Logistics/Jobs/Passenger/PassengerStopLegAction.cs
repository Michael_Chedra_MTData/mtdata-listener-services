using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Passenger
{
    /// <summary>
    /// leg action types
    /// </summary>
    public enum PassengerStopAction
    {
        Unknown,
        Board,
        Disembark,
        Absent
    }
    
    /// <summary>
    /// class represents a passenger stop leg action
    /// </summary>
    public class PassengerStopLegAction : LegAction
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private ListId _passengerListId;
        private int _passengerId;
        private ListId _organisationListId;
        private int _organisationId;
        private PassengerStopAction _passengerStopAction;
        #endregion

        #region public properties
        public ListId PassengerListId
        {
            get { return _passengerListId; }
            set { _passengerListId = value; }
        }
        public int PassengerId
        {
            get { return _passengerId; }
            set { _passengerId = value; }
        }
        public ListId OrganisationListId
        {
            get { return _organisationListId; }
            set { _organisationListId = value; }
        }
        public int OrganisationId
        {
            get { return _organisationId; }
            set { _organisationId = value; }
        }
        public PassengerStopAction PassengerStopAction
        {
            get { return _passengerStopAction; }
            set { _passengerStopAction = value; }
        }
        #endregion

        #region constructor
        public PassengerStopLegAction()
        {
            LegActionType = LegActionTypes.PassengerStop;
            _passengerListId = new ListId();
            _passengerId = -1;
            _organisationListId = new ListId();
            _organisationId = -1;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _passengerListId.Decode(data, ref index);
            _passengerId = BaseLayer.Read4ByteInteger(data, ref index);
            _organisationListId.Decode(data, ref index);
            _organisationId = BaseLayer.Read4ByteInteger(data, ref index);
            _passengerStopAction = (PassengerStopAction)data[index++];

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }
        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                _passengerListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _passengerId);
                _organisationListId.Encode(local);
                BaseLayer.Write4ByteInteger(local, _organisationId);
                local.WriteByte((byte)_passengerStopAction);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

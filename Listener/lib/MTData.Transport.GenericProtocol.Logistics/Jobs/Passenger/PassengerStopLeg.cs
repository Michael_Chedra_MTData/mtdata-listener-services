using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs.Passenger
{
    /// <summary>
    /// class that represents a passenger stop leg
    /// </summary>
    public class PassengerStopLeg : Leg
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private int _passengerStopId;
        /// <summary>
        /// the list id/version of the passenger list
        /// </summary>
        private List<PassengerListItem> _passengerLists;
        #endregion

        #region public properties
        public int PassengerStopId
        {
            get { return _passengerStopId; }
            set { _passengerStopId = value; }
        }
        public List<PassengerListItem> PassengerLists
        {
            get { return _passengerLists; }
            set { _passengerLists = value; }
        }
        #endregion

        #region constructor
        public PassengerStopLeg()
        {
            LegType = LegTypes.PassengerStop;
            _passengerLists = new List<PassengerListItem>();
            CompleteLegWhenAllActionsAreComplete = false;
        }
        #endregion

        #region override methods
        public override void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            base.Decode(data, ref index);
            _passengerStopId = BaseLayer.Read4ByteInteger(data, ref index);

            int passengerListCount = BaseLayer.Read2ByteInteger(data, ref index);
            _passengerLists.Clear();
            for (int i = 0; i < passengerListCount; i++)
            {
                PassengerListItem passengerlist = new PassengerListItem();
                passengerlist.Decode(data, ref index);
                _passengerLists.Add(passengerlist);
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public override void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                base.Encode(local);
                BaseLayer.Write4ByteInteger(local, _passengerStopId);
                BaseLayer.Write2ByteInteger(local, _passengerLists.Count);
                foreach (PassengerListItem passengerlist in _passengerLists)
                {
                    passengerlist.Encode(local);
                }

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
        #endregion
    }
}

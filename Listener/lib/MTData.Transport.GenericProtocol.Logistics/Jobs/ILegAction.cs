using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// leg action types
    /// </summary>
    public enum LegActionTypes
    {
        /// <summary>
        ///  Questions leg action type
        /// </summary>
        Questions = 0,
        /// <summary>
        ///  Pallet pickup leg action type
        /// </summary>
        PalletPickup = 1,
        /// <summary>
        ///  Pallet delivery leg action type
        /// </summary>
        PalletDelivery = 2,
        /// <summary>
        ///  Pallet question leg action type
        /// </summary>
        PalletQuestions = 3,
        /// <summary>
        ///  Trailer coupling leg action type
        /// </summary>
        Coupling = 4,
        /// <summary>
        ///  Trailer decoupling leg action type
        /// </summary>
        Decoupling = 5,
        /// <summary>
        ///  Bin pickup leg action type
        /// </summary>
        BinPickup = 6,
        /// <summary>
        ///  Bin pickup drop leg action type
        /// </summary>
        BinPickupDrop = 7,
        /// <summary>
        ///  Bin transfer station leg action type
        /// </summary>
        BinTransferStation = 8,
        /// <summary>
        ///  Add driver leg action type
        /// </summary>
        AddDriver = 9,
        /// <summary>
        ///  Drop driver leg action type
        /// </summary>
        DropDriver = 10,
        /// <summary>
        ///  Add trailer leg action type
        /// </summary>
        AddTrailer = 11,
        /// <summary>
        ///  Drop trailer leg action type
        /// </summary>
        DropTrailer = 12,
        /// <summary>
        ///  Tag location leg action type
        /// </summary>
        TagLocation = 13,
        /// <summary>
        ///  Recovery leg action type
        /// </summary>
        Recovery = 14,
        /// <summary>
        /// passenger stop action type
        /// </summary>
        PassengerStop = 15,
        /// <summary>
        /// pickup action type
        /// </summary>
        Pickup = 16,
        /// <summary>
        /// deliver action type
        /// </summary>
        Deliver = 17,
        /// <summary>
        /// activate route action type
        /// </summary>
        ActivateRoute = 18,
        /// <summary>
        /// Service action type
        /// </summary>
        Service = 19,
        /// <summary>
        /// Mass declaration action type
        /// </summary>
        Mass = 20,
        /// <summary>
        /// Container action type
        /// </summary>
        Container = 21,
        /// <summary>
        /// Alert action type
        /// </summary>
        Alert = 22,
        /// <summary>
        /// Photo Leg Action
        /// </summary>
        Photo = 23,
        /// <summary>
        /// Ticket leg action
        /// </summary>
        Ticket = 24,
        /// <summary>
        /// Concrete loading action
        /// </summary>
        ConcreteLoad = 25,
        /// <summary>
        /// Concrete pouring action
        /// </summary>
        ConcretePour = 26,
        /// <summary>
        /// Leg has been done out of Sequence
        /// </summary>
        OutOfSequence = 27,
        /// <summary>
        /// Trackable Asset Pickup
        /// </summary>
        AddAsset = 28,
        /// <summary>
        /// Trackable Asset DropOff
        /// </summary>
        DropAsset = 29,
        /// <summary>
        /// Pallet Transfer
        /// </summary>
        PalletTransfer = 30
    }

    /// <summary>
    /// Action states
    /// </summary>
    public enum LegActionStatus
    {
        /// <summary>
        ///  Not started status
        /// </summary>
        NotStarted = 0,
        /// <summary>
        ///  in progress status
        /// </summary>
        InProgress = 1,
        /// <summary>
        ///  complete status
        /// </summary>
        Complete = 2,
        /// <summary>
        ///  complete with changes status
        /// </summary>
        CompleteWithChanges = 3
    }

    /// <summary>
    /// delegate for leg action events
    /// </summary>
    /// <param name="leg"></param>
    public delegate void LegActionEvent(ILegAction legAction);

    /// <summary>
    /// The Interface for a Leg Action
    /// </summary>
    public interface ILegAction : IPacketEncode
    {
        /// <summary>
        /// the Leg action type
        /// </summary>
        LegActionTypes LegActionType { get; }

        /// <summary>
        /// The id of the leg the leg action belongs to
        /// </summary>
        int LegId { get; }

        /// <summary>
        /// The Action id
        /// </summary>
        int LegActionId { get; }

        /// <summary>
        /// the leg action number (used for sequencing)
        /// </summary>
        int LegActionNumber { get; }

        /// <summary>
        /// the action title
        /// </summary>
        string Title { get; }
        /// <summary>
        /// the notes assoicated with the action, can be null
        /// </summary>
        string Notes { get; }

        /// <summary>
        /// Any Custom Data to be displayed to the driver
        /// </summary>
        Dictionary<string, string> CustomData { get; }

        /// <summary>
        /// the leg action status
        /// </summary>
        LegActionStatus Status { get; set;}

        /// <summary>
        /// get an external reference for the action
        /// </summary>
        string ExternalReference { get; }

        /// <summary>
        /// event for when status of the leg action has changed
        /// </summary>
        event LegActionEvent OnLegActionStatusChanged;
    }
}

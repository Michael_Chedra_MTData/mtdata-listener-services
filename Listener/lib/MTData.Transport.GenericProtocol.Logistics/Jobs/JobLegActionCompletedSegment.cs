using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Pallets;
using MTData.Transport.GenericProtocol.Logistics.Jobs.Bin;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// sent by the unit when a job leg action is completed
    /// </summary>
    public class JobLegActionCompletedSegment : SegmentLayer
    {
        #region private fields
        private int _jobDispatchId;
        private int _legId;
        private ICompetedAction _completedAction;
        private int _odometer;
        private int _totalFuel;
        #endregion

        #region properties
        public int JobDispatchId
        {
            get { return _jobDispatchId; }
            set { _jobDispatchId = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public ICompetedAction CompletedAction
        {
            get { return _completedAction; }
            set { _completedAction = value; }
        }
        public int Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }
        #endregion

        #region constructor
        public JobLegActionCompletedSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.JobLegActionComplete;
        }
        public JobLegActionCompletedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.JobLegActionComplete)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.JobLegActionComplete, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.JobLegActionComplete;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _jobDispatchId);
                Write4ByteInteger(stream, _legId);
                Write4ByteInteger(stream, _odometer);
                Write4ByteInteger(stream, _totalFuel);
                stream.WriteByte((byte)_completedAction.LegActionType);
                _completedAction.Encode(stream);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _jobDispatchId = Read4ByteInteger(Data, ref index);
            _legId = Read4ByteInteger(Data, ref index);
            _odometer = Read4ByteInteger(Data, ref index);
            _totalFuel = Read4ByteInteger(Data, ref index);
            LegActionTypes t = (LegActionTypes)Data[index++];
            _completedAction = CreateNewItem(t);
            _completedAction.Decode(Data, ref index);
        }

        private ICompetedAction CreateNewItem(LegActionTypes t)
        {
            switch (t)
            {
                case LegActionTypes.PalletPickup:
                    return new PalletsPickupCompletedAction();
                case LegActionTypes.PalletDelivery:
                    return new PalletsDeliveryCompletedAction();
                case LegActionTypes.Questions:
                case LegActionTypes.OutOfSequence:
                    return new QuestionCompletedAction(t);
                case LegActionTypes.PalletQuestions:
                    return new PalletsQuestionCompletedAction();
                case LegActionTypes.BinPickup:
                    return new BinPickupCompletedAction();
                case LegActionTypes.BinPickupDrop:
                    return new BinDropCompletedAction();
                case LegActionTypes.BinTransferStation:
                    return new BinTransferStationCompletedAction();
                case LegActionTypes.TagLocation:
                    return new TagLocationCompletedAction();
                case LegActionTypes.Coupling:
                case LegActionTypes.Decoupling:
                case LegActionTypes.AddTrailer:
                case LegActionTypes.DropTrailer:
                case LegActionTypes.Alert:
                case LegActionTypes.Container:
                    CompletedAction action = new CompletedAction();
                    action.LegActionType = t;
                    return action;
                case LegActionTypes.Recovery:
                    return new Recovery.RecoveryCompletedLegAction();
                case LegActionTypes.PassengerStop:
                    return new Passenger.PassengerStopCompletedLegAction();
                case LegActionTypes.Pickup:
                case LegActionTypes.Deliver:
                    Delivery.ItemsCompletedLegAction itemsAction = new Delivery.ItemsCompletedLegAction();
                    itemsAction.LegActionType = t;
                    return itemsAction;
                case LegActionTypes.Service:
                    return new Service.ServiceCompletedLegAction();
                case LegActionTypes.Mass:
                    return new Delivery.MassCompletedLegAction();
                case LegActionTypes.Photo:
                    return new Delivery.PhotoCompletedLegAction();
                case LegActionTypes.Ticket:
                    return new Bus.TicketCompletedLegAction();
                case LegActionTypes.ConcreteLoad:
                case LegActionTypes.ConcretePour:
                    return new Common.ProductsCompletedLegAction();
                case LegActionTypes.PalletTransfer:
                    return new PalletTransferCompletedAction();
                case LegActionTypes.AddAsset:
                case LegActionTypes.DropAsset:
                    Delivery.TrackableAssetCompletedLegAction asset = new Delivery.TrackableAssetCompletedLegAction();
                    asset.LegActionType = t;
                    return asset;
            }
            throw new Exception(string.Format("Unknown Lag Action Type {0}", t));
        }
        #endregion
    }
}

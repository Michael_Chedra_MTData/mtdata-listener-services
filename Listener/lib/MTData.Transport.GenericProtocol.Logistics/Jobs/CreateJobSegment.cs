using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.Jobs
{
    /// <summary>
    /// used by base to send to a unit to a new job
    /// </summary>
    public class CreateJobSegment : SegmentLayer
    {
        public static int JOB_WAS_NOT_CREATED_ON_UNIT = 0;

        #region private fields
        private int _allocationId;
        private IJob _job;
        /// <summary>
        /// only used if job was a templated job created on a unit - this is the id the job currently has on the unit
        /// </summary>
        private int _originalDispatchId;
        #endregion

        #region properties
        public int AllocationId
        {
            get { return _allocationId; }
            set { _allocationId = value; }
        }
        public IJob Job
        {
            get { return _job; }
            set { _job = value; }
        }
        public int OriginalDispatchId
        {
            get { return _originalDispatchId; }
            set { _originalDispatchId = value; }
        }
        #endregion

        #region constructor
        public CreateJobSegment()
        {
            Version = 2;
            Type = (int)LogisticsSegmentTypes.CreateJob;
            _originalDispatchId = JOB_WAS_NOT_CREATED_ON_UNIT;
        }
        public CreateJobSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.CreateJob)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.CreateJob, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.CreateJob;
            Data = segment.Data;
            _originalDispatchId = JOB_WAS_NOT_CREATED_ON_UNIT;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _allocationId);
                stream.WriteByte((byte)_job.JobType);
                _job.Encode(stream);
                if (Version >= 2)
                {
                    Write4ByteInteger(stream, _originalDispatchId);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _allocationId = Read4ByteInteger(Data, ref index);
            JobTypes t = (JobTypes)Data[index++];
            switch (t)
            {
                case JobTypes.Pallets:
                    _job = new Pallets.PalletsJob();
                    break;
                case JobTypes.Bin:
                    _job = new Bin.BinJob();
                    break;
                case JobTypes.Simple:
                    _job = new Job();
                    ((Job)_job).JobType = t;
                    break;
                case JobTypes.Recovery:
                    _job = new Recovery.RecoveryJob();
                    break;
                case JobTypes.Passenger:
                    _job = new Passenger.PassengerJob();
                    break;
                case JobTypes.Delivery:
                    _job = new Delivery.DeliveryJob();
                    break;
                case JobTypes.Trailer:
                    _job = new Trailer.TrailerJob();
                    break;
                case JobTypes.Service:
                    _job = new Service.ServiceJob();
                    break;
                case JobTypes.Container:
                case JobTypes.ContainerBulk:
                    _job = new Container.ContainerJob(t);
                    break;
                case JobTypes.Bus:
                    _job = new Bus.BusJob();
                    break;
                case JobTypes.Concrete:
                    _job = new Concrete.ConcreteJob();
                    break;
                default:
                    throw new Exception(string.Format("Unknown job type {0}", t));
            }
            _job.Decode(Data, ref index);
            if (Version >= 2)
            {
                _originalDispatchId = Read4ByteInteger(Data, ref index);
            }
        }
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public enum SafeJourneyEventType : byte
    {
        Created,
        Sent,
        ReceivedByUnit,
        DriverDeclared,
        DriverDeclaredWithAmends,
        DriverChangeAcknowledged,
        Completed,
        Rejected, // Rejected by driver
        Cancelled, // Cancelled by user
        CancelReceivedByUnit,
        SendFailure
    }
}

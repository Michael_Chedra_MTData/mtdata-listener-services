﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public enum SafeJourneyAnswerType : byte
    {
        PreJourney,
        PostJourney
    }
}

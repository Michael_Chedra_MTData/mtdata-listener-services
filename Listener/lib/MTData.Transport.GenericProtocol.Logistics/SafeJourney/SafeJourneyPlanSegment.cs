﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SafeJourneyPlanSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SafeJourneyPlan;

        private int _id;

        private int _eventId;

        private SafeJourneyStatus _status;

        private SafeJourneyAmendFlag _amendFlag;

        private SafeJourneyPlanAddress _consignor;

        private SafeJourneyPlanAddress _consignee;

        private SafeJourneyPlanAddress _contractor;

        private SafeJourneyPlanAddress _pickupAddress;

        private SafeJourneyPlanAddress _deliveryAddress;

        private ListId _startQuestionListId;

        private ListId _endQuestionListId;

        private short _nonDrivingWorkMinsEstimate;

        private int _drivingMinsEstimate;

        private short _shortRestMinsEstimate;

        private short _longRestMinsEstimate;

        private DateTime _startEstimate;

        private int _distanceMetresEstimate;

        private string _comments;

        private List<SafeJourneyTrailerAssets> _trailerAssets = new List<SafeJourneyTrailerAssets>();

        private List<int> _drivers = new List<int>();

        private List<SafeJourneyTask> _tasks = new List<SafeJourneyTask>();

        public SafeJourneyPlanSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SafeJourneyPlanSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public int Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        public SafeJourneyStatus Status
        {
            get { return _status; }
            set { _status = value; }
        }

        public SafeJourneyPlanAddress Consignor
        {
            get { return _consignor; }
            set { _consignor = value; }
        }

        public SafeJourneyPlanAddress Consignee
        {
            get { return _consignee; }
            set { _consignee = value; }
        }

        public SafeJourneyPlanAddress Contractor
        {
            get { return _contractor; }
            set { _contractor = value; }
        }

        public SafeJourneyPlanAddress PickupAddress
        {
            get { return _pickupAddress; }
            set { _pickupAddress = value; }
        }

        public SafeJourneyPlanAddress DeliveryAddress
        {
            get { return _deliveryAddress; }
            set { _deliveryAddress = value; }
        }

        public ListId StartQuestionListId
        {
            get { return _startQuestionListId; }
            set { _startQuestionListId = value; }
        }

        public ListId EndQuestionListId
        {
            get { return _endQuestionListId; }
            set { _endQuestionListId = value; }
        }

        public short NonDrivingWorkMinsEstimate
        {
            get { return _nonDrivingWorkMinsEstimate; }
            set { _nonDrivingWorkMinsEstimate = value; }
        }

        public int DrivingMinsEstimate
        {
            get { return _drivingMinsEstimate; }
            set { _drivingMinsEstimate = value; }
        }

        public short ShortRestMinsEstimate
        {
            get { return _shortRestMinsEstimate; }
            set { _shortRestMinsEstimate = value; }
        }
        public short LongRestMinsEstimate
        {
            get { return _longRestMinsEstimate; }
            set { _longRestMinsEstimate = value; }
        }

        public DateTime StartEstimate
        {
            get { return _startEstimate; }
            set { _startEstimate = value; }
        }

        public DateTime EndEstimate
        {
            get { return _startEstimate.AddMinutes(_nonDrivingWorkMinsEstimate + _drivingMinsEstimate + _shortRestMinsEstimate + _longRestMinsEstimate); }
        }

        public int DistanceMetresEstimate
        {
            get { return _distanceMetresEstimate; }
            set { _distanceMetresEstimate = value; }
        }

        public string Comments
        {
            get { return _comments; }
            set { _comments = value; }
        }

        public decimal? AverageSpeedKmhEstimate
        {
            get { return (_drivingMinsEstimate != 0) ? ((60m * _distanceMetresEstimate) / (_drivingMinsEstimate * 1000m)) : (decimal?)null; }
        }

        public List<SafeJourneyTrailerAssets> TrailerAssets
        {
            get { return _trailerAssets; }
            set { _trailerAssets = value; }
        }

        public List<int> Drivers
        {
            get { return _drivers; }
            set { _drivers = value; }
        }

        public SafeJourneyAmendFlag AmendFlag
        {
            get { return _amendFlag; }
            set { _amendFlag = value; }
        }

        public List<SafeJourneyTask> Tasks
        {
            get { return _tasks; }
            set { _tasks = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {

                Write4ByteInteger(stream, _id);
                Write4ByteInteger(stream, _eventId);
                stream.WriteByte((byte)_status);
                stream.WriteByte((byte)_amendFlag);


                int fieldsFlags = (_consignor != null ? 1 << 0 : 0)
                    | (_consignee != null ? 1 << 1 : 0)
                    | (_contractor != null ? 1 << 2 : 0)
                    | (_pickupAddress != null ? 1 << 3 : 0)
                    | (_deliveryAddress != null ? 1 << 4 : 0)
                    | (_startQuestionListId != null ? 1 << 5 : 0)
                    | (_endQuestionListId != null ? 1 << 6 : 0);

                WriteMoreFlag(stream, fieldsFlags);

                if (_consignor != null)
                {
                    _consignor.Encode(stream);
                }

                if (_consignee != null)
                {
                    _consignee.Encode(stream);
                }

                if (_contractor != null)
                {
                    _contractor.Encode(stream);
                }

                if (_pickupAddress != null)
                {
                    _pickupAddress.Encode(stream);
                }

                if (_deliveryAddress != null)
                {
                    _deliveryAddress.Encode(stream);
                }

                if (_startQuestionListId != null)
                {
                    _startQuestionListId.Encode(stream);
                }

                if (_endQuestionListId != null)
                {
                    _endQuestionListId.Encode(stream);
                }

                Write2ByteInteger(stream, _nonDrivingWorkMinsEstimate);
                Write4ByteInteger(stream, _drivingMinsEstimate);
                Write2ByteInteger(stream, _shortRestMinsEstimate);
                Write2ByteInteger(stream, _longRestMinsEstimate);
                WriteDateTime(stream, _startEstimate);
                Write4ByteInteger(stream, _distanceMetresEstimate);
                WriteString(stream, _comments, 0x12);

                WriteMoreFlag(stream, (_trailerAssets != null) ? _trailerAssets.Count : 0);

                foreach (SafeJourneyTrailerAssets trailer in _trailerAssets)
                {
                    trailer.Encode(stream);
                }

                WriteMoreFlag(stream, (_drivers != null) ? _drivers.Count : 0);

                foreach (int driverID in _drivers)
                {
                    Write4ByteInteger(stream, driverID);
                }


                WriteMoreFlag(stream, (_tasks != null) ? _tasks.Count : 0);

                foreach (SafeJourneyTask task in _tasks)
                {
                    task.Encode(stream);
                }


                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            _id = Read4ByteInteger(Data, ref index);
            _eventId = Read4ByteInteger(Data, ref index);
            _status = (SafeJourneyStatus)Data[index++];
            _amendFlag = (SafeJourneyAmendFlag)Data[index++];

            int fieldsFlags = ReadMoreFlag(Data, ref index);

            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _consignor = new SafeJourneyPlanAddress();
                _consignor.Decode(Data, ref index);
            }

            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _consignee = new SafeJourneyPlanAddress();
                _consignee.Decode(Data, ref index);
            }

            if ((fieldsFlags & (1 << 2)) != 0)
            {
                _contractor = new SafeJourneyPlanAddress();
                _contractor.Decode(Data, ref index);
            }

            if ((fieldsFlags & (1 << 3)) != 0)
            {
                _pickupAddress = new SafeJourneyPlanAddress();
                _pickupAddress.Decode(Data, ref index);
            }

            if ((fieldsFlags & (1 << 4)) != 0)
            {
                _deliveryAddress = new SafeJourneyPlanAddress();
                _deliveryAddress.Decode(Data, ref index);
            }


            if ((fieldsFlags & (1 << 5)) != 0)
            {
                _startQuestionListId = new ListId();
                _startQuestionListId.Decode(Data, ref index);
            }

            if ((fieldsFlags & (1 << 6)) != 0)
            {
                _endQuestionListId = new ListId();
                _endQuestionListId.Decode(Data, ref index);
            }

            _nonDrivingWorkMinsEstimate = (short)Read2ByteInteger(Data, ref index);
            _drivingMinsEstimate = Read4ByteInteger(Data, ref index);
            _shortRestMinsEstimate = (short)Read2ByteInteger(Data, ref index);
            _longRestMinsEstimate = (short)Read2ByteInteger(Data, ref index);
            _startEstimate = ReadDateTime(Data, ref index);
            _distanceMetresEstimate = Read4ByteInteger(Data, ref index);
            _comments = ReadString(Data, ref index, 0x12);

            int count = ReadMoreFlag(Data, ref index);
            List<SafeJourneyTrailerAssets> trailerAssets = new List<SafeJourneyTrailerAssets>();
            for (int i = 0; i < count; i++)
            {
                SafeJourneyTrailerAssets t = new SafeJourneyTrailerAssets();
                t.Decode(Data, ref index);
                trailerAssets.Add(t);
            }
            _trailerAssets = trailerAssets;


            count = ReadMoreFlag(Data, ref index);
            List<int> drivers = new List<int>();
            for (int i = 0; i < count; i++)
            {
                int driverID = Read4ByteInteger(Data, ref index);
                drivers.Add(driverID);
            }
            _drivers = drivers;

            count = ReadMoreFlag(Data, ref index);
            List<SafeJourneyTask> tasks = new List<SafeJourneyTask>();
            for (int i = 0; i < count; i++)
            {
                SafeJourneyTask t = new SafeJourneyTask();
                t.Decode(Data, ref index);
                tasks.Add(t);
            }
            _tasks = tasks;
        }
    }
}

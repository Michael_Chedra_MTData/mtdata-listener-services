﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SafeJourneySimpleEventSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SafeJourneySimpleEvent;

        private int _sjpId;

        private SafeJourneyEventType _eventType;

        private AnsweredQuestions _answers;

        public SafeJourneySimpleEventSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SafeJourneySimpleEventSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }


        public int SjpId
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }

        public SafeJourneyEventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public AnsweredQuestions Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _sjpId);
                stream.WriteByte((byte)_eventType);

                int fieldsFlags = (_answers != null ? 1 << 0 : 0);
                WriteMoreFlag(stream, fieldsFlags);

                if (_answers != null)
                {
                    _answers.Encode(stream);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;

            _sjpId = Read4ByteInteger(Data, ref index);
            _eventType = (SafeJourneyEventType)Data[index++];
            int fieldsFlags = ReadMoreFlag(Data, ref index);

            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _answers = new AnsweredQuestions();
                _answers.Decode(Data, ref index);
            }
            else
            {
                _answers = null;
            }

        }

    }
}

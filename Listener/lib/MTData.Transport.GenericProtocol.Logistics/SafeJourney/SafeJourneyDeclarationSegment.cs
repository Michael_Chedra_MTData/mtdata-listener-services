﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SafeJourneyDeclarationSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SafeJourneyDeclaration;

        private int _sjpId;

        private SafeJourneyAmendFlag _amendFlag;

        private SafeJourneyPlanAddress _consignor;

        private SafeJourneyPlanAddress _consignee;

        private SafeJourneyPlanAddress _contractor;

        private SafeJourneyPlanAddress _pickupAddress;

        private SafeJourneyPlanAddress _deliveryAddress;

        private short _nonDrivingWorkMins;

        private int _drivingMins;

        private short _longRestMins;

        private short _shortRestMins;

        private DateTime _start;

        private int _distanceMetres;

        private List<SafeJourneyTrailerAssets> _trailerAssets = new List<SafeJourneyTrailerAssets>();

        private List<SafeJourneyTask> _tasks = new List<SafeJourneyTask>();

        private AnsweredQuestions _answers;

        public SafeJourneyDeclarationSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SafeJourneyDeclarationSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public int SjpId
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }

        public List<SafeJourneyTrailerAssets> TrailerAssets
        {
            get { return _trailerAssets; }
            set { _trailerAssets = value; }
        }

        public short NonDrivingWorkMins
        {
            get { return _nonDrivingWorkMins; }
            set { _nonDrivingWorkMins = value; }
        }

        public int DrivingMins
        {
            get { return _drivingMins; }
            set { _drivingMins = value; }
        }

        public short LongRestMins
        {
            get { return _longRestMins; }
            set { _longRestMins = value; }
        }

        public short ShortRestMins
        {
            get { return _shortRestMins; }
            set { _shortRestMins = value; }
        }


        public DateTime Start
        {
            get { return _start; }
            set { _start = value; }
        }


        public int DistanceMetres
        {
            get { return _distanceMetres; }
            set { _distanceMetres = value; }
        }

        public SafeJourneyPlanAddress Consignor
        {
            get { return _consignor; }
            set { _consignor = value; }
        }

        public SafeJourneyPlanAddress Consignee
        {
            get { return _consignee; }
            set { _consignee = value; }
        }

        public SafeJourneyPlanAddress Contractor
        {
            get { return _contractor; }
            set { _contractor = value; }
        }

        public SafeJourneyPlanAddress PickupAddress
        {
            get { return _pickupAddress; }
            set { _pickupAddress = value; }
        }

        public SafeJourneyPlanAddress DeliveryAddress
        { get { return _deliveryAddress; } set { _deliveryAddress = value; } }

        public SafeJourneyAmendFlag AmendFlag
        { get { return _amendFlag; } set { _amendFlag = value; } }

        public List<SafeJourneyTask> Tasks
        {
            get { return _tasks; }
            set { _tasks = value; }
        }

        public AnsweredQuestions Answers
        {
            get { return _answers; }
            set { _answers = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _sjpId);
                stream.WriteByte((byte)_amendFlag);
                Write2ByteInteger(stream, _nonDrivingWorkMins);
                Write4ByteInteger(stream, _drivingMins);
                Write2ByteInteger(stream, _longRestMins);
                Write2ByteInteger(stream, _shortRestMins);
                WriteDateTime(stream, _start);
                Write4ByteInteger(stream, _distanceMetres);

                WriteMoreFlag(stream, _trailerAssets.Count);
                foreach (SafeJourneyTrailerAssets a in _trailerAssets)
                {
                    a.Encode(stream);
                }

                WriteMoreFlag(stream, _tasks.Count);
                foreach (SafeJourneyTask a in _tasks)
                {
                    a.Encode(stream);
                }

                int fieldsFlags = (_consignor != null ? 1 << 0 : 0)
                    | (_consignee != null ? 1 << 1 : 0)
                    | (_contractor != null ? 1 << 2 : 0)
                    | (_pickupAddress != null ? 1 << 3 : 0)
                    | (_deliveryAddress != null ? 1 << 4 : 0)
                    | (_answers != null ? 1 << 5 : 0);

                WriteMoreFlag(stream, fieldsFlags);

                if (_consignor != null)
                {
                    _consignor.Encode(stream);
                }

                if (_consignee != null)
                {
                    _consignee.Encode(stream);
                }

                if (_contractor != null)
                {
                    _contractor.Encode(stream);
                }

                if (_pickupAddress != null)
                {
                    _pickupAddress.Encode(stream);
                }

                if (_deliveryAddress != null)
                {
                    _deliveryAddress.Encode(stream);
                }

                if (_answers != null)
                {
                    _answers.Encode(stream);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }


        private void DecodeData()
        {
            try
            {
                int index = 0;

                _sjpId = Read4ByteInteger(Data, ref index);
                _amendFlag = (SafeJourneyAmendFlag)Data[index++];
                _nonDrivingWorkMins = (short)Read2ByteInteger(Data, ref index);
                _drivingMins = Read4ByteInteger(Data, ref index);
                _longRestMins = (short)Read2ByteInteger(Data, ref index);
                _shortRestMins = (short)Read2ByteInteger(Data, ref index);
                _start = ReadDateTime(Data, ref index);
                _distanceMetres = Read4ByteInteger(Data, ref index);

                int count = ReadMoreFlag(Data, ref index);

                List<SafeJourneyTrailerAssets> trailerAssets = new List<SafeJourneyTrailerAssets>();
                for (int i = 0; i < count; i++)
                {
                    SafeJourneyTrailerAssets t = new SafeJourneyTrailerAssets();
                    t.Decode(Data, ref index);
                    trailerAssets.Add(t);
                }
                _trailerAssets = trailerAssets;

                count = ReadMoreFlag(Data, ref index);
                List<SafeJourneyTask> tasks = new List<SafeJourneyTask>();
                for (int i = 0; i < count; i++)
                {
                    SafeJourneyTask t = new SafeJourneyTask();
                    t.Decode(Data, ref index);
                    tasks.Add(t);
                }
                _tasks = tasks;

                int fieldsFlags = ReadMoreFlag(Data, ref index);

                if ((fieldsFlags & (1 << 0)) != 0)
                {
                    _consignor = new SafeJourneyPlanAddress();
                    _consignor.Decode(Data, ref index);
                }
                else
                {
                    _consignor = null;
                }

                if ((fieldsFlags & (1 << 1)) != 0)
                {
                    _consignee = new SafeJourneyPlanAddress();
                    _consignee.Decode(Data, ref index);
                }
                else
                {
                    _consignee = null;
                }

                if ((fieldsFlags & (1 << 2)) != 0)
                {
                    _contractor = new SafeJourneyPlanAddress();
                    _contractor.Decode(Data, ref index);
                }
                else
                {
                    _contractor = null;
                }

                if ((fieldsFlags & (1 << 3)) != 0)
                {
                    _pickupAddress = new SafeJourneyPlanAddress();
                    _pickupAddress.Decode(Data, ref index);
                }
                else
                {
                    _pickupAddress = null;
                }

                if ((fieldsFlags & (1 << 4)) != 0)
                {
                    _deliveryAddress = new SafeJourneyPlanAddress();
                    _deliveryAddress.Decode(Data, ref index);
                }
                else
                {
                    _deliveryAddress = null;
                }

                if ((fieldsFlags & (1 << 5)) != 0)
                {
                    _answers = new AnsweredQuestions();
                    _answers.Decode(Data, ref index);
                }
                else
                {
                    _answers = null;
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                Console.WriteLine(ex.StackTrace);
                throw;
            }
        }
    }
}

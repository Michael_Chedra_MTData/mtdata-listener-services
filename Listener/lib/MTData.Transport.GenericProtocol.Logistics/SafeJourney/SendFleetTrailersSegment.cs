﻿using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SendFleetTrailersSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SendFleetTrailers;

        private int _fleetID;

        private List<SafeJourneyTrailerAssets> _trailers = new List<SafeJourneyTrailerAssets>();

        private List<ListId> _trackableAssetTrailerLists = new List<ListId>();
        public SendFleetTrailersSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SendFleetTrailersSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public List<SafeJourneyTrailerAssets> Trailers
        {
            get { return _trailers; }
            set { _trailers = value; }
        }

        public List<ListId> TrackableAssetTrailerLists
        {
            get { return _trackableAssetTrailerLists; }
            set { _trackableAssetTrailerLists = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetID);
                WriteMoreFlag(stream, _trailers.Count);
                foreach (SafeJourneyTrailerAssets t in _trailers)
                {
                    t.Encode(stream);
                }

                WriteMoreFlag(stream, _trackableAssetTrailerLists.Count);
                foreach (ListId t in _trackableAssetTrailerLists)
                {
                    t.Encode(stream);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {

            int index = 0;
            _fleetID = Read4ByteInteger(Data, ref index);
            int count = ReadMoreFlag(Data, ref index);

            List<SafeJourneyTrailerAssets> trailers = new List<SafeJourneyTrailerAssets>();
            for (int i = 0; i < count; i++)
            {
                SafeJourneyTrailerAssets t = new SafeJourneyTrailerAssets();
                t.Decode(Data, ref index);
                trailers.Add(t);
            }
            _trailers = trailers;

            count = ReadMoreFlag(Data, ref index);

            List<ListId> trackableAssetTrailerLists = new List<ListId>();
            for (int i = 0; i < count; i++)
            {
                ListId id = new ListId();
                id.Decode(Data, ref index);
                trackableAssetTrailerLists.Add(id);
            }
            _trackableAssetTrailerLists = trackableAssetTrailerLists;
        }
    }
}

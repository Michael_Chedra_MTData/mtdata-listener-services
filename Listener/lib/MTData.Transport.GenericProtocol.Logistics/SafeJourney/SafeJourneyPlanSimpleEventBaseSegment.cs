﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    /// <summary>
    /// Used to notify the Shell of simple events. Such as a user has cancelled an SJP.
    /// This segment is similiar to SafeJourneyPlanSimple event however is intended to be used 
    /// for packets originating from the server (sending to the Shell). To support this functionality the eventBaseId
    /// is included to allow the shell to ack this particular event.
    /// </summary>
    public class SafeJourneyPlanSimpleEventBaseSegment : SegmentLayer
    {
        public const LogisticsSegmentTypes SegmentType = LogisticsSegmentTypes.SafeJourneySimpleEventBase;

        private int _sjpId;

        private SafeJourneyEventType _eventType;

        private int _eventBaseId;

        public SafeJourneyPlanSimpleEventBaseSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SafeJourneyPlanSimpleEventBaseSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public int SjpId
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }

        public SafeJourneyEventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public int EventBaseId
        {
            get { return _eventBaseId; }
            set { _eventBaseId = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _sjpId);
                stream.WriteByte((byte)_eventType);
                Write4ByteInteger(stream, _eventBaseId);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }


        private void DecodeData()
        {
            int index = 0;
            _sjpId = Read4ByteInteger(Data, ref index);
            _eventType = (SafeJourneyEventType)Data[index++];
            _eventBaseId = Read4ByteInteger(Data, ref index);
        }
    }
}

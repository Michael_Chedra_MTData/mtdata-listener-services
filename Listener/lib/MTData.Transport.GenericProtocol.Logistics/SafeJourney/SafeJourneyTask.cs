﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SafeJourneyTask : IPacketEncode
    {
        private const int _versionNumber = 1;

        private string _description;

        private int? _addressListItemId;

        private int? _additionalAddressId;

        private int? _addressAdHocId;

        private SafeJourneyPlanAddress _address;

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }

        /// <summary>
        /// ID used to identify a record within L_ListItemAddress table.
        /// If null then address must be an ad hoc address (or no address is specified).
        /// </summary>
        public int? AddressListItemId
        {
            get { return _addressListItemId; }
            set { _addressListItemId = value; }
        }

        /// <summary>
        /// ID used to identify a record within L_CustomerAdditionalAddress table.
        /// If null then the primary address for the list item will be used. If this is also null then 
        /// the address must be an ad hoc address.
        /// </summary>
        public int? AdditionalAddressId
        {
            get { return _additionalAddressId; }
            set { _additionalAddressId = value; }
        }

        /// <summary>
        /// ID used to identifiy a record within the L_SjpAdHocAddress table
        /// If this is a new record (not yet inserted) then the values must be set to -1 
        /// </summary>
        public int? AddressAdHocId
        {
            get { return _addressAdHocId; }
            set { _addressAdHocId = value; }
        }

        /// <summary>
        /// Contains the Street, Suburb, Postcode, State, Latitude, Longitude, etc
        /// When sending to the Shell this must be populated. However when sending from the Shell
        /// to the server to reduce the data sent this property only needs to be set if an 
        /// new address is specified. 
        /// </summary>
        public SafeJourneyPlanAddress Address
        {
            get { return _address; }
            set { _address = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;
            _description = BaseLayer.ReadString(data, ref index, 0x12);
            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
            _addressListItemId = ((fieldsFlags & (1 << 0)) != 0) ? BaseLayer.Read4ByteInteger(data, ref index) : (int?)null;
            _additionalAddressId = ((fieldsFlags & (1 << 1)) != 0) ? BaseLayer.Read4ByteInteger(data, ref index) : (int?)null;
            _addressAdHocId = ((fieldsFlags & (1 << 2)) != 0) ? BaseLayer.Read4ByteInteger(data, ref index) : (int?)null;
            if ((fieldsFlags & (1 << 3)) != 0)
            {
                SafeJourneyPlanAddress address = new SafeJourneyPlanAddress();
                address.Decode(data, ref index);
                _address = address;
            }
            else
            {
                _address = null;
            }
            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.WriteString(local, _description, 0x12);
                int fieldsFlags = (_addressListItemId.HasValue ? 1 << 0 : 0)
                    | (_additionalAddressId.HasValue ? 1 << 1 : 0)
                    | (_addressAdHocId.HasValue ? 1 << 2 : 0)
                    | (_address != null ? 1 << 3 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                if (_addressListItemId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _addressListItemId.Value);
                }
                if (_additionalAddressId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _additionalAddressId.Value);
                }
                if (_addressAdHocId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _addressAdHocId.Value);
                }
                if (_address != null)
                {
                    _address.Encode(local);
                }
                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

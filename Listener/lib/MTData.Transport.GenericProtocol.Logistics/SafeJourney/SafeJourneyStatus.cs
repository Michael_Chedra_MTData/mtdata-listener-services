﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public enum SafeJourneyStatus : byte
    {
        Draft,
        Sending,
        ReceivedByUnit,
        DriverDeclared,
        DriverDeclaredWithAmends,
        DriverChangeAcknowledged,
        Completed,
        Rejected,
        Cancelled,
        Error
    }
}

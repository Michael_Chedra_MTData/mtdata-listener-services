﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    [Flags]
    public enum SafeJourneyAmendFlag : byte
    {
        None = 0,
        TripEstimate = 1,
        Trailers = 2,
        Tasks = 4
    }
}

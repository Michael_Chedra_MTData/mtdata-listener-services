﻿using MTData.Transport.GenericProtocol.Logistics.Lists;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SafeJourneyTrailerAssets : IPacketEncode
    {
        public enum TrailerType : byte
        {
            FleetVehicle,
            TrackableAssets
        }

        private const int _versionNumber = 3;

        private TrailerType _type;

        private int _fleetID;

        private int _vehicleID;

        private int _trackableAssetListItemID;

        private ListId _listId = new ListId();

        private string _name;

        private string _vehicleTypeDescription;

        private int? _defectTemplateUniqueKey;
        private int? _defectTemplateListId;

        public int FleetID
        {
            get { return _fleetID; }
            set { _fleetID = value; }
        }

        public int VehicleID
        {
            get { return _vehicleID; }
            set { _vehicleID = value; }
        }

        public int TrackableAssetListItemID
        {
            get { return _trackableAssetListItemID; }
            set { _trackableAssetListItemID = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public ListId ListId
        {
            get { return _listId; }
            set { _listId = value; }
        }

        public TrailerType Type
        {
            get { return _type; }
            set { _type = value; }
        }

        public string VehicleTypeDescription
        {
            get { return _vehicleTypeDescription; }
            set { _vehicleTypeDescription = value; }
        }

        public int? DefectTemplateUniqueKey
        {
            get { return _defectTemplateUniqueKey; }
            set { _defectTemplateUniqueKey = value; }
        }
        public int? DefectTemplateListId
        {
            get { return _defectTemplateListId; }
            set { _defectTemplateListId = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _type = (TrailerType)data[index++];
            _name = BaseLayer.ReadString(data, ref index, 0x12);


            switch (_type)
            {
                case TrailerType.FleetVehicle:
                    _fleetID = BaseLayer.Read4ByteInteger(data, ref index);
                    _vehicleID = BaseLayer.Read4ByteInteger(data, ref index);
                    break;
                case TrailerType.TrackableAssets:
                    _trackableAssetListItemID = BaseLayer.Read4ByteInteger(data, ref index);
                    ListId id = new ListId();
                    id.Decode(data, ref index);
                    _listId = id;
                    break;
                default:
                    break;
            }

            if (versionNumber >= 2)
            {
                int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
                if ((fieldsFlags & (1 << 0)) != 0)
                {
                    _vehicleTypeDescription = BaseLayer.ReadString(data, ref index);
                }
            }
            if (versionNumber >= 3)
            {
                int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
                if ((fieldsFlags & (1 << 0)) != 0)
                {
                    _defectTemplateUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
                    _defectTemplateListId = BaseLayer.Read4ByteInteger(data, ref index);
                }
            }

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                local.WriteByte((byte)_type);
                BaseLayer.WriteString(local, _name, 0x12);

                switch (_type)
                {
                    case TrailerType.FleetVehicle:
                        BaseLayer.Write4ByteInteger(local, _fleetID);
                        BaseLayer.Write4ByteInteger(local, _vehicleID);
                        break;
                    case TrailerType.TrackableAssets:
                        BaseLayer.Write4ByteInteger(local, _trackableAssetListItemID);
                        _listId.Encode(local);
                        break;
                    default:
                        break;
                }

                // Added version 2
                int fieldsFlags = _vehicleTypeDescription != null ? 1 : 0;
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                if (_vehicleTypeDescription != null)
                {
                    BaseLayer.WriteString(local, _vehicleTypeDescription);
                }

                //version 3
                fieldsFlags = _defectTemplateUniqueKey.HasValue ? 1 : 0;
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                if (_defectTemplateUniqueKey.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _defectTemplateUniqueKey.Value);
                    BaseLayer.Write4ByteInteger(local, _defectTemplateListId.Value);
                }


                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }

}

﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics.SafeJourney
{
    public class SafeJourneyPlanAddress : IPacketEncode
    {
        private const int _versionNumber = 1;

        private string _name;

        private string _street;

        private string _suburb;

        private string _state;

        private string _postcode;

        private double _latitude;

        private double _longitude;

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public string Postcode
        {
            get { return _postcode; }
            set { _postcode = value; }
        }

        public string State
        {
            get { return _state; }
            set { _state = value; }
        }

        public string Suburb
        {
            get { return _suburb; }
            set { _suburb = value; }
        }

        public string Street
        {
            get { return _street; }
            set { _street = value; }
        }

        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);
            int startIndex = index;

            _name = BaseLayer.ReadString(data, ref index, 0x12);
            _street = BaseLayer.ReadString(data, ref index, 0x12);
            _suburb = BaseLayer.ReadString(data, ref index, 0x12);
            _state = BaseLayer.ReadString(data, ref index, 0x12);
            _postcode = BaseLayer.ReadString(data, ref index, 0x12);
            _latitude = BaseLayer.ReadCoordinate(data, ref index);
            _longitude = BaseLayer.ReadCoordinate(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            using (MemoryStream local = new MemoryStream())
            {
                BaseLayer.WriteString(local, _name, 0x12);
                BaseLayer.WriteString(local, _street, 0x12);
                BaseLayer.WriteString(local, _suburb, 0x12);
                BaseLayer.WriteString(local, _state, 0x12);
                BaseLayer.WriteString(local, _postcode, 0x12);
                BaseLayer.WriteCoordinate(local, _latitude);
                BaseLayer.WriteCoordinate(local, _longitude);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }
}

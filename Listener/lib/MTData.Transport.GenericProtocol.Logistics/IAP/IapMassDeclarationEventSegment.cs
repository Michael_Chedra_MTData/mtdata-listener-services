﻿namespace MTData.Transport.GenericProtocol.Logistics.IAP
{
    using System;
    using System.IO;

    /// <summary>
    /// Defines the <see cref="IapMassDeclarationEventSegment" />
    /// </summary>
    public class IapMassDeclarationEventSegment : SegmentLayer
    {
        private ushort vehicleCategory;
        private byte noOfAxles;
        private float totalMass;

        /// <summary>
        /// Initialises a new instance of the <see cref="IapMassDeclarationEventSegment"/> class.
        /// </summary>
        public IapMassDeclarationEventSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.IapMassDeclarationEvent;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="IapMassDeclarationEventSegment"/> class.
        /// </summary>
        /// <param name="segment">The <see cref="SegmentLayer"/></param>
        public IapMassDeclarationEventSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.IapMassDeclarationEvent)
            {
                throw new Exception($"Can only create a segment of type {LogisticsSegmentTypes.IapMassDeclarationEvent}, not {segment.Type}");
            }

            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.IapMassDeclarationEvent;
            Data = segment.Data;
            DecodeData();
        }

        /// <summary>
        /// Gets or sets the CommentCode
        /// </summary>
        public ushort VehicleCategory
        {
            get { return vehicleCategory; }
            set { vehicleCategory = value; }
        }

        /// <summary>
        /// Gets or sets the NoOfAxles
        /// </summary>
        public byte NoOfAxles
        {
            get { return noOfAxles; }
            set { noOfAxles = value; }
        }

        /// <summary>
        /// Gets or sets the mass declaration value.
        /// </summary>
        public float TotalMass
        {
            get { return totalMass; }
            set { totalMass = value; }
        }

        /// <summary>
        /// The GetBytes
        /// </summary>
        /// <returns>The byte array of the segment.</returns>
        public override byte[] GetBytes()
        {
            // set the data
            using (var stream = new MemoryStream())
            {
                Write2ByteInteger(stream, VehicleCategory);
                Write2ByteInteger(stream, NoOfAxles);
                WriteFloat(stream, TotalMass);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        /// <summary>
        /// The Decode
        /// </summary>
        /// <param name="payload">The byte array of the segment to decode.</param>
        /// <returns>The number of bytes decoded.</returns>
        public override int Decode(byte[] payload)
        {
            var read = base.Decode(payload);
            DecodeData();
            return read;
        }

        /// <summary>
        /// The DecodeData
        /// </summary>
        private void DecodeData()
        {
            var index = 0;
            VehicleCategory = (ushort)Read2ByteInteger(Data, ref index);
            NoOfAxles = (byte)Read2ByteInteger(Data, ref index);
            TotalMass = ReadFloat(Data, ref index);
        }
    }
}

﻿namespace MTData.Transport.GenericProtocol.Logistics.IAP
{
    using System;
    using System.IO;

    /// <summary>
    /// Defines the <see cref="IapCommentEventSegment" />
    /// </summary>
    public class IapCommentEventSegment : SegmentLayer
    {
        private ushort commentCode;
        private string comment;

        /// <summary>
        /// Initialises a new instance of the <see cref="IapCommentEventSegment"/> class.
        /// </summary>
        public IapCommentEventSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.IapCommentEvent;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="IapCommentEventSegment"/> class.
        /// </summary>
        /// <param name="segment">The <see cref="SegmentLayer"/></param>
        public IapCommentEventSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.IapCommentEvent)
            {
                throw new Exception($"Can only create a segment of type {LogisticsSegmentTypes.IapCommentEvent}, not {segment.Type}");
            }

            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.IapCommentEvent;
            Data = segment.Data;
            DecodeData();
        }

        /// <summary>
        /// Gets or sets the CommentCode
        /// </summary>
        public ushort CommentCode
        {
            get { return commentCode; }
            set { commentCode = value; }
        }

        /// <summary>
        /// Gets or sets the Comment
        /// </summary>
        public string Comment
        {
            get { return comment; }
            set { comment = value; }
        }

        /// <summary>
        /// The GetBytes
        /// </summary>
        /// <returns>The byte array of the segment</returns>
        public override byte[] GetBytes()
        {
            // set the data
            using (var stream = new MemoryStream())
            {
                Write2ByteInteger(stream, CommentCode);
                WriteString(stream, Comment);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        /// <summary>
        /// The Decode
        /// </summary>
        /// <param name="payload">The byte array of the segment to decode.</param>
        /// <returns>The number of bytes decoded.></returns>
        public override int Decode(byte[] payload)
        {
            var read = base.Decode(payload);
            DecodeData();
            return read;
        }

        /// <summary>
        /// The DecodeData
        /// </summary>
        private void DecodeData()
        {
            var index = 0;
            CommentCode = (ushort)Read2ByteInteger(Data, ref index);
            Comment = ReadString(Data, ref index);
        }
    }
}

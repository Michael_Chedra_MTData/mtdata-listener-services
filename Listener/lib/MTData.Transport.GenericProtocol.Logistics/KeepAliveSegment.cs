using System;
using System.Collections.Generic;
using System.Text;

namespace MTData.Transport.GenericProtocol.Logistics
{
    /// <summary>
    /// this segment represents a keep alive packet
    /// </summary>
    public class KeepAliveSegment : SegmentLayer
    {

        #region constructor
        public KeepAliveSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.KeepAlive;
        }
        public KeepAliveSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.KeepAlive)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.KeepAlive, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.KeepAlive;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }

        #endregion

    }
}

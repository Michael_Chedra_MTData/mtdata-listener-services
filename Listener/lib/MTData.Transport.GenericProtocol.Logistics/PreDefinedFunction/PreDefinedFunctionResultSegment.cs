using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.PreDefinedFunction
{
    /// <summary>
    /// a predefined function result segment sent from unit
    /// </summary>
    public class PreDefinedFunctionResultSegment : SegmentLayer
    {
        #region private fields
        private PredefinedFunction _function;
        private bool _complete;
        private byte[] _result;
        #endregion

        #region properties
        public PredefinedFunction Function
        {
            get { return _function; }
            set { _function = value; }
        }
        public bool Complete
        {
            get { return _complete; }
            set { _complete = value; }
        }
        public byte[] Result
        {
            get { return _result; }
            set { _result = value; }
        }
        #endregion

        #region constructor
        public PreDefinedFunctionResultSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.PreDefinedFunctionResult;
        }
        public PreDefinedFunctionResultSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.PreDefinedFunctionResult)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.PreDefinedFunctionResult, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.PreDefinedFunctionResult;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_function);
                WriteBool(stream, _complete);
                if (_result == null || _result.Length == 0)
                {
                    Write4ByteUInteger(stream, 0);
                }
                else
                {
                    Write4ByteInteger(stream, _result.Length);
                    stream.Write(_result, 0, _result.Length);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _function = (PredefinedFunction)Data[index++];
            _complete = ReadBool(Data, ref index);
            int length = Read4ByteInteger(Data, ref index);
            _result = new byte[length];
            if (length > 0)
            {
                Array.Copy(Data, index, _result, 0, length);
                index += length;
            }
        }
        #endregion
    }
}

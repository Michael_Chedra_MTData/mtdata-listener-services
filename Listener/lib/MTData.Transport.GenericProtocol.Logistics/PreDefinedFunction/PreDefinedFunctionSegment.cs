using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics.PreDefinedFunction
{
    /// <summary>
    /// enum defining the predefined functions
    /// </summary>
    public enum PredefinedFunction
    {
        RebootWinCE = 0,
        SendConfigFile,
        SendLogFile,
        DropMccQueue,
        RequestAssetPacket,
        StartPerformanceLogging,
        RequestPerformanceLog,
        RunScript,
        RetrieveFile,
        ClearLoginCache,
        MccQueueCount,
        GetFirstMccPacket,
        ResendFirstMccPacket,
        DeleteFirstMccPacketAndResendNext,
        SendCommsLog,
        RetrieveDatabaseSize,
        RetrieveDatabase,
        ExecuteNonQuery,
        ExecuteQuery,
        RestartTalon,
        WipeTalonDatabaseAndRestart

    }

    /// <summary>
    /// a predefined function segment sent from base
    /// </summary>
    public class PreDefinedFunctionSegment : SegmentLayer
    {
        #region private fields
        private PredefinedFunction _function;
        private bool _start;
        private byte[] _parameters;
        #endregion

        #region properties
        public PredefinedFunction Function
        {
            get { return _function; }
            set { _function = value; }
        }
        public bool Start
        {
            get { return _start; }
            set { _start = value; }
        }
        public byte[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
        #endregion

        #region constructor
        public PreDefinedFunctionSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.PreDefinedFunction;
        }
        public PreDefinedFunctionSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.PreDefinedFunction)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.PreDefinedFunction, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.PreDefinedFunction;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_function);
                WriteBool(stream, _start);
                if (_parameters == null || _parameters.Length == 0)
                {
                    Write4ByteUInteger(stream, 0);
                }
                else
                {
                    Write4ByteInteger(stream, _parameters.Length);
                    stream.Write(_parameters, 0, _parameters.Length);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _function = (PredefinedFunction)Data[index++];
            _start = ReadBool(Data, ref index);
            int length = Read4ByteInteger(Data, ref index);
            _parameters = new byte[length];
            if (length > 0)
            {
                Array.Copy(Data, index, _parameters, 0, length);
                index += length;
            }
        }
        #endregion
    }
}

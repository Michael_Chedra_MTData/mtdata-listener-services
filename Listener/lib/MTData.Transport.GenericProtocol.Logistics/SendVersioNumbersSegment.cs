using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.Logistics
{
    public class SendVersioNumbersSegment : SegmentLayer
    {
        #region private fields
        private int _firmwareMajor;
        private int _firmwareMinor;
        private bool _firmwareForceUpdate;
        private int _winCeMajor;
        private int _winCeMinor;
        private bool _winCeForceUpdate;
        private int _necMajor;
        private int _necMinor;
        private bool _necForceUpdate;
        private int _firmware4000Major;
        private int _firmware4000Minor;
        private bool _firmware4000ForceUpdate;
        private int _shellConfigMajor;
        private int _shellConfigMinor;
        private bool _ShellConfigForceUpdate;
        private List<Talon3rdPartyAppVersionInfo> _talon3rdPartyApps;
        #endregion

        #region properties
        public int FirmwareMajor
        {
            get { return _firmwareMajor; }
            set { _firmwareMajor = value; }
        }
        public int FirmwareMinor
        {
            get { return _firmwareMinor; }
            set { _firmwareMinor = value; }
        }
        public bool FirmwareForceUpdate
        {
            get { return _firmwareForceUpdate; }
            set { _firmwareForceUpdate = value; }
        }
        public int WinCeMajor
        {
            get { return _winCeMajor; }
            set { _winCeMajor = value; }
        }
        public int WinCeMinor
        {
            get { return _winCeMinor; }
            set { _winCeMinor = value; }
        }
        public bool WinCeForceUpdate
        {
            get { return _winCeForceUpdate; }
            set { _winCeForceUpdate = value; }
        }
        public int NecMajor
        {
            get { return _necMajor; }
            set { _necMajor = value; }
        }
        public int NecMinor
        {
            get { return _necMinor; }
            set { _necMinor = value; }
        }
        public bool NecForceUpdate
        {
            get { return _necForceUpdate; }
            set { _necForceUpdate = value; }
        }
        public int Firmware4000Major
        {
            get { return _firmware4000Major; }
            set { _firmware4000Major = value; }
        }
        public int Firmware4000Minor
        {
            get { return _firmware4000Minor; }
            set { _firmware4000Minor = value; }
        }
        public bool Firmware4000ForceUpdate
        {
            get { return _firmware4000ForceUpdate; }
            set { _firmware4000ForceUpdate = value; }
        }
        public int ShellConfigMajor
        {
            get { return _shellConfigMajor; }
            set { _shellConfigMajor = value; }
        }
        public int ShellConfigMinor
        {
            get { return _shellConfigMinor; }
            set { _shellConfigMinor = value; }
        }
        public bool ShellConfigForceUpdate
        {
            get { return _ShellConfigForceUpdate; }
            set { _ShellConfigForceUpdate = value; }
        }
        public List<Talon3rdPartyAppVersionInfo> Talon3rdPartyApps
        {
            get { return _talon3rdPartyApps; }
            set { _talon3rdPartyApps = value; }
        }
        #endregion

        #region constructor
        public SendVersioNumbersSegment()
        {
            Version = 4;
            Type = (int)LogisticsSegmentTypes.SendVersioNumbers;
            _talon3rdPartyApps = new List<Talon3rdPartyAppVersionInfo>();
        }
        public SendVersioNumbersSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendVersioNumbers)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendVersioNumbers, segment.Type));
            }
            _talon3rdPartyApps = new List<Talon3rdPartyAppVersionInfo>();
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendVersioNumbers;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_firmwareMajor);
                stream.WriteByte((byte)_firmwareMinor);
                WriteBool(stream, _firmwareForceUpdate);
                stream.WriteByte((byte)_winCeMajor);
                stream.WriteByte((byte)_winCeMinor);
                WriteBool(stream, _winCeForceUpdate);
                stream.WriteByte((byte)_necMajor);
                stream.WriteByte((byte)_necMinor);
                WriteBool(stream, _necForceUpdate);
                stream.WriteByte((byte)_firmware4000Major);
                stream.WriteByte((byte)_firmware4000Minor);
                WriteBool(stream, _firmware4000ForceUpdate);
                if (Version >= 3)
                {
                    Write4ByteInteger(stream, _shellConfigMajor);
                    Write4ByteInteger(stream, _shellConfigMinor);
                    WriteBool(stream, _ShellConfigForceUpdate);
                }
                Write2ByteInteger(stream, _talon3rdPartyApps.Count);
                foreach(Talon3rdPartyAppVersionInfo t in _talon3rdPartyApps)
                {
                    t.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _firmwareMajor = (int)Data[index++];
            _firmwareMinor = (int)Data[index++];
            _firmwareForceUpdate = ReadBool(Data, ref index);
            _winCeMajor = (int)Data[index++];
            _winCeMinor = (int)Data[index++];
            _winCeForceUpdate = ReadBool(Data, ref index);
            _necMajor = (int)Data[index++];
            _necMinor = (int)Data[index++];
            _necForceUpdate = ReadBool(Data, ref index);
            if (Version >= 2)
            {
                _firmware4000Major = (int)Data[index++];
                _firmware4000Minor = (int)Data[index++];
                _firmware4000ForceUpdate = ReadBool(Data, ref index);
            }
            if (Version >= 3)
            {
                _shellConfigMajor = Read4ByteInteger(Data, ref index);
                _shellConfigMinor = Read4ByteInteger(Data, ref index);
                _ShellConfigForceUpdate = ReadBool(Data, ref index);
            }
            _talon3rdPartyApps = new List<Talon3rdPartyAppVersionInfo>();
            if (Version >= 4)
            {
                int count = Read2ByteInteger(Data, ref index);
                for (int i=0; i< count; i++)
                {
                    Talon3rdPartyAppVersionInfo t = new Talon3rdPartyAppVersionInfo();
                    t.Decode(Data, ref index);
                    _talon3rdPartyApps.Add(t);
                }
            }
        }
        #endregion
    }
}

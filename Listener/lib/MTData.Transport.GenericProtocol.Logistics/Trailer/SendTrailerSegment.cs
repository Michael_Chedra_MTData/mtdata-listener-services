using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.Logistics.Trailer
{
    public class SendTrailerSegment : SegmentLayer
    {
        #region private fields
        private TrailerItem _trailerItem;
        #endregion

        #region properties
        public TrailerItem TrailerItem
        {
            get { return _trailerItem; }
            set { _trailerItem = value; }
        }
        #endregion

        #region constructor
        public SendTrailerSegment()
        {
            Version = 1;
            Type = (int)LogisticsSegmentTypes.SendTrailer;
            _trailerItem = new TrailerItem();
        }
        public SendTrailerSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsSegmentTypes.SendTrailer)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsSegmentTypes.SendTrailer, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsSegmentTypes.SendTrailer;
            _trailerItem = new TrailerItem();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                _trailerItem.Encode(stream);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _trailerItem.Decode(Data, ref index);
        }
        #endregion

    }
}

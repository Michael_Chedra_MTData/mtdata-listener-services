using System.Reflection;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
#if Android
using Android.App;
#endif
[assembly: AssemblyTitle("MTData.Transport.GenericProtocol.Logistics")]
[assembly: AssemblyDescription("")]
[assembly: AssemblyConfiguration("")]
[assembly: AssemblyCompany("Mobile Tracking and Data")]
[assembly: AssemblyProduct("MTData Transport Protocol Library")]
[assembly: AssemblyCopyright("Copyright © 2013")]
[assembly: AssemblyTrademark("")]
[assembly: AssemblyCulture("")]
[assembly: ComVisible(false)]
[assembly: Guid("f72a8e01-01df-444d-b9b5-a89978c5cd61")]
[assembly: AssemblyVersion("8.2.0.0")]
#if Android
// Add some common permissions, these can be removed if not needed
[assembly: UsesPermission(Android.Manifest.Permission.Internet)]
[assembly: UsesPermission(Android.Manifest.Permission.WriteExternalStorage)]
#endif

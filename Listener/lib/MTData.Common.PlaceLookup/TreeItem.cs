using System;
using log4net;
using MTData.Common.Utilities;

namespace MTData.Common.PlaceLookup
{
    public class TreeItem
    {        
        private const double dDegToRad = Math.PI / 180;
        private const double dRadToDeg = 180.0 / Math.PI;
        private const string sClassName = "MTData.Common.PlaceLookup.cTreeItem.";
        private static ILog _log = LogManager.GetLogger(typeof(TreeItem));

        public int iID = 0;
        public string sPlaceName = "";
        public double dLat = 0;
        public double dLong = 0;
        public Boundary oTestArea = null;

        public TreeItem(int ID, string PlaceName, double MinLat, double MinLong, double MaxLat, double MaxLong)
        {
            try
            {
                iID = ID;
                sPlaceName = PlaceName;
                dLat = (MinLat + MaxLat) / 2.0;
                dLong = (MinLong + MaxLong) / 2.0;
                oTestArea = new Boundary(MinLat, MinLong, MaxLat, MaxLong);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public void Dispose()
        {
            if (oTestArea != null)
                oTestArea = null;
        }

        public double GetDistanceToPoint(double dCompLat, double dCompLong)
        {
            double dRet = 0;
            try
            {
                dRet = Util.CalculateDistanceSquared(dLat, dLong, dCompLat, dCompLong, dDegToRad);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetDistanceToPoint(double dCompLat, double dCompLong)", ex);
                throw new System.Exception(sClassName + "GetDistanceToPoint(double dCompLat, double dCompLong)", ex);
            }
            return dRet;
        }

        public string GetDirectionFromPoint(double dCompLat, double dCompLong)
        {
            string sRet = "";
            try
            {
                sRet = Util.GetDirectionFromUnitToPoint(dCompLat, dCompLong, dLat, dLong, dDegToRad, dRadToDeg);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetDirectionFromPoint(double dCompLat, double dCompLong)", ex);
                throw new System.Exception(sClassName + "GetDirectionFromPoint(double dCompLat, double dCompLong)", ex);
            }
            return sRet;
        }

        public double GetDistanceInMeters(double lat1, double lon1)
        {
            double lat2;
            double lon2;
            double c = 0;
            try
            {
                //convert the degree values to radians before calculation
                lat1 = ToRadians(lat1);
                lon1 = ToRadians(lon1);
                lat2 = ToRadians(dLat);
                lon2 = ToRadians(dLong);

                double dlon = lon2 - lon1;
                double dlat = lat2 - lat1;

                double a = Math.Pow(Math.Sin(dlat / 2), 2) + Math.Cos(lat1) * Math.Cos(lat2) * Math.Pow(Math.Sin(dlon / 2), 2);
                c = (2 * Math.Atan2(Math.Sqrt(a), Math.Sqrt(1 - a))) * 6367000.0;

            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetDistanceInMeters(double dCompLat, double dCompLong)", ex);
                throw new System.Exception(sClassName + "GetDistanceInMeters(double dCompLat, double dCompLong)", ex);
            }
            return c;
        }

        private double ToRadians(double degree)
        {
            return (degree * Math.PI) / 180.0;
        }
    }
}

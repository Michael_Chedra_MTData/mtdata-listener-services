using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Common.PlaceLookup
{
    /// <summary>
    /// Summary description for cPlaceLookupTree.
    /// </summary>
    public class PlaceLookupTree
    {
        /// <summary>
        /// The root of the tree
        /// </summary>
        private BoundaryBox oTree = null;
		private MelwaysLookup oMelwaysRef = null;

        public PlaceLookupTree()
        {
            try
            {
                oTree = new BoundaryBox((int)3);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
        public PlaceLookupTree(string sDSN)
        {
            CreateObject(sDSN, 3);
        }
        public PlaceLookupTree(string sDSN, int KeepXItemsAtEachNode)
        {
            CreateObject(sDSN, KeepXItemsAtEachNode);
        }
        private void CreateObject(string sDSN, int KeepXItemsAtEachNode)
        {
            int iID = 0;
            double dLat = 0;
            double dLong = 0;
            string sPlaceName = "";
			string sMelwaysRef = "";
            SqlConnection oConn = null;
            SqlCommand oCmd = null;
            DataSet oRawData = null;
            SqlDataAdapter oSQLDA = null;

            #region Connect to the DB
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
            }
            catch (System.Exception ex)
            {
                if (oConn != null)
                {
                    oConn.Dispose();
                    oConn = null;
                }
                throw (ex);
            }
            #endregion
            #region Retrieve data from the T_Places table
            try
            {
                oCmd = new SqlCommand("SELECT ID, PlaceName, Lat, Long FROM T_Places WITH (NOLOCK)", oConn);
                oSQLDA = new System.Data.SqlClient.SqlDataAdapter(oCmd);
                oRawData = new DataSet();
                oSQLDA.Fill(oRawData);
                oSQLDA.Dispose();
                oSQLDA = null;
                oCmd.Dispose();
                oCmd = null;
                oConn.Close();
                oConn.Dispose();
                oConn = null;
            }
            catch (System.Exception ex)
            {
                if (oSQLDA != null)
                {
                    oSQLDA.Dispose();
                    oSQLDA = null;
                }
                if (oCmd != null)
                {
                    oCmd.Dispose();
                    oCmd = null;
                }
                if (oConn != null)
                {
                    oConn.Dispose();
                    oConn = null;
                }
                throw (ex);
            }
            #endregion
			#region Create the melways lookup
			try
			{
				oMelwaysRef = new MelwaysLookup(sDSN);
			}
			catch(System.Exception)
			{
				oMelwaysRef = null;
			}
			#endregion
            #region Create and populate the tree
            try
            {
                oTree = new BoundaryBox(KeepXItemsAtEachNode);
                if (oTree != null)
                {
                    if (oRawData != null)
                    {
                        if (oRawData.Tables.Count > 0)
                        {
                            for (int X = 0; X < oRawData.Tables[0].Rows.Count; X++)
                            {
                                // If the record has all the required values
                                if (oRawData.Tables[0].Rows[X]["ID"] != System.DBNull.Value && oRawData.Tables[0].Rows[X]["PlaceName"] != System.DBNull.Value && oRawData.Tables[0].Rows[X]["Lat"] != System.DBNull.Value && oRawData.Tables[0].Rows[X]["Long"] != System.DBNull.Value)
                                {
                                    // If the location is valid.
                                    if (Convert.ToDouble(oRawData.Tables[0].Rows[X]["Lat"]) != 0 || Convert.ToDouble(oRawData.Tables[0].Rows[X]["Long"]) != 0)
                                    {
                                        iID = Convert.ToInt32(oRawData.Tables[0].Rows[X]["ID"]);
                                        sPlaceName = Convert.ToString(oRawData.Tables[0].Rows[X]["PlaceName"]);
                                        dLat = Convert.ToDouble(oRawData.Tables[0].Rows[X]["Lat"]);
                                        dLong = Convert.ToDouble(oRawData.Tables[0].Rows[X]["Long"]);
										if(oMelwaysRef != null)
											sMelwaysRef = oMelwaysRef.Query(dLat, dLong);
										else
											sMelwaysRef = "";
										this.Insert(0, iID, sPlaceName, dLat, dLong, sMelwaysRef);
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            #endregion
        }
		public MelwaysLookup oMelwaysRefLookup { get {return oMelwaysRef;}}
        /// <summary>
        /// Get the count of items in the tree
        /// </summary>
        public int Count { get { return oTree.Count; } }
        /// <summary>
        /// Insert the place into the tree
        /// </summary>
        /// <param name="item"></param>
		public void Insert(int iGroupID, int iID, string sPlaceName, double dLat, double dLong, string sMelwaysRef)
        {
            try
            {
				PlaceName oItem = new PlaceName(iGroupID, iID, sPlaceName, dLat, dLong, sMelwaysRef);
                oTree.Insert(oItem);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
        /// <summary>
        /// Query the QuadTree, returning the items that are in the given area
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        public PlaceName Query(double dLat, double dLong)
        {
            ArrayList oItems = null;
            PlaceName oResult = null;
            double dDistance = double.MaxValue;
            double dTemp = double.MaxValue;
            double dSearchRadius = double.MaxValue;
            int iSearchCount = 0;
            double dSearchArea = 0.025;

            try
            {
                dSearchArea = dSearchArea / 2;
                oItems = new ArrayList();
                while (oItems.Count == 0 && iSearchCount < 15)
                {
                    dSearchArea = dSearchArea * 2;
                    oItems = oTree.Query(dLat, dLong, dSearchArea);
                    if (oItems.Count > 0)
                    {
                        oResult = (PlaceName)oItems[0];
                        dDistance = oResult.GetDistanceToPoint(dLat, dLong);
                        for (int X = 1; X < oItems.Count; X++)
                        {
                            dTemp = ((PlaceName)oItems[X]).GetDistanceToPoint(dLat, dLong);
                            if (dTemp < dSearchRadius)
                            {
                                if (dTemp < dDistance)
                                {
                                    oResult = (PlaceName)oItems[X];
                                    dDistance = dTemp;
                                }
                            }
                        }
                        // Convert the search area to a circle
                        dSearchRadius = Math.Pow(dSearchArea, 2);
                        if (dDistance > dSearchRadius)
                        {
                            oResult = null;
                            oItems = new ArrayList();
                        }
                    }
                    iSearchCount++;
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return oResult;
        }
    }
}

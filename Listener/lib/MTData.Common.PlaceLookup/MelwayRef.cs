using System;

namespace MTData.Common.PlaceLookup
{
	/// <summary>
	/// Summary description for cMelwayRef.
	/// </summary>
	public class MelwayRef : TreeItem
	{
		public double dArea = 0;

		public MelwayRef(int ID, string PlaceName, double MinLat, double MinLong, double MaxLat, double MaxLong)
			: base(ID, PlaceName, MinLat, MinLong, MaxLat, MaxLong) 
		{
			dArea = (MaxLong - MinLong) * (MaxLat - MinLat);
		}

		public string GetMelwaysSector(double dLat, double dLong)
		{
			string sRet = this.sPlaceName;
			string sLatSector = "";
			string sLongSector = "";
			try
			{
				sLatSector = GetMelwaysLatSector(dLat);
				if(sLatSector != "")
				{
					sLongSector = GetMelwaysLongSector(dLong);
					if(sLongSector != "")
					{
						sRet += " " + sLatSector + " " + sLongSector;
					}
				}
			}
			catch(System.Exception)
			{
				sRet = this.sPlaceName;
			}
			return sRet;
		}

		private string GetMelwaysLatSector(double dTestLat)
		{
			string sRet = "";
			double dSectorSize = 0;
			double dPageDivisions = 0;
			double dSector = 0;
			try
			{
				// This is a sydney map
				if(this.sPlaceName.IndexOf("M") == 0)
					dPageDivisions = 12;
				else if(this.sPlaceName.IndexOf("S") == 0)
					dPageDivisions = 20;

				if(dPageDivisions > 0)
				{
					dSectorSize = Math.Abs(this.oTestArea.dMaxLat - this.oTestArea.dMinLat) / dPageDivisions;
					if(dSectorSize > 0)
					{
						if(dTestLat > this.oTestArea.dMaxLat)
							dSector = Math.Ceiling(dTestLat - this.oTestArea.dMaxLat / dSectorSize);
						else
							dSector = Math.Ceiling((this.oTestArea.dMaxLat - dTestLat) / dSectorSize);
						if(dSector > 0 && dSector <= dPageDivisions)
						{
							sRet = Convert.ToString(dSector);
						}
					}
				}
			}
			catch(System.Exception)
			{
				sRet = "";
			}
			return sRet;
		}

		private string GetMelwaysLongSector(double dTestLong)
		{
			string sRet = "";
			double dSectorSize = 0;
			double dPageDivisions = 0;
			double dSector = 0;
			try
			{
				// This is a sydney map
				if(this.sPlaceName.IndexOf("M") == 0)
					dPageDivisions = 10;
				else if(this.sPlaceName.IndexOf("S") == 0)
					dPageDivisions = 16;

				if(dPageDivisions > 0)
				{
					dSectorSize = Math.Abs(this.oTestArea.dMaxLong - this.oTestArea.dMinLong) / dPageDivisions;
					if(dSectorSize > 0)
					{
						if(dTestLong > this.oTestArea.dMinLong)
							dSector = Math.Ceiling((dTestLong - this.oTestArea.dMinLong) / dSectorSize);
						else 
							dSector = Math.Ceiling((this.oTestArea.dMinLong - dTestLong) / dSectorSize);
						if(dSector > 0 && dSector <= dPageDivisions)
						{
							// Melways do not have an I or O columns
							if(this.sPlaceName.IndexOf("M") == 0 || this.sPlaceName.IndexOf("S") == 0)
							{
								if(dSector >= 9)
								{
									if(dSector >= 15)	// If the column is greater than 15 add 2, one for the missing I column and one for the missing O column
										sRet = Convert.ToString(Convert.ToChar(Convert.ToInt32(dSector) + 66));
									else	// If the column is greater than 9 add 1 for the missing I column
										sRet = Convert.ToString(Convert.ToChar(Convert.ToInt32(dSector) + 65));
								}
								else
									sRet = Convert.ToString(Convert.ToChar(Convert.ToInt32(dSector) + 64));
							}
							else
								sRet = Convert.ToString(Convert.ToChar(Convert.ToInt32(dSector) + 64));
						}
					}
				}
			}
			catch(System.Exception)
			{
				sRet = "";
			}
			return sRet;
		}
	}
}

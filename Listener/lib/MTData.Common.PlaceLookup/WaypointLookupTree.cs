using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using log4net;

namespace MTData.Common.PlaceLookup
{
	/// <summary>
	/// Summary description for cWaypointLookupTree.
	/// </summary>
	public class WaypointLookupTree
	{
        private const string sClassName = "MTData.Common.PlaceLookup.cWaypointLookupTree";
        private static ILog _log = LogManager.GetLogger(typeof(WaypointLookupTree));
		/// <summary>
		/// The root of the tree
		/// </summary>
		private Hashtable oWPTrees = null;
		private Hashtable oWPIDLookups = null;
        private Hashtable oPolygonNodesLookup = null;
        // The oPolygonNodesLookup table is keyed by SetpointGroupID, each element is a hashtable keyed by SetPointID, each element of this table
        // is an array list holding the node lat/long points in sequence order.
		private MelwaysLookup oMelwaysRef = null;
		private int iKeepXItemsAtEachNode = 15;
		/// <summary>
		/// Construct a new place lookup tree
		/// </summary>
		public WaypointLookupTree()
		{
			try
			{
			}
			catch(System.Exception ex)
			{
				throw(ex);
			}
		}
		public WaypointLookupTree(string sDSN)
		{
            CreateObject(sDSN, 15, -1);
		}
		public WaypointLookupTree(string sDSN, int KeepXItemsAtEachNode)
		{
            CreateObject(sDSN, KeepXItemsAtEachNode, -1);
		}
        public WaypointLookupTree(string sDSN, int KeepXItemsAtEachNode, int SPGroupID)
        {
            CreateObject(sDSN, KeepXItemsAtEachNode, SPGroupID);
        }
        private void CreateObject(string sDSN, int KeepXItemsAtEachNode, int SPGroupID)
		{
			int iGroupID = 0;
            int iCurGroupID = 0;
			int iID = 0;
            int iSetPointID = 0;
			int iSetPointNumber = 0;
            int iCurSetPointID = 0;
			int iFlags = 0;
			double dLat = 0;
			double dLong = 0;
			double dXTolerance = 0;
			double dYTolerance = 0;
			string sPlaceName = "";
			string sMelwaysRef = "";
			double dMinLat = 0;
			double dMinLong = 0;
			double dMaxLat = 0;
			double dMaxLong = 0;

			SqlConnection oConn = null;
			SqlCommand oCmd = null;
			DataSet oRawData = null;
            DataSet dsPolygonNodes = null;
            DataRow drPolygonNode = null;
			SqlDataAdapter oSQLDA = null;
            Hashtable oPolygonNodesToGroup = null;
            Hashtable oPolygonNodesToPoint = null;
            ArrayList oPolygonNodes = null;
            double[] dLatLong = null;

            try
            {
                iKeepXItemsAtEachNode = KeepXItemsAtEachNode;
                #region Connect to the DB
                try
                {
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                }
                catch (System.Exception ex)
                {
                    if (oConn != null)
                    {
                        oConn.Dispose();
                        oConn = null;
                    }
                    _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
                    throw (ex);
                }
                #endregion
                #region Retrieve data from the T_SetPoint tables
                try
                {
                    if (oConn != null)
                    {
                        string sSQL = "SELECT sp.[ID],spg.ID as [SetPointGroupID],sp.[SetPointNumber],sp.[Name],sp.[Flags],sp.[Latitude],sp.[Longitude],sp.YTolerance,sp.XTolerance, sp.ToleranceNSMetres FROM [T_SetPoint] sp INNER JOIN [T_SetPointGroup] spg ON sp.SetPointGroupID = spg.ID WHERE sp.[Active] = 1";
                        if (SPGroupID > 0)
                            sSQL += " AND spg.ID = " + SPGroupID;
                        oCmd = new SqlCommand(sSQL, oConn);
                        oSQLDA = new System.Data.SqlClient.SqlDataAdapter(oCmd);
                        oRawData = new DataSet();
                        oSQLDA.Fill(oRawData);
                        oSQLDA.Dispose();
                        oSQLDA = null;
                        oCmd.Dispose();
                        oCmd = null;
                    }
                }
                catch (System.Exception ex)
                {
                    if (oSQLDA != null)
                    {
                        oSQLDA.Dispose();
                        oSQLDA = null;
                    }
                    if (oCmd != null)
                    {
                        oCmd.Dispose();
                        oCmd = null;
                    }
                    if (oConn != null)
                    {
                        oConn.Dispose();
                        oConn = null;
                    }
                    _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
                    throw (ex);
                }
                #endregion
                #region Retrieve data from the T_SetPointNode tables
                try
                {
                    if (oConn != null)
                    {
                        string sSQL = "SELECT sp.ID, sp.SetPointGroupID, sp.SetPointNumber, spn.Sequence, spn.Latitude,spn.Longitude FROM (T_SetPointNode spn INNER JOIN T_SetPoint sp ON spn.SetPointID = sp.ID) INNER JOIN T_SetPointGroup spg ON sp.SetPointGroupID = spg.ID WHERE sp.Active = 1 ORDER BY sp.SetPointGroupID, spn.SetPointID, spn.Sequence";
                        if (SPGroupID > 0)
                            sSQL = "SELECT sp.ID, sp.SetPointGroupID, sp.SetPointNumber, spn.Sequence, spn.Latitude,spn.Longitude FROM (T_SetPointNode spn INNER JOIN T_SetPoint sp ON spn.SetPointID = sp.ID) INNER JOIN T_SetPointGroup spg ON sp.SetPointGroupID = spg.ID WHERE sp.Active = 1 AND sp.SetPointGroupID =  " + SPGroupID + " ORDER BY sp.SetPointGroupID, spn.SetPointID, spn.Sequence";
                        oCmd = new SqlCommand(sSQL, oConn);
                        oSQLDA = new System.Data.SqlClient.SqlDataAdapter(oCmd);
                        dsPolygonNodes = new DataSet();
                        oSQLDA.Fill(dsPolygonNodes);
                        oSQLDA.Dispose();
                        oSQLDA = null;
                        oCmd.Dispose();
                        oCmd = null;
                        oConn.Close();
                        oConn.Dispose();
                        oConn = null;
                    }
                }
                catch (System.Exception ex)
                {
                    if (oSQLDA != null)
                    {
                        oSQLDA.Dispose();
                        oSQLDA = null;
                    }
                    if (oCmd != null)
                    {
                        oCmd.Dispose();
                        oCmd = null;
                    }
                    if (oConn != null)
                    {
                        oConn.Dispose();
                        oConn = null;
                    }
                    _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
                    throw (ex);
                }
                #endregion
                #region Create the melways lookup
                try
                {
                    oMelwaysRef = new MelwaysLookup(sDSN);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
                    oMelwaysRef = null;
                }
                #endregion
                #region Populate the Polygon Node lookup objects
                try
                {
                    if (dsPolygonNodes != null)
                    {
                        if (dsPolygonNodes.Tables.Count > 0)
                        {
                            if (dsPolygonNodes.Tables[0].Rows.Count > 0)
                            {
                                for (int X = 0; X < dsPolygonNodes.Tables[0].Rows.Count; X++)
                                {
                                    drPolygonNode = dsPolygonNodes.Tables[0].Rows[X];
                                    iGroupID = Convert.ToInt32(drPolygonNode["SetPointGroupID"]);
                                    iSetPointID = Convert.ToInt32(drPolygonNode["ID"]);
                                    if (iCurGroupID == iGroupID && iCurSetPointID == iSetPointID && oPolygonNodesToGroup != null && oPolygonNodesToGroup.ContainsKey(iGroupID))
                                    {
                                        #region We are looking at another node on the same point
                                        oPolygonNodesToPoint = (Hashtable)oPolygonNodesToGroup[iGroupID];
                                        oPolygonNodesToGroup.Remove(iGroupID);
                                        if (oPolygonNodesToPoint.ContainsKey(iSetPointID))
                                        {
                                            oPolygonNodes = (ArrayList)oPolygonNodesToPoint[iSetPointID];
                                            oPolygonNodesToPoint.Remove(iSetPointID);
                                        }
                                        else
                                        {
                                            oPolygonNodes = new ArrayList();
                                        }
                                        dLatLong = new double[2];
                                        dLatLong[0] = Convert.ToDouble(drPolygonNode["Latitude"]);
                                        dLatLong[1] = Convert.ToDouble(drPolygonNode["Longitude"]);
                                        oPolygonNodes.Add(dLatLong);
                                        oPolygonNodesToPoint.Add(iSetPointID, oPolygonNodes);
                                        oPolygonNodesToGroup.Add(iGroupID, oPolygonNodesToPoint);
                                        #endregion
                                    }
                                    else
                                    {
                                        if (oPolygonNodesToGroup == null)
                                            oPolygonNodesToGroup = Hashtable.Synchronized(new Hashtable());
                                        #region Check the last added group to see if the WP is a circle
                                        if (iCurSetPointID > 0 && iCurGroupID > 0)
                                        {
                                            if (oPolygonNodesToGroup.ContainsKey(iCurGroupID))
                                            {
                                                oPolygonNodesToPoint = (Hashtable)oPolygonNodesToGroup[iCurGroupID];
                                                if (oPolygonNodesToPoint.Contains(iCurSetPointID))
                                                {
                                                    oPolygonNodes = (ArrayList)oPolygonNodesToPoint[iCurSetPointID];
                                                    if (oPolygonNodes.Count == 1)
                                                    {
                                                        #region This is a circle point
                                                        if (oRawData.Tables.Count > 0)
                                                        {
                                                            DataRow[] drSetPoints = oRawData.Tables[0].Select("SetPointGroupID = " + Convert.ToString(iCurGroupID) + " AND ID = " + Convert.ToString(iCurSetPointID));
                                                            oPolygonNodesToGroup.Remove(iCurGroupID);
                                                            oPolygonNodesToPoint.Remove(iCurSetPointID);

                                                            oPolygonNodes = new ArrayList();
                                                            dLatLong = new double[2];
                                                            dLatLong[0] = Convert.ToDouble(drSetPoints[0]["ToleranceNSMetres"]) + 5.0;
                                                            dLatLong[1] = Convert.ToDouble(0);
                                                            oPolygonNodes.Add(dLatLong);

                                                            oPolygonNodesToPoint.Add(iCurSetPointID, oPolygonNodes);
                                                            oPolygonNodesToGroup.Add(iCurGroupID, oPolygonNodesToPoint);
                                                        }
                                                        #endregion
                                                    }
                                                }
                                            }
                                        }
                                        #endregion
                                        #region We are looking at new set point
                                        iCurGroupID = iGroupID;
                                        iCurSetPointID = iSetPointID;
                                        if (oPolygonNodesToGroup.ContainsKey(iGroupID))
                                        {
                                            #region We already have another polygon for this setpoint group
                                            oPolygonNodesToPoint = (Hashtable)oPolygonNodesToGroup[iGroupID];
                                            oPolygonNodesToGroup.Remove(iGroupID);
                                            #endregion
                                        }
                                        else
                                        {
                                            #region This is the first polygon for this setpoint group
                                            oPolygonNodesToPoint = new Hashtable();
                                            #endregion
                                        }
                                        oPolygonNodes = new ArrayList();
                                        dLatLong = new double[2];
                                        dLatLong[0] = Convert.ToDouble(drPolygonNode["Latitude"]);
                                        dLatLong[1] = Convert.ToDouble(drPolygonNode["Longitude"]);
                                        oPolygonNodes.Add(dLatLong);
                                        oPolygonNodesToPoint.Add(iSetPointID, oPolygonNodes);
                                        oPolygonNodesToGroup.Add(iGroupID, oPolygonNodesToPoint);
                                        #endregion
                                    }
                                }
                                // Copy the synchronised hashtable to the class variable oPolygonNodesLookup.
                                oPolygonNodesLookup = oPolygonNodesToGroup;
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
                    throw (ex);
                }
                #endregion
                #region Create and populate the tree
                try
                {
                    if (oRawData != null)
                    {
                        if (oRawData.Tables.Count > 0)
                        {
                            for (int X = 0; X < oRawData.Tables[0].Rows.Count; X++)
                            {
                                // If the record has all the required values
                                if (oRawData.Tables[0].Rows[X]["ID"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["SetPointGroupID"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["SetPointNumber"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["Name"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["Flags"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["Latitude"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["Longitude"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["XTolerance"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["YTolerance"] != System.DBNull.Value)
                                {
                                    // sp.[ID],spg.ID as [SetPointGroupID],sp.[SetPointNumber],sp.[Name],sp.[Flags],sp.[Latitude],sp.[Longitude],sp.[ToleranceNSMetres],sp.[ToleranceEWMetres]
                                    // If the location is valid.
                                    try
                                    {
                                        iGroupID = Convert.ToInt32(oRawData.Tables[0].Rows[X]["SetPointGroupID"]);
                                        iID = Convert.ToInt32(oRawData.Tables[0].Rows[X]["ID"]);
                                        iSetPointNumber = Convert.ToInt32(oRawData.Tables[0].Rows[X]["SetPointNumber"]);
                                        iFlags = Convert.ToInt32(oRawData.Tables[0].Rows[X]["Flags"]);
                                        sPlaceName = Convert.ToString(oRawData.Tables[0].Rows[X]["Name"]);
                                        dXTolerance = Convert.ToDouble(oRawData.Tables[0].Rows[X]["XTolerance"]);
                                        dYTolerance = Convert.ToDouble(oRawData.Tables[0].Rows[X]["YTolerance"]);
                                        dLat = Convert.ToDouble(oRawData.Tables[0].Rows[X]["Latitude"]);
                                        dLong = Convert.ToDouble(oRawData.Tables[0].Rows[X]["Longitude"]);

                                        //dYTolerance = dYTolerance + 42;
                                        //dXTolerance = dXTolerance + 42;

                                        dMinLat = ((dLat * 60000) - dYTolerance) / 60000;
                                        dMaxLat = ((dLat * 60000) + dYTolerance) / 60000;
                                        dMinLong = ((dLong * 60000) - dXTolerance) / 60000;
                                        dMaxLong = ((dLong * 60000) + dXTolerance) / 60000;
                                        // Trucate values after the 5th decimal place.
                                        dMinLat = Convert.ToDouble(Convert.ToInt32(dMinLat * 100000)) / Convert.ToDouble(100000);
                                        dMaxLat = Convert.ToDouble(Convert.ToInt32(dMaxLat * 100000)) / Convert.ToDouble(100000);
                                        dMinLong = Convert.ToDouble(Convert.ToInt32(dMinLong * 100000)) / Convert.ToDouble(100000);
                                        dMaxLong = Convert.ToDouble(Convert.ToInt32(dMaxLong * 100000)) / Convert.ToDouble(100000);
                                        // Get the melways ref for the WP
                                        if (oMelwaysRef != null)
                                            sMelwaysRef = oMelwaysRef.Query(dLat, dLong);
                                        else
                                            sMelwaysRef = "";
                                        this.Insert(iGroupID, iID, iSetPointNumber, iFlags, sPlaceName, dMinLat, dMinLong, dMaxLat, dMaxLong, sMelwaysRef);
                                    }
                                    catch (System.Exception exAddToTree)
                                    {
                                        string sValues = "Values : iGroupID = " + Convert.ToString(iGroupID) + ", iID = " + Convert.ToString(iID) + ", iSetPointNumber = " + Convert.ToString(iSetPointNumber) + ", iFlags = " + Convert.ToString(iFlags) + "\r\n";
                                        sValues += "sPlaceName = '" + sPlaceName + "', dXTolerance = " + Convert.ToString(dXTolerance) + ", dYTolerance = " + Convert.ToString(dYTolerance) + "\r\n";
                                        sValues += "dLat = " + Convert.ToString(dLat) + ", dLong = " + Convert.ToString(dLong) + "\r\n";
                                        _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)\r\n" + sValues, exAddToTree);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
                    throw (ex);
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "CreateObject(string sDSN, int KeepXItemsAtEachNode)", ex);
            }
		}

        public void Dispose()
        {
            if(oWPTrees != null)
            {
                if (oWPTrees.Keys.Count > 0)
                {
                    int[] iKeys = new int[oWPTrees.Keys.Count];
                    oWPTrees.Keys.CopyTo(iKeys, 0);

                    for (int X = 0; X < iKeys.Length; X++)
                    {
                        BoundaryBox oWPTree = (BoundaryBox)oWPTrees[iKeys[X]];
                        oWPTree.Dispose();
                        oWPTrees.Remove(iKeys[X]);
                    }
                }
                oWPTrees.Clear();
                oWPTrees = null;
            }
            if(oWPIDLookups != null)
            {
                oWPIDLookups.Clear();
                oWPIDLookups = null;
            }
            if(oPolygonNodesLookup != null)
            {
                oPolygonNodesLookup.Clear();
                oPolygonNodesLookup = null;
            }
        }
		public MelwaysLookup oMelwaysRefLookup { get {return oMelwaysRef;}}

		/// <summary>
		/// Get the count of items in the tree
		/// </summary>
		public int Count { 
			get
			{ 
				BoundaryBox oWPTree = null;
				int iResult = 0;
				if(oWPTrees.Keys.Count > 0)
				{
					int[] iKeys = new int[oWPTrees.Keys.Count];
					oWPTrees.Keys.CopyTo(iKeys, 0);

					for (int X = 0; X < iKeys.Length; X++)
					{
						oWPTree = (BoundaryBox) oWPTrees[iKeys[X]];
						iResult += oWPTree.Count;
					}
				}
				return iResult; 
			}
		}

		/// <summary>
		/// Insert the place into the tree
		/// </summary>
		/// <param name="item"></param>
		public void Insert(int iGroupID, int iID, int iSetPointNumber, int iFlags, string sPlaceName, double dMinLat, double dMinLong, double dMaxLat, double dMaxLong, string sMelwaysRef)
		{
			BoundaryBox oWPTree = null;
			Hashtable oWPItems = null;
			try
			{
				WayPoint oItem = new WayPoint(iID, iSetPointNumber, iFlags, sPlaceName, dMinLat, dMinLong, dMaxLat, dMaxLong, sMelwaysRef);

				if(oWPTrees == null)
					oWPTrees = Hashtable.Synchronized(new Hashtable());
				if(oWPIDLookups == null)
					oWPIDLookups = Hashtable.Synchronized(new Hashtable());

				lock(oWPTrees.SyncRoot)
				{
					if(oWPTrees.Contains(iGroupID))
					{
						oWPTree = (BoundaryBox) oWPTrees[iGroupID];
						oWPTree.Insert((TreeItem) oItem);
					}
					else
					{
						oWPTree = new BoundaryBox(iKeepXItemsAtEachNode);
						oWPTree.Insert((TreeItem) oItem);
						oWPTrees.Add(iGroupID, oWPTree);
					}
				}

				lock(oWPIDLookups.SyncRoot)
				{
					if(oWPIDLookups.Contains(iGroupID))
					{
						oWPItems = (Hashtable) oWPIDLookups[iGroupID];
						if(oWPItems.Contains(oItem.iSetPointNumber))
						{
							// There is a duplicate WP
						}
						else
						{
							oWPItems.Add(oItem.iSetPointNumber, oItem);
						}	
					}
					else
					{
						oWPItems = new Hashtable();
						oWPItems.Add(oItem.iSetPointNumber, oItem);
						oWPIDLookups.Add(iGroupID, oWPItems);
					}
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "Insert(int iGroupID, int iID, int iSetPointNumber, int iFlags, string sPlaceName, double dMinLat, double dMinLong, double dMaxLat, double dMaxLong, string sMelwaysRef)", ex);
				throw(ex);
			}
		}
		/// <summary>
		/// Query the QuadTree, returning the items that are in the given area
		/// </summary>
		/// <param name="area"></param>
		/// <returns></returns>
		public ArrayList Query(int iGroupID, double dLat, double dLong)
		{
			return Query(iGroupID, dLat, dLong, false);
		}
		public ArrayList Query(int iGroupID, double dLat, double dLong, bool bReturnMultipleResults)
		{
			ArrayList oResults = new ArrayList();
			ArrayList oItems = null;
			WayPoint oResult = null;
			double dSearchArea = 0.9;  // A litte bigger than a 10km area...  
			BoundaryBox oWPTree = null;
			double dDistance = double.MaxValue;
			double dSize = double.MaxValue;
			double dTemp = 0;
			ArrayList oFilter = null;
            bool bProcessedPolygon = false;
            Hashtable oPolygonPoints = null;
            ArrayList oPolygonNodes = null;
			try
			{
				oItems = new ArrayList();
				oResults = new ArrayList();
                #region If there are any polygon points for this iGroupID, get the Hashtable of setpointIDs for the group
                if (oPolygonNodesLookup != null)
                {
                    if (oPolygonNodesLookup.ContainsKey(iGroupID))
                    {
                        oPolygonPoints = (Hashtable)oPolygonNodesLookup[iGroupID];
                    }
                }
                #endregion
                if (oWPTrees != null)
				{
					lock(oWPTrees.SyncRoot)
					{
						if(oWPTrees.Contains(iGroupID))
						{
							oWPTree = (BoundaryBox) oWPTrees[iGroupID];
							oItems = oWPTree.Query(dLat, dLong, dSearchArea);
						}
					}
				}
				if(oItems != null)
				{
					if (oItems.Count > 0)
					{
						#region If we have customers in the search area, check which ones we are in
						for (int X = 0; X < oItems.Count; X++)
						{
							oResult = (WayPoint)oItems[X];
                            bProcessedPolygon = false;
                            #region Polygon Processing
                            if (oPolygonPoints != null)
                            {
                                if (oPolygonPoints.ContainsKey(oResult.iID))
                                {
                                    oPolygonNodes = (ArrayList)oPolygonPoints[oResult.iID];
                                    if (oPolygonNodes.Count > 1)
                                    {
                                        if (PolygonContainsPoint(dLat, dLong, oPolygonNodes))
                                            oResults.Add(oResult);
                                        bProcessedPolygon = true;
                                    }
                                    else
                                    {
                                        if (oPolygonNodes.Count == 1)
                                        {
                                            #region This is a polygon circle
                                            dTemp = oResult.GetDistanceInMeters(dLat, dLong);
                                            oPolygonNodes = (ArrayList)oPolygonPoints[oResult.iID];
                                            double dRadius = ((double[]) oPolygonNodes[0])[0];
                                            if (dTemp <= dRadius)
                                                oResults.Add(oResult);
                                            bProcessedPolygon = true;
                                            #endregion
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region If this result is not a polygon WP or not in the oPolygonNodesLookup object
                            if (!bProcessedPolygon)
                            {
                                if (oResult.oTestArea.ContainsPoint(dLat, dLong))
                                {
                                    oResults.Add(oResult);
                                }
                            }
                            #endregion
                        }
						#endregion
						if(!bReturnMultipleResults)
						{
							#region If we are within more than 1 way point
							if(oResults.Count > 1)
							{
								oFilter = new ArrayList();
								#region Find the smallest 
								for(int X= 0; X < oResults.Count; X++)
								{
									oResult = (WayPoint) oResults[X];
									dTemp = oResult.oTestArea.AreaSize;
									if(dTemp < dSize)
									{
										oFilter = new ArrayList();
										oFilter.Add(oResult);
										dSize = dTemp;
									}
									else if (dTemp == dSize)
									{
										oFilter.Add(oResult);
									}
								}
								#endregion

								if(oFilter.Count > 1)
								{
									#region If we are within more than 1 way point and the two smallest waypoints we are within are the same size, find the Way Point we are closest to the centre of.
									#region Copy the smallest WP items back into oResults
									oResults = new ArrayList();
									for(int X= 0; X < oFilter.Count; X++)
									{
										oResults.Add(oFilter[X]);
									}
									oFilter = new ArrayList();
									#endregion
									for(int X= 0; X < oResults.Count; X++)
									{
										oResult = (WayPoint) oResults[X];
										dTemp = oResult.GetDistanceToPoint(dLat, dLong);
										if(dTemp < dDistance)
										{
											oFilter = new ArrayList();
											oFilter.Add(oResult);
											dDistance = dTemp;
										}
										else if (dTemp == dDistance)
										{
											oFilter.Add(oResult);
										}
									}
									#endregion
								}
								oResults = new ArrayList();
								if(oFilter.Count > 0)
								{
									// If we still have more than one match, just pick the first in the list.
									oResults.Add(oFilter[0]);
								}
							}
							#endregion
						}
					}
				}
			}
			catch (System.Exception ex)
			{
				throw (ex);
			}
			return oResults;
		}
		public ArrayList Query(int iGroupID, int iSetPointID)
		{
			// Query for a given group/setpoint number
			ArrayList oResults = new ArrayList();
			Hashtable oWPItems = null;
			WayPoint oItem = null;
			try
			{
				lock(oWPIDLookups.SyncRoot)
				{
					if(oWPIDLookups.Contains(iGroupID))
					{
						oWPItems = (Hashtable) oWPIDLookups[iGroupID];
                        if (oWPItems.Contains(iSetPointID))
						{
                            oItem = (WayPoint)oWPItems[iSetPointID];
							oResults.Add(oItem);
						}
					}
				}
			}
			catch (System.Exception ex)
			{
				throw (ex);
			}
			return oResults;
		}
        private bool PolygonContainsPoint(double dLat, double dLong, ArrayList oPolygonNodes)
        {
            // Taken from embedded code :
            //for (i = 0, j = _polysides - 1; i < _polysides; j = i++)
            //{
            //    if (((_lat[i] <= _lat_pos) && (_lat_pos < _lat[j])) || ((_lat[j] <= _lat_pos) && (_lat_pos < _lat[i])))
            //    {
            //        if (_long_pos < (_long[j] - _long[i]) * (_lat_pos - _lat[i]) / (_lat[j] - _lat[i]) + _long[i])
            //        {
            //            oddNodes = !oddNodes;
            //        }
            //    }
            //}

            bool bRet = false;
            bool bInsidePolygon = false;
            int Y = 0;
            double[] dTemp = null;
            double dNodeLat = 0;
            double dNodeLong = 0;
            double dNextNodeLat = 0;
            double dNextNodeLong = 0;

            try
            {
                for (int X = 0; X < oPolygonNodes.Count; X++)
                {
                    #region Set Y equal to the next point in the array, set Y to the first point if X is the last node in the array.
                    if (X == oPolygonNodes.Count - 1)
                        Y = 0;
                    else
                        Y = X + 1;
                    #endregion
                    #region Get dNodeLat and dNodeLong as current lat/long and dNextNodeLat and dNextNodeLong as the next lat/long
                    dTemp = (double[])oPolygonNodes[X];
                    dNodeLat = dTemp[0];
                    dNodeLong = dTemp[1];
                    dTemp = (double[])oPolygonNodes[Y];
                    dNextNodeLat = dTemp[0];
                    dNextNodeLong = dTemp[1];
                    #endregion
                    // If the current lat (dLat) is between the dNodeLat and dNextNodeLat points
                    if ((dNodeLat <= dLat && dLat < dNextNodeLat) || (dNextNodeLat <= dLat && dLat < dNodeLat))
                    {
                        if (dLong < (dNextNodeLong - dNodeLong) * (dLat - dNodeLat) / (dNextNodeLat - dNodeLat) + dNodeLong)
                        {
                            bInsidePolygon = !bInsidePolygon;
                        }
                    }
                }
                bRet = bInsidePolygon;
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return bRet;
        }
	}
}
using System;

namespace MTData.Common.PlaceLookup
{
    /// <summary>
    /// Summary description for cBoundary.
    /// </summary>
    public class Boundary
    {
        public double dMinLat = 0;
        public double dMinLong = 0;
        public double dMaxLat = 0;
        public double dMaxLong = 0;

        public Boundary(double MinLat, double MinLong, double MaxLat, double MaxLong)
        {
            dMinLat = MinLat;
            dMinLong = MinLong;
            dMaxLat = MaxLat;
            dMaxLong = MaxLong;
        }

        public bool ContainsArea(Boundary oTestArea)
        {
            return ContainsArea(oTestArea.dMinLat, oTestArea.dMinLong, oTestArea.dMaxLat, oTestArea.dMaxLong);
        }
        public bool ContainsArea(double MinLat, double MinLong, double MaxLat, double MaxLong)
        {
            bool bRet = false;
            if (MinLat >= dMinLat && MinLat <= dMaxLat && MinLong >= dMinLong && MaxLong <= dMaxLong)
            {
                if (MaxLat >= dMinLat && MaxLat <= dMaxLat && MaxLong >= dMinLong && MaxLong <= dMaxLong)
                {
                    bRet = true;
                }
            }
            return bRet;
        }

        public bool IntersectsArea(Boundary oTestArea)
        {
            return IntersectsArea(oTestArea.dMinLat, oTestArea.dMinLong, oTestArea.dMaxLat, oTestArea.dMaxLong);
        }
        public bool IntersectsArea(double MinLat, double MinLong, double MaxLat, double MaxLong)
        {
            bool bRet = false;
            // If any point is within the bounds, than the two shaped intersect.
            if (MinLat >= dMinLat && MinLat <= dMaxLat)
            {
                bRet = true;
            }
            else if (MinLong >= dMinLong && MinLong <= dMaxLong)
            {
                bRet = true;
            }
            else if (MaxLat >= dMinLat && MaxLat <= dMaxLat)
            {
                bRet = true;
            }
            else if (MaxLong >= dMinLong && MaxLong <= dMaxLong)
            {
                bRet = true;
            }
            return bRet;
        }

        public bool ContainedByArea(Boundary oTestArea)
        {
            return ContainedByArea(oTestArea.dMinLat, oTestArea.dMinLong, oTestArea.dMaxLat, oTestArea.dMaxLong);
        }
        public bool ContainedByArea(double MinLat, double MinLong, double MaxLat, double MaxLong)
        {
            bool bRet = false;
            if (dMinLat >= MinLat && dMinLat <= MaxLat && dMinLong >= MinLong && dMaxLong <= MaxLong)
            {
                if (dMaxLat >= MinLat && dMaxLat <= MaxLat && dMaxLong >= MinLong && dMaxLong <= MaxLong)
                {
                    bRet = true;
                }
            }
            return bRet;
        }

        public bool ContainsPoint(double Lat, double Long)
        {
            if (Lat >= dMinLat && Lat <= dMaxLat && Long >= dMinLong && Long <= dMaxLong)
                return true;
            else
                return false;
        }
		public double AreaSize
		{
			get { return (dMaxLat - dMinLat) * (dMaxLong - dMinLong);}
		}
	}
}

using System;

namespace MTData.Common.PlaceLookup
{
    /// <summary>
    /// Summary description for cPlaceName.
    /// </summary>
    public class PlaceName : TreeItem
    {
        public int iGroupID = 0;
        public string sMapRef = "";
        public string sPosition = "";
        public double dDistance = 0;

		public PlaceName(int GroupID, int ID, string PlaceName, double Lat, double Long, string sMelwaysRef)
            : base(ID, PlaceName, Lat, Long, Lat + 0.000001, Long + 0.000001)
        {
            iGroupID = GroupID;
			sMapRef = sMelwaysRef;
        }
    }
}

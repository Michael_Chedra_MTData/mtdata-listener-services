using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;

namespace MTData.Common.PlaceLookup
{
	public class MelwaysLookup
	{
		/// <summary>
		/// The root of the tree
		/// </summary>
		private BoundaryBox oTree = null; // m_root
		/// <summary>
		/// Construct a new place lookup tree
		/// </summary>
		public MelwaysLookup()
		{
			try
			{
				oTree = new BoundaryBox(15);
			}
			catch(System.Exception ex)
			{
				throw(ex);
			}
		}
		public MelwaysLookup(string sDSN)
		{
			CreateObject(sDSN, 15);
		}
		public MelwaysLookup(string sDSN, int KeepXItemsAtEachNode)
		{
			CreateObject(sDSN, KeepXItemsAtEachNode);
		}
		private void CreateObject(string sDSN, int KeepXItemsAtEachNode)
		{
			int iID = 0;
			string sPlaceName = "";
			double dMinLat = 0;
			double dMinLong = 0;
			double dMaxLat = 0;
			double dMaxLong = 0;			
			SqlConnection oConn = null;
			SqlCommand oCmd = null;
			DataSet oRawData = null;
			SqlDataAdapter oSQLDA = null;

			#region Connect to the DB
			try
			{
				oConn = new SqlConnection(sDSN);
				oConn.Open();
			}
			catch (System.Exception ex)
			{
				if (oConn != null)
				{
					oConn.Dispose();
					oConn = null;
				}
				throw (ex);
			}
			#endregion
			#region Retrieve data from the T_MelwayMap table
			try
			{
				oCmd = new SqlCommand("SELECT ID, FileID as Name, LatitudeLowerLeft as MinLat, LongitudeLowerLeft as MinLong, LatitudeUpperRight as MaxLat, LongitudeUpperRight as MaxLong	FROM T_MelwayMap WITH (NOLOCK)", oConn);
				oSQLDA = new System.Data.SqlClient.SqlDataAdapter(oCmd);
				oRawData = new DataSet();
				oSQLDA.Fill(oRawData);
				oSQLDA.Dispose();
				oSQLDA = null;
				oCmd.Dispose();
				oCmd = null;
				oConn.Close();
				oConn.Dispose();
				oConn = null;
			}
			catch (System.Exception ex)
			{
				if (oSQLDA != null)
				{
					oSQLDA.Dispose();
					oSQLDA = null;
				}
				if (oCmd != null)
				{
					oCmd.Dispose();
					oCmd = null;
				}
				if (oConn != null)
				{
					oConn.Dispose();
					oConn = null;
				}
				throw (ex);
			}
			#endregion
			#region Create and populate the tree
			try
			{
				oTree = new BoundaryBox(KeepXItemsAtEachNode);
				if (oTree != null)
				{
					if (oRawData != null)
					{
						if (oRawData.Tables.Count > 0)
						{
							for (int X = 0; X < oRawData.Tables[0].Rows.Count; X++)
							{
								// SELECT FileID, LongitudeLowerLeft, LatitudeLowerLeft, LongitudeUpperRight, LatitudeUpperRight FROM T_MelwayMap
								// If the record has all the required values
								if (oRawData.Tables[0].Rows[X]["ID"] != System.DBNull.Value && oRawData.Tables[0].Rows[X]["Name"] != System.DBNull.Value 
									&& oRawData.Tables[0].Rows[X]["MinLat"] != System.DBNull.Value && oRawData.Tables[0].Rows[X]["MinLong"] != System.DBNull.Value 
									&& oRawData.Tables[0].Rows[X]["MaxLat"] != System.DBNull.Value && oRawData.Tables[0].Rows[X]["MaxLong"] != System.DBNull.Value) 
								{
									// If the location is valid.
									try
									{
										iID = Convert.ToInt32(oRawData.Tables[0].Rows[X]["ID"]);
										sPlaceName = Convert.ToString(oRawData.Tables[0].Rows[X]["Name"]).Trim();
										dMinLat = Convert.ToDouble(oRawData.Tables[0].Rows[X]["MinLat"]);
										dMaxLat = Convert.ToDouble(oRawData.Tables[0].Rows[X]["MaxLat"]);
										dMinLong = Convert.ToDouble(oRawData.Tables[0].Rows[X]["MinLong"]);
										dMaxLong = Convert.ToDouble(oRawData.Tables[0].Rows[X]["MaxLong"]);
										this.Insert(iID, sPlaceName, dMinLat, dMinLong, dMaxLat, dMaxLong);
									}
									catch(System.Exception)
									{
									}
								}
							}
						}
					}
				}
			}
			catch (System.Exception ex)
			{
				throw (ex);
			}
			#endregion
		}
		/// <summary>
		/// Get the count of items in the tree
		/// </summary>
		public int Count { get { return oTree.Count; } }

		/// <summary>
		/// Insert the place into the tree
		/// </summary>
		/// <param name="item"></param>
		public void Insert(int iID, string sPlaceName, double dMinLat, double dMinLong, double dMaxLat, double dMaxLong)
		{
			try
			{
				MelwayRef oItem = new MelwayRef(iID, sPlaceName, dMinLat, dMinLong, dMaxLat, dMaxLong);
				oTree.Insert((TreeItem) oItem);
			}
			catch(System.Exception ex)
			{
				throw(ex);
			}
		}
		/// <summary>
		/// Query the QuadTree, returning the items that are in the given area
		/// </summary>
		/// <param name="area"></param>
		/// <returns></returns>
		public string Query(double dLat, double dLong)
		{
			string sRet = "";
			ArrayList oItems = null;
			ArrayList oResults = null;
			MelwayRef oResult = null;
			double dRefSize = double.MaxValue;
			double dSearchArea = 0.02;  // A litte bigger than a 2km area...  

			try
			{
				oItems = new ArrayList();
				oResults = new ArrayList();
				oItems = oTree.Query(dLat, dLong, dSearchArea);
				if (oItems.Count > 0)
				{
					#region If we have customers in the search area, check which ones we are in
					for (int X = 0; X < oItems.Count; X++)
					{
						oResult = (MelwayRef)oItems[X];
						if(oResult.oTestArea.ContainsPoint(dLat, dLong))
						{
							oResults.Add(oResult);
						}
					}
					oResult = null;
					for(int X = 0; X < oResults.Count; X++)
					{
						if(((MelwayRef) oResults[X]).dArea < dRefSize)
						{
							oResult = (MelwayRef) oResults[X];
							dRefSize = oResult.dArea;
						}
					}
					if(oResult != null)
					{
						sRet = oResult.GetMelwaysSector(dLat, dLong);
					}
					#endregion
				}
			}
			catch (System.Exception ex)
			{
				throw (ex);
			}
			return sRet;
		}
	}
}
using System;
using System.Collections;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Common.PlaceLookup
{
    public class CustLookupTree
    {
        /// <summary>
        /// The root of the tree
        /// </summary>
        private Hashtable oByGroup = Hashtable.Synchronized(new Hashtable());
        private BoundaryBox oTree = null; // m_root
        private MelwaysLookup oMelwaysRef = null;
        private int iKeepXNodesAtEachLevel = 15;
        /// <summary>
        /// Construct a new place lookup tree
        /// </summary>
        public CustLookupTree()
        {
            try
            {
                oTree = new BoundaryBox(15);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
        public CustLookupTree(string sDSN)
        {
            CreateObject(sDSN, 15);
        }
        public CustLookupTree(string sDSN, int KeepXItemsAtEachNode)
        {
            CreateObject(sDSN, KeepXItemsAtEachNode);
        }
        private void CreateObject(string sDSN, int KeepXItemsAtEachNode)
        {
            bool bCustomerListSelected = false;
            bool bIsCountryCustomer = false;
            int iGroupID = 0;
            int iListVersion = 0;
            int iID = 0;
            double dLat = 0;
            double dLong = 0;
            double dXTolerance = 0;
            double dYTolerance = 0;
            string sPlaceName = "";
            string sMelwaysRef = "";
            double dMinLat = 0;
            double dMinLong = 0;
            double dMaxLat = 0;
            double dMaxLong = 0;
            double dLatHeight = 0;
            double dLongWidth = 0;
            SqlConnection oConn = null;
            SqlCommand oCmd = null;
            DataSet oRawData = null;
            SqlDataAdapter oSQLDA = null;

            iKeepXNodesAtEachLevel = KeepXItemsAtEachNode;
            if (iKeepXNodesAtEachLevel < 2)
                iKeepXNodesAtEachLevel = 2;

            #region Retrieve data from the T_Places table
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
                oCmd = new SqlCommand("SELECT Distinct cust.CustomerGroupID as GroupID, cust.ListVersion as Version, cust.CustomerID as ID, cust.Name, cust.CountryMetroType as IsCountryCustomer, cust.Latitude, cust.Longitude, cust.Tolerance as XTolerance, cust.Tolerance as YTolerance FROM T_CustomerVersions cust WHERE cust.ListVersion = (SELECT max(grp.ListVersion) FROM T_CustomerGroup grp WHERE grp.CustomerGroupID = cust.CustomerGroupID) Order BY cust.ID", oConn);
                oSQLDA = new System.Data.SqlClient.SqlDataAdapter(oCmd);
                oRawData = new DataSet();
                oSQLDA.Fill(oRawData);
                oSQLDA.Dispose();
                oSQLDA = null;
                oCmd.Dispose();
                oCmd = null;
                oConn.Close();
                oConn.Dispose();
                oConn = null;
                bCustomerListSelected = true;
            }
            catch (System.Exception)
            {
                bCustomerListSelected = false;
                if (oSQLDA != null)
                {
                    oSQLDA.Dispose();
                    oSQLDA = null;
                }
                if (oCmd != null)
                {
                    oCmd.Dispose();
                    oCmd = null;
                }
                if (oConn != null)
                {
                    oConn.Dispose();
                    oConn = null;
                }
            }
            try
            {
                if (!bCustomerListSelected)
                {
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                    oCmd = new SqlCommand("SELECT Distinct cust.CustomerGroupID as GroupID, 1 as Version, cust.CustomerID as ID, cust.Name, cust.CountryMetroType as IsCountryCustomer, cust.Latitude, cust.Longitude, cust.Tolerance as XTolerance, cust.Tolerance as YTolerance FROM T_Customers cust Order BY cust.ID", oConn);
                    oSQLDA = new System.Data.SqlClient.SqlDataAdapter(oCmd);
                    oRawData = new DataSet();
                    oSQLDA.Fill(oRawData);
                    oSQLDA.Dispose();
                    oSQLDA = null;
                    oCmd.Dispose();
                    oCmd = null;
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                    bCustomerListSelected = true;
                }
            }
            catch (System.Exception)
            {
                bCustomerListSelected = false;
                if (oSQLDA != null)
                {
                    oSQLDA.Dispose();
                    oSQLDA = null;
                }
                if (oCmd != null)
                {
                    oCmd.Dispose();
                    oCmd = null;
                }
                if (oConn != null)
                {
                    oConn.Dispose();
                    oConn = null;
                }
            }
            #endregion
            #region Create the melways lookup
            try
            {
                oMelwaysRef = new MelwaysLookup(sDSN);
            }
            catch (System.Exception)
            {
                oMelwaysRef = null;
            }
            #endregion
            #region Create and populate the tree
            try
            {
                oTree = new BoundaryBox(iKeepXNodesAtEachLevel);
                if (oTree != null && bCustomerListSelected)
                {
                    if (oRawData != null)
                    {
                        if (oRawData.Tables.Count > 0)
                        {
                            for (int X = 0; X < oRawData.Tables[0].Rows.Count; X++)
                            {
                                // If the record has all the required values
                                if (oRawData.Tables[0].Rows[X]["GroupID"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["ID"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["Name"] != System.DBNull.Value
                                     && oRawData.Tables[0].Rows[X]["XTolerance"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["YTolerance"] != System.DBNull.Value
                                    && oRawData.Tables[0].Rows[X]["IsCountryCustomer"] != System.DBNull.Value)
                                {
                                    // If the location is valid.
                                    if (Convert.ToDouble(oRawData.Tables[0].Rows[X]["Latitude"]) != 0 || Convert.ToDouble(oRawData.Tables[0].Rows[X]["Longitude"]) != 0)
                                    {
                                        try
                                        {
                                            iGroupID = Convert.ToInt32(oRawData.Tables[0].Rows[X]["GroupID"]);
                                            if (oRawData.Tables[0].Columns.Contains("Version"))
                                                iListVersion = Convert.ToInt32(oRawData.Tables[0].Rows[X]["Version"]);
                                            else
                                                iListVersion = 1;
                                            iID = Convert.ToInt32(oRawData.Tables[0].Rows[X]["ID"]);
                                            sPlaceName = Convert.ToString(oRawData.Tables[0].Rows[X]["Name"]);
                                            if (Convert.ToInt32(oRawData.Tables[0].Rows[X]["IsCountryCustomer"]) == 1)
                                                bIsCountryCustomer = true;
                                            else
                                                bIsCountryCustomer = false;

                                            dXTolerance = Convert.ToDouble(oRawData.Tables[0].Rows[X]["XTolerance"]);
                                            dYTolerance = Convert.ToDouble(oRawData.Tables[0].Rows[X]["YTolerance"]);
                                            dLat = Convert.ToDouble(oRawData.Tables[0].Rows[X]["Latitude"]);
                                            dLong = Convert.ToDouble(oRawData.Tables[0].Rows[X]["Longitude"]);
                                            dYTolerance = dYTolerance / Convert.ToDouble(111120);
                                            dXTolerance = dXTolerance / (Math.Cos((dLat * Math.PI) / Convert.ToDouble(180)) * Convert.ToDouble(111120));
                                            dMinLat = dLat - dYTolerance;
                                            dMaxLat = dLat + dYTolerance;
                                            dMinLong = dLong - dXTolerance;
                                            dMaxLong = dLong + dXTolerance;
                                            dLatHeight = dYTolerance * 2;
                                            dLongWidth = dXTolerance * 2;
                                            if (oMelwaysRef != null)
                                                sMelwaysRef = oMelwaysRef.Query(dLat, dLong);
                                            else
                                                sMelwaysRef = "";
                                            this.Insert(iGroupID, iListVersion, iID, sPlaceName, bIsCountryCustomer, dMinLat, dMinLong, dMaxLat, dMaxLong, sMelwaysRef);
                                        }
                                        catch (System.Exception exAddToTree)
                                        {
                                            throw (exAddToTree);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            #endregion
        }
        public MelwaysLookup oMelwaysRefLookup { get { return oMelwaysRef; } }
        /// <summary>
        /// Get the count of items in the tree
        /// </summary>
        public int Count { get { return oTree.Count; } }

        /// <summary>
        /// Insert the place into the tree
        /// </summary>
        /// <param name="item"></param>
        public void Insert(int iGroupID, int iListVersion, int iID, string sPlaceName, bool bIsCountryCustomer, double dMinLat, double dMinLong, double dMaxLat, double dMaxLong, string sMelwaysRef)
        {
            BoundaryBox oTemp = null;
            try
            {
                CustomerName oItem = new CustomerName(iGroupID, iListVersion, iID, sPlaceName, bIsCountryCustomer, dMinLat, dMinLong, dMaxLat, dMaxLong, sMelwaysRef);
                oTree.Insert((TreeItem)oItem);

                if (oByGroup.ContainsKey(iGroupID))
                {
                    ((BoundaryBox)oByGroup[iGroupID]).Insert((TreeItem)oItem);
                }
                else
                {
                    oTemp = new BoundaryBox(iKeepXNodesAtEachLevel);
                    oTemp.GroupID = iGroupID;
                    oTemp.ListVersion = iListVersion;
                    oTemp.Insert((TreeItem)oItem);
                    oByGroup.Add(iGroupID, oTemp);
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        public void GetListAndVerForGroup(int iGroupID, ref int iListID, ref int iListVer)
        {
            BoundaryBox oBoundry = null;
            try
            {
                if (oByGroup.ContainsKey(iGroupID))
                {
                    oBoundry = (BoundaryBox)oByGroup[iGroupID];
                    iListID = oBoundry.GroupID;
                    iListVer = oBoundry.ListVersion;
                }
                else
                {
                    iListID = -1;
                    iListVer = -1;
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        /// <summary>
        /// Query the QuadTree, returning the items that are in the given area
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        public ArrayList Query(double dLat, double dLong)
        {
            ArrayList oResults = null;
            ArrayList oItems = null;
            CustomerName oResult = null;
            double dSearchArea = 0.9;  // A litte bigger than a 10km area...  

            try
            {
                oItems = new ArrayList();
                oResults = new ArrayList();
                oItems = oTree.Query(dLat, dLong, dSearchArea);
                if (oItems.Count > 0)
                {
                    #region If we have customers in the search area, check which ones we are in
                    for (int X = 0; X < oItems.Count; X++)
                    {
                        oResult = (CustomerName)oItems[X];
                        //						if(oResult.iID == 30)
                        //							Console.WriteLine("Blah");
                        if (oResult.oTestArea.ContainsPoint(dLat, dLong))
                        {
                            oResults.Add(oResult);
                        }
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return oResults;
        }

        /// <summary>
        /// Query the QuadTree, for a given group of customers.
        /// </summary>
        /// <param name="area"></param>
        /// <returns></returns>
        public ArrayList Query(int iGroupID, double dLat, double dLong)
        {
            ArrayList oResults = null;
            ArrayList oItems = null;
            CustomerName oResult = null;
            double dSearchArea = 0.9;  // A litte bigger than a 10km area...  

            try
            {
                oItems = new ArrayList();
                oResults = new ArrayList();

                if (oByGroup.ContainsKey(iGroupID))
                {
                    oItems = ((BoundaryBox)oByGroup[iGroupID]).Query(dLat, dLong, dSearchArea);
                    if (oItems.Count > 0)
                    {
                        #region If we have customers in the search area, check which ones we are in
                        for (int X = 0; X < oItems.Count; X++)
                        {
                            oResult = (CustomerName)oItems[X];
                            if (oResult.oTestArea.ContainsPoint(dLat, dLong))
                            {
                                oResults.Add(oResult);
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return oResults;
        }
    }
}

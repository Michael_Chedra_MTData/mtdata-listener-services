using System;
using System.Collections;
using System.Text;

namespace MTData.Common.PlaceLookup
{
    public class CustomerName : TreeItem
    {
        public bool bIsCountryCustomer = false;
        public int iGroupID = 0;
        public int iListVersion = 0;
        public string sMapRef = "";

        public CustomerName(int GroupID, int ListVersion, int ID, string PlaceName, bool IsCountryCustomer, double MinLat, double MinLong, double MaxLat, double MaxLong, string sMelwaysRef)
            : base(ID, PlaceName, MinLat, MinLong, MaxLat, MaxLong)
        {
            iGroupID = GroupID;
            iListVersion = ListVersion;
            bIsCountryCustomer = IsCountryCustomer;
            sMapRef = sMelwaysRef;
        }
    }
}

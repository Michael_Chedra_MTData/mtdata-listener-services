using System;

namespace MTData.Common.PlaceLookup
{
	/// <summary>
	/// Summary description for cWayPoint.
	/// </summary>
	public class WayPoint : TreeItem
	{
        private const string sClassName = "cWayPoint.";
		public new int iID = 0;
		public int iSetPointNumber = 0;
		public int iFlags = 0;
		public string sMapRef = "";

        public WayPoint(int ID, int SetPointNumber, int Flags, string PlaceName, double MinLat, double MinLong, double MaxLat, double MaxLong, string sMelwaysRef)
            : base(ID, PlaceName, MinLat, MinLong, MaxLat, MaxLong)
        {
            try
            {
                iID = ID;
                iSetPointNumber = SetPointNumber;
                iFlags = Flags;
                sMapRef = sMelwaysRef;
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
   
	}
}
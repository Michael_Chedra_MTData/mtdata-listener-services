using System;
using System.Collections;

namespace MTData.Common.PlaceLookup
{
    /// <summary>
    /// Summary description for cBoundaryBox.
    /// </summary>
    public class BoundaryBox
    {
        private const double dRad = Math.PI / 180;
        private Boundary oBoundary = null;
        private ArrayList oSubNodes = null;
        private ArrayList oContents = null;
        private int iItemsAtEachNode = 2;
        public int GroupID = 0;
        public int ListVersion = 0;

        /// <summary>
        /// Create a new boundary box that encompases the whole world
        /// </summary>
        public BoundaryBox()
        {
            CreateObject((double)-90, (double)-180, (double)90, (double)180, 2);
        }

        public BoundaryBox(int KeepXItemsAtEachNode)
        {
            CreateObject((double)-90, (double)-180, (double)90, (double)180, KeepXItemsAtEachNode);
        }
        /// <summary>
        /// Create a new boundary box by supplying the RectangleF objec
        /// </summary>
        public BoundaryBox(double MinLat, double MinLong, double MaxLat, double MaxLong)
        {
            CreateObject(MinLat, MinLong, MaxLat, MaxLong, 2);
        }
        public BoundaryBox(double MinLat, double MinLong, double MaxLat, double MaxLong, int KeepXItemsAtEachNode)
        {
            CreateObject(MinLat, MinLong, MaxLat, MaxLong, KeepXItemsAtEachNode);
        }

        private void CreateObject(double MinLat, double MinLong, double MaxLat, double MaxLong, int KeepXItemsAtEachNode)
        {
            iItemsAtEachNode = KeepXItemsAtEachNode;
            oBoundary = new Boundary(MinLat, MinLong, MaxLat, MaxLong);
        }

        public void Dispose()
        {
            if (oContents != null)
            {
                for (int X = oContents.Count - 1; X >= 0; X--)
                {
                    TreeItem box = (TreeItem)oContents[X];
                    box.Dispose();
                    oContents.RemoveAt(X);
                }
            }
            if (oSubNodes != null)
            {
                for (int X = oSubNodes.Count - 1; X >= 0; X--)
                {
                    BoundaryBox box = (BoundaryBox)oSubNodes[X];
                    box.Dispose();
                    oSubNodes.RemoveAt(X);
                }
            }
        }
        /// <summary>
        /// Is the node empty
        /// </summary>
        public Boundary Bounds { get { return (oBoundary); } }
        /// <summary>
        /// Is the node empty
        /// </summary>
        public bool IsEmpty { get { return (oContents == null && oSubNodes == null); } }
        /// <summary>
        /// Get the place names assosiated with this node.
        /// </summary>
        public ArrayList Contents { get { return oContents; } }
        /// <summary>
        /// Total number of nodes in the this node and all SubNodes
        /// </summary>
        public int Count
        {
            get
            {
                int iCount = 0;
                iCount += oContents.Count;
                if (oSubNodes != null)
                {
                    for (int X = 0; X < oSubNodes.Count; X++)
                    {
                        iCount += ((BoundaryBox)oSubNodes[X]).Count;
                    }
                }
                return iCount;
            }
        }
        /// <summary>
        /// Return the contents of this node and all subnodes in the true below this one.
        /// </summary>
        public ArrayList SubTreeContents
        {
            get
            {
                ArrayList oResult = new ArrayList();
                // If we have any places at this level, add them to the results list
                if (oContents != null)
                {
                    if (oContents.Count > 0)
                        oResult.AddRange(oContents);
                }
                if (oSubNodes != null)
                {
                    // If we have any sub nodes, add their contents to the result.
                    for (int X = 0; X < oSubNodes.Count; X++)
                    {
                        oResult.AddRange(((BoundaryBox)oSubNodes[X]).SubTreeContents);
                    }
                }
                return oResult;
            }
        }

        /// <summary>
        /// Query the tree for items that are in the given area
        /// </summary>
        /// <param name="queryArea"></pasram>
        /// <returns></returns>
        public ArrayList Query(double dLat, double dLong, double dSearchArea)
        {
            Boundary oTestArea = new Boundary(dLat - (dSearchArea / 2), dLong - (dSearchArea / 2), dLat + (dSearchArea / 2), dLong + (dSearchArea / 2));
            return Query(oTestArea);
        }

        public ArrayList Query(Boundary oTestArea)
        {
            ArrayList oResults = new ArrayList();
            BoundaryBox oSubNode = null;

            // If there are any points at this level, copy them to the result list
            if (oContents != null)
            {
                if (oContents.Count > 0)
                {
                    if (oTestArea.IntersectsArea(oBoundary) || oTestArea.ContainsArea(oBoundary) || oTestArea.ContainedByArea(oBoundary))
                    {
                        oResults.AddRange(oContents);
                    }
                }
            }
            if (oSubNodes != null)
            {
                for (int X = 0; X < oSubNodes.Count; X++)
                {
                    oSubNode = (BoundaryBox)oSubNodes[X];

                    if (!oSubNode.IsEmpty)
                    {
                        if (oSubNode.Bounds.ContainsArea(oTestArea))
                        {
                            // Case 1: search area completely contained by sub-quad
                            // if a node completely contains the query area, go down that branch
                            // and skip the remaining nodes (break this loop)
                            oResults.AddRange(oSubNode.Query(oTestArea));
                        }
                        else if (oTestArea.ContainsArea(oSubNode.Bounds))
                        {
                            // Case 2: Sub-quad completely contained by search area 
                            // if the query area completely contains a sub-quad,
                            // just add all the contents of that quad and it's children 
                            // to the result set. You need to continue the loop to test 
                            // the other quads
                            oResults.AddRange(oSubNode.SubTreeContents);
                        }
                        else if (oSubNode.Bounds.IntersectsArea(oTestArea))
                        {
                            // Case 3: search area intersects with sub-quad
                            // traverse into this quad, continue the loop to search other
                            // quads
                            oResults.AddRange(oSubNode.Query(oTestArea));
                        }
                    }
                }
            }
            return oResults;
        }

        /// <summary>
        /// Insert an item to this node
        /// </summary>
        /// <param name="item"></param>
        public void Insert(TreeItem oItem)
        {
            TreeItem oTempItem = null;
            BoundaryBox oSubNode = null;

            try
            {
                if (!oBoundary.ContainsArea(oItem.oTestArea))
                {
                    #region If the item is not contained in this quad, there's a problem
                    return;
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            try
            {
                if (oSubNodes == null)
                {
                    #region If there are no sub nodes for this item
                    if (oContents == null)
                    {
                        #region If the oContents list is null, create it and add this item to it.
                        oContents = new ArrayList();
                        oContents.Add(oItem);
                        return;
                        #endregion
                    }
                    else
                    {
                        #region If there is an existing oContents list
                        if (oContents.Count < iItemsAtEachNode)
                        {
                            #region If the max number of items to cache at each level has not been reached, add the item to the oContents list
                            oContents.Add(oItem);
                            return;
                            #endregion
                        }
                        else
                        {
                            #region Create the sub nodes is possible and move the oContents items into the sub nodes
                            if (oSubNodes == null)
                            {
                                CreateSubNodes();
                                #region Move the oContents items into the sub nodes
                                if (oSubNodes != null)
                                {
                                    for (int X = oContents.Count - 1; X >= 0; X--)
                                    {
                                        #region For each item in the oContents array at this level
                                        oTempItem = (TreeItem)oContents[X];
                                        for (int Y = 0; Y < oSubNodes.Count; Y++)
                                        {
                                            oSubNode = (BoundaryBox)oSubNodes[Y];
                                            #region Find the sub node to move the item to.
                                            if (oSubNode.Bounds.ContainsArea(oTempItem.oTestArea))
                                            {
                                                oSubNode.Insert(oTempItem);
                                                oContents.RemoveAt(X);
                                                break;
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                #endregion
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            try
            {
                if (oSubNodes != null)
                {
                    #region For each sub node, check which one this item fits in.
                    for (int X = 0; X < oSubNodes.Count; X++)
                    {
                        oSubNode = (BoundaryBox)oSubNodes[X];
                        if (oSubNode.Bounds.ContainsArea(oItem.oTestArea))
                        {
                            oSubNode.Insert(oItem);
                            return;
                        }
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            try
            {			// if we make it to here, either
                // 1) none of the subnodes completely contained the item. or
                // 2) we're at the smallest subnode size allowed 
                // add the item to this node's contents.
                if (oContents == null)
                    oContents = new ArrayList();
                oContents.Add(oItem);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
        /// <summary>
        /// Internal method to create the subnodes (partitions space)
        /// </summary>
        private void CreateSubNodes()
        {
            double dWidth = ((oBoundary.dMaxLong - oBoundary.dMinLong) / (double)2);
            double dHeight = ((oBoundary.dMaxLat - oBoundary.dMinLat) / (double)2);
            if (dWidth > 0.03 && dHeight > 0.03)
            {
                oSubNodes = new ArrayList();
                #region If the width and height of the new boundaries are greater than 0.1 of a degree, create the new sub nodes
                // The 4 new boxes are a splt of the large into quaters
                // MaxLat / MinLong								MaxLat / MaxLong
                //	 ------------------------------------------------------
                //	|				1					|				2					|
                //	------------------------------------------------------
                //	|				3					|				4					|
                //	------------------------------------------------------
                //  MinLat / Min Long									MinLat / MaxLong
                //

                oSubNodes.Add(new BoundaryBox(oBoundary.dMinLat + dHeight, oBoundary.dMinLong, oBoundary.dMaxLat, oBoundary.dMinLong + dWidth));
                oSubNodes.Add(new BoundaryBox(oBoundary.dMinLat + dHeight, oBoundary.dMinLong + dWidth, oBoundary.dMaxLat, oBoundary.dMaxLong));
                oSubNodes.Add(new BoundaryBox(oBoundary.dMinLat, oBoundary.dMinLong, oBoundary.dMinLat + dHeight, oBoundary.dMinLong + dWidth));
                oSubNodes.Add(new BoundaryBox(oBoundary.dMinLat, oBoundary.dMinLong + dWidth, oBoundary.dMinLat + dHeight, oBoundary.dMaxLong));
                #endregion
            }
        }
    }
}


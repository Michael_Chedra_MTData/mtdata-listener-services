using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;
using MTData.Transport.GenericProtocol.Logistics.IncidentReport;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class InspectionLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.InspectionLiveUpdate;
        private int _inspectionId;
        private EventType _eventType;
        private int _eventId;
        private int _fleetId;
        private int _vehicleId;
        private string _vehicleName;
        private int _driverId;
        private string _driverName;
        private int? _inspectedAssetUniqueKey;
        private int? _inspectedFleetId;
        private int? _inspectedVehicleId;
        private int _noOfDefects;
        private int _noOfNewDefects;
        private bool _stopWork;
        private DateTime _eventTime;

        #endregion

        #region properties
        public int InspectionId
        {
            get { return _inspectionId; }
            set { _inspectionId = value; }
        }

        public EventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        
        /// <remarks>
        /// A value of 0 is used to represent null.
        /// </remarks>
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        
        public string VehicleName
        {
            get { return _vehicleName; }
            set { _vehicleName = value; }
        }

        /// <remarks>
        /// A value of 0 is used to represent null
        /// </remarks>
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
    
        public string DriverName
        {
            get { return _driverName; }     
            set { _driverName = value; }
        }

        /// <summary>
        /// Nullable InspectedAssetUniqueKey in Inspection
        /// </summary>
        public int? InspectedAssetUniqueKey
        {
            get { return _inspectedAssetUniqueKey; }
            set { _inspectedAssetUniqueKey = value; }
        }

        public int? InspectedFleetId
        {
            get { return _inspectedFleetId; }
            set { _inspectedFleetId = value; }
        }

        public int? InspectedVehicleId
        {
            get { return _inspectedVehicleId; }
            set { _inspectedVehicleId = value; }
        }

        public int NoOfDefects
        {
            get { return _noOfDefects; }
            set { _noOfDefects = value; }
        }

        public int NoOfNewDefects
        {
            get { return _noOfNewDefects; }
            set { _noOfNewDefects = value; }
        }

        public bool StopWork
        { 
            get { return _stopWork; }
            set { _stopWork = value; }
        }

        public DateTime EventTime
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }

        #endregion

        #region constructor
        public InspectionLiveUpdateSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }
        public InspectionLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                int fieldsFlags = (_vehicleName != null ? 1 << 0 : 0)
                    | (_driverName != null ? 1 << 1 : 0);

                WriteMoreFlag(stream, fieldsFlags);
                Write4ByteInteger(stream, _inspectionId);
                Write2ByteInteger(stream, (int)_eventType);
                Write4ByteInteger(stream, _eventId);
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                if(_vehicleName != null)
                    WriteString(stream, _vehicleName);
                Write4ByteInteger(stream, _driverId);
                if(_driverName != null)
                    WriteString(stream, _driverName);
                Write4ByteInteger(stream, _inspectedAssetUniqueKey.HasValue ? _inspectedAssetUniqueKey.Value : -1);
                Write4ByteInteger(stream, _inspectedFleetId.HasValue ? _inspectedFleetId.Value : -1);
                Write4ByteInteger(stream, _inspectedVehicleId.HasValue ? _inspectedVehicleId.Value : -1);
                Write4ByteInteger(stream, _noOfDefects);
                Write4ByteInteger(stream, _noOfNewDefects);
                WriteBool(stream, _stopWork);
                WriteDateTime(stream, _eventTime);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            int fieldsFlags = ReadMoreFlag(Data, ref index);
            _inspectionId = Read4ByteInteger(Data, ref index);
            _eventType = (EventType)Read2ByteInteger(Data, ref index);
            _eventId = Read4ByteInteger(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            if ((fieldsFlags & (1 << 0)) != 0)
                _vehicleName = ReadString(Data, ref index);
            else
                _vehicleName = null;
            _driverId = Read4ByteInteger(Data, ref index);
            if ((fieldsFlags & (1 << 1)) != 0)
                _driverName = ReadString(Data, ref index);
            else
                _driverName = null;
            _inspectedAssetUniqueKey = Read4ByteInteger(Data, ref index);
            if (_inspectedAssetUniqueKey == -1)
                _inspectedAssetUniqueKey = null;
            _inspectedFleetId = Read4ByteInteger(Data, ref index);
            if (_inspectedFleetId == -1)
                _inspectedFleetId = null;
            _inspectedVehicleId = Read4ByteInteger(Data, ref index);
            if (_inspectedVehicleId == -1)
                _inspectedVehicleId = null;
            _noOfDefects = Read4ByteInteger(Data, ref index);
            _noOfNewDefects = Read4ByteInteger(Data, ref index);
            _stopWork = ReadBool(Data, ref index);
            _eventTime = ReadDateTime(Data, ref index);
        }
        #endregion    
    }
}

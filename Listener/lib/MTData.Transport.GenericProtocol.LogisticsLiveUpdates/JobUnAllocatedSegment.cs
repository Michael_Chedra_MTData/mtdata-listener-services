using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class JobUnAllocatedSegment : SegmentLayer
    {
        #region private fields
        private int _fleetId;
        private int _dispatchId;
        #endregion

        #region properties
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        #endregion

        #region constructor
        public JobUnAllocatedSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobUnAllocated;
        }
        public JobUnAllocatedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.JobUnAllocated)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.JobUnAllocated, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobUnAllocated;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _dispatchId);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetId = Read4ByteInteger(Data, ref index);
            _dispatchId = Read4ByteInteger(Data, ref index);
        }
        #endregion    
    }
}

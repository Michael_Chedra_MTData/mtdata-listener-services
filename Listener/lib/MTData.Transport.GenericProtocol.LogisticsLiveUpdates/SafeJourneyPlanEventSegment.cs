﻿using MTData.Transport.GenericProtocol.Logistics.SafeJourney;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{

    /// <summary>
    /// Segment used to notify clients when various events occur.
    /// Segment is not sent to the shell so minimising the amount of data sent is less important.
    /// </summary>
    public class SafeJourneyPlanEventSegment : SegmentLayer
    {
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.SafeJourneyPlanEvent;
        private int _sjpId;
        private int _eventId;
        private SafeJourneyEventType _eventType;
        private DateTime _eventTime;
        private int _fleetId;
        private int _vehicleId;
        private int _driverId;

        public SafeJourneyPlanEventSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }

        public SafeJourneyPlanEventSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        public int SjpId
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        public SafeJourneyEventType EventType 
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public DateTime EventTime 
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }

        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }

        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }

        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _sjpId);
                Write4ByteInteger(stream, _eventId);
                stream.WriteByte((byte)_eventType);
                WriteDateTime(stream, _eventTime);
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _driverId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }

        private void DecodeData()
        {
            int index = 0;
            _sjpId = Read4ByteInteger(Data, ref index);
            _eventId = Read4ByteInteger(Data, ref index);
            _eventType = (SafeJourneyEventType)Data[index++];
            _eventTime = ReadDateTime(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
        }
    }
}

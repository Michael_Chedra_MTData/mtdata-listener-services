using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public enum JobActionAlertStatuses
    {
        Cancelled=-1,
        FireNow=0,
        Scheduled,
    }

    public class AlertStatusChanged : IPacketEncode
    {
        #region private fields
        /// <summary>
        /// the version number of the decode/encode of this item
        /// </summary>
        private int _versionNumber = 1;
        private int _jobActionId;
        private int _dispatchId;
        private int _fleetId;
        private DateTime _triggerTime;
        private JobActionAlertStatuses _alertState;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public DateTime TriggerTime
        {
            get { return _triggerTime; }
            set { _triggerTime = value; }
        }
        public int JobActionId
        {
            get { return _jobActionId; }
            set { _jobActionId = value; }
        }
        public JobActionAlertStatuses AlertState
        {
            get { return _alertState; }
            set { _alertState = value; }
        }
        #endregion

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            _jobActionId = BaseLayer.Read4ByteInteger(data, ref index);
            _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
            _fleetId = BaseLayer.Read4ByteInteger(data, ref index);
            _alertState = (JobActionAlertStatuses)BaseLayer.Read4ByteInteger(data, ref index);
            _triggerTime = BaseLayer.ReadDateTime(data, ref index);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(MemoryStream stream)
        {
            //write the data to a local stream
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                BaseLayer.Write4ByteInteger(local, _jobActionId);
                BaseLayer.Write4ByteInteger(local, _dispatchId);
                BaseLayer.Write4ByteInteger(local, _fleetId);
                BaseLayer.Write4ByteInteger(local, (int)_alertState);
                BaseLayer.WriteDateTime(local, _triggerTime);

                //write version number
                BaseLayer.WriteMoreFlag(stream, _versionNumber);
                //write the length
                BaseLayer.WriteMoreFlag(stream, (int)local.Length);
                //now write the data
                BaseLayer.CopyStream(local, stream);
            }
        }
    }

    public class JobActionAlertStatusChangedSegment : SegmentLayer
    {
        #region private fields
        private List<AlertStatusChanged> _alerts;
        #endregion

        #region properties
        public List<AlertStatusChanged> Alerts
        {
            get { return _alerts; }
            set { _alerts = value; }
        }
        #endregion

        #region constructor
        public JobActionAlertStatusChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobActionAlertStatusChanged;
            _alerts = new List<AlertStatusChanged>();
        }
        public JobActionAlertStatusChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.JobActionAlertStatusChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.JobActionAlertStatusChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobActionAlertStatusChanged;
            _alerts = new List<AlertStatusChanged>();
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write2ByteInteger(stream, _alerts.Count);
                foreach (AlertStatusChanged a in _alerts)
                {
                    a.Encode(stream);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            int count = Read2ByteInteger(Data, ref index);
            _alerts.Clear();
            for (int i = 0; i < count; i++ )
            {
                AlertStatusChanged a = new AlertStatusChanged();
                a.Decode(Data, ref index);
                _alerts.Add(a);
            }
        }
        #endregion    

    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;
using MTData.Transport.GenericProtocol.Logistics.IncidentReport;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class IncidentLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.IncidentLiveUpdate;
        private int _incidentEventId;
        private EventType _eventType;
        private int _eventId;
        private int _fleetId;
        private int _vehicleId;
        private int _userId;
        private int _driverId;
        private int _incidentTemplateListItemId;
        private string _incidentTypeName;
        private DateTime _eventTime;
        private AssociationType _associationType;
        private string _description;
        #endregion

        #region properties
        public int IncidentEventId 
        {
            get { return _incidentEventId; }
            set { _incidentEventId = value; }
        }

        public EventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        
        /// <remarks>
        /// A value of 0 is used to represent null.
        /// </remarks>
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        /// <remarks>
        /// A value of 0 is used to represent null
        /// </remarks>
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }

        public int IncidentTemplateListItemId 
        {
            get { return _incidentTemplateListItemId; }
            set { _incidentTemplateListItemId = value; }
        }

        public string IncidentTypeName
        {
            get { return _incidentTypeName; }
            set { _incidentTypeName = value; }
        }
        public DateTime EventTime
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }

        public AssociationType AssociationType
        {
            get { return _associationType; }
            set { _associationType = value; }
        }

        public string Description
        {
            get { return _description; }
            set { _description = value; }
        }
        #endregion

        #region constructor
        public IncidentLiveUpdateSegment()
        {
            Version = 4;
            Type = (int)SegmentType;
        }
        public IncidentLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {

                int fieldsFlags = (_incidentTypeName != null ? 1 : 0)
                    | (_description != null ? 1 << 1 : 0);
                WriteMoreFlag(stream, fieldsFlags);
                Write4ByteInteger(stream, _incidentEventId);
                Write2ByteInteger(stream, (int)_eventType);
                Write4ByteInteger(stream, _eventId);
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _userId);
                Write4ByteInteger(stream, _driverId);
                Write4ByteInteger(stream, _incidentTemplateListItemId);
                if (_incidentTypeName != null)
                {
                    WriteString(stream, _incidentTypeName);
                }
                WriteDateTime(stream, _eventTime);
                // Version 3
                WriteMoreFlag(stream, (int)_associationType);
                // Version 4
                if (_description != null)
                {
                    WriteString(stream, _description);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            int fieldsFlags = ReadMoreFlag(Data, ref index);
            _incidentEventId = Read4ByteInteger(Data, ref index);
            _eventType = (EventType)Read2ByteInteger(Data, ref index);
            _eventId = Read4ByteInteger(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _userId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _incidentTemplateListItemId = Read4ByteInteger(Data, ref index);
            if ((fieldsFlags & 1) != 0)
            {
                _incidentTypeName = ReadString(Data, ref index);
            }
            else
            {
                _incidentTypeName = null;
            }
            _eventTime = DateTime.MinValue;
            if (Version >= 2)
            {
                _eventTime = ReadDateTime(Data, ref index);
            }
            if (Version >= 3)
            {
                _associationType = (AssociationType)ReadMoreFlag(Data, ref index);
            }
            else
            {
                _associationType = AssociationType.Vehicle;
            }
            if (Version >= 4 && ((fieldsFlags & (1 << 1)) != 0))
            {
                _description = ReadString(Data, ref index);
            }
            else
            {
                _description = null;
            }
        }
        #endregion    
    }
}

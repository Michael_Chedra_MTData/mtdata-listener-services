using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class LoginLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        private int _userId;
        private string _userName;
        private string _password;
        #endregion

        #region properties
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }
        public string Password
        {
            get { return _password; }
            set { _password = value; }
        }
        #endregion

        #region constructor
        public LoginLiveUpdateSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Login;
        }
        public LoginLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.Login)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.Login, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Login;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _userId);
                WriteString(stream, _userName, 0x12);
                WriteString(stream, _password, 0x12);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _userId = Read4ByteInteger(Data, ref index);
            _userName = ReadString(Data, ref index, 0x12);
            _password = ReadString(Data, ref index, 0x12);
        }
        #endregion
    }
}

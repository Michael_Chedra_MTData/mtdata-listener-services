﻿using MTData.Transport.GenericProtocol.Logistics.TrackableAsset;
using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class AssociationLiveUpdateItem : IPacketEncode
    {
        #region private fields
        private int _versionNumber = 3;
        private int _associationId;
        private AssociationTypes _associationType;
        private int? _assetUniqueKey;
        private int? _assetListId;
        private string _assetName;
        private string _assetListName;
        private int? _slaveFleetId;
        private int? _slaveVehicleId;
        private int? _masterFleetId;
        private int? _masterVehicleId;
        private int? _userId;
        private string _userName;
        private int? _driverId;
        private DateTime _associationTime;
        private DateTime _insertedTime;
        private double _latitude;
        private double _longitude;
        private int? _declaredOnFleetId;
        private int? _declaredOnVehicleId;
        private int? _dispatchId;
        private int? _legActionId;
        #endregion

        #region properties
        public int AssociationId 
        {
            get { return _associationId; }
            set { _associationId = value; }
        }

        public AssociationTypes AssociationType
        {
            get { return _associationType; }
            set { _associationType = value; }
        }

        public int? AssetUniqueKey 
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }

        public int? AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        }
        public string AssetName
        {
            get { return _assetName; }
            set { _assetName = value; }
        }

        public string AssetListName
        {
            get { return _assetListName; }
            set { _assetListName = value; }
        }

        public int? SlaveFleetId
        {
            get { return _slaveFleetId; }
            set { _slaveFleetId = value; }
        }

        public int? SlaveVehicleId
        {
            get { return _slaveVehicleId; }
            set { _slaveVehicleId = value; }
        }

        public int? MasterFleetId
        {
            get { return _masterFleetId; }
            set { _masterFleetId = value; }
        }

        public int? MasterVehicleId
        {
            get { return _masterVehicleId; }
            set { _masterVehicleId = value; }
        }

        public int? UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        public int? DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }

        public DateTime AssociationTime
        {
            get { return _associationTime; }
            set { _associationTime = value; }
        }

        public DateTime InsertedTime
        {
            get { return _insertedTime; }
            set { _insertedTime = value; }
        }

        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }

        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }

        public bool IsLocationAssociation
        {
            get { return !MasterVehicleId.HasValue ; }
        }

        public int? DeclaredOnFleetId
        {
            get { return _declaredOnFleetId; }
            set { _declaredOnFleetId = value; }
        }

        public int? DeclaredOnVehicleId
        {
            get { return _declaredOnVehicleId; }
            set { _declaredOnVehicleId = value; }
        }

        public int? DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }

        public int? LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }
        #endregion

        #region constructor
        public AssociationLiveUpdateItem()
        {
        }
        #endregion

        #region IPacketEncode Members

        public void Decode(byte[] data, ref int index)
        {
            int versionNumber = BaseLayer.ReadMoreFlag(data, ref index);
            int length = BaseLayer.ReadMoreFlag(data, ref index);

            int startIndex = index;
            DecodeData(data, ref index, versionNumber);

            //skip any unread bytes
            int readBytes = index - startIndex;
            if (length > readBytes)
            {
                index += length - readBytes;
            }
        }

        public void Encode(System.IO.MemoryStream stream)
        {
            //write the data to a local stream
            byte[] data = GetBytes();
            //write version number
            BaseLayer.WriteMoreFlag(stream, _versionNumber);
            //write the length
            BaseLayer.WriteMoreFlag(stream, data.Length);
            //now write the data
            stream.Write(data, 0, data.Length);
        }
        #endregion

        private byte[] GetBytes()
        {
            using (System.IO.MemoryStream local = new System.IO.MemoryStream())
            {
                int fieldsFlags = (_assetUniqueKey.HasValue ? 1 : 0)
                    | (_assetListId.HasValue ? 1 << 1 : 0)
                    | (_assetName != null ? 1 << 2 : 0)
                    | (_assetListName != null ? 1 << 3 : 0)
                    | (_slaveFleetId.HasValue ? 1 << 4 : 0)
                    | (_slaveVehicleId.HasValue ? 1 << 5 : 0)
                    | (_masterFleetId.HasValue ? 1 << 6 : 0)
                    | (_masterVehicleId.HasValue ? 1 << 7 : 0)
                    | (_userId.HasValue ? 1 << 8 : 0)
                    | (_userName != null ? 1 << 9 : 0)
                    | (_driverId.HasValue ? 1 << 10 : 0)
                    | (_declaredOnFleetId.HasValue ? 1 << 11 : 0)
                    | (_declaredOnVehicleId.HasValue ? 1 << 12 : 0)
                    | (_dispatchId.HasValue ? 1 << 13 : 0)
                    | (_legActionId.HasValue ? 1 << 14 : 0);
                BaseLayer.WriteMoreFlag(local, fieldsFlags);
                BaseLayer.Write4ByteInteger(local, _associationId);
                local.WriteByte((byte)_associationType);
                if (_assetUniqueKey.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _assetUniqueKey.Value);
                }
                if (_assetListId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _assetListId.Value);
                }
                if (_assetName != null)
                {
                    BaseLayer.WriteString(local, _assetName);
                }
                if (_assetListName != null)
                {
                    BaseLayer.WriteString(local, _assetListName);
                }
                if (_slaveFleetId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _slaveFleetId.Value);
                }
                if (_slaveVehicleId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _slaveVehicleId.Value);
                }
                if (_masterFleetId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _masterFleetId.Value);
                }
                if (_masterVehicleId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _masterVehicleId.Value);
                }
                if (_userId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _userId.Value);
                }
                if (_userName != null)
                {
                    BaseLayer.WriteString(local, _userName);
                }
                if (_driverId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _driverId.Value);
                }
                BaseLayer.WriteDateTime(local, _associationTime);
                BaseLayer.WriteDateTime(local, _insertedTime);
                BaseLayer.WriteCoordinate(local, _latitude);
                BaseLayer.WriteCoordinate(local, _longitude);
                if (_declaredOnFleetId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _declaredOnFleetId.Value);
                }
                if (_declaredOnVehicleId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _declaredOnVehicleId.Value);
                }
                if (_dispatchId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _dispatchId.Value);
                }
                if (_legActionId.HasValue)
                {
                    BaseLayer.Write4ByteInteger(local, _legActionId.Value);
                }
                return local.ToArray();
            }
        }

        private void DecodeData(byte[] data, ref int index, int versionNumber)
        {

            int fieldsFlags = BaseLayer.ReadMoreFlag(data, ref index);
            _associationId = BaseLayer.Read4ByteInteger(data, ref index);
            _associationType = (AssociationTypes)data[index++];
            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _assetUniqueKey = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _assetUniqueKey = null;
            }
            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _assetListId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _assetListId = null;
            }
            if ((fieldsFlags & (1 << 2)) != 0)
            {
                _assetName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _assetName = null;
            }
            if ((fieldsFlags & (1 << 3)) != 0)
            {
                _assetListName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _assetListName = null;
            }
            if ((fieldsFlags & (1 << 4)) != 0)
            {
                _slaveFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _slaveFleetId = null;
            }
            if ((fieldsFlags & (1 << 5)) != 0)
            {
                _slaveVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _slaveVehicleId = null;
            }
            if ((fieldsFlags & (1 << 6)) != 0)
            {
                _masterFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _masterFleetId = null;
            }
            if ((fieldsFlags & (1 << 7)) != 0)
            {
                _masterVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _masterVehicleId = null;
            }
            if ((fieldsFlags & (1 << 8)) != 0)
            {
                _userId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _userId = null;
            }
            if ((fieldsFlags & (1 << 9)) != 0)
            {
                _userName = BaseLayer.ReadString(data, ref index);
            }
            else
            {
                _userName = null;
            }
            if ((fieldsFlags & (1 << 10)) != 0)
            {
                _driverId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            else
            {
                _driverId = null;
            }
            _associationTime = BaseLayer.ReadDateTime(data, ref index);
            _insertedTime = BaseLayer.ReadDateTime(data, ref index);
            _latitude = BaseLayer.ReadCoordinate(data, ref index);
            _longitude = BaseLayer.ReadCoordinate(data, ref index);
            if ((fieldsFlags & (1 << 11)) != 0)
            {
                _declaredOnFleetId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            if ((fieldsFlags & (1 << 12)) != 0)
            {
                _declaredOnVehicleId = BaseLayer.Read4ByteInteger(data, ref index);
            }
            if (versionNumber >= 3)
            {
                if ((fieldsFlags & (1 << 13)) != 0)
                {
                    _dispatchId = BaseLayer.Read4ByteInteger(data, ref index);
                }
                if ((fieldsFlags & (1 << 14)) != 0)
                {
                    _legActionId = BaseLayer.Read4ByteInteger(data, ref index);
                }
            }
        }
    }
}
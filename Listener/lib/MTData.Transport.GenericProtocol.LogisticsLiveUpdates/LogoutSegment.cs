using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class LogoutLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        private int _userId;
        #endregion

        #region properties
        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        #endregion

        #region constructor
        public LogoutLiveUpdateSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Logout;
        }
        public LogoutLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.Logout)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.Logout, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Logout;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _userId);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _userId = Read4ByteInteger(Data, ref index);
        }
        #endregion    
    }
}

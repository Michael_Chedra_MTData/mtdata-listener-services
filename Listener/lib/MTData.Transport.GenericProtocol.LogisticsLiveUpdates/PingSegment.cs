using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class PingSegment : SegmentLayer
    {
        #region constructor
        public PingSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Ping;
        }
        public PingSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.Ping)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.Ping, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Ping;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class PongSegment : SegmentLayer
    {
        #region constructor
        public PongSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Pong;
        }
        public PongSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.Pong)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.Pong, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Pong;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }

        #endregion

    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.PreDefinedFunction;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class PredefinedRequestSegment : SegmentLayer
    {
        #region private fields
        private int _fleetId;
        private int _vehicleId;
        private PredefinedFunction _function;
        private bool _start;
        private byte[] _parameters;
        #endregion

        #region properties
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        public PredefinedFunction Function
        {
            get { return _function; }
            set { _function = value; }
        }
        public bool Start
        {
            get { return _start; }
            set { _start = value; }
        }
        public byte[] Parameters
        {
            get { return _parameters; }
            set { _parameters = value; }
        }
        #endregion

        #region constructor
        public PredefinedRequestSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.PredefinedRequest;
        }
        public PredefinedRequestSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.PredefinedRequest)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.PredefinedRequest, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.PredefinedRequest;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                stream.WriteByte((byte)_function);
                WriteBool(stream, _start);
                if (_parameters == null || _parameters.Length == 0)
                {
                    Write4ByteUInteger(stream, 0);
                }
                else
                {
                    Write4ByteInteger(stream, _parameters.Length);
                    stream.Write(_parameters, 0, _parameters.Length);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _function = (PredefinedFunction)Data[index++];
            _start = ReadBool(Data, ref index);
            int length = Read4ByteInteger(Data, ref index);
            _parameters = new byte[length];
            if (length > 0)
            {
                Array.Copy(Data, index, _parameters, 0, length);
                index += length;
            }
        }
        #endregion    

    }
}

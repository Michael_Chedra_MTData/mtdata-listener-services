﻿namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    using System;
    using System.IO;

    /// <summary>
    /// Defines the <see cref="IapMassDeclarationLiveUpdateSegment" />
    /// </summary>
    public class IapMassDeclarationLiveUpdateSegment : SegmentLayer
    {
        /// <summary>
        /// Defines the SegmentType
        /// </summary>
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.IapMassDeclartion;

        /// <summary>
        /// Initialises a new instance of the <see cref="IapMassDeclarationLiveUpdateSegment"/> class.
        /// </summary>
        /// <param name="eventId">The event id.</param>
        /// <param name="fleetId">The fleet id.</param>
        /// <param name="vehicleId">The vehicle id.</param>
        /// <param name="driverId">The driver id.</param>
        /// <param name="eventTime">The event time.</param>
        /// <param name="serverTime">The server time.</param>
        /// <param name="latitude">The latitude.</param>
        /// <param name="longitude">The longitude.</param>
        /// <param name="vehicleCategory">The vehicle category.</param>
        /// <param name="noOfAxles">The number of axles.</param>
        /// <param name="totalMass">The total mass.</param>
        public IapMassDeclarationLiveUpdateSegment(int eventId, int fleetId, int vehicleId, int driverId, DateTime eventTime, DateTime serverTime, double latitude, double longitude, ushort vehicleCategory, ushort noOfAxles, float totalMass)
        {
            Version = 1;
            Type = (int)SegmentType;
            EventId = eventId;
            FleetId = fleetId;
            VehicleId = vehicleId;
            DriverId = driverId;
            EventTime = eventTime;
            ServerTime = serverTime;
            Latitude = latitude;
            Longitude = longitude;
            VehicleCategory = vehicleCategory;
            NoOfAxles = noOfAxles;
            TotalMass = totalMass;
        }

        /// <summary>
        /// Initialises a new instance of the <see cref="IapMassDeclarationLiveUpdateSegment"/> class.
        /// </summary>
        /// <param name="segment">The <see cref="SegmentLayer"/></param>
        public IapMassDeclarationLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception($"Can only create a segment of type {SegmentType}, not {segment.Type}");
            }

            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }

        /// <summary>
        /// Gets the event Id.
        /// </summary>
        public int EventId { get; private set; }

        /// <summary>
        /// Gets the Fleet Id.
        /// </summary>
        public int FleetId { get; private set; }

        /// <summary>
        /// Gets the vehicle Id.
        /// </summary>
        public int VehicleId { get; private set; }

        /// <summary>
        /// Gets the driver Id.
        /// </summary>
        public int DriverId { get; private set; }

        /// <summary>
        /// Gets the event time.
        /// </summary>
        public DateTime EventTime { get; private set; }

        /// <summary>
        /// Gets the time the event was inserted into the database (UTC)
        /// </summary>
        public DateTime ServerTime { get; private set; }

        /// <summary>
        /// Gets the latitude of the event.
        /// </summary>
        public double Latitude { get; private set; }

        /// <summary>
        /// Gets the longitude of the event.
        /// </summary>
        public double Longitude { get; private set; }

        /// <summary>
        /// Gets the vehicle category.
        /// </summary>
        public ushort VehicleCategory { get; private set; }

        /// <summary>
        /// Gets the number of axles.
        /// </summary>
        public ushort NoOfAxles { get; private set; }

        /// <summary>
        /// Gets the mass declaration value.
        /// </summary>
        public float TotalMass { get; private set; }

        /// <summary>
        /// The GetBytes
        /// </summary>
        /// <returns>The byte array of the segment.</returns>
        public override byte[] GetBytes()
        {
            using (var stream = new MemoryStream())
            {
                Write4ByteInteger(stream, EventId);
                Write4ByteInteger(stream, FleetId);
                Write4ByteInteger(stream, VehicleId);
                Write4ByteInteger(stream, DriverId);
                WriteDateTime(stream, EventTime);
                WriteDateTime(stream, ServerTime);
                WriteCoordinate(stream, Latitude);
                WriteCoordinate(stream, Longitude);
                Write2ByteInteger(stream, VehicleCategory);
                Write2ByteInteger(stream, NoOfAxles);
                WriteFloat(stream, TotalMass);
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        /// <summary>
        /// The Decode
        /// </summary>
        /// <param name="payload">The byte array of the segment to decode.</param>
        /// <returns>The number of bytes decoded.</returns>
        public override int Decode(byte[] payload)
        {
            var read = base.Decode(payload);
            DecodeData();
            return read;
        }

        /// <summary>
        /// The DecodeData
        /// </summary>
        private void DecodeData()
        {
            var index = 0;
            EventId = Read4ByteInteger(Data, ref index);
            FleetId = Read4ByteInteger(Data, ref index);
            VehicleId = Read4ByteInteger(Data, ref index);
            DriverId = Read4ByteInteger(Data, ref index);
            EventTime = ReadDateTime(Data, ref index);
            ServerTime = ReadDateTime(Data, ref index);
            Latitude = ReadCoordinate(Data, ref index);
            Longitude = ReadCoordinate(Data, ref index);
            VehicleCategory = (ushort)Read2ByteInteger(Data, ref index);
            NoOfAxles = (ushort)Read2ByteInteger(Data, ref index);
            TotalMass = ReadFloat(Data, ref index);
        }
    }
}

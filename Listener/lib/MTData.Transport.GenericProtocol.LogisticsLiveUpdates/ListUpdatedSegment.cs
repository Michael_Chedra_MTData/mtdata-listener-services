using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Lists;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class ListUpdatedSegment : SegmentLayer
    {
        #region private fields
        private int _listId;
        private int _listVersion;
        private ListTypes _listType;
        #endregion

        #region properties
        public int ListId
        {
            get { return _listId; }
            set { _listId = value; }
        }
        public int ListVersion
        {
            get { return _listVersion; }
            set { _listVersion = value; }
        }
        public ListTypes ListType
        {
            get { return _listType; }
            set { _listType = value; }
        }
        #endregion

        #region constructor
        public ListUpdatedSegment()
        {
            Version = 3;
            Type = (int)LogisticsLiveUpdateSegmentTypes.ListUpdated;
        }
        public ListUpdatedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.ListUpdated)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.ListUpdated, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.ListUpdated;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _listId);
                if (Version >= 2)
                {
                    Write4ByteInteger(stream, _listVersion);
                    if (Version >= 3)
                    {
                        stream.WriteByte((byte)_listType);
                    }
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _listId = Read4ByteInteger(Data, ref index);
            _listType = ListTypes.Unknown;
            if (Version >= 2)
            {
                _listVersion = Read4ByteInteger(Data, ref index);
                if (Version >= 3)
                {
                    _listType = (ListTypes)Data[index++];
                }
            }
            else
            {
                _listVersion = 0;
            }
        }
        #endregion
    }
}

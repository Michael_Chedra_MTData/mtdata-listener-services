using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public enum NonJobUpdateType
    {
        Login = 0,
        Logoff= 1,
        BreakOn = 2,
        VehicleUsageOn = 3,
        PhoneCallIncoming = 4,
        Fuel = 5,
        PreTrip = 6,
        PostTrip = 7,
        BreakOff = 8,
        VehicleUsageOff = 9,
        MessageSent = 10,
        MessageReceived = 11,
        MessageReadByDriver = 12,
        PhoneCallOutgoing = 13,
        PhoneCallOutgoing3rdParty = 14,
        PhoneCallSmsSent = 15,
        PhoneCallSmsReceived = 16,
        PredefinedFunction = 17,
        WorkOn = 18,
        WorkOff = 19,
        DefectRaised = 20,
        DefectNote = 21,
        DefectAssessment = 22,
        DefectWorkOrder = 23,
        DefectClosed = 24,
        IncidentRaised = 25,
        IncidentNote = 26,
        IncidentClosed = 27,
        Association = 28,
        Inspection = 29,
        MissedStartBreak = 30,
        BreakFinishedEarly = 31,
        IapComment = 32,
        IapMassDeclaration = 33
    }

    public class NonJobEventSegment : SegmentLayer
    {
        #region private fields
        private int _eventId;
        private int _fleetId;
        private int _vehicleId;
        private int _driverId;
        private NonJobUpdateType _update;
        private double _latitude;
        private double _longitude;
        private DateTime _gpsTime;
        private int? _odometer;
        private int? _totalFuel;
        private BreakType? _breakType;
        #endregion

        #region properties
        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public NonJobUpdateType Update
        {
            get { return _update; }
            set { _update = value; }
        }
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public DateTime GpsTime
        {
            get { return _gpsTime; }
            set { _gpsTime = value; }
        }
        public int? Odometer
        {
            get { return _odometer; }
            set { _odometer = value; }
        }
        public int? TotalFuel
        {
            get { return _totalFuel; }
            set { _totalFuel = value; }
        }

        public BreakType? BreakType
        {
            get
            {
                return _breakType;
            }
            set { _breakType = value; }
        }
        #endregion

        #region constructor
        public NonJobEventSegment()
        {
            Version = 3;
            Type = (int)LogisticsLiveUpdateSegmentTypes.NonJobEvent;
        }
        public NonJobEventSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.NonJobEvent)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.NonJobEvent, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.NonJobEvent;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _driverId);
                stream.WriteByte((byte)_update);
                WriteCoordinate(stream, _latitude);
                WriteCoordinate(stream, _longitude);
                WriteDateTime(stream, _gpsTime);
                Write4ByteInteger(stream, _eventId);
                if (Version >= 3)
                {
                    int fieldsFlags = (_odometer != null ? 1 << 0 : 0)
                                      | (_totalFuel != null ? 1 << 1 : 0)
                                      | (_breakType != null ? 1 << 2 : 0);

                    WriteMoreFlag(stream, fieldsFlags);
                    if (_odometer != null)
                    {
                        Write4ByteInteger(stream, _odometer.Value);
                    }

                    if (_totalFuel != null)
                    {
                        Write4ByteInteger(stream, _totalFuel.Value);
                    }

                    if (_breakType != null)
                    {
                        stream.WriteByte((byte)_breakType.Value);
                    }
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _update = (NonJobUpdateType)Data[index++];
            _latitude = ReadCoordinate(Data, ref index);
            _longitude = ReadCoordinate(Data, ref index);
            _gpsTime = ReadDateTime(Data, ref index);
            _eventId = -1;
            if (Version >= 2)
            {
                _eventId = Read4ByteInteger(Data, ref index);
            }

            if (Version >= 3)
            {
                int fieldsFlags = ReadMoreFlag(Data, ref index);
                if ((fieldsFlags & (1 << 0)) != 0)
                {
                    _odometer = Read4ByteInteger(Data, ref index);
                }

                if ((fieldsFlags & (1 << 1)) != 0)
                {
                    _totalFuel = Read4ByteInteger(Data, ref index);
                }

                if ((fieldsFlags & (1 << 2)) != 0)
                {
                    _breakType = (BreakType)Data[index++];
                }
                else
                {
                    _breakType = null;
                }
            }
        }
        #endregion
    }
}

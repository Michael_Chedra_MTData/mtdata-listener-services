using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;
using MTData.Transport.GenericProtocol.Logistics.TrackableAsset;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class AssociationLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.AssociationLiveUpdateSegment;
        private AssociationLiveUpdateItem _prevAssociationSyncItem;
        private AssociationLiveUpdateItem _associationSyncItem;
        #endregion

        #region properties
        public AssociationLiveUpdateItem PrevAssociation 
        {
            get { return _prevAssociationSyncItem; }
        }

        public AssociationLiveUpdateItem Association 
        {
            get { return _associationSyncItem; }
        }
        #endregion

        #region constructor
        public AssociationLiveUpdateSegment(AssociationLiveUpdateItem prevAssociation, AssociationLiveUpdateItem association)
        {
            _prevAssociationSyncItem = prevAssociation;
            _associationSyncItem = association;
            Version = 1;
            Type = (int)SegmentType;
        }
        public AssociationLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                int fieldsFlags = (_prevAssociationSyncItem != null ? 1 << 0 : 0)
                    | (_associationSyncItem != null ? 1 << 1 : 0);
                WriteMoreFlag(stream, fieldsFlags);
                if (_prevAssociationSyncItem != null)
                {
                    _prevAssociationSyncItem.Encode(stream);
                }
                if (_associationSyncItem != null)
                {
                    _associationSyncItem.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            int fieldsFlags = ReadMoreFlag(Data, ref index);
            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _prevAssociationSyncItem = new AssociationLiveUpdateItem();
                _prevAssociationSyncItem.Decode(Data, ref index);
            }
            else
            {
                _prevAssociationSyncItem = null;
            }
            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _associationSyncItem = new AssociationLiveUpdateItem();
                _associationSyncItem.Decode(Data, ref index);
            }
            else
            {
                _associationSyncItem = null;
            }
        }
        #endregion    
    }
}

﻿using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.TrackableAsset;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    /// <summary>
    /// the type of object that has been assoicatied to a vehicle
    /// </summary>
    public enum AssociationObject
    {
        /// <summary>
        /// the object is a trackable asset
        /// </summary>
        TrackableAsset = 0,
        /// <summary>
        /// the object is another unit
        /// </summary>
        TrackingDevice = 1
    }


    public class AssetAssociationChangedSegment : SegmentLayer
    {
        #region private fields
        private AssociationObject _associationObject;
        private int? _assetUniqueId;
        private int? _assetListId;
        private int? _deviceId;
        private int? _deviceFleetId;
        private bool _associationStart;
        private AssociationTypes _associationType;
        private int? _fleetId;
        private int? _vehicleId;
        private DateTime _gpsTime;
        private DateTime _createTime;
        private double _latitude;
        private double _longitude;
        private int? _dispatchId;
        private int? _legActionId;
        private int? _driverId;
        private int? _userId;
        private string _assetName;
        private string _assetListName;
        private string _userName;
        #endregion

        #region properties
        /// <summary>
        /// the assoication object (asset or unit)
        /// </summary>
        public AssociationObject AssociationObject
        {
            get { return _associationObject; }
            set { _associationObject = value; }
        }
        /// <summary>
        /// If AssociationObject == TrackableAsset then the assets unique id otherwise null
        /// </summary>
        public int? AssetUniqueId
        {
            get { return _assetUniqueId; }
            set { _assetUniqueId = value; }
        }
        /// <summary>
        /// If AssociationObject == TrackableAsset then the assets name otherwise null
        /// </summary>
        public string AssetName
        {
            get { return _assetName; }
            set { _assetName = value; }
        }
        /// <summary>
        /// If AssociationObject == TrackableAsset then the list id the assets is in otherwise null
        /// </summary>
        public int? AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        }
        /// <summary>
        /// If AssociationObject == TrackableAsset then the assets list name otherwise null
        /// </summary>
        public string AssetListName
        {
            get { return _assetListName; }
            set { _assetListName = value; }
        }
        /// <summary>
        /// If AssociationObject == TrackingDevice then the vehicle's id otherwise null
        /// </summary>
        public int? DeviceId
        {
            get { return _deviceId; }
            set { _deviceId = value; }
        }
        /// <summary>
        /// If AssociationObject == TrackingDevice then the vehicle's fleet id otherwise null
        /// </summary>
        public int? DeviceFleetId
        {
            get { return _deviceFleetId; }
            set { _deviceFleetId = value; }
        }
        /// <summary>
        /// true if this is a start of an association, false oif it is an end
        /// </summary>
        public bool IsAssociationStart
        {
            get { return _associationStart; }
            set { _associationStart = value; }
        }
        /// <summary>
        /// the association type
        /// </summary>
        public AssociationTypes AssociationType
        {
            get { return _associationType; }
            set { _associationType = value; }
        }
        /// <summary>
        /// the fleet id of the vehicle the object has been associated to. Null if the AssociationType is Location
        /// </summary>
        public int? FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        /// <summary>
        /// the vehicle id of the vehicle the object has been associated to. Null if the AssociationType is Location
        /// </summary>
        public int? VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        /// <summary>
        /// the gps time (in UTC) of the assocaition event
        /// </summary>
        public DateTime GpsTime
        {
            get { return _gpsTime; }
            set { _gpsTime = value; }
        }
        /// <summary>
        /// the server time (in UTC) the assocaition event was created
        /// </summary>
        public DateTime CreateTime
        {
            get { return _createTime; }
            set { _createTime = value; }
        }
        /// <summary>
        /// the latitude of the assocaition event
        /// </summary>
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        /// <summary>
        /// the longitude of the assocaition event
        /// </summary>
        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }


        /// <summary>
        /// if a driver created/ended this assoication the driver id
        /// </summary>
        public int? DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        /// <summary>
        /// if a user created/ended this assoication the user's id
        /// </summary>
        public int? UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }
        /// <summary>
        /// if a user created/ended this assoication the user's name
        /// </summary>
        public string UserName
        {
            get { return _userName; }
            set { _userName = value; }
        }

        /// <summary>
        /// if assoication is linked to a job the jobs dispatch id, otherwise null
        /// </summary>
        public int? DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        /// <summary>
        /// if assoication is linked to a job the jobs leg action id, otherwise null
        /// </summary>
        public int? LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }
        #endregion

        #region constructor
        public AssetAssociationChangedSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.AssetAssociationChanged;
        }
        public AssetAssociationChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.AssetAssociationChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.AssetAssociationChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.AssetAssociationChanged;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                stream.WriteByte((byte)_associationObject);
                if (_associationObject == AssociationObject.TrackableAsset)
                {
                    Write4ByteInteger(stream, _assetUniqueId.Value);
                    WriteString(stream, _assetName, 0x10);
                    Write4ByteInteger(stream, _assetListId.Value);
                    WriteString(stream, _assetListName, 0x10);
                }
                else
                {
                    Write4ByteInteger(stream, _deviceId.Value);
                    Write4ByteInteger(stream, _deviceFleetId.Value);
                }
                WriteBool(stream, _associationStart);
                stream.WriteByte((byte)_associationType);
                Write4ByteInteger(stream, _fleetId.HasValue ? _fleetId.Value : -1);
                Write4ByteInteger(stream, _vehicleId.HasValue ? _vehicleId.Value : -1);
                WriteDateTime(stream, _gpsTime);
                WriteDateTime(stream, _createTime);
                WriteCoordinate(stream, _latitude);
                WriteCoordinate(stream, _longitude);
                Write4ByteInteger(stream, _dispatchId.HasValue ? _dispatchId.Value : -1);
                Write4ByteInteger(stream, _legActionId.HasValue ? _legActionId.Value : -1);
                Write4ByteInteger(stream, _driverId.HasValue ? _driverId.Value : -1);
                Write4ByteInteger(stream, _userId.HasValue ? _userId.Value : -1);
                WriteString(stream, _userName, 0x10);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _associationObject = (AssociationObject)Data[index++];
            if (_associationObject == AssociationObject.TrackableAsset)
            {
                _assetUniqueId = Read4ByteInteger(Data, ref index);
                _assetName = ReadString(Data, ref index, 0x10);
                _assetListId = Read4ByteInteger(Data, ref index);
                _assetListName = ReadString(Data, ref index, 0x10);
                _deviceId = null;
                _deviceFleetId = null;
            }
            else
            {
                _deviceId = Read4ByteInteger(Data, ref index);
                _deviceFleetId = Read4ByteInteger(Data, ref index);
                _assetListId = null;
                _assetListName = null;
                _assetName = null;
                _assetUniqueId = null;
            }
            _associationStart = ReadBool(Data, ref index);
            _associationType = (AssociationTypes)Data[index++];
            _fleetId = Read4ByteInteger(Data, ref index);
            if (_fleetId == -1)
            {
                _fleetId = null;
            }
            _vehicleId = Read4ByteInteger(Data, ref index);
            if (_vehicleId == -1)
            {
                _vehicleId = null;
            }
            _gpsTime = ReadDateTime(Data, ref index);
            _createTime = ReadDateTime(Data, ref index);
            _latitude = ReadCoordinate(Data, ref index);
            _longitude = ReadCoordinate(Data, ref index);
            _dispatchId = Read4ByteInteger(Data, ref index);
            if (_dispatchId == -1)
            {
                _dispatchId = null;
            }
            _legActionId = Read4ByteInteger(Data, ref index);
            if (_legActionId == -1)
            {
                _legActionId = null;
            }
            _driverId = Read4ByteInteger(Data, ref index);
            if (_driverId == -1)
            {
                _driverId = null;
            }
            _userId = Read4ByteInteger(Data, ref index);
            if (_userId == -1)
            {
                _userId = null;
            }
            _userName = ReadString(Data, ref index, 0x10);
        }
        #endregion        
    }
}

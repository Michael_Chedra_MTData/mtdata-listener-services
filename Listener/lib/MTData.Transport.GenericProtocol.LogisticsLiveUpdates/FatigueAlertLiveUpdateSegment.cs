using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;
using MTData.Transport.GenericProtocol.Logistics.IncidentReport;
using MTData.Transport.GenericProtocol.Logistics.Fatigue;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class FatigueAlertLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.FatigueAlert;
        private int _eventId;
        private int _fleetId;
        private int _vehicleId;
        private int _driverId;
        private DateTime _eventTime;
        private DateTime _serverTime;
        private double _latitude;
        private double _longitude;
        private List<FatigueAlert> _alerts;
        #endregion

        #region properties
        public int EventId
        {
            get { return _eventId; }
        }

        public int FleetId
        {
            get { return _fleetId; }
        }
        
        public int VehicleId
        {
            get { return _vehicleId; }
        }
        
        public int DriverId 
        {
            get { return _driverId; }
        }

        public DateTime EventTime
        {
            get { return _eventTime; }
        }

        /// <summary>
        /// The time the event was inserted into the database (UTC)
        /// </summary>
        public DateTime ServerTime
        {
            get { return _serverTime; }
        }

        public double Latitude
        {
            get { return _latitude; }
        }

        public double Longitude
        {
            get { return _longitude; }
        }

        public IEnumerable<FatigueAlert> Alerts 
        {
            get { return _alerts; }
        }

        #endregion

        #region constructor
        public FatigueAlertLiveUpdateSegment(int eventId, int fleetId, int vehicleId, int driverId, DateTime eventTime, DateTime serverTime, double latitude, double longitude, List<FatigueAlert> alerts)
        {
            Version = 1;
            Type = (int)SegmentType;
            _eventId = eventId;
            _fleetId = fleetId;
            _vehicleId = vehicleId;
            _driverId = driverId;
            _eventTime = eventTime;
            _serverTime = serverTime;
            _latitude = latitude;
            _longitude = longitude;
            _alerts = alerts;
        }

        public FatigueAlertLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _eventId);
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _driverId);
                WriteDateTime(stream, _eventTime);
                WriteDateTime(stream, _serverTime);
                WriteCoordinate(stream, _latitude);
                WriteCoordinate(stream, _longitude);
                WriteMoreFlag(stream, _alerts.Count);
                foreach (var alert in _alerts)
                {
                    alert.Encode(stream);
                }
                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _eventId = Read4ByteInteger(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _eventTime = ReadDateTime(Data, ref index);
            _serverTime = ReadDateTime(Data, ref index);
            _latitude = ReadCoordinate(Data, ref index);
            _longitude = ReadCoordinate(Data, ref index);
            _alerts = new List<FatigueAlert>();
            int alertCount = ReadMoreFlag(Data, ref index);
            for (int i = 0; i < alertCount; i++)
            {
                FatigueAlert alert = new FatigueAlert();
                alert.Decode(Data, ref index);
                _alerts.Add(alert);
            }
        }
        #endregion    
    }
}

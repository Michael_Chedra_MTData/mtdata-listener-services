using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.PreDefinedFunction;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class PredefinedResultSegment : SegmentLayer
    {
        #region private fields
        private int _fleetId;
        private int _vehicleId;
        private PredefinedFunction _function;
        private bool _complete;
        private byte[] _result;
        private string _message;
        #endregion

        #region properties
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        public PredefinedFunction Function
        {
            get { return _function; }
            set { _function = value; }
        }
        public bool Complete
        {
            get { return _complete; }
            set { _complete = value; }
        }
        public byte[] Result
        {
            get { return _result; }
            set { _result = value; }
        }
        public string Message
        {
            get { return _message; }
            set { _message = value; }
        }
        #endregion

        #region constructor
        public PredefinedResultSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.PredefinedResult;
        }
        public PredefinedResultSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.PredefinedResult)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.PredefinedResult, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.PredefinedResult;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                stream.WriteByte((byte)_function);
                WriteBool(stream, _complete);
                WriteString(stream, _message, 0x12);
                if (_result == null || _result.Length == 0)
                {
                    Write4ByteUInteger(stream, 0);
                }
                else
                {
                    Write4ByteInteger(stream, _result.Length);
                    stream.Write(_result, 0, _result.Length);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _function = (PredefinedFunction)Data[index++];
            _complete = ReadBool(Data, ref index);
            _message = ReadString(Data, ref index, 0x12);
            int length = Read4ByteInteger(Data, ref index);
            _result = new byte[length];
            if (length > 0)
            {
                Array.Copy(Data, index, _result, 0, length);
                index += length;
            }
        }
        #endregion    
    }
}

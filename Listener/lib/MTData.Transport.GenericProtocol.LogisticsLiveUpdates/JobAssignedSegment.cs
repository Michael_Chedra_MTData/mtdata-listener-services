using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class JobAssignedSegment : SegmentLayer
    {
        #region private fields
        private int _fleetId;
        private int _vehicleId;
        private int _dispatchId;
        private JobStatus _jobStatus;
        #endregion

        #region properties
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public JobStatus JobStatus
        {
            get { return _jobStatus; }
            set { _jobStatus = value; }
        }
        #endregion

        #region constructor
        public JobAssignedSegment()
        {
            Version = 2;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobAssignment;
        }
        public JobAssignedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.JobAssignment)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.JobAssignment, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobAssignment;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _dispatchId);
                if (Version >= 2)
                {
                    stream.WriteByte((byte)_jobStatus);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _dispatchId = Read4ByteInteger(Data, ref index);
            if (Version >= 2)
            {
                _jobStatus = (JobStatus)Data[index++];
            }
            else
            {
                _jobStatus = JobStatus.NotStarted;
            }
        }
        #endregion    
    }
}

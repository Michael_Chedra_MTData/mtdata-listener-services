using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class JobChangedSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchedId;
        private JobStatus _jobStatus;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchedId; }
            set { _dispatchedId = value; }
        }
        public JobStatus JobStatus
        {
            get { return _jobStatus; }
            set { _jobStatus = value; }
        }
        #endregion

        #region constructor
        public JobChangedSegment()
        {
            Version = 2;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobChanged;
        }
        public JobChangedSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.JobChanged)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.JobChanged, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobChanged;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchedId);
                if (Version >= 2)
                {
                    stream.WriteByte((byte)_jobStatus);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchedId = Read4ByteInteger(Data, ref index);
            if (Version >= 2)
            {
                _jobStatus = (JobStatus)Data[index++];
            }
            else
            {
                _jobStatus = JobStatus.NotStarted;
            }
        }
        #endregion    
    }
}

using System;
using System.Collections.Generic;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    /// <summary>
    /// the segemnt types for the logisitcs live updates
    /// </summary>
    public enum LogisticsLiveUpdateSegmentTypes
    {
        Heartbeat = SegmentTypes.LogisticsLiveUpdateSegments + 1,
        Ping,
        Pong,
        Login,
        Logout,
        Message,
        JobChanged,
        JobAssignment,
        JobCancelled,
        JobAddNewLegs,
        JobDeleteLegs,
        JobStatus,
        JobStartTransaction,
        JobEndTransaction,
        PredefinedRequest,
        PredefinedResult,
        NonJobEvent,
        ListUpdated,
        JobUnAllocated,
        [Obsolete]
        AssetAssociationChanged,
        JobActionAlertStatusChanged,
        SafeJourneyPlanEvent,
        DefectEventNotification,
        DefectLiveUpdate,
        IncidentEventNotification,
        IncidentLiveUpdate,
        AssociationEventNotification,
        AssociationLiveUpdateSegment,
        InspectionLiveUpdate,
        TerminalUpdate,
		FatigueAlert,
        IapComment,
        IapMassDeclartion
    }
}

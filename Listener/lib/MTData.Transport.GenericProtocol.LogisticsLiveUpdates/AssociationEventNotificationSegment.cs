using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class AssociationEventNotificationSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.AssociationEventNotification;
        private int _associationId;
        private int _userId;
        #endregion

        #region properties
        public int AssociationId
        {
            get { return _associationId; }
            set { _associationId = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        #endregion

        #region constructor
        public AssociationEventNotificationSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }
        public AssociationEventNotificationSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _associationId);
                Write4ByteInteger(stream, _userId);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _associationId = Read4ByteInteger(Data, ref index);
            _userId = Read4ByteInteger(Data, ref index);
        }
        #endregion    
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics.Jobs;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public enum JobUpdateType
    {
        JobReceived=0,
        JobRead,
        ArriveSite,
        DepartSite,
        StartActions,
        EmailAlert,
        LegComplete,
        LegActionComplete,
        CreateNewLeg,
        LegActionStatusChanged,
        JobComplete,
        JobChanged,
        JobActionStatusChanged,
        JobActionComplete,
        JobStatusChanged,
        CreateNewJobAction,
        CreateNewLegAction
    }

    public class JobStatusSegment : SegmentLayer
    {
        #region private fields
        private int _dispatchId;
        private int _fleetId;
        private int _vehicleId;
        private int _driverId;
        private JobUpdateType _update;
        private double _latitude;
        private double _longitude;
        private DateTime _gpsTime;
        private JobStatus _jobStatus;
        private int _legId;
        private LegStatus _legStatus;
        private int _legActionId;
        private LegActionStatus _legActionStatus;
        private EMailFlag _emailFlag;
        private int _jobActionId;
        private JobActionStatus _jobActionStatus;
        private LegActionTypes _legActionType;
        private JobActionTypes _jobActionType;
        private int _eventId;
        #endregion

        #region properties
        public int DispatchId
        {
            get { return _dispatchId; }
            set { _dispatchId = value; }
        }
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public JobUpdateType Update
        {
            get { return _update; }
            set { _update = value; }
        }
        public double Latitude
        {
            get { return _latitude; }
            set { _latitude = value; }
        }
        public double Longitude
        {
            get { return _longitude; }
            set { _longitude = value; }
        }
        public DateTime GpsTime
        {
            get { return _gpsTime; }
            set { _gpsTime = value; }
        }
        public JobStatus JobStatus
        {
            get { return _jobStatus; }
            set { _jobStatus = value; }
        }
        public int LegId
        {
            get { return _legId; }
            set { _legId = value; }
        }
        public LegStatus LegStatus
        {
            get { return _legStatus; }
            set { _legStatus = value; }
        }
        public int LegActionId
        {
            get { return _legActionId; }
            set { _legActionId = value; }
        }
        public LegActionStatus LegActionStatus
        {
            get { return _legActionStatus; }
            set { _legActionStatus = value; }
        }
        public EMailFlag EmailFlag
        {
            get { return _emailFlag; }
            set { _emailFlag = value; }
        }
        public int JobActionId
        {
            get { return _jobActionId; }
            set { _jobActionId = value; }
        }
        public JobActionStatus JobActionStatus
        {
            get { return _jobActionStatus; }
            set { _jobActionStatus = value; }
        }
        public LegActionTypes LegActionType
        {
            get { return _legActionType; }
            set { _legActionType = value; }
        }
        public JobActionTypes JobActionType
        {
            get { return _jobActionType; }
            set { _jobActionType = value; }
        }
        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }
        #endregion

        #region constructor
        public JobStatusSegment()
        {
            Version = 4;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobStatus;
        }
        public JobStatusSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.JobStatus)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.JobStatus, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.JobStatus;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _dispatchId);
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _driverId);
                stream.WriteByte((byte)_update);
                WriteCoordinate(stream, _latitude);
                WriteCoordinate(stream, _longitude);
                WriteDateTime(stream, _gpsTime);
                stream.WriteByte((byte)_jobStatus);
                Write4ByteInteger(stream, _legId);
                stream.WriteByte((byte)_legStatus);
                Write4ByteInteger(stream, _legActionId);
                stream.WriteByte((byte)_legActionStatus);
                stream.WriteByte((byte)_emailFlag);
                if (Version >= 2)
                {
                    Write4ByteInteger(stream, _jobActionId);
                    stream.WriteByte((byte)_jobActionStatus);
                    if (Version >= 3)
                    {
                        stream.WriteByte((byte)_legActionType);
                        stream.WriteByte((byte)_jobActionType);
                    }
                    if (Version >= 4)
                    {
                        Write4ByteInteger(stream, _eventId);
                    }
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _dispatchId = Read4ByteInteger(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _update = (JobUpdateType)Data[index++];
            _latitude = ReadCoordinate(Data, ref index);
            _longitude = ReadCoordinate(Data, ref index);
            _gpsTime = ReadDateTime(Data, ref index);
            _jobStatus = (JobStatus)Data[index++];
            _legId = Read4ByteInteger(Data, ref index);
            _legStatus = (LegStatus)Data[index++];
            _legActionId = Read4ByteInteger(Data, ref index);
            _legActionStatus = (LegActionStatus)Data[index++];
            _emailFlag = (EMailFlag)Data[index++];

            _legActionType = (LegActionTypes)(-1);
            _jobActionType = (JobActionTypes)(-1);
            _eventId = -1;
            if (Version >= 2)
            {
                _jobActionId = Read4ByteInteger(Data, ref index);
                _jobActionStatus = (JobActionStatus)Data[index++];
                if (Version >= 3)
                {
                    _legActionType = (LegActionTypes)Data[index++];
                    _jobActionType = (JobActionTypes)Data[index++];
                    if (Version >= 4)
                    {
                        _eventId = Read4ByteInteger(Data, ref index);
                    }
                }
            }
            else
            {
                _jobActionId = -1;
                _jobActionStatus = JobActionStatus.NotStarted;
            }
        }
        #endregion    

    }
}

using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class HeartbeatSegment : SegmentLayer
    {
        #region constructor
        public HeartbeatSegment()
        {
            Version = 1;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Heartbeat;
        }
        public HeartbeatSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.Heartbeat)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.Heartbeat, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Heartbeat;
            Data = segment.Data;
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            Data = null;
            return base.GetBytes();
        }

        #endregion

    }
}

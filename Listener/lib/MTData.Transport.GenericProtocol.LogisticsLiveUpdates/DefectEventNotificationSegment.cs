using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class DefectEventNotificationSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.DefectEventNotification;
        private int _defectId;
        private EventType _eventType;
        private int _eventId;
        private int _userId;
        #endregion

        #region properties
        public int DefectId
        {
            get { return _defectId; }
            set { _defectId = value; }
        }

        public EventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        #endregion

        #region constructor
        public DefectEventNotificationSegment()
        {
            Version = 1;
            Type = (int)SegmentType;
        }
        public DefectEventNotificationSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _defectId);
                Write2ByteInteger(stream, (int)_eventType);
                Write4ByteInteger(stream, _eventId);
                Write4ByteInteger(stream, _userId);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _defectId = Read4ByteInteger(Data, ref index);
            _eventType = (EventType)Read2ByteInteger(Data, ref index);
            _eventId = Read4ByteInteger(Data, ref index);
            _userId = Read4ByteInteger(Data, ref index);
        }
        #endregion    
    }
}

using System;
using System.Collections.Generic;
using System.IO;
using MTData.Transport.GenericProtocol.Logistics;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public class DefectLiveUpdateSegment : SegmentLayer
    {
        #region private fields
        public const LogisticsLiveUpdateSegmentTypes SegmentType = LogisticsLiveUpdateSegmentTypes.DefectLiveUpdate;
        private int _defectId;
        private EventType _eventType;
        private int _eventId;
        private int _fleetId;
        private int _vehicleId;
        private int _assetListItemId;
        private string _assetName;
        private int _userId;
        private int _driverId;
        private int _defectTypeId;
        private string _defectTypeName;
        private int _assetListId;
        private int _assetUniqueKey;
        private string _driverName;
        private DateTime _eventTime;
        private bool _defaultStopWork;
        #endregion

        #region properties
        public int DefectId
        {
            get { return _defectId; }
            set { _defectId = value; }
        }

        public EventType EventType
        {
            get { return _eventType; }
            set { _eventType = value; }
        }

        public int EventId
        {
            get { return _eventId; }
            set { _eventId = value; }
        }

        /// <summary>
        /// The ID of the fleet that this defect corresponds to. 
        /// If zero the defect must belong to a trackable asset.
        /// </summary>
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }

        /// <summary>
        /// The ID of the vehicle that this defect corresponds to. 
        /// If zero the defect must belong to a trackable asset.
        /// </summary>
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        
        /// <summary>
        /// The ID of the trackable asset that this defect corresponds to.  
        /// </summary>
        public int AssetListItemId
        {
            get { return _assetListItemId; }
            set { _assetListItemId = value; }
        }

        public string AssetName
        {
            get { return _assetName; }
            set { _assetName = value; }
        }

        public int UserId
        {
            get { return _userId; }
            set { _userId = value; }
        }

        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }

        public int DefectType
        {
            get { return _defectTypeId; }
            set { _defectTypeId = value; }
        }

        public string DefectTypeName
        {
            get { return _defectTypeName; }
            set { _defectTypeName = value; }
        }

        public int AssetListId
        {
            get { return _assetListId; }
            set { _assetListId = value; }
        } 

        public int AssetUniqueKey
        {
            get { return _assetUniqueKey; }
            set { _assetUniqueKey = value; }
        }

        public string DriverName
        {
            get { return _driverName; }
            set { _driverName = value; }
        }
        public DateTime EventTime
        {
            get { return _eventTime; }
            set { _eventTime = value; }
        }

        public bool DefaultStopWork
        {
            get { return _defaultStopWork; }
            set { _defaultStopWork = value; }
        }
        #endregion

        #region constructor
        public DefectLiveUpdateSegment()
        {
            Version = 3;
            Type = (int)SegmentType;
        }
        public DefectLiveUpdateSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)SegmentType)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", SegmentType, segment.Type));
            }
            Version = segment.Version;
            Type = (int)SegmentType;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {

                int fieldsFlags = (_assetName != null ? 1 << 0 : 0)
                    | (_defectTypeName != null ? 1 << 1 : 0)
                    | (_driverName != null ? 1 << 2 : 0);

                WriteMoreFlag(stream, fieldsFlags);
                Write4ByteInteger(stream, _defectId);
                Write2ByteInteger(stream, (int)_eventType);
                Write4ByteInteger(stream, _eventId);
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _assetListItemId);
                if (_assetName != null)
                {
                    WriteString(stream, _assetName);
                }
                Write4ByteInteger(stream, _userId);
                Write4ByteInteger(stream, _driverId);
                Write4ByteInteger(stream, _defectTypeId);
                if (_defectTypeName != null)
                {
                    WriteString(stream, _defectTypeName);
                }
                Write4ByteInteger(stream, _assetListId);
                Write4ByteInteger(stream, _assetUniqueKey);
                if (_driverName != null)
                {
                    WriteString(stream, _driverName);
                }
                WriteDateTime(stream, _eventTime);
                WriteBool(stream, _defaultStopWork);

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            int fieldsFlags = ReadMoreFlag(Data, ref index);
            _defectId = Read4ByteInteger(Data, ref index);
            _eventType = (EventType)Read2ByteInteger(Data, ref index);
            _eventId = Read4ByteInteger(Data, ref index);
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _assetListItemId = Read4ByteInteger(Data, ref index);
            if ((fieldsFlags & (1 << 0)) != 0)
            {
                _assetName = ReadString(Data, ref index);
            }
            else
            {
                _assetName = null;
            }
            _userId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _defectTypeId = Read4ByteInteger(Data, ref index);
            if ((fieldsFlags & (1 << 1)) != 0)
            {
                _defectTypeName = ReadString(Data, ref index);
            }
            else
            {
                _defectTypeName = null;
            }
            _assetListId = Read4ByteInteger(Data, ref index);
            _assetUniqueKey = Read4ByteInteger(Data, ref index);
            if ((fieldsFlags & (1 << 2)) != 0)
            {
                _driverName = ReadString(Data, ref index);
            }
            else
            {
                _driverName = null;
            }
            _eventTime = DateTime.MinValue;
            if (Version >= 2)
            {
                _eventTime = ReadDateTime(Data, ref index);
            }
            if(Version >=3)
                _defaultStopWork = ReadBool(Data, ref index);

        }
        #endregion    
    }
}

using System;
using System.Collections.Generic;
using System.IO;

namespace MTData.Transport.GenericProtocol.LogisticsLiveUpdates
{
    public enum DriverMessageType
    {
        MessageFromBase=0,
        MessageFromUnit,
        EmailFromUnit,
        SjpMessageFromBase,
        SjpMessageFromUnit
    }

    public enum DriverMessageStatus
    {
        NewMessage = 0,
        MessageReceivedByUnit,
        MessageReadByDriver,
    }

    public class MessageSegment : SegmentLayer
    {
        #region private fields
        private int _fleetId;
        private int _vehicleId;
        private int _driverId;
        private int _messageId;
        private DriverMessageStatus _messageStatus;
        private DriverMessageType _messageType;
        private int _messageReplyId;
        private int _sjpId;
        #endregion

        #region properties
        public int FleetId
        {
            get { return _fleetId; }
            set { _fleetId = value; }
        }
        public int VehicleId
        {
            get { return _vehicleId; }
            set { _vehicleId = value; }
        }
        public int DriverId
        {
            get { return _driverId; }
            set { _driverId = value; }
        }
        public int MessageId
        {
            get { return _messageId; }
            set { _messageId = value; }
        }
        public DriverMessageStatus MessageStatus
        {
            get { return _messageStatus; }
            set { _messageStatus = value; }
        }
        public DriverMessageType MessageType
        {
            get { return _messageType; }
            set { _messageType = value; }
        }
        /// <summary>
        /// the id of the message that this is a reply to or -1 if not
        /// </summary>
        public int MessageReplyId
        {
            get { return _messageReplyId; }
            set { _messageReplyId = value; }
        }

        public int SjpID
        {
            get { return _sjpId; }
            set { _sjpId = value; }
        }
        #endregion

        #region constructor
        public MessageSegment()
        {
            Version = 3;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Message;
            _messageReplyId = -1;
        }
        public MessageSegment(SegmentLayer segment)
        {
            if (segment.Type != (int)LogisticsLiveUpdateSegmentTypes.Message)
            {
                throw new Exception(string.Format("Can only create a segment of type {0}, not {1}", LogisticsLiveUpdateSegmentTypes.Message, segment.Type));
            }
            Version = segment.Version;
            Type = (int)LogisticsLiveUpdateSegmentTypes.Message;
            Data = segment.Data;
            DecodeData();
        }
        #endregion

        #region override methods
        public override byte[] GetBytes()
        {
            //set the data
            using (MemoryStream stream = new MemoryStream())
            {
                Write4ByteInteger(stream, _fleetId);
                Write4ByteInteger(stream, _vehicleId);
                Write4ByteInteger(stream, _driverId);
                Write4ByteInteger(stream, _messageId);
                stream.WriteByte((byte)_messageStatus);
                stream.WriteByte((byte)_messageType);
                Write4ByteInteger(stream, _messageReplyId);

                if (Version >= 3)
                {
                    Write4ByteInteger(stream, _sjpId);
                }

                Data = stream.ToArray();
                stream.Close();
                return base.GetBytes();
            }
        }

        public override int Decode(byte[] payload)
        {
            int read = base.Decode(payload);
            DecodeData();
            return read;
        }
        #endregion

        #region private methods
        private void DecodeData()
        {
            int index = 0;
            _fleetId = Read4ByteInteger(Data, ref index);
            _vehicleId = Read4ByteInteger(Data, ref index);
            _driverId = Read4ByteInteger(Data, ref index);
            _messageId = Read4ByteInteger(Data, ref index);
            _messageStatus = (DriverMessageStatus)Data[index++];
            _messageType = (DriverMessageType)Data[index++];
            _messageReplyId = Read4ByteInteger(Data, ref index);
            if (Version >= 3)
            {
                _sjpId = Read4ByteInteger(Data, ref index);
            }
        }
        #endregion    
    }
}

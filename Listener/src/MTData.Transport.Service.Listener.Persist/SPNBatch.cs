﻿using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue;
using System;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Linq;
using log4net;

namespace MTData.Transport.Service.Listener.Persist
{
    // Class to provide thread safe access to adding batched SPN data
    public class SPNBatch
    {
        private readonly object _bulkInsertSyncLock = new object();
        private DataTable _spnData;
        private readonly Stopwatch sw = new Stopwatch();
        private TimeSpan _batchInsertInterval;
        private ILog _log = LogManager.GetLogger(typeof(SPNBatch));

        public SPNBatch(int batchInsertInterval)
        {
            InitialiseTable();
            _batchInsertInterval = new TimeSpan(0, 0, batchInsertInterval);
            sw.Start();
        }

        public void AddRecord(long tableId, AdvancedEcm spn)
        {
            lock(_bulkInsertSyncLock)
            { 
                _spnData.Rows.Add(0, tableId, spn.SourceType, spn.SpnId, spn.Flags, spn.Value == null ? new byte[0] : spn.Value, spn.ValueFloat);
            }
        }

        /// <summary>
        /// Gets a DataTable containing all of the batched SPN data added since the last time this method was invoked.
        /// </summary>
        /// <param name="ignoreElapsedTime">if true don't check how long since method was last completed successfully.
        /// Set to true when calling for last time before running thread finished to make sure all data is saved to db</param>
        /// <remarks>
        /// This method is called periodically by one or more persit processing threads.
        /// </remarks>
        public DataTable TryGetBatchedRecords(bool ignoreElapsedTime)
        {
            DataTable batchedData = null;
            lock (_bulkInsertSyncLock)
            {
                if (ignoreElapsedTime || sw.Elapsed > _batchInsertInterval)
                {
                    batchedData = _spnData;

                    //remove duplicates
                    if (batchedData.Rows.Count > 0)
                    {
                        try
                        {
                            batchedData = batchedData.AsEnumerable().GroupBy(
                            r => new
                            {
                                TableID = r.Field<long>("TableID"),
                                SourceType = r.Field<byte>("SourceType"),
                                SPNCode = r.Field<int>("SPNCode"),
                                Flags = r.Field<int>("Flags")
                            }).Select(g => g.First()).CopyToDataTable();
                        }
                        catch (Exception exp)
                        {
                            _log.ErrorFormat("Error checking for duplicates - {0}", exp.Message);
                        }
                    }
                    InitialiseTable();
                    sw.Restart();
                }
            }
            return batchedData;
        }

        private void InitialiseTable()
        {
            _spnData = new DataTable();
            _spnData.Columns.Add(new DataColumn("StorageTable", typeof(int)));
            _spnData.Columns.Add(new DataColumn("TableID", typeof(long)));
            _spnData.Columns.Add(new DataColumn("SourceType", typeof(byte)));
            _spnData.Columns.Add(new DataColumn("SPNCode", typeof(int)));
            _spnData.Columns.Add(new DataColumn("Flags", typeof(int)));
            _spnData.Columns.Add(new DataColumn("SPNValue", typeof(byte[])));
            _spnData.Columns.Add(new DataColumn("SPNValueFloat", typeof(float)));
        }
    }
}

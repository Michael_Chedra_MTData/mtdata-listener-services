﻿using System;
using System.Collections;
using System.ComponentModel;
using System.Configuration.Install;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.Persist
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
            //this.serviceInstaller1.DisplayName = Util.DisplayName;
            //this.serviceInstaller1.ServiceName = Util.ServiceName;
        }
    }
}

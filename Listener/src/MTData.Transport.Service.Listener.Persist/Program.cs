﻿using System;
using System.Diagnostics;
using System.ServiceProcess;

namespace MTData.Transport.Service.Listener.Persist
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new ListenerPersist()
            };
            ServiceBase.Run(ServicesToRun);
        }

        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = (Exception)e.ExceptionObject;
            string sMsg = "App Domain Error :\r\n" + ex.Message + "\r\n\r\nSource : \r\n" + ex.Source + "\r\n\r\nStack Trace : \r\n" + ex.StackTrace;
            EventLog el = new EventLog();
            el.Source = "MTData Listener Persist Service";
            el.WriteEntry(sMsg, System.Diagnostics.EventLogEntryType.Error);
            el.Close();
        }
    }
}

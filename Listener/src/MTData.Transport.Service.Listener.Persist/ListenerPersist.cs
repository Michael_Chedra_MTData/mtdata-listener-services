﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Json;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using log4net;
using MTData.Common.Database;
using MTData.Common.Queues;
using MTData.Common.Threading;
using MTData.Common.Utilities;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue;

namespace MTData.Transport.Service.Listener.Persist
{
    public partial class ListenerPersist : ServiceBase
    {
        private ILog _log = LogManager.GetLogger(typeof(ListenerPersist));
        private static ILog _transactionLog = LogManager.GetLogger("TransactionLogger");
        private static ILog _statsLog = LogManager.GetLogger("StatsLogger");

        private List<EnhancedThread> _processingThreads;
        private EnhancedThread _archivingThread;
        private EnhancedThread _statsThread;
        private string _databaseConnection = "";
        private MSQueue _msQueue = null;
        private MSQueue _msArchiveQueue = null;
        private bool bAllowArchiveCommand = false;
        private string _wmiServiceName = "ListenerPersist";
        private SPNBatch _spnBatch;
        private EmailHelper _emailHelper;

        public ListenerPersist()
        {
            InitializeComponent();

            string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
            _emailHelper = new EmailHelper(new EmailClient(emailServiceUrl));

            //check database version
            CheckDatabaseVersion checkDB = new CheckDatabaseVersion(ConfigurationManager.AppSettings["ConnectString"]);
            DatabaseVersionErrorMessage errorMessage;
            if (checkDB.CheckDatabaseRequiredVersion(7, 4, 1, 11, out errorMessage) == false &&
                        errorMessage != null)
            {
                _emailHelper.SendDatabaseVersionErrorEmail(errorMessage);
            }

            if (ConfigurationManager.AppSettings["DebugMode"] != null)
            {
                if (ConfigurationManager.AppSettings["DebugMode"].ToLower() == "true")
                    OnStart(null);
            }

            try
            {
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WmiServiceName"]))
                {
                    _wmiServiceName = ConfigurationManager.AppSettings["WmiServiceName"];
                }

                MTData.Common.WMI.MTDataMonitor monitor = MTData.Common.WMI.MTDataMonitor.GetInstance(_wmiServiceName);
                if (monitor != null)
                {
                    if (!System.Management.Instrumentation.Instrumentation.IsAssemblyRegistered(typeof(MTData.Common.WMI.MTDataMonitor).Assembly))
                    {
                        System.Management.Instrumentation.Instrumentation.RegisterAssembly(typeof(MTData.Common.WMI.MTDataMonitor).Assembly);
                    }
                    System.Management.Instrumentation.Instrumentation.Publish(monitor);
                }
            }
            catch (Exception exWMIPublish)
            {
                _log.Error("Error publishing WMI service", exWMIPublish);
            }
        }

        protected override void OnStart(string[] args)
        {
            //check queue exists
            _databaseConnection = ConfigurationManager.AppSettings["ConnectString"];
            try
            {
                _msQueue = new MSQueue(ConfigurationManager.AppSettings["MessageQueueName"]);
            }
            catch (Exception exp)
            {
                _log.Error("Error creating MSMQ ", exp);
                throw exp;
            }
            try
            {
                _msArchiveQueue = new MSQueue(ConfigurationManager.AppSettings["ArchiveMessageQueueName"]);
            }
            catch { }

            int spnDataSaveTimeOut;
            if (!int.TryParse(ConfigurationManager.AppSettings["SpnDataSaveTimeOut"], out spnDataSaveTimeOut))
            {
                spnDataSaveTimeOut = 1;
            }
            _spnBatch = new SPNBatch(spnDataSaveTimeOut);

            //load the threads to unload the queue and persisit to the database
            int count = 1;
            try
            {
                count = Convert.ToInt32(ConfigurationManager.AppSettings["NumberOfProcessingThreads"]);
            }
            catch { }
            _processingThreads = new List<EnhancedThread>();
            for (int i = 0; i < count; i++)
            {
                _log.InfoFormat("Starting MSMQ Processing Thread {0}.", i + 1);
                EnhancedThread t = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(FastProcessSQLCommandsThreadHandler), i);
                t.Start();
                _processingThreads.Add(t);
            }
            _archivingThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ArchivePeriodThreadHandler), null);
            _archivingThread.Start();

            int iPerfLogging = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PacketRateLogging"]);
            if ((iPerfLogging > 0))
            {
                _statsThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(StatsThread), iPerfLogging);
                _statsThread.Start();
            }
        }

        protected override void OnStop()
        {
            //stop threads
            _processingThreads.ForEach(t => t.Stop());
            _archivingThread.Stop();
            _statsThread.Stop();

            //close queues
            if (_msQueue != null)
            {
                _msQueue.Q.Dispose();
                _msQueue = null;
            }
            if (_msArchiveQueue != null)
            {
                _msArchiveQueue.Q.Dispose();
                _msArchiveQueue = null;
            }
        }

        private object FastProcessSQLCommandsThreadHandler(EnhancedThread sender, object data)
        {
            int threadIndex = (int)data + 1;
            #region Local Vars
            int iReconnectSleepTime = 0;
            int iPauseOnFailedInsert = 0;
            int iTimeCounter = 0;
            int iTimeoutCounter = 0;
            int iMessageQueueSQLInsertTimeout = 0;
            bool bDatabaseConnected = false;
            byte[] bMessage = null;
            byte[] bAchiveMessage = null;
            MemoryStream stream = null;
            DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(TrackingQueueItem));
            TrackingQueueItem item = null;
            SqlConnection oConn = null;
            SqlCommand oCmd = null;
            long iStartDeQueueTicks = 0;
            long dequeueTicks = 0;

            #endregion

            _log.InfoFormat("Process SQL Queue Thread Started {0}.", threadIndex);
            #region Read Configuration Information
            try
            {
                iReconnectSleepTime = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ReconnectSleepTime"]);
            }
            catch (System.Exception)
            {
                iReconnectSleepTime = 10000;
            }
            try
            {
                iPauseOnFailedInsert = Convert.ToInt32(ConfigurationManager.AppSettings["PauseOnFailedSQLInsert"]);
            }
            catch (System.Exception)
            {
                iPauseOnFailedInsert = 5000;
            }
            long logQueueTimeTicks = 60000 * 10000;
            long logTransactionTimeTicks = 1000 * 10000;
            try
            {
                logQueueTimeTicks = Convert.ToInt64(ConfigurationManager.AppSettings["LogIfQueueTime"]) * 10000;
                logTransactionTimeTicks = Convert.ToInt64(ConfigurationManager.AppSettings["LogIfTransactionTime"]) * 10000;
            }
            catch (System.Exception)
            {
                iPauseOnFailedInsert = 5000;
            }
            try
            {
                iMessageQueueSQLInsertTimeout = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MessageQueueSQLInsertTimeout"]);
            }
            catch (System.Exception)
            {
                iMessageQueueSQLInsertTimeout = 60000;
            }
            #endregion
            #region Open the database connection
            bDatabaseConnected = false;
            while (!bDatabaseConnected && !sender.Stopping)
            {
                try
                {
                    oConn = new SqlConnection(_databaseConnection);
                    oConn.Open();
                    bDatabaseConnected = true;
                }
                catch (System.Exception ex)
                {
                    _log.Error("Failed to connect to DB.", ex);
                    bDatabaseConnected = false;
                    //sleep for reconnect sleep timer before trying again to connect to the db
                    iTimeCounter = 0;
                    while (!sender.Stopping && iTimeCounter < iReconnectSleepTime)
                    {
                        Thread.Sleep(100);
                        iTimeCounter += 100;
                    }
                }
            }
            #endregion
            while (!sender.Stopping)
            {
                #region Main Thread Loop
                if (bDatabaseConnected && bMessage == null)
                {
                    #region Read the first item in the MSMQ, if there is nothing there check for items in the archive queue
                    try
                    {
                        // Peek at the next message from the Q
                        bAchiveMessage = null;
                        iStartDeQueueTicks = DateTime.Now.Ticks;
                        bMessage = (byte[])_msQueue.ReadBody();
                        if (bMessage != null)
                        {
                            lock (oSyncUpdateCounter)
                            {
                                _transactionsRemoved++;
                            }
                        }
                        dequeueTicks = DateTime.Now.Ticks - iStartDeQueueTicks;
                        if (bMessage != null)
                        {
                            #region Process the byte array to retrieve the SQL Parameter values
                            stream = new MemoryStream(bMessage);
                            item = ser.ReadObject(stream) as TrackingQueueItem;
                            item.QueueTime = DateTime.Now.Ticks - item.TimePutOnQueue;
                            item.DequeueTime = dequeueTicks;
                            lock (oSyncUpdateCounter)
                            {
                                _transactionCounter++;
                                _transactionTotalQueuedTime += item.QueueTime;
                            }
                            #endregion
                        }
                        else
                        {
                            #region If there are no vehicle notification records to store, read the next archive from the queue
                            if (bAllowArchiveCommand)
                            {
                                bAchiveMessage = (byte[])_msArchiveQueue.ReadBody();
                            }
                            else
                                bAchiveMessage = null;
                            #endregion
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error("Error reading message from queue", ex);
                    }
                    #endregion
                }
                if (bDatabaseConnected && bMessage == null && bAchiveMessage == null)
                {
                    Thread.Sleep(100);	// There is no data to process
                }
                else
                {
                    if (bDatabaseConnected)
                    {
                        #region Process an insert InsertIntoVehicleNotification command
                        if (bMessage != null)
                        {
                            if (item != null)
                            {
                                #region Execute the insert command
                                try
                                {
                                    item.ExecuteCommand(oConn, iMessageQueueSQLInsertTimeout, (tableId, spn) => _spnBatch.AddRecord(tableId, spn));
                                    lock (oSyncUpdateCounter)
                                        _transactionTotalSavedTime += item.TransactionTime;
                                    iTimeoutCounter = 0;
                                    bMessage = null;
                                }
                                catch (System.Data.SqlClient.SqlException exSQL)
                                {
                                    #region SQL Error handling

                                    bDatabaseConnected = false;
                                    if (exSQL.Number == 50000)
                                    {
                                        #region If this is a data error
                                        bDatabaseConnected = true;
                                        iTimeoutCounter = 0;
                                        _log.Error("Data Error", exSQL);
                                        _log.Info("SQL Command : " + item.GetLastSqlStatement());
                                        bMessage = null;
                                        #endregion
                                    }
                                    else if (exSQL.Message.IndexOf("General network error", 0) >= 0)
                                    {
                                        //If this is a general network error, don't skip this command, wait for the SQL server/network to return
                                        _log.Info("SQL Server Connection Error");
                                        iTimeoutCounter = 0;
                                    }
                                    else if (exSQL.Message.IndexOf("Timeout expired.", 0) >= 0)
                                    {
                                        //If this is a command time out, then try the command again, a max of 5 times before moving on.
                                        _log.Info("Command Timeout");
                                        iTimeoutCounter++;
                                        if (iTimeoutCounter == 5)
                                        {
                                            _log.Info("SQL Command : " + item.GetLastSqlStatement());
                                        }
                                    }
                                    else if (exSQL.Message.IndexOf("resulted in an out-of-range datetime", 0) >= 0)
                                    {
                                        #region This command has an invalid date time field, skip it and move on to the next one.

                                        _log.Info("Bad Date Time in Status Report");
                                        iTimeoutCounter = 0;
                                        bMessage = null;
                                        bDatabaseConnected = true;
                                        _log.Info("SQL Command : " + item.GetLastSqlStatement());
                                        #endregion
                                    }
                                    else
                                    {
                                        #region If this is another SQL server not already handled, try the command 5 times, then move on.
                                        _log.Error("SQL Error", exSQL);
                                        iTimeoutCounter++;
                                        if (iTimeoutCounter == 5)
                                        {
                                            _log.Info("SQL Command : " + item.GetLastSqlStatement());
                                        }
                                        #endregion
                                    }

                                    #endregion
                                }
                                catch (System.Exception ex)
                                {
                                    #region Generic Error handling

                                    // The insert failed for some reason that is not an SQL error.. 
                                    _log.Error("Execute Insert - Gen Error", ex);
                                    bDatabaseConnected = false;
                                    iTimeoutCounter = 0;

                                    #endregion
                                }
                                #endregion

                                #region Check the results of the insert

                                try
                                {
                                    #region The SQL failed to return a new table ID, close the DB connection and reconnect

                                    if (iTimeoutCounter > 5)
                                    {
                                        _log.Info("Command has timed out 5 times, moving to next.  Sql : " + item.GetLastSqlStatement());
                                        bMessage = null;
                                    }
                                    if (!bDatabaseConnected)
                                    {
                                        _log.Info("Reconnecting to DB - Pausing for " + Convert.ToString(iPauseOnFailedInsert) + "ms.");
                                        Thread.Sleep(iPauseOnFailedInsert);
                                    }

                                    #endregion
                                }
                                catch (System.Exception ex)
                                {
                                    _log.Error("Closing connections to DB", ex);
                                }

                                #endregion

                                if (_transactionLog.IsDebugEnabled)
                                {
                                    _transactionLog.Debug(item.GetStatsString());
                                }
                                else if (item.QueueTime > logQueueTimeTicks || item.TransactionTime > logTransactionTimeTicks)
                                {
                                    _transactionLog.Info(item.GetStatsString());
                                }

                                lock (oSyncUpdateCounter)
                                {
                                    _transactionTotalPersistTime += DateTime.Now.Ticks - iStartDeQueueTicks;
                                }
                            }
                            else
                                bMessage = null;
                        }
                        #endregion

                        //Bulk save SPN Data
                        if (bDatabaseConnected && bMessage == null)
                        {
                            DataTable spnData = _spnBatch.TryGetBatchedRecords(sender.Stopping);
                            if (spnData != null && spnData.Rows.Count > 0)
                            {
                                try
                                {
                                    long startSpn = DateTime.Now.Ticks;
                                    using (SqlBulkCopy bulkCopy = new SqlBulkCopy(oConn))
                                    {
                                        bulkCopy.BulkCopyTimeout = 5; // in seconds
                                        bulkCopy.DestinationTableName = "T_SPN_Data";
                                        bulkCopy.WriteToServer(spnData);
                                    }
                                    lock (oSyncUpdateCounter)
                                    {
                                        _transactionSpnTotal++;
                                        _transactionSpnTotalTime += DateTime.Now.Ticks - startSpn;
                                        _transactionSpnCount += spnData.Rows.Count;
                                    }
                                }
                                catch (Exception exp)
                                {
                                    _log.Error("Execute Bulk SPNData Insert - Error", exp);
                                    bDatabaseConnected = false;
                                    foreach (DataRow r in spnData.Rows)
                                    {
                                        _log.ErrorFormat("{0},{1},{2},{3},{4},{5}", r[0], r[1], r[2], r[3], r[4], r[6]);
                                    }
                                    string smtpServer = ConfigurationManager.AppSettings["EMailSMTPServer"];
                                    string title = string.Format("{0} - Listener Persist", ConfigurationManager.AppSettings["ServerName"]);
                                    _emailHelper.SendEmail("Transport_TechService@mtdata.com.au", 
                                        "MTData.Listener.Persist@mtdata.com.au", title, "SPN Data not Saved", 
                                        string.Format("SPN data was not saved at {0} due to {1}", DateTime.Now, exp.Message), false);
                                }
                            }
                        }

                        #region Process an archive command
                        if (bDatabaseConnected && bMessage == null)
                        {
                            if (bAchiveMessage != null)
                            {
                                _log.Info("Processing Archive Command : " + BitConverter.ToString(bAchiveMessage, 0));
                                oCmd = _msArchiveQueue.ConvertByteArrayToSQLCommand(bAchiveMessage);
                                #region Execute the insert command
                                try
                                {
                                    oCmd.Connection = oConn;
                                    oCmd.ExecuteNonQuery();
                                    iTimeoutCounter = 0;
                                }
                                catch (System.Data.SqlClient.SqlException exSQL)
                                {
                                    bDatabaseConnected = false;
                                    if (exSQL.Number == 50000)
                                    {
                                        _log.Error("Archive Data Error", exSQL);
                                    }
                                    else if (exSQL.Message.IndexOf("Timeout expired.", 0) >= 0)
                                    {
                                        _log.Info("Archive Command Timeout");
                                    }
                                    else
                                        _log.Error("Archive Command Failed", exSQL);
                                }
                                catch (System.Exception ex)
                                {
                                    _log.Error("Execute Archive Command - Gen Error", ex);
                                    bDatabaseConnected = false;
                                }
                                #endregion
                                _log.Info("Archive Command Complete.");
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Close the database connection
                        try
                        {
                            if (oConn != null)
                            {
                                oConn.Dispose();
                                oConn = null;
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error("Closing connections to DB", ex);
                        }
                        #endregion
                        #region Try and re-connect to the database
                        try
                        {
                            oConn = new SqlConnection(_databaseConnection);
                            oConn.Open();
                            bDatabaseConnected = true;
                        }
                        catch (System.Exception ex)
                        {
                            if (ex.Message.IndexOf("SQL Server does not exist", 0) >= 0)
                            {
                                _log.Info("Failed to reconnect to DB.");
                            }
                            else
                                _log.Error("Connect to DB Failure", ex);
                            bDatabaseConnected = false;
                        }
                        #endregion
                        #region Sleep for the time specified in the ReconnectSleepTime config item
                        if (!bDatabaseConnected)
                        {
                            iTimeCounter = 0;
                            while (!sender.Stopping && iTimeCounter < iReconnectSleepTime)
                            {
                                Thread.Sleep(100);
                                iTimeCounter += 100;
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            #region Close the database connection
            try
            {
                oConn.Close();
                oConn.Dispose();
                oConn = null;
            }
            catch (System.Exception ex)
            {
                _log.Error("Closing connections to DB", ex);
                oConn = null;
            }
            #endregion
            _log.InfoFormat("Process Message Queue Thread Ended {0}.", threadIndex);
            return null;
        }

        private object ArchivePeriodThreadHandler(EnhancedThread sender, object data)
        {
            bool bInProccesingPeriod = false;
            List<int[]> oArchiveProcessingPeriods = null;
            string[] sSplit = null;
            int[] iItem = null;
            string sValue = "";
            int iCounter = 0;
            DateTime dtStartDate = DateTime.MinValue;
            DateTime dtEndDate = DateTime.MinValue;
            DateTime dtDate = DateTime.MinValue;

            try
            {
                for (int X = 1; X < 256; X++)
                {
                    // Try and read a "00:00" value from the config
                    sValue = ConfigurationManager.AppSettings["ProcessArchiveMessagesStart" + Convert.ToString(X)];
                    if (sValue == null) // If no value found, break the loop
                        break;
                    sSplit = sValue.Split(":".ToCharArray());   // Break the value into two ints, hours and minutes
                    if (sSplit.Length != 2) // If the string was not in the correct format, break the loop
                        break;
                    // Put the values into an int array ([0] = Start Hour, [1] = Start Mins, [2] = End Hour, [3] = End Minute)
                    iItem = new int[4];
                    iItem[0] = Convert.ToInt32(sSplit[0]);  // Start Hour
                    iItem[1] = Convert.ToInt32(sSplit[1]);  // Start Mins
                    // Try and read a "00:00" value from the config
                    sValue = ConfigurationManager.AppSettings["ProcessArchiveMessagesEnd" + Convert.ToString(X)];
                    if (sValue == null) // If no value found, break the loop
                        break;
                    sSplit = sValue.Split(":".ToCharArray());   // Break the value into two ints, hours and minutes
                    if (sSplit.Length != 2) // If the string was not in the correct format, break the loop
                        break;
                    iItem[2] = Convert.ToInt32(sSplit[0]);  // End Hour
                    iItem[3] = Convert.ToInt32(sSplit[1]);  // End Minute
                    // Check that the start and end times are not equal
                    dtDate = DateTime.Now;
                    dtStartDate = new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, iItem[0], iItem[1], 0);
                    dtEndDate = new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, iItem[2], iItem[3], 0);
                    if (dtStartDate.CompareTo(dtEndDate) != 0)
                    {
                        // Create an array to hold a list of time periods
                        if (oArchiveProcessingPeriods == null)
                            oArchiveProcessingPeriods = new List<int[]>();
                        // Add this time period to the list.
                        oArchiveProcessingPeriods.Add(iItem);
                    }
                }
                // If there are no time periods to process, set bAllowArchiveCommand = true, otherwise process this loop.
                if (oArchiveProcessingPeriods != null && oArchiveProcessingPeriods.Count > 0)
                {
                    while (!sender.Stopping)
                    {
                        iCounter++;
                        // Once a minute, check if we are in a processing period.
                        if (iCounter > 600)
                        {
                            #region Check if we are in a processing period
                            bInProccesingPeriod = false;
                            for (int X = 0; X < oArchiveProcessingPeriods.Count; X++)
                            {
                                iItem = oArchiveProcessingPeriods[X];
                                dtDate = DateTime.Now;
                                dtStartDate = new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, iItem[0], iItem[1], 0);
                                dtEndDate = new DateTime(dtDate.Year, dtDate.Month, dtDate.Day, iItem[2], iItem[3], 0);
                                iCounter = dtStartDate.CompareTo(dtEndDate);
                                if (iCounter != 0)  // If dtStartDate and dtEndDate are not equal
                                {
                                    if (iCounter > 0) // If dtStartDate is later than dtEndDate, set dtEndDate to tommorow (i.e. 16:00 to 02:00)
                                    {
                                        // dtStartDate > dtEndDate
                                        dtEndDate = dtEndDate.AddDays(1);
                                    }
                                    // If the current time is equal to or greater than dtStartDate and less then or equal to dtEndDate
                                    //      We are in a process period, stop comparing times and continue.
                                    if (dtDate.CompareTo(dtStartDate) >= 0 && dtDate.CompareTo(dtEndDate) <= 0)
                                    {
                                        bInProccesingPeriod = true;
                                        break;
                                    }
                                }
                            }
                            // Set the class var bAllowArchiveCommand to let the MSMQ threads know if they can process archive commands.
                            if (bInProccesingPeriod)
                                bAllowArchiveCommand = true;
                            else
                                bAllowArchiveCommand = false;
                            #endregion
                            iCounter = 0;
                        }
                        Thread.Sleep(100);
                    }
                }
                else
                    bAllowArchiveCommand = true; // If there are no archive period values, allow archiving all the time.
            }
            catch (System.Exception ex)
            {
                _log.Error("ArchivePeriodThreadHandler", ex);
            }
            return null;
        }

        #region Performace Counters
        private object oSyncUpdateCounter = new object();
        private int _transactionsRemoved = 0;
        private long _transactionCounter;
        private long _transactionTotalQueuedTime;
        private long _transactionTotalSavedTime;
        private long _transactionTotalPersistTime;
        private long _transactionSpnTotalTime;
        private int _transactionSpnTotal;
        private int _transactionSpnCount;

        private object StatsThread(EnhancedThread sender, object data)
        {
            int iTimeOut = 0;

            int transactionsRemoved = 0;
            double avTransactionQueueTime = 0;
            double avTransactionSaveTime = 0;
            double avTransactionPersistTime = 0;
            int spnTotal = 0;
            long spnTime = 0;
            double aveSpn;
            int spnCount = 0;

            DateTime lastStatsTime = DateTime.Now;
            try
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        iTimeOut = 0;
                        while (!sender.Stopping && iTimeOut < 600)
                        {
                            Thread.Sleep(100);
                            iTimeOut++;
                        }
                        if (!sender.Stopping)
                        {
                            DateTime now = DateTime.Now;
                            TimeSpan span = now - lastStatsTime;
                            lastStatsTime = now;

                            lock (oSyncUpdateCounter)
                            {
                                // Retrrieve the values
                                transactionsRemoved = _transactionsRemoved;
                                avTransactionQueueTime = (double)_transactionTotalQueuedTime / (double)_transactionCounter;
                                avTransactionSaveTime = (double)_transactionTotalSavedTime / (double)_transactionCounter;
                                avTransactionPersistTime = (double)_transactionTotalPersistTime / (double)_transactionCounter;
                                spnTime = _transactionSpnTotalTime;
                                spnTotal = _transactionSpnTotal;
                                spnCount = _transactionSpnCount;

                                // Reset the counters
                                _transactionsRemoved = 0;
                                _transactionCounter = 0;
                                _transactionTotalQueuedTime = 0;
                                _transactionTotalSavedTime = 0;
                                _transactionTotalPersistTime = 0;
                                _transactionSpnTotal = 0;
                                _transactionSpnTotalTime = 0;
                                _transactionSpnCount = 0;
                            }

                            MTData.Common.WMI.MTDataMonitor monitor = MTData.Common.WMI.MTDataMonitor.GetInstance(_wmiServiceName);
                            if (monitor != null && monitor.Published)
                            {
                                monitor.AvTransactionQueueTimeTicks = avTransactionQueueTime;
                                monitor.AvTransactionSaveTimeTicks = avTransactionSaveTime;
                                monitor.TransactionsRemovedFromMsmq = transactionsRemoved;
                            }

                            _statsLog.InfoFormat("Transactions Persisited : {0} ({1:0.0}/sec), Average Queue Time {2:0.0} ms, Average Save time {3:0.0} ms, Average Persist time {4:0.0})",
                                transactionsRemoved, transactionsRemoved / span.TotalSeconds,
                                avTransactionQueueTime / 10000f, avTransactionSaveTime / 10000f, avTransactionPersistTime / 10000f);
                            aveSpn = (double)spnTime / (double)spnTotal;
                            _statsLog.InfoFormat("SPN Data Saved {0} Transactions taking : {1:0.0} ms, ave time per transaction ({2:0.0} ms), total SPN's saved {3}, ave {4:0.0}", spnTotal, spnTime / 10000f, aveSpn / 10000f, spnCount, spnCount / (float)spnTotal);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _statsLog.Error("Error processing stats)", ex);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _statsLog.Error("Error in processing stats thread)", ex);
            }
            return null;
        }

        #endregion

    }
}

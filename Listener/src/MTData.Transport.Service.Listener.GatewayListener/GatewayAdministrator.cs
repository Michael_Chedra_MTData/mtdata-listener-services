using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Collections;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Configuration;
using log4net;
using MTData.Transport.Service.Listener.GatewayListener.Dashboard;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;
using MTData.Transport.Application.Communication;
using MTData.Transport.Application.Communication.LiveUdpates;
using MTData.Transport.Application.Communication.MsmqInterface;
using MTData.Common.Threading;
using MTData.Common.Utilities;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;

namespace MTData.Transport.Service.Listener.GatewayListener
{
    /// <summary>
    /// This class provides the hub of the Gateway Listener.
    /// It will configure all aspects of the listener, including the
    /// implementation of all packet handling methods
    /// </summary>
    public class GatewayAdministrator : IDisposable
    {
        public delegate void QueueClientUpdateDel(GatewayProtocolPacket packet);
        public delegate void SetAcceptUnitTrafficFlagDel(bool AcceptUnitTraffic);

        #region Private Variables
        private static ILog _log = LogManager.GetLogger(typeof(GatewayAdministrator));
        private static ILog _statsLog = LogManager.GetLogger("StatsLogger");
        private static ILog _ignorePacketsLog = LogManager.GetLogger("IgnorePacketsLogger");
        private readonly string _wmiServiceName = "ListenerService";
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator.";

        private bool _acceptTrafficFromUnits;
        private DateTime _lastQueuedMessageCheck = DateTime.MinValue;
        private DateTime _lastDownloadCheck = DateTime.MinValue;
        private object obMessCheck = new object();
        private object obDownloadCheck = new object();

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> obMess = null;
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> obDownloads = null;
        /// <summary>
        /// List to maintian valid and invalid packet history..
        /// </summary>
        private InvalidPacketHistory _invalidPacketHistory = null;

        /// <summary>
        /// This will set the packet count in the UdpQueue to a default value.
        /// </summary>
        private int _udpRxQueue_MaxPacketCount = 4;

        private static List<DownloadEntry> currentDownloads = new List<DownloadEntry>();

        private List<UnitEventCounter> accidentEventCounter = new List<UnitEventCounter>();
        private List<UnitEventCounter> gforceEventCounter = new List<UnitEventCounter>();
        private List<UnitEventCounter> gforceCalibrationEventCounter = new List<UnitEventCounter>();
        private DateTime lastClearedTime;
        private int packetAbortTimeout;
        private bool _logListenerMessage;
        private ListenerLogToMsmq _logToMsmq;
        #region EMail Variables

        /// <summary>
        /// Indicates whether email is active or not.
        /// </summary>
        private bool _emailActive = true;

        /// <summary>
        /// This is the email address that the email is from
        /// </summary>
        private string _emailSourceAddress = @"DATSSERVER@mtdata.com.au";

        /// <summary>
        /// Indicates target email address.
        /// </summary>
        private string _emailTargetAddress = @"DATSSupport@mtdata.com.au";

        /// <summary>
        /// Indicates email subject
        /// </summary>
        private string _emailSubject = "DATSListener Error Notification";

        /// <summary>
        /// Inidcates email messagwe to be sent. along with error.
        /// </summary>
        private string _emailMessage = "DATSListener encountered the following error";

        #endregion

        #region RouteManagement

        private RouteManagement _routeManagement = null;

        #endregion

        #region Packet Exchange

        private PacketSource.PacketExchange _packetExchange = null;

        #endregion

        #region Server Download Variables
        private int _maxServerDownloads = 100;
        private int _fleetDownloadDefault = 10;
        private Dictionary<int, int> _fleetDownloadMax = new Dictionary<int, int>();
        #endregion
        /// <summary>
        /// MySql Parameters for MySql update and integration
        /// </summary>
        private MySQLInterface.cMySQLInterfaceParams _mySqlParams;

        /// <summary>
        /// This is the connection string for use in Database interaction
        /// </summary>
        private string sConnectString = null;

        /// <summary>
        /// This is the port that the mobile units will connect to
        /// </summary>
        private int iConnectPort;

        /// <summary>
        /// Port to which client applications will connect for live updates
        /// </summary>
        private int iClientNotifyPort;

        /// <summary>
        /// Packet Rate Limiter interval
        /// </summary>
        private int iPrlInterval;

        /// <summary>
        /// Packet Rate Limiter Threshold
        /// </summary>
        private int iPrlThreshold;

        /// <summary>
        /// This is the interface bind address ??
        /// </summary>
        private string sInterfaceBindAddress;

        /// <summary>
        /// This is a list of the fleets that need the listener to restore the MDT/Aux States between reports
        /// </summary>
        private string sRestoreStatusFleets;
        /// <summary>
        /// This is a list of the fleets that use concrete settings
        /// </summary>
        private string sConcreteFleets;

        /// <summary>
        /// This is a list of the fleets that are to send their concrete status through to Command via the com port.
        /// </summary>
        private string sFleetsToForwardToCommand;

        /// <summary>
        /// This is the port that configuration updates are notified to the listener
        /// </summary>
        private int iStateUpdatePort;

        /// <summary>
        /// This is the download interval to use when retrying downloads
        /// </summary>
        public int iTrackingDeviceDownloadRetryInterval;

        /// <summary>
        /// Retry interval for downloading items
        /// </summary>
        public int iInterfaceBoxDownloadRetryInterval;

        /// <summary>
        /// Retyr interval for data terminal downloads.
        /// </summary>
        public int iDataTerminalDownloadRetryInterval;
        /// <summary>
        /// Retry interval for 4000 downloads.
        /// </summary>
        public int i4000DownloadRetryInterval;

        /// <summary>
        /// This is the number of times to retry the download packet before giving up.
        /// </summary>
        public int iTrackingDeviceDownloadRetryCount;

        /// <summary>
        /// This is the number of times to retry the download packet before giving up.
        /// </summary>
        public int iInterfaceBoxDownloadRetryCount;

        /// <summary>
        /// This is the number of times to retry the download packet before giving up.
        /// </summary>
        public int iDataTerminalDownloadRetryCount;
        /// <summary>
        /// This is the number of times to retry the download packet before giving up.
        /// </summary>
        public int i4000DownloadRetryCount;

        /// <summary>
        /// Log details of the received packet information to the log file
        /// </summary>
        private bool bLogPacketData = false;

        /// <summary>
        /// Log updates passed on to registered clients to log file.
        /// </summary>
        private bool bLogClientUpdateData = false;

        /// <summary>
        /// This is the timeo9ut in seconds for command execution
        /// </summary>
        private int _commandTimeout = 30;

        /// <summary>
        /// This is packet creator.  This is a centralised place for creating/decoding packets.
        /// </summary>
        private PacketCreater oPC = null;

        /// <summary>
        /// This is packet creator.  This is a centralised place for creating/decoding packets.
        /// </summary>
        private Dashboard.DashboardProcessing _dashProcessing = null;

        private bool _stopped = true;

        public bool Stopped { get { return _stopped; } }

        private int listenerUserID = 0;
        private string listenerUser;
        private string listenerPassword;
        private int listenerAccidentThreshold = 5;
        private int listenerGForceThreshold = 20;
        private class RxThread
        {
            public IPEndPoint ipe;
            public byte[] rxContents;
        }
        private Thread ts;
        private Thread clientUpdateThread;
        private Queue mRxThreads;
        private Queue _clientLiveUpdateQueue;

        private UdpClient mClient;
        private UniversalListenerThread mListenerThread;
        public PacketDatabaseInterface mDBInterface;
        public ClientRegistry mClientRegistry;
        private StateUpdateListenerThread mStateUpdateThread;
        private RemoteUpdateEngine _remoteUpdateEngine = null;
        private GatewayProtocolPacket[] mRxedPackets;
        public int mRxedPacketCount;
        public int mViewedPacket;
        public object SyncRoot = new object();
        public bool bLogExceptions;

        /// <summary>
        /// This is used to ensure that the date format in USerDefined is one that is usable by all clients.
        /// </summary>
        private string _userDefinedDateFormat = null;

        /// <summary>
        /// This is the date format to use for ServerTime in live updates
        /// </summary>
        private string _serverTime_DateFormat = null;
        private string _sUserInfoDefaultField = "";
        private EnhancedThread oPacketRateTimer = null;

        private EnhancedThread _scheduleTasksThread;

        #endregion

        /// <summary>
        /// Prepare the class for use. This will instantiate all standard
        /// items, but will not kick off the threads. That is performed within
        /// the Start method.
        /// </summary>
        public GatewayAdministrator()
        {
            int iPerfLogging = 0;
            bool performanceLogging = false;

            try
            {
                lastClearedTime = DateTime.Now;

                if (System.Configuration.ConfigurationManager.AppSettings["DefaultUserInfoTagValue"] != null)
                    _sUserInfoDefaultField = System.Configuration.ConfigurationManager.AppSettings["DefaultUserInfoTagValue"];

                _logListenerMessage = false;
                if (ConfigurationManager.AppSettings["LogPacketsToDatabase"] != null)
                {
                    _logListenerMessage = Convert.ToBoolean(ConfigurationManager.AppSettings["LogPacketsToDatabase"]);
                }

                if (System.Configuration.ConfigurationManager.AppSettings["PacketAbortTimeout"] != null)
                {
                    try
                    {
                        this.packetAbortTimeout = Int32.Parse(System.Configuration.ConfigurationManager.AppSettings["PacketAbortTimeout"]);

                        if(this.packetAbortTimeout == 0)
                        {
                            this.packetAbortTimeout = 90;
                        }
                    }
                    catch (Exception)
                    {
                        this.packetAbortTimeout = 90;
                    }
                }

                this.LoadDownloadSettings();
                try
                {
                    _logToMsmq = (ListenerLogToMsmq)System.Configuration.ConfigurationManager.GetSection("ListenerLogToMsmq");
                }
                catch (Exception exp)
                {
                    _log.Error("Error creating Listener Log Message Queue", exp);
                    _logToMsmq = null;
                }


                oPC = new PacketCreater();
                oPC.OutboundPacket += new MTData.Transport.Application.Communication.LiveUdpates.PacketCreater.OutboundPacketDelegate(oPC_OutboundPacket);
                _invalidPacketHistory = new InvalidPacketHistory();
                _invalidPacketHistory.RequiresACK += new InvalidPacketHistoryRequiresACKDelegate(_invalidPacketHistory_RequiresACK);
                _mySqlParams = null;

                string temp = System.Configuration.ConfigurationManager.AppSettings["PerformanceLogging"];
                if ((temp != null) && (temp.ToLower() == "true"))
                    performanceLogging = true;

                MTData.Common.Performance.PerformanceLogging.Suspend(!performanceLogging);
            }
            catch (Exception exMain)
            {
                _log.Error(sClassName + "GatewayAdministrator(GatewayGUIInterface guiInterface, GatewayGUITrafficInterface guiTrafficInterface)", exMain);
                throw new System.Exception(sClassName + "GatewayAdministrator(GatewayGUIInterface guiInterface, GatewayGUITrafficInterface guiTrafficInterface)", exMain);
            }
            try
            {
                if (_dashProcessing == null)
                {
                    _dashProcessing = (Dashboard.DashboardProcessing)System.Configuration.ConfigurationManager.GetSection("DashProcessing");
                }
            }
            catch (Exception exDash)
            {
                _log.Error(sClassName + "GatewayAdministrator(bool transportSolution, GatewayGUIInterface guiInterface, GatewayGUITrafficInterface guiTrafficInterface)", exDash);
            }
            #region WMI publish
            try
            {
                if (!string.IsNullOrEmpty(System.Configuration.ConfigurationManager.AppSettings["WmiServiceName"]))
                    this._wmiServiceName = System.Configuration.ConfigurationManager.AppSettings["WmiServiceName"];
                MTData.Common.WMI.MTDataMonitor monitor = MTData.Common.WMI.MTDataMonitor.GetInstance(this._wmiServiceName);
                if (monitor != null)
                {

                    if (!System.Management.Instrumentation.Instrumentation.IsAssemblyRegistered(typeof(MTData.Common.WMI.MTDataMonitor).Assembly))
                    {
                        System.Management.Instrumentation.Instrumentation.RegisterAssembly(typeof(MTData.Common.WMI.MTDataMonitor).Assembly);
                    }

                    System.Management.Instrumentation.Instrumentation.Publish(monitor);

                }
                else
                {

                }
            }
            catch (Exception exWMIPublish)
            {
                _log.Error(sClassName + "GatewayAdministrator(GatewayGUIInterface guiInterface, GatewayGUITrafficInterface guiTrafficInterface)", exWMIPublish);
            }
            #endregion
            try
            {
                iPerfLogging = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PacketRateLogging"]);
                if ((iPerfLogging > 0))
                {
                    oPacketRateTimer = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(PacketRateThread), iPerfLogging);
                    oPacketRateTimer.Start();
                }
            }
            catch (System.Exception exPerf)
            {
                _log.Error(sClassName + "GatewayAdministrator(GatewayGUIInterface guiInterface, GatewayGUITrafficInterface guiTrafficInterface)", exPerf);
            }
        }

        private void LoadDownloadSettings()
        {
            if (System.Configuration.ConfigurationManager.AppSettings["ServerMaxDownloads"] != null)
            {
                // Otherwise use the default of 100
                this._maxServerDownloads = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ServerMaxDownloads"]);
            }

            if (System.Configuration.ConfigurationManager.AppSettings["FleetDownloadDefault"] != null)
            {
                // Otherwise use the default of 10
                this._fleetDownloadDefault = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["FleetDownloadDefault"]);
            }

            if (System.Configuration.ConfigurationManager.AppSettings["FleetDownloadMaximums"] != null)
            {
                this._fleetDownloadMax = (ConfigurationManager.GetSection("FleetDownloadMaximums") as System.Collections.Hashtable)
                 .Cast<System.Collections.DictionaryEntry>()
                 .ToDictionary(n => Convert.ToInt32(n.Key), n => Convert.ToInt32(n.Value));
            }
        }

        //public ~GatewayAdministrator()
        //{
        //    MTData.Common.WMI.MTDataMonitor monitor = MTData.Common.WMI.MTDataMonitor.GetInstance(this._wmiServiceName);
        //    if (monitor != null && monitor.Published)
        //    {

        //        if (System.Management.Instrumentation.Instrumentation.IsAssemblyRegistered(typeof(MTData.Common.WMI.MTDataMonitor).Assembly))
        //        {
        //            System.Management.Instrumentation.Instrumentation.Revoke(monitor);
        //        }
        //    }
        //}
        private const int ACTIVITYLIST_MAXLENGTH = 250;

        /// <summary>
        /// Confirm that the prerequisites are available
        /// </summary>
        /// <param name="logging"></param>
        /// <returns></returns>
        public static bool ConfirmPreRequisites()
        {
            bool bRet = false;

            try
            {
                string sValue = ConfigurationManager.AppSettings["ConnectString"];
                if (sValue == null)
                    throw new Exception("No connection string available");

                bRet = DatabaseInterface.ConfirmConnection(sValue);
                if (bRet)
                {
                    bRet = DatabaseInterface.ConfirmMSMQ();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error("MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator.ConfirmPreRequisites()", ex);
                bRet = false;
            }
            return bRet;
        }

        bool _allowAckOfAcks = false;

        /// <summary>
        /// This method will initiate all threads for use within the Gateway Listener.
        /// It will coordinate all aspects of initiation.
        /// </summary>
        public string Start(IEmailHelper emailHelper)
        {
            _stopped = false;
            string titleText = "MTData DATS Listener";
            string statusText = "";
            IPEndPoint ip = null;

            try
            {
                _log.Info("Starting Listener Services.");
                _clientLiveUpdateQueue = Queue.Synchronized(new Queue());

                // Read app.config file:
                string sConfRead = SetupFromConfigFile();
                if (sConfRead != null)
                    return sConfRead;

                #region Read Config Values
                _userDefinedDateFormat = ConfigurationManager.AppSettings["UserDefined_DateFormat"];
                _serverTime_DateFormat = ConfigurationManager.AppSettings["ServerTime_DateFormat"];

                string acKAllRequest = ConfigurationManager.AppSettings["AckAllRequested"];
                if ((acKAllRequest != null) && (acKAllRequest.ToLower() == "true"))
                    _allowAckOfAcks = true;

                if (ConfigurationManager.AppSettings["LogPacketData"] == "true")
                    bLogPacketData = true;
                else
                    bLogPacketData = false;
                if (ConfigurationManager.AppSettings["LogClientUpdateData"] == "true")
                    bLogClientUpdateData = true;
                else
                    bLogClientUpdateData = false;

                try
                {
                    IPAddress ipAddress = System.Net.IPAddress.Parse(sInterfaceBindAddress);
                    if (ipAddress != null)
                        ip = new IPEndPoint(ipAddress, iConnectPort);
                }
                catch
                {
                    IPHostEntry ipHostInfo = Dns.GetHostEntry(sInterfaceBindAddress); //"host.contoso.com"
                    IPAddress ipAddress = null;
                    for (int X = 0; X < ipHostInfo.AddressList.Length; X++)
                    {
                        if (ipHostInfo.AddressList[X].AddressFamily == AddressFamily.InterNetwork)
                        {
                            ipAddress = ipHostInfo.AddressList[X];
                            break;
                        }
                    }
                    if (ipAddress != null)
                    {
                        ip = new IPEndPoint(ipAddress, iConnectPort);
                    }
                    else
                    {
                        ip = null;
                    }
                }
                try
                {
                    mClient = new UdpClient(ip);
                }
                catch (Exception exClient)
                {
                    _log.Error(sClassName + "Start()", exClient);
                    return "An Error Occurred when attempting to connect to the Listening Port";
                }
                statusText = "Unit Port: " + iConnectPort + ", DB: " + sConnectString + ", LiveUpdate Port: " + iClientNotifyPort;


                if (ConfigurationManager.AppSettings["ListenerUserName"] != null)
                {
                    this.listenerUser = ConfigurationManager.AppSettings["ListenerUserName"];
                    _log.Info("Configured Listener Username");
                }

                if (ConfigurationManager.AppSettings["ListenerPassword"] != null)
                {
                    this.listenerPassword = ConfigurationManager.AppSettings["ListenerPassword"];
                    _log.Info("Configured Listener Password");
                }

                if (this.listenerUser != string.Empty && this.listenerPassword != string.Empty)
                {
                    try
                    {
                        MTData.Common.Crypto.TextEncryption encryption = new MTData.Common.Crypto.TextEncryption();

                        this.listenerUserID = MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.CheckUsernamePassword(this.listenerUser,
                            encryption.EncryptBase64(this.listenerPassword));
                        _log.Info("Configured Listener UserID");
                    }
                    catch (Exception)
                    { }
                }

                if (ConfigurationManager.AppSettings["ListenerAccidentThreshold"] != null)
                {
                    try
                    {
                        this.listenerAccidentThreshold = int.Parse(ConfigurationManager.AppSettings["ListenerAccidentThreshold"]);
                    }
                    catch (Exception)
                    { }

                }

                _log.Info("Configured Listener Accident Threshold to " + this.listenerAccidentThreshold.ToString());

                if (ConfigurationManager.AppSettings["ListenerGForceThreshold"] != null)
                {
                    try
                    {
                        this.listenerGForceThreshold = int.Parse(ConfigurationManager.AppSettings["ListenerGForceThreshold"]);
                    }
                    catch (Exception)
                    { }

                }

                _log.Info("Configured Listener GForce Threshold to " + this.listenerGForceThreshold.ToString());
                #endregion
                #region Create mDBInterface
                _log.Info("Starting Database Interface.");
                _remoteUpdateEngine = new RemoteUpdateEngine(ConfigurationManager.AppSettings["RemoteUpdateBackupFile"]);

                //	Initialise the PacketExchange
                _packetExchange = new MTData.Transport.Service.Listener.GatewayListener.PacketSource.PacketExchange();

                #region Routing - POD
                //	Initialise the RouteManagement class
                string temp = ConfigurationManager.AppSettings["RouteSupportActive"];
                if ((temp != null) && (Boolean.Parse(temp)))
                {
                    temp = ConfigurationManager.AppSettings["RouteLookAheadIntervalHrs"];
                    int lookAheadHrs = 24;
                    if (temp != null)
                        lookAheadHrs = Int32.Parse(temp);

                    _routeManagement = new RouteManagement(lookAheadHrs, sConnectString);
                    _packetExchange.RegisterPacketSource(_routeManagement);
                }
                #endregion
                mDBInterface = new
                    PacketDatabaseInterface(sConnectString, mClient, iPrlInterval, iPrlThreshold, _commandTimeout, 
                    _remoteUpdateEngine, _userDefinedDateFormat, _allowAckOfAcks, _serverTime_DateFormat, _sUserInfoDefaultField,
                    _routeManagement, _packetExchange, (_dashProcessing != null ? _dashProcessing.DashInterface : null), new SetAcceptUnitTrafficFlagDel(SetAcceptUnitTrafficFlag), emailHelper);

                mDBInterface.eConsoleEvent += new MTData.Transport.Service.Listener.GatewayListener.PacketDatabaseInterface.WriteToConsoleEvent(mDBInterface_eConsoleEvent);
                mDBInterface.bLogPacketData = bLogPacketData;
                mDBInterface.StationaryMinuteThreshold = Convert.ToInt32(ConfigurationManager.AppSettings["ConstantPositionTimeMinutes"]);
                mDBInterface.ConstantPositionThreshold = Convert.ToDecimal(ConfigurationManager.AppSettings["ConstantPositionThreshold"]);
                // Assume user wants to watch all:
                mDBInterface.eSendLiveUpdate += new PacketDatabaseInterface.SendLiveUpdateDelegate(mDBInterface_eSendLiveUpdate);

                if (ConfigurationManager.AppSettings["ForwardToCommandEnabled"] == "true")
                {
                    _log.Info("Forward To Command Interface enabled.");

                    if (ConfigurationManager.AppSettings["DigiComSendTimestamps"] == "true")
                    {
                        mDBInterface.SendCommandTimestamps = true;
                        _log.Info("Digi Command Interface will pass timestamps.");
                    }
                    else
                    {
                        mDBInterface.SendCommandTimestamps = false;
                        _log.Info("Digi Command Interface will not pass timestamps.");
                    }
                    mDBInterface.eConcreteStatusChanged += new MTData.Transport.Service.Listener.GatewayListener.PacketDatabaseInterface.ConcreteStatusChangeEvent(mDBInterface_eConcreteStatusChanged);
                }
                #endregion
                #region Creating Vehicle Queues.
                _log.Info("Creating Vehicle Queues.");
                mRxThreads = Queue.Synchronized(new Queue());
                mRxedPackets = new GatewayProtocolPacket[ACTIVITYLIST_MAXLENGTH];
                _clientLiveUpdateQueue = Queue.Synchronized(new Queue());

                mRxedPacketCount = 0;
                // Create the delegate stuff here rather than when a packet 
                // comes in - saves cycles
                mViewedPacket = 1;	// 1-based visible counter

                ts = new Thread(new ThreadStart(ProcessPacket));
                ts.Name = "Listening Form Process Thread";
                clientUpdateThread = new Thread(new ThreadStart(SendClientUpdates));
                clientUpdateThread.Name = "Client Update Thread";

                titleText += " on " + ip.Address.ToString();
                bProcessPacketThreadActive = true;
                ts.Start();
                clientUpdateThread.Start();
                #endregion
                #region Create _remoteUpdateEngine
                _log.Info("Preparing Remote Update Engine.");
                //	prepare the remoteupdate engine for use.
                _remoteUpdateEngine.VehicleCreated += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleModifiedDelegate(_remoteUpdateEngine_VehicleCreated);
                _remoteUpdateEngine.VehicleDeleted += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleModifiedDelegate(_remoteUpdateEngine_VehicleDeleted);
                _remoteUpdateEngine.VehicleLogin += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleLoginDelegate(_remoteUpdateEngine_VehicleLogin);
                _remoteUpdateEngine.VehicleMessageRecievd += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleMessageRecievdDelegate(_remoteUpdateEngine_VehicleMessageRecievd);
                _remoteUpdateEngine.VehicleMessageQueued += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleMessageQueuedDelegate(_remoteUpdateEngine_VehicleMessageQueued);
                _remoteUpdateEngine.VehicleMessageNotQueued += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleMessageNotQueuedDelegate(_remoteUpdateEngine_VehicleMessageNotQueued);
                _remoteUpdateEngine.VehicleSetAtLunchStateRecievd += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleSetAtLunchStateDelegate(_remoteUpdateEngine_VehicleSetAtLunchStateRecievd);
                _remoteUpdateEngine.VehicleUnsetAtLunchStateRecievd += new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleUnsetAtLunchStateDelegate(_remoteUpdateEngine_VehicleUnsetAtLunchStateRecievd);


                titleText = "MTData Transport Listener";
                #region DEBUG options
#if DEBUG
                titleText += " - DEBUG Build";
                bLogExceptions = true;
#else
				bLogExceptions = false;
#endif
                #endregion
                #endregion
                #region Load Concrete Fleet Data
                char[] commas = { ',' };
                string[] fleets = sConcreteFleets.Split(commas);

                List<byte> concrete = new List<byte>();
                foreach (string s in fleets)
                {
                    try
                    {
                        concrete.Add(Convert.ToByte(Convert.ToInt32(s) & 0xFF));
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "Start()", ex);
                    }
                }
                mDBInterface.SetConcreteFleets(concrete);

                fleets = sFleetsToForwardToCommand.Split(commas);

                foreach (string s in fleets)
                {
                    try
                    {
                        mDBInterface.AddCommandStatusFleet(Convert.ToByte(Convert.ToInt32(s) & 0xFF));
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "Start()", ex);
                    }
                }

                fleets = sRestoreStatusFleets.Split(commas);
                foreach (string s in fleets)
                {
                    try
                    {
                        mDBInterface.AddRestoreMDTStatusFleet(Convert.ToByte(Convert.ToInt32(s) & 0xFF));
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "Start()", ex);
                    }
                }
                #endregion
                #region Create mClientRegistry
                if ((_mySqlParams != null) && (_mySqlParams.ConnectionString.Length > 0))
                    _log.Info("Starting Client Updater Thread (with MySQL Interface).");
                else
                    _log.Info("Starting Client Updater Thread.");

                // System for informing interested clients:
                mClientRegistry = new ClientRegistry(sInterfaceBindAddress, iClientNotifyPort, mDBInterface.GetInterfaceServersTable(), _mySqlParams, _userDefinedDateFormat, _serverTime_DateFormat, emailHelper);
                mClientRegistry.eConsoleEvent += new MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.ClientRegistry.WriteToConsoleEvent(mClientRegistry_eConsoleEvent);
                mClientRegistry.bLogPacketData = bLogClientUpdateData;
                mClientRegistry.DatabaseInterface = this.mDBInterface;
                mDBInterface.eDriverPointsCurrentRuleGroupChanged += new PacketDatabaseInterface.DriverPointsCurrentRuleGroupChangedEvent(mClientRegistry.DriverPointsCurrentRuleGroupChanged);
                mClientRegistry.ConcreteFleets = concrete;
                #endregion
                #region Create mStateUpdateThread
                if (iStateUpdatePort != 0)
                {
                    try
                    {
                        _log.Info("Starting Remote State Update Thread (on udp port " + Convert.ToString(iStateUpdatePort) + ").");

                        mStateUpdateThread = new StateUpdateListenerThread(sInterfaceBindAddress,
                            iStateUpdatePort,
                            mDBInterface.Database,
                            mDBInterface.Vehicles,
                            mClientRegistry,
                            _remoteUpdateEngine,
                            _serverTime_DateFormat);

                        mStateUpdateThread.eConsoleEvent += new StateUpdateListenerThread.WriteToConsoleEvent(mStateUpdateThread_eConsoleEvent);
                        titleText += " Polygon Waypoint Support";
                        statusText += ", RSU Port: " + iStateUpdatePort;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "Start()", ex);
                    }
                }
                #endregion
                #region Create mListenerThread
                _log.Info("Starting Mobile Listener Thread on " + ip.ToString());
                // Assign the delegate to decode packets:
                UniversalListenerDelegate myProcessor = new UniversalListenerDelegate(this.ProcessPacketBytes);
                mListenerThread = new UniversalListenerThread(mClient, myProcessor, "Vehicle Listener");
                mListenerThread.eConsoleEvent += new MTData.Transport.Service.Listener.GatewayListener.Network_Threads.UniversalListenerThread.WriteToConsoleEvent(mListenerThread_eConsoleEvent);
                mListenerThread.bLogPacketData = bLogPacketData;
                #endregion
                #region Update the system verision label
                System.Version version = this.GetType().Assembly.GetName().Version;
                titleText += string.Format(" - Ver. {0}.{1} Rev. {2}", version.Major, version.Minor, version.Build);
                _log.InfoFormat("Title : {0}", titleText);
                _log.InfoFormat("Status : {0}", statusText);
                #endregion
                #region schedule tasks
                _scheduleTasksThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ScheduledTaskThread), null);
                _scheduleTasksThread.Start();
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Start()", ex);
                throw new System.Exception(sClassName + "Start()", ex);
            }
            return null;
        }

        /// <summary>
        /// This method will stop all aspects of the Listener.
        /// </summary>
        public void Stop()
        {
            // Kill off the GUI updater thread.
            bProcessPacketThreadActive = false;
            _rxThreadsWaitHandle.Set();
            Thread.Sleep(100);

            try
            {
                if (_scheduleTasksThread != null)
                {
                    _scheduleTasksThread.Stop();
                    _scheduleTasksThread = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }
            
            try
            {
                if (_remoteUpdateEngine != null)
                {
                    _remoteUpdateEngine.VehicleCreated -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleModifiedDelegate(_remoteUpdateEngine_VehicleCreated);
                    _remoteUpdateEngine.VehicleDeleted -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleModifiedDelegate(_remoteUpdateEngine_VehicleDeleted);
                    _remoteUpdateEngine.VehicleLogin -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleLoginDelegate(_remoteUpdateEngine_VehicleLogin);
                    _remoteUpdateEngine.VehicleMessageRecievd -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleMessageRecievdDelegate(_remoteUpdateEngine_VehicleMessageRecievd);
                    _remoteUpdateEngine.VehicleMessageQueued -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleMessageQueuedDelegate(_remoteUpdateEngine_VehicleMessageQueued);
                    _remoteUpdateEngine.VehicleMessageNotQueued -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleMessageNotQueuedDelegate(_remoteUpdateEngine_VehicleMessageNotQueued);
                    _remoteUpdateEngine.VehicleSetAtLunchStateRecievd -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleSetAtLunchStateDelegate(_remoteUpdateEngine_VehicleSetAtLunchStateRecievd);
                    _remoteUpdateEngine.VehicleUnsetAtLunchStateRecievd -= new MTData.Transport.Service.Listener.GatewayListener.RemoteUpdateEngine.VehicleUnsetAtLunchStateDelegate(_remoteUpdateEngine_VehicleUnsetAtLunchStateRecievd);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (mClientRegistry != null)
                {
                    mClientRegistry.eConsoleEvent -= new MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.ClientRegistry.WriteToConsoleEvent(mClientRegistry_eConsoleEvent);
                    mClientRegistry.Dispose(true);
                    mClientRegistry = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (mClient != null)
                {
                    mClient.Close();
                    mClient = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (mListenerThread != null)
                {
                    mListenerThread.StopThread();
                    int timeout = 1000;

                    while ((timeout > 0) && (!mListenerThread.Stopped))
                    {
                        timeout -= 10;
                        Thread.Sleep(10);
                    }
                    if (!mListenerThread.Stopped)
                        mListenerThread.AbortThread();

                    mListenerThread.eConsoleEvent -= new MTData.Transport.Service.Listener.GatewayListener.Network_Threads.UniversalListenerThread.WriteToConsoleEvent(mListenerThread_eConsoleEvent);
                    mListenerThread = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (mDBInterface != null)
                {
                    mDBInterface.eConsoleEvent -= new MTData.Transport.Service.Listener.GatewayListener.PacketDatabaseInterface.WriteToConsoleEvent(mDBInterface_eConsoleEvent);
                    if (ConfigurationManager.AppSettings["ForwardToCommandEnabled"] == "true")
                        mDBInterface.eConcreteStatusChanged -= new MTData.Transport.Service.Listener.GatewayListener.PacketDatabaseInterface.ConcreteStatusChangeEvent(mDBInterface_eConcreteStatusChanged);
                    mDBInterface.Dispose();
                    mDBInterface = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (mStateUpdateThread != null)
                {
                    mStateUpdateThread.Dispose(true);
                    mStateUpdateThread = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (mRxThreads != null)
                {
                    mRxThreads.Clear();
                    mRxThreads = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            try
            {
                if (_routeManagement != null)
                {
                    _routeManagement.Dispose();
                    _routeManagement = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }

            _stopped = true;
        }

        public GatewayProtocolPacket GetViewedPacket()
        {
            try
            {
                if (mRxedPacketCount > 0)
                    return mRxedPackets[mViewedPacket - 1];
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetViewedPacket()", ex);
                throw new System.Exception(sClassName + "GetViewedPacket()", ex);
            }
            return null;
        }

        public void SendGPSHistoryRequest()
        {
            try
            {
                mDBInterface.SendHistoryRequestToUnit(mRxedPackets[mViewedPacket - 1].cFleetId,
                    mRxedPackets[mViewedPacket - 1].iVehicleId);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendGPSHistoryRequest()", ex);
                throw new System.Exception(sClassName + "SendGPSHistoryRequest()", ex);
            }
        }

        public void SendFlush()
        {
            try
            {
                // Send a flush request packet to the viewed unit
                if (mDBInterface.SendFlushRequest(mRxedPackets[mViewedPacket - 1]) == null)
                {
                    _log.InfoFormat("Status : Cannot decode packet {0}", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent flush request to fleet {0} unit {1}",
                        mRxedPackets[mViewedPacket - 1].cFleetId,
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendFlush()", ex);
                throw new System.Exception(sClassName + "SendFlush()", ex);
            }
        }

        public void OutputOn()
        {
            string errString;
            try
            {
                // Send an output enable packet to the viewed unit
                GeneralOutputGPPacket oPacket = new GeneralOutputGPPacket(mRxedPackets[mViewedPacket - 1], _serverTime_DateFormat);
                oPacket.cOutputNumber = 2;
                oPacket.cSequence = 1;
                if ((errString =
                    mDBInterface.SendOutputMessage(oPacket)) != null)
                {
                    _log.InfoFormat("Status : Cannot decode packet {0}", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent output message to unit {0}",
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "OutputOn()", ex);
                throw new System.Exception(sClassName + "OutputOn()", ex);
            }
        }

        public void OutputOff()
        {
            string errString;
            try
            {
                // Send an output disable packet to the viewed unit
                GeneralOutputGPPacket oPacket = new GeneralOutputGPPacket(mRxedPackets[mViewedPacket - 1], _serverTime_DateFormat);
                oPacket.cOutputNumber = 2;
                oPacket.cSequence = 0;
                if ((errString =
                    mDBInterface.SendOutputMessage(oPacket)) != null)
                {
                    _log.InfoFormat("Status : Cannot decode packet ", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent output message to unit ",
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "OutputOff()", ex);
                throw new System.Exception(sClassName + "OutputOff()", ex);
            }
        }

        public void OutputPattern()
        {
            string errString;
            try
            {
                // Send an output PATTERN packet to the viewed unit
                GeneralOutputGPPacket oPacket = new GeneralOutputGPPacket(mRxedPackets[mViewedPacket - 1], _serverTime_DateFormat);
                oPacket.cOutputNumber = 2;
                oPacket.cSequence = 2;
                if ((errString =
                    mDBInterface.SendOutputMessage(oPacket)) != null)
                {
                    _log.InfoFormat("Status : Cannot decode packet {0}", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent output message to unit {0}",
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "OutputPattern()", ex);
                throw new System.Exception(sClassName + "OutputPattern()", ex);
            }
        }

        public void SendSchedule()
        {
            string errString;
            try
            {
                // Send a SCHEDULE packet to the viewed unit
                ConfigScheduleInfoGPPacket sPacket = new ConfigScheduleInfoGPPacket(mRxedPackets[mViewedPacket - 1], _serverTime_DateFormat);
                sPacket.cScheduleNumber = 0x10;
                sPacket.cScheduleVersionMajor = 0x12;
                sPacket.cScheduleVersionMinor = 0x34;

                // Create two profiles - one running config 1, the other 2:
                ConfigScheduleInfoGPPacket.ConfigScheduleHour rc1 =
                    new ConfigScheduleInfoGPPacket.ConfigScheduleHour(11, 0, 0);

                ConfigScheduleInfoGPPacket.ConfigScheduleHour rc2 =
                    new ConfigScheduleInfoGPPacket.ConfigScheduleHour(2, 0, 0);

                ConfigScheduleInfoGPPacket.ConfigScheduleHour rc17 =
                    new ConfigScheduleInfoGPPacket.ConfigScheduleHour(17, 0, 0);


                ConfigScheduleInfoGPPacket.ConfigScheduleHour rc0 =
                    new ConfigScheduleInfoGPPacket.ConfigScheduleHour(0, 0, 0);

                // Now populate the schedule with the different profiles - 

                sPacket.mHourList = new ConfigScheduleInfoGPPacket.ConfigScheduleHour[]
			{
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Sunday AM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Sunday PM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc2,rc1,rc2, // Monday AM
				rc0,rc1,rc17,rc0,rc17,rc2,rc0,rc0,rc0,rc0,rc0,rc0, // Monday AM
				rc1,rc2,rc1,rc0,rc2,rc0,rc1,rc17,rc0,rc17,rc0,rc0, // Tuesday AM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Tuesday PM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Wednesday AM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Wednesday PM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Thursday AM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Thursday AM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Friday PM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Friday AM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0, // Saturday PM
				rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0,rc0  // Saturday AM
			};

                if ((errString =
                    mDBInterface.SendSchedule(sPacket)) != null)
                {
                    _log.InfoFormat("Status : Cannot decode packet {0}", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent Schedule message to unit {0}",
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendSchedule()", ex);
                throw new System.Exception(sClassName + "SendSchedule()", ex);
            }
        }

        public void ResetVehicle()
        {
            try
            {
                // Send a position request packet to the viewed unit
                if (mDBInterface.SendResetRequest(mRxedPackets[mViewedPacket - 1]) == null)
                {
                    _log.InfoFormat("Status : Cannot decode packet {0}", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent reset request to fleet {0} unit {1}",
                        mRxedPackets[mViewedPacket - 1].cFleetId,
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ResetVehicle()", ex);
                throw new System.Exception(sClassName + "ResetVehicle()", ex);
            }
        }

        public void QueueClientUpdate(GatewayProtocolPacket packet)
        {
            try
            {
                // Else pass the packet straight to the live update processing.
                lock (_clientLiveUpdateQueue.SyncRoot)
                    _clientLiveUpdateQueue.Enqueue(packet);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "QueueClientUpdate(GatewayProtocolPacket packet)", ex);
            }
        }

        #region IDisposable Members

        /// <summary>
        /// This method will close up and tidy up all aspects of the listener.
        /// </summary>
        public void Dispose()
        {
            try
            {
                if (oPacketRateTimer != null)
                {
                    oPacketRateTimer.Stop();
                    oPacketRateTimer = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Dispose()", ex);
            }
            try
            {
                if (_dashProcessing != null)
                {
                    if (!_dashProcessing.Stopped)
                        _dashProcessing.Stop();
                    _dashProcessing.Dispose();
                    _dashProcessing = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Dispose()", ex);
            }

            if (_logToMsmq != null)
            {
                _logToMsmq.Dispose();
            }
            _logToMsmq = null;

            oPC = null;
        }

        #endregion

        #region incoming packet processing
        /// <summary>
        /// Process the unparsed packet bytes
        /// </summary>
        /// <param name="ipe"></param>
        /// <param name="contents"></param>
        private void ProcessPacketBytes(IPEndPoint ipe, byte[] contents)
        {
            try
            {
                //received a packet, increase counter
                lock (oRateSyncRoot)
                {
                    _packetsReceived++;
                }
                if (_acceptTrafficFromUnits)
                {
                    #region Accept Unit traffic
                    // Create a new RxThread to do this work:
                    RxThread rxt = new RxThread();
                    rxt.ipe = ipe;
                    rxt.rxContents = contents;
                    lock (mRxThreads.SyncRoot)
                    {
                        // Keep on the most recent packets in the queue (config usually set to 100)
                        int queueCount = mRxThreads.Count;
                        if (queueCount > _udpRxQueue_MaxPacketCount)
                        {
                            //dequeue oldest packet and throw it away
                            mRxThreads.Dequeue();
                            _packetsThrownAway++;
                            queueCount--;
                        }
                        mRxThreads.Enqueue(rxt);
                        _rxThreadsWaitHandle.Set();
                        queueCount++;
                        if (_maxMemoryQueueSize < queueCount)
                        {
                            _maxMemoryQueueSize = queueCount;
                        }
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ProcessPacketBytes(IPEndPoint ipe, byte[] contents)", ex);
                throw new System.Exception(sClassName + "ProcessPacketBytes(IPEndPoint ipe, byte[] contents)", ex);
            }
        }

        private bool bProcessPacketThreadActive = false;

        private System.Threading.AutoResetEvent _rxThreadsWaitHandle = new AutoResetEvent(true);

        private object oRateSyncRoot = new object();
        private int _packetsReceived = 0;
        private int _maxMemoryQueueSize = 0;
        private int _packetsProcessed = 0;
        private int _packetsThrownAway = 0;
        private object _processingSync = new object();
        private long _packetTransactionTime = 0;
        private long _packetProcessingTime = 0;
        private long _packetQueuedTime = 0;
        private long _packetLoggingTime = 0;

        private object PacketRateThread(EnhancedThread sender, object data)
        {
            int iTimeOut = 0;

            int packetsReceived = 0;
            int memoryQueueSize = 0;
            int maxMemoryQueueSize = 0;
            int packetsProcessed = 0;
            int packetsThrownAway = 0;
            int packetsSent = 0;
            int transactionsAdded = 0;
            int currentTransactionQueueSize = 0;
            long packetTransactionTime = 0;
            long packetProcessingTime = 0;
            long packetQueuedTime = 0;
            long packetLoggingTime = 0;

            int routeCount = 0;
            long routeTime = 0;

            ulong liveUpdateBehind = 0;
            List<ulong> liveUpdateCount = null;
            List<long> kinesisliveUpdateCount = null;

            DateTime lastStatsTime = DateTime.Now;
            try
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        iTimeOut = 0;
                        while (!sender.Stopping && iTimeOut < 600)
                        {
                            Thread.Sleep(100);
                            iTimeOut++;
                        }
                        if (!sender.Stopping)
                        {
                            lock (oRateSyncRoot)
                            {
                                packetsReceived = _packetsReceived;
                                _packetsReceived = 0;
                            }
                            if (mRxThreads != null)
                            {
                                lock (mRxThreads.SyncRoot)
                                {
                                    memoryQueueSize = mRxThreads.Count;
                                    maxMemoryQueueSize = _maxMemoryQueueSize;
                                    _maxMemoryQueueSize = 0;
                                    packetsProcessed = _packetsProcessed;
                                    _packetsProcessed = 0;
                                    packetsThrownAway = _packetsThrownAway;
                                    _packetsThrownAway = 0;
                                }
                            }
                            DateTime now = DateTime.Now;
                            TimeSpan span = now - lastStatsTime;
                            lastStatsTime = now;

                            if (mDBInterface != null)
                            {
                                if (mDBInterface.Database != null)
                                {
                                    mDBInterface.Database.GetAndResetUpdateCounters(ref transactionsAdded, ref currentTransactionQueueSize);
                                }
                                if (mDBInterface.Vehicles != null)
                                {
                                    packetsSent = mDBInterface.Vehicles.GetPacketSentCountAndReset;
                                }
                            }
                            if (mClientRegistry != null)
                            {
                                liveUpdateCount = mClientRegistry.LiveUpdateEnqueueCount;
                                kinesisliveUpdateCount = mClientRegistry.LiveUpdateKinesisInsertCount;
                            }
                            if (_routeManagement != null)
                            {
                                _routeManagement.GetStats(ref routeTime, ref routeCount);
                            }
                            lock (_processingSync)
                            {
                                packetTransactionTime = _packetTransactionTime;
                                packetProcessingTime = _packetProcessingTime;
                                packetQueuedTime = _packetQueuedTime;
                                packetLoggingTime = _packetLoggingTime;
                                _packetTransactionTime = 0;
                                _packetProcessingTime = 0;
                                _packetQueuedTime = 0;
                                _packetLoggingTime = 0;
                            }

                            MTData.Common.WMI.MTDataMonitor monitor = MTData.Common.WMI.MTDataMonitor.GetInstance(this._wmiServiceName);
                            if (monitor != null && monitor.Published)
                            {
                                monitor.CurrentMemoryQueueSize = memoryQueueSize;
                                monitor.MaxMemoryQueueSize = maxMemoryQueueSize;
                                monitor.PacketsProcessed = packetsProcessed;
                                monitor.PacketsReceived = packetsReceived;
                                monitor.PacketsSent = packetsSent;
                                monitor.PacketsThrownAway = packetsThrownAway;
                                monitor.TransactionsAddedToMsmq = transactionsAdded;
                                monitor.TransactionsMaxQueueSize = currentTransactionQueueSize;
                            }

                            _statsLog.InfoFormat("Packets Recv : {0} ({5:0.0}/sec), Packets thrown away : {1}, Memory Queue - current {2}, max {3}, Packets Processed {4} ({6:0.0}/sec)", 
                                packetsReceived, packetsThrownAway, memoryQueueSize, maxMemoryQueueSize, packetsProcessed, packetsReceived/ span.TotalSeconds, packetsProcessed/ span.TotalSeconds);
                            _statsLog.InfoFormat("Transactions Queued : {0} ({1:0.0}/sec), Queue count {2}",
                                transactionsAdded, transactionsAdded / span.TotalSeconds, currentTransactionQueueSize);
                            _statsLog.InfoFormat("Processing total time : {0:0.0} ({1:0.0}/packet), Processing time : {2:0.0} ({3:0.0}/packet), logging time : {4:0.0} ({5:0.0}/packet), queued cmds time : {6:0.0} ({7:0.0}/packet)", 
                                packetTransactionTime / 10000f, packetTransactionTime / packetsProcessed / 10000f,
                                packetProcessingTime / 10000f, packetProcessingTime / packetsProcessed / 10000f,
                                packetLoggingTime / 10000f, packetLoggingTime / packetsProcessed / 10000f,
                                packetQueuedTime / 10000f, packetQueuedTime / packetsProcessed / 10000f);
                            _statsLog.InfoFormat("Route Processing count : {0}, route time {1:0.0} ({2:0.0}/packet)",
                                routeCount, routeTime / 10000f, routeTime / routeCount / 10000f);
                            _statsLog.InfoFormat("Packets Sent : {0} ({1:0.0}/sec)", packetsSent, packetsSent / span.TotalSeconds);

                            if (liveUpdateCount != null && liveUpdateCount.Count > 0)
                            {
                                StringBuilder str = new StringBuilder();
                                str.AppendFormat("Live update async queued {0}, DeQueued {1}", liveUpdateCount[0], liveUpdateCount[1]);
                                liveUpdateBehind += (liveUpdateCount[0] - liveUpdateCount[1]);
                                str.AppendFormat(", behind {0}, added to MSMQ ", liveUpdateBehind);
                                for (int i=2; i<liveUpdateCount.Count; i++)
                                {
                                    if (i < liveUpdateCount.Count - 1)
                                    {
                                        str.AppendFormat("{0}, ", liveUpdateCount[i]);
                                    }
                                    else
                                    {
                                        str.AppendFormat("{0}", liveUpdateCount[i]);
                                    }
                                }
                                _statsLog.Info(str.ToString());
                            }
                            if (kinesisliveUpdateCount != null && kinesisliveUpdateCount.Count > 0)
                            {
                                StringBuilder str = new StringBuilder();
                                str.AppendFormat("Live updates Kinesis inserted {0}, added to streams ", kinesisliveUpdateCount[0]);
                                for (int i = 1; i < kinesisliveUpdateCount.Count; i++)
                                {
                                    if (i < kinesisliveUpdateCount.Count - 1)
                                    {
                                        str.AppendFormat("{0}, ", kinesisliveUpdateCount[i]);
                                    }
                                    else
                                    {
                                        str.AppendFormat("{0}", kinesisliveUpdateCount[i]);
                                    }
                                }
                                _statsLog.Info(str.ToString());
                            }
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _statsLog.Error("Error processing stats)", ex);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _statsLog.Error("Error in processing stats thread)", ex);
            }
            return null;
        }
        private void ProcessPacket()
        {
            string errMsg;
            bool bProcessError = false;
            int newPacketNum;
            int iDataCount = 0;
            RxThread rxt;
            UDPPacket latestUdpPacket = null;
            GatewayProtocolPacket latestPacket = null;
            long startTransactionTime = 0;
            long startProcessTime = 0;
            long startLoggingTime = 0;
            long startQueuedTime = 0;


            _log.Info("Starting GUI Updater Thread.");
            while (bProcessPacketThreadActive && mRxThreads != null)
            {
                CheckAndClearList();
                bProcessError = false;
                try
                {
                    // Fetch the latest data to decode:
                    lock (mRxThreads.SyncRoot)
                    {
                        iDataCount = mRxThreads.Count;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "ProcessPacket()", ex);
                }
                try
                {

                    if (iDataCount > 0)
                    {
                        startTransactionTime = DateTime.Now.Ticks;

                        if (bLogPacketData)
                            _log.Info("ProcessPacket : Start Next Packet - " + DateTime.Now.ToString("HH:mm:ss.fff"));

                        #region Process the next in the queue.
                        try
                        {
                            lock (mRxThreads.SyncRoot)
                            {
                                rxt = (RxThread)mRxThreads.Dequeue();
                                _packetsProcessed++;
                            }
                        }
                        catch (System.Exception ex)
                        {
                            rxt = null;
                            _log.Error(sClassName + "ProcessPacket()", ex);
                        }

                        if (rxt != null)
                        {
                            if (bLogPacketData)
                            {
                                _log.Info("ProcessPacket : Decode Packet");
                            }
                            try
                            {
                                //	// And make a UDP packet out of it:
                                latestUdpPacket = new UDPPacket(_serverTime_DateFormat);
                                latestUdpPacket.mSenderIP = rxt.ipe;
                                errMsg = latestUdpPacket.Decode(rxt.rxContents);
                                if (errMsg != null)
                                {
                                    bProcessError = true;
                                    _log.InfoFormat("Status : {0}", errMsg);

                                    //	Register the invalid packet with the history recorder.
                                    InvalidPacketHistoryEntry invalidEntry = _invalidPacketHistory.RegisterInvalidPacket(latestUdpPacket.mSenderIP, rxt.rxContents);

                                    _log.Info(string.Format("Vehicle IP {0} : Last Known Fleet {1}, Vehicle {2} : {3} : Data : {4}",
                                            rxt.ipe.ToString(),
                                            invalidEntry.FleetID,
                                            invalidEntry.VehicleID,
                                            errMsg,
                                            Util.EncodeByteArrayAsHex(rxt.rxContents)));

                                }
                            }
                            catch (System.Exception ex)
                            {
                                _log.Error(sClassName + "ProcessPacket()", ex);
                            }
                            if (!bProcessError)
                            {
                                if (bLogPacketData)
                                    _log.Info("ProcessPacket : Process Packet through DBInterface");

                                if (latestUdpPacket.mNumPackets > 0)
                                {
                                    #region Update database and clients
                                    // Add the packet to our collection
                                    for (newPacketNum = 0; newPacketNum < latestUdpPacket.mNumPackets; newPacketNum++)
                                    {
                                        #region Update the database
                                        try
                                        {
                                            // Roll the packet count if needed:
                                            if (mRxedPacketCount >= ACTIVITYLIST_MAXLENGTH)
                                            {
                                                Array.Clear(mRxedPackets, 0, mRxedPacketCount);
                                                mRxedPacketCount = 0;
                                            }

                                            if (mRxedPacketCount >= mRxedPackets.Length || mRxedPacketCount < 0)
                                                mRxedPacketCount = 0;

                                            mRxedPackets[mRxedPacketCount++] = latestUdpPacket.mPackets[newPacketNum];

                                            // For convenience, access the latest packet with a handy name:
                                            latestPacket = mRxedPackets[(mRxedPacketCount - 1)];

                                            //	Register valid packets with the invalid packet history recorder
                                            _invalidPacketHistory.RegisterValidPacket(latestUdpPacket.mSenderIP,
                                                                                      latestPacket.cFleetId, latestPacket.iVehicleId, latestPacket.cOurSequence, latestPacket.cAckSequence);

                                            if (_dashProcessing != null)
                                                _dashProcessing.UpdateRecvdFromFleet((int)latestPacket.cFleetId);

                                            // Deal with the message
                                            startProcessTime = DateTime.Now.Ticks;
                                            object processResult = mDBInterface.ProcessData(latestPacket, currentDownloads, obDownloads);
                                            lock (_processingSync)
                                            {
                                                _packetProcessingTime += DateTime.Now.Ticks - startProcessTime;
                                            }

                                            startLoggingTime = DateTime.Now.Ticks;
                                            if (processResult != null)
                                            {
                                                if (processResult is String)
                                                {
                                                    string p = processResult as string;
                                                    if (p.StartsWith("Ignore Packet"))
                                                    {
                                                        _ignorePacketsLog.Info(p);
                                                    }
                                                    else if (latestPacket.cMsgType == GeneralGPPacket.GEN_CONFIG_DOWNLOADED ||
                                                        latestPacket.cMsgType == ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE)
                                                    {
                                                        _log.Info(string.Format("Fleet {0} Vehicle {1} : {2}",
                                                                                                  latestPacket.cFleetId,
                                                                                                  latestPacket.iVehicleId,
                                                                                                  (string)processResult));
                                                    }
                                                    else
                                                    {
                                                        _log.InfoFormat("Status : {0}", processResult);
                                                        _log.Info(string.Format("Fleet {0} Vehicle {1} : {2} : Data : {3}",
                                                                                                  latestPacket.cFleetId,
                                                                                                  latestPacket.iVehicleId,
                                                                                                  (string)processResult,
                                                                                                  Util.EncodeByteArrayAsHex(latestPacket.mRawBytes)));
                                                    }
                                                }
                                                else
                                                {
                                                    // Else pass the packet straight to the live update processing.
                                                    lock (_clientLiveUpdateQueue.SyncRoot)
                                                        _clientLiveUpdateQueue.Enqueue(latestPacket);
                                                }
                                            }
                                            else
                                            {
                                                // Else pass the packet straight to the live update processing.
                                                lock (_clientLiveUpdateQueue.SyncRoot)
                                                    _clientLiveUpdateQueue.Enqueue(latestPacket);
                                            }
                                            if (latestPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || latestPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || latestPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                            {
                                                switch (latestPacket.cMsgType)
                                                {
                                                    case GeneralGPPacket.PENDENT_ALARM:
                                                        _log.Info("Finished processing Pendant alarm packet");
                                                        break;
                                                    case GeneralGPPacket.PENDENT_ALARM_TEST:
                                                        _log.Info("Finished processing Pendant test alarm packet");
                                                        break;
                                                    case GeneralGPPacket.PENDENT_ALARM_CLEARED:
                                                        _log.Info("Finished processing Pendant alarm cleared packet");
                                                        break;
                                                }
                                            }

                                            //add the listener message to message queue
                                            ListenerLogItem logItem = new ListenerLogItem();
                                            logItem.FleetID = Convert.ToInt32(latestPacket.cFleetId);
                                            logItem.VehicleID = Convert.ToInt32(latestPacket.iVehicleId);
                                            logItem.ListenerMessage = System.BitConverter.ToString(latestPacket.mRawBytes);
                                            logItem.Direction = LogDirection.InBound;
                                            logItem.MessageType = GatewayProtocolLookup.GetMessageType(latestPacket.cMsgType);
                                            logItem.IPAddress = latestPacket.mSenderIP.ToString();
                                            logItem.LogDate = DateTime.UtcNow;
                                            if (_logToMsmq != null)
                                            {
                                                _logToMsmq.InsertIntoQueuesAsync(logItem);
                                            }
                                            if (_logListenerMessage)
                                            {
                                                mDBInterface.InsertListenerMessage(logItem);
                                            }
                                            lock (_processingSync)
                                            {
                                                _packetLoggingTime += DateTime.Now.Ticks - startLoggingTime;
                                            }
                                            startQueuedTime = DateTime.Now.Ticks;

                                            if (latestPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN || latestPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT)
                                            {
                                                //send a cmd to tell API to reload its driver/vehicle table
                                                int fleetId = (int)latestPacket.cFleetId;
                                                oPC.DriverChanged(new PacketCreaterContext(fleetId, "Driver Changed"), fleetId, (int)latestPacket.iVehicleId);
                                            }

                                            // Check for an accident event and check to see how many times this unit has sent in
                                            // against the configuration settings
                                            bool sendGForceCalib = false;

                                            if (latestPacket.cMsgType == GeneralGPPacket.STATUS_ACCIDENT_NEW)
                                            {
                                                if (AddEventToCounter(accidentEventCounter, (int)latestPacket.cFleetId, (int)latestPacket.iVehicleId, listenerAccidentThreshold))
                                                {
                                                    sendGForceCalib = true;
                                                }
                                            }
                                            else if (latestPacket.cMsgType == GeneralGPPacket.ENGINE_G_FORCES_HIGH)
                                            {
                                                if (AddEventToCounter(gforceEventCounter, (int)latestPacket.cFleetId, (int)latestPacket.iVehicleId, listenerGForceThreshold))
                                                {
                                                    sendGForceCalib = true;
                                                }
                                            }
                                            else if(latestPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION)
                                            {
                                                // Check the Calibration packect and see if this is a complete
                                                GeneralGPPacket gpPacket = new GeneralGPPacket(latestPacket, _serverTime_DateFormat);

                                                /// Now lets look for a complete so we can remove this unit from the list if we sent it a calibration request
                                                if (gpPacket.cInputNumber == 0x02)
                                                {
                                                    RemoveUnitFromEventCounter(gforceCalibrationEventCounter, (int)latestPacket.cFleetId, (int)latestPacket.iVehicleId);
                                                }
                                            }

                                            if ((sendGForceCalib && listenerUserID != 0) && (UnitNotInCalibrationList((int)latestPacket.cFleetId, (int)latestPacket.iVehicleId)))
                                            {

                                                // As we are sending out a calibration, the unit needs to be removed from both lists
                                                RemoveUnitFromEventCounter(accidentEventCounter, (int)latestPacket.cFleetId, (int)latestPacket.iVehicleId);
                                                RemoveUnitFromEventCounter(gforceEventCounter, (int)latestPacket.cFleetId, (int)latestPacket.iVehicleId);

                                                AddUnitToCalibrationList((int)latestPacket.cFleetId, (int)latestPacket.iVehicleId);
                                                // We will queue a GForce for sending, any new messages that trigger this will not be added as
                                                // the listener will already have this queued for the unit
                                                GatewayServiceBL.Classes.QueuedMessage qm = new GatewayServiceBL.Classes.QueuedMessage();
                                                qm.FleetId = (int)latestPacket.cFleetId;
                                                qm.VehicleId = (int)latestPacket.iVehicleId;
                                                qm.StartDate = DateTime.Now.ToUniversalTime();
                                                qm.ExpiryDate = DateTime.Now.AddDays(1).ToUniversalTime();
                                                qm.CreatedDate = DateTime.Now.ToUniversalTime();
                                                qm.ExtraData = "Forced";
                                                qm.MessageTypeId = ListenerMessageTypes.GForceAutoCalibrate;

                                                MTData.Common.Crypto.TextEncryption encryptor = new Common.Crypto.TextEncryption();

                                                GatewayServiceBL.Credentials creds = new GatewayServiceBL.Credentials();
                                                creds.UserID = listenerUserID;
                                                creds.Username = listenerUser;
                                                creds.Password = encryptor.Encrypt(encryptor.EncryptBase64(listenerPassword));

                                                try
                                                {
                                                    MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.SendMessageToQueue(qm, creds);
                                                }
                                                catch (Exception ex)
                                                {
                                                    _log.Error("Cannot Queue messsage configured user is invalid: User, " + listenerUser);
                                                }
                                            }

                                            // Check for a status packet if it is then check to see if we have any Messages or Downloads for this vehicle
                                            // Only start a new message as a response to a status packet.
                                            if (latestPacket.cMsgType == GeneralGPPacket.STATUS_REPORT)
                                            {
                                                try
                                                {
                                                    ProcessQueuedMessage(latestPacket);
                                                }
                                                catch (Exception ex)
                                                {
                                                    _log.Error(sClassName + "ProcessPacket.ProcessQueuedMessage()", ex);
                                                }

                                                try
                                                {
                                                    // Check to see if the server is capable of sending to this unit
                                                    if (currentDownloads.Count < ServerDownloadMax())
                                                    {
                                                        if (currentDownloads.FindAll(dl => dl.FleetID == latestPacket.cFleetId).Count < ServerFleetDownloadMax(latestPacket.cFleetId))
                                                        {
                                                            ProcessQueuedDownload(latestPacket);
                                                        }
                                                    }
                                                }
                                                catch (Exception ex)
                                                {
                                                    _log.Error(sClassName + "ProcessPacket.ProcessQueuedDownload()", ex);
                                                }
                                            }

                                            if (bLogPacketData)
                                                _log.Info("ProcessPacket : Update GUI Display");


                                            lock (_processingSync)
                                            {
                                                _packetQueuedTime += DateTime.Now.Ticks - startQueuedTime;
                                            }

                                            if (bLogPacketData)
                                                _log.Info("ProcessPacket : Packet Complete");
                                        }
                                        catch (System.Exception ex)
                                        {
                                            _log.InfoFormat("Status : {0}", ex.Message);
                                            _log.Error(sClassName + "ProcessPacket()", ex);
                                        }
                                        #endregion
                                    }
                                    #endregion
                                }
                            }
                        }

                        lock (_processingSync)
                        {
                            _packetTransactionTime += DateTime.Now.Ticks - startTransactionTime;
                        }

                        #endregion

                        if (bLogPacketData)
                            _log.Info("ProcessPacket : End Next Packet - " + DateTime.Now.ToString("HH:mm:ss.fff"));
                    }
                    else
                        _rxThreadsWaitHandle.WaitOne();
                }
                catch (Exception exMain)
                {
                    _log.Error(sClassName + "ProcessPacket()", exMain);
                }
            }
            _log.Info("Stopping GUI Updater Thread.");
            //try
            //{
            //    if (ts.)
            //    {
            //        ts.Abort();
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    _log.Error(sClassName + "ProcessPacket()", ex);
            //}
            _log.Info("Stopped Client Live Update Thread.");

        }

        /// <summary>
        /// Checks to see if the amount of time since we last cleared the list is greater than 1 day and if so clears the list
        /// </summary>
        private void CheckAndClearList()
        {
            // Check to see if the amount of days is greater than one then clear lists if it is and set the lastCleardDate
            if (DateTime.Now.Subtract(lastClearedTime).Days > 1)
            {
                accidentEventCounter.Clear();
                gforceEventCounter.Clear();
                gforceCalibrationEventCounter.Clear();
                lastClearedTime = DateTime.Now;
            }
        }

        private Boolean UnitNotInCalibrationList(int fleetId, int vehicleId)
        {
            UnitEventCounter unit = gforceCalibrationEventCounter.Find(uec => uec.FleetID == fleetId && uec.VehicleID == vehicleId);

            return unit == null;
        }

        private void AddUnitToCalibrationList(int fleetId, int vehicleId)
        {
            UnitEventCounter unit = gforceCalibrationEventCounter.Find(uec => uec.FleetID == fleetId && uec.VehicleID == vehicleId);

            if (unit == null)
            {
                unit = new UnitEventCounter(fleetId, vehicleId);
                gforceCalibrationEventCounter.Add(unit);
            }
        }

        private void RemoveUnitFromEventCounter(List<UnitEventCounter> list, int fleetId, int vehicleId)
        {
            UnitEventCounter unit = list.Find(uec => uec.FleetID == fleetId && uec.VehicleID == vehicleId);

            if (unit != null)
            {
                // Clear this one out of the list as we have sent a GForce Calib
                list.Remove(unit);
            }
        }

        private bool AddEventToCounter(List<UnitEventCounter> list, int fleetId, int vehicleId, int threshold)
        {
            bool retVal = false;

            UnitEventCounter unit = list.Find(uec => uec.FleetID == fleetId && uec.VehicleID == vehicleId);

            if (unit != null)
            {
                unit.AddEventCount();
            }
            else
            {
                unit = new UnitEventCounter(fleetId, vehicleId);
                list.Add(unit);
            }

            if (unit.GetEventTotal >= threshold)
            {
                //// Clear this one out of the list as we are going to send a GForce Calib
                //list.Remove(unit);
                retVal = true;
            }

            return retVal;
        }

        private int ServerDownloadMax()
        {
            return this._maxServerDownloads;
        }

        private int ServerFleetDownloadMax(int fleetID)
        {
            int tmpVal = this._fleetDownloadDefault;

            if (this._fleetDownloadMax.TryGetValue(fleetID, out tmpVal))
            {
                return tmpVal;
            }
            else
            {
                return this._fleetDownloadDefault;
            }
        }

        private void CheckForValidOBDownloadList()
        {
            int step = 0;
            try
            {
                lock (obDownloadCheck)
                {
                    if (obDownloads == null || DateTime.Now.Subtract(_lastDownloadCheck).TotalMinutes > 1)
                    {
                        if (obDownloads == null)
                        {
                            obDownloads = MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedDownloads(false, true);
                        }
                        else
                        {
                            step = 1;
                            List<GatewayServiceBL.Classes.QueuedDownload> tmpList =
                                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedDownloads(false, true);

                            // Now loop through each of the ones in the current list if they are no longer in the tmpList then send the abort.
                            // As we cannot remove them from the list just build a quick list then loop and remove
                            List<GatewayServiceBL.Classes.QueuedDownload> obRemoveDownloads = new List<GatewayServiceBL.Classes.QueuedDownload>();

                            foreach (GatewayServiceBL.Classes.QueuedDownload currentQD in obDownloads)
                            {
                                _log.Debug(String.Format("Checking Current Download for removal F:{0}, V:{1}", currentQD.FleetID, currentQD.VehicleId));
                                step = 2;
                                if (tmpList.FindAll(download => download.FleetID == currentQD.FleetID && download.VehicleId == currentQD.VehicleId)
                                    .OrderBy(download => download.ID).ToList().Count == 0)
                                {
                                    _log.Debug("Checking to see if download in progress");
                                    step = 3;
                                    // Check to see if the download has actually been started if not just remove if from the list
                                    if (currentDownloads.FindAll(rdl => rdl.FleetID == currentQD.FleetID && rdl.VehicleID == currentQD.VehicleId).Count == 0)
                                    {
                                        _log.Debug("Download not in progress");
                                        step = 4;
                                        _log.Debug(String.Format("Adding F:{0}, V:{1} to remove", currentQD.FleetID, currentQD.VehicleId));
                                        obRemoveDownloads.Add(currentQD);
                                    }
                                    //else
                                    //{
                                    //    _log.Debug("Download in progress removing from list");
                                    //    step = 5;
                                    //    // Set the entry to abort so that the downloadProcessor Abort event picks it up
                                    //    DownloadEntry de = currentDownloads.FindAll(download => download.FleetID == currentQD.FleetID
                                    //        && download.VehicleID == currentQD.VehicleId)[0];

                                    //    de.AbortDownload = true;

                                    //    _log.Debug("Removing from download list");
                                    //    _log.Debug("Current Download Count before remove:" + currentDownloads.Count.ToString());
                                    //    currentDownloads.Remove(de);
                                    //    _log.Debug("Current Download Count:" + currentDownloads.Count.ToString());
                                    //}
                                }
                                else
                                {
                                    _log.Debug("Vehicle Not Found");
                                }
                            }

                            // Now remove them from the obDownloads
                            foreach (GatewayServiceBL.Classes.QueuedDownload d in obRemoveDownloads)
                            {
                                _log.Debug("Removing Units");
                                obDownloads.Remove(d);
                            }

                            step = 6;
                            // Next add any new ones into the current list.
                            foreach (GatewayServiceBL.Classes.QueuedDownload dl in tmpList)
                            {
                                step = 7;
                                if (obDownloads.FindAll(download => download.FleetID == dl.FleetID && download.VehicleId == dl.VehicleId).Count == 0)
                                {
                                    step = 8;
                                    obDownloads.Add(dl);
                                }
                            }
                        }

                        _log.Info("Loading Queued Downloads : " + obDownloads.Count.ToString() + " units in list");
                        _log.Debug(String.Format("Current downloads count:{0}", currentDownloads.Count));

                        foreach (GatewayServiceBL.Classes.QueuedDownload qd in obDownloads)
                        {
                            _log.Debug("Download for F:" + qd.FleetID.ToString() + ", V:" + qd.VehicleId.ToString());
                        }

                        _lastDownloadCheck = DateTime.Now;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error("CheckForValidOBDownloadList():Step:" + step.ToString(), ex);
            }
        }

        private void CheckForValidOBMessageList()
        {
            lock (obMessCheck)
            {
                if (obMess == null || DateTime.Now.Subtract(_lastQueuedMessageCheck).TotalMinutes > 1)
                {
                    obMess = MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedMessages(0, 0, DateTime.MinValue, 
                        DateTime.MinValue, 0, true);

                    _log.Info("Loading Queued Messages : " + obMess.Count.ToString() + " in list");
                    _lastQueuedMessageCheck = DateTime.Now;
                }
            }
        }

        private void ProcessQueuedDownload(GatewayProtocolPacket packet)
        {
            try
            {
                CheckForValidOBDownloadList();

                lock (obDownloadCheck)
                {
                    _log.Debug("Checking Downloads for Fleet: " + packet.cFleetId.ToString() + ", V: " + packet.iVehicleId.ToString());
                    if (currentDownloads.FindAll(cdl => cdl.FleetID == packet.cFleetId && cdl.VehicleID == packet.iVehicleId).Count == 0)
                    {
                        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> unitDownloads
                            = obDownloads.FindAll(download => download.FleetID == packet.cFleetId && download.VehicleId == packet.iVehicleId).OrderBy(download => download.ID).ToList();

                        if (unitDownloads.Count > 0)
                        {
                            _log.Debug("Downloading file to unit, F: " + packet.cFleetId.ToString() + ", V:" + packet.iVehicleId.ToString());

                            MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qd = unitDownloads[0];
                            //    // We will only do one at a time and we also need to make sure that we check to see if we are waiting for a unit to log back in first.
                            //    // And if we are waiting we need to only mark the download as complete once the SentSegment and TotalSegment are equal and still active
                            Common.Threading.EnhancedThread et = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(StartDownload), new List<object> { packet, qd });

                            et.Start();

                        }
                        else
                        {
                            _log.Debug("No Downloads Queued for Fleet: " + packet.cFleetId.ToString() + ", V: " + packet.iVehicleId.ToString());
                        }
                    }
                    else
                    {
                        _log.Debug("Currently downloading to Fleet: " + packet.cFleetId.ToString() + ", V: " + packet.iVehicleId.ToString());
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "ProcessQueuedDownload()", ex);
            }
        }

        private object StartDownload(EnhancedThread sender, object data)
        {
            try
            {
                List<object> baseData = (List<object>)data;

                GatewayProtocolPacket gp = (GatewayProtocolPacket)baseData[0];
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qd =
                    (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload)baseData[1];

                DownloadProcessor dp = new DownloadProcessor(gp, mDBInterface, qd, _serverTime_DateFormat, this.packetAbortTimeout);


                dp.DownloadStart += DownloadStartEvent;
                dp.DownloadComplete += DownloadCompleteEvent;
                dp.FireAbortEvent += FireAbortEvent;

                dp.BeginDownload(6, qd);

                return null;
            }
            catch (Exception ex)
            {
                _log.Error("StartDownload, caused " + ex.Message);
            }

            return null;
        }

        void FireAbortEvent(object sender, int fleetID, int vehicleID)
        {
            _log.Debug(string.Format("FireAbortEvent:Attempting to remove download: F:{0}, V:{1}", fleetID, vehicleID));

            _log.Debug(String.Format("FireAbortEvent:Current downloads before removal count:{0}", currentDownloads.Count));
            currentDownloads.Remove(currentDownloads.Find(cdl => cdl.FleetID == fleetID && cdl.VehicleID == vehicleID));
            _log.Debug(String.Format("FireAbortEvent:Current downloads count:{0}", currentDownloads.Count));

            _log.Debug(String.Format("FireAbortEvent:Queued downloads before removal count:{0}", obDownloads.Count));
            lock (obDownloadCheck)
            {

                foreach (GatewayServiceBL.Classes.QueuedDownload qd in obDownloads)
                {
                    if (qd.FleetID == fleetID && qd.VehicleId == vehicleID)
                    {
                        GatewayServiceBL.Classes.Manager.AbortDownloadInQueue(qd, null);
                        obDownloads.Remove(qd);
                        break;
                    }
                }
            }
            _log.Debug(String.Format("FireAbortEvent:Queued downloads count:{0}", obDownloads.Count));
        }

        void DownloadCompleteEvent(int queuedDownloadID)
        {
            // Remove this one from the lists as it has finished
            try
            {
                _log.Debug(string.Format("DownloadCompleteEvent:Attempting to remove download: {0}", queuedDownloadID));

                _log.Debug(String.Format("DownloadCompleteEvent:Current downloads before removal count:{0}", currentDownloads.Count));
                currentDownloads.Remove(currentDownloads.Find(cdl => cdl.ID == queuedDownloadID));
                _log.Debug(String.Format("DownloadCompleteEvent:Current downloads count:{0}", currentDownloads.Count));

                _log.Debug(String.Format("DownloadCompleteEvent:Queued downloads before removal count:{0}", obDownloads.Count));
                lock (obDownloadCheck)
                {
                    foreach (GatewayServiceBL.Classes.QueuedDownload qd in obDownloads)
                    {
                        if (qd.ID == queuedDownloadID)
                        {
                            obDownloads.Remove(qd);
                            break;
                        }
                    }
                }
                _log.Debug(String.Format("DownloadCompleteEvent:Queued downloads count:{0}", obDownloads.Count));
            }
            catch (Exception ex)
            {
                _log.Error("DownloadCompleteEvent: " + queuedDownloadID.ToString(), ex);
            }
        }

        void DownloadStartEvent(int queuedDownloadID, int fleetID, int vehicleID)
        {
            // Add this one to the list, we need a way to see what
            try
            {
                currentDownloads.Add(new DownloadEntry(queuedDownloadID, fleetID, vehicleID));
            }
            catch (Exception ex)
            {
                _log.Error("DownloadStartEvent: " + queuedDownloadID.ToString(), ex);
            }
        }

        private void ProcessQueuedMessage(GatewayProtocolPacket packet)
        {
            CheckForValidOBMessageList();

            lock (obMessCheck)
            {
                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> unitMesages
                    = obMess.FindAll(mess => mess.FleetId == packet.cFleetId && mess.VehicleId == packet.iVehicleId).OrderBy(mess => mess.Sequence).ToList();

                foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage qm in unitMesages)
                {
                    GatewayProtocolPacket retVal = null;

                    // Update the status of the message to "InProgress"
                    _log.Info(String.Format("Sending {0} Message To Unit {1} / {2}", qm.MessageTypeId, Convert.ToInt32(packet.cFleetId).ToString(), packet.iVehicleId.ToString()));
                    MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.UpdateMessageStatus(qm, GatewayServiceBL.Classes.MessageStatus.InProgress);

                    switch (qm.MessageTypeId)
                    {
                        case ListenerMessageTypes.ChangeIPAddress:
                            try
                            {
                                ConfigNetworkInfoGPPacket netPacket = new ConfigNetworkInfoGPPacket(packet, "");
                                netPacket.cNewFleetId = 0;
                                netPacket.iNewUnitId = 0;
                                netPacket.cNetwork = 1;

                                if (qm.ExtraDataAsIP.UseHostName)
                                {
                                    netPacket.ServerHostName = qm.ExtraDataAsIP.HostIP;
                                }
                                else
                                {
                                    netPacket.mPrimaryServerIPAddress = System.Net.IPAddress.Parse(qm.ExtraDataAsIP.HostIP);
                                }

                                netPacket.iPrimaryServerRxPort = Convert.ToInt16(qm.ExtraDataAsIP.IPPort);

                                if (qm.ExtraDataAsIP.SecIP.Length == 0)
                                {
                                    netPacket.mSecondaryServerIPAddress = System.Net.IPAddress.Parse("0.0.0.0");
                                    netPacket.iSecondaryServerRxPort = 0;
                                }
                                else
                                {
                                    netPacket.mSecondaryServerIPAddress = System.Net.IPAddress.Parse(qm.ExtraDataAsIP.SecIP);

                                    netPacket.iSecondaryServerRxPort = Convert.ToInt16(qm.ExtraDataAsIP.SecIPPort);
                                }

                                if (qm.ExtraDataAsIP.PingIP.Length > 0)
                                {
                                    netPacket.mPingableIPAddress = System.Net.IPAddress.Parse(qm.ExtraDataAsIP.PingIP);
                                }
                                else
                                {
                                    netPacket.mPingableIPAddress = System.Net.IPAddress.Parse("173.194.38.152");
                                }

                                netPacket.cMaxPingRetries = Convert.ToByte(qm.ExtraDataAsIP.PingRetries);
                                netPacket.cPort1BaudRate = 0;

                                mDBInterface.SendNetworkInfo(netPacket);

                                retVal = netPacket;

                                //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Change IP Address Packet.");
                                _log.Info("Change of IP Address Sent to Unit " + qm.FleetId + "/" + qm.VehicleId
                                    + " : New IP = " + qm.ExtraDataAsIP.HostIP + ":" + qm.ExtraDataAsIP.NewIPPort + ", Ping Address = "
                                    + qm.ExtraDataAsIP.PingIP.ToString() + ", Ping Retries = " + qm.ExtraDataAsIP.PingRetries);
                            }
                            catch (System.Exception exNetPacket)
                            {
                                _log.Error(sClassName + "ProcessQueuedMessage()", exNetPacket);
                            }

                            break;
                        case ListenerMessageTypes.Flush:     // Flush
                            retVal = mDBInterface.SendFlushRequest(packet);
                            //UpdateStatus(sFleetID, sVehicleID, "Flush Sent To Unit.");
                            _log.Info("Send Flush to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.FlushThenReset:     // Flush Then Reset
                            retVal = mDBInterface.SendFlushRequest(packet);

                            if (retVal != null)
                            {
                                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.StorePacket(qm, retVal.ToString());
                            }

                            retVal = null;

                            Thread.Sleep(1000);
                            retVal = mDBInterface.SendResetRequest(packet);
                            //UpdateStatus(sFleetID, sVehicleID, "Flush and Reset Sent To Unit.");
                            _log.Info("Send Flush and Reset to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.GForceAutoCalibrate:     // G-Force Auto Calibrate
                            bool auto = false;
                            if (qm.ExtraData == "Forced")
                            {
                                auto = true;
                            }
                            retVal = mDBInterface.SendGForceAutoCalibrateRequest(null, packet, auto);
                            //UpdateStatus(sFleetID, sVehicleID, "G-Force Auto Calibrate To Unit.");
                            _log.Info("Sent G-Force Auto Calibrate to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.Reset:     // Reset
                            retVal = mDBInterface.SendResetRequest(packet);
                            //UpdateStatus(sFleetID, sVehicleID, "Reset Sent To Unit.");
                            _log.Info("Send Reset to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.Output1On:     // Output 1 On
                            retVal = mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 1, true);
                            _log.Info("Sent Output 1 On to Unit " + qm.FleetId + "/" + qm.VehicleId);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 1 On Packet.");

                            break;
                        case ListenerMessageTypes.Output1Off:     // Output 1 Off
                            retVal = mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 1, false);
                            _log.Info("Sent Output 1 Off to Unit " + qm.FleetId + "/" + qm.VehicleId);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 1 Off Packet.");

                            break;
                        case ListenerMessageTypes.Output2On:     // Output 2 On
                            retVal = mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 2, true);
                            _log.Info("Sent Output 2 On to Unit " + qm.FleetId + "/" + qm.VehicleId);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 1 On Packet.");

                            break;
                        case ListenerMessageTypes.Output2Off:     // Output 2 Off
                            retVal = mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 2, false);
                            _log.Info("Sent Output 2 Off to Unit " + qm.FleetId + "/" + qm.VehicleId);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 1 Off Packet.");

                            break;
                        case ListenerMessageTypes.AccidentBuffer1:    // Accident Buffer 1
                            //	Accidents are actually held as 0, 2, 4, 6..
                            retVal = mDBInterface.SendAccidentUploadRequest(packet, 0);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent To Unit.");
                            _log.Info("Sent Accident Buffer " + 1 + " to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.AccidentBuffer2:    // Accident Buffer 2
                            //	Accidents are actually held as 0, 2, 4, 6..
                            retVal = mDBInterface.SendAccidentUploadRequest(packet, 2);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent To Unit.");
                            _log.Info("Sent Accident Buffer " + 2 + " to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.AccidentBuffer3:    // Accident Buffer 3
                            //	Accidents are actually held as 0, 2, 4, 6..
                            retVal = mDBInterface.SendAccidentUploadRequest(packet, 4);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent To Unit.");
                            _log.Info("Sent Accident Buffer " + 3 + " to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        case ListenerMessageTypes.AccidentBuffer4:    // Accident Buffer 4
                            //	Accidents are actually held as 0, 2, 4, 6..
                            retVal = mDBInterface.SendAccidentUploadRequest(packet, 6);
                            //UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent To Unit.");
                            _log.Info("Sent Accident Buffer " + 4 + " to Unit " + qm.FleetId + "/" + qm.VehicleId);

                            break;
                        default:
                            _log.Error(String.Format("Unknown MessageID {0}, F:{1},V:{2}", qm.MessageTypeId, qm.FleetId, qm.VehicleId));
                            break;
                    }

                    if (retVal != null)
                    {
                        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.StorePacket(qm, System.BitConverter.ToString(retVal.mRawBytes));
                        // Update the status to "Complete"
                        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.UpdateMessageStatus(qm, GatewayServiceBL.Classes.MessageStatus.Complete);

                        _log.Info("Storing sent message details");
                        ListenerLogItem logItem = new ListenerLogItem();
                        logItem.FleetID = qm.FleetId;
                        logItem.VehicleID = qm.VehicleId;
                        logItem.ListenerMessage = System.BitConverter.ToString(retVal.mRawBytes);
                        logItem.Direction = LogDirection.Outbound;
                        logItem.MessageType = GatewayProtocolLookup.GetMessageType(retVal.cMsgType);
                        logItem.IPAddress = packet.mSenderIP.ToString();
                        logItem.LogDate = DateTime.UtcNow;
                        if (_logToMsmq != null)
                        {
                            _logToMsmq.InsertIntoQueuesAsync(logItem);
                        }
                        _log.Info("Finished storing sent message details");
                    }
                    else
                    {
                        // Update the status to "Failed"
                        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.UpdateMessageStatus(qm, GatewayServiceBL.Classes.MessageStatus.Failed);
                    }

                    obMess.Remove(qm);

                    _log.Debug("Queued Messages : " + obMess.Count.ToString());
                    // Only need to process one at a time as the preceeding one may cause a restart

                    break;
                }
            }
        }

        public void SendClientUpdates()
        {
            while (bProcessPacketThreadActive && mRxThreads != null)
            {
                try
                {
                    GatewayProtocolPacket oGP = null;
                    lock (_clientLiveUpdateQueue.SyncRoot)
                        if (_clientLiveUpdateQueue.Count > 0)
                            oGP = (GatewayProtocolPacket)_clientLiveUpdateQueue.Dequeue();
                    if (oGP != null)
                    {
                        //if packet is dignositic, diagnostic level or IF config restart don't send live update
                        if (oGP.cMsgType != GeneralGPPacket.DIAGNOSTIC_LEVEL && 
                            oGP.cMsgType != GeneralGPPacket.DIAGNOSTIC && 
                            oGP.cMsgType != GeneralGPPacket.GEN_IF_CONFIG_RESTART &&
                            oGP.cMsgType != GeneralGPPacket.ECM_HYSTERESIS_ERROR &&
                            oGP.cMsgType != GeneralGPPacket.GEN_NOGPS &&
                            oGP.cMsgType != GeneralGPPacket.SYSTEM_GPS_FAILURE &&
                            oGP.cMsgType != GeneralGPPacket.SAT_PING)
                        {
                            if (oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM || oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                            {
                                switch (oGP.cMsgType)
                                {
                                    case GeneralGPPacket.PENDENT_ALARM:
                                        _log.Info("Sending client live updates for Pendant alarm packet");
                                        break;
                                    case GeneralGPPacket.PENDENT_ALARM_TEST:
                                        _log.Info("Sending client live updates for Pendant test alarm packet");
                                        break;
                                    case GeneralGPPacket.PENDENT_ALARM_CLEARED:
                                        _log.Info("Sending client live updates for Pendant alarm cleared packet");
                                        break;
                                }
                            }
                            Vehicle v = mDBInterface.GetVehicle(oGP);
                            if (v != null)
                            {
                                if (_remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(v.cFleet), Convert.ToUInt32(v.iUnit)))
                                {
                                    //	Do not propagate live updates for vehicles that are in the ignore list.
                                    if (!oGP.bArchivalData)
                                    {
                                        Action del = () => { mClientRegistry.NotifyClientsOfPacket(oGP, v, false); };
                                        del.BeginInvoke(null, null);

                                        List<MTData.Transport.Gateway.Packet.GeneralGPPacket> updates = new List<MTData.Transport.Gateway.Packet.GeneralGPPacket>();
                                        lock (v.oAddtionalLiveUpdateSync)
                                        {
                                            if (v.oAddtionalLiveUpdatePackets.Count > 0)
                                            {
                                                for (int X = 0; X < v.oAddtionalLiveUpdatePackets.Count; X++)
                                                {
                                                    updates.Add(((MTData.Transport.Gateway.Packet.GeneralGPPacket)v.oAddtionalLiveUpdatePackets[X]).CreateCopy());
                                                }
                                                v.oAddtionalLiveUpdatePackets.Clear();
                                            }
                                        }
                                        mClientRegistry.NotifyClientOfRemoteStatusChange(updates, v);
                                    }
                                    else
                                    {
                                        Action del = () => { mClientRegistry.NotifyClientsOfPacket(oGP, v, true); };
                                        del.BeginInvoke(null, null);
                                    }
                                }
                                if (oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM || oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                {
                                    switch (oGP.cMsgType)
                                    {
                                        case GeneralGPPacket.PENDENT_ALARM:
                                            _log.Info("Finished sending client live updates for Pendant alarm packet");
                                            break;
                                        case GeneralGPPacket.PENDENT_ALARM_TEST:
                                            _log.Info("Finished sending client live updates for Pendant test alarm packet");
                                            break;
                                        case GeneralGPPacket.PENDENT_ALARM_CLEARED:
                                            _log.Info("Finished sending client live updates for Pendant alarm cleared packet");
                                            break;
                                    }
                                }
                            }
                            else
                            {
                                if (oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM || oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || oGP.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                {
                                    switch (oGP.cMsgType)
                                    {
                                        case GeneralGPPacket.PENDENT_ALARM:
                                            _log.Info("F/V " + Convert.ToString(oGP.cFleetId) + "/" + Convert.ToString(oGP.iVehicleId) + " - Could not load vehicle object for Pendant alarm packet");
                                            break;
                                        case GeneralGPPacket.PENDENT_ALARM_TEST:
                                            _log.Info("F/V " + Convert.ToString(oGP.cFleetId) + "/" + Convert.ToString(oGP.iVehicleId) + " - Could not load vehicle object for Pendant test alarm packet");
                                            break;
                                        case GeneralGPPacket.PENDENT_ALARM_CLEARED:
                                            _log.Info("F/V " + Convert.ToString(oGP.cFleetId) + "/" + Convert.ToString(oGP.iVehicleId) + " - Could not load vehicle object for Pendant alarm cleared packet");
                                            break;
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
                catch (Exception exMain)
                {
                    _log.Error(sClassName + "SendClientUpdates()", exMain);
                }
            }
            _log.Info("Stopping Client Live Update Thread.");
            try
            {
                clientUpdateThread.Abort();
            }
            catch (System.Exception)
            {
            }
        }

        public void SendAlarm()
        {
            string errString;
            try
            {
                // Send an ALARM packet to the viewed unit
                if ((errString =
                    mDBInterface.SendAlarm(mRxedPackets[mViewedPacket - 1])) != null)
                {
                    _log.InfoFormat("Status : Cannot decode packet {0}", mViewedPacket);
                }
                else
                {
                    _log.InfoFormat("Status : Sent ALARM to unit {0}",
                        mRxedPackets[mViewedPacket - 1].iVehicleId);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendAlarm()", ex);
                throw new System.Exception(sClassName + "SendAlarm()", ex);
            }
        }
        #endregion

        #region Serial Access Events

        private void mDBInterface_eConcreteStatusChanged(int iFleetID, byte[] bData)
        {
            try
            {
                // Send the Concrete status change to the appropriate interface server.
                mClientRegistry.PassBytesToInterfaceServer(iFleetID, bData);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "mDBInterface_eConcreteStatusChanged(int iFleetID, byte[] bData)", ex);
            }
        }

        private void mListenerThread_eConsoleEvent(string sMsg)
        {
            _log.Info(sMsg);
        }

        private void mDBInterface_eConsoleEvent(string sMsg)
        {
            _log.Info(sMsg);
        }

        private void mClientRegistry_eConsoleEvent(string sMsg)
        {
            _log.Info(sMsg);
        }

        private void mStateUpdateThread_eConsoleEvent(string sMsg)
        {
            _log.Info(sMsg);
        }

        #endregion
        #region Utility Functions

        /// <summary>
        /// Read out a alue from the configuration file and determine if all is well.
        /// </summary>
        /// <param name="settingName"></param>
        /// <param name="sValue"></param>
        /// <returns></returns>
        private bool ReadAppSetting(string settingName, ref string sValue)
        {
            try
            {
                sValue = ConfigurationManager.AppSettings[settingName];
                if (sValue == null)
                    return false;
                else
                    return true;
            }
            catch (Exception)
            {
                sValue = "Key " + settingName + " not found.";
                return false;
            }
        }

        /// <summary>
        /// This method will prepare all items from the configuration file.
        /// </summary>
        /// <returns></returns>
        private string SetupFromConfigFile()
        {
            string sValue = null;
            string sRet = "";
            try
            {
                if (!ReadAppSetting("BindAddress", ref sValue))
                    sRet += "Failed to read 'BindAddress' key\r\n";
                else
                    sInterfaceBindAddress = sValue;

                if (!ReadAppSetting("ConnectPort", ref sValue))
                    sRet += "Failed to read 'ConnectPort' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iConnectPort = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'ConnectPort' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("ClientNotifyPort", ref sValue))
                    sRet += "Failed to read 'ClientNotifyPort' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iClientNotifyPort = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'ClientNotifyPort' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("RemoteStateUpdatePort", ref sValue))
                    sRet += "Failed to read 'RemoteStateUpdatePort' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iStateUpdatePort = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'RemoteStateUpdatePort' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("ResetUnitOnFirstReport", ref sValue))
                    sRet += "Failed to read 'ResetUnitOnFirstReport' key\r\n";
                if (!ReadAppSetting("MessageQueueName", ref sValue))
                    sRet += "Failed to read 'MessageQueueName' key\r\n";
                if (!ReadAppSetting("MessageQueueMaxSize", ref sValue))
                    sRet += "Failed to read 'MessageQueueMaxSize' key\r\n";
                if (!ReadAppSetting("MessageQueueIsRecoverable", ref sValue))
                    sRet += "Failed to read 'MessageQueueIsRecoverable' key\r\n";
                if (!ReadAppSetting("PauseOnFailedMSMQInsert", ref sValue))
                    sRet += "Failed to read 'PauseOnFailedMSMQInsert' key\r\n";
                if (!ReadAppSetting("PacketRateLogging", ref sValue))
                    sRet += "Failed to read 'PacketRateLogging' key\r\n";
                if (!ReadAppSetting("ConnectString", ref sValue))
                    sRet += "Failed to read 'ConnectString' key\r\n";
                else
                    sConnectString = sValue;

                if (!ReadAppSetting("DatabaseVer", ref sValue))
                    sRet += "Failed to read 'DatabaseVer' key\r\n";
                if (!ReadAppSetting("CommandTimeout", ref sValue))
                    sRet += "Failed to read 'CommandTimeout' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        _commandTimeout = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'CommandTimeout' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("UseCustomerDatabase", ref sValue))
                    sRet += "Failed to read 'UseCustomerDatabase' key\r\n";
                if (!ReadAppSetting("CustomerConnectString", ref sValue))
                    sRet += "Failed to read 'CustomerConnectString' key\r\n";
                if (!ReadAppSetting("MySQLConnectString", ref sValue))
                    sRet += "Failed to read 'MySQLConnectString' key\r\n";
                {
                    if (sValue != "")
                    {
                        if (!ReadAppSetting("MySQLNoOperationDelay", ref sValue))
                            sRet += "Failed to read 'MySQLNoOperationDelay' key\r\n";
                        if (!ReadAppSetting("MySQLNoConnectionDelay", ref sValue))
                            sRet += "Failed to read 'MySQLNoConnectionDelay' key\r\n";
                        if (!ReadAppSetting("MySQLSuccessDelay", ref sValue))
                            sRet += "Failed to read 'MySQLSuccessDelay' key\r\n";
                        if (ReadAppSetting("MySQLNoOperationDelay", ref sValue) && ReadAppSetting("MySQLNoConnectionDelay", ref sValue) && ReadAppSetting("MySQLSuccessDelay", ref sValue))
                        {
                            _mySqlParams = new MySQLInterface.cMySQLInterfaceParams(
                            ConfigurationManager.AppSettings["MySQLConnectString"],
                            ConfigurationManager.AppSettings["MySQLNoOperationDelay"],
                            ConfigurationManager.AppSettings["MySQLNoConnectionDelay"],
                            ConfigurationManager.AppSettings["MySQLSuccessDelay"]);
                        }
                    }
                }
                if (ReadAppSetting("EMailActive", ref sValue))
                {
                    _emailActive = false;
                }
                else
                {
                    if (ReadAppSetting("EMailActive", ref sValue))
                        _emailActive = (sValue.ToLower() == "true");
                    else
                        _emailActive = true;

                    if (_emailActive)
                    {
                        if (ReadAppSetting("EMailSourceAddress", ref sValue))
                            _emailSourceAddress = sValue;
                        if (ReadAppSetting("EMailTargetAddress", ref sValue))
                            _emailTargetAddress = sValue;
                        if (ReadAppSetting("EMailSubject", ref sValue))
                            _emailSubject = sValue;
                        if (ReadAppSetting("EMailMessage", ref sValue))
                            _emailMessage = sValue;
                    }
                }

                if (!ReadAppSetting("StatusFilePath", ref sValue))
                    sRet += "Failed to read 'StatusFilePath' key\r\n";
                if (!ReadAppSetting("WriteStatusInterval", ref sValue))
                    sRet += "Failed to read 'WriteStatusInterval' key\r\n";
                if (!ReadAppSetting("RemoteUpdateBackupFile", ref sValue))
                    sRet += "Failed to read 'RemoteUpdateBackupFile' key\r\n";
                if (!ReadAppSetting("MinOutgoingPacketSize", ref sValue))
                    sRet += "Failed to read 'MinOutgoingPacketSize' key\r\n";
                if (!ReadAppSetting("UdpRxQueue_MaxPacketCount", ref sValue))
                    sRet += "Failed to read 'UdpRxQueue_MaxPacketCount' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        _udpRxQueue_MaxPacketCount = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'UdpRxQueue_MaxPacketCount' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("ReplicateMode", ref sValue))
                    sRet += "Failed to read 'ReplicateMode' key\r\n";
                if (!ReadAppSetting("ReplicationTargetHost", ref sValue))
                    sRet += "Failed to read 'ReplicationTargetHost' key\r\n";
                if (!ReadAppSetting("ReplicationTargetPort", ref sValue))
                    sRet += "Failed to read 'ReplicationTargetPort' key\r\n";
                if (!ReadAppSetting("ReplicationInputPort", ref sValue))
                    sRet += "Failed to read 'ReplicationInputPort' key\r\n";
                if (!ReadAppSetting("PacketRateInterval", ref sValue))
                    sRet += "Failed to read 'PacketRateInterval' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iPrlInterval = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'PacketRateInterval' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("PacketRateThreshold", ref sValue))
                    sRet += "Failed to read 'PacketRateThreshold' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iPrlThreshold = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'PacketRateThreshold' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("LoadConfigDataSettings", ref sValue))
                    sRet += "Failed to read 'LoadConfigDataSettings' key\r\n";
                if (!ReadAppSetting("UserDefined_DateFormat", ref sValue))
                    sRet += "Failed to read 'UserDefined_DateFormat' key\r\n";
                if (!ReadAppSetting("ServerTime_DateFormat", ref sValue))
                    sRet += "Failed to read 'ServerTime_DateFormat' key\r\n";
                if (!ReadAppSetting("AckAllRequested", ref sValue))
                    sRet += "Failed to read 'AckAllRequested' key\r\n";
                if (!ReadAppSetting("DefaultUserInfoTagValue", ref sValue))
                    sRet += "Failed to read 'DefaultUserInfoTagValue' key\r\n";
                if (!ReadAppSetting("AdjustForGPSBounce", ref sValue))
                    sRet += "Failed to read 'AdjustForGPSBounce' key\r\n";
                if (!ReadAppSetting("DefaultGPSRules", ref sValue))
                    sRet += "Failed to read 'DefaultGPSRules' key\r\n";
                if (!ReadAppSetting("RouteSupportActive", ref sValue))
                    sRet += "Failed to read 'RouteSupportActive' key\r\n";
                if (!ReadAppSetting("RouteLookAheadIntervalHrs", ref sValue))
                    sRet += "Failed to read 'RouteLookAheadIntervalHrs' key\r\n";
                if (!ReadAppSetting("RefreshWPListTimer", ref sValue))
                    sRet += "Failed to read 'RefreshWPListTimer' key\r\n";
                if (!ReadAppSetting("ProcessServerWPsWhilstVehicleIsMoving", ref sValue))
                    sRet += "Failed to read 'ProcessServerWPsWhilstVehicleIsMoving' key\r\n";
                if (!ReadAppSetting("SupportServerAndUnitWPs", ref sValue))
                    sRet += "Failed to read 'SupportServerAndUnitWPs' key\r\n";
                if (!ReadAppSetting("TrackingDeviceDownloadRetryInterval", ref sValue))
                    sRet += "Failed to read 'TrackingDeviceDownloadRetryInterval' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iTrackingDeviceDownloadRetryInterval = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'TrackingDeviceDownloadRetryInterval' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("TrackingDeviceDownloadRetryCount", ref sValue))
                    sRet += "Failed to read 'TrackingDeviceDownloadRetryCount' key\r\n";
                if (!ReadAppSetting("InterfaceBoxDownloadRetryInterval", ref sValue))
                    sRet += "Failed to read 'InterfaceBoxDownloadRetryInterval' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iInterfaceBoxDownloadRetryInterval = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'InterfaceBoxDownloadRetryInterval' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("InterfaceBoxDownloadRetryCount", ref sValue))
                    sRet += "Failed to read 'InterfaceBoxDownloadRetryCount' key\r\n";
                if (!ReadAppSetting("DataTerminalDownloadRetryInterval", ref sValue))
                    sRet += "Failed to read 'DataTerminalDownloadRetryInterval' key\r\n";
                else
                {
                    if (Util.IsInteger(sValue))
                        iDataTerminalDownloadRetryInterval = Convert.ToInt32(sValue);
                    else
                        sRet += "Failed to read 'DataTerminalDownloadRetryInterval' key, value is not numeric\r\n";
                }
                if (!ReadAppSetting("4000DownloadRetryInterval", ref sValue))
                    i4000DownloadRetryInterval = iTrackingDeviceDownloadRetryInterval;
                else
                {
                    if (Util.IsInteger(sValue))
                        i4000DownloadRetryInterval = Convert.ToInt32(sValue);
                    else
                        i4000DownloadRetryInterval = iTrackingDeviceDownloadRetryInterval;
                }
                if (!ReadAppSetting("DataTerminalDownloadRetryCount", ref sValue))
                    sRet += "Failed to read 'DataTerminalDownloadRetryCount' key\r\n";
                if (!ReadAppSetting("4000DownloadRetryCount", ref sValue))
                    sRet += "Failed to read '4000DownloadRetryCount' key\r\n";
                if (!ReadAppSetting("ForceMessageForVerify", ref sValue))
                    sRet += "Failed to read 'ForceMessageForVerify' key\r\n";
                if (!ReadAppSetting("ConstantPositionThreshold", ref sValue))
                    sRet += "Failed to read 'ConstantPositionThreshold' key\r\n";
                if (!ReadAppSetting("ConstantPositionTimeMinutes", ref sValue))
                    sRet += "Failed to read 'ConstantPositionTimeMinutes' key\r\n";
                if (!ReadAppSetting("PreReqTimeout", ref sValue))
                    sRet += "Failed to read 'PreReqTimeout' key\r\n";
                if (!ReadAppSetting("PreReqMaxRetries", ref sValue))
                    sRet += "Failed to read 'PreReqMaxRetries' key\r\n";
                if (!ReadAppSetting("LogOutboundMessages", ref sValue))
                    sRet += "Failed to read 'LogOutboundMessages' key\r\n";
                if (!ReadAppSetting("LogOutboundMessagesPort", ref sValue))
                    sRet += "Failed to read 'LogOutboundMessagesPort' key\r\n";
                if (!ReadAppSetting("ConcreteFleets", ref sValue))
                    sRet += "Failed to read 'ConcreteFleets' key\r\n";
                else
                    sConcreteFleets = sValue;

                if (!ReadAppSetting("RestoreMDTStatusForFleet", ref sValue))
                    sRestoreStatusFleets = "";
                else
                    sRestoreStatusFleets = sValue;

                if (!ReadAppSetting("ForwardToCommandEnabled", ref sValue))
                    sRet += "Failed to read 'ForwardToCommandEnabled' key\r\n";
                if (!ReadAppSetting("FleetsToForwardToCommand", ref sValue))
                    sRet += "Failed to read 'FleetsToForwardToCommand' key\r\n";
                else
                    sFleetsToForwardToCommand = sValue;
                if (!ReadAppSetting("DigiComEnabled", ref sValue))
                    sRet += "Failed to read 'DigiComEnabled' key\r\n";
                if (!ReadAppSetting("DigiComSendTimestamps", ref sValue))
                    sRet += "Failed to read 'DigiComSendTimestamps' key\r\n";
                if (!ReadAppSetting("DigiComPort", ref sValue))
                    sRet += "Failed to read 'DigiComPort' key\r\n";
                if (!ReadAppSetting("DigiComBaudRate", ref sValue))
                    sRet += "Failed to read 'DigiComBaudRate' key\r\n";
                if (!ReadAppSetting("DigiComParity", ref sValue))
                    sRet += "Failed to read 'DigiComParity' key\r\n";
                if (!ReadAppSetting("DigiComDataBits", ref sValue))
                    sRet += "Failed to read 'DigiComDataBits' key\r\n";
                if (!ReadAppSetting("DigiComStopBits", ref sValue))
                    sRet += "Failed to read 'DigiComStopBits' key\r\n";
                if (!ReadAppSetting("DigiComUseFlowControl", ref sValue))
                    sRet += "Failed to read 'DigiComUseFlowControl' key\r\n";
                if (!ReadAppSetting("DigiComRecieveData", ref sValue))
                    sRet += "Failed to read 'DigiComRecieveData' key\r\n";
                if (!ReadAppSetting("LogPacketData", ref sValue))
                    sRet += "Failed to read 'LogPacketData' key\r\n";
                if (!ReadAppSetting("LogClientUpdateData", ref sValue))
                    sRet += "Failed to read 'LogClientUpdateData' key\r\n";
                if (!ReadAppSetting("LogRemoteStateUpdateData", ref sValue))
                    sRet += "Failed to read 'LogRemoteStateUpdateData' key\r\n";
                if (!ReadAppSetting("LogDatabaseData", ref sValue))
                    sRet += "Failed to read 'LogDatabaseData' key\r\n";
                if (!ReadAppSetting("LogRouteData", ref sValue))
                    sRet += "Failed to read 'LogRouteData' key\r\n";
                if (!ReadAppSetting("PerformanceLogging", ref sValue))
                    sRet += "Failed to read 'PerformanceLogging' key\r\n";

                if (sRet == "")
                    return null;
                else
                    return sRet;
            }
            catch (Exception e)
            {
                _log.Error(sClassName + "SetupFromConfigFile()", e);
                return "Exception " + e.Message + " on reading application configuration file";
            }

        }


        #endregion

        /// <summary>
        /// This is raised when an invalid packet has come from the same IP address 5 times.
        /// </summary>
        /// <param name="sender"></param>
        private void _invalidPacketHistory_RequiresACK(InvalidPacketHistoryEntry sender, byte[] packet)
        {
            try
            {
                if ((sender.FleetID != -1) && (sender.VehicleID != 0))
                {
                    _log.Info(string.Format("IPAddress {0}, Fleet {1}, Vehicle {2} : Sending Forced ACK for repeated invalid packet : {3}",
                        sender.EndPoint.ToString(), sender.FleetID, sender.VehicleID, Util.EncodeByteArrayAsHex(packet)));

                    GeneralGPPacket ackPacket = new GeneralGPPacket(_serverTime_DateFormat);
                    ackPacket.cMsgType = GeneralGPPacket.GEN_ACK;
                    ackPacket.iVehicleId = sender.VehicleID;
                    ackPacket.cFleetId = Convert.ToByte(sender.FleetID);
                    ackPacket.cPacketNum = 1;
                    ackPacket.cPacketTotal = 1;

                    ackPacket.cAckSequence = Convert.ToByte((sender.MobileSequence <= 30) ? (sender.MobileSequence + 1) : 0);
                    sender.MobileSequence = ackPacket.cAckSequence;

                    ackPacket.cOurSequence = Convert.ToByte((sender.BaseSequence <= 30) ? (sender.BaseSequence + 1) : 0);
                    sender.BaseSequence = ackPacket.cOurSequence;

                    ackPacket.bAckImmediately = true;
                    ackPacket.bAckRegardless = true;
                    ackPacket.bSendFlash = true;
                    ackPacket.bArchivalData = true;
                    ackPacket.ServerLoadLevel = mDBInterface.Database.CurrentServerLoadLevel;
                    ackPacket.ServerLoadStatusReport = mDBInterface.Database.CurrentServerLoadStatus;
                    byte[] dummy = new byte[0];
                    ackPacket.Encode(ref dummy);

                    ackPacket.mSenderIP = sender.EndPoint;

                    mDBInterface.SendForcedPacket(ackPacket, true);
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "_invalidPacketHistory_RequiresACK(InvalidPacketHistoryEntry sender, byte[] packet)", ex);
                throw new System.Exception(sClassName + "_invalidPacketHistory_RequiresACK(InvalidPacketHistoryEntry sender, byte[] packet)", ex);
            }
        }
        #region RemoteUpdate Engine Event Handlers
        private void _remoteUpdateEngine_VehicleSetAtLunchStateRecievd(int fleetID, uint vehicleID)
        {
            try
            {
                mDBInterface.SetAtLunchState(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleSetAtLunchStateRecievd(int fleetID, uint vehicleID)", ex);
            }
        }

        private void _remoteUpdateEngine_VehicleUnsetAtLunchStateRecievd(int fleetID, uint vehicleID)
        {
            try
            {
                mDBInterface.UnsetAtLunchState(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleUnsetAtLunchStateRecievd(int fleetID, uint vehicleID)", ex);
            }
        }

        /// <summary>
        /// This handler will be triggered whenever a vehicle is created through an external means and notified into the RemoteUdpateEngine
        /// </summary>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void _remoteUpdateEngine_VehicleCreated(int fleetID, uint vehicleID)
        {
            try
            {
                mClientRegistry.NotifyClientsOfVehicleCreate(fleetID, vehicleID);
                mDBInterface.RefreshVehicleData("creating a new vehicle");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleCreated(int fleetID, uint vehicleID)", ex);
            }
        }

        /// <summary>
        /// This is raised whenever a vehicle is deleted externally and notified through the remote udpate channel.
        /// </summary>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        private void _remoteUpdateEngine_VehicleDeleted(int fleetID, uint vehicleID)
        {
            try
            {
                mClientRegistry.NotifyClientsOfVehicleDelete(fleetID, vehicleID);
                mDBInterface.RefreshVehicleData("Deleting a vehicle");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleDeleted(int fleetID, uint vehicleID)", ex);
            }
        }

        /// <summary>
        /// This is raised whenever a 5010 login message is sent via remote update.
        /// </summary>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        /// <param name="DriverID"></param>
        /// <param name="sDriverName"></param>
        private void _remoteUpdateEngine_VehicleLogin(int fleetID, uint vehicleID, int DriverID, string sDriverName)
        {
            try
            {
                oPC.DriverChanged(new PacketCreaterContext(fleetID, "Driver Changed"), fleetID, Convert.ToInt32(vehicleID), DriverID, sDriverName);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleLogin(int fleetID, uint vehicleID, int DriverID, string sDriverName)", ex);
            }
        }

        private void _remoteUpdateEngine_VehicleMessageRecievd(int fleetID, uint vehicleID)
        {
            try
            {
                oPC.MessageRecieved(new PacketCreaterContext(fleetID, "Message Recieved"), fleetID, Convert.ToInt32(vehicleID));
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleMessageRecievd(int fleetID, uint vehicleID)", ex);
            }
        }

        private void _remoteUpdateEngine_VehicleMessageQueued(int fleetID, uint vehicleID)
        {
            try
            {
                oPC.MessageQueued(new PacketCreaterContext(fleetID, "Message Queued"), fleetID, Convert.ToInt32(vehicleID));
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleMessageQueued(int fleetID, uint vehicleID)", ex);
            }
        }

        private void _remoteUpdateEngine_VehicleMessageNotQueued(int fleetID, uint vehicleID)
        {
            try
            {
                oPC.MessageNotQueued(new PacketCreaterContext(fleetID, "Message Not Queued"), fleetID, Convert.ToInt32(vehicleID));
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_remoteUpdateEngine_VehicleMessageNotQueued(int fleetID, uint vehicleID)", ex);
            }
        }

        /// <summary>
        /// This is raised the packet creator create a new login message to send to the clients.
        /// </summary>
        private void oPC_OutboundPacket(object context, byte[] packet)
        {
            try
            {
                PacketCreaterContext createrContext = (PacketCreaterContext)context;
                string sPacket = System.Text.ASCIIEncoding.ASCII.GetString(packet);

                if (createrContext.FleetIDs == null)
                    mClientRegistry.NotifyClientsOfVehicleLogin(-1, sPacket);
                else
                    for (int loop = 0; loop < createrContext.FleetIDs.Length; loop++)
                        mClientRegistry.NotifyClientsOfVehicleLogin(createrContext.FleetIDs[loop], sPacket);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "oPC_OutboundPacket(object context, byte[] packet)", ex);
            }
        }
        #endregion

        #region Delegate to stop accepting unit traffic if there is a hold up in subsequent processing
        private void SetAcceptUnitTrafficFlag(bool AcceptUnitTraffic)
        {
            try
            {
                _acceptTrafficFromUnits = AcceptUnitTraffic;
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "SetAcceptUnitTrafficFlag(bool AcceptUnitTraffic)", ex);
            }
        }
        #endregion

        #region Passing a list of units to ignore reports from
        public bool IgnoreListReplyToPings
        {
            get
            {
                try
                {
                    if (mListenerThread != null) return mListenerThread.IgnoreListReplyToPings;
                }
                catch (Exception ex)
                {
                    _log.Error(sClassName + "IgnoreListReplyToPings (Get)", ex);
                }
                return false;
            }
            set
            {
                try
                {
                    if (mListenerThread != null) mListenerThread.IgnoreListReplyToPings = value;
                }
                catch (Exception ex)
                {
                    _log.Error(sClassName + "IgnoreListReplyToPings (Set)", ex);
                }
            }
        }
        public bool IgnoreListReplyToPingsFromUnknownUnits
        {
            get
            {
                try
                {
                    if (mListenerThread != null) return mListenerThread.IgnoreListReplyToPingsFromUnknownUnits;
                }
                catch (Exception ex)
                {
                    _log.Error(sClassName + "IgnoreListReplyToPings (Get)", ex);
                }
                return false;
            }
            set
            {
                try
                {
                    if (mListenerThread != null) mListenerThread.IgnoreListReplyToPingsFromUnknownUnits = value;
                }
                catch (Exception ex)
                {
                    _log.Error(sClassName + "IgnoreListReplyToPings (Set)", ex);
                }
            }
        }
        public void IgnoreListAddVehicle(int fleetId, int vehicleId)
        {
            try
            {
                if (mListenerThread != null) mListenerThread.IgnoreListAddVehicle(fleetId, vehicleId);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "IgnoreListAddVehicle(int fleetId, int vehicleId)", ex);
                throw new System.Exception(sClassName + "IgnoreListAddVehicle(int fleetId, int vehicleId)", ex);
            }
        }
        public void IgnoreListClearList()
        {
            try
            {
                if (mListenerThread != null) mListenerThread.IgnoreListClearList();
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "IgnoreListClearList()", ex);
                throw new System.Exception(sClassName + "IgnoreListClearList()", ex);
            }
        }
        public void IgnoreListRemoveVehicle(int fleetId, int vehicleId)
        {
            try
            {
                if (mListenerThread != null) mListenerThread.IgnoreListRemoveVehicle(fleetId, vehicleId);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "IgnoreListRemoveVehicle(int fleetId, int vehicleId)", ex);
                throw new System.Exception(sClassName + "IgnoreListRemoveVehicle(int fleetId, int vehicleId)", ex);
            }
        }
        #endregion

        #region scheduled Tasks
        private object ScheduledTaskThread(EnhancedThread sender, object data)
        {
            try
            {
                //use the previous half hour time as the start time
                DateTime time = DateTime.UtcNow;
                int minute = 0;
                if (time.Minute >= 30)
                {
                    minute = 30;
                }
                time = new DateTime(time.Year, time.Month, time.Day, time.Hour, minute, 1);

                while (!sender.Stopping)
                {
                    try
                    {
                        DateTime currentTime = DateTime.UtcNow;

                        //check if stationary alert is to be activated
                        DataRow[] rows = mDBInterface.GetScheduledStationaryAlerts(currentTime);
                        if (rows != null)
                        {
                            foreach (DataRow r in rows)
                            {
                                GatewayProtocolPacket liveUpdate = mDBInterface.SetStationaryAlertToActive(r);
                                if (liveUpdate != null)
                                {
                                    QueueClientUpdate(liveUpdate);
                                }
                            }
                        }
                        
                        //check if out of hours usage has been finished
                        rows = mDBInterface.GetScheduledOutOfHoursFinished(currentTime);
                        if (rows != null)
                        {
                            foreach (DataRow r in rows)
                            {
                                GatewayProtocolPacket liveUpdate = mDBInterface.EndOutOfHoursUsage(r);
                                if (liveUpdate != null)
                                {
                                    QueueClientUpdate(liveUpdate);
                                }
                            }
                        }

                        //check scheduled tasks
                        if (currentTime > time)
                        {
                            //check all tasks against this data
                            mDBInterface.Database.CheckScheduledTasksAgainstDate(time);

                            //set time to next half hour block
                            time = time.AddMinutes(30);
                        }

                        if (!sender.Stopping)
                        {
                            Thread.Sleep(100);
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error("error in ScheduledTaskThread", ex);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error("error in ScheduledTaskThread", ex);
            }
            return null;
        }
        #endregion

        private void mDBInterface_eSendLiveUpdate(GatewayProtocolPacket aPacket)
        {
            QueueClientUpdate(aPacket);
        }

        public class DownloadEntry
        {
            #region Private Fields
            private int id;
            private int fleetID;
            private int vehicleID;
            private bool abort;
            #endregion

            #region Constructor
            public DownloadEntry(int downloadId, int fleetID, int vehicleID)
            {
                this.id = downloadId;
                this.fleetID = fleetID;
                this.vehicleID = vehicleID;
            }
            #endregion

            #region Public Events/Delegates/Enums
            #endregion

            #region Public Properties
            public int ID
            {
                get { return this.id; }
            }

            public int FleetID
            {
                get { return this.fleetID; }
            }

            public int VehicleID
            {
                get { return this.vehicleID; }
            }

            public bool AbortDownload
            {
                get { return this.abort; }
                set { this.abort = value; }
            }
            #endregion

            #region Methods
            #endregion

            #region Override Methods
            #endregion

            #region Interface Methods
            #endregion
        }

        private class UnitEventCounter
        {
            #region Private Fields
            private int fleetID;
            private int vehicleID;
            private int eventTotal;
            #endregion

            #region Constructor
            /// <summary>
            /// This will create a unit entry and auto add one to the event counter
            /// </summary>
            /// <param name="fleetID"></param>
            /// <param name="vehicleID"></param>
            public UnitEventCounter(int fleetID, int vehicleID)
            {
                this.fleetID = fleetID;
                this.vehicleID = vehicleID;
                this.eventTotal = 1;
            }
            #endregion

            #region Public Events/Delegates/Enums
            #endregion

            #region Public Properties
            public int FleetID
            {
                get { return this.fleetID; }
            }

            public int VehicleID
            {
                get { return this.vehicleID; }
            }

            public int GetEventTotal
            {
                get { return this.eventTotal; }
            }
            #endregion

            #region Methods
            #endregion

            #region Override Methods
            public void AddEventCount()
            {
                this.eventTotal++;
            }
            #endregion

            #region Interface Methods
            #endregion
        }
    }
}

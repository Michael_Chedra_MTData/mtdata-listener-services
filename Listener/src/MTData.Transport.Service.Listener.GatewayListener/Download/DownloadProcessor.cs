using System;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Service.Listener.GatewayServiceBL;
using MTData.Transport.Gateway.Packet;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// Summary description for DownloadProcessor.
	/// </summary>
	
	public enum DownloadType
	{
		TrackingDevice,
		InterfaceBox,
		MobileDataTerminal,
		BinaryFile
	};

	public class DownloadProcessor
	{
		private const string sClassName = "GatewayListener.DownloadProcessor.";
		private static ILog _log = LogManager.GetLogger(typeof(DownloadProcessor));

		//private string sFleetID = "";
		//private string sVehicleID = "";

		#region Events
		/// <summary>
		/// This delegate defined the eUpdateVehicleStatus event
		/// </summary>
		private delegate void StatusEvent(int fleetID, int vehicleID, string sStatus);
		/// <summary>
		/// This event is raised whenever the class has a message to pass back to the multidownload form
		/// </summary>
		private event StatusEvent DownloadStatusChanged;
		/// <summary>
		/// This delegate defined the eUpdateVehicleStatus event
		/// </summary>
		public delegate void DownloadCompleteEvent(int queuedDownloadID);
		/// <summary>
		/// This event is raised whenever the class has a message to pass back to the multidownload form
		/// </summary>
		public event DownloadCompleteEvent DownloadComplete;
		/// <summary>
		/// This delete definies the DownloadStart event
		/// </summary>
		/// <param name="queuedDownloadID"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public delegate void DownloadStartEvent(int queuedDownloadID, int fleetID, int vehicleID);
		/// <summary>
		/// The event delegate
		/// </summary>
		public event DownloadStartEvent DownloadStart;
		/// <summary>
		/// The event delegate
		/// </summary>
		/// <param name="sender"></param>
		public delegate void FireAbort(object sender, int fleetID, int vehicleID);
		/// <summary>
		/// Delegate to check if user has aborted download
		/// </summary>
		public event FireAbort FireAbortEvent;
		#endregion
		#region Variables
		private string _serverTime_DateFormat = "";
		private bool RequireUserToIssueCommands;
		public enum ProcessState : byte
		{
			Running,
			Failed,
			TimedOut,
			Successful
		};

		public ProcessState State;

		
		private MTData.Transport.Service.Listener.GatewayServiceBL.Classes.ListenerDownloadType mDownloadType = GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice;
		//public DownloadType DownloadType
		//{
		//    get
		//    {
		//        return mDownloadType;
		//    }
		//}
		private System.Timers.Timer mVerifyTimer;
		private GatewayProtocolPacket mPacket;
		private GatewayServiceBL.Classes.QueuedDownload currentDownload;
		private PacketDatabaseInterface mDatabase;
		private int iTotalBlockCount;
		private int iBlockCount;
		private byte cDownloadVersion;
		private MotorolaSRecordFile mMotFile;
		private cBinaryFile mBinFile = null;
		public MotorolaSRecordFile MotFile
		{
			set
			{
				mMotFile = value;
			}
			get
			{
				return mMotFile;
			}
		}
		private long lPreviousAddress, lThisAddress, lNextAddress;
		private int iPreferredDataLength = 250;
		private int iDownloadRetryIntervalSecs;
		private int iDownloadRetryMaxCount;
		public bool bUserWantsToContinue;
		

		// Things for the "verify" packet:
		private int iProgramChecksumTotal;
		private int iPacketChecksumTotal;
		private int iProgramBytesCountTotal;

        private System.Timers.Timer packetResponseTimer;    // 1 second counter so that we can abort if needed
        private int packetResponseCounter;  // Counter to trigger the abort
        private int packetResetValue = 90;    // 90 Seconds per packet response before we consider this aborted, each response will reset the counter
		#endregion

		private delegate void StateChangedDelegate(ProcessState state);
		private event StateChangedDelegate StateChanged;

		public DownloadProcessor(GatewayProtocolPacket packet,
								PacketDatabaseInterface db,
								MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qd,
								string serverTime_DateFormat, int packetAbortTimeout)
		{
			this._serverTime_DateFormat = serverTime_DateFormat;
            this.packetResetValue = packetAbortTimeout;

            if (this.packetResetValue == 0)
            {
                this.packetResetValue = 90;
            }

            this.packetResponseCounter = this.packetResetValue;
			try
			{
				this.RequireUserToIssueCommands = true;
				this.State = ProcessState.Running;
				this.mPacket = packet;
				this.mDatabase = db;
				this.currentDownload = qd;
				this.bUserWantsToContinue = true;
				this.mDownloadType = qd.ListenerDownloadType;

				switch  (this.mDownloadType)
				{
					case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
						iPreferredDataLength = 256;
						break;
					case GatewayServiceBL.Classes.ListenerDownloadType.BinaryFile:
					case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
					case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
					default:
						break;
				}

				iTotalBlockCount = 0;
				iBlockCount = 1;

				iProgramChecksumTotal = 0;
				iPacketChecksumTotal = 0;
				iProgramBytesCountTotal = 0;
				mMotFile = null;
				mBinFile = null;
				mVerifyTimer = new System.Timers.Timer();
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "DownloadProcessor(GatewayProtocolPacket packet, PacketDatabaseInterface db, DownloadType type, string serverTime_DateFormat)", ex);
			}
		}

		public void BeginDownload(byte downloadVersion, GatewayServiceBL.Classes.QueuedDownload qd)
		{
            try
            {
                this.cDownloadVersion = downloadVersion;
                this.iDownloadRetryMaxCount = qd.RetryCount;
                this.iDownloadRetryIntervalSecs = qd.RetryInterval;

                _log.Debug("Begining to download to unit, Fleet: " + qd.FleetID.ToString() + ", Vehicle: " + qd.VehicleId.ToString());

                if (this.DownloadStart != null)
                {
                    DownloadStart(qd.ID, qd.FleetID, qd.VehicleId);
                }

                packetResponseTimer = new System.Timers.Timer();
                packetResponseTimer.Elapsed += packetResponseTimer_Elapsed;
                packetResponseTimer.Interval = 1000; // 1 second
                packetResponseTimer.AutoReset = true;
                

                // Try to see if we can open the file as a MOT, if so send it as one
                MotorolaSRecordFile motFile = new MotorolaSRecordFile(qd.FileData);

                if (motFile.FileIsGood)
                {
                    SendMOTFile(motFile);
                }
                else
                {
                    SendBinaryFile(qd.FileData);
                }
            }
            catch (Exception ex)
            {
                _log.Error("BeginDownload", ex);
            }
		}

        void packetResponseTimer_Elapsed(object sender, System.Timers.ElapsedEventArgs e)
        {
            packetResponseCounter -= 1;

            if (packetResponseCounter == 0)
            {
                _log.Debug(String.Format("Aborting download for F:{0}, V:{1}", mPacket.cFleetId, mPacket.iVehicleId));
                // We have elapsed sending and waiting for data send the abort
                if (DownloadStatusChanged != null)
                {
                    DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "The packet " + Convert.ToString(iBlockCount) + " was not recieved by the unit.");
                }

                mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
                UpdateState(ProcessState.Failed);

                if (FireAbortEvent != null)
                {
                    // This is so that we can get it out of the list of currentDownloads
                    FireAbortEvent(this, this.currentDownload.FleetID, this.currentDownload.VehicleId);
                }

                packetResponseTimer.Stop();

                try
                {
                    packetResponseTimer = null;
                }
                catch (Exception)
                {
                    _log.Debug("Could Not Dispose Packet Response Timer");
                }
            }
            else
            {
                _log.Debug(String.Format("F{0}, V{1}, Has {2} seconds before abort", this.mPacket.cFleetId, this.mPacket.iVehicleId, packetResponseCounter));
            }
        }

		private void SendMOTFile(MotorolaSRecordFile motFile)
		{
            string step = string.Empty;

            try
            {
                _log.Debug("Starting to send MOT File to unit");
                step = "Begin";
                mMotFile = motFile;

                byte[] dataBytes = null;

                #region Measure how many packets we'll need to do the download
                // A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
                step = "Step 1a";
                lThisAddress = mMotFile.GetAddressOfRow(2);
                lNextAddress = 0;
                dataBytes = new byte[0];

                step = "Step 1b";
                while (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
                {
                    iTotalBlockCount++;
                    // keep looping while there is data
                    lThisAddress = lNextAddress;
                }
                #endregion

                step = "Step 2";
                lThisAddress = mMotFile.GetAddressOfRow(2);

                lNextAddress = 0;

                dataBytes = new byte[0];

                step = "Step 3";
                if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
                {
                    MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;
                    switch (mDownloadType)
                    {
                        case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
                            pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
                            break;
                        case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
                            pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
                            break;
                        case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
                            pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
                            break;
                        default:
                            _log.Error("File Type Incorrect Found:" + mDownloadType);
                            break;
                    }

                    step = "Step 4";

                    SendFileData(pdp, dataBytes);
                }
            }
            catch (Exception ex)
            {
                _log.Error("SendMOTFile:" + step, ex);
            }
		}

		private void SendBinaryFile(byte[] fileData)
		{
            try
            {
                _log.Debug("Starting to send Binary File to unit");

                byte[] dataBytes = new byte[0];

                if (mBinFile == null)
                {
                    mBinFile = new cBinaryFile(fileData, ProgramDownloadPacket.DOWNLOAD_FILE_DATA_LENGTH);
                }

                if (mBinFile.sError != "")
                {
                    _log.Info("File Downloader : " + mBinFile.sError);
                }
                else
                {
                    iTotalBlockCount = mBinFile.TotalBlockCount;

                    lThisAddress = 0;
                    lNextAddress = 0;

                    MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket,
                        _serverTime_DateFormat);

                    dataBytes = mBinFile.NextBlock;

                    SendFileData(pdp, dataBytes);
                }
            }
            catch (Exception ex)
            {
                _log.Error("SendBinaryFile caused, " + ex.Message);
            }
		}

		private void SendFileData(MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp, byte[] dataBytes)
		{
			_log.Debug("Sending first packet to unit");
			pdp.cDownloadVersion = cDownloadVersion;
			pdp.iDownloadPacketNumber = iBlockCount;
			pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
			pdp.mDownloadData = dataBytes;
			// We can also do the program checksum for the Verify packet:
			iProgramChecksumTotal += SumOfBytes(dataBytes);
			// And count the program bytes:
			iProgramBytesCountTotal += dataBytes.Length;
			// Begin download and inform the packet ack system
			// that ContinueDownload wants to know when that first
			// download packet has been acked
			AcknowledgementNotificationDelegate del = null;

			if (iTotalBlockCount > 1)
			{
				del = new AcknowledgementNotificationDelegate(ContinueDownload);
			}
			else
			{
				del = new AcknowledgementNotificationDelegate(CompleteDownload);
			}

			// Update the status of the record within the downloads table so that if we are restarted we know where we were up to.
			System.Threading.ThreadStart updater = delegate { UpdateDownloadRecord(this.currentDownload.ID, iBlockCount, iTotalBlockCount, 0); };
			System.Threading.Thread updateThread = new System.Threading.Thread(updater);

			updateThread.Start();

            packetResponseTimer.Start();
			mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
			// Update the packet checksum total:
			iPacketChecksumTotal += pdp.iDownloadChecksum;
			// set up for ContinueDownload() to carry on...
			lThisAddress = lNextAddress;
			iBlockCount++;
		}

		[Obsolete("This function is no longer required", true)]
		public void BeginDownload(	byte cDownloadVersion, 
			int iDownloadRetryMaxCount,
			int iDownloadRetryIntervalSecs,
			MotorolaSRecordFile oFile)
		{
			byte[] dataBytes = null;
			bool bContinue = true;
			try
			{
				mMotFile = oFile;
				this.cDownloadVersion = cDownloadVersion;
				this.iDownloadRetryMaxCount = iDownloadRetryMaxCount;
				this.iDownloadRetryIntervalSecs = iDownloadRetryIntervalSecs;
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, MotorolaSRecordFile oFile)", ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
					#region Measure how many packets we'll need to do the download
					// A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
					lThisAddress = mMotFile.GetAddressOfRow(2);
					lNextAddress = 0;
					dataBytes = new byte[0];
			
					while (mMotFile.GetContiguousByteArray(
						lThisAddress,
						iPreferredDataLength, 
						ref dataBytes,
						ref lNextAddress, true, false) == true)
					{
						// Create a count so we can do a pretty little progress bar!
						iTotalBlockCount++;
						// keep looping while there is data
						lThisAddress = lNextAddress;
					}			
					#endregion
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, MotorolaSRecordFile oFile)", ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
					#region Break MOT file into packets of correct size (250B / 256B) and send the first
					lThisAddress = mMotFile.GetAddressOfRow(2);
					lNextAddress = 0;
					dataBytes = new byte[0];
					if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
					{					
						MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;
						switch (mDownloadType)
						{
							case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
								break;
							case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
								break;
							case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
								break;
						}
						pdp.cDownloadVersion = cDownloadVersion;
						pdp.iDownloadPacketNumber = iBlockCount;
						pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
						pdp.mDownloadData = dataBytes;
						// We can also do the program checksum for the Verify packet:
						iProgramChecksumTotal += SumOfBytes(dataBytes);
						// And count the program bytes:
						iProgramBytesCountTotal += dataBytes.Length;
						// Begin download and inform the packet ack system
						// that ContinueDownload wants to know when that first
						// download packet has been acked
						AcknowledgementNotificationDelegate del = null;
						if (iTotalBlockCount > 1)
						{
							del = new AcknowledgementNotificationDelegate(ContinueDownload);
						}
						else
						{
							del = new AcknowledgementNotificationDelegate(CompleteDownload);
						}
						mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
						// Update the packet checksum total:
						iPacketChecksumTotal += pdp.iDownloadChecksum;
						// set up for ContinueDownload() to carry on...
						lThisAddress = lNextAddress;
						iBlockCount++;
					}

					#endregion
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, MotorolaSRecordFile oFile)", ex);
			}
		}

		[Obsolete("This function is no longer required", true)]
		public void BeginDownload(byte cDownloadVersion,
			int iDownloadRetryMaxCount,
			int iDownloadRetryIntervalSecs,
			string fileName)
		{
			byte[] dataBytes = null;
			bool bContinue = true;
			try
			{
				this.cDownloadVersion = cDownloadVersion;
				this.iDownloadRetryMaxCount = iDownloadRetryMaxCount;
				this.iDownloadRetryIntervalSecs = iDownloadRetryIntervalSecs;

				if (mBinFile == null) mBinFile = new cBinaryFile(fileName, ProgramDownloadPacket.DOWNLOAD_FILE_DATA_LENGTH);
				if (mBinFile.sError != "")
				{
					_log.Info("File Downloader : " + mBinFile.sError);
					bContinue = false;
				}
				iTotalBlockCount = mBinFile.TotalBlockCount;
			}
			catch (System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, string fileName)", ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
					#region Break MOT file into packets of correct size (250B / 256B) and send the first
					lThisAddress = 0;
					lNextAddress = 0;
					dataBytes = new byte[0];

					MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
					dataBytes = mBinFile.NextBlock;
					pdp.cDownloadVersion = cDownloadVersion;
					pdp.iDownloadPacketNumber = iBlockCount;
					pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
					pdp.mDownloadData = dataBytes;
					// We can also do the program checksum for the Verify packet:
					iProgramChecksumTotal += SumOfBytes(dataBytes);
					// And count the program bytes:
					iProgramBytesCountTotal += dataBytes.Length;
					// Begin download and inform the packet ack system
					// that ContinueDownload wants to know when that first
					// download packet has been acked
					AcknowledgementNotificationDelegate del = null;
					if (iTotalBlockCount > 1)
					{
						del = new AcknowledgementNotificationDelegate(ContinueDownload);
					}
					else
					{
						del = new AcknowledgementNotificationDelegate(CompleteDownload);
					}
					mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
					// Update the packet checksum total:
					iPacketChecksumTotal += pdp.iDownloadChecksum;
					// set up for ContinueDownload() to carry on...
					lThisAddress = lNextAddress;
					iBlockCount++;


					#endregion
				}
			}
			catch (System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, string fileName)", ex);
			}
		}

		private void UpdateState(ProcessState state)
		{
			try
			{
				State = state;
				if (StateChanged != null)
				{
					StateChanged(State);
				}

				if (state == ProcessState.Failed || state == ProcessState.TimedOut)
				{
					mDatabase.SendFlushRequest(mPacket);
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "UpdateState(ProcessState state)", ex);
			}
		}

		private static int SumOfBytes(byte[] array)
		{
			int total = 0;

			try
			{
				foreach (byte b in array)
				{
					total += Convert.ToInt32(b);
				}
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}

			return total;
		}

		private void ContinueDownload(bool bLastPacketWasGood)
		{
            string step = "Start";
			bool bContinue = true;
			bool bFirstTimeForThisPacket = true;
			AcknowledgementNotificationDelegate del;
			byte[] dataBytes = new byte[0];


            // Reset the packetCounter
            _log.Debug("Resetting Packet Counter");
            packetResponseCounter = packetResetValue;

			if (!bUserWantsToContinue)
				return;

            _log.Debug(String.Format("Continue download F:{0},V:{1}", currentDownload.FleetID, currentDownload.VehicleId));

			try
			{
				if (!bLastPacketWasGood)
				{
                    _log.Error(String.Format("Aborting download F:{0},V:{1}", currentDownload.FleetID, currentDownload.VehicleId));
					if (DownloadStatusChanged != null)
					{
						DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "The packet " + Convert.ToString(iBlockCount) + " was not recieved by the unit.");
					}

                    mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
					UpdateState(ProcessState.Failed);

                    if (FireAbortEvent != null)
                    {
                        FireAbortEvent(this, this.currentDownload.FleetID, this.currentDownload.VehicleId);
                    }
					return;
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(step, ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
                    step = "Step 1";
					// Update the status of the record within the downloads table so that if we are restarted we know where we were up to.
                    //if (this.currentDownload != null)
                    //{
                        System.Threading.ThreadStart updater = delegate { UpdateDownloadRecord(this.currentDownload.ID, iBlockCount, iTotalBlockCount, 1); };
                        step = "step 1.1";
                        System.Threading.Thread updateThread = new System.Threading.Thread(updater);
                        step = "step 1.2";
                        updateThread.Start();
                    //}

                    step = "Step 1a";
					if (DownloadStatusChanged != null)
					{
						DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "Downloading packet " + Convert.ToString(iBlockCount) + " of " + Convert.ToString(iTotalBlockCount));
					}

                    step = "step 2";

                    _log.Debug("Download Type is : " + mDownloadType.ToString());

                    step = "step 2.0";
                    if (mDownloadType == GatewayServiceBL.Classes.ListenerDownloadType.BinaryFile)
					{
                        step = "step 2.1";
						MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
                        step = "step 2.2";
						dataBytes = mBinFile.NextBlock;
                        step = "step 2.3";
						pdp.cDownloadVersion = cDownloadVersion;
                        step = "step 2.4";
						pdp.iDownloadPacketNumber = iBlockCount;
                        step = "step 2.5";
						pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
                        step = "step 2.6";
						pdp.mDownloadData = dataBytes;
                        step = "step 2.7";
						if (bFirstTimeForThisPacket)
						{
							iProgramChecksumTotal += SumOfBytes(dataBytes);
							iProgramBytesCountTotal += dataBytes.Length;
						}
						if (iBlockCount != iTotalBlockCount)
						{
							del = new AcknowledgementNotificationDelegate(ContinueDownload);
						}
						else
						{
							del = new AcknowledgementNotificationDelegate(CompleteDownload);
						}
						mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
						iPacketChecksumTotal += pdp.iDownloadChecksum;
						iBlockCount++;
					}
					else if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, (iBlockCount == iTotalBlockCount))	== true)
					{
                        step = "Step 3";
						MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;				
						switch (mDownloadType)
						{
							case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
								break;
							case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
								break;
							case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
								break;
						}
						pdp.cDownloadVersion = cDownloadVersion;
						pdp.iDownloadPacketNumber = iBlockCount;		
						pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
						if (iBlockCount == iTotalBlockCount) pdp.iDownloadAddress = Convert.ToInt32(lNextAddress & 0xFFFFFF);
						pdp.mDownloadData = dataBytes;
						if (bFirstTimeForThisPacket)
						{
							iProgramChecksumTotal += SumOfBytes(dataBytes);
							iProgramBytesCountTotal += dataBytes.Length;
						}
						if (iBlockCount != iTotalBlockCount) 
						{
							del = new AcknowledgementNotificationDelegate(ContinueDownload);
						}
						else
						{
							del = new AcknowledgementNotificationDelegate(CompleteDownload);
						}
						mDatabase.SendProgramDownload(	pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
						iPacketChecksumTotal += pdp.iDownloadChecksum;
						lPreviousAddress = lThisAddress;
						lThisAddress = lNextAddress;
						iBlockCount++;
					}
					else
					{
                        step = "Step 4";
						if (DownloadStatusChanged != null)
						{
							DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "Failed to read block " + Convert.ToString(iBlockCount) + " fom the MOT file.  Please check this file is not corrupt.");
						}

                        mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
						UpdateState(ProcessState.Failed);

                        if (FireAbortEvent != null)
                        {
                            FireAbortEvent(this, this.currentDownload.FleetID, this.currentDownload.VehicleId);
                        }
						return;
					}			
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(step, ex);
				bContinue = false;
			}
		}

		private void UpdateDownloadRecord(int downloadId, int bytesSent, int totalBytes, int updateType)
		{
            _log.Debug(String.Format("Updating Record for DL:{0}, BS:{1}, TB:{2}", downloadId, bytesSent, totalBytes));

            try
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.UpdateDownloadStatus(downloadId, bytesSent, totalBytes, updateType);
            }
            catch (Exception ex)
            {
                _log.Error("UpdateDownloadRecord", ex);
            }
		}

		private void CompleteDownload(bool bLastPacketWasGood)
		{
			_log.Debug("Sending complete download for unit");
			bool bContinue = true;
			AcknowledgementNotificationDelegate del = null;


            // Reset the packetCounter then stop the timer
            _log.Debug("Resetting Packet Counter");
            packetResponseCounter = packetResetValue;

            packetResponseTimer.Stop();

			if (!bUserWantsToContinue)
				return;

			if (!bLastPacketWasGood)
			{
				try
				{
					// Last packet was not ACKed!
					if (DownloadStatusChanged != null)
					{
						DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "The packet " + Convert.ToString(iBlockCount) + " was not recieved by the unit.");
					}

                    mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
					UpdateState(ProcessState.Failed);

                    if (FireAbortEvent != null)
                    {
                        FireAbortEvent(this, this.currentDownload.FleetID, this.currentDownload.VehicleId);
                    }
					return;
				}
				catch(System.Exception ex)
				{
					_log.Error(sClassName + "CompleteDownloadNoForm(bool bLastPacketWasGood)", ex);
					bContinue = false;
				}
			}
			try
			{
				if (bContinue)
				{
					if (DownloadStatusChanged != null)
					{
						DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "Sending verify packet to unit.");
					}

					ProgramVerifyPacket pvp = new ProgramVerifyPacket(mPacket, _serverTime_DateFormat);
					switch (mDownloadType)
					{
						case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
							break;
						case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_IOBOX_VERIFY;
							break;
						case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_MDT_VERIFY;
							break;
						case GatewayServiceBL.Classes.ListenerDownloadType.BinaryFile:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
							break;
						default:
							break;
					}
					pvp.cDownloadVersion = cDownloadVersion;
					pvp.iProgramChecksumTotal = iProgramChecksumTotal;
					pvp.iPacketChecksumTotal = iPacketChecksumTotal;
					pvp.iPacketCountTotal = iTotalBlockCount;
					pvp.iProgramBytesCountTotal = iProgramBytesCountTotal;
					// The next ACK indicates a successful verify!
					del = new AcknowledgementNotificationDelegate(VerifyComplete);
					mDatabase.SendVerifyDownload(pvp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
					mVerifyTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.VerifyTimerElapsed);
					mVerifyTimer.Interval = 20 * 1000; // 20 seconds
					mVerifyTimer.AutoReset = false;
					mVerifyTimer.Start();
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "CompleteDownloadNoForm(bool bLastPacketWasGood)", ex);
				bContinue = false;
			}
		}

		private void VerifyComplete(bool bLastPacketWasGood)
		{
			bool bContinue = true;

			_log.Debug(String.Format("Recieved Verify Complete from unit:F:{0},V:{1}", currentDownload.FleetID, currentDownload.VehicleId));

			if (!bUserWantsToContinue)
				return;

			if (!bLastPacketWasGood)
			{
				try
				{
					mVerifyTimer.Stop();
					mVerifyTimer.Elapsed -= new System.Timers.ElapsedEventHandler(VerifyTimerElapsed);
					mVerifyTimer = null;
				}
				catch(System.Exception ex)
				{
					bContinue = false;
					_log.Error(sClassName + "VerifyCompleteNoForm(bool bLastPacketWasGood)", ex);
				}
				try
				{
					if(bContinue)
					{
						if (DownloadStatusChanged != null)
						{
							DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "Unit did not verify the download correctly.");
						}

						_log.Info(String.Format("Unit did not verify the download correctly, sending abort F:{0},V:{1}", currentDownload.FleetID, currentDownload.VehicleId));

                        mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);

						UpdateState(ProcessState.Failed);

						if (DownloadComplete != null)
						{
							_log.Debug("Sending download complete from abort");
							DownloadComplete(currentDownload.ID);
						}

                        if (FireAbortEvent != null)
                        {
                            FireAbortEvent(this, this.currentDownload.FleetID, this.currentDownload.VehicleId);
                        }
						return;
					}
				}
				catch(System.Exception ex)
				{
					bContinue = false;
					_log.Error(sClassName + "VerifyCompleteNoForm(bool bLastPacketWasGood)", ex);
				}
			}
			try
			{
				if(bContinue)
				{
					UpdateState(ProcessState.Successful);

					if (DownloadStatusChanged != null)
					{
						DownloadStatusChanged(mPacket.cFleetId, Convert.ToInt32(mPacket.iVehicleId), "Download Complete.");
					}

					if (DownloadComplete != null)
					{
						_log.Debug("Sending download complete from success");
						DownloadComplete(currentDownload.ID);
					}
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "VerifyCompleteNoForm(bool bLastPacketWasGood)", ex);
			}
		}

		private void VerifyTimerElapsed(object o, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				mVerifyTimer.Stop();
				// Call the VerifyComplete function as if we got an ACK!
				VerifyComplete(true);
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "VerifyTimerElapsedNoForm(object o, System.Timers.ElapsedEventArgs e)", ex);
			}
		}


		// This function is called when the previous packet
		// has been acknowledged and the far end is ready 
		// to send the next one.
		[Obsolete("This function is no longer required", true)]
		private void ContinueDownloadForm(bool bLastPacketWasGood)
		{
			try
			{
				SafeContinueDownload(bLastPacketWasGood);
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "ContinueDownload(bool bLastPacketWasGood)", ex);
			}
		}

		/// <summary>
		/// This method will be called in the GUI thread.
		/// </summary>
		/// <param name="bLastPacketWasGood"></param>
		private void SafeContinueDownload(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			bool bFirstTimeForThisPacket = true;
			AcknowledgementNotificationDelegate del = null;
			MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;
			byte[] dataBytes = new byte[0];

			try
			{
				if (!bUserWantsToContinue)
				{
					mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
					State = ProcessState.Failed;
					return;
				}

				if (!bLastPacketWasGood)
				{
                    mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
					UpdateState(ProcessState.Failed);
					return;
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
				_log.Error(sClassName + "SafeContinueDownload(bool bLastPacketWasGood)", ex);
			}

			try
			{
				if (bContinue)
				{
					#region Download next block
					if (mDownloadType == GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice || mDownloadType == GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox || mDownloadType == GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal)
					{
						if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, (iBlockCount == iTotalBlockCount)) == true)
						{
							switch (mDownloadType)
							{
								case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
									break;
								case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
									break;
								case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
									break;
							}
						}
					}
					else
					{
						pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
						//pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket);
						dataBytes = mBinFile.NextBlock;
					}
					pdp.cDownloadVersion = cDownloadVersion;
					pdp.iDownloadPacketNumber = iBlockCount;		
					pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
					// For the last page, use the front-padded version:
					if (iBlockCount == iTotalBlockCount) pdp.iDownloadAddress = Convert.ToInt32(lNextAddress & 0xFFFFFF);

					pdp.mDownloadData = dataBytes;

					if (bFirstTimeForThisPacket)
					{
						// We can also do the program checksum for the Verify packet:
						iProgramChecksumTotal += SumOfBytes(dataBytes);
						// And count the program bytes:
						iProgramBytesCountTotal += dataBytes.Length;
					}

					// Begin download and inform the packet ack system
					// that ContinueDownload wants to know when that first
					// download packet has been acked
					if (iBlockCount != iTotalBlockCount) 
					{
						del = new AcknowledgementNotificationDelegate(ContinueDownload);
					}
					else
					{
						// Last packet is about to go!
						// Change the delegate to CompleteDownload to finish
						del = new AcknowledgementNotificationDelegate(CompleteDownload);
					}
					mDatabase.SendProgramDownload(	pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
					// Update the packet checksum total:
					iPacketChecksumTotal += pdp.iDownloadChecksum;
					// set up for ContinueDownload() to carry on...
					lPreviousAddress = lThisAddress;
					lThisAddress = lNextAddress;
					// Move to the next block ID.
					iBlockCount++;
					#endregion
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "SafeContinueDownload(bool bLastPacketWasGood)", ex);
			}
		}

		public void AbortDownload()
		{
			try
			{
				bUserWantsToContinue = false;
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "AbortDownload()", ex);
			}
		}

		[Obsolete("This function is no longer required", true)]
		private void CompleteDownloadForm(bool bLastPacketWasGood)
		{
			try
			{
				SafeCompleteDownload(bLastPacketWasGood);
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "CompleteDownload(bool bLastPacketWasGood)", ex);
			}
		}

		// This function is called when the last packet of the 
		// program download has been acknowledged, and all that
		// remains is to send a "Verify" packet which will
		// cause the remote device to verify the download, transfer
		// the program into the execution area and reboot!
		private void SafeCompleteDownload(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			ProgramVerifyPacket pvp = null;
			AcknowledgementNotificationDelegate del = null;

			try
			{
				if (!bLastPacketWasGood)
				{
					if (mDownloadType == GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal)
					{	// We can tolerate a few errors in an MDT download,
						// because the IrDA channel is error-prone:
						SafeContinueDownload(false);
						return;
					}
					else
					{
						// Last packet was not ACKed!
                        mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
						UpdateState(ProcessState.Failed);

                        if (FireAbortEvent != null)
                        {
                            FireAbortEvent(this, this.currentDownload.FleetID, this.currentDownload.VehicleId);
                        }

						return;
					}
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
				_log.Error(sClassName + "SafeCompleteDownload(bool bLastPacketWasGood)", ex);
			}

			try
			{
				if (bContinue)
				{
					// Update the status of the record within the downloads table so that if we are restarted we know where we were up to.
					System.Threading.ThreadStart updater = delegate { UpdateDownloadRecord(this.currentDownload.ID, iBlockCount, iTotalBlockCount, 2); };
					System.Threading.Thread updateThread = new System.Threading.Thread(updater);

					updateThread.Start();

					// Create a "Verify" packet:
					pvp = new ProgramVerifyPacket(mPacket, _serverTime_DateFormat);
					switch (mDownloadType)
					{
						case GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
							break;
						case GatewayServiceBL.Classes.ListenerDownloadType.InterfaceBox:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_IOBOX_VERIFY;
							break;
						case GatewayServiceBL.Classes.ListenerDownloadType.MobileDataTerminal:
						case GatewayServiceBL.Classes.ListenerDownloadType.BinaryFile:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_MDT_VERIFY;
							break;
						default:
							break;
					}
					pvp.cDownloadVersion = cDownloadVersion;
					pvp.iProgramChecksumTotal = iProgramChecksumTotal;
					pvp.iPacketChecksumTotal = iPacketChecksumTotal;
					pvp.iPacketCountTotal = iTotalBlockCount;
					pvp.iProgramBytesCountTotal = iProgramBytesCountTotal;

					_log.Info("Sending Verify Packet : Program Checksum = " + iProgramChecksumTotal + ", Packet Checksum : " + iPacketChecksumTotal + ", Packet Count : " + iTotalBlockCount + ", Program Bytes Count : " + iProgramBytesCountTotal);
					// The next ACK indicates a successful verify!
					del = new AcknowledgementNotificationDelegate(VerifyComplete);
					mDatabase.SendVerifyDownload(pvp, del, 2, 10);
					// 302x devices don't ack, so if we go 20 seconds without hearing
					// anything, assume success!
					if ((!RequireUserToIssueCommands) && (mDownloadType == GatewayServiceBL.Classes.ListenerDownloadType.TrackingDevice))
					{
						mVerifyTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.VerifyTimerElapsed);
						mVerifyTimer.Interval = 20 * 1000; // 20 seconds
						mVerifyTimer.AutoReset = false;
						mVerifyTimer.Start();
					}
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
				_log.Error(sClassName + "SafeCompleteDownload(bool bLastPacketWasGood)", ex);
			}
		}

		[Obsolete("This function is no longer required", true)]
		private void VerifyTimerElapsedForm(object o, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				mVerifyTimer.Stop();
				// Call the VerifyComplete function as if we got an ACK!
				VerifyComplete(true);
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "VerifyTimerElapsed(object o, System.Timers.ElapsedEventArgs e)", ex);
			}
		}

		[Obsolete("This function is no longer required", true)]
		private void VerifyCompleteForm(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			try
			{
				if (!bLastPacketWasGood)
				{
                    mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
					UpdateState(ProcessState.Failed);
					return;
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
				_log.Error(sClassName + "VerifyComplete(bool bLastPacketWasGood)", ex);
			}

			try
			{
				if(bContinue)
				{
					if (RequireUserToIssueCommands)
					{
                        mDatabase.AbortProgramDownload(this.mPacket.cFleetId, this.mPacket.iVehicleId);
					}
					UpdateState(ProcessState.Successful);
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
				_log.Error(sClassName + "VerifyComplete(bool bLastPacketWasGood)", ex);
			}
		}
	}
}

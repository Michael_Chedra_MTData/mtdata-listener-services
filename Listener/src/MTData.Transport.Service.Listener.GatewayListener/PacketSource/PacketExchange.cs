using System;
using System.Threading;
using System.Collections;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener.PacketSource
{
	/// <summary>
	/// This class will take in received packets for a unit, and determine if there
	/// is pending outbound packets based on the packet sources registered.
	/// </summary>
	public class PacketExchange
	{
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.PacketSource.PacketExchange.";
        private static ILog _log = LogManager.GetLogger(typeof(PacketExchange));

		/// <summary>
		/// This is a map of the registered sources, by Priority
		/// </summary>
		private Hashtable _sourceMap = new Hashtable();

		/// <summary>
		/// This is a list of the registered sources.
		/// </summary>
		private ArrayList _sourceList = new ArrayList();

		/// <summary>
		/// This is a map of the pending packets by fleet and vehicleid
		/// </summary>
		private Hashtable _pendingPackets = new Hashtable();

		/// <summary>
		/// Synchronisation object
		/// </summary>
		private object _syncRoot = new object();

		/// <summary>
		/// This timer will process timeouts on packets.
		/// </summary>
		private System.Threading.Timer _timout = null;

		#region Vehicle Key

		private class VehicleKey 
		{
			private int _fleetID = 0;
			private int _unitID = 0;

			public int FleetID { get{ return _fleetID; }}
			public int UnitID {get{ return _unitID; }}

			public VehicleKey(int fleetID, int unitID)
			{
				_fleetID = fleetID;
				_unitID = unitID;
			}

			public override bool Equals(object obj)
			{
				if (obj is VehicleKey)
				{
					return (((VehicleKey)obj).FleetID == this.FleetID) && (((VehicleKey)obj)._unitID == this._unitID);
				}
				else
					return base.Equals (obj);
			}

			public override int GetHashCode()
			{
				long key = (long)this._fleetID << 16;
				key += this._unitID;

				return key.GetHashCode(); 
			}
		}

		#endregion

		#region Transmit Request

		private class TransmitRequest
		{
			private DateTime _UTCTransmitTime;
			private IPacketEnvelope _envelope = null;
			private DateTime _UTCExpiryTime = DateTime.MinValue;

			public DateTime UTCTransmitTime {get{ return _UTCTransmitTime; }}
			public IPacketEnvelope Envelope { get{ return _envelope; }}
			public DateTime UTCExpiryTime {get{ return _UTCExpiryTime; }}
			
			public TransmitRequest(IPacketEnvelope envelope)
			{
				_UTCTransmitTime = DateTime.UtcNow;
				_envelope = envelope;
				if (envelope.RetryCount == 0)
					_UTCExpiryTime = _UTCTransmitTime.AddSeconds(15);
				else
					_UTCExpiryTime = _UTCTransmitTime.AddSeconds((envelope.RetryCount + 1) * envelope.RetryInterval);
			}
		}

		#endregion

		/// <summary>
		/// Initialise the exchange
		/// </summary>
        public PacketExchange()
		{
			_timout = new System.Threading.Timer(new System.Threading.TimerCallback(PacketTimeoutHandler), null, 500, 500);
		}

		/// <summary>
		/// Register a packet source for the system
		/// </summary>
		/// <param name="source"></param>
		public void RegisterPacketSource(IPacketSource source)
		{
			_sourceList.Add(source);
			PacketPriority priority = source.Priority;
			ArrayList list = (ArrayList)_sourceMap[priority];
			if (list == null)
			{
				list = new ArrayList();
				_sourceMap.Add(priority, list);
			}

			list.Add(source);
		}

		/// <summary>
		/// Remove a packet source from the list.
		/// </summary>
		/// <param name="source"></param>
		public void UnRegisterPacketSource(IPacketSource source)
		{
			if (_sourceList.Contains(source))
				_sourceList.Remove(source);
			ArrayList list = (ArrayList)_sourceMap[source.Priority];
			if ((list != null) && (list.Contains(source)))
				list.Remove(source);
		}

		/// <summary>
		/// This method will retriev ethe next packet for the vehicle from the registered
		/// sources. IT iterates the sources based on Priority.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="unitID"></param>
		/// <returns></returns>
		private IPacketEnvelope GetNextPacket(int fleetID, int unitID)
		{
			if ((fleetID == 0) || (unitID == 0))
				return null;

			IPacketEnvelope result = null;

			foreach(PacketPriority priority in Enum.GetValues(typeof(PacketPriority)))
			{
				ArrayList sources = (ArrayList)_sourceMap[priority];
				if (sources != null)
				{
					foreach(IPacketSource source in sources)
					{
						result = source.GetPendingPacketForUnit(fleetID, unitID);
						if (result != null)
							break;
					}
				}
				if (result != null)
					break;
			}
			return result;
		}

		private AutoResetEvent _waitHandle = new AutoResetEvent(true);		

		/// <summary>
		/// This is called whenever a packet is received from a vehicle, and we need to check 
		/// to see if we have to send a new packet.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="unitID"></param>
		public void PacketReceived(int fleetID, int unitID, MTData.Transport.Gateway.Packet.GatewayProtocolPacket packet, VehicleCollection vehicles, Vehicle vehicle)
		{
			if ((fleetID == 0) || (unitID == 0))
				return; 

			_waitHandle.WaitOne();
			try
			{
				VehicleKey key = new VehicleKey(fleetID, unitID);
				TransmitRequest request = null;
				lock(_syncRoot)
					request = (TransmitRequest)_pendingPackets[key];
				if (request == null)
				{
                    // If we are not currently sending something to the vehicle and this is not a route update accepted packet
                    if (packet.cMsgType != MTData.Transport.Gateway.Packet.RoutingModuleGPPacket.ROUTES_UPDATE_ACCEPTED)
                    {
                        //	Retrieve the next packet to send.
                        IPacketEnvelope envelope = GetNextPacket(fleetID, unitID);
                        if (envelope != null)
                        {
                            request = new TransmitRequest(envelope);

                            envelope.Packet.mSenderIP = packet.mSenderIP;

                            //	now try to transmit it.
                            if (!vehicles.UpdateVehicle(vehicle, envelope.Packet, false, false))
                                envelope.Source.PacketAborted(envelope);
                            else
                            {
                                byte[] encodedBytes = null;
                                envelope.Packet.Encode(ref encodedBytes);
                                _log.Info(string.Format("Queued Envelope : Fleet {0}; Unit {1}; Data : {2}", vehicle.cFleet, vehicle.iUnit, MTData.Transport.Service.Listener.GatewayListener.Vehicle.ConvertToAscii(encodedBytes)));
                                // And send as needed:
                                if (envelope.RetryCount > 0)
                                    vehicle.SendWithRetries(envelope.RetryCount, envelope.RetryInterval);
                                else
                                    vehicle.SendWithNoRetries();

                                //	register this against the vehicle
                                lock (_syncRoot)
                                    _pendingPackets[key] = request;
                            }
                        }
                    }
				}
			}
			finally
			{
				_waitHandle.Set();
			}
		}

		/// <summary>
		/// This is called whenver a packet is acked successfully
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="unitID"></param>
		public void PacketAcked(int fleetID, int unitID)
		{
			VehicleKey key = new VehicleKey(fleetID, unitID);
			TransmitRequest request = null;
			lock(_syncRoot)
			{
				request = (TransmitRequest)_pendingPackets[key];
				if (request != null)
					_pendingPackets.Remove(key);
			}
			if (request != null)
				request.Envelope.Source.PacketAcked(request.Envelope);

		}

		/// <summary>
		/// When a send is aborted, this method will be called.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="unitID"></param>
		/// <param name="packet"></param>
		public void PacketAborted(int fleetID, int unitID, MTData.Transport.Gateway.Packet.GatewayProtocolPacket packet)
		{
			VehicleKey key = new VehicleKey(fleetID, unitID);
			TransmitRequest request = null;
			lock(_syncRoot)
			{
				request = (TransmitRequest)_pendingPackets[key];
				if (request != null)
					_pendingPackets.Remove(key);
			}
			if (request != null)
				request.Envelope.Source.PacketAborted(request.Envelope);
		}

		/// <summary>
		/// This method will be called by the timer to check timeouts on transmitted packets
		/// </summary>
		/// <param name="state"></param>
		private void PacketTimeoutHandler(object state)
		{
			ArrayList _expiredRequests = new ArrayList();
			ArrayList _expiredKeys = new ArrayList();
			lock(_syncRoot)
			{
				DateTime checkDate = DateTime.UtcNow;

				foreach(DictionaryEntry entry in _pendingPackets)
				{
					if (((TransmitRequest)entry.Value).UTCExpiryTime < checkDate)
					{
						_expiredRequests.Add(entry.Value);
						_expiredKeys.Add(entry.Key);
					}
				}

				foreach(VehicleKey key in _expiredKeys)
					_pendingPackets.Remove(key);
			}

			foreach(TransmitRequest request in _expiredRequests)
				request.Envelope.Source.PacketAborted(request.Envelope);
		}
	}
}

using System;

namespace MTData.Transport.Service.Listener.GatewayListener.PacketSource
{
	public enum PacketPriority
	{
		High,			//	This source gets priority treatment
		Med,			//	This source gets shared.. ie when no High left
		Low				//	This source gets low priority.. ie when no med left
	}

	/// <summary>
	/// This interface identifies a mechanism that can supply packets for a vehicle.
	/// </summary>
	public interface IPacketSource
	{
		string Name {get;}
		PacketPriority	Priority {get;}
		IPacketEnvelope GetPendingPacketForUnit(int fleetID, int unitID);
		void PacketAcked(IPacketEnvelope envelope);
		void PacketAborted(IPacketEnvelope envelope);
	}
}

using System;

namespace MTData.Transport.Service.Listener.GatewayListener.PacketSource
{
	/// <summary>
	/// This is a standard implementation of the packet envelope.
	/// </summary>
	public class PacketEnvelopeStandard : IPacketEnvelope
	{
		private object _context = null;
		private int _fleetID;
		private int _unitID;
		private MTData.Transport.Gateway.Packet.GatewayProtocolPacket _packet;
		private IPacketSource _source;
		private int _retryCount = 0;
		private int _retryInterval = 0;

		public PacketEnvelopeStandard(
			object context, 
			int fleetID, 
			int unitID, 
			MTData.Transport.Gateway.Packet.GatewayProtocolPacket packet,
            IPacketSource source,
			int retryCount,
			int retryInterval)
		{
			_context = context;
			_fleetID = fleetID;
			_unitID = unitID;
			_packet = packet;
			_source = source;
			_retryCount = retryCount;
			_retryInterval = retryInterval;
		}

		#region IPacketEnvelope Members

		public object Context
		{
			get
			{
				return _context;
			}
		}

		public int FleetID
		{
			get
			{
				return _fleetID;
			}
		}

		public int UnitID
		{
			get
			{
				return _unitID;
			}
		}

		public MTData.Transport.Gateway.Packet.GatewayProtocolPacket Packet
		{
			get
			{
				return _packet;
			}
		}

		public IPacketSource Source
		{
			get
			{
				return _source;
			}
		}

		public int RetryCount
		{
			get
			{
				return _retryCount;
			}
		}

		public int RetryInterval
		{
			get
			{
				return _retryInterval;
			}
		}

		#endregion
	}
}

using System;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Service.Listener.GatewayListener.PacketSource
{
	/// <summary>
	/// This interface identifies the packet request.
	/// </summary>
	public interface IPacketEnvelope
	{
		object Context {get;}
		int FleetID {get;}
		int UnitID {get;}
		GatewayProtocolPacket Packet {get;}
		IPacketSource Source {get;}
		int RetryCount {get;}
		int RetryInterval {get;}
	}
}

using System;
using System.Net;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// Summary description for cClientMsg.
	/// </summary>
	public class ClientMsg
	{
		#region Vars
		private IPEndPoint oEndPoint = null;
		private byte[] bData = null;
		#endregion
		#region Properties
		public byte[] Data	{get	{return bData;}}
		public IPEndPoint EndPoint	{get	{return oEndPoint;}}
		#endregion
		#region Constructor
		public ClientMsg(IPEndPoint endPoint, byte[] contents)
		{
			oEndPoint = endPoint;
			bData = contents;
		}
		#endregion
	}
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Threading;
using System.Xml;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener.Dashboard
{
    public class DashboardProcessing : System.Configuration.IConfigurationSectionHandler
    {
        public delegate void DashInterfaceDelegate(int queueSize);
        public delegate void DashInterfaceUpdateCompleteEvent(long ticksUpdateTime, bool isFail);
        public DashInterfaceUpdateCompleteEvent eDashboardUpdateComplete;
        #region Private Variables
        private DashInterfaceDelegate _dashInterfaceDel = null;
        
        private static ILog _log = LogManager.GetLogger(typeof(DashboardProcessing));
        private bool _running;
        private bool _stopping;
        private bool _stopped = true;
        private string _dsn;
        private int _loopTimer;
        private int _processLastXHours;
        private Thread _mainThread;
        private object _listSync;
        private List<int> _fleetList;
        private List<int> _updateList;
        private bool _commandComplete;
        private int _queueSize;
        private int _dontProcessWhenQueueIsGreaterThan;
        #endregion
        #region Public Properties
        public string DSN
        {
            get
            {
                return _dsn;
            }
            set
            {
                _dsn = value;
            }
        }
        public bool IsRunning
        {
            get
            {
                return _running;
            }
        }
        public bool Stopping
        {
            get
            {
                return _stopping;
            }
        }
        public bool Stopped
        {
            get
            {
                return _stopped;
            }
        }
        public int ExecuteEveryXMinutes
        {
            get
            {
                return (_loopTimer / 600);
            }
            set
            {
                _loopTimer = (value * 600);
            }
        }
        public int[] ExecuteForFleets
        {
            set
            {
                if (value != null)
                {
                    for (int X = 0; X < value.Length; X++)
                    {
                        _fleetList.Add(value[X]);
                    }
                }
            }

            get
            {
                int[] ret = new int[_fleetList.Count];
                _fleetList.CopyTo(ret, 0);
                return ret;
            }
        }
        public int ProcessLastXHours
        {
            get
            {
                return _processLastXHours;
            }
            set
            {
                _processLastXHours = value;
            }
        }
        #endregion
        #region Public Methods
        public DashboardProcessing()
        {
            try
            {
                _queueSize = 0;
                _dontProcessWhenQueueIsGreaterThan = 1000;
                _dashInterfaceDel = new DashInterfaceDelegate(DashInterfaceEvent);
                _dsn = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                _listSync = new object();
                _fleetList = new List<int>();
                _updateList = new List<int>();
                _loopTimer = 18000; // Every 30 mins
                _mainThread = new Thread(new ThreadStart(MainThread));
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".DashboardProcessing()", ex);
                throw new System.Exception(this.GetType().FullName + ".DashboardProcessing()", ex);
            }

        }
        public DashboardProcessing(int executeEveryXMinutes, int[] executeForFleets, bool startThread)
        {
            try
            {
                _queueSize = 0;
                _dontProcessWhenQueueIsGreaterThan = 1000;
                _dashInterfaceDel = new DashInterfaceDelegate(DashInterfaceEvent);
                _dsn = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                // Convert executeEveryXMinutes to 100ms blocks
                _loopTimer = (executeEveryXMinutes * 600);
                _listSync = new object();
                _fleetList = new List<int>();
                _updateList = new List<int>();
                _mainThread = new Thread(new ThreadStart(MainThread));
                ExecuteForFleets = executeForFleets;
                if (startThread)
                    Start();
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".DashboardProcessing(int executeEveryXMinutes = " + executeEveryXMinutes + ", int[] executeForFleets = [" + executeForFleets.Length + " items], bool startThread = " + (startThread?"True":"False") + ")", ex);
                throw new System.Exception(this.GetType().FullName + ".DashboardProcessing(int executeEveryXMinutes = " + executeEveryXMinutes + ", int[] executeForFleets = [" + executeForFleets.Length + " items], bool startThread = " + (startThread ? "True" : "False") + ")", ex);
            }
        }
        public void Dispose()
        {
            try
            {
                this.Stop();
                _mainThread = null;
                lock (_listSync)
                {
                    _updateList = null;
                    _fleetList = null;
                }
                _listSync = null;
                _dashInterfaceDel = null;
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Dispose()", ex);
                throw new System.Exception(this.GetType().FullName + ".Dispose()", ex);
            }
        }
        public void Start()
        {
            try
            {
                _stopped = false;
                _stopping = false;
                _mainThread.Start();
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Start()", ex);
                throw new System.Exception(this.GetType().FullName + ".Start()", ex);
            }
        }
        public void Stop()
        {
            try
            {
                if (_running)
                {
                    _stopping = true;
                    int timeout = 100;
                    while (!_stopped && timeout > 0)
                    {
                        Thread.Sleep(50);
                        timeout--;
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".Stop()", ex);
                throw new System.Exception(this.GetType().FullName + ".Stop()", ex);
            }
        }
        public void UpdateRecvdFromFleet(int fleetId)
        {
            try
            {
                lock (_listSync)
                {
                    if (_fleetList.Contains(fleetId) && !_updateList.Contains(fleetId))
                        _updateList.Add(fleetId);
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateRecvdFromFleet(int fleetId = " + fleetId + ")", ex);
            }
        }
        public DashInterfaceDelegate DashInterface
        {
            get
            {
                return _dashInterfaceDel;
            }
        }
        #endregion
        #region Private Methods (Thread Code)
        private void DashInterfaceEvent(int queueSize)
        {
            try
            {
                lock (_listSync)
                {
                    _queueSize = queueSize;
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".DashInterfaceEvent(int queueSize)", ex);
                throw new System.Exception(this.GetType().FullName + ".DashInterfaceEvent(int queueSize)", ex);
            }
        }

        /// <summary>
        /// This method is called when the BeginInvoke methods used in this class are completed.
        /// This function cleans up memory for the async call delegate.
        /// </summary>
        /// <param name="IAsyncResult ar">This is result of the delegate run.</param>
        private void AsyncDashboardProcessingCallBack(IAsyncResult ar)
        {
            try
            {
                if (ar.AsyncState is DashInterfaceDelegate)
                    (ar.AsyncState as DashInterfaceDelegate).EndInvoke(ar);
                else if (ar.AsyncState is DashInterfaceUpdateCompleteEvent)
                    (ar.AsyncState as DashInterfaceUpdateCompleteEvent).EndInvoke(ar);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncDashboardProcessingCallBack(IAsyncResult ar)", ex);
            }
        }

        private void MainThread()
        {
            string sqlCmdString = "EXEC dsp_ProcessTrackingDashDeltas '[StartDate]', '[EndDate]', [FleetID]";
            try
            {
                _log.Info("Dashboard Processing : Started dashboard data processing thread - First run in 5 mins.");
                int counter = 3000;
                int queueSize = 0;
                _running = true;
                while(!_stopping)
                {
                    try
                    {
                        counter--;
                        if (counter == 0)
                        {
                            counter = _loopTimer;
                            int[] executeForFleetIds = null;
                            lock (_listSync)
                            {
                                queueSize = _queueSize;
                                executeForFleetIds = _updateList.ToArray();
                                _updateList.Clear();
                            }

                            // If we can't process at the moment, then display the reason why.
                            if (queueSize > _dontProcessWhenQueueIsGreaterThan)
                            {
                                _log.Info("Dashboard Processing : Did not process dash data due to listener size being too large (Current : " + queueSize + ", Processing Max : " + _dontProcessWhenQueueIsGreaterThan + "), will try again in 5 mins.");
                                counter = 3000;
                            }
                            else if (executeForFleetIds == null || executeForFleetIds.Length == 0)
                            {
                                _log.Info("Dashboard Processing : No reports from any nominated fleets since the last processing period, will check again in 5 mins.");
                                counter = 3000;
                            }
                            else if (queueSize < _dontProcessWhenQueueIsGreaterThan && executeForFleetIds != null && executeForFleetIds.Length > 0)
                            {
                                DateTime dtNow = DateTime.Now.ToUniversalTime();
                                DateTime dtTemp = new DateTime(dtNow.Year, dtNow.Month, dtNow.Day, dtNow.Hour, (dtNow.Minute >= 30?30:0), 0);
                                dtTemp = dtTemp.AddMinutes(-30);
                                string sqlString = sqlCmdString.Replace("[EndDate]", dtTemp.ToString("MM/dd/yyyy HH:mm:ss"));
                                dtTemp = dtTemp.AddHours(-1 * _processLastXHours);
                                sqlString = sqlString.Replace("[StartDate]", dtTemp.ToString("MM/dd/yyyy HH:mm:ss"));
                                SqlConnection conn = null;
                                SqlCommand cmd = null;
                                foreach (int fleetId in executeForFleetIds)
                                {
                                    if (!_stopping)
                                    {
                                        _log.Info("Dashboard Processing : Started processing dash data for fleet " + fleetId + ".");
                                        string sql = sqlString.Replace("[FleetID]", fleetId.ToString());
                                        try
                                        {
                                            conn = new SqlConnection(_dsn);
                                            conn.Open();
                                            cmd = conn.CreateCommand();
                                            cmd.CommandText = sql;
                                            _commandComplete = false;
                                            long startMs = DateTime.Now.Ticks;

                                            AsyncCallback asyncCallback = new AsyncCallback(SQLCommandComplete);
                                            IAsyncResult asyncRes = cmd.BeginExecuteNonQuery(asyncCallback, asyncCallback);
                                            int timeout = 6000;
                                            while (!_stopping && !_commandComplete && timeout > 0)
                                            {
                                                timeout--;
                                                Thread.Sleep(100);
                                            }

                                            if (_commandComplete)
                                            {
                                                _log.Info("Dashboard Processing : Finished processing dash data for fleet " + fleetId + ".");
                                                if (eDashboardUpdateComplete != null)
                                                {
                                                    long endMs = startMs - DateTime.Now.Ticks;
                                                    eDashboardUpdateComplete.BeginInvoke(endMs, false, AsyncDashboardProcessingCallBack, eDashboardUpdateComplete);
                                                }
                                            }
                                            else if (!_stopping && !_commandComplete)
                                            {
                                                try
                                                {
                                                    if (eDashboardUpdateComplete != null)
                                                        eDashboardUpdateComplete.BeginInvoke(0, true, AsyncDashboardProcessingCallBack, eDashboardUpdateComplete);
                                                    _log.Info("Dashboard Processing : Canceling processing of dash data for fleet " + fleetId + ", command took too long to run.");
                                                    cmd.EndExecuteNonQuery(asyncRes);
                                                }
                                                catch (Exception)
                                                {
                                                }
                                            }
                                            else if(_stopping)
                                            {
                                                try
                                                {
                                                    if (eDashboardUpdateComplete != null)
                                                        eDashboardUpdateComplete.BeginInvoke(0, true, AsyncDashboardProcessingCallBack, eDashboardUpdateComplete);
                                                    _log.Info("Dashboard Processing : Canceling processing of dash data for fleet " + fleetId + ", thread stop has been requested.");
                                                    cmd.EndExecuteNonQuery(asyncRes);
                                                }
                                                catch (Exception)
                                                {
                                                }
                                            }
                                        }
                                        catch (Exception exSQL)
                                        {
                                            if (eDashboardUpdateComplete != null)
                                                eDashboardUpdateComplete.BeginInvoke(0, true, AsyncDashboardProcessingCallBack, eDashboardUpdateComplete);
                                            _log.Error(this.GetType().FullName + ".MainThread() - SQL Execution", exSQL);
                                        }
                                        finally
                                        {
                                            if(cmd != null)
                                                cmd.Dispose();
                                            cmd = null;
                                            if (conn != null)
                                            {
                                                conn.Close();
                                                conn.Dispose();
                                            }
                                            conn = null;
                                        }
                                        if (_stopping)
                                            break;
                                    }
                                }
                            }
                        }
                        if (!_stopping)
                            Thread.Sleep(100);
                    }
                    catch (Exception threadEx)
                    {
                        _log.Error(this.GetType().FullName + ".MainThread() - Main Loop", threadEx);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".MainThread()", ex);
            }
            finally
            {
                _running = false;
                _stopped = true;
                _log.Info("Dashboard Processing : Stopped dashboard data processing thread.");
            }
        }
        private void SQLCommandComplete(IAsyncResult ar)
        {
            try
            {
                _commandComplete = true;
                if (ar.AsyncState is AsyncCallback)
                    (ar.AsyncState as AsyncCallback).EndInvoke(ar);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SQLCommandComplete(IAsyncResult ar)", ex);
            }
        }
        #endregion
        #region IConfigurationSectionHandler Members

        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        ///   <DashProcessing Enabled="true" ExecuteForFleets="1,5" ExecuteEveryXMinutes="30" ProcessLastXHours="24" DontProcessWhenQueueIsGreaterThan="1000" DSN="server=hera;uid=dats_sa;pwd=20miles;database=Transport_DashboardData;Asynchronous Processing=true;"/>
        /// </code>
        /// </summary>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                string enabled = section.Attributes.GetNamedItem("Enabled").Value.ToUpper().Trim();
                if(enabled == "TRUE")
                {
                    _dashInterfaceDel = new DashInterfaceDelegate(DashInterfaceEvent);
                    _dontProcessWhenQueueIsGreaterThan = Convert.ToInt32(section.Attributes.GetNamedItem("DontProcessWhenQueueIsGreaterThan").Value);
                    if (_dontProcessWhenQueueIsGreaterThan < 100)
                        _dontProcessWhenQueueIsGreaterThan = 100;
                    _loopTimer = Convert.ToInt32(section.Attributes.GetNamedItem("ExecuteEveryXMinutes").Value) * 600;
                    if (_loopTimer < 6000)
                        _loopTimer = 6000;
                    _processLastXHours = Convert.ToInt32(section.Attributes.GetNamedItem("ProcessLastXHours").Value);
                    if (_processLastXHours < 1)
                        _processLastXHours = 1;
                    if (_processLastXHours > 48)
                        _processLastXHours = 48;
                    _listSync = new object();
                    _fleetList = new List<int>();
                    _updateList = new List<int>();
                    _dsn = section.Attributes.GetNamedItem("DSN").Value;
                    _mainThread = new Thread(new ThreadStart(MainThread));
                    string fleetIdsString = section.Attributes.GetNamedItem("ExecuteForFleets").Value;

                    string[] fleetItems = fleetIdsString.Split(",".ToCharArray());
                    for(int X = 0; X < fleetItems.Length; X++)
                    {
                        int fleetId = Convert.ToInt32(fleetItems[X].Trim());
                        if (fleetId > 0 && !_fleetList.Contains(fleetId))
                        {
                            _fleetList.Add(fleetId);
                            _updateList.Add(fleetId);
                        }
                    }
                    _stopped = true;
                    _stopping = false;
                    _running = false;

                    if (_fleetList.Count > 0)
                    {
                        Start();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }
        #endregion
    }
}

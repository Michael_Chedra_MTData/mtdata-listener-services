using System;
using System.IO;
using System.Collections;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	///	This class will provide the following functionality for use with the 
	///	Remote State udpate functionality of the system.
	///		1 : List maintainance for resetting of units
	///		2 : IgnoreUpdate functionality to ignore updates from certain vechiles
	///		3 : Persistance of this information to a recoverable file.
	///		4 : Recovery of this information once the application is reloaded.
	///	The methods of this class are split into two broad sections
	///		a : Commands inbound from the RemoteStateUpdate port
	///		b : Updates inbound from vehicles
	///	All thread syncronisation will be based on the fact that each of these,
	///	a and b runs in their own thread only, and are limited to 1 thread per section 
	/// </summary>
	public class RemoteUpdateEngine
	{
		#region Private supporting classes

		/// <summary>
		/// This class deifnes an entry for vehicle resets.
		/// The vehicle will be reset based on fleetID and vehicleID,
		/// and removed form the list based on SerialNumber
		/// </summary>
		private class VehicleResetEntry
		{
			public long SerialNumber = 0;
			public int FleetID = 0;
			public uint VehicleID = 0;
			public DateTime IssueDate = DateTime.MinValue;
			public DateTime LastResetDate = DateTime.MinValue;

			public string GetFleetVehicleKey()
			{
				return RemoteUpdateEngine.GetFleetVehicleKey(FleetID, VehicleID);
			}

			/// <summary>
			/// Convert from a string into valid class data
			/// </summary>
			/// <param name="value"></param>
			public void FromString(string value)
			{
				string[] splits = value.Split(',');
				SerialNumber = Int32.Parse(splits[0]);
				FleetID = Int32.Parse(splits[1]);
				VehicleID = UInt32.Parse(splits[2]);
				IssueDate = RemoteUpdateEngine.StringToDate(splits[3]);
				LastResetDate = RemoteUpdateEngine.StringToDate(splits[4]);
			}

			/// <summary>
			/// Format the string for placing in the file.
			/// </summary>
			/// <returns></returns>
			public override string ToString()
			{
				return string.Format("{0},{1},{2},{3},{4}", 
					SerialNumber, 
					FleetID, 
					VehicleID, 
					RemoteUpdateEngine.DateToString(IssueDate), 
					RemoteUpdateEngine.DateToString(LastResetDate));
			}

		}

		/// <summary>
		/// This class will hold an entry that indicates all communications form this vehicle
		/// should be ignored, and not passed on to the reset of the server.
		/// </summary>
		private class VehicleIgnoreUpdateEntry
		{
			public int FleetID = 0;
			public uint VehicleID = 0;
			public DateTime IssueDate = DateTime.MinValue;
			public int TimeoutMinutes = 0;

			public string GetFleetVehicleKey()
			{
				return RemoteUpdateEngine.GetFleetVehicleKey(FleetID, VehicleID);
			}

			/// <summary>
			/// Convert from a string into valid class data
			/// </summary>
			/// <param name="value"></param>
			public void FromString(string value)
			{
				string[] splits = value.Split(',');
				FleetID = Int32.Parse(splits[0]);
				VehicleID = UInt32.Parse(splits[1]);
				IssueDate = RemoteUpdateEngine.StringToDate(splits[2]);
				TimeoutMinutes = Int32.Parse(splits[3]);
			}

			/// <summary>
			/// Format the string for placing in the file.
			/// </summary>
			/// <returns></returns>
			public override string ToString()
			{
				return string.Format("{0},{1},{2},{3}", 
					FleetID, 
					VehicleID, 
					RemoteUpdateEngine.DateToString(IssueDate), 
					TimeoutMinutes);
			}
		}

		#endregion

		#region static manipulators

		/// <summary>
		/// Convert stored dates into valid dates
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		public static DateTime StringToDate(string date)
		{
			string[] splits = date.Split(':');

			return new DateTime(Int32.Parse(splits[0]), Int32.Parse(splits[1]), Int32.Parse(splits[2]),
				Int32.Parse(splits[3]), Int32.Parse(splits[4]), Int32.Parse(splits[5]), 0);
		}

		/// <summary>
		/// Convert the dates to strings in a consistent manner.
		/// </summary>
		/// <param name="date"></param>
		/// <returns></returns>
		public static string DateToString(DateTime date)
		{
			return date.ToString("yyyy:MM:dd:HH:mm:ss");
		}

		/// <summary>
		/// Format the fleet and vehicle to match the required key format.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <returns></returns>
		public static string GetFleetVehicleKey(int fleetID, uint vehicleID)
		{
			return string.Format("{0}_{1}", fleetID, vehicleID);
		}

		#endregion

		#region Private instance data

		#region Operational

		/// <summary>
		/// This is the file into which the items will be persisted.
		/// </summary>
		private string _fileName = "";

		/// <summary>
		/// This object is used for synchronisation
		/// </summary>
		private object _syncRoot = new object();

		#endregion
		#region Reset Requests

		/// <summary>
		/// Index of reset entries by fleet and vehicle, separated by an underscore
		/// </summary>
		private Hashtable _vehicleResetEntriesByFleetVehicle = null;

		#endregion

		#region Ignore Update Requests

		/// <summary>
		/// Index of IgnoreUpdate entries by fleet and vehicle, separated by an underscore
		/// </summary>
		private Hashtable _vehicleUpdateEntriesByFleetVehicle = new Hashtable();

		#endregion

		#endregion

		/// <summary>
		/// This class will be initiated form a given filename if it exists.
		/// If not, the class will begin in a default blank state.
		/// </summary>
		/// <param name="fileName"></param>
		public RemoteUpdateEngine(string fileName)
		{
			_vehicleResetEntriesByFleetVehicle = Hashtable.Synchronized(new Hashtable());
			_fileName = fileName;
			
			if ((fileName != null) && (fileName != "") && File.Exists(fileName))
				LoadFromFile();

			Housekeeping();
		}

		#region Persistence Methods

		/// <summary>
		/// These are the different states of data in the file.
		/// </summary>
		private enum FileState
		{
			Unknown,
			ResetRequests,
			IgnoreUpdates
		}

		/// <summary>
		/// This method will load up the details from the file.
		/// The format is as follows.. 
		///		Each entry is a line to itself.
		///		A keyword heads a section indicating the type of section to follow.
		///		Each section then contains records of that type.
		/// </summary>
		private void LoadFromFile()
		{
			lock(_syncRoot)
			{
				StreamReader reader = new StreamReader(_fileName, System.Text.Encoding.Unicode);
				try
				{
					FileState currentState = FileState.Unknown;

					string line = reader.ReadLine();
					while(line != null)
					{
						if (line.Length > 0)
						{
							if(line.IndexOf(',') >= 0)
							{
								switch(currentState)
								{
									case FileState.ResetRequests : 
									{
										VehicleResetEntry entry = new VehicleResetEntry();
										entry.FromString(line);
										AddResetEntry(entry);								
										break;
									}
									case FileState.IgnoreUpdates :
									{
										VehicleIgnoreUpdateEntry entry = new VehicleIgnoreUpdateEntry();
										entry.FromString(line);
										AddIgnoreUpdateEntry(entry);
										break;
									}
								}
							}
							else
								currentState = (FileState)Enum.Parse(typeof(FileState), line.Trim(), true);
						}
						line = reader.ReadLine();
					}
				}
				finally
				{
					reader.Close();
				}
			}
		}

		/// <summary>
		/// Save off the lists to a file.
		/// </summary>
		private void SaveToFile()
		{
			if ((_fileName != null) && (_fileName.Length > 0))
			{
				lock(_syncRoot)
				{
					if (File.Exists(_fileName))
						File.Copy(_fileName, _fileName + ".bak", true);

					StreamWriter writer = new StreamWriter(_fileName, false, System.Text.Encoding.Unicode);
					try
					{
						writer.WriteLine(FileState.ResetRequests.ToString());
						foreach(VehicleResetEntry entry in _vehicleResetEntriesByFleetVehicle.Values)
							writer.WriteLine(entry.ToString());
						writer.WriteLine(FileState.IgnoreUpdates.ToString());
						foreach(VehicleIgnoreUpdateEntry entry in _vehicleUpdateEntriesByFleetVehicle.Values)
							writer.WriteLine(entry.ToString());
					}
					finally
					{
						writer.Close();
					}
				}
			}
		}

		#endregion

		#region Supporting Methods - Vehicle Reset

		/// <summary>
		/// Add an entry to the reset request list.
		/// </summary>
		/// <param name="entry"></param>
		private void AddResetEntry(VehicleResetEntry entry)
		{
			lock(_syncRoot)
			{
				string fleetVehicle = entry.GetFleetVehicleKey();

				if (_vehicleResetEntriesByFleetVehicle.Contains(fleetVehicle))
					_vehicleResetEntriesByFleetVehicle[fleetVehicle] = entry;
				else
					_vehicleResetEntriesByFleetVehicle.Add(fleetVehicle, entry);
			}
		}

		/// <summary>
		/// This is where the remote state update code will register a request for a vehicle reset.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="serialNumber"></param>
		public void AddVehicleResetRequest(int fleetID, uint vehicleID, long serialNumber)
		{
			lock(_syncRoot)
			{
				VehicleResetEntry entry = new VehicleResetEntry();
				entry.FleetID = fleetID;
				entry.VehicleID = vehicleID;
				entry.SerialNumber = serialNumber;
				entry.IssueDate = DateTime.Now;
				AddResetEntry(entry);
				SaveToFile();
			}
		
		}

		/// <summary>
		/// This method will indicate ot the packet processing code that a reset is required for that vehicle.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <returns></returns>
		public bool IsResetRequired(int fleetID, uint vehicleID)
		{
			bool result = false;
			lock(_syncRoot)
			{
				Housekeeping();

				string key = GetFleetVehicleKey(fleetID, vehicleID);
				if (_vehicleResetEntriesByFleetVehicle.Contains(key))
				{
					VehicleResetEntry entry = (VehicleResetEntry)_vehicleResetEntriesByFleetVehicle[key];
                    entry.LastResetDate = DateTime.Now;
					SaveToFile();
					result = true;
				}
				
			}
			return result;
		}

		/// <summary>
		/// This method will remove a unit from the reset required list.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <returns></returns>
		public void ClearResetRequired(int fleetID, uint vehicleID)
		{
			lock(_syncRoot)
			{
				string key = GetFleetVehicleKey(fleetID, vehicleID);
				if (_vehicleResetEntriesByFleetVehicle.Contains(key))
				{
					_vehicleResetEntriesByFleetVehicle.Remove(key);
				}				
			}
		}

		/// <summary>
		/// This method is called when a vehicle performs a gen_im_alive or gen_im_alive_imie
		/// It is passed the serial number of the vehicle and if this is a match to one of the serial numbers
		/// in the list then the vehicle was successfully reset, or has been out of range and performned it
		/// of it's own accord. Either way, we have acheived the goal of giving the vehicle a new VehicleID.
		/// </summary>
		/// <param name="serialNumber"></param>
		public void VerifyImAlive(long serialNumber)
		{
			lock(_syncRoot)
			{
				Housekeeping();

				//	For each entry in the Fleet/Vehicle list, check the serial number.
				//	If they match this serial number, remove them from the list.
				ArrayList entriesToBeRemoved = null;

				foreach(VehicleResetEntry entry in _vehicleResetEntriesByFleetVehicle.Values)
				{
					if (entry.SerialNumber == serialNumber)
					{
						if (entriesToBeRemoved == null)
							entriesToBeRemoved = new ArrayList();

						entriesToBeRemoved.Add(entry);
					}
				}

				if ((entriesToBeRemoved != null) && (entriesToBeRemoved.Count > 0))
				{
					foreach(VehicleResetEntry entry in entriesToBeRemoved)
						_vehicleResetEntriesByFleetVehicle.Remove(entry.GetFleetVehicleKey());
					SaveToFile();
				}
			}
		}

		#endregion

		#region Supporting methods - Vehicle Ignore Udpates

		/// <summary>
		/// Add an entry to the ignore updates list.
		/// </summary>
		/// <param name="entry"></param>
		private void AddIgnoreUpdateEntry(VehicleIgnoreUpdateEntry entry)
		{
			lock(_syncRoot)
			{
				string fleetVehicle = entry.GetFleetVehicleKey();
				if (_vehicleUpdateEntriesByFleetVehicle.Contains(fleetVehicle))
					_vehicleUpdateEntriesByFleetVehicle[fleetVehicle] = entry;
				else
					_vehicleUpdateEntriesByFleetVehicle.Add(fleetVehicle, entry);
			}
		}

		/// <summary>
		/// This is where the remote state update code will register a request to ignore live feeds
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="timeoutMinutes"></param>
		public void AddVehicleIgnoreUpdateRequest(int fleetID, uint vehicleID, int timeoutMinutes)
		{
			lock(_syncRoot)
			{
				VehicleIgnoreUpdateEntry entry = new VehicleIgnoreUpdateEntry();
				entry.FleetID = fleetID;
				entry.VehicleID = vehicleID;
				entry.TimeoutMinutes = timeoutMinutes;
				entry.IssueDate = DateTime.Now;

				AddIgnoreUpdateEntry(entry);

				SaveToFile();
			}
		
		}

		/// <summary>
		/// This method is called when a general packet is recived by the listener.
		/// It will determine whether the records should be placed in the database or not.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <returns></returns>
		public bool CheckPacketValidity(int fleetID, uint vehicleID)
		{
			lock(_syncRoot)
			{
				Housekeeping();

				return (!_vehicleUpdateEntriesByFleetVehicle.Contains(GetFleetVehicleKey(fleetID, vehicleID)));
			}
		}

		#endregion

		#region Additional Vehicle State Handling
		/// <summary>
		/// This is the format of a message indicating that a driver to vehicle record has been modified.
		/// </summary>
		public delegate void VehicleLoginDelegate(int fleetID, uint vehicleID, int DriverID, string sDriverName);

		/// <summary>
		/// Event raised if a driv er to vehicle change is sent through as a remote state updsate.
		/// </summary>
		public event VehicleLoginDelegate VehicleLogin;
		/// <summary>
		/// This indicates that a driver to  vehicle entry has been deleted by a system other than us, and the listener
		/// has been notificaed through a remote state update.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="driverID"></param>
		public void RegisterVehicleLogin(int fleetID, uint vehicleID, int driverID, string driverName)
		{
			if (VehicleLogin != null)
				VehicleLogin(fleetID, vehicleID, driverID, driverName);
		}

		/// <summary>
		/// This is the format of a message indicating that a driver to vehicle record has been modified.
		/// </summary>
		public delegate void VehicleMessageRecievdDelegate(int fleetID, uint vehicleID);

		/// <summary>
		/// Event raised if a driv er to vehicle change is sent through as a remote state updsate.
		/// </summary>
		public event VehicleMessageRecievdDelegate VehicleMessageRecievd;

		/// <summary>
		/// This indicates that a terminal has recieved a message sent to it.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void RegisterVehicleMessageRecieved(int fleetID, uint vehicleID)
		{
			if (VehicleMessageRecievd != null)
				VehicleMessageRecievd(fleetID, vehicleID);
		}

		/// <summary>
		/// Setting the 'At Lunch' state
		/// </summary>
		public delegate void VehicleSetAtLunchStateDelegate(int fleetID, uint vehicleID);
		public event VehicleSetAtLunchStateDelegate VehicleSetAtLunchStateRecievd;
		public void RegisterVehicleSetAtLunchStateRecievd(int fleetID, uint vehicleID)
		{
			if (VehicleSetAtLunchStateRecievd != null)
				VehicleSetAtLunchStateRecievd(fleetID, vehicleID);
		}

		/// <summary>
		/// Unsetting the 'At Lunch' state
		/// </summary>
		public delegate void VehicleUnsetAtLunchStateDelegate(int fleetID, uint vehicleID);
		public event VehicleUnsetAtLunchStateDelegate VehicleUnsetAtLunchStateRecievd;
		public void RegisterVehicleUnsetAtLunchStateRecievd(int fleetID, uint vehicleID)
		{
			if (VehicleUnsetAtLunchStateRecievd != null)
				VehicleUnsetAtLunchStateRecievd(fleetID, vehicleID);
		}

		/// <summary>
		/// This is the format of a message indicating that a message is queued to be sent to a vehicle
		/// </summary>
		public delegate void VehicleMessageQueuedDelegate(int fleetID, uint vehicleID);

		/// <summary>
		/// Event raised when a message is queued to be sent to a vehicle
		/// </summary>
		public event VehicleMessageQueuedDelegate VehicleMessageQueued;

		/// <summary>
		/// This indicates that a message is queued to be sent to a terminal.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void RegisterVehicleMessageQueued(int fleetID, uint vehicleID)
		{
			if (VehicleMessageQueued != null)
				VehicleMessageQueued(fleetID, vehicleID);
		}

		/// <summary>
		/// This is the format of a message indicating that a message is queued to be sent to a vehicle
		/// </summary>
		public delegate void VehicleMessageNotQueuedDelegate(int fleetID, uint vehicleID);

		/// <summary>
		/// Event raised when a message is queued to be sent to a vehicle
		/// </summary>
		public event VehicleMessageNotQueuedDelegate VehicleMessageNotQueued;

		/// <summary>
		/// This indicates that a message is queued to be sent to a terminal.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void RegisterVehicleMessageNotQueued(int fleetID, uint vehicleID)
		{
			if (VehicleMessageNotQueued != null)
				VehicleMessageNotQueued(fleetID, vehicleID);
		}
		
		/// <summary>
		/// This is the format of a message indicating that a vehicle has been modified.
		/// </summary>
		public delegate void VehicleModifiedDelegate(int fleetID, uint vehicleID);

		/// <summary>
		/// Event raised if a vehicle is deleted and notified through a remote state updsate.
		/// </summary>
		public event VehicleModifiedDelegate VehicleDeleted;
		/// <summary>
		/// This indicates that a vehicle has been deleted by a system other than us, and the listener
		/// has been notificaed through a remote state update.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void RegisterVehicleDeletion(int fleetID, uint vehicleID)
		{
			if (VehicleDeleted != null)
				VehicleDeleted(fleetID, vehicleID);
		}

		/// <summary>
		/// This will be raised when the listneer is informed of a vehicle creation
		/// </summary>
		public event VehicleModifiedDelegate VehicleCreated;

		/// <summary>
		/// This will allow the remote state update to register the creation of a unit externally
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		public void RegisterVehicleCreation(int fleetID, uint vehicleID)
		{
			//	if there are any entries in the IgnoreUpdates list for the vehicle specified, 
			//	remove them as they do not now apply.
			lock(_syncRoot)
			{
				string key = GetFleetVehicleKey(fleetID, vehicleID);
				if (_vehicleUpdateEntriesByFleetVehicle.Contains(key))
				{
					_vehicleUpdateEntriesByFleetVehicle.Remove(key);
					SaveToFile();
				}
			}

			//	Resets can be left, as they will only make the unit reset once, and it may be that
			//	another unit is running around with a different serial number, but the same unit number.

			//	Pass on this information
			if (VehicleCreated != null)
				VehicleCreated(fleetID, vehicleID);
		}

		#endregion

		#region Housekeeping. 

		/// <summary>
		/// This is the date and time of the last spring clean.
		/// </summary>
		private DateTime _lastHousekeepingRun = DateTime.MinValue;

		/// <summary>
		/// This method will run through the lists to see if any of the requests have expried.
		/// If they have, they are removed from the lists.
		/// It will be triggered by a call from the inbound vehicle packet handler
		/// </summary>
		private void Housekeeping()
		{
			lock(_syncRoot)
			{
				DateTime now = DateTime.Now;
				
				//	Determine if a housekeeping run is required.
				//	Every 10 minutes should be adequate
				if (_lastHousekeepingRun.AddMinutes(10) < now)
				{
					//	Live update ignore requests
					ArrayList listToBeRemoved = new ArrayList();
					foreach(VehicleIgnoreUpdateEntry entry in _vehicleUpdateEntriesByFleetVehicle.Values)
					{
						if (entry.IssueDate.AddMinutes(entry.TimeoutMinutes) < now)
							listToBeRemoved.Add(entry);
					}
			
					foreach(VehicleIgnoreUpdateEntry entry in listToBeRemoved)
						_vehicleUpdateEntriesByFleetVehicle.Remove(entry.GetFleetVehicleKey());

					//	Vehicle Reset Requests
					listToBeRemoved.Clear();

					//	Give resets a 90 minute grace from the last send.
					//	And a 4 hour grace from the issue date
					foreach(VehicleResetEntry entry in _vehicleResetEntriesByFleetVehicle.Values)
						if (((entry.LastResetDate != DateTime.MinValue) && 
							 (entry.LastResetDate.AddMinutes(90) < now)) ||
							(entry.IssueDate.AddHours(4) < now))
							listToBeRemoved.Add(entry);
				
					foreach(VehicleResetEntry entry in listToBeRemoved)
						_vehicleResetEntriesByFleetVehicle.Remove(entry.GetFleetVehicleKey());

					SaveToFile();

					_lastHousekeepingRun = now;
				}
			}
		
		}

		#endregion
	}
}

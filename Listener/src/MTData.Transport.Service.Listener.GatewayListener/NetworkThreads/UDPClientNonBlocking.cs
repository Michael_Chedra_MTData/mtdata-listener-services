using System;
using System.Net;
using System.Net.Sockets;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
	/// <summary>
	/// This interface will be implemented by all udp mechanisms.
	/// </summary>
	public interface UDPClientInterface
	{
		byte[] Receive(ref System.Net.IPEndPoint rEP);
		int Send(byte[] dgram, int bytes, IPEndPoint endPoint);
	}

	/// <summary>
	/// This class will allow non-blocking access to a UDP
	/// listener.
	/// </summary>
	public class UDPClientNonBlocking : UdpClient
	{
		public UDPClientNonBlocking(int Port) : base(Port) 
		{
		}

		public UDPClientNonBlocking(IPEndPoint localEP) : base(localEP) 
		{

		}

		/// <summary>
		/// Receive from the udp port in a non-blocking manner
		/// </summary>
		/// <param name="rEP"></param>
		/// <returns></returns>
		public byte[] ReceiveNonBlocking(ref System.Net.IPEndPoint rEP)
		{
			Socket s = this.Client;
			s.Blocking = false;
			return this.Receive(ref rEP);
		}

		/// <summary>
		/// Indicates if there is data available.
		/// </summary>
		/// <returns></returns>
		public bool DataAvailable()
		{
			Socket s = this.Client;
			return (s.Available > 0);
		}

	}
}

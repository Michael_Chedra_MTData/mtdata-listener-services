using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections.Generic;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
    public delegate void UniversalListenerDelegate(IPEndPoint farend, byte[] contents);

    public delegate void UniversalListenerConnectionClosed(object sender);

    public class UniversalListenerThread
    {

        private static ILog _log = LogManager.GetLogger(typeof(UniversalListenerThread));
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.Network_Threads.UniversalListenerThread.";
        /// <summary>
        /// UDP Listening mechanism which operates on its own
        /// independent thread. Pass in a UDPClient which will
        /// be listened to, and a delegate class (AKA Function Pointer)
        /// which will be called when data arrives on a this port.
        /// The called method must accept two arguments.
        /// The first will be the IPEndPoint of the sending entity.
        /// The second will be the byte[] contents of the UDP packet.
        /// 
        /// For an example of how to set up a delegate to use this class,
        /// see PacketDatabaseInterface.cs
        /// 
        /// Author: John Marshall
        /// Date:	8/7/2003
        /// </summary>
        /// 
        public event WriteToConsoleEvent eConsoleEvent;

        public delegate void WriteToConsoleEvent(string sMsg);

        public event RemoteConnectionClosedEvent eRemoteConnectionClosed;

        public delegate void RemoteConnectionClosedEvent(IPEndPoint oIP);

        public event UniversalListenerConnectionClosed eSocketConnectionClosed;

        public bool bLogPacketData;

        private bool bIsRunning;
        private UdpClient mUDPClient;
        private IPEndPoint mHost;
        private Thread myListenerThread;
        private UniversalListenerDelegate mProcessFunction;
        private string _description = "";
        private byte[] bAppPingResponse = new byte[32];
        private IPEndPoint _lastEndpoint;
        private object _lastEndpointSync;
        private bool _ignoreListReplyToPings;
        private bool _ignoreListReplyToPingsFromUnknownUnits;
        private object _ignoreReportsFromTheseUnitsSyncRoot;
        private List<ulong> _ignoreReportsFromTheseUnits;
        
        // Nominate a port and a delegate function which will
        // be invoked when we get something.
        // The function will be passed an array of objects,
        // the first will be the IPEndPoint of the sender,
        // the second will be a byte[] containing the packet contents
        public UniversalListenerThread
            (UdpClient aClient,
             UniversalListenerDelegate aProcessingFunction,
             string description)
        {
            // Listen with this client
            _lastEndpointSync = new object();
            _ignoreReportsFromTheseUnitsSyncRoot = new object();
            _ignoreReportsFromTheseUnits = new List<ulong>();
            bAppPingResponse[0] = 0x77;
            mUDPClient = aClient;
            mProcessFunction = aProcessingFunction;
            bIsRunning = false;
            mHost = null;
            _description = description;
            ThreadStart myThreadStart = HandleThread;
            myListenerThread = new Thread(myThreadStart);
            myListenerThread.Name = description + " - Will call " + aProcessingFunction.Method.Name;
            myListenerThread.Start();
        }

        /// <summary>
        /// </summary>
        /// <param name="aClient"></param>
        /// <param name="aProcessingFunction"></param>
        /// <param name="hostToListenFrom"></param>
        /// <param name="description"></param>
        public UniversalListenerThread
            (UdpClient aClient,
             UniversalListenerDelegate aProcessingFunction,
             IPEndPoint hostToListenFrom,
             string description)
        {
            _lastEndpointSync = new object();
            bAppPingResponse[0] = 0x77;
            // Listen with this client
            mUDPClient = aClient;
            mProcessFunction = aProcessingFunction;
            bIsRunning = false;
            mHost = hostToListenFrom;
            _description = description;
            //mByteArray = new byte[1000];

            ThreadStart myThreadStart = HandleThread;
            myListenerThread = new Thread(myThreadStart);
            myListenerThread.Name = "Will call " + aProcessingFunction.Method.Name;
            myListenerThread.Start();
            _stopped = false;
        }

        private bool _stopped = true;

        public bool Stopped
        {
            get { return _stopped; }
        }

        public void AbortThread()
        {
            try
            {
                if (!_stopped)
                    myListenerThread.Abort();
            }
            catch (Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        public void StopThread()
        {
            try
            {
                bIsRunning = false;
                if (mUDPClient != null)
                    mUDPClient.Close();
            }
            catch (Exception ex)
            {
                WriteToErrorConsole(ex);
            }
            try
            {
                if (_ignoreReportsFromTheseUnits != null)
                {
                    lock (_ignoreReportsFromTheseUnitsSyncRoot)
                    {
                        _ignoreReportsFromTheseUnits.Clear();
                        _ignoreReportsFromTheseUnits = null;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Dispose()", ex);
            }
        }


        public bool Send(byte[] data, int length, IPEndPoint address)
        {
            bool bRet = false;
            try
            {
                lock (_lastEndpointSync)
                    _lastEndpoint = new IPEndPoint(address.Address, address.Port);

                mUDPClient.Send(data, length, address);
                bRet = true;
            }
            catch (Exception ex)
            {
                WriteToErrorConsole(ex);
            }
            return bRet;
        }

        public void HandleThread()
        {
            IPEndPoint otherEnd = null;
            bIsRunning = true;
            byte[] bytes = null;
            IPEndPoint copyLastEndPoint = null;
            while (bIsRunning)
            {
                try
                {
                    if (mUDPClient != null)
                    {
                        if (mHost != null)
                            otherEnd = mHost;
                        else
                            otherEnd = new IPEndPoint(IPAddress.Any, 0); // don't care

                        //	NOTE : This is a blocking operation.. we need to verify the effect
                        //	of this on a Send.
                        try
                        {
                            bytes = null;
                            bytes = mUDPClient.Receive(ref otherEnd);
                        }
                        catch (Exception ex)
                        {
                            if (ex.Message == "A blocking operation was interrupted by a call to WSACancelBlockingCall")
                                bIsRunning = false;
                            else if (ex.Message == "An existing connection was forcibly closed by the remote host")
                            {
                                // This error is raised when you are sending to an endpoint that is no longer available.
                                // If there is something monitoring, raise the event, else just go on listening for the next
                                // client packet.                               
                                lock (_lastEndpointSync)
                                    copyLastEndPoint = new IPEndPoint(_lastEndpoint.Address, _lastEndpoint.Port);
                                if (eRemoteConnectionClosed != null)
                                    eRemoteConnectionClosed.BeginInvoke(copyLastEndPoint, null, null);
                                bytes = null;
                            }
                            else
                            {
                                WriteToConsole(ex.Message);
                            }
                        }
                        if (bIsRunning && bytes != null)
                        {
                            bool ignoreThisUnit = false;
                            bool pingFromUnknownUnit = false;
                            if (_ignoreReportsFromTheseUnits != null && _ignoreReportsFromTheseUnits.Count > 0)
                            {
                                lock (_ignoreReportsFromTheseUnitsSyncRoot)
                                {
                                    UInt64 fleetId = 0;
                                    UInt64 vehicleId = 0;

                                    if (bytes[0] == (byte)0x01 && bytes[bytes.Length - 1] == (byte)0x04)
                                    {
                                        fleetId = Convert.ToUInt64(bytes[4]);
                                        vehicleId = (UInt64)bytes[6] + (UInt64)(bytes[5] << 8);
                                    }
                                    else if (bytes[0] == 0x77)
                                    {
                                        if (bytes.Length > 3)
                                        {
                                            fleetId = Convert.ToUInt64(bytes[1]);
                                            vehicleId = (UInt64)bytes[3] + (UInt64)(bytes[2] << 8);
                                        }
                                        else
                                            pingFromUnknownUnit = true;
                                    }
                                    if (pingFromUnknownUnit && !_ignoreListReplyToPingsFromUnknownUnits)
                                    {
                                        ignoreThisUnit = true;
                                        _log.Info("Ignored ping from unknown unit : " + BitConverter.ToString(bytes));
                                    }
                                    else
                                    {
                                        ulong key = (fleetId << 32) + vehicleId;
                                        if (_ignoreReportsFromTheseUnits.Contains(key))
                                        {
                                            if (bytes[0] == 0x77)
                                            {
                                                if (!_ignoreListReplyToPings)
                                                {
                                                    ignoreThisUnit = true;
                                                    _log.Info("Ignored ping from " + fleetId.ToString() + "/" + vehicleId.ToString() + " : " + BitConverter.ToString(bytes));
                                                }
                                            }
                                            else
                                            {
                                                ignoreThisUnit = true;
                                                _log.Info("Ignored report from " + fleetId.ToString() + "/" + vehicleId.ToString() + " : " + BitConverter.ToString(bytes));
                                            }
                                        }
                                    }
                                }
                            }

                            if (!ignoreThisUnit)
                            {
                                if (bytes[0] == 0x77 && bytes.Length <= 10)
                                {
                                    // This is an app ping.
                                    mUDPClient.Send(bAppPingResponse, bAppPingResponse.Length, otherEnd);
                                    WriteToConsole(string.Format("Ping Reply Sent To ({0}:{1}, Ping Request Data : {2}", otherEnd.Address, otherEnd.Port, BitConverter.ToString(bytes)));
                                }
                                else
                                {
                                    if (bLogPacketData)
                                        WriteToConsole(string.Format("Received From ({0}:{1}, Data : {2}", otherEnd.Address, otherEnd.Port, BitConverter.ToString(bytes)));
                                    mProcessFunction(otherEnd, bytes);
                                }
                            }
                        }
                    }
                }
                catch (ObjectDisposedException exDispose)
                {
                    if (bIsRunning)
                    {
                        bIsRunning = false;
                        WriteToErrorConsole(exDispose);
                    }
                }
                catch (SocketException exSocket)
                {
                    if (bIsRunning && exSocket.Message.IndexOf("An existing connection was forcibly closed by the remote host") >= 0)
                    {
                        if (otherEnd != null)
                            WriteToConsole("Client Connection on " + otherEnd.Address + ":" + Convert.ToString(otherEnd.Port) + " closed.");
                        if (eSocketConnectionClosed != null)
                            eSocketConnectionClosed(_lastEndpointSync);
                    }
                }
                catch (Exception ex)
                {
                    if (bIsRunning)
                        WriteToErrorConsole(ex);
                }
                finally
                {
                    if (!bIsRunning)
                    {
                        myListenerThread.Abort();
                    }
                }
            }
            _stopped = true;
            WriteToConsole("Stopping " + _description);
        }

        #region Log Event Interface Support

        private void WriteToConsole(string sMsg)
        {
            if (eConsoleEvent != null) eConsoleEvent(_description + " : " + sMsg);
        }

        private void WriteToErrorConsole(Exception ex)
        {
            string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
            WriteToConsole(sMsg);
        }

        #endregion

        #region Ignore reports from unit
        public bool IgnoreListReplyToPings
        {
            get { return _ignoreListReplyToPings; }
            set { _ignoreListReplyToPings = value; }
        }
        public bool IgnoreListReplyToPingsFromUnknownUnits
        {
            get { return _ignoreListReplyToPingsFromUnknownUnits; }
            set { _ignoreListReplyToPingsFromUnknownUnits = value; }
        }
        public void IgnoreListAddVehicle(int fleetId, int vehicleId)
        {
            try
            {
                ulong key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(vehicleId);
                bool added = false;
                lock (_ignoreReportsFromTheseUnitsSyncRoot)
                {
                    if (_ignoreReportsFromTheseUnits != null && !_ignoreReportsFromTheseUnits.Contains(key))
                    {
                        _ignoreReportsFromTheseUnits.Add(key);
                        added = true;
                    }
                }
                if (added)
                    _log.Info("Added request to ignore reports from " + fleetId.ToString() + "/" + vehicleId.ToString());
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "IgnoreListAddVehicle(int fleetId, int vehicleId)", ex);
                throw new System.Exception(sClassName + "IgnoreListAddVehicle(int fleetId, int vehicleId)", ex);
            }
        }
        public void IgnoreListClearList()
        {
            try
            {
                lock (_ignoreReportsFromTheseUnitsSyncRoot)
                {
                    if (_ignoreReportsFromTheseUnits != null)
                    {
                        _ignoreReportsFromTheseUnits.Clear();
                        _ignoreReportsFromTheseUnits = new List<ulong>();
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "IgnoreListClearList()", ex);
                throw new System.Exception(sClassName + "IgnoreListClearList()", ex);
            }
        }
        public void IgnoreListRemoveVehicle(int fleetId, int vehicleId)
        {
            try
            {
                ulong key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(vehicleId);
                bool removed = false;
                lock (_ignoreReportsFromTheseUnitsSyncRoot)
                {
                    if (_ignoreReportsFromTheseUnits != null && _ignoreReportsFromTheseUnits.Contains(key))
                    {
                        _ignoreReportsFromTheseUnits.Remove(key);
                        removed = true;
                    }
                }
                if (removed)
                    _log.Info("Removed request to ignore reports from " + fleetId.ToString() + "/" + vehicleId.ToString());
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "IgnoreListRemoveVehicle(int fleetId, int vehicleId)", ex);
                throw new System.Exception(sClassName + "IgnoreListRemoveVehicle(int fleetId, int vehicleId)", ex);
            }
        }
        #endregion
    }
}

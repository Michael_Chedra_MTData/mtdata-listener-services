using System;
using System.Data;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Collections;
using System.Configuration;
using System.Collections.Generic;
using System.Timers;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	#region UnitState enums

	public enum UnitState : long
	{
		NotKnown		=	0x00000000,
		AtWayPoint		=	0x00000001,
		DepWayPoint		=	0x00000002,
		IgnitionOn		=	0x00000004,
		IgnitionOff		=	0x00000008,
		Input1On		=	0x00000010,
		Input2On		=	0x00000020,
		Stationary1		=	0x00000040,
		Stationary2		=	0x00000080,
		Stationary3		=	0x00000100,
		DepStationary	=	0x00000200,
		MDTState1		=	0x00000400,
		MDTState2		=	0x00000800,
		MDTState3		=	0x00001000,
		MDTState4		=	0x00002000,
		AuxState1		= 	0x00004000,		
		AuxState2		=	0x00008000,		
		AuxState3		=	0x00010000,	
		AuxState4		=	0x00020000,	
		AtDepotNoLoad	=	0x00040000,		// 9
		AtDepotLoading	=	0x00080000,		// 2
		AtDepotLoaded	=	0x00100000,			// 2
		OnWayToJob		=	0x00200000,		// 3
		SwitchingPlants	=	0x00400000,		
		OnSite			=	0x00800000,			// 4
		UnloadingAtSite	=	0x01000000,		// 5
		ReturningHome	=	0x02000000,		// 6 then 7, then 8 on depart.
		ExcessiveIdle   =   0x04000000,
		LoggedIn   =   0x08000000,
		AtLunch   =   0x10000000,
		AtWPSpecial1 =   0x20000000,
		AtWPSpecial2 =   0x40000000,
		GlobalStarPosition =   0x80000000,
		GlobalStarAlert1 =   0x100000000,
		GlobalStarAlert2 =   0x200000000,
		GlobalStarAlert3 =   0x400000000,
		GlobalStarAlert4 =   0x800000000
	}

	#region New State Flags..

	public enum StatusVehicle59 : long
	{
		STATUS_WP_AT				= 0x00000001,
		STATUS_WP_TOO_LONG		= 0x00000002,
		STATUS_WP_DOCK			= 0x00000004,
		STATUS_WP_NOGO			= 0x00000008,
		STATUS_WP_SPEEDING		= 0x00000010,
		STATUS_WP_HOLDING			= 0x00000020,
		STATUS_IGN_ON				= 0x00000040,
		STATUS_INP1_ON				= 0x00000080,
		STATUS_INP2_ON				= 0x00000100,
		STATUS_INP3_ON				= 0x00000200,
		STATUS_INP4_ON				= 0x00000400,
		STATUS_SPEEDING			= 0x00000800,
		STATUS_EXCESS_IDLE			= 0x00001000,
		STATUS_ROUTE_OFF			= 0x00002000,
		STATUS_ROUTE_ONTIME		= 0x00004000,
		STATUS_ROUTE_EARLY		= 0x00008000,
		STATUS_ROUTE_LATE			= 0x00010000,
		STATUS_LOGGED_ON			= 0x00020000,
		STATUS_LUNCH				= 0x00040000,
		STATUS_ECM_EXCEPTION		= 0x00080000,
		STATUS_OVER_HOURS			= 0x00100000,
		STATUS_OVER_HOURS_24		= 0x00200000,
		STATUS_WP_WORKSHOP		= 0x00400000,
		STATUS_SPARE4				= 0x00800000,
		STATUS_CONC_NOT_LOADED	= 0x01000000,
		STATUS_CONC_LOADING		= 0x02000000,
		STATUS_CONC_LOADED		= 0x04000000,
		STATUS_CONC_TO_JOB		= 0x08000000,
		STATUS_CONC_AT_JOB		= 0x10000000,
		STATUS_CONC_UNLOADING	= 0x20000000,
		STATUS_CONC_RETURNING		= 0x40000000,
		STATUS_CONC_SWITCHING		= 0x80000000,
        STATUS_OUT_OF_HOURS = 0x100000000,
        STATUS_STATIONARY_ALERT = 0x200000000,
        STATUS_LOW_BATTERY = 0x400000000,
        STATUS_MOVING = 0x800000000,
        STATUS_BATTERY_CHARGING = 0x1000000000,
        STATUS_CRITICAL_BATTERY = 0x2000000000,
        STATUS_FLAT_BATTERY = 0x4000000000,
        STATUS_PTO_ON = 0x8000000000,
        STATUS_SEAT_BELT_ON = 0x10000000000,
        STATUS_ABS_ON = 0x20000000000,
        STATUS_PASSENGER_SEAT_BELT_ON = 0x40000000000,
        STATUS_FOUR_WHEEL_DRIVE_ON = 0x80000000000,
        STATUS_DOORS_OPEN = 0x100000000000,
        STATUS_HAND_BRAKE_ON = 0x200000000000,
        STATUS_CRUISE_CONTROL_ON = 0x400000000000,
        STATUS_WIPERS_ON = 0x800000000000,
        STATUS_HEAD_LIGHTS_ON = 0x1000000000000,
        STATUS_HIGH_BEAM_ON = 0x2000000000000,
        STATUS_LEFT_INDICATOR_ON = 0x4000000000000,
        STATUS_LOW_FUEL_INDICATOR_ON = 0x8000000000000,
        STATUS_EBS_ON = 0x10000000000000,
        STATUS_RIGHT_INDICATOR_ON = 0x20000000000000,
    }

	#endregion

	public enum InternalUnitState
	{
		Input1Off		= 1, 
		Input2Off		= 2,
		ExcessiveIdleOver = 3
	}
	#endregion
	/// <summary>
	/// Represents a device connection and provides packet-send functionality with persistent retries
	/// </summary>
	public class Vehicle
	{
        private static ILog _log = LogManager.GetLogger(typeof(Vehicle));
		#region Events
		public event WriteToConsoleEvent eConsoleEvent; 
		public delegate void WriteToConsoleEvent(string sMsg) ;
		public delegate void SendRetriesExceededDelegate(Vehicle vehicle, GatewayProtocolPacket packet);
		public event SendRetriesExceededDelegate SendRetriesExceeded;
        public delegate void SendPossibleAccidentLiveUpdateDelegate(Vehicle vehicle, GeneralGPPacket packet);
        public event SendPossibleAccidentLiveUpdateDelegate SendPossibleAccidentLiveUpdate;
        #endregion
		#region Public Vars
		/// <summary>
		/// ServerUnitState will contain bits which can only be SET by Server side
		/// </summary>
		public long ServerUnitStateEx;
		/// <summary>
		/// ServerAuxiliaryState will contain bits which can only be SET by Server side
		/// </summary>
		public long ServerAuxiliaryState;
		public GatewayProtocolPacket MostRecentLiveUpdatePacket;
	    public DateTime MostRecentLiveReportTime = DateTime.MinValue;
		public MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails oPos = new MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails();
		public int StationaryMinutesThreshold = 1;
		public bool bLogPacketData = false;
		public decimal StationarySpeedNM
		{
			get
			{ 
				return dStationarySpeedNM;
			}
			set
			{
				dStationarySpeedNM = value;
			}
		}

		public decimal PositionThreshold	// The threshold before saying that a unit has changed position
		{
			get
			{
				return dPositionThreshold;
			}
			set
			{
				dPositionThreshold = value;
				// Automatically generate the speed theshold
				decimal miles =  dPositionThreshold * 54; // Get dist in nautical miles
				decimal time = (decimal) this.StationaryMinutesThreshold / 60; // get time in hours

				this.dStationarySpeedNM =  miles/time;
			}
		}
															
		public const int SEQ_BITMAP_SIZE = 32;	// Record last 32 packets
		public const int SEQ_WINDOW_SIZE = 15;	// Check the consecutivity(?) of the last X
		public Boolean bUnitAlive;
		public UdpClient mSender;

		public GatewayProtocolPacket mPacket;
		public bool bRevertUnitToPrimary;
		public bool SwallowThisPacket;
		public IPEndPoint	mSenderIP;							// the IP address we use to talk to this unit
		public int			iUnitWPCount = 0;						// For a unit that has a mix of Server Processed and Unit Processed WPs.  The number of WPs the unit has sent arrives for.  This number is increased when the unit send an 'Arrive WP' and decreased when the unit sends a 'Depart WP'
		public int			iUnitSpecail1WPCount = 0;		// 
		public int			iUnitSpecail2WPCount = 0;		// 
		public ArrayList oUnitWPs = null;						// 
		public ArrayList oUnitSpecail1WPs = null;			// 
		public ArrayList oUnitSpecail2WPs = null;		// 
		public byte			cOurSequence;							// the last SN we used with this unit
		public byte			cAckSequence;							// the last ACK we used
		public int			iServerRXBytes = 0;					// Bytes recieved by server from this unit
		public int			iServerTXBytes = 0;					// Bytes sent to unit by server.
		public uint			iBitmap;										// the bitmap of SNs we've had
		public bool			bInRegardlessMode;					// used to check if we have been dumb-ACKing everything
		public byte			cLastSeq;									// the last sequence number that arrived
		public byte			cSecondLastSeq;						// the Seq before that
		public byte			cLastAck;									// the last ACK we should use
		public byte			cSecondLastAck;						// the ACK before that
		public byte			cLastFileSent;							// The type of the last Schedule/Config/RP/WP
		public byte			cSecondLastFileSent;				// The type of the second last Schedule/Config/RP/WP
		public byte			cThirdLastFileSent;					// The type of the third last Schedule/Config/RP/WP
		public bool			bNeedsConfigCheck = true;					// Set if there has been a config change to the fleet
	    public ConfigWatchList oWatchListPacket;                        // If the vehicle has been added or removed from a watch list, this object will be set.
        public FirmwareDriverLoginReply oFirmwareLoginReply;      // If the vehicle has sent a firmware login, this object will be populated to return the result to the unit.
        public GPSendScriptFile oSendScriptFilePacket;            // If the GUI has a script file to send to the unit, this value will be populated.
		public bool			bHasMail;									// Set if there is a mail message waiting
		public MailMessagePacket MailMessage;			// The message which awaits
		public System.Collections.ArrayList oWayPointIDs = null;	// Contains a list of MTData.Transport.Service.Listener.GatewayListener.Database_Access.cWPItem objects indicating which WPs the unit is currently at.
        public object oAddtionalLiveUpdateSync = new object();	// Contains a list of addition live updates that need to be sent to the client.
		public System.Collections.ArrayList oAddtionalLiveUpdatePackets = null;	// Contains a list of addition live updates that need to be sent to the client.
        public SortedDictionary<DateTime, GeneralGPPacket> oFlashReportDictionary = null;
		public bool IsTicketed = false;
        private List<ServerTrackingRule> _serverRuleBreakReasonCodes;
        private int _ruleBreakUniqueId = 0;
        public AcknowledgementNotificationDelegate mAckDelegate;
        public GatewayProtocolPacket mAckNotifyMatchPacket;
        public bool IsWaitingForAccident { get; set; }

		#endregion
		#region Public Properties
		/// <summary>
		/// IS a buffer request pending?
		/// </summary>
		public bool AccidentRequestPending
		{
			get
			{ 
				bool result = false;
				lock(_accidentBufferIndices.SyncRoot)
					result = ( _accidentBufferIndices.Count > 0); 
				return result;
			}
		}

		/// <summary>
		/// Get the next request, removing it from the pending list
		/// </summary>
		/// <returns></returns>
		public int AccidentGetNextBufferRequest()
		{
			int result = -1;
			lock(_accidentBufferIndices.SyncRoot)
			{
				if (_accidentBufferIndices.Count > 0)
				{
					result = (int)_accidentBufferIndices[0];
					_accidentBufferIndices.RemoveAt(0);
				}
			}
			return result;
		}

		/// <summary>
		/// Add an accidnet buffer request
		/// </summary>
		/// <param name="bufferIndex"></param>
		public void AddAccidentRequest(int bufferIndex)
		{
			lock(_accidentBufferIndices)
				if (_accidentBufferIndices.IndexOf(bufferIndex) == -1)
					_accidentBufferIndices.Add(bufferIndex);
		}

		/// <summary>
		/// Indicates if this unit is a tracking unit
		/// </summary>
		public bool IsTrackingUnit{get{return _bIsTrackingUnit;}set{_bIsTrackingUnit = value;}}
		/// <summary>
		/// Indicates if this unit is a refrigeration unit
		/// </summary>
		public bool IsRefrigerationUnit{get{return _bIsRefrigerationUnit;}set{_bIsRefrigerationUnit = value;}}
		/// <summary>
		/// This is the tag to pass through on live updates for the vehicle label
		/// </summary>
		public string sUserInfo 
		{
			get
			{
				string sRet = "";
				if (_sUserInfo == "")
				{
					try
					{
						sRet = mDatabase.GetUserInfoTag(cFleet, iUnit);
					}
					catch(System.Exception)
					{
						sRet = "";
					}
					_sUserInfo = sRet;
				}
				else
				{
					sRet = _sUserInfo;
				}
				return sRet;
			}
			set
			{
				_sUserInfo = value;
			}
		}

		public string VehicleTag 
		{
			get
			{
				string sRet = "";
				try
				{
					sRet = mDatabase.GetVehicleTag(cFleet, iUnit);
				}
				catch(System.Exception)
				{
					sRet = "";
				}
				return sRet;
			}
		}

		public UnitState State
		{
			get
			{
				return this.mUnitState;
			}
			set
			{
				this.mUnitState = value;
			}
		}

		public byte cFleet
		{
			get{ return _cFleet; }
			set { _cFleet = value; _iUniqueId = VehicleCollection.GetUniqueUnitId(_cFleet, _iUnit); }
		}

		public uint iUnit
		{
			get{ return _iUnit; }
			set { _iUnit = value; _iUniqueId = VehicleCollection.GetUniqueUnitId(_cFleet, _iUnit); }
		}

		public int iUniqueId
		{
			get{ return _iUniqueId; }
		}

		public long GetAuxiliaryState()
		{
			return MapOldStatusToAuxiliaryState() | ServerAuxiliaryState;
		}
		public long GetUnitStateEx()
		{
			//	Convert the old status to the new status.
			return MapOldStatusToUnitState() | ServerUnitStateEx;
		}
        public float TotalMass
        {
            get { return fTotalMass; }
            set { fTotalMass = value; }
        }
        public float WeightLimit { get { return fWeightLimit; } }

        public int OdometerOffset
        {
            get {return mDatabase.GetOdometerOffset(cFleet, iUnit);}
        }

        public int EngineHourOffset
        {
            get {return mDatabase.GetEngineHourOffset(cFleet, iUnit);}
        }

        public bool UseGPSOdometer
        {
            get { return mDatabase.UsingGPSOdometer(cFleet, iUnit); }
        }

        public DateTime CompletedExtenededHistoryRequestStart { get; set; }
        public DateTime CompletedExtenededHistoryRequestEnd { get; set; }

        public int GetPacketSentCountAndReset
        {
            get
            {
                int sent = 0;
                lock (SyncSender)
                {
                    sent = _packetsSent;
                    _packetsSent = 0;
                }
                return sent;
            }

        }

        #endregion

        public void Dispose()
        {
            //remove delegates
            mDatabase.eRefreshDatasets -= new DatabaseInterface.RefreshDataSetsDelegate(mDatabase_RefreshDatasets);
            mDatabase.OnRefreshScheduledTaskDataset -= new DatabaseInterface.RefreshDataSetsDelegate(mDatabase_OnRefreshScheduledTaskDataset);

            try
            {
                if (_lastIncidentTimer != null)
                {
                    _lastIncidentTimer.Dispose();
                    _lastIncidentTimer = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mAckNotifyMatchPacket != null)
                {
                    mAckNotifyMatchPacket.Dispose();
                    mAckNotifyMatchPacket = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mAckDelegate != null)
                {
                    mAckDelegate = null;
                }
            }
            catch (System.Exception)
            {
            }
        }

		#region Private Vars
		private bool _bIsTrackingUnit = true;			// Indicates if this unit is a tracking unit
		private bool _bIsRefrigerationUnit = true;	// Indicates if this unit is a refrigeration unit
		public bool bResendActive = false;				// 
		private byte _cFleet;											// Fleet of this unit
		private int iRetries;											//
		private int iMaxRetries;									//
		private int iTimeout;											//
		private int _iUniqueId;										// Unique combination of fleet and unit
		private uint _iUnit;											// Unit number
		private decimal dStationarySpeedNM;			// 7 Nautical Miles per hour == stationary 
		private decimal dPositionThreshold;				// 
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";	// 
		private string _sUserInfo = "";						// 
		private object SyncRoot = null;						// 
		private object SyncSender = null;					// 
		private GPPositionFix mLastFix;						// The last known location of this device
		private DateTime	mLastNewPosTime;			// The time when a movement was last detected
		private ArrayList _accidentBufferIndices = ArrayList.Synchronized(new ArrayList()); // 
		private UnitState	mUnitState;						// The detected state of this device
		private DatabaseInterface mDatabase = null; //
		private Thread mThread;									// 
		private GatewayProtocolPacket retryPacket;	// 
        private int iMinPacketLength = 0;           // The minimum size of packets sent to the unit.
        private float fTotalMass = 0;
        private float fWeightLimit = 0;
        private List<ScheduledTask> _tasks;
        private object _taskSyncObject = new object();
        private List<RequestExtendedHistory> _extendedHistories;
        private object _extendedHistoriesSyncObject = new object();
        private GeneralGPPacket _lastIncidentPacket = null;
        private DateTime _lastIncidentDeviceTime = DateTime.MinValue;
        private DateTime _lastIncidentEventTime = DateTime.MinValue;
        private int _lastIncidentBeforeCount;
        private int _lastIncidentAfterCount;
        private System.Threading.Timer _lastIncidentTimer;
        private int _packetsSent = 0;

        #endregion
        #region Static Vars
        // Concrete states are from		0x02000000 
        //		down to							0x00040000
        // Which is a mask of:				0x03FC0000    
        public static readonly int ConcreteFieldMask = 0x13FC0000;   
		private static object _syncObj = new object();
		private static long[] _oldStatusPatterns = null;
		private static long[] _newStatusPatterns = null;
		private static long[] _oldAuxiliaryPatterns = null;
		private static long[] _newAuxiliaryPatterns = null;

		#endregion
		#region Constants
		private const long STATUS_AUXILIARY_MDTSTATE1 = 0x0001;
		private const long STATUS_AUXILIARY_MDTSTATE2 = 0x0002;
		private const long STATUS_AUXILIARY_MDTSTATE3 = 0x0004;
		private const long STATUS_AUXILIARY_MDTSTATE4 = 0x0008;
		private const long STATUS_AUXILIARY_AUXSTATE1 = 0x0010;
		private const long STATUS_AUXILIARY_AUXSTATE2 = 0x0020;
		private const long STATUS_AUXILIARY_AUXSTATE3 = 0x0040;
		private const long STATUS_AUXILIARY_AUXSTATE4 = 0x0080;
		private const long STATUS_AUXILIARY_GLOBALSTAR = 0x0100;
		private const long STATUS_AUXILIARY_GLOBALSTAR_ALERT_1 = 0x0200;
		private const long STATUS_AUXILIARY_GLOBALSTAR_ALERT_2 = 0x0400;
		private const long STATUS_AUXILIARY_GLOBALSTAR_ALERT_3 = 0x0800;
		private const long STATUS_AUXILIARY_GLOBALSTAR_ALERT_4 = 0x1000;
		#endregion

        #region Acknowledgement delegates (for Minimail confirmed delivery)
        /// <summary>
        ///  The user-provided delegate function will be called when the given packet has
        ///  been acknowledged by the remote device. Only one delegate can be provided.
        /// </summary>
        /// <param name="del"></param>
        /// <param name="p"></param>
        public void SetAcknowledgementNotificationDelegate(AcknowledgementNotificationDelegate del,
            GatewayProtocolPacket p
            )
        {
            try
            {
                mAckDelegate = del;
                mAckNotifyMatchPacket = p;
            }
            catch (System.Exception ex1)
            {
                WriteToErrorConsole(ex1);
            }
        }
        public void RemoveAcknowledgementNotificationDelegate()
        {
            try
            {
                mAckDelegate = null;
                mAckNotifyMatchPacket = null;
            }
            catch (System.Exception ex1)
            {
                WriteToErrorConsole(ex1);
            }
        }
        #endregion

		#region Constructors
		
		public Vehicle(GatewayProtocolPacket aPacket, ref DatabaseInterface DBInterface, string serverTime_DateFormat)
		{
			mDatabase = DBInterface;
            CreateVehicle(aPacket.cFleetId, aPacket.iVehicleId, aPacket, serverTime_DateFormat);
			mSenderIP = aPacket.mSenderIP;
		}

        public Vehicle(byte cFleetId, uint iVehicleId, GatewayProtocolPacket aPacket, DatabaseInterface DBInterface, string serverTime_DateFormat)
        {
            mDatabase = DBInterface;
            CreateVehicle(cFleetId, iVehicleId, aPacket, serverTime_DateFormat);
        }

        public Vehicle(byte cFleetId, uint iVehicleId, GatewayProtocolPacket aPacket, ref DatabaseInterface DBInterface, string serverTime_DateFormat)
        {
            mDatabase = DBInterface;
            CreateVehicle(cFleetId, iVehicleId, aPacket, serverTime_DateFormat);
        }

        private void CreateVehicle(byte cFleetId, uint iVehicleId, GatewayProtocolPacket aPacket, string serverTime_DateFormat)
        {
            mAckDelegate = null;
            mAckNotifyMatchPacket = null;

            oFlashReportDictionary = new SortedDictionary<DateTime, GeneralGPPacket>();
            _serverTime_DateFormat = serverTime_DateFormat;
            SyncRoot = new object();
            SyncSender = new object();
            oUnitWPs = ArrayList.Synchronized(new ArrayList());
            oUnitSpecail1WPs = ArrayList.Synchronized(new ArrayList());
            oUnitSpecail2WPs = ArrayList.Synchronized(new ArrayList());
            iRetries = 0;
            mPacket = aPacket;
            bUnitAlive = true;
            cFleet = cFleetId;
            iUnit = iVehicleId;
            //POD - taken care of in PRoperty Setters..
            //iUniqueId = VehicleCollection.GetUniqueUnitId(aPacket);
            mSenderIP = null;
            cOurSequence = 0;
            cAckSequence = 0;
            iBitmap = 0;
            bInRegardlessMode = false;
            cLastSeq = 255;
            cLastAck = 255;
            cSecondLastAck = 255;
            cLastFileSent = 0;
            cSecondLastFileSent = 0;
            cThirdLastFileSent = 0;
            bNeedsConfigCheck = true;
            mUnitState = UnitState.NotKnown;
            mLastFix = new GPPositionFix();
            mLastNewPosTime = DateTime.UtcNow;
            SwallowThisPacket = false;
            MostRecentLiveUpdatePacket = null;
            oWayPointIDs = new System.Collections.ArrayList();
            oAddtionalLiveUpdatePackets = new System.Collections.ArrayList();
            mDatabase.GetUnitType(cFleet, iUnit, ref _bIsTrackingUnit, ref _bIsRefrigerationUnit);
            mDatabase.GetMassForUnit(cFleet, iUnit, ref fTotalMass, ref fWeightLimit);
            oWatchListPacket = mDatabase.GetWatchListState(cFleet, iUnit);
            oFirmwareLoginReply = null;
            oSendScriptFilePacket = null;
            mDatabase.eRefreshDatasets += new DatabaseInterface.RefreshDataSetsDelegate(mDatabase_RefreshDatasets);
            mDatabase.OnRefreshScheduledTaskDataset += new DatabaseInterface.RefreshDataSetsDelegate(mDatabase_OnRefreshScheduledTaskDataset);
            try
            {
                iMinPacketLength = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinOutgoingPacketSize"]);
            }
            catch (System.Exception)
            {
                iMinPacketLength = 0;
            }
            UpdateServerTrackingRules();
            UpdateTasks();
            UpdateExtendedHistoryRequests();

            //check if there is an existing out of hours usage for this vehicle
            if (mDatabase.FindActiveOutOfHoursUsage((int)_cFleet, (int)iUnit) > 0)
            {
                ServerUnitStateEx |= (long)StatusVehicle59.STATUS_OUT_OF_HOURS;
            }
            //check if there is an existing Stationary alert for this vehicle
            DataRow row = mDatabase.FindStationaryAlert((int)_cFleet, (int)iUnit);
            if (row != null)
            {
                bool active = Convert.ToBoolean(row["Active"]);
                if (active)
                {
                    ServerUnitStateEx |= (long)StatusVehicle59.STATUS_STATIONARY_ALERT;
                }
            }

            //if concreate vehicle, need to load last state from database
            InitialiseConcrete();
        }
		#endregion
		#region Sequence Numbering
		public void InitialiseBitmap(byte aStartSeq)
		{
			iBitmap = 0xFFFFFFFF; // Simulate that we've got everything
			cLastSeq = aStartSeq;
		}

		// Add the given sequence number to the SN bitmap we're keeping
		// for the given unit ID
		public void AddToBitmap (byte aSeq)
		{
			bool processGaps = true;

			// ignore duplicate sequence numbers:
			if (cLastSeq == aSeq) return; // nothing has changed - we can return

			// Set the appropriate bit
			iBitmap |= (uint) ( 1 << (aSeq));

			// Also allow duplicates of the last 15 packets we got: 
			// Hence there is a window, trailing 15 packets behind the
			// most recently-received packet
			int dupWindowSize = SEQ_WINDOW_SIZE;
			int lowestAllowedDuplicate;
			if (cLastSeq >= dupWindowSize) 
			{
				// Normal case - example: we just got 14, now we rxed 12
				// so lastSeq = 14, aSeq = 12, lowestAllowed = 4
				lowestAllowedDuplicate = cLastSeq - dupWindowSize;
				if ((aSeq < cLastSeq) && (aSeq >= lowestAllowedDuplicate))
				{
					processGaps = false;
				}
			}
			else
			{
				// Rollover case - example: we just got 3, now we rxed 1
				// so lastSeq = 3, aSeq = 1, lowestAllowed = 27
				lowestAllowedDuplicate = (SEQ_BITMAP_SIZE - dupWindowSize) + cLastSeq;
				
				if (aSeq < cLastSeq)
				{
					// Rxed packet must be in the first few, no problem
					// This is like the above example case
				{
					processGaps = false; 
				}
				}
				else if ((aSeq > lowestAllowedDuplicate) &&
					(aSeq < SEQ_BITMAP_SIZE))
				{
					// Example here is lastSeq = 2, aSeq = 29, lowest = 26
					processGaps = false;
				}
			}

			if (processGaps)
			{
				// Get to here if the packet was not a legal duplicate, and hence should be
				// fully processed for gaps

				// Clear any bits between this packet and the last we got!
				if ( cLastSeq <= (aSeq) )
				{	// normal case, plus the ideal case
					for (int pos = cLastSeq+1; (pos < aSeq); pos++)
					{
						iBitmap &= (uint) ~((1 << (pos)));
					}
				}
				else
				{	// rollover case
					// Top bit
					for (int pos = cLastSeq+1; (pos < SEQ_BITMAP_SIZE); pos++)
					{
						iBitmap &= (uint) ~((1 << (pos)));
					}
					
					// Lower chunk - Process from 0 up to aSeq:
					for (int pos = 0; (pos < aSeq); pos++)
					{
						iBitmap &= (uint) ~((1 << (pos)));
					}
					
				}
			}
			// Store everything back for next time
			
			cSecondLastSeq = cLastSeq;	// Shuffle up the last seq
			cLastSeq = aSeq;	// Last seq was this one

		}

		// Check for holes in the bitmap:
		// Report whether we have any holes in the bitmap for this unit
		// from position <startseq> back to <lastseq>, including rollovers
		public byte VerifyBitmap( int aStartSeq)
		{
			int retVal = -1;
			int lastSeq;

			// Begin the search at the last packet we acknowledged:
			lastSeq = cLastAck;
			
			// Only had a few packets, or perhaps just rolled over
			if (aStartSeq < lastSeq)
			{
				// Check the top part of the window:
				for (int pos = lastSeq; pos < SEQ_BITMAP_SIZE; pos++)
				{
					if ((iBitmap & ( 1 << (int) pos)) == 0) 
					{
						retVal = pos;
						break;
					}
				}
				if ( retVal == -1)
				{	// Haven't found it in the top part
					// Check the rolled-over area:
					for (int pos = 0; pos < aStartSeq; pos++)
					{
						// Look through what we do have
						if ((iBitmap & ( 1 << (int) pos)) == 0)
						{
							retVal = pos;
							break;
						}
					}
				}
			}
			else // Normal case - this sequence is larger than the previous one
			{
				for (int pos = lastSeq; pos < aStartSeq; pos++)
				{
					// Look through the last few packets
					if ((iBitmap & (  1 << (int) pos)) == 0) 
					{
						retVal = pos;
						break;
					}
				}
			}
			// Get to here if all was consistent. We'll return the same number, to
			// indicate no holes up to the requested point
			if (retVal == -1) retVal = aStartSeq;
			else
			{
				// retVal currently indicates the lost packet.
				// we must return the LAST packet we actually GOT!
				// so subtract one, and allow for a reverse-rollover
				retVal--;
				if (retVal == -1) retVal = 31;
			}
			return (byte) retVal;
		}
		
		// We've just sent something - inc the sequence number in the 
		// centralised list:
		public void IncrementSequenceNumber()
		{
			cOurSequence++;
			if (cOurSequence > 31) cOurSequence = 0;
		}

		#endregion
		#region Send Methods
		public void SendWithNoRetries()
		{
			SendOnce(mPacket);
		}

		public void SendWithRetries(int aMaxRetries, int aIntervalSecs)
		{
			lock(this.SyncRoot)
				bResendActive = false;

			iRetries = 0;
			iMaxRetries = aMaxRetries;
			iTimeout = aIntervalSecs * 1000; // convert to msec for Timer
			if (iTimeout < 5000) iTimeout = 5000;
			ThreadStart myThreadStart = new ThreadStart(this.HandleThread);
			mThread= new Thread(myThreadStart);
			mThread.IsBackground = true;
			lock(this.SyncRoot)
				bResendActive = true;
			mThread.Start();
			lock(this.SyncRoot)
			{
				retryPacket = mPacket;
			}
			SendOnce(mPacket);			// Send the first
		}

		public void SendAck()
		{
			GeneralGPPacket ackPacket = new GeneralGPPacket( mPacket, _serverTime_DateFormat);
			ackPacket.bAckRegardless = false;
			ackPacket.bAckImmediately = false;
            //if (mPacket._receivedFlashAvailable)
//                ackPacket._receivedFlashAvailable = true;
			ackPacket.cMsgType = GeneralGPPacket.GEN_ACK;
			ackPacket.iLength = 0;
            ackPacket.ServerLoadLevel = mDatabase.CurrentServerLoadLevel;
            ackPacket.ServerLoadStatusReport = mDatabase.CurrentServerLoadStatus;
            byte[] dummy = new byte[0];
            ackPacket.Encode(ref dummy);
			SendOnce(ackPacket);
		}
		public void SendOnce(GatewayProtocolPacket p)
		{
			SendOnce(p, false);
		}

		public void SendOnce(GatewayProtocolPacket p, bool overrideSequence)
		{
			Byte[] encodedBytes = null;
            byte[] bExtendedBytes = null;
			// See if we have to revert to primary:
			try
			{
                if (mSenderIP != null && ((int)this.cFleet > 0 && this.iUnit > 0) || (p.cMsgType == GeneralGPPacket.GEN_ACK || p.cMsgType == GeneralGPPacket.GEN_CLEAR_SENDBUF || p.cMsgType == GeneralGPPacket.GEN_RESET))
                {
                    p.bRevertToPrimaryServer = bRevertUnitToPrimary;
                    p.cFleetId = this.cFleet;
                    p.iVehicleId = this.iUnit;
                    if (!overrideSequence)
                    {
                        p.cAckSequence = this.cAckSequence;
                        p.cOurSequence = this.cOurSequence;
                    }
                    p.bSendFlash = mDatabase.SendFlash;

                    p.Encode(ref encodedBytes);
                    p.mSenderIP = this.mSenderIP;
                    if (iMinPacketLength > 0)
                    {
                        if (encodedBytes.Length < iMinPacketLength)
                        {
                            bExtendedBytes = new byte[iMinPacketLength];
                            for (int X = 0; X < encodedBytes.Length; X++)
                            {
                                bExtendedBytes[X] = encodedBytes[X];
                            }
                            lock (SyncSender)
                            {
                                mSender.Send(bExtendedBytes, bExtendedBytes.Length, this.mSenderIP);
                                _packetsSent++;
                            }
                            _log.Debug($"Sent to : {mSenderIP?.Address}:{mSenderIP?.Port}" + ConvertToAscii(bExtendedBytes));
                        }
                        else
                        {
                            lock (SyncSender)
                            {
                                mSender.Send(encodedBytes, encodedBytes.Length, this.mSenderIP);
                                _packetsSent++;
                            }
                            _log.Debug($"Sent to : {mSenderIP?.Address}:{mSenderIP?.Port}" + ConvertToAscii(encodedBytes));
                        }
                    }
                    else
                    {
                        lock (SyncSender)
                        {
                            mSender.Send(encodedBytes, encodedBytes.Length, this.mSenderIP);
                            _packetsSent++;
                        }
                        _log.Debug($"Sent to : {mSenderIP?.Address}:{mSenderIP?.Port}" + ConvertToAscii(encodedBytes));
                    }
                }
                else
                {
                    _log.Error("Error Sending to Unit : Requested a send to fleet 0 vehicle 0.");
                }
			}
			catch(System.Exception ex)
			{
                _log.Error("Error Sending to Unit : " + ex.Message);
			}
		}
		private void mTimer_Elapsed()
		{
			GatewayProtocolPacket oPacket = null;
			try
			{
				if (bResendActive)
				{
					// Called if we timed out and need to send again
					iRetries++;

					if (iRetries <= iMaxRetries) 
					{
						lock(this.SyncRoot)
						{
							oPacket = retryPacket;
						}
						SendOnce(oPacket);
					}
					else
					{
						iRetries = 0;
						lock(this.SyncRoot)
							bResendActive = false;
						if (SendRetriesExceeded != null)
						{
							try
							{
								SendRetriesExceeded(this, retryPacket);
							}
							catch(Exception)
							{
								
							}
						}
						bUnitAlive = false;
					}
				}
			}
			catch(System.Exception ex)
			{
				Console.WriteLine("Resend Timer expired exception : " + ex.Message);
			}
		}

		#endregion
		#region Thread Handlers
		public void StopThread()
		{
			try
			{
				lock(this.SyncRoot)
					bResendActive = false;
			}
			catch(System.Exception)
			{
			}
			try
			{
				if (mThread != null) mThread.Abort();
			}
			catch(System.Exception)
			{
			}
		}
		private void HandleThread() 
		{
			int X = 0;
			int iTarget = iTimeout / 100;				//in miliseconds, and dividing for loop sleep
			Console.Write(System.DateTime.Now.ToString("HH:mm:ss") + " :  Resend Timer Started.\n");
			while (bResendActive)
			{
				try
				{
					X++;
					if (X == iTarget)
					{
						mTimer_Elapsed();
						X = 0;
					}
					Thread.Sleep(100);						//sleeping for 100ms
				}
				catch(System.Exception ex)
				{
					Console.Write(System.DateTime.Now.ToString("HH:mm:ss") + " : " + ex.Message);
				}
			}
			Console.Write(System.DateTime.Now.ToString("HH:mm:ss") + " :  Resend Timer End.\n");
		}
		#endregion
		#region Log Event Interface Support
		private void WriteToConsole(string sMsg)
		{
			string sTemp = "";
			if(mSenderIP != null)
				sTemp += " (" + mSenderIP.Address.ToString() + ":" + Convert.ToString(mSenderIP.Port) + ")";
			sTemp += " (F : " + Convert.ToString(this.cFleet) + ", V : " + Convert.ToString(this.iUnit) + "), " + sMsg;
			if(eConsoleEvent != null) eConsoleEvent(sTemp.Trim());
		}
		private void WriteToErrorConsole(System.Exception ex)
		{
			string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;

			WriteToConsole(sMsg);
		}
		#endregion	
		#region ConvertTo Ascii
		public static string ConvertToAscii(byte[] bPacket)
		{
			return ConvertToAscii(bPacket, true);
		}

		public static string ConvertToAscii(byte[] bPacket, bool bAddSpacing)
		{
			string sRet = "";
			int X = 0;
			//			byte bLower = 0x21;	// 33
			//			byte bUpper = 0x7E;  // 126
			byte bTest = 0x00;  // null
			byte[] bTestArray = new byte[1];

			if (bPacket.Length > 0)
			{
				try
				{
					for (X = 0; X<bPacket.Length; X++)
					{
						bTest = bPacket[X];	
						bTestArray = new byte[1];
						bTestArray[0] = bTest;
						if (bTest >= 33 && bTest <= 126 )  // If the character is in printable range
							if (bAddSpacing)
								if (sRet.Length > 0)
									sRet += " - " + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
								else
									sRet = System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
							else
								sRet += System.Text.ASCIIEncoding.ASCII.GetString(bTestArray);
						else  // Show it as a number
							if (bAddSpacing)
							if (sRet.Length > 0)
								sRet += " - [" +  bTest + "]";
							else
								sRet = "[" + Convert.ToString(bTest) + "]";
						else
							sRet += "[" + System.Text.ASCIIEncoding.ASCII.GetString(bTestArray) + "]";
					}
					
				}
				catch(System.Exception ex)
				{
					Console.Write(ex.Message + "\n");
				}
			}
			return sRet;
		}
		public new string ToString()
		{
			// Describe what this thread is doing at this instant:
			return	"Sending " + GatewayProtocolLookup.GetMessageType(mPacket.cMsgType) +
				" to Unit " + mPacket.iVehicleId + ", Fleet " + mPacket.cFleetId +
				", IsAlive = " + bUnitAlive.ToString();
		}
		#endregion
		#region Unit State stuff
		public void UpdateConcreteState(UnitState newState)
		{
			// The concrete states are mutually exclusive,
			// so if there is any bit set, overwrite the 
			// concrete state area. Otherwise, just leave it:

			if (newState == UnitState.NotKnown) return;

			mUnitState = (UnitState) (((int) mUnitState) & ~ConcreteFieldMask);

			mUnitState |= newState;

		}

		public void UpdateUnitState(UnitState newState)
		{
			#region Logic to control some of the mutually-exclusive states:
			mUnitState |= newState;
			switch (newState)
			{
				case UnitState.AtWayPoint:
					//currentState |= UnitState.AtWayPoint;
					mUnitState &= ~UnitState.DepWayPoint;
					break;
				case UnitState.DepWayPoint:
					//currentState |= UnitState.DepWayPoint;
					mUnitState &= ~UnitState.AtWayPoint;
					break;
				case UnitState.IgnitionOn:
					mUnitState &= ~UnitState.IgnitionOff;
					break;
				case UnitState.IgnitionOff:
					mUnitState &= ~UnitState.IgnitionOn;
					break;
					//case UnitState.MDTState1:
					//	currentState &= ~UnitState.MDTState2;
					//	currentState &= ~UnitState.MDTState3;
					//	currentState &= ~UnitState.MDTState4;
					//	break;
					// etc 
				default:
					break;
			}
			#endregion
		}

		public void UpdateUnitState(InternalUnitState newState)
		{
			#region Adjust the state based on these not-externally-visible states:
			switch (newState)
			{
				case InternalUnitState.Input1Off:
					mUnitState &= ~UnitState.Input1On;
					break;
				case InternalUnitState.Input2Off:
					mUnitState &= ~UnitState.Input2On;
					break;
				case InternalUnitState.ExcessiveIdleOver : 
					mUnitState &= ~UnitState.ExcessiveIdle;
					break;
				default:
					break;
			}
			#endregion
		}

		public void UpdateUnitPosition(GPPositionFix fix, GPClock fixClock)
		{
			if (fix == null) return;
			if (fixClock == null) return;
            if (!fixClock.IsDate(null)) return;

			if (!mLastFix.bIsPopulated)
			{	// Never had fix before, so update
				mLastFix = fix;
			}
			else
			{	
				// We must also be LATER in time than the one we're comparing against -
				// this protects us against "replay" problems:

				TimeSpan diff = fixClock.ToDateTime().Subtract(mLastNewPosTime);
				if (diff.TotalSeconds <= 0) return;
		
				// Compare with previous fix:
				if ((System.Math.Abs(fix.dLatitude - mLastFix.dLatitude) > dPositionThreshold) ||
					(System.Math.Abs(fix.dLongitude - mLastFix.dLongitude) > dPositionThreshold))
				{
					// Position is different enough to say "we've moved"
					mLastNewPosTime = fixClock.ToDateTime();
					mLastFix = fix;
				}
			}
		}

		public DateTime GetLastNewPositionTime()
		{
			return mLastNewPosTime;
		}

		public UnitState GetUnitState()
		{
			return mUnitState;	
		}

		public static bool IsBitSet(UnitState us, UnitState testBit)
		{
			if ((us & testBit) == testBit) return true;
			return false;
		}

		public void SetStateBit(UnitState newBit)
		{
			this.mUnitState |= newBit;
		}

		public void ClearStateBit(UnitState newBit)
		{
			this.mUnitState &= ~newBit;
		}

        public void ClearStateFlags(long stateFlags)
        {
            long stateValue = (long)this.mUnitState;
            stateValue &= ~stateFlags;
            this.mUnitState = (UnitState) stateValue;
        }

        public void SetStateFlags(long stateFlags)
        {
            long stateValue = (long)this.mUnitState;
            stateValue |= stateFlags;
            this.mUnitState = (UnitState)stateValue;
        }

		public static string UnitStateToString(UnitState us)
		{
			System.Text.StringBuilder s = new System.Text.StringBuilder("");
		
			if (IsBitSet(us,UnitState.AtWayPoint)) s.Append("AtWayPoint ");
			if (IsBitSet(us,UnitState.DepWayPoint)) s.Append("DepWayPoint ");
			if (IsBitSet(us,UnitState.IgnitionOn)) s.Append("IgnitionOn ");
			if (IsBitSet(us,UnitState.IgnitionOff)) s.Append("IgnitionOff ");
			if (IsBitSet(us,UnitState.Input1On)) s.Append("Input1On ");
			if (IsBitSet(us,UnitState.Input2On)) s.Append("Input2On ");
			if (IsBitSet(us,UnitState.Stationary1)) s.Append("Stationary1 ");
			if (IsBitSet(us,UnitState.Stationary2)) s.Append("Stationary2 ");
			if (IsBitSet(us,UnitState.Stationary3)) s.Append("Stationary3 ");
			if (IsBitSet(us,UnitState.DepStationary)) s.Append("DepStationary ");
			if (IsBitSet(us,UnitState.MDTState1)) s.Append("MDTState1 ");
			if (IsBitSet(us,UnitState.MDTState2)) s.Append("MDTState2 ");
			if (IsBitSet(us,UnitState.MDTState3)) s.Append("MDTState3 ");
			if (IsBitSet(us,UnitState.MDTState4)) s.Append("MDTState4 ");
			if (IsBitSet(us,UnitState.AuxState1)) s.Append("AuxState1 ");
			if (IsBitSet(us,UnitState.AuxState2)) s.Append("AuxState2 ");
			if (IsBitSet(us,UnitState.AuxState3)) s.Append("AuxState3 ");
			if (IsBitSet(us,UnitState.AuxState4)) s.Append("AuxState4 ");
			if (IsBitSet(us,UnitState.AtDepotNoLoad)) s.Append("AtDepotNoLoad ");
			if (IsBitSet(us,UnitState.AtDepotLoading)) s.Append("AtDepotLoading ");
			if (IsBitSet(us,UnitState.AtDepotLoaded)) s.Append("AtDepotLoaded ");
			if (IsBitSet(us,UnitState.OnWayToJob)) s.Append("OnWayToJob ");
			if (IsBitSet(us,UnitState.SwitchingPlants)) s.Append("SwitchingPlants ");
			if (IsBitSet(us,UnitState.OnSite)) s.Append("OnSite	 ");
			if (IsBitSet(us,UnitState.UnloadingAtSite)) s.Append("UnloadingAtSite ");
			if (IsBitSet(us,UnitState.ReturningHome)) s.Append("ReturningHome ");
			if (s.Length == 0) return "NotKnown";

			return s.ToString();
		}
		#endregion
		#region Status Mapping
		public long GetAuxiliaryStateForReport(RoutePtSetPtGPPacket statusPacket)
		{
			return MapOldStatusToAuxiliaryState();
		}

		public long GetUnitStateForReport(RoutePtSetPtGPPacket statusPacket)
		{
			if (statusPacket.mExtendedVariableDetails.Populated)
				return (MapOldStatusToUnitState() & ~((long)StatusVehicle59.STATUS_WP_DOCK)) | ServerUnitStateEx | statusPacket.mExtendedVariableDetails.UnitStatus;
			else
				return MapOldStatusToUnitState() | ServerUnitStateEx;
		}

		public long GetAuxiliaryStateForReport(RoutingModuleGPPacket statusPacket)
		{
			return MapOldStatusToAuxiliaryState();
		}

		public long GetUnitStateForReport(RoutingModuleGPPacket statusPacket)
		{
			if (statusPacket.mExtendedVariableDetails.Populated)
				return statusPacket.mExtendedVariableDetails.UnitStatus;
			else
				//	Convert the old status to the new status.
				return MapOldStatusToUnitState();
		}

		/// <summary>
		/// this method will retrieve the auxiliary 
		/// state of the unit at the date and time specified
		/// this is calculated from a list of state changes maintained 
		/// by the remote ad vehicle state update code.
		/// </summary>
		/// <param name="requestTime"></param>
		/// <returns></returns>
		public long GetAuxiliaryStateForReport(GeneralGPPacket statusPacket)
		{
            return MapOldStatusToAuxiliaryState();
		}

		/// <summary>
		/// This method will allow the states of older vehicles to be processed
		/// if the new states are not specified.
		/// </summary>
		/// <param name="statusPacket"></param>
		/// <returns></returns>
		public long GetUnitStateForReport(GeneralGPPacket statusPacket)
		{
            long status = 0;
			if (statusPacket.mExtendedVariableDetails.Populated)
				status = (MapOldStatusToUnitState() & ~((long)StatusVehicle59.STATUS_WP_DOCK)) | ServerUnitStateEx | statusPacket.mExtendedVariableDetails.UnitStatus;
			else
				status = MapOldStatusToUnitState() | ServerUnitStateEx;

            if (statusPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON)
            {
                //we are igniton on - remove out of hours status
                status &= ~(long)StatusVehicle59.STATUS_OUT_OF_HOURS;
            }
            return status;
		}
		
		public long MapOldStatusToUnitState()
		{
			return MapOldStatusToUnitState((long)this.State);
		}

		public long MapOldStatusToUnitState(long test)
		{
			if (_oldStatusPatterns == null)
				lock(_syncObj)
					if (_oldStatusPatterns == null)
						PrepareUnitStateMap();
			if(test == 0)
				test = (long)this.State;
			long result = 0;
			for(int loop = 0; loop < _oldStatusPatterns.Length; loop++)
				if ((test & _oldStatusPatterns[loop]) == _oldStatusPatterns[loop])
					result |= _newStatusPatterns[loop];
			return result;
		}

		public long MapOldStatusToAuxiliaryState()
		{
			return MapOldStatusToAuxiliaryState((long)this.State);
		}

		public long MapOldStatusToAuxiliaryState(long test)
		{
			if (_oldAuxiliaryPatterns == null)
				lock(_syncObj)
					if (_oldAuxiliaryPatterns == null)
						PrepareAuxiliaryStateMap();

			if(test == 0)
				test = (long)this.State;
			long result = 0;
			for(int loop = 0; loop < _oldAuxiliaryPatterns.Length; loop++)
				if ((test & _oldAuxiliaryPatterns[loop]) == _oldAuxiliaryPatterns[loop])
					result |= _newAuxiliaryPatterns[loop];
			return result;
		}

		public long MapUnitStateToOldStatus(long test)
		{
			if (_oldStatusPatterns == null)
				lock(_syncObj)
					if (_oldStatusPatterns == null)
						PrepareUnitStateMap();
			if(test == 0)
				test = (long)this.State;
			long result = 0;

			for(int loop = 0; loop < _newStatusPatterns.Length; loop++)
				if ((test & _newStatusPatterns[loop]) == _newStatusPatterns[loop])
					result |= _oldStatusPatterns[loop];
			return result;
		}

		public long MapAuxiliaryStateToOldStatus(long test)
		{
			if (_oldAuxiliaryPatterns == null)
				lock(_syncObj)
					if (_oldAuxiliaryPatterns == null)
						PrepareAuxiliaryStateMap();

			if(test == 0)
				test = (long)this.State;
			long result = 0;
			for(int loop = 0; loop < _newAuxiliaryPatterns.Length; loop++)
				if ((test & _newAuxiliaryPatterns[loop]) == _newAuxiliaryPatterns[loop])
					result |= _oldAuxiliaryPatterns[loop];
			return result;
		}

		private void PrepareAuxiliaryStateMap()
		{
			_oldAuxiliaryPatterns = new long[13];
			_newAuxiliaryPatterns = new long[13];

			int index = 0;
			_oldAuxiliaryPatterns[index] = (long)UnitState.MDTState1;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_MDTSTATE1;

			_oldAuxiliaryPatterns[index] = (long)UnitState.MDTState2;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_MDTSTATE2;

			_oldAuxiliaryPatterns[index] = (long)UnitState.MDTState3;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_MDTSTATE3;

			_oldAuxiliaryPatterns[index] = (long)UnitState.MDTState4;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_MDTSTATE4;

			_oldAuxiliaryPatterns[index] = (long)UnitState.AuxState1;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_AUXSTATE1;

			_oldAuxiliaryPatterns[index] = (long)UnitState.AuxState2;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_AUXSTATE2;

			_oldAuxiliaryPatterns[index] = (long)UnitState.AuxState3;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_AUXSTATE3;
		
			_oldAuxiliaryPatterns[index] = (long)UnitState.AuxState4;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_AUXSTATE4;

			_oldAuxiliaryPatterns[index] = (long)	UnitState.GlobalStarPosition;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_GLOBALSTAR;

			_oldAuxiliaryPatterns[index] = (long)UnitState.GlobalStarAlert1;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_GLOBALSTAR_ALERT_1;

			_oldAuxiliaryPatterns[index] = (long)UnitState.GlobalStarAlert2;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_GLOBALSTAR_ALERT_2;

			_oldAuxiliaryPatterns[index] = (long)UnitState.GlobalStarAlert3;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_GLOBALSTAR_ALERT_3;

			_oldAuxiliaryPatterns[index] = (long)UnitState.GlobalStarAlert4;
			_newAuxiliaryPatterns[index++] = STATUS_AUXILIARY_GLOBALSTAR_ALERT_4;

		}

		private void PrepareUnitStateMap()
		{
			_oldStatusPatterns = new long[16];
			_newStatusPatterns = new long[16];

			int index = 0;
			//	STATUS_WP_AT
			_oldStatusPatterns[index] = (long)UnitState.AtWayPoint;
			_newStatusPatterns[index++] = (long)0x00000001;

			//	STATUS_IGN_ON				0x00000040
			_oldStatusPatterns[index] = (long)UnitState.IgnitionOn;
			_newStatusPatterns[index++] = (long)0x00000040;

			//	STATUS_WP_TOO_LONG		0x00000002

			//	STATUS_INP1_ON				0x00000080
			_oldStatusPatterns[index] = (long)UnitState.Input1On;
			_newStatusPatterns[index++] = (long)0x00000080;

			//	STATUS_INP2_ON				0x00000100	
			_oldStatusPatterns[index] = (long)UnitState.Input2On;
			_newStatusPatterns[index++] = (long)0x00000100;

			//	STATUS_CONC_NOT_LOADED	0x01000000
			_oldStatusPatterns[index] = (long)UnitState.AtDepotNoLoad;
			_newStatusPatterns[index++] = (long)0x01000000;

			//	STATUS_CONC_LOADING		0x02000000
			_oldStatusPatterns[index] = (long)UnitState.AtDepotLoading;
			_newStatusPatterns[index++] = (long)0x02000000;

			//	STATUS_CONC_LOADED		0x04000000
			_oldStatusPatterns[index] = (long)UnitState.AtDepotLoaded;
			_newStatusPatterns[index++] = (long)0x04000000;

			//	STATUS_CONC_TO_JOB		0x08000000
			_oldStatusPatterns[index] = (long)UnitState.OnWayToJob;
			_newStatusPatterns[index++] = (long)0x08000000;

			//	STATUS_CONC_SWITCHING		0x80000000
			_oldStatusPatterns[index] = (long)UnitState.SwitchingPlants;
			_newStatusPatterns[index++] = (long)0x80000000;

			//	STATUS_CONC_AT_JOB		0x10000000
			_oldStatusPatterns[index] = (long)UnitState.OnSite;
			_newStatusPatterns[index++] = (long)0x10000000;

			//	STATUS_CONC_UNLOADING	0x20000000
			_oldStatusPatterns[index] = (long)UnitState.UnloadingAtSite;
			_newStatusPatterns[index++] = (long)0x20000000;

			//	STATUS_CONC_RETURNING		0x40000000
			_oldStatusPatterns[index] = (long)UnitState.ReturningHome;
			_newStatusPatterns[index++] = (long)0x40000000;

			//	STATUS_EXCESS_IDLE			0x00001000
			_oldStatusPatterns[index] = (long)UnitState.ExcessiveIdle;
			_newStatusPatterns[index++] = (long)0x00001000;

			//	STATUS_LOGGED_ON			0x00020000
			_oldStatusPatterns[index] = (long)UnitState.LoggedIn;
			_newStatusPatterns[index++] = (long)0x00020000;

			//	STATUS_LUNCH				0x00040000
			_oldStatusPatterns[index] = (long)UnitState.AtLunch;
			_newStatusPatterns[index++] = (long)0x00040000;

			//	STATUS_WP_DOCK			0x00000004
			_oldStatusPatterns[index] = (long)UnitState.AtWPSpecial2;
			_newStatusPatterns[index++] = (long)0x00000004;
		
		}

		#endregion
		#region Database Refresh event
		private void mDatabase_RefreshDatasets()
		{
			try
			{
				mDatabase.GetUnitType(cFleet, iUnit, ref _bIsTrackingUnit, ref _bIsRefrigerationUnit);
				_sUserInfo = mDatabase.GetUserInfoTag(cFleet, iUnit);
                mDatabase.GetMassForUnit(cFleet, iUnit, ref fTotalMass, ref fWeightLimit);
                UpdateTasks();
                UpdateExtendedHistoryRequests();
			}
			catch(System.Exception)
			{

			}
		}
		#endregion

		#region Load Vehicle Report from database
		public Vehicle LoadVehicleDataFromHistory(DateTime dtDeviceTime)
		{
			Vehicle oRet = null;
			GeneralGPPacket mPacket = null;
			try
			{
				mPacket = mDatabase.LoadVehicleDataFromHistory(Convert.ToInt32(this.cFleet), Convert.ToInt32(this.iUnit), dtDeviceTime, this);
				oRet = new Vehicle(mPacket, ref mDatabase, _serverTime_DateFormat);
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);
				oRet = null;
			}
			return oRet;
		}
		#endregion

        #region server side tracking rule breaks
        public void UpdateServerTrackingRules()
        {
            _serverRuleBreakReasonCodes = mDatabase.GetServerRuleBreaksForVehicle((int)cFleet, iUnit);
        }

	    public void ProcessServerSidePacketForDriverPointRuleBreak(ServerTrackingRule rule, GeneralGPPacket packet)
        {
            _log.Info("ProcessServerSidePacketForDriverPointRuleBreak");
            try
            {
                if (rule != null)
                {
                    if (packet.mFixClock.IsDate(null) && packet.mCurrentClock.IsDate(null))
                    {
                        ////create a Rule Break message
                        DriverPointsServerTrackingRuleBroken unitPacket = new DriverPointsServerTrackingRuleBroken(_serverTime_DateFormat);

                        DateTime dtTemp = DateTime.Now.ToUniversalTime();
                        if(packet.mCurrentClock.IsDate(null))
                            dtTemp = packet.mFixClock.ToDateTime();
                        dtTemp = dtTemp.AddSeconds(1);
                        //send a message back to unit
                        unitPacket.cFleetId = packet.cFleetId;
                        unitPacket.iVehicleId = packet.iVehicleId;
                        unitPacket.mSenderIP = packet.mSenderIP;
                        unitPacket.RuleId = rule.Id;
                        unitPacket.Points = rule.Points;
                        unitPacket.Output1 = (short)rule.Output1;
                        unitPacket.Output2 = (short)rule.Output2;
                        unitPacket.Id = _ruleBreakUniqueId++;
                        unitPacket.GpsTime.FromDateTime(dtTemp);
                        unitPacket.GpsTime.bIsPopulated = true;
                        unitPacket.Location = oPos.PlaceName;
                        byte[] dummy = new byte[1];
                        unitPacket.Encode(ref dummy);
                        SendOnce(unitPacket);
                       _log.Info($"Processed ProcessServerSidePacketForDriverPointRuleBreak rule id: {rule.Id}");
                    }
                }
            }
            catch(System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessServerSidePacketForDriverPointRuleBreak(GeneralGPPacket packet)", ex);
            }
        }

	    public void AddAdditionalLiveUpdate(ServerTrackingRule rule, GeneralGPPacket packet)
	    {
            lock (oAddtionalLiveUpdateSync)
            {
                oAddtionalLiveUpdatePackets.Add(packet);
            }
            ProcessServerSidePacketForDriverPointRuleBreak(rule, packet);
        }

        public void AddAdditionalLiveUpdate(GeneralGPPacket packet)
        {
            var rule = GetTrackingRule(packet);
            AddAdditionalLiveUpdate(rule, packet);
        }

        public ServerTrackingRule GetTrackingRule(GeneralGPPacket packet)
        {
            //check if the reason code is in the list of server rule breaks
            var code = (int)packet.cMsgType;
            var rule = GetTrackingRule(code);
            if (rule == null && packet.cButtonNumber > 0)
            {
                code = Convert.ToInt32((packet.cButtonNumber << 8) + (packet.cMsgType & 0xFF));
                rule = GetTrackingRule(code);
            }
            return rule;
        }

        public ServerTrackingRule GetTrackingRule(int reasonCode)
        {
            _log.DebugFormat("checking server side rule break for {0}", reasonCode);
            foreach (ServerTrackingRule rule in _serverRuleBreakReasonCodes)
            {
                if (rule.ReasonCode == reasonCode)
                {
                    _log.DebugFormat("found server side rule break for {0}", reasonCode);
                    return rule;
                }
            }
            return null;
        }
        #endregion
        #region Scheduled Tasks
        private void mDatabase_OnRefreshScheduledTaskDataset()
        {
            UpdateTasks();
        }
        private void UpdateTasks()
        {
            lock (_taskSyncObject)
            {
                _tasks = mDatabase.GetScheduledTasksForVehicle((int)cFleet, iUnit);
            }
        }
        public void CheckTasks(GatewayProtocolPacket aPacket)
        {
            //find odometer and engine hours
            try
            {
                GeneralGPPacket gpPacket = aPacket as GeneralGPPacket;
                if (gpPacket == null)
                {
                    gpPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                }
                int odometer = 0;
                int engineHours = 0;
                int odometerOffset = OdometerOffset;

                if (gpPacket.mECMAdvanced != null)
                {
                    if (UseGPSOdometer)
                    {
                        if (gpPacket.mECMAdvanced.GPSOdometer() + odometerOffset > 0)
                        {
                            odometer = (int)gpPacket.mECMAdvanced.GPSOdometer() + odometerOffset;
                        }
                        else
                        {
                            odometer = (int)gpPacket.mECMAdvanced.GPSOdometer();
                        }
                    }
                    else if (gpPacket.mECMAdvanced.Odometer() + odometerOffset > 0)
                    {
                        odometer = (int)gpPacket.mECMAdvanced.Odometer() + odometerOffset;
                    }

                    if (gpPacket.mECMAdvanced.TotalEngHours() > 0)
                    {
                        engineHours = (int)gpPacket.mECMAdvanced.TotalEngHours() + EngineHourOffset;
                    }
                }
                else if (gpPacket.mTransportExtraDetails != null) {
                    if (gpPacket.mTransportExtraDetails.iOdometer + odometerOffset > 0)
                    {
                        odometer = (int)gpPacket.mTransportExtraDetails.iOdometer + odometerOffset;
                    }

                    if (gpPacket.mTransportExtraDetails.iEngineHours > 0)
                    {
                        engineHours = gpPacket.mTransportExtraDetails.iEngineHours + EngineHourOffset;
                    }
                }

                //iterate through each rule and check if odo or engine hours triggers next state
                lock (_taskSyncObject)
                {
                    foreach (ScheduledTask t in _tasks)
                    {
                        t.CheckOdoAndEngineHours(odometer, engineHours, mDatabase);
                    }
                }
            }
            catch (Exception exp)
            {
                _log.Error("Error checking vehicle's tasks", exp);
            }
        }

        public TaskStates GetCombinedTaskStates()
        {
            TaskStates state = TaskStates.Unknown;
            lock (_taskSyncObject)
            {
                foreach (ScheduledTask t in _tasks)
                {
                    if (t.State < TaskStates.Complete && t.State > state)
                    {
                        state = t.State;
                    }
                }
            }
            return state;
        }
        #endregion

        #region Extended History Requests
        public void UpdateExtendedHistoryRequests()
        {
            lock (_extendedHistoriesSyncObject)
            {
                _extendedHistories = mDatabase.GetExtendedHistoryForVehicle((int)cFleet, iUnit);
            }
        }

        public void ExtentedHistoryRequestSent(RequestExtendedHistory request)
        {
            request.RequestSentDate = DateTime.UtcNow;
            mDatabase.UpdateExtendedHistoryRequestSent(request);
        }

        public RequestExtendedHistory GetNextExtendedHistoryRequest()
        {
            lock (_extendedHistoriesSyncObject)
            {
                if (_extendedHistories != null && _extendedHistories.Count > 0)
                {
                    return _extendedHistories[0];
                }
            }
            return null;
        }
        public void ExtendedHistoryComplete()
        {
            RequestExtendedHistory request = GetNextExtendedHistoryRequest();
            if (request != null && request.RequestSentDate.HasValue)
            {
                mDatabase.UpdateExtendedHistoryRequestComplete(request, this);
                _log.InfoFormat("Extended History request {2} is complete for {0}:{1}", _cFleet, _iUnit, request.Id);
                lock (_extendedHistoriesSyncObject)
                {
                    _extendedHistories.Remove(request);
                }
            }
        }
        #endregion

        private void InitialiseConcrete()
        {
            try
            {
                string fleetsStr = ConfigurationManager.AppSettings["ConcreteFleets"];
                string[] fleets = fleetsStr.Split(new char[] { ',' });
                foreach (string s in fleets)
                {
                    if (cFleet == Convert.ToByte(Convert.ToInt32(s) & 0xFF))
                    {
                        //we are concrete fleet, load the last vehicle record
                        DataRow row = mDatabase.LoadVehicleDataFromHistory((int)cFleet, (int)iUnit, DateTime.Now.ToUniversalTime());
                        if (row != null)
                        {
                            //need to get UserDefined
                            string userDefined = Convert.ToString(row["UserDefined"]);
                            string[] splits = userDefined.Split(new char[] { '%' });
                            if (splits.Length > 0)
                            {
                                State = (UnitState)Convert.ToInt64(splits[0]);
                            }
                        }
                        return;
                    }
                }
            }
            catch (Exception exp)
            {
                _log.ErrorFormat("Error initailising concrete status. {0}", exp.Message);
            }
        }

        #region incident Checking
        public bool NewIncidentBufferRecieved(GeneralGPPacket gPacket)
        {
            _log.DebugFormat("INCIDENT_EVENT :: NewIncidentBufferRecieved for fleet {0}, vehicle {1}", gPacket.cFleetId, gPacket.iVehicleId);

            if (_lastIncidentPacket != null)
            {
                //cancel current event
                if (SendPossibleAccidentLiveUpdate != null)
                {
                    SendPossibleAccidentLiveUpdate(this, null);
                }
                //stop existing timer
                if (_lastIncidentTimer != null)
                {
                    _lastIncidentTimer.Dispose();
                    _lastIncidentTimer = null;
                }
            }

            //check fleet setting on wether to start timer
            if (mDatabase.IsFleetPossibleAccident((int)_cFleet))
            {

                //only store the incident if it is not at a workshop
                if (!((gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_WP_WORKSHOP) == (long)StatusVehicle59.STATUS_WP_WORKSHOP))
                {
                    _log.Debug("INCIDENT_EVENT :: Not at a workshop so starting timer");
                    _lastIncidentPacket = gPacket;
                    _lastIncidentDeviceTime = gPacket.mCurrentClock.ToDateTime();
                    _lastIncidentEventTime = DateTime.Now;
                    _lastIncidentAfterCount = 0;
                    _lastIncidentBeforeCount = 0;
                    IsWaitingForAccident = true;

                    // How long to initially wait after receiving the incident event (5 minutes) to then check whether to raise an Possible Accident.
                    int initialWait = 300000;

                    // Start timer
                    _lastIncidentTimer = new System.Threading.Timer(IncidentTimerCallback, null, initialWait, Timeout.Infinite);
                    return true;
                }
                IsWaitingForAccident = false;
                _log.Debug("INCIDENT_EVENT :: At a workshop so not checking fro possible accident");
            }
            else
            {
                IsWaitingForAccident = false;
                _log.Debug("INCIDENT_EVENT :: fleet setting for possible accident switched off");
            }
            return false;
        }

        public void NewIncidentRecieved(DateTime deviceTime)
        {
            //if time is in the 2 minutes before or after increase correct counter
            if (_lastIncidentPacket != null)
            {
                _lastIncidentEventTime = DateTime.Now;

                // The unit is sending 120 seconds before and after but we aren't confident it is inside the 120 seconds
                int timePeriod = 200;

                TimeSpan span = deviceTime - _lastIncidentDeviceTime;
                if (span.TotalSeconds > 0 && span.TotalSeconds <= timePeriod)
                {
                    _lastIncidentAfterCount++;
                }
                else if (span.TotalSeconds < 0 && span.TotalSeconds >= -timePeriod)
                {
                    _lastIncidentBeforeCount++;
                }
                else
                {
                    _log.Debug(string.Format("INCIDENT_EVENT :: NewIncidentRecieved. OutsideRange.  TotalSeconds:{0}, GpsTime:{1}, LastIncidentTime:{2}", span.TotalSeconds, deviceTime, _lastIncidentDeviceTime));
                }

                _log.Debug("INCIDENT_EVENT :: NewIncidentRecieved. AfterCount:" + _lastIncidentAfterCount + "  BeforeCount:" + _lastIncidentBeforeCount + " VehicleID:" + _lastIncidentPacket.iVehicleId);

                // How many buffer events to expect (should be 120 after and 120 before the incident)
                // was 20 and should be 120 but sometimes we don't get all 120 packets! so set to 100
                int reportsToReceive = 100; 
                if (_lastIncidentAfterCount >= reportsToReceive && _lastIncidentBeforeCount >= reportsToReceive)
                {
                    //stop timer and reset last incident time
                    _lastIncidentDeviceTime = DateTime.MinValue;
                    _lastIncidentPacket = null;
                    IsWaitingForAccident = false;

                    _log.Debug("INCIDENT_EVENT :: _lastIncidentPacket is being cleared");

                    if (SendPossibleAccidentLiveUpdate != null)
                    {
                        SendPossibleAccidentLiveUpdate(this, null);
                    }
                    if (_lastIncidentTimer != null)
                    {
                        _lastIncidentTimer.Dispose();
                        _lastIncidentTimer = null;
                    }
                }
            }
            else
            {
                _log.Debug("INCIDENT_EVENT :: NewIncidentRecieved BUT LAST INCIDENT PACKET IS NULL");
            }
        }

        private void IncidentTimerCallback(Object state)
        {
            try
            {
                //stop timer
                if (_lastIncidentTimer != null)
                {
                    _lastIncidentTimer.Dispose();
                    _lastIncidentTimer = null;
                }

                // Calculate the time difference from the last incident event or incident buffer event
                var timeDiff = DateTime.Now - _lastIncidentEventTime;

                _log.Debug("INCIDENT_EVENT :: Incident timer callback. LastTime:" + _lastIncidentEventTime + "  Now:" + DateTime.Now + "  Diff: " + timeDiff.TotalSeconds);

                // If it hasn't been 120 seconds since we last received an incident event report, then wait another 30 seconds. While we are still receiving
                // incident events we have to keep the timer active.
                if (timeDiff.TotalSeconds < 120)
                {
                    _lastIncidentTimer = new System.Threading.Timer(IncidentTimerCallback, null, 30000, Timeout.Infinite);

                    _log.Debug("INCIDENT_EVENT :: Incident time callback, still busy so wait another 30 seconds");
                }
                else
                {
                    // Clear the last incident time
                    _lastIncidentDeviceTime = DateTime.MinValue;
                    _lastIncidentEventTime = DateTime.MinValue;

                    // Create the possible accident event and only continue if we have the initial incident packet (if is null then we can't raise the possible accident)
                    if (_lastIncidentPacket != null)
                    {
                        _log.Info("INCIDENT_EVENT :: Create a possible accident event");

                        GeneralGPPacket possibleAccidentEvent = _lastIncidentPacket.CreateCopy();
                        possibleAccidentEvent.cMsgType = GeneralGPPacket.POSSIBLE_ACCIDENT;
                        mDatabase.InsertNotificationRecord(possibleAccidentEvent, this);

                        //fire live update
                        //cancel current event
                        if (SendPossibleAccidentLiveUpdate != null)
                        {
                            SendPossibleAccidentLiveUpdate(this, possibleAccidentEvent);
                        }
                    }

                    _lastIncidentPacket = null;
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error on IncidentTimerCallback", ex);
            }
        }
        #endregion

    }
	
}

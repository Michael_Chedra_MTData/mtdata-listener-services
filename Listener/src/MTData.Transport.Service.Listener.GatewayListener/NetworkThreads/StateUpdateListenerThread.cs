using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using System.Linq;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Application.Communication;
using log4net;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
    public enum ListenerJobState
    {
        None = 0,
        Dispatched = 1,
        ArrivePickup = 2,
        DepartPickup = 3,
        ArriveDelivery = 4,
        DepartDelivery = 5,
        Complete = 6,
        AtSiteEarly = 7,
        AtSiteLate = 8,
        Arrive = 9,
        Depart = 10,
        LegComplete = 11,
        BinPickup = 12,
        DropTransferStation = 13,
        CreateLeg = 14,
        CreateJobAction = 15,
        CreateJob = 16,
        CreateLegAction = 17,
        StartLeg = 18,
        LegActionComplete = 19,
        JobActionComplete = 20,

        //leave this one to be last
        JobStateMaxValue
    }

    /// <summary>
    /// Summary description for StateUpdateListenerThread.
    /// </summary>
    public class StateUpdateListenerThread
    {
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.Network_Threads.StateUpdateListenerThread.";
        private static ILog _log = LogManager.GetLogger(typeof(StateUpdateListenerThread));
        public event WriteToConsoleEvent eConsoleEvent;
        public delegate void WriteToConsoleEvent(string sMsg);

        private delegate void UDPSendBack(byte[] bData);
        private UDPSendBack dSendBack = null;

        private RemoteUpdateEngine _engine = null;
        private string _serverTime_DateFormat = "";

        #region Variables
        public const char VEHICLE_SETATLUNCHSTATE = 'A';
        public const char VEHICLE_UNSETATLUNCHSTATE = 'a';
        public const char VEHICLE_TIPPER_CHANGE = 'B';
        public const char VEHICLE_PRESSURE_SENSOR_CHANGE = 'C';
        public const char DRIVER_UPDATE_MESSAGE = 'D';
        public const char VEHICLE_JOB_STATUS_CHANGE = 'E';
        public const char VEHICLE_USAGE_UPDATE = 'F';
        public const char GROUP_UPDATE_MESSAGE = 'G';
        public const char FLEET_CHECK_CONFIG = 'H';
        public const char USER_INFORMATION_TAG = 'I';
        public const char JOB_UPDATE_MESSAGE = 'J';
        public const char VEHICLE_PHONECALL_REPORT = 'K';
        public const char VEHICLE_LOGIN = 'L';
        public const char VEHICLE_FORCE_LOGOUT = 'l';
        public const char VEHICLE_MESSAGE_RECIEVED = 'M';
        public const char VEHICLE_CONCRETE_TICKETED = 'N';
        public const char VEHICLE_MESSAGE_QUEUED = 'Q';
        public const char VEHICLE_MESSAGE_NOT_QUEUED = 'q';
        public const char VEHICLE_RESET_REQUEST = 'R';
        public const char DRIVER_BREAK = 'S';
        public const char IAP_MESSAGE = 'P';
        public const char DOWN_UNDER_MESSAGE = 'T';
        public const char DRIVER_POINTS_GRAPH_CURRENT_UPDATE = 'U';
        public const char VEHICLE_DISABLE_UPDATES = 'u';
        public const char VEHICLE_DELETED = 'v';
        public const char VEHICLE_CREATED = 'V';
        public const char DSS_EVENT_RAISED = 'X';

        public const char TRUE_LETTER = 'T';
        public const char FALSE_LETTER = 'F';

        private const int JOB_SECTIONS = 7;
        private const int GROUP_SECTIONS = 5;
        private const int DRIVER_SECTIONS = 4;
        private const int DOWN_UNDER_SECTIONS = 4;

        private System.Net.Sockets.Socket mIP;
        private bool bRecvThreadActive;
        private Thread tRecvThread = null;
        private System.Net.IPEndPoint oEP = null;
        private DatabaseInterface mDB;
        private VehicleCollection mCollection;
        private ClientRegistry mRegistry;


        #endregion
        #region constructor/destructor
        public StateUpdateListenerThread(string address,
                                        int port,
                                        DatabaseInterface db,
                                        VehicleCollection c,
                                        ClientRegistry reg,
                                        RemoteUpdateEngine engine,
                                        string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            mIP = new Socket(System.Net.Sockets.AddressFamily.InterNetwork, System.Net.Sockets.SocketType.Dgram, System.Net.Sockets.ProtocolType.Udp);

            _engine = engine;

            // Hook up the interfaces.
            mDB = db;
            mCollection = c;
            mRegistry = reg;

            // Create an internal delegate to keep the recieve thread active.
            dSendBack = new UDPSendBack(this.SendBackMsg);

            // Setup the thread
            tRecvThread = new Thread(new ThreadStart(RecvBytes));
            tRecvThread.Name = "UDPListener.RecvThread";
            bRecvThreadActive = false;

            // Setup the port to listen.
            // If address is null, listen on all interfaces.
            if (address == null)
            {
                oEP = new IPEndPoint(IPAddress.Parse("0.0.0.0"), port);
            }
            else
            {
                oEP = new IPEndPoint(IPAddress.Parse(address), port);
            }

            Start();
        }

        public void Start()
        {
            if (!bRecvThreadActive)
            {
                if (oEP != null && mIP != null)
                {
                    bRecvThreadActive = true;
                    try
                    {
                        mIP.Bind(oEP);
                    }
                    catch (System.Exception ex)
                    {
                        WriteToErrorConsole(ex);
                        bRecvThreadActive = false;
                    }

                    if (bRecvThreadActive)
                    {
                        // Start the recieve thread.  When the calling object hooks up the interface.
                        tRecvThread.Start();
                    }
                }
            }
        }


        public void Stop()
        {
            if (bRecvThreadActive)
            {
                bRecvThreadActive = false;
            }
        }

        public void RecvBytes()
        {
            byte[] bData = null;

            while (bRecvThreadActive)
            {
                if (mIP != null)
                {
                    if (mIP.Available > 0)
                    {
                        bData = new byte[mIP.Available];
                        mIP.Receive(bData);
                        if (dSendBack != null && bData.Length > 0)
                            dSendBack(bData);

                        bData = null;
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
            }
        }

        public void Dispose(bool disposing)
        {
            bRecvThreadActive = false;
            if (mIP != null)
            {
                mIP.Close();
                mIP = null;
            }
        }
        #endregion

        private void SendBackMsg(byte[] contents)
        {
            try
            {
                if (contents.Length > 0)
                {
                    if (contents[0] == (byte)0x01)
                    {
                        ProcessByteArrayPacket(contents);
                        return;
                    }
                }
                string messageText = System.Text.Encoding.ASCII.GetString(contents).TrimEnd('\0').Trim();
                WriteToConsole("Recieved Remote Status Update -> " + messageText);
                char[] delimiter = { ',' };

                string[] sections = messageText.Split(delimiter);

                switch (messageText[0])
                {
                    case USER_INFORMATION_TAG:
                        HandleUserInformationTagUpdate(sections);
                        break;
                    case JOB_UPDATE_MESSAGE:
                        HandleJobUpdate(sections);
                        break;
                    case GROUP_UPDATE_MESSAGE:
                        HandleGroupUpdate(sections);
                        break;
                    case DRIVER_UPDATE_MESSAGE:
                        HandleDriverUpdate(sections);
                        break;
                    case DOWN_UNDER_MESSAGE:
                        HandleDownUnderUpdate(sections);
                        break;
                    case VEHICLE_DISABLE_UPDATES:
                        HandleVehicleDisableUpdates(sections);
                        break;
                    case VEHICLE_DELETED:
                        HandleVehicleDeleted(sections);
                        break;
                    case VEHICLE_CREATED:
                        HandleVehicleCreated(sections);
                        break;
                    case VEHICLE_LOGIN:
                        HandleVehicleLogin(sections);
                        break;
                    case VEHICLE_FORCE_LOGOUT:
                        HandleVehicleForcedLogout(sections);
                        break;
                    case VEHICLE_MESSAGE_RECIEVED:
                        HandleVehicleMessageRecieved(sections);
                        break;
                    case VEHICLE_MESSAGE_QUEUED:
                        HandleVehicleMessageQueued(sections);
                        break;
                    case VEHICLE_MESSAGE_NOT_QUEUED:
                        HandleVehicleMessageNotQueued(sections);
                        break;
                    case VEHICLE_SETATLUNCHSTATE:
                        HandleSetAtLunchStateRecieved(sections);
                        break;
                    case VEHICLE_UNSETATLUNCHSTATE:
                        HandleUnsetAtLunchStateRecieved(sections);
                        break;
                    case VEHICLE_RESET_REQUEST:
                        HandleVehicleResetRequest(sections);
                        break;
                    case VEHICLE_TIPPER_CHANGE:
                        HandleTipperChange(sections);
                        break;
                    case VEHICLE_PRESSURE_SENSOR_CHANGE:
                        HandlePressureChange(sections);
                        break;
                    case VEHICLE_JOB_STATUS_CHANGE:
                        HandleJobStateChange(sections);
                        break;
                    case VEHICLE_USAGE_UPDATE:
                        HandleVehicleUsageUpdate(sections);
                        break;
                    case FLEET_CHECK_CONFIG:
                        HandleVehicleConfigCheckForEntireFleet(sections);
                        break;
                    case VEHICLE_PHONECALL_REPORT:
                        HandleVehiclePhoneCallReport(sections);
                        break;
                    case VEHICLE_CONCRETE_TICKETED:
                        HandleConcreteTicket(sections);
                        break;
                    case DRIVER_POINTS_GRAPH_CURRENT_UPDATE:
                        HandleDriverPointsGraphCurrentUpdate(sections);
                        break;
                    case DRIVER_BREAK:
                        HandleDriverBreak(sections);
                        break;
                    case DSS_EVENT_RAISED:
                        HandleDssEvent(sections);
                        break;
                    case IAP_MESSAGE:
                        HandleIapMessage(sections);
                        break;
                    default:
                        break;
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }


        private void ProcessByteArrayPacket(byte[] contents)
        {
            MemoryStream oMS = null;
            byte bTemp = (byte)0x00;
            short isTemp = 0;
            string sCmd = "";
            try
            {
                WriteToConsole("Recieved Remote Status Byte Array Update -> " + System.Text.ASCIIEncoding.ASCII.GetString(contents).Trim());
                oMS = new MemoryStream(contents, 0, contents.Length);
                PacketUtilities.ReadFromStream(oMS, ref bTemp);
                if (bTemp != (byte)0x01)
                {
                    WriteToConsole("Remote State Update failed : SOP was not found in the correct position.");
                    return;
                }
                PacketUtilities.ReadFromStream(oMS, ref isTemp);
                if (contents.Length != isTemp)
                {
                    WriteToConsole("Remote State Update failed : Packet Length was incorrect.");
                    return;
                }
                PacketUtilities.ReadFromStream(oMS, ref bTemp);
                sCmd = Convert.ToString((char)bTemp);
                switch (sCmd)
                {
                    case "G":
                        HandleGlobalStarUpdates(ref oMS);
                        break;
                    default:
                        WriteToConsole("Remote State Update failed : Unknown command type.");
                        break;
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }
        private static bool ParseBool(string s)
        {
            if (s == null) return false;
            s = s.Trim();
            if (s.Length == 0) return false;
            s = s.ToUpper();

            return s[0].Equals(TRUE_LETTER);
        }


        private void HandleGlobalStarUpdates(ref MemoryStream oMS)
        {
            byte bTemp = (byte)0x00;
            byte bInputStatus = (byte)0x00;
            bool bUseLastKnownPacket = false;
            short iProtcolVer = 0;
            short isTemp = 0;
            short iFleetID = 0;
            int iVehicleID = 0;
            int iReasonID = 0;
            long iStatusValue = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMin = 0;
            int iSec = 0;
            string sSubCmd = "";
            string sESN = "";
            string sSuburb = "";
            string sMelwayRef = "";
            double dLat = 0;
            double dLong = 0;
            Vehicle thisVehicle = null;
            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;

            try
            {
                WriteToConsole("Received GlobalStar Position Update");
                #region Read the data from the packet
                PacketUtilities.ReadFromStream(oMS, ref bTemp);
                sSubCmd = Convert.ToString((char)bTemp);
                if (sSubCmd == "P")
                {
                    PacketUtilities.ReadFromStream(oMS, ref iProtcolVer);							// Protocol Version
                    PacketUtilities.ReadFromStream(oMS, ref iFleetID);								// Fleet ID
                    PacketUtilities.ReadFromStream(oMS, ref iVehicleID);							// Vehicle ID
                    PacketUtilities.ReadFromStream(oMS, ref iReasonID);							// Reason ID
                    PacketUtilities.ReadFromStream(oMS, ref iStatusValue);						// Status Value					
                    PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sESN);				// GlobalStar ESN
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									 // Day
                    iDay = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									 // Month
                    iMonth = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref isTemp);									 // Year
                    iYear = (int)isTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									 // Hour
                    iHour = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									 // Min
                    iMin = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref bTemp);									 // Second
                    iSec = (int)bTemp;
                    PacketUtilities.ReadFromStream(oMS, ref dLat);										// Latitude
                    PacketUtilities.ReadFromStream(oMS, ref dLong);									// Longitude
                    PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sSuburb);			// Suburb
                    PacketUtilities.ReadFromStream(oMS, (byte)0x12, ref sMelwayRef);	// Map Ref
                    PacketUtilities.ReadFromStream(oMS, ref bInputStatus);						// Input Status
                }
                #endregion
                #region Try to find a last packet from this unit
                thisVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(iFleetID))[0], (uint)iVehicleID);
                if (thisVehicle == null)
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(iFleetID) + ", Unit " + Convert.ToString(iVehicleID) + ") is not logged into the listener.");
                    return;
                }
                try
                {
                    thisVehicle.State = (UnitState)iStatusValue;
                    oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(thisVehicle.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                    bUseLastKnownPacket = true;
                }
                catch (System.Exception)
                {
                }
                #endregion
                #region Create the update packet
                if (!bUseLastKnownPacket)
                {
                    #region If there was no last packet for this unit, create a new one
                    oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(_serverTime_DateFormat);
                    byte[] bConvert = BitConverter.GetBytes(iFleetID);
                    oGP.cFleetId = bConvert[0];
                    oGP.iVehicleId = Convert.ToUInt32(iVehicleID);
                    #endregion
                }
                #region Set the time and location values on the unit
                oGP.cMsgType = (byte)iReasonID;
                oGP.cButtonNumber = bInputStatus;
                oGP.mStatus.cInputStatus = bInputStatus;
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mFix.iDirection = 0;
                if (iMonth == 0 && iDay == 0 && iHour == 0 && iMin == 0 && iSec == 0)
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                }
                else
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMin, iSec, 0), _serverTime_DateFormat);
                }
                oGP.mCurrentClock = oGP.mFixClock;
                #endregion
                #region Set the Customer name as the current suburb name
                thisVehicle.oPos.Distance = 0;
                thisVehicle.oPos.SetPointGroupID = 0;
                thisVehicle.oPos.SetPointGroupNumber = 0;
                thisVehicle.oPos.SetPointID = 0;
                thisVehicle.oPos.SetPointNumber = 0;
                thisVehicle.oPos.MapRef = sMelwayRef;
                thisVehicle.oPos.PlaceName = sSuburb;
                thisVehicle.oPos.Position = "N";
                #endregion
                #region Update the database and send the live update
                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle);
                if(sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);
                #endregion
                #endregion
                _log.Info("Updated job status for fleet " + Convert.ToString(iFleetID) + ", Unit " + Convert.ToString(iVehicleID) + ".");
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// This handler will take care of inbound merssage requesting that the listener ignore 
        /// updates from a specific vehicle for a period of time, and not log them to the
        /// database. 
        /// The listener is also not to ACK the unit concerned.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleDisableUpdates(string[] sections)
        {
            _engine.AddVehicleIgnoreUpdateRequest(Int32.Parse(sections[1]), UInt32.Parse(sections[2]), Int32.Parse(sections[3]));
        }

        /// <summary>
        /// If the remote item request an reset of a vehicle, this is how it is acocmplished.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleResetRequest(string[] sections)
        {
            _engine.AddVehicleResetRequest(Int32.Parse(sections[1]), UInt32.Parse(sections[2]), Int64.Parse(sections[3]));
        }

        /// <summary>
        /// This indicates that a vehicle has been deleted, and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleDeleted(string[] sections)
        {
            int fleetID = Int32.Parse(sections[1]);
            uint vehicleID = UInt32.Parse(sections[2]);

            _engine.RegisterVehicleDeletion(fleetID, vehicleID);
        }

        /// <summary>
        /// This indicates that a terminal has recieved a message sent to it.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleMessageRecieved(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;

            try
            {
                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                _engine.RegisterVehicleMessageRecieved(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// A client message requesting the 'At Lunch' state be set for a vehicle.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleSetAtLunchStateRecieved(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;

            try
            {
                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                _engine.RegisterVehicleSetAtLunchStateRecievd(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// A client message requesting the 'At Lunch' state be cleared for a vehicle.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleUnsetAtLunchStateRecieved(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;

            try
            {
                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                _engine.RegisterVehicleUnsetAtLunchStateRecievd(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// This indicates that a terminal has a message queued for it
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleMessageQueued(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;

            try
            {
                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                _engine.RegisterVehicleMessageQueued(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// This indicates that the interface server was unable to queue a message for the terminal.  The terminal may not be logged in.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleMessageNotQueued(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;

            try
            {
                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                _engine.RegisterVehicleMessageNotQueued(fleetID, vehicleID);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// This indicates that the driver have specified a change of config and wants units to check the versions they are running
        /// </summary>
        /// <param name="sections"></param>

        private void HandleVehicleConfigCheckForEntireFleet(string[] sections)
        {
            System.Collections.ArrayList oVehiclesInFleet = null;
            int iFleetID = 0;
            int iVehicleID = 0;
            byte bFleetID = (byte)0x00;
            Vehicle thisVehicle = null;
            #region Read the data from the remote state update string
            try
            {
                if (sections.Length != 3)
                {
                    WriteToConsole("Check Vehicle Config requeststring is invalid.");
                    return;
                }

                iFleetID = Int32.Parse(sections[1]);
                iVehicleID = Int32.Parse(sections[2]);
                bFleetID = ((byte[])BitConverter.GetBytes(iFleetID))[0];
                if (iVehicleID == 0)
                {
                    oVehiclesInFleet = mCollection.GetUnitsInThisFleet(bFleetID);
                    for (int X = 0; X < oVehiclesInFleet.Count; X++)
                    {
                        thisVehicle = mCollection.GetVehicle(bFleetID, (uint)oVehiclesInFleet[X]);
                        if (thisVehicle != null)
                        {
                            thisVehicle.bNeedsConfigCheck = true;
                            _log.InfoFormat("Vehicle {0}/{1} config check message, configCheck set to true", iFleetID, oVehiclesInFleet[X]);
                        }
                    }
                }
                else
                {
                    thisVehicle = mCollection.GetVehicle(bFleetID, (uint)iVehicleID);
                    if (thisVehicle != null)
                    {
                        thisVehicle.bNeedsConfigCheck = true;
                        _log.InfoFormat("Vehicle {0}/{1} config check message, configCheck set to true", iFleetID, iVehicleID);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
            }
            #endregion
        }

        /// <summary>
        /// This indicates that the unit has made or recieved a phone call
        /// </summary>
        /// <param name="sections"></param>

        private void HandleVehiclePhoneCallReport(string[] sections)
        {
            #region Variables
            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            Vehicle thisVehicle = null;
            bool bUseLastKnownPacket = false;
            byte bMsgType = (byte)0x00;
            int fleetID = 0;
            uint vehicleID = 0;
            int iSetState = 0;
            double dLat = 0;
            double dLong = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            int iSpeed = 0;
            int iHeading = 0;
            byte cButton = (byte)0x00;
            #endregion

            #region Read the data from the remote state update string
            try
            {
                if (sections.Length != 14)
                {
                    WriteToConsole("Phonecall update string is invalid.");
                    return;
                }

                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                iSetState = Convert.ToInt32(sections[3]);
                dLat = Convert.ToDouble(sections[4]);
                dLong = Convert.ToDouble(sections[5]);
                iSpeed = Convert.ToInt32(sections[6]);
                iHeading = Convert.ToInt32(sections[7]);
                iDay = Convert.ToInt32(sections[8]);
                iMonth = Convert.ToInt32(sections[9]);
                iYear = Convert.ToInt32(sections[10]);
                iHour = Convert.ToInt32(sections[11]);
                iMinute = Convert.ToInt32(sections[12]);
                iSecond = Convert.ToInt32(sections[13]);

                thisVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
                if (thisVehicle == null)
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ") is not logged into the listener.");
                    return;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
                return;
            }
            #endregion
            #region Try and get a copy of the last packet sent by this unit
            try
            {
                oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(thisVehicle.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                bUseLastKnownPacket = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
                return;
            }
            #endregion
            #region Set the Packet type and Vehicle State
            try
            {
                switch (iSetState)
                {
                    case 0:
                        bMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_PHONECALL_PACKET;
                        cButton = (byte)0x00;
                        break;
                    case 1:
                        bMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_PHONECALL_PACKET;
                        cButton = (byte)0x01;
                        break;
                    default:
                        _log.Info("Vehicle " + Convert.ToString(fleetID) + "/" + Convert.ToString(vehicleID) + " : Unknown Phonecall State '" + iSetState + "' sent via remote state update.");
                        return;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
                return;
            }
            #endregion
            #region Create the update packet
            try
            {
                if (!bUseLastKnownPacket)
                {
                    oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(_serverTime_DateFormat);
                    byte[] bConvert = BitConverter.GetBytes(fleetID);
                    oGP.cFleetId = bConvert[0];
                    oGP.iVehicleId = Convert.ToUInt32(vehicleID);
                }
                oGP.cMsgType = bMsgType;
                oGP.cButtonNumber = cButton;
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = ((byte[])BitConverter.GetBytes(iSpeed))[0];
                oGP.mFix.iDirection = ((byte[])BitConverter.GetBytes(iHeading))[0];
                if (iMonth == 0 && iDay == 0 && iHour == 0 && iMinute == 0 && iSecond == 0)
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                }
                else
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                }
                oGP.mCurrentClock = oGP.mFixClock;
                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle);
                if(sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
            }
            #endregion

        }

        public void HandleConcreteTicket(string[] sections)
        {
            // Concrete Ticket = N,[FleetID],[VehicleID],[OrderDate],[OrderNumber],[TicketCode],[ItemCode],[Customer Name]
            #region Variables
            bool bUseLastKnownPacket = false;
            int fleetID = 0;
            uint vehicleID = 0;
            DateTime dtOrderDate = System.DateTime.MinValue;
            string sOrderNumber = "";
            string sTicketCode = "";
            string sItemCode = "";
            string sOrderDate = "";
            string sCustomerName = "";
            string[] sOrderDateSplit = null;
            Vehicle thisVehicle = null;
            byte bMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_JOB_PACKET;
            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            DatabaseAccess.PositionDetails oTempPos = null;
            #endregion

            #region Read the data from the remote state update string
            try
            {
                if (sections.Length != 8)
                {
                    WriteToConsole("Concrete Ticket string is invalid.");
                    return;
                }

                fleetID = Int32.Parse(sections[1]);
                vehicleID = UInt32.Parse(sections[2]);
                sOrderDate = sections[3];
                sOrderDateSplit = sOrderDate.Split("|".ToCharArray());
                dtOrderDate = new DateTime(Convert.ToInt32(sOrderDateSplit[0]), Convert.ToInt32(sOrderDateSplit[1]), Convert.ToInt32(sOrderDateSplit[2]), Convert.ToInt32(sOrderDateSplit[3]), Convert.ToInt32(sOrderDateSplit[4]), Convert.ToInt32(sOrderDateSplit[5]), 0);
                sOrderNumber = sections[4];
                sTicketCode = sections[5];
                sItemCode = sections[6];
                sCustomerName = sections[7];
                sItemCode = sItemCode.Replace("|", ",");
                sItemCode = sItemCode.Trim();
                sCustomerName = sCustomerName.Replace("|", ",");
                sCustomerName = sCustomerName.Trim();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
                return;
            }
            #endregion
            #region Find the vehicle record and set the IsTicketed flag to true.
            try
            {
                thisVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
                if (thisVehicle == null)
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ") is not logged into the listener.");
                    return;
                }
                else
                {
                    thisVehicle.IsTicketed = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
                return;
            }
            #endregion
            #region Try and get a copy of the last packet sent by this unit
            try
            {
                oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(thisVehicle.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                bUseLastKnownPacket = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
                return;
            }
            #endregion
            try
            {
                #region Create the update packet
                if (!bUseLastKnownPacket)
                {
                    return;
                }
                #region Set the time and location values on the unit
                oGP.cMsgType = bMsgType;
                oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0).ToUniversalTime(), _serverTime_DateFormat);
                oGP.mCurrentClock = oGP.mFixClock;
                oGP.cButtonNumber = (byte)0x01;
                #endregion
                #region Set the Customer name as the current suburb name
                oTempPos = new DatabaseAccess.PositionDetails();
                oTempPos.PlaceName = thisVehicle.oPos.PlaceName;
                oTempPos.Distance = thisVehicle.oPos.Distance;
                oTempPos.SetPointGroupID = thisVehicle.oPos.SetPointGroupID;
                oTempPos.SetPointGroupNumber = thisVehicle.oPos.SetPointGroupNumber;
                oTempPos.SetPointID = thisVehicle.oPos.SetPointID;
                oTempPos.SetPointNumber = thisVehicle.oPos.SetPointNumber;
                oTempPos.MapRef = thisVehicle.oPos.MapRef;
                oTempPos.Position = thisVehicle.oPos.Position;
                if (sCustomerName == "")
                    thisVehicle.oPos.PlaceName = sCustomerName + " (Order :" + sOrderNumber + ")";
                else
                    thisVehicle.oPos.PlaceName = "Order :" + sOrderNumber;
                thisVehicle.oPos.MapRef = mDB.GetMelwaysRef(oGP.mFix.dLatitude, oGP.mFix.dLongitude);
                thisVehicle.oPos.Position = "N";
                #endregion
                #region Update the database and send the live update
                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle, thisVehicle.oPos.PlaceName, sOrderNumber);
                if (sRet != null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);
                #endregion
                #region Restore the previous position details.
                thisVehicle.oPos.PlaceName = oTempPos.PlaceName;
                thisVehicle.oPos.Distance = oTempPos.Distance;
                thisVehicle.oPos.SetPointGroupID = oTempPos.SetPointGroupID;
                thisVehicle.oPos.SetPointGroupNumber = oTempPos.SetPointGroupNumber;
                thisVehicle.oPos.SetPointID = oTempPos.SetPointID;
                thisVehicle.oPos.SetPointNumber = oTempPos.SetPointNumber;
                thisVehicle.oPos.MapRef = oTempPos.MapRef;
                thisVehicle.oPos.Position = oTempPos.Position;
                #endregion
                #endregion
                _log.Info("Updated job status for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "HandleConcreteTicket(string[] sections)", ex);
            }
        }
        /// <summary>
        /// This indicates that the driver have specified a vehicle usage change
        /// </summary>
        /// <param name="sections"></param>

        private void HandleVehicleUsageUpdate(string[] sections)
        {

        }

        /// <summary>
        /// This indicates that the driver has tipped a load and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleTipperChange(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;
            int TipperState = 0;
            double dLat = 0;
            double dLong = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;

            if (sections.Length != 12)
            {
                WriteToConsole("Driver Tipper State Update string is invalid.");
                return;
            }

            fleetID = Int32.Parse(sections[1]);
            vehicleID = UInt32.Parse(sections[2]);
            TipperState = Int32.Parse(sections[3]);
            dLat = Convert.ToDouble(sections[4]);
            dLong = Convert.ToDouble(sections[5]);
            iDay = Convert.ToInt32(sections[6]);
            iMonth = Convert.ToInt32(sections[7]);
            iYear = Convert.ToInt32(sections[8]);
            iHour = Convert.ToInt32(sections[9]);
            iMinute = Convert.ToInt32(sections[10]);
            iSecond = Convert.ToInt32(sections[11]);

            Vehicle thisVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
            if (thisVehicle == null)
            {
                WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ") is not logged into the listener.");
                return;
            }

            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            bool bUseLastKnownPacket = false;
            try
            {
                oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(thisVehicle.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                bUseLastKnownPacket = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
            }

            if (bUseLastKnownPacket)
            {

                if (TipperState > 0)
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_TIPPER_UP;
                }
                else
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_TIPPER_DOWN;
                }
                mDB.UpdateVehilcleStatus(thisVehicle, true);
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mFix.iDirection = 0;
                if (iMonth > 0 && iDay > 0 && iHour >= 0 && iMinute >= 0 && iSecond >= 0)
                {
                    try
                    {
                        oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                    }
                    catch (System.Exception)
                    {
                        oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                    }
                }
                else
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                }
                oGP.mCurrentClock = oGP.mFixClock;
                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle);
                if (sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);
                 _log.Info("Updated tipper status for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
            }
            else
            {
                oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(_serverTime_DateFormat);
                byte[] bConvert = BitConverter.GetBytes(fleetID);
                oGP.cFleetId = bConvert[0];
                oGP.iVehicleId = Convert.ToUInt32(vehicleID);

                if (TipperState > 0)
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_TIPPER_UP;
                }
                else
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_TIPPER_DOWN;
                }
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mFix.iDirection = 0;
                if (iMonth > 0 && iDay > 0 && iHour >= 0 && iMinute >= 0 && iSecond >= 0)
                {
                    try
                    {
                        oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                    }
                    catch (System.Exception)
                    {
                        oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                    }
                }
                else
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                }
                oGP.mCurrentClock = oGP.mFixClock;
                mDB.UpdateVehilcleStatus(thisVehicle, true);
                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle);
                if (sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);
                _log.Info("Updated tipper status for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
            }
        }

        /// <summary>
        /// This indicates that the pressure sensor on the quarry truck has changesd state and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandlePressureChange(string[] sections)
        {
            int fleetID = 0;
            uint vehicleID = 0;
            int PressureSensorState = 0;
            double dLat = 0;
            double dLong = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;

            if (sections.Length != 12)
            {
                WriteToConsole("Driver Login Update string is invalid.");
                return;
            }

            fleetID = Int32.Parse(sections[1]);
            vehicleID = UInt32.Parse(sections[2]);
            PressureSensorState = Int32.Parse(sections[3]);
            dLat = Convert.ToDouble(sections[4]);
            dLong = Convert.ToDouble(sections[5]);
            iDay = Convert.ToInt32(sections[6]);
            iMonth = Convert.ToInt32(sections[7]);
            iYear = Convert.ToInt32(sections[8]);
            iHour = Convert.ToInt32(sections[9]);
            iMinute = Convert.ToInt32(sections[10]);
            iSecond = Convert.ToInt32(sections[11]);

            Vehicle thisVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
            if (thisVehicle == null)
            {
                WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ") is not logged into the listener.");
                return;
            }

            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            bool bUseLastKnownPacket = false;
            try
            {
                oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(thisVehicle.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                bUseLastKnownPacket = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "XXX()", ex);
            }

            if (bUseLastKnownPacket)
            {

                if (PressureSensorState > 0)
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_PRESSURE_SENSOR_LOADED;
                }
                else
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_PRESSURE_SENSOR_UNLOADED;
                }
                mDB.UpdateVehilcleStatus(thisVehicle, true);
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mFix.iDirection = 0;
                oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                oGP.mCurrentClock = oGP.mFixClock;

                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle);
                if (sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);

                _log.Info("Updated pressure switch status for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
            }
            else
            {
                oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(_serverTime_DateFormat);
                byte[] bConvert = BitConverter.GetBytes(fleetID);
                oGP.cFleetId = bConvert[0];
                oGP.iVehicleId = Convert.ToUInt32(vehicleID);

                if (PressureSensorState > 0)
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_PRESSURE_SENSOR_LOADED;
                }
                else
                {
                    oGP.cMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_PRESSURE_SENSOR_UNLOADED;
                }
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mFix.iDirection = 0;
                if (iMonth == 0 && iDay == 0 && iHour == 0 && iMinute == 0 && iSecond == 0)
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                }
                else
                {
                    oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                }
                oGP.mCurrentClock = oGP.mFixClock;
                mDB.UpdateVehilcleStatus(thisVehicle, true);
                string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle);
                if (sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                else
                    _log.Info(sRet);
                _log.Info("Updated pressure switch for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
            }

        }


        /// <summary>
        /// This indicates that the vehicle has changed job state and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleJobStateChange(string[] sections)
        {
            byte bMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_JOB_PACKET;
            byte cButton = (byte)0x00;
            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            Vehicle thisVehicle = null;
            bool bUseLastKnownPacket = false;
            int fleetID = 0;
            uint vehicleID = 0;
            ListenerJobState JobState = 0;
            string sOrderNumber = "";
            string sCustomerName = "";
            double dLat = 0;
            double dLong = 0;
            int iSpeed = 0;
            int iHeading = 0;
            int iDay = 0;
            int iMonth = 0;
            int iYear = 0;
            int iHour = 0;
            int iMinute = 0;
            int iSecond = 0;
            int iSetUnitState = 0;
            long iClearStates = -1;
            long iSetStates = -1;
            bool writeRecord = true;
            DatabaseAccess.PositionDetails oTempPos = null;

            try
            {
                #region Read the values from the status update string
                if (sections.Length < 16 || sections.Length > 20)
                {
                    string sError = "(Length = " + Convert.ToString(sections.Length) + ")";
                    for (int X = 0; X < sections.Length; X++)
                    {
                        if (X > 0)
                            sError += "," + sections[X];
                        else
                            sError += " " + sections[X];
                    }
                    WriteToConsole("Driver Job Status Update string is invalid : " + sError);
                    return;
                }

                fleetID = Int32.Parse(sections[1]);
                int v = Int32.Parse(sections[2]);
                if (v < 0)
                {
                    vehicleID = 0;
                }
                else
                {
                    vehicleID = (uint)v;
                }
                JobState = (ListenerJobState)Int32.Parse(sections[3]);
                sOrderNumber = sections[4];
                sCustomerName = sections[5];
                dLat = Convert.ToDouble(sections[6]);
                dLong = Convert.ToDouble(sections[7]);
                iSpeed = Convert.ToInt32(sections[8]);
                iHeading = Convert.ToInt32(sections[9]);
                iDay = Convert.ToInt32(sections[10]);
                iMonth = Convert.ToInt32(sections[11]);
                iYear = Convert.ToInt32(sections[12]);
                iHour = Convert.ToInt32(sections[13]);
                iMinute = Convert.ToInt32(sections[14]);
                iSecond = Convert.ToInt32(sections[15]);
                if (sections.Length > 16)
                    iSetUnitState = Convert.ToInt32(sections[16]);
                else
                    iSetUnitState = 1;

                if (sections.Length > 17)
                    iClearStates = Convert.ToInt64(sections[17]);

                if (sections.Length > 18)
                    iSetStates = Convert.ToInt64(sections[18]);
                if (sections.Length > 19 && sections[19] == "0")
                    writeRecord = false;

                _log.InfoFormat("Received Handle Job state update for {0}-{1}, iSetUnitState = {4}, clearStates = {2}, setStates = {3}, writeRecord = {5}", fleetID, vehicleID, iClearStates, iSetStates, iSetUnitState, writeRecord);
                // Put the comma's back in the customer name and order number.
                sCustomerName = sCustomerName.Replace(Convert.ToString((char)0x01), ",");
                sOrderNumber = sOrderNumber.Replace(Convert.ToString((char)0x01), ",");
                #endregion
                #region Validate / Retrieve the Job state
                if (JobState < ListenerJobState.None || JobState >= ListenerJobState.JobStateMaxValue)
                {
                    _log.Info("Invalid Job State '" + Convert.ToString(JobState) + "' recieved from vehicle " + Convert.ToString(fleetID) + "/" + Convert.ToString(vehicleID));
                    return;
                }
                else
                {
                    if (JobState > ListenerJobState.None)
                        cButton = ((byte[])BitConverter.GetBytes((int)JobState))[0];
                    else
                    {
                        bMsgType = MTData.Transport.Gateway.Packet.GeneralGPPacket.STATUS_REPORT;
                        cButton = (byte)0x00;
                    }
                }
                #endregion
                #region Get the vehicle object
                thisVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
                if (thisVehicle == null)
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ") is not logged into the listener.");
                    return;
                }
                else
                {
                    if (iSetUnitState == 1)
                    {
                        // Allow states to be specified by the remote interface
                        if (iClearStates > 0 || iSetStates > 0)
                        {
                            if (iClearStates > 0)
                                thisVehicle.ClearStateFlags(iClearStates);
                            if (iSetStates > 0)
                                thisVehicle.SetStateFlags(iSetStates);
                        }
                        else
                        {
                            // Set only the MDT States
                            #region Set the state bits
                            thisVehicle.ClearStateBit(UnitState.MDTState1);
                            thisVehicle.ClearStateBit(UnitState.MDTState2);
                            thisVehicle.ClearStateBit(UnitState.MDTState3);
                            thisVehicle.ClearStateBit(UnitState.MDTState4);
                            thisVehicle.ClearStateBit(UnitState.AuxState1);
                            thisVehicle.ClearStateBit(UnitState.AuxState2);
                            thisVehicle.ClearStateBit(UnitState.AuxState3);
                            thisVehicle.ClearStateBit(UnitState.AuxState4);
                            switch (JobState)
                            {
                                case ListenerJobState.Dispatched:
                                    thisVehicle.SetStateBit(UnitState.MDTState1);
                                    break;
                                case ListenerJobState.ArrivePickup:
                                    thisVehicle.SetStateBit(UnitState.MDTState2);
                                    break;
                                case ListenerJobState.DepartPickup:
                                    thisVehicle.SetStateBit(UnitState.MDTState3);
                                    break;
                                case ListenerJobState.ArriveDelivery:
                                    thisVehicle.SetStateBit(UnitState.MDTState4);
                                    break;
                                case ListenerJobState.DepartDelivery:
                                    thisVehicle.SetStateBit(UnitState.AuxState2);
                                    break;
                                case ListenerJobState.Complete:
                                    thisVehicle.SetStateBit(UnitState.AuxState1);
                                    break;
                                case ListenerJobState.AtSiteEarly:
                                    thisVehicle.SetStateBit(UnitState.AuxState3);
                                    break;
                                case ListenerJobState.AtSiteLate:
                                    thisVehicle.SetStateBit(UnitState.AuxState4);
                                    break;
                            }
                            #endregion
                        }
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "HandleJobStateChange(string[] sections)", ex);
            }

            if (writeRecord)
            {
                try
                {
                    #region Create the update packet

                    #region If there was no last packet for this unit, create a new one
                    oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(_serverTime_DateFormat);
                    byte[] bConvert = BitConverter.GetBytes(fleetID);
                    oGP.cFleetId = bConvert[0];
                    oGP.iVehicleId = Convert.ToUInt32(vehicleID);
                    #endregion

                    #region Set the time and location values on the unit
                    oGP.cMsgType = bMsgType;
                    oGP.cButtonNumber = cButton;
                    oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                    oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                    //oGP.mFix.cSpeed = ((byte[]) BitConverter.GetBytes(iSpeed))[0];
                    oGP.mFix.cSpeed = 0;
                    oGP.mDistance.cMaxSpeed = 0;
                    oGP.mFix.iDirection = ((byte[])BitConverter.GetBytes(iHeading))[0];
                    if (iMonth == 0 && iDay == 0 && iHour == 0 && iMinute == 0 && iSecond == 0)
                    {
                        oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0), _serverTime_DateFormat);
                    }
                    else
                    {
                        oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                    }
                    oGP.mCurrentClock = oGP.mFixClock;
                    #endregion
                    #region Set the Customer name as the current suburb name
                    oTempPos = new DatabaseAccess.PositionDetails();
                    oTempPos.PlaceName = thisVehicle.oPos.PlaceName;
                    oTempPos.Distance = thisVehicle.oPos.Distance;
                    oTempPos.SetPointGroupID = thisVehicle.oPos.SetPointGroupID;
                    oTempPos.SetPointGroupNumber = thisVehicle.oPos.SetPointGroupNumber;
                    oTempPos.SetPointID = thisVehicle.oPos.SetPointID;
                    oTempPos.SetPointNumber = thisVehicle.oPos.SetPointNumber;
                    oTempPos.MapRef = thisVehicle.oPos.MapRef;
                    oTempPos.Position = thisVehicle.oPos.Position;
                    thisVehicle.oPos.Distance = 0;
                    thisVehicle.oPos.SetPointGroupID = 0;
                    thisVehicle.oPos.SetPointGroupNumber = 0;
                    thisVehicle.oPos.SetPointID = 0;
                    thisVehicle.oPos.SetPointNumber = 0;
                    thisVehicle.oPos.MapRef = mDB.GetMelwaysRef(oGP.mFix.dLatitude, oGP.mFix.dLongitude);
                    thisVehicle.oPos.Position = "N";
                    PositionDetails oPos = thisVehicle.oPos.Copy;
                    #endregion
                    #region Update the database and send the live update
                    mDB.UpdateVehilcleStatus(thisVehicle, true);
                    string sRet = mDB.InsertNotificationRecord(oGP, thisVehicle, sCustomerName, sOrderNumber);
                    if (sRet != null)
                        mRegistry.NotifyClientOfRemoteStatusChange(oGP, thisVehicle);
                    else
                    {
                        _log.Info(sRet);
                    }
                    #endregion
                    #region Restore the previous position details.
                    thisVehicle.oPos.PlaceName = oTempPos.PlaceName;
                    thisVehicle.oPos.Distance = oTempPos.Distance;
                    thisVehicle.oPos.SetPointGroupID = oTempPos.SetPointGroupID;
                    thisVehicle.oPos.SetPointGroupNumber = oTempPos.SetPointGroupNumber;
                    thisVehicle.oPos.SetPointID = oTempPos.SetPointID;
                    thisVehicle.oPos.SetPointNumber = oTempPos.SetPointNumber;
                    thisVehicle.oPos.MapRef = oTempPos.MapRef;
                    thisVehicle.oPos.Position = oTempPos.Position;
                    #endregion
                    #endregion
                    _log.Info("Updated job status for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "XXX()", ex);
                }
            }
        }

        private void HandleVehicleForcedLogout(string[] sections)
        {
            //sections = Fleet, Vehicle, Driver Login ID
            try
            {
                byte fleetId = (byte)Int32.Parse(sections[1]);
                uint vehicleId = UInt32.Parse(sections[2]);
                int driverLoginId = Int32.Parse(sections[3]);
                int driverPin = Int32.Parse(sections[4]);

                Vehicle oVehicle = null;

                FirmwareDriverLoginReply oReply = new FirmwareDriverLoginReply(_serverTime_DateFormat);
                oReply.LoginResult = FirmwareDriverLoginReply.FirmwareLoginResult.LoggedIntoAnotherVehicle;
                oReply.DriverID = driverLoginId;
                oReply.DriverPin = driverPin;
                oReply.CardID = new byte[0];
                oReply.DriverName = "";
                oReply.EncodeDataField();

                oVehicle = mCollection.GetVehicle(fleetId, vehicleId);
                if (oVehicle != null)
                    oVehicle.oFirmwareLoginReply = oReply;
            }
            catch(System.Exception ex)
            {
                _log.Error(sClassName + "HandleVehicleForcedLogout(string[] sections)", ex);
            }
        }

        /// <summary>
        /// This indicates that a driver has changed vehicle, and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleLogin(string[] sections)
        {
            if (sections.Length != 13)
            {
                WriteToConsole("Driver Login Update string is invalid.");
                return;
            }

            int fleetID = Int32.Parse(sections[1]);
            uint vehicleID = UInt32.Parse(sections[2]);
            int driverID = Int32.Parse(sections[3]);
            string driverName = sections[4];
            double dLat = Convert.ToDouble(sections[5]);
            double dLong = Convert.ToDouble(sections[6]);
            int iDay = Convert.ToInt32(sections[7]);
            int iMonth = Convert.ToInt32(sections[8]);
            int iYear = Convert.ToInt32(sections[9]);
            int iHour = Convert.ToInt32(sections[10]);
            int iMinute = Convert.ToInt32(sections[11]);
            int iSecond = Convert.ToInt32(sections[12]);

            // All , chars have been replaced with ||
            // Decode the name
            driverName = driverName.Replace("||", ",");

            DateTime dtDeviceTime = DateTime.MinValue;
            bool bValidDate = false;
            try
            {
                dtDeviceTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                bValidDate = true;
            }
            catch (System.Exception)
            {
                bValidDate = false;
            }

            if (bValidDate)
            {
                _engine.RegisterVehicleLogin(fleetID, vehicleID, driverID, driverName);
                Vehicle oVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
                if (oVehicle == null)
                {
                    GeneralGPPacket mPacket = new GeneralGPPacket(_serverTime_DateFormat);
                    oVehicle = new Vehicle((byte)fleetID, (uint)vehicleID, mPacket, mDB, _serverTime_DateFormat);
                    mCollection.Add(oVehicle);
                }

                GeneralGPPacket oGP = mDB.LoadVehicleDataFromHistory(fleetID, (int)vehicleID, dtDeviceTime, oVehicle);
                //if no packet from history or is greater than 2 minutes old then use a new packet
                if (oGP == null || (dtDeviceTime - oGP.mCurrentClock.ToDateTime()).TotalSeconds > 120)
                {
                    oGP = new GeneralGPPacket(_serverTime_DateFormat);
                    oGP.cFleetId = (byte)fleetID;
                    oGP.iVehicleId = Convert.ToUInt32(vehicleID);
                }
                if (driverID > 0)
                {
                    oGP.cMsgType = GeneralGPPacket.STATUS_LOGIN;
                    oVehicle.SetStateBit(UnitState.LoggedIn);
                }
                else
                {
                    oGP.cMsgType = GeneralGPPacket.STATUS_LOGOUT;
                    oVehicle.ClearStateBit(UnitState.LoggedIn);
                }
                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mDistance.cMaxSpeed = 0;
                oGP.mFix.iDirection = 0;
                oGP.mFixClock = new GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                oGP.mCurrentClock = oGP.mFixClock;
                PositionDetails oPos = mDB.GetPositionForVehicle(oVehicle, oGP, Convert.ToDecimal(dLat), Convert.ToDecimal(dLong));
                mDB.UpdateVehilcleStatus(oVehicle, true);
                string sRet = mDB.InsertNotificationRecord(oGP, oVehicle);
                if (sRet == null)
                {
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, oVehicle);
                }
                else
                {
                    _log.Info(sRet);
                }
                _log.Info("Updated login status for fleet " + Convert.ToString(fleetID) + ", Unit " + Convert.ToString(vehicleID) + ".");
            }
        }

        /// <summary>
        /// This indicates that a driver has started / stopped taking break, and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleDriverBreak(string[] sections)
        {
            if (sections.Length != 12)
            {
                WriteToConsole("Driver Login Update string is invalid.");
                return;
            }

            /// "S,[FleetID],[VehicleID],[Lat],[Long],[Day],[Month],[Year],[Hour],[Minute],[Second],[BreakFunction]"

            int fleetID = Int32.Parse(sections[1]);
            uint vehicleID = UInt32.Parse(sections[2]);
            double dLat = Convert.ToDouble(sections[3]);
            double dLong = Convert.ToDouble(sections[4]);
            int iDay = Convert.ToInt32(sections[5]);
            int iMonth = Convert.ToInt32(sections[6]);
            int iYear = Convert.ToInt32(sections[7]);
            int iHour = Convert.ToInt32(sections[8]);
            int iMinute = Convert.ToInt32(sections[9]);
            int iSecond = Convert.ToInt32(sections[10]);
            int breakFunction = Convert.ToInt32(sections[11]);

            DateTime dtDeviceTime = DateTime.MinValue;
            bool bValidDate = false;
            try
            {
                dtDeviceTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
                bValidDate = true;
            }
            catch (System.Exception)
            {
                bValidDate = false;
            }

            if (bValidDate)
            {
                Vehicle oVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
                if (oVehicle == null)
                {
                    GeneralGPPacket mPacket = new GeneralGPPacket(_serverTime_DateFormat);
                    oVehicle = new Vehicle((byte)fleetID, (uint)vehicleID, mPacket, mDB, _serverTime_DateFormat);
                    mCollection.Add(oVehicle);
                }

                GeneralGPPacket oGP = mDB.LoadVehicleDataFromHistory(fleetID, (int)vehicleID, dtDeviceTime, oVehicle);
                //if no packet from history or is greater than 2 minutes old then use a new packet
                    if (oGP == null || (dtDeviceTime - oGP.mCurrentClock.ToDateTime()).TotalSeconds > 120 )
                {
                    oGP = new GeneralGPPacket(_serverTime_DateFormat);
                    oGP.cFleetId = (byte)fleetID;
                    oGP.iVehicleId = Convert.ToUInt32(vehicleID);
                }

                oGP.mFix.dLatitude = Convert.ToDecimal(dLat);
                oGP.mFix.dLongitude = Convert.ToDecimal(dLong);
                oGP.mFix.cSpeed = (byte)0x00;
                oGP.mDistance.cMaxSpeed = 0;
                oGP.mFix.iDirection = 0;
                oGP.mFixClock = new GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
                oGP.mCurrentClock = oGP.mFixClock;
                PositionDetails oPos = mDB.GetPositionForVehicle(oVehicle, oGP, Convert.ToDecimal(dLat), Convert.ToDecimal(dLong));

                string breakType = "None";
                switch (breakFunction)
                {
                    case 0: //end break
                        oGP.cMsgType = GeneralGPPacket.STATUS_VEHICLE_BREAK_END;
                        breakType = "Break End";
                        break;
                    case 1: // start break
                        oGP.cMsgType = GeneralGPPacket.STATUS_VEHICLE_BREAK_START;
                        breakType = "Break Start";
                        break;
                    case 2: //missed start break
                        oGP.cMsgType = GeneralGPPacket.STATUS_VEHICLE_BREAK_START;
                        oGP.cInputNumber = 1;
                        breakType = "Break Missed";
                        break;
                    case 3: //break end early
                        oGP.cMsgType = GeneralGPPacket.STATUS_VEHICLE_BREAK_END;
                        oGP.cInputNumber = 1;
                        breakType = "Break End Early";
                        break;
                }

                string sRet = mDB.InsertNotificationRecord(oGP, oVehicle);
                if (sRet == null)
                    mRegistry.NotifyClientOfRemoteStatusChange(oGP, oVehicle);
                else
                    _log.Info(sRet);
                _log.InfoFormat("Updated {0} status for fleet {1}, Unit {2}.", breakType, fleetID, vehicleID);
            }

        }


        /// <summary>
        /// This indicates that a vehicle has been created, and we would like to broadcast this to all clients.
        /// </summary>
        /// <param name="sections"></param>
        private void HandleVehicleCreated(string[] sections)
        {
            int fleetID = Int32.Parse(sections[1]);
            uint vehicleID = UInt32.Parse(sections[2]);

            _engine.RegisterVehicleCreation(fleetID, vehicleID);
        }

        private void HandleDownUnderUpdate(string[] sections)
        {
            byte cFleetId = 0;
            uint iVehicleId = 0;

            try
            {
                /* This function produces two packet to pass on to the listener
                 eg. T,1,1010,S
                 T = packet type
                 1 = FleetID
                 1010 = VehicleID
                 S = Status type
				
                S description
                    C = Coach Commenced Job
                    I = Coach in place
                    O = Coach on route loaded
                    E = Empty running
                    P = Pickup
                    D = Dropoff
					
				
                */
                if (sections.Length != DOWN_UNDER_SECTIONS) return;

                cFleetId = Convert.ToByte(sections[1]);
                iVehicleId = Convert.ToUInt32(sections[2]);

                if ((cFleetId == 0) || (iVehicleId == 0)) return;

                WriteToConsole("Received Update for fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ".");

                string sStatus = sections[3];

                Vehicle thisVehicle = mCollection.GetVehicle(cFleetId, iVehicleId);
                if (thisVehicle == null)
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ") is not logged into the listener.");
                    return;
                }

                thisVehicle.ClearStateBit(UnitState.AuxState1);
                thisVehicle.ClearStateBit(UnitState.AuxState2);
                thisVehicle.ClearStateBit(UnitState.AuxState3);
                thisVehicle.ClearStateBit(UnitState.AuxState4);
                thisVehicle.ClearStateBit(UnitState.MDTState1);
                thisVehicle.ClearStateBit(UnitState.MDTState2);
                thisVehicle.ClearStateBit(UnitState.MDTState3);
                thisVehicle.ClearStateBit(UnitState.MDTState4);
                switch (sStatus)
                {
                    case "C":
                        thisVehicle.SetStateBit(UnitState.AuxState1);
                        break;
                    case "I":
                        thisVehicle.SetStateBit(UnitState.AuxState2);
                        break;
                    case "O":
                        thisVehicle.SetStateBit(UnitState.AuxState3);
                        break;
                    case "E":
                        thisVehicle.SetStateBit(UnitState.AuxState4);
                        break;
                    case "P":
                        thisVehicle.SetStateBit(UnitState.MDTState1);
                        break;
                    case "D":
                        thisVehicle.SetStateBit(UnitState.MDTState2);
                        break;
                    default:
                        break;
                }

                mDB.UpdateVehilcleStatus(thisVehicle);

                if (thisVehicle.MostRecentLiveUpdatePacket != null)
                {
                    List<MTData.Transport.Gateway.Packet.GeneralGPPacket> updates = new List<MTData.Transport.Gateway.Packet.GeneralGPPacket>();
                    lock (thisVehicle.oAddtionalLiveUpdateSync)
                    {                    
                        if (thisVehicle.oAddtionalLiveUpdatePackets.Count > 0)
                        {
                            for (int X = 0; X < thisVehicle.oAddtionalLiveUpdatePackets.Count; X++)
                            {
                                updates.Add(((MTData.Transport.Gateway.Packet.GeneralGPPacket)thisVehicle.oAddtionalLiveUpdatePackets[X]).CreateCopy());
                            }
                            thisVehicle.oAddtionalLiveUpdatePackets.Clear();
                        }
                    }
                    mRegistry.NotifyClientOfRemoteStatusChange(updates, thisVehicle);
                    mRegistry.NotifyClientsOfPacket(thisVehicle.MostRecentLiveUpdatePacket, thisVehicle, false);
                    string sMsg = "Updated status " + sStatus + " for fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ".";
                    WriteToConsole(sMsg);
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        private void HandleUserInformationTagUpdate(string[] sections)
        {
            /* This function produces two packet to pass on to the listener
                 eg. I,1,1010,User Information
				
                sections[X]
                    [I Type] = 0
                    [FleetID] = 1
                    [VehicleID] = 2
                    [User Message] = 3
					
            The [User Message] field can be 20 chars long, all "," are to be replace with Convert.ToString((char) 0x40)
			
            */

            byte cFleetId = 0;
            uint iVehicleId = 0;
            string sMsg = "";

            try
            {
                if (sections.Length == 4)
                {
                    cFleetId = Convert.ToByte(sections[1]);
                    iVehicleId = Convert.ToUInt32(sections[2]);
                    sMsg = sections[3];
                    if (sMsg.Length >= 255)
                        sMsg = sMsg.Substring(0, 255);
                    sMsg = sMsg.Replace(Convert.ToString((char)0x40), ",");

                    if ((cFleetId == 0) || (iVehicleId == 0)) return;
                    WriteToConsole("Received User Information Update for fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ".");
                    Vehicle thisVehicle = mCollection.GetVehicle(cFleetId, iVehicleId);
                    thisVehicle.sUserInfo = sMsg;
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        // Job states are mapped to the vehicle state as follows:
        // Endangered = AuxState1
        // Late		  = AuxState2
        // Clear	  = AuxState3

        private void HandleJobUpdate(string[] sections)
        {

            bool bAuxState1, bAuxState2, bAuxState3;
            byte cFleetId = 0;
            uint iVehicleId = 0;

            try
            {
                /* This function produces two packet to pass on to the listener
                 eg. J,1,1010,2999,F,F,T 
				
                sections[X]
                    [J Type] = 0
                    [FleetID] = 1
                    [VehicleID] = 2
                    [DriverID] = 3
                    [Endangered] = 4
                    [Late] = 5
                    [Clear] = 6

                    7/3/2006 - Andrew 

                    sections[X]
                    [J Type] = 0
                    [FleetID] = 1
                    [VehicleID] = 2
                    [DriverID] = 3
                    [Late] = 4
                    [Endangered] = 5
                    [Clear] = 6

                    Kings requested a change in priority, Late now takes priority over Endangered.

                                */
                if (sections.Length != JOB_SECTIONS) return;

                cFleetId = Convert.ToByte(sections[1]);
                iVehicleId = Convert.ToUInt32(sections[2]);

                if ((cFleetId == 0) || (iVehicleId == 0)) return;

                WriteToConsole("Received Update for fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ".");

                Vehicle thisVehicle = mCollection.GetVehicle(cFleetId, iVehicleId);
                if (thisVehicle == null)
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ") is not logged into the listener.");
                    return;
                }

                bAuxState1 = ParseBool(sections[4]);
                bAuxState2 = ParseBool(sections[5]);
                bAuxState3 = ParseBool(sections[6]);

                // Mapping to AuxStates:
                //  Clear		= Aux1
                //  Endangered	= Aux2
                //  Late		= Aux3
                //
                //  7/3/2006 - Andrew 
                //							Kings requested a change in priority, Late now takes priority over Endangered.
                //  Clear	= Aux1
                //  Late	= Aux2
                //  Endangered		= Aux3
                //  No Status = Aux4
                // The change has been made in the XML end, no code changes where made here.


                if (bAuxState1) thisVehicle.SetStateBit(UnitState.AuxState1);
                else thisVehicle.ClearStateBit(UnitState.AuxState1);

                if (bAuxState2) thisVehicle.SetStateBit(UnitState.AuxState2);
                else thisVehicle.ClearStateBit(UnitState.AuxState2);

                if (bAuxState3) thisVehicle.SetStateBit(UnitState.AuxState3);
                else thisVehicle.ClearStateBit(UnitState.AuxState3);

                if (!bAuxState2 && !bAuxState3 && !bAuxState1)
                    thisVehicle.SetStateBit(UnitState.AuxState4);

                mDB.UpdateVehilcleStatus(thisVehicle);

                string sMsg = "(MDTState1 = {0}, MDTState2 = {1}, MDTState3 = {2}) for fleet {3} , Unit {4}.";

                sMsg = string.Format(sMsg, (bAuxState1) ? "True" : "False", (bAuxState2) ? "True" : "False", (bAuxState3) ? "True" : "False", Convert.ToString(cFleetId), Convert.ToString(iVehicleId));
                /*if(bAuxState1)
                    sMsg = sMsg.Replace("[Clear]", "True");
                else
                    sMsg = sMsg.Replace("[Clear]", "False");
                if(bAuxState2)
                    sMsg = sMsg.Replace("[Late]", "True");
                else
                    sMsg = sMsg.Replace("[Late]", "False");
                if(bAuxState3)
                    sMsg = sMsg.Replace("[Endangered]", "True");
                else
                    sMsg = sMsg.Replace("[Endangered]", "False");*/

                if (thisVehicle.MostRecentLiveUpdatePacket != null)
                {
                    // Fire the last position report with the new status out the the clients.
                    if (thisVehicle.oAddtionalLiveUpdatePackets.Count > 0)
                    {
                        List<MTData.Transport.Gateway.Packet.GeneralGPPacket> updates = new List<MTData.Transport.Gateway.Packet.GeneralGPPacket>();
                        lock (thisVehicle.oAddtionalLiveUpdateSync)
                        {
                            if (thisVehicle.oAddtionalLiveUpdatePackets.Count > 0)
                            {
                                for (int X = 0; X < thisVehicle.oAddtionalLiveUpdatePackets.Count; X++)
                                {
                                    updates.Add(((MTData.Transport.Gateway.Packet.GeneralGPPacket)thisVehicle.oAddtionalLiveUpdatePackets[X]).CreateCopy());
                                }
                                thisVehicle.oAddtionalLiveUpdatePackets.Clear();
                            }
                        }
                        mRegistry.NotifyClientOfRemoteStatusChange(updates, thisVehicle);
                    }
                    mRegistry.NotifyClientsOfPacket(thisVehicle.MostRecentLiveUpdatePacket, thisVehicle, false);
                    sMsg = "Updated status " + sMsg;
                    WriteToConsole(sMsg);
                }
                else
                {
                    WriteToConsole("No Update Sent : The 3020 (fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + ") is not logged into the listener.");
                    sMsg = "No previous position report for fleet " + Convert.ToString(cFleetId) + ", Unit " + Convert.ToString(iVehicleId) + " - Status update not sent to clients. Status :  " + sMsg;
                    WriteToConsole(sMsg);
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        private void HandleGroupUpdate(string[] sections)
        {
            // Send an update out to the clients to let them know that the need to update their group list.
            int iFleetId = Convert.ToInt32(sections[1]);

            mRegistry.SendClientsGroupRefresh(iFleetId);
        }

        private void HandleDriverUpdate(string[] sections)
        {
            //			try
            //			{
            //
            //			}
            //			catch(System.Exception ex)
            //			{
            //				WriteToErrorConsole(ex);
            //			}
        }

        private void HandleDriverPointsGraphCurrentUpdate(string[] sections)
        {
            //command = "U,FleetId,VehicleId,graphGroupId,GraphVersion
            XMLUpdate xmlUpdate = new XMLUpdate();
            xmlUpdate.cSubCmd = 'D';
            xmlUpdate.iItemID = 0;
            xmlUpdate.sXMLData = string.Format("Graph,{0},{1},{2},{3}", sections[1], sections[2], sections[3], sections[4]);
            xmlUpdate.iPacketVersion = 1;
            string error = string.Empty;
            byte[] data = xmlUpdate.Encode(ref error);
            if (error.Equals(string.Empty))
            {
                mRegistry.TellClients(Convert.ToInt32(sections[1]), data, false, true);
            }
            else
            {
                _log.Error(sClassName + "HandleDriverPointsGraphCurrentUpdate(string[] sections)", new System.Exception("Failed to encode DriverPoints current running graph update" + error));
            }
        }

        #region Log Event Interface Support
        private void WriteToConsole(string sMsg)
        {
            if (eConsoleEvent != null) eConsoleEvent(sMsg);
        }

        private void WriteToErrorConsole(System.Exception ex)
        {
            string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
            WriteToConsole(sMsg);
        }
        #endregion

        private void mListenThread_eConsoleEvent(string sMsg)
        {
            WriteToConsole("Status Update : " + sMsg);
        }

        #region Dss

        private void ProcessServerSidePacketForDriverPointRuleBreakDssEvent(Vehicle vehicle, int reasonCode, GeneralGPPacket mPacket)
        {
            _log.Info("ProcessServerSidePacketForDriverPointRuleBreakDssEvent, preparing for driver notification.");
            mPacket.cButtonNumber = (byte)0x01;
            DateTime dtTemp = mPacket.mFixClock.ToDateTime();
            dtTemp = dtTemp.AddSeconds(1);
            mPacket.mFixClock.FromDateTime(dtTemp);
            dtTemp = mPacket.mCurrentClock.ToDateTime();
            dtTemp = dtTemp.AddSeconds(1);
            mPacket.mCurrentClock.FromDateTime(dtTemp);
            mDB.InsertNotificationRecord(mPacket, vehicle);
            GeneralGPPacket oLiveUpdatePacket = ((GeneralGPPacket)mPacket).CreateCopy();
            var rule = vehicle.GetTrackingRule(reasonCode);
            if (rule != null)
            {
                _log.Info($"ProcessServerSidePacketForDriverPointRuleBreakDssEvent, notifying driver for rule break, rule id: {rule.Id}.");
                vehicle.AddAdditionalLiveUpdate(rule, oLiveUpdatePacket);
            }
            else
            {
                _log.Info("ProcessServerSidePacketForDriverPointRuleBreakDssEvent, rule not found.");
            }
        }

        private void HandleDssEvent(string[] sections)
        {
            try
            {
                int fleetId = int.Parse(sections[1]);
                int vehicleId = int.Parse(sections[2]);
                int reasonId = int.Parse(sections[3]);
                long dssEventId = long.Parse(sections[4]);
                double latitude = Convert.ToDouble(sections[5]);
                double logitude = Convert.ToDouble(sections[6]);
                DateTime date = DateTime.ParseExact(sections[7], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture);

                _log.InfoFormat("Dss event update received. FleetId: {0}, VehicleId: {1}, ReasonId: {2}, DssEventId: {3}, Date: {4}", fleetId, vehicleId, reasonId, dssEventId, date);

                Vehicle vehicle = mCollection.GetVehicle((byte)fleetId, (uint)vehicleId);

                if (vehicle == null)
                {
                    GeneralGPPacket mPacket = new GeneralGPPacket(_serverTime_DateFormat);
                    vehicle = new Vehicle((byte)fleetId, (uint)vehicleId, mPacket, mDB, _serverTime_DateFormat);
                    mCollection.Add(vehicle);
                }

                GeneralGPPacket generalGpPacket = mDB.LoadVehicleDataFromHistory(fleetId, vehicleId, date, vehicle);

                //if no packet from history or is greater than 2 minutes old then use a new packet
                if (generalGpPacket == null || (date - generalGpPacket.mCurrentClock.ToDateTime()).TotalSeconds > 120)
                {
                    generalGpPacket = new GeneralGPPacket(_serverTime_DateFormat);
                    generalGpPacket.cFleetId = (byte)fleetId;
                    generalGpPacket.iVehicleId = Convert.ToUInt32(vehicleId);
                }

                generalGpPacket.mFix.dLatitude = Convert.ToDecimal(latitude);
                generalGpPacket.mFix.dLongitude = Convert.ToDecimal(logitude);
                generalGpPacket.mFix.cSpeed = (byte)0x00;
                generalGpPacket.mDistance.cMaxSpeed = 0;
                generalGpPacket.mFix.iDirection = 0;
                generalGpPacket.mFixClock = new GPClock("", new DateTime(date.Year, date.Month, date.Day, date.Hour, date.Minute, date.Second, 0), _serverTime_DateFormat);
                generalGpPacket.mCurrentClock = generalGpPacket.mFixClock;
                PositionDetails oPos = mDB.GetPositionForVehicle(vehicle, generalGpPacket, Convert.ToDecimal(latitude), Convert.ToDecimal(logitude));

                generalGpPacket.cMsgType = GeneralGPPacket.DSS_START;
                generalGpPacket.cInputNumber = Convert.ToByte(reasonId >> 8 & 0xFF);

                //Notify driver rule break.
                ProcessServerSidePacketForDriverPointRuleBreakDssEvent(vehicle, reasonId, generalGpPacket);

                mRegistry.NotifyClientOfRemoteStatusChange(generalGpPacket, vehicle, dssEventId);

                _log.InfoFormat("Dss event update sent. FleetId: {0}, VehicleId: {1}, ReasonId: {2}, DssEventId: {3}, Date: {4}", fleetId, vehicleId, reasonId, dssEventId, date);
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + " - HandleDssEvent - Failed to handle Dss event.", ex);
            }
        }

        private void HandleIapMessage(IList<string> sections)
        {
            if (sections.Count != 12 || sections.Contains("[_COMMENT]") == false && sections.Contains("[_MASSDECAL]") == false)
            {
                _log.Info("HandleIapMessage, comment section string is invalid");
            }

            var fleetID = int.Parse(sections[1]);
            var vehicleID = uint.Parse(sections[2]);
            var dLat = Convert.ToDouble(sections[3]);
            var dLong = Convert.ToDouble(sections[4]);
            var iDay = Convert.ToInt32(sections[5]);
            var iMonth = Convert.ToInt32(sections[6]);
            var iYear = Convert.ToInt32(sections[7]);
            var iHour = Convert.ToInt32(sections[8]);
            var iMinute = Convert.ToInt32(sections[9]);
            var iSecond = Convert.ToInt32(sections[10]);

            DateTime dtDeviceTime;
            try
            {
                dtDeviceTime = new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error("HandleIapMessage, Unable to parse device time from message.", ex);
                return;
            }

            var oVehicle = mCollection.GetVehicle(((byte[])BitConverter.GetBytes(fleetID))[0], vehicleID);
            if (oVehicle == null)
            {
                var mPacket = new GeneralGPPacket(_serverTime_DateFormat);
                oVehicle = new Vehicle((byte)fleetID, (uint)vehicleID, mPacket, mDB, _serverTime_DateFormat);
                mCollection.Add(oVehicle);
            }

            var oGp = mDB.LoadVehicleDataFromHistory(fleetID, (int)vehicleID, dtDeviceTime, oVehicle);

            // if no packet from history or is greater than 2 minutes old then use a new packet
            if (oGp == null || (dtDeviceTime - oGp.mCurrentClock.ToDateTime()).TotalSeconds > 120)
            {
                oGp = new GeneralGPPacket(_serverTime_DateFormat)
                {
                    cFleetId = (byte) fleetID,
                    iVehicleId = Convert.ToUInt32(vehicleID)
                };
            }

            oGp.mFix.dLatitude = Convert.ToDecimal(dLat);
            oGp.mFix.dLongitude = Convert.ToDecimal(dLong);
            oGp.mFix.cSpeed = (byte)0x00;
            oGp.mDistance.cMaxSpeed = 0;
            oGp.mFix.iDirection = 0;
            oGp.mFixClock = new GPClock("", new DateTime(iYear, iMonth, iDay, iHour, iMinute, iSecond, 0), _serverTime_DateFormat);
            oGp.mCurrentClock = oGp.mFixClock;

            var commentMessage = sections.Contains("[_COMMENT]");
            oGp.cMsgType = GeneralGPPacket.IAP_UPDATE;
            oGp.cInputNumber = commentMessage ? (byte)0x00 : (byte)0x01;

            var sRet = mDB.InsertNotificationRecord(oGp, oVehicle);
            if (sRet == null)
                mRegistry.NotifyClientOfRemoteStatusChange(oGp, oVehicle);
            else
                _log.Info(sRet);

            _log.InfoFormat(
                commentMessage
                    ? "Finished handling IAP comment message type for fleet {0}, Unit {1}."
                    : "Finished handling IAP mass declaration message type for fleet {0}, Unit {1}.", fleetID,
                vehicleID);
        }

        #endregion
    }
}

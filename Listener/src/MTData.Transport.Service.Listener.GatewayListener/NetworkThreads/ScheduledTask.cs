using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using log4net;
using MTData.Common;
using MTData.Common.Threading;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
    public enum TaskStates
    {
        Unknown = 0,
        Active = 1,
        Warning,
        Due,
        Alarm,
        Complete,
        Cancelled
    }

    public enum TriggerTypes
    {
        Unknown = 0,
        ScheduleDate = 1,
        WarningDate,
        AlarmDate,
        Distance,
        WarningDistance,
        AlarmDistance,
        EngineHours,
        WarningEngineHours,
        AlarmEngineHours
    }

    public class ScheduledTask
    {
        #region private fields
        private static ILog _log = LogManager.GetLogger(typeof(ScheduledTask));
        private int _id;
        private int? _fleetId;
        private int? _vehicleId;
        private int? _assetId;
        private int _templateId;
        private TaskStates _state;
        private DateTime? _scheduledDate;
        private DateTime? _warningDate;
        private DateTime? _alarmDate;
        private int? _distance;
        private int? _warningDistance;
        private int? _alarmDistance;
        private int? _engineHours;
        private int? _warningEngineHours;
        private int? _alarmEngineHours;
        private string _taskName;
        private IEmailHelper emailHelper;
        #endregion

        #region public properties
        public int Id { get { return _id; } }
        public int TemplateId { get { return _templateId; } }
        public TaskStates State { get { return _state; } }
        public DateTime? ScheduledDate { get { return _scheduledDate; } }
        public DateTime? WarningDate { get { return _warningDate; } }
        public DateTime? AlarmDate { get { return _alarmDate; } }
        public int? Distance { get { return _distance; } }
        public int? WarningDistance { get { return _warningDistance; } }
        public int? AlarmDistance { get { return _alarmDistance; } }
        public int? EngineHours { get { return _engineHours; } }
        public int? WarningEngineHours { get { return _warningEngineHours; } }
        public int? AlarmEngineHours { get { return _alarmEngineHours; } }
        public string TaskName { get { return _taskName; } }
        #endregion

        #region constructor
        public ScheduledTask(DataRow row, IEmailHelper emailHelper)
        {
            _id = Convert.ToInt32(row["Id"]);
            _fleetId = row.Field<int?>("FleetId");
            _vehicleId = row.Field<int?>("VehicleId");
            _assetId = row.Field<int?>("AssetId");
            _templateId = -1;
            _taskName = string.Empty;
            if (!DBNull.Value.Equals(row["TemplateId"]))
            {
                _templateId = Convert.ToInt32(row["TemplateId"]);
            }
            _state = (TaskStates)Convert.ToInt32(row["State"]);

            if (!DBNull.Value.Equals(row["ScheduledDate"]))
            {
                _scheduledDate = Convert.ToDateTime(row["ScheduledDate"]);
            }
            if (!DBNull.Value.Equals(row["WarningDate"]))
            {
                _warningDate = Convert.ToDateTime(row["WarningDate"]);
            }
            if (!DBNull.Value.Equals(row["AlarmDate"]))
            {
                _alarmDate = Convert.ToDateTime(row["AlarmDate"]);
            }

            if (!DBNull.Value.Equals(row["DistanceValue"]))
            {
                _distance = Convert.ToInt32(row["DistanceValue"]);
            }
            if (!DBNull.Value.Equals(row["WarnKmsPrior"]))
            {
                _warningDistance = Convert.ToInt32(row["WarnKmsPrior"]);
            }
            if (!DBNull.Value.Equals(row["AlarmOnKmsOver"]))
            {
                _alarmDistance = Convert.ToInt32(row["AlarmOnKmsOver"]);
            }

            if (!DBNull.Value.Equals(row["EngineHoursValue"]))
            {
                _engineHours = Convert.ToInt32(row["EngineHoursValue"]);
            }
            if (!DBNull.Value.Equals(row["WarnEngineHoursPrior"]))
            {
                _warningEngineHours = Convert.ToInt32(row["WarnEngineHoursPrior"]);
            }
            if (!DBNull.Value.Equals(row["AlarmOnEngineHoursOver"]))
            {
                _alarmEngineHours = Convert.ToInt32(row["AlarmOnEngineHoursOver"]);
            }

            if (!DBNull.Value.Equals(row["Description"]))
            {
                _taskName = Convert.ToString(row["Description"]);
            }
            this.emailHelper = emailHelper;
        }
        #endregion

        #region public methods
        public void CheckOdoAndEngineHours(int odometer, int engineHours, DatabaseInterface database)
        {
            TriggerTypes trigger = TriggerTypes.Unknown;
            string notes = null;
            string value = "";

            //if current state is not in alarm state, check alarm conditions
            if (_state < TaskStates.Alarm)
            {
                if (_alarmDistance.HasValue && _alarmDistance.Value < odometer)
                {
                    _state = TaskStates.Alarm;
                    trigger = TriggerTypes.AlarmDistance;
                    value = odometer.ToString();
                    notes = string.Format("Odometer is greater than configured alarm value - Odometer: {0}, Alarm: {1}", odometer, _alarmDistance.Value);
                }
                else if (_alarmEngineHours.HasValue && _alarmEngineHours.Value < engineHours)
                {
                    _state = TaskStates.Alarm;
                    trigger = TriggerTypes.AlarmEngineHours;
                    value = engineHours.ToString();
                    notes = string.Format("Engine Hours is greater than configured alarm value - EngineHours: {0}, Alarm: {1}", engineHours, _alarmEngineHours.Value);
                }
            }
            //if current state is not in due state, check due conditions
            if (_state < TaskStates.Due)
            {
                if (_distance.HasValue && _distance.Value < odometer)
                {
                    _state = TaskStates.Due;
                    trigger = TriggerTypes.Distance;
                    value = odometer.ToString();
                    notes = string.Format("Odometer is greater than configured scheduled value - Odometer: {0}, Scheduled: {1}", odometer, _distance.Value);
                }
                else if (_engineHours.HasValue && _engineHours.Value < engineHours)
                {
                    _state = TaskStates.Due;
                    trigger = TriggerTypes.EngineHours;
                    value = engineHours.ToString();
                    notes = string.Format("Engine Hours is greater than configured scheduled value - EngineHours: {0}, Scheduled: {1}", engineHours, _engineHours.Value);
                }
            }
            //if current state is not in warning state, check warning conditions
            if (_state < TaskStates.Warning)
            {
                if (_warningDistance.HasValue && _warningDistance.Value < odometer)
                {
                    _state = TaskStates.Warning;
                    trigger = TriggerTypes.WarningDistance;
                    value = odometer.ToString();
                    notes = string.Format("Odometer is greater than configured warning value - Odometer: {0}, Warning: {1}", odometer, _warningDistance.Value);
                }
                else if (_warningEngineHours.HasValue && _warningEngineHours.Value < engineHours)
                {
                    _state = TaskStates.Warning;
                    trigger = TriggerTypes.WarningEngineHours;
                    value = engineHours.ToString();
                    notes = string.Format("Engine Hours is greater than configured warning value - EngineHours: {0}, Warning: {1}", engineHours, _warningEngineHours.Value);
                }
            }

            if (trigger != TriggerTypes.Unknown)
            {
                //need to put this on a new Thread so that the packet processing can continue and isn't held up by the database save
                UpdateTask update = new UpdateTask();
                update.Task = this;
                update.Notes = notes;
                update.Database = database;
                update.Trigger = trigger;
                update.Value = value;
                EnhancedThread thread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(UpdateTaskState), update);
                thread.Start();
            }
        }
        public bool CheckDate(DateTime date, DatabaseInterface database)
        {
            TriggerTypes trigger = TriggerTypes.Unknown;
            string notes = null;

            //if current state is not in alarm state, check alarm conditions
            if (_state < TaskStates.Alarm)
            {
                if (_alarmDate.HasValue && _alarmDate.Value < date)
                {
                    _state = TaskStates.Alarm;
                    trigger = TriggerTypes.AlarmDate;
                    notes = "Date is greater than configured alarm value";
                }
            }
            //if current state is not in due state, check due conditions
            if (_state < TaskStates.Due)
            {
                if (_scheduledDate.HasValue && _scheduledDate.Value < date)
                {
                    _state = TaskStates.Due;
                    trigger = TriggerTypes.ScheduleDate;
                    notes = "Date is greater than configured scheduled value";
                }
            }
            //if current state is not in warning state, check warning conditions
            if (_state < TaskStates.Warning)
            {
                if (_warningDate.HasValue && _warningDate.Value < date)
                {
                    _state = TaskStates.Warning;
                    trigger = TriggerTypes.WarningDistance;
                    notes = "Date is greater than configured warning value";
                }
            }

            if (trigger != TriggerTypes.Unknown)
            {
                //don't need to put this on a new thread as this is processed in the scheduledTask thread in GatewayAdministrator
                DataSet data = database.UpdateTaskState(this, notes, trigger);
                SendNotifications(data, database, trigger, date.ToString("dd/MM/yyyy HH:mm:ss"));
                return true;
            }
            return false;
        }
        #endregion

        private void SendNotifications(DataSet data, DatabaseInterface database, TriggerTypes trigger, string value)
        {
            try
            {
                if (data != null && data.Tables.Count >= 2)
                {
                    string sourceAddress = ConfigurationManager.AppSettings["EMailSourceAddress"];
                    string sourceDisplayName = ConfigurationManager.AppSettings["MaintenanceEmailDisplayName"];

                    DataRow vehicle = database.GetVehicleRow((byte)_fleetId, (uint)_vehicleId);
                    foreach (DataRow row in data.Tables[0].Rows)
                    {
                        int id = Convert.ToInt32(row["Id"]);
                        string subject = Convert.ToString(row["MessageTitle"]);
                        string message = Convert.ToString(row["Message"]);
                        subject = ReplaceTags(subject, vehicle, trigger, value);
                        message = ReplaceTags(message, vehicle, trigger, value);

                        DataRow[] emailRows = data.Tables[1].Select(string.Format("NotificationId = {0}", id));
                        foreach (DataRow emailRow in emailRows)
                        {
                            var email = Convert.ToString(emailRow["Email"]);
                            emailHelper.SendEmail(email, sourceAddress, sourceDisplayName, subject, message, false);
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                _log.ErrorFormat("Error sending Schedule Notification - {0}", exp.Message);
            }
        }

        private string ReplaceTags(string message, DataRow vehicle, TriggerTypes trigger, string value)
        {
            message = message.Replace("[Fleet]", Convert.ToString(vehicle["FleetName"]));
            message = message.Replace("[Vehicle]", Convert.ToString(vehicle["DisplayName"]));
            message = message.Replace("[Time]", DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss"));
            message = message.Replace("[State]", _state.ToString());
            message = message.Replace("[Value]", value);
            string triggerTypeStr = "";
            string triggerValueStr = "";
            switch (trigger)
            {
                case TriggerTypes.ScheduleDate:
                    triggerTypeStr = "Schedule Date";
                    triggerValueStr = _scheduledDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    break;
                case TriggerTypes.WarningDate:
                    triggerTypeStr = "Warning Date";
                    triggerValueStr = _warningDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    break;
                case TriggerTypes.AlarmDate:
                    triggerTypeStr = "Alarm Date";
                    triggerValueStr = _alarmDate.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    break;
                case TriggerTypes.Distance:
                    triggerTypeStr = "Distance";
                    triggerValueStr = _distance.Value.ToString();
                    break;
                case TriggerTypes.WarningDistance:
                    triggerTypeStr = "Warning Distance";
                    triggerValueStr = _warningDistance.Value.ToString();
                    break;
                case TriggerTypes.AlarmDistance:
                    triggerTypeStr = "Alarm Distance";
                    triggerValueStr = _alarmDistance.Value.ToString();
                    break;
                case TriggerTypes.EngineHours:
                    triggerTypeStr = "Engine Hours";
                    triggerValueStr = _engineHours.Value.ToString();
                    break;
                case TriggerTypes.WarningEngineHours:
                    triggerTypeStr = "Warning Engine Hours";
                    triggerValueStr = _warningEngineHours.Value.ToString();
                    break;
                case TriggerTypes.AlarmEngineHours:
                    triggerTypeStr = "Alarm Engine Hours";
                    triggerValueStr = _alarmEngineHours.Value.ToString();
                    break;
            }
            message = message.Replace("[TriggerType]", triggerTypeStr);
            message = message.Replace("[TriggerValue]", triggerValueStr);
            message = message.Replace("[Name]", _taskName);
            return message;
        }

        private object UpdateTaskState(EnhancedThread sender, object data)
        {
            UpdateTask update = data as UpdateTask;
            if (update != null)
            {
                DataSet dataSet = update.Database.UpdateTaskState(update.Task, update.Notes, update.Trigger);
                SendNotifications(dataSet, update.Database, update.Trigger, update.Value);
            }
            return null;
        }

        private class UpdateTask
        {
            public ScheduledTask Task;
            public string Notes;
            public TriggerTypes Trigger;
            public DatabaseInterface Database;
            public string Value;
        }
    }
}

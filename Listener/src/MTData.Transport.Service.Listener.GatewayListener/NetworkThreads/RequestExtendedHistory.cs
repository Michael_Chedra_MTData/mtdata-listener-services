﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
    public class RequestExtendedHistory
    {
        #region private fileds
        private int _id;
        private DateTime _requestDate;
        private DateTime _startDate;
        private DateTime _endDate;
        private DateTime? _requestSentDate;
        #endregion

        #region properties
        public int Id { get { return _id; } }
        public DateTime RequestDate { get { return _requestDate; } }
        public DateTime StartDate { get { return _startDate; } }
        public DateTime EndDate { get { return _endDate; } }
        public DateTime? RequestSentDate { get { return _requestSentDate; } set { _requestSentDate = value; } }
        public DateTime? LastHistoryPacketReceived { get; set; }
        #endregion

        #region constructor
        public RequestExtendedHistory(DataRow row)
        {
            _id = Convert.ToInt32(row["Id"]);
            _requestDate = Convert.ToDateTime(row["RequestDate"]);
            _startDate = Convert.ToDateTime(row["StartDate"]);
            _endDate = Convert.ToDateTime(row["EndDate"]);
            if (!DBNull.Value.Equals(row["RequestSentDate"]))
            {
                _requestSentDate = Convert.ToDateTime(row["RequestSentDate"]);
            }
        }
        #endregion

    }
}

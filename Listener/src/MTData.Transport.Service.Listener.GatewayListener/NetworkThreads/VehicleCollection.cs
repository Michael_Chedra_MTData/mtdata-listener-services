using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	public delegate void AcknowledgementNotificationDelegate(bool bIsGood);

	/// <summary>
	/// Holds one SenderThread to ACK per mobile unit,
	/// plus additional threads as needed per unit to 
	/// interact.
	/// The collection knows the last IP, Seq and Ack values
	/// used to talk with any given unit
	/// </summary>
	public class VehicleCollection 
	{
		public event WriteToConsoleEvent eConsoleEvent; 
		public delegate void WriteToConsoleEvent(string sMsg) ;
		public event ConcreteStatusChangeEvent eConcreteStatusChanged; 
		public delegate void ConcreteStatusChangeEvent(int iFleetID, byte[] bData) ;
		public event SendResetToUnitEvent eResetUnitEvent; 
		public delegate void SendResetToUnitEvent(GatewayProtocolPacket aPacket) ;

		#region Variables
		private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
		public bool bLogPacketData = false;
		public bool bAckAllRequested = false;
		private int iMaxNumThreads;	// max number of units to track
		private System.Timers.Timer mPurgeTimer;	// schedules the check and removal of dead units
		private SortedList List;
        private SortedList WatchList;
		private SortedList StatusList;
		private UdpClient mSender;				
		private bool bNotifyDelegate;
		private bool bNotifyIsPositive;
		private bool bRevertUnitsToPrimary;
		private DatabaseInterface mDatabase = null;

		private PacketSource.PacketExchange _packetExchange = null;

		public bool RevertUnitsToPrimary
		{
			get
			{
				return bRevertUnitsToPrimary;
			}
			set
			{
				bRevertUnitsToPrimary = value;
				lock(List.SyncRoot)
				{
					foreach (Vehicle v in List.GetValueList())
					{
						v.bRevertUnitToPrimary = value;
					}
				}
			}
		}

		public int StationaryMinutes;

        public int GetPacketSentCountAndReset
        {
            get
            {
                int sent = 0;
                lock (List.SyncRoot)
                {
                    foreach (Vehicle v in List.GetValueList())
                    {
                        sent += v.GetPacketSentCountAndReset;
                    }
                }
                return sent;
            }

        }
        #endregion
        #region Constructor
        public void Dispose()
		{
			

			try
			{
                PurgeAllUnits();
			}
			catch(System.Exception)
			{
			}
            try
			{
                if (WatchList != null)
				{
                    lock (WatchList.SyncRoot)
                        WatchList.Clear();
                    WatchList = null;
				}
			}
			catch(System.Exception)
			{
			}

			try
			{
				if(StatusList != null)
				{
					StatusList.Clear();
					StatusList = null;
				}
			}
			catch(System.Exception)
			{
			}

			try
			{
				if(mSender != null)
				{
					mSender.Close();
				}
			}
			catch(System.Exception)
			{
			}

			try
			{
				if(mSender != null)
				{
					mSender = null;
				}
			}
			catch(System.Exception)
			{
			}

			
		}

		public VehicleCollection(	UdpClient theClient, 
			int maxNum, 
			int purgeFrequencySecs,
			int iPacketRateLimitInterval,
			int iPacketRateLimitThreshold,
			ref DatabaseInterface DBInterface,
			string serverTime_DateFormat,
			PacketSource.PacketExchange packetExchange)
		{
			_packetExchange = packetExchange;
			_serverTime_DateFormat = serverTime_DateFormat;
			mDatabase = DBInterface;
			mSender = theClient;
			iMaxNumThreads = maxNum;
			List = SortedList.Synchronized(new SortedList());
            WatchList = SortedList.Synchronized(new SortedList());
			List.Clear();	// List holds the Vehicles

			// List holds the vehicle statuses.  
			// The status is held independent of the vehicle object so that when a vehicle sends a GEN_IM_ALIVE, the last
			// know status can be re-applied to the new vehicle object.
			StatusList = SortedList.Synchronized(new SortedList());
			StatusList.Clear();	

			bNotifyDelegate = false;

			mPurgeTimer = new System.Timers.Timer();
			mPurgeTimer.Enabled = true;
			mPurgeTimer.Interval = purgeFrequencySecs * 1000;
			// Add a handler which will purge the dead units
			mPurgeTimer.Elapsed += new System.Timers.ElapsedEventHandler(PurgeDeadUnits);
			// At Matts request 9/5/07 - Making the amount of minutes loaded from the config file
			//						   - This value is used to determine how long a vehicle must be stationary before
			//						   - the concrete state changes to on-site
			try
			{
				this.StationaryMinutes = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["StationaryMins"]);
			}
			catch(System.Exception)
			{
				this.StationaryMinutes = 2;
			}
		}

		/// <summary>
		/// This method will ensure that all events get triggered correctly for a vehicle.
		/// </summary>
		/// <param name="aPacket"></param>
		/// <returns></returns>
		public Vehicle CreateVehicle(GatewayProtocolPacket aPacket, bool isInboundPacket, bool bTransitioning)
		{
			Vehicle aVehicle = null;
			try
			{
				aVehicle = new Vehicle(aPacket, ref mDatabase, _serverTime_DateFormat);

				// Get the seq and ack numbers. These will
				// need to be changed as they will be based on
				// the packet we are responding to.
				//  we should of course acknowledge their packet:
				aVehicle.cAckSequence = aVehicle.mPacket.cOurSequence;

				// and we should begin our Seq numbers with whatever the other
				// end is expecting us to:
				aVehicle.cOurSequence = Convert.ToByte((aVehicle.mPacket.cAckSequence + 1) % 32);
		
				// Initialise the bitmap list:
				aVehicle.InitialiseBitmap(aVehicle.mPacket.cOurSequence);	
				
				if ((!isInboundPacket) && bTransitioning)
				{	// Packet is outbound and is based on a recent inbound
					// packet. If the inbound packet appeared to come 
					// from 0,0 as opposed to the actual target (as is the
					// case for a GEN_IM_ALIVE) then we won't update the
					// unit info for the actual target unless we do it here:
					aVehicle.cAckSequence = aVehicle.mPacket.cOurSequence;		
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
				aVehicle = null;
			}	
			return aVehicle;
		}

		#endregion
		#region Create a unique identifier based on fleet and vehicle Ids:
		public static int GetUniqueUnitId(GatewayProtocolPacket p)
		{
			return (((int) p.cFleetId) << 16) + (int) p.iVehicleId;
		}
		public static int GetUniqueUnitId(byte fleet, uint unit)
		{
			return ((int) fleet << 16) + (int) unit;
		}

		#endregion
		public bool Add(Vehicle aVehicle)
		{
			try
			{
				#region Check List limits
				int iCount = 0;
				lock(List.SyncRoot)
				{
					iCount = List.Count;
				}
				if (iCount == (iMaxNumThreads - (iMaxNumThreads / 10)))
				{
					// Getting within 10% of our limit. Check for dead units.
					PurgeDeadUnits();	
				}
				if (iCount == iMaxNumThreads) return false; // hopeless case
				#endregion
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);
			}

			try
			{
				// Finally, add it:
				lock(List.SyncRoot)
				{
					List.Add(aVehicle.iUniqueId, aVehicle);
				}

                aVehicle.eConsoleEvent += new MTData.Transport.Service.Listener.GatewayListener.Vehicle.WriteToConsoleEvent(aVehicle_eConsoleEvent);
                aVehicle.bLogPacketData = bLogPacketData;
                // Tell this thread who to use to talk back:
                aVehicle.mSender = this.mSender;
            }
			catch(System.Exception ex3)
			{
				WriteToErrorConsole(ex3);
			}			
			return true;
		}
		public void UpdateVehicleIP(GatewayProtocolPacket aPacket)
		{
			Vehicle target = GetVehicle(aPacket);
            if (target != null)
            {
                target.mSenderIP = aPacket.mSenderIP;
                target.bUnitAlive = true;
            }
		}
		/// <summary>
		/// We got another packet in, we should simply make sure we
		/// will ACK it when it is time to do so.
		/// </summary>
		/// <param name="targetVehicle"></param>
		/// <param name="aPacket"></param>
		/// <param name="isInboundPacket"></param>
		/// <param name="bBlockAck"></param>
		/// <returns></returns>
		public bool UpdateVehicle(Vehicle targetVehicle, GatewayProtocolPacket aPacket, bool isInboundPacket, bool bBlockAck)
		{
			if (isInboundPacket)
			{
				targetVehicle.mSenderIP = aPacket.mSenderIP;
                targetVehicle.bUnitAlive = true;
				#region Verify Continuity of inbound packets:
				// ACK details. We can only ACK the last packet which we received 
				// consecutively with its predecessor. To check this we use a bitmap
				// of the last 32 sequence numbers we received. Any "holes" in the 
				// bitmap mean a lost or out-of-sequence packet, and we can only
				// ACK the packet received just BEFORE this problem.
				// If the packet has bAckRegardless set, just blindly accept it:
				
				if(aPacket.cMsgType == GeneralGPPacket.GEN_ACK || aPacket.cMsgType == GeneralGPPacket.GEN_NAK)
				{
					Console.WriteLine("F/V : " + Convert.ToString((int)targetVehicle.cFleet) + "/" + Convert.ToString((int) targetVehicle.iUnit) + " - We sent a packet with Sequence " + Convert.ToString((int) targetVehicle.cOurSequence));
					Console.WriteLine("F/V : " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString((int) aPacket.iVehicleId) + " - Unit Send Ack Sequence " + Convert.ToString((int) aPacket.cAckSequence));
				}
				
				try
				{
					if (aPacket.bAckRegardless ) // && (aPacket.iVehicleId != 0)
					{
						targetVehicle.InitialiseBitmap(aPacket.cOurSequence);
						targetVehicle.bInRegardlessMode = true;
					}
				
			
					// Check for coming OUT of ack regardless mode. If we are, we must accept the next packet
					// as being in-sequence too.
					if ((!aPacket.bAckRegardless) && (targetVehicle.bInRegardlessMode))
					{
						// First update the per-unit info:
						targetVehicle.bInRegardlessMode = false;
				
						// Now initialise the bitmap one more time, to the Seq number of this packet.
						targetVehicle.InitialiseBitmap(aPacket.cOurSequence);
						// This will result in VerifyBitmap returning the same value - all is happy

						// Fake it that the packet was still wanting ACK regardless -
						// helps the unit come out cleanly by acknowledging its first
						// real-time packet.
						aPacket.bAckRegardless = true;
					}
					else 
					{	// If the packet is OFF and we're NOT, turn it off:
						targetVehicle.bInRegardlessMode = aPacket.bAckRegardless;
					}
				

					targetVehicle.AddToBitmap(aPacket.cOurSequence);
					// Set our ACK field to the last packet we got consecutively:
					targetVehicle.cAckSequence = targetVehicle.VerifyBitmap(aPacket.cOurSequence);
				}
				catch(System.Exception ex2)
				{
					WriteToErrorConsole(ex2);
				}

				#endregion
			}
			else 
			{
				#region Outbound - check for not sending files thrice	
				try
				{

					targetVehicle.mPacket = aPacket;
					if (targetVehicle.mPacket.bIsFileType)
					{	
						// This is an outbound file-type packet (Sched/Conf/WP/RP)
						// Check to see if we've sent three of these exact same files already.
						// If so, proceed no further!
						// If not, add it to the head of the list and shuffle any others down.
						
						if ((targetVehicle.mPacket.cMsgType == targetVehicle.cLastFileSent) &&
							(targetVehicle.mPacket.cMsgType == targetVehicle.cSecondLastFileSent) &&
							(targetVehicle.mPacket.cMsgType == targetVehicle.cThirdLastFileSent))
						{
							if (bAckAllRequested || ((!bAckAllRequested) && (aPacket.cMsgType != GeneralGPPacket.GEN_ACK)))
								targetVehicle.SendAck();
							return false;
						}
						else // it's a different type of packet - good:
						{
							targetVehicle.cThirdLastFileSent	= targetVehicle.cSecondLastFileSent;
							targetVehicle.cSecondLastFileSent	= targetVehicle.cLastFileSent;
							targetVehicle.cLastFileSent			= targetVehicle.mPacket.cMsgType;
						}
					}
					else 
					{
						// not a file-type packet, let's clear our list of file packets.
						targetVehicle.cThirdLastFileSent		= 0;
						targetVehicle.cSecondLastFileSent	= 0;
						targetVehicle.cLastFileSent			= 0;
					}
				}
				catch(System.Exception ex4)
				{
					WriteToErrorConsole(ex4);
				}
				
					// Store this back into the per-unit record:
					//List[thisUnitId] = thisUnit;
					#endregion
			}
				
			// if the packet we got acknowledges a packet we sent, increment
			// our sequence number:
			try
			{
				// This is a patch to get download for Booth Units.
//				if (aPacket.cMsgType == GeneralGPPacket.GEN_ACK || aPacket.cMsgType == GeneralGPPacket.GEN_NAK)
//				{
//					if (mAckDelegate != null) 
//					{
//						aPacket.cAckSequence = targetVehicle.cOurSequence;
//					}
//				}

				if (aPacket.cAckSequence == targetVehicle.cOurSequence)
				{
					targetVehicle.IncrementSequenceNumber();
				}
			}
			catch(System.Exception ex5)
			{
				WriteToErrorConsole(ex5);
			}

			// Accept all flash packets from the units
			aPacket.bSendFlash = true;

			if (isInboundPacket)
			{
				#region The other end wants an ACK now!
				if (aPacket.bAckImmediately || aPacket.bAckRegardless)
				{
					try
					{
						if (bBlockAck) 
							return true;

				
						// Check for a triple-duplicate ACK number, and avoid sending it:
						if ((targetVehicle.mPacket.cAckSequence == targetVehicle.cLastAck) &&
							(targetVehicle.mPacket.cAckSequence == targetVehicle.cSecondLastAck))
						{
							// We're about to ACK the same packet 3 times. Not good.
							// We MUST move on - just ack regardless the incoming packet:
							targetVehicle.mPacket.cAckSequence = aPacket.cOurSequence;

							#region Triple-duplicates are not a concern if REGARDLESS is set:
							if (!aPacket.bAckRegardless)
							{
								// If the number we just put in is the same as the one that's
								// causing all the problems, this won't fix anything. So use the
								// number ONE LESS than that to try and break out of the loop:
								if (targetVehicle.mPacket.cAckSequence == targetVehicle.cLastAck)
								{
									// Clear out the old numbers:
									targetVehicle.cLastAck = 0;
									targetVehicle.cSecondLastAck = 0;

									if (((int) aPacket.cOurSequence) > 0) 
										targetVehicle.mPacket.cAckSequence = (byte) ((int) aPacket.cOurSequence - 1);
									else
										targetVehicle.mPacket.cAckSequence = 31;
								}
							}
							#endregion
						}
						// Add the latest transmitted ACK value to the lists and shuffle them down:
						targetVehicle.cSecondLastAck = targetVehicle.cLastAck;
						targetVehicle.cLastAck = targetVehicle.mPacket.cAckSequence;
					}
					catch(System.Exception ex6)
					{
						WriteToErrorConsole(ex6);
					}
					try
					{
						// Device wants an ACK, so create one:
						// Don't send an ack, if this is an outgoing download packet or the last packet in from the unit is an Ack.

                        if (aPacket.cMsgType != (byte)0x8F && aPacket.cMsgType != GeneralGPPacket.GEN_ACK &&
							(bAckAllRequested || !bAckAllRequested))
							targetVehicle.SendAck();
					}
					catch(System.Exception ex7)
					{
						WriteToErrorConsole(ex7);
					}

				}
				#endregion

				try
				{
					#region See if anyone is interested in the fact that an ACK has come in:
					if (bNotifyDelegate)
					{
						if (aPacket.cMsgType == GeneralGPPacket.GEN_ACK || aPacket.cMsgType == GeneralGPPacket.GEN_NAK)
						{
                            if(GetVehicle(aPacket.cFleetId, aPacket.iVehicleId).mAckDelegate != null)
							//if (mAckDelegate != null) 
							{
                                if (aPacket.cMsgType == GeneralGPPacket.GEN_ACK)
                                    GetVehicle(aPacket.cFleetId, aPacket.iVehicleId).mAckDelegate(true);
                                //mAckDelegate(true);
                                else
                                    GetVehicle(aPacket.cFleetId, aPacket.iVehicleId).mAckDelegate(false);
                                    //mAckDelegate(false);
							}
							bNotifyDelegate = false;
						}
					}
					else
					{
						if (aPacket.cMsgType == GeneralGPPacket.GEN_ACK || aPacket.cMsgType == GeneralGPPacket.GEN_NAK)
						{
							Console.WriteLine("No Delegate is set to recieve this ack.");
						}

						//POD : No0tify the packet exchange of the inbound packet.
						_packetExchange.PacketReceived(Convert.ToInt32(targetVehicle.cFleet), Convert.ToInt32(targetVehicle.iUnit), aPacket, this, targetVehicle);

					}
					#endregion
				}
				catch(System.Exception ex8)
				{
					WriteToErrorConsole(ex8);
				}
			}
			return true;
		}
        // Returns the numbers of all known units within this fleet
        #region Watch List Processing
        public bool AddToWatchList(Vehicle aVehicle)
        {
            try
            {
                #region Check List limits
                lock (WatchList.SyncRoot)
                {
                    if (!WatchList.ContainsKey(aVehicle.iUniqueId))
                        WatchList.Add(aVehicle.iUniqueId, aVehicle);
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
                return false;
            }
            return true;
        }
        public bool UnitIsOnWatchList(Vehicle aVehicle)
        {
            bool bRet = false;
            try
            {
                #region Check List limits
                lock (WatchList.SyncRoot)
                {
                    if (WatchList.ContainsKey(aVehicle.iUniqueId))
                        bRet = true;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
            return bRet;
        }
        public bool RemoveFromWatchList(Vehicle aVehicle)
        {
            bool bRet = false;
            try
            {
                #region Check List limits
                lock (WatchList.SyncRoot)
                {
                    if (WatchList.ContainsKey(aVehicle.iUniqueId))
                        WatchList.Remove(aVehicle.iUniqueId);
                    bRet = true;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
            return bRet;
        }
        #endregion
        #region Config checking
        //		public void SetUnitConfigCheckFlag(byte fleet, uint unit, bool value)
//		{
//			try
//			{
//				int uniqueId = GetUniqueUnitId(fleet, unit);
//				// Quite possibly the unit is not being talked to. No problem:
//				lock (List.SyncRoot)
//				{
//					if (List.ContainsKey(uniqueId))
//						((Vehicle) List[uniqueId]).bNeedsConfigCheck = value;
//				}
//			}
//			catch(System.Exception ex1)
//			{
//				WriteToErrorConsole(ex1);
//			}
//		}

//		public bool GetUnitConfigCheckFlag(GatewayProtocolPacket p)
//		{
//			bool bRet = false;
//			try
//			{
//				int uniqueId = GetUniqueUnitId(p);
//				lock (List.SyncRoot)
//				{
//					if (List.ContainsKey(uniqueId)) 
//						bRet = ((Vehicle) List[uniqueId]).bNeedsConfigCheck;
//				}
//			}
//			catch(System.Exception ex1)
//			{
//				WriteToErrorConsole(ex1);
//			}
//			return bRet;
//		}
		#endregion
		#region Mail checking
		public void SetUnitMailWaitingFlag(Vehicle vehicle, bool value,
			MailMessagePacket mail)
		{
			vehicle.bHasMail = value;
			vehicle.MailMessage = mail;
		}

		public void SetUnitMailWaitingFlag(byte fleet, uint unit, bool value,
			MailMessagePacket mail)
		{
			try
			{
				int uniqueId = GetUniqueUnitId(fleet, unit);
				// Quite possibly the unit is not being talked to. No problem:
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(uniqueId)) 
						SetUnitMailWaitingFlag(((Vehicle) List[uniqueId]), value, mail);
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
		}

//		public bool GetUnitMailWaitingFlag(GatewayProtocolPacket p)
//		{
//			bool bRet = false;
//			try
//			{
//				int uniqueId = GetUniqueUnitId(p);
//				lock (List.SyncRoot)
//				{
//					if (List.ContainsKey(uniqueId)) 
//                        bRet = ((Vehicle) List[uniqueId]).bHasMail;
//				}
//			}
//			catch(System.Exception ex1)
//			{
//				WriteToErrorConsole(ex1);
//			}
//			return bRet;
//		}
//
		#endregion
		#region state and position updates

		// Mask this UnitState so we just get the concrete part:
		private UnitState MaskConcreteState(UnitState s)
		{
			return (UnitState) ((int) s & Vehicle.ConcreteFieldMask); 
		}

		public void SetAtLunchState(int iFleetID, uint iVehicleID)
		{
			bool bHasVehicleEntry = false;
			try
			{
				if(oVehiclesAtLunch == null)
					oVehiclesAtLunch = Hashtable.Synchronized(new Hashtable());

				string sKey = Convert.ToString(iFleetID).PadLeft(5, '0') + Convert.ToString(iVehicleID).PadLeft(5, '0');
				lock(oVehiclesAtLunch.SyncRoot)
				{
					if (!oVehiclesAtLunch.ContainsKey(sKey))
					{
						oVehiclesAtLunch.Add(sKey, sKey);
					}
				}
				byte[] bConvert = BitConverter.GetBytes(iFleetID);
				byte cFleet = bConvert[0];
				int id = (((int) cFleet) << 16) + (int) iVehicleID;
				if (List.ContainsKey(id))
				{
					bHasVehicleEntry = true;
					((Vehicle) List[id]).SetStateBit(UnitState.AtLunch);
				}
				if (bHasVehicleEntry)
				{
					lock (StatusList.SyncRoot)
					{
						if (StatusList.ContainsKey(id)) 
						{
							StatusList.Remove(id);
							StatusList.Add(id, ((Vehicle) List[id]).State);
						}
						else
							StatusList.Add(id, UnitState.AtLunch);
					}
				}
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);			
			}
		}

		public void UnsetAtLunchState(byte bFleetID, uint uVehicleID)
		{
			try
			{
				byte[] bConvert = new byte[4];
				bConvert[0] = bFleetID;
				int iFleetID = BitConverter.ToInt32(bConvert, 0);
				UnsetAtLunchState(iFleetID, uVehicleID);
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);			
			}
		}

		public void UnsetAtLunchState(int iFleetID, uint iVehicleID)
		{
			bool bHasVehicleEntry = false;
			try
			{
				if(oVehiclesAtLunch == null)
					oVehiclesAtLunch = Hashtable.Synchronized(new Hashtable());

				string sKey = Convert.ToString(iFleetID).PadLeft(5, '0') + Convert.ToString(iVehicleID).PadLeft(5, '0');
				lock(oVehiclesAtLunch.SyncRoot)
				{
					if (oVehiclesAtLunch.ContainsKey(sKey))
					{
						oVehiclesAtLunch.Remove(sKey);
					}
				}
				byte[] bConvert = BitConverter.GetBytes(iFleetID);
				byte cFleet = bConvert[0];
				int id = (((int) cFleet) << 16) + (int) iVehicleID;
				if (List.ContainsKey(id))
				{
					bHasVehicleEntry = true;
					((Vehicle) List[id]).ClearStateBit(UnitState.AtLunch);
				}

				if (bHasVehicleEntry)
				{
					lock (StatusList.SyncRoot)
					{
						if (StatusList.ContainsKey(id)) 
						{
							StatusList.Remove(id);
							StatusList.Add(id, ((Vehicle) List[id]).State);
						}
					}
				}
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);			
			}
		}

		public bool IsAtLunch(byte bFleetID, uint uVehicleID)
		{
			bool bRet = false;
			try
			{
				byte[] bConvert = new byte[4];
				bConvert[0] = bFleetID;
				int iFleetID = BitConverter.ToInt32(bConvert, 0);
				int iVehicleID = Convert.ToInt32(uVehicleID);
				string sKey = Convert.ToString(iFleetID).PadLeft(5, '0') + Convert.ToString(iVehicleID).PadLeft(5, '0');
				lock(oVehiclesAtLunch.SyncRoot)
				{
					if (oVehiclesAtLunch.ContainsKey(sKey))
					{
						bRet = true;
					}
				}
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);			
			}
			return bRet;
		}

		// Based on this packet and the current vehicle state, change the
		// concrete state. This function returns false if the packet
		// should be "swallowed" by the listener - i.e. not shown to clients
		private Hashtable oVehiclesAtLunch = null;
		private Hashtable oVehicleIgnitions = null;
		private int iCurrentDay = 0;
		public void UpdateUnitConcreteStatus(GatewayProtocolPacket p, bool bSendCommandTimestamps, bool bSendStatusToCommand, long iUnitState)
		{
			bool bAtLunch = false;
			bool bSendCommandUpdate = true;
			bool bCheckForTicket = false;
			bool bIsTicketed = true;
			UnitState currentState = UnitState.NotKnown;
			UnitState newState = UnitState.NotKnown;
			int id = 0;
			int iUnitID = 0;

			if(oVehiclesAtLunch == null)
				oVehiclesAtLunch = Hashtable.Synchronized(new Hashtable());

			try
			{
				if(System.Configuration.ConfigurationManager.AppSettings["ForwardToCommandEnabled"] == "true")
					bCheckForTicket = true;
			}
			catch(System.Exception)
			{
				bCheckForTicket = false;
			}

			try
			{
				#region Get the current concrete state				
				id = GetUniqueUnitId(p);
				iUnitID = (int) p.iVehicleId;
				if (iUnitID == 0) return;

                bool found = false;
				lock (StatusList.SyncRoot)
				{
                    if (StatusList.ContainsKey(id))
                    {
                        found = true;
                        currentState = (UnitState)StatusList[id];
                    }
				}

				lock (List.SyncRoot)
				{
					if (List.ContainsKey(id))
					{
                        //if current state not found in states list, get it from vehicle
                        if (!found)
                        {
                            currentState = ((Vehicle)List[id]).GetUnitState();
                        }

						if (currentState != UnitState.NotKnown)
						{
							if(currentState != MaskConcreteState(((Vehicle) List[id]).State))
								((Vehicle) List[id]).UpdateConcreteState(currentState);
						}
					}
				}
				#endregion
			}
			catch(System.Exception ex)
			{
				WriteToConsole("Error looking up Concrete state : " + ex.Message + "/nSource : " + ex.Source);
			}
			
			if (bSendStatusToCommand && eConcreteStatusChanged != null)
			{
				#region Send a report for the first ignition event of the day
				try
				{
					if (p.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON)
					{
						#region For the first ignition on of the day, force a stauts update to command.
						#region Clear the ignition status table at the start of each day.
						if(oVehicleIgnitions == null)
							oVehicleIgnitions = Hashtable.Synchronized(new Hashtable());
						
						int iTempDay = System.DateTime.Now.Day;
						if (iTempDay != iCurrentDay)
						{
							iCurrentDay = iTempDay;
							oVehicleIgnitions.Clear();
						}
						#endregion
						#region Get the key for this vehicle.
						byte[] bConvert = new byte[4];
						bConvert[0] = (byte) ((GeneralGPPacket) p).cFleetId;
						int iFleetID = BitConverter.ToInt32(bConvert, 0);
						int iVehicleID = Convert.ToInt32(((GeneralGPPacket) p).iVehicleId);
						string sKey = Convert.ToString(iFleetID).PadLeft(5, '0') + Convert.ToString(iVehicleID).PadLeft(5, '0');
						#endregion
						lock(oVehicleIgnitions.SyncRoot)
						{
							if (!oVehicleIgnitions.ContainsKey(sKey))
							{
								#region If there is no first ignition event for the vehicle, force and update to Command.
								oVehicleIgnitions.Add(sKey, iVehicleID);
								string sCommandData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-";
								if(bSendCommandTimestamps)
									sCommandData += "1-00\r";
								else
									sCommandData += "1\r";
								WriteToConsole("Concrete Status Change : First ignition event : Fleet : " + Convert.ToString(Convert.ToInt32(p.cFleetId)) + " Vehicle : " + Convert.ToString(p.iVehicleId) + " Changed to AtDepotNoLoad");
								// Send the message
								byte[] bCommandData = System.Text.ASCIIEncoding.ASCII.GetBytes(sCommandData);
								eConcreteStatusChanged(Convert.ToInt32(p.cFleetId), bCommandData);
								WriteToConsole("Queuing Status Change : " + sCommandData);	
								#endregion
							}
						}
						#endregion
					}	
				}
				catch(System.Exception ex2)
				{
					WriteToErrorConsole(ex2);
				}
				#endregion
			}

			if (iUnitState > 0)
			{
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_NOT_LOADED) == (long) StatusVehicle59.STATUS_CONC_NOT_LOADED) newState = UnitState.AtDepotNoLoad;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_LOADING) == (long) StatusVehicle59.STATUS_CONC_LOADING) newState = UnitState.AtDepotLoading;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_LOADED) == (long) StatusVehicle59.STATUS_CONC_LOADED) newState = UnitState.AtDepotLoaded;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_TO_JOB) == (long) StatusVehicle59.STATUS_CONC_TO_JOB) newState = UnitState.OnWayToJob;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_AT_JOB) == (long) StatusVehicle59.STATUS_CONC_AT_JOB) newState = UnitState.OnSite;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_UNLOADING) == (long) StatusVehicle59.STATUS_CONC_UNLOADING) newState = UnitState.UnloadingAtSite;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_RETURNING) == (long) StatusVehicle59.STATUS_CONC_RETURNING) newState = UnitState.ReturningHome;
				if ((iUnitState & (long) StatusVehicle59.STATUS_CONC_SWITCHING) == (long) StatusVehicle59.STATUS_CONC_SWITCHING) newState = UnitState.SwitchingPlants;
			}
			else
			{
				#region Check if the vehicle is in the 'At Lunch' state
				try
				{
					bAtLunch = this.IsAtLunch(p.cFleetId, p.iVehicleId);
				}
				catch(System.Exception ex)
				{
					WriteToErrorConsole(ex);
					bAtLunch = false;
				}
				#endregion
				#region State Machine
				try
				{
					switch (p.cMsgType)
					{
							#region Arrived at waypoint:
						case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_ARRIVE:
							// Easy - the state is just AtDepot, and we should be empty.
							newState = UnitState.AtDepotNoLoad;
							if (bAtLunch)
								this.UnsetAtLunchState(p.cFleetId, p.iVehicleId);
							break;	
							#endregion
							#region Departed Waypoint:
						case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_DEPART:
							if (!bAtLunch)
							{
								switch (currentState)
								{
										// At Matts request 9/5/07 - Matt asked that when the unit is in the 'At Depot No Load' state and it departs the plant
										//							the unit should go to 'Left Plant Not Loaded' (SwitchingPlants) state.
										//							This will cause the listener to send a state 1 to command.

									case UnitState.AtDepotLoading:
										// Mix has not taken place:
										// DRIVER ERROR 3
										newState = UnitState.OnWayToJob;
										break;
									case UnitState.AtDepotLoaded:
										// Perfect - we're mixed and on our way
										newState = UnitState.OnWayToJob;
										break;
									default:
										newState = UnitState.SwitchingPlants;
										break;
								}
							}
							break;
							#endregion
							#region Barrel up to speed
						case GeneralGPPacket.BARREL_RPM_UP_TO_SPEED:
							if(!bAtLunch)
							{
								switch (currentState)
								{
									case UnitState.AtDepotNoLoad:
										newState = UnitState.AtDepotLoading;
										break;
									case UnitState.SwitchingPlants:
										// DRIVER ERROR 1!
										// We thought we were switching plants because
										// they left the plant without spinning up the barrel.
										p.cMsgType = GeneralGPPacket.BARREL_DRIVER_ERROR_1;
										newState = UnitState.OnWayToJob;
										break;
									case UnitState.UnloadingAtSite:
										// let the message through!!
										break;
									case UnitState.NotKnown:
										newState = UnitState.AtDepotLoading;
										break;
									default:
										break;
								}
							}
							break;
							#endregion
							#region Barrel Reversed
						case GeneralGPPacket.BARREL_RPM_REVERSED:
							if(!bAtLunch)
							{
								switch (currentState)
								{
										// At Matts request 9/5/07 - Matt asked that when the unit is in the Loading state and the barrel is reversed,
										//							set the unit back to at plant no load.
										//							This will cause the listener to send a state 8 to command.
									case UnitState.AtDepotLoading:
										//								case UnitState.AtDepotLoaded:
										// If we haven't yet left the depot, 
										// it's probably just being cleaned etc...
										newState = UnitState.AtDepotNoLoad;
										bSendCommandUpdate = false;
										break;
									case UnitState.SwitchingPlants:
										// Driver Error 2
										// We thought we were switching plants because
										// they left the plant without spinning up the barrel,
										// and now they're unloading something!
										p.cMsgType = GeneralGPPacket.BARREL_DRIVER_ERROR_2;
										newState = UnitState.UnloadingAtSite;
										break;
									case UnitState.OnWayToJob:
									case UnitState.OnSite:
									case UnitState.ReturningHome:
										newState = UnitState.UnloadingAtSite;
										break;
									default:
										break;
								}
							}
							break;
							#endregion
							#region Left Loader
						case GeneralGPPacket.BARREL_LEFT_LOADER:
							if(!bAtLunch)
							{
								if (currentState == UnitState.AtDepotLoading)
								{
									newState = UnitState.AtDepotLoaded;
								}
							}
							break;
							#endregion
							#region Barrel Stopped
						case GeneralGPPacket.BARREL_RPM_STOPPED:
							//						if(!bAtLunch)
							//						{
							//							switch (currentState)
							//							{
							//									// Only care about the barrel stopping
							//									// on the way to the job:
							//								case UnitState.AtDepotLoaded:
							//								case UnitState.OnWayToJob:
							//								case UnitState.OnSite:
							//									// DRIVER ERROR 2:
							//									p.cMsgType = GeneralGPPacket.BARREL_DRIVER_ERROR_2;
							//									break;
							//								case UnitState.UnloadingAtSite:
							//									// Let the message pass through
							//									break;
							//								default:
							//									break;
							//							}
							//						}
							break;
							#endregion
							#region Moving Off
						case GeneralGPPacket.BARREL_ABOVE_ONSITE_SPEED:
							if(!bAtLunch)
							{
								switch (currentState)
								{
									case UnitState.UnloadingAtSite:
										// We've finished, we're going home
										newState = UnitState.ReturningHome;
										break;

										// At Matts request 9/5/07 - 
										//								case UnitState.OnSite:
										//									// If we are on the site, but haven't started unloading, then we moved faster then the given speed limit.
										//									newState = UnitState.OnWayToJob;
										//									break;
									default:
										break;
								}
							}
							break;
				
							#endregion
							#region Ignition On/Off
						case GeneralGPPacket.STATUS_IGNITION_ON:
						case GeneralGPPacket.STATUS_IGNITION_OFF:
							if(!bAtLunch)
							{
								// Ignition ON/OFF. If we are home, switch states back to unloaded:
								if ((((GeneralGPPacket) p).mStatus.cInputStatus & 0x01) == 0x01)
								{
									// Ignition is on.
								}
								else
								{
									// Ignition Off
									switch (currentState)
									{
										case UnitState.AtDepotLoading:
										case UnitState.AtDepotLoaded:
											// If we haven't yet left the depot, 
											// it's probably just being cleaned etc...
											newState = UnitState.AtDepotNoLoad;
											bSendStatusToCommand = false;
											break;
										default:
											newState = currentState;
											break;
									}
								}
							}
							break;
							#endregion
						default:
							break;
					}
				}
				catch(System.Exception ex2)
				{
					WriteToErrorConsole(ex2);
					newState = UnitState.NotKnown;
				}
				#endregion
				#region Check if we have stopped moving (determinie 'At Site' status)
				try
				{
					#region Time check if in state OnWayToJob or OnSite:
					if(!bAtLunch)
					{
						if ((currentState == UnitState.OnWayToJob) &&
							(newState == UnitState.NotKnown))
						{	
							GPClock fixClock = new GPClock("GPS", _serverTime_DateFormat);
							GPPositionFix fix = new GPPositionFix();
							fixClock  = ((IPositionHolder) p).GetFixClock();
							fix  = ((IPositionHolder) p).GetPosition();
							TimeSpan diffTime = new TimeSpan(0);
							lock (List.SyncRoot)
							{
								if (List.ContainsKey(id))
								{
									diffTime = (fixClock.ToDateTime()).Subtract(((Vehicle) List[id]).GetLastNewPositionTime());
									if (diffTime.TotalMinutes >= this.StationaryMinutes)
									{
										if (fix.cSpeed <= ((Vehicle) List[id]).StationarySpeedNM)
										{
											// We're standing still. Move to state OnSite:
											newState = UnitState.OnSite;
										}
									}
								}
							}
						}
						else
						{
							if ((currentState == UnitState.OnSite) && (newState == UnitState.NotKnown))
							{
								GPPositionFix fix = new GPPositionFix();
								fix  = ((IPositionHolder) p).GetPosition();
								lock (List.SyncRoot)
								{
									if (List.ContainsKey(id))
									{
										if (fix.cSpeed >= 20)
										{
											newState = UnitState.OnWayToJob;
											bSendCommandUpdate = false;
										}
									}
								}
							}
						}
					}
					#endregion
				}
				catch(System.Exception ex3)
				{
					WriteToErrorConsole(ex3);
				}
				#endregion
			}
			#region Update the state table with the new vehicle state

			// Set the 'At Lunch' State
			if(bAtLunch)
			{
				((Vehicle) List[id]).SetStateBit(UnitState.AtLunch);
				newState = UnitState.AtLunch;
			}
			else
			{
				((Vehicle) List[id]).ClearStateBit(UnitState.AtLunch);
			}

			try
			{
				if(newState != UnitState.NotKnown)
				{
                    WriteToConsole(string.Format("Concrete Status Change : Fleet : {0} Vehicle : {1} Changed to {2}", p.cFleetId, p.iVehicleId, newState));
                    lock (List.SyncRoot)
					{
						if (List.ContainsKey(id))
						{
							((Vehicle) List[id]).UpdateConcreteState(newState);
						}
					}

					lock (StatusList.SyncRoot)
					{
						if (StatusList.ContainsKey(id)) 
						{
							// Remove the exiting entry
							StatusList.Remove(id);
							// Add a new status entry.
							StatusList.Add(id, newState);
						}
						else
						{
							// Add a new status entry.
							StatusList.Add(id, newState);
						}
					}
				}
			}
			catch(System.Exception ex5)
			{
				WriteToErrorConsole(ex5);
			}
			#endregion

			try
			{
				#region If the state has changed, then send off the update for the Command interface.
				if((currentState != newState && newState != UnitState.NotKnown) && bSendCommandUpdate)
				{
					if (newState == UnitState.AtDepotNoLoad) iUnitState = 8;
					if (newState == UnitState.AtDepotLoading) iUnitState = 2;
					if (newState == UnitState.AtDepotLoaded) iUnitState = 9;
					if (newState == UnitState.OnWayToJob) iUnitState = 3;
					if (newState == UnitState.SwitchingPlants) iUnitState = 3;
					if (newState == UnitState.OnSite) iUnitState = 4;
					if (newState == UnitState.UnloadingAtSite) iUnitState = 5;
					if (newState == UnitState.ReturningHome) iUnitState = 6;

					if(bCheckForTicket)
					{
						lock (List.SyncRoot)
						{
							if (List.ContainsKey(id))
							{
								bIsTicketed = ((Vehicle) List[id]).IsTicketed;
								// When unit is at the OnWayToJob, set the ticketed state to false.
								if(iUnitState == 3)
								{
									((Vehicle) List[id]).IsTicketed = false;
								}
							}
						}
					}
					else
						bIsTicketed = true;

					// If the unit is in the AtDepotLoading and it is not ticketed, then return without sending the state to Command.
					if (iUnitState == 2 && !bIsTicketed)
					{
						return;
					}
					// If the unit was at depot loading and the barrel was reveresed or ignition was turned off, then don't send the state through to Command.
					if (currentState ==  UnitState.AtDepotLoading && newState == UnitState.AtDepotNoLoad)
					{
						return;
					}
					// If the unit was at depot loaded and the barrel was reveresed or ignition was turned off, then don't send the state through to Command.
					if (currentState ==  UnitState.AtDepotLoaded && newState == UnitState.AtDepotNoLoad)
					{
						return;
					}

					if (bSendStatusToCommand && iUnitState < 9)
					{
						#region Send status via the com port.
						byte[] bData = null;
						string sData = "";

						if(bSendCommandTimestamps)
						{
							if (iUnitState == 6)  // Finish pour
							{
								// If the return home state was not initiated by an ignition event
								// Subtract 3 mins for washing.
								sData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-" + Convert.ToString(iUnitState) + "-03\r";
							}
							else
							{
								if (iUnitState == 4) // On-Site
								{
									// We take 3 mins to determine we are on-site so the status change needs -3 mins on it.
									sData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-" + Convert.ToString(iUnitState) + "-03\r";
								}
								else
								{
									// Just send the status
									sData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-" + Convert.ToString(iUnitState) + "-00\r";
								}
							}
						}
						else
						{
							// If timestamps are not supported, just send the status.
							sData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-" + Convert.ToString(iUnitState) + "\r";
						}


						// Send the message
						if (!bAtLunch)
						{
							
							bData = System.Text.ASCIIEncoding.ASCII.GetBytes(sData);
							if (eConcreteStatusChanged != null) eConcreteStatusChanged(Convert.ToInt32(p.cFleetId), bData);
							WriteToConsole("Queuing Status Change : " + sData);						
						}
						else
							WriteToConsole("No Status Sent : Vehicle is in at lunch state.  -> " + sData);		

						#region If the message was finish pour, send another for returning home.
						if (iUnitState == 6)  
						{
							iUnitState = 7;
							bData = null;

							if(bSendCommandTimestamps)
							{
								sData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-" + Convert.ToString(iUnitState) + "-00\r";
							}
							else
							{
								sData = "1-" + Convert.ToString(iUnitID).PadLeft(4, '0') + "-" + Convert.ToString(iUnitState) + "\r";
							}

							if (!bAtLunch)
							{
								bData = System.Text.ASCIIEncoding.ASCII.GetBytes(sData);
								if (eConcreteStatusChanged != null) eConcreteStatusChanged(Convert.ToInt32(p.cFleetId), bData);				
								WriteToConsole("Queuing Status Change : " + sData);						
							}
							else
								WriteToConsole("No Status Sent : Vehicle is in at lunch state.  -> " + sData);		
						}
						#endregion
						#endregion
					}

				}
				#endregion
			}
			catch(System.Exception ex)
			{
				WriteToConsole("Error occured updating Command : " + ex.Message + "\nSource : " + ex.Source);
			}
		}

		public void UpdateUnitState(GatewayProtocolPacket p, UnitState newState)
		{
			//bool bUpdated = false;

			int id = GetUniqueUnitId(p);
			
			if (id == 0) return;

			try
			{
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(id))
					{
						((Vehicle) List[id]).UpdateUnitState(newState);
						//bUpdated = true;
					}
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
		}

		public void UpdateUnitState(GatewayProtocolPacket p, InternalUnitState newState)
		{
			//bool bUpdated = false;

			int id = GetUniqueUnitId(p);
			
			if (id == 0) return;

			try
			{
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(id))
					{
						((Vehicle) List[id]).UpdateUnitState(newState);
						//bUpdated = true;
					}
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}

		}

		public void ClearUnitState(GatewayProtocolPacket p, UnitState newState)
		{
			//bool bUpdated = false;

			int id = GetUniqueUnitId(p);
			
			if (id == 0) return;

			try
			{
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(id))
					{
						((Vehicle) List[id]).ClearStateBit(newState);
						//bUpdated = true;
					}
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
		}

		public void UpdateUnitPosition(GatewayProtocolPacket p, GPPositionFix fix, GPClock fixClock)
		{
			int id = GetUniqueUnitId(p);
			if (id == 0) return;
			try
			{
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(id))
					{
						((Vehicle) List[id]).UpdateUnitPosition(fix, fixClock);
					}
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
        }
        #endregion
        #region Finding what items are in this container
        public ArrayList GetUnitsInThisFleet(byte fleet)
		{
			ArrayList unitList = new ArrayList();
			
			lock (List.SyncRoot)
			{
				foreach (Vehicle v in List.GetValueList())
				{
					if (v.cFleet == fleet)
					{
						unitList.Add(v.iUnit);
					}
				}
			}
			return unitList;
		}

		public string[] ListUnits()
		{
			int count = 0;
			string[] retString = new string[1];
			retString[0] = "No Unit information available.";

			lock(List.SyncRoot)
			{
				count = List.Count;
			}

			if(count > 0)
			{
				count = 0;
				lock(List.SyncRoot)
				{
					retString = new string[List.Count];
					foreach (Vehicle v in List.GetValueList())
					{
						try
						{
							retString[count++] = "Fleet: " + v.cFleet + ", " +
								"Unit: " + v.iUnit + ", " +
								"Check?: " + v.bNeedsConfigCheck.ToString() + ", " +
								"Seq: " + v.cOurSequence + ", " +
								"Ack: " + v.cAckSequence + ", " +
								"State: " + Vehicle.UnitStateToString(v.GetUnitState()) + ", " +
								"Time: " + v.GetLastNewPositionTime().ToShortTimeString();
						}
						catch(System.Exception ex1)
						{
							WriteToErrorConsole(ex1);
						}
					}
				}
			}
			return retString;
		}

		public string[] KnownUnits()
		{
			int count = 0;
			string[] retString = new string[1];
			retString[0] = "0~0";

			lock(List.SyncRoot)
			{
				count = List.Count;
			}

			if(count > 0)
			{
				count = 0;
				lock(List.SyncRoot)
				{
					retString = new string[List.Count];
					foreach (Vehicle v in List.GetValueList())
					{
						try
						{
							retString[count++] = v.cFleet + "~" + v.iUnit;
						}
						catch(System.Exception ex1)
						{
							WriteToErrorConsole(ex1);
						}
					}
				}
			}
			return retString;
		}

		public Vehicle GetVehicle(GatewayProtocolPacket p)
		{
			Vehicle temp;
			try
			{
                if (p != null)
                {
                    if ((temp = GetVehicle(p.cFleetId, p.iVehicleId)) != null)
                        return temp;
                }
			    //POD return GetVehicleByIPAddress(p.mSenderIP); 
				//	It should never go on IP address. if the Fleet and VehicleID are insufficient 
				//	to ascertain who it is, then we don't know who it is!
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
			return null;
		}

		public Vehicle GetVehicleByIPAddress(IPEndPoint ip)
		{
			lock (List.SyncRoot)
			{
				foreach (Vehicle v in List.GetValueList())
				{
					try
					{
                        if (v.mSenderIP.Address.Equals(ip.Address) &&
							(v.mSenderIP.Port == ip.Port))
						{
							if (v.cFleet == 0) return null;
							return v;
						}
					}
					catch(System.Exception ex1)
					{
						WriteToErrorConsole(ex1);
					}										 
				}
			}
			return null;
		}

		public Vehicle GetVehicle(byte cFleetId, uint iVehicleId)
		{
			int unitId = GetUniqueUnitId(cFleetId, iVehicleId);
			
			lock(List.SyncRoot)
			{
				try
				{
					if (List.ContainsKey(unitId))
					{
						return (Vehicle) List[unitId];
					}
				}
				catch(System.Exception ex1)
				{
					WriteToErrorConsole(ex1);
				}
			}
			return null;
		}

		// Returns if we know of a matching unit with a matching message type
		public bool Contains(GatewayProtocolPacket aPacket)
		{
			int id = GetUniqueUnitId(aPacket);
			lock (List.SyncRoot)
			{
				try
				{
					return List.ContainsKey(id);
				}
				catch(System.Exception ex1)
				{
					WriteToErrorConsole(ex1);
				}
			}
			return false;
		}
//POD 10/1/2005 - If the GPRS netwrok restarts, all ip addresses will be different.
//				  If the vehicle cannot be found based in Fleet and unit, then it should not be found.
//		public GatewayProtocolPacket ItemWithSameIPAddress(GatewayProtocolPacket aPacket)
//		{	// return a sample packet from a thread which is talking to the same IP	-
//			// as long as this one DOES have a unit id
//			lock (List.SyncRoot)
//			{
//				foreach (Vehicle v in List.GetValueList())
//				{	 
//					try
//					{
//						if ((v.mPacket.mSenderIP.Address.Equals(aPacket.mSenderIP.Address)) &&
//							(v.mPacket.mSenderIP.Port.Equals(aPacket.mSenderIP.Port)))
//						{
//							// found a matching IP. If it has a decent unit and fleet ID, we should use it:
//
//							if ((v.mPacket.cFleetId != 0) &&
//								(v.mPacket.iVehicleId != 0))
//							{
//								return v.mPacket;
//							}
//						}
//					}
//					catch(System.Exception ex1)
//					{
//						WriteToErrorConsole(ex1);
//					}
//				}
//			}
//			return null;
//		}
		
		// Find the thread which sent the packet which is acked in the
		// the packet being passed in here, and stop its retry timer

		// The definition of "Matching" is :
		// unit IDs are the same,
		// ACK number of the argument packet = SEQ number of the sent packet
		public void StopMatching(GatewayProtocolPacket aPacket)
		{
			Vehicle v = null;
			int unitId = GetUniqueUnitId(aPacket);

			try
			{
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(unitId))
					{
						v = (Vehicle) List[unitId];
					}
					else return;
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}

			bool bAckMatched = false;

			if (v.cOurSequence == aPacket.cAckSequence)
			{
				bAckMatched = true;
			}
			else
			{
				byte[] bConvert = new byte[4];
				bConvert[0] = aPacket.cAckSequence;
				int iConvert = BitConverter.ToInt32(bConvert, 0);
				iConvert += 1;
				if (iConvert > 31)
					iConvert -= 31;
				bConvert = BitConverter.GetBytes(iConvert);
				if (bConvert[0] == v.cOurSequence)
				{
					bAckMatched = true;
					aPacket.cAckSequence = v.cOurSequence;
					v.mPacket.cAckSequence = v.cOurSequence;
				}
			}

			if (bAckMatched)
			{
				try
				{
					// Yep this is it.
					// The only exception is Position Requests, which must
					// be allowed to run out:
					if (aPacket.cMsgType == GeneralGPPacket.STATUS_POS_REPORT) 
						return;

					v.StopThread();

                    if(GetVehicle(aPacket.cFleetId, aPacket.iVehicleId).mAckDelegate != null)
					//if (mAckDelegate != null)
					{
                        if(GetVehicle(aPacket.cFleetId, aPacket.iVehicleId).mAckNotifyMatchPacket != null)
						//if (mAckNotifyMatchPacket != null)
						{
                            if(GetVehicle(aPacket.cFleetId, aPacket.iVehicleId).mAckNotifyMatchPacket.Equals(v.mPacket))
							//if (mAckNotifyMatchPacket.Equals(v.mPacket))
							{
								// Would be great if we could just call the
								// delegate here, but it's too early, the
								// Seq and Ack numbers aren't up-to-date yet.
								// So we set a flag and will do it in UpdateDetails...
								//mAckDelegate();
							
								bNotifyIsPositive = (aPacket.cMsgType != GeneralGPPacket.GEN_NAK);
								bNotifyDelegate = true;
								return;
							}
						}
					}

					//POD Notify Packet Exchange of Ack
					_packetExchange.PacketAcked(Convert.ToInt32(v.cFleet), Convert.ToInt32(v.iUnit));


					// Line here for breakpoint:
					bNotifyIsPositive = false;
					bNotifyDelegate = false;
				}
				catch(System.Exception ex2)
				{
					WriteToErrorConsole(ex2);
				}
			}
			else
			{
			}
		}

		#endregion
		
		#region Unit removal
		public void Remove(Vehicle v)
		{
			v.StopThread();
			lock (List.SyncRoot)
			{
				if (List.GetValueList().Contains(v)) 
				{
					List.RemoveAt(List.IndexOfValue(v));
				}
			}
            v.Dispose();
		}

		/// <summary>
		/// Remove Dead Units from the list of units.
		/// This needs to be a reverse iteration to avoid dropping items out of the list.
		/// </summary>
		private void PurgeDeadUnits()
		{
			//int iListLength; 
			Vehicle v = null;
            if (List == null || List.SyncRoot == null)
                return;
			lock(List.SyncRoot)
			{
				IList keys = List.GetKeyList();
				for(int pos = keys.Count-1; pos >= 0; pos--)
				{
					try
					{
						int key = (int)keys[pos];
						v = (Vehicle)List[key];
						if ((v != null) && (!v.bUnitAlive) && !v.IsWaitingForAccident)
						{
							// Found a dead'un:
							v.StopThread();
							List.Remove(key);
                            v.Dispose();
						}
					}
					catch(System.Exception ex1)
					{
						WriteToErrorConsole(ex1);
					}
				}

//				for (int pos = 0; pos < iListLength; pos++)
//				{	
//					try
//					{
//						v = (Vehicle) (List.GetValueList())[pos]; 
//						if ((v != null) && (!v.bUnitAlive))
//						{
//							// Found a dead'un:
//							v.StopThread();
//							List.Remove(GetUniqueUnitId(v.cFleet, v.iUnit));
//							// restart from the front:
//							pos = 0;
//							iListLength = List.Count;
//						}
//					}
//					catch(System.Exception ex1)
//					{
//						WriteToErrorConsole(ex1);
//					}
//				}	
			}
		}

		// Scan the list of threads for threads which have timed out.
		// If found, such threads are cleaned up, and the associated unitId
		// sequence number list entry removed, so it will restart at seq = 0
		private void PurgeDeadUnits(Object obj, System.Timers.ElapsedEventArgs args)
		{
			try
			{
				PurgeDeadUnits();
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
		}

		public void PurgeUnit(GatewayProtocolPacket theUnitPacket)
		{
			try
			{
				int unitId = GetUniqueUnitId(theUnitPacket);
			
				lock (List.SyncRoot)
				{
					if (List.ContainsKey(unitId))
					{
                        Vehicle v = (Vehicle)List[unitId];
						v.StopThread();
						List.Remove(unitId);
                        v.Dispose();
					}
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
		}

		public void PurgeAllUnits()
		{
			try
			{
				lock(List.SyncRoot)
				{
					foreach (Vehicle t in List.GetValueList())
					{
						t.StopThread();
                        t.Dispose();
					}
					List.Clear();
				}
			}
			catch(System.Exception ex1)
			{
				WriteToErrorConsole(ex1);
			}
		}
		#endregion
		#region Log Event Interface Support
		private void WriteToConsole(string sMsg)
		{
			if(eConsoleEvent != null) eConsoleEvent(sMsg);
		}

		private void WriteToErrorConsole(System.Exception ex)
		{
			string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
			WriteToConsole(sMsg);
		}
		#endregion

        public void OnVehicleReset(GatewayProtocolPacket aPacket)
        {
            if (eResetUnitEvent != null)
                eResetUnitEvent(aPacket);
        }

		private void aVehicle_eConsoleEvent(string sMsg)
		{
			if(eConsoleEvent != null)
				eConsoleEvent("Vehicle Msg : " + sMsg);
		}
	}
}


using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/*
	public delegate void AcknowledgementNotificationDelegate(bool bIsGood);


	public enum UnitState
	{
		NotKnown		=	0x0000,
		AtWayPoint		=	0x0001,
		DepWayPoint		=	0x0002,
		IgnitionOn		=	0x0004,
		IgnitionOff		=	0x0008,
		Input1On		=	0x0010,
		Input2On		=	0x0020,
		Stationary1		=	0x0040,
		Stationary2		=	0x0080,
		Stationary3		=	0x0100,
		DepStationary	=	0x0200,
		MDTState1		=	0x0400,
		MDTState2		=	0x0800,
		MDTState3		=	0x1000,
		MDTState4		=	0x2000,
		AuxState1		= 	0x4000,		// (an alias for Barrel Up to speed)
		BarrelUpToSpeed	=	0x4000,		
		AuxState2		=	0x8000,		// (Barrel Stopped)
		BarrelStopped	=	0x8000,
		AuxState3		=	0x10000,	// (Barrel Reversed)
		BarrelReversed	=	0x10000,
		AuxState4		=	0x20000,	// (Barrel Forward)
		BarrelForward	=	0x20000,
	}

	public enum InternalUnitState
	{
		Input1Off		= 1, 
		Input2Off		= 2,
	}
	*/

	/// <summary>
	/// Holds one SenderThread to ACK per mobile unit,
	/// plus additional threads as needed per unit to 
	/// interact.
	/// The collection knows the last IP, Seq and Ack values
	/// used to talk with any given unit
	/// </summary>
	public class SenderThreadCollection : System.Collections.CollectionBase
	{
		#region Variables

		public class PerUnitInformation
		{
			public IPEndPoint	mSenderIP;		// the IP address we use to talk to this unit
			public byte			cFleet;			// Fleet of this unit
			public uint			iUnit;			// Unit number
			public byte			cOurSequence;	// the last SN we used with this unit
			public byte			cAckSequence;	// the last ACK we used
			public uint			iBitmap;		// the bitmap of SNs we've had
			public bool			bInRegardlessMode; // used to check if we have been dumb-ACKing everything
			public byte			cLastSeq;		// the last sequence number that arrived
			public byte			cSecondLastSeq;	// the Seq before that
			public byte			cLastAck;		// the last ACK we should use
			public byte			cSecondLastAck;	// the ACK before that
			public byte			cLastFileSent;		// The type of the last Schedule/Config/RP/WP
			public byte			cSecondLastFileSent;	// The type of the second last Schedule/Config/RP/WP
			public byte			cThirdLastFileSent;	// The type of the third last Schedule/Config/RP/WP
			public bool			bNeedsConfigCheck;	// Set if there has been a config change to the fleet
			public bool			bHasMail;			// Set if there is a mail message waiting
			public MailMessagePacket MailMessage;	// The message which awaits
			public UnitState	mUnitState;			// The detected state of this device
			//public 
													
			public PerUnitInformation()
			{
				mSenderIP = null;
				cOurSequence = 0;
				cAckSequence = 0;
				iBitmap = 0;
				bInRegardlessMode = false;
				cLastSeq = 255;
				cLastAck = 255;
				cSecondLastAck = 255;
				cLastFileSent = 0;
				cSecondLastFileSent = 0;
				cThirdLastFileSent = 0;
				bNeedsConfigCheck = false;
				mUnitState = UnitState.NotKnown;
			}

			public PerUnitInformation(byte fleet, uint unit)
			{
				cFleet = fleet;
				iUnit = unit;
				mSenderIP = null;
				cOurSequence = 0;
				cAckSequence = 0;
				iBitmap = 0;
				bInRegardlessMode = false;
				cLastSeq = 255;
				cLastAck = 255;
				cSecondLastAck = 255;
				cLastFileSent = 0;
				cSecondLastFileSent = 0;
				cThirdLastFileSent = 0;
				bNeedsConfigCheck = false;
				mUnitState = UnitState.NotKnown;
			}


		}
		private int iMaxNumThreads;	// max number of units to track
		private System.Timers.Timer mPurgeTimer;	// schedules the check and removal of dead units
	
		private UdpClient mSender;				
		private SortedList mUnitInfoList;
		private AcknowledgementNotificationDelegate mAckDelegate;
		private GatewayProtocolPacket mAckNotifyMatchPacket;
		private bool bNotifyDelegate;
		public bool bRevertUnitsToPrimary;
		public const int SEQ_BITMAP_SIZE = 32;	// Record last 32 packets
		public const int SEQ_WINDOW_SIZE = 15;	// Check the consecutivity(?) of the last X
		#endregion
		public SenderThreadCollection(	UdpClient theClient, 
										int maxNum, 
										int purgeFrequencySecs,
										int iPacketRateLimitInterval,
										int iPacketRateLimitThreshold)
		{
			mSender = theClient;
			iMaxNumThreads = maxNum;
			List.Clear();

			mAckDelegate = null;
			mAckNotifyMatchPacket = null;
			bNotifyDelegate = false;

			SortedList sl = new SortedList();
			mUnitInfoList = SortedList.Synchronized(sl);
			mUnitInfoList.Clear();

			mPurgeTimer = new System.Timers.Timer();
			mPurgeTimer.Enabled = true;
			mPurgeTimer.Interval = purgeFrequencySecs * 1000;
			// Add a handler which will purge the dead units
			mPurgeTimer.Elapsed += new System.Timers.ElapsedEventHandler(PurgeDeadUnits);
		}

		// Describe all the elements of this collection:
		public string[] ListThreads()
		{
			string[] retString = new string[List.Count];
			int tCount = 0;
			foreach (SenderThread t in List)
			{
				retString[tCount++] = t.ToString();
			}
			return retString;
		}

		public string[] ListUnits()
		{
			string[] retString = null;
			lock(mUnitInfoList.SyncRoot)
			{
				int uToCount = mUnitInfoList.Count;
				if(uToCount > 0)
				{
					retString = new string[uToCount];
			
					for (int uCount = 0; uCount < uToCount; uCount++)
					{
						PerUnitInformation pui = (PerUnitInformation) mUnitInfoList.GetByIndex(uCount);

						retString[uCount] = "Fleet: " + pui.cFleet + ", " +
							"Unit: " + pui.iUnit + ", " +
							"Check?: " + pui.bNeedsConfigCheck.ToString() + ", " +
							"Seq: " + pui.cOurSequence + ", " +
							"Ack: " + pui.cAckSequence + ", " +
							"State: " + UnitStateToString(pui.mUnitState);
					}
				}
				else
				{
					retString = new string[1];
					retString[0] = "No Unit Information Available";
				}
			}
			return retString;
		}


		#region Create a unique identifier based on fleet and vehicle Ids:
		public ulong GetUniqueUnitId(GatewayProtocolPacket p)
		{
			return ((ulong) (p.cFleetId) << 32) + p.iVehicleId;
		}
		public ulong GetUniqueUnitId(byte fleet, uint unit)
		{
			return ((ulong) (fleet) << 32) + unit;
		}

		#endregion

		public bool Add(ref SenderThread aThread, bool isInboundPacket, bool bBlockAck, bool bTransitioning)
		{
			if (List.Count == (iMaxNumThreads - (iMaxNumThreads / 10)))
			{
				// Getting within 10% of our limit. Check for dead units.
				PurgeDeadUnits();	
			}
			if (List.Count == iMaxNumThreads) return false; // hopeless case

			// Tell this thread who to use to talk back:
			aThread.mSender = this.mSender;

			// If the target and the packet type of this thread are the same as a thread
			// which already exists, we must not allow it to go ahead. 
			// This prevents us getting stuck in loops when the car
			// rejects something we have sent.
			//if (this.Contains(aThread.mPacket)) return false;
			if (!isInboundPacket)
			{
				ulong thisUnitId = GetUniqueUnitId(aThread.mPacket);
				lock (mUnitInfoList.SyncRoot)
				{
					if (mUnitInfoList.ContainsKey(thisUnitId))
					{
						// Get the info about this unit:
						PerUnitInformation thisUnit = ((PerUnitInformation) mUnitInfoList[thisUnitId]);	
						if (aThread.mPacket.bIsFileType)
						{	
							// This is an outbound file-type packet (Sched/Conf/WP/RP)
							// Check to see if we've sent three of these exact same files already.
							// If so, proceed no further!
							// If not, add it to the head of the list and shuffle any others down.
								
							if ((aThread.mPacket.cMsgType == thisUnit.cLastFileSent) &&
								(aThread.mPacket.cMsgType == thisUnit.cSecondLastFileSent) &&
								(aThread.mPacket.cMsgType == thisUnit.cThirdLastFileSent))
							{
								return false;
							}
							else // it's a different type of packet - good:
							{
								thisUnit.cThirdLastFileSent		= thisUnit.cSecondLastFileSent;
								thisUnit.cSecondLastFileSent	= thisUnit.cLastFileSent;
								thisUnit.cLastFileSent			= aThread.mPacket.cMsgType;
							}
						}
						else 
						{
							// not a file-type packet, let's clear our list of file packets.
							thisUnit.cThirdLastFileSent		= 0;
							thisUnit.cSecondLastFileSent	= 0;
							thisUnit.cLastFileSent			= 0;
						}
				
						// Store this back into the per-unit record:
						mUnitInfoList[thisUnitId] = thisUnit;
					}
				}
			}
			lock(List.SyncRoot)
			{
				List.Add(aThread);
			}
			// Get the seq and ack numbers. These will
			// need to be changed as they will be based on
			// the packet we are responding to.
			lock (mUnitInfoList.SyncRoot)
			{
				if (!mUnitInfoList.ContainsKey(GetUniqueUnitId(aThread.mPacket)))
				{	// First time we've tried to send to this unit. 
					// Hence the information this packet has is as good
					// as it gets. Add it.
					PerUnitInformation newUnit = new PerUnitInformation(aThread.mPacket.cFleetId, aThread.mPacket.iVehicleId);

					//  we should of course acknowledge their packet:
					newUnit.cAckSequence = aThread.mPacket.cOurSequence;

					// and we should begin our Seq numbers with whatever the other
					// end is expecting us to:
					newUnit.cOurSequence = Convert.ToByte((aThread.mPacket.cAckSequence + 1) % 32);
				
					// Add it to the central list, shared amongst all threads
					mUnitInfoList.Add(GetUniqueUnitId(aThread.mPacket), newUnit);

					// Initialise the bitmap list:
					InitialiseBitmap(GetUniqueUnitId(aThread.mPacket), aThread.mPacket.cOurSequence);	
				}
				else
				{
					if ((!isInboundPacket) && bTransitioning)
					{	// Packet is outbound and is based on a recent inbound
						// packet. If the inbound packet appeared to come 
						// from 0,0 as opposed to the actual target (as is the
						// case for a GEN_IM_ALIVE) then we won't update the
						// unit info for the actual target unless we do it here:
						((PerUnitInformation) mUnitInfoList[GetUniqueUnitId(aThread.mPacket)]).cAckSequence = aThread.mPacket.cOurSequence;		
					}
				}
			}
		
			// Process the packet the same way, whether it's from a new unit or not
			UpdateDetails(aThread.mPacket, isInboundPacket, bBlockAck);	
			return true;
		}

		#region Sequence number checking and modification
		private void InitialiseBitmap(ulong aUnitId, byte aStartSeq)
		{
			lock (mUnitInfoList.SyncRoot)
			{
				if(mUnitInfoList.ContainsKey(aUnitId))
				{
					((PerUnitInformation) mUnitInfoList[aUnitId]).iBitmap = 0xFFFFFFFF; // Simulate that we've got everything
					((PerUnitInformation) mUnitInfoList[aUnitId]).cLastSeq = aStartSeq;
				}
			}
		}

		// Add the given sequence number to the SN bitmap we're keeping
		// for the given unit ID
		private void AddToBitmap (ulong aUnitId, byte aSeq)
		{
			uint bitmap = 0;
			int lastSeq = 0;
			bool processGaps = true;
			bool bFound = false;

			lock (mUnitInfoList.SyncRoot)
			{
				if(mUnitInfoList.ContainsKey(aUnitId))
				{
					bFound = true;
					bitmap = ((PerUnitInformation) mUnitInfoList[aUnitId]).iBitmap;
					lastSeq = ((PerUnitInformation) mUnitInfoList[aUnitId]).cLastSeq;
				}
			}
	
			if(!bFound) return;

			// ignore duplicate sequence numbers:
			if (lastSeq == aSeq) return; // nothing has changed - we can return

			// Set the appropriate bit
			bitmap |= (uint) ( 1 << (aSeq));

			// Also allow duplicates of the last 15 packets we got: 
			// Hence there is a window, trailing 15 packets behind the
			// most recently-received packet
			int dupWindowSize = SEQ_WINDOW_SIZE;
			int lowestAllowedDuplicate;
			if (lastSeq >= dupWindowSize) 
			{
				// Normal case - example: we just got 14, now we rxed 12
				// so lastSeq = 14, aSeq = 12, lowestAllowed = 4
				lowestAllowedDuplicate = lastSeq - dupWindowSize;
				if ((aSeq < lastSeq) && (aSeq >= lowestAllowedDuplicate))
				{
					processGaps = false;
				}
			}
			else
			{
				// Rollover case - example: we just got 3, now we rxed 1
				// so lastSeq = 3, aSeq = 1, lowestAllowed = 27
				lowestAllowedDuplicate = (SEQ_BITMAP_SIZE - dupWindowSize) + lastSeq;
				
				if (aSeq < lastSeq)
				{
					// Rxed packet must be in the first few, no problem
					// This is like the above example case
					{
						processGaps = false; 
					}
				}
				else if ((aSeq > lowestAllowedDuplicate) &&
						 (aSeq < SEQ_BITMAP_SIZE))
				{
					// Example here is lastSeq = 2, aSeq = 29, lowest = 26
					processGaps = false;
				}
			}

			if (processGaps)
			{
				// Get to here if the packet was not a legal duplicate, and hence should be
				// fully processed for gaps

				// Clear any bits between this packet and the last we got!
				if ( lastSeq <= (aSeq) )
				{	// normal case, plus the ideal case
					for (int pos = lastSeq+1; (pos < aSeq); pos++)
					{
						bitmap &= (uint) ~((1 << (pos)));
					}
				}
				else
				{	// rollover case
					// Top bit
					for (int pos = lastSeq+1; (pos < SEQ_BITMAP_SIZE); pos++)
					{
						bitmap &= (uint) ~((1 << (pos)));
					}
					
					// Lower chunk - Process from 0 up to aSeq:
					for (int pos = 0; (pos < aSeq); pos++)
					{
						bitmap &= (uint) ~((1 << (pos)));
					}
					
				}
			}
			// Store everything back for next time
			lock (mUnitInfoList.SyncRoot)
			{
				((PerUnitInformation) mUnitInfoList[aUnitId]).cSecondLastSeq = (byte) lastSeq;	// Shuffle up the last seq
				((PerUnitInformation) mUnitInfoList[aUnitId]).cLastSeq = aSeq;	// Last seq was this one
				((PerUnitInformation) mUnitInfoList[aUnitId]).iBitmap = bitmap;
			}
		}

		// Check for holes in the bitmap:
		// Report whether we have any holes in the bitmap for this unit
		// from position <startseq> back to <lastseq>, including rollovers
		private byte VerifyBitmap(ulong aUnitId, int aStartSeq)
		{
			int retVal = -1;
			uint bitmap;
			int lastSeq;

			lock (mUnitInfoList.SyncRoot)
			{
				bitmap = ((PerUnitInformation) mUnitInfoList[aUnitId]).iBitmap;
				// Begin the search at the last packet we acknowledged:
				lastSeq = ((PerUnitInformation) mUnitInfoList[aUnitId]).cLastAck;
			}
			// Only had a few packets, or perhaps just rolled over
			if (aStartSeq < lastSeq)
			{
				// Check the top part of the window:
				for (int pos = lastSeq; pos < SEQ_BITMAP_SIZE; pos++)
				{
					if ((bitmap & ( 1 << (int) pos)) == 0) 
					{
						retVal = pos;
						break;
					}
				}
				if ( retVal == -1)
				{	// Haven't found it in the top part
					// Check the rolled-over area:
					for (int pos = 0; pos < aStartSeq; pos++)
					{
						// Look through what we do have
						if ((bitmap & ( 1 << (int) pos)) == 0)
						{
							retVal = pos;
							break;
						}
					}
				}
			}
			else // Normal case - this sequence is larger than the previous one
			{
				for (int pos = lastSeq; pos < aStartSeq; pos++)
				{
					// Look through the last few packets
					if ((bitmap & (  1 << (int) pos)) == 0) 
					{
						retVal = pos;
						break;
					}
				}
			}
			// Get to here if all was consistent. We'll return the same number, to
			// indicate no holes up to the requested point
			if (retVal == -1) retVal = aStartSeq;
			else
			{
				// retVal currently indicates the lost packet.
				// we must return the LAST packet we actually GOT!
				// so subtract one, and allow for a reverse-rollover
				retVal--;
				if (retVal == -1) retVal = 31;
			}
			return (byte) retVal;
		}
		
		// We've just sent something - inc the sequence number in the 
		// centralised list:
		public void IncrementSequenceNumber(GatewayProtocolPacket aPacket)
		{
			lock (mUnitInfoList.SyncRoot)
			{
				((PerUnitInformation) mUnitInfoList[GetUniqueUnitId(aPacket)]).cOurSequence++;
				if (((PerUnitInformation) mUnitInfoList[GetUniqueUnitId(aPacket)]).cOurSequence > 31)
					((PerUnitInformation) mUnitInfoList[GetUniqueUnitId(aPacket)]).cOurSequence = 0;
			}
		}
	

		// Return the most-recent packet details for this unit
		public PerUnitInformation GetLatestPacket(ulong aUnitId)
		{
			PerUnitInformation oRet = null;

			lock (mUnitInfoList.SyncRoot)
				if (mUnitInfoList.ContainsKey(aUnitId))
					oRet = (PerUnitInformation) mUnitInfoList[aUnitId];

			return oRet;
		}

		// Retrieve the SN of last packet that the unit got from us
		public byte GetLastAckedPacketNumber(ulong aUnitId)
		{
			byte bRet =(byte) 0x00;

			lock (mUnitInfoList.SyncRoot)
				if(mUnitInfoList.ContainsKey(aUnitId))
					bRet =  ((PerUnitInformation) mUnitInfoList[aUnitId]).cLastAck;

			return bRet;
		}

		#endregion
		// We got another packet in, we should simply make sure we
		// will ACK it when it is time to do so.
		public void UpdateDetails(GatewayProtocolPacket aPacket, bool isInboundPacket, bool bBlockAck)
		{
			bool bFound = false;
			ulong id = GetUniqueUnitId(aPacket);

			// Firstly, the IP details can never be disputed - 
			// newer is always better, so copy them in:
			lock (mUnitInfoList.SyncRoot)
				if(mUnitInfoList.ContainsKey(id))
				{
					((PerUnitInformation) mUnitInfoList[id]).mSenderIP = aPacket.mSenderIP;
					bFound = true;
				}

			// We can't update the details of an unknown unit.
			if (!bFound) return;

			if (isInboundPacket)
			{
				#region Verify Continuity of inbound packets:
				// ACK details. We can only ACK the last packet which we received 
				// consecutively with its predecessor. To check this we use a bitmap
				// of the last 32 sequence numbers we received. Any "holes" in the 
				// bitmap mean a lost or out-of-sequence packet, and we can only
				// ACK the packet received just BEFORE this problem.
				// If the packet has bAckRegardless set, just blindly accept it:
				lock (mUnitInfoList.SyncRoot)
				{
					if (aPacket.bAckRegardless ) // && (aPacket.iVehicleId != 0)
					{
						InitialiseBitmap(id, aPacket.cOurSequence);
						((PerUnitInformation) mUnitInfoList[id]).bInRegardlessMode = true;
					}
				}
			
				// Check for coming OUT of ack regardless mode. If we are, we must accept the next packet
				// as being in-sequence too.
				lock (mUnitInfoList.SyncRoot)
				{
					if ((!aPacket.bAckRegardless) && (((PerUnitInformation) mUnitInfoList[id]).bInRegardlessMode))
					{
						// First update the per-unit info:
						((PerUnitInformation) mUnitInfoList[id]).bInRegardlessMode = false;
					
						// Now initialise the bitmap one more time, to the Seq number of this packet.
						InitialiseBitmap(id, aPacket.cOurSequence);
						// This will result in VerifyBitmap returning the same value - all is happy

						// Fake it that the packet was still wanting ACK regardless -
						// helps the unit come out cleanly by acknowledging its first
						// real-time packet.
						aPacket.bAckRegardless = true;
					}
					else 
					{	// If the packet is OFF and we're NOT, turn it off:
						((PerUnitInformation) mUnitInfoList[id]).bInRegardlessMode = aPacket.bAckRegardless;
					}
				}

				AddToBitmap(id, aPacket.cOurSequence);

				lock (mUnitInfoList.SyncRoot)
				{
					// Set our ACK field to the last packet we got consecutively:
					((PerUnitInformation) mUnitInfoList[id]).cAckSequence =
						VerifyBitmap(id, aPacket.cOurSequence);
				}
				#endregion
			}
				
			// Tell any others:
			UpdateThreadsWithSameTarget(aPacket, bBlockAck);

			if (isInboundPacket)
			{
				#region See if anyone is interested in the fact that an ACK has come in:
				if (bNotifyDelegate)
				{
					if (mAckDelegate != null) 
					{
						mAckDelegate(true);
					}
					bNotifyDelegate = false;
				}
				#endregion
				#region Update our internal record of the state of this device
				//UpdateUnitState(aPacket);
				#endregion
			}
		}


		// Run around each thread. If it's talking to the unit
		// with the given ID, force out some new data to it - 
		// IP, Seq and Ack numbers.
		private void UpdateThreadsWithSameTarget(GatewayProtocolPacket aPacket, bool bBlockAck)
		{
			ulong unitId = GetUniqueUnitId(aPacket);
			lock(List.SyncRoot)
			{
				foreach (SenderThread targetThread in List)
				{
					if (GetUniqueUnitId(targetThread.mPacket) == unitId)
					{
						lock (mUnitInfoList.SyncRoot)
						{
							PerUnitInformation thisUnit = ((PerUnitInformation) mUnitInfoList[unitId]);
						
							lock(targetThread)
							{
								targetThread.mPacket.mSenderIP = thisUnit.mSenderIP;
								targetThread.mPacket.cAckSequence = thisUnit.cAckSequence;	
							
								// if the packet we got acknowledges a packet we sent, increment
								// our sequence number:
								if ((aPacket.cAckSequence == targetThread.mPacket.cOurSequence) &&
									(targetThread.mPacket.cMsgType == GeneralGPPacket.GEN_ACK))
								{
									IncrementSequenceNumber(aPacket);
								}

								targetThread.mPacket.cOurSequence = thisUnit.cOurSequence;
								targetThread.mPacket.bAckRegardless = aPacket.bAckRegardless;
						
								#region The other end wants an ACK now!
								if ((targetThread.mPacket.cMsgType == GeneralGPPacket.GEN_ACK) &&
										(aPacket.bAckImmediately || aPacket.bAckRegardless))
								{
									// Always accept flash packets from the units
									targetThread.mPacket.bSendFlash = true;
									if (bBlockAck) return;
									// Check for a triple-duplicate ACK number, and avoid sending it:
									if ((targetThread.mPacket.cAckSequence == (int) thisUnit.cLastAck) &&
										(targetThread.mPacket.cAckSequence == (int) thisUnit.cSecondLastAck))
									{
										// We're about to ACK the same packet 3 times. Not good.
										// We MUST move on - just ack regardless the incoming packet:
										targetThread.mPacket.cAckSequence = aPacket.cOurSequence;

									#region Triple-duplicates are not a concern if REGARDLESS is set:
										if (!aPacket.bAckRegardless)
										{
											// If the number we just put in is the same as the one that's
											// causing all the problems, this won't fix anything. So use the
											// number ONE LESS than that to try and break out of the loop:
											if (targetThread.mPacket.cAckSequence == thisUnit.cLastAck)
											{
												// Clear out the old numbers:
												thisUnit.cLastAck = 0;
												thisUnit.cSecondLastAck = 0;

												if (((int) aPacket.cOurSequence) > 0) 
													targetThread.mPacket.cAckSequence = (byte) ((int) aPacket.cOurSequence - 1);
												else
													targetThread.mPacket.cAckSequence = 31;
				
											}
										}
									#endregion
									}
									// Add the latest transmitted ACK value to the lists and shuffle them down:
									((PerUnitInformation) mUnitInfoList[unitId]).cSecondLastAck = thisUnit.cLastAck;
									((PerUnitInformation) mUnitInfoList[unitId]).cLastAck = (byte) targetThread.mPacket.cAckSequence;
						
									targetThread.SendWithNoRetries(); // Try and send the ACK - no retries!!!
								}
								#endregion
							}	
						}
					}
				}
			}
		}


		// Returns the numbers of all known units within this fleet
		public ArrayList GetUnitsInThisFleet(byte fleet)
		{
			ArrayList unitList = new ArrayList();
			
			lock (List.SyncRoot)
			{
				foreach (SenderThread t in List)
				{
					if (t.mPacket.cFleetId == fleet)
					{
						unitList.Add(t.mPacket.iVehicleId);
					}
				}
			}
			return unitList;
		}

		#region Config checking
		public void SetUnitConfigCheckFlag(byte fleet, uint unit, bool value)
		{
			ulong uniqueId = GetUniqueUnitId(fleet, unit);
			// Quite possibly the unit is not being talked to. No problem:
			lock (mUnitInfoList.SyncRoot)
			{
				if (mUnitInfoList.ContainsKey(uniqueId)) 
					((PerUnitInformation) mUnitInfoList[uniqueId]).bNeedsConfigCheck = value;
			}
		}
		public bool GetUnitConfigCheckFlag(GatewayProtocolPacket p)
		{
			bool bRet = false;
			ulong uniqueId = GetUniqueUnitId(p);
			lock (mUnitInfoList.SyncRoot)
			{
				if (mUnitInfoList.ContainsKey(uniqueId))
					bRet = ((PerUnitInformation) mUnitInfoList[uniqueId]).bNeedsConfigCheck;
			}
			return bRet;
		}
		#endregion
		#region Mail checking
		public void SetUnitMailWaitingFlag(byte fleet, uint unit, bool value,
											MailMessagePacket mail)
		{
			ulong uniqueId = GetUniqueUnitId(fleet, unit);
			// Quite possibly the unit is not being talked to. No problem:
			lock (mUnitInfoList.SyncRoot)
			{
				if (mUnitInfoList.ContainsKey(uniqueId)) 
				{
					((PerUnitInformation) mUnitInfoList[uniqueId]).bHasMail = value;
					((PerUnitInformation) mUnitInfoList[uniqueId]).MailMessage = mail;
				}
			}
		}
		public bool GetUnitMailWaitingFlag(GatewayProtocolPacket p)
		{
			bool bRet = false;
			ulong uniqueId = GetUniqueUnitId(p);
			lock (mUnitInfoList.SyncRoot)
			{
				if (mUnitInfoList.ContainsKey(uniqueId)) 
					bRet = ((PerUnitInformation) mUnitInfoList[uniqueId]).bHasMail;
			}
			return bRet;
		}
		public MailMessagePacket GetUnitMailWaiting(GatewayProtocolPacket p)
		{
			MailMessagePacket oRet = null;
			ulong uniqueId = GetUniqueUnitId(p);
			lock (mUnitInfoList.SyncRoot)
			{
				if (mUnitInfoList.ContainsKey(uniqueId)) 
					oRet = ((PerUnitInformation) mUnitInfoList[uniqueId]).MailMessage;
			}
			return oRet;
		}
		#endregion
		// Returns if we know of a thread which is talking to
		// the matching unit with a matching message type
		public bool Contains(GatewayProtocolPacket aPacket)
		{
			SenderThread targetThread;
			lock (List.SyncRoot)
			{
				for (int pos = 0; pos < List.Count; pos++)
				{	 
					targetThread = (SenderThread) List[pos];
					if ((GetUniqueUnitId(targetThread.mPacket) == GetUniqueUnitId(aPacket)) &&
						(targetThread.mPacket.cMsgType == aPacket.cMsgType))
					{
						return true;
					}
				}
			}
			return false;
		}

		// Find the thread pertaining to the given message from the given vehicle
		public SenderThread ItemMatching(GatewayProtocolPacket aPacket)
		{
			SenderThread targetThread;
			if (!Contains(aPacket)) return null;

			lock(List.SyncRoot)
			{
				for (int pos = 0; pos < List.Count; pos++)
				{	 
					targetThread = (SenderThread) List[pos];
					if ((GetUniqueUnitId(targetThread.mPacket) == GetUniqueUnitId(aPacket)) &&
						(targetThread.mPacket.cMsgType == aPacket.cMsgType))
					{
						return targetThread;
					}
				}
			}
			return null;
		}

		public GatewayProtocolPacket ItemWithSameIPAddress(GatewayProtocolPacket aPacket)
		{	// return a sample packet from a thread which is talking to the same IP	-
			// as long as this one DOES have a unit id
			SenderThread targetThread;

			lock (List.SyncRoot)
			{
				for (int pos = 0; pos < List.Count; pos++)
				{	 
					targetThread = (SenderThread) List[pos];
					if ((targetThread.mPacket.mSenderIP.Address.Equals(aPacket.mSenderIP.Address)) &&
						(targetThread.mPacket.mSenderIP.Port.Equals(aPacket.mSenderIP.Port)))
					{
						// found a matching IP. If it has a decent unit and fleet ID, we should use it:

						if ((targetThread.mPacket.cFleetId != 0) &&
							(targetThread.mPacket.iVehicleId != 0))
						{
							return targetThread.mPacket;
						}
					}
				}
			}
			return null;
		}
		#region Unit removal
		// Find the thread which sent the packet which is acked in the
		// the packet being passed in here, stop the retry timer and
		// destroy the send thread.

		// The definition of "Matching" is :
		// unit IDs are the same,
		// ACK number of the argument packet = SEQ number of the sent packet
        //public void StopAndDestroyMatching(GatewayProtocolPacket aPacket)
        //{
        //    SenderThread targetThread;
        //    ulong unitId = GetUniqueUnitId(aPacket);

        //    lock (List.SyncRoot)
        //    {
        //        for (int pos = 0; pos < List.Count; pos++)
        //        {	 
        //            targetThread = (SenderThread) List[pos];
        //            // Find the matching thread, but only one that's not busy
        //            // doing ACKs - we have a special mechanism for them.
        //            // We also don't count adhoc position reports
        //            if ((GetUniqueUnitId(targetThread.mPacket) == unitId) &&
        //                (targetThread.mPacket.cOurSequence == aPacket.cAckSequence))
        //            {
        //                if (aPacket.cMsgType == GeneralGPPacket.STATUS_POS_REPORT)
        //                {
        //                    // Tell all interested threads:
        //                    UpdateThreadsWithSameTarget(aPacket, false);

        //                    // We don't stop the thread though. Let it play out for
        //                    // as many retries as were requested by the client
        //                    return;
        //                }
        //                else if (targetThread.mPacket.cMsgType != GeneralGPPacket.GEN_ACK)
        //                {
        //                    // Found it!

        //                    // if there is a delegate defined which is interested
        //                    // in knowing when this packet has been matched, check now.
        //                    if (mAckDelegate != null)
        //                    {
        //                        if (mAckNotifyMatchPacket != null)
        //                        {
        //                            if (mAckNotifyMatchPacket.Equals(targetThread.mPacket))
        //                            {
        //                                // Would be great if we could just call the
        //                                // delegate here, but it's too early, the
        //                                // Seq and Ack numbers aren't up-to-date yet.
        //                                // So we set a flag and will do it in UpdateDetails...
        //                                //mAckDelegate();
        //                                bNotifyIsPositive = 
        //                                    (aPacket.cMsgType != GeneralGPPacket.GEN_NAK);
        //                                bNotifyDelegate = true;
        //                            }
        //                        }
        //                    }

        //                    // Ditch it:
						
        //                    List.Remove(targetThread);
						
        //                    targetThread.StopThread();						

        //                    // should only ever be one match, we can go now
        //                    return;
        //                }
        //            }
        //        }
        //    }
        //}


		/// <summary>
		///  The user-provided delegate function will be called when the given packet has
		///  been acknowledged by the remote device. Only one delegate can be provided.
		/// </summary>
		/// <param name="del"></param>
		/// <param name="p"></param>
		public void SetAcknowledgementNotificationDelegate(AcknowledgementNotificationDelegate del,
															GatewayProtocolPacket p)
		{
			mAckDelegate = del;
			mAckNotifyMatchPacket = p;
		}
		public void RemoveAcknowledgementNotificationDelegate()
		{
			mAckDelegate = null;
			mAckNotifyMatchPacket = null;
		}
		public void Remove(SenderThread t)
		{
			t.StopThread();
			lock (List.SyncRoot)
			{
				if (List.Contains(t)) List.Remove(t);
			}
		}

		private void PurgeDeadUnits()
		{
			SenderThread targetThread;
			lock(List.SyncRoot)
			{
				for (int pos = 0; pos < List.Count; pos++)
				{	 
					targetThread = (SenderThread) List[pos];
					if (!targetThread.bUnitAlive)
					{
						// Found a dead'un:
						targetThread.StopThread();
						List.Remove(targetThread);
						//	mUnitInfoList.Remove(GetUniqueUnitId(targetThread.mPacket));
					}
				}	
			}
		}

		// Scan the list of threads for threads which have timed out.
		// If found, such threads are cleaned up, and the associated unitId
		// sequence number list entry removed, so it will restart at seq = 0
		private void PurgeDeadUnits(Object obj, System.Timers.ElapsedEventArgs args)
		{
			PurgeDeadUnits();
		}

		public void PurgeUnit(GatewayProtocolPacket theUnitPacket)
		{
			ulong unitId = GetUniqueUnitId(theUnitPacket);
			SenderThread targetThread;
			

			lock(List.SyncRoot)
			{
				for (int pos = 0; pos < List.Count; pos++)
				{	 
					targetThread = (SenderThread) List[pos];
					// Find all matching threads
					if (GetUniqueUnitId(targetThread.mPacket) == unitId)	
					{
						// Found one!
						targetThread.StopThread();
						List.Remove(targetThread);
						lock (mUnitInfoList.SyncRoot)
						{
							mUnitInfoList.Remove(GetUniqueUnitId(targetThread.mPacket));
						}
					}
				}
			}
		}

		public void PurgeAllUnits()
		{
			lock(List.SyncRoot)
			{
				foreach (SenderThread t in List)
				{
					t.StopThread();
				}
				List.Clear();
			}
			lock (mUnitInfoList.SyncRoot)
			{
				mUnitInfoList.Clear();
			}
		}
		#endregion

		public void UpdateUnitState(GatewayProtocolPacket p, UnitState newState)
		{
			bool bFound = false;
			UnitState currentState = UnitState.NotKnown;
			ulong id = GetUniqueUnitId(p);


			lock (mUnitInfoList.SyncRoot)
			{
				if(mUnitInfoList.ContainsKey(id))
				{
					currentState = ((PerUnitInformation) mUnitInfoList[id]).mUnitState;
					bFound = true;
				}
			}

			// If we can't find the unit, then we can't update it.
			if (!bFound) return;

			#region Logic to control some of the mutually-exclusive states:
			currentState |= newState;
			switch (newState)
			{
				case UnitState.AtWayPoint:
					//currentState |= UnitState.AtWayPoint;
					currentState &= ~UnitState.DepWayPoint;
					break;
				case UnitState.DepWayPoint:
					//currentState |= UnitState.DepWayPoint;
					currentState &= ~UnitState.AtWayPoint;
					break;
				case UnitState.IgnitionOn:
					currentState &= ~UnitState.IgnitionOff;
					break;
				case UnitState.IgnitionOff:
					currentState &= ~UnitState.IgnitionOn;
					break;
				//case UnitState.MDTState1:
				//	currentState &= ~UnitState.MDTState2;
				//	currentState &= ~UnitState.MDTState3;
				//	currentState &= ~UnitState.MDTState4;
				//	break;
				
				default:
					break;
			}
			#endregion

			lock (mUnitInfoList.SyncRoot)
			{
				((PerUnitInformation) mUnitInfoList[id]).mUnitState = currentState;
			}

		}

		public void UpdateUnitState(GatewayProtocolPacket p, InternalUnitState newState)
		{
			bool bFound = false;
			UnitState currentState = UnitState.NotKnown;
			ulong id = GetUniqueUnitId(p);

			lock (mUnitInfoList.SyncRoot)
			{
				if(mUnitInfoList.ContainsKey(id))
				{
					currentState = ((PerUnitInformation) mUnitInfoList[id]).mUnitState;
					bFound = true;
				}
			}

			// if we can't find the unit then we can't update it.
			if(!bFound) return;

			#region Adjust the state based on these not-externally-visible states:
			switch (newState)
			{
				case InternalUnitState.Input1Off:
					currentState &= ~UnitState.Input1On;
					break;
				case InternalUnitState.Input2Off:
					currentState &= ~UnitState.Input2On;
					break;
				default:
					break;
			}
			#endregion

			lock (mUnitInfoList.SyncRoot)
			{
				if(mUnitInfoList.ContainsKey(id))
				{
					((PerUnitInformation) mUnitInfoList[id]).mUnitState = currentState;
				}
			}

		}

		public UnitState GetUnitState(byte cFleet, uint uVehicle)
		{
			UnitState oRet = UnitState.NotKnown;
			ulong id = GetUniqueUnitId(cFleet, uVehicle);
			lock (mUnitInfoList.SyncRoot)
			{
				if (mUnitInfoList.ContainsKey(id))
				{
					oRet =  ((PerUnitInformation) mUnitInfoList[id]).mUnitState;
				}
			}
			return oRet;
		}

		private bool IsBitSet(UnitState us, UnitState testBit)
		{
			if ((us & testBit) == testBit) return true;
			return false;
		}

		public string UnitStateToString(UnitState us)
		{
			System.Text.StringBuilder s = new System.Text.StringBuilder("");
		
			if (IsBitSet(us,UnitState.AtWayPoint)) s.Append("AtWayPoint ");
			if (IsBitSet(us,UnitState.DepWayPoint)) s.Append("DepWayPoint ");
			if (IsBitSet(us,UnitState.IgnitionOn)) s.Append("IgnitionOn ");
			if (IsBitSet(us,UnitState.IgnitionOff)) s.Append("IgnitionOff ");
			if (IsBitSet(us,UnitState.Input1On)) s.Append("Input1On ");
			if (IsBitSet(us,UnitState.Input2On)) s.Append("Input2On ");
			if (IsBitSet(us,UnitState.Stationary1)) s.Append("Stationary1 ");
			if (IsBitSet(us,UnitState.Stationary2)) s.Append("Stationary2 ");
			if (IsBitSet(us,UnitState.Stationary3)) s.Append("Stationary3 ");
			if (IsBitSet(us,UnitState.DepStationary)) s.Append("DepStationary ");
			if (IsBitSet(us,UnitState.MDTState1)) s.Append("MDTState1 ");
			if (IsBitSet(us,UnitState.MDTState2)) s.Append("MDTState2 ");
			if (IsBitSet(us,UnitState.MDTState3)) s.Append("MDTState3 ");
			if (IsBitSet(us,UnitState.MDTState4)) s.Append("MDTState4 ");
			if (IsBitSet(us,UnitState.AuxState1)) s.Append("AuxState1 ");
			if (IsBitSet(us,UnitState.AuxState2)) s.Append("AuxState2 ");
			if (IsBitSet(us,UnitState.AuxState3)) s.Append("AuxState3 ");
			if (IsBitSet(us,UnitState.AuxState4)) s.Append("AuxState4 ");
/*			
			if (IsBitSet(us,UnitState.BarrelUpToSpeed)) s.Append("BarrelUpToSpeed ");
			if (IsBitSet(us,UnitState.BarrelStopped)) s.Append("BarrelStopped ");
			if (IsBitSet(us,UnitState.BarrelReversed)) s.Append("BarrelReversed ");
			if (IsBitSet(us,UnitState.BarrelForward)) s.Append("BarrelForward ");
			*/
			if (s.Length == 0) return "NotKnown";

			return s.ToString();
		}
	}
}

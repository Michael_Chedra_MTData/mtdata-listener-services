using System;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
	/// <summary>
	/// Stops devices from flooding the listener with FLASH stuff
	/// by keeping tabs on the incoming data rate
	/// </summary>
	public class PacketRateLimiter
	{
		private bool bAcceptMorePackets;
		public bool AcceptMorePackets
		{
			get 
			{
				return bAcceptMorePackets;
			}
		}

		private int iMeasureIntervalSeconds;
		private int iLimiterThreshold;
		private int iPacketsThisInterval;
		private System.Timers.Timer mIntervalTimer;

		public PacketRateLimiter(int iMeasureIntervalSeconds, int iLimiterThreshold)
		{
			bAcceptMorePackets = true;
			this.iMeasureIntervalSeconds = iMeasureIntervalSeconds;
			this.iLimiterThreshold = iLimiterThreshold;
			mIntervalTimer = new System.Timers.Timer();
			mIntervalTimer.Interval = (double) iMeasureIntervalSeconds * 1000;
			mIntervalTimer.AutoReset = true;
			mIntervalTimer.Elapsed += new 
				System.Timers.ElapsedEventHandler(this.MeasurementPeriodElapsed);
			iPacketsThisInterval = 0;
			mIntervalTimer.Start();
		}

		private void MeasurementPeriodElapsed(object o, System.Timers.ElapsedEventArgs ea)
		{
			if (iPacketsThisInterval > iLimiterThreshold)
			{
				bAcceptMorePackets = false;
			}
			else
			{ 
				bAcceptMorePackets = true;
			}
			iPacketsThisInterval = 0;
		}

		public void PacketReceived()
		{
			iPacketsThisInterval++;
			// We must also try and slow things down in THIS interval if there is too much coming in:
			if (iPacketsThisInterval > iLimiterThreshold) bAcceptMorePackets = false;
		}
	}
}

using System;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// Provides packet-send functionality with persistent retries
	/// </summary>
	public class SenderThread
	{
		#region Variables
		private int iRetries;
		private int iMaxRetries;
		private int iTimeout;
		public Boolean bUnitAlive;
		public UdpClient mSender;
		private Thread mThread;
		public GatewayProtocolPacket mPacket;
        private int iMinPacketLength = 0;
		private System.Timers.Timer mTimer;
		private SenderThreadCollection mCollection;

		private Boolean bIsPeriodic;
		#endregion
		public SenderThread()
		{
			iRetries = 0;
			mPacket = null;
			bUnitAlive = true;
			mCollection = null;
			bIsPeriodic = false;
            try
            {
                iMinPacketLength = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinOutgoingPacketSize"]);
            }
            catch (System.Exception)
            {
                iMinPacketLength = 0;
            }
		}


		
		public SenderThread(GatewayProtocolPacket aPacket,
							SenderThreadCollection aCollection)
		{
			iRetries = 0;
			mPacket = aPacket;
			bUnitAlive = true;
			mCollection = aCollection;
			bIsPeriodic = false;
            try
            {
                iMinPacketLength = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MinOutgoingPacketSize"]);
            }
            catch (System.Exception)
            {
                iMinPacketLength = 0;
            }
		}


		public new string ToString()
		{
			// Describe what this thread is doing at this instant:
			return	"Sending " + GatewayProtocolLookup.GetMessageType(mPacket.cMsgType) +
				" to Unit " + mPacket.iVehicleId + ", Fleet " + mPacket.cFleetId +
				", IsAlive = " + bUnitAlive.ToString();
		}

		public void StopThread()
		{
			if (mTimer != null) mTimer.Stop();
			if (mThread != null) mThread.Abort();
		}

		#region Public send methods - 3 different policies
		public void SendWithNoRetries()
		{
			SendOnce();
		}

		// Send now, and try N times with interval T if no ack
//		public void SendWithRetries(int aMaxRetries, int aIntervalSecs)
//		{
//			iMaxRetries = aMaxRetries;
//			iTimeout = aIntervalSecs * 1000; // convert to msec for Timer
//			ThreadStart myThreadStart = new ThreadStart(this.HandleThread);
//			if (mThread != null)
//			{
//				try
//				{
//					mThread.Abort();
//				}
//				catch(System.Exception)
//				{
//				}
//				try
//				{
//					mThread = null;
//				}
//				catch(System.Exception)
//				{
//				}
//			}
//			mThread= new Thread(myThreadStart);
//			SendOnce();			// Send the first
//			mThread.Start();	// and start the retry system
//		}

		// Send after one interval T.
		// If there is no apparent response at all after MaxRetries,
		// then terminate this thread.
		public void SendDeferredPeriodic(int aIntervalSecs, 
										int aMaxRetries)
		{
			iMaxRetries = aMaxRetries;	
			bIsPeriodic = true;
			iTimeout = aIntervalSecs * 1000; // convert to msec for Timer
			ThreadStart myThreadStart = new ThreadStart(this.HandleThread);
			mThread= new Thread(myThreadStart);	
			mThread.Start();	// and start the retry system
		}

		#endregion

		#region private implementation functions
        

		private void SendOnce()
		{
			Byte[] encodedBytes = null;
            Byte[] bExtendedBytes = null;
			// See if we have to revert to primary:
			mPacket.bRevertToPrimaryServer = mCollection.bRevertUnitsToPrimary;
			mPacket.Encode(ref encodedBytes);

            if (iMinPacketLength > 0)
            {
                if (encodedBytes.Length < iMinPacketLength)
                {
                    bExtendedBytes = new byte[iMinPacketLength];
                    for (int X = 0; X < encodedBytes.Length; X++)
                    {
                        bExtendedBytes[X] = encodedBytes[X];
                    }
                    mSender.Send(bExtendedBytes, bExtendedBytes.Length, mPacket.mSenderIP);
                }
                else
                {
                    mSender.Send(encodedBytes, encodedBytes.Length, mPacket.mSenderIP);
                }
            }
            else
            {
                mSender.Send(encodedBytes, encodedBytes.Length, mPacket.mSenderIP);
            }
		}

		private void SendAgain(Object obj, System.Timers.ElapsedEventArgs args)
		{
			// Called if we timed out and need to send again
			iRetries++;

			if (iRetries <= iMaxRetries) 
			{
				SendOnce();
				// And restart the timer
				mTimer.Start();
			}
			else
			{
				// we've had too many retries. Give up. Perhaps the unit is offline?
				StopThread();
				bUnitAlive = false;
			}
	
			if (bIsPeriodic)// Deferred ACK system never expires
			{
				// Did the far end get our last one?
				// If so, modify the sending Sequence number:
				if (mCollection == null) return;
				if (mCollection.GetLastAckedPacketNumber(mPacket.iVehicleId) == 
					mPacket.cOurSequence)
				{
				//	mCollection.IncrementSequenceNumber(mPacket);
					// We can also reset our retry counter,
					// because the other end is clearly alive
					iRetries = 0;
				}
			}
		}

		private void HandleThread() 
		{
			// Set a timer that will retry the send in mTimeout seconds,
			// unless we get stopped
			mTimer = new System.Timers.Timer();
			mTimer.Enabled = true;
			mTimer.Interval = iTimeout;
			// Add a handler which will send again or quit out
			mTimer.Elapsed += new System.Timers.ElapsedEventHandler(SendAgain);
		}

        
		#endregion
	}
}

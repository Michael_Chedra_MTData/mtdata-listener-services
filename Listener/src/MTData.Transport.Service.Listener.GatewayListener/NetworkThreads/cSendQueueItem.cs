using System;
using System.Net;
using System.Net.Sockets;

namespace MTData.Transport.Service.Listener.GatewayListener.Network_Threads
{
	/// <summary>
	/// Summary description for cSendQueueItem.
	/// </summary>
	public class cSendQueueItem
	{
		public byte[] bMsg= null;
		public int iLen = 0;
		public IPEndPoint oRemote = null;

		public cSendQueueItem(byte[] Msg, int SendLength, IPEndPoint RemoteEP)
		{
			if (Msg != null && SendLength > 0 && RemoteEP != null)
			{
				bMsg = Msg;
				iLen = SendLength;
				oRemote = RemoteEP;
			}
		}

		public string SendItem(ref Socket oSoc)
		{
			string sRet = "";

			if (oSoc != null && bMsg != null && iLen > 0 && oRemote != null)
			{
				try
				{
					oSoc.Connect(oRemote);
					oSoc.Send(bMsg);
					
				}
				catch(System.Exception ex)
				{
					sRet = "Sending Error : " + ex.Message + ", Source : " + ex.Source;
				}
			}
			return sRet;
		}
	}
}

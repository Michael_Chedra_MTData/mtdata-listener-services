using System;
using System.Collections.Generic;
using System.Data;

namespace MTData.Transport.Service.Listener.GatewayListener
{
    public class ServerTrackingRule
    {
        #region private fields
        private int _id;
        private int _points;
        private int _output1;
        private int _output2;
        private int _period;
        private int _reasonCode;
        #endregion

        #region public properties
        public int Id { get { return _id; } }
        public int Points { get { return _points; } }
        public int Output1 { get { return _output1; } }
        public int Output2 { get { return _output2; } }
        public int Period { get { return _period; } }
        public int ReasonCode { get { return _reasonCode; } }
        #endregion

        #region constructor
        public ServerTrackingRule(DataRow row)
        {
            _id = Convert.ToInt32(row["Id"]);
            _points = Convert.ToInt32(row["Points"]);
            _output1 = Convert.ToInt32(row["Output1"]);
            _output2 = Convert.ToInt32(row["Output2"]);
            _period = Convert.ToInt32(row["Period"]);
            _reasonCode = Convert.ToInt32(row["ReasonCode"]);
        }
        #endregion
    }
}

using System;
using System.Threading;
using System.Collections;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// Summary description for cQueueItems.
	/// </summary>
	public class QueueItems
	{
		#region Events
		public delegate void ItemToProcessEvent(object oData);
		public event ItemToProcessEvent eNewItemToProcess;

		public event WriteToConsoleEvent eConsoleEvent ; 
		public delegate void WriteToConsoleEvent(string sMsg);
		#endregion

		#region Private Vars
		private bool bThreadActive = false;
		private int iMaxQueueLength = 100;
		private int iMinPeriod = 500;
		private Queue oQueue = null;
		private Thread tProcessQueue = null;
		#endregion

		#region Constructor / Destructor
		public QueueItems(int MaxQueueLength, int PaketRateInms)
		{
			oQueue = Queue.Synchronized(new Queue());
			tProcessQueue = new Thread(new ThreadStart(ProcessQueue));
			tProcessQueue.Name = "cQueueItems.ProcessQueue";

			iMaxQueueLength = MaxQueueLength;
			iMinPeriod = PaketRateInms;
		}

		public void Dispose()
		{
			bThreadActive = false;

			if (oQueue != null)
			{
				oQueue.Clear();
				oQueue = null;
			}
		}
		#endregion

		#region Start and Stop Procedures

		public void Start()
		{
			bThreadActive = true;
			tProcessQueue.Start();
		}

		public void Stop()
		{
			if (!bThreadActive) return;

			bThreadActive = false;
			Thread.Sleep(100);
			oQueue.Clear();
		}
		#endregion

		#region Thread loop
		public void ProcessQueue()
		{
			object oData = null;
			int iQueueCount = 0;
			
			while(bThreadActive)
			{
				if (true)
				{
					try
					{
						lock(oQueue.SyncRoot)
							iQueueCount = oQueue.Count;

						if(iQueueCount > 0)
						{
							oData = null;
							lock(oQueue.SyncRoot)
							{
								oData = oQueue.Dequeue();
							}
							if(eNewItemToProcess != null) eNewItemToProcess(oData);
							// Space the update to iMinPeriod ms apart.
							Thread.Sleep(iMinPeriod);
						}
						else
						{
							// If there is no data, then check again every 200ms
							Thread.Sleep(200);
						}
					}
					catch(System.Exception ex)
					{
						WriteToErrorConsole(ex);
					}
				}
			}

			try
			{
				tProcessQueue.Abort();
			}
			catch(System.Exception)
			{
				// Ignore the abort error.
			}
		}
		#endregion

		#region Public functions
		public void SendNextItem()
		{
			Thread.Sleep(200);
		}

		public void EnqueueItem(object oData)
		{
			
			try
			{
				int iQueueCount = 0;

				if (oQueue != null & bThreadActive)
				{
					lock(oQueue.SyncRoot)
					{
						oQueue.Enqueue(oData);
						iQueueCount = oQueue.Count;
					}
				}

				
				if(iQueueCount > iMaxQueueLength)
				{
					object oDitch = null;
					lock(oQueue.SyncRoot)
					{
						oDitch = oQueue.Dequeue();
					}
					oDitch = null;
					WriteToConsole("Ditching object, to many objects queued. Limit = " + Convert.ToString(iMaxQueueLength));
				}
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);
			}
		}

		public void ClearQueue()
		{
			try
			{
				if (oQueue != null)
				{
					lock(oQueue.SyncRoot)
					{
						oQueue.Clear();
					}
				}
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole(ex);
			}
		}
		#endregion
		#region Log Event Interface Support
		private void WriteToConsole(string sMsg)
		{
			if(eConsoleEvent != null) 
				eConsoleEvent(sMsg);
			else
				Console.WriteLine(sMsg);
		}

		private void WriteToErrorConsole(System.Exception ex)
		{
			string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
			WriteToConsole(sMsg);
		}
		#endregion	
	}
}

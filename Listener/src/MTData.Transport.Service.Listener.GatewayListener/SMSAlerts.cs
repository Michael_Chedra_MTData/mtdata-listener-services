using System;
using System.Collections.Generic;
using System.Text;
using System.Xml;
using MTData.Transport.Application.Communication;

namespace MTData.Transport.Service.Listener.GatewayListener
{
    public class SMSAlerts : System.Configuration.IConfigurationSectionHandler
    {
        private Dictionary<short, List<SMSAlertItem>> fleetEmailSourceOveride;
        public string DefaultEmailAddress;
        public SMSAlerts()
        {
            DefaultEmailAddress = "";
        }

        public List<SMSAlertItem> GetSMSEmailAlertDetails(LiveUpdate oLiveUpdate)
        {
            if (fleetEmailSourceOveride != null)
            {
                if (fleetEmailSourceOveride.ContainsKey(oLiveUpdate.iFleetID))
                {
                    List<SMSAlertItem> possibleList = fleetEmailSourceOveride[oLiveUpdate.iFleetID];
                    List<SMSAlertItem> returnList = new List<SMSAlertItem>();
                    for (int X = 0; X < possibleList.Count; X++)
                    {
                        if (possibleList[X].ContainsReasonId(oLiveUpdate.iReasonID))
                        {
                            returnList.Add(possibleList[X]);
                        }
                    }
                    if (returnList.Count > 0)
                        return returnList;
                }
            }
            return null;
        }
        #region IConfigurationSectionHandler Members

        /// <summary>
        /// The format of the descriptors in the app config will be the following..
        /// <code>
        /// <PendantAlarmSMSAlert>
        ///     <EmailOveride fleet="1" EMailSourceAddress="MTData.Service@mtdata.com.au" />
        /// </PendantAlarmSMSAlert>
        /// </code>
        /// </summary>
        /// <param name="parent"></param>
        /// <param name="configContext"></param>
        /// <param name="section"></param>
        /// <returns></returns>
        public object Create(object parent, object configContext, System.Xml.XmlNode section)
        {
            try
            {
                fleetEmailSourceOveride = new Dictionary<short, List<SMSAlertItem>>();
                XmlNodeList list = section.SelectNodes("SMSAlert");
                if ((list != null) && (list.Count > 0))
                {
                    foreach (XmlNode node in list)
                    {
                        string[] fleetList = node.Attributes.GetNamedItem("fleet").Value.Split(",".ToCharArray());
                        foreach (string fleet in fleetList)
                        {
                            SMSAlertItem item = new SMSAlertItem();
                            item.FleetId = Convert.ToInt16(fleet.Trim());
                            item.ReasonIds = node.Attributes.GetNamedItem("ReasonIds").Value;
                            item.EMailSourceAddress = node.Attributes.GetNamedItem("EMailSourceAddress").Value;
                            item.EMailTargetAddress = node.Attributes.GetNamedItem("EMailTargetAddress").Value;
                            item.EMailSubject = node.Attributes.GetNamedItem("EMailSubject").Value.Replace("\\r", "\r").Replace("\\n", "\n");
                            item.EMailBody = node.Attributes.GetNamedItem("EMailBody").Value.Replace("\\r", "\r").Replace("\\n", "\n");
                            List<SMSAlertItem> itemList = null;
                            if (!fleetEmailSourceOveride.ContainsKey(item.FleetId))
                            {
                                itemList = new List<SMSAlertItem>();
                                itemList.Add(item);
                            }
                            else
                            {
                                itemList = fleetEmailSourceOveride[item.FleetId];
                                fleetEmailSourceOveride.Remove(item.FleetId);
                                itemList.Add(item);
                            }
                            fleetEmailSourceOveride.Add(item.FleetId, itemList);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new System.Exception(this.GetType().FullName + ".Create(object parent, object configContext, System.Xml.XmlNode section)", ex);
            }
            return this;
        }

        #endregion
    }
}

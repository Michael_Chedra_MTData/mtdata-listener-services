using System;
using log4net;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class ReportDetails
    {
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.Database_Access.ReportDetails.";
        private static ILog _log = LogManager.GetLogger(typeof(ReportDetails));
        #region Private vars
        private object _packet;
        private int _fleetId;
        private int _vehicleId;
        private byte _cmdType;
        private DateTime? _deviceTime;
        private DateTime? _gpsTime;
        private decimal _latitude;
        private decimal _longitude;
        private uint _routeScheduleId;
        private int _routeCheckPointId;
        private int _setpointGroupID;
        private int _setpointID;
        private int _setpointNumber;
        private bool _hasServerSideWPGroupAssigned;
        private bool _isPacketFromFlash;
        private bool _areThereMorePacketsInFlash;
        private bool _inputPacketIsValid;
        private long _unitStatus;
        private GeneralGPPacket _gpp;
        private string _streetName;
        private byte _speed;
        #endregion

        #region Public Properties
        public int FleetId { get { return _fleetId; } set { _fleetId = value; } }
        public int VehicleId { get { return _vehicleId; } set { _vehicleId = value; } }
        public byte CmdType { get { return _cmdType; } set { _cmdType = value; } }
        public DateTime? DeviceTime { get { return _deviceTime; } set { _deviceTime = value; } }
        public DateTime? GPSTime { get { return _gpsTime; } set { _gpsTime = value; } }
        
        public decimal Latitude { get { return _latitude; } set { _latitude = value; } }
        public decimal Longitude { get { return _longitude; } set { _longitude = value; } }
        public byte Speed { get { return _speed; } set { _speed = value; } }
        
        public uint RouteScheduleId { get { return _routeScheduleId; } set { _routeScheduleId = value; } }
        public int RouteCheckPointId { get { return _routeCheckPointId; } set { _routeCheckPointId = value; } }
        public int SetpointGroupID { get { return _setpointGroupID; } set { _setpointGroupID = value; } }
        public int SetpointID { get { return _setpointID; } set { _setpointID = value; } }
        public int SetpointNumber { get { return _setpointNumber; } set { _setpointNumber = value; } }
        public long UnitStatus { get { return _unitStatus; } set { _unitStatus = value; } }
        public bool HasServerSideWPGroupAssigned { get { return _hasServerSideWPGroupAssigned; } set { _hasServerSideWPGroupAssigned = value; } }
        public bool IsPacketFromFlash { get { return _isPacketFromFlash; } set { _isPacketFromFlash = value; } }
        public bool AreThereMorePacketsInFlash { get { return _areThereMorePacketsInFlash; } set { _areThereMorePacketsInFlash = value; } }
        public bool InputPacketIsValid { get { return _inputPacketIsValid; } set { _inputPacketIsValid = value; } }
        public GeneralGPPacket GPPacket { get { return _gpp; } }
        public string StreetName { get { return _streetName; } set { _streetName = value; } }
        public bool UnitIsAtUnitSideWP
        {
            get
            {
                bool ret = false;
                if ((_unitStatus & (long)StatusVehicle59.STATUS_WP_AT) == (long)StatusVehicle59.STATUS_WP_AT
                    || (_unitStatus & (long)StatusVehicle59.STATUS_WP_DOCK) == (long)StatusVehicle59.STATUS_WP_DOCK
                    || (_unitStatus & (long)StatusVehicle59.STATUS_WP_HOLDING) == (long)StatusVehicle59.STATUS_WP_HOLDING
                    || (_unitStatus & (long)StatusVehicle59.STATUS_WP_NOGO) == (long)StatusVehicle59.STATUS_WP_NOGO
                    || (_unitStatus & (long)StatusVehicle59.STATUS_WP_WORKSHOP) == (long)StatusVehicle59.STATUS_WP_WORKSHOP)
                    ret = true;
                return ret;
            } 
        }

        public long VehicleKey
        {
            get
            {
                long lRet = 0;
                byte[] bData = null;
                byte[] bConvert = null;
                try
                {
                    bConvert = new byte[8];
                    bData = BitConverter.GetBytes(_fleetId);
                    bConvert[0] = bData[0];
                    bConvert[1] = bData[1];
                    bConvert[2] = bData[2];
                    bConvert[3] = bData[3];
                    bData = BitConverter.GetBytes(_vehicleId);
                    bConvert[4] = bData[0];
                    bConvert[5] = bData[1];
                    bConvert[6] = bData[2];
                    bConvert[7] = bData[3];
                    lRet = BitConverter.ToInt64(bConvert, 0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "VehicleKey (Get)", ex);
                }
                return lRet;
            }
        }
        #endregion

        public ReportDetails(object packet)
        {
            try
            {
                _packet = packet;
                GBVariablePacketExtended59 extVars = null;
                if (packet is RoutePtSetPtGPPacket)
                {
                    _streetName = ((RoutePtSetPtGPPacket)packet).StreetName;

                    _gpp = new GeneralGPPacket((GatewayProtocolPacket)_packet, "dd/MM/yyyy HH:mm:ss");
                    _gpp.mCurrentClock = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mCurrentClock;
                    _gpp.mFixClock = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mFixClock;
                    _gpp.mFix = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mFix;
                    _gpp.mStatus = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mStatus;
                    _gpp.mDistance = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mDistance;
                    _gpp.mExtendedValues = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mExtendedValues;
                    _gpp.mEngineData = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mEngineData;
                    _gpp.mTransportExtraDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mTransportExtraDetails;
                    _gpp.mTrailerTrack = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mTrailerTrack;
                    _gpp.mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)_packet).mExtendedVariableDetails;

                    extVars = ((RoutePtSetPtGPPacket)packet).mExtendedVariableDetails;
                    _fleetId = (int) ((RoutePtSetPtGPPacket)packet).cFleetId;
                    _vehicleId = (int)((RoutePtSetPtGPPacket)packet).iVehicleId;
                    _cmdType = ((RoutePtSetPtGPPacket)packet).cMsgType;
                    if (((RoutePtSetPtGPPacket)packet).mCurrentClock.IsDate(null))
                        _deviceTime = ((RoutePtSetPtGPPacket)packet).mCurrentClock.ToDateTime();
                    else
                        _deviceTime = null;
                    if (((RoutePtSetPtGPPacket)packet).mFixClock.IsDate(null))
                        _gpsTime = ((RoutePtSetPtGPPacket)packet).mFixClock.ToDateTime();
                    else
                        _gpsTime = null;
                    if (((RoutePtSetPtGPPacket)packet).mFix != null)
                    {
                        _latitude = ((RoutePtSetPtGPPacket)packet).mFix.dLatitude;
                        _longitude = ((RoutePtSetPtGPPacket)packet).mFix.dLongitude;
                        _speed = ((RoutePtSetPtGPPacket)packet).mFix.cSpeed;
                    }
                    _isPacketFromFlash = ((RoutePtSetPtGPPacket)packet).bArchivalData;
                    _areThereMorePacketsInFlash = ((RoutePtSetPtGPPacket)packet)._receivedFlashAvailable;
                }
                else if (packet is RoutingModuleGPPacket)
                {
                    _streetName = ((RoutingModuleGPPacket)packet).StreetName;
                    _gpp = new GeneralGPPacket((GatewayProtocolPacket)_packet, "dd/MM/yyyy HH:mm:ss");
                    _gpp.mCurrentClock = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mCurrentClock;
                    _gpp.mFixClock = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mFixClock;
                    _gpp.mFix = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mFix;
                    _gpp.mStatus = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mStatus;
                    _gpp.mDistance = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mDistance;
                    _gpp.mExtendedValues = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mExtendedValues;
                    _gpp.mEngineData = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mEngineData;
                    _gpp.mTransportExtraDetails = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mTransportExtraDetails;
                    _gpp.mTrailerTrack = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mTrailerTrack;
                    _gpp.mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)_packet).mExtendedVariableDetails;
                    
                    extVars = ((RoutingModuleGPPacket)packet).mExtendedVariableDetails;
                    _fleetId = (int)((RoutingModuleGPPacket)packet).cFleetId;
                    _vehicleId = (int)((RoutingModuleGPPacket)packet).iVehicleId;
                    _cmdType = ((RoutingModuleGPPacket)packet).cMsgType;
                    if (((RoutingModuleGPPacket)packet).mCurrentClock.IsDate(null))
                        _deviceTime = ((RoutingModuleGPPacket)packet).mCurrentClock.ToDateTime();
                    else
                        _deviceTime = null;
                    if (((RoutingModuleGPPacket)packet).mFixClock.IsDate(null))
                        _gpsTime = ((RoutingModuleGPPacket)packet).mFixClock.ToDateTime();
                    else
                        _gpsTime = null;

                    if (((RoutingModuleGPPacket)packet).mFix != null)
                    {
                        _latitude = ((RoutingModuleGPPacket)packet).mFix.dLatitude;
                        _longitude = ((RoutingModuleGPPacket)packet).mFix.dLongitude;
                        _speed = ((RoutingModuleGPPacket)packet).mFix.cSpeed;
                    }
                    _isPacketFromFlash = ((RoutingModuleGPPacket)packet).bArchivalData;
                    _areThereMorePacketsInFlash = ((RoutingModuleGPPacket)packet)._receivedFlashAvailable;
                }
                else if (packet is ConfigCustomerInfoGPPacket || packet is ConfigDriverPointsGPPacket
               || packet is ConfigFileGPPacket || packet is ConfigNetworkInfoGPPacket
               || packet is ConfigNewRoutePtGPPacket || packet is ConfigNewSetPtGPPacket
               || packet is ConfigRequestGPPacket || packet is ConfigRouteUpdateGPPacket
               || packet is ConfigScheduleClearGPPacket || packet is ConfigScheduleGPPacket
               || packet is ConfigScheduleInfoGPPacket || packet is ConfigTotalGPPacket
               || packet is ConfigWatchList || packet is DriverPointsConfigError
               || packet is DriverPointsRuleRequest
               || packet is DriverPointsServerTrackingRuleBroken || packet is FirmwareDriverLoginReply
               || packet is GeneralGPPacket || packet is GeneralOutputGPPacket
               || packet is GPSendScriptFile || packet is MailMessagePacket
               || packet is ProgramDownloadPacket || packet is ProgramVerifyPacket
               || packet is UserAckForUnitAlarm)
                {
                    _streetName = ((GeneralGPPacket)packet).StreetName;
                    _gpp = (GeneralGPPacket)packet;
                    extVars = ((GeneralGPPacket)packet).mExtendedVariableDetails;
                    _fleetId = (int)((GeneralGPPacket)packet).cFleetId;
                    _vehicleId = (int)((GeneralGPPacket)packet).iVehicleId;
                    _cmdType = ((GeneralGPPacket)packet).cMsgType;
                    if (((GeneralGPPacket)packet).mCurrentClock.IsDate(null))
                        _deviceTime = ((GeneralGPPacket)packet).mCurrentClock.ToDateTime();
                    else
                        _deviceTime = null;
                    if (((GeneralGPPacket)packet).mFixClock.IsDate(null))
                        _gpsTime = ((GeneralGPPacket)packet).mFixClock.ToDateTime();
                    else
                        _gpsTime = null;

                    if (((GeneralGPPacket)packet).mFix != null)
                    {
                        _latitude = ((GeneralGPPacket)packet).mFix.dLatitude;
                        _longitude = ((GeneralGPPacket)packet).mFix.dLongitude;
                        _speed = ((GeneralGPPacket)packet).mFix.cSpeed;
                    }
                    _isPacketFromFlash = ((GeneralGPPacket)packet).bArchivalData;
                    _areThereMorePacketsInFlash = ((GeneralGPPacket)packet)._receivedFlashAvailable;
                }

                if(extVars != null)
                {
                    _routeScheduleId = extVars.CurrentVehicleRouteScheduleID;
                    _routeCheckPointId = extVars.CurrentRouteCheckPointIndex;
                    _setpointGroupID = extVars.CurrentSetPointGroup;
                    _setpointID = extVars.CurrentSetPointID;
                    _setpointNumber = extVars.CurrentSetPointNumber;
                    _unitStatus = extVars.UnitStatus;
                    _inputPacketIsValid = true;
                }

            }
            catch(System.Exception ex)
            {
                _log.Error(sClassName + "ReportDetails(object packet)", ex);
                _inputPacketIsValid = false;
            }
        }
    }
}

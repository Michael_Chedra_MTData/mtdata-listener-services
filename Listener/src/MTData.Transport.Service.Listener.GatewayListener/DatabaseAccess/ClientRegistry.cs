using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.IO;
using System.Net.Sockets;
using System.Threading;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;
using System.Timers;
using System.Data;
using System.Configuration;
using System.Globalization;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Application.Communication;
using MTData.Transport.Application.Communication.LiveUdpates;
using MTData.Transport.Application.Communication.MsmqInterface;
using MTData.Transport.Application.Communication.Kinesis;
using MTData.Common.Network;
using log4net;
using MTData.Common.Threading;
using MTData.Common.Queues;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    /// <summary>
    /// Records Client details, and allows them to
    /// be notified of activity in a given fleet.
    /// Also allows clients to make "push" requests such as periodic position requests,
    /// alarms, output control, etc
    /// </summary>
    public class ClientRegistry
    {
        private const string sClassName = "Database Access.ClientRegistry.";
        private static ILog _log = LogManager.GetLogger(typeof(ClientRegistry));
        private string _serverTime_DateFormat = "MM/dd/yyyy HH:mm:ss";
        private Hashtable oLastKnowPosition = null;

        public event WriteToConsoleEvent eConsoleEvent;

        public delegate void WriteToConsoleEvent(string sMsg);

        public const double CLIENT_TEST_INTERVAL = 5 * (60 * 1000); // 5 minutes - in milliseconds;
        public const double CLIENT_RESPONSE_INTERVAL = 1 * (60 * 1000); // 1 minute - in milliseconds;

        #region Variables
        private List<int> _showPlaceNameForThisReasonIDList = new List<int>();
        public bool bLogPacketData = false;
        private string _userDefinedDateFormat = null;
        private SortedList mFleetList;
        private SortedList mFromFlashFleetList;
        private SortedList mFleetNonTrackingList;
        private UdpClient mIP;
        private UniversalListenerThread mClientListenerThread;
        private SMSAlerts mSMSAlerts;
        private LiveUpdatesToMsmq _luToMsmq;
        private ECMLiveUpdatesToMsmq _ecmLuToMsmq;

        private Hashtable mECMLiveUpdRegister_FVtoEP;
        private Hashtable mECMLiveUpdRegister_EPtoFV;

        private Hashtable mRouteLiveUpdRegister_FtoEP;
        private Hashtable mRouteLiveUpdRegister_EPtoF;

        private PacketDatabaseInterface mDatabaseInterface;

        public PacketDatabaseInterface DatabaseInterface
        {
            get { return mDatabaseInterface; }
            set
            {
                if (mDatabaseInterface != null)
                {
                    mDatabaseInterface.eSendECMLiveUpdate -= new PacketDatabaseInterface.SendECMLiveUpdateDelegate(mDatabaseInterface_eSendECMLiveUpdate);
                    mDatabaseInterface.eResendPendantAlarm -= new PacketDatabaseInterface.ResendPendantAlarmDelegate(mDatabaseInterface_eResendPendantAlarm);
                }

                mDatabaseInterface = value;
                if (mDatabaseInterface != null)
                {
                    mDatabaseInterface.eSendECMLiveUpdate += new PacketDatabaseInterface.SendECMLiveUpdateDelegate(mDatabaseInterface_eSendECMLiveUpdate);
                    mDatabaseInterface.eResendPendantAlarm += new PacketDatabaseInterface.ResendPendantAlarmDelegate(mDatabaseInterface_eResendPendantAlarm);
                }
            }
        }

        private MySQLInterface oMySQL = null;
        private List<IPEndPoint> mClientList;
        private List<IPEndPoint> mClientIsAlive;
        private Hashtable mClientVersion;
        private Hashtable oTrailerESNs;
        public Hashtable mFleetInterfaceServers = null;
        private MTData.Transport.Application.Communication.LiveUdpates.PacketCreater _updateWriter = null;
        private PacketInterpreter _packetInterpreter = null;
        private DateTime dtTrailerListTimestamp = System.DateTime.Now;
        private EnhancedThread oProcessClientMsgThread = null;
        private StandardQueue oClientMsgQueue = null;
        private EnhancedThread _clientAliveThread = null;

        /// <summary>
        /// This object is used to synchronise access to the client list and clientisAlive list.
        /// If you use the syncroots from the individual lists, you can get a 
        /// </summary>
        private object UDPClientListSync = new object();

        private object ClientIsAliveSync = new object();
        public string sLogFilePath;
        private bool bLogging;
        private bool bLogClientUpdateData = false;
        private IEmailHelper emailHelper;

        private LiveUpdateToKinesis _kinesisUpdates;

        public bool MessageLogging
        {
            get { return bLogging; }
            set
            {
                if (sLogFilePath != null)
                {
                    bLogging = value;
                }
            }
        }

        private bool bRestrictUpdates = true;

        public bool RestrictUpdates
        {
            get { return bRestrictUpdates; }
            set { bRestrictUpdates = value; }
        }

        #endregion

        public List<ulong> LiveUpdateEnqueueCount
        {
            get
            {
                if (_luToMsmq != null)
                {
                    return _luToMsmq.EnqueueCount;
                }
                return null;
            }
        }

        public List<long> LiveUpdateKinesisInsertCount
        {
            get
            {
                if (_kinesisUpdates != null)
                {
                    return _kinesisUpdates.InsertCount;
                }
                return null;
            }
        }

        public List<byte> ConcreteFleets { get; set; }
        private bool IsConcreteFleet(byte fleet)
        {
            if (ConcreteFleets != null && ConcreteFleets.Contains(fleet))
            {
                return true;
            }
            return false;
        }

        #region Constructor
        public ClientRegistry(string address, int port, Hashtable FleetInterfaceServers, MySQLInterface.cMySQLInterfaceParams mySqlParams, string userDefinedDateFormat, string serverTime_DateFormat, IEmailHelper emailHelper)
        {
            try
            {
                _luToMsmq = (LiveUpdatesToMsmq)System.Configuration.ConfigurationManager.GetSection("LiveUpdatesToMsmq");
            }
            catch (Exception)
            {
                _luToMsmq = null;
            }
            try
            {
                _kinesisUpdates = (LiveUpdateToKinesis)System.Configuration.ConfigurationManager.GetSection("LiveUpdateToKinesis");
            }
            catch (Exception exp)
            {
                _kinesisUpdates = null;
            }
            try
            {
                _ecmLuToMsmq = (ECMLiveUpdatesToMsmq)System.Configuration.ConfigurationManager.GetSection("ECMLiveUpdatesToMsmq");
            }
            catch (Exception)
            {
                _ecmLuToMsmq = null;
            }

            try
            {
                if (ConfigurationManager.AppSettings["LogClientUpdateData"] == "true")
                    bLogClientUpdateData = true;
                else
                    bLogClientUpdateData = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ClientRegistry(string address, int port, Hashtable FleetInterfaceServers, cMySQLInterface.cMySQLInterfaceParams mySqlParams, string userDefinedDateFormat, string serverTime_DateFormat)()", ex);
            }


            try
            {
                if (mSMSAlerts == null)
                    mSMSAlerts = (SMSAlerts)System.Configuration.ConfigurationManager.GetSection("SMSAlerts");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ClientRegistry(string address, int port, Hashtable FleetInterfaceServers, cMySQLInterface.cMySQLInterfaceParams mySqlParams, string userDefinedDateFormat, string serverTime_DateFormat)()", ex);
                mSMSAlerts = null;
            }
            SetupPlaceNameForThisReasonIDList();
            mFleetInterfaceServers = FleetInterfaceServers;
            oLastKnowPosition = Hashtable.Synchronized(new Hashtable());
            _userDefinedDateFormat = userDefinedDateFormat;
            if (serverTime_DateFormat != null)
                _serverTime_DateFormat = serverTime_DateFormat;

            bLogging = false;
            mFleetList = SortedList.Synchronized(new SortedList());
            mFromFlashFleetList = SortedList.Synchronized(new SortedList());
            mFleetNonTrackingList = SortedList.Synchronized(new SortedList());

            mECMLiveUpdRegister_FVtoEP = Hashtable.Synchronized(new Hashtable());
            mECMLiveUpdRegister_EPtoFV = Hashtable.Synchronized(new Hashtable());

            mRouteLiveUpdRegister_FtoEP = Hashtable.Synchronized(new Hashtable());
            mRouteLiveUpdRegister_EPtoF = Hashtable.Synchronized(new Hashtable());

            if (address == null)
            {
                mIP = new UdpClient(port);
            }
            else
            {
                mIP = new UdpClient(new IPEndPoint(IPAddress.Parse(address), port));
            }

            this.emailHelper = emailHelper;

            _updateWriter = new MTData.Transport.Application.Communication.LiveUdpates.PacketCreater();
            _updateWriter.OutboundPacket += new MTData.Transport.Application.Communication.LiveUdpates.PacketCreater.OutboundPacketDelegate(_updateWriter_OutboundPacket);

            _packetInterpreter = new PacketInterpreter();
            _packetInterpreter.PendentAlarmAckEvent += new PacketInterpreter.PendentAlarmAckPacketDelegate(_packetInterpreter_PendentAlarmAck);
            _packetInterpreter.VehicleActiveStateChange += new PacketInterpreter.VehicleActiveStateChangedDelegate(_packetInterpreter_VehicleActiveStateChange);
            _packetInterpreter.VehicleDriverChange += new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.VehicleDriverChangePacketDelegate(_packetInterpreter_VehicleDriverChange);
            _packetInterpreter.VehicleMDTMessage += new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.VehicleMDTMessageDelegate(_packetInterpreter_VehicleMDTMessage);
            _packetInterpreter.TimeZoneChanged += new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.TimeZoneChangedBytePacketDelegate(_packetInterpreter_TimeZoneChanged);
            _packetInterpreter.TimeZoneChangedDecodeError += new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.TimeZoneChangedDecodeErrorDelegate(_packetInterpreter_TimeZoneChangedDecodeError);
            _packetInterpreter.RouteStateUpdateAdd += new PacketInterpreter.RouteStateUpdateRegisterDelegate(_packetInterpreter_RouteStateUpdateAdd);
            _packetInterpreter.RouteStateUpdateRemove += new PacketInterpreter.RouteStateUpdateRegisterDelegate(_packetInterpreter_RouteStateUpdateRemove);
            _packetInterpreter.ReasonCodeGroupUpdate += new PacketInterpreter.ReasonCodeGroupUpdateDelegate(_packetInterpreter_ReasonCodeGroupUpdate);
            _packetInterpreter.ECMLiveUpdateAdd += new PacketInterpreter.ECMLiveUpdateRegisterDelegate(_packetInterpreter_ECMLiveUpdateAdd);
            _packetInterpreter.ECMLiveUpdateRemove += new PacketInterpreter.ECMLiveUpdateRegisterDelegate(_packetInterpreter_ECMLiveUpdateRemove);
            _packetInterpreter.DriverCreated += new PacketInterpreter.DriverUpdateDelegate(_packetInterpreter_DriverUpdated);
            _packetInterpreter.DriverDeleted += new PacketInterpreter.DriverUpdateDelegate(_packetInterpreter_DriverUpdated);
            _packetInterpreter.DriverUpdated += new PacketInterpreter.DriverUpdateDelegate(_packetInterpreter_DriverUpdated);
            _packetInterpreter.DriverFatigueUpdate += new PacketInterpreter.DriverUpdateDelegate(_packetInterpreter_DriverFatigueUpdated);
            _packetInterpreter.ExtendedHistoryRequest += new PacketInterpreter.ExtendedHistoryRequestDelegate(_packetInterpreter_ExtendedHistoryRequest);
            _packetInterpreter.ExtendedHistoryRequestComplete += new PacketInterpreter.ExtendedHistoryRequestCompleteDelegate(_packetInterpreter_ExtendedHistoryRequestComplete);
            _packetInterpreter.DvrAlarm += new PacketInterpreter.DvrAlarmDelegate(_packetInterpreter_DvrAlarm);
            _packetInterpreter.DvrAlarmContent += new PacketInterpreter.DvrAlarmContentDelegate(_packetInterpreter_DvrAlarmContent);
            _packetInterpreter.DvrError += new PacketInterpreter.DvrErrorDelegate(_packetInterpreter_DvrError);
            _packetInterpreter.DvrRequestContent += new PacketInterpreter.DvrRequestContentDelegate(_packetInterpreter_DvrRequestContent);
            _packetInterpreter.DvrStatus += new PacketInterpreter.DvrStatusDelegate(_packetInterpreter_DvrStatus);

            mClientList = new List<IPEndPoint>();
            mClientIsAlive = new List<IPEndPoint>();
            mClientVersion = Hashtable.Synchronized(new Hashtable());
            // Add a Push request listener - for DB front end
            UniversalListenerDelegate myPush = new UniversalListenerDelegate(this.HandleClientMessage);
            mClientListenerThread = new UniversalListenerThread(mIP, myPush, "Client Listener");
            mClientListenerThread.eConsoleEvent += new MTData.Transport.Service.Listener.GatewayListener.Network_Threads.UniversalListenerThread.WriteToConsoleEvent(subThread_eConsoleEvent);
            mClientListenerThread.eRemoteConnectionClosed += new UniversalListenerThread.RemoteConnectionClosedEvent(mClientListenerThread_eRemoteConnectionClosed);
            mClientListenerThread.bLogPacketData = bLogPacketData;
            mClientListenerThread.eSocketConnectionClosed += new UniversalListenerConnectionClosed(mClientListenerThread_eSocketConnectionClosed);

            if ((mySqlParams != null) && (mySqlParams.ConnectionString != null) && (mySqlParams.ConnectionString != ""))
            {
                oMySQL = new MySQLInterface(mySqlParams);
                oMySQL.Start();
            }

            try
            {
                oClientMsgQueue = new StandardQueue();
                EnhancedThread.EnhancedThreadStart ProcessClientMsgThreadStart = new EnhancedThread.EnhancedThreadStart(ProcessClientMsgsThread);
                oProcessClientMsgThread = new EnhancedThread(ProcessClientMsgThreadStart, null);
                oProcessClientMsgThread.Name = "Process Client Data";
                oProcessClientMsgThread.Start();
            }
            catch (System.Exception)
            {
            }

            try
            {
                EnhancedThread.EnhancedThreadStart KeepAliveThread = new EnhancedThread.EnhancedThreadStart(ClientAliveThread);
                _clientAliveThread = new EnhancedThread(KeepAliveThread, null);
                _clientAliveThread.Name = "Client Alive Thread";
                _clientAliveThread.Start();
            }
            catch (System.Exception)
            {
            }
        }

        public void Dispose(bool disposing)
        {
            int threadTimeout = 0;
            try
            {
                // Un-hook packet interpreter events.
                _packetInterpreter.PendentAlarmAckEvent -= new PacketInterpreter.PendentAlarmAckPacketDelegate(_packetInterpreter_PendentAlarmAck);
                _packetInterpreter.VehicleActiveStateChange -= new PacketInterpreter.VehicleActiveStateChangedDelegate(_packetInterpreter_VehicleActiveStateChange);
                _packetInterpreter.VehicleDriverChange -= new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.VehicleDriverChangePacketDelegate(_packetInterpreter_VehicleDriverChange);
                _packetInterpreter.VehicleMDTMessage -= new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.VehicleMDTMessageDelegate(_packetInterpreter_VehicleMDTMessage);
                _packetInterpreter.TimeZoneChanged -= new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.TimeZoneChangedBytePacketDelegate(_packetInterpreter_TimeZoneChanged);
                _packetInterpreter.TimeZoneChangedDecodeError -= new MTData.Transport.Application.Communication.LiveUdpates.PacketInterpreter.TimeZoneChangedDecodeErrorDelegate(_packetInterpreter_TimeZoneChangedDecodeError);
                _packetInterpreter.RouteStateUpdateAdd -= new PacketInterpreter.RouteStateUpdateRegisterDelegate(_packetInterpreter_RouteStateUpdateAdd);
                _packetInterpreter.RouteStateUpdateRemove -= new PacketInterpreter.RouteStateUpdateRegisterDelegate(_packetInterpreter_RouteStateUpdateRemove);
                _packetInterpreter.ReasonCodeGroupUpdate -= new PacketInterpreter.ReasonCodeGroupUpdateDelegate(_packetInterpreter_ReasonCodeGroupUpdate);
                _packetInterpreter.ECMLiveUpdateAdd -= new PacketInterpreter.ECMLiveUpdateRegisterDelegate(_packetInterpreter_ECMLiveUpdateAdd);
                _packetInterpreter.ECMLiveUpdateRemove -= new PacketInterpreter.ECMLiveUpdateRegisterDelegate(_packetInterpreter_ECMLiveUpdateRemove);
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mClientListenerThread != null)
                {
                    mClientListenerThread.eConsoleEvent -= new UniversalListenerThread.WriteToConsoleEvent(subThread_eConsoleEvent);
                    mClientListenerThread.eRemoteConnectionClosed -= new UniversalListenerThread.RemoteConnectionClosedEvent(mClientListenerThread_eRemoteConnectionClosed);
                    mClientListenerThread.StopThread();
                    threadTimeout = 0;
                    while (threadTimeout < 20 && (!mClientListenerThread.Stopped))
                    {
                        Thread.Sleep(50);
                        threadTimeout++;
                    }
                    mClientListenerThread = null;
                    mIP.Close();
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (oProcessClientMsgThread != null)
                {
                    oProcessClientMsgThread.Stop();
                    threadTimeout = 0;
                    while (threadTimeout < 20 && oProcessClientMsgThread.Running)
                    {
                        Thread.Sleep(50);
                        threadTimeout++;
                    }
                    if (oProcessClientMsgThread != null && oProcessClientMsgThread.Running)
                        oProcessClientMsgThread.Abort();
                    oProcessClientMsgThread = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (_clientAliveThread != null)
                {
                    _clientAliveThread.Stop();
                    threadTimeout = 0;
                    while (threadTimeout < 20 && _clientAliveThread.Running)
                    {
                        Thread.Sleep(50);
                        threadTimeout++;
                    }
                    if (_clientAliveThread != null && _clientAliveThread.Running)
                        _clientAliveThread.Abort();
                    _clientAliveThread = null;
                }
            }
            catch (System.Exception)
            {
            }

            if (oMySQL != null)
            {
                try
                {
                    oMySQL.Stop();
                    oMySQL.Dispose();
                    oMySQL = null;
                }
                catch (System.Exception)
                {
                }
            }

            if (mClientVersion != null)
            {
                lock (mClientVersion.SyncRoot)
                {
                    mClientVersion.Clear();
                }
                mClientVersion = null;
            }

            if (oTrailerESNs != null)
            {
                lock (oTrailerESNs.SyncRoot)
                {
                    oTrailerESNs.Clear();
                }
                oTrailerESNs = null;
            }
            if (mDatabaseInterface != null)
            {
                mDatabaseInterface.eSendECMLiveUpdate -= new PacketDatabaseInterface.SendECMLiveUpdateDelegate(mDatabaseInterface_eSendECMLiveUpdate);
                mDatabaseInterface.eResendPendantAlarm -= new PacketDatabaseInterface.ResendPendantAlarmDelegate(mDatabaseInterface_eResendPendantAlarm);
                mDatabaseInterface.Dispose();
                mDatabaseInterface = null;
            }
            if (_updateWriter != null)
            {
                _updateWriter = null;
            }

            if (mECMLiveUpdRegister_FVtoEP != null)
            {
                lock (mECMLiveUpdRegister_FVtoEP.SyncRoot)
                    mECMLiveUpdRegister_FVtoEP.Clear();
                mECMLiveUpdRegister_FVtoEP = null;
            }
            if (mECMLiveUpdRegister_EPtoFV != null)
            {
                lock (mECMLiveUpdRegister_EPtoFV.SyncRoot)
                    mECMLiveUpdRegister_EPtoFV.Clear();
                mECMLiveUpdRegister_EPtoFV = null;
            }

            if (mRouteLiveUpdRegister_FtoEP != null)
            {
                lock (mRouteLiveUpdRegister_FtoEP.SyncRoot)
                    mRouteLiveUpdRegister_FtoEP.Clear();
                mRouteLiveUpdRegister_FtoEP = null;
            }
            if (mRouteLiveUpdRegister_EPtoF != null)
            {
                lock (mRouteLiveUpdRegister_EPtoF.SyncRoot)
                    mRouteLiveUpdRegister_EPtoF.Clear();
                mRouteLiveUpdRegister_EPtoF = null;
            }
            if (_kinesisUpdates != null)
            {
                _kinesisUpdates.Dispose();
            }
            if (_luToMsmq != null)
            {
                _luToMsmq.Dispose();
            }
            _luToMsmq = null;
            if (_ecmLuToMsmq != null)
                _ecmLuToMsmq.Dispose();
            _ecmLuToMsmq = null;

            _showPlaceNameForThisReasonIDList = null;
        }

        #endregion

        #region Client Message Handling Code

        public void HandleClientMessage(IPEndPoint endPoint, byte[] contents)
        {
            ClientMsg oMsg = null;

            try
            {
                if (oClientMsgQueue != null)
                {
                    oMsg = new ClientMsg(endPoint, contents);
                    lock (oClientMsgQueue.SyncRoot)
                        oClientMsgQueue.Enqueue(oMsg);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "HandleClientMessage(IPEndPoint endPoint, byte[] contents)", ex);
            }
        }

        public object ProcessClientMsgsThread(EnhancedThread sender, object data)
        {
            ClientMsg oMsg = null;

            #region Write thread start message to log

            try
            {
                _log.Info("Client message queue processing thread started.");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ProcessClientMsgsThread(EnhancedThread sender, object data)", ex);
            }

            #endregion

            while (!sender.Stopping)
            {
                #region Thread Loop Code

                try
                {
                    lock (oClientMsgQueue.SyncRoot)
                    {
                        if (oClientMsgQueue.Count > 0)
                            oMsg = (ClientMsg)oClientMsgQueue.Dequeue();
                        else
                            oMsg = null;
                    }
                    if (oMsg != null)
                    {
                        ProcessClientMessage(oMsg.EndPoint, oMsg.Data);
                    }
                    else
                        Thread.Sleep(100);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "ProcessClientMsgsThread(EnhancedThread sender, object data)", ex);
                }

                #endregion
            }

            #region Write thread stopped message to log

            try
            {
                _log.Info("Client message queue processing thread stopped.");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ProcessClientMsgsThread(EnhancedThread sender, object data)", ex);
            }

            #endregion

            return null;
        }

        public void ProcessClientMessage(IPEndPoint endPoint, byte[] contents)
        {
            string messageText = System.Text.Encoding.ASCII.GetString(contents);
            char[] delimiter = { ',' };
            string sFleetMsg = "";


            try
            {
                if (endPoint != null && contents != null)
                {
                    if (contents.Length > 0)
                    {
                        if (contents[0] == (byte)0x01)
                        {
                            if (bLogClientUpdateData)
                                _log.Info("Client Message : " + System.BitConverter.ToString(contents));

                            _packetInterpreter.ProcessPacket(endPoint, contents);
                        }
                        else
                        {
                            #region Process Text Message

                            messageText = System.Text.Encoding.ASCII.GetString(contents);
                            if (bLogClientUpdateData)
                                _log.Info("Client Message : " + messageText);

                            if (messageText.StartsWith(PacketConstants.COMMAND_STD_PING_REQUEST))
                            {
                                //only send if endpoint is in our list of clients - after listner restart, client keep sending ping, 
                                //and if we send a pong they won't timeout and re connect
                                bool sendPong = false;
                                lock (UDPClientListSync)
                                {
                                    if (mClientList.Contains(endPoint))
                                    {
                                        sendPong = true;
                                    }
                                }
                                if (sendPong)
                                {
                                    byte[] bData = System.Text.ASCIIEncoding.ASCII.GetBytes(PacketConstants.COMMAND_STD_PING_RESPONSE + "                              ");
                                    mClientListenerThread.Send(bData, bData.Length, endPoint);
                                }
                            }
                            else if (messageText.StartsWith("Connect,"))
                            {
                                try
                                {
                                    #region Connection

                                    int pos = 0;
                                    messageText = messageText.Remove(0, 8); // Remove the word "Connect,"
                                    string[] fleets = messageText.Split(delimiter);
                                    uint[] desiredFleets = new uint[fleets.Length];
                                    foreach (string fleetName in fleets)
                                    {
                                        sFleetMsg += fleetName + ", ";
                                        desiredFleets[pos++] = Convert.ToUInt16(fleetName);
                                    }

                                    if (sFleetMsg.Length > 2)
                                    {
                                        sFleetMsg = sFleetMsg.Substring(0, sFleetMsg.Length - 2);
                                    }

                                    AddClient(0, endPoint, desiredFleets, false, false);

                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'Connect' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Requested updates for fleets : " + sFleetMsg);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Client Connection", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ConnectV,"))
                            {
                                try
                                {
                                    #region Connection

                                    int pos = 0;
                                    messageText = messageText.Replace("ConnectV,", "");
                                    string[] sSplit = messageText.Split(delimiter);
                                    double versionMajorMinor = double.Parse(sSplit[0].Replace("_", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                                    string[] fleets = new string[sSplit.Length - 1];
                                    uint[] desiredFleets = new uint[fleets.Length];

                                    for (int X = 1; X < sSplit.Length; X++)
                                    {
                                        sFleetMsg += sSplit[X] + ", ";
                                        desiredFleets[pos++] = Convert.ToUInt16(sSplit[X]);
                                    }
                                    if (sFleetMsg.Length > 2)
                                    {
                                        sFleetMsg = sFleetMsg.Substring(0, sFleetMsg.Length - 2);
                                    }

                                    AddClient(versionMajorMinor, endPoint, desiredFleets, false, false);

                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'Connect' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Requested updates for fleets : " + sFleetMsg);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Client Version Connection", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ConnectWithHistoryV,"))
                            {
                                try
                                {
                                    #region Connection

                                    int pos = 0;
                                    messageText = messageText.Replace("ConnectWithHistoryV,", "");
                                    string[] sSplit = messageText.Split(delimiter);
                                    double versionMajorMinor = double.Parse(sSplit[0].Replace("_", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                                    string[] fleets = new string[sSplit.Length - 1];
                                    uint[] desiredFleets = new uint[fleets.Length];

                                    for (int X = 1; X < sSplit.Length; X++)
                                    {
                                        sFleetMsg += sSplit[X] + ", ";
                                        desiredFleets[pos++] = Convert.ToUInt16(sSplit[X]);
                                    }
                                    if (sFleetMsg.Length > 2)
                                    {
                                        sFleetMsg = sFleetMsg.Substring(0, sFleetMsg.Length - 2);
                                    }

                                    AddClient(versionMajorMinor, endPoint, desiredFleets, true, false);

                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'ConnectWithHistory' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Requested updates for fleets : " + sFleetMsg);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Client Version Connection", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ConnectNonTrackingV,"))
                            {
                                try
                                {
                                    #region Connection

                                    int pos = 0;
                                    messageText = messageText.Replace("ConnectNonTrackingV,", "");
                                    string[] sSplit = messageText.Split(delimiter);
                                    double versionMajorMinor = double.Parse(sSplit[0].Replace("_", System.Globalization.CultureInfo.CurrentCulture.NumberFormat.NumberDecimalSeparator));
                                    string[] fleets = new string[sSplit.Length - 1];
                                    uint[] desiredFleets = new uint[fleets.Length];

                                    for (int X = 1; X < sSplit.Length; X++)
                                    {
                                        sFleetMsg += sSplit[X] + ", ";
                                        desiredFleets[pos++] = Convert.ToUInt16(sSplit[X]);
                                    }
                                    if (sFleetMsg.Length > 2)
                                    {
                                        sFleetMsg = sFleetMsg.Substring(0, sFleetMsg.Length - 2);
                                    }

                                    AddClient(versionMajorMinor, endPoint, desiredFleets, true, true);

                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'ConnectWithHistory' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Requested updates for fleets : " + sFleetMsg);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Client Version Connection", ex1);
                                }
                            }
                            else if (messageText.StartsWith("RefreshSWPList"))
                            {
                                // Refresh the way point list for ServerProcessedWaypoints.
                                try
                                {
                                    #region ServerProcessedWaypoints

                                    mDatabaseInterface.UpdateDataSets();

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Refresh Server Processed WPs", ex1);
                                }
                            }
                            else if (messageText.ToUpper().StartsWith("ALIVE"))
                            {
                                try
                                {
                                    lock (this.ClientIsAliveSync)
                                    {
                                        if (!mClientIsAlive.Contains(endPoint))
                                        {
                                            mClientIsAlive.Add(endPoint);
                                        }
                                    }
                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'Alive' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Client Alive Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ROUTE2UPDATE") || messageText.IndexOf("DATAMODEL:CUSTOM:ROUTE2VEHICLESCHEDULEUPDATE", 0) > 0)
                            {
                                try
                                {
                                    // Message Format : ROUTE2UPDATE;fleetID,routeID,action
                                    mDatabaseInterface.RefreshRouteData();
                                }
                                catch (System.Exception ex)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Route Update Msg", ex);
                                }
                            }
                            else if (messageText.StartsWith("ConfigCheck,"))
                            {
                                try
                                {
                                    #region Units in given fleet should check their configs

                                    // Message format - "ConfigCheck,fleet,Vehicle/Schedule/Config/Set/Route/DriverPointsRules/DriverPointsGraphs,number"
                                    messageText = messageText.Remove(0, 12);
                                    string[] parameters = messageText.Split(delimiter);
                                    if (parameters.Length == 3 || parameters.Length == 4)
                                    {
                                        if ((parameters[1] == "Route") && mDatabaseInterface.SupportsNewRoutes)
                                        {
                                            if (bLogPacketData)
                                                WriteToConsole("Received Client 'Force Routing Refresh' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                                            mDatabaseInterface.RefreshRouteData();
                                        }
                                        else if (parameters[1] == "SetPoint")
                                        {
                                            if (bLogPacketData)
                                                WriteToConsole("Received Client 'Setpoint Update' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                                            // Refresh the cached set point data
                                            mDatabaseInterface.UpdateDataSets();
                                            // Set config check flags for units running the setpoint group
                                            mDatabaseInterface.SendConfigChangeToUnitsInFleet(parameters);
                                            // Send out the set point change message to connected clients that are interested in the group.
                                            NotifyClientsOfSetPointChange(parameters[0], parameters[2], parameters[3]);
                                        }
                                        else if (parameters[1] == "DriverPointsGraphs")
                                        {
                                            if (bLogPacketData)
                                                WriteToConsole("Received Client DriverPointsGraphs Config Check' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                                            mDatabaseInterface.UpdateDataSets();
                                            mDatabaseInterface.SendConfigChangeToUnitsInFleet(parameters);
                                            NotifyClientsOfConfigChange(parameters[0]);
                                        }
                                        else if (parameters[1] == "DriverPointsRules")
                                        {
                                            if (bLogPacketData)
                                                WriteToConsole("Received Client DriverPointsRules Config Check' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                                            // Refresh the cached set point data
                                            mDatabaseInterface.UpdateDataSets();
                                            mDatabaseInterface.SendConfigChangeToUnitsInFleet(parameters);
                                            NotifyClientsOfConfigChange(parameters[0]);
                                        }
                                        else
                                        {
                                            if (bLogPacketData)
                                                WriteToConsole("Received Client 'Force Fleet Config Check' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                                            mDatabaseInterface.UpdateDataSets();
                                            mDatabaseInterface.SendConfigChangeToUnitsInFleet(parameters);
                                            NotifyClientsOfConfigChange(parameters[0]);
                                        }
                                    }

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Config Change Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("VEHICLE_DRIVERCHANGED"))
                            {
                                #region Driver to vehicle assignment has changed

                                // messageText = VEHICLE_DRIVERCHANGED,[FleetID],[VehicleID]
                                _packetInterpreter.ProcessPacket(null, messageText);

                                #endregion
                            }
                            else if (messageText.StartsWith("VEHICLE_MDTMESSAGE"))
                            {
                                #region Send a message to the MDT

                                // messageText = VEHICLE_MDTMESSAGE_[FleetID]_[VehicleID]_[Message]
                                _packetInterpreter.ProcessPacket(null, messageText);

                                #endregion
                            }
                            else if (messageText.StartsWith("Update,"))
                            {
                                try
                                {
                                    #region Periodic Updates

                                    messageText = messageText.Remove(0, 7);
                                    //Client wants periodic updates for a certain unit
                                    // Format for these messages: 
                                    //"Update",<fleet>,<unit>,<repetitions>,<interval>
                                    if (mDatabaseInterface != null)
                                    {
                                        string[] numbers = messageText.Split(delimiter);
                                        if (numbers.Length == 4)
                                        {
                                            string msg =
                                                mDatabaseInterface.SendPositionRequestToUnit(Convert.ToByte(numbers[0]),
                                                                                             Convert.ToUInt32(numbers[1]),
                                                                                             Convert.ToUInt32(numbers[2]),
                                                                                             Convert.ToUInt32(numbers[3]));

                                            //	if (msg != null) mParentForm.mStatusBar.Text = msg;
                                        }
                                        else
                                        {
                                            //	mParentForm.mStatusBar.Text = "Incorrect UPDATE arguments";
                                        }
                                    }
                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'Request Rapid Updates' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Rapid Update Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("History,"))
                            {
                                try
                                {
                                    #region GPS History Request

                                    // Format for these messages: 
                                    //"History",<fleet>,<unit>
                                    messageText = messageText.Remove(0, 8); // Strip the word
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length == 2)
                                    {
                                        string msg =
                                            mDatabaseInterface.SendHistoryRequestToUnit(Convert.ToByte(numbers[0]),
                                                                                        Convert.ToUInt32(numbers[1]));

                                        //	if (msg != null) mParentForm.mStatusBar.Text = msg
                                    }
                                    else
                                    {
                                        //	mParentForm.mStatusBar.Text = "Incorrect HISTORY arguments";
                                    }
                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'Force History Unload' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - GPS History Request Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("AccidentBuffer,"))
                            {
                                try
                                {
                                    #region Accident Buffer Upload Request

                                    // Format for these messages: 
                                    //"AccidentBuffer",<fleet>,<unit>,<bufferIndex>
                                    messageText = messageText.Remove(0, 15); // Strip the word
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length == 3)
                                        mDatabaseInterface.SendAccidentUploadRequest(Convert.ToByte(numbers[0]), Convert.ToUInt32(numbers[1]), Convert.ToInt32(numbers[2]));
                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'Upload Accident Buffer' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Unload Accident Buffer Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ClientBroadcast,"))
                            {
                                try
                                {
                                    #region Broadcast message.. this message should be

                                    // Format for these messages: 
                                    //"Broadcast",<fleets (1:2:3:4)>,Broadcast Message
                                    messageText = messageText.Remove(0, 16); // Strip the word
                                    int index = messageText.IndexOf(",");
                                    if (index >= 0)
                                    {
                                        string fleets = messageText.Substring(0, index);
                                        string[] fleetIDList = fleets.Split(':');
                                        uint[] fleetIDs = new uint[fleetIDList.Length];
                                        for (int loop = 0; loop < fleetIDList.Length; loop++)
                                            fleetIDs[loop] = UInt32.Parse(fleetIDList[loop]);

                                        string broadcastMessage = messageText.Substring(index + 1, messageText.Length - index - 1);
                                        SendClientBroadcastMessage(endPoint, fleetIDs, broadcastMessage);
                                    }
                                    if (bLogPacketData)
                                        WriteToConsole("Received Client 'ClientBroadcast' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Client Broadcast Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("Outputs,"))
                            {
                                try
                                {
                                    #region Drive device outputs

                                    messageText = messageText.Remove(0, 8); // Strip the word
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length == 4)
                                    {
                                        // Format for these messages: 
                                        // "Outputs",<fleet>,<unit>,OutputID,OutputState
                                        if (Convert.ToUInt32(numbers[3]) == 1)
                                            mDatabaseInterface.SendOutputRequestToUnit(Convert.ToByte(numbers[0]), Convert.ToUInt32(numbers[1]), Convert.ToInt32(numbers[2]), true);
                                        else
                                            mDatabaseInterface.SendOutputRequestToUnit(Convert.ToByte(numbers[0]), Convert.ToUInt32(numbers[1]), Convert.ToInt32(numbers[2]), false);
                                    }

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Drive Device Output Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'Activate Output 1' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("UpdateWatchList,"))
                            {
                                try
                                {
                                    #region Add or remove a unit from the watch list

                                    string[] numbers = messageText.Split(delimiter);
                                    string sErr = null;
                                    if (numbers.Length >= 4)
                                    {
                                        // Format for these messages: 
                                        // "UpdateWatchList",<fleet>,<unit>,<Add or Remove>
                                        if (numbers[3] == "1")
                                            sErr = mDatabaseInterface.AddUnitToWatchList(Convert.ToByte(numbers[1]), Convert.ToUInt32(numbers[2]));
                                        else
                                            sErr = mDatabaseInterface.RemoveUnitFromWatchList(Convert.ToByte(numbers[1]), Convert.ToUInt32(numbers[2]));

                                        if (sErr == null)
                                            NotifyClientsOfWatchListChange(numbers[1], messageText);
                                        else
                                        {
                                            _log.Info(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Update Watch List - Error : " + sErr);
                                        }
                                    }

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Drive Device Output Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'Activate Output 1' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("Mail,"))
                            {
                                try
                                {
                                    #region MiniMail Message

                                    // Format for these messages: 
                                    //"Mail",<fleet>,<unit>;[<unit>];[<unit>],
                                    // <sender>,<urgent>,<time>,<subject>,<message>
                                    // where the comma char is actually character 0x02
                                    // First split into 0x02-separated params:
                                    messageText = messageText.Remove(0, 5);
                                    char[] mailItemDelimiter = { (char)0x02 };
                                    string[] parameters = messageText.Split(mailItemDelimiter);
                                    if (parameters.Length < 7) return; // too few params
                                    // The unit list should now be in the second string,
                                    // split that into individual units:
                                    char[] unitDelimiter = { ';' };
                                    string[] units = parameters[1].Split(unitDelimiter);
                                    if (units.Length < 1) return; // too few units
                                    mDatabaseInterface.SendMiniMailToUnitsInFleet(
                                        Convert.ToByte(parameters[0]), // FleetID
                                        units, // units
                                        parameters[2], // sender
                                        Convert.ToBoolean(parameters[3]), // urgent
                                        parameters[4], // time
                                        parameters[5], // subject
                                        parameters[6]); // message

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Mini Mail Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'Mini-Mail' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("Attachment,"))
                            {
                                try
                                {
                                    #region MiniMail Attachment

                                    // Format for these messages: 
                                    //"Attachment",<fleet>,<unit>,
                                    // <sender>,<urgent>,<time>,<subject>,<message>
                                    // where the comma char is actually character 0x02
                                    // First split into 0x02-separated params:
                                    messageText = messageText.Remove(0, 11);
                                    char[] mailItemDelimiter = { (char)0x02 };
                                    string[] parameters = messageText.Split(mailItemDelimiter);
                                    if (parameters.Length < 7) return; // too few params
                                    // The unit list should now be in the second string,
                                    // split that into individual units:
                                    char[] unitDelimiter = { ';' };
                                    string[] units = parameters[1].Split(unitDelimiter);
                                    if (units.Length < 1) return; // too few units
                                    mDatabaseInterface.SendMiniMailToUnitsInFleet(
                                        Convert.ToByte(parameters[0]), // FleetID
                                        units, // units
                                        parameters[2], // sender
                                        Convert.ToBoolean(parameters[3]), // urgent
                                        parameters[4], // time
                                        parameters[5], // subject
                                        parameters[6]); // message

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents)  - Mini Mail Attachment Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'Mini-Mail Attachment' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("Disconnect")) // This is a sign-off message
                            {
                                try
                                {
                                    #region Disconnection

                                    if (bLogPacketData)
                                        WriteToConsole("Client: " + endPoint.ToString() + " initiated disconnection.");
                                    RemoveClient(endPoint);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Clietn Disconnect Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'Disconnect' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("ResetUnit")) // This is a request to send out a reset to a unit.
                            {
                                try
                                {
                                    #region Reset a unit.

                                    string[] numbers = messageText.Split(delimiter);
                                    mDatabaseInterface.SendResetRequestToUnit(Convert.ToByte(numbers[1]), Convert.ToUInt32(numbers[2]));

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Reset Unit Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'ResetUnit' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("A,") || messageText.StartsWith("a,")) // This is a request to send out a reset to a unit.
                            {
                                try
                                {
                                    #region Set/Unset the 'At Lunch State'

                                    string[] sections = messageText.Split(",".ToCharArray());
                                    int iFleetID = Convert.ToInt32(sections[1]);
                                    uint uVehicleID = Convert.ToUInt32(sections[2]);
                                    if (messageText.StartsWith("a,"))
                                        mDatabaseInterface.UnsetAtLunchState(iFleetID, uVehicleID);
                                    else
                                        mDatabaseInterface.SetAtLunchState(iFleetID, uVehicleID);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Lunch State Msg", ex1);
                                }
                                if (bLogPacketData)
                                    WriteToConsole("Received Client 'ResetUnit' Message from " + endPoint.Address.ToString() + ":" + endPoint.Port + ", Data : " + messageText);
                            }
                            else if (messageText.StartsWith("RefreshGlobalStarUnits,"))
                            {
                                try
                                {
                                    #region Changes have been made to a globalstar unit details.

                                    // Message format - "RefreshGlobalStarUnits,fleet,GlobalStarUnitID"
                                    string[] parameters = messageText.Split(delimiter);
                                    if (parameters.Length == 3)
                                    {
                                        NotifyClientsOfConfigChange(parameters[0]);
                                    }

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - GlobalStar Unit Change", ex1);
                                }
                            }
                            else if (messageText.StartsWith("VEHICLE_ACTIVESTATECHANGED"))
                            {
                                try
                                {
                                    #region The active state of a vehicle has changed.

                                    _packetInterpreter.ProcessPacket(null, messageText);

                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Vehicle Active State Change", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ExtendedHistoryRequest,"))
                            {
                                try
                                {
                                    #region ExtendedHistoryRequest Request

                                    // Format for these messages: 
                                    //"ExtendedHistoryRequest",<fleet>,<unit>
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length == 3)
                                    {
                                        _packetInterpreter_ExtendedHistoryRequest(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]));
                                    }
                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - ExtendedHistoryRequest Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("ExtendedHistoryRequestComplete,"))
                            {
                                try
                                {
                                    #region ExtendedHistoryRequestComplete Request

                                    // Format for these messages: 
                                    //"ExtendedHistoryRequestComplete",<fleet>,<unit>,<start>,<end>,userId
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 5)
                                    {
                                        int userId = -1;
                                        if (numbers.Length >=6)
                                        {
                                            userId = Convert.ToInt32(numbers[5]);
                                        }

                                        DateTime startDate;
                                        DateTime endDate;

                                        // Attempt to parse the start and end date in the format of "dd/MM/yyyy HH:mm:ss" otherwise return min date
                                        DateTime.TryParseExact(numbers[3], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                                        DateTime.TryParseExact(numbers[4], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);

                                        // If the dates weren't parsed then try "dd:MM:yyyy HH:mm:ss" which was the previous format
                                        // 2016-08-16 (7.4.2.4) - This shouldn't occur as all the code has been converted to "dd/MM/yyyy HH:mm:ss" but during testing the old format was still being received.
                                        if (startDate == DateTime.MinValue && endDate == DateTime.MinValue)
                                        {
                                            DateTime.TryParseExact(numbers[3], "dd:MM:yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out startDate);
                                            DateTime.TryParseExact(numbers[4], "dd:MM:yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out endDate);
                                        }

                                        _packetInterpreter_ExtendedHistoryRequestComplete(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]), startDate, endDate, userId);
                                    }
                                    #endregion
                                }
                                catch (System.Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - ExtendedHistoryRequestComplete Msg. MessageText:" + messageText, ex1);
                                }
                            }
                            else if (messageText.StartsWith("DvrAlarm,"))
                            {
                                try
                                {
                                    #region DvrAlarm
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 8)
                                    {
                                        _packetInterpreter_DvrAlarm(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]), Convert.ToInt32(numbers[3]), Convert.ToInt32(numbers[4]),
                                            DateTime.ParseExact(numbers[5], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                                            Convert.ToDecimal(numbers[6]), Convert.ToDecimal(numbers[7]));
                                    }
                                    #endregion
                                }
                                catch (Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - DvrAlarm Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("DvrAlarmContent,"))
                            {
                                try
                                {
                                    #region DvrAlarmContent

                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 6)
                                    {
                                        _packetInterpreter_DvrAlarmContent(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]),
                                            Convert.ToInt32(numbers[3]), Convert.ToInt32(numbers[4]), DateTime.ParseExact(numbers[5], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture));
                                    }
                                    #endregion
                                }
                                catch (Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - DvrAlarmContent Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("DvrError,"))
                            {
                                try
                                {
                                    #region DvrError
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 8)
                                    {
                                        _packetInterpreter_DvrError(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]), numbers[3],
                                            DateTime.ParseExact(numbers[4], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                                            Convert.ToDecimal(numbers[5]), Convert.ToDecimal(numbers[6]), Convert.ToInt16(numbers[7]));
                                    }
                                    #endregion
                                }
                                catch (Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - DvrError Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("DvrRequest,"))
                            {
                                try
                                {
                                    #region DvrRequest
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 8)
                                    {
                                        _packetInterpreter_DvrRequest(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]),
                                            Convert.ToInt32(numbers[3]), Convert.ToInt32(numbers[4]), DateTime.ParseExact(numbers[5], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                                            Convert.ToDecimal(numbers[6]), Convert.ToDecimal(numbers[7]));
                                    }
                                    #endregion
                                }
                                catch (Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - DvrRequest Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("DvrRequestContent,"))
                            {
                                try
                                {
                                    #region DvrRequestContent
                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 5)
                                    {
                                        _packetInterpreter_DvrRequestContent(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]),
                                            Convert.ToInt32(numbers[3]), Convert.ToInt32(numbers[4]));
                                    }
                                    #endregion
                                }
                                catch (Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - DvrRequestContent Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("DvrStatus,"))
                            {
                                try
                                {
                                    #region DvrStatus

                                    string[] numbers = messageText.Split(delimiter);
                                    if (numbers.Length >= 19)
                                    {
                                        _packetInterpreter_DvrStatus(this, Convert.ToInt32(numbers[1]), Convert.ToInt32(numbers[2]), numbers[3],
                                            DateTime.ParseExact(numbers[4], "dd/MM/yyyy HH:mm:ss", CultureInfo.InvariantCulture),
                                            Convert.ToDecimal(numbers[5]), Convert.ToDecimal(numbers[6]), true,
                                            Convert.ToInt16(numbers[7]), Convert.ToInt32(numbers[8]) == 1, Convert.ToInt32(numbers[9]) == 1, Convert.ToInt32(numbers[10]) == 1,
                                            Convert.ToInt16(numbers[11]), Convert.ToInt16(numbers[12]),
                                            Convert.ToInt16(numbers[13]), Convert.ToInt16(numbers[14]), Convert.ToInt32(numbers[15]), numbers[16], numbers[17], numbers[18]);
                                    }
                                    #endregion
                                }
                                catch (Exception ex1)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - DvrStatus Msg", ex1);
                                }
                            }
                            else if (messageText.StartsWith("VEHICLE_VEHICLECHANGED"))
                            {
                                try
                                {
                                    #region Vehicle Changed Request

                                    mDatabaseInterface.RefreshVehicleDataSet();
                                    
                                    #endregion
                                }
                                catch (Exception ex)
                                {
                                    _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents) - Vehicle Changed", ex);
                                }
                            }

                            #endregion
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ProcessClientMessage(IPEndPoint endPoint, byte[] contents)", ex);
            }
        }

        public object ClientAliveThread(EnhancedThread sender, object data)
        {
            _log.Info("Client Alive thread started.");

            bool waitingForAliveMessages = false;
            int count = 0;
            while (!sender.Stopping)
            {
                try
                {
                    if (!waitingForAliveMessages)
                    {
                        //if 10 minutes up then send test message to all clients
                        if (count > 6000)
                        {
                            count = 0;
                            waitingForAliveMessages = true;
                            lock (ClientIsAliveSync)
                            {
                                mClientIsAlive.Clear();
                            }
                            _log.Info("Tell clients to send alive packets");
                            TellClients(-1, PacketConstants.COMMAND_STD_KEEPALIVE_TEST, false, false);
                        }
                    }
                    else
                    {
                        //give client 10 seconds to return I'm alive message
                        if (count > 100)
                        {
                            count = 0;
                            waitingForAliveMessages = false;

                            //get all clients that have sent alive messages
                            List<IPEndPoint> aliveClients;
                            lock (ClientIsAliveSync)
                            {
                                aliveClients = mClientIsAlive.ToList();
                            }
                            _log.InfoFormat("{0} clients have sent in alive packets", aliveClients.Count);
                            //remove any clients no longer alive
                            lock (UDPClientListSync)
                            {
                                _log.InfoFormat("{0} clients currently in list", mClientList.Count);
                                var result = mClientList.Except(aliveClients).ToArray();
                                foreach (var endPoint in result)
                                {
                                    _log.InfoFormat("removing {0} client", endPoint);
                                    UnsafeRemoveClient(endPoint);
                                }
                            }

                        }
                    }
                    count++;
                    Thread.Sleep(100);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "ProcessClientMsgsThread(EnhancedThread sender, object data)", ex);
                }
            }

            _log.Info("Client alive thread stopped.");
            return null;
        }
        #endregion

        #region Broadcast Functionality

        /// <summary>
        /// Given the list of fleetIDs, get a unique list of all clients registered for those fleets, and
        /// broadcast the equivalent message to them.
        /// NOTE : This is only valid for clients Version 5.9 and over, which uses 
        /// </summary>
        /// <param name="fleetIDs"></param>
        /// <param name="broadcastMessage"></param>
        public void SendClientBroadcastMessage(IPEndPoint source, uint[] fleetIDs, string broadcastMessage)
        {
            ArrayList targets = new ArrayList();

            for (int loop = 0; loop < fleetIDs.Length; loop++)
            {
                ArrayList list = (ArrayList)mFleetList[fleetIDs[loop]];
                if (list != null)
                {
                    foreach (IPEndPoint endPoint in list)
                        if (targets.IndexOf(endPoint) == -1)
                        {
                            double clientVersion = (double)mClientVersion[endPoint];
                            if (clientVersion >= 3.0)
                                targets.Add(endPoint);
                        }
                }
            }

            if (targets.Count > 0)
            {
                byte[] message = System.Text.ASCIIEncoding.ASCII.GetBytes(string.Format("Broadcast,{0}", broadcastMessage));
                foreach (IPEndPoint endPoint in targets)
                    if (!endPoint.Equals(source))
                        mClientListenerThread.Send(message, message.Length, endPoint);
            }
        }

        #endregion

        #region Add and Remove clients

        public void AddClient(double clientVersionMajorMinor, IPEndPoint endPoint, uint[] fleetsOfInterest, bool bSendUpdatesFromFlash, bool nonTrackingOnly)
        {
            ArrayList fleetClients = null;
            string sClientAddMsg = "";

            #region Log Client Connects

            if (bSendUpdatesFromFlash)
                sClientAddMsg = "Client Login (with updates from flash) from " + endPoint.ToString() + ", Version : " + clientVersionMajorMinor.ToString() + ", Registered for fleets : ";
            else
                sClientAddMsg = "Client Login from " + endPoint.ToString() + ", Version : " + clientVersionMajorMinor.ToString() + ", Registered for fleets : ";

            for (int X = 0; X < fleetsOfInterest.Length; X++)
            {
                if (X == 0)
                    sClientAddMsg += Convert.ToString(fleetsOfInterest[X]);
                else
                    sClientAddMsg += ", " + Convert.ToString(fleetsOfInterest[X]);
            }
            WriteToConsole(sClientAddMsg);

            #endregion

            if (bLogPacketData)
                WriteToConsole("Adding Client: " + endPoint.ToString());
            // See if this client has already connected:
            //POD - ensure that only one syncobject is used 
            try
            {
                bool bEndPointAdded = false;
                lock (this.UDPClientListSync)
                {
                    if (!mClientList.Contains(endPoint))
                    {
                        mClientList.Add(endPoint);
                        bEndPointAdded = true;
                    }
                }
                if (bEndPointAdded)
                {
                    if (bLogPacketData)
                        WriteToConsole("Adding new client from : " + endPoint.Address.ToString() + ":" + endPoint.Port + ".");
                    mClientVersion.Add(endPoint, clientVersionMajorMinor);
                }
                else
                {
                    if (bLogPacketData)
                        WriteToConsole("Client from is already connected : " + endPoint.Address.ToString() + ":" + endPoint.Port);

                    //	Remove from Fleet List, and Readd, as Fleet List can be unreliable for this IP
                    UnsafeRemoveFromFleetList(endPoint);
                }

                //	This list also falls under the umbrella of the one SyncRoot..
                foreach (uint fleet in fleetsOfInterest)
                {
                    try
                    {
                        if (nonTrackingOnly)
                        {
                            if (!mFleetNonTrackingList.ContainsKey(fleet))
                            {
                                // Never stored this fleet before.
                                // Create a queue to hold the client
                                fleetClients = new ArrayList();
                                fleetClients.Add(endPoint);
                                mFleetNonTrackingList.Add(fleet, fleetClients);
                            }
                            else
                            {
                                // Somebody else is already interested in this fleet
                                fleetClients = (ArrayList)mFleetNonTrackingList[fleet];
                                fleetClients.Add(endPoint);
                            }
                        }
                        else
                        {

                            if (!mFleetList.ContainsKey(fleet))
                            {
                                // Never stored this fleet before.
                                // Create a queue to hold the client
                                fleetClients = new ArrayList();
                                fleetClients.Add(endPoint);
                                mFleetList.Add(fleet, fleetClients);
                            }
                            else
                            {
                                // Somebody else is already interested in this fleet
                                fleetClients = (ArrayList)mFleetList[fleet];
                                fleetClients.Add(endPoint);
                            }
                            if (bSendUpdatesFromFlash)
                            {
                                #region If this client has registered to recieve updates with the flash flag set

                                if (!mFromFlashFleetList.ContainsKey(fleet))
                                {
                                    // Never stored this fleet before.
                                    // Create a queue to hold the client
                                    fleetClients = new ArrayList();
                                    fleetClients.Add(endPoint);
                                    mFromFlashFleetList.Add(fleet, fleetClients);
                                }
                                else
                                {
                                    // Somebody else is already interested in this fleet
                                    fleetClients = (ArrayList)mFromFlashFleetList[fleet];
                                    fleetClients.Add(endPoint);
                                }

                                #endregion
                            }
                        }
                    }
                    catch (System.Exception ex2)
                    {
                        WriteToErrorConsole(ex2);
                    }
                }

                //still add to I'm Alive so not removed when purging.
                lock (this.ClientIsAliveSync)
                {
                    if (!mClientIsAlive.Contains(endPoint))
                    {
                        mClientIsAlive.Add(endPoint);
                    }
                }
                if (bLogPacketData)
                    WriteToConsole("    Client: " + endPoint.ToString() + " has been added");
            }
            catch (Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// This method will have to be called within a lock cycle.
        /// </summary>
        /// <param name="endPoint"></param>
        private void UnsafeRemoveFromFleetList(IPEndPoint endPoint)
        {
            ArrayList oEndPointList = null;
            try
            {
                #region Clear the registration for updates from a fleet

                for (int pos = mFleetList.Count - 1; pos >= 0; pos--)
                {
                    oEndPointList = ((ArrayList)mFleetList.GetByIndex(pos));
                    if (oEndPointList.Contains(endPoint))
                    {
                        oEndPointList.Remove(endPoint);
                        if (oEndPointList.Count == 0)
                            mFleetList.RemoveAt(pos);
                    }
                }

                #endregion

                #region If the endpoint is register to recieve updates from flash, clear that entry as well

                for (int pos = mFromFlashFleetList.Count - 1; pos >= 0; pos--)
                {
                    oEndPointList = ((ArrayList)mFromFlashFleetList.GetByIndex(pos));
                    if (oEndPointList.Contains(endPoint))
                    {
                        oEndPointList.Remove(endPoint);
                        if (oEndPointList.Count == 0)
                            mFromFlashFleetList.RemoveAt(pos);
                    }
                }

                #endregion

                #region Clear the registration for updates from a fleet for non tracking events

                for (int pos = mFleetNonTrackingList.Count - 1; pos >= 0; pos--)
                {
                    oEndPointList = ((ArrayList)mFleetNonTrackingList.GetByIndex(pos));
                    if (oEndPointList.Contains(endPoint))
                    {
                        oEndPointList.Remove(endPoint);
                        if (oEndPointList.Count == 0)
                            mFleetNonTrackingList.RemoveAt(pos);
                    }
                }

                #endregion
            }
            catch (System.Exception ex2)
            {
                WriteToErrorConsole(ex2);
            }
        }

        private void UnsafeRemoveClient(IPEndPoint endPoint)
        {
            // Search through the entire list of clients, removing
            // any with the given endpoint
            try
            {
                if (bLogPacketData)
                    WriteToConsole("Closing connection to client from : " + endPoint.Address.ToString() + ":" + endPoint.Port + ".");

                RouteUpdateRemoveEndPoint(endPoint); // Remove any registrations for Database Persist Complete updates
                ECMLiveUpdateRemoveEndPoint(endPoint); // Remove any registrations for ECM live updates
                if (mClientList.Contains(endPoint))
                    mClientList.Remove(endPoint);
                if (mClientVersion.ContainsKey(endPoint))
                    mClientVersion.Remove(endPoint);
                UnsafeRemoveFromFleetList(endPoint);
            }
            catch (System.Exception ex1)
            {
                WriteToErrorConsole(ex1);
            }
        }

        /// <summary>
        /// This will remove the client form the list.
        /// If locked is true, then the SyncRoot has already been locked by the caller.
        /// </summary>
        /// <param name="endPoint"></param>
        public void RemoveClient(IPEndPoint endPoint)
        {
            // Search through the entire list of clients, removing
            // any with the given endpoint
            try
            {
                lock (this.ClientIsAliveSync)
                {
                    if (mClientIsAlive.Contains(endPoint))
                        mClientIsAlive.Remove(endPoint);
                }
                lock (this.UDPClientListSync)
                {
                    UnsafeRemoveClient(endPoint);
                }
            }
            catch (System.Exception ex1)
            {
                WriteToErrorConsole(ex1);
            }
        }

        #endregion
        

        /// <summary>
        /// This allows remote state update to send the clients information regarding a driver deletion.
        /// It is also the beginnings of the conversion to new centralised Inter Application Communication
        /// </summary>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        public void NotifyClientsOfVehicleDelete(int fleetID, uint vehicleID)
        {
            _updateWriter.VehicleDelete(new PacketCreaterContext(new int[] { fleetID }, "Vehicle Delete"), fleetID, vehicleID);
        }

        /// <summary>
        /// This allows remote state update to send the clients information regarding a driver deletion.
        /// It is also the beginnings of the conversion to new centralised Inter Application Communication
        /// </summary>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        public void NotifyClientsOfVehicleLogin(int fleetID, string sMessageString)
        {
            TellClients(fleetID, sMessageString, false, true);
        }

        /// <summary>
        /// This function lets the clients know they need to refresh their local lists because a user has changed a config/WP/RP/Schedule
        /// </summary>
        /// <param name="fleetID"></param>
        public void NotifyClientsOfConfigChange(string sFleetID)
        {
            try
            {
                NotifyClientsOfConfigChange(Convert.ToInt32(sFleetID));
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        public void NotifyClientsOfConfigChange(int iFleetID)
        {
            try
            {
                TellClients(iFleetID, "CONFIG_CHANGED", false, true);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        /// <summary>
        /// This function lets the clients know they need to refresh their local setpoint data.
        /// </summary>
        /// <param name="fleetID"></param>
        public void NotifyClientsOfSetPointChange(string sFleetID, string sSetPointGroupID, string sSetPointID)
        {
            try
            {
                NotifyClientsOfSetPointChange(Convert.ToInt32(sFleetID), Convert.ToInt32(sSetPointGroupID), Convert.ToInt32(sSetPointID));
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        public void NotifyClientsOfSetPointChange(int iFleetID, int iSetPointGroupID, int iSetPointID)
        {
            XMLUpdate oXMLUpdate = new XMLUpdate();
            DataSet dsSetPointUpdate = null;
            string sErrMsg = "";
            byte[] bClientUpdate = null;
            try
            {
                // Cerate a new hash table
                Hashtable oMsgVariants = new Hashtable();
                oMsgVariants.Add((double)0, System.Text.ASCIIEncoding.ASCII.GetBytes("CONFIG_CHANGED"));
                dsSetPointUpdate = mDatabaseInterface.Database.GetSetPointUpdateDataset(iSetPointID);
                oXMLUpdate.cSubCmd = 'S';
                oXMLUpdate.iItemID = iSetPointID;
                oXMLUpdate.ConvertDataSetToXML(dsSetPointUpdate);
                bClientUpdate = oXMLUpdate.Encode(ref sErrMsg);
                if (sErrMsg == "")
                    oMsgVariants.Add((double)5, bClientUpdate);
                else
                    _log.Info("NotifyClientsOfSetPointChange(int iFleetID, int iSetPointGroupID, int iSetPointID) - XML Encode failed : " + sErrMsg);

                TellClients(iFleetID, oMsgVariants, "SetPoint Update", false, true);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "NotifyClientsOfSetPointChange(int iFleetID, int iSetPointGroupID, int iSetPointID)", ex);
            }
        }

        public void NotifyClientsOfWatchListChange(string sFleetID, string sMessage)
        {
            try
            {
                NotifyClientsOfWatchListChange(Convert.ToInt32(sFleetID), sMessage);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        public void NotifyClientsOfWatchListChange(int iFleetID, string sMessage)
        {
            try
            {
                // Cerate a new hash table
                Hashtable oMsgVariants = new Hashtable();
                oMsgVariants.Add((double)0, System.Text.ASCIIEncoding.ASCII.GetBytes(sMessage));
                _log.Info("Sending Watch List Update to Clients : " + sMessage);
                TellClients(iFleetID, oMsgVariants, "WatchList Update", false, true);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "NotifyClientsOfWatchListChange(int iFleetID, string sMessage)", ex);
            }
        }

        private void _packetInterpreter_TimeZoneChanged(object context, TimezoneChangeUpdate oTimeZoneChanged)
        {
            byte[] bData = null;
            string sErrorMsg = "";
            try
            {
                bData = oTimeZoneChanged.Encode(ref sErrorMsg);
                if (sErrorMsg == "")
                    if (oTimeZoneChanged.iFleetID < 1)
                        TellClients(-1, bData, false, true); // If this is an update to the timezone data, send the refresh message to all connected clients
                    else
                        TellClients(oTimeZoneChanged.iFleetID, bData, false, true); // If the change only affects a given fleet, send a refresh message to all clients interested in that fleet.
                else _log.Info(sErrorMsg);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        private void _packetInterpreter_TimeZoneChangedDecodeError(object context, string sErrorMsg)
        {
            _log.Info(sErrorMsg);
        }

        /// <summary>
        /// This allows remote state update to send the clients information regarding a driver creation.
        /// It is also the beginnings of the conversion to new centralised Inter Application Communication
        /// </summary>
        /// <param name="fleetID"></param>
        /// <param name="vehicleID"></param>
        public void NotifyClientsOfVehicleCreate(int fleetID, uint vehicleID)
        {
            _updateWriter.VehicleCreate(new PacketCreaterContext(new int[] { fleetID }, "Vehicle Create"), fleetID, vehicleID);
        }

        public void NotifyClientOfRemoteStatusChange(List<MTData.Transport.Gateway.Packet.GeneralGPPacket> gpPackets, Vehicle v)
        {
            try
            {
                foreach (GeneralGPPacket gpPacket in gpPackets)
                {
                    NotifyClientOfRemoteStatusChange(gpPacket, v);
                }
            }
            catch (System.Exception ex)
            {
                WriteToConsole(ex.Message);
            }
        }
        public void NotifyClientOfRemoteStatusChange(MTData.Transport.Gateway.Packet.GeneralGPPacket gpPacket, Vehicle v, long dssEventId = 0)
        {
            ArrayList oKeys = new ArrayList();
            Hashtable oMsgVariants = new Hashtable();

            int iFleetID = 0;
            int iVehicleID = 0;
            double iHoursDiff = 0;
            string sUserDefinedValue = "";
            string sVehicleTag = "";
            byte[] bConvert = new byte[4];
            string sKey = "";
            DateTime oTestDate = System.DateTime.Now;
            DateTime oCurrentDate = System.DateTime.Now;
            TimeSpan oTS = new System.TimeSpan(0, 0, 0, 0, 0);


            try
            {
                #region Replace Engine values with advanced values where applicable.

                try
                {
                    if (gpPacket.mECMAdvanced != null)
                    {
                        #region mTransportExtraDetails
                        if (v.UseGPSOdometer && gpPacket.mECMAdvanced.Odometer() > 0)
                        {
                            gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mECMAdvanced.GPSOdometer() + v.OdometerOffset);
                        }
                        else if (gpPacket.mECMAdvanced.Odometer() > 0)
                        {
                            if (gpPacket.mECMAdvanced.Odometer() + v.OdometerOffset > 0)
                                gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mECMAdvanced.Odometer() + v.OdometerOffset);
                            else
                                gpPacket.mTransportExtraDetails.iOdometer = 0;
                        }
                        else if (gpPacket.mTransportExtraDetails.iOdometer > 0)
                        {
                            if ((gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                            else
                                gpPacket.mTransportExtraDetails.iOdometer = 0;
                        }

                        gpPacket.mTransportExtraDetails.iEngineHours = GetTotalEngineHours(gpPacket.mECMAdvanced, v);
                        if (gpPacket.mECMAdvanced.TotalFuelUsed() > 0) gpPacket.mTransportExtraDetails.iTotalFuelUsed = (int)gpPacket.mECMAdvanced.TotalFuelUsed();
                        if (gpPacket.mECMAdvanced.FuelEconomy() > 0) gpPacket.mTransportExtraDetails.fFuelEconomy = Convert.ToSingle(gpPacket.mECMAdvanced.FuelEconomy());
                        if (gpPacket.mECMAdvanced.BatteryVolts() > 0) gpPacket.mTransportExtraDetails.iBatteryVolts = gpPacket.mECMAdvanced.BatteryVolts();
                        if (gpPacket.mECMAdvanced.BrakeUsageSeconds() > 0) gpPacket.mTransportExtraDetails.iBrakeUsageSeconds = gpPacket.mECMAdvanced.BrakeUsageSeconds();
                        if (gpPacket.mECMAdvanced.ECMVehicleSpeed(true) > 0) gpPacket.mTransportExtraDetails.iCANVehicleSpeed = gpPacket.mECMAdvanced.ECMVehicleSpeed(true);
                        // RPM Zone Readings
                        if (gpPacket.mECMAdvanced.RPMZone1() > 0) gpPacket.mTransportExtraDetails.iRPMZone1 = gpPacket.mECMAdvanced.RPMZone1();
                        if (gpPacket.mECMAdvanced.RPMZone2() > 0) gpPacket.mTransportExtraDetails.iRPMZone2 = gpPacket.mECMAdvanced.RPMZone2();
                        if (gpPacket.mECMAdvanced.RPMZone3() > 0) gpPacket.mTransportExtraDetails.iRPMZone3 = gpPacket.mECMAdvanced.RPMZone3();
                        if (gpPacket.mECMAdvanced.RPMZone4() > 0) gpPacket.mTransportExtraDetails.iRPMZone4 = gpPacket.mECMAdvanced.RPMZone4();
                        // Speed Zone Readings
                        if (gpPacket.mECMAdvanced.SpeedZone1() > 0) gpPacket.mTransportExtraDetails.iSpeedZone1 = gpPacket.mECMAdvanced.SpeedZone1();
                        if (gpPacket.mECMAdvanced.SpeedZone2() > 0) gpPacket.mTransportExtraDetails.iSpeedZone2 = gpPacket.mECMAdvanced.SpeedZone2();
                        if (gpPacket.mECMAdvanced.SpeedZone3() > 0) gpPacket.mTransportExtraDetails.iSpeedZone3 = gpPacket.mECMAdvanced.SpeedZone3();
                        if (gpPacket.mECMAdvanced.SpeedZone4() > 0) gpPacket.mTransportExtraDetails.iSpeedZone4 = gpPacket.mECMAdvanced.SpeedZone4();

                        #endregion

                        #region mEngineData

                        if (gpPacket.mECMAdvanced.MaxEngCoolTemp() > 0) gpPacket.mEngineData.iCoolantTemperature = gpPacket.mECMAdvanced.MaxEngCoolTemp();
                        if (gpPacket.mECMAdvanced.MaxEngOilTemp() > 0) gpPacket.mEngineData.iOilTemperature = gpPacket.mECMAdvanced.MaxEngOilTemp();
                        if (gpPacket.mECMAdvanced.MinOilPressure() > 0) gpPacket.mEngineData.iOilPressure = gpPacket.mECMAdvanced.MinOilPressure();
                        if (gpPacket.mECMAdvanced.GearPosition() > 0) gpPacket.mEngineData.iGear = gpPacket.mECMAdvanced.GearPosition();
                        if (gpPacket.mECMAdvanced.MaxEngSpeed() > 0) gpPacket.mEngineData.iMaxRPM = gpPacket.mECMAdvanced.MaxEngSpeed();
                        if (gpPacket.mECMAdvanced.BrakeHits() > 0) gpPacket.mEngineData.iBrakeApplications = gpPacket.mECMAdvanced.BrakeHits();
                        // G-Force Readings
                        if (gpPacket.mECMAdvanced.HasGForce)
                        {
                            gpPacket.mEngineData.fGForceFront = Convert.ToSingle(gpPacket.mECMAdvanced.GForceFront());
                            gpPacket.mEngineData.fGForceBack = Convert.ToSingle(gpPacket.mECMAdvanced.GForceBack());
                            gpPacket.mEngineData.fGForceLeftRight = Convert.ToSingle(gpPacket.mECMAdvanced.GForceLeftRight());
                        }

                        #endregion

                        #region mEngineSummaryData

                        if (gpPacket.mECMAdvanced.TripFuel() > 0) gpPacket.mEngineSummaryData.iTripFuel = (int)gpPacket.mECMAdvanced.TripFuel();
                        gpPacket.mEngineSummaryData.iTotalEngineHours = GetTotalEngineHours(gpPacket.mECMAdvanced, v);
                        if (gpPacket.mECMAdvanced.FuelEconomy() > 0) gpPacket.mEngineSummaryData.fFuelEconomy = Convert.ToSingle(gpPacket.mECMAdvanced.FuelEconomy());

                        #endregion

                        #region mExtendedValues

                        gpPacket.mExtendedValues.iValue1 = gpPacket.mECMAdvanced.ExtendedIO1();
                        gpPacket.mExtendedValues.iValue2 = gpPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(gpPacket.cFleetId));
                        gpPacket.mExtendedValues.iValue3 = gpPacket.mECMAdvanced.ExtendedIO3();
                        gpPacket.mExtendedValues.iValue4 = gpPacket.mECMAdvanced.ExtendedIO4();

                        #endregion
                    }
                    else
                    {
                        if (gpPacket.mTransportExtraDetails.iOdometer > 0)
                        {
                            if ((gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                            else
                                gpPacket.mTransportExtraDetails.iOdometer = 0;
                        }
                    }
                }
                catch (System.Exception exAdvanceECM)
                {
                    _log.Error(sClassName + "NotifyClientOfRemoteStatusChange(MTData.Transport.Gateway.Packet.GeneralGPPacket gpPacket, Vehicle v)", exAdvanceECM);
                }

                #endregion

                #region Protect against 0 lat/longs and no Time.

                if (gpPacket.mFixClock.iYear == 0 && gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                    return;
                if (gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                    return;
                sKey = gpPacket.cFleetId.ToString().PadLeft(5, '0') + gpPacket.iVehicleId.ToString().PadLeft(5, '0');
                if ((gpPacket.mFix.dLatitude == 0) || (gpPacket.mFix.dLongitude == 0))
                {
                    if (gpPacket.mFixClock.iYear == 0 && gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                        return;
                    if (gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                        return;
                    // Check if the fix time is within 24 hours of current time.
                    oTestDate = new DateTime(Convert.ToInt32(gpPacket.mFixClock.iYear) + 2000, Convert.ToInt32(gpPacket.mFixClock.iMonth), Convert.ToInt32(gpPacket.mFixClock.iDay), Convert.ToInt32(gpPacket.mFixClock.iHour), Convert.ToInt32(gpPacket.mFixClock.iMinute), Convert.ToInt32(gpPacket.mFixClock.iSecond), 0);
                    oCurrentDate = DateTime.Now.ToUniversalTime();
                    oTS = oCurrentDate.Subtract(oTestDate);
                    iHoursDiff = oTS.TotalHours;
                    if (iHoursDiff > 24 || iHoursDiff < -24)
                        return;
                    // Check if we have a last known position
                    lock (oLastKnowPosition.SyncRoot)
                    {
                        if (oLastKnowPosition.ContainsKey(sKey))
                        {
                            string sValue = (string)oLastKnowPosition[sKey];
                            string[] sValues = sValue.Split("~".ToCharArray());
                            if (sValues.Length == 2)
                            {
                                gpPacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                gpPacket.mFix.dLatitude = Convert.ToDecimal(sValues[1]);
                            }
                        }
                    }
                    // If we still don't have a position, don't send the update.
                    if ((gpPacket.mFix.dLatitude == 0) || (gpPacket.mFix.dLongitude == 0))
                    {
                        #region Protect against 0 lat/longs:

                        try
                        {
                            sKey = gpPacket.cFleetId.ToString().PadLeft(5, '0') + gpPacket.iVehicleId.ToString().PadLeft(5, '0');
                            if ((gpPacket.mFix.dLatitude == 0) || (gpPacket.mFix.dLongitude == 0))
                            {
                                // Check if we have a device time.
                                if (gpPacket.mFixClock.iYear == 0 && gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                                    return;
                                // Check if the fix time is within 24 hours of current time.
                                oTestDate = new DateTime(Convert.ToInt32(gpPacket.mFixClock.iYear) + 2000, Convert.ToInt32(gpPacket.mFixClock.iMonth), Convert.ToInt32(gpPacket.mFixClock.iDay), Convert.ToInt32(gpPacket.mFixClock.iHour), Convert.ToInt32(gpPacket.mFixClock.iMinute), Convert.ToInt32(gpPacket.mFixClock.iSecond), 0);
                                oCurrentDate = DateTime.Now.ToUniversalTime();
                                oTS = oCurrentDate.Subtract(oTestDate);
                                iHoursDiff = oTS.TotalHours;
                                if (iHoursDiff > 24 || iHoursDiff < -24)
                                    return;
                                // Check if we have a last known position
                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                    {
                                        string sValue = (string)oLastKnowPosition[sKey];
                                        string[] sValues = sValue.Split("~".ToCharArray());
                                        if (sValues.Length == 2)
                                        {
                                            gpPacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                            gpPacket.mFix.dLatitude = Convert.ToDecimal(sValues[1]);
                                        }
                                    }
                                }
                                // If we still don't have a position, don't send the update.
                                if ((gpPacket.mFix.dLatitude == 0) || (gpPacket.mFix.dLongitude == 0))
                                {
                                    return;
                                }
                            }
                            else
                            {
                                // If this entry has a postiion, then set the last known position for the unit.
                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                        oLastKnowPosition.Remove(sKey);
                                    oLastKnowPosition.Add(sKey, Convert.ToString(gpPacket.mFix.dLatitude) + "~" + Convert.ToString(gpPacket.mFix.dLongitude));
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            // If an error occurs in here, then don't process the packet.
                            WriteToErrorConsole(ex);
                            return;
                        }

                        #endregion
                    }
                }
                else
                {
                    // If this entry has a postiion, then set the last known position for the unit.
                    lock (oLastKnowPosition.SyncRoot)
                    {
                        if (oLastKnowPosition.ContainsKey(sKey))
                            oLastKnowPosition.Remove(sKey);
                        oLastKnowPosition.Add(sKey, Convert.ToString(gpPacket.mFix.dLatitude) + "~" + Convert.ToString(gpPacket.mFix.dLongitude));
                    }
                }

                #endregion

                //GpsHistory - complete packet
                if (gpPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY && (int)gpPacket.GetStatus().cSpareStatus == 2)
                {
                    //dont send standard live update, send request complete
                    _packetInterpreter_ExtendedHistoryRequestComplete(this, (int)gpPacket.cFleetId, (int)gpPacket.iVehicleId, v.CompletedExtenededHistoryRequestStart, v.CompletedExtenededHistoryRequestEnd, -1);
                    return;
                }


                // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                if (_userDefinedDateFormat != null)
                    sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                else
                    sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');

                if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES)
                    sUserDefinedValue += "%" + gpPacket.iPulseCount.ToString();
                else
                    sUserDefinedValue += "%0";

                bConvert[0] = (byte)gpPacket.cFleetId;
                iFleetID = BitConverter.ToInt32(bConvert, 0);
                iVehicleID = Convert.ToInt32(gpPacket.iVehicleId);

                #region Version 0 Format

                // [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],
                // [UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],
                // [CoolantTemperature],[OilTemperature],[OilPressure],[Gear],[MaxRPM],[BrakeApplications],[GForceFront],[GForceBack],[GForceLeftRight],
                // [TotalEngineHours],[TotalFuel],[TripFuel],[FuelEconomy],[Odometer],[ExtendedValues]

                string s = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                s = s.Replace("[FleetID]", Convert.ToString(iFleetID));
                s = s.Replace("[VehicleID]", Convert.ToString(iVehicleID));

                if (gpPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                {
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32((gpPacket.cRefrigerationZone << 16) + (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END || 
                    gpPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN || 
                    gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gpPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART || 
                    gpPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT || gpPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY ||
                    gpPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION || gpPacket.cMsgType == GeneralGPPacket.TAMPER)
                {
                    // Add the flags to the message type
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
                {
                    // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                {
                    // The top 4 bits of the cInputNumber field holds the speed zone flag for this packet type, 
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32(((gpPacket.bSpeedZoneID & 0x0F) << 12) + ((gpPacket.bSpeedZoneFlags & 0x0F) << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                {
                    // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32(((1 << ((int)gpPacket.cInputNumber)) << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF || gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON || gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD)
                {
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_PHONECALL_PACKET || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || gpPacket.cMsgType == GeneralGPPacket.STATUS_JOB_PACKET)
                {
                    s = s.Replace("[MsgType]", Convert.ToString(Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF))));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
                {
                    s = s.Replace("[MsgType]", Convert.ToString((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF)));
                }
                else if (gpPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION && (int)gpPacket.cButtonNumber > 0)
                {
                    s = s.Replace("[MsgType]", Convert.ToString((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF)));
                }
                else if (gpPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY && (int)gpPacket.GetStatus().cSpareStatus > 0)
                {
                    s = s.Replace("[MsgType]", Convert.ToString((gpPacket.GetStatus().cSpareStatus << 8) + (gpPacket.cMsgType & 0xFF)));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR && gpPacket.EcmError != null && gpPacket.EcmError.Cancelled)
                {
                    s = s.Replace("[MsgType]", "295");
                }
                else
                {
                    bConvert[0] = (byte)gpPacket.cMsgType;
                    s = s.Replace("[MsgType]", Convert.ToString(BitConverter.ToInt32(bConvert, 0)));
                }

                s = s.Replace("[GPSTime]", gpPacket.mFixClock.ToString());
                s = s.Replace("[DeviceTime]", gpPacket.mCurrentClock.ToString());
                s = s.Replace("[ServerTime]", DateUtility.GetDateInFormat(System.DateTime.Now, _serverTime_DateFormat));
                s = s.Replace("[Lat]", Convert.ToString(gpPacket.mFix.dLatitude));
                s = s.Replace("[Long]", Convert.ToString(gpPacket.mFix.dLongitude));
                s = s.Replace("[Direction]", Convert.ToString(gpPacket.mFix.iDirection));
                bConvert[0] = (byte)gpPacket.mFix.cSpeed;
                s = s.Replace("[Speed]", Convert.ToString(BitConverter.ToInt32(bConvert, 0)));
                s = s.Replace("[RawGPS]", gpPacket.sRawGPS);
                s = s.Replace("[Status]", "0");
                bConvert[0] = (byte)gpPacket.mFix.cFlags;
                s = s.Replace("[Flags]", Convert.ToString(BitConverter.ToInt32(bConvert, 0)));
                s = s.Replace("[UserDefinedValue]", sUserDefinedValue);
                bConvert[0] = (byte)gpPacket.mStatus.cInputStatus;
                s = s.Replace("[InputStatus]", Convert.ToString(BitConverter.ToInt32(bConvert, 0)));
                bConvert[0] = (byte)gpPacket.mStatus.cOutputStatus;
                s = s.Replace("[OutputStatus]", Convert.ToString(BitConverter.ToInt32(bConvert, 0)));
                s = s.Replace("[LightStatus]", Convert.ToString(gpPacket.mStatus.cLightStatus));
                s = s.Replace("[ButtonStatus]", Convert.ToString(gpPacket.mStatus.cButtonStatus));
                bConvert[0] = (byte)gpPacket.mDistance.cMaxSpeed;
                s = s.Replace("[MaxSpeed]", Convert.ToString(BitConverter.ToInt32(bConvert, 0)));
                s = s.Replace("[SpeedAccumulator]", Convert.ToString(gpPacket.mDistance.lSpeedAccumulator));
                s = s.Replace("[Samples]", Convert.ToString(gpPacket.mDistance.lSpeedAccumulator));

                #region Add Transport fields if appropriate:

                if (gpPacket.mEngineData != null)
                {
                    s += "," +
                         gpPacket.mEngineData.iCoolantTemperature + "," +
                         gpPacket.mEngineData.iOilTemperature + "," +
                         gpPacket.mEngineData.iOilPressure + "," +
                         gpPacket.mEngineData.iGear + "," +
                         gpPacket.mEngineData.iMaxRPM + "," +
                         gpPacket.mEngineData.iBrakeApplications + "," +
                         gpPacket.mEngineData.fGForceFront + "," +
                         gpPacket.mEngineData.fGForceBack + "," +
                         gpPacket.mEngineData.fGForceLeftRight;

                    if (gpPacket.mEngineSummaryData != null)
                    {
                        s += "," +
                             gpPacket.mEngineSummaryData.iTotalEngineHours + "," +
                             gpPacket.mEngineSummaryData.iTotalFuel + "," +
                             gpPacket.mEngineSummaryData.iTripFuel + "," +
                             gpPacket.mEngineSummaryData.fFuelEconomy + "," +
                             gpPacket.mEngineSummaryData.iOdometer;
                    }
                    else // No summary - five zeroes:
                    {
                        s += ",0,0,0,0,0";
                    }
                }
                else
                {
                    s += ",0,0,0,0,0,0,0,0,0"; // No mEngineData
                    s += ",0,0,0,0,0"; // No mEngineSummaryData
                }

                #endregion

                if (gpPacket.mExtendedValues != null)
                {
                    s += "," +
                         gpPacket.mExtendedValues.ToDatabaseFormatString();
                }
                else
                {
                    s += ",";
                }

                #endregion

                oKeys.Add((double)0);
                oMsgVariants.Add((double)0, s);
                sVehicleTag = v.VehicleTag.Trim();

                #region Version 2.7 Live update string

                //sVer27Format = 
                // [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],
                // [UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],
                // [CoolantTemperature],[OilTemperature],[OilPressure],[Gear],[MaxRPM],[BrakeApplications],[GForceFront],[GForceBack],[GForceLeftRight],
                // [TotalEngineHours],[TotalFuel],[TripFuel],[FuelEconomy],[Odometer],[ExtendedValues],
                // [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag]
                string sVer27Format = s + ",";
                sVer27Format += v.oPos.Position.Trim() + ",";
                sVer27Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                sVer27Format += Math.Round(v.oPos.Distance, 0) + ",";
                sVer27Format += v.oPos.MapRef.Trim() + ",";
                sVer27Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim();
                oKeys.Add((double)2.7);
                oMsgVariants.Add((double)2.7, sVer27Format);

                #endregion

                #region Version 2.71 Live update string

                //sVer271Format = 
                // [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],
                // [UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],
                // [CoolantTemperature],[OilTemperature],[OilPressure],[Gear],[MaxRPM],[BrakeApplications],[GForceFront],[GForceBack],[GForceLeftRight],
                // [TotalEngineHours],[TotalFuel],[TripFuel],[FuelEconomy],[Odometer],[ExtendedValues],
                // [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],
                // [VehicleTag]
                string sVer271Format = s + ",";
                sVer271Format += v.oPos.Position.Trim() + ",";
                sVer271Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                sVer271Format += Math.Round(v.oPos.Distance, 0) + ",";
                sVer271Format += v.oPos.MapRef.Trim() + ",";
                sVer271Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                sVer271Format += sVehicleTag.Replace(",", Convert.ToString((char)0x40)).Trim();
                oKeys.Add((double)2.71);
                oMsgVariants.Add((double)2.71, sVer271Format);

                #endregion

                string sVer28Format = s + ",";

                #region Version 2.8 Live update string

                //sVer28Format = 
                // [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],
                // [UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],
                // [CoolantTemperature],[OilTemperature],[OilPressure],[Gear],[MaxRPM],[BrakeApplications],[GForceFront],[GForceBack],[GForceLeftRight],
                // [TotalEngineHours],[TotalFuel],[TripFuel],[FuelEconomy],[Odometer],[ExtendedValues],
                // [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],
                // [VehicleTag],
                // [Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],
                // [Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                // [Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name]

                sVer28Format += v.oPos.Position.Trim() + ",";
                sVer28Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                sVer28Format += Math.Round(v.oPos.Distance, 0) + ",";
                sVer28Format += v.oPos.MapRef.Trim() + ",";
                sVer28Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                sVer28Format += sVehicleTag + ",";

                #region Add the Transport Extra Details data to the sVer28Format update string

                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iBatteryVolts) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iBrakeUsageSeconds) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iCANVehicleSpeed) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iOdometer) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone1) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone2) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone3) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone4) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone1) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone2) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone3) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone4) + ",";
                sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iTotalFuelUsed) + ",";

                #endregion

                #region Add the Trailer Track values

                string sTrailerName = "";
                string[] sSplit = new string[2];
                if (gpPacket.mTrailerTrack.oTrailers.Count > 0)
                {
                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                    sSplit = sTrailerName.Split("~".ToCharArray());
                    if (sSplit.Length == 2)
                    {
                        sVer28Format += sSplit[0] + ",";
                        sVer28Format += sSplit[1].Replace(",", "||") + ",";
                    }
                }
                else
                    sVer28Format += "0,,";
                if (gpPacket.mTrailerTrack.oTrailers.Count > 1)
                {
                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                    sSplit = sTrailerName.Split("~".ToCharArray());
                    if (sSplit.Length == 2)
                    {
                        sVer28Format += sSplit[0] + ",";
                        sVer28Format += sSplit[1].Replace(",", "||") + ",";
                    }
                }
                else
                    sVer28Format += "0,,";
                if (gpPacket.mTrailerTrack.oTrailers.Count > 2)
                {
                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                    sSplit = sTrailerName.Split("~".ToCharArray());
                    if (sSplit.Length == 2)
                    {
                        sVer28Format += sSplit[0] + ",";
                        sVer28Format += sSplit[1].Replace(",", "||") + ",";
                    }
                }
                else
                    sVer28Format += "0,,";
                if (gpPacket.mTrailerTrack.oTrailers.Count > 3)
                {
                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                    sSplit = sTrailerName.Split("~".ToCharArray());
                    if (sSplit.Length == 2)
                    {
                        sVer28Format += sSplit[0] + ",";
                        sVer28Format += sSplit[1].Replace(",", "||") + ",";
                    }
                }
                else
                    sVer28Format += "0,,";
                if (gpPacket.mTrailerTrack.oTrailers.Count > 4)
                {
                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                    sSplit = sTrailerName.Split("~".ToCharArray());
                    if (sSplit.Length == 2)
                    {
                        sVer28Format += sSplit[0] + ",";
                        sVer28Format += sSplit[1].Replace(",", "||") + ",";
                    }
                }
                else
                    sVer28Format += "0,,";

                #endregion

                oKeys.Add((double)2.8);
                oMsgVariants.Add((double)2.8, sVer28Format);

                #endregion

                #region Version 2.9 Live update string

                string sVer29Format = sVer28Format + ",";
                //sVer29Format = 
                // [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],
                // [UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],
                // [CoolantTemperature],[OilTemperature],[OilPressure],[Gear],[MaxRPM],[BrakeApplications],[GForceFront],[GForceBack],[GForceLeftRight],
                // [TotalEngineHours],[TotalFuel],[TripFuel],[FuelEconomy],[Odometer],[ExtendedValues],
                // [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],
                // [VehicleTag],
                // [Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],
                // [Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                // [Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name],
                // [Fuel Economy]

                sVer29Format += Convert.ToString(gpPacket.mEngineSummaryData.fFuelEconomy) + ",";
                oKeys.Add((double)2.9);
                oMsgVariants.Add((double)2.9, sVer29Format);

                #endregion

                #region Version 3.0 format

                //sVer30Format = 
                // [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],
                // [UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],
                // [CoolantTemperature],[OilTemperature],[OilPressure],[Gear],[MaxRPM],[BrakeApplications],[GForceFront],[GForceBack],[GForceLeftRight],
                // [TotalEngineHours],[TotalFuel],[TripFuel],[FuelEconomy],[Odometer],[ExtendedValues],
                // [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],
                // [VehicleTag],
                // [Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],
                // [Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                // [Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name],
                // [Fuel Economy],
                // [ExtendedState],[AuxiliaryState][SetPointGroupID][SetPointID]

                string sVer30Format = string.Format("{0}{1},{2},{3},{4}",
                                                    sVer29Format,
                                                    v.GetUnitStateForReport(gpPacket),
                                                    v.GetAuxiliaryStateForReport(gpPacket),
                                                    v.oPos.SetPointGroupID,
                                                    v.oPos.SetPointID);
                oKeys.Add((double)3.0);
                oMsgVariants.Add((double)3.0, sVer30Format);

                #endregion

                #region Version 4.0 format

                LiveUpdate oLiveUpdate = new LiveUpdate();
                oLiveUpdate.iPacketVersion = 4;

                #region Populate fleet/vehicle, reason and time details

                oLiveUpdate.iFleetID = (short)iFleetID;
                oLiveUpdate.iVehicleID = iVehicleID;
                oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                if (gpPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cRefrigerationZone << 16) + (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END || 
                    gpPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN || 
                    gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gpPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                    gpPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT || gpPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY ||
                    gpPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION || gpPacket.cMsgType == GeneralGPPacket.TAMPER)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
                {
                    // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                {
                    // The reason code for this report type is
                    // [Speed Zone ID (4 bits)][Speed Zone Flags (4 bits)][Message Type]
                    //

                    oLiveUpdate.iReasonID = Convert.ToInt32(((gpPacket.bSpeedZoneID & 0x0F) << 12) + ((gpPacket.bSpeedZoneFlags & 0x0F) << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                {
                    // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                    oLiveUpdate.iReasonID = Convert.ToInt32(((1 << ((int)gpPacket.cInputNumber)) << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF || gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON || gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_PHONECALL_PACKET || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || gpPacket.cMsgType == GeneralGPPacket.STATUS_JOB_PACKET)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION && (int)gpPacket.cButtonNumber > 0)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY && (int)gpPacket.GetStatus().cSpareStatus > 0)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.GetStatus().cSpareStatus << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR && gpPacket.EcmError != null && gpPacket.EcmError.Cancelled)
                {
                    oLiveUpdate.iReasonID = 295;
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DSS_START)
                {                    
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else
                {
                    oLiveUpdate.iReasonID = (int)gpPacket.cMsgType;
                }

                if (gpPacket.mFixClock != null)
                {
                    oLiveUpdate.dtGPSTime = gpPacket.mFixClock.ToDateTime();
                }
                if (gpPacket.mCurrentClock != null)
                {
                    oLiveUpdate.dtDeviceTime = gpPacket.mCurrentClock.ToDateTime();
                }
                oLiveUpdate.dtServerTime = DateTime.Now;

                #endregion

                #region Populate GPS details

                if (gpPacket.mFix != null)
                {
                    oLiveUpdate.dLat = gpPacket.mFix.dLatitude;
                    oLiveUpdate.dLong = gpPacket.mFix.dLongitude;
                    oLiveUpdate.bFlags = gpPacket.mFix.cFlags;
                    oLiveUpdate.iDirection = (short)gpPacket.mFix.iDirection;
                    oLiveUpdate.bSpeed = gpPacket.mFix.cSpeed;
                }

                #endregion

                #region Populate Distance details

                if (gpPacket.mDistance != null)
                {
                    oLiveUpdate.lSpeedAcc = gpPacket.mDistance.lSpeedAccumulator;
                    oLiveUpdate.bMaxSpeed = gpPacket.mDistance.cMaxSpeed;
                }

                #endregion

                #region Populate Status details

                if (gpPacket.mStatus != null)
                {
                    oLiveUpdate.bInputStatus = gpPacket.mStatus.cInputStatus;
                    oLiveUpdate.bOutputStatus = gpPacket.mStatus.cOutputStatus;
                    oLiveUpdate.iLightStatus = gpPacket.mStatus.cLightStatus;
                    oLiveUpdate.iButtonStatus = gpPacket.mStatus.cButtonStatus;
                }

                #endregion

                #region Populate Engine Data details

                if (gpPacket.mEngineData != null)
                {
                    oLiveUpdate.iCoolantTemperature = (short)gpPacket.mEngineData.iCoolantTemperature;
                    oLiveUpdate.iOilTemp = (short)gpPacket.mEngineData.iOilTemperature;
                    oLiveUpdate.iOilPressure = (short)gpPacket.mEngineData.iOilPressure;
                    oLiveUpdate.iGear = (short)gpPacket.mEngineData.iGear;
                    oLiveUpdate.iMaxRPM = (short)gpPacket.mEngineData.iMaxRPM;
                    oLiveUpdate.iBrakeApplications = (short)gpPacket.mEngineData.iBrakeApplications;
                    oLiveUpdate.fGForceFront = gpPacket.mEngineData.fGForceFront;
                    oLiveUpdate.fGForceBack = gpPacket.mEngineData.fGForceBack;
                    oLiveUpdate.fGForceLeftRight = gpPacket.mEngineData.fGForceLeftRight;
                }

                #endregion

                #region Populate Engine Summary Data details

                if (gpPacket.mEngineSummaryData != null)
                {
                    oLiveUpdate.iTotalEngineHours = gpPacket.mEngineSummaryData.iTotalEngineHours;
                    oLiveUpdate.iTripFuel = gpPacket.mEngineSummaryData.iTripFuel;
                    oLiveUpdate.dFuelEconomy = gpPacket.mEngineSummaryData.fFuelEconomy;
                }

                #endregion

                #region Populate Extended Value details

                if (gpPacket.mExtendedValues != null)
                {
                    oLiveUpdate.iExtendedValue1 = gpPacket.mExtendedValues.iValue1;
                    oLiveUpdate.iExtendedValue2 = gpPacket.mExtendedValues.iValue2;
                    oLiveUpdate.iExtendedValue3 = gpPacket.mExtendedValues.iValue3;
                    oLiveUpdate.iExtendedValue4 = gpPacket.mExtendedValues.iValue4;
                }

                #endregion

                #region Populate Transport Extra  details

                if (gpPacket.mTransportExtraDetails != null)
                {
                    oLiveUpdate.iBatteryVolts = (short)gpPacket.mTransportExtraDetails.iBatteryVolts;
                    oLiveUpdate.iBrakeUsageSeconds = (short)gpPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                    oLiveUpdate.iOdometer = (uint)gpPacket.mTransportExtraDetails.iOdometer;
                    oLiveUpdate.iTotalFuelUsed = gpPacket.mTransportExtraDetails.iTotalFuelUsed;
                    oLiveUpdate.iRPMZone1 = (short)gpPacket.mTransportExtraDetails.iRPMZone1;
                    oLiveUpdate.iRPMZone2 = (short)gpPacket.mTransportExtraDetails.iRPMZone2;
                    oLiveUpdate.iRPMZone3 = (short)gpPacket.mTransportExtraDetails.iRPMZone3;
                    oLiveUpdate.iRPMZone4 = (short)gpPacket.mTransportExtraDetails.iRPMZone4;
                    oLiveUpdate.iSpeedZone1 = (short)gpPacket.mTransportExtraDetails.iSpeedZone1;
                    oLiveUpdate.iSpeedZone2 = (short)gpPacket.mTransportExtraDetails.iSpeedZone2;
                    oLiveUpdate.iSpeedZone3 = (short)gpPacket.mTransportExtraDetails.iSpeedZone3;
                    oLiveUpdate.iSpeedZone4 = (short)gpPacket.mTransportExtraDetails.iSpeedZone4;
                }

                #endregion

                #region Populate Extended / Refrigeration details

                if (gpPacket.mExtendedVariableDetails != null)
                {
                    if (gpPacket.mExtendedVariableDetails.Refrigeration != null)
                    {
                        #region Populate Refrigeration Common Values

                        oLiveUpdate.iReferFlags = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                        oLiveUpdate.dReferFuelPercentage = gpPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                        oLiveUpdate.dReferBatteryVolts = gpPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                        oLiveUpdate.bReferInput1 = gpPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                        oLiveUpdate.bReferInput2 = gpPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                        oLiveUpdate.bReferInput3 = gpPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                        oLiveUpdate.bReferInput4 = gpPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                        oLiveUpdate.iReferSensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                        oLiveUpdate.dReferHumidity = gpPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                        oLiveUpdate.fReferSensor1 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                        oLiveUpdate.fReferSensor2 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                        oLiveUpdate.fReferSensor3 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                        oLiveUpdate.fReferSensor4 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                        oLiveUpdate.fReferSensor5 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                        oLiveUpdate.fReferSensor6 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                        #endregion

                        #region Populate Refrigeration Zone Values

                        if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                        {
                            if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                            {
                                #region Populate Zone 1 values

                                oLiveUpdate.iReferZ1Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                oLiveUpdate.iReferZ1OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                oLiveUpdate.iReferZ1Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                oLiveUpdate.iReferZ1SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                oLiveUpdate.fReferZ1ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                oLiveUpdate.fReferZ1SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                oLiveUpdate.fReferZ1SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                oLiveUpdate.fReferZ1Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                oLiveUpdate.fReferZ1Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                #endregion
                            }
                            if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                            {
                                #region Populate Zone 2 values

                                oLiveUpdate.iReferZ2Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                oLiveUpdate.iReferZ2OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                oLiveUpdate.iReferZ2Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                oLiveUpdate.iReferZ2SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                oLiveUpdate.fReferZ2ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                oLiveUpdate.fReferZ2ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                oLiveUpdate.fReferZ2SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                oLiveUpdate.fReferZ2SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                oLiveUpdate.fReferZ2Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                oLiveUpdate.fReferZ2Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                #endregion
                            }
                            if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                            {
                                #region Populate Zone 3 values

                                oLiveUpdate.iReferZ3Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                oLiveUpdate.iReferZ3OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                oLiveUpdate.iReferZ3Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                oLiveUpdate.iReferZ3SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                oLiveUpdate.fReferZ3ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                oLiveUpdate.fReferZ3ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                oLiveUpdate.fReferZ3SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                oLiveUpdate.fReferZ3SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                oLiveUpdate.fReferZ3Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                oLiveUpdate.fReferZ3Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                #endregion
                            }
                        }

                        #endregion
                    }
                }

                #endregion

                #region Populate Raw GPS and User Defined details

                oLiveUpdate.sRawGPS = gpPacket.sRawGPS;
                oLiveUpdate.sUserDefined = sUserDefinedValue;

                #endregion

                #region Populate Vehicle Location/labeling details

                if (v != null)
                {
                    oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(gpPacket);
                    oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(gpPacket);
                    if (v.oPos != null)
                    {
                        oLiveUpdate.sPosition = v.oPos.Position.Trim();

                        if (!String.IsNullOrEmpty(gpPacket.StreetName))
                        {
                            if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                            {
                                oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + gpPacket.StreetName;
                            }
                            else
                            {
                                oLiveUpdate.sPlaceName = gpPacket.StreetName;
                            }
                        }
                        else
                            oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();

                        oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                        oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                        oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                        oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                    }

                    oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                    oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                }

                #endregion

                #region Populate Trailer Track details

                if (gpPacket.mTrailerTrack != null)
                {
                    if (gpPacket.mTrailerTrack.oTrailers != null)
                    {
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 1)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 2)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 3)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 4)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 5)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                            }
                        }
                    }
                }

                #endregion

                #region Populate the plugin field to indicate if this messages is commingout of flash

                // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                byte[] bFlashInfo = new byte[2];
                if (gpPacket._receivedFlashAvailable)
                    bFlashInfo[0] = 0x01;
                else
                    bFlashInfo[0] = 0x00;
                if (gpPacket.bArchivalData)
                    bFlashInfo[1] = 0x01;
                else
                    bFlashInfo[1] = 0x00;
                cPluginData oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                oLiveUpdate.AddPluginDataField(oPluginData);

                #endregion

                #region Populate the plugin field to add any extra GPS or G-Force information - Plugin data is only attached if values are present.

                if (gpPacket.mECMAdvanced != null)
                {

                    if (gpPacket.mECMAdvanced.SatellitesHDOP() > 0 || gpPacket.mECMAdvanced.SatellitesInView() > 0 || gpPacket.mECMAdvanced.Altitude() > 0)
                    {
                        MemoryStream oMS = new MemoryStream();
                        oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                        oMS.WriteByte(gpPacket.mECMAdvanced.SatellitesInView());
                        oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.Altitude()), 0, 2);
                        oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                    if (gpPacket.mECMAdvanced.GForceZAxis() > 0 || gpPacket.mECMAdvanced.GForceTemperature() > 0)
                    {
                        MemoryStream oMS = new MemoryStream();
                        oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                        oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                        oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                }

                #endregion
                #region Populate the plugin field to provide the current route data.

                if (gpPacket.mExtendedVariableDetails != null && gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                {
                    byte[] bRouteInfo = new byte[8];
                    byte[] bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                    bRouteInfo[0] = bConv[0];
                    bRouteInfo[1] = bConv[1];
                    bRouteInfo[2] = bConv[2];
                    bRouteInfo[3] = bConv[3];
                    bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                    bRouteInfo[4] = bConv[0];
                    bRouteInfo[5] = bConv[1];
                    bRouteInfo[6] = bConv[2];
                    bRouteInfo[7] = bConv[3];
                    oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                    oLiveUpdate.AddPluginDataField(oPluginData);
                }

                #endregion

                #region Populate the plugin field to provide the current route data.

                if (gpPacket.mTransportExtraDetails != null && gpPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                {
                    byte[] bCANVehicleSpeed = new byte[4];
                    byte[] bConv = BitConverter.GetBytes(gpPacket.mTransportExtraDetails.iCANVehicleSpeed);
                    bCANVehicleSpeed[0] = bConv[0];
                    bCANVehicleSpeed[1] = bConv[1];
                    bCANVehicleSpeed[2] = bConv[2];
                    bCANVehicleSpeed[3] = bConv[3];
                    oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                    oLiveUpdate.AddPluginDataField(oPluginData);
                }

                #endregion

                #region Populate the plugin field to provide more accurate speed readings
                if (gpPacket.mECMAdvanced != null)
                {
                    float speedAccuracy = gpPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                    float maxSpeedAccuracy = gpPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                    if ((speedAccuracy > 0 && gpPacket.mFix != null) || (maxSpeedAccuracy > 0 && gpPacket.mDistance != null))
                    {
                        if (gpPacket.mFix != null)
                            speedAccuracy += ((float)((int)gpPacket.mFix.cSpeed));
                        else
                            speedAccuracy = 0;

                        if (gpPacket.mDistance != null)
                            maxSpeedAccuracy += ((float)((int)gpPacket.mDistance.cMaxSpeed));
                        else
                            maxSpeedAccuracy = 0;

                        MemoryStream oMS = new MemoryStream();
                        oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                        oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                        oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                }
                #endregion

                string sErrMsg = "";
                byte[] bVer4Format = oLiveUpdate.Encode(ref sErrMsg);
                if (sErrMsg == "")
                {
                    oKeys.Add((double)4.0);
                    oMsgVariants.Add((double)4.0, bVer4Format);
                }
                else
                    WriteToConsole(sErrMsg);

                #endregion

                #region Version 5.0 format

                oLiveUpdate = new LiveUpdate();
                oLiveUpdate.iPacketVersion = 5;

                #region Populate fleet/vehicle, reason and time details

                oLiveUpdate.iFleetID = (short)iFleetID;
                oLiveUpdate.iVehicleID = iVehicleID;
                oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                if (gpPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cRefrigerationZone << 16) + (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                    gpPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                    gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gpPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                    gpPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT || gpPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY ||
                    gpPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION || gpPacket.cMsgType == GeneralGPPacket.TAMPER)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
                {
                    // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                {
                    // The cInputNumber field holds the speed zone flag for this packet type, 
                    oLiveUpdate.iReasonID = Convert.ToInt32(((gpPacket.bSpeedZoneID & 0x0F) << 12) + ((gpPacket.bSpeedZoneFlags & 0x0F) << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                {
                    // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                    oLiveUpdate.iReasonID = Convert.ToInt32(((1 << ((int)gpPacket.cInputNumber)) << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF || gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON || gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_PHONECALL_PACKET || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || gpPacket.cMsgType == GeneralGPPacket.STATUS_JOB_PACKET)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION && (int)gpPacket.cButtonNumber > 0)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY && (int)gpPacket.GetStatus().cSpareStatus > 0)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.GetStatus().cSpareStatus << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else if (gpPacket.cMsgType == GenericPacket.GENERIC_PACKET)
                {
                    GenericPacket g = new GenericPacket(gpPacket, _serverTime_DateFormat); 
                    oLiveUpdate.iReasonID = Convert.ToInt32(((int)(g.SubCommand + 1) << 8) + (g.cMsgType & 0xFF));

                    if (g.SubCommand == GenericSubCommands.DvrAlarm)
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            memoryStream.Write(BitConverter.GetBytes(g.DvrDeviceId), 0, 4);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrEventId), 0, 4);
                            oPluginData = new cPluginData(PluginIds.DvrEventInfo, memoryStream.ToArray());
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }
                    }

                    if (g.SubCommand == GenericSubCommands.DvrStatus ||
                        g.SubCommand == GenericSubCommands.DvrError ||
                        g.SubCommand == GenericSubCommands.DvrErrorDiskStatus ||
                        g.SubCommand == GenericSubCommands.DvrErrorDiskRecording ||
                        g.SubCommand == GenericSubCommands.DvrErrorCamerasConnected ||
                        g.SubCommand == GenericSubCommands.DvrErrorComms ||
                        g.SubCommand == GenericSubCommands.DvrErrorDataUsageLimit ||
                        g.SubCommand == GenericSubCommands.DvrErrorUploadAttemptLimit ||
                        g.SubCommand == GenericSubCommands.DvrErrorIncompatibleFirmware)
                    {
                        using (MemoryStream memoryStream = new MemoryStream())
                        {
                            memoryStream.Write(BitConverter.GetBytes(g.DvrErrorCode), 0, 4);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusHddOk), 0, 1);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusRecording), 0, 1);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusConnected), 0, 1);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusVideoConnectedChannel), 0, 4);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusVideoRecordChannel), 0, 4);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusHddSize), 0, 4);
                            memoryStream.Write(BitConverter.GetBytes(g.DvrStatusHddFree), 0, 4);

                            oPluginData = new cPluginData(PluginIds.DvrStatusInfo, memoryStream.ToArray());
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }
                    }
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR && gpPacket.EcmError != null && gpPacket.EcmError.Cancelled)
                {
                    oLiveUpdate.iReasonID = 295;
                }
                else if (gpPacket.cMsgType == GeneralGPPacket.DSS_START)
                {
                    oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                }
                else
                {
                    oLiveUpdate.iReasonID = (int) gpPacket.cMsgType;
                }

                if (gpPacket.mFixClock != null)
                {
                    oLiveUpdate.dtGPSTime = gpPacket.mFixClock.ToDateTime();
                }
                if (gpPacket.mCurrentClock != null)
                {
                    oLiveUpdate.dtDeviceTime = gpPacket.mCurrentClock.ToDateTime();
                }
                oLiveUpdate.dtServerTime = DateTime.Now;

                #endregion

                #region Populate GPS details

                if (gpPacket.mFix != null)
                {
                    oLiveUpdate.dLat = gpPacket.mFix.dLatitude;
                    oLiveUpdate.dLong = gpPacket.mFix.dLongitude;
                    oLiveUpdate.bFlags = gpPacket.mFix.cFlags;
                    oLiveUpdate.iDirection = (short)gpPacket.mFix.iDirection;
                    oLiveUpdate.bSpeed = gpPacket.mFix.cSpeed;
                }

                #endregion

                #region Populate Distance details

                if (gpPacket.mDistance != null)
                {
                    oLiveUpdate.lSpeedAcc = gpPacket.mDistance.lSpeedAccumulator;
                    oLiveUpdate.bMaxSpeed = gpPacket.mDistance.cMaxSpeed;
                }

                #endregion

                #region Populate Status details

                if (gpPacket.mStatus != null)
                {
                    oLiveUpdate.bInputStatus = gpPacket.mStatus.cInputStatus;
                    oLiveUpdate.bOutputStatus = gpPacket.mStatus.cOutputStatus;
                    oLiveUpdate.iLightStatus = gpPacket.mStatus.cLightStatus;
                    oLiveUpdate.iButtonStatus = gpPacket.mStatus.cButtonStatus;
                }

                #endregion

                #region Populate Engine Data details

                if (gpPacket.mEngineData != null)
                {
                    oLiveUpdate.iCoolantTemperature = (short)gpPacket.mEngineData.iCoolantTemperature;
                    oLiveUpdate.iOilTemp = (short)gpPacket.mEngineData.iOilTemperature;
                    oLiveUpdate.iOilPressure = (short)gpPacket.mEngineData.iOilPressure;
                    oLiveUpdate.iGear = (short)gpPacket.mEngineData.iGear;
                    oLiveUpdate.iMaxRPM = (short)gpPacket.mEngineData.iMaxRPM;
                    oLiveUpdate.iBrakeApplications = (short)gpPacket.mEngineData.iBrakeApplications;
                    oLiveUpdate.fGForceFront = gpPacket.mEngineData.fGForceFront;
                    oLiveUpdate.fGForceBack = gpPacket.mEngineData.fGForceBack;
                    oLiveUpdate.fGForceLeftRight = gpPacket.mEngineData.fGForceLeftRight;
                }

                #endregion

                #region Populate Engine Summary Data details

                if (gpPacket.mEngineSummaryData != null)
                {
                    oLiveUpdate.iTotalEngineHours = gpPacket.mEngineSummaryData.iTotalEngineHours;
                    oLiveUpdate.iTripFuel = gpPacket.mEngineSummaryData.iTripFuel;
                    oLiveUpdate.dFuelEconomy = gpPacket.mEngineSummaryData.fFuelEconomy;
                }

                #endregion

                #region Populate Extended Value details

                if (gpPacket.mExtendedValues != null)
                {
                    oLiveUpdate.iExtendedValue1 = gpPacket.mExtendedValues.iValue1;
                    oLiveUpdate.iExtendedValue2 = gpPacket.mExtendedValues.iValue2;
                    oLiveUpdate.iExtendedValue3 = gpPacket.mExtendedValues.iValue3;
                    oLiveUpdate.iExtendedValue4 = gpPacket.mExtendedValues.iValue4;
                }

                #endregion

                #region Populate Transport Extra  details

                if (gpPacket.mTransportExtraDetails != null)
                {
                    oLiveUpdate.iBatteryVolts = (short)gpPacket.mTransportExtraDetails.iBatteryVolts;
                    oLiveUpdate.iBrakeUsageSeconds = (short)gpPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                    oLiveUpdate.iOdometer = (uint)gpPacket.mTransportExtraDetails.iOdometer;
                    oLiveUpdate.iTotalFuelUsed = gpPacket.mTransportExtraDetails.iTotalFuelUsed;
                    oLiveUpdate.iRPMZone1 = (short)gpPacket.mTransportExtraDetails.iRPMZone1;
                    oLiveUpdate.iRPMZone2 = (short)gpPacket.mTransportExtraDetails.iRPMZone2;
                    oLiveUpdate.iRPMZone3 = (short)gpPacket.mTransportExtraDetails.iRPMZone3;
                    oLiveUpdate.iRPMZone4 = (short)gpPacket.mTransportExtraDetails.iRPMZone4;
                    oLiveUpdate.iSpeedZone1 = (short)gpPacket.mTransportExtraDetails.iSpeedZone1;
                    oLiveUpdate.iSpeedZone2 = (short)gpPacket.mTransportExtraDetails.iSpeedZone2;
                    oLiveUpdate.iSpeedZone3 = (short)gpPacket.mTransportExtraDetails.iSpeedZone3;
                    oLiveUpdate.iSpeedZone4 = (short)gpPacket.mTransportExtraDetails.iSpeedZone4;
                }

                #endregion

                #region Populate Extended / Refrigeration details

                if (gpPacket.mExtendedVariableDetails != null)
                {
                    if (gpPacket.mExtendedVariableDetails.Refrigeration != null)
                    {
                        #region Populate Refrigeration Common Values

                        oLiveUpdate.iReferFlags = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                        oLiveUpdate.dReferFuelPercentage = gpPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                        oLiveUpdate.dReferBatteryVolts = gpPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                        oLiveUpdate.bReferInput1 = gpPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                        oLiveUpdate.bReferInput2 = gpPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                        oLiveUpdate.bReferInput3 = gpPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                        oLiveUpdate.bReferInput4 = gpPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                        oLiveUpdate.iReferSensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                        oLiveUpdate.dReferHumidity = gpPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                        oLiveUpdate.fReferSensor1 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                        oLiveUpdate.fReferSensor2 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                        oLiveUpdate.fReferSensor3 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                        oLiveUpdate.fReferSensor4 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                        oLiveUpdate.fReferSensor5 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                        oLiveUpdate.fReferSensor6 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                        #endregion

                        #region Populate Refrigeration Zone Values

                        if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                        {
                            if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                            {
                                #region Populate Zone 1 values

                                oLiveUpdate.iReferZ1Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                oLiveUpdate.iReferZ1OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                oLiveUpdate.iReferZ1Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                oLiveUpdate.iReferZ1SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                oLiveUpdate.fReferZ1ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                oLiveUpdate.fReferZ1SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                oLiveUpdate.fReferZ1SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                oLiveUpdate.fReferZ1Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                oLiveUpdate.fReferZ1Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                #endregion
                            }
                            if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                            {
                                #region Populate Zone 2 values

                                oLiveUpdate.iReferZ2Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                oLiveUpdate.iReferZ2OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                oLiveUpdate.iReferZ2Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                oLiveUpdate.iReferZ2SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                oLiveUpdate.fReferZ2ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                oLiveUpdate.fReferZ2ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                oLiveUpdate.fReferZ2SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                oLiveUpdate.fReferZ2SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                oLiveUpdate.fReferZ2Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                oLiveUpdate.fReferZ2Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                #endregion
                            }
                            if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                            {
                                #region Populate Zone 3 values

                                oLiveUpdate.iReferZ3Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                oLiveUpdate.iReferZ3OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                oLiveUpdate.iReferZ3Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                oLiveUpdate.iReferZ3SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                oLiveUpdate.fReferZ3ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                oLiveUpdate.fReferZ3ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                oLiveUpdate.fReferZ3SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                oLiveUpdate.fReferZ3SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                oLiveUpdate.fReferZ3Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                oLiveUpdate.fReferZ3Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                #endregion
                            }
                        }

                        #endregion
                    }
                }

                #endregion

                #region Populate Raw GPS and User Defined details

                oLiveUpdate.sRawGPS = gpPacket.sRawGPS;
                oLiveUpdate.sUserDefined = sUserDefinedValue;

                #endregion

                #region Populate Vehicle Location/labeling details

                if (v != null)
                {
                    oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(gpPacket);
                    oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(gpPacket);
                    if (v.oPos != null)
                    {
                        oLiveUpdate.sPosition = v.oPos.Position.Trim();
                        if (!String.IsNullOrEmpty(gpPacket.StreetName))
                        {
                            if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                            {
                                oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + gpPacket.StreetName;
                            }
                            else
                            {
                                oLiveUpdate.sPlaceName = gpPacket.StreetName;
                            }
                        }
                        else
                            oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();

                        oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                        oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                        oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                        oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                    }
                    oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                    oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                }

                #endregion

                #region Populate Trailer Track details

                if (gpPacket.mTrailerTrack != null)
                {
                    if (gpPacket.mTrailerTrack.oTrailers != null)
                    {
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 1)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 2)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 3)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 4)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                            }
                        }
                        if (gpPacket.mTrailerTrack.oTrailers.Count >= 5)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                            }
                        }
                    }
                }

                #endregion

                #region Mass

                oLiveUpdate.fTotalMass = v.TotalMass;
                oLiveUpdate.fWeightLimit = v.WeightLimit;

                #endregion

                #region Populate the plugin field to indicate if this messages is commingout of flash

                // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                bFlashInfo = new byte[2];
                if (gpPacket._receivedFlashAvailable)
                    bFlashInfo[0] = 0x01;
                else
                    bFlashInfo[0] = 0x00;
                if (gpPacket.bArchivalData)
                    bFlashInfo[1] = 0x01;
                else
                    bFlashInfo[1] = 0x00;
                oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                oLiveUpdate.AddPluginDataField(oPluginData);

                #endregion
                #region Populate the plugin field to add any extra GPS or G-Force information - Plugin data is only attached if values are present.

                if (gpPacket.mECMAdvanced != null)
                {

                    if (gpPacket.mECMAdvanced.SatellitesHDOP() > 0 || gpPacket.mECMAdvanced.SatellitesInView() > 0 || gpPacket.mECMAdvanced.Altitude() > 0)
                    {
                        MemoryStream oMS = new MemoryStream();
                        oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                        oMS.WriteByte(gpPacket.mECMAdvanced.SatellitesInView());
                        oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.Altitude()), 0, 2);
                        oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                    if (gpPacket.mECMAdvanced.GForceZAxis() > 0 || gpPacket.mECMAdvanced.GForceTemperature() > 0)
                    {
                        MemoryStream oMS = new MemoryStream();
                        oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                        oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                        oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                }

                #endregion
                #region Populate the plugin field to provide the current route data.

                if (gpPacket.mExtendedVariableDetails != null && gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                {
                    byte[] bRouteInfo = new byte[8];
                    byte[] bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                    bRouteInfo[0] = bConv[0];
                    bRouteInfo[1] = bConv[1];
                    bRouteInfo[2] = bConv[2];
                    bRouteInfo[3] = bConv[3];
                    bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                    bRouteInfo[4] = bConv[0];
                    bRouteInfo[5] = bConv[1];
                    bRouteInfo[6] = bConv[2];
                    bRouteInfo[7] = bConv[3];
                    oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                    oLiveUpdate.AddPluginDataField(oPluginData);
                }

                #endregion
                #region Populate the plugin field to provide the current route data.

                if (gpPacket.mTransportExtraDetails != null && gpPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                {
                    byte[] bCANVehicleSpeed = new byte[4];
                    byte[] bConv = BitConverter.GetBytes(gpPacket.mTransportExtraDetails.iCANVehicleSpeed);
                    bCANVehicleSpeed[0] = bConv[0];
                    bCANVehicleSpeed[1] = bConv[1];
                    bCANVehicleSpeed[2] = bConv[2];
                    bCANVehicleSpeed[3] = bConv[3];
                    oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                    oLiveUpdate.AddPluginDataField(oPluginData);
                }

                #endregion

                #region Populate the plugin field to provide more accurate speed readings
                if (gpPacket.mECMAdvanced != null)
                {
                    float speedAccuracy = gpPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                    float maxSpeedAccuracy = gpPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                    if ((speedAccuracy > 0 && gpPacket.mFix != null) || (maxSpeedAccuracy > 0 && gpPacket.mDistance != null))
                    {
                        if (gpPacket.mFix != null)
                            speedAccuracy += ((float)((int)gpPacket.mFix.cSpeed));
                        else
                            speedAccuracy = 0;

                        if (gpPacket.mDistance != null)
                            maxSpeedAccuracy += ((float)((int)gpPacket.mDistance.cMaxSpeed));
                        else
                            maxSpeedAccuracy = 0;

                        MemoryStream oMS = new MemoryStream();
                        oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                        oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                        oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                }
                #endregion

                #region Populate Dss Event Info

                if (dssEventId > 0)
                {
                    using (MemoryStream memoryStream = new MemoryStream())
                    {
                        memoryStream.Write(BitConverter.GetBytes(dssEventId), 0, 8);
                        oPluginData = new cPluginData(PluginIds.DssEventInfo, memoryStream.ToArray());
                        oLiveUpdate.AddPluginDataField(oPluginData);
                    }
                }

                #endregion

                sErrMsg = "";
                byte[] bVer5Format = oLiveUpdate.Encode(ref sErrMsg);

                if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                SendSMSEmail(oLiveUpdate);
                if (sErrMsg == "")
                {
                    oKeys.Add((double)5.0);
                    oMsgVariants.Add((double)5.0, bVer5Format);
                }
                else
                    WriteToConsole(sErrMsg);

                #endregion

                TellClients(gpPacket.cFleetId, oMsgVariants, oKeys, false, false);
                UpdateMySQL(Convert.ToInt32(gpPacket.iVehicleId), gpPacket.mFix);
                v.MostRecentLiveUpdatePacket = gpPacket;
            }
            catch (System.Exception ex)
            {
                WriteToConsole(ex.Message);
            }
        }

        /// <summary>
        /// This function lets the clients know they need to refresh their local lists because a user has changed a GlobalStar unit's details.
        /// </summary>
        /// <param name="fleetID"></param>
        public void NotifyClientsOfGlobalStarUnitChange(string sFleetID, string sGlobalStarUnitID)
        {
            try
            {
                NotifyClientsOfGlobalStarUnitChange(Convert.ToInt32(sFleetID), Convert.ToInt32(sGlobalStarUnitID));
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        public void NotifyClientsOfGlobalStarUnitChange(int iFleetID, int iGlobalStarUnitID)
        {
            try
            {
                TellClients(iFleetID, "GLOBALSTAR_CHANGED," + Convert.ToString(iGlobalStarUnitID), false, true);
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        private bool AssertTrue(bool condition, string message)
        {
            if (condition)
                return true;
            else
            {
                _log.Info(message);
                return false;
            }
        }

        private Hashtable VerifyVariants(Hashtable newVariants, Hashtable oldVariants)
        {
            bool valid = AssertTrue((newVariants.Keys.Count == oldVariants.Keys.Count), string.Format("Variant KeyCode incorrect New : {0}; Old : {1}", newVariants.Keys.Count, oldVariants.Keys.Count));
            if (valid)
            {
                foreach (DictionaryEntry entry in newVariants)
                {
                    string message = (string)oldVariants[entry.Key];
                    valid = AssertTrue((message != null), string.Format("Key '{0}' does not exist", entry.Key));
                    if (!valid) break;

                    valid = AssertTrue(message == (string)entry.Value, string.Format("Message for Key '{0}' differs : New {1}; Old {2}", entry.Key, (string)entry.Value, message));
                    if (!valid) break;
                }
            }

            if (valid)
                return newVariants;
            else
                return oldVariants;
        }

        public void NotifyClientsOfPacket(GatewayProtocolPacket aPacket, Vehicle v, bool bUpdateFromFlash)
        {
            string s;
            int iMessageType;
            string sUserDefinedValue = "";
            string sVehicleTag = "";
            string sKey = "";
            string sVer27Format = "";
            string sVer271Format = "";
            string sVer28Format = "";
            string sVer29Format = "";
            string sVer30Format = "";
            string sTrailerName = "";
            DateTime oTestDate = DateTime.Now;
            DateTime oCurrentDate = DateTime.Now;
            TimeSpan oTS = new TimeSpan(0);
            double iHoursDiff = 0;
            ArrayList oKeys = new ArrayList();
            Hashtable oMsgVariants = new Hashtable();
            string sErrMsg = "";
            LiveUpdate oLiveUpdate = null;
            byte[] bVer4Format = null;
            byte[] bFlashInfo = null;
            cPluginData oPluginData = null;
            DateTime dtReportTime = DateTime.MinValue;

            try
            {
                // Allow a message to be blocked from client's eyes:
                if (v.SwallowThisPacket) return;

                iMessageType = aPacket.cMsgType;

                if (aPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                {
                    iMessageType = (int)GeneralGPPacket.STATUS_REPORT;
                }

                //create PluginData for Combined Scheduled Task
                TaskStates taskState = v.GetCombinedTaskStates();
                cPluginData scheduleTaskPluginData = null;
                if (taskState != TaskStates.Unknown)
                {
                    scheduleTaskPluginData = new cPluginData(PluginIds.TaskState, new byte[] { (byte)taskState });
                }

                GeneralGPPacket gpPacket = null;
                switch (iMessageType & 0xF0)
                {
                    case GeneralGPPacket.GEN_PACKET_MASK:
                    case GeneralGPPacket.STATUS_PACKET_MASK:
                    case GeneralGPPacket.ENGINE_PACKET_MASK:
                    case GeneralGPPacket.BARREL_PACKET_MASK:
                    case GeneralGPPacket.EXTENDED_IO_PACKET_MASK:
                    case GeneralGPPacket.ZONE_ALERT_MESSAGES:
                    case GeneralGPPacket.GEN_NO_GPS_PACKET_MASK:
                    case GeneralGPPacket.STATUS_VEHICLE_PACKET_MASK:
                    case GeneralGPPacket.GEN_IM_ALIVE_IMIE:

                        #region General Packet Live Updates

                        #region Avoid notifying on irrelevant packets:

                        if (aPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                        {
                            GeneralGPPacket oReferPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                            iMessageType = (oReferPacket.cRefrigerationZone << 16) + (oReferPacket.cInputNumber << 8) + GeneralGPPacket.REFRIG_REPORT;
                        }
                        if (aPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                        {
                            GeneralGPPacket gpDOS = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                            // The cInputNumber field holds the speed zone flag for this packet type, 
                            iMessageType = ((gpDOS.bSpeedZoneID & 0x0F) << 12) + ((gpDOS.bSpeedZoneFlags & 0x0F) << 8) + (gpDOS.cMsgType & 0xFF);
                        }
                        switch (iMessageType)
                        {
                            case GeneralGPPacket.GEN_IM_ALIVE_FULL:
                            case GeneralGPPacket.GEN_IM_ALIVE:
                            case GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN:
                            case GeneralGPPacket.GEN_STARTUP:
                            case GeneralGPPacket.GEN_RESET:
                            case GeneralGPPacket.GEN_SLEEPING:
                            case GeneralGPPacket.GSP_APP_PING:
                            case GeneralGPPacket.GEN_ACK:
                            case GeneralGPPacket.SYSTEM_UNIT_RESET:
                            case GeneralGPPacket.GEN_NAK:
                            case GeneralGPPacket.STATUS_RX_TX:
                            case GeneralGPPacket.STATUS_RX_TX_SATTELITE:
                                // Do nothing cos there's no real info change
                                return;
                            default:
                                // Keep going
                                break;
                        }

                        #endregion

                        if (aPacket is GeneralGPPacket)
                            gpPacket = (GeneralGPPacket)aPacket;
                        else
                            gpPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);

                        #region Replace Engine values with advanced values where applicable.

                        try
                        {
                            if (gpPacket.mECMAdvanced != null)
                            {
                                #region mTransportExtraDetails
                                if (v.UseGPSOdometer && gpPacket.mECMAdvanced.Odometer() > 0)
                                {
                                    gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mECMAdvanced.GPSOdometer() + v.OdometerOffset);
                                }
                                else if (gpPacket.mECMAdvanced.Odometer() > 0)
                                {
                                    if (gpPacket.mECMAdvanced.Odometer() + v.OdometerOffset > 0)
                                        gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mECMAdvanced.Odometer() + v.OdometerOffset);
                                    else
                                        gpPacket.mTransportExtraDetails.iOdometer = 0;
                                }
                                else if (gpPacket.mTransportExtraDetails.iOdometer > 0)
                                {
                                    if ((gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                        gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                                    else
                                        gpPacket.mTransportExtraDetails.iOdometer = 0;
                                }
                                gpPacket.mTransportExtraDetails.iEngineHours = GetTotalEngineHours(gpPacket.mECMAdvanced, v);
                                if (gpPacket.mECMAdvanced.TotalFuelUsed() > 0) gpPacket.mTransportExtraDetails.iTotalFuelUsed = (int)gpPacket.mECMAdvanced.TotalFuelUsed();
                                if (gpPacket.mECMAdvanced.FuelEconomy() > 0) gpPacket.mTransportExtraDetails.fFuelEconomy = Convert.ToSingle(gpPacket.mECMAdvanced.FuelEconomy());
                                if (gpPacket.mECMAdvanced.BatteryVolts() > 0) gpPacket.mTransportExtraDetails.iBatteryVolts = gpPacket.mECMAdvanced.BatteryVolts();
                                if (gpPacket.mECMAdvanced.BrakeUsageSeconds() > 0) gpPacket.mTransportExtraDetails.iBrakeUsageSeconds = gpPacket.mECMAdvanced.BrakeUsageSeconds();
                                if (gpPacket.mECMAdvanced.ECMVehicleSpeed(true) > 0) gpPacket.mTransportExtraDetails.iCANVehicleSpeed = gpPacket.mECMAdvanced.ECMVehicleSpeed(true);
                                // RPM Zone Readings
                                if (gpPacket.mECMAdvanced.RPMZone1() > 0) gpPacket.mTransportExtraDetails.iRPMZone1 = gpPacket.mECMAdvanced.RPMZone1();
                                if (gpPacket.mECMAdvanced.RPMZone2() > 0) gpPacket.mTransportExtraDetails.iRPMZone2 = gpPacket.mECMAdvanced.RPMZone2();
                                if (gpPacket.mECMAdvanced.RPMZone3() > 0) gpPacket.mTransportExtraDetails.iRPMZone3 = gpPacket.mECMAdvanced.RPMZone3();
                                if (gpPacket.mECMAdvanced.RPMZone4() > 0) gpPacket.mTransportExtraDetails.iRPMZone4 = gpPacket.mECMAdvanced.RPMZone4();
                                // Speed Zone Readings
                                if (gpPacket.mECMAdvanced.SpeedZone1() > 0) gpPacket.mTransportExtraDetails.iSpeedZone1 = gpPacket.mECMAdvanced.SpeedZone1();
                                if (gpPacket.mECMAdvanced.SpeedZone2() > 0) gpPacket.mTransportExtraDetails.iSpeedZone2 = gpPacket.mECMAdvanced.SpeedZone2();
                                if (gpPacket.mECMAdvanced.SpeedZone3() > 0) gpPacket.mTransportExtraDetails.iSpeedZone3 = gpPacket.mECMAdvanced.SpeedZone3();
                                if (gpPacket.mECMAdvanced.SpeedZone4() > 0) gpPacket.mTransportExtraDetails.iSpeedZone4 = gpPacket.mECMAdvanced.SpeedZone4();

                                #endregion

                                #region mEngineData

                                if (gpPacket.mECMAdvanced.MaxEngCoolTemp() > 0) gpPacket.mEngineData.iCoolantTemperature = gpPacket.mECMAdvanced.MaxEngCoolTemp();
                                if (gpPacket.mECMAdvanced.MaxEngOilTemp() > 0) gpPacket.mEngineData.iOilTemperature = gpPacket.mECMAdvanced.MaxEngOilTemp();
                                if (gpPacket.mECMAdvanced.MinOilPressure() > 0) gpPacket.mEngineData.iOilPressure = gpPacket.mECMAdvanced.MinOilPressure();
                                if (gpPacket.mECMAdvanced.GearPosition() > 0) gpPacket.mEngineData.iGear = gpPacket.mECMAdvanced.GearPosition();
                                if (gpPacket.mECMAdvanced.MaxEngSpeed() > 0) gpPacket.mEngineData.iMaxRPM = gpPacket.mECMAdvanced.MaxEngSpeed();
                                if (gpPacket.mECMAdvanced.BrakeHits() > 0) gpPacket.mEngineData.iBrakeApplications = gpPacket.mECMAdvanced.BrakeHits();
                                // G-Force Readings
                                if (gpPacket.mECMAdvanced.HasGForce)
                                {
                                    gpPacket.mEngineData.fGForceFront = Convert.ToSingle(gpPacket.mECMAdvanced.GForceFront());
                                    gpPacket.mEngineData.fGForceBack = Convert.ToSingle(gpPacket.mECMAdvanced.GForceBack());
                                    gpPacket.mEngineData.fGForceLeftRight = Convert.ToSingle(gpPacket.mECMAdvanced.GForceLeftRight());
                                }

                                #endregion

                                #region mEngineSummaryData

                                if (gpPacket.mECMAdvanced.TripFuel() > 0) gpPacket.mEngineSummaryData.iTripFuel = (int)gpPacket.mECMAdvanced.TripFuel();
                                gpPacket.mEngineSummaryData.iTotalEngineHours = GetTotalEngineHours(gpPacket.mECMAdvanced, v);
                                if (gpPacket.mECMAdvanced.FuelEconomy() > 0) gpPacket.mEngineSummaryData.fFuelEconomy = Convert.ToSingle(gpPacket.mECMAdvanced.FuelEconomy());

                                #endregion

                                #region mExtendedValues

                                gpPacket.mExtendedValues.iValue1 = gpPacket.mECMAdvanced.ExtendedIO1();
                                gpPacket.mExtendedValues.iValue2 = gpPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(gpPacket.cFleetId));
                                gpPacket.mExtendedValues.iValue3 = gpPacket.mECMAdvanced.ExtendedIO3();
                                gpPacket.mExtendedValues.iValue4 = gpPacket.mECMAdvanced.ExtendedIO4();

                                #endregion
                            }
                            else
                            {
                                if (gpPacket.mTransportExtraDetails.iOdometer > 0)
                                {
                                    if ((gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                        gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                                    else
                                        gpPacket.mTransportExtraDetails.iOdometer = 0;
                                }
                            }
                        }
                        catch (System.Exception exAdvanceECM)
                        {
                            _log.Error(sClassName + "NotifyClientsOfPacket(GatewayProtocolPacket aPacket, Vehicle v, bool bUpdateFromFlash) : F/V : " + Convert.ToInt32(gpPacket.cFleetId).ToString() 
                                + "/" + gpPacket.iVehicleId.ToString(), exAdvanceECM);
                        }

                        #endregion

                        if (gpPacket.mCurrentClock.IsDate(null))
                            dtReportTime = gpPacket.mCurrentClock.ToDateTime();
                        if (aPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF && bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                        {
                            try
                            {
                                if (v.MostRecentLiveReportTime.CompareTo(DateTime.MinValue) != 0)
                                {
                                    if (dtReportTime.CompareTo(v.MostRecentLiveReportTime.AddMinutes(-1)) > 0)
                                    {
                                        bUpdateFromFlash = false;
                                        gpPacket.bArchivalData = false;
                                    }
                                }
                            }
                            catch (System.Exception)
                            {
                                // If we don't have a valid value to compare too, leave the flags set as they are.
                            }
                        }
                        if (!bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                            v.MostRecentLiveReportTime = dtReportTime;

                        // If this is an update from a flash report and there are no clients registered to recieve updates from flash, just return.
                        if (bUpdateFromFlash && mFromFlashFleetList.Count == 0 && _luToMsmq == null && _ecmLuToMsmq == null)
                            return;

                        #region If there is no time attached to the packet, don't send it to the clients

                        if (gpPacket.mFixClock.iYear == 0 && gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                        {
                            if (gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                gpPacket.mFixClock.FromDateTime(DateTime.Now.ToUniversalTime());
                            else
                                return;
                        }
                        if (gpPacket.mCurrentClock.iMonth == 0 && gpPacket.mCurrentClock.iDay == 0 && gpPacket.mCurrentClock.iHour == 0 && gpPacket.mCurrentClock.iMinute == 0 && gpPacket.mCurrentClock.iSecond == 0)
                            if (gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                gpPacket.mCurrentClock.FromDateTime(DateTime.Now.ToUniversalTime());
                            else
                                return;

                        #endregion

                        #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                        sKey = gpPacket.cFleetId.ToString().PadLeft(5, '0') + gpPacket.iVehicleId.ToString().PadLeft(5, '0');

                        if ((gpPacket.mFix.dLatitude == 0 && gpPacket.mFix.dLongitude == 0))
                        {
                            try
                            {
                                oTestDate = new DateTime(Convert.ToInt32((gpPacket.mFixClock.iYear > 2000 ? gpPacket.mFixClock.iYear : gpPacket.mFixClock.iYear + 2000)), Convert.ToInt32(gpPacket.mFixClock.iMonth), Convert.ToInt32(gpPacket.mFixClock.iDay), Convert.ToInt32(gpPacket.mFixClock.iHour), Convert.ToInt32(gpPacket.mFixClock.iMinute), Convert.ToInt32(gpPacket.mFixClock.iSecond), 0);
                                oCurrentDate = DateTime.Now.ToUniversalTime();
                                oTS = oCurrentDate.Subtract(oTestDate);
                                iHoursDiff = oTS.TotalHours;
                                if (iHoursDiff > 24 || iHoursDiff < -24)
                                {
                                    return;
                                }
                            }
                            catch (Exception ex2)
                            {
                                _log.Error("Unable to parse the gpPacket.mFixClock", ex2);

                                _log.Error(string.Format("gpPacket.mFixClock  Year:{0}, Month:{1}, Day:{2}, Hour:{3}, Minute:{4}, Second:{5}, Vehicle:{6}, Fleet:{7}", 
                                    gpPacket.mFixClock.iYear, 
                                    gpPacket.mFixClock.iMonth, 
                                    gpPacket.mFixClock.iDay,
                                    gpPacket.mFixClock.iHour,
                                    gpPacket.mFixClock.iMinute,
                                    gpPacket.mFixClock.iSecond,
                                    gpPacket.iVehicleId,
                                    gpPacket.cFleetId));

                                _log.Error("gpPacket.mFixClock Data: " + BitConverter.ToString(gpPacket.mRawBytes));

                                // Need to consume the error as this prevents the incidents buffer events from being processed correctly
                                //throw;
                            }

                            lock (oLastKnowPosition.SyncRoot)
                            {
                                if (oLastKnowPosition.ContainsKey(sKey))
                                {
                                    string sValue = (string)oLastKnowPosition[sKey];
                                    string[] sValues = sValue.Split("~".ToCharArray());
                                    if (sValues.Length == 2)
                                    {
                                        gpPacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                        gpPacket.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                    }
                                }
                            }
                            // If we still don't have a position, don't send the update.
                            if ((gpPacket.mFix.dLatitude == 0) || (gpPacket.mFix.dLongitude == 0))
                            {
                                return;
                            }
                        }
                        else
                        {
                            // If this entry has a postiion, then set the last known position for the unit.
                            lock (oLastKnowPosition.SyncRoot)
                            {
                                if (oLastKnowPosition.ContainsKey(sKey))
                                    oLastKnowPosition.Remove(sKey);
                                oLastKnowPosition.Add(sKey, Convert.ToString(gpPacket.mFix.dLatitude) + "~" + Convert.ToString(gpPacket.mFix.dLongitude));
                            }
                        }

                        #endregion

                        #region Setup the userdefined field

                        // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                        if (_userDefinedDateFormat != null)
                            sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                        else
                            sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');

                        // Insert the count if necessary into the UserDefined field
                        if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES)
                            sUserDefinedValue += "%" + gpPacket.iPulseCount.ToString();
                        else
                            sUserDefinedValue += "%0";

                        #endregion

                        #region Change the message type for IO, Vehicle Usage and Job messages

                        if (iMessageType == GeneralGPPacket.REFRIG_REPORT)
                        {
                            iMessageType = (gpPacket.cRefrigerationZone << 16) + (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF);
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                            gpPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                            gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gpPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                            gpPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT)
                        {
                            iMessageType = (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF);
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
                        {
                            // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                            iMessageType = (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF);
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                        {
                            // The cInputNumber field holds the speed zone flag for this packet type, 
                            iMessageType = ((gpPacket.bSpeedZoneID & 0x0F) << 12) + ((gpPacket.bSpeedZoneFlags & 0x0F) << 8) + (gpPacket.cMsgType & 0xFF);
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                        {
                            // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                            iMessageType = Convert.ToInt32(((1 << ((int)gpPacket.cInputNumber)) << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if ((iMessageType == GeneralGPPacket.STATUS_IGNITION_ON) || (iMessageType == GeneralGPPacket.STATUS_IGNITION_OFF) || (iMessageType == GeneralGPPacket.STATUS_IO_PULSES) || (iMessageType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD) || (iMessageType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD))
                        {
                            iMessageType = (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF);
                        }
                        else if (iMessageType == GeneralGPPacket.STATUS_PHONECALL_PACKET || iMessageType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || iMessageType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || iMessageType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || iMessageType == GeneralGPPacket.STATUS_JOB_PACKET)
                        {
                            iMessageType = (gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF);
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
                        {
                            iMessageType = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION && gpPacket.cButtonNumber == 0x01)
                        {
                            iMessageType = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        //GpsHistory - complete packet
                        else if (gpPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY)
                        {
                            if ((int)gpPacket.GetStatus().cSpareStatus == 2)
                            {
                                //dont send standard live update, send request complete
                                _packetInterpreter_ExtendedHistoryRequestComplete(this, (int)gpPacket.cFleetId, (int)gpPacket.iVehicleId, v.CompletedExtenededHistoryRequestStart, v.CompletedExtenededHistoryRequestEnd, -1);
                                return;
                            }
                            else if ((int)gpPacket.GetStatus().cSpareStatus == 1)
                            {
                                iMessageType = Convert.ToInt32((gpPacket.GetStatus().cSpareStatus << 8) + (gpPacket.cMsgType & 0xFF));
                            }
                        } 

                        #endregion

                        #region Version 0 Live Update string

                        //Standard Format = [FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples],[Ex IO 1],[Ex IO 2],[Ex IO 3],[Ex IO 4],

                        s = gpPacket.cFleetId.ToString() + "," +
                            gpPacket.iVehicleId.ToString() + "," +
                            iMessageType + "," +
                            gpPacket.mFixClock.ToString() + "," +
                            gpPacket.mCurrentClock.ToString() + "," +
                            DateUtility.GetDateInFormat(System.DateTime.Now, _serverTime_DateFormat) + "," +
                            gpPacket.mFix.dLatitude + "," +
                            gpPacket.mFix.dLongitude + "," +
                            gpPacket.mFix.iDirection + "," +
                            gpPacket.mFix.cSpeed + "," +
                            gpPacket.sRawGPS + "," +
                            0 + "," + // Status
                            gpPacket.mFix.cFlags + "," +
                            sUserDefinedValue + "," + // User Defined
                            gpPacket.mStatus.cInputStatus + "," +
                            gpPacket.mStatus.cOutputStatus + "," +
                            gpPacket.mStatus.cLightStatus + "," +
                            gpPacket.mStatus.cButtonStatus + "," +
                            gpPacket.mDistance.cMaxSpeed + "," +
                            gpPacket.mDistance.lSpeedAccumulator + "," +
                            gpPacket.mDistance.iSamples;

                        #region Add Transport fields if appropriate:

                        if (gpPacket.mEngineData != null)
                        {
                            s += "," +
                                 gpPacket.mEngineData.iCoolantTemperature + "," +
                                 gpPacket.mEngineData.iOilTemperature + "," +
                                 gpPacket.mEngineData.iOilPressure + "," +
                                 gpPacket.mEngineData.iGear + "," +
                                 gpPacket.mEngineData.iMaxRPM + "," +
                                 gpPacket.mEngineData.iBrakeApplications + "," +
                                 gpPacket.mEngineData.fGForceFront + "," +
                                 gpPacket.mEngineData.fGForceBack + "," +
                                 gpPacket.mEngineData.fGForceLeftRight;

                            if (gpPacket.mEngineSummaryData != null)
                            {
                                s += "," +
                                     gpPacket.mEngineSummaryData.iTotalEngineHours + "," +
                                     gpPacket.mEngineSummaryData.iTotalFuel + "," +
                                     gpPacket.mEngineSummaryData.iTripFuel + "," +
                                     gpPacket.mEngineSummaryData.fFuelEconomy + "," +
                                     gpPacket.mEngineSummaryData.iOdometer;
                            }
                            else // No summary - five zeroes:
                            {
                                s += ",0,0,0,0,0";
                            }
                        }
                        else
                        {
                            s += ",0,0,0,0,0,0,0,0,0"; // No mEngineData
                            s += ",0,0,0,0,0"; // No mEngineSummaryData
                        }

                        #endregion

                        s += "," + gpPacket.mExtendedValues.ToDatabaseFormatString();
                        // Add the packet as a client version 0 variant.
                        oKeys.Add((double)0);
                        oMsgVariants.Add((double)0, s);
                        sVehicleTag = v.VehicleTag.Trim();

                        #endregion

                        #region Version 2.7 Live update string

                        //sVer27Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag]

                        sVer27Format = s + ",";
                        sVer27Format += v.oPos.Position.Trim() + ",";
                        sVer27Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer27Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer27Format += v.oPos.MapRef.Trim() + ",";
                        sVer27Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim();
                        oKeys.Add((double)2.7);
                        oMsgVariants.Add((double)2.7, sVer27Format);

                        #endregion

                        #region Version 2.71 Live update string

                        //sVer27Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag]
                        sVer271Format = s + ",";
                        sVer271Format += v.oPos.Position.Trim() + ",";
                        sVer271Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer271Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer271Format += v.oPos.MapRef.Trim() + ",";
                        sVer271Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer271Format += sVehicleTag.Replace(",", Convert.ToString((char)0x40)).Trim();
                        oKeys.Add((double)2.71);
                        oMsgVariants.Add((double)2.71, sVer271Format);

                        #endregion

                        #region Version 2.8 Live update string

                        //sVer28Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag],[Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],[Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                        //							[Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name]

                        sVer28Format = s + ",";
                        sVer28Format += v.oPos.Position.Trim() + ",";
                        sVer28Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer28Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer28Format += v.oPos.MapRef.Trim() + ",";
                        sVer28Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer28Format += sVehicleTag + ",";

                        #region Add the Transport Extra Details data to the sVer28Format update string

                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iBatteryVolts) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iBrakeUsageSeconds) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iCANVehicleSpeed) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iOdometer) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone1) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone2) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone3) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iRPMZone4) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone1) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone2) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone3) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iSpeedZone4) + ",";
                        sVer28Format += Convert.ToString(gpPacket.mTransportExtraDetails.iTotalFuelUsed) + ",";

                        #endregion

                        #region Add the Trailer Track values

                        sTrailerName = "";
                        string[] sSplit = new string[2];

                        if (gpPacket.mTrailerTrack.oTrailers.Count > 0)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (gpPacket.mTrailerTrack.oTrailers.Count > 1)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (gpPacket.mTrailerTrack.oTrailers.Count > 2)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (gpPacket.mTrailerTrack.oTrailers.Count > 3)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (gpPacket.mTrailerTrack.oTrailers.Count > 4)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";

                        #endregion

                        oKeys.Add((double)2.8);
                        oMsgVariants.Add((double)2.8, sVer28Format);

                        #endregion

                        #region Version 2.9 Live update string

                        sVer29Format = sVer28Format;

                        //sVer29Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag],[Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],[Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                        //							[Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name],[Fuel Economy]

                        sVer29Format += Convert.ToString(gpPacket.mEngineSummaryData.fFuelEconomy) + ",";
                        oKeys.Add((double)2.9);
                        oMsgVariants.Add((double)2.9, sVer29Format);

                        #endregion

                        #region Version 3.0 format

                        sVer30Format = string.Format("{0}{1},{2},{3},{4}",
                                                     sVer29Format,
                                                     v.GetUnitStateForReport(gpPacket),
                                                     v.GetAuxiliaryStateForReport(gpPacket),
                                                     gpPacket.mExtendedVariableDetails.CurrentSetPointGroup,
                                                     gpPacket.mExtendedVariableDetails.CurrentSetPointNumber);
                        oKeys.Add((double)3.0);
                        oMsgVariants.Add((double)3.0, sVer30Format);

                        #endregion

                        #region Version 4.0 format

                        oLiveUpdate = new LiveUpdate();
                        oLiveUpdate.iPacketVersion = 4;

                        #region Populate fleet/vehicle, reason and time details

                        oLiveUpdate.iFleetID = (short)gpPacket.cFleetId;
                        oLiveUpdate.iVehicleID = (int)gpPacket.iVehicleId;
                        oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                        oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;

                        if (gpPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cRefrigerationZone << 16) + (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                            gpPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                            gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gpPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                            gpPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
                        {
                            // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                        {
                            // The cInputNumber field holds the speed zone flag for this packet type, 
                            oLiveUpdate.iReasonID = Convert.ToInt32(((gpPacket.bSpeedZoneID & 0x0F) << 12) + ((gpPacket.bSpeedZoneFlags & 0x0F) << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                        {
                            // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                            oLiveUpdate.iReasonID = Convert.ToInt32(((1 << ((int)gpPacket.cInputNumber)) << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF || gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON || gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_PHONECALL_PACKET || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || gpPacket.cMsgType == GeneralGPPacket.STATUS_JOB_PACKET)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else
                        {
                            oLiveUpdate.iReasonID = (int)gpPacket.cMsgType;
                        }
                        if (gpPacket.mFixClock != null && gpPacket.mFixClock.IsDate(null))
                        {
                            oLiveUpdate.dtGPSTime = gpPacket.mFixClock.ToDateTime();
                        }
                        else
                            oLiveUpdate.dtGPSTime = DateTime.Now.ToUniversalTime();

                        if (gpPacket.mCurrentClock != null && gpPacket.mCurrentClock.IsDate(null))
                        {
                            oLiveUpdate.dtDeviceTime = gpPacket.mCurrentClock.ToDateTime();
                        }
                        else
                            oLiveUpdate.dtDeviceTime = DateTime.Now.ToUniversalTime();

                        oLiveUpdate.dtServerTime = DateTime.Now;

                        #endregion

                        #region Populate GPS details

                        if (gpPacket.mFix != null)
                        {
                            oLiveUpdate.dLat = gpPacket.mFix.dLatitude;
                            oLiveUpdate.dLong = gpPacket.mFix.dLongitude;
                            oLiveUpdate.bFlags = gpPacket.mFix.cFlags;
                            oLiveUpdate.iDirection = (short)gpPacket.mFix.iDirection;
                            oLiveUpdate.bSpeed = gpPacket.mFix.cSpeed;
                        }

                        #endregion

                        #region Populate Distance details

                        if (gpPacket.mDistance != null)
                        {
                            oLiveUpdate.lSpeedAcc = gpPacket.mDistance.lSpeedAccumulator;
                            oLiveUpdate.bMaxSpeed = gpPacket.mDistance.cMaxSpeed;
                        }

                        #endregion

                        #region Populate Status details

                        if (gpPacket.mStatus != null)
                        {
                            oLiveUpdate.bInputStatus = gpPacket.mStatus.cInputStatus;
                            oLiveUpdate.bOutputStatus = gpPacket.mStatus.cOutputStatus;
                            oLiveUpdate.iLightStatus = gpPacket.mStatus.cLightStatus;
                            oLiveUpdate.iButtonStatus = gpPacket.mStatus.cButtonStatus;
                        }

                        #endregion

                        #region Populate Engine Data details

                        if (gpPacket.mEngineData != null)
                        {
                            oLiveUpdate.iCoolantTemperature = (short)gpPacket.mEngineData.iCoolantTemperature;
                            oLiveUpdate.iOilTemp = (short)gpPacket.mEngineData.iOilTemperature;
                            oLiveUpdate.iOilPressure = (short)gpPacket.mEngineData.iOilPressure;
                            oLiveUpdate.iGear = (short)gpPacket.mEngineData.iGear;
                            oLiveUpdate.iMaxRPM = (short)gpPacket.mEngineData.iMaxRPM;
                            oLiveUpdate.iBrakeApplications = (short)gpPacket.mEngineData.iBrakeApplications;
                            oLiveUpdate.fGForceFront = gpPacket.mEngineData.fGForceFront;
                            oLiveUpdate.fGForceBack = gpPacket.mEngineData.fGForceBack;
                            oLiveUpdate.fGForceLeftRight = gpPacket.mEngineData.fGForceLeftRight;
                        }

                        #endregion

                        #region Populate Engine Summary Data details

                        if (gpPacket.mEngineSummaryData != null)
                        {
                            oLiveUpdate.iTotalEngineHours = gpPacket.mEngineSummaryData.iTotalEngineHours;
                            oLiveUpdate.iTripFuel = gpPacket.mEngineSummaryData.iTripFuel;
                            oLiveUpdate.dFuelEconomy = gpPacket.mEngineSummaryData.fFuelEconomy;
                        }

                        #endregion

                        #region Populate Extended Value details

                        if (gpPacket.mExtendedValues != null)
                        {
                            oLiveUpdate.iExtendedValue1 = gpPacket.mExtendedValues.iValue1;
                            oLiveUpdate.iExtendedValue2 = gpPacket.mExtendedValues.iValue2;
                            oLiveUpdate.iExtendedValue3 = gpPacket.mExtendedValues.iValue3;
                            oLiveUpdate.iExtendedValue4 = gpPacket.mExtendedValues.iValue4;
                        }

                        #endregion

                        #region Populate Transport Extra  details

                        if (gpPacket.mTransportExtraDetails != null)
                        {
                            oLiveUpdate.iBatteryVolts = (short)gpPacket.mTransportExtraDetails.iBatteryVolts;
                            oLiveUpdate.iBrakeUsageSeconds = (short)gpPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            oLiveUpdate.iOdometer = (uint)gpPacket.mTransportExtraDetails.iOdometer;
                            oLiveUpdate.iTotalFuelUsed = gpPacket.mTransportExtraDetails.iTotalFuelUsed;
                            oLiveUpdate.iRPMZone1 = (short)gpPacket.mTransportExtraDetails.iRPMZone1;
                            oLiveUpdate.iRPMZone2 = (short)gpPacket.mTransportExtraDetails.iRPMZone2;
                            oLiveUpdate.iRPMZone3 = (short)gpPacket.mTransportExtraDetails.iRPMZone3;
                            oLiveUpdate.iRPMZone4 = (short)gpPacket.mTransportExtraDetails.iRPMZone4;
                            oLiveUpdate.iSpeedZone1 = (short)gpPacket.mTransportExtraDetails.iSpeedZone1;
                            oLiveUpdate.iSpeedZone2 = (short)gpPacket.mTransportExtraDetails.iSpeedZone2;
                            oLiveUpdate.iSpeedZone3 = (short)gpPacket.mTransportExtraDetails.iSpeedZone3;
                            oLiveUpdate.iSpeedZone4 = (short)gpPacket.mTransportExtraDetails.iSpeedZone4;
                        }

                        #endregion

                        #region Populate Extended / Refrigeration details

                        if (gpPacket.mExtendedVariableDetails != null)
                        {
                            if (gpPacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                #region Populate Refrigeration Common Values

                                oLiveUpdate.iReferFlags = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                oLiveUpdate.dReferFuelPercentage = gpPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                oLiveUpdate.dReferBatteryVolts = gpPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                oLiveUpdate.bReferInput1 = gpPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                oLiveUpdate.bReferInput2 = gpPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                oLiveUpdate.bReferInput3 = gpPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                oLiveUpdate.bReferInput4 = gpPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                oLiveUpdate.iReferSensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                oLiveUpdate.dReferHumidity = gpPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                oLiveUpdate.fReferSensor1 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                oLiveUpdate.fReferSensor2 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                oLiveUpdate.fReferSensor3 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                oLiveUpdate.fReferSensor4 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                oLiveUpdate.fReferSensor5 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                oLiveUpdate.fReferSensor6 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                #endregion

                                #region Populate Refrigeration Zone Values

                                if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        #region Populate Zone 1 values

                                        oLiveUpdate.iReferZ1Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        oLiveUpdate.iReferZ1OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        oLiveUpdate.iReferZ1Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        oLiveUpdate.iReferZ1SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        oLiveUpdate.fReferZ1ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        oLiveUpdate.fReferZ1ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        oLiveUpdate.fReferZ1SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        oLiveUpdate.fReferZ1SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        oLiveUpdate.fReferZ1Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        oLiveUpdate.fReferZ1Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        #region Populate Zone 2 values

                                        oLiveUpdate.iReferZ2Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        oLiveUpdate.iReferZ2OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        oLiveUpdate.iReferZ2Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        oLiveUpdate.iReferZ2SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        oLiveUpdate.fReferZ2ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        oLiveUpdate.fReferZ2ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        oLiveUpdate.fReferZ2SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        oLiveUpdate.fReferZ2SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        oLiveUpdate.fReferZ2Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        oLiveUpdate.fReferZ2Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        #region Populate Zone 3 values

                                        oLiveUpdate.iReferZ3Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        oLiveUpdate.iReferZ3OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        oLiveUpdate.iReferZ3Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        oLiveUpdate.iReferZ3SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        oLiveUpdate.fReferZ3ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        oLiveUpdate.fReferZ3ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        oLiveUpdate.fReferZ3SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        oLiveUpdate.fReferZ3SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        oLiveUpdate.fReferZ3Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        oLiveUpdate.fReferZ3Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Populate Raw GPS and User Defined details

                        oLiveUpdate.sRawGPS = gpPacket.sRawGPS;
                        oLiveUpdate.sUserDefined = sUserDefinedValue;

                        #endregion

                        #region Populate Vehicle Location/labeling details

                        if (v != null)
                        {
                            oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(gpPacket);
                            oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(gpPacket);
                            if (v.oPos != null)
                            {
                                oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                if (!String.IsNullOrEmpty(gpPacket.StreetName))
                                {
                                    if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                    {
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + gpPacket.StreetName;
                                    }
                                    else
                                    {
                                        oLiveUpdate.sPlaceName = gpPacket.StreetName;
                                    }
                                }
                                else
                                    oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                            }
                            oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                            oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                        }

                        #endregion

                        #region Populate Trailer Track details

                        if (gpPacket.mTrailerTrack != null)
                        {
                            if (gpPacket.mTrailerTrack.oTrailers != null)
                            {
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 1)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 2)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 3)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 4)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 5)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Populate the plugin field to indicate if this messages is commingout of flash

                        // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                        // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                        bFlashInfo = new byte[2];
                        if (gpPacket._receivedFlashAvailable)
                            bFlashInfo[0] = 0x01;
                        else
                            bFlashInfo[0] = 0x00;
                        if (gpPacket.bArchivalData)
                            bFlashInfo[1] = 0x01;
                        else
                            bFlashInfo[1] = 0x00;
                        oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                        oLiveUpdate.AddPluginDataField(oPluginData);

                        #endregion
                        #region Populate the plugin field to add any extra GPS or G-Force information - Plugin data is only attached if values are present.

                        if (gpPacket.mECMAdvanced != null)
                        {
                            if (gpPacket.mECMAdvanced.SatellitesHDOP() > 0 || gpPacket.mECMAdvanced.SatellitesInView() > 0 || gpPacket.mECMAdvanced.Altitude() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                oMS.WriteByte(gpPacket.mECMAdvanced.SatellitesInView());
                                oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.Altitude()), 0, 2);
                                oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                            if (gpPacket.mECMAdvanced.GForceZAxis() > 0 || gpPacket.mECMAdvanced.GForceTemperature() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }

                        #endregion
                        #region Populate the plugin field to provide the current route data.

                        if (gpPacket.mExtendedVariableDetails != null && gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                        {
                            byte[] bRouteInfo = new byte[8];
                            byte[] bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                            bRouteInfo[0] = bConv[0];
                            bRouteInfo[1] = bConv[1];
                            bRouteInfo[2] = bConv[2];
                            bRouteInfo[3] = bConv[3];
                            bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                            bRouteInfo[4] = bConv[0];
                            bRouteInfo[5] = bConv[1];
                            bRouteInfo[6] = bConv[2];
                            bRouteInfo[7] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion

                        #region Populate the plugin field to provide the current route data.

                        if (gpPacket.mTransportExtraDetails != null && gpPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                        {
                            byte[] bCANVehicleSpeed = new byte[4];
                            byte[] bConv = BitConverter.GetBytes(gpPacket.mTransportExtraDetails.iCANVehicleSpeed);
                            bCANVehicleSpeed[0] = bConv[0];
                            bCANVehicleSpeed[1] = bConv[1];
                            bCANVehicleSpeed[2] = bConv[2];
                            bCANVehicleSpeed[3] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion

                        #region Populate the plugin field to provide more accurate speed readings
                        if (gpPacket.mECMAdvanced != null)
                        {
                            float speedAccuracy = gpPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                            float maxSpeedAccuracy = gpPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                            if ((speedAccuracy > 0 && gpPacket.mFix != null) || (maxSpeedAccuracy > 0 && gpPacket.mDistance != null))
                            {
                                if (gpPacket.mFix != null)
                                    speedAccuracy += ((float)((int)gpPacket.mFix.cSpeed));
                                else
                                    speedAccuracy = 0;

                                if (gpPacket.mDistance != null)
                                    maxSpeedAccuracy += ((float)((int)gpPacket.mDistance.cMaxSpeed));
                                else
                                    maxSpeedAccuracy = 0;

                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }
                        #endregion

                        sErrMsg = "";
                        bVer4Format = oLiveUpdate.Encode(ref sErrMsg);

                        //Console.WriteLine(oLiveUpdate.ToDisplayString());
                        //LiveUpdate oDecodeLiveUpdate = new LiveUpdate();
                        //oDecodeLiveUpdate.Decode(bVer4Format, ref sErrMsg);
                        //Console.WriteLine(oDecodeLiveUpdate.ToDisplayString());

                        if (sErrMsg == "")
                        {
                            oKeys.Add((double)4.0);
                            oMsgVariants.Add((double)4.0, bVer4Format);
                        }
                        else
                            WriteToConsole(sErrMsg);

                        #endregion

                        #region Version 5.0 format

                        oLiveUpdate = new LiveUpdate();
                        oLiveUpdate.iPacketVersion = 5;

                        #region Populate fleet/vehicle, reason and time details

                        oLiveUpdate.iFleetID = (short)gpPacket.cFleetId;
                        oLiveUpdate.iVehicleID = (int)gpPacket.iVehicleId;
                        oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                        oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;

                        if (gpPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cRefrigerationZone << 16) + (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                            gpPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                            gpPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gpPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                            gpPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
                        {
                            // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                        {
                            // The cInputNumber field holds the speed zone flag for this packet type, 
                            oLiveUpdate.iReasonID = Convert.ToInt32(((gpPacket.bSpeedZoneID & 0x0F) << 12) + ((gpPacket.bSpeedZoneFlags & 0x0F) << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                        {
                            // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                            oLiveUpdate.iReasonID = Convert.ToInt32(((1 << ((int)gpPacket.cInputNumber)) << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF || gpPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON || gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD || gpPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_PHONECALL_PACKET || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || gpPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || gpPacket.cMsgType == GeneralGPPacket.STATUS_JOB_PACKET)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if (gpPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION && (int)gpPacket.cButtonNumber > 0)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cButtonNumber << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else if ((int)gpPacket.GetStatus().cSpareStatus == 1)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.GetStatus().cSpareStatus << 8) + (gpPacket.cMsgType & 0xFF));
                        }
                        else
                        {
                            oLiveUpdate.iReasonID = (int)gpPacket.cMsgType;
                        }

                        if (gpPacket.mFixClock != null && gpPacket.mFixClock.IsDate(null))
                        {
                            oLiveUpdate.dtGPSTime = gpPacket.mFixClock.ToDateTime();
                        }
                        else
                            oLiveUpdate.dtGPSTime = DateTime.Now.ToUniversalTime();

                        if (gpPacket.mCurrentClock != null && gpPacket.mCurrentClock.IsDate(null))
                        {
                            oLiveUpdate.dtDeviceTime = gpPacket.mCurrentClock.ToDateTime();
                        }
                        else
                            oLiveUpdate.dtDeviceTime = DateTime.Now.ToUniversalTime();

                        oLiveUpdate.dtServerTime = DateTime.Now;

                        #endregion

                        #region Populate GPS details

                        if (gpPacket.mFix != null)
                        {
                            oLiveUpdate.dLat = gpPacket.mFix.dLatitude;
                            oLiveUpdate.dLong = gpPacket.mFix.dLongitude;
                            oLiveUpdate.bFlags = gpPacket.mFix.cFlags;
                            oLiveUpdate.iDirection = (short)gpPacket.mFix.iDirection;
                            oLiveUpdate.bSpeed = gpPacket.mFix.cSpeed;
                        }

                        #endregion

                        #region Populate Distance details

                        if (gpPacket.mDistance != null)
                        {
                            oLiveUpdate.lSpeedAcc = gpPacket.mDistance.lSpeedAccumulator;
                            oLiveUpdate.bMaxSpeed = gpPacket.mDistance.cMaxSpeed;
                        }

                        #endregion

                        #region Populate Status details

                        if (gpPacket.mStatus != null)
                        {
                            oLiveUpdate.bInputStatus = gpPacket.mStatus.cInputStatus;
                            oLiveUpdate.bOutputStatus = gpPacket.mStatus.cOutputStatus;
                            oLiveUpdate.iLightStatus = gpPacket.mStatus.cLightStatus;
                            oLiveUpdate.iButtonStatus = gpPacket.mStatus.cButtonStatus;
                        }

                        #endregion

                        #region Populate Engine Data details

                        if (gpPacket.mEngineData != null)
                        {
                            oLiveUpdate.iCoolantTemperature = (short)gpPacket.mEngineData.iCoolantTemperature;
                            oLiveUpdate.iOilTemp = (short)gpPacket.mEngineData.iOilTemperature;
                            oLiveUpdate.iOilPressure = (short)gpPacket.mEngineData.iOilPressure;
                            oLiveUpdate.iGear = (short)gpPacket.mEngineData.iGear;
                            oLiveUpdate.iMaxRPM = (short)gpPacket.mEngineData.iMaxRPM;
                            oLiveUpdate.iBrakeApplications = (short)gpPacket.mEngineData.iBrakeApplications;
                            oLiveUpdate.fGForceFront = gpPacket.mEngineData.fGForceFront;
                            oLiveUpdate.fGForceBack = gpPacket.mEngineData.fGForceBack;
                            oLiveUpdate.fGForceLeftRight = gpPacket.mEngineData.fGForceLeftRight;
                        }

                        #endregion

                        #region Populate Engine Summary Data details

                        if (gpPacket.mEngineSummaryData != null)
                        {
                            oLiveUpdate.iTotalEngineHours = gpPacket.mEngineSummaryData.iTotalEngineHours;
                            oLiveUpdate.iTripFuel = gpPacket.mEngineSummaryData.iTripFuel;
                            oLiveUpdate.dFuelEconomy = gpPacket.mEngineSummaryData.fFuelEconomy;
                        }

                        #endregion

                        #region Populate Extended Value details

                        if (gpPacket.mExtendedValues != null)
                        {
                            oLiveUpdate.iExtendedValue1 = gpPacket.mExtendedValues.iValue1;
                            oLiveUpdate.iExtendedValue2 = gpPacket.mExtendedValues.iValue2;
                            oLiveUpdate.iExtendedValue3 = gpPacket.mExtendedValues.iValue3;
                            oLiveUpdate.iExtendedValue4 = gpPacket.mExtendedValues.iValue4;
                        }

                        #endregion

                        #region Populate Transport Extra  details

                        if (gpPacket.mTransportExtraDetails != null)
                        {
                            oLiveUpdate.iBatteryVolts = (short)gpPacket.mTransportExtraDetails.iBatteryVolts;
                            oLiveUpdate.iBrakeUsageSeconds = (short)gpPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            oLiveUpdate.iOdometer = (uint)gpPacket.mTransportExtraDetails.iOdometer;
                            oLiveUpdate.iTotalFuelUsed = gpPacket.mTransportExtraDetails.iTotalFuelUsed;
                            oLiveUpdate.iRPMZone1 = (short)gpPacket.mTransportExtraDetails.iRPMZone1;
                            oLiveUpdate.iRPMZone2 = (short)gpPacket.mTransportExtraDetails.iRPMZone2;
                            oLiveUpdate.iRPMZone3 = (short)gpPacket.mTransportExtraDetails.iRPMZone3;
                            oLiveUpdate.iRPMZone4 = (short)gpPacket.mTransportExtraDetails.iRPMZone4;
                            oLiveUpdate.iSpeedZone1 = (short)gpPacket.mTransportExtraDetails.iSpeedZone1;
                            oLiveUpdate.iSpeedZone2 = (short)gpPacket.mTransportExtraDetails.iSpeedZone2;
                            oLiveUpdate.iSpeedZone3 = (short)gpPacket.mTransportExtraDetails.iSpeedZone3;
                            oLiveUpdate.iSpeedZone4 = (short)gpPacket.mTransportExtraDetails.iSpeedZone4;
                        }

                        #endregion

                        #region Populate Extended / Refrigeration details

                        if (gpPacket.mExtendedVariableDetails != null)
                        {
                            if (gpPacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                #region Populate Refrigeration Common Values

                                oLiveUpdate.iReferFlags = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                oLiveUpdate.dReferFuelPercentage = gpPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                oLiveUpdate.dReferBatteryVolts = gpPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                oLiveUpdate.bReferInput1 = gpPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                oLiveUpdate.bReferInput2 = gpPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                oLiveUpdate.bReferInput3 = gpPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                oLiveUpdate.bReferInput4 = gpPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                oLiveUpdate.iReferSensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                oLiveUpdate.dReferHumidity = gpPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                oLiveUpdate.fReferSensor1 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                oLiveUpdate.fReferSensor2 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                oLiveUpdate.fReferSensor3 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                oLiveUpdate.fReferSensor4 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                oLiveUpdate.fReferSensor5 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                oLiveUpdate.fReferSensor6 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                #endregion

                                #region Populate Refrigeration Zone Values

                                if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        #region Populate Zone 1 values

                                        oLiveUpdate.iReferZ1Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        oLiveUpdate.iReferZ1OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        oLiveUpdate.iReferZ1Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        oLiveUpdate.iReferZ1SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        oLiveUpdate.fReferZ1ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        oLiveUpdate.fReferZ1ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        oLiveUpdate.fReferZ1SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        oLiveUpdate.fReferZ1SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        oLiveUpdate.fReferZ1Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        oLiveUpdate.fReferZ1Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        #region Populate Zone 2 values

                                        oLiveUpdate.iReferZ2Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        oLiveUpdate.iReferZ2OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        oLiveUpdate.iReferZ2Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        oLiveUpdate.iReferZ2SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        oLiveUpdate.fReferZ2ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        oLiveUpdate.fReferZ2ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        oLiveUpdate.fReferZ2SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        oLiveUpdate.fReferZ2SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        oLiveUpdate.fReferZ2Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        oLiveUpdate.fReferZ2Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        #region Populate Zone 3 values

                                        oLiveUpdate.iReferZ3Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        oLiveUpdate.iReferZ3OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        oLiveUpdate.iReferZ3Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        oLiveUpdate.iReferZ3SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        oLiveUpdate.fReferZ3ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        oLiveUpdate.fReferZ3ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        oLiveUpdate.fReferZ3SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        oLiveUpdate.fReferZ3SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        oLiveUpdate.fReferZ3Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        oLiveUpdate.fReferZ3Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Populate Raw GPS and User Defined details

                        oLiveUpdate.sRawGPS = gpPacket.sRawGPS;
                        oLiveUpdate.sUserDefined = sUserDefinedValue;

                        #endregion

                        #region Populate Vehicle Location/labeling details

                        if (v != null)
                        {
                            oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(gpPacket);
                            oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(gpPacket);
                            if (v.oPos != null)
                            {
                                oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                if (!String.IsNullOrEmpty(gpPacket.StreetName))
                                {
                                    if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                    {
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + gpPacket.StreetName;
                                    }
                                    else
                                    {
                                        oLiveUpdate.sPlaceName = gpPacket.StreetName;
                                    }
                                }
                                else
                                    oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                            }

                            GetPlaceName(gpPacket, oLiveUpdate);

                            oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                            oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                        }

                        #endregion

                        #region Populate Trailer Track details

                        if (gpPacket.mTrailerTrack != null)
                        {
                            if (gpPacket.mTrailerTrack.oTrailers != null)
                            {
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 1)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 2)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 3)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 4)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                    }
                                }
                                if (gpPacket.mTrailerTrack.oTrailers.Count >= 5)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Mass

                        oLiveUpdate.fTotalMass = v.TotalMass;
                        oLiveUpdate.fWeightLimit = v.WeightLimit;

                        #endregion

                        #region Populate the plugin field to indicate if this messages is commingout of flash

                        // gpPacket._receivedFlashAvailable  on an incomming packet indicates there is more data to be unloaded from memory
                        // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                        bFlashInfo = new byte[2];
                        if (gpPacket._receivedFlashAvailable)
                            bFlashInfo[0] = 0x01;
                        else
                            bFlashInfo[0] = 0x00;
                        if (gpPacket.bArchivalData)
                            bFlashInfo[1] = 0x01;
                        else
                            bFlashInfo[1] = 0x00;
                        oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                        oLiveUpdate.AddPluginDataField(oPluginData);

                        #endregion
                        #region Populate the plugin field to indicate the HDOP and number of satellites

                        if (gpPacket.mECMAdvanced != null)
                        {
                            if (gpPacket.mECMAdvanced.SatellitesHDOP() > 0 || gpPacket.mECMAdvanced.SatellitesInView() > 0 || gpPacket.mECMAdvanced.Altitude() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                oMS.WriteByte(gpPacket.mECMAdvanced.SatellitesInView());
                                oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.Altitude()), 0, 2);
                                oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                            if (gpPacket.mECMAdvanced.GForceZAxis() > 0 || gpPacket.mECMAdvanced.GForceTemperature() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }

                        #endregion
                        #region Populate the plugin field to provide the current route data.

                        if (gpPacket.mExtendedVariableDetails != null && gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                        {
                            byte[] bRouteInfo = new byte[8];
                            byte[] bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                            bRouteInfo[0] = bConv[0];
                            bRouteInfo[1] = bConv[1];
                            bRouteInfo[2] = bConv[2];
                            bRouteInfo[3] = bConv[3];
                            bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                            bRouteInfo[4] = bConv[0];
                            bRouteInfo[5] = bConv[1];
                            bRouteInfo[6] = bConv[2];
                            bRouteInfo[7] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to CAN vehicle speed.

                        if (gpPacket.mTransportExtraDetails != null && gpPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                        {
                            byte[] bCANVehicleSpeed = new byte[4];
                            byte[] bConv = BitConverter.GetBytes(gpPacket.mTransportExtraDetails.iCANVehicleSpeed);
                            bCANVehicleSpeed[0] = bConv[0];
                            bCANVehicleSpeed[1] = bConv[1];
                            bCANVehicleSpeed[2] = bConv[2];
                            bCANVehicleSpeed[3] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to provide more accurate speed readings
                        if (gpPacket.mECMAdvanced != null)
                        {
                            float speedAccuracy = gpPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                            float maxSpeedAccuracy = gpPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                            if ((speedAccuracy > 0 && gpPacket.mFix != null) || (maxSpeedAccuracy > 0 && gpPacket.mDistance != null))
                            {
                                if (gpPacket.mFix != null)
                                    speedAccuracy += ((float)((int)gpPacket.mFix.cSpeed));
                                else
                                    speedAccuracy = 0;

                                if (gpPacket.mDistance != null)
                                    maxSpeedAccuracy += ((float)((int)gpPacket.mDistance.cMaxSpeed));
                                else
                                    maxSpeedAccuracy = 0;

                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }
                        #endregion
                        if (scheduleTaskPluginData != null)
                        {
                            oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                        }
                        #region Populate the plugin field to provide breach max speed data
                        if (gpPacket.mECMAdvanced != null && gpPacket.mECMAdvanced.ECMAdvancedItems != null)
                        {
                            AddOverSpeedBreachInfo(gpPacket, oLiveUpdate);
                        }
                        #endregion

                        sErrMsg = "";
                        byte[] bVer5Format = oLiveUpdate.Encode(ref sErrMsg);
                        if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                        if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                        SendSMSEmail(oLiveUpdate);
                        if (sErrMsg == "")
                        {
                            oKeys.Add((double)5.0);
                            oMsgVariants.Add((double)5.0, bVer5Format);
                        }
                        else
                            WriteToConsole(sErrMsg);

                        #endregion

                        TellClients(gpPacket.cFleetId, oMsgVariants, oKeys, false, false);
                        UpdateMySQL(Convert.ToInt32(gpPacket.iVehicleId), gpPacket.mFix);
                        v.MostRecentLiveUpdatePacket = gpPacket;
                        break;

                        #endregion

                    case RoutePtSetPtGPPacket.SETPT_PACKET_MASK:

                        #region Set Point Packet Live Updates

                        RoutePtSetPtGPPacket pointPacket = new RoutePtSetPtGPPacket(aPacket, _serverTime_DateFormat);

                        #region Replace Engine values with advanced values where applicable.

                        try
                        {
                            if (pointPacket.mECMAdvanced != null)
                            {
                                #region mTransportExtraDetails
                                if (v.UseGPSOdometer && pointPacket.mECMAdvanced.Odometer() > 0)
                                {
                                    pointPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(pointPacket.mECMAdvanced.GPSOdometer() + v.OdometerOffset);
                                }
                                else if (pointPacket.mECMAdvanced.Odometer() > 0)
                                {
                                    if (pointPacket.mECMAdvanced.Odometer() + v.OdometerOffset > 0)
                                        pointPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(pointPacket.mECMAdvanced.Odometer() + v.OdometerOffset);
                                    else
                                        pointPacket.mTransportExtraDetails.iOdometer = 0;
                                }
                                pointPacket.mTransportExtraDetails.iEngineHours = GetTotalEngineHours(pointPacket.mECMAdvanced, v);
                                if (pointPacket.mECMAdvanced.TotalFuelUsed() > 0) pointPacket.mTransportExtraDetails.iTotalFuelUsed = (int)pointPacket.mECMAdvanced.TotalFuelUsed();
                                if (pointPacket.mECMAdvanced.FuelEconomy() > 0) pointPacket.mTransportExtraDetails.fFuelEconomy = Convert.ToSingle(pointPacket.mECMAdvanced.FuelEconomy());
                                if (pointPacket.mECMAdvanced.BatteryVolts() > 0) pointPacket.mTransportExtraDetails.iBatteryVolts = pointPacket.mECMAdvanced.BatteryVolts();
                                if (pointPacket.mECMAdvanced.BrakeUsageSeconds() > 0) pointPacket.mTransportExtraDetails.iBrakeUsageSeconds = pointPacket.mECMAdvanced.BrakeUsageSeconds();
                                // RPM Zone Readings
                                if (pointPacket.mECMAdvanced.RPMZone1() > 0) pointPacket.mTransportExtraDetails.iRPMZone1 = pointPacket.mECMAdvanced.RPMZone1();
                                if (pointPacket.mECMAdvanced.RPMZone2() > 0) pointPacket.mTransportExtraDetails.iRPMZone2 = pointPacket.mECMAdvanced.RPMZone2();
                                if (pointPacket.mECMAdvanced.RPMZone3() > 0) pointPacket.mTransportExtraDetails.iRPMZone3 = pointPacket.mECMAdvanced.RPMZone3();
                                if (pointPacket.mECMAdvanced.RPMZone4() > 0) pointPacket.mTransportExtraDetails.iRPMZone4 = pointPacket.mECMAdvanced.RPMZone4();
                                // Speed Zone Readings
                                if (pointPacket.mECMAdvanced.SpeedZone1() > 0) pointPacket.mTransportExtraDetails.iSpeedZone1 = pointPacket.mECMAdvanced.SpeedZone1();
                                if (pointPacket.mECMAdvanced.SpeedZone2() > 0) pointPacket.mTransportExtraDetails.iSpeedZone2 = pointPacket.mECMAdvanced.SpeedZone2();
                                if (pointPacket.mECMAdvanced.SpeedZone3() > 0) pointPacket.mTransportExtraDetails.iSpeedZone3 = pointPacket.mECMAdvanced.SpeedZone3();
                                if (pointPacket.mECMAdvanced.SpeedZone4() > 0) pointPacket.mTransportExtraDetails.iSpeedZone4 = pointPacket.mECMAdvanced.SpeedZone4();

                                #endregion

                                #region mEngineData

                                if (pointPacket.mECMAdvanced.MaxEngCoolTemp() > 0) pointPacket.mEngineData.iCoolantTemperature = pointPacket.mECMAdvanced.MaxEngCoolTemp();
                                if (pointPacket.mECMAdvanced.MaxEngOilTemp() > 0) pointPacket.mEngineData.iOilTemperature = pointPacket.mECMAdvanced.MaxEngOilTemp();
                                if (pointPacket.mECMAdvanced.MinOilPressure() > 0) pointPacket.mEngineData.iOilPressure = pointPacket.mECMAdvanced.MinOilPressure();
                                if (pointPacket.mECMAdvanced.GearPosition() > 0) pointPacket.mEngineData.iGear = pointPacket.mECMAdvanced.GearPosition();
                                if (pointPacket.mECMAdvanced.MaxEngSpeed() > 0) pointPacket.mEngineData.iMaxRPM = pointPacket.mECMAdvanced.MaxEngSpeed();
                                if (pointPacket.mECMAdvanced.BrakeHits() > 0) pointPacket.mEngineData.iBrakeApplications = pointPacket.mECMAdvanced.BrakeHits();
                                // G-Force Readings
                                if (pointPacket.mECMAdvanced.HasGForce)
                                {
                                    pointPacket.mEngineData.fGForceFront = Convert.ToSingle(pointPacket.mECMAdvanced.GForceFront());
                                    pointPacket.mEngineData.fGForceBack = Convert.ToSingle(pointPacket.mECMAdvanced.GForceBack());
                                    pointPacket.mEngineData.fGForceLeftRight = Convert.ToSingle(pointPacket.mECMAdvanced.GForceLeftRight());
                                }

                                #endregion

                                #region mEngineSummaryData

                                if (pointPacket.mECMAdvanced.TripFuel() > 0) pointPacket.mEngineSummaryData.iTripFuel = (int)pointPacket.mECMAdvanced.TripFuel();
                                pointPacket.mEngineSummaryData.iTotalEngineHours = GetTotalEngineHours(pointPacket.mECMAdvanced, v);
                                if (pointPacket.mECMAdvanced.FuelEconomy() > 0) pointPacket.mEngineSummaryData.fFuelEconomy = Convert.ToSingle(pointPacket.mECMAdvanced.FuelEconomy());

                                #endregion

                                #region mExtendedValues

                                pointPacket.mExtendedValues.iValue1 = pointPacket.mECMAdvanced.ExtendedIO1();
                                pointPacket.mExtendedValues.iValue2 = pointPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(pointPacket.cFleetId));
                                pointPacket.mExtendedValues.iValue3 = pointPacket.mECMAdvanced.ExtendedIO3();
                                pointPacket.mExtendedValues.iValue4 = pointPacket.mECMAdvanced.ExtendedIO4();

                                #endregion
                            }
                            else
                            {
                                if (pointPacket.mTransportExtraDetails.iOdometer > 0)
                                {
                                    if ((pointPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                        pointPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(pointPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                                    else
                                        pointPacket.mTransportExtraDetails.iOdometer = 0;
                                }
                            }
                        }
                        catch (System.Exception exAdvanceECM)
                        {
                            _log.Error(sClassName + "NotifyClientOfRemoteStatusChange(MTData.Transport.Gateway.Packet.GeneralGPPacket pointPacket, Vehicle v)", exAdvanceECM);
                        }

                        #endregion

                        if (pointPacket.mCurrentClock.IsDate(null))
                            dtReportTime = pointPacket.mCurrentClock.ToDateTime();
                        if (!bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                            v.MostRecentLiveReportTime = dtReportTime;
                        // If this is an update from a flash report and there are no clients registered to recieve updates from flash, just return.
                        if (bUpdateFromFlash && mFromFlashFleetList.Count == 0)
                            return;

                        #region If there is no time attached to the packet, don't send it to the clients

                        if (pointPacket.mFixClock.iYear == 0 && pointPacket.mFixClock.iMonth == 0 && pointPacket.mFixClock.iDay == 0 && pointPacket.mFixClock.iHour == 0 && pointPacket.mFixClock.iMinute == 0 && pointPacket.mFixClock.iSecond == 0)
                            return;
                        if (pointPacket.mFixClock.iMonth == 0 && pointPacket.mFixClock.iDay == 0 && pointPacket.mFixClock.iHour == 0 && pointPacket.mFixClock.iMinute == 0 && pointPacket.mFixClock.iSecond == 0)
                            return;

                        #endregion

                        #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                        sKey = pointPacket.cFleetId.ToString().PadLeft(5, '0') + pointPacket.iVehicleId.ToString().PadLeft(5, '0');
                        if ((pointPacket.mFix.dLatitude == 0) || (pointPacket.mFix.dLongitude == 0))
                        {
                            oTestDate = new DateTime(Convert.ToInt32(pointPacket.mFixClock.iYear) + 2000, Convert.ToInt32(pointPacket.mFixClock.iMonth), Convert.ToInt32(pointPacket.mFixClock.iDay), Convert.ToInt32(pointPacket.mFixClock.iHour), Convert.ToInt32(pointPacket.mFixClock.iMinute), Convert.ToInt32(pointPacket.mFixClock.iSecond), 0);
                            oCurrentDate = DateTime.Now.ToUniversalTime();
                            oTS = oCurrentDate.Subtract(oTestDate);
                            iHoursDiff = oTS.TotalHours;
                            if (iHoursDiff > 24 || iHoursDiff < -24)
                                return;

                            lock (oLastKnowPosition.SyncRoot)
                            {
                                if (oLastKnowPosition.ContainsKey(sKey))
                                {
                                    string sValue = (string)oLastKnowPosition[sKey];
                                    string[] sValues = sValue.Split("~".ToCharArray());
                                    if (sValues.Length == 2)
                                    {
                                        pointPacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                        pointPacket.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                    }
                                }
                            }
                            // If we still don't have a position, don't send the update.
                            if ((pointPacket.mFix.dLatitude == 0) || (pointPacket.mFix.dLongitude == 0))
                            {
                                return;
                            }
                        }
                        else
                        {
                            // If this entry has a postiion, then set the last known position for the unit.
                            lock (oLastKnowPosition.SyncRoot)
                            {
                                if (oLastKnowPosition.ContainsKey(sKey))
                                    oLastKnowPosition.Remove(sKey);
                                oLastKnowPosition.Add(sKey, Convert.ToString(pointPacket.mFix.dLatitude) + "~" + Convert.ToString(pointPacket.mFix.dLongitude));
                            }
                        }

                        #endregion

                        #region Setup the userdefined field

                        // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                        if (_userDefinedDateFormat != null)
                            sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                        else
                            sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');
                        sUserDefinedValue += "%0";

                        #endregion

                        #region Version 0 Live Update string

                        s = pointPacket.cFleetId.ToString() + "," +
                            pointPacket.iVehicleId.ToString() + "," +
                            iMessageType + "," +
                            pointPacket.mFixClock.ToString() + "," +
                            pointPacket.mCurrentClock.ToString() + "," +
                            DateUtility.GetDateInFormat(System.DateTime.Now, _serverTime_DateFormat) + "," +
                            pointPacket.mFix.dLatitude + "," +
                            pointPacket.mFix.dLongitude + "," +
                            pointPacket.mFix.iDirection + "," +
                            pointPacket.mFix.cSpeed + "," +
                            "" + "," + // point packets don't hold Raw GPS
                            pointPacket.cPointSetNumber + "," + // The Point Group Number
                            pointPacket.cPointPointNumber + "," + // The Point Number
                            sUserDefinedValue + "," + // User Defined
                            pointPacket.mStatus.cInputStatus + "," +
                            pointPacket.mStatus.cOutputStatus + "," +
                            pointPacket.mStatus.cLightStatus + "," +
                            pointPacket.mStatus.cButtonStatus + "," +
                            pointPacket.mDistance.cMaxSpeed + "," +
                            pointPacket.mDistance.lSpeedAccumulator + "," +
                            pointPacket.mDistance.iSamples;

                        #region Add Transport fields if appropriate:

                        if (pointPacket.mEngineData != null)
                        {
                            s += "," +
                                 pointPacket.mEngineData.iCoolantTemperature + "," +
                                 pointPacket.mEngineData.iOilTemperature + "," +
                                 pointPacket.mEngineData.iOilPressure + "," +
                                 pointPacket.mEngineData.iGear + "," +
                                 pointPacket.mEngineData.iMaxRPM + "," +
                                 pointPacket.mEngineData.iBrakeApplications + "," +
                                 pointPacket.mEngineData.fGForceFront + "," +
                                 pointPacket.mEngineData.fGForceBack + "," +
                                 pointPacket.mEngineData.fGForceLeftRight +
                                 ",0,0,0,0,0";
                        }

                        #endregion

                        s += "," + pointPacket.mExtendedValues.ToDatabaseFormatString();

                        // Add the packet as a client version 0 variant.
                        oKeys.Add((double)0);
                        oMsgVariants.Add((double)0, s);
                        sVehicleTag = v.VehicleTag.Trim();

                        #endregion

                        #region Version 2.7 Live update string

                        //sVer27Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag]
                        sVer27Format = s + ",";
                        sVer27Format += v.oPos.Position.Trim() + ",";
                        sVer27Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer27Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer27Format += v.oPos.MapRef.Trim() + ",";
                        sVer27Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim();
                        oKeys.Add((double)2.7);
                        oMsgVariants.Add((double)2.7, sVer27Format);

                        #endregion

                        #region Version 2.71 Live update string

                        //sVer27Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag]
                        sVer271Format = s + ",";
                        sVer271Format += v.oPos.Position.Trim() + ",";
                        sVer271Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer271Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer271Format += v.oPos.MapRef.Trim() + ",";
                        sVer271Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer271Format += sVehicleTag.Replace(",", Convert.ToString((char)0x40)).Trim();
                        oKeys.Add((double)2.71);
                        oMsgVariants.Add((double)2.71, sVer271Format);

                        #endregion

                        #region Version 2.8 Live update string

                        //sVer28Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag],[Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],[Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                        sVer28Format = s + ",";
                        sVer28Format += v.oPos.Position.Trim() + ",";
                        sVer28Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer28Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer28Format += v.oPos.MapRef.Trim() + ",";
                        sVer28Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer28Format += sVehicleTag + ",";

                        #region Add the Transport Extra Details data to the sVer28Format update string

                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iBatteryVolts) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iBrakeUsageSeconds) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iCANVehicleSpeed) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iOdometer) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iRPMZone1) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iRPMZone2) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iRPMZone3) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iRPMZone4) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iSpeedZone1) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iSpeedZone2) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iSpeedZone3) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iSpeedZone4) + ",";
                        sVer28Format += Convert.ToString(pointPacket.mTransportExtraDetails.iTotalFuelUsed) + ",";

                        #endregion

                        #region Add the Trailer Track values

                        sTrailerName = "";
                        if (pointPacket.mTrailerTrack.oTrailers.Count > 0)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[0], (int)pointPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (pointPacket.mTrailerTrack.oTrailers.Count > 1)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[1], (int)pointPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (pointPacket.mTrailerTrack.oTrailers.Count > 2)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[2], (int)pointPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (pointPacket.mTrailerTrack.oTrailers.Count > 3)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[3], (int)pointPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (pointPacket.mTrailerTrack.oTrailers.Count > 4)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[4], (int)pointPacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";

                        #endregion

                        oKeys.Add((double)2.8);
                        oMsgVariants.Add((double)2.8, sVer28Format);

                        #endregion

                        #region Version 2.9 Live update string

                        sVer29Format = sVer28Format;

                        //sVer29Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag],[Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],[Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                        //							[Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name],[Fuel Economy]

                        sVer29Format += Convert.ToString(pointPacket.mTransportExtraDetails.fFuelEconomy) + ",";
                        oKeys.Add((double)2.9);
                        oMsgVariants.Add((double)2.9, sVer29Format);

                        #endregion

                        #region Version 3.0 format

                        sVer30Format = string.Format("{0}{1},{2},{3},{4}",
                                                     sVer29Format,
                                                     v.GetUnitStateForReport(pointPacket),
                                                     v.GetAuxiliaryStateForReport(pointPacket),
                                                     v.oPos.SetPointGroupID,
                                                     v.oPos.SetPointID);
                        oKeys.Add((double)3.0);
                        oMsgVariants.Add((double)3.0, sVer30Format);

                        #endregion

                        #region Version 4.0 format

                        oLiveUpdate = new LiveUpdate();
                        oLiveUpdate.iPacketVersion = 4;

                        #region Populate fleet/vehicle, reason and time details

                        oLiveUpdate.iFleetID = (short)pointPacket.cFleetId;
                        oLiveUpdate.iVehicleID = Convert.ToInt32(pointPacket.iVehicleId);
                        oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                        oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                        oLiveUpdate.iReasonID = (int)pointPacket.cMsgType;
                        if (pointPacket.mFixClock != null)
                        {
                            oLiveUpdate.dtGPSTime = pointPacket.mFixClock.ToDateTime();
                        }
                        if (pointPacket.mCurrentClock != null)
                        {
                            oLiveUpdate.dtDeviceTime = pointPacket.mCurrentClock.ToDateTime();
                        }
                        oLiveUpdate.dtServerTime = DateTime.Now;

                        #endregion

                        #region Populate GPS details

                        if (pointPacket.mFix != null)
                        {
                            oLiveUpdate.dLat = pointPacket.mFix.dLatitude;
                            oLiveUpdate.dLong = pointPacket.mFix.dLongitude;
                            oLiveUpdate.bFlags = pointPacket.mFix.cFlags;
                            oLiveUpdate.iDirection = (short)pointPacket.mFix.iDirection;
                            oLiveUpdate.bSpeed = pointPacket.mFix.cSpeed;
                        }

                        #endregion

                        #region Populate Distance details

                        if (pointPacket.mDistance != null)
                        {
                            oLiveUpdate.lSpeedAcc = pointPacket.mDistance.lSpeedAccumulator;
                            oLiveUpdate.bMaxSpeed = pointPacket.mDistance.cMaxSpeed;
                        }

                        #endregion

                        #region Populate Status details

                        if (pointPacket.mStatus != null)
                        {
                            oLiveUpdate.bInputStatus = pointPacket.mStatus.cInputStatus;
                            oLiveUpdate.bOutputStatus = pointPacket.mStatus.cOutputStatus;
                            oLiveUpdate.iLightStatus = pointPacket.mStatus.cLightStatus;
                            oLiveUpdate.iButtonStatus = pointPacket.mStatus.cButtonStatus;
                        }

                        #endregion

                        #region Populate Engine Data details

                        if (pointPacket.mEngineData != null)
                        {
                            oLiveUpdate.iCoolantTemperature = (short)pointPacket.mEngineData.iCoolantTemperature;
                            oLiveUpdate.iOilTemp = (short)pointPacket.mEngineData.iOilTemperature;
                            oLiveUpdate.iOilPressure = (short)pointPacket.mEngineData.iOilPressure;
                            oLiveUpdate.iGear = (short)pointPacket.mEngineData.iGear;
                            oLiveUpdate.iMaxRPM = (short)pointPacket.mEngineData.iMaxRPM;
                            oLiveUpdate.iBrakeApplications = (short)pointPacket.mEngineData.iBrakeApplications;
                            oLiveUpdate.fGForceFront = pointPacket.mEngineData.fGForceFront;
                            oLiveUpdate.fGForceBack = pointPacket.mEngineData.fGForceBack;
                            oLiveUpdate.fGForceLeftRight = pointPacket.mEngineData.fGForceLeftRight;
                        }

                        #endregion

                        #region Populate Engine Summary Data details

                        //						if (pointPacket.mEngineSummaryData != null)
                        //						{
                        //							oLiveUpdate.iTotalEngineHours = pointPacket.mEngineSummaryData.iTotalEngineHours;
                        //							oLiveUpdate.iTripFuel = pointPacket.mEngineSummaryData.iTripFuel;
                        //							oLiveUpdate.dFuelEconomy =  pointPacket.mEngineSummaryData.fFuelEconomy;
                        //						}

                        #endregion

                        #region Populate Extended Value details

                        if (pointPacket.mExtendedValues != null)
                        {
                            oLiveUpdate.iExtendedValue1 = pointPacket.mExtendedValues.iValue1;
                            oLiveUpdate.iExtendedValue2 = pointPacket.mExtendedValues.iValue2;
                            oLiveUpdate.iExtendedValue3 = pointPacket.mExtendedValues.iValue3;
                            oLiveUpdate.iExtendedValue4 = pointPacket.mExtendedValues.iValue4;
                        }

                        #endregion

                        #region Populate Transport Extra  details

                        if (pointPacket.mTransportExtraDetails != null)
                        {
                            oLiveUpdate.iBatteryVolts = (short)pointPacket.mTransportExtraDetails.iBatteryVolts;
                            oLiveUpdate.iBrakeUsageSeconds = (short)pointPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            oLiveUpdate.iOdometer = (uint)pointPacket.mTransportExtraDetails.iOdometer;
                            oLiveUpdate.iTotalFuelUsed = pointPacket.mTransportExtraDetails.iTotalFuelUsed;
                            oLiveUpdate.iRPMZone1 = (short)pointPacket.mTransportExtraDetails.iRPMZone1;
                            oLiveUpdate.iRPMZone2 = (short)pointPacket.mTransportExtraDetails.iRPMZone2;
                            oLiveUpdate.iRPMZone3 = (short)pointPacket.mTransportExtraDetails.iRPMZone3;
                            oLiveUpdate.iRPMZone4 = (short)pointPacket.mTransportExtraDetails.iRPMZone4;
                            oLiveUpdate.iSpeedZone1 = (short)pointPacket.mTransportExtraDetails.iSpeedZone1;
                            oLiveUpdate.iSpeedZone2 = (short)pointPacket.mTransportExtraDetails.iSpeedZone2;
                            oLiveUpdate.iSpeedZone3 = (short)pointPacket.mTransportExtraDetails.iSpeedZone3;
                            oLiveUpdate.iSpeedZone4 = (short)pointPacket.mTransportExtraDetails.iSpeedZone4;
                        }

                        #endregion

                        #region Populate Extended / Refrigeration details

                        if (pointPacket.mExtendedVariableDetails != null)
                        {
                            if (pointPacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                #region Populate Refrigeration Common Values

                                oLiveUpdate.iReferFlags = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                oLiveUpdate.dReferFuelPercentage = pointPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                oLiveUpdate.dReferBatteryVolts = pointPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                oLiveUpdate.bReferInput1 = pointPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                oLiveUpdate.bReferInput2 = pointPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                oLiveUpdate.bReferInput3 = pointPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                oLiveUpdate.bReferInput4 = pointPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                oLiveUpdate.iReferSensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                oLiveUpdate.dReferHumidity = pointPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                oLiveUpdate.fReferSensor1 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                oLiveUpdate.fReferSensor2 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                oLiveUpdate.fReferSensor3 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                oLiveUpdate.fReferSensor4 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                oLiveUpdate.fReferSensor5 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                oLiveUpdate.fReferSensor6 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                #endregion

                                #region Populate Refrigeration Zone Values

                                if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        #region Populate Zone 1 values

                                        oLiveUpdate.iReferZ1Zone = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        oLiveUpdate.iReferZ1OperatingMode = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        oLiveUpdate.iReferZ1Alarms = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        oLiveUpdate.iReferZ1SensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        oLiveUpdate.fReferZ1ReturnAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        oLiveUpdate.fReferZ1ReturnAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        oLiveUpdate.fReferZ1SupplyAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        oLiveUpdate.fReferZ1SupplyAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        oLiveUpdate.fReferZ1Setpoint = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        oLiveUpdate.fReferZ1Evap = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        #region Populate Zone 2 values

                                        oLiveUpdate.iReferZ2Zone = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        oLiveUpdate.iReferZ2OperatingMode = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        oLiveUpdate.iReferZ2Alarms = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        oLiveUpdate.iReferZ2SensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        oLiveUpdate.fReferZ2ReturnAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        oLiveUpdate.fReferZ2ReturnAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        oLiveUpdate.fReferZ2SupplyAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        oLiveUpdate.fReferZ2SupplyAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        oLiveUpdate.fReferZ2Setpoint = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        oLiveUpdate.fReferZ2Evap = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        #region Populate Zone 3 values

                                        oLiveUpdate.iReferZ3Zone = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        oLiveUpdate.iReferZ3OperatingMode = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        oLiveUpdate.iReferZ3Alarms = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        oLiveUpdate.iReferZ3SensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        oLiveUpdate.fReferZ3ReturnAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        oLiveUpdate.fReferZ3ReturnAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        oLiveUpdate.fReferZ3SupplyAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        oLiveUpdate.fReferZ3SupplyAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        oLiveUpdate.fReferZ3Setpoint = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        oLiveUpdate.fReferZ3Evap = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Populate Raw GPS and User Defined details

                        oLiveUpdate.sUserDefined = sUserDefinedValue;

                        #endregion

                        #region Populate Vehicle Location/labeling details

                        if (v != null)
                        {
                            oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(pointPacket);
                            oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(pointPacket);

                            if (v.oPos != null)
                            {
                                oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                if (!String.IsNullOrEmpty(pointPacket.StreetName))
                                {
                                    if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                    {
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + pointPacket.StreetName;
                                    }
                                    else
                                    {
                                        oLiveUpdate.sPlaceName = pointPacket.StreetName;
                                    }
                                }
                                else
                                    oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                            }
                            oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                            oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                        }

                        #endregion

                        #region Populate Trailer Track details

                        if (pointPacket.mTrailerTrack != null)
                        {
                            if (pointPacket.mTrailerTrack.oTrailers != null)
                            {
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 1)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[0], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 2)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[1], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 3)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[2], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 4)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[3], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 5)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[4], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Populate the plugin field to indicate if this messages is commingout of flash

                        // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                        // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                        bFlashInfo = new byte[2];
                        if (pointPacket._receivedFlashAvailable)
                            bFlashInfo[0] = 0x01;
                        else
                            bFlashInfo[0] = 0x00;
                        if (pointPacket.bArchivalData)
                            bFlashInfo[1] = 0x01;
                        else
                            bFlashInfo[1] = 0x00;
                        oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                        oLiveUpdate.AddPluginDataField(oPluginData);
                        #region Populate the plugin field to indicate the HDOP and number of satellites

                        if (pointPacket.mECMAdvanced != null)
                        {
                            if (pointPacket.mECMAdvanced.SatellitesHDOP() > 0 || pointPacket.mECMAdvanced.SatellitesInView() > 0 || pointPacket.mECMAdvanced.Altitude() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(pointPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                oMS.WriteByte(pointPacket.mECMAdvanced.SatellitesInView());
                                oMS.Write(BitConverter.GetBytes(pointPacket.mECMAdvanced.Altitude()), 0, 2);
                                oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                            if (pointPacket.mECMAdvanced.GForceZAxis() > 0 || pointPacket.mECMAdvanced.GForceTemperature() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(pointPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(pointPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }

                        #endregion
                        #region Populate the plugin field to provide the current route data.

                        if (pointPacket.mExtendedVariableDetails != null && pointPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && pointPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                        {
                            byte[] bRouteInfo = new byte[8];
                            byte[] bConv = BitConverter.GetBytes(pointPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                            bRouteInfo[0] = bConv[0];
                            bRouteInfo[1] = bConv[1];
                            bRouteInfo[2] = bConv[2];
                            bRouteInfo[3] = bConv[3];
                            bConv = BitConverter.GetBytes(pointPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                            bRouteInfo[4] = bConv[0];
                            bRouteInfo[5] = bConv[1];
                            bRouteInfo[6] = bConv[2];
                            bRouteInfo[7] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to CAN vehicle speed.

                        if (pointPacket.mTransportExtraDetails != null && pointPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                        {
                            byte[] bCANVehicleSpeed = new byte[4];
                            byte[] bConv = BitConverter.GetBytes(pointPacket.mTransportExtraDetails.iCANVehicleSpeed);
                            bCANVehicleSpeed[0] = bConv[0];
                            bCANVehicleSpeed[1] = bConv[1];
                            bCANVehicleSpeed[2] = bConv[2];
                            bCANVehicleSpeed[3] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to provide more accurate speed readings
                        if (pointPacket.mECMAdvanced != null)
                        {
                            float speedAccuracy = pointPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                            float maxSpeedAccuracy = pointPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                            if ((speedAccuracy > 0 && pointPacket.mFix != null) || (maxSpeedAccuracy > 0 && pointPacket.mDistance != null))
                            {
                                if (pointPacket.mFix != null)
                                    speedAccuracy += ((float)((int)pointPacket.mFix.cSpeed));
                                else
                                    speedAccuracy = 0;

                                if (pointPacket.mDistance != null)
                                    maxSpeedAccuracy += ((float)((int)pointPacket.mDistance.cMaxSpeed));
                                else
                                    maxSpeedAccuracy = 0;

                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }
                        #endregion
                        #endregion

                        sErrMsg = "";
                        bVer4Format = oLiveUpdate.Encode(ref sErrMsg);
                        if (sErrMsg == "")
                        {
                            oKeys.Add((double)4.0);
                            oMsgVariants.Add((double)4.0, bVer4Format);
                        }
                        else
                            WriteToConsole(sErrMsg);

                        #endregion

                        #region Version 5.0 format

                        oLiveUpdate = new LiveUpdate();
                        oLiveUpdate.iPacketVersion = 5;

                        #region Populate fleet/vehicle, reason and time details

                        oLiveUpdate.iFleetID = (short)pointPacket.cFleetId;
                        oLiveUpdate.iVehicleID = Convert.ToInt32(pointPacket.iVehicleId);
                        oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                        oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                        if (pointPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_SPEEDING_AT_WP)
                        {
                            oLiveUpdate.iReasonID = Convert.ToInt32(((pointPacket.speedZoneID & 0x0F) << 12) + ((pointPacket.speedZoneFlags & 0x0F) << 8) + (pointPacket.cMsgType & 0xFF));
                        }
                        else
                        {
                            oLiveUpdate.iReasonID = (int)pointPacket.cMsgType;
                        }

                        if (pointPacket.mFixClock != null)
                        {
                            oLiveUpdate.dtGPSTime = pointPacket.mFixClock.ToDateTime();
                        }
                        if (pointPacket.mCurrentClock != null)
                        {
                            oLiveUpdate.dtDeviceTime = pointPacket.mCurrentClock.ToDateTime();
                        }
                        oLiveUpdate.dtServerTime = DateTime.Now;

                        #endregion

                        #region Populate GPS details

                        if (pointPacket.mFix != null)
                        {
                            oLiveUpdate.dLat = pointPacket.mFix.dLatitude;
                            oLiveUpdate.dLong = pointPacket.mFix.dLongitude;
                            oLiveUpdate.bFlags = pointPacket.mFix.cFlags;
                            oLiveUpdate.iDirection = (short)pointPacket.mFix.iDirection;
                            oLiveUpdate.bSpeed = pointPacket.mFix.cSpeed;
                        }

                        #endregion

                        #region Populate Distance details

                        if (pointPacket.mDistance != null)
                        {
                            oLiveUpdate.lSpeedAcc = pointPacket.mDistance.lSpeedAccumulator;
                            oLiveUpdate.bMaxSpeed = pointPacket.mDistance.cMaxSpeed;
                        }

                        #endregion

                        #region Populate Status details

                        if (pointPacket.mStatus != null)
                        {
                            oLiveUpdate.bInputStatus = pointPacket.mStatus.cInputStatus;
                            oLiveUpdate.bOutputStatus = pointPacket.mStatus.cOutputStatus;
                            oLiveUpdate.iLightStatus = pointPacket.mStatus.cLightStatus;
                            oLiveUpdate.iButtonStatus = pointPacket.mStatus.cButtonStatus;
                        }

                        #endregion

                        #region Populate Engine Data details

                        if (pointPacket.mEngineData != null)
                        {
                            oLiveUpdate.iCoolantTemperature = (short)pointPacket.mEngineData.iCoolantTemperature;
                            oLiveUpdate.iOilTemp = (short)pointPacket.mEngineData.iOilTemperature;
                            oLiveUpdate.iOilPressure = (short)pointPacket.mEngineData.iOilPressure;
                            oLiveUpdate.iGear = (short)pointPacket.mEngineData.iGear;
                            oLiveUpdate.iMaxRPM = (short)pointPacket.mEngineData.iMaxRPM;
                            oLiveUpdate.iBrakeApplications = (short)pointPacket.mEngineData.iBrakeApplications;
                            oLiveUpdate.fGForceFront = pointPacket.mEngineData.fGForceFront;
                            oLiveUpdate.fGForceBack = pointPacket.mEngineData.fGForceBack;
                            oLiveUpdate.fGForceLeftRight = pointPacket.mEngineData.fGForceLeftRight;
                        }

                        #endregion

                        #region Populate Engine Summary Data details

                        //						if (pointPacket.mEngineSummaryData != null)
                        //						{
                        //							oLiveUpdate.iTotalEngineHours = pointPacket.mEngineSummaryData.iTotalEngineHours;
                        //							oLiveUpdate.iTripFuel = pointPacket.mEngineSummaryData.iTripFuel;
                        //							oLiveUpdate.dFuelEconomy =  pointPacket.mEngineSummaryData.fFuelEconomy;
                        //						}

                        #endregion

                        #region Populate Extended Value details

                        if (pointPacket.mExtendedValues != null)
                        {
                            oLiveUpdate.iExtendedValue1 = pointPacket.mExtendedValues.iValue1;
                            oLiveUpdate.iExtendedValue2 = pointPacket.mExtendedValues.iValue2;
                            oLiveUpdate.iExtendedValue3 = pointPacket.mExtendedValues.iValue3;
                            oLiveUpdate.iExtendedValue4 = pointPacket.mExtendedValues.iValue4;
                        }

                        #endregion

                        #region Populate Transport Extra  details

                        if (pointPacket.mTransportExtraDetails != null)
                        {
                            oLiveUpdate.iBatteryVolts = (short)pointPacket.mTransportExtraDetails.iBatteryVolts;
                            oLiveUpdate.iBrakeUsageSeconds = (short)pointPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            oLiveUpdate.iOdometer = (uint)pointPacket.mTransportExtraDetails.iOdometer;
                            oLiveUpdate.iTotalFuelUsed = pointPacket.mTransportExtraDetails.iTotalFuelUsed;
                            oLiveUpdate.iRPMZone1 = (short)pointPacket.mTransportExtraDetails.iRPMZone1;
                            oLiveUpdate.iRPMZone2 = (short)pointPacket.mTransportExtraDetails.iRPMZone2;
                            oLiveUpdate.iRPMZone3 = (short)pointPacket.mTransportExtraDetails.iRPMZone3;
                            oLiveUpdate.iRPMZone4 = (short)pointPacket.mTransportExtraDetails.iRPMZone4;
                            oLiveUpdate.iSpeedZone1 = (short)pointPacket.mTransportExtraDetails.iSpeedZone1;
                            oLiveUpdate.iSpeedZone2 = (short)pointPacket.mTransportExtraDetails.iSpeedZone2;
                            oLiveUpdate.iSpeedZone3 = (short)pointPacket.mTransportExtraDetails.iSpeedZone3;
                            oLiveUpdate.iSpeedZone4 = (short)pointPacket.mTransportExtraDetails.iSpeedZone4;
                        }

                        #endregion

                        #region Populate Extended / Refrigeration details

                        if (pointPacket.mExtendedVariableDetails != null)
                        {
                            if (pointPacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                #region Populate Refrigeration Common Values

                                oLiveUpdate.iReferFlags = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                oLiveUpdate.dReferFuelPercentage = pointPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                oLiveUpdate.dReferBatteryVolts = pointPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                oLiveUpdate.bReferInput1 = pointPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                oLiveUpdate.bReferInput2 = pointPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                oLiveUpdate.bReferInput3 = pointPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                oLiveUpdate.bReferInput4 = pointPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                oLiveUpdate.iReferSensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                oLiveUpdate.dReferHumidity = pointPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                oLiveUpdate.fReferSensor1 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                oLiveUpdate.fReferSensor2 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                oLiveUpdate.fReferSensor3 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                oLiveUpdate.fReferSensor4 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                oLiveUpdate.fReferSensor5 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                oLiveUpdate.fReferSensor6 = pointPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                #endregion

                                #region Populate Refrigeration Zone Values

                                if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        #region Populate Zone 1 values

                                        oLiveUpdate.iReferZ1Zone = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        oLiveUpdate.iReferZ1OperatingMode = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        oLiveUpdate.iReferZ1Alarms = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        oLiveUpdate.iReferZ1SensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        oLiveUpdate.fReferZ1ReturnAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        oLiveUpdate.fReferZ1ReturnAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        oLiveUpdate.fReferZ1SupplyAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        oLiveUpdate.fReferZ1SupplyAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        oLiveUpdate.fReferZ1Setpoint = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        oLiveUpdate.fReferZ1Evap = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        #region Populate Zone 2 values

                                        oLiveUpdate.iReferZ2Zone = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        oLiveUpdate.iReferZ2OperatingMode = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        oLiveUpdate.iReferZ2Alarms = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        oLiveUpdate.iReferZ2SensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        oLiveUpdate.fReferZ2ReturnAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        oLiveUpdate.fReferZ2ReturnAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        oLiveUpdate.fReferZ2SupplyAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        oLiveUpdate.fReferZ2SupplyAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        oLiveUpdate.fReferZ2Setpoint = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        oLiveUpdate.fReferZ2Evap = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        #region Populate Zone 3 values

                                        oLiveUpdate.iReferZ3Zone = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        oLiveUpdate.iReferZ3OperatingMode = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        oLiveUpdate.iReferZ3Alarms = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        oLiveUpdate.iReferZ3SensorsAvaliable = (short)pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        oLiveUpdate.fReferZ3ReturnAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        oLiveUpdate.fReferZ3ReturnAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        oLiveUpdate.fReferZ3SupplyAir1 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        oLiveUpdate.fReferZ3SupplyAir2 = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        oLiveUpdate.fReferZ3Setpoint = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        oLiveUpdate.fReferZ3Evap = pointPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Populate Raw GPS and User Defined details

                        oLiveUpdate.sUserDefined = sUserDefinedValue;

                        #endregion

                        #region Populate Vehicle Location/labeling details

                        if (v != null)
                        {
                            oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(pointPacket);
                            oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(pointPacket);
                            if (v.oPos != null)
                            {
                                oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                if (!String.IsNullOrEmpty(pointPacket.StreetName))
                                {
                                    if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                    {
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + pointPacket.StreetName;
                                    }
                                    else
                                    {
                                        oLiveUpdate.sPlaceName = pointPacket.StreetName;
                                    }
                                }
                                else
                                    oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                            }

                            GetPlaceName(pointPacket, oLiveUpdate);

                            oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                            oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                        }

                        #endregion

                        #region Populate Trailer Track details

                        if (pointPacket.mTrailerTrack != null)
                        {
                            if (pointPacket.mTrailerTrack.oTrailers != null)
                            {
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 1)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[0], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 2)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[1], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 3)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[2], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 4)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[3], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                    }
                                }
                                if (pointPacket.mTrailerTrack.oTrailers.Count >= 5)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])pointPacket.mTrailerTrack.oTrailers[4], (int)pointPacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Mass

                        oLiveUpdate.fTotalMass = v.TotalMass;
                        oLiveUpdate.fWeightLimit = v.WeightLimit;

                        #endregion

                        #region Populate the plugin field to indicate if this messages is commingout of flash

                        // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                        // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                        bFlashInfo = new byte[2];
                        if (pointPacket._receivedFlashAvailable)
                            bFlashInfo[0] = 0x01;
                        else
                            bFlashInfo[0] = 0x00;
                        if (pointPacket.bArchivalData)
                            bFlashInfo[1] = 0x01;
                        else
                            bFlashInfo[1] = 0x00;
                        oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                        oLiveUpdate.AddPluginDataField(oPluginData);

                        #endregion
                        #region Populate the plugin field to add any extra GPS or G-Force information - Plugin data is only attached if values are present.

                        if (pointPacket.mECMAdvanced != null)
                        {
                            if (pointPacket.mECMAdvanced.SatellitesHDOP() > 0 || pointPacket.mECMAdvanced.SatellitesInView() > 0 || pointPacket.mECMAdvanced.Altitude() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(pointPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                oMS.WriteByte(pointPacket.mECMAdvanced.SatellitesInView());
                                oMS.Write(BitConverter.GetBytes(pointPacket.mECMAdvanced.Altitude()), 0, 2);
                                oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                            if (pointPacket.mECMAdvanced.GForceZAxis() > 0 || pointPacket.mECMAdvanced.GForceTemperature() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(pointPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(pointPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }

                        #endregion
                        #region Populate the plugin field to provide the current route data.

                        if (pointPacket.mExtendedVariableDetails != null && pointPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && pointPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                        {
                            byte[] bRouteInfo = new byte[8];
                            byte[] bConv = BitConverter.GetBytes(pointPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                            bRouteInfo[0] = bConv[0];
                            bRouteInfo[1] = bConv[1];
                            bRouteInfo[2] = bConv[2];
                            bRouteInfo[3] = bConv[3];
                            bConv = BitConverter.GetBytes(pointPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                            bRouteInfo[4] = bConv[0];
                            bRouteInfo[5] = bConv[1];
                            bRouteInfo[6] = bConv[2];
                            bRouteInfo[7] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to CAN vehicle speed.
                        if (pointPacket.mTransportExtraDetails != null && pointPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                        {
                            byte[] bCANVehicleSpeed = new byte[4];
                            byte[] bConv = BitConverter.GetBytes(pointPacket.mTransportExtraDetails.iCANVehicleSpeed);
                            bCANVehicleSpeed[0] = bConv[0];
                            bCANVehicleSpeed[1] = bConv[1];
                            bCANVehicleSpeed[2] = bConv[2];
                            bCANVehicleSpeed[3] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to provide more accurate speed readings
                        if (pointPacket.mECMAdvanced != null)
                        {
                            float speedAccuracy = pointPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                            float maxSpeedAccuracy = pointPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                            if ((speedAccuracy > 0 && pointPacket.mFix != null) || (maxSpeedAccuracy > 0 && pointPacket.mDistance != null))
                            {
                                if (pointPacket.mFix != null)
                                    speedAccuracy += ((float)((int)pointPacket.mFix.cSpeed));
                                else
                                    speedAccuracy = 0;

                                if (pointPacket.mDistance != null)
                                    maxSpeedAccuracy += ((float)((int)pointPacket.mDistance.cMaxSpeed));
                                else
                                    maxSpeedAccuracy = 0;

                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }
                        #endregion
                        if (scheduleTaskPluginData != null)
                        {
                            oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                        }

                        if (pointPacket.mECMAdvanced != null && pointPacket.mECMAdvanced.ECMAdvancedItems != null)
                        {
                            AddOverSpeedBreachInfo(pointPacket, oLiveUpdate);
                        }

                        sErrMsg = "";
                        bVer5Format = oLiveUpdate.Encode(ref sErrMsg);
                        if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                        if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                        SendSMSEmail(oLiveUpdate);
                        if (sErrMsg == "")
                        {
                            oKeys.Add((double)5.0);
                            oMsgVariants.Add((double)5.0, bVer5Format);
                        }
                        else
                            WriteToConsole(sErrMsg);

                        #endregion

                        TellClients(pointPacket.cFleetId, oMsgVariants, oKeys, false, false);
                        UpdateMySQL(Convert.ToInt32(pointPacket.iVehicleId), pointPacket.mFix);
                        v.MostRecentLiveUpdatePacket = aPacket;
                        break;

                        #endregion

                    case RoutingModuleGPPacket.ROUTES_PACKET_MASK:

                        #region Route Point Live Update Packet

                        RoutingModuleGPPacket routePacket = new RoutingModuleGPPacket(aPacket, _serverTime_DateFormat);

                        #region Replace Engine values with advanced values where applicable.

                        try
                        {
                            if (routePacket.mECMAdvanced != null)
                            {
                                #region mTransportExtraDetails
                                if (v.UseGPSOdometer && routePacket.mECMAdvanced.Odometer() > 0)
                                {
                                    routePacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(routePacket.mECMAdvanced.GPSOdometer() + v.OdometerOffset);
                                }
                                else if (routePacket.mECMAdvanced.Odometer() > 0)
                                {
                                    if (routePacket.mECMAdvanced.Odometer() + v.OdometerOffset > 0)
                                        routePacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(routePacket.mECMAdvanced.Odometer() + v.OdometerOffset);
                                    else
                                        routePacket.mTransportExtraDetails.iOdometer = 0;
                                }
                                routePacket.mTransportExtraDetails.iEngineHours = GetTotalEngineHours(routePacket.mECMAdvanced, v);
                                if (routePacket.mECMAdvanced.TotalFuelUsed() > 0) routePacket.mTransportExtraDetails.iTotalFuelUsed = (int)routePacket.mECMAdvanced.TotalFuelUsed();
                                if (routePacket.mECMAdvanced.FuelEconomy() > 0) routePacket.mTransportExtraDetails.fFuelEconomy = Convert.ToSingle(routePacket.mECMAdvanced.FuelEconomy());
                                if (routePacket.mECMAdvanced.BatteryVolts() > 0) routePacket.mTransportExtraDetails.iBatteryVolts = routePacket.mECMAdvanced.BatteryVolts();
                                if (routePacket.mECMAdvanced.BrakeUsageSeconds() > 0) routePacket.mTransportExtraDetails.iBrakeUsageSeconds = routePacket.mECMAdvanced.BrakeUsageSeconds();
                                // RPM Zone Readings
                                if (routePacket.mECMAdvanced.RPMZone1() > 0) routePacket.mTransportExtraDetails.iRPMZone1 = routePacket.mECMAdvanced.RPMZone1();
                                if (routePacket.mECMAdvanced.RPMZone2() > 0) routePacket.mTransportExtraDetails.iRPMZone2 = routePacket.mECMAdvanced.RPMZone2();
                                if (routePacket.mECMAdvanced.RPMZone3() > 0) routePacket.mTransportExtraDetails.iRPMZone3 = routePacket.mECMAdvanced.RPMZone3();
                                if (routePacket.mECMAdvanced.RPMZone4() > 0) routePacket.mTransportExtraDetails.iRPMZone4 = routePacket.mECMAdvanced.RPMZone4();
                                // Speed Zone Readings
                                if (routePacket.mECMAdvanced.SpeedZone1() > 0) routePacket.mTransportExtraDetails.iSpeedZone1 = routePacket.mECMAdvanced.SpeedZone1();
                                if (routePacket.mECMAdvanced.SpeedZone2() > 0) routePacket.mTransportExtraDetails.iSpeedZone2 = routePacket.mECMAdvanced.SpeedZone2();
                                if (routePacket.mECMAdvanced.SpeedZone3() > 0) routePacket.mTransportExtraDetails.iSpeedZone3 = routePacket.mECMAdvanced.SpeedZone3();
                                if (routePacket.mECMAdvanced.SpeedZone4() > 0) routePacket.mTransportExtraDetails.iSpeedZone4 = routePacket.mECMAdvanced.SpeedZone4();

                                #endregion

                                #region mEngineData

                                if (routePacket.mECMAdvanced.MaxEngCoolTemp() > 0) routePacket.mEngineData.iCoolantTemperature = routePacket.mECMAdvanced.MaxEngCoolTemp();
                                if (routePacket.mECMAdvanced.MaxEngOilTemp() > 0) routePacket.mEngineData.iOilTemperature = routePacket.mECMAdvanced.MaxEngOilTemp();
                                if (routePacket.mECMAdvanced.MinOilPressure() > 0) routePacket.mEngineData.iOilPressure = routePacket.mECMAdvanced.MinOilPressure();
                                if (routePacket.mECMAdvanced.GearPosition() > 0) routePacket.mEngineData.iGear = routePacket.mECMAdvanced.GearPosition();
                                if (routePacket.mECMAdvanced.MaxEngSpeed() > 0) routePacket.mEngineData.iMaxRPM = routePacket.mECMAdvanced.MaxEngSpeed();
                                if (routePacket.mECMAdvanced.BrakeHits() > 0) routePacket.mEngineData.iBrakeApplications = routePacket.mECMAdvanced.BrakeHits();
                                // G-Force Readings
                                if (routePacket.mECMAdvanced.HasGForce)
                                {
                                    routePacket.mEngineData.fGForceFront = Convert.ToSingle(routePacket.mECMAdvanced.GForceFront());
                                    routePacket.mEngineData.fGForceBack = Convert.ToSingle(routePacket.mECMAdvanced.GForceBack());
                                    routePacket.mEngineData.fGForceLeftRight = Convert.ToSingle(routePacket.mECMAdvanced.GForceLeftRight());
                                }

                                #endregion

                                #region mEngineSummaryData

                                if (routePacket.mECMAdvanced.TripFuel() > 0) routePacket.mEngineSummaryData.iTripFuel = (int)routePacket.mECMAdvanced.TripFuel();
                                routePacket.mEngineSummaryData.iTotalEngineHours = GetTotalEngineHours(routePacket.mECMAdvanced, v);
                                if (routePacket.mECMAdvanced.FuelEconomy() > 0) routePacket.mEngineSummaryData.fFuelEconomy = Convert.ToSingle(routePacket.mECMAdvanced.FuelEconomy());

                                #endregion

                                #region mExtendedValues

                                routePacket.mExtendedValues.iValue1 = routePacket.mECMAdvanced.ExtendedIO1();
                                routePacket.mExtendedValues.iValue2 = routePacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(routePacket.cFleetId));
                                routePacket.mExtendedValues.iValue3 = routePacket.mECMAdvanced.ExtendedIO3();
                                routePacket.mExtendedValues.iValue4 = routePacket.mECMAdvanced.ExtendedIO4();

                                #endregion
                            }
                            else
                            {
                                if (routePacket.mTransportExtraDetails.iOdometer > 0)
                                {
                                    if ((routePacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                        routePacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(routePacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                                    else
                                        routePacket.mTransportExtraDetails.iOdometer = 0;
                                }
                            }
                        }
                        catch (System.Exception exAdvanceECM)
                        {
                            _log.Error(sClassName + "NotifyClientOfRemoteStatusChange(MTData.Transport.Gateway.Packet.GeneralGPPacket routePacket, Vehicle v)", exAdvanceECM);
                        }

                        #endregion

                        if (routePacket.mCurrentClock.IsDate(null))
                            dtReportTime = routePacket.mCurrentClock.ToDateTime();
                        if (!bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                            v.MostRecentLiveReportTime = dtReportTime;

                        #region If there is no time attached to the packet, don't send it to the clients

                        if (routePacket.mFixClock.iYear == 0 && routePacket.mFixClock.iMonth == 0 && routePacket.mFixClock.iDay == 0 && routePacket.mFixClock.iHour == 0 && routePacket.mFixClock.iMinute == 0 && routePacket.mFixClock.iSecond == 0)
                            return;
                        if (routePacket.mFixClock.iMonth == 0 && routePacket.mFixClock.iDay == 0 && routePacket.mFixClock.iHour == 0 && routePacket.mFixClock.iMinute == 0 && routePacket.mFixClock.iSecond == 0)
                            return;

                        #endregion

                        #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                        sKey = routePacket.cFleetId.ToString().PadLeft(5, '0') + routePacket.iVehicleId.ToString().PadLeft(5, '0');
                        if ((routePacket.mFix.dLatitude == 0) || (routePacket.mFix.dLongitude == 0))
                        {
                            oTestDate = new DateTime(Convert.ToInt32(routePacket.mFixClock.iYear) + 2000, Convert.ToInt32(routePacket.mFixClock.iMonth), Convert.ToInt32(routePacket.mFixClock.iDay), Convert.ToInt32(routePacket.mFixClock.iHour), Convert.ToInt32(routePacket.mFixClock.iMinute), Convert.ToInt32(routePacket.mFixClock.iSecond), 0);
                            oCurrentDate = DateTime.Now.ToUniversalTime();
                            oTS = oCurrentDate.Subtract(oTestDate);
                            iHoursDiff = oTS.TotalHours;
                            if (iHoursDiff > 24 || iHoursDiff < -24)
                                return;

                            lock (oLastKnowPosition.SyncRoot)
                            {
                                if (oLastKnowPosition.ContainsKey(sKey))
                                {
                                    string sValue = (string)oLastKnowPosition[sKey];
                                    string[] sValues = sValue.Split("~".ToCharArray());
                                    if (sValues.Length == 2)
                                    {
                                        routePacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                        routePacket.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                    }
                                }
                            }
                            // If we still don't have a position, don't send the update.
                            if ((routePacket.mFix.dLatitude == 0) || (routePacket.mFix.dLongitude == 0))
                            {
                                return;
                            }
                        }
                        else
                        {
                            // If this entry has a postiion, then set the last known position for the unit.
                            lock (oLastKnowPosition.SyncRoot)
                            {
                                if (oLastKnowPosition.ContainsKey(sKey))
                                    oLastKnowPosition.Remove(sKey);
                                oLastKnowPosition.Add(sKey, Convert.ToString(routePacket.mFix.dLatitude) + "~" + Convert.ToString(routePacket.mFix.dLongitude));
                            }
                        }

                        #endregion

                        #region Setup the userdefined field

                        // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                        if (_userDefinedDateFormat != null)
                            sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                        else
                            sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');
                        sUserDefinedValue += "%0";

                        #endregion

                        #region Version 0 Live Update string

                        s = routePacket.cFleetId.ToString() + "," +
                            routePacket.iVehicleId.ToString() + "," +
                            iMessageType + "," +
                            routePacket.mFixClock.ToString() + "," +
                            routePacket.mCurrentClock.ToString() + "," +
                            DateUtility.GetDateInFormat(System.DateTime.Now, _serverTime_DateFormat) + "," +
                            routePacket.mFix.dLatitude + "," +
                            routePacket.mFix.dLongitude + "," +
                            routePacket.mFix.iDirection + "," +
                            routePacket.mFix.cSpeed + "," +
                            "" + "," + // point packets don't hold Raw GPS
                            routePacket.RouteNumber + "," + // The Point Group Number
                            routePacket.CheckPointIndex + "," + // The Point Number
                            sUserDefinedValue + "," + // User Defined
                            routePacket.mStatus.cInputStatus + "," +
                            routePacket.mStatus.cOutputStatus + "," +
                            routePacket.mStatus.cLightStatus + "," +
                            routePacket.mStatus.cButtonStatus + "," +
                            routePacket.mDistance.cMaxSpeed + "," +
                            routePacket.mDistance.lSpeedAccumulator + "," +
                            routePacket.mDistance.iSamples;

                        #region Add Transport fields if appropriate:

                        if (routePacket.mEngineData != null)
                        {
                            s += "," +
                                 routePacket.mEngineData.iCoolantTemperature + "," +
                                 routePacket.mEngineData.iOilTemperature + "," +
                                 routePacket.mEngineData.iOilPressure + "," +
                                 routePacket.mEngineData.iGear + "," +
                                 routePacket.mEngineData.iMaxRPM + "," +
                                 routePacket.mEngineData.iBrakeApplications + "," +
                                 routePacket.mEngineData.fGForceFront + "," +
                                 routePacket.mEngineData.fGForceBack + "," +
                                 routePacket.mEngineData.fGForceLeftRight +
                                 ",0,0,0,0,0";
                        }

                        #endregion

                        s += "," + routePacket.mExtendedValues.ToDatabaseFormatString();

                        // Add the packet as a client version 0 variant.
                        oKeys.Add((double)0);
                        oMsgVariants.Add((double)0, s);
                        sVehicleTag = v.VehicleTag.Trim();

                        #endregion

                        #region Version 2.7 Live update string

                        //sVer27Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag]
                        sVer27Format = s + ",";
                        sVer27Format += v.oPos.Position.Trim() + ",";
                        sVer27Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer27Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer27Format += v.oPos.MapRef.Trim() + ",";
                        sVer27Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim();
                        oKeys.Add((double)2.7);
                        oMsgVariants.Add((double)2.7, sVer27Format);

                        #endregion

                        #region Version 2.71 Live update string

                        //sVer27Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag]
                        sVer271Format = s + ",";
                        sVer271Format += v.oPos.Position.Trim() + ",";
                        sVer271Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer271Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer271Format += v.oPos.MapRef.Trim() + ",";
                        sVer271Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer271Format += sVehicleTag.Replace(",", Convert.ToString((char)0x40)).Trim();
                        oKeys.Add((double)2.71);
                        oMsgVariants.Add((double)2.71, sVer271Format);

                        #endregion

                        #region Version 2.8 Live update string

                        //sVer28Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag],[Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],[Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                        sVer28Format = s + ",";
                        sVer28Format += v.oPos.Position.Trim() + ",";
                        sVer28Format += v.oPos.PlaceName.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer28Format += Math.Round(v.oPos.Distance, 0) + ",";
                        sVer28Format += v.oPos.MapRef.Trim() + ",";
                        sVer28Format += v.sUserInfo.Replace(",", Convert.ToString((char)0x40)).Trim() + ",";
                        sVer28Format += sVehicleTag + ",";

                        #region Add the Transport Extra Details data to the sVer28Format update string

                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iBatteryVolts) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iBrakeUsageSeconds) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iCANVehicleSpeed) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iOdometer) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iRPMZone1) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iRPMZone2) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iRPMZone3) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iRPMZone4) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iSpeedZone1) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iSpeedZone2) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iSpeedZone3) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iSpeedZone4) + ",";
                        sVer28Format += Convert.ToString(routePacket.mTransportExtraDetails.iTotalFuelUsed) + ",";

                        #endregion

                        #region Add the Trailer Track values

                        sTrailerName = "";
                        if (routePacket.mTrailerTrack.oTrailers.Count > 0)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[0], (int)routePacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (routePacket.mTrailerTrack.oTrailers.Count > 1)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[1], (int)routePacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (routePacket.mTrailerTrack.oTrailers.Count > 2)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[2], (int)routePacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (routePacket.mTrailerTrack.oTrailers.Count > 3)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[3], (int)routePacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";
                        if (routePacket.mTrailerTrack.oTrailers.Count > 4)
                        {
                            sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[4], (int)routePacket.cFleetId);
                            sSplit = sTrailerName.Split("~".ToCharArray());
                            if (sSplit.Length == 2)
                            {
                                sVer28Format += sSplit[0] + ",";
                                sVer28Format += sSplit[1].Replace(",", "||") + ",";
                            }
                        }
                        else
                            sVer28Format += "0,,";

                        #endregion

                        oKeys.Add((double)2.8);
                        oMsgVariants.Add((double)2.8, sVer28Format);

                        #endregion

                        #region Version 2.9 Live update string

                        sVer29Format = sVer28Format;

                        //sVer29Format = "[FleetID],[VehicleID],[MsgType],[GPSTime],[DeviceTime],[ServerTime],[Lat],[Long],[Direction],[Speed],[RawGPS],[Status],[Flags],[UserDefinedValue],[InputStatus],[OutputStatus],[LightStatus],[ButtonStatus],[MaxSpeed],[SpeedAccumulator],[Samples]";
                        //							 [Direction To Place].[Place Name],[Distance From Place],[Map Ref],[User Tag],[Vehicle Tag],[Battery Volts],[Brake Usage Seconds],[CAN Vehicle Speed],[Odometer],[RPM Zone 1],[RPM Zone 2],[RPM Zone 3],[RPM Zone 4],[Speed Zone 1],[Speed Zone 2],[Speed Zone 3],[Speed Zone 4],[TotalFuelUsed],
                        //							[Trailer 1 ID],[Trailer 1 Name],[Trailer 2 ID],[Trailer 2 Name],[Trailer 3 ID],[Trailer 3 Name],[Trailer 4 ID],[Trailer 4 Name],[Trailer 5 ID],[Trailer 5 Name],[Fuel Economy]

                        sVer29Format += Convert.ToString(routePacket.mTransportExtraDetails.fFuelEconomy) + ",";
                        oKeys.Add((double)2.9);
                        oMsgVariants.Add((double)2.9, sVer29Format);

                        #endregion

                        #region Version 3.0 format

                        sVer30Format = string.Format("{0}{1},{2},{3},{4}",
                                                     sVer29Format,
                                                     v.GetUnitStateForReport(routePacket),
                                                     v.GetAuxiliaryStateForReport(routePacket),
                                                     v.oPos.SetPointGroupID,
                                                     v.oPos.SetPointID);
                        oKeys.Add((double)3.0);
                        oMsgVariants.Add((double)3.0, sVer30Format);

                        #endregion

                        #region Version 4.0 format

                        oLiveUpdate = new LiveUpdate();
                        oLiveUpdate.iPacketVersion = 4;

                        #region Populate fleet/vehicle, reason and time details

                        oLiveUpdate.iFleetID = (short)routePacket.cFleetId;
                        oLiveUpdate.iVehicleID = Convert.ToInt32(routePacket.iVehicleId);
                        oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                        oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                        oLiveUpdate.iReasonID = (int)routePacket.cMsgType;
                        if (routePacket.mFixClock != null)
                        {
                            oLiveUpdate.dtGPSTime = routePacket.mFixClock.ToDateTime();
                        }
                        if (routePacket.mCurrentClock != null)
                        {
                            oLiveUpdate.dtDeviceTime = routePacket.mCurrentClock.ToDateTime();
                        }
                        oLiveUpdate.dtServerTime = DateTime.Now;

                        #endregion

                        #region Populate GPS details

                        if (routePacket.mFix != null)
                        {
                            oLiveUpdate.dLat = routePacket.mFix.dLatitude;
                            oLiveUpdate.dLong = routePacket.mFix.dLongitude;
                            oLiveUpdate.bFlags = routePacket.mFix.cFlags;
                            oLiveUpdate.iDirection = (short)routePacket.mFix.iDirection;
                            oLiveUpdate.bSpeed = routePacket.mFix.cSpeed;
                        }

                        #endregion

                        #region Populate Distance details

                        if (routePacket.mDistance != null)
                        {
                            oLiveUpdate.lSpeedAcc = routePacket.mDistance.lSpeedAccumulator;
                            oLiveUpdate.bMaxSpeed = routePacket.mDistance.cMaxSpeed;
                        }

                        #endregion

                        #region Populate Status details

                        if (routePacket.mStatus != null)
                        {
                            oLiveUpdate.bInputStatus = routePacket.mStatus.cInputStatus;
                            oLiveUpdate.bOutputStatus = routePacket.mStatus.cOutputStatus;
                            oLiveUpdate.iLightStatus = routePacket.mStatus.cLightStatus;
                            oLiveUpdate.iButtonStatus = routePacket.mStatus.cButtonStatus;
                        }

                        #endregion

                        #region Populate Engine Data details

                        if (routePacket.mEngineData != null)
                        {
                            oLiveUpdate.iCoolantTemperature = (short)routePacket.mEngineData.iCoolantTemperature;
                            oLiveUpdate.iOilTemp = (short)routePacket.mEngineData.iOilTemperature;
                            oLiveUpdate.iOilPressure = (short)routePacket.mEngineData.iOilPressure;
                            oLiveUpdate.iGear = (short)routePacket.mEngineData.iGear;
                            oLiveUpdate.iMaxRPM = (short)routePacket.mEngineData.iMaxRPM;
                            oLiveUpdate.iBrakeApplications = (short)routePacket.mEngineData.iBrakeApplications;
                            oLiveUpdate.fGForceFront = routePacket.mEngineData.fGForceFront;
                            oLiveUpdate.fGForceBack = routePacket.mEngineData.fGForceBack;
                            oLiveUpdate.fGForceLeftRight = routePacket.mEngineData.fGForceLeftRight;
                        }

                        #endregion

                        #region Populate Engine Summary Data details

                        //						if (routePacket.mEngineSummaryData != null)
                        //						{
                        //							oLiveUpdate.iTotalEngineHours = routePacket.mEngineSummaryData.iTotalEngineHours;
                        //							oLiveUpdate.iTripFuel = routePacket.mEngineSummaryData.iTripFuel;
                        //							oLiveUpdate.dFuelEconomy =  routePacket.mEngineSummaryData.fFuelEconomy;
                        //						}

                        #endregion

                        #region Populate Extended Value details

                        if (routePacket.mExtendedValues != null)
                        {
                            oLiveUpdate.iExtendedValue1 = routePacket.mExtendedValues.iValue1;
                            oLiveUpdate.iExtendedValue2 = routePacket.mExtendedValues.iValue2;
                            oLiveUpdate.iExtendedValue3 = routePacket.mExtendedValues.iValue3;
                            oLiveUpdate.iExtendedValue4 = routePacket.mExtendedValues.iValue4;
                        }

                        #endregion

                        #region Populate Transport Extra  details

                        if (routePacket.mTransportExtraDetails != null)
                        {
                            oLiveUpdate.iBatteryVolts = (short)routePacket.mTransportExtraDetails.iBatteryVolts;
                            oLiveUpdate.iBrakeUsageSeconds = (short)routePacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            oLiveUpdate.iOdometer = (uint)routePacket.mTransportExtraDetails.iOdometer;
                            oLiveUpdate.iTotalFuelUsed = routePacket.mTransportExtraDetails.iTotalFuelUsed;
                            oLiveUpdate.iRPMZone1 = (short)routePacket.mTransportExtraDetails.iRPMZone1;
                            oLiveUpdate.iRPMZone2 = (short)routePacket.mTransportExtraDetails.iRPMZone2;
                            oLiveUpdate.iRPMZone3 = (short)routePacket.mTransportExtraDetails.iRPMZone3;
                            oLiveUpdate.iRPMZone4 = (short)routePacket.mTransportExtraDetails.iRPMZone4;
                            oLiveUpdate.iSpeedZone1 = (short)routePacket.mTransportExtraDetails.iSpeedZone1;
                            oLiveUpdate.iSpeedZone2 = (short)routePacket.mTransportExtraDetails.iSpeedZone2;
                            oLiveUpdate.iSpeedZone3 = (short)routePacket.mTransportExtraDetails.iSpeedZone3;
                            oLiveUpdate.iSpeedZone4 = (short)routePacket.mTransportExtraDetails.iSpeedZone4;
                        }

                        #endregion

                        #region Populate Extended / Refrigeration details

                        if (routePacket.mExtendedVariableDetails != null)
                        {
                            if (routePacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                #region Populate Refrigeration Common Values

                                oLiveUpdate.iReferFlags = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                oLiveUpdate.dReferFuelPercentage = routePacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                oLiveUpdate.dReferBatteryVolts = routePacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                oLiveUpdate.bReferInput1 = routePacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                oLiveUpdate.bReferInput2 = routePacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                oLiveUpdate.bReferInput3 = routePacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                oLiveUpdate.bReferInput4 = routePacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                oLiveUpdate.iReferSensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                oLiveUpdate.dReferHumidity = routePacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                oLiveUpdate.fReferSensor1 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                oLiveUpdate.fReferSensor2 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                oLiveUpdate.fReferSensor3 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                oLiveUpdate.fReferSensor4 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                oLiveUpdate.fReferSensor5 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                oLiveUpdate.fReferSensor6 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                #endregion

                                #region Populate Refrigeration Zone Values

                                if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        #region Populate Zone 1 values

                                        oLiveUpdate.iReferZ1Zone = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        oLiveUpdate.iReferZ1OperatingMode = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        oLiveUpdate.iReferZ1Alarms = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        oLiveUpdate.iReferZ1SensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        oLiveUpdate.fReferZ1ReturnAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        oLiveUpdate.fReferZ1ReturnAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        oLiveUpdate.fReferZ1SupplyAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        oLiveUpdate.fReferZ1SupplyAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        oLiveUpdate.fReferZ1Setpoint = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        oLiveUpdate.fReferZ1Evap = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        #region Populate Zone 2 values

                                        oLiveUpdate.iReferZ2Zone = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        oLiveUpdate.iReferZ2OperatingMode = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        oLiveUpdate.iReferZ2Alarms = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        oLiveUpdate.iReferZ2SensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        oLiveUpdate.fReferZ2ReturnAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        oLiveUpdate.fReferZ2ReturnAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        oLiveUpdate.fReferZ2SupplyAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        oLiveUpdate.fReferZ2SupplyAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        oLiveUpdate.fReferZ2Setpoint = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        oLiveUpdate.fReferZ2Evap = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        #region Populate Zone 3 values

                                        oLiveUpdate.iReferZ3Zone = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        oLiveUpdate.iReferZ3OperatingMode = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        oLiveUpdate.iReferZ3Alarms = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        oLiveUpdate.iReferZ3SensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        oLiveUpdate.fReferZ3ReturnAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        oLiveUpdate.fReferZ3ReturnAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        oLiveUpdate.fReferZ3SupplyAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        oLiveUpdate.fReferZ3SupplyAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        oLiveUpdate.fReferZ3Setpoint = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        oLiveUpdate.fReferZ3Evap = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Populate Raw GPS and User Defined details

                        oLiveUpdate.sUserDefined = sUserDefinedValue;

                        #endregion

                        #region Populate Vehicle Location/labeling details

                        if (v != null)
                        {
                            oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(routePacket);
                            oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(routePacket);

                            if (v.oPos != null)
                            {
                                oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                if (!String.IsNullOrEmpty(routePacket.StreetName))
                                {
                                    if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                    {
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + routePacket.StreetName;
                                    }
                                    else
                                    {
                                        oLiveUpdate.sPlaceName = routePacket.StreetName;
                                    }
                                }
                                else
                                    oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                            }
                            oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                            oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                        }

                        #endregion

                        #region Populate Trailer Track details

                        if (routePacket.mTrailerTrack != null)
                        {
                            if (routePacket.mTrailerTrack.oTrailers != null)
                            {
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 1)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[0], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 2)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[1], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 3)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[2], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 4)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[3], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 5)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[4], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Populate the plugin field to indicate if this messages is commingout of flash

                        // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                        // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                        bFlashInfo = new byte[2];
                        if (routePacket._receivedFlashAvailable)
                            bFlashInfo[0] = 0x01;
                        else
                            bFlashInfo[0] = 0x00;
                        if (routePacket.bArchivalData)
                            bFlashInfo[1] = 0x01;
                        else
                            bFlashInfo[1] = 0x00;
                        oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                        oLiveUpdate.AddPluginDataField(oPluginData);

                        #endregion
                        #region Populate the plugin field to add any extra GPS or G-Force information - Plugin data is only attached if values are present.

                        if (routePacket.mECMAdvanced != null)
                        {
                            if (routePacket.mECMAdvanced.SatellitesHDOP() > 0 || routePacket.mECMAdvanced.SatellitesInView() > 0 || routePacket.mECMAdvanced.Altitude() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(routePacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                oMS.WriteByte(routePacket.mECMAdvanced.SatellitesInView());
                                oMS.Write(BitConverter.GetBytes(routePacket.mECMAdvanced.Altitude()), 0, 2);
                                oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                            if (routePacket.mECMAdvanced.GForceZAxis() > 0 || routePacket.mECMAdvanced.GForceTemperature() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(routePacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(routePacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }

                        #endregion
                        #region Populate the plugin field to provide the current route data.

                        if (routePacket.mExtendedVariableDetails != null && routePacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && routePacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                        {
                            byte[] bRouteInfo = new byte[8];
                            byte[] bConv = BitConverter.GetBytes(routePacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                            bRouteInfo[0] = bConv[0];
                            bRouteInfo[1] = bConv[1];
                            bRouteInfo[2] = bConv[2];
                            bRouteInfo[3] = bConv[3];
                            bConv = BitConverter.GetBytes(routePacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                            bRouteInfo[4] = bConv[0];
                            bRouteInfo[5] = bConv[1];
                            bRouteInfo[6] = bConv[2];
                            bRouteInfo[7] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to CAN vehicle speed.
                        if (routePacket.mTransportExtraDetails != null && routePacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                        {
                            byte[] bCANVehicleSpeed = new byte[4];
                            byte[] bConv = BitConverter.GetBytes(routePacket.mTransportExtraDetails.iCANVehicleSpeed);
                            bCANVehicleSpeed[0] = bConv[0];
                            bCANVehicleSpeed[1] = bConv[1];
                            bCANVehicleSpeed[2] = bConv[2];
                            bCANVehicleSpeed[3] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to provide more accurate speed readings
                        if (routePacket.mECMAdvanced != null)
                        {
                            float speedAccuracy = routePacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                            float maxSpeedAccuracy = routePacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                            if ((speedAccuracy > 0 && routePacket.mFix != null) || (maxSpeedAccuracy > 0 && routePacket.mDistance != null))
                            {
                                if (routePacket.mFix != null)
                                    speedAccuracy += ((float)((int)routePacket.mFix.cSpeed));
                                else
                                    speedAccuracy = 0;

                                if (routePacket.mDistance != null)
                                    maxSpeedAccuracy += ((float)((int)routePacket.mDistance.cMaxSpeed));
                                else
                                    maxSpeedAccuracy = 0;

                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }
                        #endregion
                        sErrMsg = "";
                        bVer4Format = oLiveUpdate.Encode(ref sErrMsg);
                        if (sErrMsg == "")
                        {
                            oKeys.Add((double)4.0);
                            oMsgVariants.Add((double)4.0, bVer4Format);
                        }
                        else
                            WriteToConsole(sErrMsg);

                        #endregion

                        #region Version 5.0 format

                        oLiveUpdate = new LiveUpdate();
                        oLiveUpdate.iPacketVersion = 5;

                        #region Populate fleet/vehicle, reason and time details

                        oLiveUpdate.iFleetID = (short)routePacket.cFleetId;
                        oLiveUpdate.iVehicleID = Convert.ToInt32(routePacket.iVehicleId);
                        oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                        oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                        oLiveUpdate.iReasonID = (int)routePacket.cMsgType;
                        if (routePacket.mFixClock != null)
                        {
                            oLiveUpdate.dtGPSTime = routePacket.mFixClock.ToDateTime();
                        }
                        if (routePacket.mCurrentClock != null)
                        {
                            oLiveUpdate.dtDeviceTime = routePacket.mCurrentClock.ToDateTime();
                        }
                        oLiveUpdate.dtServerTime = DateTime.Now;

                        #endregion

                        #region Populate GPS details

                        if (routePacket.mFix != null)
                        {
                            oLiveUpdate.dLat = routePacket.mFix.dLatitude;
                            oLiveUpdate.dLong = routePacket.mFix.dLongitude;
                            oLiveUpdate.bFlags = routePacket.mFix.cFlags;
                            oLiveUpdate.iDirection = (short)routePacket.mFix.iDirection;
                            oLiveUpdate.bSpeed = routePacket.mFix.cSpeed;
                        }

                        #endregion

                        #region Populate Distance details

                        if (routePacket.mDistance != null)
                        {
                            oLiveUpdate.lSpeedAcc = routePacket.mDistance.lSpeedAccumulator;
                            oLiveUpdate.bMaxSpeed = routePacket.mDistance.cMaxSpeed;
                        }

                        #endregion

                        #region Populate Status details

                        if (routePacket.mStatus != null)
                        {
                            oLiveUpdate.bInputStatus = routePacket.mStatus.cInputStatus;
                            oLiveUpdate.bOutputStatus = routePacket.mStatus.cOutputStatus;
                            oLiveUpdate.iLightStatus = routePacket.mStatus.cLightStatus;
                            oLiveUpdate.iButtonStatus = routePacket.mStatus.cButtonStatus;
                        }

                        #endregion

                        #region Populate Engine Data details

                        if (routePacket.mEngineData != null)
                        {
                            oLiveUpdate.iCoolantTemperature = (short)routePacket.mEngineData.iCoolantTemperature;
                            oLiveUpdate.iOilTemp = (short)routePacket.mEngineData.iOilTemperature;
                            oLiveUpdate.iOilPressure = (short)routePacket.mEngineData.iOilPressure;
                            oLiveUpdate.iGear = (short)routePacket.mEngineData.iGear;
                            oLiveUpdate.iMaxRPM = (short)routePacket.mEngineData.iMaxRPM;
                            oLiveUpdate.iBrakeApplications = (short)routePacket.mEngineData.iBrakeApplications;
                            oLiveUpdate.fGForceFront = routePacket.mEngineData.fGForceFront;
                            oLiveUpdate.fGForceBack = routePacket.mEngineData.fGForceBack;
                            oLiveUpdate.fGForceLeftRight = routePacket.mEngineData.fGForceLeftRight;
                        }

                        #endregion

                        #region Populate Engine Summary Data details

                        //						if (routePacket.mEngineSummaryData != null)
                        //						{
                        //							oLiveUpdate.iTotalEngineHours = routePacket.mEngineSummaryData.iTotalEngineHours;
                        //							oLiveUpdate.iTripFuel = routePacket.mEngineSummaryData.iTripFuel;
                        //							oLiveUpdate.dFuelEconomy =  routePacket.mEngineSummaryData.fFuelEconomy;
                        //						}

                        #endregion

                        #region Populate Extended Value details

                        if (routePacket.mExtendedValues != null)
                        {
                            oLiveUpdate.iExtendedValue1 = routePacket.mExtendedValues.iValue1;
                            oLiveUpdate.iExtendedValue2 = routePacket.mExtendedValues.iValue2;
                            oLiveUpdate.iExtendedValue3 = routePacket.mExtendedValues.iValue3;
                            oLiveUpdate.iExtendedValue4 = routePacket.mExtendedValues.iValue4;
                        }

                        #endregion

                        #region Populate Transport Extra  details

                        if (routePacket.mTransportExtraDetails != null)
                        {
                            oLiveUpdate.iBatteryVolts = (short)routePacket.mTransportExtraDetails.iBatteryVolts;
                            oLiveUpdate.iBrakeUsageSeconds = (short)routePacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            oLiveUpdate.iOdometer = (uint)routePacket.mTransportExtraDetails.iOdometer;
                            oLiveUpdate.iTotalFuelUsed = routePacket.mTransportExtraDetails.iTotalFuelUsed;
                            oLiveUpdate.iRPMZone1 = (short)routePacket.mTransportExtraDetails.iRPMZone1;
                            oLiveUpdate.iRPMZone2 = (short)routePacket.mTransportExtraDetails.iRPMZone2;
                            oLiveUpdate.iRPMZone3 = (short)routePacket.mTransportExtraDetails.iRPMZone3;
                            oLiveUpdate.iRPMZone4 = (short)routePacket.mTransportExtraDetails.iRPMZone4;
                            oLiveUpdate.iSpeedZone1 = (short)routePacket.mTransportExtraDetails.iSpeedZone1;
                            oLiveUpdate.iSpeedZone2 = (short)routePacket.mTransportExtraDetails.iSpeedZone2;
                            oLiveUpdate.iSpeedZone3 = (short)routePacket.mTransportExtraDetails.iSpeedZone3;
                            oLiveUpdate.iSpeedZone4 = (short)routePacket.mTransportExtraDetails.iSpeedZone4;
                        }

                        #endregion

                        #region Populate Extended / Refrigeration details

                        if (routePacket.mExtendedVariableDetails != null)
                        {
                            if (routePacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                #region Populate Refrigeration Common Values

                                oLiveUpdate.iReferFlags = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                oLiveUpdate.dReferFuelPercentage = routePacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                oLiveUpdate.dReferBatteryVolts = routePacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                oLiveUpdate.bReferInput1 = routePacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                oLiveUpdate.bReferInput2 = routePacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                oLiveUpdate.bReferInput3 = routePacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                oLiveUpdate.bReferInput4 = routePacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                oLiveUpdate.iReferSensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                oLiveUpdate.dReferHumidity = routePacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                oLiveUpdate.fReferSensor1 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                oLiveUpdate.fReferSensor2 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                oLiveUpdate.fReferSensor3 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                oLiveUpdate.fReferSensor4 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                oLiveUpdate.fReferSensor5 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                oLiveUpdate.fReferSensor6 = routePacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                #endregion

                                #region Populate Refrigeration Zone Values

                                if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        #region Populate Zone 1 values

                                        oLiveUpdate.iReferZ1Zone = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        oLiveUpdate.iReferZ1OperatingMode = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        oLiveUpdate.iReferZ1Alarms = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        oLiveUpdate.iReferZ1SensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        oLiveUpdate.fReferZ1ReturnAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        oLiveUpdate.fReferZ1ReturnAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        oLiveUpdate.fReferZ1SupplyAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        oLiveUpdate.fReferZ1SupplyAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        oLiveUpdate.fReferZ1Setpoint = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        oLiveUpdate.fReferZ1Evap = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        #region Populate Zone 2 values

                                        oLiveUpdate.iReferZ2Zone = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        oLiveUpdate.iReferZ2OperatingMode = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        oLiveUpdate.iReferZ2Alarms = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        oLiveUpdate.iReferZ2SensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        oLiveUpdate.fReferZ2ReturnAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        oLiveUpdate.fReferZ2ReturnAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        oLiveUpdate.fReferZ2SupplyAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        oLiveUpdate.fReferZ2SupplyAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        oLiveUpdate.fReferZ2Setpoint = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        oLiveUpdate.fReferZ2Evap = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                        #endregion
                                    }
                                    if (routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        #region Populate Zone 3 values

                                        oLiveUpdate.iReferZ3Zone = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        oLiveUpdate.iReferZ3OperatingMode = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        oLiveUpdate.iReferZ3Alarms = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        oLiveUpdate.iReferZ3SensorsAvaliable = (short)routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        oLiveUpdate.fReferZ3ReturnAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        oLiveUpdate.fReferZ3ReturnAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        oLiveUpdate.fReferZ3SupplyAir1 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        oLiveUpdate.fReferZ3SupplyAir2 = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        oLiveUpdate.fReferZ3Setpoint = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        oLiveUpdate.fReferZ3Evap = routePacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                        #endregion
                                    }
                                }

                                #endregion
                            }
                        }

                        #endregion

                        #region Populate Raw GPS and User Defined details

                        oLiveUpdate.sUserDefined = sUserDefinedValue;

                        #endregion

                        #region Populate Vehicle Location/labeling details

                        if (v != null)
                        {
                            oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(routePacket);
                            oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(routePacket);
                            if (v.oPos != null)
                            {
                                oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                if (!String.IsNullOrEmpty(routePacket.StreetName))
                                {
                                    if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                    {
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + routePacket.StreetName;
                                    }
                                    else
                                    {
                                        oLiveUpdate.sPlaceName = routePacket.StreetName;
                                    }
                                }
                                else
                                    oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                            }

                            GetPlaceName(routePacket, oLiveUpdate);

                            oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                            oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                        }

                        #endregion

                        #region Populate Trailer Track details

                        if (routePacket.mTrailerTrack != null)
                        {
                            if (routePacket.mTrailerTrack.oTrailers != null)
                            {
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 1)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[0], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 2)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[1], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 3)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[2], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 4)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[3], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                    }
                                }
                                if (routePacket.mTrailerTrack.oTrailers.Count >= 5)
                                {
                                    sTrailerName = GetTrailerNameFromID((byte[])routePacket.mTrailerTrack.oTrailers[4], (int)routePacket.cFleetId);
                                    sSplit = sTrailerName.Split("~".ToCharArray());
                                    if (sSplit.Length == 2)
                                    {
                                        oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                        oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                    }
                                }
                            }
                        }

                        #endregion

                        #region Mass

                        oLiveUpdate.fTotalMass = v.TotalMass;
                        oLiveUpdate.fWeightLimit = v.WeightLimit;

                        #endregion

                        #region Populate the plugin field to indicate if this messages is commingout of flash

                        // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                        // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                        bFlashInfo = new byte[2];
                        if (routePacket._receivedFlashAvailable)
                            bFlashInfo[0] = 0x01;
                        else
                            bFlashInfo[0] = 0x00;
                        if (routePacket.bArchivalData)
                            bFlashInfo[1] = 0x01;
                        else
                            bFlashInfo[1] = 0x00;
                        oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                        oLiveUpdate.AddPluginDataField(oPluginData);

                        #endregion

                        #region Populate the plugin field to provide the current route data.

                        if (routePacket.mExtendedVariableDetails != null && routePacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && routePacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                        {
                            byte[] bRouteInfo = new byte[8];
                            byte[] bConv = BitConverter.GetBytes(routePacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                            bRouteInfo[0] = bConv[0];
                            bRouteInfo[1] = bConv[1];
                            bRouteInfo[2] = bConv[2];
                            bRouteInfo[3] = bConv[3];
                            bConv = BitConverter.GetBytes(routePacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                            bRouteInfo[4] = bConv[0];
                            bRouteInfo[5] = bConv[1];
                            bRouteInfo[6] = bConv[2];
                            bRouteInfo[7] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion

                        #region Populate the plugin field to add any extra GPS or G-Force information - Plugin data is only attached if values are present.

                        if (routePacket.mECMAdvanced != null)
                        {
                            if (routePacket.mECMAdvanced.SatellitesHDOP() > 0 || routePacket.mECMAdvanced.SatellitesInView() > 0 || routePacket.mECMAdvanced.Altitude() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(routePacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                oMS.WriteByte(routePacket.mECMAdvanced.SatellitesInView());
                                oMS.Write(BitConverter.GetBytes(routePacket.mECMAdvanced.Altitude()), 0, 2);
                                oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                            if (routePacket.mECMAdvanced.GForceZAxis() > 0 || routePacket.mECMAdvanced.GForceTemperature() > 0)
                            {
                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(routePacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                oMS.Write(BitConverter.GetBytes(Convert.ToSingle(routePacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }

                        #endregion
                        #region Populate the plugin field to CAN vehicle speed.
                        if (routePacket.mTransportExtraDetails != null && routePacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                        {
                            byte[] bCANVehicleSpeed = new byte[4];
                            byte[] bConv = BitConverter.GetBytes(routePacket.mTransportExtraDetails.iCANVehicleSpeed);
                            bCANVehicleSpeed[0] = bConv[0];
                            bCANVehicleSpeed[1] = bConv[1];
                            bCANVehicleSpeed[2] = bConv[2];
                            bCANVehicleSpeed[3] = bConv[3];
                            oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                            oLiveUpdate.AddPluginDataField(oPluginData);
                        }

                        #endregion
                        #region Populate the plugin field to provide more accurate speed readings
                        if (routePacket.mECMAdvanced != null)
                        {
                            float speedAccuracy = routePacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                            float maxSpeedAccuracy = routePacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                            if ((speedAccuracy > 0 && routePacket.mFix != null) || (maxSpeedAccuracy > 0 && routePacket.mDistance != null))
                            {
                                if (routePacket.mFix != null)
                                    speedAccuracy += ((float)((int)routePacket.mFix.cSpeed));
                                else
                                    speedAccuracy = 0;

                                if (routePacket.mDistance != null)
                                    maxSpeedAccuracy += ((float)((int)routePacket.mDistance.cMaxSpeed));
                                else
                                    maxSpeedAccuracy = 0;

                                MemoryStream oMS = new MemoryStream();
                                oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }
                        }
                        #endregion
                        if (scheduleTaskPluginData != null)
                        {
                            oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                        }
                        sErrMsg = "";
                        bVer5Format = oLiveUpdate.Encode(ref sErrMsg);
                        if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                        if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                        SendSMSEmail(oLiveUpdate);
                        if (sErrMsg == "")
                        {
                            oKeys.Add((double)5.0);
                            oMsgVariants.Add((double)5.0, bVer5Format);
                        }
                        else
                            WriteToConsole(sErrMsg);

                        #endregion

                        TellClients(routePacket.cFleetId, oMsgVariants, oKeys, false, false);
                        UpdateMySQL(Convert.ToInt32(routePacket.iVehicleId), routePacket.mFix);
                        v.MostRecentLiveUpdatePacket = aPacket;
                        break;

                        #endregion

                    case MassDeclarationPacket.MASS_PACKET_MASK:
                        if (aPacket.cMsgType == GeneralGPPacket.GEN_CONFIG_DOWNLOADED)
                            // Don't send as client update.
                            break;
                        else if (aPacket.cMsgType == DriverPointsRuleBreakPacket.DRIVER_POINTS_RULE_BROKEN)
                        {
                            #region Driver Points Rule Break Packet

                            DriverPointsRuleBreakPacket dp = new DriverPointsRuleBreakPacket(aPacket, _serverTime_DateFormat);
                            if (dp.mCurrentClock.IsDate(null))
                                dtReportTime = dp.mCurrentClock.ToDateTime();
                            if (!bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                                v.MostRecentLiveReportTime = dtReportTime;

                            #region If there is no time attached to the packet, don't send it to the clients

                            if (dp.mFixClock.iYear == 0 && dp.mFixClock.iMonth == 0 && dp.mFixClock.iDay == 0 && dp.mFixClock.iHour == 0 && dp.mFixClock.iMinute == 0 && dp.mFixClock.iSecond == 0)
                                return;
                            if (dp.mFixClock.iMonth == 0 && dp.mFixClock.iDay == 0 && dp.mFixClock.iHour == 0 && dp.mFixClock.iMinute == 0 && dp.mFixClock.iSecond == 0)
                                return;

                            #endregion

                            #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                            sKey = dp.cFleetId.ToString().PadLeft(5, '0') + dp.iVehicleId.ToString().PadLeft(5, '0');
                            if ((dp.mFix.dLatitude == 0) || (dp.mFix.dLongitude == 0))
                            {
                                oTestDate = new DateTime(Convert.ToInt32(dp.mFixClock.iYear) + 2000, Convert.ToInt32(dp.mFixClock.iMonth), Convert.ToInt32(dp.mFixClock.iDay), Convert.ToInt32(dp.mFixClock.iHour), Convert.ToInt32(dp.mFixClock.iMinute), Convert.ToInt32(dp.mFixClock.iSecond), 0);
                                oCurrentDate = DateTime.Now.ToUniversalTime();
                                oTS = oCurrentDate.Subtract(oTestDate);
                                iHoursDiff = oTS.TotalHours;
                                if (iHoursDiff > 24 || iHoursDiff < -24)
                                    return;

                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                    {
                                        string sValue = (string)oLastKnowPosition[sKey];
                                        string[] sValues = sValue.Split("~".ToCharArray());
                                        if (sValues.Length == 2)
                                        {
                                            dp.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                            dp.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                        }
                                    }
                                }
                                // If we still don't have a position, don't send the update.
                                if ((dp.mFix.dLatitude == 0) || (dp.mFix.dLongitude == 0))
                                {
                                    return;
                                }
                            }
                            else
                            {
                                // If this entry has a postiion, then set the last known position for the unit.
                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                        oLastKnowPosition.Remove(sKey);
                                    oLastKnowPosition.Add(sKey, Convert.ToString(dp.mFix.dLatitude) + "~" + Convert.ToString(dp.mFix.dLongitude));
                                }
                            }

                            #endregion

                            #region Setup the userdefined field

                            // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                            if (_userDefinedDateFormat != null)
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat) + "%0";
                            else
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0') + "%0";

                            #endregion

                            #region Version 5.0 format

                            oLiveUpdate = new LiveUpdate();
                            oLiveUpdate.iPacketVersion = 5;

                            #region Populate fleet/vehicle, reason and time details

                            oLiveUpdate.iFleetID = (short)dp.cFleetId;
                            oLiveUpdate.iVehicleID = (int)dp.iVehicleId;
                            oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                            oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                            oLiveUpdate.iReasonID = (int)dp.cMsgType;
                            if (dp.mFixClock != null)
                            {
                                if (dp.mFixClock.IsDate(null))
                                    oLiveUpdate.dtGPSTime = dp.mFixClock.ToDateTime();
                                else
                                    oLiveUpdate.dtGPSTime = DateTime.Now.ToUniversalTime();
                            }
                            if (dp.mCurrentClock != null)
                            {
                                if (dp.mCurrentClock.IsDate(null))
                                    oLiveUpdate.dtDeviceTime = dp.mCurrentClock.ToDateTime();
                                else
                                    oLiveUpdate.dtDeviceTime = DateTime.Now.ToUniversalTime();
                            }
                            oLiveUpdate.dtServerTime = DateTime.Now;

                            #endregion

                            #region Populate GPS details

                            if (dp.mFix != null)
                            {
                                oLiveUpdate.dLat = dp.mFix.dLatitude;
                                oLiveUpdate.dLong = dp.mFix.dLongitude;
                                oLiveUpdate.bFlags = dp.mFix.cFlags;
                                oLiveUpdate.iDirection = (short)dp.mFix.iDirection;
                                oLiveUpdate.bSpeed = dp.mFix.cSpeed;
                            }

                            #endregion

                            #region Populate Distance details

                            if (dp.mDistance != null)
                            {
                                oLiveUpdate.lSpeedAcc = dp.mDistance.lSpeedAccumulator;
                                oLiveUpdate.bMaxSpeed = dp.mDistance.cMaxSpeed;
                            }

                            #endregion

                            #region Populate Status details

                            if (dp.mStatus != null)
                            {
                                oLiveUpdate.bInputStatus = dp.mStatus.cInputStatus;
                                oLiveUpdate.bOutputStatus = dp.mStatus.cOutputStatus;
                                oLiveUpdate.iLightStatus = dp.mStatus.cLightStatus;
                                oLiveUpdate.iButtonStatus = dp.mStatus.cButtonStatus;
                            }

                            #endregion

                            #region Populate User Defined details

                            oLiveUpdate.sUserDefined = sUserDefinedValue;

                            #endregion

                            #region Populate Vehicle Location/labeling details

                            if (v != null)
                            {
                                oLiveUpdate.lExtendedStatus = v.MapOldStatusToUnitState();
                                oLiveUpdate.lAuxilaryStatus = v.MapOldStatusToAuxiliaryState();

                                if (v.oPos != null)
                                {
                                    oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                    if (!String.IsNullOrEmpty(dp.StreetName))
                                    {
                                        if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                        {
                                            oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + dp.StreetName;
                                        }
                                        else
                                        {
                                            oLiveUpdate.sPlaceName = dp.StreetName;
                                        }
                                    }
                                    else
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                    oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                    oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                    oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                    oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                                }

                                GetPlaceName(dp, oLiveUpdate);

                                oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                                oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                            }

                            #endregion

                            #region Mass

                            oLiveUpdate.fTotalMass = v.TotalMass;
                            oLiveUpdate.fWeightLimit = v.WeightLimit;

                            #endregion

                            #region Populate the plugin field to indicate if this messages is commingout of flash

                            // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                            // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                            bFlashInfo = new byte[2];
                            if (dp._receivedFlashAvailable)
                                bFlashInfo[0] = 0x01;
                            else
                                bFlashInfo[0] = 0x00;
                            if (dp.bArchivalData)
                                bFlashInfo[1] = 0x01;
                            else
                                bFlashInfo[1] = 0x00;
                            oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);

                            #endregion
                            if (scheduleTaskPluginData != null)
                            {
                                oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                            }

                            sErrMsg = "";
                            bVer5Format = oLiveUpdate.Encode(ref sErrMsg);
                            if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                            if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                            SendSMSEmail(oLiveUpdate);
                            if (sErrMsg == "")
                            {
                                oKeys.Add((double)5.0);
                                oMsgVariants.Add((double)5.0, bVer5Format);
                            }
                            else
                                WriteToConsole(sErrMsg);

                            #endregion

                            TellClients(dp.cFleetId, oMsgVariants, oKeys, false, false);
                            UpdateMySQL(Convert.ToInt32(dp.iVehicleId), dp.mFix);
                            v.MostRecentLiveUpdatePacket = dp;

                            #endregion
                        }
                        else if (aPacket.cMsgType == GenericPacket.GENERIC_PACKET)
                        {
                            #region GENERIC_PACKET Packet

                            // Attempt to get the GenericPacket from the GatewayProtocalPacket as this contains the GenericPacket custom information (rule set and dvr)
                            GenericPacket g = aPacket is GenericPacket ? aPacket as GenericPacket : new GenericPacket(aPacket, _serverTime_DateFormat);

                            if (g.mCurrentClock.IsDate(null))
                                dtReportTime = g.mCurrentClock.ToDateTime();
                            if (!bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                                v.MostRecentLiveReportTime = dtReportTime;

                            #region If there is no time attached to the packet, don't send it to the clients

                            if (g.mFixClock.iYear == 0 && g.mFixClock.iMonth == 0 && g.mFixClock.iDay == 0 && g.mFixClock.iHour == 0 && g.mFixClock.iMinute == 0 && g.mFixClock.iSecond == 0)
                                return;
                            if (g.mFixClock.iMonth == 0 && g.mFixClock.iDay == 0 && g.mFixClock.iHour == 0 && g.mFixClock.iMinute == 0 && g.mFixClock.iSecond == 0)
                                return;

                            #endregion

                            #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                            sKey = g.cFleetId.ToString().PadLeft(5, '0') + g.iVehicleId.ToString().PadLeft(5, '0');
                            if ((g.mFix.dLatitude == 0) || (g.mFix.dLongitude == 0))
                            {
                                oTestDate = new DateTime(Convert.ToInt32(g.mFixClock.iYear) + 2000, Convert.ToInt32(g.mFixClock.iMonth), Convert.ToInt32(g.mFixClock.iDay), Convert.ToInt32(g.mFixClock.iHour), Convert.ToInt32(g.mFixClock.iMinute), Convert.ToInt32(g.mFixClock.iSecond), 0);
                                oCurrentDate = DateTime.Now.ToUniversalTime();
                                oTS = oCurrentDate.Subtract(oTestDate);
                                iHoursDiff = oTS.TotalHours;
                                if (iHoursDiff > 24 || iHoursDiff < -24)
                                    return;

                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                    {
                                        string sValue = (string)oLastKnowPosition[sKey];
                                        string[] sValues = sValue.Split("~".ToCharArray());
                                        if (sValues.Length == 2)
                                        {
                                            g.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                            g.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                        }
                                    }
                                }
                                // If we still don't have a position, don't send the update.
                                if ((g.mFix.dLatitude == 0) || (g.mFix.dLongitude == 0))
                                {
                                    return;
                                }
                            }
                            else
                            {
                                // If this entry has a postiion, then set the last known position for the unit.
                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                        oLastKnowPosition.Remove(sKey);
                                    oLastKnowPosition.Add(sKey, Convert.ToString(g.mFix.dLatitude) + "~" + Convert.ToString(g.mFix.dLongitude));
                                }
                            }

                            #endregion

                            #region Setup the userdefined field

                            // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                            if (_userDefinedDateFormat != null)
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat) + "%0";
                            else
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0') + "%0";

                            #endregion

                            #region Version 5.0 format

                            oLiveUpdate = new LiveUpdate();
                            oLiveUpdate.iPacketVersion = 5;

                            #region Populate fleet/vehicle, reason and time details

                            oLiveUpdate.iFleetID = (short)g.cFleetId;
                            oLiveUpdate.iVehicleID = (int)g.iVehicleId;
                            oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                            oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;

                            switch (g.SubCommand)
                            {
                                case GenericSubCommands.DvrStatus:
                                case GenericSubCommands.DvrError:
                                case GenericSubCommands.DvrErrorDiskStatus:
                                case GenericSubCommands.DvrErrorDiskRecording:
                                case GenericSubCommands.DvrErrorCamerasConnected:
                                case GenericSubCommands.DvrErrorComms:
                                case GenericSubCommands.DvrErrorDataUsageLimit:
                                case GenericSubCommands.DvrErrorUploadAttemptLimit:
                                case GenericSubCommands.DvrErrorIncompatibleFirmware:
                                    // Update the sub command based on the DVR error code
                                    if (g.DvrErrorCode > 0)
                                    {
                                        g.ParseDvrErrorCode(g.DvrErrorCode);
                                    }
                                    break;
                                default:
                                    break;
                            }

                            oLiveUpdate.iReasonID = Convert.ToInt32(((int)(g.SubCommand + 1) << 8) + (g.cMsgType & 0xFF)); ;
                            if (g.mFixClock != null)
                            {
                                if (g.mFixClock.IsDate(null))
                                    oLiveUpdate.dtGPSTime = g.mFixClock.ToDateTime();
                                else
                                    oLiveUpdate.dtGPSTime = DateTime.Now.ToUniversalTime();
                            }
                            if (g.mCurrentClock != null)
                            {
                                if (g.mCurrentClock.IsDate(null))
                                    oLiveUpdate.dtDeviceTime = g.mCurrentClock.ToDateTime();
                                else
                                    oLiveUpdate.dtDeviceTime = DateTime.Now.ToUniversalTime();
                            }
                            oLiveUpdate.dtServerTime = DateTime.Now;

                            // Populate the plugin information with the relevant information based on the sub command
                            switch (g.SubCommand)
                            {
                                case GenericSubCommands.DvrAlarm:
                                case GenericSubCommands.DvrRequest:
                                    using (MemoryStream memoryStream = new MemoryStream())
                                    {
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrDeviceId);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrEventId);

                                        oPluginData = new cPluginData(PluginIds.DvrEventInfo, memoryStream.ToArray());
                                        oLiveUpdate.AddPluginDataField(oPluginData);
                                    }

                                    break;

                                case GenericSubCommands.DvrStatus:
                                case GenericSubCommands.DvrError:
                                case GenericSubCommands.DvrErrorDiskStatus:
                                case GenericSubCommands.DvrErrorDiskRecording:
                                case GenericSubCommands.DvrErrorCamerasConnected:
                                case GenericSubCommands.DvrErrorComms:
                                case GenericSubCommands.DvrErrorDataUsageLimit:
                                case GenericSubCommands.DvrErrorUploadAttemptLimit:
                                case GenericSubCommands.DvrErrorIncompatibleFirmware:
                                    using (MemoryStream memoryStream = new MemoryStream())
                                    {
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusVersion);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrErrorCode);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusHddOk);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusRecording);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusConnected);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusVideoConnectedChannel);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusVideoRecordChannel);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusHddSize);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusHddFree);
                                        PacketUtilities.WriteToStream(memoryStream, g.DvrStatusTimeOffset);
                                        PacketUtilities.WriteToStream(memoryStream, 0, g.DvrStatusDeviceName);
                                        PacketUtilities.WriteToStream(memoryStream, 0, g.DvrStatusFirmwareVersion);
                                        PacketUtilities.WriteToStream(memoryStream, 0, g.DvrStatusModel);
                                        PacketUtilities.WriteToStream(memoryStream, 0, g.DvrStatusVideoFormat);

                                        oPluginData = new cPluginData(PluginIds.DvrStatusInfo, memoryStream.ToArray());
                                        oLiveUpdate.AddPluginDataField(oPluginData);
                                    }
                                    break;
                            }

                            #endregion

                            #region Populate GPS details

                            if (g.mFix != null)
                            {
                                oLiveUpdate.dLat = g.mFix.dLatitude;
                                oLiveUpdate.dLong = g.mFix.dLongitude;
                                oLiveUpdate.bFlags = g.mFix.cFlags;
                                oLiveUpdate.iDirection = (short)g.mFix.iDirection;
                                oLiveUpdate.bSpeed = g.mFix.cSpeed;
                            }

                            #endregion

                            #region Populate Distance details

                            if (g.mDistance != null)
                            {
                                oLiveUpdate.lSpeedAcc = g.mDistance.lSpeedAccumulator;
                                oLiveUpdate.bMaxSpeed = g.mDistance.cMaxSpeed;
                            }

                            #endregion

                            #region Populate Status details

                            if (g.mStatus != null)
                            {
                                oLiveUpdate.bInputStatus = g.mStatus.cInputStatus;
                                oLiveUpdate.bOutputStatus = g.mStatus.cOutputStatus;
                                oLiveUpdate.iLightStatus = g.mStatus.cLightStatus;
                                oLiveUpdate.iButtonStatus = g.mStatus.cButtonStatus;
                            }

                            #endregion

                            #region Populate User Defined details

                            oLiveUpdate.sUserDefined = sUserDefinedValue;

                            #endregion

                            #region Populate Vehicle Location/labeling details

                            if (v != null)
                            {
                                oLiveUpdate.lExtendedStatus = v.MapOldStatusToUnitState();
                                oLiveUpdate.lAuxilaryStatus = v.MapOldStatusToAuxiliaryState();

                                if (v.oPos != null)
                                {
                                    oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                    if (!String.IsNullOrEmpty(g.StreetName))
                                    {
                                        if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                        {
                                            oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + g.StreetName;
                                        }
                                        else
                                        {
                                            oLiveUpdate.sPlaceName = g.StreetName;
                                        }
                                    }
                                    else
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                    oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                    oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                    oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                    oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                                }

                                GetPlaceName(g, oLiveUpdate);

                                oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                                oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                            }

                            #endregion

                            #region Mass

                            oLiveUpdate.fTotalMass = v.TotalMass;
                            oLiveUpdate.fWeightLimit = v.WeightLimit;

                            #endregion

                            #region Populate the plugin field to indicate if this messages is commingout of flash

                            // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                            // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                            bFlashInfo = new byte[2];
                            if (g._receivedFlashAvailable)
                                bFlashInfo[0] = 0x01;
                            else
                                bFlashInfo[0] = 0x00;
                            if (g.bArchivalData)
                                bFlashInfo[1] = 0x01;
                            else
                                bFlashInfo[1] = 0x00;
                            oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);

                            #endregion
                            if (scheduleTaskPluginData != null)
                            {
                                oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                            }
                            if (g.Speed > 0)
                            {
                                byte[] bCANVehicleSpeed = new byte[4];
                                byte[] bConv = BitConverter.GetBytes(g.Speed);
                                bCANVehicleSpeed[0] = bConv[0];
                                bCANVehicleSpeed[1] = bConv[1];
                                bCANVehicleSpeed[2] = bConv[2];
                                bCANVehicleSpeed[3] = bConv[3];
                                oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }


                            sErrMsg = "";
                            bVer5Format = oLiveUpdate.Encode(ref sErrMsg);
                            if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                            if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                            SendSMSEmail(oLiveUpdate);
                            if (sErrMsg == "")
                            {
                                oKeys.Add((double)5.0);
                                oMsgVariants.Add((double)5.0, bVer5Format);
                            }
                            else
                                WriteToConsole(sErrMsg);

                            #endregion

                            TellClients(g.cFleetId, oMsgVariants, oKeys, false, false);
                            UpdateMySQL(Convert.ToInt32(g.iVehicleId), g.mFix);
                            v.MostRecentLiveUpdatePacket = g;

                            #endregion
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START 
                            || aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END
                            || aPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY 
                            || aPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION
                            || aPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR
                            || aPacket.cMsgType == GeneralGPPacket.POSSIBLE_ACCIDENT
                            || aPacket.cMsgType == GeneralGPPacket.GPS_SETTINGS
                            || aPacket.cMsgType == GeneralGPPacket.TAMPER)
                        {
                            if (aPacket is GeneralGPPacket)
                                gpPacket = (GeneralGPPacket)aPacket;
                            else
                                gpPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);

                            #region Replace Engine values with advanced values where applicable.

                            try
                            {
                                if (gpPacket.mECMAdvanced != null)
                                {
                                    #region mTransportExtraDetails
                                    if (v.UseGPSOdometer && gpPacket.mECMAdvanced.Odometer() > 0)
                                    {
                                        gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mECMAdvanced.GPSOdometer() + v.OdometerOffset);
                                    }
                                    else if (gpPacket.mECMAdvanced.Odometer() > 0)
                                    {
                                        if (gpPacket.mECMAdvanced.Odometer() + v.OdometerOffset > 0)
                                            gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mECMAdvanced.Odometer() + v.OdometerOffset);
                                        else
                                            gpPacket.mTransportExtraDetails.iOdometer = 0;
                                    }
                                    else if (gpPacket.mTransportExtraDetails.iOdometer > 0)
                                    {
                                        if ((gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                            gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                                        else
                                            gpPacket.mTransportExtraDetails.iOdometer = 0;
                                    }
                                    gpPacket.mTransportExtraDetails.iEngineHours = GetTotalEngineHours(gpPacket.mECMAdvanced, v);
                                    if (gpPacket.mECMAdvanced.TotalFuelUsed() > 0) gpPacket.mTransportExtraDetails.iTotalFuelUsed = (int)gpPacket.mECMAdvanced.TotalFuelUsed();
                                    if (gpPacket.mECMAdvanced.FuelEconomy() > 0) gpPacket.mTransportExtraDetails.fFuelEconomy = Convert.ToSingle(gpPacket.mECMAdvanced.FuelEconomy());
                                    if (gpPacket.mECMAdvanced.BatteryVolts() > 0) gpPacket.mTransportExtraDetails.iBatteryVolts = gpPacket.mECMAdvanced.BatteryVolts();
                                    if (gpPacket.mECMAdvanced.BrakeUsageSeconds() > 0) gpPacket.mTransportExtraDetails.iBrakeUsageSeconds = gpPacket.mECMAdvanced.BrakeUsageSeconds();
                                    if (gpPacket.mECMAdvanced.ECMVehicleSpeed(true) > 0) gpPacket.mTransportExtraDetails.iCANVehicleSpeed = gpPacket.mECMAdvanced.ECMVehicleSpeed(true);
                                    // RPM Zone Readings
                                    if (gpPacket.mECMAdvanced.RPMZone1() > 0) gpPacket.mTransportExtraDetails.iRPMZone1 = gpPacket.mECMAdvanced.RPMZone1();
                                    if (gpPacket.mECMAdvanced.RPMZone2() > 0) gpPacket.mTransportExtraDetails.iRPMZone2 = gpPacket.mECMAdvanced.RPMZone2();
                                    if (gpPacket.mECMAdvanced.RPMZone3() > 0) gpPacket.mTransportExtraDetails.iRPMZone3 = gpPacket.mECMAdvanced.RPMZone3();
                                    if (gpPacket.mECMAdvanced.RPMZone4() > 0) gpPacket.mTransportExtraDetails.iRPMZone4 = gpPacket.mECMAdvanced.RPMZone4();
                                    // Speed Zone Readings
                                    if (gpPacket.mECMAdvanced.SpeedZone1() > 0) gpPacket.mTransportExtraDetails.iSpeedZone1 = gpPacket.mECMAdvanced.SpeedZone1();
                                    if (gpPacket.mECMAdvanced.SpeedZone2() > 0) gpPacket.mTransportExtraDetails.iSpeedZone2 = gpPacket.mECMAdvanced.SpeedZone2();
                                    if (gpPacket.mECMAdvanced.SpeedZone3() > 0) gpPacket.mTransportExtraDetails.iSpeedZone3 = gpPacket.mECMAdvanced.SpeedZone3();
                                    if (gpPacket.mECMAdvanced.SpeedZone4() > 0) gpPacket.mTransportExtraDetails.iSpeedZone4 = gpPacket.mECMAdvanced.SpeedZone4();

                                    #endregion

                                    #region mEngineData

                                    if (gpPacket.mECMAdvanced.MaxEngCoolTemp() > 0) gpPacket.mEngineData.iCoolantTemperature = gpPacket.mECMAdvanced.MaxEngCoolTemp();
                                    if (gpPacket.mECMAdvanced.MaxEngOilTemp() > 0) gpPacket.mEngineData.iOilTemperature = gpPacket.mECMAdvanced.MaxEngOilTemp();
                                    if (gpPacket.mECMAdvanced.MinOilPressure() > 0) gpPacket.mEngineData.iOilPressure = gpPacket.mECMAdvanced.MinOilPressure();
                                    if (gpPacket.mECMAdvanced.GearPosition() > 0) gpPacket.mEngineData.iGear = gpPacket.mECMAdvanced.GearPosition();
                                    if (gpPacket.mECMAdvanced.MaxEngSpeed() > 0) gpPacket.mEngineData.iMaxRPM = gpPacket.mECMAdvanced.MaxEngSpeed();
                                    if (gpPacket.mECMAdvanced.BrakeHits() > 0) gpPacket.mEngineData.iBrakeApplications = gpPacket.mECMAdvanced.BrakeHits();
                                    // G-Force Readings
                                    if (gpPacket.mECMAdvanced.HasGForce)
                                    {
                                        gpPacket.mEngineData.fGForceFront = Convert.ToSingle(gpPacket.mECMAdvanced.GForceFront());
                                        gpPacket.mEngineData.fGForceBack = Convert.ToSingle(gpPacket.mECMAdvanced.GForceBack());
                                        gpPacket.mEngineData.fGForceLeftRight = Convert.ToSingle(gpPacket.mECMAdvanced.GForceLeftRight());
                                    }

                                    #endregion

                                    #region mEngineSummaryData

                                    if (gpPacket.mECMAdvanced.TripFuel() > 0) gpPacket.mEngineSummaryData.iTripFuel = (int)gpPacket.mECMAdvanced.TripFuel();
                                    gpPacket.mEngineSummaryData.iTotalEngineHours = GetTotalEngineHours(gpPacket.mECMAdvanced, v);
                                    if (gpPacket.mECMAdvanced.FuelEconomy() > 0) gpPacket.mEngineSummaryData.fFuelEconomy = Convert.ToSingle(gpPacket.mECMAdvanced.FuelEconomy());

                                    #endregion

                                    #region mExtendedValues

                                    gpPacket.mExtendedValues.iValue1 = gpPacket.mECMAdvanced.ExtendedIO1();
                                    gpPacket.mExtendedValues.iValue2 = gpPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(gpPacket.cFleetId));
                                    gpPacket.mExtendedValues.iValue3 = gpPacket.mECMAdvanced.ExtendedIO3();
                                    gpPacket.mExtendedValues.iValue4 = gpPacket.mECMAdvanced.ExtendedIO4();

                                    #endregion
                                }
                                else
                                {
                                    if (gpPacket.mTransportExtraDetails.iOdometer > 0)
                                    {
                                        if ((gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset) > 0)
                                            gpPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(gpPacket.mTransportExtraDetails.iOdometer + v.OdometerOffset);
                                        else
                                            gpPacket.mTransportExtraDetails.iOdometer = 0;
                                    }
                                }
                            }
                            catch (System.Exception exAdvanceECM)
                            {
                                _log.Error(sClassName + "NotifyClientsOfPacket(GatewayProtocolPacket aPacket, Vehicle v, bool bUpdateFromFlash)", exAdvanceECM);
                            }

                            #endregion

                            if (gpPacket.mCurrentClock.IsDate(null))
                                dtReportTime = gpPacket.mCurrentClock.ToDateTime();
                            if (!bUpdateFromFlash && !dtReportTime.Equals(DateTime.MinValue))
                                v.MostRecentLiveReportTime = dtReportTime;

                            // If this is an update from a flash report and there are no clients registered to recieve updates from flash, just return.
                            if (bUpdateFromFlash && mFromFlashFleetList.Count == 0 && _luToMsmq == null && _ecmLuToMsmq == null)
                                return;

                            #region If there is no time attached to the packet, don't send it to the clients

                            if (gpPacket.mFixClock.iYear == 0 && gpPacket.mFixClock.iMonth == 0 && gpPacket.mFixClock.iDay == 0 && gpPacket.mFixClock.iHour == 0 && gpPacket.mFixClock.iMinute == 0 && gpPacket.mFixClock.iSecond == 0)
                            {
                                if (gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                    gpPacket.mFixClock.FromDateTime(DateTime.Now.ToUniversalTime());
                                else
                                    return;
                            }
                            if (gpPacket.mCurrentClock.iMonth == 0 && gpPacket.mCurrentClock.iDay == 0 && gpPacket.mCurrentClock.iHour == 0 && gpPacket.mCurrentClock.iMinute == 0 && gpPacket.mCurrentClock.iSecond == 0)
                                if (gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER || gpPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                                    gpPacket.mCurrentClock.FromDateTime(DateTime.Now.ToUniversalTime());
                                else
                                    return;

                            #endregion

                            #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                            sKey = gpPacket.cFleetId.ToString().PadLeft(5, '0') + gpPacket.iVehicleId.ToString().PadLeft(5, '0');

                            if ((gpPacket.mFix.dLatitude == 0 && gpPacket.mFix.dLongitude == 0))
                            {
                                oTestDate = new DateTime(Convert.ToInt32((gpPacket.mFixClock.iYear > 2000 ? gpPacket.mFixClock.iYear : gpPacket.mFixClock.iYear + 2000)), Convert.ToInt32(gpPacket.mFixClock.iMonth), Convert.ToInt32(gpPacket.mFixClock.iDay), Convert.ToInt32(gpPacket.mFixClock.iHour), Convert.ToInt32(gpPacket.mFixClock.iMinute), Convert.ToInt32(gpPacket.mFixClock.iSecond), 0);
                                oCurrentDate = DateTime.Now.ToUniversalTime();
                                oTS = oCurrentDate.Subtract(oTestDate);
                                iHoursDiff = oTS.TotalHours;
                                if (iHoursDiff > 24 || iHoursDiff < -24)
                                    return;

                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                    {
                                        string sValue = (string)oLastKnowPosition[sKey];
                                        string[] sValues = sValue.Split("~".ToCharArray());
                                        if (sValues.Length == 2)
                                        {
                                            gpPacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                            gpPacket.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                        }
                                    }
                                }
                                // If we still don't have a position, don't send the update.
                                if ((gpPacket.mFix.dLatitude == 0) || (gpPacket.mFix.dLongitude == 0))
                                {
                                    return;
                                }
                            }
                            else
                            {
                                // If this entry has a postiion, then set the last known position for the unit.
                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                        oLastKnowPosition.Remove(sKey);
                                    oLastKnowPosition.Add(sKey, Convert.ToString(gpPacket.mFix.dLatitude) + "~" + Convert.ToString(gpPacket.mFix.dLongitude));
                                }
                            }

                            #endregion

                            #region Setup the userdefined field

                            // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                            if (_userDefinedDateFormat != null)
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                            else
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');

                            // Insert the count if necessary into the UserDefined field
                            if (gpPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES)
                                sUserDefinedValue += "%" + gpPacket.iPulseCount.ToString();
                            else
                                sUserDefinedValue += "%0";

                            #endregion

                            #region Change the message type for IO, Vehicle Usage and Job messages
                            if (gpPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR && gpPacket.EcmError != null && gpPacket.EcmError.Cancelled)
                            {
                                iMessageType = 295;
                            }
                            else
                            {
                                iMessageType = (gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF);
                            }
                            #endregion

                            #region Version 5.0 format

                            oLiveUpdate = new LiveUpdate();
                            oLiveUpdate.iPacketVersion = 5;

                            #region Populate fleet/vehicle, reason and time details

                            oLiveUpdate.iFleetID = (short)gpPacket.cFleetId;
                            oLiveUpdate.iVehicleID = (int)gpPacket.iVehicleId;
                            oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                            oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;

                            oLiveUpdate.iReasonID = Convert.ToInt32((gpPacket.cInputNumber << 8) + (gpPacket.cMsgType & 0xFF));

                            if (gpPacket.mFixClock != null && gpPacket.mFixClock.IsDate(null))
                            {
                                oLiveUpdate.dtGPSTime = gpPacket.mFixClock.ToDateTime();
                            }
                            else
                                oLiveUpdate.dtGPSTime = DateTime.Now.ToUniversalTime();

                            if (gpPacket.mCurrentClock != null && gpPacket.mCurrentClock.IsDate(null))
                            {
                                oLiveUpdate.dtDeviceTime = gpPacket.mCurrentClock.ToDateTime();
                            }
                            else
                                oLiveUpdate.dtDeviceTime = DateTime.Now.ToUniversalTime();

                            oLiveUpdate.dtServerTime = DateTime.Now;

                            #endregion

                            #region Populate GPS details

                            if (gpPacket.mFix != null)
                            {
                                oLiveUpdate.dLat = gpPacket.mFix.dLatitude;
                                oLiveUpdate.dLong = gpPacket.mFix.dLongitude;
                                oLiveUpdate.bFlags = gpPacket.mFix.cFlags;
                                oLiveUpdate.iDirection = (short)gpPacket.mFix.iDirection;
                                oLiveUpdate.bSpeed = gpPacket.mFix.cSpeed;
                            }

                            #endregion

                            #region Populate Distance details

                            if (gpPacket.mDistance != null)
                            {
                                oLiveUpdate.lSpeedAcc = gpPacket.mDistance.lSpeedAccumulator;
                                oLiveUpdate.bMaxSpeed = gpPacket.mDistance.cMaxSpeed;
                            }

                            #endregion

                            #region Populate Status details

                            if (gpPacket.mStatus != null)
                            {
                                oLiveUpdate.bInputStatus = gpPacket.mStatus.cInputStatus;
                                oLiveUpdate.bOutputStatus = gpPacket.mStatus.cOutputStatus;
                                oLiveUpdate.iLightStatus = gpPacket.mStatus.cLightStatus;
                                oLiveUpdate.iButtonStatus = gpPacket.mStatus.cButtonStatus;
                            }

                            #endregion

                            #region Populate Engine Data details

                            if (gpPacket.mEngineData != null)
                            {
                                oLiveUpdate.iCoolantTemperature = (short)gpPacket.mEngineData.iCoolantTemperature;
                                oLiveUpdate.iOilTemp = (short)gpPacket.mEngineData.iOilTemperature;
                                oLiveUpdate.iOilPressure = (short)gpPacket.mEngineData.iOilPressure;
                                oLiveUpdate.iGear = (short)gpPacket.mEngineData.iGear;
                                oLiveUpdate.iMaxRPM = (short)gpPacket.mEngineData.iMaxRPM;
                                oLiveUpdate.iBrakeApplications = (short)gpPacket.mEngineData.iBrakeApplications;
                                oLiveUpdate.fGForceFront = gpPacket.mEngineData.fGForceFront;
                                oLiveUpdate.fGForceBack = gpPacket.mEngineData.fGForceBack;
                                oLiveUpdate.fGForceLeftRight = gpPacket.mEngineData.fGForceLeftRight;
                            }

                            #endregion

                            #region Populate Engine Summary Data details

                            if (gpPacket.mEngineSummaryData != null)
                            {
                                oLiveUpdate.iTotalEngineHours = gpPacket.mEngineSummaryData.iTotalEngineHours;
                                oLiveUpdate.iTripFuel = gpPacket.mEngineSummaryData.iTripFuel;
                                oLiveUpdate.dFuelEconomy = gpPacket.mEngineSummaryData.fFuelEconomy;
                            }

                            #endregion

                            #region Populate Extended Value details

                            if (gpPacket.mExtendedValues != null)
                            {
                                oLiveUpdate.iExtendedValue1 = gpPacket.mExtendedValues.iValue1;
                                oLiveUpdate.iExtendedValue2 = gpPacket.mExtendedValues.iValue2;
                                oLiveUpdate.iExtendedValue3 = gpPacket.mExtendedValues.iValue3;
                                oLiveUpdate.iExtendedValue4 = gpPacket.mExtendedValues.iValue4;
                            }

                            #endregion

                            #region Populate Transport Extra  details

                            if (gpPacket.mTransportExtraDetails != null)
                            {
                                oLiveUpdate.iBatteryVolts = (short)gpPacket.mTransportExtraDetails.iBatteryVolts;
                                oLiveUpdate.iBrakeUsageSeconds = (short)gpPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                                oLiveUpdate.iOdometer = (uint)gpPacket.mTransportExtraDetails.iOdometer;
                                oLiveUpdate.iTotalFuelUsed = gpPacket.mTransportExtraDetails.iTotalFuelUsed;
                                oLiveUpdate.iRPMZone1 = (short)gpPacket.mTransportExtraDetails.iRPMZone1;
                                oLiveUpdate.iRPMZone2 = (short)gpPacket.mTransportExtraDetails.iRPMZone2;
                                oLiveUpdate.iRPMZone3 = (short)gpPacket.mTransportExtraDetails.iRPMZone3;
                                oLiveUpdate.iRPMZone4 = (short)gpPacket.mTransportExtraDetails.iRPMZone4;
                                oLiveUpdate.iSpeedZone1 = (short)gpPacket.mTransportExtraDetails.iSpeedZone1;
                                oLiveUpdate.iSpeedZone2 = (short)gpPacket.mTransportExtraDetails.iSpeedZone2;
                                oLiveUpdate.iSpeedZone3 = (short)gpPacket.mTransportExtraDetails.iSpeedZone3;
                                oLiveUpdate.iSpeedZone4 = (short)gpPacket.mTransportExtraDetails.iSpeedZone4;
                            }

                            #endregion

                            #region Populate Extended / Refrigeration details

                            if (gpPacket.mExtendedVariableDetails != null)
                            {
                                if (gpPacket.mExtendedVariableDetails.Refrigeration != null)
                                {
                                    #region Populate Refrigeration Common Values

                                    oLiveUpdate.iReferFlags = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                    oLiveUpdate.dReferFuelPercentage = gpPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                    oLiveUpdate.dReferBatteryVolts = gpPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                    oLiveUpdate.bReferInput1 = gpPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                    oLiveUpdate.bReferInput2 = gpPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                    oLiveUpdate.bReferInput3 = gpPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                    oLiveUpdate.bReferInput4 = gpPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                    oLiveUpdate.iReferSensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                    oLiveUpdate.dReferHumidity = gpPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                    oLiveUpdate.fReferSensor1 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                    oLiveUpdate.fReferSensor2 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                    oLiveUpdate.fReferSensor3 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                    oLiveUpdate.fReferSensor4 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                    oLiveUpdate.fReferSensor5 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                    oLiveUpdate.fReferSensor6 = gpPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                    #endregion

                                    #region Populate Refrigeration Zone Values

                                    if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                    {
                                        if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                        {
                                            #region Populate Zone 1 values

                                            oLiveUpdate.iReferZ1Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                            oLiveUpdate.iReferZ1OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                            oLiveUpdate.iReferZ1Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                            oLiveUpdate.iReferZ1SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                            oLiveUpdate.fReferZ1ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                            oLiveUpdate.fReferZ1ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                            oLiveUpdate.fReferZ1SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                            oLiveUpdate.fReferZ1SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                            oLiveUpdate.fReferZ1Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                            oLiveUpdate.fReferZ1Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;

                                            #endregion
                                        }
                                        if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                        {
                                            #region Populate Zone 2 values

                                            oLiveUpdate.iReferZ2Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                            oLiveUpdate.iReferZ2OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                            oLiveUpdate.iReferZ2Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                            oLiveUpdate.iReferZ2SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                            oLiveUpdate.fReferZ2ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                            oLiveUpdate.fReferZ2ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                            oLiveUpdate.fReferZ2SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                            oLiveUpdate.fReferZ2SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                            oLiveUpdate.fReferZ2Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                            oLiveUpdate.fReferZ2Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;

                                            #endregion
                                        }
                                        if (gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                        {
                                            #region Populate Zone 3 values

                                            oLiveUpdate.iReferZ3Zone = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                            oLiveUpdate.iReferZ3OperatingMode = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                            oLiveUpdate.iReferZ3Alarms = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                            oLiveUpdate.iReferZ3SensorsAvaliable = (short)gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                            oLiveUpdate.fReferZ3ReturnAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                            oLiveUpdate.fReferZ3ReturnAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                            oLiveUpdate.fReferZ3SupplyAir1 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                            oLiveUpdate.fReferZ3SupplyAir2 = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                            oLiveUpdate.fReferZ3Setpoint = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                            oLiveUpdate.fReferZ3Evap = gpPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;

                                            #endregion
                                        }
                                    }

                                    #endregion
                                }
                            }

                            #endregion

                            #region Populate Raw GPS and User Defined details

                            oLiveUpdate.sRawGPS = gpPacket.sRawGPS;
                            oLiveUpdate.sUserDefined = sUserDefinedValue;

                            #endregion

                            #region Populate Vehicle Location/labeling details

                            if (v != null)
                            {
                                oLiveUpdate.lExtendedStatus = v.GetUnitStateForReport(gpPacket);
                                oLiveUpdate.lAuxilaryStatus = v.GetAuxiliaryStateForReport(gpPacket);                                
                                if (v.oPos != null)
                                {
                                    oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                    if (!String.IsNullOrEmpty(gpPacket.StreetName))
                                    {
                                        if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                        {
                                            oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + gpPacket.StreetName;
                                        }
                                        else
                                        {
                                            oLiveUpdate.sPlaceName = gpPacket.StreetName;
                                        }
                                    }
                                    else
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                    oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                    oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                    oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                    oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                                }

                                GetPlaceName(gpPacket, oLiveUpdate);

                                oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                                oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                            }

                            #endregion

                            #region Populate Trailer Track details

                            if (gpPacket.mTrailerTrack != null)
                            {
                                if (gpPacket.mTrailerTrack.oTrailers != null)
                                {
                                    if (gpPacket.mTrailerTrack.oTrailers.Count >= 1)
                                    {
                                        sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[0], (int)gpPacket.cFleetId);
                                        sSplit = sTrailerName.Split("~".ToCharArray());
                                        if (sSplit.Length == 2)
                                        {
                                            oLiveUpdate.iTrailer1ID = Convert.ToInt32(sSplit[0]);
                                            oLiveUpdate.sTrailer1Name = sSplit[1].Trim();
                                        }
                                    }
                                    if (gpPacket.mTrailerTrack.oTrailers.Count >= 2)
                                    {
                                        sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[1], (int)gpPacket.cFleetId);
                                        sSplit = sTrailerName.Split("~".ToCharArray());
                                        if (sSplit.Length == 2)
                                        {
                                            oLiveUpdate.iTrailer2ID = Convert.ToInt32(sSplit[0]);
                                            oLiveUpdate.sTrailer2Name = sSplit[1].Trim();
                                        }
                                    }
                                    if (gpPacket.mTrailerTrack.oTrailers.Count >= 3)
                                    {
                                        sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[2], (int)gpPacket.cFleetId);
                                        sSplit = sTrailerName.Split("~".ToCharArray());
                                        if (sSplit.Length == 2)
                                        {
                                            oLiveUpdate.iTrailer3ID = Convert.ToInt32(sSplit[0]);
                                            oLiveUpdate.sTrailer3Name = sSplit[1].Trim();
                                        }
                                    }
                                    if (gpPacket.mTrailerTrack.oTrailers.Count >= 4)
                                    {
                                        sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[3], (int)gpPacket.cFleetId);
                                        sSplit = sTrailerName.Split("~".ToCharArray());
                                        if (sSplit.Length == 2)
                                        {
                                            oLiveUpdate.iTrailer4ID = Convert.ToInt32(sSplit[0]);
                                            oLiveUpdate.sTrailer4Name = sSplit[1].Trim();
                                        }
                                    }
                                    if (gpPacket.mTrailerTrack.oTrailers.Count >= 5)
                                    {
                                        sTrailerName = GetTrailerNameFromID((byte[])gpPacket.mTrailerTrack.oTrailers[4], (int)gpPacket.cFleetId);
                                        sSplit = sTrailerName.Split("~".ToCharArray());
                                        if (sSplit.Length == 2)
                                        {
                                            oLiveUpdate.iTrailer5ID = Convert.ToInt32(sSplit[0]);
                                            oLiveUpdate.sTrailer5Name = sSplit[1].Trim();
                                        }
                                    }
                                }
                            }

                            #endregion

                            #region Mass

                            oLiveUpdate.fTotalMass = v.TotalMass;
                            oLiveUpdate.fWeightLimit = v.WeightLimit;

                            #endregion

                            #region Populate the plugin field to indicate if this messages is commingout of flash

                            // gpPacket._receivedFlashAvailable  on an incomming packet indicates there is more data to be unloaded from memory
                            // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                            bFlashInfo = new byte[2];
                            if (gpPacket._receivedFlashAvailable)
                                bFlashInfo[0] = 0x01;
                            else
                                bFlashInfo[0] = 0x00;
                            if (gpPacket.bArchivalData)
                                bFlashInfo[1] = 0x01;
                            else
                                bFlashInfo[1] = 0x00;
                            oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);

                            #endregion
                            #region Populate the plugin field to indicate the HDOP and number of satellites

                            if (gpPacket.mECMAdvanced != null)
                            {
                                if (gpPacket.mECMAdvanced.SatellitesHDOP() > 0 || gpPacket.mECMAdvanced.SatellitesInView() > 0 || gpPacket.mECMAdvanced.Altitude() > 0)
                                {
                                    MemoryStream oMS = new MemoryStream();
                                    oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.SatellitesHDOP()), 0, 4);
                                    oMS.WriteByte(gpPacket.mECMAdvanced.SatellitesInView());
                                    oMS.Write(BitConverter.GetBytes(gpPacket.mECMAdvanced.Altitude()), 0, 2);
                                    oPluginData = new cPluginData(PluginIds.SatelliteInfo, oMS.ToArray());
                                    oLiveUpdate.AddPluginDataField(oPluginData);
                                }
                                if (gpPacket.mECMAdvanced.GForceZAxis() > 0 || gpPacket.mECMAdvanced.GForceTemperature() > 0)
                                {
                                    MemoryStream oMS = new MemoryStream();
                                    oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceZAxis())), 0, 4);
                                    oMS.Write(BitConverter.GetBytes(Convert.ToSingle(gpPacket.mECMAdvanced.GForceTemperature())), 0, 4);
                                    oPluginData = new cPluginData(PluginIds.GForceInfo, oMS.ToArray());
                                    oLiveUpdate.AddPluginDataField(oPluginData);
                                }
                            }

                            #endregion
                            #region Populate the plugin field to provide the current route data.

                            if (gpPacket.mExtendedVariableDetails != null && gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID > 0 && gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex >= 0)
                            {
                                byte[] bRouteInfo = new byte[8];
                                byte[] bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID);
                                bRouteInfo[0] = bConv[0];
                                bRouteInfo[1] = bConv[1];
                                bRouteInfo[2] = bConv[2];
                                bRouteInfo[3] = bConv[3];
                                bConv = BitConverter.GetBytes(gpPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex);
                                bRouteInfo[4] = bConv[0];
                                bRouteInfo[5] = bConv[1];
                                bRouteInfo[6] = bConv[2];
                                bRouteInfo[7] = bConv[3];
                                oPluginData = new cPluginData(PluginIds.RouteInfo, bRouteInfo);
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }

                            #endregion
                            #region Populate the plugin field to CAN vehicle speed.

                            if (gpPacket.mTransportExtraDetails != null && gpPacket.mTransportExtraDetails.iCANVehicleSpeed > 0)
                            {
                                byte[] bCANVehicleSpeed = new byte[4];
                                byte[] bConv = BitConverter.GetBytes(gpPacket.mTransportExtraDetails.iCANVehicleSpeed);
                                bCANVehicleSpeed[0] = bConv[0];
                                bCANVehicleSpeed[1] = bConv[1];
                                bCANVehicleSpeed[2] = bConv[2];
                                bCANVehicleSpeed[3] = bConv[3];
                                oPluginData = new cPluginData(PluginIds.CanVehicleSpeed, bCANVehicleSpeed);
                                oLiveUpdate.AddPluginDataField(oPluginData);
                            }

                            #endregion
                            #region Populate the plugin field to provide more accurate speed readings
                            if (gpPacket.mECMAdvanced != null)
                            {
                                float speedAccuracy = gpPacket.mECMAdvanced.SpeedAccuracyDecimalPlaces;
                                float maxSpeedAccuracy = gpPacket.mECMAdvanced.MaxSpeedAccuracyDecimalPlaces;
                                if ((speedAccuracy > 0 && gpPacket.mFix != null) || (maxSpeedAccuracy > 0 && gpPacket.mDistance != null))
                                {
                                    if (gpPacket.mFix != null)
                                        speedAccuracy += ((float)((int)gpPacket.mFix.cSpeed));
                                    else
                                        speedAccuracy = 0;

                                    if (gpPacket.mDistance != null)
                                        maxSpeedAccuracy += ((float)((int)gpPacket.mDistance.cMaxSpeed));
                                    else
                                        maxSpeedAccuracy = 0;

                                    MemoryStream oMS = new MemoryStream();
                                    oMS.Write(BitConverter.GetBytes(speedAccuracy), 0, 4);
                                    oMS.Write(BitConverter.GetBytes(maxSpeedAccuracy), 0, 4);
                                    oPluginData = new cPluginData(PluginIds.AccurateSpeed, oMS.ToArray());
                                    oLiveUpdate.AddPluginDataField(oPluginData);
                                }
                            }
                            #endregion
                            if (scheduleTaskPluginData != null)
                            {
                                oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                            }

                            sErrMsg = "";
                            byte[] bytes = oLiveUpdate.Encode(ref sErrMsg);
                            if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                            if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate); SendSMSEmail(oLiveUpdate);
                            if (sErrMsg == "")
                            {
                                oKeys.Add((double)5.0);
                                oMsgVariants.Add((double)5.0, bytes);
                            }
                            else
                                WriteToConsole(sErrMsg);

                            #endregion

                            TellClients(gpPacket.cFleetId, oMsgVariants, oKeys, false, false);
                            UpdateMySQL(Convert.ToInt32(gpPacket.iVehicleId), gpPacket.mFix);
                            v.MostRecentLiveUpdatePacket = gpPacket;
                        }
                        else
                        {
                            #region Mass Declaration Packet Live Update

                            MassDeclarationPacket massPacket = new MassDeclarationPacket(aPacket, _serverTime_DateFormat);
                            if (massPacket.mCurrentClock.IsDate(null))
                                dtReportTime = massPacket.mCurrentClock.ToDateTime();
                            if (!massPacket.bArchivalData && !dtReportTime.Equals(DateTime.MinValue))
                                v.MostRecentLiveReportTime = dtReportTime;

                            #region If there is no time attached to the packet, don't send it to the clients

                            if (massPacket.mFixClock.iYear == 0 && massPacket.mFixClock.iMonth == 0 && massPacket.mFixClock.iDay == 0 && massPacket.mFixClock.iHour == 0 && massPacket.mFixClock.iMinute == 0 && massPacket.mFixClock.iSecond == 0)
                                return;
                            if (massPacket.mFixClock.iMonth == 0 && massPacket.mFixClock.iDay == 0 && massPacket.mFixClock.iHour == 0 && massPacket.mFixClock.iMinute == 0 && massPacket.mFixClock.iSecond == 0)
                                return;

                            #endregion

                            #region If the lat/long is 0, check if there is a last known position (within the last 24hrs) that can be used, otherwise update the last known postion

                            sKey = massPacket.cFleetId.ToString().PadLeft(5, '0') + massPacket.iVehicleId.ToString().PadLeft(5, '0');
                            if ((massPacket.mFix.dLatitude == 0) || (massPacket.mFix.dLongitude == 0))
                            {
                                oTestDate = new DateTime(Convert.ToInt32(massPacket.mFixClock.iYear) + 2000, Convert.ToInt32(massPacket.mFixClock.iMonth), Convert.ToInt32(massPacket.mFixClock.iDay), Convert.ToInt32(massPacket.mFixClock.iHour), Convert.ToInt32(massPacket.mFixClock.iMinute), Convert.ToInt32(massPacket.mFixClock.iSecond), 0);
                                oCurrentDate = DateTime.Now.ToUniversalTime();
                                oTS = oCurrentDate.Subtract(oTestDate);
                                iHoursDiff = oTS.TotalHours;
                                if (iHoursDiff > 24 || iHoursDiff < -24)
                                    return;

                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                    {
                                        string sValue = (string)oLastKnowPosition[sKey];
                                        string[] sValues = sValue.Split("~".ToCharArray());
                                        if (sValues.Length == 2)
                                        {
                                            massPacket.mFix.dLatitude = Convert.ToDecimal(sValues[0]);
                                            massPacket.mFix.dLongitude = Convert.ToDecimal(sValues[1]);
                                        }
                                    }
                                }
                                // If we still don't have a position, don't send the update.
                                if ((massPacket.mFix.dLatitude == 0) || (massPacket.mFix.dLongitude == 0))
                                {
                                    return;
                                }
                            }
                            else
                            {
                                // If this entry has a postiion, then set the last known position for the unit.
                                lock (oLastKnowPosition.SyncRoot)
                                {
                                    if (oLastKnowPosition.ContainsKey(sKey))
                                        oLastKnowPosition.Remove(sKey);
                                    oLastKnowPosition.Add(sKey, Convert.ToString(massPacket.mFix.dLatitude) + "~" + Convert.ToString(massPacket.mFix.dLongitude));
                                }
                            }

                            #endregion

                            #region Setup the userdefined field

                            // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                            if (_userDefinedDateFormat != null)
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat) + "%0";
                            else
                                sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0') + "%0";

                            #endregion

                            #region Version 5.0 format

                            oLiveUpdate = new LiveUpdate();
                            oLiveUpdate.iPacketVersion = 5;

                            #region Populate fleet/vehicle, reason and time details

                            oLiveUpdate.iFleetID = (short)massPacket.cFleetId;
                            oLiveUpdate.iVehicleID = (int)massPacket.iVehicleId;
                            oLiveUpdate.bIsTrackingUnit = v.IsTrackingUnit;
                            oLiveUpdate.bIsRefrigerationUnit = v.IsRefrigerationUnit;
                            oLiveUpdate.iReasonID = (int)massPacket.cMsgType;
                            if (massPacket.mFixClock != null)
                            {
                                oLiveUpdate.dtGPSTime = massPacket.mFixClock.ToDateTime();
                            }
                            if (massPacket.mCurrentClock != null)
                            {
                                oLiveUpdate.dtDeviceTime = massPacket.mCurrentClock.ToDateTime();
                            }
                            oLiveUpdate.dtServerTime = DateTime.Now;

                            #endregion

                            #region Populate GPS details

                            if (massPacket.mFix != null)
                            {
                                oLiveUpdate.dLat = massPacket.mFix.dLatitude;
                                oLiveUpdate.dLong = massPacket.mFix.dLongitude;
                                oLiveUpdate.bFlags = massPacket.mFix.cFlags;
                                oLiveUpdate.iDirection = (short)massPacket.mFix.iDirection;
                                oLiveUpdate.bSpeed = massPacket.mFix.cSpeed;
                            }

                            #endregion

                            #region Populate Distance details

                            if (massPacket.mDistance != null)
                            {
                                oLiveUpdate.lSpeedAcc = massPacket.mDistance.lSpeedAccumulator;
                                oLiveUpdate.bMaxSpeed = massPacket.mDistance.cMaxSpeed;
                            }

                            #endregion

                            #region Populate Status details

                            if (massPacket.mStatus != null)
                            {
                                oLiveUpdate.bInputStatus = massPacket.mStatus.cInputStatus;
                                oLiveUpdate.bOutputStatus = massPacket.mStatus.cOutputStatus;
                                oLiveUpdate.iLightStatus = massPacket.mStatus.cLightStatus;
                                oLiveUpdate.iButtonStatus = massPacket.mStatus.cButtonStatus;
                            }

                            #endregion

                            #region Populate User Defined details

                            oLiveUpdate.sUserDefined = sUserDefinedValue;

                            #endregion

                            #region Populate Vehicle Location/labeling details

                            if (v != null)
                            {
                                oLiveUpdate.lExtendedStatus = v.MapOldStatusToUnitState();
                                oLiveUpdate.lAuxilaryStatus = v.MapOldStatusToAuxiliaryState();
                                if (v.oPos != null)
                                {
                                    oLiveUpdate.sPosition = v.oPos.Position.Trim();
                                    if (!String.IsNullOrEmpty(massPacket.StreetName))
                                    {
                                        if ((oLiveUpdate.lExtendedStatus & (long)UnitState.AtWayPoint) == (long)UnitState.AtWayPoint || _showPlaceNameForThisReasonIDList.Contains(oLiveUpdate.iReasonID))
                                        {
                                            oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim() + " - " + massPacket.StreetName;
                                        }
                                        else
                                        {
                                            oLiveUpdate.sPlaceName = massPacket.StreetName;
                                        }
                                    }
                                    else
                                        oLiveUpdate.sPlaceName = v.oPos.PlaceName.Trim();
                                    oLiveUpdate.sMapRef = v.oPos.MapRef.Trim();
                                    oLiveUpdate.dDistance = Math.Round(v.oPos.Distance, 0);
                                    oLiveUpdate.iSetPointGroupID = v.oPos.SetPointGroupID;
                                    oLiveUpdate.iSetPointID = v.oPos.SetPointID;
                                }

                                GetPlaceName(massPacket, oLiveUpdate);

                                oLiveUpdate.sUserInfo = v.sUserInfo.Trim();
                                oLiveUpdate.sVehicleTag = sVehicleTag.Trim();
                            }

                            #endregion

                            #region Mass

                            oLiveUpdate.fTotalMass = v.TotalMass;
                            oLiveUpdate.fWeightLimit = v.WeightLimit;

                            #endregion

                            #region Populate the plugin field to indicate if this messages is commingout of flash

                            // gpPacket.bSendFlash  on an incomming packet indicates there is more data to be unloaded from memory
                            // gpPacket.bArchivalData on an incomming packet indicates this packet was unloaded from memory
                            bFlashInfo = new byte[2];
                            if (massPacket._receivedFlashAvailable)
                                bFlashInfo[0] = 0x01;
                            else
                                bFlashInfo[0] = 0x00;
                            if (massPacket.bArchivalData)
                                bFlashInfo[1] = 0x01;
                            else
                                bFlashInfo[1] = 0x00;
                            oPluginData = new cPluginData(PluginIds.FlashInfo, bFlashInfo);
                            oLiveUpdate.AddPluginDataField(oPluginData);

                            #endregion
                            if (scheduleTaskPluginData != null)
                            {
                                oLiveUpdate.AddPluginDataField(scheduleTaskPluginData);
                            }
                         
                            sErrMsg = "";
                            bVer5Format = oLiveUpdate.Encode(ref sErrMsg);
                            if (_luToMsmq != null) _luToMsmq.InsertIntoQueuesAsync(oLiveUpdate);
                            if (_kinesisUpdates != null) _kinesisUpdates.InsertLiveUpdate(oLiveUpdate);
                            SendSMSEmail(oLiveUpdate);
                            if (sErrMsg == "")
                            {
                                oKeys.Add((double)5.0);
                                oMsgVariants.Add((double)5.0, bVer5Format);
                            }
                            else
                                WriteToConsole(sErrMsg);

                            #endregion

                            TellClients(massPacket.cFleetId, oMsgVariants, oKeys, false, false);
                            UpdateMySQL(Convert.ToInt32(massPacket.iVehicleId), massPacket.mFix);
                            v.MostRecentLiveUpdatePacket = massPacket;

                            #endregion
                        }
                        break;
                    default:
                        // Don't send anything to clients
                        break;
                }

                //if live update is an possible accident, set IsWaitingForAccident to false;
                if (aPacket.cMsgType == GeneralGPPacket.POSSIBLE_ACCIDENT)
                {
                    v.IsWaitingForAccident = false;
                }
            }
            catch (System.Exception ex1)
            {
                WriteToErrorConsole(ex1);
                _log.Error(ex1.Message + " : NotifyClientsOfPacket F/V : " + Convert.ToInt32(aPacket.cFleetId).ToString() + "/" + Convert.ToInt32(aPacket.iVehicleId).ToString()
                    + " message type: " + GatewayProtocolLookup.GetMessageType(aPacket.cMsgType));
            }
        }

        public void MakeClientsRefresh(uint[] fleets)
        {
            foreach (uint fleet in fleets)
            {
                try
                {
                    TellClients(Convert.ToInt32(fleet), "!Refresh!", false, true);
                }
                catch (System.Exception ex1)
                {
                    WriteToErrorConsole(ex1);
                }
            }
        }

        private double ConvertDateToCarryFormat()
        {
            double dDays = 0;
            System.DateTime oDate = System.DateTime.Now;
            System.DateTime oStartDate = new DateTime(1899, 12, 30, 0, 0, 0, 0);

            System.TimeSpan oElapsed = oDate.Subtract(oStartDate);

            dDays = oElapsed.TotalDays;

            return dDays;
        }

        private void UpdateMySQL(int iVehicleID, GPPositionFix oGPPosition)
        {
            //			string sSQL = "";
            try
            {
                if (oMySQL != null)
                {
                    if (bLogPacketData)
                        WriteToConsole("Queue MySQL Update.");
                    GPPositionFix oGP = ((GPPositionFix)oGPPosition);

                    //					int iCheckForRec = 0;

                    double dModified = ConvertDateToCarryFormat();

                    //	Queue the update so that it is processed in it's entirety as an atomic unit by the 
                    //	MySql update thread.
                    oMySQL.AddUpdate(new MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.MySQLInterface.MySqlUpdatePacket(
                                         iVehicleID,
                                         oGP.dLatitude,
                                         oGP.dLongitude,
                                         oGP.cSpeed,
                                         oGP.iDirection,
                                         dModified));

                    //					object oValue = oMySQL.ExecuteScalar("SELECT mobileID FROM cmo WHERE mobileid = " + Convert.ToString(iVehicleID) + ";");
                    //					iCheckForRec = Convert.ToInt32(oValue); 
                    //
                    //					if (iCheckForRec > 0)
                    //					{
                    //						sSQL = "UPDATE cmo  ";
                    //						sSQL += "SET Modified = " + sModified + ", Latitude = " + Convert.ToString(oGP.dLatitude) + ", Longitude = " + Convert.ToString(oGP.dLongitude) + ", heading = " + Convert.ToString(oGP.iDirection) + ", speed = " + Convert.ToString(Convert.ToInt32(oGP.cSpeed)) + " ";
                    //						sSQL += "WHERE mobileid = " + Convert.ToString(iVehicleID);
                    //
                    //						oMySQL.ExecuteCmd(sSQL);
                    //					}
                    //					else
                    //					{
                    //						sSQL = "INSERT INTO cmo (mobileid, alias, vehicletype, logcode, GPSFlags, JobNumber, SubJobNumber, Channel, Latitude, Longitude, heading, information, status, speed, OperatorNo1, OperatorNo2, OperatorNo3, OperatorNo4, OperatorNo5, OperatorNo6, OperatorNo7, OperatorNo8, OperatorNo9, OperatorNo10, State, Modified, UserID) ";
                    //						sSQL += "Values(" + Convert.ToString(iVehicleID) + " , '', '', 0, 0, '', 0, 1, " + Convert.ToString(oGP.dLatitude) + ", " + Convert.ToString(oGP.dLongitude) + ", " + Convert.ToString(oGP.iDirection) + ", '', 1, " + Convert.ToString(Convert.ToInt32(oGP.cSpeed)) + ", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, " + sModified + ", 'MTData')";
                    //						oMySQL.ExecuteCmd(sSQL);
                    //
                    //						object oValue2 = oMySQL.ExecuteScalar("SELECT count(Drv_num) FROM driver_info  WHERE Drv_num = " + Convert.ToString(iVehicleID) + ";");
                    //						iCheckForRec = Convert.ToInt32(oValue2); 
                    //
                    //						if(iCheckForRec > 0)
                    //						{
                    //							sSQL = "UPDATE driver_info Set mobileid = " + Convert.ToString(iVehicleID) + "  WHERE Drv_num = " + Convert.ToString(iVehicleID);
                    //							oMySQL.ExecuteCmd(sSQL);
                    //						}
                    //					}
                    //
                    //					sSQL = "INSERT INTO log (LogCode, GPSFlags, Mobileid, Category, JobNumber, SubJobNumber, Channel, Status, Latitude, Longitude, Speed, Heading, Information, Modified, UserID) " +
                    //						"Values (6, 0, " + Convert.ToString(iVehicleID) + ", 6, '', 0, 6, 1, " + Convert.ToString(oGP.dLatitude) + ", " + Convert.ToString(oGP.dLongitude) + ", " + Convert.ToString(Convert.ToInt32(oGP.cSpeed)) + ", " + Convert.ToString(oGP.iDirection) + ", '', '" + sModified + "', 'MTData')";
                    //
                    //					oMySQL.ExecuteCmd(sSQL);
                }
            }
            catch (System.Exception ex)
            {
                WriteToErrorConsole(ex);
            }
        }

        #region TellClients Methods

        /// <summary>
        /// Send a message that has only one format for all client versions.
        /// </summary>
        /// <param name="targetFleet"></param>
        /// <param name="messageString"></param>
        /// <param name="bUpdateFromFlash"></param>
        private void TellClients(int targetFleet, string messageString, bool bUpdateFromFlash, bool nonTrackingUpdate)
        {
            try
            {
                // Encode the client message
                byte[] message = System.Text.Encoding.ASCII.GetBytes(messageString);
                // Create a new message variant table.
                Hashtable oMsgVariants = new Hashtable();
                // Add variant type 0 and the encoded bytes
                oMsgVariants.Add((double)0, message);
                // Send the message.
                TellClients(targetFleet, oMsgVariants, "Client Update", bUpdateFromFlash, nonTrackingUpdate);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "TellClients(int targetFleet, string messageString, bool bUpdateFromFlash)", ex);
            }
        }

        /// <summary>
        /// Send a message that has only one format for all client versions.
        /// </summary>
        /// <param name="targetFleet"></param>
        /// <param name="message"></param>
        /// <param name="bUpdateFromFlash"></param>
        public void TellClients(int targetFleet, byte[] message, bool bUpdateFromFlash, bool nonTrackingUpdate)
        {
            try
            {
                // Create a new message variant table.
                Hashtable oMsgVariants = new Hashtable();
                // Add variant type 0 and the encoded bytes
                oMsgVariants.Add((double)0, message);
                // Send the message.
                TellClients(targetFleet, oMsgVariants, "Client Update", bUpdateFromFlash, nonTrackingUpdate);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "TellClients(int targetFleet, byte[] message, bool bUpdateFromFlash)", ex);
            }
        }

        /// <summary>
        /// Send a message that has different formats depending on the client version.
        /// </summary>
        /// <param name="targetFleet"></param>
        /// <param name="oMessageVariants"></param>
        /// <param name="oVariantKeys"></param>
        /// <param name="bUpdateFromFlash"></param>
        private void TellClients(int targetFleet, Hashtable oMessageVariants, ArrayList oVariantKeys, bool bUpdateFromFlash, bool nonTrackingUpdate)
        {
            try
            {
                // Cerate a new hash table
                Hashtable oMsgVariants = new Hashtable();
                // For each variant key
                for (int X = 0; X < oVariantKeys.Count; X++)
                {
                    double variantKey = (double)oVariantKeys[X];
                    // If there is a message variant for the key
                    if (oMessageVariants.ContainsKey(variantKey))
                    {
                        byte[] message = null;
                        if (variantKey < 4.0)
                            // Encode the message variant
                            message = System.Text.Encoding.ASCII.GetBytes((string)oMessageVariants[variantKey]);
                        else
                            message = (byte[])oMessageVariants[variantKey];

                        // Add the message variant and it's key to the hash table.
                        oMsgVariants.Add(variantKey, message);
                    }
                }
                // Pass the message variants to be sent out to interested clients.
                TellClients(targetFleet, oMsgVariants, "Client Update", bUpdateFromFlash, nonTrackingUpdate);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "TellClients(int targetFleet, Hashtable oMessageVariants, ArrayList oVariantKeys, bool bUpdateFromFlash)", ex);
            }
        }

        /// <summary>
        /// Send a message to all clients within a specific fleet, the oMsgVariants tables contains all versions of the message, keyed by client version.
        /// </summary>
        /// <param name="targetFleet"></param>
        /// <param name="oMsgVariants"></param>
        /// <param name="messageType"></param>
        /// <param name="bUpdateFromFlash"></param>
        private void TellClients(int requestedFleet, Hashtable oMsgVariants, string messageType, bool bUpdateFromFlash, bool nonTrackingUpdate)
        {
            // Don't notify about activity on fleet 0 -
            // Although a client may say they are interested,
            // they definitely don't want to know about units
            // which are booting up etc...
            if (requestedFleet == 0) return;
            if (bUpdateFromFlash && mFromFlashFleetList.Count == 0) return;

            lock (this.UDPClientListSync)
            {
                ArrayList interestedClients = null;
                if ((requestedFleet == -1) || (!bRestrictUpdates))
                {
                    lock (this.UDPClientListSync)
                    {
                        interestedClients = new ArrayList(mClientList.ToArray());
                    }
                }
                else
                {
                    uint targetFleet = 0;
                    try
                    {
                        targetFleet = Convert.ToUInt32(requestedFleet);
                    }
                    catch (System.Exception ex)
                    {
                        _log.Info("Invalid Fleet ID on client update : " + Convert.ToString(requestedFleet) + ", Error : " + ex.Message);
                        return;
                    }

                    //if non tracking update set, send to all clients only interested in non tracking update
                    if (nonTrackingUpdate)
                    {
                        if (mFleetNonTrackingList.Contains(targetFleet))
                        {
                            interestedClients = (ArrayList)mFleetNonTrackingList[targetFleet];
                            if (interestedClients != null)
                            {
                                SendMessageToClients(oMsgVariants, messageType, interestedClients);
                            }
                            interestedClients = null;
                        }
                    }


                    if (bUpdateFromFlash)
                    {
                        #region Send the update to all connected clients for that fleet and registered for updates from flash reports

                        if (mFromFlashFleetList.Contains(targetFleet))
                            interestedClients = (ArrayList)mFromFlashFleetList[targetFleet];

                        #endregion
                    }
                    else
                    {
                        #region Send the update to all connected clients for that fleet

                        if (mFleetList.Contains(targetFleet))
                            interestedClients = (ArrayList)mFleetList[targetFleet];

                        #endregion
                    }
                }
                // Send update to client that has registered and interest in this fleet.
                if (interestedClients != null)
                {
                    SendMessageToClients(oMsgVariants, messageType, interestedClients);
                }
                else
                {
                    if (bLogPacketData)
                        WriteToConsole("No Clients registered for updates from fleet " + Convert.ToString(requestedFleet) + ".");
                }
            }
        }

        private void SendMessageToClients(Hashtable oMsgVariants, string messageType, ArrayList interestedClients)
        {
            IPEndPoint client = null;
            bool bSent = false;
            double clientVersion = 0;
            for (int loop = interestedClients.Count - 1; loop >= 0; loop--)
            {
                try
                {
                    // Reset the var to indicate is something was sent to the client.
                    bSent = false;
                    // Reset the client version number.
                    clientVersion = 0;
                    // Get an interested client address from the list.
                    client = (IPEndPoint)interestedClients[loop];
                    // Get the version of the client.  0 is the default version for pre 2.8 client releases.
                    if (mClientVersion.ContainsKey(client))
                        clientVersion = (double)mClientVersion[client];

                    // If there is a variant message for the client version
                    if (oMsgVariants.ContainsKey(clientVersion))
                    {
                        // Get the message
                        byte[] message = (byte[])oMsgVariants[clientVersion];
                        // Send the message
                        mClientListenerThread.Send(message, message.Length, client);
                        // Set the flag to indicate the message was sent correctly.
                        bSent = true;
                        // If we are logging packet data, then update the log.
                        if (bLogPacketData)
                        {
                            if (message != null)
                            {
                                string sIPAddress = "0.0.0.0";
                                string sPort = "0";

                                #region Get the IP address

                                try
                                {
                                    if (client != null)
                                    {
                                        if (client.Address != null)
                                        {
                                            sIPAddress = client.Address.ToString();
                                        }
                                    }
                                }
                                catch (System.Exception)
                                {
                                    sIPAddress = "0.0.0.0";
                                }

                                #endregion

                                #region Get the Client Port

                                try
                                {
                                    if (client != null)
                                    {
                                        if (client.Address != null)
                                        {
                                            sPort = Convert.ToString(client.Port);
                                        }
                                    }
                                }
                                catch (System.Exception)
                                {
                                    sPort = "0";
                                }

                                #endregion

                                string formatString = "Sent " + messageType + " to " + sIPAddress + ":" + sPort + ", Data : " + System.BitConverter.ToString(message) + "\r\n";
                                _log.Info(formatString);
                            }
                        }
                    }
                    else
                    {
                        // If there wasn't a variant for the client version and the client is not running version 0
                        if (clientVersion != 0)
                        {
                            //	It the message is not available in the selected format, fint the highest value
                            //  of variant that there is a value for, that is below the given client version.
                            double maxVersion = 0d;

                            foreach (double versionKey in oMsgVariants.Keys)
                                if ((versionKey > maxVersion) && (versionKey < clientVersion))
                                    maxVersion = versionKey;

                            // Get the message in the default format.
                            byte[] message = (byte[])oMsgVariants[maxVersion];
                            if (message != null)
                            {
                                // Send the message
                                mClientListenerThread.Send(message, message.Length, client);
                                // Set the flag to indicate the message was sent correctly.
                                bSent = true;
                                // If we are logging packet data, then update the log.
                                if (bLogPacketData)
                                {
                                    WriteToConsole(string.Format("Sent {0} to {1}:{2}, Data : {3}", message, client.Address.ToString(), client.Port, System.Text.Encoding.ASCII.GetString(message)));
                                }
                            }
                        }
                    }

                    // If no message was sent to the client..
                    if (!bSent)
                        WriteToConsole("No message could be found for client version " + clientVersion.ToString() + ", nothing was sent to " + client.Address.ToString() + ":" + Convert.ToString(client.Port));
                }
                catch (System.Net.Sockets.SocketException sockEx)
                {
                    WriteToErrorConsole(sockEx);
                    WriteToConsole("Error with socket : Removing Client IP From UdpateList : " + client.Address.ToString() + ":" + client.Port);
                    interestedClients.RemoveAt(loop);
                }
                catch (System.Exception ex1)
                {
                    WriteToErrorConsole(ex1);
                }
            }
        }

        #endregion

        public void SendClientsGroupRefresh(int targetFleet)
        {
            // Send "Test" to all clients - if they respond they will not be reaped.
            if (bLogPacketData)
                WriteToConsole("Sending Group Updates for Fleet " + Convert.ToString(targetFleet) + ".");

            _updateWriter.GroupUpdate(new PacketCreaterContext(new int[] { targetFleet }, "Group Update"));
        }

        #region Log Event Interface Support

        private void WriteToConsole(string sMsg)
        {
            if (eConsoleEvent != null) eConsoleEvent(sMsg);
        }

        private void WriteToErrorConsole(System.Exception ex)
        {
            string sMsg = "Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
            WriteToConsole(sMsg);
        }

        #endregion

        private void subThread_eConsoleEvent(string sMsg)
        {
            if (eConsoleEvent != null)
                eConsoleEvent(sMsg);
        }

        /// <summary>
        /// This event is fired if the client registry failed to send to an IP address.
        /// </summary>
        /// <param name="ipAddress"></param>
        private void mClientListenerThread_eRemoteConnectionClosed(IPEndPoint ipAddress)
        {
            if (ipAddress != null)
                CleanupClientConnection(ipAddress);
        }

        /// <summary>
        /// This event is triggered if a socket receive raises a Disconnected Host exception.
        /// Accelerate the ReapClient process to clean out invalid clients.
        /// </summary>
        /// <param name="sender"></param>
        private void mClientListenerThread_eSocketConnectionClosed(object sender)
        {
            if (sender != null && sender is IPEndPoint)
                CleanupClientConnection((IPEndPoint)sender);
        }

        private bool _clientCleanupInProgress = false;

        private void CleanupClientConnection(IPEndPoint ipAddress)
        {
            try
            {
                while (_clientCleanupInProgress)
                    Thread.Sleep(50);
                _clientCleanupInProgress = true;
                lock (this.UDPClientListSync)
                {
                    for (int X = mClientList.Count - 1; X >= 0; X--)
                    {
                        IPEndPoint client = mClientList[X];
                        if (client.Equals(ipAddress))
                        {
                            mClientList.RemoveAt(X);
                            // Get the version of the client.  0 is the default version for pre 2.8 client releases.
                            if (mClientVersion.ContainsKey(client))
                            {
                                mClientVersion.Remove(client);
                            }
                        }
                    }
                    bool changesMade = false;
                    if (mFromFlashFleetList != null && mFromFlashFleetList.Count > 0)
                    {
                        uint[] keys = new uint[mFromFlashFleetList.Count];
                        mFromFlashFleetList.Keys.CopyTo(keys, 0);
                        foreach (uint key in keys)
                        {
                            ArrayList clients = (ArrayList)mFromFlashFleetList[key];
                            for (int X = clients.Count - 1; X >= 0; X--)
                            {
                                IPEndPoint client = (IPEndPoint)clients[X];
                                if (client.Equals(ipAddress))
                                {
                                    clients.RemoveAt(X);
                                    changesMade = true;
                                }
                            }
                            if (changesMade)
                            {
                                changesMade = false;
                                mFromFlashFleetList[key] = clients;
                            }
                        }
                    }


                    if (mFleetList != null && mFleetList.Count > 0)
                    {
                        uint[] keys = new uint[mFleetList.Count];
                        mFleetList.Keys.CopyTo(keys, 0);
                        foreach (uint key in keys)
                        {
                            changesMade = false;
                            ArrayList clients = (ArrayList)mFleetList[key];
                            for (int X = clients.Count - 1; X >= 0; X--)
                            {
                                IPEndPoint client = (IPEndPoint)clients[X];
                                if (client.Equals(ipAddress))
                                {
                                    clients.RemoveAt(X);
                                    changesMade = true;
                                }
                            }
                            if (changesMade)
                                mFleetList[key] = clients;
                        }
                    }

                    if (mFleetNonTrackingList != null && mFleetNonTrackingList.Count > 0)
                    {
                        uint[] keys = new uint[mFleetNonTrackingList.Count];
                        mFleetNonTrackingList.Keys.CopyTo(keys, 0);
                        foreach (uint key in keys)
                        {
                            changesMade = false;
                            ArrayList clients = (ArrayList)mFleetNonTrackingList[key];
                            for (int X = clients.Count - 1; X >= 0; X--)
                            {
                                IPEndPoint client = (IPEndPoint)clients[X];
                                if (client.Equals(ipAddress))
                                {
                                    clients.RemoveAt(X);
                                    changesMade = true;
                                }
                            }
                            if (changesMade)
                                mFleetNonTrackingList[key] = clients;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                WriteToConsole("mClientListenerThread_eRemoteConnectionClosed(IPEndPoint ipAddress)");
                WriteToErrorConsole(ex);
            }
            finally
            {
                _clientCleanupInProgress = false;
            }
        }

        /// <summary>
        /// This will be raised when a message is created to be send to clients.
        /// The context will be the list of fleets affected, so send the 
        /// message to all clients registered in those fleets
        /// </summary>
        /// <param name="context"></param>
        /// <param name="packet"></param>
        private void _updateWriter_OutboundPacket(object context, byte[] packet)
        {
            PacketCreaterContext createrContext = (PacketCreaterContext)context;

            // Create a new message variant table.
            Hashtable oMsgVariants = new Hashtable();
            // Add variant type 0 and the encoded bytes
            oMsgVariants.Add((double)0, packet);

            // If there is no list of fleets attached, send it to fleet id -1
            if (createrContext.FleetIDs == null)
                TellClients(-1, oMsgVariants, createrContext.UpdateType, false, true);
            else
                // For each fleet, send the message to interested clients.
                for (int loop = 0; loop < createrContext.FleetIDs.Length; loop++)
                    TellClients(createrContext.FleetIDs[loop], oMsgVariants, createrContext.UpdateType, false, true);
        }

        private void _packetInterpreter_VehicleDriverChange(object context, int fleetID, int vehicleID, int driverID, string driverName)
        {
            _updateWriter.DriverChanged(new PacketCreaterContext(new int[] { fleetID }, "Vehicle Driver Changed"), fleetID, vehicleID, driverID, driverName);
        }

        private void _packetInterpreter_VehicleActiveStateChange(object context, int fleetId, int vehicleId, bool isActive)
        {
            _updateWriter.VehicleActiveStateChange(new PacketCreaterContext(new int[] { fleetId }, "Vehicle Active Status Changed"), fleetId, vehicleId, isActive);
        }

        public void PassBytesToInterfaceServer(int iFleetID, byte[] bData)
        {
            if (mFleetInterfaceServers != null)
            {
                lock (mFleetInterfaceServers.SyncRoot)
                {
                    if (mFleetInterfaceServers.ContainsKey(iFleetID))
                    {
                        IPEndPoint[] oEPs = (IPEndPoint[])mFleetInterfaceServers[iFleetID];
                        for (int X = 0; X < oEPs.Length; X++)
                        {
                            IPEndPoint oEP = oEPs[X];
                            _log.Info("Sending Message for fleet " + Convert.ToString(iFleetID) + " to Interface server - " + oEP.Address.ToString() + ":" + Convert.ToString(oEP.Port) + "  : " + System.Text.ASCIIEncoding.ASCII.GetString(bData, 0, bData.Length));
                            UDPSender.UDPClientSendMessage(oEP, System.Text.ASCIIEncoding.ASCII.GetString(bData, 0, bData.Length));
                        }
                    }
                }
            }
        }

        private void _packetInterpreter_VehicleMDTMessage(object context, int fleetID, int vehicleID, string sMsg)
        {
            _log.Info("Recieved MDT Message request from client : Fleet " + Convert.ToString(fleetID) + " - Unit " + Convert.ToString(vehicleID) + " : " + sMsg);
            if (mFleetInterfaceServers != null)
            {
                lock (mFleetInterfaceServers.SyncRoot)
                {
                    if (mFleetInterfaceServers.ContainsKey(fleetID))
                    {
                        string sSendMsg = "M_" + Convert.ToString(fleetID) + "_" + Convert.ToString(vehicleID) + "_" + sMsg;
                        IPEndPoint[] oEPs = (IPEndPoint[])mFleetInterfaceServers[fleetID];
                        for (int X = 0; X < oEPs.Length; X++)
                        {
                            IPEndPoint oEP = oEPs[X];
                            _log.Info("Sending Messqage to Interface server - " + oEP.Address.ToString() + ":" + Convert.ToString(oEP.Port));
                            UDPSender.UDPClientSendMessage(oEP, sSendMsg);
                        }
                    }
                }
            }
        }

        #region Get the trailer names from ESN numbers.

        private void LoadTrailerESNs()
        {
            DataSet oDSTrailers = null;

            try
            {
                if (oTrailerESNs == null)
                {
                    oTrailerESNs = Hashtable.Synchronized(new Hashtable());
                }
                oDSTrailers = mDatabaseInterface.GetTrailerESNs();
                if (oDSTrailers != null)
                {
                    if (oDSTrailers.Tables.Count == 1)
                    {
                        if (oDSTrailers.Tables[0].Rows.Count > 0)
                        {
                            lock (oTrailerESNs.SyncRoot)
                            {
                                oTrailerESNs.Clear();
                                for (int X = 0; X < oDSTrailers.Tables[0].Rows.Count; X++)
                                {
                                    if (oDSTrailers.Tables[0].Rows[X]["Name"] != System.DBNull.Value && oDSTrailers.Tables[0].Rows[X]["ESN"] != System.DBNull.Value)
                                    {
                                        string sID = Convert.ToString(oDSTrailers.Tables[0].Rows[X]["ID"]);
                                        int iFleetID = Convert.ToInt32(oDSTrailers.Tables[0].Rows[X]["FleetID"]);
                                        string sName = Convert.ToString(oDSTrailers.Tables[0].Rows[X]["Name"]);
                                        long lKey = GetTrailerKey((byte[])oDSTrailers.Tables[0].Rows[X]["ESN"], iFleetID);
                                        if (!oTrailerESNs.ContainsKey(lKey))
                                            oTrailerESNs.Add(lKey, sID + "~" + sName);
                                        lKey = GetTrailerKey((byte[])oDSTrailers.Tables[0].Rows[X]["ESN"], 0);
                                        if (!oTrailerESNs.ContainsKey(lKey))
                                            oTrailerESNs.Add(lKey, sID + "~" + sName);
                                    }
                                }
                            }
                            dtTrailerListTimestamp = System.DateTime.Now;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadTrailerESNs()", ex);
            }
        }

        private long GetTrailerKey(byte[] bESN, int iFleetID)
        {
            long lRet = 0;
            try
            {
                byte[] bKeyConvert = new byte[8];
                bKeyConvert[0] = bESN[0];
                bKeyConvert[1] = bESN[1];
                bKeyConvert[2] = bESN[2];
                bKeyConvert[3] = bESN[3];
                bKeyConvert[4] = bESN[4];
                bKeyConvert[5] = bESN[5];
                byte[] bFleetID = BitConverter.GetBytes(iFleetID);
                bKeyConvert[6] = bFleetID[0];
                bKeyConvert[7] = bFleetID[1];
                lRet = BitConverter.ToInt64(bKeyConvert, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetTrailerKey(byte[] bESN, int iFleetID)", ex);
                lRet = 0;
            }
            return lRet;
        }

        private string GetTrailerNameFromID(byte[] bTrailerESN, int iFleetID)
        {
            string sRet = "";
            try
            {
                // If we don't have a table of trailer IDs, then load it.
                if (oTrailerESNs == null)
                    LoadTrailerESNs();
                // If the table loaded
                if (oTrailerESNs != null)
                {
                    long lKey = GetTrailerKey(bTrailerESN, iFleetID);

                    lock (oTrailerESNs.SyncRoot)
                    {
                        if (oTrailerESNs.ContainsKey(lKey))
                        {
                            sRet = (string)oTrailerESNs[lKey];
                        }
                    }

                    if (sRet == "")
                    {
                        lKey = GetTrailerKey(bTrailerESN, 0);
                        lock (oTrailerESNs.SyncRoot)
                        {
                            if (oTrailerESNs.ContainsKey(lKey))
                            {
                                sRet = (string)oTrailerESNs[lKey];
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetTrailerNameFromID(byte[] bTrailerESN, int iFleetID)", ex);
            }
            return sRet;
        }

        #endregion

        public void DriverPointsCurrentRuleGroupChanged(int iFleetID, byte[] bData)
        {
            TellClients(iFleetID, bData, false, true);
        }

        #region Route State Updates

        // This event is fired when a client requests updates for a given fleet
        private void _packetInterpreter_RouteStateUpdateRemove(object context, int fleetId)
        {
            try
            {
                IPEndPoint oEndPoint = (IPEndPoint)context;
                lock (mRouteLiveUpdRegister_FtoEP.SyncRoot)
                {
                    #region Update the mRouteLiveUpdRegister_FtoEP register

                    if (mRouteLiveUpdRegister_FtoEP.ContainsKey(fleetId))
                    {
                        // Get the list of IPEndPoints assosiated with the fleetId
                        ArrayList oInterestedClients = (ArrayList)mRouteLiveUpdRegister_FtoEP[fleetId];
                        // If the list contains a IPEndPoint for this fleet, then remove the IPEndPoint from the list.
                        if (oInterestedClients != null && oInterestedClients.Contains(oEndPoint))
                            oInterestedClients.Remove(oEndPoint);
                        // If the list is empty, then remove the fleet/vehicle fleetId from the mRouteLiveUpdRegister_FtoEP registry
                        if (oInterestedClients == null || oInterestedClients.Count == 0)
                            mRouteLiveUpdRegister_FtoEP.Remove(fleetId);
                    }

                    #endregion
                }
                lock (mRouteLiveUpdRegister_EPtoF.SyncRoot)
                {
                    #region Update the mRouteLiveUpdRegister_EPtoF register

                    if (mRouteLiveUpdRegister_EPtoF.ContainsKey(oEndPoint))
                    {
                        // Get the list of keys assosiated with the IPEndPoint
                        ArrayList oListOfFleetKeys = (ArrayList)mRouteLiveUpdRegister_EPtoF[oEndPoint];
                        // If the list contains this fleetId, then remove the fleetId from the list.
                        if (oListOfFleetKeys != null && oListOfFleetKeys.Contains(fleetId))
                            oListOfFleetKeys.Remove(fleetId);
                        // If the list is empty, then remove the IPEndPoint reference from the mRouteLiveUpdRegister_EPtoF registry
                        if (oListOfFleetKeys == null || oListOfFleetKeys.Count == 0)
                            mRouteLiveUpdRegister_EPtoF.Remove(oEndPoint);
                    }

                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_packetInterpreter_RouteStateUpdateRemove(object context, int fleetId)", ex);
            }
        }

        // This event is fired when a client requests a stop to updates for a given fleet
        private void _packetInterpreter_RouteStateUpdateAdd(object context, int fleetId)
        {
            // mRouteLiveUpdRegister_FtoEP
            // mRouteLiveUpdRegister_EPtoF

            try
            {
                IPEndPoint oEndPoint = (IPEndPoint)context;

                if (bLogPacketData && oEndPoint != null)
                {
                    try
                    {
                        string formatString = "Client Registered for Route Live Update : " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + ", Fleet : " + fleetId + "\r\n";
                        _log.Info(formatString);
                    }
                    catch (System.Exception exLogging)
                    {
                        _log.Error(sClassName + "_packetInterpreter_RouteStateUpdateAdd(object context, int fleetId) - Logging Error", exLogging);
                    }
                }

                lock (mRouteLiveUpdRegister_EPtoF.SyncRoot)
                {
                    if (mRouteLiveUpdRegister_EPtoF.ContainsKey(oEndPoint))
                    {
                        ArrayList oListOfFleetKeys = (ArrayList)mRouteLiveUpdRegister_EPtoF[oEndPoint];
                        if (!oListOfFleetKeys.Contains(fleetId))
                            oListOfFleetKeys.Add(fleetId);
                    }
                    else
                    {
                        ArrayList oListOfFleetKeys = new ArrayList();
                        oListOfFleetKeys.Add(fleetId);
                        mRouteLiveUpdRegister_EPtoF.Add(oEndPoint, oListOfFleetKeys);
                    }
                }

                lock (mRouteLiveUpdRegister_FtoEP.SyncRoot)
                {
                    if (mRouteLiveUpdRegister_FtoEP.ContainsKey(fleetId))
                    {
                        ArrayList oInterestedClients = (ArrayList)mRouteLiveUpdRegister_FtoEP[fleetId];
                        if (!oInterestedClients.Contains(oEndPoint))
                            oInterestedClients.Add(oEndPoint);
                    }
                    else
                    {
                        ArrayList oInterestedClients = new ArrayList();
                        oInterestedClients.Add(oEndPoint);
                        mRouteLiveUpdRegister_FtoEP.Add(fleetId, oInterestedClients);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_packetInterpreter_RouteStateUpdateAdd(object context, int fleetId)", ex);
            }
        }

        // This method will remove all references to a IPEndPoint
        private void RouteUpdateRemoveEndPoint(IPEndPoint oEndPoint)
        {
            try
            {
                ArrayList oRemoveTheseFleetKeys = new ArrayList();
                lock (mRouteLiveUpdRegister_EPtoF.SyncRoot)
                {
                    #region Update the mRouteLiveUpdRegister_EPtoF register

                    if (mRouteLiveUpdRegister_EPtoF.ContainsKey(oEndPoint))
                    {
                        // Get the list of keys assosiated with the IPEndPoint
                        ArrayList oListOfFleetKeys = (ArrayList)mRouteLiveUpdRegister_EPtoF[oEndPoint];
                        if (oListOfFleetKeys != null)
                        {
                            #region Take a copy of the fleet/vehicle keys registered against this IPEndPoint into oRemoveTheseFleetKeys

                            for (int X = 0; X < oListOfFleetKeys.Count; X++)
                            {
                                if (!oRemoveTheseFleetKeys.Contains(oListOfFleetKeys[X]))
                                    oRemoveTheseFleetKeys.Add(oListOfFleetKeys[X]);
                            }

                            #endregion
                        }
                    }
                    // Remove the IPEndPoint from the mRouteLiveUpdRegister_EPtoF registry
                    mRouteLiveUpdRegister_EPtoF.Remove(oEndPoint);

                    #endregion
                }
                lock (mRouteLiveUpdRegister_FtoEP.SyncRoot)
                {
                    #region Update the mRouteLiveUpdRegister_FtoEP register

                    for (int X = 0; X < oRemoveTheseFleetKeys.Count; X++)
                    {
                        int key = (int)oRemoveTheseFleetKeys[X];
                        if (mRouteLiveUpdRegister_FtoEP.ContainsKey(key))
                        {
                            // Get the list of IPEndPoints assosiated with the fleet/vehicle key
                            ArrayList oInterestedClients = (ArrayList)mRouteLiveUpdRegister_FtoEP[key];
                            // If the list contains a IPEndPoint for this fleet/vehicle, then remove the IPEndPoint from the list.
                            if (oInterestedClients != null && oInterestedClients.Contains(oEndPoint))
                            {
                                if (bLogPacketData && oEndPoint != null)
                                {
                                    try
                                    {
                                        string formatString = "Removing Client Registeristration for Route Live Update : " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + "\r\n";
                                        _log.Info(formatString);
                                    }
                                    catch (System.Exception exLogging)
                                    {
                                        _log.Error(sClassName + "RouteUpdateRemoveEndPoint(IPEndPoint oEndPoint) - Logging Error", exLogging);
                                    }
                                }
                                oInterestedClients.Remove(oEndPoint);
                            }
                            // If the list is empty, then remove the fleet/vehicle key from the mRouteLiveUpdRegister_FtoEP registry
                            if (oInterestedClients == null || oInterestedClients.Count == 0)
                                mRouteLiveUpdRegister_FtoEP.Remove(key);
                        }
                    }

                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RouteUpdateRemoveEndPoint(IPEndPoint oEndPoint)", ex);
            }
        }

        // This method selects the route information for this leg and sends it as a live update
        private void RouteUpdateCreateAndSend(long tableId)
        {
            int fleetId;
            int vehicleID;
            int driverID;
            int vehicleScheduleID;
            int checkPointIndex;
            double actualDepartTime = DateTime.MinValue.ToOADate();
            double scheduledDepartTime = actualDepartTime;
            double estimatedCPArrivalTime = actualDepartTime;
            double scheduledCPArrivalTime = actualDepartTime;
            double scheduledCustomerArrivalTime = actualDepartTime;
            double estimatedCustomerArrivalTime = actualDepartTime;
            double estimatedRouteEndTime = actualDepartTime;
            double scheduledRouteEndTime = actualDepartTime;
            byte[] bMessage;
            string ErrMsg = "";
            try
            {
                DataRow dtRouteUpdateValues = mDatabaseInterface.GetRouteUpdateValues(tableId);
                if (dtRouteUpdateValues != null)
                {
                    fleetId = Convert.ToInt32(dtRouteUpdateValues["FleetID"]);
                    vehicleID = Convert.ToInt32(dtRouteUpdateValues["VehicleID"]);
                    driverID = Convert.ToInt32(dtRouteUpdateValues["DriverID"]);
                    vehicleScheduleID = Convert.ToInt32(dtRouteUpdateValues["VehicleScheduleID"]);
                    checkPointIndex = Convert.ToInt32(dtRouteUpdateValues["CheckPointIndex"]);
                    if (dtRouteUpdateValues["ActualDepartTime"] != System.DBNull.Value)
                        actualDepartTime = Convert.ToDateTime(dtRouteUpdateValues["ActualDepartTime"]).ToOADate();
                    if (dtRouteUpdateValues["ScheduledDepartTime"] != System.DBNull.Value)
                        scheduledDepartTime = Convert.ToDateTime(dtRouteUpdateValues["ScheduledDepartTime"]).ToOADate();
                    if (dtRouteUpdateValues["ExpectedCPArriveTime"] != System.DBNull.Value)
                        estimatedCPArrivalTime = Convert.ToDateTime(dtRouteUpdateValues["ExpectedCPArriveTime"]).ToOADate();
                    if (dtRouteUpdateValues["ScheduledCPArriveTime"] != System.DBNull.Value)
                        scheduledCPArrivalTime = Convert.ToDateTime(dtRouteUpdateValues["ScheduledCPArriveTime"]).ToOADate();
                    if (dtRouteUpdateValues["ExpectedCustomerArriveTime"] != System.DBNull.Value)
                        estimatedCustomerArrivalTime = Convert.ToDateTime(dtRouteUpdateValues["ExpectedCustomerArriveTime"]).ToOADate();
                    if (dtRouteUpdateValues["ScheduledCustomerArriveTime"] != System.DBNull.Value)
                        scheduledCustomerArrivalTime = Convert.ToDateTime(dtRouteUpdateValues["ScheduledCustomerArriveTime"]).ToOADate();
                    if (dtRouteUpdateValues["ExpectedRouteEndTime"] != System.DBNull.Value)
                        estimatedRouteEndTime = Convert.ToDateTime(dtRouteUpdateValues["ExpectedRouteEndTime"]).ToOADate();
                    if (dtRouteUpdateValues["ScheduledRouteEndTime"] != System.DBNull.Value)
                        scheduledRouteEndTime = Convert.ToDateTime(dtRouteUpdateValues["ScheduledRouteEndTime"]).ToOADate();


                    RouteStateUpdate oUpdate = new RouteStateUpdate(fleetId, vehicleID, driverID, vehicleScheduleID, checkPointIndex, actualDepartTime, scheduledDepartTime, estimatedCPArrivalTime, scheduledCPArrivalTime, estimatedCustomerArrivalTime, scheduledCustomerArrivalTime, estimatedRouteEndTime, scheduledRouteEndTime);

                    if (driverID > 0)
                    {
                        DataTable dtDriverBreaks = null;
                        try
                        {
                            dtDriverBreaks = mDatabaseInterface.GetNextDriverBreaks(driverID);
                        }
                        catch (Exception exDriverBreak)
                        {
                            _log.Error(sClassName + "RouteUpdateCreateAndSend(long tableId) - Error retrieving driver breaks.", exDriverBreak);
                            dtDriverBreaks = null;
                        }

                        if (dtDriverBreaks != null && dtDriverBreaks.Rows.Count > 0)
                        {
                            foreach (DataRow drDriverBreak in dtDriverBreaks.Rows)
                            {
                                try
                                {
                                    if (drDriverBreak["Start"] != System.DBNull.Value && drDriverBreak["BreakLength"] != System.DBNull.Value && drDriverBreak["Type"] != System.DBNull.Value)
                                    {
                                        FatigueBreakItem oBreak = new FatigueBreakItem();
                                        oBreak.NextBreakTime = Convert.ToDateTime(drDriverBreak["Start"]).ToOADate();
                                        oBreak.BreakMinLength = Convert.ToInt32(drDriverBreak["BreakLength"]);
                                        oBreak.BreakType = Convert.ToInt32(drDriverBreak["Type"]);
                                        oUpdate.ScheduledBreaks.Add(oBreak);
                                    }
                                }
                                catch (Exception exDriverBreakItem)
                                {
                                    _log.Error(sClassName + "RouteUpdateCreateAndSend(long tableId) - Error adding driver breaks to packet.", exDriverBreakItem);
                                }
                            }
                        }
                    }

                    bMessage = oUpdate.EncodeByStream(ref ErrMsg);
                    if (ErrMsg == "")
                        TellClientInterestedInRouteUpdates(fleetId, bMessage);
                    else
                    {
                        _log.Info(sClassName + "RouteUpdateCreateAndSend(long tableId) - Failed to encode route update Error : " + ErrMsg);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RouteUpdateCreateAndSend(long tableId)", ex);
            }
        }

        // This method tries to match the fleet ID to interested clients
        private void TellClientInterestedInRouteUpdates(int fleetId, byte[] bMessage)
        {
            try
            {
                lock (mRouteLiveUpdRegister_FtoEP.SyncRoot)
                {
                    if (mRouteLiveUpdRegister_FtoEP.Contains(fleetId))
                    {
                        // Get a list of IPEndPOints interested in the fleet/vehicle fleetId
                        ArrayList oInterestedClients = (ArrayList)mRouteLiveUpdRegister_FtoEP[fleetId];
                        for (int X = 0; X < oInterestedClients.Count; X++)
                        {
                            #region Send the update to each client

                            IPEndPoint oEndPoint = (IPEndPoint)oInterestedClients[X];
                            bool bSucess = false;

                            #region Try to send the update to the client

                            try
                            {
                                mClientListenerThread.Send(bMessage, bMessage.Length, oEndPoint);
                                bSucess = true;
                            }
                            catch (Exception)
                            {
                                _log.Info("Update not recieved : Removing " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + " from persistance update list for fleet " + fleetId.ToString());
                            }

                            #endregion

                            // If we are logging packet data, then update the log.
                            if (bSucess && bLogPacketData && bMessage != null && oEndPoint != null)
                            {
                                try
                                {
                                    string formatString = "";

                                    #region Get the message type

                                    if (bMessage.Length > 4)
                                    {
                                        byte[] bMsgType = new byte[2];
                                        bMsgType[0] = bMessage[3];
                                        bMsgType[1] = bMessage[4];
                                        formatString = "Sent " + System.Text.ASCIIEncoding.ASCII.GetString(bMsgType) + " to " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + ", Data : " + System.BitConverter.ToString(bMessage) + "\r\n";
                                    }
                                    else
                                        formatString = "Sent to " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + ", Data : " + System.BitConverter.ToString(bMessage) + "\r\n";

                                    #endregion

                                    _log.Info(formatString);
                                }
                                catch (System.Exception exLogging)
                                {
                                    _log.Error(sClassName + "TellClientInterestedInRouteUpdates(int fleetId, byte[] bMessage) - Logging Error", exLogging);
                                }
                            }
                            if (!bSucess)
                            {
                                #region If the send failed, remove all references to the IPEndPoint from the Persist Update registries

                                try
                                {
                                    RouteUpdateRemoveEndPoint(oEndPoint);
                                }
                                catch (Exception)
                                {
                                }

                                #endregion
                            }

                            #endregion
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "TellClientInterestedInRouteUpdates(int fleetId, byte[] bMessage)", ex);
            }
        }

        #endregion

        #region ECM Live Updates

        // This event is fired when an update completes
        private void mDatabaseInterface_eSendECMLiveUpdate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues)
        {
            try
            {
                Hashtable oMsgVariants = new Hashtable();
                ArrayList oKeys = new ArrayList();

                ECMUpdate oECMLiveUpdate = new ECMUpdate(fleetId, vehicleId, dGPSTime, _location, _latitude, _longitude);
                for (int X = 0; X < oECMValues.Length; X++)
                    oECMLiveUpdate.AddECMValue(oECMValues[X].SPN_ID, (byte)oECMValues[X].SourceType, (byte)oECMValues[X].Flags, oECMValues[X].RawData);

                if (_ecmLuToMsmq != null)
                    _ecmLuToMsmq.InsertIntoQueuesAsync(oECMLiveUpdate);

                string sErr = "";
                byte[] bUpdateMessage = oECMLiveUpdate.Encode(ref sErr);
                if (sErr != "")
                    _log.Info("mDatabaseInterface_eSendECMLiveUpdate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues) - Failed to encode update.  Error : " + sErr);
                else
                    TellClientInterestedInECMLiveUpdate(fleetId, vehicleId, bUpdateMessage);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "mDatabaseInterface_eSendECMLiveUpdate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues)", ex);
            }
        }

        // This event is fired when a pendant alarm needs to be resent
        private void mDatabaseInterface_eResendPendantAlarm(GatewayProtocolPacket aPacket, Vehicle v)
        {
            NotifyClientsOfPacket(aPacket, v, false);
        }

        // This event is fired when a client requests a stop to updates for a given fleet/vehicle
        private void _packetInterpreter_ECMLiveUpdateRemove(object context, int fleetId, int vehicleId)
        {
            IPEndPoint oEndPoint = (IPEndPoint)context;
            ulong key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(vehicleId);

            lock (mECMLiveUpdRegister_FVtoEP.SyncRoot)
            {
                #region Update the mECMLiveUpdRegister_FVtoEP register

                if (mECMLiveUpdRegister_FVtoEP.ContainsKey(key))
                {
                    // Get the list of IPEndPoints assosiated with the fleet/vehicle key
                    ArrayList oInterestedClients = (ArrayList)mECMLiveUpdRegister_FVtoEP[key];
                    // If the list contains a IPEndPoint for this fleet/vehicle, then remove the IPEndPoint from the list.
                    if (oInterestedClients != null && oInterestedClients.Contains(oEndPoint))
                        oInterestedClients.Remove(oEndPoint);
                    // If the list is empty, then remove the fleet/vehicle key from the mECMLiveUpdRegister_FVtoEP registry
                    if (oInterestedClients == null || oInterestedClients.Count == 0)
                        mECMLiveUpdRegister_FVtoEP.Remove(key);
                }

                #endregion
            }
            lock (mECMLiveUpdRegister_EPtoFV.SyncRoot)
            {
                #region Update the mECMLiveUpdRegister_EPtoFV register

                if (mECMLiveUpdRegister_EPtoFV.ContainsKey(oEndPoint))
                {
                    // Get the list of keys assosiated with the IPEndPoint
                    ArrayList oListOfFleetVehicleKeys = (ArrayList)mECMLiveUpdRegister_EPtoFV[oEndPoint];
                    // If the list contains a key for this fleet/vehicle, then remove the key from the list.
                    if (oListOfFleetVehicleKeys != null && oListOfFleetVehicleKeys.Contains(key))
                        oListOfFleetVehicleKeys.Remove(key);
                    // If the list is empty, then remove the IPEndPoint reference from the mECMLiveUpdRegister_EPtoFV registry
                    if (oListOfFleetVehicleKeys == null || oListOfFleetVehicleKeys.Count == 0)
                        mECMLiveUpdRegister_EPtoFV.Remove(oEndPoint);
                }

                #endregion
            }
        }

        // This event is fired when a client requests a feed of updates for a given fleet/vehicle
        private void _packetInterpreter_ECMLiveUpdateAdd(object context, int fleetId, int vehicleId)
        {
            IPEndPoint oEndPoint = (IPEndPoint)context;
            ulong key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(vehicleId);

            if (bLogPacketData && oEndPoint != null)
            {
                try
                {
                    string formatString = "Client Registered for ECM Live Update : " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + ", Fleet : " + fleetId + ", Vehicle : " + vehicleId.ToString() + "\r\n";
                    _log.Info(formatString);
                }
                catch (System.Exception exLogging)
                {
                    _log.Error(sClassName + "_packetInterpreter_ECMLiveUpdateAdd(object context, int fleetId, int vehicleId) - Logging Error", exLogging);
                }
            }

            lock (mECMLiveUpdRegister_EPtoFV.SyncRoot)
            {
                if (mECMLiveUpdRegister_EPtoFV.ContainsKey(oEndPoint))
                {
                    ArrayList oListOfFleetVehicleKeys = (ArrayList)mECMLiveUpdRegister_EPtoFV[oEndPoint];
                    if (!oListOfFleetVehicleKeys.Contains(key))
                        oListOfFleetVehicleKeys.Add(key);
                }
                else
                {
                    ArrayList oListOfFleetVehicleKeys = new ArrayList();
                    oListOfFleetVehicleKeys.Add(key);
                    mECMLiveUpdRegister_EPtoFV.Add(oEndPoint, oListOfFleetVehicleKeys);
                }
            }

            lock (mECMLiveUpdRegister_FVtoEP.SyncRoot)
            {
                if (mECMLiveUpdRegister_FVtoEP.ContainsKey(key))
                {
                    ArrayList oInterestedClients = (ArrayList)mECMLiveUpdRegister_FVtoEP[key];
                    if (!oInterestedClients.Contains(oEndPoint))
                        oInterestedClients.Add(oEndPoint);
                }
                else
                {
                    ArrayList oInterestedClients = new ArrayList();
                    oInterestedClients.Add(oEndPoint);
                    mECMLiveUpdRegister_FVtoEP.Add(key, oInterestedClients);
                }
            }
        }

        // This method will remove all references to a IPEndPoint
        private void ECMLiveUpdateRemoveEndPoint(IPEndPoint oEndPoint)
        {
            ArrayList oRemoveTheseFleetVehicleKeys = new ArrayList();
            lock (mECMLiveUpdRegister_EPtoFV.SyncRoot)
            {
                #region Update the mECMLiveUpdRegister_EPtoFV register

                if (mECMLiveUpdRegister_EPtoFV.ContainsKey(oEndPoint))
                {
                    // Get the list of keys assosiated with the IPEndPoint
                    ArrayList oListOfFleetVehicleKeys = (ArrayList)mECMLiveUpdRegister_EPtoFV[oEndPoint];
                    if (oListOfFleetVehicleKeys != null)
                    {
                        #region Take a copy of the fleet/vehicle keys registered against this IPEndPoint into oRemoveTheseFleetVehicleKeys

                        for (int X = 0; X < oListOfFleetVehicleKeys.Count; X++)
                        {
                            oRemoveTheseFleetVehicleKeys.Add(oListOfFleetVehicleKeys[X]);
                        }

                        #endregion
                    }
                }
                // Remove the IPEndPoint from the mECMLiveUpdRegister_EPtoFV registry
                mECMLiveUpdRegister_EPtoFV.Remove(oEndPoint);

                #endregion
            }
            lock (mECMLiveUpdRegister_FVtoEP.SyncRoot)
            {
                #region Update the mECMLiveUpdRegister_FVtoEP register

                for (int X = 0; X < oRemoveTheseFleetVehicleKeys.Count; X++)
                {
                    ulong key = (ulong)oRemoveTheseFleetVehicleKeys[X];
                    if (mECMLiveUpdRegister_FVtoEP.ContainsKey(key))
                    {
                        // Get the list of IPEndPoints assosiated with the fleet/vehicle key
                        ArrayList oInterestedClients = (ArrayList)mECMLiveUpdRegister_FVtoEP[key];
                        // If the list contains a IPEndPoint for this fleet/vehicle, then remove the IPEndPoint from the list.
                        if (oInterestedClients != null && oInterestedClients.Contains(oEndPoint))
                        {
                            if (bLogPacketData && oEndPoint != null)
                            {
                                try
                                {
                                    string formatString = "Removing Client Registeristration for ECM Live Update : " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + "\r\n";
                                    _log.Info(formatString);
                                }
                                catch (System.Exception exLogging)
                                {
                                    _log.Error(sClassName + "ECMLiveUpdateRemoveEndPoint(IPEndPoint oEndPoint) - Logging Error", exLogging);
                                }
                            }
                            oInterestedClients.Remove(oEndPoint);
                        }
                        // If the list is empty, then remove the fleet/vehicle key from the mECMLiveUpdRegister_FVtoEP registry
                        if (oInterestedClients == null || oInterestedClients.Count == 0)
                            mECMLiveUpdRegister_FVtoEP.Remove(key);
                    }
                }

                #endregion
            }
        }

        // This method tries to match the fleet/vehicle ID to interested clients
        private void TellClientInterestedInECMLiveUpdate(int fleetId, int vehicleId, byte[] bMessage)
        {
            try
            {
                ulong key = (Convert.ToUInt64(fleetId) << 32) + Convert.ToUInt64(vehicleId);
                lock (mECMLiveUpdRegister_FVtoEP.SyncRoot)
                {
                    if (mECMLiveUpdRegister_FVtoEP.Contains(key))
                    {
                        // Get a list of IPEndPOints interested in the fleet/vehicle key
                        ArrayList oInterestedClients = (ArrayList)mECMLiveUpdRegister_FVtoEP[key];
                        for (int X = 0; X < oInterestedClients.Count; X++)
                        {
                            #region Send the update to each client

                            IPEndPoint oEndPoint = (IPEndPoint)oInterestedClients[X];
                            bool bSucess = false;

                            #region Try to send the update to the client

                            try
                            {
                                mClientListenerThread.Send(bMessage, bMessage.Length, oEndPoint);
                                bSucess = true;
                            }
                            catch (Exception)
                            {
                                _log.Info("Update not recieved : Removing " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + " from persistance update list for fleet/vehicle " + fleetId.ToString() + "/" + vehicleId.ToString());
                            }

                            #endregion

                            // If we are logging packet data, then update the log.
                            if (bSucess && bLogPacketData && bMessage != null && oEndPoint != null)
                            {
                                try
                                {
                                    string formatString = "";

                                    #region Get the message type

                                    if (bMessage.Length > 4)
                                    {
                                        byte[] bMsgType = new byte[2];
                                        bMsgType[0] = bMessage[3];
                                        bMsgType[1] = bMessage[4];
                                        formatString = "Sent " + System.Text.ASCIIEncoding.ASCII.GetString(bMsgType) + " to " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + ", Data : " + System.BitConverter.ToString(bMessage) + "\r\n";
                                    }
                                    else
                                        formatString = "Sent to " + oEndPoint.Address.ToString() + ":" + oEndPoint.Port.ToString() + ", Data : " + System.BitConverter.ToString(bMessage) + "\r\n";

                                    #endregion

                                    _log.Info(formatString);
                                }
                                catch (System.Exception exLogging)
                                {
                                    _log.Error(sClassName + "TellClientInterestedInECMLiveUpdate(int fleetId, int vehicleId, byte[] bMessage) - Logging Error", exLogging);
                                }
                            }
                            if (!bSucess)
                            {
                                #region If the send failed, remove all references to the IPEndPoint from the Persist Update registries

                                try
                                {
                                    ECMLiveUpdateRemoveEndPoint(oEndPoint);
                                }
                                catch (Exception)
                                {
                                }

                                #endregion
                            }

                            #endregion
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "TellClientInterestedInECMLiveUpdate(int fleetId, int vehicleId, byte[] bMessage)", ex);
            }
        }

        #endregion

        #region Reason Code Live Updates

        private void _packetInterpreter_ReasonCodeGroupUpdate(object context, int iFleetID, DataSet dsGroupDetails)
        {
            XMLUpdate oXMLUpdate = new XMLUpdate();
            string sErrMsg = "";
            byte[] bClientUpdate = null;
            try
            {
                // Only client running application protocol version 5 can receive this mesage.
                if (dsGroupDetails != null && dsGroupDetails.Tables.Count > 0 && dsGroupDetails.Tables[0].Rows.Count > 0)
                {
                    oXMLUpdate.cSubCmd = 'R';
                    oXMLUpdate.iItemID = iFleetID;
                    oXMLUpdate.ConvertDataSetToXML(dsGroupDetails);
                    bClientUpdate = oXMLUpdate.Encode(ref sErrMsg);
                    Hashtable oMsgVariants = new Hashtable();
                    oMsgVariants.Add((double)5, bClientUpdate);
                    TellClients(iFleetID, oMsgVariants, "Reason Code Overide Group Update", false, true);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_packetInterpreter_ReasonCodeGroupUpdate(object context, int iFleetID, DataSet dsGroupDetails)", ex);
            }
        }

        #endregion

        #region Pendent Alarm Acks

        private void _packetInterpreter_PendentAlarmAck(object context, MTData.Transport.Application.Communication.PendentAlarmAck pendentAlarmAck)
        {
            _log.Info("Recieved Pendent Alarm Acknowledgement from client : \r\n" + pendentAlarmAck.ToDisplayString());
            mDatabaseInterface.Database.PendentActiveAlarmList.AcknowledgeAlarm((byte)pendentAlarmAck.iFleetID, (uint)pendentAlarmAck.iVehicleID);
            Vehicle v = mDatabaseInterface.SendPendentAlarmAckToUnit(pendentAlarmAck.iFleetID, pendentAlarmAck.iVehicleID);
            if (v != null)
            {
                GeneralGPPacket oGP = mDatabaseInterface.Database.LoadVehicleDataFromHistory(pendentAlarmAck.iFleetID, pendentAlarmAck.iVehicleID, DateTime.Now.ToUniversalTime(), v);
                oGP.cMsgType = 72;
                oGP.Encode(ref oGP.mDataField);
                NotifyClientsOfPacket(oGP, v, false);
            }
        }

        private void SendSMSEmail(LiveUpdate oLiveUpdate)
        {
            try
            {
                Action pSMSThread = () => { SMSThread(oLiveUpdate); };
                pSMSThread.BeginInvoke(AsyncSMSProcessingCallBack, pSMSThread);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendSMSEmail(LiveUpdate oLiveUpdate)", ex);
            }
        }
        /// <summary>
        /// This method is called when the SendSMSEmail(LiveUpdate oLiveUpdate) calls BeginInvoke is completed.
        /// This function cleans up memory for the async call delegate.
        /// </summary>
        /// <param name="IAsyncResult ar">This is result of the delegate run.</param>
        private void AsyncSMSProcessingCallBack(IAsyncResult ar)
        {
            try
            {
                if (ar.AsyncState is Action)
                    (ar.AsyncState as Action).EndInvoke(ar);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncSMSProcessingCallBack(IAsyncResult ar)", ex);
            }
        }

        private void SMSThread(LiveUpdate oLiveUpdate)
        {
            try
            {
                if (mSMSAlerts != null)
                {
                    List<SMSAlertItem> smsAlerts = mSMSAlerts.GetSMSEmailAlertDetails(oLiveUpdate);
                    if (smsAlerts != null && smsAlerts.Count > 0)
                    {
                        for (int X = 0; X < smsAlerts.Count; X++)
                        {
                            SMSAlertItem smsAlert = smsAlerts[X];
                            try
                            {
                                DataRow drVehicle = mDatabaseInterface.Database.GetVehicleRow((byte)oLiveUpdate.iFleetID, (uint)oLiveUpdate.iVehicleID);
                                string sReason = mDatabaseInterface.Database.GetReasonName(oLiveUpdate.iFleetID, oLiveUpdate.iVehicleID, oLiveUpdate.iReasonID);
                                if (sReason == "")
                                    sReason = "Alarm Acknowledged By User";
                                string[] sSubjectAndBody = smsAlert.GenerateSubjectAndBody(oLiveUpdate, drVehicle, sReason);
                                if (emailHelper != null)
                                {
                                    emailHelper.SendEmail(smsAlert.EMailTargetAddress, smsAlert.EMailSourceAddress, smsAlert.EMailSourceAddress, sSubjectAndBody[0], sSubjectAndBody[1], false);
                                }
                            }
                            catch (Exception)
                            {
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendSMSEmail(LiveUpdate oLiveUpdate)", ex);
            }
        }

        #endregion

        #region Driver Update

        private void _packetInterpreter_DriverUpdated(object context, DriverUpdate update)
        {
            string errMsg = null;
            byte[] b = update.Encode(ref errMsg);

            this.TellClients(update.FleetID, b, false, true);
        }

        private void _packetInterpreter_DriverFatigueUpdated(object context, DriverUpdate update)
        {
            string errMsg = null;
            byte[] b = update.Encode(ref errMsg);

            this.TellClients(update.FleetID, b, false, true);
        }

        #endregion

        private void SetupPlaceNameForThisReasonIDList()
        {
            try
            {
                _showPlaceNameForThisReasonIDList = new List<int>();
                _showPlaceNameForThisReasonIDList.Add(74);
                _showPlaceNameForThisReasonIDList.Add(75);
                _showPlaceNameForThisReasonIDList.Add(76);
                _showPlaceNameForThisReasonIDList.Add(77);
                _showPlaceNameForThisReasonIDList.Add(161);
                _showPlaceNameForThisReasonIDList.Add(162);
                _showPlaceNameForThisReasonIDList.Add(166);
                _showPlaceNameForThisReasonIDList.Add(167);
                _showPlaceNameForThisReasonIDList.Add(168);
                _showPlaceNameForThisReasonIDList.Add(169);
                _showPlaceNameForThisReasonIDList.Add(171);
                _showPlaceNameForThisReasonIDList.Add(172);
                _showPlaceNameForThisReasonIDList.Add(173);
                _showPlaceNameForThisReasonIDList.Add(174);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".", ex);
            }
        }

        private void _packetInterpreter_ExtendedHistoryRequest(object context, int fleetID, int vehicleID)
        {
            _log.InfoFormat("Recieved Extended History Request from client for {0}, {1}", fleetID, vehicleID);
            mDatabaseInterface.UpdateExtendedHistoryRequests(fleetID, vehicleID);
        }
        private void _packetInterpreter_ExtendedHistoryRequestComplete(object context, int fleetID, int vehicleID, DateTime start, DateTime end, int userId)
        {
            _log.InfoFormat("Recieved Extended History Request Complete from client for {0}, {1}, {2}, {3}", fleetID, vehicleID, start.ToString("dd/MM/yyyy HH:mm:ss"), end.ToString("dd/MM/yyyy HH:mm:ss"));
            TellClients(fleetID, string.Format("{0},{1},{2},{3},{4},{5}", PacketConstants.COMMAND_COMMA_EXTENDED_HISTORY_REQUEST_COMPLETE,
                fleetID, vehicleID, start.ToString("dd/MM/yyyy HH:mm:ss"), end.ToString("dd/MM/yyyy HH:mm:ss"), userId), false, true);
        }

        private void _packetInterpreter_DvrAlarm(object context, int fleetId, int vehicleId, int deviceId, int alarmId, DateTime date, decimal latitude, decimal longitude)
        {
            _log.InfoFormat("DVR alarm raised by device. FleetID:{0}, VehicleID:{1}, DeviceID:{2}, AlarmID:{3}, Date:{4}, Latitude:{5}, Longitude:{6}", fleetId, vehicleId, deviceId, alarmId, date.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude);

            mDatabaseInterface.CreateDvrEventRecord(fleetId, vehicleId, deviceId, 0, alarmId, date, latitude, longitude);

            TellClients(fleetId, string.Format("{0},{1},{2},{3},{4},{5},{6},{7}", PacketConstants.COMMAND_COMMA_DVR_ALARM, fleetId, vehicleId, deviceId, alarmId, date.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude), false, true);
        }

        private void _packetInterpreter_DvrAlarmContent(object context, int fleetId, int vehicleId, int deviceId, int alarmId, DateTime date)
        {
            _log.InfoFormat("DVR alarm content. FleetId:{0}, VehicleId:{1}, DeviceId:{2}, AlarmId:{3}, Date:{4}", fleetId, vehicleId, deviceId, alarmId, date.ToString("dd/MM/yyyy HH:mm:ss"));
            TellClients(fleetId, string.Format("{0},{1},{2},{3},{4},{5}", PacketConstants.COMMAND_COMMA_DVR_ALARM_CONTENT, fleetId, vehicleId, deviceId, alarmId, date.ToString("dd/MM/yyyy HH:mm:ss")), false, true);
        }

        private void _packetInterpreter_DvrError(object context, int fleetId, int vehicleId, string device, DateTime timestamp, decimal latitude, decimal longitude, short errorCode)
        {
            _log.InfoFormat("DVR error received from device. FleetID:{0}, VehicleID:{1}, Device:{2}, Date:{3}, Latitude:{4}, Longitude:{5}, ErrorCode:{6}", fleetId, vehicleId, device, timestamp.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude, errorCode);

            mDatabaseInterface.CreateDvrErrorRecord(fleetId, vehicleId, device, timestamp, latitude, longitude, errorCode);
        }

        private void _packetInterpreter_DvrRequest(object context, int fleetId, int vehicleId, int deviceId, int requestId, DateTime date, decimal latitude, decimal longitude)
        {
            _log.InfoFormat("DVR request. FleetId:{0}, VehicleId:{1}, DeviceId:{2}, RequestId:{3}, Date:{4}, Latitude:{5}, Longitude:{6}", fleetId, vehicleId, deviceId, requestId, date.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude);

            mDatabaseInterface.CreateDvrEventRecord(fleetId, vehicleId, deviceId, 1, requestId, date, latitude, longitude);
        }

        private void _packetInterpreter_DvrRequestContent(object context, int fleetId, int vehicleId, int deviceId, int requestId)
        {
            _log.InfoFormat("DVR requested content. FleetId:{0}, VehicleId:{1}, DeviceId:{2}, RequestId:{3}", fleetId, vehicleId, deviceId, requestId);
            TellClients(fleetId, string.Format("{0},{1},{2},{3},{4}", PacketConstants.COMMAND_COMMA_DVR_REQUEST_CONTENT, fleetId, vehicleId, deviceId, requestId), false, true);
        }

        private void _packetInterpreter_DvrStatus(object context, int fleetId, int vehicleId, string device, DateTime timestamp, decimal latitude, decimal longitude, bool isHealthStatus, short errorCode, bool connected, bool recordOn, bool hddOk,
            short hddSize, short hddFree, short videoConnectedChannel, short videoRecordChannel, int timeOffset, string firmwareVersion, string model, string videoFormat)
        {
            _log.InfoFormat("DVR status received from device. FleetID:{0}, VehicleID:{1}, Device:{2}, Date:{3}, Latitude:{4}, Longitude:{5}", fleetId, vehicleId, device, timestamp.ToString("dd/MM/yyyy HH:mm:ss"), latitude, longitude);
            mDatabaseInterface.CreateDvrStatusRecord(fleetId, vehicleId, device, timestamp, latitude, longitude, errorCode, isHealthStatus, connected, recordOn, hddOk, hddSize, hddFree, videoConnectedChannel, videoRecordChannel, timeOffset, firmwareVersion, model, videoFormat);
        }

        /// <summary>
        /// Return the total engine hours value from the ECM Advanced value with the value adjusted by the offset for the vehicle
        /// </summary>
        /// <returns>Return the total engine hours or 0 for invalid value</returns>
        private int GetTotalEngineHours(GPECMAdvanced mECMAdvanced, Vehicle v)
        {
            uint newValue = mECMAdvanced.TotalEngHours();

            if (newValue > 0)
            {
                int offset = v.EngineHourOffset;

                return (int)newValue + offset > 0 ? (int)newValue + offset : 0;
            }

            return 0;
        }

        private static void AddOverSpeedBreachInfo(GatewayProtocolPacket gpPacket, LiveUpdate oLiveUpdate)
        {
            cPluginData oPluginData;

            GPECMAdvancedItem maxGps = null;
            GPECMAdvancedItem maxEcm = null;
            GPECMAdvancedItem maxTime = null;
            GPECMAdvancedItem duration = null;

            if (gpPacket is GeneralGPPacket)
            {
                GeneralGPPacket packet = gpPacket as GeneralGPPacket;
                if (packet != null)
                {
                    maxGps = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 7);
                    maxEcm = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 8);
                    maxTime = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 11);
                    duration = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 12);
                }
            }

            if (gpPacket is RoutePtSetPtGPPacket)
            {
                RoutePtSetPtGPPacket packet = gpPacket as RoutePtSetPtGPPacket;
                if (packet != null)
                {
                    maxGps = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 7);
                    maxEcm = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 8);
                    maxTime = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 11);
                    duration = packet.mECMAdvanced.ECMAdvancedItems.FirstOrDefault(a => a.SourceType == GPECMAdvancedItem.SourceTypes.Common && a.SPN_ID == 12);
                }
            }

            if (maxGps != null || maxEcm != null || maxTime != null || duration != null || (gpPacket != null && gpPacket.RoadTypeData != null))
            {
                MemoryStream oMS = new MemoryStream();
                if (maxGps != null)
                {
                    oMS.Write(BitConverter.GetBytes(maxGps.RawValue), 0, 8);
                }
                else
                {
                    oMS.Write(BitConverter.GetBytes(0d), 0, 8);
                }
                if (maxEcm != null)
                {
                    oMS.Write(BitConverter.GetBytes(maxEcm.RawValue), 0, 8);
                }
                else
                {
                    oMS.Write(BitConverter.GetBytes(0d), 0, 8);
                }
                if (maxTime != null)
                {
                    oMS.Write(BitConverter.GetBytes(maxTime.RawValue), 0, 8);
                }
                else
                {
                    oMS.Write(BitConverter.GetBytes(0d), 0, 8);
                }
                if (duration != null)
                {
                    oMS.Write(BitConverter.GetBytes(duration.RawValue), 0, 8);
                }
                else
                {
                    oMS.Write(BitConverter.GetBytes(0d), 0, 8);
                }

                if (gpPacket != null && gpPacket.RoadTypeData != null)
                {
                    oMS.Write(BitConverter.GetBytes(gpPacket.RoadTypeData.SpeedLimit), 0, 8);
                }
                else
                {
                    oMS.Write(BitConverter.GetBytes(0d), 0, 8);
                }

                oPluginData = new cPluginData(PluginIds.OverSpeedBreachInfo, oMS.ToArray());
                oLiveUpdate.AddPluginDataField(oPluginData);
            }
        }

        private static void GetPlaceName(GatewayProtocolPacket gatewayProtocolPacket, LiveUpdate oLiveUpdate)
        {           
            if (gatewayProtocolPacket.RoadTypeData != null)
            {
                List<string> placeNameParts = new List<string>();
                if (!string.IsNullOrEmpty(gatewayProtocolPacket.RoadTypeData.Street))
                {
                    placeNameParts.Add(gatewayProtocolPacket.RoadTypeData.Street);
                }

                if (!string.IsNullOrEmpty(gatewayProtocolPacket.RoadTypeData.Suburb))
                {
                    placeNameParts.Add(gatewayProtocolPacket.RoadTypeData.Suburb);
                }

                if (placeNameParts.Any())
                {
                    oLiveUpdate.sPlaceName = string.Join(",", placeNameParts);
                }
            }
        }
    }
}

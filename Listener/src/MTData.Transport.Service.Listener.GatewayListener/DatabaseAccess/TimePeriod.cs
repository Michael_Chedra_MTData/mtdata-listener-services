﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class TimePeriod
    {
        private DayOfWeek _startDay;
        private int _startTime;
        private int _duration;

        public DayOfWeek StartDay { get { return _startDay; } }
        public int StartTime { get { return _startTime; } }
        public int Duration { get { return _duration; } }

        public TimePeriod(DayOfWeek startDay, int startTime, int duration)
        {
            _startDay = startDay;
            _startTime = startTime;
            _duration = duration;
        }

        public DateTime CalculateStartTime(DateTime currentTime)
        {
            //find the start day 
            int day = (int)_startDay - (int)currentTime.DayOfWeek;
            //get start day and add the statr minutes
            DateTime start = new DateTime(currentTime.Year, currentTime.Month, currentTime.Day, 0, 0, 0).AddDays(day);
            start = start.AddMinutes(_startTime);
            //check that end date - if before current time then add a week to the start
            DateTime end = start.AddMinutes(_duration);
            if (end < currentTime)
            {
                //add another week
                start = start.AddDays(7);
            }
            return start;
        }
    }
}

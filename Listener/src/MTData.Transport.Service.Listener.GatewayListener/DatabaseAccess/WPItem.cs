using System;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
	/// <summary>
	/// Summary description for cWPItem.
	/// </summary>
	public class cWPItem
	{
		//sp.ID, sp.SetPointGroupID, sp.Name, sp.Latitude, sp.Longitude, sp.XTolerance as ToleranceInNm
		public int SetPointGroupID = 0;
		public int SetPointGroupNumber = 0;
		public int SetPointID = 0;
		public int SetPointNumber = 0;
		public int iWP_Flags = 0;
		public int iZoomLevel = 0;
		public string Name = "";
		public string MapRef = "";
		public decimal dLatitude = 0;
		public decimal dLongitude = 0;
		public decimal dMinLatitude = 0;
		public decimal dMinLongitude = 0;
		public decimal dMaxLatitude = 0;
		public decimal dMaxLongitude = 0;
		public decimal dToleranceInNm = 0;
		public decimal dTopX = 0;
		public decimal dTopY = 0;
		public decimal dBottomX = 0;
		public decimal dBottomY = 0;
		public bool bIsSpecial1 = false;
		public bool bIsSpecial2 = false;
		public PositionDetails oPos = new PositionDetails();

		public cWPItem()
		{
		}

		public int[] CalculateKeys()
		{
			CalculateCorners();
			// Get the boundaries into integer format i.e. decimal degrees rounded to 2 decimal places * 100
			int iTX = Convert.ToInt32(Math.Round(dTopX, 2) * 100);
			int iTY = Convert.ToInt32(Math.Round(dTopY, 2) * 100);
			int iBX = Convert.ToInt32(Math.Round(dBottomX, 2) * 100);
			int iBY = Convert.ToInt32(Math.Round(dBottomY, 2) * 100);
			// Work out how many grids high and wide this way point is
			// 1 grid = 1/100 of a degree
			int iXDistance = Math.Abs(iTX - iBX);
			int iYDistance = Math.Abs(iTY - iBY);
			
			// Find the top corner
			int iStartX = iTX;
			int iStartY = iTY;
			if(iBX < iTX)
				iStartX = iBX;
			if(iBY < iTY)
				iStartY = iBY;

			
			// Create the result array.
			System.Collections.ArrayList oList = new System.Collections.ArrayList();

			// Find the bottom corner
			iXDistance += iStartX;
			iYDistance += iStartY;

			// Minimum tolerance is 1 grid.
			if (iStartX == iXDistance)
				iXDistance++;
			if (iStartY == iYDistance)
				iYDistance++;

			// Populate the array with the grid ids that are touched by this waypoint.
			for(int X = iStartX; X < iXDistance; X++)
			{
				for(int Y = iStartY; Y < iYDistance; Y++)
				{
					oList.Add((Math.Abs(X) * 100000) + Math.Abs(Y));
				}
			}

			int[] iResult = new int[oList.Count];
			for(int X = 0; X < oList.Count; X++)
			{
				iResult[X] = (int) oList[X];
			}			
			return iResult;
		}

		public void CalculateCorners()
		{
			decimal dTolInDegrees = dToleranceInNm / Convert.ToDecimal(60.1);  // 1 deg = 60.1 nm at the equator
			// Calculate the top and bottom of the boundary
			dTopY = dLatitude + dTolInDegrees;
			dBottomY = dLatitude - dTolInDegrees;

			// Factor in the curviture of the earth into the tolerance
			// Use the cos(lat) at the lat at the top of the box.
			decimal dFactor = Convert.ToDecimal(Math.Cos(Convert.ToDouble(dTopY)));
			dTolInDegrees = dTolInDegrees * dFactor;
			
			// Calculate the left and right sides of the box
			dTopX = dLongitude - dTolInDegrees;
			dBottomX = dLongitude + dTolInDegrees;

			// Make sure the boundaries are within -180 to 180 degrees
			if (dTopX >= 180)
				dTopX -= 180;
			if (dTopY >= 180)
				dTopY -= 180;
			if (dBottomX <= -180)
				dBottomX += 180;
			if (dBottomY <= -180)
				dBottomY += 180;

		}

		public bool IsInWayPoint(decimal dCurLat, decimal dCurLong)
		{
			bool bRet = false;
			decimal dTemp = 0;
			if (dBottomY > dTopY)
			{
				dTemp = dBottomY;
				dBottomY = dTopY;
				dTopY = dTemp;
			}

			if (dBottomX > dTopX)
			{
				dTemp = dBottomX;
				dBottomX = dTopX;
				dTopX = dTemp;
			}

			Console.Write("Comparing Points (Vehicle Lat / :Long = " + Convert.ToString(Math.Round(dCurLat, 4)) + " / " + Convert.ToString(Math.Round(dCurLong, 4)) + ")");
			Console.Write("Way Point (Top Lat / :Long = " + Convert.ToString(Math.Round(dTopY, 4)) + " / " + Convert.ToString(Math.Round(dTopX, 4)) + ")");
			Console.Write("         (Bottom Lat / :Long = " + Convert.ToString(Math.Round(dBottomY, 4)) + " / " + Convert.ToString(Math.Round(dBottomX, 4)) + ")");
			if (dCurLat <= dTopY)
			{
				if (dCurLat >= dBottomY)
				{
					if (dCurLong <= dTopX)
					{
						if (dCurLong >= dBottomX)
						{
							Console.Write("Match Found\n");
							bRet = true;
						}
					}
				}
			}
			return bRet;
		}

	}
}

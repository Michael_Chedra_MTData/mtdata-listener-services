﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class ServerLoadLevels : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<ServerLoadLevel> levels = new List<ServerLoadLevel>();

            int maxQueueSize = Convert.ToInt32(ConfigurationManager.AppSettings["MessageQueueMaxSize"]);

            XmlNodeList list = section.SelectNodes("ServerLoadLevel");
            foreach (XmlNode n in list)
            {
                int upper = Convert.ToInt32(n.Attributes["upper"].Value);
                int lower = Convert.ToInt32(n.Attributes["lower"].Value);
                levels.Add(new ServerLoadLevel()
                    {
                        Upper = upper * maxQueueSize / 100,
                        Lower = lower * maxQueueSize / 100,
                        Level = Convert.ToInt32(n.Attributes["index"].Value),
                        SendFlash = Convert.ToBoolean(n.Attributes["sendFlash"].Value),
                        StatusReports = Convert.ToInt32(n.Attributes["statusReports"].Value)
                    });
            }
            return levels;
        }
    }
    
    public class ServerLoadLevel
    {
        public int Upper { get; set; }
        public int Lower { get; set; }
        public int Level { get; set; }
        public bool SendFlash { get; set; }
        public int StatusReports { get; set; }
    }
}

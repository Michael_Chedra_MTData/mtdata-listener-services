﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{

    public class TemporalWayPoint : TemporalObject
    {
        public TemporalWayPoint(int setPointId)
            :base(setPointId)
        {
        }

        /// <summary>
        /// check if the temporal waypoint is active
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="mDatabase"></param>
        /// <param name="v"></param>
        /// <param name="messageType"></param>
        /// <returns>true if waypoint is active and events can be saved normally</returns>
        public bool CheckTimes(DateTime dateTime, DatabaseInterface mDatabase, Vehicle v, byte messageType)
        {
            bool waypointActive = false;

            CheckTimeResult result = CheckTimes(dateTime, mDatabase, v);
            if (result.InsideTimePeriod)
            {
                waypointActive = true;
                //check message type
                if (IsArriveMessage(messageType))
                {
                    mDatabase.ArrivedTemporalWaypoint(this, v, dateTime, dateTime, result.ScheduledDepartTime, 1);
                }
                else if (IsDepartMessage(messageType))
                {
                    //check that no arrive has been triggered yet - if so we don't want to save the deaprt (can't have arrive and depart at the same time)
                    if (!mDatabase.IsTemporalWaypointArrived((int)v.cFleet, (int)v.iUnit, Id))
                    {
                        waypointActive = false;
                    }
                    mDatabase.EndTemporalWaypoint((int)v.cFleet, (int)v.iUnit, Id);
                }
            }
            else
            {
                //outside all time zones, 
                //if arrive add a new scheduled waypoint
                //if depart delete scheduled
                if (IsArriveMessage(messageType))
                {
                    mDatabase.ArrivedTemporalWaypoint(this, v, dateTime, result.ScheduledArriveTime, result.ScheduledDepartTime, 0);
                }
                else if (IsDepartMessage(messageType))
                {
                    mDatabase.EndTemporalWaypoint((int)v.cFleet, (int)v.iUnit, Id);
                }
            }
            return waypointActive;
        }

        private bool IsArriveMessage(byte messageType)
        {
            switch (messageType)
            {
                case RoutePtSetPtGPPacket.SETPT_ARRIVE:
                case RoutePtSetPtGPPacket.SETPT_ARRIVE_WORKSHOP:
                case RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK:
                case RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO:
                case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_ARRIVE:
                    return true;
                default:
                    return false;
            }
        }
        public bool IsDepartMessage(byte messageType)
        {
            switch (messageType)
            {
                case RoutePtSetPtGPPacket.SETPT_DEPART:
                case RoutePtSetPtGPPacket.SETPT_DEPART_WORKSHOP:
                case RoutePtSetPtGPPacket.SETPT_DEPART_DOCK:
                case RoutePtSetPtGPPacket.SETPT_DEPART_NOGO:
                case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_DEPART:
                    return true;
                default:
                    return false;
            }
        }
    }
}

using System;
using System.Collections;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
	/// <summary>
	/// Thread safe Hashtable of ChronologicalList's
	/// </summary>
	public class ChronologicalCache
	{
		private Hashtable			_listsByKey = Hashtable.Synchronized(new Hashtable());
		
		public ChronologicalCache()
		{
		}

		/// <summary>
		/// Get or Create a ChronologicalList for the specified key
		/// </summary>
		/// <param name="key">Key used to retrieve or create ChronologicalList</param>
		/// <returns>A new ChronologicalList if Key is new, else an existing ChronologicalList</returns>
		public ChronologicalList GetOrCreate(object key)
		{
			if (key == null)
				throw new ArgumentException("Key cannot be Null", "key");

			ChronologicalList chronologicalList = (ChronologicalList) _listsByKey[key];		
			if (chronologicalList == null)
			{
				lock(_listsByKey.SyncRoot)
				{
					chronologicalList = (ChronologicalList) _listsByKey[key];		
					if (chronologicalList == null)
					{
						chronologicalList	= new ChronologicalList();
						_listsByKey[key]	= chronologicalList;
					}
				}
			}
			return chronologicalList;
		}
	}
}

using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading;
using System.Net.Sockets;
using log4net;
using MTData.Transport.Application.Communication.MsmqInterface;
using MTData.Transport.Service.Listener.GatewayListener.Dashboard;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;
using MTData.Transport.Gateway.Packet;
using MTData.Common.Threading;
using MTData.Common.Utilities;
using MTData.Transport.Service.Map.Data.Broker;
using MTData.Transport.Service.Map.Data.Parameters;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// Understands Packets, and calls the right DatabaseInterface
	/// functions to add them to the correct tables.
	/// </summary>
    public class PacketDatabaseInterface
    {
        private const string sClassName = "PacketDatabaseInterface.";
        private static ILog _log = LogManager.GetLogger(typeof(PacketDatabaseInterface));
        #region Events and Event Delegates
        public event WriteToConsoleEvent eConsoleEvent;
        public delegate void WriteToConsoleEvent(string sMsg);
        public event ConcreteStatusChangeEvent eConcreteStatusChanged;
        public delegate void ConcreteStatusChangeEvent(int iFleetID, byte[] bData);
        public event DriverPointsCurrentRuleGroupChangedEvent eDriverPointsCurrentRuleGroupChanged;
        public delegate void DriverPointsCurrentRuleGroupChangedEvent(int iFleetID, byte[] bData);
        public delegate void SendECMLiveUpdateDelegate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues);
        public event SendECMLiveUpdateDelegate eSendECMLiveUpdate;
        public delegate void ResendPendantAlarmDelegate(GatewayProtocolPacket aPacket, Vehicle v);
        public event ResendPendantAlarmDelegate eResendPendantAlarm;
        public delegate void SendLiveUpdateDelegate(GatewayProtocolPacket aPacket);
        public event SendLiveUpdateDelegate eSendLiveUpdate;
        #endregion
        #region Public Variables
        public bool bLogPacketData = false;
        public int cFleetToReset;
        public bool bResetFleets = false;
        public int cFleetToRespondTo;
        public bool bRespondToFleets = false;
        public bool bSendCTAAlways = true;
        public bool bAckEveryPacket; // For emergency situations!
        public decimal ConstantPositionThreshold;
        #endregion
        #region Public Properties
        public bool bRevertUnitsToPrimary
        {
            set
            {
                if (mSendThreads != null)
                {
                    mSendThreads.RevertUnitsToPrimary = value;
                }
            }
            get
            {
                return mSendThreads.RevertUnitsToPrimary;
            }
        }
        public DatabaseInterface Database
        {
            get
            {
                return mDatabase;
            }
        }
        public VehicleCollection Vehicles
        {
            get
            {
                return mSendThreads;
            }
        }
        public bool SendCommandTimestamps
        {
            get { return bSendCommandTimestamps; }
            set { bSendCommandTimestamps = value; }
        }
        public void AddRestoreMDTStatusFleet(byte bFleet)
        {
            try
            {
                if (alRestoreMDTStatusForFleet != null)
                    lock (alRestoreMDTStatusForFleet.SyncRoot)
                        if (!alRestoreMDTStatusForFleet.Contains(bFleet))
                            alRestoreMDTStatusForFleet.Add(bFleet);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AddRestoreMDTStatusFleet(byte bFleet)", ex);
            }
        }
        public void SetConcreteFleets(List<byte> fleets)
        {
            alConcreteFleets = fleets;
            //pass it down to the databseInterface
            mDatabase.ConcreteFleets = fleets;
        }
        public void AddCommandStatusFleet(byte bFleet)
        {
            try
            {
                if (alCommandStatusFleets != null)
                    lock (alCommandStatusFleets.SyncRoot)
                        if (!alCommandStatusFleets.Contains(bFleet))
                            alCommandStatusFleets.Add(bFleet);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AddCommandStatusFleet(byte bFleet)", ex);
            }
        }
        public int StationaryMinuteThreshold
        {
            set
            {
                if (mSendThreads != null)
                {
                    mSendThreads.StationaryMinutes = value;
                }
            }
            get
            {
                return mSendThreads.StationaryMinutes;
            }
        }
        public DataRow[] GetScheduledStationaryAlerts(DateTime time)
        {
            return mDatabase.GetScheduledStationaryAlerts(time);
        }
        public DataRow[] GetScheduledOutOfHoursFinished(DateTime time)
        {
            return mDatabase.GetScheduledOutOfHoursFinished(time);
        }
        #endregion
        #region Private Variables
        private EnhancedThread ResendPendantAlertsThread = null;
        private int iAdjustForGPSBounceWhenSpeedBelow = 0;
        private Queue mECMLiveUpdateQ;
        private EnhancedThread mECMLiveUpdateThread;
        private DatabaseInterface mDatabase;
        private UdpClient mSender;
        private VehicleCollection mSendThreads;
        private bool bSendCommandTimestamps = false;
        private ArrayList alRestoreMDTStatusForFleet;
        private List<byte> alConcreteFleets;
        private ArrayList alCommandStatusFleets;
        private float _ignoreOverSpeedPacketsSpeed;
        private List<IgnorePackets.Vehicle> _ignorePackets;
        private IEmailHelper emailHelper;
		#region Delgates
        private GatewayAdministrator.SetAcceptUnitTrafficFlagDel _delAcceptUnitTraffic;
		#endregion
        #endregion
        #region Constructor and Packet Sending functions
        public void Dispose()
        {
            alConcreteFleets = null;
            alRestoreMDTStatusForFleet = null;
            _dashInterface = null;
            try
            {
                if (mSendThreads != null)
                {
                    mSendThreads.eConsoleEvent -= new MTData.Transport.Service.Listener.GatewayListener.VehicleCollection.WriteToConsoleEvent(mSendThreads_eConsoleEvent);
                    mSendThreads.eConcreteStatusChanged -= new MTData.Transport.Service.Listener.GatewayListener.VehicleCollection.ConcreteStatusChangeEvent(mSendThreads_eConcreteStatusChanged);
                    mSendThreads.Dispose();
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mSender != null)
                {
                    mSender.Close();
                    mSender = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mDatabase != null)
                {
                    mDatabase.Dispose();
                    mDatabase = null;
                }
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (mECMLiveUpdateThread != null)
                {
                    if (mECMLiveUpdateThread.Running)
                    {
                        mECMLiveUpdateThread.Stop();
                        int iCount = 0;
                        while (mECMLiveUpdateThread.Stopping && iCount < 500)
                        {
                            Thread.Sleep(10);
                            iCount++;
                        }
                        mECMLiveUpdateThread = null;
                    }
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mECMLiveUpdateQ != null)
                {
                    lock (mECMLiveUpdateQ.SyncRoot)
                        mECMLiveUpdateQ.Clear();
                    mECMLiveUpdateQ = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (ResendPendantAlertsThread != null)
                {
                    if (ResendPendantAlertsThread.Running)
                    {
                        ResendPendantAlertsThread.Stop();
                        int iCount = 0;
                        while (ResendPendantAlertsThread.Stopping && iCount < 500)
                        {
                            Thread.Sleep(10);
                            iCount++;
                        }
                        ResendPendantAlertsThread = null;
                    }
                }
            }
            catch (System.Exception)
            {
            }
            

        }

        /// <summary>
        /// This engine is used to monitor activity and determine whether a vehicle needs to be reset or not.
        /// It also determines whether any live updates should recorded to the database, and passed on to the clients.
        /// </summary>
        private RemoteUpdateEngine _remoteUpdateEngine = null;
        private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";

        private DatabaseAccess.RouteManagement _routeManagement = null;
        private DashboardProcessing.DashInterfaceDelegate _dashInterface = null;
        public PacketDatabaseInterface(string connString,
            UdpClient connClient,
            int iPacketRateLimitInterval,
            int iPacketRateLimitThreshold,
            int commandTimeout,
            RemoteUpdateEngine remoteUpdateEngine,
            string userDefinedDateFormat,
            bool ackAllRequested,
            string serverTime_DateFormat,
            string sUserInfoDefaultField,
            DatabaseAccess.RouteManagement routeManagement,
            PacketSource.PacketExchange packetExchange,
            DashboardProcessing.DashInterfaceDelegate dashInterface,
			GatewayAdministrator.SetAcceptUnitTrafficFlagDel delAcceptUnitTraffic,
            IEmailHelper emailHelper)
        {
            try
            {
                // Allow the processing thread to throttle the incomming packets
                _delAcceptUnitTraffic = delAcceptUnitTraffic;
                this.emailHelper = emailHelper;
	            _dashInterface = dashInterface;
	            mECMLiveUpdateQ = Queue.Synchronized(new Queue());
	            _serverTime_DateFormat = serverTime_DateFormat;
	            _remoteUpdateEngine = remoteUpdateEngine;
	            _routeManagement = routeManagement;
                _ignoreOverSpeedPacketsSpeed = 0;
                if (ConfigurationManager.AppSettings["IgnoreOverSpeedPacketsWithSpeedLessThan"] != null)
                {
                    //need to convert speed from km to knots
                    _ignoreOverSpeedPacketsSpeed = Convert.ToSingle(ConfigurationManager.AppSettings["IgnoreOverSpeedPacketsWithSpeedLessThan"]) / 1.852f;
                }
                _ignorePackets = ConfigurationManager.GetSection("IgnorePackets") as List<IgnorePackets.Vehicle>;
                if (_ignorePackets == null)
                {
                    _ignorePackets = new List<IgnorePackets.Vehicle>();
                }

            }
            catch
            {
				// log this error
            }

            try
            {
                mECMLiveUpdateThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ECMLiveUpdateThreadHandler), null);
                mECMLiveUpdateThread.Start();
            }
            catch (Exception)
            {
            }

            try
            {
                iAdjustForGPSBounceWhenSpeedBelow = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["AdjustForGPSBounce"]);
            }
            catch (System.Exception)
            {
                iAdjustForGPSBounceWhenSpeedBelow = 0;
            }
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["DoNotSendCTAWhilstResendActive"].ToUpper() == "TRUE")
                    bSendCTAAlways = false;
                else
                    bSendCTAAlways = true;
            }
            catch (System.Exception)
            {
                bSendCTAAlways = true;
            }

            mDatabase = new DatabaseInterface(connString, commandTimeout, userDefinedDateFormat, _serverTime_DateFormat, sUserInfoDefaultField, routeManagement, _dashInterface, _delAcceptUnitTraffic, emailHelper);
            mDatabase.eSendECMLiveUpdate += new DatabaseInterface.SendECMLiveUpdateDelegate(mDatabase_eSendECMLiveUpdate);
            //mDatabase.TransactionError += new MTData.Transport.Service.Listener.GatewayListener.DatabaseInterface.TransactionErrorDelegate(mDatabase_TransactionError);
            mSender = connClient;

            mSendThreads = new VehicleCollection(mSender,	// UDP client 
                10000, // Keep track of up to 10000 units
                600,  // flush dead units every 10 minutes
                iPacketRateLimitInterval,	// limit FLASH quantities 
                iPacketRateLimitThreshold, ref mDatabase, _serverTime_DateFormat,
                packetExchange);

            mSendThreads.eConsoleEvent += new MTData.Transport.Service.Listener.GatewayListener.VehicleCollection.WriteToConsoleEvent(mSendThreads_eConsoleEvent);
            mSendThreads.eConcreteStatusChanged += new MTData.Transport.Service.Listener.GatewayListener.VehicleCollection.ConcreteStatusChangeEvent(mSendThreads_eConcreteStatusChanged);
            mSendThreads.eResetUnitEvent += new MTData.Transport.Service.Listener.GatewayListener.VehicleCollection.SendResetToUnitEvent(mSendThreads_eResetUnitEvent);
            mSendThreads.bLogPacketData = bLogPacketData;
            mSendThreads.bAckAllRequested = ackAllRequested;

            //			mFleetsToUpdate = ArrayList.Synchronized(new ArrayList(50));
            alRestoreMDTStatusForFleet = ArrayList.Synchronized(new ArrayList());
            alConcreteFleets = new List<byte>();
            alCommandStatusFleets = ArrayList.Synchronized(new ArrayList());

            try
            {

                EnhancedThread.EnhancedThreadStart ResendPendantAlertsThreadStart = new EnhancedThread.EnhancedThreadStart(ResendPendantAlerts);
                ResendPendantAlertsThread = new EnhancedThread(ResendPendantAlertsThreadStart, null);
                ResendPendantAlertsThread.Name = "Resend Pendant Active Alerts";
                ResendPendantAlertsThread.Start();
            }
            catch (System.Exception)
            {
            }
        }

        // Lists the units held by the Vehicle collection

        public string[] ListUnits()
        {
            return mSendThreads.ListUnits();
        }

        public string[] KnownUnits()
        {
            return mSendThreads.KnownUnits();
        }

        private bool SendPacket(GatewayProtocolPacket aPacket, int aRetries)
        {
            return SendPacket(aPacket, aRetries, 10);
        }

        private bool SendPacket(Vehicle vehicle, GatewayProtocolPacket aPacket, int aRetries)
        {
            return SendPacket(vehicle, aPacket, aRetries, 10);
        }

        private bool SendPacket(GatewayProtocolPacket aPacket, int aRetries, int interval)
        {
            return SendPacket(aPacket, aRetries, interval, false);
        }

        private bool SendPacket(Vehicle vehicle, GatewayProtocolPacket aPacket, int aRetries, int interval)
        {
            return SendPacket(vehicle, aPacket, aRetries, interval, false);
        }

        /// <summary>
        /// Send a packet to a sdpecific vehicle that may not be in the list.,
        /// </summary>
        /// <param name="aPacket"></param>
        /// <param name="aRetries"></param>
        /// <param name="interval"></param>
        /// <param name="bTransitioning"></param>
        /// <returns></returns>
        private bool SendPacket(GatewayProtocolPacket aPacket, int aRetries, int interval, bool bTransitioning)
        {
            Vehicle vehicle = null;
            try
            {
                // A packet we send should never have the ACK REQ bit set:
                aPacket.bAckImmediately = false;


                // Allow the VehicleCollection to look after the 
                // setting of sequence numbers and retries as necessary:
                vehicle = mSendThreads.GetVehicle(aPacket);
                if (vehicle == null)
                    vehicle = AddVehicle(aPacket, false, bTransitioning);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendPacket(GatewayProtocolPacket aPacket, int aRetries, int interval, bool bTransitioning)", ex);
            }

            if (vehicle != null)
                return SendPacket(vehicle, aPacket, aRetries, interval, bTransitioning);
            else
                return false;
        }

        private bool SendPacket(Vehicle vehicle, GatewayProtocolPacket aPacket, int aRetries, int interval, bool bTransitioning)
        {
            try
            {
                if (!mSendThreads.UpdateVehicle(vehicle, aPacket, false, bTransitioning))
                    return false;
                // And send as needed:
                if (aRetries > 0)
                    vehicle.SendWithRetries(aRetries, interval);
                else
                    vehicle.SendWithNoRetries();
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendPacket(Vehicle vehicle, GatewayProtocolPacket aPacket, int aRetries, int interval, bool bTransitioning)", ex);
            }
            return true;
        }

        #endregion
        #region Packet acknowledgement
        // Prevent a particular packet from being re-sent, cos we got
        // an acknowledgement from the vehicle at the other end.
        private void PacketAcked(GatewayProtocolPacket aPacket)
        {
            if (aPacket.iVehicleId == 0)
            {
                // We never send anything with vehicle ID = 0,
                // so we could never expect an ACK back. Ignore.
                return;
            }
            try
            {
                // Destroy the correspoding send thread waiting for an ACK
                mSendThreads.StopMatching(aPacket);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".PacketAcked(GatewayProtocolPacket aPacket)", ex);
            }
        }


        /// <summary>
        /// Update the ack status and send an ack to the unit if required.
        /// </summary>
        /// <param name="aPacket"></param>
        /// <param name="bBlockAck"></param>
        /// <returns></returns>
        private Vehicle UpdateAckStatus(GatewayProtocolPacket aPacket, bool bBlockAck)
        {
            Vehicle myVehicle = null;
            try
            {
                #region ACK-every functionality
                if (((aPacket.iVehicleId == 0) && ((aPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE) || (aPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN) || (aPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE_FULL)))
                    || bAckEveryPacket)
                {
                    // Sometimes a unit might send a packet out of its buffer,
                    // from a time before it was allocated a unit Id.
                    // In this case, it is important that we ack the unit,
                    // otherwise it will get "stuck" waiting for an ack that
                    // we would never otherwise give it. So the effective
                    // policy is unit 0 = ACK REGARDLESS. 
                    aPacket.bAckRegardless = true;

                    //					Under NO Circumstances should we try to relate units based on IP Address.
                    //					IF they can't be found in the list based on unit and fleet, then we do not process them
                    //					IF a fleetid and vehicleid of 0 are passed in, we ignore the report.. it is from an indeterminate unit!
                    //					// However we still need
                    //					// to track sequence numbers for this unit, no matter what
                    //					// it's calling itself. So we try and match on IP details,
                    //					// extract the unit ID and go from there:
                    //					GatewayProtocolPacket matchPacket = mSendThreads.ItemWithSameIPAddress(aPacket);
                    //					if (matchPacket == null) 
                    //					{
                    //						// no match. We're stuffed. We'll carry on, and ack this, but there
                    //						// will probably be problems in the future due to the window for the
                    //						// ACTUAL unit number having a gap in it...
                    //					}
                    //					else
                    //					{	// We located this unit via it's IP address. Now we can treat it
                    //						// as if it arrived with the correct vehicle and fleet numbers!
                    //						aPacket.iVehicleId	= matchPacket.iVehicleId;
                    //						aPacket.cFleetId	= matchPacket.cFleetId;	
                    //					}
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateAckStatus(GatewayProtocolPacket aPacket, bool bBlockAck)", ex);
            }

            #region IM_ALIVE ID changes:
            try
            {
                //	POD - Make sure that we do NOT add Fleet 0, Vehicle 0 to the list, as it will
                //	cause us grief in case of an error
                // Units logging in with an IM_ALIVE should be treated like they
                // had Fleet 0, Unit 0 even if they don't:
                if ((aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE) ||
                    (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN))
                {
                    aPacket.cFleetId = 0;
                    aPacket.iVehicleId = 0;
                }

                // if we don't have an existing vehicle record for this unit, add one:
                myVehicle = mSendThreads.GetVehicle(aPacket);
                if (myVehicle == null)
                    myVehicle = AddVehicle(aPacket, true, false);

                mSendThreads.UpdateVehicle(myVehicle, aPacket, true, bBlockAck);

                //check any tasks on the vehicle
                myVehicle.CheckTasks(aPacket);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateAckStatus(GatewayProtocolPacket aPacket, bool bBlockAck)", ex);
            }
            #endregion
            return myVehicle;
        }

        /// <summary>
        /// This sends a forced packet, ignoring sequence numbers etc..
        /// This allows us to force a unit to progress past some invalid packets.
        /// </summary>
        /// <param name="packet"></param>
        public void SendForcedPacket(GatewayProtocolPacket packet, bool overrideSequence)
        {
            try
            {
                if (((int)packet.cFleetId) > 0 && packet.iVehicleId > 0)
                {
                    Vehicle vehicle = mSendThreads.GetVehicle(packet.cFleetId, packet.iVehicleId);
                    if (vehicle != null)
                        vehicle.SendOnce(packet, overrideSequence);
                }
                else
                {
                    _log.Info("Attempted 'SendForcedPacket' to fleet 0 vehicle 0.");
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "SendForcedPacket(GatewayProtocolPacket packet, bool overrideSequence)", ex);
            }
        }

        #endregion
        #region Push-to-unit functions
        public void SendMiniMailToUnitsInFleet(byte cFleetId,
            string[] units,
            string sSender,
            bool bUrgent,
            string sTime,
            string sSubject,
            string sMessage)
        {
            MailMessagePacket mailPacket = null;
            try
            {
                mailPacket = new MailMessagePacket(_serverTime_DateFormat);
                mailPacket.sSubject = sSubject;
                mailPacket.bUrgent = bUrgent;
                mailPacket.sTargetUser = sSender;
                mailPacket.sMessage = sMessage;
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendMiniMailToUnitsInFleet(byte cFleetId, string[] units, string sSender, bool bUrgent, string sTime, string sSubject, string sMessage)", ex);
            }
            foreach (string unit in units)
            {
                try
                {
                    mSendThreads.SetUnitMailWaitingFlag(cFleetId,
                        Convert.ToUInt16(unit),
                        true,
                        mailPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendMiniMailToUnitsInFleet(byte cFleetId, string[] units, string sSender, bool bUrgent, string sTime, string sSubject, string sMessage)", ex);
                }
            }
        }

        public void UpdateDataSets()
        {
            UpdateDataSets(true);
        }
        public void UpdateDataSets(bool bWaitForRefresh)
        {
            try
            {
                mDatabase.ForceRefreshDataSets(bWaitForRefresh);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateDataSets(bool bWaitForRefresh)", ex);
            }
        }

        public bool SupportsNewRoutes
        {
            get { return (_routeManagement != null); }
        }

        public void RefreshRouteData()
        {
            if (_routeManagement != null)
            {
                mDatabase.RefreshRouteData();
            }
        }

        // Called by the Push Listener to force out updated CONFIG_TOTAL_ACTIVE
        // messages to each of specified units in this list in the fleet:
        // The format of the array is FLEET unit unit unit unit
        public void SendConfigChangeToUnitsInFleet(string[] fleetAndConfig)
        {
            byte fleet = (byte)0x00;
            string type = "";
            int config = 0;
            uint[] units = null;

            try
            {
                fleet = Convert.ToByte(Convert.ToInt16(fleetAndConfig[0]) & 0xFF);
                type = fleetAndConfig[1];
                config = Convert.ToInt16(fleetAndConfig[2]);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendConfigChangeToUnitsInFleet(string[] fleetAndConfig)", ex);
            }

            try
            {
                if (type.Equals("Vehicle"))
                {
                    // If an explicit vehicle was passed in, it will be in the "config" parameter
                    units = new uint[1];
                    units[0] = (uint)config;
                }
                else
                {
                    // Find all units which are running this config number:
                    units = mDatabase.GetVehicleIdsRunningThis(fleet, type, config);
                }
                if (units == null) return;
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendConfigChangeToUnitsInFleet(string[] fleetAndConfig)", ex);
            }

            foreach (uint unit in units)
            {
                try
                {
                    Vehicle vehicle = mSendThreads.GetVehicle(fleet, unit);
                    if (vehicle != null)
                    {
                        vehicle.bNeedsConfigCheck = true;
                        _log.InfoFormat("Vehicle {0}/{1} config has been updated, configCheck set to true", fleet, unit);
                    }
                    else
                    {
                        _log.Debug($"Unable to get Vehicle mSendThreads.GetVehicle(fleet, unit), Fleet:{fleet}, Unit: {unit}");
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendConfigChangeToUnitsInFleet(string[] fleetAndConfig)", ex);
                }
            }
        }

        // Send the originator of the sourcePacket the given mail packet.
        private void SendMailPacket(Vehicle vehicle, GatewayProtocolPacket sourcePacket,
            MailMessagePacket mailPacket)
        {
            //Create a new packet which has the desired aspects of the source
            // i.e. ack numbers etc
            // AND the mail data from the other packet
            byte[] dummy = null;
            MailMessagePacket newMail = null;

            try
            {
                newMail = new MailMessagePacket(sourcePacket, _serverTime_DateFormat);
                newMail.sSubject = mailPacket.sSubject;
                newMail.bUrgent = mailPacket.bUrgent;
                newMail.sTargetUser = mailPacket.sTargetUser;
                newMail.sMessage = mailPacket.sMessage;
                newMail.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendMailPacket(Vehicle vehicle, GatewayProtocolPacket sourcePacket, MailMessagePacket mailPacket)", ex);
            }

            try
            {
                if (vehicle == null)
                    vehicle = GetVehicle(sourcePacket);

                if (vehicle != null)
                {
                    SendPacket(vehicle, newMail, 0, 0);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendMailPacket(Vehicle vehicle, GatewayProtocolPacket sourcePacket, MailMessagePacket mailPacket)", ex);
            }

            try
            {
                if (vehicle != null)
                    mSendThreads.SetUnitMailWaitingFlag(vehicle,
                        false,
                        null);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendMailPacket(Vehicle vehicle, GatewayProtocolPacket sourcePacket, MailMessagePacket mailPacket)", ex);
            }
        }

        public string SendConfigurationCheck(Vehicle vehicle, GatewayProtocolPacket inPacket)
        {
            byte[] dummy = null;
            string errString = null;
            ConfigTotalGPPacket cfPacket = null;

            // Create a ConfigTotalActive packet for this unit.
            // This should show that some aspect of the configuration
            // has changed. The unit will then send back a CTA showing
            // what it has, and we will automatically download whatever
            // the unit needs.
            try
            {

                cfPacket = new ConfigTotalGPPacket(inPacket, _serverTime_DateFormat, true);
                if ((errString = mDatabase.GetVehicleConfigTotal(ref cfPacket)) == null)
                {
                    // Got the config. Now send it to the unit.
                    cfPacket.Encode(ref dummy);
                    SendPacket(vehicle, cfPacket, 0, 0);
                }
                else
                {
                    // Possibly the car's unit number has been changed or deleted behind its back?
                    // Ideally we could send a CTA to the device and it would accept the new
                    // Fleet/Unit combo on it.
                    // However, it's not easy to trace the path of 
                    // {what I had} to {what I should be} because the record that would most
                    // help us (the T_Vehicle entry which maps the F/U ids of the unit that
                    // has sent us this packet to a device serial) has been deleted or changed!

                    // So the best we can do is tell the unit to reset itself, that way it
                    // will pick up the new F/U ids automatically when it restarts.
                    SendResetRequest(vehicle, inPacket);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendConfigurationCheck(Vehicle vehicle, GatewayProtocolPacket inPacket)", ex);
            }

            //This unit was instructed to check its config,
            // it has now satisfied that requirement:
            try
            {
                vehicle.bNeedsConfigCheck = false;
                //mSendThreads.SetUnitConfigCheckFlag(inPacket.cFleetId,inPacket.iVehicleId, false);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendConfigurationCheck(Vehicle vehicle, GatewayProtocolPacket inPacket)", ex);
            }

            return errString;
        }

        /// <summary>
        /// Send a configuration check to the vehicle specified in the inbound packet
        /// </summary>
        /// <param name="inPacket"></param>
        /// <returns></returns>
        public string SendConfigurationCheck(GatewayProtocolPacket inPacket)
        {
            Vehicle vehicle = GetVehicle(inPacket);
            return SendConfigurationCheck(vehicle, inPacket);
        }

        public void SendWatchListPacketToUnit(byte fleetId, uint unitId, Vehicle unitInfo)
        {
            int iRetries = 0;
            int iRetryPeriod = 0;

            if (unitInfo.oWatchListPacket != null)
            {
                // Find if the unit is a known unit.
                try
                {
                    iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                    iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendWatchListPacketToUnit(byte fleetId, uint unitId, Vehicle unitInfo)", ex);
                    iRetries = 10;
                    iRetryPeriod = 10;
                }
                try
                {
                    unitInfo.oWatchListPacket.iLength = 0;
                    unitInfo.oWatchListPacket.mDataField = null;
                    unitInfo.oWatchListPacket.cFleetId = fleetId;
                    unitInfo.oWatchListPacket.iVehicleId = unitId;
                    unitInfo.oWatchListPacket.Encode();
                    SendPacket(unitInfo, unitInfo.oWatchListPacket, iRetries, iRetryPeriod);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendWatchListPacketToUnit(byte fleetId, uint unitId, Vehicle unitInfo)", ex);
                }
                finally
                {
                    unitInfo.oWatchListPacket = null;
                }

            }
        }

        public void SendResetRequestToUnit(byte fleetId, uint unitId)
        {
            byte[] dummy = null;
            Vehicle unitInfo = null;
            GatewayProtocolPacket spPacket = null;
            int iRetries = 0;
            int iRetryPeriod = 0;

            // Find if the unit is a known unit.
            try
            {
                unitInfo = mSendThreads.GetVehicle(fleetId, unitId);
                iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendResetRequestToUnit(byte fleetId, uint unitId)", ex);
                iRetries = 10;
                iRetryPeriod = 10;
            }

            if (unitInfo != null)
            {
                // if the unit is found, send it a reset request.
                try
                {
                    spPacket = new GatewayProtocolPacket(_serverTime_DateFormat);
                    spPacket.cMsgType = GeneralGPPacket.GEN_RESET;
                    spPacket.iLength = 0;
                    spPacket.mDataField = null;
                    spPacket.cFleetId = fleetId;
                    spPacket.iVehicleId = unitId;
                    spPacket.Encode(ref dummy);
                    SendPacket(unitInfo, spPacket, iRetries, iRetryPeriod);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendResetRequestToUnit(byte fleetId, uint unitId)", ex);
                }
            }
        }

        public string SendPositionRequestToUnit(byte fleetId, uint unitId, uint totalSends, uint interval)
        {
            byte[] dummy = null;
            Vehicle unitInfo = null;
            GatewayProtocolPacket spPacket = null;

            try
            {
                unitInfo = mSendThreads.GetVehicle(fleetId, unitId);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendPositionRequestToUnit(byte fleetId, uint unitId, uint totalSends, uint interval)", ex);
            }

            if (unitInfo != null)
            {
                try
                {
                    spPacket = new GatewayProtocolPacket(_serverTime_DateFormat);
                    spPacket.cMsgType = GeneralGPPacket.STATUS_POS_REPORT;
                    spPacket.iLength = 0;
                    spPacket.mDataField = null;
                    spPacket.cFleetId = Convert.ToByte(fleetId);
                    spPacket.iVehicleId = unitId;
                    spPacket.cOurSequence = Convert.ToByte((unitInfo.cOurSequence + 1) % 32); // Use the next available
                    spPacket.cAckSequence = unitInfo.cAckSequence;
                    spPacket.mSenderIP = unitInfo.mSenderIP;
                    spPacket.Encode(ref dummy);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendPositionRequestToUnit(byte fleetId, uint unitId, uint totalSends, uint interval)", ex);
                }
                try
                {
                    SendPacket(spPacket, (int)totalSends - 1, (int)interval); // translate from total# to # of rpts, the unit doesn't ack this packet.
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".SendPositionRequestToUnit(byte fleetId, uint unitId, uint totalSends, uint interval)", ex);
                }
                return null;
            }
            else return "Could not locate Fleet " + fleetId + " Unit " + unitId + " in active unit list";
        }

        public string SendOutputRequestToUnit(byte cFleetId, uint uUnitId, bool bOutput1State, bool bOutput2State)
        {
            byte[] dummy = null;
            Vehicle unitInfo = null;
            GeneralOutputGPPacket gop = null;
            int iRetries = 0;
            int iRetryPeriod = 0;

            try
            {
                unitInfo = mSendThreads.GetVehicle(cFleetId, uUnitId);
                iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendOutputRequestToUnit(byte cFleetId, uint uUnitId, bool bOutput1State, bool bOutput2State)", ex);
                iRetries = 10;
                iRetryPeriod = 10;
            }

            if (unitInfo != null)
            {
                try
                {
                    gop = new GeneralOutputGPPacket(_serverTime_DateFormat);
                    gop.iLength = 0;
                    gop.mDataField = null;
                    gop.cFleetId = Convert.ToByte(cFleetId);
                    gop.iVehicleId = uUnitId;
                    gop.cOutputNumber = (byte)0x01;
                    if (bOutput1State)
                        gop.cSequence = (byte)0x01;
                    else
                        gop.cSequence = (byte)0x00;
                    gop.Encode(ref dummy);
                    SendPacket(unitInfo, gop, iRetries, iRetryPeriod);
                }
                catch (System.Exception ex1)
                {
                    _log.Error(this.GetType().FullName + ".SendOutputRequestToUnit(byte cFleetId, uint uUnitId, bool bOutput1State, bool bOutput2State)", ex1);
                }
                //SendPacket(gop);// totalSends-1, (int) interval); // translate from total# to # of rpts
                return null;
            }
            else return "Could not locate Fleet " + cFleetId + " unit " + uUnitId + " to drive outputs";
        }

        public GatewayProtocolPacket SendOutputRequestToUnit(byte cFleetId, uint uUnitId, int iOutputID, bool bOutputState)
        {
            byte[] dummy = null;
            Vehicle unitInfo = null;
            GeneralOutputGPPacket gop = null;
            int iRetries = 0;
            int iRetryPeriod = 0;

            try
            {
                unitInfo = mSendThreads.GetVehicle(cFleetId, uUnitId);
                iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendOutputRequestToUnit(byte cFleetId, uint uUnitId, int iOutputID, bool bOutputState)", ex);
                iRetries = 10;
                iRetryPeriod = 10;
            }

            if (unitInfo != null)
            {
                try
                {
                    gop = new GeneralOutputGPPacket(_serverTime_DateFormat);
                    gop.iLength = 0;
                    gop.mDataField = null;
                    gop.cFleetId = Convert.ToByte(cFleetId);
                    gop.iVehicleId = uUnitId;
                    gop.cOutputNumber = (byte)iOutputID;
                    if (bOutputState)
                        gop.cSequence = (byte)0x01;
                    else
                        gop.cSequence = (byte)0x00;
                    gop.Encode(ref dummy);
                    SendPacket(unitInfo, gop, iRetries, iRetryPeriod);
                }
                catch (System.Exception ex1)
                {
                    _log.Error(this.GetType().FullName + ".SendOutputRequestToUnit(byte cFleetId, uint uUnitId, int iOutputID, bool bOutputState)", ex1);
                }
                //SendPacket(gop);// totalSends-1, (int) interval); // translate from total# to # of rpts
                return gop;
            }
            else
            {
                return null;
            }
        }

        private List<GPSendScriptFile> _sendFileList = new List<GPSendScriptFile>();
        public GPSendScriptFile[] FileList
        {
            get
            {
                GPSendScriptFile[] ret = new GPSendScriptFile[_sendFileList.Count];
                _sendFileList.CopyTo(ret);
                return ret;
            }
        }

        public string SendScriptFileToUnit(byte cFleetId, uint uUnitId, string scriptFileName, byte[] scriptFileBytes)
        {
            try
            {
                byte[] dummy = null;
                Vehicle v = mSendThreads.GetVehicle(cFleetId, uUnitId);
                GPSendScriptFile scriptPacket = new GPSendScriptFile(_serverTime_DateFormat);
                scriptPacket.mDataField = null;
                scriptPacket.cFleetId = Convert.ToByte(cFleetId);
                scriptPacket.iVehicleId = uUnitId;
                scriptPacket.FileId = (ushort)_sendFileList.Count;
                scriptPacket.FileName = scriptFileName;
                scriptPacket.FileBytes = scriptFileBytes;
                scriptPacket.SubCommand = 0;
                scriptPacket.Encode(ref dummy);
                _sendFileList.Add(scriptPacket);
                v.oSendScriptFilePacket = scriptPacket;
                SendPacket(v, scriptPacket, 0);
            }
            catch (Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendScriptFileToUnit(byte cFleetId, uint uUnitId, string scriptFileName, byte[] scriptFileBytes)", ex);
            }
            return null;
        }

        public string SendHistoryRequestToUnit(byte fleetId, uint unitId)
        {
            /*
            byte[] dummy = null;
            SenderThreadCollection.PerUnitInformation unitInfo =
                mSendThreads.GetLatestPacket(VehicleCollection.GetUniqueUnitId(fleetId, unitId));

            if (unitInfo != null)
            {
                GatewayProtocolPacket histReqPacket = new GPSHistoryGPPacket(_serverTime_DateFormat);
                histReqPacket.iLength		= 0;
                histReqPacket.mDataField	= null;
                histReqPacket.cFleetId		= Convert.ToByte(fleetId);
                histReqPacket.iVehicleId	= unitId;
                histReqPacket.cOurSequence	= unitInfo.cOurSequence;
                histReqPacket.cAckSequence	= unitInfo.cAckSequence;
                histReqPacket.mSenderIP		= unitInfo.mSenderIP;
                histReqPacket.Encode(ref dummy);
                SendPacket(histReqPacket, 0, 0);
                return null;
            }	
            else return "Could not locate Fleet " + fleetId + " Unit " + unitId + " in active unit list";	
            */
            return "Commented";
        }

        public Vehicle SendPendentAlarmAckToUnit(int fleetId, int vehicleId)
        {
            Vehicle v = null;
            #region Send an ack to the pendent alarm on this unit
            try
            {
                v = mSendThreads.GetVehicle((byte)fleetId, (uint)vehicleId);
                if (v == null)
                {
                    if (mDatabase.GetVehicleRow((byte)fleetId, (uint)vehicleId) != null)
                    {
                        // If the vehicle has not talked to the server yet, create an object, the update will be sent once the unit reports in.
                        v = new Vehicle((byte)fleetId, (uint)vehicleId, null, ref mDatabase, _serverTime_DateFormat);
                        mSendThreads.Add(v);
                    }
                }
                var alarms = mDatabase.PendentActiveAlarmList.GetAcknowledgedAlarms((byte)fleetId, (uint)vehicleId);
                foreach (var item in alarms) 
                {
                    if (v.mSenderIP != null)
                    {
                        _log.Info("Sending Pendant Alarm ack to unit (F/V) " + fleetId + "/" + vehicleId);
                        v.SendOnce(item.AckPacket);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SendPendentAlarmAckToUnit(int fleetId, int vehicleId)", ex);
            }
            #endregion
            return v;
        }
        public string AddUnitToWatchList(byte fleetId, uint unitId)
        {
            Vehicle v = null;
            #region Update the WatchList lookup
            try
            {
                mDatabase.SetWatchListState(fleetId, unitId, true, 0);
                v = mSendThreads.GetVehicle(fleetId, unitId);
                if (v == null)
                    return "Could not locate Fleet " + fleetId + " unit " + unitId + " to add to the watch list";
                else
                {
                    v.oWatchListPacket = new ConfigWatchList(_serverTime_DateFormat);
                    v.oWatchListPacket.AddToWatchList = true;

                    mSendThreads.AddToWatchList(v);
                    _log.InfoFormat("Vehicle {0}/{1} added to watch list, configCheck set to true", fleetId, unitId);
                    v.bNeedsConfigCheck = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AddUnitToWatchList(byte fleetId, uint unitId)", ex);
                return ex.Message;
            }
            #endregion
            return null;
        }
        public string RemoveUnitFromWatchList(byte fleetId, uint unitId)
        {
            Vehicle v = null;
            #region Update the WatchList lookup
            try
            {
                mDatabase.SetWatchListState(fleetId, unitId, false, 0);
                v = mSendThreads.GetVehicle(fleetId, unitId);
                if (v == null)
                    return "Could not locate Fleet " + fleetId + " unit " + unitId + " to add to the watch list";
                else
                {
                    v.oWatchListPacket = new ConfigWatchList(_serverTime_DateFormat);
                    v.oWatchListPacket.AddToWatchList = false;

                    mSendThreads.RemoveFromWatchList(v);
                    _log.InfoFormat("Vehicle {0}/{1} removed from watch list, configCheck set to true", fleetId, unitId);
                    v.bNeedsConfigCheck = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".RemoveUnitFromWatchList(byte fleetId, uint unitId)", ex);
                return ex.Message;
            }
            #endregion
            return null;
        }

        #region Accident Upload Request

        public void SendAccidentUploadRequest(byte fleetID, uint vehicleID, int accidentIndex)
        {
            Vehicle vehicle = mSendThreads.GetVehicle(fleetID, vehicleID);
            if (vehicle != null)
                vehicle.AddAccidentRequest(accidentIndex);
        }

        public GatewayProtocolPacket SendAccidentUploadRequest(GatewayProtocolPacket aTarget, int index)
        {
            return SendAccidentUploadRequest(null, aTarget, index);
        }

        public GatewayProtocolPacket SendAccidentUploadRequest(Vehicle vehicle, GatewayProtocolPacket aTarget, int accidentIndex)
        {
            byte[] dummy = null;
            GatewayProtocolPacket spPacket = null;

            // Create a  packet with 
            // don't-care contents, aside from the target's IP
            // and the type 
            try
            {
                spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                spPacket.cMsgType = GeneralGPPacket.GPS_HISTORY;
                spPacket.iLength = 2;

                spPacket.mDataField = new byte[2];
                spPacket.mDataField[0] = (byte)'\n';
                spPacket.mDataField[1] = (byte)(accidentIndex % 256);

                spPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendAccidentUploadRequest(Vehicle vehicle, GatewayProtocolPacket aTarget, int accidentIndex)", ex);
            }

            try
            {
                if (vehicle != null)
                    SendPacket(vehicle, spPacket, 0);
                else
                    SendPacket(spPacket, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendAccidentUploadRequest(Vehicle vehicle, GatewayProtocolPacket aTarget, int accidentIndex)", ex);
            }
            return spPacket;
        }

        public string SendExtendedHistoryRequest(Vehicle vehicle, GatewayProtocolPacket aTarget, RequestExtendedHistory request)
        {
            byte[] dummy = null;
            GatewayProtocolPacket spPacket = null;

            // Create a  packet with 
            // don't-care contents, aside from the target's IP
            // and the type 
            try
            {
                spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                spPacket.cMsgType = GeneralGPPacket.GPS_HISTORY;
                spPacket.iLength = 14;

                spPacket.mDataField = new byte[14];
                spPacket.mDataField[0] = (byte)'\n';
                spPacket.mDataField[1] = (byte)(10);
                spPacket.mDataField[2] = (byte)request.StartDate.Day;
                spPacket.mDataField[3] = (byte)request.StartDate.Month;
                spPacket.mDataField[4] = (byte)(request.StartDate.Year - 2000);
                spPacket.mDataField[5] = (byte)request.StartDate.Hour;
                spPacket.mDataField[6] = (byte)request.StartDate.Minute;
                spPacket.mDataField[7] = (byte)request.StartDate.Second;
                spPacket.mDataField[8] = (byte)request.EndDate.Day;
                spPacket.mDataField[9] = (byte)request.EndDate.Month;
                spPacket.mDataField[10] = (byte)(request.EndDate.Year - 2000);
                spPacket.mDataField[11] = (byte)request.EndDate.Hour;
                spPacket.mDataField[12] = (byte)request.EndDate.Minute;
                spPacket.mDataField[13] = (byte)request.EndDate.Second;

                spPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendExtendedHistoryRequest()", ex);
            }

            try
            {
                if (vehicle != null)
                    SendPacket(vehicle, spPacket, 0);
                else
                    SendPacket(spPacket, 0);

                vehicle.ExtentedHistoryRequestSent(request);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendExtendedHistoryRequest()", ex);
            }
            return null;
        }
        #endregion
        #region Send IO Box Serial Number requests
        public string SendIOBoxSerialNumber(GatewayProtocolPacket aTarget)
        {
            return SendIOBoxSerialNumber(null, aTarget);
        }

        public string SendIOBoxSerialNumber(Vehicle vehicle, GatewayProtocolPacket aTarget)
        {
            byte[] bData = null;
            GatewayProtocolPacket spPacket = null;
            uint iNextID = 0;

            try
            {
                iNextID = mDatabase.GetNextIOBoxSerialNumber();
                if (iNextID > 0)
                {
                    bData = BitConverter.GetBytes(iNextID);
                    spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                    spPacket.cMsgType = GeneralGPPacket.GEN_NEW_IOBOX_SERIALNUMBER;
                    spPacket.mDataField = BitConverter.GetBytes(iNextID);
                    spPacket.cPacketNum = (byte)0x01;
                    spPacket.cPacketTotal = (byte)0x01;
                    spPacket.Encode(ref bData);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendIOBoxSerialNumber(Vehicle vehicle, GatewayProtocolPacket aTarget)", ex);
            }

            try
            {
                if (vehicle != null)
                    SendPacket(vehicle, spPacket, 0);
                else
                    SendPacket(spPacket, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendIOBoxSerialNumber(Vehicle vehicle, GatewayProtocolPacket aTarget)", ex);
            }
            return null;
        }
        #endregion

        public GatewayProtocolPacket SendGForceAutoCalibrateRequest(GatewayProtocolPacket aTarget)
        {
            return SendGForceAutoCalibrateRequest(null, aTarget, false);
        }

        public GatewayProtocolPacket SendGForceAutoCalibrateRequest(Vehicle vehicle, GatewayProtocolPacket aTarget, bool autoGenerated)
        {
            byte[] dummy = null;
            GatewayProtocolPacket spPacket = null;
            int iRetries = 0;
            int iRetryPeriod = 0;

            _log.Debug("Step 1 Auto Calibrate");

            try
            {
                iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
            }
            catch (System.Exception)
            {
                iRetries = 10;
                iRetryPeriod = 10;
            }

            _log.Debug("Step 2 Auto Calibrate");

            try
            {
                spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                spPacket.cMsgType = GeneralGPPacket.GEN_GFORCEAUTOCALIBRATE;
                spPacket.iLength = 1;
                if (autoGenerated)
                {
                    spPacket.mDataField = new byte[] { 0x05 };
                }
                else
                {
                    spPacket.mDataField = new byte[] { 0x02 };
                }
                spPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendGForceAutoCalibrateRequest(Vehicle vehicle, GatewayProtocolPacket aTarget)", ex);
            }

            _log.Debug("Step 3 Auto Calibrate");
            try
            {
                bool retVal;

                if (vehicle != null)
                {
                    retVal = SendPacket(vehicle, spPacket, iRetries, iRetryPeriod);
                }
                else
                {
                    retVal = SendPacket(spPacket, iRetries, iRetryPeriod);
                }

                _log.Debug(String.Format("GForceAutoCalibrate Step 3 returned {0}, Vehicle Was Null:{1}", retVal, vehicle == null));
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendGForceAutoCalibrateRequest(Vehicle vehicle, GatewayProtocolPacket aTarget)", ex);
            }

            _log.Debug("Return From Auto Calibrate");

            return spPacket;
        }

        private void mSendThreads_eResetUnitEvent(GatewayProtocolPacket aTarget)
        {
            SendResetRequest(null, aTarget);
        }

        public GatewayProtocolPacket SendResetRequest(GatewayProtocolPacket aTarget)
        {
            return SendResetRequest(null, aTarget);
        }

        public GatewayProtocolPacket SendResetRequest(Vehicle vehicle, GatewayProtocolPacket aTarget)
        {
            byte[] dummy = null;
            GatewayProtocolPacket spPacket = null;
            int iRetries = 0;
            int iRetryPeriod = 0;

            try
            {
                iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
            }
            catch (System.Exception)
            {
                iRetries = 10;
                iRetryPeriod = 10;
            }
            // Create a GEN_RESET packet with 
            // don't-care contents, aside from the target's IP
            // and the type 
            try
            {
                spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                spPacket.cMsgType = GeneralGPPacket.GEN_RESET;
                spPacket.iLength = 0;
                spPacket.mDataField = null;
                spPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendResetRequest(Vehicle vehicle, GatewayProtocolPacket aTarget)", ex);
            }

            try
            {
                if (vehicle != null)
                    SendPacket(vehicle, spPacket, iRetries, iRetryPeriod);
                else
                    SendPacket(spPacket, iRetries, iRetryPeriod);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendResetRequest(Vehicle vehicle, GatewayProtocolPacket aTarget)", ex);
            }

            return spPacket;
        }

        public GatewayProtocolPacket SendFlushRequest(GatewayProtocolPacket aTarget)
        {
            byte[] dummy = null;
            GatewayProtocolPacket spPacket = null;
            // Create a GEN_CLEAR_SENDBUF packet with 
            // don't-care contents, aside from the target's IP
            // and the type 
            try
            {
                spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                spPacket.cMsgType = GeneralGPPacket.GEN_CLEAR_SENDBUF;
                spPacket.iLength = 0;
                spPacket.mDataField = null;
                spPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendFlushRequest(GatewayProtocolPacket aTarget)", ex);
            }
            try
            {
                SendPacket(spPacket, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendFlushRequest(GatewayProtocolPacket aTarget)", ex);
            }
            return spPacket;
        }


        public string SendNetworkInfo(ConfigNetworkInfoGPPacket netPacket)
        {
            byte[] dummy = null;
            try
            {
                netPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendNetworkInfo(ConfigNetworkInfoGPPacket netPacket)", ex);
            }
            try
            {
                SendPacket(netPacket, 0, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendNetworkInfo(ConfigNetworkInfoGPPacket netPacket)", ex);
            }
            return null;
        }

        public string SendCustomerInfo(ConfigCustomerInfoGPPacket custPacket)
        {
            byte[] dummy = null;
            try
            {
                custPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendCustomerInfo(ConfigCustomerInfoGPPacket custPacket)", ex);
            }
            try
            {
                SendPacket(custPacket, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendCustomerInfo(ConfigCustomerInfoGPPacket custPacket)", ex);
            }
            return null;
        }

        public string SendOutputMessage(GeneralOutputGPPacket oPacket)
        {
            byte[] dummy = null;
            int iRetries = 0;
            int iRetryPeriod = 0;

            try
            {
                iRetries = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                iRetryPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
            }
            catch (System.Exception)
            {
                iRetries = 10;
                iRetryPeriod = 10;
            }

            try
            {
                oPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendOutputMessage(GeneralOutputGPPacket oPacket)", ex);
            }

            try
            {
                SendPacket(oPacket, iRetries, iRetryPeriod);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendOutputMessage(GeneralOutputGPPacket oPacket)", ex);
            }
            return null;
        }
        public string SendSchedule(ConfigScheduleInfoGPPacket schedulePacket)
        {
            byte[] dummy = null;
            try
            {
                schedulePacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendSchedule(ConfigScheduleInfoGPPacket schedulePacket)", ex);
            }
            try
            {
                SendPacket(schedulePacket, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendSchedule(ConfigScheduleInfoGPPacket schedulePacket)", ex);
            }
            return null;
        }

        public string SendAlarm(GatewayProtocolPacket aTarget)
        {
            byte[] dummy = null;
            GatewayProtocolPacket spPacket = null;
            // Create a GEN_ALARM packet with 
            // don't-care contents, aside from the target's IP
            // and the type 
            try
            {
                spPacket = new GatewayProtocolPacket(aTarget, _serverTime_DateFormat);
                spPacket.cMsgType = GeneralGPPacket.GEN_ALARM;
                spPacket.iLength = 0;
                spPacket.mDataField = null;
                spPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendAlarm(GatewayProtocolPacket aTarget)", ex);
            }
            try
            {
                SendPacket(spPacket, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendAlarm(GatewayProtocolPacket aTarget)", ex);
            }
            return null;
        }

        public string SendProgramDownload(ProgramDownloadPacket dlPacket,
            AcknowledgementNotificationDelegate del,
            int iProgramRetryMaxCount,
            int iProgramRetryIntervalSecs)
        {
            byte[] dummy = null;

            try
            {
                dlPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendProgramDownload(ProgramDownloadPacket dlPacket, AcknowledgementNotificationDelegate del, int iProgramRetryMaxCount, int iProgramRetryIntervalSecs)", ex);
            }
            try
            {
                SendPacket(dlPacket,
                    iProgramRetryMaxCount,
                    iProgramRetryIntervalSecs);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendProgramDownload(ProgramDownloadPacket dlPacket, AcknowledgementNotificationDelegate del, int iProgramRetryMaxCount, int iProgramRetryIntervalSecs)", ex);
            }
            // Register our interest in knowing when the packet we just
            // sent was acked.
            try
            {
                mSendThreads.GetVehicle(dlPacket.cFleetId, dlPacket.iVehicleId).SetAcknowledgementNotificationDelegate(del, dlPacket);
                //mSendThreads.SetAcknowledgementNotificationDelegate(del,
                //    dlPacket);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendProgramDownload(ProgramDownloadPacket dlPacket, AcknowledgementNotificationDelegate del, int iProgramRetryMaxCount, int iProgramRetryIntervalSecs)", ex);
            }
            return null;
        }

        public string SendVerifyDownload(ProgramVerifyPacket dvPacket,
            AcknowledgementNotificationDelegate del,
            int iProgramRetryMaxCount,
            int iProgramRetryIntervalSecs)
        {
            byte[] dummy = null;

            try
            {
                dvPacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendVerifyDownload(ProgramVerifyPacket dvPacket, AcknowledgementNotificationDelegate del, int iProgramRetryMaxCount, int iProgramRetryIntervalSecs)", ex);
            }

            try
            {
                SendPacket(dvPacket, 1, 10);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendVerifyDownload(ProgramVerifyPacket dvPacket, AcknowledgementNotificationDelegate del, int iProgramRetryMaxCount, int iProgramRetryIntervalSecs)", ex);
            }

            // Register our interest in knowing when the packet we just
            // sent was acked.
            try
            {
                mSendThreads.GetVehicle(dvPacket.cFleetId, dvPacket.iVehicleId).SetAcknowledgementNotificationDelegate(del, dvPacket);
                //mSendThreads.SetAcknowledgementNotificationDelegate(del,
                //    dvPacket);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SendVerifyDownload(ProgramVerifyPacket dvPacket, AcknowledgementNotificationDelegate del, int iProgramRetryMaxCount, int iProgramRetryIntervalSecs)", ex);
            }
            return null;
        }

        public void AbortProgramDownload(byte cFleetId, uint iVehicleId)
        {
            try
            {
                mSendThreads.GetVehicle(cFleetId, iVehicleId).RemoveAcknowledgementNotificationDelegate();
                //mSendThreads.RemoveAcknowledgementNotificationDelegate();
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AbortProgramDownload()", ex);
            }
        }



        #endregion
        #region Packet Processing Functions
        public Hashtable GetInterfaceServersTable()
        {
            Hashtable oFleetInterfaceServers = null;
            try
            {
                oFleetInterfaceServers = mDatabase.GetInterfaceServersTable();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetInterfaceServersTable()", ex);
            }
            return oFleetInterfaceServers;
        }

        public Vehicle GetVehicle(GatewayProtocolPacket p)
        {
            return mSendThreads.GetVehicle(p);
        }

        public string InsertListenerMessage(MTData.Transport.Application.Communication.ListenerLogItem logItem)
        {
            return mDatabase.InsertListenerMessage(logItem);
        }

        #region Status stuff
        private void UpdateInputStatus(GatewayProtocolPacket gPacket)
        {
            GPStatus state = null;

            try
            {
                state = ((IStatusHolder)gPacket).GetStatus();
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }

            #region Get the Input Status
            try
            {
                if ((state.cInputStatus & 0x01) == 0x01) mSendThreads.UpdateUnitState(gPacket, UnitState.IgnitionOn);
                else mSendThreads.UpdateUnitState(gPacket, UnitState.IgnitionOff);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }

            try
            {
                if ((state.cInputStatus & 0x04) == 0x04) mSendThreads.UpdateUnitState(gPacket, UnitState.Input1On);
                else mSendThreads.UpdateUnitState(gPacket, InternalUnitState.Input1Off);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }
            try
            {
                if ((state.cInputStatus & 0x08) == 0x08) mSendThreads.UpdateUnitState(gPacket, UnitState.Input2On);
                else mSendThreads.UpdateUnitState(gPacket, InternalUnitState.Input2Off);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }
            #endregion
            #region Get the Vehicle State
            //#define  STSFLAG_EXCESS_IDLE			0x01
            //#define  STSFLAG_MDT_LOGGED_IN		0x02
            //#define  STSFLAG_ANGEL_GEAR			0x04
            //#define  STSFLAG_OVERSPEED_ZONE1		0x08
            //#define  STSFLAG_OVERSPEED_ZONE2		0x10
            //#define  STSFLAG_OVERSPEED_ZONE3		0x20
            //#define STSFLAG_AT_WAYPOINT			0x40
            //#define STSFLAG_STATUS_FLAGS2_USED	0x80
            try
            {
                if ((state.cStatusFlag & 0x01) == 0x01)
                    mSendThreads.UpdateUnitState(gPacket, UnitState.ExcessiveIdle);
                else
                    mSendThreads.UpdateUnitState(gPacket, InternalUnitState.ExcessiveIdleOver);

                if ((state.cStatusFlag & 0x02) == 0x02)
                    mSendThreads.UpdateUnitState(gPacket, UnitState.LoggedIn);
                else
                    mSendThreads.ClearUnitState(gPacket, UnitState.LoggedIn);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }
            #region Process Extended States (if ((state.cStatusFlag & 0x80) == 0x80))
            try
            {
                if ((state.cStatusFlag & 0x80) == 0x80)
                {
                    if ((state.cExtendedStatusFlag & 0x01) == 0x01)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.MDTState1);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.MDTState1);
                    if ((state.cExtendedStatusFlag & 0x02) == 0x02)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.MDTState2);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.MDTState2);
                    if ((state.cExtendedStatusFlag & 0x04) == 0x04)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.MDTState3);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.MDTState3);
                    if ((state.cExtendedStatusFlag & 0x08) == 0x08)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.MDTState4);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.MDTState4);
                    if ((state.cExtendedStatusFlag & 0x10) == 0x10)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.AuxState1);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.AuxState1);
                    if ((state.cExtendedStatusFlag & 0x20) == 0x20)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.AuxState2);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.AuxState2);
                    if ((state.cExtendedStatusFlag & 0x40) == 0x40)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.AuxState3);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.AuxState3);
                    if ((state.cExtendedStatusFlag & 0x80) == 0x80)
                        mSendThreads.UpdateUnitState(gPacket, UnitState.AuxState4);
                    else
                        mSendThreads.ClearUnitState(gPacket, UnitState.AuxState4);
                }
                else
                {
                    mSendThreads.ClearUnitState(gPacket, UnitState.MDTState1);
                    mSendThreads.ClearUnitState(gPacket, UnitState.MDTState2);
                    mSendThreads.ClearUnitState(gPacket, UnitState.MDTState3);
                    mSendThreads.ClearUnitState(gPacket, UnitState.MDTState4);
                    mSendThreads.ClearUnitState(gPacket, UnitState.AuxState1);
                    mSendThreads.ClearUnitState(gPacket, UnitState.AuxState2);
                    mSendThreads.ClearUnitState(gPacket, UnitState.AuxState3);
                    mSendThreads.ClearUnitState(gPacket, UnitState.AuxState4);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }
            #endregion
            
            Vehicle oVehicle = mSendThreads.GetVehicle(gPacket);

            #region Process Extended States 2
            try
            {
                if (oVehicle != null)
                {
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.LOW_BATTERY) == (int)ExtendedStatusFlag2.LOW_BATTERY)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_LOW_BATTERY;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_LOW_BATTERY;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.MOVING) == (int)ExtendedStatusFlag2.MOVING)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_MOVING;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_MOVING;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.BATTERY_CHARGING) == (int)ExtendedStatusFlag2.BATTERY_CHARGING)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_BATTERY_CHARGING;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_BATTERY_CHARGING;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.CRITICAL_BATTERY) == (int)ExtendedStatusFlag2.CRITICAL_BATTERY)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_CRITICAL_BATTERY;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_CRITICAL_BATTERY;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.FLAT_BATTERY) == (int)ExtendedStatusFlag2.FLAT_BATTERY)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_FLAT_BATTERY;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_FLAT_BATTERY;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.PTO_ON) == (int)ExtendedStatusFlag2.PTO_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_PTO_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_PTO_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.SEAT_BELT_ON) == (int)ExtendedStatusFlag2.SEAT_BELT_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_SEAT_BELT_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_SEAT_BELT_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.ABS_ON) == (int)ExtendedStatusFlag2.ABS_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_ABS_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_ABS_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.PASSENGER_SEAT_BELT_ON) == (int)ExtendedStatusFlag2.PASSENGER_SEAT_BELT_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_PASSENGER_SEAT_BELT_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_PASSENGER_SEAT_BELT_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.FOUR_WHEEL_DRIVE_ON) == (int)ExtendedStatusFlag2.FOUR_WHEEL_DRIVE_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_FOUR_WHEEL_DRIVE_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_FOUR_WHEEL_DRIVE_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.DOORS_OPEN) == (int)ExtendedStatusFlag2.DOORS_OPEN)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_DOORS_OPEN;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_DOORS_OPEN;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.HAND_BRAKE_ON) == (int)ExtendedStatusFlag2.HAND_BRAKE_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_HAND_BRAKE_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_HAND_BRAKE_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.CRUISE_CONTROL_ON) == (int)ExtendedStatusFlag2.CRUISE_CONTROL_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_CRUISE_CONTROL_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_CRUISE_CONTROL_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.WIPERS_ON) == (int)ExtendedStatusFlag2.WIPERS_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_WIPERS_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WIPERS_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.HEAD_LIGHTS_ON) == (int)ExtendedStatusFlag2.HEAD_LIGHTS_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_HEAD_LIGHTS_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_HEAD_LIGHTS_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.HIGH_BEAM_ON) == (int)ExtendedStatusFlag2.HIGH_BEAM_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_HIGH_BEAM_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_HIGH_BEAM_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.LEFT_INDICATOR_ON) == (int)ExtendedStatusFlag2.LEFT_INDICATOR_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_LEFT_INDICATOR_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_LEFT_INDICATOR_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.LOW_FUEL_INDICATOR_ON) == (int)ExtendedStatusFlag2.LOW_FUEL_INDICATOR_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_LOW_FUEL_INDICATOR_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_LOW_FUEL_INDICATOR_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.EBS_ON) == (int)ExtendedStatusFlag2.EBS_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_EBS_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_EBS_ON;
                    if ((state.ExtendedStatusFlag2 & (int)ExtendedStatusFlag2.RIGHT_INDICATOR_ON) == (int)ExtendedStatusFlag2.RIGHT_INDICATOR_ON)
                        oVehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_RIGHT_INDICATOR_ON;
                    else
                        oVehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_RIGHT_INDICATOR_ON;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateInputStatus(GatewayProtocolPacket gPacket)", ex);
            }
            #endregion
            #endregion

            if (oVehicle != null)
                mDatabase.UpdateVehilcleStatus(oVehicle);
        }

        private void UpdatePositionStatus(GatewayProtocolPacket gPacket)
        {
            GPPositionFix fix = null;
            GPClock fixClock = null;

            try
            {
                fix = ((IPositionHolder)gPacket).GetPosition();
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdatePositionStatus(GatewayProtocolPacket gPacket)", ex);
            }
            try
            {
                fixClock = ((IPositionHolder)gPacket).GetFixClock();
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdatePositionStatus(GatewayProtocolPacket gPacket)", ex);
            }
            try
            {
                mSendThreads.UpdateUnitPosition(gPacket, fix, fixClock);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdatePositionStatus(GatewayProtocolPacket gPacket)", ex);
            }
        }

        private void UpdateConcreteStatus(GatewayProtocolPacket gPacket, long iUnitState, GPECMAdvanced advanced)
        {
            bool bRet = true;
            #region Check if this packet was from a 'Concrete' fleet
            try
            {
                bRet = alConcreteFleets.Contains(gPacket.cFleetId);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UpdateConcreteStatus(GatewayProtocolPacket gPacket, long iUnitState)", ex);
            }
            #endregion

            if (bRet)
            {
                #region If this is a fleet that needs to be forwarded to command set bRet = true
                try
                {
                    lock (alCommandStatusFleets.SyncRoot)
                        bRet = alCommandStatusFleets.Contains(gPacket.cFleetId);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".UpdateConcreteStatus(GatewayProtocolPacket gPacket, long iUnitState)", ex);
                }
                #endregion

                #region If this packet is from a unit in a 'Concrete' fleet that is allowed to send an update to Command
                try
                {
                    mSendThreads.UpdateUnitConcreteStatus(gPacket, SendCommandTimestamps, bRet, iUnitState);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".UpdateConcreteStatus(GatewayProtocolPacket gPacket, long iUnitState)", ex);
                }
                #endregion

                Vehicle oVehicle = mSendThreads.GetVehicle(gPacket);
                if (oVehicle != null)
                    mDatabase.UpdateVehilcleStatus(oVehicle);
            }
        }

        public void SetAtLunchState(int iFleetID, uint iVehicleID)
        {
            try
            {
                mSendThreads.SetAtLunchState(iFleetID, iVehicleID);
                _log.Info("Client Status change request : Setting " + Convert.ToString(iFleetID) + "/" + Convert.ToString(iVehicleID) + " to 'At Lunch' state.");
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".SetAtLunchState(int iFleetID, uint iVehicleID)", ex);
            }
        }

        public void UnsetAtLunchState(int iFleetID, uint iVehicleID)
        {
            try
            {
                mSendThreads.UnsetAtLunchState(iFleetID, iVehicleID);
                _log.Info("Client Status change request : Clearing 'At Lunch' state.");
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".UnsetAtLunchState(int iFleetID, uint iVehicleID)", ex);
            }
        }

        #endregion
        private Hashtable oLoggedInUnits = null;
        private string ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)
        {

            bool bTransitioning = false;
            bool bWriteDBRecord = true;
            System.Data.DataSet dsCredentials = null;
            ConfigTotalGPPacket configPacket = null;
            byte[] dummy = null;
            string errMsg = null;

            try
            {
                if (v.cFleet > 0 && v.iUnit > 0)
                {
                    if (oLoggedInUnits == null) oLoggedInUnits = Hashtable.Synchronized(new Hashtable());
                    lock (oLoggedInUnits.SyncRoot)
                    {
                        if (!oLoggedInUnits.ContainsKey(v.GetHashCode()))
                        {
                            bool bRestoreState = false;
                            lock (alRestoreMDTStatusForFleet.SyncRoot)
                                if (alRestoreMDTStatusForFleet.Contains(v.cFleet))
                                    bRestoreState = true;
                            if (bRestoreState)
                                mDatabase.UpdateRestoreStoreStatus(v);
                            oLoggedInUnits.Add(v.GetHashCode(), null);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex);
            }

            try
            {
                if ((gPacket.cFleetId == 0) && (gPacket.iVehicleId == 0))
                    bTransitioning = true;

            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex);
            }

            if (v == null)
                return "ProcessGenPacket: Could not find Vehicle object for fleet: " + gPacket.cFleetId + " unit: " + gPacket.iVehicleId;
            // If it isn't a login type packet
            if (gPacket.cMsgType != GeneralGPPacket.GEN_ACK &&
                gPacket.cMsgType != GeneralGPPacket.GEN_NAK &&
                gPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE &&
                gPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN &&
                gPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE_FULL &&
                gPacket.cMsgType != GPSHistoryGPPacket.GPS_HISTORY &&
                gPacket.cMsgType != GeneralGPPacket.GEN_STARTUP &&
                gPacket.cMsgType != GeneralGPPacket.GEN_RESET)
            {
                #region PENDENT_ALARM and PENDENT_ALARM_TEST Processing
                if (gPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || gPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST)
                {
                    if (gPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM)
                        _log.Info("F/V" + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Processing pendant alarm.");
                    else
                        _log.Info("F/V" + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Processing pendant test alarm.");
                    #region On a new alarm, check it isn't a repeat of a previous alarm, update the last alarm event and active alarm event lists
                    DateTime alarm;
                    GPClock alarmTime = gPacket.mCurrentClock;
                    if (!alarmTime.IsDate(null))
                    {
                        alarm = DateTime.UtcNow;
                        _log.Info("Pendant alarm packet time invalid - using listener clock (UTC): " + alarm);
                    }
                    else
                    {
                        alarm = alarmTime.ToDateTime();
                        _log.Info("Pendant alarm packet time (UTC): " + alarm);
                    }

                    bool success = mDatabase.PendentActiveAlarmList.AddAlarm(gPacket.cFleetId, gPacket.iVehicleId, alarm, gPacket);
                    if (!success)
                    {
                        // This is a repeat of a previous alarm, make sure the register is correct
                        if (gPacket.mRawBytes != null)
                            _log.Info("F/V " + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Recieved a duplicate alarm request. This Request : " + alarm.ToString("dd/MM/yyyy HH:mm:ss") + "\r\nPacket Data : " + BitConverter.ToString(gPacket.mRawBytes));
                        else
                            _log.Info("F/V " + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Recieved a duplicate alarm request. This Request : " + alarm.ToString("dd/MM/yyyy HH:mm:ss"));
                        bWriteDBRecord = false;
                    }
                    #endregion
                }
                #endregion
                #region PENDENT_ALARM_CLEARED Processing
                else if (gPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                {
                    _log.Info("F/V" + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Processing pendant alarm cleared.");
                    #region If this is a clear packet, stop sending the unit alarm cleared packets
                    DateTime packetTime;
                    GPClock alarmTime = gPacket.mCurrentClock;
                    if (!alarmTime.IsDate(null))
                    {
                        packetTime = DateTime.UtcNow;
                        _log.Info("Pendant alarm clear time invalid - using listener clock (UTC): " + packetTime);
                    }
                    else
                    {
                        packetTime = alarmTime.ToDateTime();
                        _log.Info("Pendant alarm clear time (UTC): " + packetTime);
                    }

                    int removeCount = mDatabase.PendentActiveAlarmList.RemoveAlarms(gPacket.cFleetId, gPacket.iVehicleId, packetTime);
                    if (removeCount <= 0)
                    {
                        if (gPacket.mRawBytes != null)
                            _log.Info("F/V" + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Recieved an alarm clear with no outstanding alarms.\r\nPacket Data : " + BitConverter.ToString(gPacket.mRawBytes));
                        else
                            _log.Info("F/V" + Convert.ToString((int)gPacket.cFleetId) + "/" + gPacket.iVehicleId.ToString() + " - Recieved an alarm clear with no outstanding alarms.");

                        bWriteDBRecord = false;
                    }
                    #endregion
                }
                #endregion
                #region Check for outstanding pendant alarm acks  
                else
                {
                    #region If there is an pendant alarm ack waiting to be sent to the unit, send it.
                    _log.Debug("Checking for outstanding pendant alarm acks for vehicle (F/V) " + gPacket.cFleetId + "/" + gPacket.iVehicleId);
                    //PendantAlarmListItem listItem = null;
                    var alarms = mDatabase.PendentActiveAlarmList.GetAcknowledgedAlarms(gPacket.cFleetId, gPacket.iVehicleId);

                    foreach (var listItem in alarms)
                    {
                        _log.Info("Sending alarm ack to vehicle (F/V) " + gPacket.cFleetId + "/" + gPacket.iVehicleId);
                        // If there is an ack to send.
                        try
                        {
                            if (!(gPacket._receivedFlashAvailable && listItem.WaitTillFlashClearBeforeResend))
                            {
                                if (gPacket._receivedFlashAvailable)
                                    listItem.WaitTillFlashClearBeforeResend = true;
                                else
                                    listItem.WaitTillFlashClearBeforeResend = false;
                                v.SendOnce(listItem.AckPacket); // Problem acks do not include the alarm time therefore vehicle does not know which alarm has been acked!
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex);
                        }
                    }
                    #endregion
                }
                #endregion
            }

            //ignore Start end breaks unless they are from a card swipe. These events are now sent in via jobs interface
            //Also allow sub packet through
            if ((gPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || 
                gPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END) 
                && !gPacket.mECMAdvanced.DriverCardData && gPacket.cInputNumber == 0)
            {
                return "Ignore break packet";
            }

            bool processPacket = true;

            switch (gPacket.cMsgType)
            {
                case GeneralGPPacket.GEN_IM_ALIVE:
                case GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN:
                case GeneralGPPacket.GEN_IM_ALIVE_FULL:
                    _remoteUpdateEngine.VerifyImAlive(gPacket.lSerialNumber);
                    break;
                case GPSHistoryGPPacket.GPS_HISTORY:
                    processPacket = true;
                    break;
                default:
                    processPacket = _remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(gPacket.cFleetId), gPacket.iVehicleId);
                    break;
            }

            if (!processPacket)
            {
                return string.Format("Fleet {0}, Vehicle {1} - Packet Ignored by RemoteRequest", Convert.ToInt32(gPacket.cFleetId), gPacket.iVehicleId);
            }
            else
            {
                try
                {
                    if (iAdjustForGPSBounceWhenSpeedBelow > 0)
                    {
                        #region If the speed > 0 and < 5 natical miles and the ignition is off, zero the speed values.
                        if (gPacket != null)
                        {
                            int iSpeed = 0;
                            int iInputStatus = 0;

                            if (gPacket.mFix != null)
                            {
                                iSpeed = Convert.ToInt32(gPacket.mFix.cSpeed);
                            }
                            if (gPacket.mStatus != null)
                            {
                                iInputStatus = Convert.ToInt32(gPacket.mStatus.cInputStatus);
                            }
                            if (iSpeed < iAdjustForGPSBounceWhenSpeedBelow && (iInputStatus & 1) != 1)
                            {
                                if (gPacket.mFix != null) gPacket.mFix.cSpeed = (byte)0x00;
                                if (gPacket.mDistance != null) gPacket.mDistance.cMaxSpeed = (byte)0x00;
                            }
                        }
                        #endregion
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex);
                }
                switch (gPacket.cMsgType)
                {
                    case GeneralGPPacket.GEN_EXCESSIVE_IDLE:		//POD added 09092005
                    case GeneralGPPacket.STATUS_REPORT:	// For speed we check this first
                    case GeneralGPPacket.GEN_NOGPS:
                    case GeneralGPPacket.GEN_NO_GPS_ANT_OPEN:
                    case GeneralGPPacket.GEN_NO_GPS_ANT_SHORT:
                    case GeneralGPPacket.GEN_SLEEPING:
                    case GeneralGPPacket.STATUS_TRAILER_HITCH:
                    case GeneralGPPacket.STATUS_TRAILER_DEHITCH:
                    case GeneralGPPacket.OVERSPEED_ZONE_1_START:
                    case GeneralGPPacket.OVERSPEED_ZONE_2_START:
                    case GeneralGPPacket.OVERSPEED_ZONE_3_START:
                    case GeneralGPPacket.OVERSPEED_ZONE_1_END:
                    case GeneralGPPacket.OVERSPEED_ZONE_2_END:
                    case GeneralGPPacket.OVERSPEED_ZONE_3_END:
                    case GeneralGPPacket.ANGEL_GEAR_ALERT_START:
                    case GeneralGPPacket.ANGEL_GEAR_ALERT_END:
                    case GeneralGPPacket.FATIGUE_REPORT_IGNON:
                    case GeneralGPPacket.FATIGUE_REPORT_24:
                    case GeneralGPPacket.STATUS_ACCIDENT_NEW:
                    case GeneralGPPacket.POSSIBLE_ACCIDENT:
                    case GeneralGPPacket.ENGINE_FUEL_ECONOMY_ALERT:
                    case GeneralGPPacket.STATUS_DATA_USAGE_ALERT:
                    case GeneralGPPacket.STATUS_DATA_USAGE_ALERT_SATELLITE:
                    case GeneralGPPacket.BARREL_CONCRETE_AGE_MINS:
                    case GeneralGPPacket.BARREL_CONCRETE_AGE_ROTATIONS:
                    case GeneralGPPacket.REFRIG_REPORT:
                    case GeneralGPPacket.ALRM_SUSPECT_GPS:
                    case GeneralGPPacket.ALRM_IGNITION_DISCONNECT:
                    case GeneralGPPacket.ALRM_ENGINE_DATA_MISSING:
                    case GeneralGPPacket.ALRM_ENGINE_DATA_RE_CONNECT:
                    case GeneralGPPacket.ALRM_SUSPEC_ENGINE_DATA:
                    case GeneralGPPacket.ALRM_G_FORCE_MISSING:
                    case GeneralGPPacket.ALRM_G_FORCE_OUT_OF_CALIBRATION:
                    case GeneralGPPacket.ALRM_CONCETE_SENSOR_ALERT:
                    case GeneralGPPacket.ALRM_MDT_NOT_LOGGED_IN:
                    case GeneralGPPacket.ALRM_MDT_NOT_BEING_USED:
                    case GeneralGPPacket.ALRM_UNIT_POWERUP:
                    case GeneralGPPacket.ALRM_VEHICLE_GPS_SPEED_DIFF:
                    case GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START:
                    case GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END:
                    case GeneralGPPacket.GEN_ECM_OVERSPEED:
                    case GeneralGPPacket.STATUS_VEHICLE_BREAK_START:
                    case GeneralGPPacket.STATUS_VEHICLE_BREAK_END:
                    case GeneralGPPacket.DRIVER_PERSONAL_KMS:
                    case GeneralGPPacket.DRIVER_OVERSPEED_REPORT:
                    case GeneralGPPacket.GEN_MDT_STATUS_REPORT:
                    case GeneralGPPacket.PENDENT_ALARM_TEST:
                    case GeneralGPPacket.PENDENT_ALARM:
                    case GeneralGPPacket.PENDENT_ALARM_CLEARED:
                    case GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER:
                    case GeneralGPPacket.PENDENT_ALARM_NOT_ACKD_BY_USER:
                    case GeneralGPPacket.STATUS_LOGIN:
                    case GeneralGPPacket.STATUS_LOGOUT:
                    case GeneralGPPacket.GEN_IF_CONFIG_RESTART:
                    case GeneralGPPacket.GEN_ZERO_BYTES_RECEIVED_RESTART:
                    case GeneralGPPacket.SHELL_REQUEST_REBOOT:
                    case GeneralGPPacket.ALARM_LOW_BATTERY:
                    case GeneralGPPacket.ECM_HYSTERESIS_ERROR:
                    case GeneralGPPacket.GFORCE_CALIBRATION:
                    case GeneralGPPacket.GPS_SETTINGS:
                    case GeneralGPPacket.DIAGNOSTIC_LEVEL:
                    case GeneralGPPacket.DIAGNOSTIC:
                    case GeneralGPPacket.SAT_PING:
                    case GeneralGPPacket.TAMPER:
                    case GeneralGPPacket.LOW_POWER_MODE:
                        try
                        {
                            UpdateInputStatus(gPacket);
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            UpdatePositionStatus(gPacket);
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            // Check to see if any concrete state flags have been set by the unit
                            if (
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) == (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADING) == (long)StatusVehicle59.STATUS_CONC_LOADING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADED) == (long)StatusVehicle59.STATUS_CONC_LOADED) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_TO_JOB) == (long)StatusVehicle59.STATUS_CONC_TO_JOB) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_AT_JOB) == (long)StatusVehicle59.STATUS_CONC_AT_JOB) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_UNLOADING) == (long)StatusVehicle59.STATUS_CONC_UNLOADING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_RETURNING) == (long)StatusVehicle59.STATUS_CONC_RETURNING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_SWITCHING) == (long)StatusVehicle59.STATUS_CONC_SWITCHING)
                                )
                                UpdateConcreteStatus(gPacket, gPacket.mExtendedVariableDetails.UnitStatus, gPacket.mECMAdvanced);	// The unit has the concrete state
                            else
                                UpdateConcreteStatus(gPacket, 0, gPacket.mECMAdvanced);	// The concrete state needs to be processed on the server
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            if (_routeManagement != null && gPacket.mCurrentClock.IsDate(null))
                                _routeManagement.ProcessRouteState(
                                    Convert.ToDouble(gPacket.mFix.dLatitude),
                                    Convert.ToDouble(gPacket.mFix.dLongitude),
                                    v,
                                    gPacket.mExtendedVariableDetails,
                                    gPacket,
                                    gPacket.mCurrentClock.ToDateTime(),
                                    mDatabase);
                        }
                        catch (System.Exception exRoute)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", exRoute);
                        }
                        #region Firmware Login
                        if ((gPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN || gPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || gPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || gPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END) && gPacket.mECMAdvanced.DriverCardData)
                        {
                            List<object[]> sendLogoutsTo = new List<object[]>();
                            PositionDetails oPos = mDatabase.GetPositionForVehicle(v, gPacket, gPacket.GetPosition().dLatitude, gPacket.GetPosition().dLongitude);
                            FirmwareDriverLoginReply oReply = mDatabase.ValidateFirmwareLogin(gPacket.cMsgType, (int)v.cFleet, (int)v.iUnit, gPacket.mCurrentClock.ToDateTime(), gPacket.mECMAdvanced.DriverLogin, gPacket.mECMAdvanced.DriverPin, gPacket.mECMAdvanced.DriverName, gPacket.mECMAdvanced.CardID, gPacket.mECMAdvanced.CardType, null, sendLogoutsTo,
                                gPacket.GetPosition().dLatitude, gPacket.GetPosition().dLongitude, oPos.PlaceName, gPacket.mTransportExtraDetails.iOdometer, gPacket.mTransportExtraDetails.iTotalFuelUsed);
                            if (oReply != null && (gPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN || gPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START))
                            {
                                if (gPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN && sendLogoutsTo.Count > 0)
                                {
                                    foreach (object[] data in sendLogoutsTo)
                                    {
                                        Vehicle logoutVehicle = mSendThreads.GetVehicle((byte)data[0], (uint)data[1]);
                                        logoutVehicle.oFirmwareLoginReply = (FirmwareDriverLoginReply)data[2];
                                    }
                                }
                                v.oFirmwareLoginReply = oReply;
                                if (v.mSenderIP != null && !gPacket.bArchivalData && !gPacket._receivedFlashAvailable)
                                {
                                    v.SendOnce(oReply);
                                }
                            }
                            bWriteDBRecord = true;
                            if (oReply != null)
                            {
                                if (oReply.LoginResult == FirmwareDriverLoginReply.FirmwareLoginResult.CardInactive || oReply.LoginResult == FirmwareDriverLoginReply.FirmwareLoginResult.CredentailFail || oReply.LoginResult == FirmwareDriverLoginReply.FirmwareLoginResult.DriverInactive || oReply.LoginResult == FirmwareDriverLoginReply.FirmwareLoginResult.PinIncorrect)
                                    return "Supressing write of login record for invalid card login.";
                            }
                        }
                        #endregion

                        #region incident buffer start check to see if all buffered data arrives
                        if (gPacket.cMsgType == GeneralGPPacket.STATUS_ACCIDENT_NEW)
                        {
                            if (v.NewIncidentBufferRecieved(gPacket))
                            {
                                v.SendPossibleAccidentLiveUpdate += new Vehicle.SendPossibleAccidentLiveUpdateDelegate(SendPossibleAccidentLiveUpdate);
                            }
                        }
                        #endregion
                        if (bWriteDBRecord)
                        {
                            if (gPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC_LEVEL)
                            {
                                //save the current level in vehicle db
                                mDatabase.SaveDiagnosticsLevelRunning(v, gPacket.DiagnosticsLevel);
                            }
                            if (gPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR)
                            {
                                return mDatabase.InsertEcmHysteresisError(gPacket, v);
                            }
                            return mDatabase.InsertNotificationRecord(gPacket, v);
                        }
                        else
                            return "Duplicate Pendant Alarm or Alarm Clear - No DB Write or Update to Clients";
                    case GeneralGPPacket.STATUS_RX_TX_SATTELITE:
                        if (gPacket.mSatteliteDataUsage != null && v != null)
                            return mDatabase.InsertSatteliteDataUsage(gPacket.mSatteliteDataUsage, v);
                        else
                            return null;
                    case GeneralGPPacket.GEN_IM_ALIVE:
                    case GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN:
                    case GeneralGPPacket.GEN_IM_ALIVE_FULL:
                    case GeneralGPPacket.GEN_IM_ALIVE_IMIE:
                        #region Handle GEN_IM_ALIVE Packets
                        try
                        {
                            System.Data.SqlClient.SqlConnection oConn = mDatabase.GetOpenConnection();
                            if (oConn == null)
                            {
                                _log.Info("Unable to process GEN_IM_ALIVE due to database connection failure.");
                            }
                            else
                            {
                                try
                                {
                                    #region Validate the credentials
                                    dsCredentials = mDatabase.GetGatewayUnitCredentials(gPacket, oConn);
                                    if (dsCredentials == null)
                                        return string.Format("IM_ALIVE_IMEI_ESN processing (0) : Unable to find GatewayUnit : S/N {0} : IMEI {1} : ESN {2}", gPacket.lSerialNumber, gPacket.sIMEIInbound, gPacket.sCDMAESNInbound);
                                    if (dsCredentials.Tables.Count == 0)
                                        return string.Format("IM_ALIVE_IMEI_ESN processing (1) : Unable to find GatewayUnit : S/N {0} : IMEI {1} : ESN {2}", gPacket.lSerialNumber, gPacket.sIMEIInbound, gPacket.sCDMAESNInbound);
                                    if (dsCredentials.Tables[0].Rows.Count == 0)
                                        return string.Format("IM_ALIVE_IMEI_ESN processing (2) : Unable to find GatewayUnit : S/N {0} : IMEI {1} : ESN {2}", gPacket.lSerialNumber, gPacket.sIMEIInbound, gPacket.sCDMAESNInbound);
                                    if (dsCredentials.Tables[0].Rows[0]["FleetId"] == System.DBNull.Value || dsCredentials.Tables[0].Rows[0]["VehicleId"] == System.DBNull.Value)
                                        return string.Format("IM_ALIVE_IMEI_ESN processing (3) : Unable to find GatewayUnit : S/N {0} : IMEI {1} : ESN {2}", gPacket.lSerialNumber, gPacket.sIMEIInbound, gPacket.sCDMAESNInbound);
                                    if (Convert.ToInt32(dsCredentials.Tables[0].Rows[0]["FleetId"]) > 0 && Convert.ToInt32(dsCredentials.Tables[0].Rows[0]["VehicleId"]) == 0)
                                        return string.Format("IM_ALIVE_IMEI_ESN processing (4) : Found unit in fleet {0} that is not assigned to a vehicle : S/N {1} : IMEI {2} : ESN {3}", Convert.ToString(dsCredentials.Tables[0].Rows[0]["FleetId"]), gPacket.lSerialNumber, gPacket.sIMEIInbound, gPacket.sCDMAESNInbound);
                                    #endregion
                                    #region Update the T_GatewayUnit (and T_AssetRegister if possible)
                                    errMsg = mDatabase.UpdateGatewayUnitWithCredentials(gPacket, dsCredentials, oConn);
                                    #endregion
                                }
                                catch (System.Exception ex)
                                {
                                    _log.Error(sClassName + "ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket) - Updating GatewayUnit credentials.", ex);
                                }
                                finally
                                {
                                    if (oConn != null)
                                    {
                                        oConn.Close();
                                        oConn.Dispose();
                                        oConn = null;
                                    }
                                }
                                if (errMsg != null)
                                    return errMsg;
                            }
                            if ((int)gPacket.cFleetId > 0 && gPacket.iVehicleId > 0)
                            {
                                Vehicle oVehicle = mSendThreads.GetVehicle(configPacket);
                                if (bSendCTAAlways || !oVehicle.bResendActive)
                                {
                                    #region Get the configuration packet
                                    _log.InfoFormat("About to create total config active because received a {0}", gPacket.cMsgType);
                                    configPacket = new ConfigTotalGPPacket(gPacket, _serverTime_DateFormat);
                                    errMsg = mDatabase.GetVehicleConfigTotal(ref configPacket);
                                    if (errMsg != null)
                                        return errMsg;
                                    configPacket.Encode(ref dummy);
                                    mSendThreads.UpdateVehicleIP(configPacket);
                                    if (SendPacket(configPacket, 0, 1, bTransitioning) == false)
                                        return "SendPacket(CTA) failed for fleet " + configPacket.cFleetId + " unit " + configPacket.iVehicleId;
                                    #endregion
                                }
                                else
                                    return "Listener is resending data, no CTA was sent.";
                            }
                        }
                        catch (Exception credentialsEx)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", credentialsEx);
                            return string.Format("IM_ALIVE_IMEI_ESN processing : Failed processing for GatewayUnit : S/N {0} : IMEI {1} : ESN {2} : Error : {3}", gPacket.lSerialNumber, gPacket.sIMEIInbound, gPacket.sCDMAESNInbound, credentialsEx.Message);
                        }
                        return errMsg;
                        #endregion
                    case GeneralGPPacket.BARREL_RPM_FORWARD:
                    case GeneralGPPacket.BARREL_RPM_REVERSED:
                    case GeneralGPPacket.BARREL_RPM_STOPPED:
                    case GeneralGPPacket.BARREL_RPM_UP_TO_SPEED:
                    case GeneralGPPacket.BARREL_LEFT_LOADER:
                    case GeneralGPPacket.BARREL_MIX_STARTED:
                    case GeneralGPPacket.BARREL_NOT_MIXED:
                    case GeneralGPPacket.BARREL_ABOVE_ONSITE_SPEED:
                    case GeneralGPPacket.BARREL_LOADING_STARTED:
                    case GeneralGPPacket.GEN_OVERSPEED:
                    case GeneralGPPacket.GEN_TAMPER:
                    case GeneralGPPacket.GEN_ALARM:
                    case GeneralGPPacket.GEN_IOONOFF:
                    case GeneralGPPacket.GEN_EXCESSIVE_IDLE_END:
                    case GeneralGPPacket.STATUS_RAW_GPS:
                    case GeneralGPPacket.STATUS_POS_REPORT:
                    case GeneralGPPacket.ENGINE_OVER_COOLANT_TEMP:
                    case GeneralGPPacket.ENGINE_OVER_OIL_TEMP:
                    case GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_LOW:
                    case GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_HIGH:
                    case GeneralGPPacket.ENGINE_OVER_RPM:
                    case GeneralGPPacket.ENGINE_G_FORCES_HIGH:
                    case GeneralGPPacket.ENGINE_COOLANT_LEVEL_LOW:	// Not truly supported:
                    case GeneralGPPacket.ENGINE_G_FORCE_CALIBRATED_RPT:
                    case GeneralGPPacket.ENGINE_ERROR_CODE:
                        #region Handle other messages which hold a location
                        try
                        {
                            UpdateInputStatus(gPacket);
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            UpdatePositionStatus(gPacket);
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            // Check to see if any concrete state flags have been set by the unit
                            if (
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) == (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADING) == (long)StatusVehicle59.STATUS_CONC_LOADING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADED) == (long)StatusVehicle59.STATUS_CONC_LOADED) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_TO_JOB) == (long)StatusVehicle59.STATUS_CONC_TO_JOB) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_AT_JOB) == (long)StatusVehicle59.STATUS_CONC_AT_JOB) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_UNLOADING) == (long)StatusVehicle59.STATUS_CONC_UNLOADING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_RETURNING) == (long)StatusVehicle59.STATUS_CONC_RETURNING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_SWITCHING) == (long)StatusVehicle59.STATUS_CONC_SWITCHING)
                                )
                                UpdateConcreteStatus(gPacket, gPacket.mExtendedVariableDetails.UnitStatus, gPacket.mECMAdvanced);	// The unit has the concrete state
                            else
                                UpdateConcreteStatus(gPacket, 0, gPacket.mECMAdvanced);	// The concrete state needs to be processed on the server
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        if (gPacket.cMsgType == GeneralGPPacket.ENGINE_G_FORCE_CALIBRATED_RPT)
                        {
                            if (gPacket.mEngineData != null)
                            {
                                _log.Info("Fleet " + Convert.ToString((int)gPacket.cFleetId) + " Unit " + Convert.ToString(gPacket.iVehicleId) + " : G-Force Auto-Calibrated - Forwards : " + gPacket.mEngineData.fGForceFront.ToString("0.00") + ", Backwards : " + gPacket.mEngineData.fGForceBack.ToString("0.00") + ", Lateral : " + gPacket.mEngineData.fGForceLeftRight.ToString("0.00"));
                            }
                            else
                            {
                                _log.Info("Fleet " + Convert.ToString((int)gPacket.cFleetId) + " Unit " + Convert.ToString(gPacket.iVehicleId) + " : G-Force Auto-Calibrated.");
                            }
                            return null;
                        }
                        else
                        {
                            try
                            {
                                if (_routeManagement != null && gPacket.mCurrentClock.IsDate(null))
                                    _routeManagement.ProcessRouteState(
                                        Convert.ToDouble(gPacket.mFix.dLatitude),
                                        Convert.ToDouble(gPacket.mFix.dLongitude),
                                        v,
                                        gPacket.mExtendedVariableDetails,
                                        gPacket,
                                        gPacket.mCurrentClock.ToDateTime(),
                                        mDatabase);
                            }
                            catch (System.Exception exRoute)
                            {
                                if (gPacket != null && gPacket.mRawBytes != null)
                                    _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : " + BitConverter.ToString(gPacket.mRawBytes));
                                else
                                    _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : null");
                            }
                            return mDatabase.InsertNotificationRecord(gPacket, v);
                        }

                        #endregion
                    case GeneralGPPacket.STATUS_IGNITION_ON:
                    case GeneralGPPacket.STATUS_IGNITION_OFF:
                    case GeneralGPPacket.STATUS_IO_PULSES:
                    case GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD:
                    case GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD:
                        #region Handle Input state change / value change:
                        try
                        {
                            UpdateInputStatus(gPacket);
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            UpdatePositionStatus(gPacket);
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            // Check to see if any concrete state flags have been set by the unit
                            if (
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) == (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADING) == (long)StatusVehicle59.STATUS_CONC_LOADING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADED) == (long)StatusVehicle59.STATUS_CONC_LOADED) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_TO_JOB) == (long)StatusVehicle59.STATUS_CONC_TO_JOB) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_AT_JOB) == (long)StatusVehicle59.STATUS_CONC_AT_JOB) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_UNLOADING) == (long)StatusVehicle59.STATUS_CONC_UNLOADING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_RETURNING) == (long)StatusVehicle59.STATUS_CONC_RETURNING) ||
                                (((long)gPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_SWITCHING) == (long)StatusVehicle59.STATUS_CONC_SWITCHING)
                                )
                                UpdateConcreteStatus(gPacket, gPacket.mExtendedVariableDetails.UnitStatus, gPacket.mECMAdvanced);	// The unit has the concrete state
                            else
                                UpdateConcreteStatus(gPacket, 0, gPacket.mECMAdvanced);	// The concrete state needs to be processed on the server
                        }
                        catch (System.Exception ex1)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessGenPacket(Vehicle v, GeneralGPPacket gPacket)", ex1);
                        }
                        try
                        {
                            if (_routeManagement != null && gPacket.mCurrentClock.IsDate(null))
                                _routeManagement.ProcessRouteState(
                                    Convert.ToDouble(gPacket.mFix.dLatitude),
                                    Convert.ToDouble(gPacket.mFix.dLongitude),
                                    v,
                                    gPacket.mExtendedVariableDetails,
                                    gPacket,
                                    gPacket.mCurrentClock.ToDateTime(),
                                    mDatabase);
                        }
                        catch (System.Exception exRoute)
                        {
                            if (gPacket != null && gPacket.mRawBytes != null)
                                _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : " + BitConverter.ToString(gPacket.mRawBytes));
                            else
                                _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : null");
                        }

                        //check any stationary/out of usage alerts
                        if (gPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON)
                        {
                            int configId = mDatabase.GetConfigIdForUnit(v.cFleet, v.iUnit);
                            if (configId > 0)
                            {
                                TemporalObject time = mDatabase.GetOutOfHoursTimePeriods(configId);
                                if (time != null)
                                {
                                    TemporalObject.CheckTimeResult result = time.CheckTimes(gPacket.dtGPSTime, mDatabase, v);
                                    if (!result.InsideTimePeriod)
                                    {
                                        //outside usage - create a start event
                                        GenericPacket newPacket = new GenericPacket(_serverTime_DateFormat);
                                        newPacket.UpdateFromGeneralGPPacket(gPacket);
                                        newPacket.AddSeconds(1);
                                        newPacket.SubCommand = GenericSubCommands.OutOfHoursUsageStart;
                                        newPacket.Encode(ref dummy);
                                        v.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_OUT_OF_HOURS;
                                        mDatabase.StartActiveOutOfHoursUsage((int)v.cFleet, (int)v.iUnit, result.ScheduledArriveTime);

                                        mDatabase.InsertGenericPacket(newPacket, v);
                                        v.AddAdditionalLiveUpdate(newPacket.CreateGeneralGPPacket());
                                    }
                                }
                            }

                            DataRow row = mDatabase.FindStationaryAlert((int)v.cFleet, (int)v.iUnit);
                            if (row != null)
                            {
                                _log.DebugFormat("Stationary Alert - Ignition On for {0}/{1}, found alert", v.cFleet, v.iUnit);
                                //stationary alert found - create an end event if not active
                                bool active = Convert.ToBoolean(row["Active"]);
                                mDatabase.EndStationaryAlert((int)v.cFleet, (int)v.iUnit);
                                if (active)
                                {
                                    GenericPacket newPacket = new GenericPacket(_serverTime_DateFormat);
                                    newPacket.UpdateFromGeneralGPPacket(gPacket);
                                    newPacket.AddSeconds(-1);
                                    newPacket.SubCommand = GenericSubCommands.StationaryAlertEnd;
                                    newPacket.Encode(ref dummy);

                                    mDatabase.InsertGenericPacket(newPacket, v);
                                    v.AddAdditionalLiveUpdate(newPacket.CreateGeneralGPPacket());
                                    v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_STATIONARY_ALERT;
                                }
                            }
                            else
                            {
                                _log.DebugFormat("Stationary Alert - Ignition On for {0}/{1}, NO alert", v.cFleet, v.iUnit);
                            }

                        }
                        else if (gPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF)
                        {
                            int id = mDatabase.FindActiveOutOfHoursUsage((int)v.cFleet, (int)v.iUnit);
                            if (id > 0)
                            {
                                //usage found - create a end event
                                GenericPacket newPacket = new GenericPacket(_serverTime_DateFormat);
                                newPacket.UpdateFromGeneralGPPacket(gPacket);
                                newPacket.AddSeconds(-1);
                                newPacket.SubCommand = GenericSubCommands.OutOfHoursUsageEnd;
                                newPacket.Encode(ref dummy);
                                mDatabase.EndActiveOutOfHoursUsage((int)v.cFleet, (int)v.iUnit);

                                mDatabase.InsertGenericPacket(newPacket, v);
                                v.AddAdditionalLiveUpdate(newPacket.CreateGeneralGPPacket());
                                v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_OUT_OF_HOURS;
                            }

                            //check stationary alert if not at a waypoint
                            int configId = mDatabase.GetConfigIdForUnit(v.cFleet, v.iUnit);
                            _log.DebugFormat("Stationary Alert - Ignition Off for {0}/{1}, configId = {2}, isAtWaypoint = {3}, waypointCount = {4}", v.cFleet, v.iUnit, configId, mDatabase.IsAtWaypoint((int)v.cFleet, (int)v.iUnit), v.oWayPointIDs.Count);
                            _log.DebugFormat("Stationary Alert - Packet Current SetPoint {0} vehicle setPoint {1}", (gPacket.mExtendedVariableDetails == null ? -1 : gPacket.mExtendedVariableDetails.CurrentSetPointID), v.oPos.SetPointID);
                            bool atWaypoint = false;
                            if (gPacket.mExtendedVariableDetails != null && gPacket.mExtendedVariableDetails.CurrentSetPointID > 0)
                            {
                                atWaypoint = true;
                            }
                            else if (gPacket.mExtendedVariableDetails == null && v.oPos.SetPointID > 0)
                            {
                                atWaypoint = true;
                            }
                            if (configId > 0 && !atWaypoint && v.oWayPointIDs.Count == 0)
                            {
                                TemporalObject time = mDatabase.GetStationaryTimePeriods(configId);
                                if (time != null)
                                {
                                    int alertPeriod = mDatabase.GetStationaryAlertPeriod(configId);
                                    _log.DebugFormat("Stationary Alert - alert period = {0}", alertPeriod);
                                    if (alertPeriod > 0)
                                    {
                                        TemporalObject.CheckTimeResult result = time.CheckTimes(gPacket.dtGPSTime, mDatabase, v);
                                        DateTime start = gPacket.dtGPSTime.AddMinutes(alertPeriod);
                                        if (!result.InsideTimePeriod)
                                        {
                                            //outside time period, set schedule start time to be priod start time plus alert period
                                            start = result.ScheduledArriveTime.AddMinutes(alertPeriod);
                                        }
                                        mDatabase.StartStationaryAlert((int)v.cFleet, (int)v.iUnit, start);
                                    }
                                }
                            }
                        }

                        return mDatabase.InsertInputActiveDeactiveRecord(gPacket, v);
                        #endregion
                    case GeneralGPPacket.GEN_STARTUP:
                        //	Log to the database that a unit has been reset.
                        System.Text.StringBuilder builder = new System.Text.StringBuilder();
                        builder.Append(string.Format("GEN_STARTUP received from fleet {0}, unit {1}, ipAddress {2}", v.cFleet, v.iUnit, v.mSenderIP.ToString()));
                        if (gPacket.DebugDump == null)
                            builder.Append(" : NO DEBUG INFO");
                        else
                        {
                            builder.Append("; Count : ");
                            builder.Append(gPacket.DebugDump.Count);
                            builder.Append("; Index : ");
                            builder.Append(gPacket.DebugDump.Index);
                            builder.Append("; Watchdog : ");
                            builder.Append(gPacket.DebugDump.WatchDog);
                            builder.Append("; LogonWatchdog : ");
                            builder.Append(gPacket.DebugDump.LogonWatchDog);
                            builder.Append(" ProgramCounter : 0x");
                            builder.Append(gPacket.DebugDump.ProgramCounter.ToString("X2"));
                        }
                        _log.Info(builder.ToString());
                        return null;
                    case GeneralGPPacket.GEN_RESET:
                        //	Log to the database that a unit has been reset.
                        _log.Info(string.Format("GEN_RESET received from fleet {0}, unit {1}, ipAddress {2}", v.cFleet, v.iUnit, v.mSenderIP.ToString()));

                        return null;
                    case GeneralGPPacket.STATUS_RX_TX:
                        return mDatabase.UpdateBytesSentAndRecieved(gPacket, v);
                    case GeneralGPPacket.GEN_ACK:
                        return null;
                    default:
                        return string.Format("Unknown General Message Type {0}", gPacket.cMsgType);

                }
            }

        }

        /// <summary>
        /// Process a routing packet to determine if the unit is on-route or off route.
        /// </summary>
        /// <param name="packet"></param>
        /// <returns></returns>
        private string ProcessRoutingPacket(RoutingModuleGPPacket rPacket)
        {
            //	NOTE : I hate replicating the code here from the Set Point Packet, but have no choice to 
            //	get it done in the time allotted.

            Vehicle v = null;

            bool processPacket = _remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(rPacket.cFleetId), rPacket.iVehicleId);
            if (!processPacket)
            {
                return string.Format("Fleet {0}, Vehicle {1} - Packet Ignored by RemoteRequest", Convert.ToInt32(rPacket.cFleetId), rPacket.iVehicleId);
            }
            else
            {
                try
                {
                    v = mSendThreads.GetVehicle(rPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessRoutingPacket(RoutingModuleGPPacket rPacket)", ex);
                }

                try
                {
                    if (_routeManagement != null)
                        _routeManagement.ProcessRoutingPacket(rPacket, v);
                }
                catch (System.Exception exRoute)
                {
                    _log.Info("Error processing Route : " + exRoute.Message);
                }

                try
                {
                    if (_routeManagement != null && rPacket.mCurrentClock.IsDate(null))
                        _routeManagement.ProcessRouteState(
                            Convert.ToDouble(rPacket.mFix.dLatitude),
                            Convert.ToDouble(rPacket.mFix.dLongitude),
                            v,
                            rPacket.mExtendedVariableDetails,
                            rPacket,
                            rPacket.mCurrentClock.ToDateTime(),
                            mDatabase);
                }
                catch (System.Exception exRoute)
                {
                    if (rPacket != null && rPacket.mRawBytes != null)
                        _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : " + BitConverter.ToString(rPacket.mRawBytes));
                    else
                        _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : null");
                }

                if (v == null) return "ProcessRoutingPacket: Could not find Vehicle object";
                // At the moment we'll take anything which matches
                // the SETPT or ROUTEPT masks - a* or b*
                return mDatabase.InsertRoutingRecord(rPacket, v);
            }

        }

        private string ProcessPointPacket(RoutePtSetPtGPPacket rPacket)
        {
            Vehicle v = null;
            string sDBVer = System.Configuration.ConfigurationManager.AppSettings["DatabaseVer"];
            double dbVer = Convert.ToDouble(sDBVer);

            bool processPacket = _remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(rPacket.cFleetId), rPacket.iVehicleId);
            if (!processPacket)
            {
                return string.Format("Fleet {0}, Vehicle {1} - Packet Ignored by RemoteRequest", Convert.ToInt32(rPacket.cFleetId), rPacket.iVehicleId);
            }
            else
            {
                try
                {
                    v = mSendThreads.GetVehicle(rPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessPointPacket(RoutePtSetPtGPPacket rPacket)", ex);
                }

                //check if this waypoint is a temperoal waypoint or not
                //rPacket.
                TemporalWayPoint wp = mDatabase.GetTemporalWaypoint(rPacket.mExtendedVariableDetails.CurrentSetPointID);
                bool valid = true;
                if (wp != null)
                {
                    //check that the temporal waypoint is active
                    valid = wp.CheckTimes(rPacket.mCurrentClock.ToDateTime(), mDatabase, v, rPacket.cMsgType);
                }

                if (!valid)
                {
                    //convert the packet to status report and save it
                    GatewayProtocolPacket oGPP = (GatewayProtocolPacket)rPacket;
                    GeneralGPPacket newPacket = new GeneralGPPacket(oGPP, _serverTime_DateFormat);
                    newPacket.mCurrentClock = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mCurrentClock;
                    newPacket.mFixClock = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mFixClock;
                    newPacket.mFix = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mFix;
                    newPacket.mStatus = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mStatus;
                    newPacket.mDistance = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mDistance;
                    newPacket.mExtendedValues = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mExtendedValues;
                    newPacket.mEngineData = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mEngineData;
                    newPacket.mTransportExtraDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mTransportExtraDetails;
                    newPacket.mTrailerTrack = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mTrailerTrack;
                    newPacket.mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mExtendedVariableDetails;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_AT;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_DOCK;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_HOLDING;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_NOGO;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_SPEEDING;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_TOO_LONG;
                    newPacket.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_WORKSHOP;
                    newPacket.cMsgType = GeneralGPPacket.STATUS_REPORT;

                    mDatabase.InsertNotificationRecord(newPacket, v);
                    GeneralGPPacket oLiveUpdatePacket = ((GeneralGPPacket)newPacket).CreateCopy();
                    v.AddAdditionalLiveUpdate(oLiveUpdatePacket);

                    return "Setpoint is temporal and are outside its active time - saving report as status report";
                }

                // If it's a waypoint message, see if we need to alter
                // the arrived or departed state info for this unit:
                switch (rPacket.cMsgType)
                {
                    case RoutePtSetPtGPPacket.SETPT_ARRIVE:
                    case RoutePtSetPtGPPacket.SETPT_ARRIVE_WORKSHOP:
                    case RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK:
                    case RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO:
                        try
                        {
                            if (wp == null)
                            {
                                mDatabase.ArrivedWaypoint((int)v.cFleet, (int)v.iUnit, rPacket.mExtendedVariableDetails.CurrentSetPointID);
                            }
                            if (dbVer >= 5.9)
                            {
                                GBVariablePacketExtended59 mExtendedVariableDetails = null;
                                mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mExtendedVariableDetails;
                                if (mExtendedVariableDetails == null)
                                    mExtendedVariableDetails = new GBVariablePacketExtended59();
                                if (!mExtendedVariableDetails.Populated)
                                {
                                    mSendThreads.UpdateUnitState(rPacket, UnitState.AtWayPoint);
                                    if ((rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_WORKSHOP) ||
                                        (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK) ||
                                        (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO))
                                        mSendThreads.UpdateUnitState(rPacket, UnitState.AtWPSpecial2);
                                }
                            }
                            else
                            {
                                mSendThreads.UpdateUnitState(rPacket, UnitState.AtWayPoint);
                                if ((rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_WORKSHOP) ||
                                    (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK) ||
                                    (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO))
                                    mSendThreads.UpdateUnitState(rPacket, UnitState.AtWPSpecial2);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessRoutingPacket(RoutingModuleGPPacket rPacket)", ex);
                        }
                        break;
                    case RoutePtSetPtGPPacket.SETPT_DEPART:
                    case RoutePtSetPtGPPacket.SETPT_DEPART_WORKSHOP:
                    case RoutePtSetPtGPPacket.SETPT_DEPART_DOCK:
                    case RoutePtSetPtGPPacket.SETPT_DEPART_NOGO:
                        try
                        {
                            if (wp == null)
                            {
                                mDatabase.EndWaypoint((int)v.cFleet, (int)v.iUnit, rPacket.mExtendedVariableDetails.CurrentSetPointID);
                            }
                            if (dbVer >= 5.9)
                            {
                                GBVariablePacketExtended59 mExtendedVariableDetails = null;
                                mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mExtendedVariableDetails;
                                if (mExtendedVariableDetails == null)
                                    mExtendedVariableDetails = new GBVariablePacketExtended59();
                                if (!mExtendedVariableDetails.Populated)
                                {
                                    mSendThreads.UpdateUnitState(rPacket, UnitState.DepWayPoint);
                                    if ((rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_WORKSHOP) ||
                                        (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_DOCK) ||
                                        (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_NOGO))
                                        mSendThreads.ClearUnitState(rPacket, UnitState.AtWPSpecial2);
                                }
                            }
                            else
                            {
                                mSendThreads.UpdateUnitState(rPacket, UnitState.DepWayPoint);
                                if ((rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_WORKSHOP) ||
                                    (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_DOCK) ||
                                    (rPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_NOGO))
                                    mSendThreads.ClearUnitState(rPacket, UnitState.AtWPSpecial2);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessPointPacket(RoutePtSetPtGPPacket rPacket)", ex);
                        }
                        break;
                    case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_ARRIVE:
                        try
                        {
                            if (wp == null)
                            {
                                mDatabase.ArrivedWaypoint((int)v.cFleet, (int)v.iUnit, rPacket.mExtendedVariableDetails.CurrentSetPointID);
                            }
                            // Check to see if any concrete state flags have been set by the unit
                            if (
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) == (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADING) == (long)StatusVehicle59.STATUS_CONC_LOADING) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADED) == (long)StatusVehicle59.STATUS_CONC_LOADED) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_TO_JOB) == (long)StatusVehicle59.STATUS_CONC_TO_JOB) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_AT_JOB) == (long)StatusVehicle59.STATUS_CONC_AT_JOB) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_UNLOADING) == (long)StatusVehicle59.STATUS_CONC_UNLOADING) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_RETURNING) == (long)StatusVehicle59.STATUS_CONC_RETURNING) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_SWITCHING) == (long)StatusVehicle59.STATUS_CONC_SWITCHING)
                                )
                                UpdateConcreteStatus(rPacket, rPacket.mExtendedVariableDetails.UnitStatus, rPacket.mECMAdvanced);	// The unit has the concrete state
                            else
                                UpdateConcreteStatus(rPacket, 0, rPacket.mECMAdvanced);	// The concrete state needs to be processed on the server

                            if (dbVer >= 5.9)
                            {
                                GBVariablePacketExtended59 mExtendedVariableDetails = null;
                                mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mExtendedVariableDetails;
                                if (mExtendedVariableDetails == null)
                                    mExtendedVariableDetails = new GBVariablePacketExtended59();
                                if (!mExtendedVariableDetails.Populated)
                                {
                                    mSendThreads.UpdateUnitState(rPacket, UnitState.AtWayPoint);
                                    mSendThreads.UpdateUnitState(rPacket, UnitState.AtWPSpecial1);
                                }
                            }
                            else
                            {
                                mSendThreads.UpdateUnitState(rPacket, UnitState.AtWayPoint);
                                mSendThreads.UpdateUnitState(rPacket, UnitState.AtWPSpecial1);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessPointPacket(RoutePtSetPtGPPacket rPacket)", ex);
                        }
                        break;
                    case RoutePtSetPtGPPacket.SETPT_SPECIAL_1_DEPART:
                        try
                        {
                            if (wp == null)
                            {
                                mDatabase.EndWaypoint((int)v.cFleet, (int)v.iUnit, rPacket.mExtendedVariableDetails.CurrentSetPointID);
                            }
                            // Check to see if any concrete state flags have been set by the unit
                            if (
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) == (long)StatusVehicle59.STATUS_CONC_NOT_LOADED) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADING) == (long)StatusVehicle59.STATUS_CONC_LOADING) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_LOADED) == (long)StatusVehicle59.STATUS_CONC_LOADED) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_TO_JOB) == (long)StatusVehicle59.STATUS_CONC_TO_JOB) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_AT_JOB) == (long)StatusVehicle59.STATUS_CONC_AT_JOB) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_UNLOADING) == (long)StatusVehicle59.STATUS_CONC_UNLOADING) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_RETURNING) == (long)StatusVehicle59.STATUS_CONC_RETURNING) ||
                                (((long)rPacket.mExtendedVariableDetails.UnitStatus & (long)StatusVehicle59.STATUS_CONC_SWITCHING) == (long)StatusVehicle59.STATUS_CONC_SWITCHING)
                                )
                                UpdateConcreteStatus(rPacket, rPacket.mExtendedVariableDetails.UnitStatus, rPacket.mECMAdvanced);	// The unit has the concrete state
                            else
                                UpdateConcreteStatus(rPacket, 0, rPacket.mECMAdvanced);	// The concrete state needs to be processed on the server

                            if (dbVer >= 5.9)
                            {
                                GBVariablePacketExtended59 mExtendedVariableDetails = null;
                                mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)rPacket).mExtendedVariableDetails;
                                if (mExtendedVariableDetails == null)
                                    mExtendedVariableDetails = new GBVariablePacketExtended59();
                                if (!mExtendedVariableDetails.Populated)
                                {
                                    mSendThreads.UpdateUnitState(rPacket, UnitState.DepWayPoint);
                                    mSendThreads.ClearUnitState(rPacket, UnitState.AtWPSpecial1);
                                }
                            }
                            else
                            {
                                mSendThreads.UpdateUnitState(rPacket, UnitState.DepWayPoint);
                                mSendThreads.ClearUnitState(rPacket, UnitState.AtWPSpecial1);
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(this.GetType().FullName + ".ProcessPointPacket(RoutePtSetPtGPPacket rPacket)", ex);
                        }
                        break;
                    default:
                        break;
                }

                

                if (v == null) return "ProcessPointPacket: Could not find Vehicle object";

                try
                {
                    if (_routeManagement != null && rPacket.mCurrentClock.IsDate(null))
                        _routeManagement.ProcessRouteState(
                            Convert.ToDouble(rPacket.mFix.dLatitude),
                            Convert.ToDouble(rPacket.mFix.dLongitude),
                            v,
                            rPacket.mExtendedVariableDetails,
                            rPacket,
                            rPacket.mCurrentClock.ToDateTime(),
                            mDatabase);
                }
                catch (System.Exception exRoute)
                {
                    if (rPacket != null && rPacket.mRawBytes != null)
                        _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : " + BitConverter.ToString(rPacket.mRawBytes));
                    else
                        _log.Info("Unable to process Route Managment State : " + exRoute.Message + " Packet Data : null");

                }
                // At the moment we'll take anything which matches
                // the SETPT or ROUTEPT masks - a* or b*
                return mDatabase.InsertSetPointRoutePointRecord(rPacket, v);
            }
        }

        private string ProcessConfigTotalPacket(ConfigTotalGPPacket cPacket)
        {// One of the most important packets - used to make decisions
            // about whether to download a new version of configuration,
            // routes and sets to a given unit.
            byte[] dummy = new byte[1];
            string errMsg = null;
            ConfigTotalGPPacket copyPacket = null;

            // We got a config version update.
            // If there are any differences, we must
            // send a CONFIG_NEW_<something> back to the unit:
            // But only if the unit has a valid number:
            try
            {
                if (cPacket.cFleetId == 0)
                {
                    //POD - No need for this as it wont find a vehicle entry.
                    //UpdateAckStatus(cPacket, false);
                    return "Will not process a CTA from unidentified unit.";
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessConfigTotalPacket(ConfigTotalGPPacket cPacket)", ex);
            }

            try
            {
                // Ensure that we send an ACK if we need to:
                cPacket.bAckImmediately = true;
                cPacket.bSendFlash = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessConfigTotalPacket(ConfigTotalGPPacket cPacket)", ex);
            }

            try
            {
                copyPacket = new ConfigTotalGPPacket(cPacket, _serverTime_DateFormat);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessConfigTotalPacket(ConfigTotalGPPacket cPacket)", ex);
            }

            // Go fetch the database values for this vehicle,
            // and place them into the copied packet
            try
            {
                if ((errMsg = mDatabase.GetVehicleConfigTotal(ref copyPacket)) != null)
                {
                    // Problem getting the config details.
                    // In this case, it is best to simply tell the unit
                    // to stay with what it has - i.e. tell it nothing :-)
                    UpdateAckStatus(cPacket, false);
                    return errMsg + " staying with existing setup";
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessConfigTotalPacket(ConfigTotalGPPacket cPacket)", ex);
            }
            try
            {
                #region Permanent settings
                #region configs
                if (!copyPacket.ConfigurationsAreIdentical(cPacket) && (copyPacket.cConfigFileActive != 0))
                {
                    // We should send a CONFIG_NEW_FILE to the unit
                    ConfigFileGPPacket filePacket = new ConfigFileGPPacket(cPacket, _serverTime_DateFormat);
                    filePacket.cConfigFileNo = copyPacket.cConfigFileActive;
                    filePacket.cVersionMajor = copyPacket.cConfigFileActiveVersionMajor;
                    filePacket.cVersionMinor = copyPacket.cConfigFileActiveVersionMinor;

                    int iConfigGroupID = Convert.ToInt32(filePacket.cConfigFileNo);
                    if ((errMsg = mDatabase.GetConfigFile(ref filePacket)) != null)
                    {
                        // Couldn't find it - let the unit stay with what it's on:
                        UpdateAckStatus(cPacket, false);
                        return errMsg += " - staying with existing onboard config";
                    }//iConfigGroupID
                    filePacket.Encode(ref dummy);
                    if (!SendPacket(filePacket, 1, 60)) UpdateAckStatus(cPacket, false);

                    return string.Format("Sent Config ID = {0} to {1}/{2}, Data : {3}", iConfigGroupID, filePacket.cFleetId, filePacket.iVehicleId, Util.EncodeByteArrayAsHex(dummy));
                }
                #endregion
                #region ECM configs
                if (!copyPacket.EcmConfigurationsAreIdentical(cPacket) && (copyPacket.cEcmConfigFileActive != 0))
                {
                    // We should send a CONFIG_NEW_FILE to the unit
                    ConfigEcmPacket ecmPacket = new ConfigEcmPacket(cPacket, _serverTime_DateFormat);
                    ecmPacket.ConfigId = copyPacket.cEcmConfigFileActive;
                    ecmPacket.ConfigMajor = copyPacket.cEcmConfigFileActiveVersionMajor;
                    ecmPacket.ConfigMinor = copyPacket.cEcmConfigFileActiveVersionMinor;

                    if ((errMsg = mDatabase.GetEcmConfigFile(ref ecmPacket)) != null)
                    {
                        // Couldn't find it - let the unit stay with what it's on:
                        UpdateAckStatus(cPacket, false);
                        return errMsg += " - staying with existing onboard ecm config";
                    }
                    ecmPacket.Encode(ref dummy);

                    if (!SendPacket(ecmPacket, 1, 60)) UpdateAckStatus(cPacket, false);
                    return string.Format("Sent ECM Config ID = {0} to {1}/{2}, Data : {3}", ecmPacket.ConfigId, ecmPacket.cFleetId, ecmPacket.iVehicleId, Util.EncodeByteArrayAsHex(dummy));
                }
                #endregion
                #region routes
                if (!copyPacket.RoutesAreIdentical(cPacket) && (copyPacket.cRouteSetActive != 0))
                {
                    // We should send a CONFIG_NEW_ROUTE to the unit
                    ConfigNewRoutePtGPPacket routePacket = new ConfigNewRoutePtGPPacket(cPacket, _serverTime_DateFormat);
                    routePacket.cRouteSetNumber = copyPacket.cRouteSetActive;
                    int iRPGroupID = 0;
                    if ((errMsg = mDatabase.GetRoutePointGroup(ref routePacket, iRPGroupID)) != null)
                    {
                        // Couldn't find it or had a problem getting it 
                        // - let the unit stay with what it's on:
                        UpdateAckStatus(cPacket, false);
                        return errMsg;
                    }
                    routePacket.Encode(ref dummy);
                    if (!SendPacket(routePacket, 1, 15)) UpdateAckStatus(cPacket, false);// Try up to twice
                    return string.Format("Sent Route Point Group = {0} to {1}/{2}, Data : {3}", iRPGroupID, routePacket.cFleetId, routePacket.iVehicleId, Util.EncodeByteArrayAsHex(dummy));
                }
                #endregion
                #region set points
                if (!copyPacket.SetPointsAreIdentical(cPacket))
                {

                    // We should send a CONFIG_NEW_SETPT to the unit
                    copyPacket.uSetPointGroup_Flags = cPacket.uSetPointGroup_Flags;
                    ConfigNewSetPtGPPacket setPacket = new ConfigNewSetPtGPPacket(cPacket, _serverTime_DateFormat, cPacket.uSetPointGroup_Flags);
                    int iSPGroupdID = copyPacket.cSetPointSetActive;
                    setPacket.cSetPointSetNumber = copyPacket.cSetPointSetActive;
                    if ((errMsg = mDatabase.GetSetPointGroup(ref setPacket, ref  iSPGroupdID, (int)(cPacket.uSetPointGroup_Flags & 0x00000001), false)) != null)
                    {
                        // Couldn't find it or had a problem getting it 
                        // - let the unit stay with what it's on:
                        UpdateAckStatus(cPacket, false);
                        return errMsg;
                    }

                    //if the setpoing group going out to the vehicle is a polygon
                    if (mDatabase.IsPolygonWPGroup(iSPGroupdID) || ((cPacket.uSetPointGroup_Flags & 0x00000001) == 1 || (cPacket.uSetPointGroup_Flags & 0x00000002) == 2))
                    {
                        //check to see if the vehilce can support the polygon setpoint group
                        if ((cPacket.uSetPointGroup_Flags & 0x00000001) == 1 || (cPacket.uSetPointGroup_Flags & 0x00000002) == 2)
                        {
                            if ((cPacket.uSetPointGroup_Flags & 0x00000002) == 2)
                            {
                                if ((cPacket.uSetPointGroup_Flags & 0x00000008) == 8)
                                    setPacket.Encode(ref dummy, ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON, 0, (byte)0x03);		//polygon encode with setpointID
                                else if ((cPacket.uSetPointGroup_Flags & 0x00000004) == 4)
                                    setPacket.Encode(ref dummy, ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON, 0, (byte)0x02);		//polygon encode with setpointID
                                else
                                    setPacket.Encode(ref dummy, ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON, 0, (byte)0x01);		//polygon encode with setpointID
                            }
                            else
                                setPacket.Encode(ref dummy, ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON_OLD, 0, 0x01);	//polygon encode with setpointNumber
                        }
                        else
                        {
                            setPacket = new ConfigNewSetPtGPPacket(cPacket, _serverTime_DateFormat, cPacket.uSetPointGroup_Flags);
                            iSPGroupdID = copyPacket.cSetPointSetActive;
                            setPacket.cSetPointSetNumber = copyPacket.cSetPointSetActive;
                            if ((errMsg = mDatabase.GetSetPointGroup(ref setPacket, ref  iSPGroupdID, (int)(cPacket.uSetPointGroup_Flags & 0x00000001), true)) != null)
                            {
                                _log.Info("A polygon group was assigned to " + Convert.ToString((int)cPacket.cFleetId) + "/" + Convert.ToString((int)cPacket.iVehicleId) + ", this unit can not support polygon WPs.  Failed to create dummy group.");
                            }
                            else
                            {
                                setPacket.Encode(ref dummy);
                                _log.Info("A polygon group was assigned to " + Convert.ToString((int)cPacket.cFleetId) + "/" + Convert.ToString((int)cPacket.iVehicleId) + ", this unit can not support polygon WPs.  A dummy setpoint group was sent to the unit.");
                            }
                        }
                    }
                    else
                    {
                        setPacket.Encode(ref dummy);				//xy tolerance encode
                    }
                    if (setPacket.cMsgType == ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON_OLD)
                    {
                        // The unit does not ack the old polygon setpoint packet, therefore we can not send with retries.
                        if (!SendPacket(setPacket, 0, 1)) UpdateAckStatus(cPacket, false);
                    }
                    else
                    {
                        if (!SendPacket(setPacket, 10, 7)) UpdateAckStatus(cPacket, false);
                    }
                    return string.Format("Sent Set Point Group = {0} to {1}/{2}, Data : {3}", iSPGroupdID, setPacket.cFleetId, setPacket.iVehicleId, Util.EncodeByteArrayAsHex(dummy));
                }
                #endregion

                //send out a diagnostics level
                GeneralGPPacket diagPacket = new GeneralGPPacket(cPacket, _serverTime_DateFormat);
                diagPacket.cMsgType = GeneralGPPacket.DIAGNOSTIC_LEVEL;
                diagPacket.DiagnosticsLevel = mDatabase.GetVehicleDiagnosticsLevel(cPacket);
                diagPacket.Encode(ref dummy);
                SendPacket(diagPacket, 1, 60);

                // If we get here then the unit has sent in that it is running the correct versions
                // Config, Set Point and Route points.  We should now update the database with the config versions on the unit.
                errMsg = mDatabase.UpdateVehicleCurrentlyRunning(cPacket);

                // Ack the unit to let it know we are done.
                UpdateAckStatus(cPacket, false);

                // Send a message back to the client.
                if (errMsg != null) return errMsg;
                return "Configuration for Fleet " + cPacket.cFleetId + ", Unit " + cPacket.iVehicleId +
                    " is in line. (Not scheduled)";
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessConfigTotalPacket(ConfigTotalGPPacket cPacket)", ex);
            }
            return null;
        }


        private string ProcessConfigSetPointPolygonSegmentRequest(ConfigNewSetPtGPPacket cPacket)
        {
            string errMsg = "";
            byte[] dummy = null;


            try
            {
                if (cPacket.cFleetId == 0)
                {
                    //POD - No need for this as it wont find a vehicle entry.
                    //UpdateAckStatus(cPacket, false);
                    return "Will not process a setpoint segment request from an unidentified unit. (fleet is 0)";
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessConfigSetPointPolygonSegmentRequest(ConfigNewSetPtGPPacket cPacket)", ex);
            }

            // We should send a CONFIG_NEW_SETPT_POLYGON to the unit
            ConfigNewSetPtGPPacket setPacket = new ConfigNewSetPtGPPacket(cPacket, _serverTime_DateFormat, 1);

            int iSPGroupdID = 0;
            setPacket.cSetPointSetNumber = cPacket.cSetPointSetActive;
            if ((errMsg = mDatabase.GetSetPointGroup(ref setPacket, ref  iSPGroupdID, 1, false)) != null)
            {
                // Couldn't find it or had a problem getting it 
                // - let the unit stay with what it's on:
                UpdateAckStatus(cPacket, false);
                return errMsg;
            }

            #region Encode and send the packet to the unit
            setPacket.Encode(ref dummy, cPacket.cMsgType, cPacket.cSetPointSetSegmentRequest, cPacket.ProtocolVer);			//polygon encode with the segment requested
            if ((int)cPacket.ProtocolVer > 1)
            {
                #region Send the segment with retries
                if (!SendPacket(setPacket, 5, 10))
                    UpdateAckStatus(cPacket, false);
                #endregion
            }
            else
            {
                #region Send the packet with no retries (Units with protocol v1 do not send acks)
                if (!SendPacket(setPacket, 0, 15))
                    UpdateAckStatus(cPacket, false); // Try up to twice, 60 sec apart
                #endregion
            }
            #endregion
            return "Sent Set Point Segment by Request GroupID:" + iSPGroupdID + " SegmentNo:" + cPacket.cSetPointSetSegmentRequest + " to  " + Convert.ToString(setPacket.cFleetId).PadLeft(3, '0') + "/" + Convert.ToString(setPacket.iVehicleId).PadLeft(4, '0');
        }

        private string ProcessSystemPacket(GatewayProtocolPacket gPacket)
        {
            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            bool bUseLastKnownPacket = false;
            // Check that we know this type of message
            try
            {
                Vehicle vehicle = GetVehicle(gPacket);
                if ((gPacket.cMsgType >= GatewayProtocolPacket.SYSTEM_SETUP) &&
                    (gPacket.cMsgType <= GatewayProtocolPacket.SYSTEM_RESET))
                {
                    if (gPacket.cMsgType == GeneralGPPacket.GEN_NEW_NETWORK_FOUND)
                    {
                        _log.Info("New Network Found by Unit " + Convert.ToString((int)gPacket.cFleetId) + "/" + Convert.ToString(gPacket.iVehicleId) + " : " + BitConverter.ToString(gPacket.mRawBytes, 0));
                        return null;
                    }
                    if (gPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
                    {
                        if (gPacket.mDataField != null && gPacket.mDataField.Length == 1)
                        {
                            if (vehicle != null)
                            {
                                if (vehicle.MostRecentLiveUpdatePacket != null)
                                {
                                    try
                                    {
                                        oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(vehicle.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                                        bUseLastKnownPacket = true;
                                    }
                                    catch (Exception)
                                    {
                                        oGP = null;
                                        bUseLastKnownPacket = false;
                                    }
                                }

                                if (!bUseLastKnownPacket)
                                {
                                    #region If there was no last packet for this unit, create a new one

                                    oGP = new MTData.Transport.Gateway.Packet.GeneralGPPacket(_serverTime_DateFormat);
                                    oGP.cFleetId = gPacket.cFleetId;
                                    oGP.iVehicleId = gPacket.iVehicleId;
                                    oGP.mFix.dLatitude = 1;
                                    oGP.mFix.dLongitude = 1;
                                    #endregion
                                }

                                #region Set the time and location values on the unit

                                oGP.cMsgType = gPacket.cMsgType;
                                oGP.cButtonNumber = (byte)0x00;
                                oGP.cInputNumber = gPacket.mDataField[0];
                                DateTime dtValue = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute, DateTime.Now.Second, 0).ToUniversalTime();
                                oGP.mFixClock = new MTData.Transport.Gateway.Packet.GPClock("", dtValue, _serverTime_DateFormat);
                                oGP.mCurrentClock = oGP.mFixClock.CreateCopy();
                                oGP.mDataField = gPacket.mDataField;
                                #endregion

                                #region Update the database and send the live update
                                vehicle.MostRecentLiveUpdatePacket = oGP;
                                string sRet = mDatabase.InsertNotificationRecord(oGP, vehicle);
                                gPacket = oGP;
                                if (sRet != null)
                                    _log.Info(sRet);
                                #endregion
                            }
                            return null;
                        }
                    }
                    else
                    {
                        return mDatabase.InsertSystemErrorRecord(gPacket);
                    }
                }
                else
                {
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessSystemPacket(GatewayProtocolPacket gPacket)", ex);
            }
            return "Unknown System Message Type";
        }
        private string ProcessHistoryPacket(GPSHistoryGPPacket hPacket)
        {
            Vehicle v = null;
            try
            {
                v = mSendThreads.GetVehicle(hPacket);
                if (hPacket.mStatusList.cSpareStatus == 2 || hPacket.IsExtendedHistoryComplete)
                {
                    //extended history complete packet
                    v.ExtendedHistoryComplete();

                    //fire live update
                    return null;
                }
                else
                {
                    if (hPacket.mStatusList.cSpareStatus == 1)
                    {
                        RequestExtendedHistory request = v.GetNextExtendedHistoryRequest();
                        if (request != null)
                        {
                            request.LastHistoryPacketReceived = DateTime.UtcNow;
                        }
                    }
                    else if (hPacket.mStatusList.cSpareStatus == 0)
                    {
                        _log.Debug("INCIDENT_EVENT :: New incident event received. VehicleID:" + hPacket.iVehicleId + ". DateTime:" + hPacket.mClockList.ToDateTime());

                        //incident event, check counters for possible accident
                        v.NewIncidentRecieved(hPacket.mClockList.ToDateTime());
                    }
                        
                    return mDatabase.InsertHistoryRecords(hPacket, v);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessHistoryPacket(GPSHistoryGPPacket hPacket)", ex);
            }
            return "Unknown History Message Type";
        }

        private string ProcessMailPacket(MailMessagePacket mPacket)
        {
            byte[] dummy = null;
            MailMessagePacket rePacket = null;
            try
            {
                rePacket = new MailMessagePacket(mPacket, _serverTime_DateFormat);
                rePacket.sSubject = "Re: " + mPacket.sSubject;
                rePacket.sTargetUser = "DATSlistener";
                rePacket.sMessage = "Got your message";
                rePacket.Encode(ref dummy);
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessMailPacket(MailMessagePacket mPacket)", ex);
            }
            //	SendPacket(rePacket, 0);
            return null;
        }

        /// <summary>
        /// Process Data will ensure that the packet and vehicle are lept in sync.
        /// The returned string is a status message. If it is null, then no status message.
        /// If there is an error, an PacketDatabaseInterfaceException will be thrown
        /// </summary>
        /// <param name="aPacket"></param>
        /// <returns></returns>
        public object ProcessData(GatewayProtocolPacket aPacket, List<GatewayAdministrator.DownloadEntry> currentDownloads, 
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> queuedDownloads)
        {
            bool remoteUpdateResetSent = false;
            Vehicle vehicle = null;
            GeneralGPPacket gpPacket = null;
            RoutePtSetPtGPPacket pointPacket = null;
            ConfigTotalGPPacket confPacket = null;
            DriverPointsRuleRequest dr = null;
            RoutingModuleGPPacket routePacket = null;
            ConfigWatchList wlPacket = null;
            GPSHistoryGPPacket hstPacket = null;
            GPDriverDataAccumulator ddaPacket = null;
            GPDriverPerformanceMetrics dpmPacket = null;
            DriverPointsRuleBreakPacket dp = null;
            MassDeclarationPacket mass = null;

            try
            {
                #region Check for packets that are log and return processing (GSP_APP_PING, SYSTEM_UNIT_RESET, DRIVER_POINTS_CONFIG_ERROR)
                var ignore = _ignorePackets.FirstOrDefault(v => v.Fleet == aPacket.cFleetId && v.VehicleId == aPacket.iVehicleId);
                if (ignore != null && ignore.ReasonIds.Contains(aPacket.cMsgType))
                {
                    //ack the packet
                    vehicle = UpdateAckStatus(aPacket, false);
                    return string.Format("Ignore Packet from fleet {0}, unit {1}, Reason Code {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.cMsgType);
                }
                else if (aPacket.cMsgType == GatewayProtocolPacket.GSP_APP_PING)
                {
                    SendPacket(aPacket, 0);
                    return string.Format("GSP_APP_PING received from fleet {0}, unit {1}, ipAddress {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString());
                }
                else if (aPacket.cMsgType == GatewayProtocolPacket.SYSTEM_UNIT_RESET)
                {
                    #region If this is a system reset packet, log the data to file.
                    vehicle = UpdateAckStatus(aPacket, false);

                    if ((aPacket.mDataField != null) && (aPacket.mDataField.Length >= 1))
                        return string.Format("SYSTEM_UNIT_RESET received from fleet {0}, unit {1}, ipAddress {2}, Reason {3}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString(), aPacket.mDataField[0]);
                    return string.Format("SYSTEM_UNIT_RESET received from fleet {0}, unit {1}, ipAddress {2}, Reason <NONE>", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString());
                    #endregion
                }
                else if (aPacket.cMsgType == DriverPointsConfigError.DRIVER_POINTS_CONFIG_ERROR)
                {
                    #region If this is a driver points config error, log to file and return
                    vehicle = UpdateAckStatus(aPacket, false);
                    DriverPointsConfigError errorPacket = new DriverPointsConfigError(aPacket, _serverTime_DateFormat);
                    return string.Format("DRIVER_POINTS_CONFIG_ERROR received from fleet {0}, unit {1}, ipAddress {2}, Reason {3}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString(), errorPacket.Error);
                    #endregion
                }
                else if (aPacket.cMsgType == ConfigNetworkInfoGPPacket.CONFIG_NETWORK_INFO)
                {
                    #region If the unit is about to go to another server
                    vehicle = UpdateAckStatus(aPacket, false);
                    return string.Format("Recieved Ack Change of IP Address packet from fleet {0}, unit {1}, ipAddress {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString());
                    #endregion
                }
                else if (aPacket.cMsgType == GeneralGPPacket.GEN_NEW_NETWORK_FOUND)
                {
                    #region If the unit has found a new mobile phone network
                    vehicle = UpdateAckStatus(aPacket, false);
                    return string.Format("Recieved GEN_NEW_NETWORK_FOUND packet from fleet {0}, unit {1}, ipAddress {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString());
                    #endregion
                }
                #endregion
                #region If we are restricting the server to respond only to a certain fleet, check the fleet.
                if (bRespondToFleets)
                    if (aPacket.cFleetId != cFleetToRespondTo)
                        return null;
                #endregion
                #region Logging for Pendent Alarm pakets
                if (aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM || aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST || aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED)
                {
                    switch (aPacket.cMsgType)
                    {
                        case GeneralGPPacket.PENDENT_ALARM:
                            _log.Info("F/V " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId) + " - Recieved Pendant alarm packet");
                            break;
                        case GeneralGPPacket.PENDENT_ALARM_TEST:
                            _log.Info("F/V " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId) + " - Recieved Pendant test alarm packet");
                            break;
                        case GeneralGPPacket.PENDENT_ALARM_CLEARED:
                            _log.Info("F/V " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId) + " - Recieved Pendant alarm cleared packet");
                            break;
                    }
                }
                #endregion
                #region If this is a GEN_ACK, GEN_NAK, CONFIG_TOTAL_ACTIVE, ROUTES_UPDATE_ACCEPTED set any waiting acks as being acked.
                if (aPacket.cMsgType == GeneralGPPacket.GEN_ACK ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_NAK ||
                    aPacket.cMsgType == ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE ||
                    aPacket.cMsgType == RoutingModuleGPPacket.ROUTES_UPDATE_ACCEPTED)
                    PacketAcked(aPacket);
                #endregion
                #region Add this vehicle to the vehicle collection, if this is a GEN_IM_ALIVE, GEN_IM_ALIVE_IMEI_ESN, GEN_IM_ALIVE_FULL or CONFIG_TOTAL_ACTIVE, don't send an ack
                // Make changes to the record of sequence numbers we've got from
                // this unit
                if ((aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE) ||
                    (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN) ||
                    (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_FULL) ||
                    (aPacket.cMsgType == ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE))
                {
                    if (aPacket.bSendFlash)
                        vehicle = UpdateAckStatus(aPacket, false);
                    else
                        vehicle = UpdateAckStatus(aPacket, true);
                }
                else
                {
                    vehicle = UpdateAckStatus(aPacket, false);
                }
                #endregion
                #region If a vehicle was found or created, check if we have had and ack or if we have data to send to the unit.
                if (vehicle != null)
                {
                    if (aPacket.cMsgType == GeneralGPPacket.GEN_ACK || aPacket.cMsgType == GeneralGPPacket.GEN_NAK)
                    {
                        #region If we have a script file or firmware login reply waiting and this is an GEN_ACK, then clear the resend data.
                        if (vehicle.oSendScriptFilePacket != null)
                            vehicle.oSendScriptFilePacket = null;
                        else if (vehicle.oFirmwareLoginReply != null)
                            vehicle.oFirmwareLoginReply = null;
                        #endregion
                    }
                    else if (aPacket.cMsgType == GPSendScriptFile.FIRMWARE_SEND_SHELL_SCRIPT)
                    {
                        #region If this is a FIRMWARE_SEND_SHELL_SCRIPT request, send the script to the unit and return
                        GPSendScriptFile segmentRequest = new GPSendScriptFile(aPacket, _serverTime_DateFormat);
                        if (segmentRequest.SubCommand == 1)
                        {
                            _log.Info("Recieved segment request from " + segmentRequest.cFleetId + "/" + segmentRequest.iVehicleId + " for file " + segmentRequest.FileId + " segment " + segmentRequest.SegmentNumber);
                            if (_sendFileList.Count > segmentRequest.FileId)
                            {
                                GPSendScriptFile segment = _sendFileList[segmentRequest.FileId];
                                segment.SubCommand = 2;
                                segment.SegmentNumber = segment.SegmentNumber;
                                vehicle.SendOnce(segment);
                                remoteUpdateResetSent = true;
                            }
                        }
                        if (segmentRequest.SubCommand == 2)
                        {
                            _log.Info("Recieved cancel download from " + segmentRequest.cFleetId + "/" + segmentRequest.iVehicleId + " for file " + segmentRequest.FileId + " segment " + segmentRequest.SegmentNumber);
                            vehicle.oSendScriptFilePacket = null;
                        }
                        return segmentRequest;
                        #endregion
                    }


                    if (_remoteUpdateEngine.IsResetRequired(Convert.ToInt32(vehicle.cFleet), vehicle.iUnit))
                    {
                        #region If we are required to send a reset to this unit
                        //	If it needs a reset, but is uploading a GPS History, do not reset it at this time.
                        if (aPacket.cMsgType != GPSHistoryGPPacket.GPS_HISTORY && aPacket.cMsgType != ConfigWatchList.CONFIG_WATCH_LIST)
                        {
                            SendResetRequest(vehicle, aPacket);
                            remoteUpdateResetSent = true;
                            _remoteUpdateEngine.ClearResetRequired(Convert.ToInt32(vehicle.cFleet), vehicle.iUnit);
                            _log.Info(string.Format("Sent reset request to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                        }
                        #endregion
                    }

                    #region If we have a config check, watch list packet, firmware login reply or script to send AND there is not data comming from flash, send them.
                    if ((vehicle.bNeedsConfigCheck || vehicle.oWatchListPacket != null || vehicle.oFirmwareLoginReply != null || vehicle.oSendScriptFilePacket != null)
                        && vehicle.iUnit > 0
                        && aPacket.cMsgType != GeneralGPPacket.GEN_IM_ALIVE_FULL
                        && aPacket.cMsgType != GeneralGPPacket.SYSTEM_GPRS_FAILURE
                        && (aPacket.cMsgType & 0xF0) != ConfigTotalGPPacket.CONFIG_PACKET_MASK
                        && !aPacket.bArchivalData
                        && !aPacket._receivedFlashAvailable)
                    {
                        if (vehicle.oWatchListPacket != null)
                        {
                            #region if this vehicle has a watch list packet waiting to be sent, then send it to the vehicle.
                            SendWatchListPacketToUnit(aPacket.cFleetId, aPacket.iVehicleId, vehicle);
                            remoteUpdateResetSent = true;
                            _log.Info(string.Format("Sent Watch List packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                            #endregion
                        }
                        else if (vehicle.oFirmwareLoginReply != null)
                        {
                            vehicle.SendOnce(vehicle.oFirmwareLoginReply);
                            remoteUpdateResetSent = true;
                            _log.Info(string.Format("Sent Firmware Login Reply packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                        }
                        else if (vehicle.oSendScriptFilePacket != null && !aPacket.bArchivalData && !aPacket._receivedFlashAvailable)
                        {
                            vehicle.SendOnce(vehicle.oSendScriptFilePacket);
                            remoteUpdateResetSent = true;
                            _log.Info(string.Format("Sent Script packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                        }
                        else if (vehicle.bNeedsConfigCheck)
                        {
                            if ((currentDownloads == null || currentDownloads.FindAll(cd => cd.FleetID == vehicle.cFleet && cd.VehicleID == vehicle.iUnit).Count == 0)
                                && (queuedDownloads == null || queuedDownloads.FindAll(qd => qd.FleetID == vehicle.cFleet && qd.VehicleId == vehicle.iUnit).Count == 0))
                            {
                                SendConfigurationCheck(vehicle, aPacket);
                                remoteUpdateResetSent = true;
                                _log.Info(string.Format("Sent Configuration Check packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                            }
                            else
                            {
                                _log.Info(string.Format("Cannot Send Configuration Check packet to fleet {0} unit {1} as it is downloading", vehicle.cFleet, vehicle.iUnit));
                            }
                        }
                        else if (vehicle.bHasMail)
                        {
                            SendMailPacket(vehicle, aPacket, vehicle.MailMessage);
                            remoteUpdateResetSent = true;
                            _log.Info(string.Format("Sent Mail packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                        }
                    }
                    #endregion

                    #region Accident Unload Request and Extended History request
                    if (vehicle.AccidentRequestPending && (aPacket.cMsgType & 0xF0) != ConfigTotalGPPacket.CONFIG_PACKET_MASK)
                    {
                        SendAccidentUploadRequest(vehicle, aPacket, vehicle.AccidentGetNextBufferRequest());
                        remoteUpdateResetSent = true;
                        _log.Info(string.Format("Sent Accident Unload Request packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit));
                    }
                    else if ((aPacket.cMsgType & 0xF0) != ConfigTotalGPPacket.CONFIG_PACKET_MASK)
                    {
                        RequestExtendedHistory request = vehicle.GetNextExtendedHistoryRequest();
                        if (request != null)
                        {
                            //check if request has been sent and that we haven't receieved any history for an hour
                            if (request.RequestSentDate.HasValue)
                            {
                                DateTime lastPacket = request.RequestSentDate.Value;
                                if (request.LastHistoryPacketReceived.HasValue)
                                {
                                    lastPacket = request.LastHistoryPacketReceived.Value;
                                }
                                if (lastPacket.AddHours(1) < DateTime.UtcNow)
                                {
                                    //complete this request has the unit hasn't sent us any data for the last hour
                                    vehicle.ExtendedHistoryComplete();
                                    //get the next request
                                    request = vehicle.GetNextExtendedHistoryRequest();
                                }
                            }

                            if (request != null & !request.RequestSentDate.HasValue)
                            {
                                SendExtendedHistoryRequest(vehicle, aPacket, request);
                                remoteUpdateResetSent = true;
                                _log.Info(string.Format("Sent Extended History Request {2} packet to fleet {0} unit {1}", vehicle.cFleet, vehicle.iUnit, request.Id));
                            }
                        }
                    }
                    #endregion
                }
                #endregion
                #region switch on packet type
                // We have to do different things depending on what data we just got given

                int iMessageType = aPacket.cMsgType;

                //switch (aPacket.cMsgType & 0xF0)
                switch (iMessageType & 0xF0)
                {
                    case GeneralGPPacket.GEN_NO_GPS_PACKET_MASK:
                    case GeneralGPPacket.GEN_PACKET_MASK:
                    case GeneralGPPacket.STATUS_PACKET_MASK:
                    case GeneralGPPacket.ENGINE_PACKET_MASK:
                    case GeneralGPPacket.BARREL_PACKET_MASK:
                    case GeneralGPPacket.EXTENDED_IO_PACKET_MASK:
                    case GeneralGPPacket.ZONE_ALERT_MESSAGES:
                    case GeneralGPPacket.STATUS_VEHICLE_PACKET_MASK:
                        #region General Packet Processing
                        gpPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                        if (gpPacket.bIsPopulated)
                        {
                            return ProcessGenPacket(vehicle, gpPacket);
                        }
                        return gpPacket.sConstructorDecodeError;
                        #endregion
                    case RoutePtSetPtGPPacket.SETPT_PACKET_MASK:
                        #region Setpoint packet processing
                        pointPacket = new RoutePtSetPtGPPacket(aPacket, _serverTime_DateFormat);
                        if (pointPacket.bIsPopulated)
                        {
                            UpdateInputStatus(pointPacket);
                            return ProcessPointPacket(pointPacket);
                        }
                        return pointPacket.sConstructorDecodeError;
                        #endregion
                    case RoutingModuleGPPacket.ROUTES_PACKET_MASK:
                        #region Route point packet processing
                        routePacket = new RoutingModuleGPPacket(aPacket, _serverTime_DateFormat);
                        if (routePacket.bIsPopulated)
                        {
                            UpdateInputStatus(routePacket);
                            return ProcessRoutingPacket(routePacket);
                        }
                        return routePacket.sConstructorDecodeError;
                        #endregion
                    case ConfigTotalGPPacket.CONFIG_PACKET_MASK:
                        #region Config packet processing
                        if (!remoteUpdateResetSent)
                        {
                            if (aPacket.cMsgType == ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE)
                            {
                                #region Config file processing
                                _log.Info(string.Format("Recieved CONFIG_TOTAL_ACTIVE from fleet {0}, unit {1}, ipAddress {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString()));
                                confPacket = new ConfigTotalGPPacket(aPacket, _serverTime_DateFormat);
                                if (confPacket.bIsPopulated)
                                {
                                    return ProcessConfigTotalPacket(confPacket);
                                }
                                return confPacket.sConstructorDecodeError;
                                #endregion
                            }
                            else if (aPacket.cMsgType == ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON || aPacket.cMsgType == ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON_OLD)
                            {
                                #region Set point config request processing
                                if (aPacket.cMsgType == ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON)
                                    _log.Info(string.Format("Recieved CONFIG_NEW_SETPT_POLYGON from fleet {0}, unit {1}, ipAddress {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString()));
                                else
                                    _log.Info(string.Format("Recieved CONFIG_NEW_SETPT_POLYGON_OLD from fleet {0}, unit {1}, ipAddress {2}", aPacket.cFleetId, aPacket.iVehicleId, aPacket.mSenderIP.ToString()));
                                ConfigNewSetPtGPPacket SetPointPolygonSegment = new ConfigNewSetPtGPPacket(aPacket, _serverTime_DateFormat, (byte)0, (byte)0);
                                ProcessConfigSetPointPolygonSegmentRequest(SetPointPolygonSegment);
                                return "Config SetPoint Polygon Segment:" + SetPointPolygonSegment.cSetPointSetSegmentRequest + " Requested";
                                #endregion
                            }
                            else if (aPacket.cMsgType == DriverPointsRuleRequest.DRIVER_POINTS_RULE_REQUEST)
                            {
                                #region Driver Point Rule Request processing
                                dr = new DriverPointsRuleRequest(aPacket, _serverTime_DateFormat);
                                if (dr.bIsPopulated)
                                {
                                    return ProcessDriverPointsRuleRequest(dr);
                                }
                                return dr.ConstructorDecodeError;
                                #endregion
                            }
                        }
                        break;
                        #endregion
                    case GatewayProtocolPacket.SYSTEM_PACKET_MASK:
                        #region Process system packets
                        return ProcessSystemPacket(aPacket);
                        #endregion
                    case GPSHistoryGPPacket.GPS_HISTORY_PACKET_MASK:
                        if (aPacket.cMsgType == ConfigWatchList.CONFIG_WATCH_LIST)
                        {
                            #region Check the unit is in the correct config watch list state
                            if (((int)aPacket.cFleetId) > 0 && aPacket.iVehicleId > 0)
                            {
                                // Check if the unit has the correct watch list state.
                                wlPacket = new ConfigWatchList(aPacket, _serverTime_DateFormat);
                                ConfigWatchList toUnitPacket = mDatabase.GetWatchListState(aPacket.cFleetId, aPacket.iVehicleId);
                                if (toUnitPacket == null)
                                {
                                    toUnitPacket = new ConfigWatchList(aPacket, _serverTime_DateFormat);
                                    toUnitPacket.AddToWatchList = false;
                                }
                                // If the states do not match, send the state change packet to the unit, then setup to perform a config check on the next report.
                                if (wlPacket.AddToWatchList != toUnitPacket.AddToWatchList)
                                {
                                    toUnitPacket.Encode();
                                    vehicle = GetVehicle(aPacket);
                                    if (vehicle != null && vehicle.cFleet == aPacket.cFleetId && vehicle.iUnit == aPacket.iVehicleId)
                                    {
                                        vehicle.SendOnce(toUnitPacket);
                                        vehicle.bNeedsConfigCheck = true;
                                        return "Incorrect Watch List State - Sending state change packet. To Unit -> " + BitConverter.ToString(toUnitPacket.mRawBytes) + ", From Unit, configCheck set to true";
                                    }
                                    else
                                        return "Unable to change watch list state for F/V " + ((int)aPacket.cFleetId).ToString() + "/" + aPacket.iVehicleId.ToString();
                                }
                            }
                            else
                                return "Received CONFIG_WATCH_LIST from fleet 0 vehicle 0 F/V " + ((int)aPacket.cFleetId).ToString() + "/" + aPacket.iVehicleId.ToString() + " Data : " + BitConverter.ToString(aPacket.mRawBytes);

                            return "Received CONFIG_WATCH_LIST from F/V " + ((int)aPacket.cFleetId).ToString() + "/" + aPacket.iVehicleId.ToString() + " - Unit is in correct watchlist state.";
                            #endregion
                        }
                        if (aPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY)
                        {
                            #region Process GPS History packets
                            hstPacket = new GPSHistoryGPPacket(aPacket, _serverTime_DateFormat);
                            return ProcessHistoryPacket(hstPacket);
                            #endregion
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_DATAACCUMLATOR_METRICS)
                        {
                            #region Process driver data accumulator metrics packet
                            ddaPacket = new GPDriverDataAccumulator(aPacket, _serverTime_DateFormat);
                            return mDatabase.InsertDriverDataAccumlatorRecord(ddaPacket);
                            #endregion
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_PERFORMANCE_METRICS)
                        {
                            #region Process driver performance metrics packet
                            dpmPacket = new GPDriverPerformanceMetrics(aPacket, _serverTime_DateFormat);
                            return mDatabase.InsertDriverPerformanceMetricRecord(dpmPacket);
                            #endregion
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT
                            || aPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS
                            || aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMIE
                            || aPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT
                            || aPacket.cMsgType == GeneralGPPacket.GEN_TAMPER
                            || aPacket.cMsgType == GeneralGPPacket.GEN_ECM_OVERSPEED
                            || aPacket.cMsgType == GeneralGPPacket.LOW_POWER_MODE)
                        {
                            #region General Packet Processing
                            gpPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                            if (gpPacket.bIsPopulated)
                            {
                                if (aPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                                {
                                    if (gpPacket.GetPosition().cSpeed > _ignoreOverSpeedPacketsSpeed)
                                    {
                                        return ProcessGenPacket(vehicle, gpPacket);
                                    }
                                    else
                                    {
                                        return string.Format("Ignore Overspeed as speed {0} is less than {1}", gpPacket.GetPosition().cSpeed, _ignoreOverSpeedPacketsSpeed);
                                    }
                                }
                                else
                                {
                                    return ProcessGenPacket(vehicle, gpPacket);
                                }
                            }
                            return gpPacket.sConstructorDecodeError;
                            #endregion
                        }
                        return "Unknown Message Type";
                    case MassDeclarationPacket.MASS_PACKET_MASK:
                        if (aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START || aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END ||
                            aPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY || aPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR ||
                            aPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION || aPacket.cMsgType == GeneralGPPacket.GPS_SETTINGS ||
                            aPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC_LEVEL || aPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC ||
                            aPacket.cMsgType == GeneralGPPacket.POSSIBLE_ACCIDENT || aPacket.cMsgType == GeneralGPPacket.TAMPER)
                        {
                            #region General Packet Processing
                            gpPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                            if (gpPacket.bIsPopulated)
                            {
                                return ProcessGenPacket(vehicle, gpPacket);
                            }
                            return gpPacket.sConstructorDecodeError;
                            #endregion
                        }
                        else if (aPacket.cMsgType == DriverPointsRuleBreakPacket.DRIVER_POINTS_RULE_BROKEN)
                        {
                            #region Driver Point Rule Break Processing
                            dp = new DriverPointsRuleBreakPacket(aPacket, _serverTime_DateFormat);
                            if (dp.bIsPopulated)
                            {
                                return ProcessDriverPointsRuleBreak(vehicle, dp);
                            }
                            return dp.ConstructorDecodeError;
                            #endregion
                        }
                        else if (aPacket.cMsgType == GenericPacket.GENERIC_PACKET)
                        {
                            #region Genric packet Processing
                            GenericPacket gp = new GenericPacket(aPacket, _serverTime_DateFormat);
                            if (gp.bIsPopulated)
                            {
                                return ProcessGenericPacket(vehicle, gp);
                            }
                            return dp.ConstructorDecodeError;
                            #endregion
                        }
                        else if (aPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION)
                        {
                            #region Mass delaration processing
                            mass = new MassDeclarationPacket(aPacket, _serverTime_DateFormat);
                            if (mass.bIsPopulated)
                            {
                                return ProcessMassDeclarationPacket(vehicle, mass);
                            }
                            else
                            {
                                return mass.ConstructorDecodeError;
                            }
                            #endregion
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.GEN_CONFIG_DOWNLOADED)
                        {
                            ConfigDownloaded configDownload = new ConfigDownloaded();
                            configDownload.Decode(aPacket.mDataField);
                            if (configDownload.ItemCount > 0)
                            {
                                System.Text.StringBuilder builder = new System.Text.StringBuilder();
                                bool first = true;
                                foreach (Tuple<string, string> t in configDownload.ListValues)
                                {
                                    if (first)
                                    {
                                        builder.AppendFormat("{0}={1}", t.Item1, t.Item2);
                                        first = false;
                                    }
                                    else
                                    {
                                        builder.AppendFormat(", {0}={1}", t.Item1, t.Item2);
                                    }
                                }
                                return builder.ToString();
                            }
                            return null;
                        }
                        return "Unknown Message Type";
                    default:
                        return "Unknown Message Type";
                }
                #endregion

            }
            catch (Exception e)
            {
                _log.Error(this.GetType().FullName + ".ProcessData(GatewayProtocolPacket aPacket, SerializedToMsmq addressProcessing)", e);
                return this.GetType().FullName + ".ProcessData(GatewayProtocolPacket aPacket, SerializedToMsmq addressProcessing)\r\nStack : " + e.StackTrace + "\r\nInner Exception : " + e.InnerException + "\r\nError : " + e.Message;
            }
            return null;
        }

        private string ProcessMassDeclarationPacket(Vehicle vehicle, MassDeclarationPacket massPacket)
        {
            try
            {
                if (vehicle.cFleet > 0 && vehicle.iUnit > 0)
                {
                    if (oLoggedInUnits == null)
                        oLoggedInUnits = Hashtable.Synchronized(new Hashtable());

                    lock (oLoggedInUnits.SyncRoot)
                    {
                        if (!oLoggedInUnits.ContainsKey(vehicle.GetHashCode()))
                        {
                            bool bRestoreState = false;
                            lock (alRestoreMDTStatusForFleet.SyncRoot)
                                if (alRestoreMDTStatusForFleet.Contains(vehicle.cFleet))
                                    bRestoreState = true;
                            if (bRestoreState)
                            {
                                mDatabase.UpdateRestoreStoreStatus(vehicle);
                            }
                            oLoggedInUnits.Add(vehicle.GetHashCode(), null);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessMassDeclarationPacket(Vehicle vehicle, MassDeclarationPacket massPacket)", ex);
            }


            if (vehicle == null)
                return "ProcessMassDeclarationPacket: Could not find Vehicle object for fleet: " + massPacket.cFleetId + " unit: " + massPacket.iVehicleId;

            //	NOTE : POD 18/05/2006 - This is where the remoteUdpate engine
            //	is consulted to see if a vehicle has successfully been reset or not.
            //	It also indicates whether updates from the vehicle should be ignored or not.
            //	RULE : If this is a GPS History, ignore the setitng in the remote state engine.
            bool processPacket = _remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(massPacket.cFleetId), massPacket.iVehicleId);
            if (!processPacket)
            {
                return string.Format("Fleet {0}, Vehicle {1} - Packet Ignored by RemoteRequest", Convert.ToInt32(massPacket.cFleetId), massPacket.iVehicleId);
            }
            else
            {
                try
                {
                    if (iAdjustForGPSBounceWhenSpeedBelow > 0)
                    {
                        #region If the speed > 0 and < 5 natical miles and the ignition is off, zero the speed values.
                        if (massPacket != null)
                        {
                            int iSpeed = 0;
                            int iInputStatus = 0;

                            if (massPacket.mFix != null)
                            {
                                iSpeed = Convert.ToInt32(massPacket.mFix.cSpeed);
                            }
                            if (massPacket.mStatus != null)
                            {
                                iInputStatus = Convert.ToInt32(massPacket.mStatus.cInputStatus);
                            }
                            if (iSpeed < iAdjustForGPSBounceWhenSpeedBelow && (iInputStatus & 1) != 1)
                            {
                                if (massPacket.mFix != null) massPacket.mFix.cSpeed = (byte)0x00;
                                if (massPacket.mDistance != null) massPacket.mDistance.cMaxSpeed = (byte)0x00;
                            }
                        }
                        #endregion
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessMassDeclarationPacket(Vehicle vehicle, MassDeclarationPacket massPacket)", ex);
                }
                try
                {
                    UpdateInputStatus(massPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessMassDeclarationPacket(Vehicle vehicle, MassDeclarationPacket massPacket)", ex);
                }
                try
                {
                    UpdatePositionStatus(massPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessMassDeclarationPacket(Vehicle vehicle, MassDeclarationPacket massPacket)", ex);
                }

                //update the total mass on the vehicle
                vehicle.TotalMass = massPacket.TotalMass;

                //save the mass packet to the database
                string error = mDatabase.InsertMassDeclaration(massPacket);
                bool schemeError = false;
                if (error != null && error.Equals("SchemeError"))
                {
                    schemeError = true;
                    error = null;
                }
                if (error == null)
                {
                    //add the packet to the vehicle notification table
                    GeneralGPPacket oPacket = new GeneralGPPacket((GatewayProtocolPacket)massPacket, _serverTime_DateFormat);
                    error = mDatabase.InsertNotificationRecord(oPacket, vehicle);
                    if (error == null)
                    {
                        if (schemeError || (massPacket.MassSchemeItemID <= 0 && vehicle.WeightLimit > 0 && vehicle.WeightLimit < massPacket.TotalMass))
                        {
                            _log.Info("Mass declaration rule break, preparing for driver notification.");
                            oPacket.cButtonNumber = (byte)0x01;
                            DateTime dtTemp = oPacket.mFixClock.ToDateTime();
                            dtTemp = dtTemp.AddSeconds(1);
                            oPacket.mFixClock.FromDateTime(dtTemp);
                            dtTemp = oPacket.mCurrentClock.ToDateTime();
                            dtTemp = dtTemp.AddSeconds(1);
                            oPacket.mCurrentClock.FromDateTime(dtTemp);
                            var rule = vehicle.GetTrackingRule(oPacket);
                            if (rule != null)
                            {
                                _log.Info($"Mass declaration rule break, rule id is: {rule.Id}");
                                var ruleBreakPacket = oPacket.CreateCopy();
                                error = mDatabase.InsertDriverPointsRuleBreak(ruleBreakPacket, vehicle, true, rule.Id);
                                if (error == null)
                                {
                                    error = mDatabase.InsertNotificationRecord(oPacket, vehicle);
                                    GeneralGPPacket oLiveUpdatePacket = ((GeneralGPPacket) oPacket).CreateCopy();
                                    vehicle.AddAdditionalLiveUpdate(oLiveUpdatePacket);
                                }
                                else
                                {
                                    _log.Info($"Mass declaration rule break, error: {error}, during InsertDriverPointsRuleBreak : {rule.Id}");
                                }
                            }
                            else
                            {
                                _log.Info("Mass declaration rule break, unable to find the rule id.");
                            }
                        }
                    }
                }
                else
                {
                    _log.Info("No record inserted into the VehicleNotification because Insert Mass Declaration failed: " + error);
                }
                return error;
            }
        }


        private string ProcessDriverPointsRuleBreak(Vehicle vehicle, DriverPointsRuleBreakPacket dpRuleBreakPacket)
        {
            try
            {
                if (vehicle.cFleet > 0 && vehicle.iUnit > 0)
                {
                    if (oLoggedInUnits == null)
                        oLoggedInUnits = Hashtable.Synchronized(new Hashtable());

                    lock (oLoggedInUnits.SyncRoot)
                    {
                        if (!oLoggedInUnits.ContainsKey(vehicle.GetHashCode()))
                        {
                            bool bRestoreState = false;
                            lock (alRestoreMDTStatusForFleet.SyncRoot)
                                if (alRestoreMDTStatusForFleet.Contains(vehicle.cFleet))
                                    bRestoreState = true;
                            if (bRestoreState)
                            {
                                mDatabase.UpdateRestoreStoreStatus(vehicle);
                            }
                            oLoggedInUnits.Add(vehicle.GetHashCode(), null);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".ProcessDriverPointsRuleBreak(Vehicle vehicle, DriverPointsRuleBreakPacket dpRuleBreakPacket)", ex);
            }

            if (vehicle == null)
                return "ProcessDriverPointsRuleBreak: Could not find Vehicle object for fleet: " + dpRuleBreakPacket.cFleetId + " unit: " + dpRuleBreakPacket.iVehicleId;

            //	NOTE : POD 18/05/2006 - This is where the remoteUdpate engine
            //	is consulted to see if a vehicle has successfully been reset or not.
            //	It also indicates whether updates from the vehicle should be ignored or not.
            //	RULE : If this is a GPS History, ignore the setitng in the remote state engine.
            bool processPacket = _remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(dpRuleBreakPacket.cFleetId), dpRuleBreakPacket.iVehicleId);
            if (!processPacket)
            {
                return string.Format("Fleet {0}, Vehicle {1} - Packet Ignored by RemoteRequest", Convert.ToInt32(dpRuleBreakPacket.cFleetId), dpRuleBreakPacket.iVehicleId);
            }
            else
            {
                try
                {
                    if (iAdjustForGPSBounceWhenSpeedBelow > 0)
                    {
                        #region If the speed > 0 and < 5 natical miles and the ignition is off, zero the speed values.
                        if (dpRuleBreakPacket != null)
                        {
                            int iSpeed = 0;
                            int iInputStatus = 0;

                            if (dpRuleBreakPacket.mFix != null)
                            {
                                iSpeed = Convert.ToInt32(dpRuleBreakPacket.mFix.cSpeed);
                            }
                            if (dpRuleBreakPacket.mStatus != null)
                            {
                                iInputStatus = Convert.ToInt32(dpRuleBreakPacket.mStatus.cInputStatus);
                            }
                            if (iSpeed < iAdjustForGPSBounceWhenSpeedBelow && (iInputStatus & 1) != 1)
                            {
                                if (dpRuleBreakPacket.mFix != null) dpRuleBreakPacket.mFix.cSpeed = (byte)0x00;
                                if (dpRuleBreakPacket.mDistance != null) dpRuleBreakPacket.mDistance.cMaxSpeed = (byte)0x00;
                            }
                        }
                        #endregion
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessDriverPointsRuleBreak(Vehicle vehicle, DriverPointsRuleBreakPacket dpRuleBreakPacket)", ex);
                }
                try
                {
                    UpdateInputStatus(dpRuleBreakPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessDriverPointsRuleBreak(Vehicle vehicle, DriverPointsRuleBreakPacket dpRuleBreakPacket)", ex);
                }
                try
                {
                    UpdatePositionStatus(dpRuleBreakPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessDriverPointsRuleBreak(Vehicle vehicle, DriverPointsRuleBreakPacket dpRuleBreakPacket)", ex);
                }

                //save the mass packet to the database
                string error = mDatabase.InsertDriverPointsRuleBreak(dpRuleBreakPacket, vehicle);
                if (error != null)
                {
                    _log.Info("No record inserted into the VehicleNotification because InsertDriverPointsRuleBreak failed: " + error);
                }
                return error;
            }
        }

        private string ProcessDriverPointsRuleRequest(DriverPointsRuleRequest dpRuleRequestPacket)
        {
            //get the config which this vehicle should be running
            ConfigDriverPointsGPPacket configPacket = new ConfigDriverPointsGPPacket(_serverTime_DateFormat);
            configPacket.PacketVer = dpRuleRequestPacket.PacketVersion;
            configPacket.cFleetId = dpRuleRequestPacket.cFleetId;
            configPacket.iVehicleId = dpRuleRequestPacket.iVehicleId;

            mDatabase.GetDriverPointsConfig(ref configPacket, dpRuleRequestPacket.cFleetId, dpRuleRequestPacket.iVehicleId);
            byte[] dummy = new byte[1];
            configPacket.Encode(ref dummy);

            //compare the config with what the vehicle is running
            if (configPacket.GroupID != dpRuleRequestPacket.RuleGroupId ||
                configPacket.GroupVerMajor != dpRuleRequestPacket.VersionMajor ||
                configPacket.GroupVerMinor != dpRuleRequestPacket.VersionMinor)
            {
                SendPacket(configPacket, 0);
            }

            //update what is running on the vehicle
            Vehicle oVehicle = mSendThreads.GetVehicle(dpRuleRequestPacket.cFleetId, dpRuleRequestPacket.iVehicleId);
            bool updated = false;
            string error = mDatabase.UpdateDriverPointsCurrentlyRunning(dpRuleRequestPacket, eDriverPointsCurrentRuleGroupChanged, ref updated);
            if (updated)
            {
                oVehicle.UpdateServerTrackingRules();
            }

            return error;
        }

        private string ProcessGenericPacket(Vehicle vehicle, GenericPacket genericPacket)
        {
            try
            {
                if (vehicle.cFleet > 0 && vehicle.iUnit > 0)
                {
                    if (oLoggedInUnits == null)
                        oLoggedInUnits = Hashtable.Synchronized(new Hashtable());

                    lock (oLoggedInUnits.SyncRoot)
                    {
                        if (!oLoggedInUnits.ContainsKey(vehicle.GetHashCode()))
                        {
                            bool bRestoreState = false;
                            lock (alRestoreMDTStatusForFleet.SyncRoot)
                                if (alRestoreMDTStatusForFleet.Contains(vehicle.cFleet))
                                    bRestoreState = true;
                            if (bRestoreState)
                            {
                                mDatabase.UpdateRestoreStoreStatus(vehicle);
                            }
                            oLoggedInUnits.Add(vehicle.GetHashCode(), null);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error("ProcessGenericPacket", ex);
            }

            if (vehicle == null)
                return "ProcessGenericPacket: Could not find Vehicle object for fleet: " + genericPacket.cFleetId + " unit: " + genericPacket.iVehicleId;

            //	NOTE : POD 18/05/2006 - This is where the remoteUdpate engine
            //	is consulted to see if a vehicle has successfully been reset or not.
            //	It also indicates whether updates from the vehicle should be ignored or not.
            //	RULE : If this is a GPS History, ignore the setitng in the remote state engine.
            bool processPacket = _remoteUpdateEngine.CheckPacketValidity(Convert.ToInt32(genericPacket.cFleetId), genericPacket.iVehicleId);
            if (!processPacket)
            {
                return string.Format("Fleet {0}, Vehicle {1} - Packet Ignored by RemoteRequest", Convert.ToInt32(genericPacket.cFleetId), genericPacket.iVehicleId);
            }
            else
            {
                try
                {
                    if (iAdjustForGPSBounceWhenSpeedBelow > 0)
                    {
                        #region If the speed > 0 and < 5 natical miles and the ignition is off, zero the speed values.
                        if (genericPacket != null)
                        {
                            int iSpeed = 0;
                            int iInputStatus = 0;

                            if (genericPacket.mFix != null)
                            {
                                iSpeed = Convert.ToInt32(genericPacket.mFix.cSpeed);
                            }
                            if (genericPacket.mStatus != null)
                            {
                                iInputStatus = Convert.ToInt32(genericPacket.mStatus.cInputStatus);
                            }
                            if (iSpeed < iAdjustForGPSBounceWhenSpeedBelow && (iInputStatus & 1) != 1)
                            {
                                if (genericPacket.mFix != null) genericPacket.mFix.cSpeed = (byte)0x00;
                                if (genericPacket.mDistance != null) genericPacket.mDistance.cMaxSpeed = (byte)0x00;
                            }
                        }
                        #endregion
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessGenericPacket()", ex);
                }
                try
                {
                    UpdateInputStatus(genericPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessGenericPacket()", ex);
                }
                try
                {
                    UpdatePositionStatus(genericPacket);
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".ProcessGenericPacket()", ex);
                }

                //save the mass packet to the database
                string error = mDatabase.InsertGenericPacket(genericPacket, vehicle);
                if (error != null)
                {
                    _log.Info("No record inserted into the VehicleNotification because InsertGenericPacket failed: " + error);
                }
                return error;
            }
        }

        private void mSendThreads_eConsoleEvent(string sMsg)
        {
            _log.Info("DatabaseInterface -> " + sMsg);
        }

        private void mSendThreads_eConcreteStatusChanged(int iFleetID, byte[] bData)
        {
            if (eConcreteStatusChanged != null) eConcreteStatusChanged(iFleetID, bData);
        }

        /// <summary>
        /// CreateVehicle a vehicle to be used within the system.
        /// This can be added to the list or not, depending on the vehicle/fleetid
        /// </summary>
        /// <param name="aPacket"></param>
        /// <param name="isInboundPacket"></param>
        /// <param name="transitioning"></param>
        /// <returns></returns>
        private Vehicle CreateVehicle(GatewayProtocolPacket aPacket, bool isInboundPacket, bool transitioning)
        {
            Vehicle result = mSendThreads.CreateVehicle(aPacket, isInboundPacket, transitioning);
            result.StationaryMinutesThreshold = this.StationaryMinuteThreshold;
            result.PositionThreshold = this.ConstantPositionThreshold;
            return result;
        }


        /// <summary>
        /// Add a pre-prepared vehicle to the list.
        /// </summary>
        /// <param name="vehicle"></param>
        /// <returns></returns>
        private Vehicle AddVehicle(Vehicle vehicle)
        {
            if ((vehicle.cFleet != 0) && (vehicle.iUnit != 0))
                mSendThreads.Add(vehicle);
            return vehicle;
        }

        /// <summary>
        /// Add a vehicle to the list of vehicles.
        /// </summary>
        /// <param name="aPacket"></param>
        /// <param name="isInboundPacket"></param>
        /// <param name="transitioning"></param>
        /// <returns></returns>
        private Vehicle AddVehicle(GatewayProtocolPacket aPacket, bool isInboundPacket, bool transitioning)
        {
            return AddVehicle(CreateVehicle(aPacket, isInboundPacket, transitioning));
        }
        #endregion

        /// <summary>
        /// This method allows external influences to refresh the data used by the system.
        /// </summary>
        public void RefreshVehicleData(string sReason)
        {
            _log.Info("Refreshing Datasets on client " + sReason + " - Full forced reload.");
            mDatabase.ForceRefreshDataSets();
        }

        public void RefreshVehicleDataSet()
        {
            _log.Info("Refreshing Vehicle Dataset");
            mDatabase.RefreshVehicleDataSet(mDatabase.GetOpenConnection());
        }

        /// <summary>
        /// This method allows external influences to refresh the data used by the system.
        /// </summary>
        public System.Data.DataSet GetTrailerESNs()
        {
            return mDatabase.GetTrailerESNs();
        }

        #region ECM Live Update Handlers
        private object ECMLiveUpdateThreadHandler(EnhancedThread sender, object data)
        {
            while (!sender.Stopping)
            {
                if (eSendECMLiveUpdate == null)
                    Thread.Sleep(100);
                else
                {
                    try
                    {
                        object[] oData = null;
                        lock (mECMLiveUpdateQ.SyncRoot)
                            if (mECMLiveUpdateQ.Count > 0)
                                oData = (object[])mECMLiveUpdateQ.Dequeue();

                        if (oData != null && !sender.Stopping)
                            eSendECMLiveUpdate((int)oData[0], (int)oData[1], (DateTime)oData[2], (string)oData[3], (float)oData[4], (float)oData[5], (GPECMAdvancedItem[])oData[6]);
                        else if (!sender.Stopping)
                            Thread.Sleep(100);
                    }
                    catch (Exception ex)
                    {
                        _log.Error(sClassName + "ECMLiveUpdateThreadHandler(EnhancedThread sender, object data)", ex);
                    }
                }
            }
            return null;
        }

        private void mDatabase_eSendECMLiveUpdate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues)
        {
            try
            {
                if (mECMLiveUpdateQ != null && mECMLiveUpdateThread != null && !mECMLiveUpdateThread.Stopping)
                    lock (mECMLiveUpdateQ.SyncRoot)
                        mECMLiveUpdateQ.Enqueue(new object[] { fleetId, vehicleId, dGPSTime, _location, _latitude, _longitude, oECMValues });
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "mDatabase_eSendECMLiveUpdate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues)", ex);
            }
        }
        #endregion
        #region Path to select route update data.
        public System.Data.DataRow GetRouteUpdateValues(long tableId)
        {
            return mDatabase.GetRouteUpdateValues(tableId);
        }
        #endregion
        #region Path to select upcomming driver breaks.
        public System.Data.DataTable GetNextDriverBreaks(int driverId)
        {
            return mDatabase.GetNextDriverBreaks(driverId);
        }
        #endregion
        public object ResendPendantAlerts(EnhancedThread sender, object data)
        {
            #region Write thread start message to log
            try
            {
                _log.Info("Pendant Alert Resend thread started.");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ResendPendantAlerts(EnhancedThread sender, object data)", ex);
            }
            #endregion
            int iCounter = 0;

            while (!sender.Stopping)
            {
                #region Thread Loop Code
                try
                {
                    iCounter++;
                    if (iCounter >= 20)
                    {
                        if (eResendPendantAlarm != null)
                        {
                            foreach (var item in mDatabase.PendentActiveAlarmList.GetUnacknowledgedAlarms())
                            {
                                if (item.LastResend == DateTime.MinValue)
                                    item.LastResend = DateTime.Now;
                                TimeSpan ts = DateTime.Now.Subtract(item.LastResend);
                                if (ts.TotalMinutes > 5)
                                {
                                    item.LastResend = DateTime.Now;
                                    Vehicle v = mSendThreads.GetVehicle(item.Fleet, item.VehicleId);
                                    if (v == null)
                                    {
                                        if (mDatabase.GetVehicleRow(item.Fleet, item.VehicleId) != null)
                                        {
                                            // If the vehicle has not talked to the server yet, create an object, the update will be sent once the unit reports in.
                                            v = new Vehicle(item.Fleet, item.VehicleId, null, ref mDatabase, _serverTime_DateFormat);
                                            mSendThreads.Add(v);
                                        }
                                    }
                                    if (item.AlarmPacket == null)
                                    {
                                        item.AlarmPacket = mDatabase.LoadVehicleDataFromHistory((int)item.Fleet, (int)item.VehicleId, item.AlarmTime, v);
                                        item.AlarmPacket.bIsPopulated = true;

                                    }
                                    eResendPendantAlarm(item.AlarmPacket, v);
                                }
                            }
                        }
                        iCounter = 0;
                    }
                    Thread.Sleep(1000);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "ResendPendantAlerts(EnhancedThread sender, object data)", ex);
                }
                #endregion
            }
            #region Write thread stopped message to log
            try
            {
                _log.Info("Pendant Alert Resend thread stopped.");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ResendPendantAlerts(EnhancedThread sender, object data)", ex);
            }
            #endregion
            return null;
        }

        public void UpdateExtendedHistoryRequests(int fleetId, int vehicleId)
        {
            //update the database cache
            mDatabase.RefreshExtendedHistoryDataSet(null);
            //update teh copy in the vehicle object
            Vehicle v = mSendThreads.GetVehicle((byte)fleetId, (uint)vehicleId);
            if (v != null)
            {
                v.UpdateExtendedHistoryRequests();
            }
        }

        public GatewayProtocolPacket SetStationaryAlertToActive(DataRow row)
        {
            GatewayProtocolPacket liveUpdate = null;
            //need to find vehicle
            int fleet = Convert.ToInt32(row["FleetId"]);
            int vehicle = Convert.ToInt32(row["VehicleId"]);
            Vehicle v = Vehicles.GetVehicle((byte)fleet, (uint)vehicle);
            if (v != null)
            {
                //usage found - create a end event
                GenericPacket newPacket = new GenericPacket(_serverTime_DateFormat);
                if (v.MostRecentLiveUpdatePacket != null)
                {
                    try
                    {
                        GeneralGPPacket oGP = new GeneralGPPacket(v.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                        newPacket.UpdateFromGeneralGPPacket(oGP);
                    }
                    catch (Exception exp)
                    {
                        if (v.MostRecentLiveUpdatePacket.mRawBytes == null)
                        {
                            _log.ErrorFormat("SetStationaryAlertToActive - Error decoding most recentLiveUpdate for {0}/{1}, exception '{2}' - packet null", fleet, vehicle, exp.Message);
                        }
                        else
                        {
                            _log.ErrorFormat("SetStationaryAlertToActive - Error decoding most recentLiveUpdate for {0}/{1}, exception '{3}' - packet {2}", fleet, vehicle, BitConverter.ToString(v.MostRecentLiveUpdatePacket.mRawBytes), exp.Message);
                        }
                        newPacket.Payload = new byte[0];
                    }
                }
                else
                {
                    newPacket.Payload = new byte[0];
                }
                DateTime end = Convert.ToDateTime(row["ScheduledStartTime"]);
                newPacket.mCurrentClock.FromDateTime(end);
                newPacket.mFixClock.FromDateTime(end);
                newPacket.SubCommand = GenericSubCommands.StationaryAlertStart;
                byte[] dummy = null;
                newPacket.Encode(ref dummy);
                v.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_STATIONARY_ALERT;
                mDatabase.StationaryAlertActive(Convert.ToInt32(row["ID"]));

                mDatabase.InsertGenericPacket(newPacket, v);
                liveUpdate = newPacket;
            }
            return liveUpdate;
        }

        public GatewayProtocolPacket EndOutOfHoursUsage(DataRow row)
        {
            GatewayProtocolPacket liveUpdate = null;
            //need to find vehicle
            int fleet = Convert.ToInt32(row["FleetId"]);
            int vehicle = Convert.ToInt32(row["VehicleId"]);
            Vehicle v = Vehicles.GetVehicle((byte)fleet, (uint)vehicle);
            if (v != null)
            {
                //usage found - create a end event
                GenericPacket newPacket = new GenericPacket(_serverTime_DateFormat);
                if (v.MostRecentLiveUpdatePacket != null)
                {
                    GeneralGPPacket oGP = new GeneralGPPacket(v.MostRecentLiveUpdatePacket, _serverTime_DateFormat);
                    newPacket.UpdateFromGeneralGPPacket(oGP);
                }
                else
                {
                    newPacket.Payload = new byte[0];
                }
                DateTime end = Convert.ToDateTime(row["ScheduledEndTime"]);
                newPacket.mCurrentClock.FromDateTime(end);
                newPacket.mFixClock.FromDateTime(end);
                newPacket.SubCommand = GenericSubCommands.OutOfHoursUsageEnd;
                byte[] dummy = null;
                newPacket.Encode(ref dummy);
                v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_OUT_OF_HOURS;
                mDatabase.EndActiveOutOfHoursUsage(fleet, vehicle);

                mDatabase.InsertGenericPacket(newPacket, v);
                liveUpdate = newPacket;
            }
            return liveUpdate;
        }

        private void SendPossibleAccidentLiveUpdate(Vehicle vehicle, GeneralGPPacket packet)
        {
            _log.DebugFormat("INCIDENT_EVENT :: received live update event for Vehicle {0}.{1}, removing live update", vehicle.cFleet, vehicle.iUnit);
            vehicle.SendPossibleAccidentLiveUpdate -= new Vehicle.SendPossibleAccidentLiveUpdateDelegate(SendPossibleAccidentLiveUpdate);
            if (eSendLiveUpdate != null && packet != null)
            {
                _log.Debug("INCIDENT_EVENT :: have live update packet forwarding it on");
                eSendLiveUpdate(packet);
            }
        }

        /// <summary>
        /// Create a tracking event for the DVR alarm or DVR request.
        /// </summary>
        /// <param name="fleetId">The id of the fleet</param>
        /// <param name="vehicleId">The id of the vehicle</param>
        /// <param name="deviceId">The id of the dvr device</param>
        /// <param name="eventType">The type of event. 0: Alarm, 1: Requested Video</param>
        /// <param name="eventId">The id of the event. If type is alarm then value is alarmId. If type is requested video then value is requestId.</param>
        /// <param name="date">The date of the event</param>
        /// <param name="latitude">The latitude location of the event</param>
        /// <param name="longitude">The longitude location of the event</param>
        public void CreateDvrEventRecord(int fleetId, int vehicleId, int deviceId, int eventType, int eventId, DateTime date, decimal latitude, decimal longitude)
        {
            Vehicle vehicle = mSendThreads.GetVehicle((byte)fleetId, (uint)vehicleId);
            if (vehicle != null)
            {
                GenericPacket packet = CreateDvrRecord(vehicle, date, latitude, longitude, eventType == 1 ? GenericSubCommands.DvrRequest : GenericSubCommands.DvrAlarm);
                if (packet != null)
                {
                    packet.DvrDeviceId = deviceId;
                    packet.DvrEventType = eventType;
                    packet.DvrEventId = eventId;

                    string result = mDatabase.InsertGenericPacket(packet, vehicle);
                    if (result == null && eSendLiveUpdate != null)
                    {
                        eSendLiveUpdate(packet);
                    }
                }
            }
            else
            {
                _log.ErrorFormat("CreateDvrEventRecord :: Unable to create DVR event as listener has not heard from vehicle {0}.{1}", fleetId, vehicleId);
            }
        }

        /// <summary>
        /// Create a tracking event for the DVR error.
        /// </summary>
        /// <param name="fleetId">The id of the fleet</param>
        /// <param name="vehicleId">The id of the vehicle</param>
        /// <param name="device">The name of the dvr device</param>
        /// <param name="timestamp">The time of the error report</param>
        /// <param name="latitude">The latitude location of the error report</param>
        /// <param name="longitude">The longitude location of the error report</param>
        /// <param name="errorCode">The error code</param>
        public void CreateDvrErrorRecord(int fleetId, int vehicleId, string device, DateTime timestamp, decimal latitude, decimal longitude, short errorCode)
        {
            Vehicle vehicle = mSendThreads.GetVehicle((byte)fleetId, (uint)vehicleId);
            if (vehicle != null)
            {
                GenericPacket packet = CreateDvrRecord(vehicle, timestamp, latitude, longitude, GenericSubCommands.DvrError, errorCode);
                if (packet != null) { 
                    // Set the version number to -1 to indicate this isn't a status entry and just an error
                    packet.DvrStatusVersion = -1;

                    packet.DvrStatusDeviceName = device;
                    packet.DvrErrorCode = errorCode;

                    string result = mDatabase.InsertGenericPacket(packet, vehicle);
                    if (result == null && eSendLiveUpdate != null)
                    {
                        eSendLiveUpdate(packet);
                    }
                }
            }
            else
            {
                _log.ErrorFormat("CreateDvrErrorRecord :: Unable to create DVR event as listener has not heard from vehicle {0}.{1}", fleetId, vehicleId);
            }
        }

        /// <summary>
        /// Create a tracking event for the DVR status.
        /// </summary>
        /// <param name="fleetId">The id of the fleet</param>
        /// <param name="vehicleId">The id of the vehicle</param>
        /// <param name="device">The name of the dvr device</param>
        /// <param name="timestamp">The time of the status report</param>
        /// <param name="latitude">The latitude location of the status report</param>
        /// <param name="longitude">The longitude location of the status report</param>
        /// <param name="errorCode">Current status error code</param>
        /// <param name="isHealthStatus">Whether the status is a health status with associated images</param>
        /// <param name="connected">Whether the DVR device is connected to the 4000</param>
        /// <param name="recordOn">Whether the DVR is recording</param>
        /// <param name="hddOk">Whether the DVR HDD is valid</param>
        /// <param name="hddSize">The HDD size in GB</param>
        /// <param name="hddFree">The amount of free HDD space in GB</param>
        /// <param name="videoConnectedChannel">The videon connected channels</param>
        /// <param name="videoRecordChannel">The video recording channels</param>
        /// <param name="timeOffset">The time offset between the 4000 and DVR device</param>
        /// <param name="firmwareVersion">The DVR firmware version</param>
        /// <param name="model">The name of the DVR model</param>
        /// <param name="videoFormat">The video format (PAL/NTSC)</param>
        public void CreateDvrStatusRecord(int fleetId, int vehicleId, string device, DateTime timestamp, decimal latitude, decimal longitude, short errorCode, bool isHealthStatus, bool connected, bool recordOn, bool hddOk,
            short hddSize, short hddFree, short videoConnectedChannel, short videoRecordChannel, int timeOffset, string firmwareVersion, string model, string videoFormat)
        {
            Vehicle vehicle = mSendThreads.GetVehicle((byte)fleetId, (uint)vehicleId);
            if (vehicle != null)
            {
                GenericPacket packet = CreateDvrRecord(vehicle, timestamp, latitude, longitude, GenericSubCommands.DvrStatus, errorCode);
                if (packet != null)
                {
                    // Set the version number of the status packet
                    packet.DvrStatusVersion = 1;

                    // Indicates whether the status is from tracking or a health status with associated still images
                    packet.DvrStatusIsHealthStatus = isHealthStatus;

                    packet.DvrErrorCode = errorCode;
                    packet.DvrStatusConnected = connected;
                    packet.DvrStatusRecording = recordOn;
                    packet.DvrStatusHddOk = hddOk;
                    packet.DvrStatusHddSize = hddSize;
                    packet.DvrStatusHddFree = hddFree;
                    packet.DvrStatusVideoConnectedChannel = videoConnectedChannel;
                    packet.DvrStatusVideoRecordChannel = videoRecordChannel;
                    packet.DvrStatusTimeOffset = timeOffset;
                    packet.DvrStatusDeviceName = device;
                    packet.DvrStatusFirmwareVersion = firmwareVersion;
                    packet.DvrStatusModel = model;
                    packet.DvrStatusVideoFormat = videoFormat;

                    string result = mDatabase.InsertGenericPacket(packet, vehicle);
                    if (result == null && eSendLiveUpdate != null)
                    {
                        eSendLiveUpdate(packet);
                    }
                }
            }
            else
            {
                _log.ErrorFormat("CreateDvrStatusRecord :: Unable to create DVR status as listener has not heard from vehicle {0}.{1}", fleetId, vehicleId);
            }
        }

        /// <summary>
        /// Create a GenericPacket for the DVR record using the latitude and longitude. If the location isn't provided then use the last know location based on the timestamp
        /// </summary>
        private GenericPacket CreateDvrRecord(Vehicle vehicle, DateTime timestamp, decimal latitude, decimal longitude, GenericSubCommands subCommand, short? errorCode = null)
        {
            GenericPacket packet = null;

            if ((latitude != 0) && (longitude != 0))
            {
                // Create the generic packet based off provided location
                packet = new GenericPacket(_serverTime_DateFormat);
                packet.cFleetId = vehicle.cFleet;
                packet.iVehicleId = vehicle.iUnit;
                packet.mFix.dLatitude = latitude;
                packet.mFix.dLongitude = longitude;
                packet.Payload = new byte[0];
            }
            else
            {
                _log.InfoFormat("CreateDvrRecord :: No location provided, retrieving location from last known location for vehicle {0}.{1} at time {2}", vehicle.cFleet, vehicle.iUnit, timestamp);

                GeneralGPPacket oGP = mDatabase.LoadVehicleDataFromHistory(vehicle.cFleet, (int)vehicle.iUnit, timestamp, vehicle);
                if (oGP != null)
                {
                    // Create the generic packet based off last known location
                    packet = new GenericPacket(_serverTime_DateFormat);
                    packet.cFleetId = vehicle.cFleet;
                    packet.iVehicleId = vehicle.iUnit;
                    packet.mFix.dLatitude = oGP.mFix.dLatitude;
                    packet.mFix.dLongitude = oGP.mFix.dLongitude;

                    // Setting the direction to 30000 will result in the location being defined as inaccurate
                    packet.mFix.iDirection = 30000;

                    packet.Payload = new byte[0];
                }
                else
                {
                    _log.ErrorFormat("CreateDvrRecord :: Unable to create DVR record as no history packet found for vehicle {0}.{1} at time {2}", vehicle.cFleet, vehicle.iUnit, timestamp);
                }
            }

            if (packet != null)
            {
                packet.cMsgType = GenericPacket.GENERIC_PACKET;
                packet.SubCommand = subCommand;

                // Update the sub command based on the DVR error code
                if (errorCode.HasValue && errorCode.Value > 0)
                {
                    packet.ParseDvrErrorCode(errorCode.Value);
                }

                packet.mFixClock = new GPClock("", timestamp, _serverTime_DateFormat);
                packet.mCurrentClock = new GPClock("", timestamp, _serverTime_DateFormat);

                byte[] dummy = new byte[0];
                packet.Encode(ref dummy);
            }

            return packet;
        }
    }
}

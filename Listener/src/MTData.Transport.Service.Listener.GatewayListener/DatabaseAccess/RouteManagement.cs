using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections;
using log4net;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Route.Monitor;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
	/// <summary>
	/// This class will handle all of the route management requirements.
	/// </summary>
	public class RouteManagement : PacketSource.IPacketSource, IDisposable
	{
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.Database_Access.RouteManagement";
        private static ILog _log = LogManager.GetLogger(typeof(RouteManagement));

		/// <summary>
		/// This is the lookahead interval for routes.
		/// </summary>
		private int _lookAheadIntervalHrs = 24;

		/// <summary>
		/// This is a list of the pending downloads.
		/// Some of these will be updates, some will be deletes.
		/// </summary>
		private DataSet _pendingVehicleScheduleDownloads = null;

		/// <summary>
		/// Synchronisation object
		/// </summary>
		private object _syncRoot = new object();

		/// <summary>
		/// Connection string to use for storing off details.
		/// </summary>
		private string _connectionString = null;

		/// <summary>
		/// This will monitor the on/off route status of the unit.
		/// </summary>
		private RouteMonitor _routeMonitor = null;

		/// <summary>
		/// Indicates wheter route information should be logged or not.
		/// </summary>
		private bool _logRouteData = false;

		private const string ROUTELOG_FORMAT = "RouteManagement : {0}";

		/// <summary>
		/// This will hold a cache of the records that have yet to be processed by the routing module for the purposes
		/// of OnRoute and OffRoute
		/// </summary>
		private ChronologicalCache _packetCache = new ChronologicalCache();

		private enum VehicleScheduleRowState
		{
			Pending = 0,
			Executing = 1,
			Complete = 2,
			Incomplete = 3,
			Cancelled = 4
		}

        private int _routeProcessingCount;
        private long _routeProcessingTime;
        private object _routeProcessingLock = new object();

        public void GetStats(ref long time, ref int count)
        {
            lock (_routeProcessingLock)
            {
                time = _routeProcessingTime;
                count = _routeProcessingCount;
                _routeProcessingCount = 0;
                _routeProcessingTime = 0;
            }
        }

        #region Context

        public class EnvelopeContext
		{
			private DataRow _row = null;
			private uint _downloadedScheduleID = 0;

			public DataRow Row { get{ return _row; }}
			public uint DownloadedScheduleID {get{ return _downloadedScheduleID; }}

			public EnvelopeContext(DataRow row, uint downloadedScheduleID)
			{
				_row = row;
				_downloadedScheduleID = downloadedScheduleID;
			}
		}

		#endregion

		/// <summary>
		/// Prepare and configure the class instance
		/// </summary>
		/// <param name="logger"></param>
		/// <param name="lookAheadIntervalHrs"></param>
		/// <param name="connectionString"></param>
        public RouteManagement(int lookAheadIntervalHrs, string connectionString)
		{
			string temp = System.Configuration.ConfigurationManager.AppSettings["LogRouteData"];
			if (temp != null)
				_logRouteData = Boolean.Parse(temp);

			_lookAheadIntervalHrs = lookAheadIntervalHrs;
			_connectionString = connectionString;
			_routeMonitor = new RouteMonitor(connectionString);
			if (_logRouteData)
				_log.Info(string.Format(ROUTELOG_FORMAT, "Initialised"));
		}

		/// <summary>
		/// Load up a list of the active Vehicle Route Schedules that need to be downloaded to the vehicles.
		/// </summary>
		/// <param name="connection"></param>
		public void ReloadDataSets(SqlConnection connection)
		{
			try
			{
				if (_logRouteData)
					_log.Info(string.Format(ROUTELOG_FORMAT, "ReloadDatasets"));
				SqlCommand command = new SqlCommand("usp_Route2VehicleScheduleGetPending", connection);
				try
				{
					command.CommandType = CommandType.StoredProcedure;

					SqlParameter parameter = command.Parameters.Add("@UTCReferenceStartDate", SqlDbType.DateTime);
					parameter.Value = DateTime.UtcNow.AddHours(_lookAheadIntervalHrs);

			
					SqlDataAdapter adapter = new SqlDataAdapter();
					try
					{
						adapter.SelectCommand = command;
			
						DataSet pending = new DataSet();
						adapter.Fill(pending);

						lock(_syncRoot)
							_pendingVehicleScheduleDownloads = pending;
						if (_logRouteData)
							_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("ReloadDataSets : RowCount {0}", _pendingVehicleScheduleDownloads.Tables[0].Rows.Count)));

					}
					finally
					{
						adapter.Dispose();
					}
				}
				finally
				{
					command.Dispose();
				}
			}
			catch(Exception ex)
			{
                _log.Error(sClassName + "ReloadDataSets(SqlConnection connection)", ex);
			}
		}
		#region IPacketSource Members

		public string Name
		{
			get
			{
				return "RouteManagement";
			}
		}

		public MTData.Transport.Service.Listener.GatewayListener.PacketSource.PacketPriority Priority
		{
			get
			{
				return MTData.Transport.Service.Listener.GatewayListener.PacketSource.PacketPriority.Med;
			}
		}

		public MTData.Transport.Service.Listener.GatewayListener.PacketSource.IPacketEnvelope GetPendingPacketForUnit(int fleetID, int unitID)
		{
			PacketSource.PacketEnvelopeStandard result = null;
			if (_pendingVehicleScheduleDownloads != null)
			{
				try
				{
					lock(_syncRoot)
					{
						DataRow[] pending = _pendingVehicleScheduleDownloads.Tables[0].Select(string.Format("FleetID = {0} AND VehicleID = {1}", fleetID, unitID), "UTCStartDate ASC");

						if ((pending != null) && (pending.Length > 0))
						{
							if (_logRouteData)
								_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("GetPendingPacketForUnit : Unit {0},{1} : RowCount {2}", fleetID, unitID, pending.Length)));

							SqlConnection connection = new SqlConnection(_connectionString);
							connection.Open();
							try
							{
								bool userCancellation = (Convert.ToInt32(pending[0]["State"]) == 5);
								bool deleteRoute = (userCancellation || (Convert.ToInt32(pending[0]["Deleted"]) == 1));

								if (_logRouteData)
									_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("GetPendingPacketForUnit : Unit {0},{1} : VehicleScheduleID {2}, DeleteRoute {3}, UserCancellation {4}", fleetID, unitID, pending[0]["ID"], deleteRoute, userCancellation)));

								//	Clone the appropriate route..
								uint cloneID = 0;
								if ((deleteRoute) && (!pending[0].IsNull("DownloadedRouteScheduleID")))
									cloneID = Convert.ToUInt32(pending[0]["DownloadedRouteScheduleID"]);
								else
								{
									cloneID = CreateClonedRouteSchedule(connection, (int)pending[0]["RouteScheduleID"]);
									UpdateRouteScheduleID(connection, (int)pending[0]["ID"], cloneID);
								}

								if (_logRouteData)
									_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("GetPendingPacketForUnit : Unit {0},{1} : VehicleScheduleID {2}, CloneID {3}", fleetID, unitID, pending[0]["ID"], cloneID)));

								CheckPointCacheEntry checkPoints = null;
								if (!deleteRoute)
									checkPoints = GetRouteScheduleCheckPoints(connection, cloneID);

								if (_logRouteData)
									_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("GetPendingPacketForUnit : Unit {0},{1} : VehicleScheduleID {2}, CheckPointCount {3}", fleetID, unitID, pending[0]["ID"], (checkPoints != null)?checkPoints.CheckPoints.Tables[0].Rows.Count.ToString():"Null" )));

								//	Create a packet out of the first row.
								ConfigRouteUpdateGPPacket packet = CreateOutboundPacket(fleetID, unitID, pending[0], (checkPoints != null)?checkPoints.CheckPoints:null, deleteRoute);

								EnvelopeContext envelopeContext = new EnvelopeContext(pending[0], cloneID);
								result = new MTData.Transport.Service.Listener.GatewayListener.PacketSource.PacketEnvelopeStandard(
									envelopeContext,
									fleetID,
									unitID,
									packet,
									this,
									3,
									10);

								lock(_transmittedEntries)
									_transmittedEntries[packet.VehicleRouteScheduleID] = envelopeContext;
							}
							finally
							{
								connection.Close();
							}
						}
						else
						{
							//	flush out the detals here if nothign else required.
							if (IsCheckPointFlushDue())
								CheckPointFlush();
						}
					}
				}
				catch(Exception ex)
				{
					_log.Error(sClassName + "GetPendingPacketForUnit(int fleetID = " + fleetID + ", int unitID = " + unitID + ")", ex);
				}
			}
			return result;
		}

		public void PacketAcked(MTData.Transport.Service.Listener.GatewayListener.PacketSource.IPacketEnvelope envelope)
		{
			try
			{
				if (_logRouteData)
					_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("PacketAcked : Unit {0},{1}", envelope.FleetID, envelope.UnitID)));

				//	if this packet was an update, and the list now says deleted, then we may have an issue..
				//	only update the row if it matches the original one.
				EnvelopeContext context = (EnvelopeContext)envelope.Context;

				ConfigRouteUpdateGPPacket packet = (ConfigRouteUpdateGPPacket)envelope.Packet;

				if (_logRouteData)
					_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("PacketAcked : Unit {0},{1} : CloneID {2}, VehicleScheduleID {3}, PacketType {4}", envelope.FleetID, envelope.UnitID, context.DownloadedScheduleID, packet.VehicleRouteScheduleID, packet.Action)));

				UpdateDownloadedStateForRow(context.Row, (packet.Action == ConfigRouteUpdateGPPacket.RouteUpdateAction.Delete)?4:(int)context.Row["State"], context.DownloadedScheduleID);

				lock(_transmittedEntries)
					_transmittedEntries.Remove(packet.VehicleRouteScheduleID);
			}
			catch(Exception ex)
			{
				_log.Error(sClassName + "PacketAcked(MTData.Transport.Service.Listener.GatewayListener.PacketSource.IPacketEnvelope envelope)", ex);
			}
		}

		public void PacketAborted(MTData.Transport.Service.Listener.GatewayListener.PacketSource.IPacketEnvelope envelope)
		{
			try
			{
				ConfigRouteUpdateGPPacket packet = (ConfigRouteUpdateGPPacket)envelope.Packet;

				if (_logRouteData)
					_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("PacketAborted : Unit {0},{1} : VehicleScheduleID {2}, PacketType {3}", envelope.FleetID, envelope.UnitID, packet.VehicleRouteScheduleID, packet.Action)));

				lock(_transmittedEntries)
					_transmittedEntries.Remove(packet.VehicleRouteScheduleID);
			}
			catch(Exception ex)
			{
				_log.Error(sClassName + "PacketAborted(MTData.Transport.Service.Listener.GatewayListener.PacketSource.IPacketEnvelope envelope)", ex);
			}
		}

		#endregion

		#region RouteSchedule CheckPoints

		private Hashtable _checkPointCache = new Hashtable();

		#region RouteSchedule CheckPoints Cache Entry

		private class CheckPointCacheEntry : IDisposable
		{
			private uint _cloneID;
			private DataSet _checkPoints = null;
			private DateTime _lastAccessed = DateTime.UtcNow;

			public CheckPointCacheEntry(uint cloneID, DataSet checkPoints)
			{
				_cloneID = cloneID;
				_checkPoints = checkPoints;
			}

			public uint CloneID 
			{ 
				get
				{ 
					_lastAccessed = DateTime.UtcNow;
					return _cloneID; 
				}
			}

			public DataSet CheckPoints 
			{ 
				get
				{ 
					_lastAccessed = DateTime.UtcNow;
					return _checkPoints; 
				}
			}

			public string GetCheckPointName(int index)
			{
				_lastAccessed = DateTime.UtcNow;
				if (_checkPoints.Tables[0].Rows.Count > index)
					return (string)_checkPoints.Tables[0].Rows[index]["Name"];
				else
					return null;
			}

			public DateTime LastAccessed { get{ return _lastAccessed; }}
			#region IDisposable Members

			public void Dispose()
			{
				if (_checkPoints != null)
					_checkPoints.Dispose();
				_checkPoints = null;
			}

			#endregion
		}

		#endregion

		private DateTime _checkPointLastFlush = DateTime.UtcNow;
		private TimeSpan _flushUnusedCheckpointsTimeSpan = new TimeSpan(2, 0, 0);

		private bool IsCheckPointFlushDue()
		{
			return DateTime.UtcNow.Subtract(_checkPointLastFlush) > _flushUnusedCheckpointsTimeSpan;
		}

		private void CheckPointFlush()
		{
			ArrayList flushedItems = new ArrayList();
			_checkPointLastFlush = DateTime.UtcNow;
			lock(_checkPointCache)
			{
				foreach(DictionaryEntry entry in _checkPointCache)
					if (_checkPointLastFlush.Subtract(((CheckPointCacheEntry)entry.Value).LastAccessed) > _flushUnusedCheckpointsTimeSpan)
						flushedItems.Add(entry);

				foreach(DictionaryEntry entry in flushedItems)
				{
					_checkPointCache.Remove(entry.Key);
					((CheckPointCacheEntry)entry.Value).Dispose();
				}
			}
		}

		private CheckPointCacheEntry GetRouteScheduleCheckPoints(SqlConnection connection, uint cloneID)
		{
			CheckPointCacheEntry cacheEntry = (CheckPointCacheEntry)_checkPointCache[cloneID];
			if (cacheEntry == null)
			{
				lock(_checkPointCache)
				{
					cacheEntry = (CheckPointCacheEntry)_checkPointCache[cloneID];
					if (cacheEntry == null)
					{
						SqlConnection localConnection = connection; 
						if (localConnection == null)
						{
							localConnection = new SqlConnection(_connectionString);
							localConnection.Open();
						}
						try
						{

							SqlCommand command = new SqlCommand("usp_Route2ScheduleGetOrderedCheckPoints", localConnection);
							command.CommandType = CommandType.StoredProcedure;

							SqlParameter parameter = command.Parameters.Add("@Route2ScheduleID", SqlDbType.Int);
							parameter.Value = (int)cloneID;

							SqlDataAdapter adapter = new SqlDataAdapter(command);
							DataSet checkPoints = new DataSet();
							adapter.Fill(checkPoints);

							cacheEntry = new CheckPointCacheEntry(cloneID, checkPoints);
							_checkPointCache.Add(cloneID, cacheEntry);
						}
						finally
						{
							if (connection == null)
								localConnection.Close();
						}
						
					}
				}
			}
			return cacheEntry;
		}

		#endregion

		/// <summary>
		/// Create a clone of the schedule to be downloaded
		/// </summary>
		/// <param name="routeScheduleID"></param>
		/// <returns></returns>
		private uint CreateClonedRouteSchedule(SqlConnection connection, int routeScheduleID)
		{
			SqlCommand command = new SqlCommand("usp_Route2ScheduleGetUptoDateClone", connection);
			command.CommandType = CommandType.StoredProcedure;

			SqlParameter parameter = command.Parameters.Add("@OriginalRouteScheduleID", SqlDbType.Int);
			parameter.Value = routeScheduleID;

			return Convert.ToUInt32(command.ExecuteScalar());
		}

		private void UpdateRouteScheduleID(SqlConnection connection, int iRouteID, uint iRouteScheduleID)
		{
			try
			{
				SqlCommand oCmd = connection.CreateCommand();
				oCmd.CommandText = "UPDATE T_VehicleSchedule SET DownloadedRouteScheduleID = " + Convert.ToString(iRouteScheduleID) + " WHERE ID = " + Convert.ToString(iRouteID);
				oCmd.ExecuteNonQuery();
			}
			catch(System.Exception)
			{
			}
		}

		private class CacheEntry
		{
			public double Latitude;
			public double Longitude;
			public GatewayProtocolPacket Packet;
			public int VehicleScheduleID;
			public long ServersideState;

			public CacheEntry(double latitude, double longitude, GatewayProtocolPacket packet, int vehicleScheduleID, long serversideState)
			{
				Latitude = latitude;
				Longitude = longitude;
				Packet = packet;
				VehicleScheduleID = vehicleScheduleID;
				ServersideState = serversideState;
			}
		}

		/// <summary>
		/// This is used to determine if the unit is on route or not.
		/// </summary>
		/// <param name="latitude"></param>
		/// <param name="longitude"></param>
		/// <param name="vehicle"></param>
		/// <param name="extendedDetails"></param>
		public void ProcessRouteState(double latitude, double longitude, Vehicle vehicle, GBVariablePacketExtended59 extendedDetails, GatewayProtocolPacket packet, DateTime packetTime, DatabaseInterface databaseInterface)
		{
            long start = DateTime.Now.Ticks;
			if (extendedDetails != null)
			{
				// Grab Vehicles Status first
				//vehicle.ServerUnitStateEx = extendedDetails.UnitStatus;
			
				if (extendedDetails.CurrentVehicleRouteScheduleID != 0)
				{
					//	if this is a cancel route message, presume offroute end..
					bool offRoute = false;
					// Calculate On/Off Route Status and place in ServerUnitStateEx
					if ((packet.cMsgType != RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED) &&
						(packet.cMsgType != RoutingModuleGPPacket.ROUTES_STATUS_ACTIVATED))
						offRoute = _routeMonitor.ProcessMessage(packet, latitude, longitude, Convert.ToInt32(vehicle.cFleet), Convert.ToInt32(vehicle.iUnit), Convert.ToInt32(extendedDetails.CurrentVehicleRouteScheduleID));
					
					//	We do not want an activated message to show offroute status.. only the offroute message
                    if (offRoute)
                    {
                        vehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_ROUTE_OFF;
                        extendedDetails.UnitStatus |= (long)StatusVehicle59.STATUS_ROUTE_OFF;
                    }
                    else
                    { 
                        vehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_ROUTE_OFF;
                        extendedDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_ROUTE_OFF;
                    }
				}
				else
                {
                    // If not on a Schedule then cannot be OffRoute 
					vehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_ROUTE_OFF;
                    extendedDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_ROUTE_OFF;
                }
		
				// Copy any changed Server bits back into UnitStatus
				//extendedDetails.UnitStatus |= vehicle.ServerUnitStateEx;
			}

			//	This is to hold on to the state we need to write the current record out with, to protect it
			//	for the on and offroute packet changes.
			long vehicleServerState = vehicle.ServerUnitStateEx;
			
			ChronologicalList packetList = _packetCache.GetOrCreate(string.Format("{0}:{1}", vehicle.cFleet, vehicle.iUnit));
			if ((!packet.bReceivedSendFlash) && 
				(!packet.bArchivalData))
			{
				//	add the current message to ensure it is processed as the last entry...
				packetList.Insert(packetTime, new CacheEntry(latitude, longitude, packet, (int)extendedDetails.CurrentVehicleRouteScheduleID, vehicle.ServerUnitStateEx));
				
				//	process through the cached messages
				CacheEntry[] entries = (CacheEntry[])packetList.ToObjectArray(typeof(CacheEntry));
				if ((entries != null) && (entries.Length > 0))
				{
					CacheEntry cacheEntry = entries[0];
					bool prevOnRoute = false;
					if (cacheEntry.Packet.cMsgType != RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED)
						prevOnRoute = _routeMonitor.ProcessMessage(cacheEntry.Packet, 
							cacheEntry.Latitude, 
							cacheEntry.Longitude, 
							Convert.ToInt32(cacheEntry.Packet.iVehicleId),
							Convert.ToInt32(cacheEntry.Packet.cFleetId),
							cacheEntry.VehicleScheduleID);

					for(int loop = 1; loop < entries.Length; loop++)
					{
						cacheEntry = entries[loop];

						//	first packet will be the last non-flash packet received.
						bool currentOnRoute = false;
						
						if (cacheEntry.Packet.cMsgType != RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED)
							currentOnRoute = _routeMonitor.ProcessMessage(cacheEntry.Packet, 
								cacheEntry.Latitude, 
								cacheEntry.Longitude, 
								Convert.ToInt32(cacheEntry.Packet.iVehicleId),
								Convert.ToInt32(cacheEntry.Packet.cFleetId),
								cacheEntry.VehicleScheduleID);

						if (currentOnRoute != prevOnRoute)
						{
							//	Log a message to indicate whether this is an OffRoute, or an OffRouteEnd
							GatewayProtocolPacket clone = CreateClone(cacheEntry.Packet);
							if (clone != null)
							{
								if ((cacheEntry.Packet.cMsgType == RoutingModuleGPPacket.ROUTES_STATUS_ACTIVATED) &&
									currentOnRoute)
									AddOneSecond(clone);
								else if (cacheEntry.Packet.cMsgType != RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED)
									SubtractOneSecond(clone);

								vehicle.ServerUnitStateEx = cacheEntry.ServersideState;
								if (currentOnRoute)
								{
									clone.cMsgType = RoutingModuleGPPacket.ROUTES_OFF_ROUTE;
									vehicle.ServerUnitStateEx |= (long)StatusVehicle59.STATUS_ROUTE_OFF;
									ModifyExtendedState(clone, (long)StatusVehicle59.STATUS_ROUTE_OFF, 0);
								}
								else
								{
									clone.cMsgType = RoutingModuleGPPacket.ROUTES_OFF_ROUTE_END;
									vehicle.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_ROUTE_OFF;
									ModifyExtendedState(clone, 0, (long)StatusVehicle59.STATUS_ROUTE_OFF);
								}
								WriteRecord(clone, databaseInterface, vehicle);
							}
						}
					}

					//	Processed list.. now clear, and store off this packet
					packetList.Clear();
				}
			}
			//	This is not a mistake!! Add the current entry here again to ensure 
			//	that it is either added to the existing list, or is the first in line for processing next time round.
			vehicle.ServerUnitStateEx = vehicleServerState;	//	restore state here
			packetList.Insert(packetTime, new CacheEntry(latitude, longitude, packet, (int)extendedDetails.CurrentVehicleRouteScheduleID, vehicle.ServerUnitStateEx));

            lock(_routeProcessingLock)
            {
                _routeProcessingTime += DateTime.Now.Ticks - start;
                _routeProcessingCount++;
            }
		}

		private GatewayProtocolPacket CreateClone(GatewayProtocolPacket packet)
		{
			if (packet is GeneralGPPacket)
			{
				return ((GeneralGPPacket)packet).CreateCopy();
			} 
			else if (packet is RoutingModuleGPPacket)
			{
				return ((RoutingModuleGPPacket)packet).CreateCopy();
			} 
			else if (packet is RoutePtSetPtGPPacket)
			{
				return ((RoutePtSetPtGPPacket)packet).CreateCopy();
			}
			return null;
		}

		private void ModifyExtendedState(GatewayProtocolPacket packet, long orMask, long nandMask)
		{
			GBVariablePacketExtended59 extendedDetails = null;
			if (packet is GeneralGPPacket)
			{
				extendedDetails = ((GeneralGPPacket)packet).mExtendedVariableDetails;
			} 
			else if (packet is RoutingModuleGPPacket)
			{
				extendedDetails = ((RoutingModuleGPPacket)packet).mExtendedVariableDetails;
			} 
			else if (packet is RoutePtSetPtGPPacket)
			{
				extendedDetails = ((RoutePtSetPtGPPacket)packet).mExtendedVariableDetails;
			}
			if (extendedDetails != null)
			{
				extendedDetails.UnitStatus |= orMask;
				extendedDetails.UnitStatus &= ~nandMask;
			}
		}

        private void WriteRecord(GatewayProtocolPacket packet, DatabaseInterface databaseInterface, Vehicle vehicle)
        {
			if (packet is GeneralGPPacket)
			{
				databaseInterface.InsertNotificationRecord((GeneralGPPacket)packet,vehicle);
			} 
			else if (packet is RoutingModuleGPPacket)
			{
				databaseInterface.InsertRoutingRecord((RoutingModuleGPPacket)packet,vehicle);
			} 
			else if (packet is RoutePtSetPtGPPacket)
			{
				databaseInterface.InsertSetPointRoutePointRecord((RoutePtSetPtGPPacket)packet,vehicle);
			}
		}

		private void AddOneSecond(GPClock clock)
		{
			if (clock.iSecond < 59)
				clock.iSecond++;
			else
			{
				clock.iSecond = 0;
				if (clock.iMinute < 59)
					clock.iMinute++;
				else
				{
					clock.iMinute = 0;
					if (clock.iHour < 23)
						clock.iHour++;
					else
					{
						clock.iHour = 0;
						if (clock.iDay < (uint)DateTime.DaysInMonth((int)clock.iYear, (int)clock.iMonth))
							clock.iDay++;
						else
						{
							clock.iDay = 1;
							if (clock.iMonth < 12)
								clock.iMonth++;
							else
							{
								clock.iMonth = 1;
								clock.iYear++;
							}
						}
					}
				}
			}
		}

		private void AddOneSecond(GatewayProtocolPacket packet)
		{
			if (packet is GeneralGPPacket)
				AddOneSecond(((GeneralGPPacket)packet).mCurrentClock);
			if (packet is RoutingModuleGPPacket)
				AddOneSecond(((RoutingModuleGPPacket)packet).mCurrentClock);
			if (packet is RoutePtSetPtGPPacket)
				AddOneSecond(((RoutePtSetPtGPPacket)packet).mCurrentClock);
		}

		private void SubtractOneSecond(GPClock clock)
		{
			if (clock.iSecond > 0)
				clock.iSecond--;
			else
			{
				clock.iSecond = 59;
				if (clock.iMinute > 0)
					clock.iMinute--;
				else
				{
					clock.iMinute = 59;
					if (clock.iHour > 0)
						clock.iHour--;
					else
					{
						clock.iHour = 23;
						if (clock.iDay > 1)
							clock.iDay--;
						else
						{
							if (clock.iMonth > 1)
								clock.iMonth--;
							else
							{
								clock.iMonth = 12;
								if (clock.iYear > 0)
									clock.iYear--;
								else
									clock.iYear = 99;
							}
							clock.iDay = (uint)DateTime.DaysInMonth((int)clock.iYear, (int)clock.iMonth);
						}
					}
				}
			}
		}

		private void SubtractOneSecond(GatewayProtocolPacket packet)
		{
			if (packet is GeneralGPPacket)
				SubtractOneSecond(((GeneralGPPacket)packet).mCurrentClock);
			if (packet is RoutingModuleGPPacket)
				SubtractOneSecond(((RoutingModuleGPPacket)packet).mCurrentClock);
			if (packet is RoutePtSetPtGPPacket)
				SubtractOneSecond(((RoutePtSetPtGPPacket)packet).mCurrentClock);
		}

		/// <summary>
		/// From the message passed, determine the appropriate name to place in the Location/Suburb.
		/// This should be the RouteName for route messages and the CheckPoint name for CheckPOint messages, and
		/// those where the unit is at a checkpoint.
		/// </summary>
		/// <param name="msgType"></param>
		/// <param name="extendedDetails"></param>
		/// <returns></returns>
		public string GetRouteNameForRouteScheduleID(byte msgType, uint vehicleScheduleID, int checkpointIndex, MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails positionDetails)
		{
			string result = null;
			switch(msgType)
			{
				case RoutingModuleGPPacket.ROUTES_STATUS_ACTIVATED :
				case RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED :
				case RoutingModuleGPPacket.ROUTES_STATUS_COMPLETED :
				case RoutingModuleGPPacket.ROUTES_UPDATE_ACCEPTED :
				case RoutingModuleGPPacket.ROUTES_OFF_ROUTE :
				case RoutingModuleGPPacket.ROUTES_OFF_ROUTE_END :
					result = _routeMonitor.GetRouteNameFromVehicleScheduleID(Convert.ToInt32(vehicleScheduleID));
					positionDetails.PositionType = DatabaseAccess.PositionDetails.Category.RouteName;	
					break;
				default:
					//	Note : Checkpoint Index is 1 based!
					if ((vehicleScheduleID > 0) && (checkpointIndex > 0))
					{
						int cloneID = _routeMonitor.GetCloneIDFromFromVehicleScheduleID(Convert.ToInt32(vehicleScheduleID));

						CheckPointCacheEntry entry = GetRouteScheduleCheckPoints(null, Convert.ToUInt32(cloneID));
						if (entry != null) 
						{
							result = entry.GetCheckPointName(checkpointIndex -1);
							positionDetails.PositionType = DatabaseAccess.PositionDetails.Category.RouteCheckpoint;	
						}
					}
					break;
			}

			if (result != null)
			{
				positionDetails.PlaceName = result;
				positionDetails.Distance = 0;
				positionDetails.Position = "N";
				positionDetails.MapRef = "";
				positionDetails.SetPointGroupID = 0;
				positionDetails.SetPointGroupNumber = 0;
				positionDetails.SetPointID = 0;
				positionDetails.SetPointNumber = 0;
				positionDetails.RouteVehicleScheduleID = vehicleScheduleID;
				positionDetails.RouteCheckPointIndex = checkpointIndex;
			}
			return result;
		}

		/// <summary>
		/// This is called when a routing packet is passed.
		/// </summary>
		/// <param name="packet"></param>
		/// <param name="vehicle"></param>
		public void ProcessRoutingPacket(RoutingModuleGPPacket packet, Vehicle vehicle)
		{
			if (packet.cMsgType == RoutingModuleGPPacket.ROUTES_UPDATE_ACCEPTED)
			{
				lock(_transmittedEntries)
				{
					EnvelopeContext envelopeContext = (EnvelopeContext)_transmittedEntries[packet.RouteNumber];
					if (envelopeContext != null)
					{
						UpdateDownloadedStateForRow(envelopeContext.Row, 
							(packet.CheckPointIndex == 1)?4:(int)envelopeContext.Row["State"], 
							envelopeContext.DownloadedScheduleID);			
						_transmittedEntries.Remove(packet.RouteNumber);
					}
				}
			}
			else if (packet.cMsgType == RoutingModuleGPPacket.ROUTES_STATUS_COMPLETED)	// Route Completed
			{
				UpdateStateForRow((int) packet.mExtendedVariableDetails.CurrentVehicleRouteScheduleID, (int)VehicleScheduleRowState.Complete);
			}
			else if (packet.cMsgType == RoutingModuleGPPacket.ROUTES_STATUS_CANCELLED)	// Route Cancelled
			{
				UpdateStateForRow((int) packet.mExtendedVariableDetails.CurrentVehicleRouteScheduleID, (int)VehicleScheduleRowState.Cancelled);
			}
			else if (packet.cMsgType == RoutingModuleGPPacket.ROUTES_STATUS_ACTIVATED)	// Route Activated
			{
				UpdateStateForRow((int) packet.mExtendedVariableDetails.CurrentVehicleRouteScheduleID, (int)VehicleScheduleRowState.Executing);
			}
		}

		private Hashtable _transmittedEntries = new Hashtable();

		/// <summary>
		/// Create the gateway protocol packet
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="unitID"></param>
		/// <param name="row"></param>
		/// <param name="clonedID"></param>
		/// <returns></returns>
		private ConfigRouteUpdateGPPacket CreateOutboundPacket(int fleetID, int unitID, DataRow row, DataSet points, bool deleteRoute)
		{
			//	Load up the data for the clone.
			
			ConfigRouteUpdateGPPacket packet = new ConfigRouteUpdateGPPacket(null);
			packet.cFleetId = (byte)fleetID;
			packet.iVehicleId = Convert.ToUInt32(unitID);

			packet.bAckImmediately = true;
			packet.bAckRegardless = true;

			if (deleteRoute)
			{
				packet.Action = ConfigRouteUpdateGPPacket.RouteUpdateAction.Delete;
				packet.VehicleRouteScheduleID = Convert.ToUInt32(row["ID"]);
				packet.StartTimeUTC = DateTime.MinValue;
				packet.MaxDurationMins = 0;
			}
			else
			{
				packet.Action = ConfigRouteUpdateGPPacket.RouteUpdateAction.Update;
				packet.VehicleRouteScheduleID = Convert.ToUInt32(row["ID"]);
				packet.StartTimeUTC = Convert.ToDateTime(row["UTCStartDate"]);
				packet.MaxDurationMins = ((TimeSpan)(Convert.ToDateTime(row["UTCEndDate"]) - Convert.ToDateTime(row["UTCStartDate"]))).Minutes;
				
				for(int loop = 0; loop < points.Tables[0].Rows.Count; loop++)
				{
					DataRow pointRow = points.Tables[0].Rows[loop];

					ConfigRouteUpdateGPPacket.CheckPointEntry checkPoint = new MTData.Transport.Gateway.Packet.ConfigRouteUpdateGPPacket.CheckPointEntry();
					checkPoint.PointNumber = loop+1; 
					checkPoint.Latitude = Convert.ToDouble(pointRow["Latitude"]);
					checkPoint.Longitude= Convert.ToDouble(pointRow["Longitude"]);
					checkPoint.LatitudeToleranceMetres = Convert.ToInt32(pointRow["LatitudeTolerance"]);
					checkPoint.LongitudeToleranceMetres = Convert.ToInt32(pointRow["LongitudeTolerance"]);
					checkPoint.ArriveMinsStart = pointRow.IsNull("RelativeMinimumArriveTimeMins")?0:Convert.ToInt32(pointRow["RelativeMinimumArriveTimeMins"]);
					checkPoint.ArriveMinsEnd = pointRow.IsNull("RelativeMaximumArriveTimeMins")?checkPoint.ArriveMinsStart:Convert.ToInt32(pointRow["RelativeMaximumArriveTimeMins"]);
					checkPoint.DepartMinsStart = pointRow.IsNull("RelativeMinimumDepartTimeMins")?checkPoint.ArriveMinsStart:Convert.ToInt32(pointRow["RelativeMinimumDepartTimeMins"]);
					checkPoint.DepartMinsEnd = pointRow.IsNull("RelativeMaximumDepartTimeMins")?checkPoint.DepartMinsStart:Convert.ToInt32(pointRow["RelativeMaximumDepartTimeMins"]);
					checkPoint.MinMinsStopTime = pointRow.IsNull("MinimumStopTimeMins")?0:Convert.ToInt32(pointRow["MinimumStopTimeMins"]);
					checkPoint.MaxMinsStopTime = pointRow.IsNull("MaximumStopTimeMins")?0:Convert.ToInt32(pointRow["MaximumStopTimeMins"]);
					checkPoint.Flags = (byte) Convert.ToInt32(pointRow["RuleFlag"]);
                    checkPoint.DependentOnPoint = (byte) Convert.ToInt32(pointRow["DependentOnPointNum"]);
					packet.AddCheckPoint(checkPoint);
				}
			}
			return packet;
		}

		private void UpdateDownloadedStateForRow(DataRow row, int newState, uint downloadedScheduleID)
		{
			if (_logRouteData)
				_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("UpdateDownloadedStateForRow : VehicleScheduleID {0}, NewState {1}, CloneID {2}", row["ID"], newState, downloadedScheduleID)));

			SqlConnection connection = new SqlConnection(_connectionString);
			connection.Open();
			try
			{
				SqlCommand command = new SqlCommand("usp_Route2VehicleScheduleSetDownloaded", connection);
				command.CommandType = CommandType.StoredProcedure;

				SqlParameter parameter = command.Parameters.Add("@ID", SqlDbType.Int);
				parameter.Value = Convert.ToInt32(row["ID"]);

				parameter = command.Parameters.Add("@State", SqlDbType.Int);
				parameter.Value = newState;

				parameter = command.Parameters.Add("@DownloadedRouteScheduleID", SqlDbType.Int);
				parameter.Value = downloadedScheduleID;

				parameter = command.Parameters.Add("@Original_Deleted", SqlDbType.Bit);
				parameter.Value = row["Deleted"];
				parameter = command.Parameters.Add("@Original_State", SqlDbType.Int);
				parameter.Value = row["State"];
				parameter = command.Parameters.Add("@Original_VehicleID", SqlDbType.Int);
				parameter.Value = row["VehicleID"];
				parameter = command.Parameters.Add("@Original_FleetID", SqlDbType.Int);
				parameter.Value = row["FleetID"];
				parameter = command.Parameters.Add("@Original_RouteScheduleID", SqlDbType.Int);
				parameter.Value = row["RouteScheduleID"];
				parameter = command.Parameters.Add("@Original_UtcStartDate", SqlDbType.DateTime);
				parameter.Value = row["UtcStartDate"];
				parameter = command.Parameters.Add("@Original_AssignedUserID", SqlDbType.Int);
				parameter.Value = row["AssignedUserID"];
				parameter = command.Parameters.Add("@Original_UtcDownloadedDate", SqlDbType.DateTime);
				parameter.Value = row["UtcDownloadedDate"];
				parameter = command.Parameters.Add("@Original_DownloadedRouteScheduleID", SqlDbType.Int);
				parameter.Value = row["DownloadedRouteScheduleID"];
				parameter = command.Parameters.Add("@Original_UtcModifiedDate", SqlDbType.DateTime);
				parameter.Value = row["UtcModifiedDate"];

				command.ExecuteNonQuery();
				ReloadDataSets(connection);
			}
			finally
			{
				connection.Close();
			}
		}

		private void UpdateStateForRow(int vehicleScheduleID, int newState)
		{
			if (_logRouteData)
				_log.Info(string.Format(ROUTELOG_FORMAT, string.Format("UpdateStateForRow : VehicleScheduleID {0}, NewState {1}", vehicleScheduleID, newState)));
			SqlConnection connection = new SqlConnection(_connectionString);
			connection.Open();
			try
			{
				SqlCommand command = new SqlCommand("usp_Route2VehicleScheduleUpdateState", connection);
				command.CommandType = CommandType.StoredProcedure;

				SqlParameter parameter = command.Parameters.Add("@ID", SqlDbType.Int);
				parameter.Value = vehicleScheduleID;

				parameter = command.Parameters.Add("@State", SqlDbType.Int);
				parameter.Value = newState;

				command.ExecuteNonQuery();
				ReloadDataSets(connection);
			}
			finally
			{
				connection.Close();
			}
		}
		#region IDisposable Members

		public void Dispose()
		{
			if (_logRouteData)
				_log.Info(string.Format(ROUTELOG_FORMAT, "Disposing"));
		}

		#endregion
	}
}

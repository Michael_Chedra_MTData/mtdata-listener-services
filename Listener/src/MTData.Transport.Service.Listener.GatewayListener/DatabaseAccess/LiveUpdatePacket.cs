using System;
using System.Text;
using System.Collections;
using MTData.Transport.Gateway.Packet;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    /// <summary>
    /// This class will formulate the packet for the live updates, in differing versions.
    /// It will try reduce the amount of work required to create/add to a live update packet, 
    /// and make better use fo string variables by using a string builder.
    /// </summary>
    public class LiveUpdatePacket
    {
        private string _serverTime_DateFormat = null;

        public LiveUpdatePacket(string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            for (int loop = 0; loop < _trailers.Length; loop++)
                _trailers[loop] = new LiveUpdateTrailerEntry();
        }

        #region String Generation

        internal const string COMMA_TEXT = ",";

        public Hashtable ToFormattedVariants()
        {
            Hashtable result = new Hashtable();

            StringBuilder builder = new StringBuilder();
            builder.Append(_fleetID); builder.Append(COMMA_TEXT);
            builder.Append(_vehicleID); builder.Append(COMMA_TEXT);
            builder.Append(_messageType); builder.Append(COMMA_TEXT);
            builder.Append(_fixClock); builder.Append(COMMA_TEXT);
            builder.Append(_currentClock); builder.Append(COMMA_TEXT);
            builder.Append(DateUtility.GetDateInFormat(_serverTime, _serverTime_DateFormat)); builder.Append(COMMA_TEXT);
            builder.Append(_latitude); builder.Append(COMMA_TEXT);
            builder.Append(_longitude); builder.Append(COMMA_TEXT);
            builder.Append(_direction); builder.Append(COMMA_TEXT);
            builder.Append(_speed); builder.Append(COMMA_TEXT);
            builder.Append(_rawGPS); builder.Append(COMMA_TEXT);
            builder.Append(_status); builder.Append(COMMA_TEXT);
            builder.Append(_flags); builder.Append(COMMA_TEXT);
            builder.Append(_userDefined); builder.Append(COMMA_TEXT);
            builder.Append(_inputStatus); builder.Append(COMMA_TEXT);
            builder.Append(_outputStatus); builder.Append(COMMA_TEXT);
            builder.Append(_lightStatus); builder.Append(COMMA_TEXT);
            builder.Append(_buttonStatus); builder.Append(COMMA_TEXT);
            builder.Append(_maxSpeed); builder.Append(COMMA_TEXT);
            builder.Append(_speedAccumulator); builder.Append(COMMA_TEXT);
            builder.Append(_samples); builder.Append(COMMA_TEXT);

            if (_engineData != null)
                builder.Append(_engineData.ToString());

            builder.Append(_extendedValues);

            string value = builder.ToString();
            result.Add((double)0, value);
            //============= Version 0 End ===================

            builder.Append(COMMA_TEXT);
            builder.Append(_position.Trim()); builder.Append(COMMA_TEXT);
            builder.Append(_placeName.Replace(COMMA_TEXT, Convert.ToString((char)0x40)).Trim()); builder.Append(COMMA_TEXT);
            builder.Append(_distance); builder.Append(COMMA_TEXT);
            builder.Append(_mapReference.Trim()); builder.Append(COMMA_TEXT);
            builder.Append(_userInfo.Replace(COMMA_TEXT, Convert.ToString((char)0x40)).Trim());

            value = builder.ToString();
            result.Add((double)2.7, value);
            //============= Version 2.7 End ===================

            builder.Append(COMMA_TEXT);
            builder.Append(_vehicleTag.Replace(COMMA_TEXT, Convert.ToString((char)0x40)).Trim());

            value = builder.ToString();
            result.Add((double)2.71, value);
            //============= Version 2.71 End ===================

            builder.Append(COMMA_TEXT);
            builder.Append(_batteryVolts); builder.Append(COMMA_TEXT);
            builder.Append(_brakeUsageSeconds); builder.Append(COMMA_TEXT);
            builder.Append(_canVehicleSpeed); builder.Append(COMMA_TEXT);
            builder.Append(_odometer); builder.Append(COMMA_TEXT);
            builder.Append(_rpmZone1); builder.Append(COMMA_TEXT);
            builder.Append(_rpmZone2); builder.Append(COMMA_TEXT);
            builder.Append(_rpmZone3); builder.Append(COMMA_TEXT);
            builder.Append(_rpmZone4); builder.Append(COMMA_TEXT);
            builder.Append(_speedZone1); builder.Append(COMMA_TEXT);
            builder.Append(_speedZone2); builder.Append(COMMA_TEXT);
            builder.Append(_speedZone3); builder.Append(COMMA_TEXT);
            builder.Append(_speedZone4); builder.Append(COMMA_TEXT);
            builder.Append(_totalFuelUsed); builder.Append(COMMA_TEXT);

            for (int loop = 0; loop < _trailers.Length; loop++)
            {
                builder.Append(_trailers[loop].ID);
                builder.Append(COMMA_TEXT);
                builder.Append(_trailers[loop].Name.Replace(COMMA_TEXT, "||"));
                builder.Append(COMMA_TEXT);
            }

            value = builder.ToString();
            result.Add((double)2.8, value);
            //============= Version 2.8 End ===================

            builder.Append(_fuelEconomy);
            builder.Append(COMMA_TEXT);

            value = builder.ToString();
            result.Add((double)2.9, value);
            //============= Version 2.9 End ===================

            builder.Append(_unitState); builder.Append(COMMA_TEXT);
            builder.Append(_auxiliaryState); builder.Append(COMMA_TEXT);
            builder.Append(_currentSetPointGroupID); builder.Append(COMMA_TEXT);
            builder.Append(_currentSetPointNumber);

            value = builder.ToString();
            result.Add((double)3.0, value);
            //============= Version 3.0 End ===================

            return result;
        }
        #endregion

        #region Properties

        //============= Version 0 ===================
        public int FleetID { get { return _fleetID; } set { _fleetID = value; } }
        public uint VehicleID { get { return _vehicleID; } set { _vehicleID = value; } }
        public int MessageType { get { return _messageType; } set { _messageType = value; } }
        public string FixClock { get { return _fixClock; } set { _fixClock = value; } }
        public string CurrentClock { get { return _currentClock; } set { _currentClock = value; } }
        public DateTime ServerTime { get { return _serverTime; } set { _serverTime = value; } }
        public decimal Latitude { get { return _latitude; } set { _latitude = value; } }
        public decimal Longitude { get { return _longitude; } set { _longitude = value; } }
        public int Direction { get { return _direction; } set { _direction = value; } }
        public int Speed { get { return _speed; } set { _speed = value; } }
        public string RawGPS { get { return _rawGPS; } set { _rawGPS = value; } }
        public uint Status { get { return _status; } set { _status = value; } }
        public int Flags { get { return _flags; } set { _flags = value; } }
        public string UserDefined { get { return _userDefined; } set { _userDefined = value; } }
        public int InputStatus { get { return _inputStatus; } set { _inputStatus = value; } }
        public int OutputStatus { get { return _outputStatus; } set { _outputStatus = value; } }
        public int LightStatus { get { return _lightStatus; } set { _lightStatus = value; } }
        public int ButtonStatus { get { return _buttonStatus; } set { _buttonStatus = value; } }
        public int MaxSpeed { get { return _maxSpeed; } set { _maxSpeed = value; } }
        public long SpeedAccumulator { get { return _speedAccumulator; } set { _speedAccumulator = value; } }
        public int Samples { get { return _samples; } set { _samples = value; } }

        public LiveUpdateEngineData EngineData { get { return _engineData; } set { _engineData = value; } }

        public string ExtendedValues { get { return _extendedValues; } set { _extendedValues = value; } }
        //============= Version 0 End ===============

        //============= Version 2.7 Start ===============
        public string Position { get { return _position; } set { _position = value; } }
        public string PlaceName { get { return _placeName; } set { _placeName = value; } }
        public int Distance { get { return _distance; } set { _distance = value; } }
        public string MapReference { get { return _mapReference; } set { _mapReference = value; } }
        public string UserInfo { get { return _userInfo; } set { _userInfo = value; } }

        //============= Version 2.7 End ===============

        //============= Version 2.71 Start ===============
        public string VehicleTag { get { return _vehicleTag; } set { _vehicleTag = value; } }
        //============= Version 2.71 End ===============

        //============= Version 2.8 Start ===============
        public int BatteryVolts { get { return _batteryVolts; } set { _batteryVolts = value; } }
        public int BrakeUsageSeconds { get { return _brakeUsageSeconds; } set { _brakeUsageSeconds = value; } }
        public int CanVehicleSpeed { get { return _canVehicleSpeed; } set { _canVehicleSpeed = value; } }
        public uint Odometer { get { return _odometer; } set { _odometer = value; } }
        public int RpmZone1 { get { return _rpmZone1; } set { _rpmZone1 = value; } }
        public int RpmZone2 { get { return _rpmZone2; } set { _rpmZone2 = value; } }
        public int RpmZone3 { get { return _rpmZone3; } set { _rpmZone3 = value; } }
        public int RpmZone4 { get { return _rpmZone4; } set { _rpmZone4 = value; } }

        public int SpeedZone1 { get { return _speedZone1; } set { _speedZone1 = value; } }
        public int SpeedZone2 { get { return _speedZone2; } set { _speedZone2 = value; } }
        public int SpeedZone3 { get { return _speedZone3; } set { _speedZone3 = value; } }
        public int SpeedZone4 { get { return _speedZone4; } set { _speedZone4 = value; } }

        public int TotalFuelUsed { get { return _totalFuelUsed; } set { _totalFuelUsed = value; } }

        public LiveUpdateTrailerEntry[] Trailers { get { return _trailers; } }
        //============= Version 2.8 End ===============

        //============= Version 2.9 Start ===============
        public double FuelEconomy { get { return _fuelEconomy; } set { _fuelEconomy = value; } }
        //============= Version 2.9 End ===============

        //============= Version 3.0 Start ===============
        public long UnitState { get { return _unitState; } set { _unitState = value; } }
        public long AuxiliaryState { get { return _auxiliaryState; } set { _auxiliaryState = value; } }
        public int CurrentSetPointGroupID { get { return _currentSetPointGroupID; } set { _currentSetPointGroupID = value; } }
        public int CurrentSetPointNumber { get { return _currentSetPointNumber; } set { _currentSetPointNumber = value; } }
        //============= Version 3.0 End ===============

        #endregion

        #region Internal Fields

        //============= Version 0 ===================
        private int _fleetID;
        private uint _vehicleID;
        private int _messageType;
        private string _fixClock = "";
        private string _currentClock = "";
        private DateTime _serverTime;
        private decimal _latitude;
        private decimal _longitude;
        private int _direction;
        private int _speed;
        private string _rawGPS = "";
        private uint _status = 0; //0
        private int _flags;
        private string _userDefined = "";
        private int _inputStatus;
        private int _outputStatus;
        private int _lightStatus;
        private int _buttonStatus;
        private int _maxSpeed;
        private long _speedAccumulator;
        private int _samples;

        private LiveUpdateEngineData _engineData = null;

        private string _extendedValues;
        //============= Version 0 End ===============

        //============= Version 2.7 Start ===============
        private string _position = "";
        private string _placeName = "";
        private int _distance = 0;
        private string _mapReference = "";
        private string _userInfo = "";

        //============= Version 2.7 End ===============

        //============= Version 2.71 Start ===============
        private string _vehicleTag = "";
        //============= Version 2.71 End ===============

        //============= Version 2.8 Start ===============
        private int _batteryVolts;
        private int _brakeUsageSeconds;
        private int _canVehicleSpeed;
        private uint _odometer;
        private int _rpmZone1;
        private int _rpmZone2;
        private int _rpmZone3;
        private int _rpmZone4;

        private int _speedZone1;
        private int _speedZone2;
        private int _speedZone3;
        private int _speedZone4;

        private int _totalFuelUsed;

        private LiveUpdateTrailerEntry[] _trailers = new LiveUpdateTrailerEntry[5];
        //============= Version 2.8 End ===============

        //============= Version 2.9 Start ===============
        private double _fuelEconomy;
        //============= Version 2.9 End ===============

        //============= Version 3.0 Start ===============
        private long _unitState = 0;
        private long _auxiliaryState = 0;
        private int _currentSetPointGroupID = 0;
        private int _currentSetPointNumber = 0;
        //============= Version 3.0 End ===============

        #endregion

        #region Population Methods

        public delegate string GetTrailerNameFromIDDelegate(byte[] bTrailerESN, int iFleetID);

        public event GetTrailerNameFromIDDelegate GetTrailerNameFromID;

        public void PopulateFromPacket(int messageType, string userDefinedValue, string vehicleTag, string userInfo, PositionDetails position, long unitState, long auxState)
        {
            _messageType = messageType;
            _userDefined = userDefinedValue;
            _position = position.Position;
            _placeName = position.PlaceName;
            _distance = Convert.ToInt32(Math.Round(position.Distance, 0));
            _mapReference = position.MapRef.Trim();
            _userInfo = userInfo;
            _vehicleTag = vehicleTag;
            _unitState = unitState;
            _auxiliaryState = auxState;
        }

        public void PopulateFromPacket(GeneralGPPacket packet, int messageType, string userDefinedValue, string vehicleTag, string userInfo, PositionDetails position, long unitState, long auxState)
        {
            PopulateFromPacket(messageType, userDefinedValue, vehicleTag, userInfo, position, unitState, auxState);

            _fleetID = packet.cFleetId;
            _vehicleID = packet.iVehicleId;
            _fixClock = packet.mFixClock.ToString();
            _currentClock = packet.mCurrentClock.ToString();
            _serverTime = DateTime.Now;
            _latitude = packet.mFix.dLatitude;
            _longitude = packet.mFix.dLongitude;
            _direction = packet.mFix.iDirection;
            _speed = packet.mFix.cSpeed;
            _rawGPS = packet.sRawGPS;
            _status = 0;
            _flags = packet.mFix.cFlags;
            _inputStatus = packet.mStatus.cInputStatus;
            _outputStatus = packet.mStatus.cOutputStatus;
            _lightStatus = packet.mStatus.cLightStatus;
            _buttonStatus = packet.mStatus.cButtonStatus;
            _maxSpeed = packet.mDistance.cMaxSpeed;
            _speedAccumulator = packet.mDistance.lSpeedAccumulator;
            _samples = packet.mDistance.iSamples;

            _fuelEconomy = 0;
            if (packet.mEngineData == null)
                _engineData = null;
            else
            {
                _engineData = new LiveUpdateEngineData();
                _engineData.PopulateFromPacket(packet.mEngineData);
                if (packet.mEngineSummaryData != null)
                {
                    _engineData.PopulateFromPacket(packet.mEngineSummaryData);
                    _fuelEconomy = packet.mEngineSummaryData.fFuelEconomy;
                }
            }

            _extendedValues = packet.mExtendedValues.ToDatabaseFormatString();

            PopulateFromPacket(packet.mTransportExtraDetails);
            PopulateFromPacket(packet.mTrailerTrack, (int)packet.cFleetId);

            _currentSetPointGroupID = packet.mExtendedVariableDetails.CurrentSetPointGroup;
            _currentSetPointNumber = packet.mExtendedVariableDetails.CurrentSetPointNumber;
        }

        public void PopulateFromPacket(RoutingModuleGPPacket packet, int messageType, string userDefinedValue, string vehicleTag, string userInfo, PositionDetails position, long unitState, long auxState)
        {
            PopulateFromPacket(messageType, userDefinedValue, vehicleTag, userInfo, position, unitState, auxState);

            _fleetID = packet.cFleetId;
            _vehicleID = packet.iVehicleId;
            _fixClock = packet.mFixClock.ToString();
            _currentClock = packet.mCurrentClock.ToString();
            _serverTime = DateTime.Now;
            _latitude = packet.mFix.dLatitude;
            _longitude = packet.mFix.dLongitude;
            _direction = packet.mFix.iDirection;
            _speed = packet.mFix.cSpeed;
            _rawGPS = "";
            _status = packet.RouteNumber;
            _flags = packet.CheckPointIndex;
            _inputStatus = packet.mStatus.cInputStatus;
            _outputStatus = packet.mStatus.cOutputStatus;
            _lightStatus = packet.mStatus.cLightStatus;
            _buttonStatus = packet.mStatus.cButtonStatus;
            _maxSpeed = packet.mDistance.cMaxSpeed;
            _speedAccumulator = packet.mDistance.lSpeedAccumulator;
            _samples = packet.mDistance.iSamples;

            _fuelEconomy = 0;
            if (packet.mEngineData == null)
                _engineData = null;
            else
            {
                _engineData = new LiveUpdateEngineData();
                _engineData.PopulateFromPacket(packet.mEngineData);
                //	No engine summary data?
            }

            _extendedValues = packet.mExtendedValues.ToDatabaseFormatString();

            PopulateFromPacket(packet.mTransportExtraDetails);
            PopulateFromPacket(packet.mTrailerTrack, (int)packet.cFleetId);


            _currentSetPointGroupID = position.SetPointGroupID;
            _currentSetPointNumber = position.SetPointGroupNumber;

        }

        public void PopulateFromPacket(RoutePtSetPtGPPacket packet, int messageType, string userDefinedValue, string vehicleTag, string userInfo, PositionDetails position, long unitState, long auxState)
        {
            PopulateFromPacket(messageType, userDefinedValue, vehicleTag, userInfo, position, unitState, auxState);

            _fleetID = packet.cFleetId;
            _vehicleID = packet.iVehicleId;
            _fixClock = packet.mFixClock.ToString();
            _currentClock = packet.mCurrentClock.ToString();
            _serverTime = DateTime.Now;
            _latitude = packet.mFix.dLatitude;
            _longitude = packet.mFix.dLongitude;
            _direction = packet.mFix.iDirection;
            _speed = packet.mFix.cSpeed;
            _rawGPS = "";
            _status = packet.cPointSetNumber;	//	Difference here from GPPacket
            _flags = packet.cPointPointNumber;	//	Difference here form GPPacket
            _inputStatus = packet.mStatus.cInputStatus;
            _outputStatus = packet.mStatus.cOutputStatus;
            _lightStatus = packet.mStatus.cLightStatus;
            _buttonStatus = packet.mStatus.cButtonStatus;
            _maxSpeed = packet.mDistance.cMaxSpeed;
            _speedAccumulator = packet.mDistance.lSpeedAccumulator;
            _samples = packet.mDistance.iSamples;

            if (packet.mEngineData == null)
                _engineData = null;
            else
            {
                _engineData = new LiveUpdateEngineData();
                _engineData.PopulateFromPacket(packet.mEngineData);
                //	No extended engine data processed..
            }
            _fuelEconomy = packet.mTransportExtraDetails.fFuelEconomy;

            _extendedValues = packet.mExtendedValues.ToDatabaseFormatString();

            PopulateFromPacket(packet.mTransportExtraDetails);
            PopulateFromPacket(packet.mTrailerTrack, (int)packet.cFleetId);

            _currentSetPointGroupID = position.SetPointGroupID;
            _currentSetPointNumber = position.SetPointID;

        }

        public void PopulateFromPacket(GPTransportExtraDetails packet)
        {
            _batteryVolts = packet.iBatteryVolts;
            _brakeUsageSeconds = packet.iBrakeUsageSeconds;
            _canVehicleSpeed = packet.iCANVehicleSpeed;
            _odometer = packet.iOdometer;
            _rpmZone1 = packet.iRPMZone1;
            _rpmZone2 = packet.iRPMZone2;
            _rpmZone3 = packet.iRPMZone3;
            _rpmZone4 = packet.iRPMZone4;

            _speedZone1 = packet.iSpeedZone1;
            _speedZone2 = packet.iSpeedZone2;
            _speedZone3 = packet.iSpeedZone3;
            _speedZone4 = packet.iSpeedZone4;

            _totalFuelUsed = packet.iTotalFuelUsed;
        }

        public void PopulateFromPacket(GPTrailerTrack packet, int iFleetID)
        {
            if (GetTrailerNameFromID != null)
            {
                int trailerIndex = 0;
                for (int loop = 0; loop < packet.oTrailers.Count; loop++)
                {
                    string trailerName = GetTrailerNameFromID((byte[])packet.oTrailers[loop], iFleetID);
                    string[] splits = trailerName.Split("~".ToCharArray());
                    if (splits.Length == 2)
                    {
                        _trailers[trailerIndex].ID = Int32.Parse(splits[0]);
                        _trailers[trailerIndex].Name = splits[1];
                        trailerIndex++;
                    }
                }
            }
        }

        #endregion
    }

    public class LiveUpdateEngineData
    {
        #region Properties

        //	Standard Data
        public int CoolantTemperature { get { return _coolantTemperature; } set { _coolantTemperature = value; } }
        public int OilTemperature { get { return _oilTemperature; } set { _oilTemperature = value; } }
        public int OilPressure { get { return _oilPressure; } set { _oilPressure = value; } }
        public int Gear { get { return _gear; } set { _gear = value; } }
        public int MaxRpm { get { return _maxRpm; } set { _maxRpm = value; } }
        public int BrakeApplications { get { return _brakeApplications; } set { _brakeApplications = value; } }
        public float GForceFront { get { return _gForceFront; } set { _gForceFront = value; } }
        public float GForceBack { get { return _gForceBack; } set { _gForceBack = value; } }
        public float GForceLeftRight { get { return _gForceLeftRight; } set { _gForceLeftRight = value; } }

        //	Summary Data
        public int TotalEngineHours { get { return _totalEngineHours; } set { _totalEngineHours = value; } }
        public int TotalFuel { get { return _totalFuel; } set { _totalFuel = value; } }
        public int TripFuel { get { return _tripFuel; } set { _tripFuel = value; } }
        public double FuelEconomy { get { return _fuelEconomy; } set { _fuelEconomy = value; } }
        public int Odometer { get { return _odometer; } set { _odometer = value; } }

        #endregion
        #region Fields

        //	Standard Data
        private int _coolantTemperature = 0;
        private int _oilTemperature = 0;
        private int _oilPressure = 0;
        private int _gear = 0;
        private int _maxRpm = 0;
        private int _brakeApplications = 0;
        private float _gForceFront = 0;
        private float _gForceBack = 0;
        private float _gForceLeftRight = 0;

        //	Summary Data
        private int _totalEngineHours = 0;
        private int _totalFuel = 0;
        private int _tripFuel = 0;
        private double _fuelEconomy = 0;
        private int _odometer = 0;

        #endregion

        public void PopulateFromPacket(GPEngineData packet)
        {
            _coolantTemperature = packet.iCoolantTemperature;
            _oilTemperature = packet.iOilTemperature;
            _oilPressure = packet.iOilPressure;
            _gear = packet.iGear;
            _maxRpm = packet.iMaxRPM;
            _brakeApplications = packet.iBrakeApplications;
            _gForceFront = packet.fGForceFront;
            _gForceBack = packet.fGForceBack;
            _gForceLeftRight = packet.fGForceLeftRight;

        }

        public void PopulateFromPacket(GPEngineSummaryData packet)
        {
            _totalEngineHours = packet.iTotalEngineHours;
            _totalFuel = packet.iTotalFuel;
            _tripFuel = packet.iTripFuel;
            _fuelEconomy = packet.fFuelEconomy;
            _odometer = packet.iOdometer;
        }

        public override string ToString()
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(_coolantTemperature); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_oilTemperature); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_oilPressure); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_gear); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_maxRpm); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_brakeApplications); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_gForceFront); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_gForceBack); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_gForceLeftRight); builder.Append(LiveUpdatePacket.COMMA_TEXT);

            builder.Append(_totalEngineHours); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_totalFuel); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_tripFuel); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_fuelEconomy); builder.Append(LiveUpdatePacket.COMMA_TEXT);
            builder.Append(_odometer); builder.Append(LiveUpdatePacket.COMMA_TEXT);

            return builder.ToString();
        }

    }

    public class LiveUpdateTrailerEntry
    {
        private int _id = 0;
        private string _name = "";

        public int ID { get { return _id; } set { _id = value; } }
        public string Name { get { return _name; } set { _name = value; } }
    }
}

using System;
using System.Collections;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
	/// <summary>
	/// Summary description for PositionDetails.
	/// </summary>
	public class PositionDetails
	{
        private static ILog _log = LogManager.GetLogger(typeof(PositionDetails));
		public enum Category
		{
			None,
			Suburb,
			WayPoint,
			Special1,
			Special2,
			NoGoZone,
			RouteName,
			RouteCheckpoint
		}

		public decimal Distance = 0;
		public string Position = "";
		public string PlaceName = "";
		public string MapRef = "";
		
		public int SetPointGroupID = 0;
		public int SetPointGroupNumber = 0;
		public int SetPointID = 0;
		public int SetPointNumber = 0;
		public Category PositionType = Category.None;

		public uint RouteVehicleScheduleID = 0;
		public int RouteCheckPointIndex = 0;

		public PositionDetails()
		{
		}

		public void GetPositionFromLocation(ref Hashtable oPlaces, double dLat, double dLong)
		{
			byte[] bHashKey = null;
			byte[] bConvert = null;
			int iValue = 0;

			try
			{
				bHashKey = new byte[8];
				iValue = Convert.ToInt32(dLat * 100000);
				bConvert = BitConverter.GetBytes(iValue);
				bHashKey[0] = bConvert[0];
				bHashKey[1] = bConvert[1];
				bHashKey[2] = bConvert[2];
				bHashKey[3] = bConvert[3];

				iValue = Convert.ToInt32(dLong * 100000);
				bConvert = BitConverter.GetBytes(iValue);
				bHashKey[4] = bConvert[0];
				bHashKey[5] = bConvert[1];
				bHashKey[6] = bConvert[2];
				bHashKey[7] = bConvert[3];


			}
			catch(System.Exception ex)
			{
				Console.WriteLine("Error calculating place and melways ref : " + ex.Message);
			}
		}

        public PositionDetails Copy
        {
            get
            {
                PositionDetails ret = null;
                try
                {
                    ret = new PositionDetails();
                    ret.Distance = this.Distance;
                    ret.Position = this.Position;
                    ret.PlaceName = this.PlaceName;
                    ret.MapRef = this.MapRef;
                    ret.SetPointGroupID = this.SetPointGroupID;
                    ret.SetPointGroupNumber = this.SetPointGroupNumber;
                    ret.SetPointID = this.SetPointID;
                    ret.SetPointNumber = this.SetPointNumber;
                    ret.PositionType = this.PositionType;
                    ret.RouteVehicleScheduleID = this.RouteVehicleScheduleID;
                    ret.RouteCheckPointIndex = this.RouteCheckPointIndex;
                }
                catch (System.Exception ex)
                {
                    _log.Error(this.GetType().FullName + ".Copy()", ex);
                }
                return ret;
            }
        }
	}
}

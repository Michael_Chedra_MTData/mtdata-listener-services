using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class PendantAlarmListItem
    {
        public byte Fleet;
        public uint VehicleId;
        public DateTime AlarmTime;
        public DateTime LastResend = DateTime.MinValue;
        public bool WaitTillFlashClearBeforeResend;
        public GatewayProtocolPacket AlarmPacket;
        private bool acknowledged = false;

        public UserAckForUnitAlarm AckPacket
        {
            get
            {
                // Create a new ack packet and try to send it to the unit immeadiately.'
                UserAckForUnitAlarm _ackPacket = new UserAckForUnitAlarm("dd/MM/yyyy HH:mm:ss");
                _ackPacket.cFleetId = this.Fleet;
                _ackPacket.iVehicleId = this.VehicleId;
                _ackPacket.AlarmAckFlags = 0;
                _ackPacket.Encode();
                return _ackPacket;
            }
        }

        public PendantAlarmListItem(byte cFleet, uint iUnit, DateTime alarmTime, bool acknowledged)
        {
            Fleet = cFleet;
            VehicleId = iUnit;
            AlarmTime = alarmTime;
            Acknowledged = acknowledged;
        }

        public PendantAlarmListItem(byte cFleet, uint iUnit, DateTime alarmTime, GeneralGPPacket alarmPacket)
        {
            Fleet = cFleet;
            VehicleId = iUnit;
            AlarmTime = alarmTime;
            AlarmPacket = alarmPacket;
        }

        /// <summary>
        /// Determines if the alarm has been acknowledged by a user.
        /// </summary>
        public bool Acknowledged
        {
            get { return acknowledged; }
            set { acknowledged = value; }
        }
        
    }
}

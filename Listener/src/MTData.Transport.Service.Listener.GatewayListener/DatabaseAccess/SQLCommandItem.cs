using System;
using System.Collections;
using System.Data.SqlClient;
using System.Runtime.Serialization.Formatters.Binary;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// Summary description for SQLCommandItem.
	/// </summary>
	[Serializable]
	public class SQLCommandItem : ISerializable
	{
		private object[] oParameters = null;
		private string sCommand = "";

		public SQLCommandItem(SqlCommand oCmd)
		{
			oParameters = new object[oCmd.Parameters.Count];
			for(int X = 0; X < oCmd.Parameters.Count; X++)
			{
				oParameters[X] = oCmd.Parameters[X].Value;
			}
			sCommand = oCmd.CommandText;
		}

		public SqlCommand PopulateCommand (SqlCommand oCmd)
		{
			oCmd.CommandText = sCommand;
			for(int X = 0; X < oParameters.Length; X++)
				oCmd.Parameters[X] = (SqlParameter) oParameters[X];
			return oCmd;
		}

		// A method called when serializing a Singleton.
		void ISerializable.GetObjectData(
			SerializationInfo info, StreamingContext context) 
		{
			for(int X = 0; X < oParameters.Length; X++)
			{
				info.AddValue(Convert.ToString(X), oParameters[X]);
			}
		}

	}
}

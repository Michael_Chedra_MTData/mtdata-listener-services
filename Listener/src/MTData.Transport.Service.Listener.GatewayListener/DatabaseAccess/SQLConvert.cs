using System;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// This class provides standard conversion commands for operating with SQL data.
	/// </summary>
	public class SQLConvert
	{
		#region long

		/// <summary>
		/// Conversion from a reader value to a long.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static long ToLong(object returnedValue, long defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt64(returnedValue);
		}

		/// <summary>
		/// Convert from a standard long to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromLong(long fieldValue, long defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}
		
		#endregion

		#region ulong

		/// <summary>
		/// Conversion from a reader value to a ulong.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static ulong ToULong(object returnedValue, ulong defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToUInt64(returnedValue);
		}

		/// <summary>
		/// Convert form a standard ulong to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromULong(ulong fieldValue, ulong defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return Convert.ToInt64(fieldValue);
		}
		
		#endregion

		#region int

		/// <summary>
		/// Conversion from a reader value to an int.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static int ToInt(object returnedValue, int defaultValue)
		{
			return ToInt32(returnedValue, defaultValue);
		}

		/// <summary>
		/// Convert to a 32bit number
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static int ToInt32(object returnedValue, int defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt32(returnedValue);
		}

		/// <summary>
		/// Convert into a 64 bit integer
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static long ToInt64(object returnedValue, int defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt64(returnedValue);
		}

		/// <summary>
		/// Convert to a 16bit number
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static int ToInt16(object returnedValue, int defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToInt32(returnedValue);
		}

		/// <summary>
		/// Convert from a standard int to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromInt(int fieldValue, int defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}
		
		#endregion

		#region double
 
		/// <summary>
		/// Convert from a reader value to a double
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static double ToDouble(object returnedValue, double defaultValue)
		{
			if(returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToDouble(returnedValue);
		}

		/// <summary>
		/// Convert from standard double to reader value
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static object FromDouble(double fieldValue, double defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region single

		/// <summary>
		/// Convert from a reader value to a double
		/// </summary>
		/// <param name="returnedValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static float ToSingle(object returnedValue, float defaultValue)
		{
			if(returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToSingle(returnedValue);
		}

		/// <summary>
		/// Convert from standard double to reader value
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		/// <returns></returns>
		public static object FromSingle(float fieldValue, float defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion

		#region DateTime

		/// <summary>
		/// Conversion from a reader value to an int.
		/// </summary>
		/// <param name="returnedValue">Value returned from reader</param>
		/// <param name="defaultValue">Value to return if readervalue is null</param>
		/// <returns></returns>
		public static DateTime ToDateTime(object returnedValue, DateTime defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return Convert.ToDateTime(returnedValue);
		}

		/// <summary>
		/// Convert from a standard int to a reader value.
		/// </summary>
		/// <param name="fieldValue"></param>
		/// <param name="defaultValue"></param>
		public static object FromDateTime(DateTime fieldValue, DateTime defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}
		
		#endregion

		#region String

		public static string ToString(object returnedValue, string defaultValue)
		{
			if (returnedValue == DBNull.Value)
				return defaultValue;
			else
				return (string)returnedValue;
		}

		public static object FromString(string fieldValue, string defaultValue)
		{
			if (fieldValue == defaultValue)
				return DBNull.Value;
			else
				return fieldValue;
		}

		#endregion
	}
}

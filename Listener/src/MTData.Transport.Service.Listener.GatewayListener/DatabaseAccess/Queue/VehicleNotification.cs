﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class VehicleNotification : BaseSqlCommand
    {
        [DataMember]
        public int VehicleID { get; set; }
        [DataMember]
        public int FleetID { get; set; }
        [DataMember]
        public int ReasonID { get; set; }
        [DataMember]
        public DateTime? GPSTime { get; set; }
        [DataMember]
        public DateTime? DeviceTime { get; set; }
        [DataMember]
        public double Latitude { get; set; }
        [DataMember]
        public double Longitude { get; set; }
        [DataMember]
        public int Direction { get; set; }
        [DataMember]
        public int Speed { get; set; }
        [DataMember]
        public string RawGPS { get; set; }
        [DataMember]
        public int Status { get; set; }
        [DataMember]
        public int Flags { get; set; }
        [DataMember]
        public string UserDefined { get; set; }
        [DataMember]
        public int InputStatus { get; set; }
        [DataMember]
        public int OutputStatus { get; set; }
        [DataMember]
        public int LightStatus { get; set; }
        [DataMember]
        public int ButtonStatus { get; set; }
        [DataMember]
        public int MaxSpeed { get; set; }
        [DataMember]
        public int SpeedAcc { get; set; }
        [DataMember]
        public int Samples { get; set; }
        [DataMember]
        public string Spare1 { get; set; }
        [DataMember]
        public string Spare2 { get; set; }
        [DataMember]
        public string Spare3 { get; set; }
        [DataMember]
        public string Spare4 { get; set; }
        [DataMember]
        public string Spare6 { get; set; }
        [DataMember]
        public string Spare10 { get; set; }
        [DataMember]
        public int MaxEngCoolTemp { get; set; }
        [DataMember]
        public int MaxEngOilTemp { get; set; }
        [DataMember]
        public double MinOilPressure { get; set; }
        [DataMember]
        public double FuelEconomy { get; set; }
        [DataMember]
        public double TripFuel { get; set; }
        [DataMember]
        public double TotalFuelUsed { get; set; }
        [DataMember]
        public int MaxEngSpeed { get; set; }
        [DataMember]
        public int GearPosition { get; set; }
        [DataMember]
        public int TotalEngHours { get; set; }
        [DataMember]
        public int BrakeHits { get; set; }
        [DataMember]
        public int Odometer { get; set; }
        [DataMember]
        public double MaxF { get; set; }
        [DataMember]
        public double MaxB { get; set; }
        [DataMember]
        public double MaxLR { get; set; }
        [DataMember]
        public double DistanceFromNearest { get; set; }
        [DataMember]
        public string PositionFromNearest { get; set; }
        [DataMember]
        public string Suburb { get; set; }
        [DataMember]
        public string MapReference { get; set; }
        [DataMember]
        public int SpeedZone1 { get; set; }
        [DataMember]
        public int SpeedZone2 { get; set; }
        [DataMember]
        public int SpeedZone3 { get; set; }
        [DataMember]
        public int SpeedZone4 { get; set; }
        [DataMember]
        public int RPMZone1 { get; set; }
        [DataMember]
        public int RPMZone2 { get; set; }
        [DataMember]
        public int RPMZone3 { get; set; }
        [DataMember]
        public int RPMZone4 { get; set; }
        [DataMember]
        public int BatteryVolts { get; set; }
        [DataMember]
        public int BrakeUsageSeconds { get; set; }
        [DataMember]
        public int CANVehicleSpeed { get; set; }
        [DataMember]
        public byte[] Trailer1 { get; set; }
        [DataMember]
        public byte[] Trailer2 { get; set; }
        [DataMember]
        public byte[] Trailer3 { get; set; }
        [DataMember]
        public byte[] Trailer4 { get; set; }
        [DataMember]
        public byte[] Trailer5 { get; set; }
        [DataMember]
        public long StatusVehicle { get; set; }
        [DataMember]
        public long StatusAuxiliary { get; set; }
        [DataMember]
        public int SetPointGroupID { get; set; }
        [DataMember]
        public int SetPointID { get; set; }
        [DataMember]
        public long VehicleScheduleID { get; set; }
        [DataMember]
        public int CheckPointIndex { get; set; }

        [DataMember]
        public long ReferFlags { get; set; }
        [DataMember]
        public double ReferFuelPercentage { get; set; }
        [DataMember]
        public double ReferBatteryVolts { get; set; }
        [DataMember]
        public bool ReferInput1 { get; set; }
        [DataMember]
        public bool ReferInput2 { get; set; }
        [DataMember]
        public bool ReferInput3 { get; set; }
        [DataMember]
        public bool ReferInput4 { get; set; }
        [DataMember]
        public int ReferSensorsAvailable { get; set; }
        [DataMember]
        public double ReferHumidity { get; set; }
        [DataMember]
        public double ReferSensor1 { get; set; }
        [DataMember]
        public double ReferSensor2 { get; set; }
        [DataMember]
        public double ReferSensor3 { get; set; }
        [DataMember]
        public double ReferSensor4 { get; set; }
        [DataMember]
        public double ReferSensor5 { get; set; }
        [DataMember]
        public double ReferSensor6 { get; set; }
        [DataMember]
        public int ReferZ1Zone { get; set; }
        [DataMember]
        public int ReferZ1OperatingMode { get; set; }
        [DataMember]
        public int ReferZ1Alarms { get; set; }
        [DataMember]
        public int ReferZ1SensorsAvailable { get; set; }
        [DataMember]
        public double ReferZ1ReturnAir1 { get; set; }
        [DataMember]
        public double ReferZ1ReturnAir2 { get; set; }
        [DataMember]
        public double ReferZ1SupplyAir1 { get; set; }
        [DataMember]
        public double ReferZ1SupplyAir2 { get; set; }
        [DataMember]
        public double ReferZ1Setpoint { get; set; }
        [DataMember]
        public double ReferZ1Evap { get; set; }
        [DataMember]
        public int ReferZ2Zone { get; set; }
        [DataMember]
        public int ReferZ2OperatingMode { get; set; }
        [DataMember]
        public int ReferZ2Alarms { get; set; }
        [DataMember]
        public int ReferZ2SensorsAvailable { get; set; }
        [DataMember]
        public double ReferZ2ReturnAir1 { get; set; }
        [DataMember]
        public double ReferZ2ReturnAir2 { get; set; }
        [DataMember]
        public double ReferZ2SupplyAir1 { get; set; }
        [DataMember]
        public double ReferZ2SupplyAir2 { get; set; }
        [DataMember]
        public double ReferZ2Setpoint { get; set; }
        [DataMember]
        public double ReferZ2Evap { get; set; }
        [DataMember]
        public int ReferZ3Zone { get; set; }
        [DataMember]
        public int ReferZ3OperatingMode { get; set; }
        [DataMember]
        public int ReferZ3Alarms { get; set; }
        [DataMember]
        public int ReferZ3SensorsAvailable { get; set; }
        [DataMember]
        public double ReferZ3ReturnAir1 { get; set; }
        [DataMember]
        public double ReferZ3ReturnAir2 { get; set; }
        [DataMember]
        public double ReferZ3SupplyAir1 { get; set; }
        [DataMember]
        public double ReferZ3SupplyAir2 { get; set; }
        [DataMember]
        public double ReferZ3Setpoint { get; set; }
        [DataMember]
        public double ReferZ3Evap { get; set; }

        [DataMember]
        public double HDOP { get; set; }
        [DataMember]
        public int NumOfSatellites { get; set; }
        [DataMember]
        public double MaxZ { get; set; }
        [DataMember]
        public int Altitude { get; set; }
        [DataMember]
        public double GForceTemperature { get; set; }
        [DataMember]
        public bool ReportFromFlash { get; set; }
        [DataMember]
        public bool GPSOdometer { get; set; }
        [DataMember]
        public string StreetName { get; set; }

        [DataMember]
        public double? SpeedLimit { get; set; }

        protected override string GetSql()
        {
            return string.Empty;
        }

        protected override System.Data.SqlClient.SqlCommand GetSqlCommand()
        {
            string gps = string.Empty;
            string device = string.Empty;
            //check if times are in the future
            DateTime now = DateTime.UtcNow;
            if (GPSTime.HasValue)
            {
                if (GPSTime > now.AddDays(1))
                {
                    gps = now.ToString("MM/dd/yyyy HH:mm:ss");
                }
                else
                {
                    gps = GPSTime.Value.ToString("MM/dd/yyyy HH:mm:ss");
                }
            }
            if (DeviceTime.HasValue)
            {
                if (DeviceTime > now.AddDays(1))
                {
                    device = now.ToString("MM/dd/yyyy HH:mm:ss");
                }
                else
                {
                    device = DeviceTime.Value.ToString("MM/dd/yyyy HH:mm:ss");
                }
            }

            SqlCommand cmd = new SqlCommand("InsertIntoVehicleNotification") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add(new SqlParameter("@VehicleID", SqlDbType.Int, 4)).Value = VehicleID;
            cmd.Parameters.Add(new SqlParameter("@FleetID", SqlDbType.Int, 4)).Value = FleetID;
            cmd.Parameters.Add(new SqlParameter("@ReasonID", SqlDbType.Int, 4)).Value = ReasonID;
            cmd.Parameters.Add(new SqlParameter("@GPSTime", SqlDbType.VarChar, 30)).Value = gps;
            cmd.Parameters.Add(new SqlParameter("@DeviceTime", SqlDbType.VarChar, 30)).Value = device;
            cmd.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float, 8)).Value = Latitude;
            cmd.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float, 8)).Value = Longitude;
            cmd.Parameters.Add(new SqlParameter("@Direction", SqlDbType.SmallInt, 2)).Value = Direction;
            cmd.Parameters.Add(new SqlParameter("@Speed", SqlDbType.SmallInt, 2)).Value = Speed;
            cmd.Parameters.Add(new SqlParameter("@RawGPS", SqlDbType.VarChar, 80)).Value = RawGPS;
            cmd.Parameters.Add(new SqlParameter("@Status", SqlDbType.Int, 4)).Value = Status;
            cmd.Parameters.Add(new SqlParameter("@Flags", SqlDbType.Int, 4)).Value = Flags;
            cmd.Parameters.Add(new SqlParameter("@UserDefined", SqlDbType.VarChar, 63)).Value = UserDefined;
            cmd.Parameters.Add(new SqlParameter("@InputStatus", SqlDbType.SmallInt, 2)).Value = InputStatus;
            cmd.Parameters.Add(new SqlParameter("@OutputStatus", SqlDbType.SmallInt, 2)).Value = OutputStatus;
            cmd.Parameters.Add(new SqlParameter("@LightStatus", SqlDbType.SmallInt, 2)).Value = LightStatus;
            cmd.Parameters.Add(new SqlParameter("@ButtonStatus", SqlDbType.SmallInt, 2)).Value = ButtonStatus;
            cmd.Parameters.Add(new SqlParameter("@MaxSpeed", SqlDbType.SmallInt, 2)).Value = MaxSpeed;
            cmd.Parameters.Add(new SqlParameter("@SpeedAcc", SqlDbType.Int, 4)).Value = SpeedAcc;
            cmd.Parameters.Add(new SqlParameter("@Samples", SqlDbType.Int, 4)).Value = Samples;
            cmd.Parameters.Add(new SqlParameter("@Spare1", SqlDbType.VarChar, 255)).Value = Spare1;
            cmd.Parameters.Add(new SqlParameter("@Spare2", SqlDbType.VarChar, 255)).Value = Spare2;
            cmd.Parameters.Add(new SqlParameter("@Spare3", SqlDbType.VarChar, 255)).Value = Spare3;
            cmd.Parameters.Add(new SqlParameter("@Spare4", SqlDbType.VarChar, 255)).Value = Spare4;
            cmd.Parameters.Add(new SqlParameter("@Spare6", SqlDbType.VarChar, 255)).Value = Spare6;
            cmd.Parameters.Add(new SqlParameter("@Spare10", SqlDbType.VarChar, 255)).Value = Spare10;

            // Engine operational parameters:
            cmd.Parameters.Add(new SqlParameter("@MaxEngCoolTemp", SqlDbType.Int, 4)).Value = MaxEngCoolTemp;
            cmd.Parameters.Add(new SqlParameter("@MaxEngOilTemp", SqlDbType.Int, 4)).Value = MaxEngOilTemp;
            cmd.Parameters.Add(new SqlParameter("@MinOilPressure", SqlDbType.Float, 8)).Value = MinOilPressure;

            cmd.Parameters.Add(new SqlParameter("@FuelEconomy", SqlDbType.Float, 8)).Value = FuelEconomy;
            cmd.Parameters.Add(new SqlParameter("@TripFuel", SqlDbType.Float, 8)).Value = TripFuel;
            cmd.Parameters.Add(new SqlParameter("@TotalFuelUsed", SqlDbType.Float, 8)).Value = TotalFuelUsed;

            cmd.Parameters.Add(new SqlParameter("@MaxEngSpeed", SqlDbType.Int, 4)).Value = MaxEngSpeed;
            cmd.Parameters.Add(new SqlParameter("@GearPosition", SqlDbType.Int, 4)).Value = GearPosition;
            cmd.Parameters.Add(new SqlParameter("@TotalEngHours", SqlDbType.Int, 4)).Value = TotalEngHours;
            cmd.Parameters.Add(new SqlParameter("@BrakeHits", SqlDbType.Int, 4)).Value = BrakeHits;
            cmd.Parameters.Add(new SqlParameter("@Odometer", SqlDbType.Int, 4)).Value = Odometer;
            // G Forces:
            cmd.Parameters.Add(new SqlParameter("@MaxF", SqlDbType.Float, 8)).Value = MaxF;
            cmd.Parameters.Add(new SqlParameter("@MaxB", SqlDbType.Float, 8)).Value = MaxB;
            cmd.Parameters.Add(new SqlParameter("@MaxLR", SqlDbType.Float, 8)).Value = MaxLR;

            cmd.Parameters.Add(new SqlParameter("@DistanceFromNearest", SqlDbType.Float, 8)).Value = DistanceFromNearest;
            cmd.Parameters.Add(new SqlParameter("@PositionFromNearest", SqlDbType.VarChar, 7)).Value = PositionFromNearest;
            cmd.Parameters.Add(new SqlParameter("@Suburb", SqlDbType.VarChar, 63)).Value = Suburb;
            cmd.Parameters.Add(new SqlParameter("@MapReference", SqlDbType.VarChar, 15)).Value = MapReference;

            cmd.Parameters.Add(new SqlParameter("@SpeedZone1", SqlDbType.Int, 4)).Value = SpeedZone1;
            cmd.Parameters.Add(new SqlParameter("@SpeedZone2", SqlDbType.Int, 4)).Value = SpeedZone2;
            cmd.Parameters.Add(new SqlParameter("@SpeedZone3", SqlDbType.Int, 4)).Value = SpeedZone3;
            cmd.Parameters.Add(new SqlParameter("@SpeedZone4", SqlDbType.Int, 4)).Value = SpeedZone4;

            cmd.Parameters.Add(new SqlParameter("@RPMZone1", SqlDbType.Int, 4)).Value = RPMZone1;
            cmd.Parameters.Add(new SqlParameter("@RPMZone2", SqlDbType.Int, 4)).Value = RPMZone2;
            cmd.Parameters.Add(new SqlParameter("@RPMZone3", SqlDbType.Int, 4)).Value = RPMZone3;
            cmd.Parameters.Add(new SqlParameter("@RPMZone4", SqlDbType.Int, 4)).Value = RPMZone4;

            cmd.Parameters.Add(new SqlParameter("@BatteryVolts", SqlDbType.Int, 4)).Value = BatteryVolts;
            cmd.Parameters.Add(new SqlParameter("@BrakeUsageSeconds", SqlDbType.Int, 4)).Value = BrakeUsageSeconds;
            cmd.Parameters.Add(new SqlParameter("@CANVehicleSpeed", SqlDbType.Int, 4)).Value = CANVehicleSpeed;

            cmd.Parameters.Add(new SqlParameter("@Trailer1", SqlDbType.Binary, 6)).Value = Trailer1;
            cmd.Parameters.Add(new SqlParameter("@Trailer2", SqlDbType.Binary, 6)).Value = Trailer2;
            cmd.Parameters.Add(new SqlParameter("@Trailer3", SqlDbType.Binary, 6)).Value = Trailer3;
            cmd.Parameters.Add(new SqlParameter("@Trailer4", SqlDbType.Binary, 6)).Value = Trailer4;
            cmd.Parameters.Add(new SqlParameter("@Trailer5", SqlDbType.Binary, 6)).Value = Trailer5;

            cmd.Parameters.Add(new SqlParameter("@StatusVehicle", SqlDbType.BigInt, 8)).Value = StatusVehicle;
            cmd.Parameters.Add(new SqlParameter("@StatusAuxiliary", SqlDbType.BigInt, 8)).Value = StatusAuxiliary;
            cmd.Parameters.Add(new SqlParameter("@SetPointGroupID", SqlDbType.Int, 4)).Value = SetPointGroupID;
            cmd.Parameters.Add(new SqlParameter("@SetPointID", SqlDbType.Int, 4)).Value = SetPointID;
            cmd.Parameters.Add(new SqlParameter("@VehicleScheduleID", SqlDbType.BigInt, 8)).Value = VehicleScheduleID;
            cmd.Parameters.Add(new SqlParameter("@CheckPointIndex", SqlDbType.Int, 4)).Value = CheckPointIndex;

            cmd.Parameters.Add(new SqlParameter("@ReferFlags", SqlDbType.BigInt, 8)).Value = ReferFlags;
            cmd.Parameters.Add(new SqlParameter("@ReferFuelPercentage", SqlDbType.Float, 8)).Value = ReferFuelPercentage;
            cmd.Parameters.Add(new SqlParameter("@ReferBatteryVolts", SqlDbType.Float, 8)).Value = ReferBatteryVolts;
            cmd.Parameters.Add(new SqlParameter("@ReferInput1", SqlDbType.Bit, 1)).Value = ReferInput1;
            cmd.Parameters.Add(new SqlParameter("@ReferInput2", SqlDbType.Bit, 1)).Value = ReferInput2;
            cmd.Parameters.Add(new SqlParameter("@ReferInput3", SqlDbType.Bit, 1)).Value = ReferInput3;
            cmd.Parameters.Add(new SqlParameter("@ReferInput4", SqlDbType.Bit, 1)).Value = ReferInput4;
            cmd.Parameters.Add(new SqlParameter("@ReferSensorsAvailable", SqlDbType.Int, 4)).Value = ReferSensorsAvailable;
            cmd.Parameters.Add(new SqlParameter("@ReferHumidity", SqlDbType.Float, 8)).Value = ReferHumidity;
            cmd.Parameters.Add(new SqlParameter("@ReferSensor1", SqlDbType.Float, 8)).Value = ReferSensor1;
            cmd.Parameters.Add(new SqlParameter("@ReferSensor2", SqlDbType.Float, 8)).Value = ReferSensor2;
            cmd.Parameters.Add(new SqlParameter("@ReferSensor3", SqlDbType.Float, 8)).Value = ReferSensor3;
            cmd.Parameters.Add(new SqlParameter("@ReferSensor4", SqlDbType.Float, 8)).Value = ReferSensor4;
            cmd.Parameters.Add(new SqlParameter("@ReferSensor5", SqlDbType.Float, 8)).Value = ReferSensor5;
            cmd.Parameters.Add(new SqlParameter("@ReferSensor6", SqlDbType.Float, 8)).Value = ReferSensor6;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1Zone", SqlDbType.Int, 4)).Value = ReferZ1Zone;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1OperatingMode", SqlDbType.Int, 4)).Value = ReferZ1OperatingMode;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1Alarms", SqlDbType.Int, 4)).Value = ReferZ1Alarms;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1SensorsAvailable", SqlDbType.Int, 4)).Value = ReferZ1SensorsAvailable;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1ReturnAir1", SqlDbType.Float, 8)).Value = ReferZ1ReturnAir1;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1ReturnAir2", SqlDbType.Float, 8)).Value = ReferZ1ReturnAir2;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1SupplyAir1", SqlDbType.Float, 8)).Value = ReferZ1SupplyAir1;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1SupplyAir2", SqlDbType.Float, 8)).Value = ReferZ1SupplyAir2;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1Setpoint", SqlDbType.Float, 8)).Value = ReferZ1Setpoint;
            cmd.Parameters.Add(new SqlParameter("@ReferZ1Evap", SqlDbType.Float, 8)).Value = ReferZ1Evap;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2Zone", SqlDbType.Int, 4)).Value = ReferZ2Zone;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2OperatingMode", SqlDbType.Int, 4)).Value = ReferZ2OperatingMode;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2Alarms", SqlDbType.Int, 4)).Value = ReferZ2Alarms;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2SensorsAvailable", SqlDbType.Int, 4)).Value = ReferZ2SensorsAvailable;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2ReturnAir1", SqlDbType.Float, 8)).Value = ReferZ2ReturnAir1;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2ReturnAir2", SqlDbType.Float, 8)).Value = ReferZ2ReturnAir2;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2SupplyAir1", SqlDbType.Float, 8)).Value = ReferZ2SupplyAir1;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2SupplyAir2", SqlDbType.Float, 8)).Value = ReferZ2SupplyAir2;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2Setpoint", SqlDbType.Float, 8)).Value = ReferZ2Setpoint;
            cmd.Parameters.Add(new SqlParameter("@ReferZ2Evap", SqlDbType.Float, 8)).Value = ReferZ2Evap;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3Zone", SqlDbType.Int, 4)).Value = ReferZ3Zone;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3OperatingMode", SqlDbType.Int, 4)).Value = ReferZ3OperatingMode;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3Alarms", SqlDbType.Int, 4)).Value = ReferZ3Alarms;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3SensorsAvailable", SqlDbType.Int, 4)).Value = ReferZ3SensorsAvailable;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3ReturnAir1", SqlDbType.Float, 8)).Value = ReferZ3ReturnAir1;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3ReturnAir2", SqlDbType.Float, 8)).Value = ReferZ3ReturnAir2;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3SupplyAir1", SqlDbType.Float, 8)).Value = ReferZ3SupplyAir1;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3SupplyAir2", SqlDbType.Float, 8)).Value = ReferZ3SupplyAir2;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3Setpoint", SqlDbType.Float, 8)).Value = ReferZ3Setpoint;
            cmd.Parameters.Add(new SqlParameter("@ReferZ3Evap", SqlDbType.Float, 8)).Value = ReferZ3Evap;

            cmd.Parameters.Add(new SqlParameter("@HDOP", SqlDbType.Float, 8)).Value = HDOP;
            cmd.Parameters.Add(new SqlParameter("@NumOfSatellites", SqlDbType.TinyInt, 1)).Value = NumOfSatellites;
            cmd.Parameters.Add(new SqlParameter("@MaxZ", SqlDbType.Float, 8)).Value = MaxZ;
            cmd.Parameters.Add(new SqlParameter("@Altitude", SqlDbType.SmallInt, 2)).Value = Altitude;
            cmd.Parameters.Add(new SqlParameter("@GForceTemperature", SqlDbType.Float, 8)).Value = GForceTemperature;
            cmd.Parameters.Add(new SqlParameter("@ReportFromFlash", SqlDbType.Bit, 1)).Value = ReportFromFlash;
            cmd.Parameters.Add(new SqlParameter("@GPSOdometer", SqlDbType.Bit, 1)).Value = GPSOdometer;
            cmd.Parameters.Add(new SqlParameter("@StreetName", SqlDbType.NVarChar, 255)).Value = StreetName;
            cmd.Parameters.Add(new SqlParameter("@SpeedLimit", SqlDbType.Float)).Value = SpeedLimit ?? (object)DBNull.Value;

            return cmd;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = false;
        }

        public override string ToString()
        {
            return string.Format("VehicleNotification for unit {0}/{1} with reason code {2} at {3}", FleetID, VehicleID, ReasonID, DeviceTime);
        }

    }
}

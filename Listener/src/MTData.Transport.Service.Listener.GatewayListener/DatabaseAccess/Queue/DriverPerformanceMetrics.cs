﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class DriverPerformanceMetrics : BaseSqlCommand
    {
        [DataMember]
        public int FleetID { get; set; }
        [DataMember]
        public int VehicleID { get; set; }
        [DataMember]
        public string DeviceTime { get; set; }
        [DataMember]
        public int DrivingTime { get; set; }
        [DataMember]
        public int IdleTime { get; set; }
        [DataMember]
        public int ExcessiveIdleTime { get; set; }
        [DataMember]
        public int IgnitionOnTime { get; set; }
        [DataMember]
        public int WarmUpTime { get; set; }
        [DataMember]
        public int BrakeCount { get; set; }
        [DataMember]
        public int BrakeRoll { get; set; }
        [DataMember]
        public int HardBrakingCounts { get; set; }
        [DataMember]
        public int HardCorneringCounts { get; set; }
        [DataMember]
        public int HardAccelerationCounts { get; set; }
        [DataMember]
        public int SustainedGForceEvents { get; set; }
        [DataMember]
        public int AccidentBufferEvents { get; set; }
        [DataMember]
        public int BrakeOnTime { get; set; }


        protected override string GetSql()
        {
            return string.Format("exec usp_SaveDriverMetrics {0}, {1}, '{2}', {3}, {4}, {5}, {6}, {7}, {8}, {9}, {10}, {11}, {12}, {13}, {14}, {15}",
                FleetID, VehicleID, DeviceTime, DrivingTime, IdleTime, ExcessiveIdleTime, IgnitionOnTime, WarmUpTime, BrakeCount, 
                BrakeRoll, HardBrakingCounts, HardCorneringCounts, HardAccelerationCounts, SustainedGForceEvents, AccidentBufferEvents, BrakeOnTime);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("DriverPerformanceMetrics for unit {0}/{1} at {2}", FleetID, VehicleID, DeviceTime);
        }

    }
}

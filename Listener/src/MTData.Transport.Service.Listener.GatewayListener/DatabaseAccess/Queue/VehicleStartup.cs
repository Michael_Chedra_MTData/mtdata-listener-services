﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class VehicleStartup : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }
        [DataMember]
        public string LastKnownGPSTime { get; set; }
        [DataMember]
        public string LastKnownDeviceTime { get; set; }
        [DataMember]
        public string StartupReportTime { get; set; }
        [DataMember]
        public double LastKnownLat { get; set; }
        [DataMember]
        public double LastKnownLong { get; set; }
        [DataMember]
        public int Startup { get; set; }
        [DataMember]
        public int FirstIgnition { get; set; }
        [DataMember]
        public int GPRSActive { get; set; }
        [DataMember]
        public int GPSAquired { get; set; }
        [DataMember]
        public int FirstCTA { get; set; }


        protected override string GetSql()
        {
            return string.Format("exec usp_VehicleStartUpReport {0}, {1}, '{2}', '{3}', '{4}', {5}, {6}, {7}, {8}, {9}, {10}, {11}",
                StorageTable, TableID, LastKnownGPSTime, LastKnownDeviceTime, StartupReportTime, LastKnownLat, LastKnownLong,
                Startup, FirstIgnition, GPRSActive, GPSAquired, FirstCTA);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("VehicleStartup for tableId {0}", TableID);
        }

    }
}

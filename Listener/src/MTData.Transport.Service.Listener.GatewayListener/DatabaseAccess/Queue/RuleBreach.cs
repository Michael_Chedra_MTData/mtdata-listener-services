﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class RuleBreach : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }
        [DataMember]
        public int LastKnownDeviceTime { get; set; }
        [DataMember]
        public int RuleSetId { get; set; }
        [DataMember]
        public int RuleSetVersion { get; set; }
        [DataMember]
        public int RuleId { get; set; }
        [DataMember]
        public int LocationFileId { get; set; }
        [DataMember]
        public int LocationFileVersion { get; set; }
        [DataMember]
        public int LocationId { get; set; }
        [DataMember]
        public string Generic { get; set; }


        protected override string GetSql()
        {
            string generic = Generic == null ? "NULL" : string.Format("'{0}'", Generic);
            return string.Format("exec usp_InsertVNRuleBreach {0}, {1}, {2}, {3}, {4}, {5}, {6}, {7}, {8}",
                StorageTable, TableID, RuleSetId, RuleSetVersion, RuleId, LocationFileId, LocationFileVersion,
                LocationId, generic);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("RuleBreach for tableId {0}, RuleId {1}", TableID, RuleId);
        }

    }
}

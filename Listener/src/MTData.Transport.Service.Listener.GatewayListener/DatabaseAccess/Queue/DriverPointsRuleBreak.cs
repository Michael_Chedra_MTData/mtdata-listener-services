﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class DriverPointsRuleBreak : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }
        [DataMember]
        public int RuleId { get; set; }
        [DataMember]
        public string RuleBreakTime { get; set; }
        [DataMember]
        public int VehicleID { get; set; }
        [DataMember]
        public int FleetID { get; set; }

        protected override string GetSql()
        {
            return string.Format("exec usp_DriverPointsRuleBreak {0}, {1}, {2}, '{3}', {4}, {5}",
                StorageTable, TableID, RuleId, RuleBreakTime, FleetID, VehicleID);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("DriverPointsRuleBreak for tableId {0}, RuleId {1} at {2}", TableID, RuleId, RuleBreakTime);
        }
    }
}

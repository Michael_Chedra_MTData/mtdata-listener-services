﻿using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    /// <summary>
    /// Update the DVR alarm entry with a link to the vehicle notification table id
    /// </summary>
    [DataContract]
    public class DvrAlarmItem : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }

        [DataMember]
        public int DvrAlarmId { get; set; }

        protected override string GetSql()
        {
            return string.Format("UPDATE T_DvrAlarm SET NotificationId = {0} WHERE ID = {1}", TableID, DvrAlarmId);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("DvrAlarm for tableId {0}, DvrAlarmId {1}", TableID, DvrAlarmId);
        }
    }
}

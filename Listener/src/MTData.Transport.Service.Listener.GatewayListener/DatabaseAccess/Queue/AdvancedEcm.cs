﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class AdvancedEcm : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }
        [DataMember]
        public int SourceType { get; set; }
        [DataMember]
        public int SpnId { get; set; }
        [DataMember]
        public int Flags { get; set; }
        [DataMember]
        public byte[] Value { get; set; }
        [DataMember]
        public double ValueFloat { get; set; }



        protected override string GetSql()
        {
            return string.Empty;
        }

        protected override SqlCommand GetSqlCommand()
        {
            SqlCommand cmd = new SqlCommand("isp_VehicleECMAdvanceInsert") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add("@StorageTable", SqlDbType.Int, 4).Value = StorageTable;
            cmd.Parameters.Add("@TableID", SqlDbType.BigInt, 8).Value = TableID;
            cmd.Parameters.Add("@SourceType", SqlDbType.TinyInt, 1).Value = SourceType;
            cmd.Parameters.Add("@SPNID", SqlDbType.Int, 4).Value = SpnId;
            cmd.Parameters.Add("@Flags", SqlDbType.TinyInt, 1).Value = Flags;
            if (Value == null)
            {
                Value = new byte[0];
            }
            cmd.Parameters.Add("@Value", SqlDbType.VarBinary, 8).Value = Value;
            cmd.Parameters.Add("@ValueFloat", SqlDbType.Float, 8).Value = ValueFloat;
            return cmd;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = false;
        }

        public override string ToString()
        {
            return string.Format("AdvancedECM for tableId {0}, SourceType {1} SpnId {2}", TableID, SourceType, SpnId);
        }

    }
}

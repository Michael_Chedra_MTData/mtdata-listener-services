﻿using System;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class DSSEventItem : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }
        [DataMember]
        public long DSSEventId { get; set; }
        
        protected override string GetSql()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(string.Format("UPDATE T_DSSEvent SET StorageTable = {0}, NotificationId = {1} WHERE DSSEventId = {2}", StorageTable, TableID, DSSEventId));

            return stringBuilder.ToString();
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("DSSEvent for tableId {0}, DSSEventId {1}", TableID, DSSEventId);
        }
    }
}

﻿using System;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class DvrStatusItem : BaseSqlCommand
    {
        [DataMember]
        public int FleetID { get; set; }
        [DataMember]
        public uint VehicleID { get; set; }
        [DataMember]
        public string DeviceName { get; set; }
        [DataMember]
        public int DeviceType { get; set; }
        [DataMember]
        public DateTime DateTime { get; set; }

        [DataMember]
        public int ErrorCode { get; set; }
        [DataMember]
        public bool Connected { get; set; }

        [DataMember]
        public bool RecordOn { get; set; }
        [DataMember]
        public short VideoConnectedChannel { get; set; }
        [DataMember]
        public short VideoRecordChannel { get; set; }

        [DataMember]
        public bool HddOk { get; set; }
        [DataMember]
        public short HddSize { get; set; }
        [DataMember]
        public short HddFree { get; set; }
        [DataMember]
        public int TimeOffset { get; set; }
        [DataMember]
        public string FirmwareVersion { get; set; }
        [DataMember]
        public string Model { get; set; }
        [DataMember]
        public string VideoFormat { get; set; }
        [DataMember]
        public bool IsHealthStatus { get; set; }

        protected override string GetSql()
        {
            return string.Empty;
        }

        protected override SqlCommand GetSqlCommand()
        {
            SqlCommand cmd = new SqlCommand("usp_DvrStatusAdd") { CommandType = CommandType.StoredProcedure };
            cmd.Parameters.Add("@FleetID", SqlDbType.Int, 4).Value = FleetID;
            cmd.Parameters.Add("@VehicleID", SqlDbType.Int, 4).Value = VehicleID;
            cmd.Parameters.Add("@DeviceName", SqlDbType.Text).Value = DeviceName;
            cmd.Parameters.Add("@DeviceType", SqlDbType.Int, 4).Value = DeviceType;
            cmd.Parameters.Add("@Timestamp", SqlDbType.VarChar, 30).Value = DateTime.ToString("MM/dd/yyyy HH:mm:ss");

            cmd.Parameters.Add("@ErrorCode", SqlDbType.Int, 4).Value = ErrorCode;
            cmd.Parameters.Add("@Connected", SqlDbType.Bit, 1).Value = Connected;
            cmd.Parameters.Add("@HddSize", SqlDbType.SmallInt, 2).Value = HddSize;
            cmd.Parameters.Add("@HddFree", SqlDbType.SmallInt, 2).Value = HddFree;
            cmd.Parameters.Add("@HddOk", SqlDbType.Bit, 1).Value = HddOk;
            cmd.Parameters.Add("@RecordOn", SqlDbType.Bit, 1).Value = RecordOn;
            cmd.Parameters.Add("@VideoConnectedChannel", SqlDbType.SmallInt, 2).Value = VideoConnectedChannel;
            cmd.Parameters.Add("@VideoRecordChannel", SqlDbType.SmallInt, 2).Value = VideoRecordChannel;
            cmd.Parameters.Add("@TimeOffset", SqlDbType.Int, 4).Value = TimeOffset;
            cmd.Parameters.Add("@FirmwareVersion", SqlDbType.Text).Value = FirmwareVersion;
            cmd.Parameters.Add("@Model", SqlDbType.Text).Value = Model;
            cmd.Parameters.Add("@Video", SqlDbType.Text).Value = VideoFormat;
            cmd.Parameters.Add("@IsHealthStatus", SqlDbType.Bit, 1).Value = IsHealthStatus;

            return cmd;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = false;
        }

        public override string ToString()
        {
            return string.Format("DvrStatusItem for FleetId:{0}, VehicleId:{1}, Device:{2}, Timestamp:{3}, ErrorCode:{4}, Connected:{5}, HddOk:{6}, HddSize:{7}, HddFree:{8}, RecordOn:{9}, VideoConnectedChannel:{10}, VideoRecordChannel:{11}, TimeOffset:{12}, FirmwareVersion:{13}, Model:{14}, VideoFormat:{15}", 
                FleetID, VehicleID, DeviceName, DateTime, ErrorCode, Connected, HddOk, HddSize, HddFree, RecordOn, VideoConnectedChannel, VideoRecordChannel, TimeOffset, FirmwareVersion, Model, VideoFormat);
        }
    }
}

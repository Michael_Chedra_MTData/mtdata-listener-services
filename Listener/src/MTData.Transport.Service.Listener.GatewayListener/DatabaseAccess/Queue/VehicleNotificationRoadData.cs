﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class VehicleNotificationRoadData : BaseSqlCommand
    {
        [DataMember]
        public long SegmentId { get; set; }

        [DataMember]
        public double SpeedLimit { get; set; }

        [DataMember]
        public double OverrideSpeedLimit { get; set; }

        [DataMember]
        public byte SchoolZone { get; set; }

        [DataMember]
        public byte IsPrivate { get; set; }

        [DataMember]
        public byte RoadType { get; set; }

        [DataMember]
        public string StreetNumber { get; set; }

        [DataMember]
        public string Street { get; set; }

        [DataMember]
        public string Suburb { get; set; }

        [DataMember]
        public string City { get; set; }

        [DataMember]
        public string State { get; set; }

        [DataMember]
        public string Country { get; set; }

        [DataMember]
        public string PostCode { get; set; }

        protected override string GetSql()
        {
            return string.Empty;
        }

        protected override SqlCommand GetSqlCommand()
        {
            SqlCommand command = new SqlCommand() { CommandType = CommandType.Text };
            command.CommandText = @"INSERT INTO T_VehicleNotificationRoadData(TableId, SegmentId, SpeedLimit, OverrideSpeedLimit, SchoolZone, IsPrivate, RoadType, StreetNumber, Street, Suburb, City, State, Country, PostCode)
                                    VALUES (@TableId, @SegmentId, @SpeedLimit, @OverrideSpeedLimit, @SchoolZone, @IsPrivate, @RoadType, '@StreetNumber','@Street', '@Suburb', '@City', '@State', '@Country', '@PostCode')";

            command.Parameters.Add(new SqlParameter {SqlDbType = SqlDbType.BigInt, Value = TableID, ParameterName = "@TableId"});
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.BigInt, Value = SegmentId, ParameterName = "@SegmentId" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.Float, Value = SpeedLimit, ParameterName = "@SpeedLimit" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.Float, Value = OverrideSpeedLimit, ParameterName = "@OverrideSpeedLimit" });            
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.TinyInt, Value = SchoolZone, ParameterName = "@SchoolZone" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.TinyInt, Value = IsPrivate, ParameterName = "@IsPrivate" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.TinyInt, Value = RoadType, ParameterName = "@RoadType" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = StreetNumber ?? "NULL", ParameterName = "@StreetNumber" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = Street ?? "NULL", ParameterName = "@Street" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = Suburb ?? "NULL", ParameterName = "@Suburb" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = City ?? "NULL", ParameterName = "@City" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = State ?? "NULL", ParameterName = "@State" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = Country ?? "NULL", ParameterName = "@Country" });
            command.Parameters.Add(new SqlParameter { SqlDbType = SqlDbType.NVarChar, Value = PostCode ?? "NULL", ParameterName = "@PostCode" });

            return command;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = false;
        }

        public override string ToString()
        {
            return string.Format("VehicleNotificationRoadData for tableId {0} ", TableID);
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class DriverDataAccumulator : BaseSqlCommand
    {
        [DataMember]
        public int FleetID { get; set; }
        [DataMember]
        public int VehicleID { get; set; }
        [DataMember]
        public string DeviceTime { get; set; }
        [DataMember]
        public int SPNType { get; set; }
        [DataMember]
        public int SPNValue { get; set; }
        [DataMember]
        public int DataAccumulatorTypeID { get; set; }
        [DataMember]
        public string Data { get; set; }

        protected override string GetSql()
        {
            return string.Format("exec usp_SaveDriverDataAccumulator {0}, {1}, '{2}', {3}, {4}, {5}, '{6}'",
                FleetID, VehicleID, DeviceTime, SPNType, SPNValue, DataAccumulatorTypeID, Data);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("DriverDataAccumulator for unit {0}/{1} at {2}", FleetID, VehicleID, DeviceTime);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class ConfigEcmData : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }
        [DataMember]
        public int EcmId { get; set; }
        [DataMember]
        public long Raw { get; set; }
        [DataMember]
        public double Processed { get; set; }
        [DataMember]
        public long PreviousRaw { get; set; }
        [DataMember]
        public double PreviousProcessed { get; set; }

        protected override string GetSql()
        {
            return string.Format("exec isp_ConfigEcmDataInsert {0}, {1}, {2}, {3}, {4}, {5}, {6}",
                StorageTable, TableID, EcmId, Raw, Processed, PreviousRaw, PreviousProcessed);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("EcmHysteresisError for tableId {0}, EcmId {1}", TableID, EcmId);
        }

    }
}

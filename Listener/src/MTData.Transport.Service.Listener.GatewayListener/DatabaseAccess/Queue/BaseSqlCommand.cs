﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using System.Data;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    [KnownType(typeof(AdvancedEcm))]
    [KnownType(typeof(ConfigEcmData))]
    [KnownType(typeof(DriverDataAccumulator))]
    [KnownType(typeof(DriverPerformanceMetrics))]
    [KnownType(typeof(DriverPointsRuleBreak))]
    [KnownType(typeof(DSSEventItem))]
    [KnownType(typeof(DvrAlarmItem))]
    [KnownType(typeof(DvrRequestItem))]
    [KnownType(typeof(DvrStatusItem))]
    [KnownType(typeof(ListenerLog))]
    [KnownType(typeof(RuleBreach))]
    [KnownType(typeof(UpdateAssetRegister))]
    [KnownType(typeof(VehicleNotification))]
    [KnownType(typeof(VehicleNotificationRoadData))]
    [KnownType(typeof(VehicleStartup))]
    public abstract class BaseSqlCommand
    {
        [DataMember]
        public long TableID { get; set; }

        public bool SimpleCommand { get; set; }
        public bool SavedToDatabase { get; private set; }
        public long TransactionTime { get; set; }

        protected abstract string GetSql();
        protected abstract SqlCommand GetSqlCommand();

        public object ExecuteCommand(SqlConnection connection, int timeOut)
        {
            object retVal = null;
            if (SimpleCommand)
            {
                using (SqlCommand cmd = new SqlCommand(GetSql(), connection))
                {
                    cmd.CommandTimeout = timeOut;
                    long start = DateTime.Now.Ticks;
                    retVal = cmd.ExecuteScalar();
                    TransactionTime = DateTime.Now.Ticks - start;
                }
            }
            else
            {
                using (SqlCommand cmd = GetSqlCommand())
                {
                    cmd.Connection = connection;
                    cmd.CommandTimeout = timeOut;
                    long start = DateTime.Now.Ticks;
                    retVal = cmd.ExecuteScalar();
                    TransactionTime = DateTime.Now.Ticks - start;
                }
            }
            SavedToDatabase = true;
            return retVal;
        }

        public string GetSqlStatement()
        {
            if (SimpleCommand)
            {
                return GetSql();
            }
            else
            {
                StringBuilder str = new StringBuilder();
                using (SqlCommand cmd = GetSqlCommand())
                {
                    str.AppendFormat("exec {0} ", cmd.CommandText);
                    bool first = true;
                    foreach (SqlParameter p in cmd.Parameters)
                    {
                        if (p.Value == null)
                        {
                            if (first)
                            {
                                str.Append("null");
                                first = false;
                            }
                            else
                            {
                                str.Append(", null");
                            }
                        }
                        else
                        {

                            switch (p.SqlDbType)
                            {
                                case SqlDbType.Binary:
                                case SqlDbType.Image:
                                case SqlDbType.VarBinary:
                                    if (first)
                                    {
                                        str.AppendFormat("0x{0}", BitConverter.ToString((byte[])p.Value));
                                        first = false;
                                    }
                                    else
                                    {
                                        str.AppendFormat(", 0x{0}", BitConverter.ToString((byte[])p.Value));
                                    }
                                    break;
                                case SqlDbType.Char:
                                case SqlDbType.NChar:
                                case SqlDbType.NText:
                                case SqlDbType.NVarChar:
                                case SqlDbType.Text:
                                case SqlDbType.UniqueIdentifier:
                                case SqlDbType.VarChar:
                                    if (first)
                                    {
                                        str.AppendFormat("'{0}'", p.Value.ToString().Replace("'", "''"));
                                        first = false;
                                    }
                                    else
                                    {
                                        str.AppendFormat(", '{0}'", p.Value.ToString().Replace("'", "''"));
                                    }
                                    break;
                                case SqlDbType.DateTime:
                                case SqlDbType.SmallDateTime:
                                case SqlDbType.Timestamp:
                                    if (first)
                                    {
                                        str.AppendFormat("'{0}'", Convert.ToDateTime(p.Value).ToString("MM/dd/yyyy HH:mm:ss"));
                                        first = false;
                                    }
                                    else
                                    {
                                        str.AppendFormat(", '{0}'", Convert.ToDateTime(p.Value).ToString("MM/dd/yyyy HH:mm:ss"));
                                    }
                                    break;
                                default:
                                    if (first)
                                    {
                                        str.AppendFormat("{0}", p.Value);
                                        first = false;
                                    }
                                    else
                                    {
                                        str.AppendFormat(", {0}", p.Value);
                                    }
                                    break;
                            }
                        }
                    }
                }
                return str.ToString();
            }
        }
    }
}

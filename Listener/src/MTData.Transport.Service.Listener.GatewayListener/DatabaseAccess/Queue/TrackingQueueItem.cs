﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Runtime.Serialization;
using System.Data.SqlClient;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class TrackingQueueItem
    {
        private static ILog _log = LogManager.GetLogger(typeof(TrackingQueueItem));

        [DataMember]
        public VehicleNotification InsertIntoVehicleNotification { get; set; }

        [DataMember]
        public List<BaseSqlCommand> SubCommands { get; set; }

        [DataMember]
        public long TimePutOnQueue { get; set; }

        [DataMember]
        public long AddToQueueTime { get; set; }
        public long DequeueTime { get; set; }

        public long QueueTime { get; set; }
        public long TransactionTime { get; set; }

        private long TableId
        {
            get
            {
                if (InsertIntoVehicleNotification != null)
                {
                    return InsertIntoVehicleNotification.TableID;
                }
                return 0;
            }
        }

        public TrackingQueueItem()
        {
            SubCommands = new List<BaseSqlCommand>();
        }

        public void ExecuteCommand(SqlConnection connection, int timeOut, Action<long, AdvancedEcm> action)
        {
            long start = DateTime.Now.Ticks;
            bool runSubCommands = true;
            //run the insert command
            if (InsertIntoVehicleNotification != null)
            {
                if (!InsertIntoVehicleNotification.SavedToDatabase)
                {
                    InsertIntoVehicleNotification.TableID = Convert.ToInt64(InsertIntoVehicleNotification.ExecuteCommand(connection, timeOut));
                }
                if (InsertIntoVehicleNotification.TableID <= 0)
                {
                    runSubCommands = false;
                }
            }

            //run all sub commands
            if (runSubCommands && SubCommands != null)
            {
                List<Tuple<int, int, int>> spns = new List<Tuple<int, int, int>>();
                foreach (BaseSqlCommand cmd in SubCommands)
                {
                    if (!cmd.SavedToDatabase)
                    {
                        cmd.TableID = TableId;
                        AdvancedEcm spnCmd = cmd as AdvancedEcm;
                        if (spnCmd != null)
                        {
                            Tuple<int, int, int> t = new Tuple<int, int, int>(spnCmd.SourceType, spnCmd.SpnId, spnCmd.Flags);
                            if (!spns.Contains(t))
                            {
                                spns.Add(t);
                                action.Invoke(TableId, spnCmd);
                            }
                        }
                        else
                        {
                            cmd.ExecuteCommand(connection, timeOut);
                        }
                    }
                }
            }
            TransactionTime = DateTime.Now.Ticks - start;
        }

        public override string ToString()
        {
            if (InsertIntoVehicleNotification != null)
            {
                return InsertIntoVehicleNotification.ToString();
            }
            else if (SubCommands != null && SubCommands.Count > 0)
            {
                return SubCommands[0].ToString();
            }
            return "No Commands";
        }

        public string GetLastSqlStatement()
        {
            try
            {
                if (InsertIntoVehicleNotification != null && !InsertIntoVehicleNotification.SavedToDatabase)
                {
                    return InsertIntoVehicleNotification.GetSqlStatement();
                }
                if (SubCommands != null)
                {
                    foreach (BaseSqlCommand cmd in SubCommands)
                    {
                        if (!cmd.SavedToDatabase)
                        {
                            return cmd.GetSqlStatement();
                        }
                    }
                }
            }
            catch (Exception exp)
            {
                _log.Error(ToString());
                _log.Error(exp);
                if (InsertIntoVehicleNotification != null && !InsertIntoVehicleNotification.SavedToDatabase)
                {
                    return InsertIntoVehicleNotification.ToString();
                }
                if (SubCommands != null)
                {
                    foreach (BaseSqlCommand cmd in SubCommands)
                    {
                        if (!cmd.SavedToDatabase)
                        {
                            return cmd.ToString();
                        }
                    }
                }

            }
            return string.Empty;
        }

        public string GetStatsString()
        {
            StringBuilder str = new StringBuilder();
            str.AppendLine(string.Format("AddToQueue = {2:0.0} Queued = {0:0.0}, Dequeue = {3:0.0} Transaction = {1:0.0}", ConvertToMS(QueueTime),
                ConvertToMS(TransactionTime), ConvertToMS(AddToQueueTime), ConvertToMS(DequeueTime)));
            if (InsertIntoVehicleNotification != null)
            {
                str.AppendLine(string.Format("Transaction = {0:0.0} for {1}", ConvertToMS(InsertIntoVehicleNotification.TransactionTime), InsertIntoVehicleNotification.ToString()));
            }

            //run all sub commands
            if (SubCommands != null)
            {
                foreach (BaseSqlCommand cmd in SubCommands)
                {
                    str.AppendLine(string.Format("Transaction = {0:0.0} for {1}", ConvertToMS(cmd.TransactionTime), cmd.ToString()));
                }
            }
            return str.ToString();
        }

        private double ConvertToMS(long ticks)
        {
            return Convert.ToDouble(ticks) / 10000d;
        }

    }
}

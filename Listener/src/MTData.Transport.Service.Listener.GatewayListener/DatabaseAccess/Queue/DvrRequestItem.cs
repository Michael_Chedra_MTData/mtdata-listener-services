﻿using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    /// <summary>
    /// Update the DVR request entry with a link to the vehicle notification table id
    /// </summary>
    [DataContract]
    public class DvrRequestItem : BaseSqlCommand
    {
        [DataMember]
        public int StorageTable { get; set; }

        [DataMember]
        public int DvrRequestId { get; set; }

        protected override string GetSql()
        {
            return string.Format("UPDATE T_DvrRequest SET NotificationId = {0} WHERE ID = {1}", TableID, DvrRequestId);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("DvrRequest for tableId {0}, DvrRequestId {1}", TableID, DvrRequestId);
        }
    }
}

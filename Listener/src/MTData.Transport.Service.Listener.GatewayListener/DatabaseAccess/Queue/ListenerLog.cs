﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class ListenerLog : BaseSqlCommand
    {
        [DataMember]
        public int FleetID { get; set; }
        [DataMember]
        public int VehicleID { get; set; }
        [DataMember]
        public string ListenerMessage { get; set; }
        [DataMember]
        public int Direction { get; set; }
        [DataMember]
        public string MessageType { get; set; }
        [DataMember]
        public string IpAddress { get; set; }
        [DataMember]
        public int LogSizeMinutes { get; set; }

        protected override string GetSql()
        {
            return string.Format("exec AddListenerLogMessage {0}, {1}, '{2}', {3}, '{4}', '{5}', {6}",
                FleetID, VehicleID, ListenerMessage, Direction, MessageType, IpAddress, LogSizeMinutes);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("ListenerLog for unit {0}/{1}", FleetID, VehicleID);
        }

    }
}

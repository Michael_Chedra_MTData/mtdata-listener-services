﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;
using System.Data;
using System.Data.SqlClient;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue
{
    [DataContract]
    public class UpdateAssetRegister : BaseSqlCommand
    {
        [DataMember]
        public int FleetID { get; set; }
        [DataMember]
        public int VehicleID { get; set; }
        [DataMember]
        public int HardwareType { get; set; }
        [DataMember]
        public string SimCard { get; set; }
        [DataMember]
        public int SlotNumber { get; set; }
        [DataMember]
        public int PreferedSlotNumber { get; set; }
        [DataMember]
        public string NetworkName { get; set; }
        [DataMember]
        public string IMEI { get; set; }
        [DataMember]
        public string CDMAESN { get; set; }
        [DataMember]
        public long SerialNumber { get; set; }
        [DataMember]
        public int SoftwareVersion { get; set; }
        [DataMember]
        public int ProgramMajor { get; set; }
        [DataMember]
        public int ProgramMinor { get; set; }
        [DataMember]
        public int HardwareVersionNumber { get; set; }
        [DataMember]
        public long IOBoxSerialNumber { get; set; }
        [DataMember]
        public int IOBoxProgramMajor { get; set; }
        [DataMember]
        public int IOBoxProgramMinor { get; set; }
        [DataMember]
        public int IOBoxHardwareVersionNumber { get; set; }
        [DataMember]
        public long MDTSerialNumber { get; set; }
        [DataMember]
        public int MDTProgramMajor { get; set; }
        [DataMember]
        public int MDTProgramMinor { get; set; }
        [DataMember]
        public int MDTHardwareVer { get; set; }
        [DataMember]
        public int LoginFlags { get; set; }

        protected override string GetSql()
        {
            int assetRegisterExpirySeconds = 0;

            if (ConfigurationManager.AppSettings["AssetRegisterExpirySeconds"] != null)
            {
                int.TryParse(ConfigurationManager.AppSettings["AssetRegisterExpirySeconds"], out assetRegisterExpirySeconds);
            }

            return string.Format("exec usp_UpdateAssetRegister {0}, {1}, {2}, '{3}', {4}, {5}, '{6}', '{7}', '{8}', {9}, {10}, {11}, {12}, {13}, {14}, {15}, {16}, {17}, {18}, {19}, {20}, {21}, {22}, {23}",
                FleetID, VehicleID, HardwareType, SimCard, SlotNumber, PreferedSlotNumber, NetworkName, IMEI, CDMAESN, SerialNumber, SoftwareVersion, ProgramMajor, ProgramMinor,
                HardwareVersionNumber, IOBoxSerialNumber, IOBoxProgramMajor, IOBoxProgramMinor, IOBoxHardwareVersionNumber, MDTSerialNumber, MDTProgramMajor, MDTProgramMinor,
                MDTHardwareVer, LoginFlags, assetRegisterExpirySeconds);
        }

        protected override SqlCommand GetSqlCommand()
        {
            return null;
        }

        [OnDeserializing]
        protected void OnDeserialized(StreamingContext contect)
        {
            SimpleCommand = true;
        }

        public override string ToString()
        {
            return string.Format("UpdateAssetRegister for unit {0}/{1}", FleetID, VehicleID);
        }

    }
}

﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener
{
    public static class SqlConnectionExtention
    {
        /// <summary>
        /// Attempts to open to the current connection if the user is 
        /// logged in to our application.
        /// </summary>  
        public static void OpenExtentionMethod(this SqlConnection dbcon, long auditUserID) {
            if (dbcon.State == ConnectionState.Closed)
                dbcon.Open();
            SetContextInfo(dbcon, auditUserID);
        }

        /// <summary>
        /// Attempts to set context_info to the current connection if the user is 
        /// logged in to our application.
        /// </summary>
        private static void SetContextInfo(SqlConnection dbcon, long auditUserID)
        {
            using (SqlCommand command = new SqlCommand(String.Format("SET CONTEXT_INFO 0x{0:X}", auditUserID), dbcon))
            {
                command.CommandType = CommandType.Text;
                command.ExecuteNonQuery();
            }
        }
    }
}

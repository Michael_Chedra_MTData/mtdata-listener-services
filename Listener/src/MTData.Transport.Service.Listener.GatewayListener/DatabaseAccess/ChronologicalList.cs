using System;
using System.Collections;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
	/// <summary>
	/// A Thread Safe Date Sorted ArrayList
	/// </summary>
	public class ChronologicalList
	{
		/// <summary>
		/// Name Value Pair for a TimeStamp and an Object
		/// </summary>
		public class DatedItem
		{
			public DateTime	TimeStamp;
			public object	Item;

			public DatedItem(DateTime timeStamp, object item)
			{
				TimeStamp = timeStamp;
				Item = item;
			}
		}

		/// <summary>
		/// Underlying list
		/// </summary>
		private ArrayList	_list	= ArrayList.Synchronized(new ArrayList());

		public ChronologicalList()
		{

		}

		/// <summary>
		/// Inserts specified Item to list
		/// </summary>
		/// <param name="timeStamp">TimeStamp to insert Item at</param>
		/// <param name="item">Item to insert</param>
		public void Insert(DateTime timeStamp, object item)
		{
			lock(_list.SyncRoot)
			{
				for(int i = 0 ; i < _list.Count ; i++)
				{
					DatedItem datedItem = (DatedItem) _list[i];
					if (datedItem.TimeStamp > timeStamp)
					{
						_list.Insert(i, new DatedItem(timeStamp, item));
						return;
					}
				}
				_list.Add(new DatedItem(timeStamp, item));
			}
		}

		public System.Array ToObjectArray(Type type)
		{
			ArrayList result = new ArrayList();
			for(int i = 0 ; i < _list.Count; i++)
				result.Add(((DatedItem)_list[i]).Item);
			return result.ToArray(type);
		}

		/// <summary>
		/// Retrieve the list of Item's inserted in Chronological order
		/// </summary>
		/// <returns></returns>
		public object[] ToObjectArray()
		{
			lock(_list.SyncRoot)
			{
				object[] result = new object[_list.Count];
				for(int i = 0 ; i < result.Length ; i++)
					result[i] = ((DatedItem)_list[i]).Item;
				return result;
			}
		}

		/// <summary>
		/// Retrieve the list of Item's inserted in Chronological order
		/// </summary>
		/// <returns></returns>
		public DatedItem[] ToDatedItemArray()
		{
			lock(_list.SyncRoot)
			{
				DatedItem[] result = new DatedItem[_list.Count];
				_list.CopyTo(result, 0);
				return result;
			}
		}

		/// <summary>
		/// Clear Items
		/// </summary>
		public void Clear()
		{
			lock(_list.SyncRoot)
				_list.Clear();
		}

		/// <summary>
		/// Return number of Items in list
		/// </summary>
		public int Count
		{
			get { return _list.Count; }
		}
	}
}

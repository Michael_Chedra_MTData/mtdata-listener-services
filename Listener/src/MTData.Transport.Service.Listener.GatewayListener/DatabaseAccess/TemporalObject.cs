﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class TemporalObject
    {
        private int _id;
        private List<TimePeriod> _times;

        public int Id { get { return _id; } }
        public List<TimePeriod> Times { get { return _times; } }

        public TemporalObject(int setPointId)
        {
            _id = setPointId;
            _times = new List<TimePeriod>();
        }


        /// <summary>
        /// check if the temporal object is active
        /// </summary>
        /// <param name="dateTime"></param>
        /// <param name="mDatabase"></param>
        /// <param name="v"></param>
        /// <returns>CheckTimeResult that indicates wehter the tiem is inside a time period or not and returns the scheduled arrive/depart times </returns>
        public CheckTimeResult CheckTimes(DateTime dateTime, DatabaseInterface mDatabase, Vehicle v)
        {
            //convert the time (in utc) to vehicle's local time
            DateTime local = mDatabase.GetVehicleLocalTime((int)v.cFleet, (int)v.iUnit, dateTime);
            TimeSpan offset = local - dateTime;
            TimePeriod nextPeriod = null;

            //iterate through each time and check if the waypoint is currently active for this report
            DateTime nextStart = DateTime.MaxValue;
            DateTime nextEnd = DateTime.MaxValue;
            foreach (TimePeriod t in _times)
            {
                //work out start and end date of this time
                DateTime start = t.CalculateStartTime(local);
                DateTime end = start.AddMinutes(t.Duration);

                if (local >= start && local <= end)
                {
                    return new CheckTimeResult(true, t, dateTime, end.AddMinutes(offset.TotalMinutes * -1));
                }
                else
                {
                    //outside range - update next times
                    if (local >= start)
                    {
                        //start before current so next start is a week later
                        start = start.AddDays(7);
                        end = end.AddDays(7);
                    }
                    if (start < nextStart)
                    {
                        nextStart = start;
                        nextEnd = end;
                        nextPeriod = t;
                    }
                }
            }

            //outside all time zones, 
            return new CheckTimeResult(false, nextPeriod, nextStart.AddMinutes(offset.TotalMinutes * -1), nextEnd.AddMinutes(offset.TotalMinutes * -1));
        }

        public class CheckTimeResult
        {
            private bool _insideTimePeriod;
            private TimePeriod _period;
            private DateTime _scheduledArriveTime;
            private DateTime _scheduledDepartTime;

            public bool InsideTimePeriod { get { return _insideTimePeriod; } }
            public TimePeriod Period { get { return _period; } }
            public DateTime ScheduledArriveTime { get { return _scheduledArriveTime; } }
            public DateTime ScheduledDepartTime { get { return _scheduledDepartTime; } }

            public CheckTimeResult(bool inside, TimePeriod p, DateTime arrive, DateTime depart)
            {
                _insideTimePeriod = inside;
                _period = p;
                _scheduledArriveTime = arrive;
                _scheduledDepartTime = depart; 
            }
        }
    }
}

using System;
using System.Data.SqlClient;
using System.Data;
using System.Net;
using System.Globalization;
using MTData.Transport.Application.Communication.MsmqInterface;
using MTData.Transport.Gateway.Packet;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Threading;
using System.IO;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Application.Communication;
using MTData.Transport.Service.Listener.GatewayListener.Dashboard;
using MTData.Transport.Service.Listener.GatewayListener.Network_Threads;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.Queue;
using log4net;
using MTData.Common.PlaceLookup;
using MTData.Common.Queues;
using MTData.Common.Threading;
using System.Messaging;
using System.Linq;
using System.Runtime.Serialization.Json;
using MTData.Transport.GenericProtocol;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.GatewayListener
{
    /// <summary>
    /// Summary description for DatabaseInterface.
    /// </summary>
    public class DatabaseInterface
    {
        private const string sClassName = "MTData.Transport.Service.Listener.GatewayListener.DatabaseInterface.";
        private const string PERFORMANCE_TYPENAME = "DatabaseInterface";
        private static ILog _log = LogManager.GetLogger(typeof(DatabaseInterface));
        private static ILog _statsLog = LogManager.GetLogger("StatsLogger");
        #region Enumerations
        public enum GatewayUnitKey
        {
            SerialNumber,
            IMEI,
            ESN
        }
        #region CosLat
        private static readonly double[] CosLatTable = 
		{
			1,
			0.999847695,
			0.999390827,
			0.998629535,
			0.99756405,
			0.996194698,
			0.994521895,
			0.992546152,
			0.990268069,
			0.987688341,
			0.984807753,
			0.981627183,
			0.978147601,
			0.974370065,
			0.970295726,
			0.965925826,
			0.961261696,
			0.956304756,
			0.951056516,
			0.945518576,
			0.939692621,
			0.933580426,
			0.927183855,
			0.920504853,
			0.913545458,
			0.906307787,
			0.898794046,
			0.891006524,
			0.882947593,
			0.874619707,
			0.866025404,
			0.857167301,
			0.848048096,
			0.838670568,
			0.829037573,
			0.819152044,
			0.809016994,
			0.79863551,
			0.788010754,
			0.777145961,
			0.766044443,
			0.75470958,
			0.743144825,
			0.731353702,
			0.7193398,
			0.707106781,
			0.69465837,
			0.68199836,
			0.669130606,
			0.656059029,
			0.64278761,
			0.629320391,
			0.615661475,
			0.601815023,
			0.587785252,
			0.573576436,
			0.559192903,
			0.544639035,
			0.529919264,
			0.515038075,
			0.5,
			0.48480962,
			0.469471563,
			0.4539905,
			0.438371147,
			0.422618262,
			0.406736643,
			0.390731128,
			0.374606593,
			0.35836795,
			0.342020143,
			0.325568154,
			0.309016994,
			0.292371705,
			0.275637356,
			0.258819045,
			0.241921896,
			0.224951054,
			0.207911691,
			0.190808995,
			0.173648178,
			0.156434465,
			0.139173101,
			0.121869343,
			0.104528463,
			0.087155743,
			0.069756474,
			0.052335956,
			0.034899497,
			0.017452406,
			0
		};
        #endregion
        #endregion
        #region Delegates and Events
        /// <summary>
        /// Delegate defines the event structure for an  failed Insertion
        /// </summary>
        //		public delegate void TransactionErrorDelegate(object sender, string message, string stackTrace, string command);
        /// <summary>
        /// This event will, be raised if there is a transaction error.
        /// </summary>
        //		public event TransactionErrorDelegate TransactionError;
        /// <summary>
        /// Refresh Data Set event
        /// </summary>
        public delegate void RefreshDataSetsDelegate();
        /// <summary>
        /// This event will, be raised if a refresh datasets is called.
        /// </summary>
        public event RefreshDataSetsDelegate eRefreshDatasets;
        /// <summary>
        /// This event will be raised if the schedule task dataset is refreshed
        /// </summary>
        public event RefreshDataSetsDelegate OnRefreshScheduledTaskDataset;

        /// <summary>
        /// This delegate contains ECM data segment for the given fleet/vehicle/gpstime
        /// </summary>
        public delegate void SendECMLiveUpdateDelegate(int fleetId, int vehicleId, DateTime dGPSTime, string _location, float _latitude, float _longitude, GPECMAdvancedItem[] oECMValues);
        /// <summary>
        /// When a unit sends in ECM data, this event is called
        /// </summary>
        public event SendECMLiveUpdateDelegate eSendECMLiveUpdate;
        #endregion
        #region Variables
        private GatewayAdministrator.SetAcceptUnitTrafficFlagDel _delAcceptUnitTraffic;
        private double dbVer = 0;
        private static double sdbVer = 0;
        private string _serverTime_DateFormat = "";
        private MSQueue _msQueue = null;
        private object MSQSyncRoot = new object();
        private int iFailedToInsertToMSMQWaitPeriod = 0;
        private PlaceLookupTree oSuburbTree = null;
        private MelwaysLookup oMelwayTree = null;

        private bool bUseSSWPsWhilstUnitIsMoving = false;
        private string sDSN = "";

        private SqlConnection mSqlConnection;
        private object SyncRootGatewayUnitCredentials = new object();
        private SqlCommand cmdGatewayUnitCredentialsIMEI;
        private SqlCommand cmdGatewayUnitCredentialsCDMAESN;
        private SqlCommand cmdGatewayUnitCredentialsSerialNumber;
        private object SyncRootGatewayUnitUpdateWithCredentials = new object();
        private SqlCommand cmdGatewayUnitUpdateWithCredentialsIMEI;
        private SqlCommand cmdGatewayUnitUpdateWithCredentialsCDMAESN;
        private SqlCommand cmdGatewayUnitUpdateWithIMEIandCDMAESN;
        private SqlCommand cmdPositionGetForLatLong;

        private object oVehicleDataSetSync = new object();
        private object oScheduleGroupDataSetSync = new object();
        private object oScheduleDataSetSync = new object();
        private object oConfigDataSetSync = new object();
        private object oRouteGroupDataSetSync = new object();
        private object oRoutePointDataSetSync = new object();
        private object oSetGroupDataSetSync = new object();
        private object oSetPointDataSetSync = new object();
        private object oTrailerDataSetSync = new object();
        private object oMassDataSetSync = new object();
        private object oScheduledTasksDataSetSync = new object();
        private object oExtendedHistoryDataSetSync = new object();
        private object oDriverPointsDataSetSync = new object();
        private object oInterfaceChannelsDataSetSync = new object();
        private object oDriverPointsServerTrackingDataSetSync = new object();
        private object oReasonCodeOveridesSync = new object();
        private object oReasonCodeOverideAssignmentsSync = new object();
        private object oTemporalWaypointsSync = new object();

        private DataSet mVehicleDataSet;
        private DataSet mVehicleWatchList;
        private Dictionary<int, Dictionary<int, string>> mReasonCodeOverides;
        private Dictionary<int, Dictionary<int, int>> mReasonCodeOverideAssignments;
        private Dictionary<int, bool> _fleetPossibleAccident;

        private Dictionary<long, int> mVehicleOffsets;
        private Dictionary<long, int> mVehicleEngineHourOffsets;
        private Dictionary<int, Dictionary<int, int>> mSPNSignedDataType;  // mSPNSignedDataType[SourceType][SPNID] = Data Length  If a value exists in this table, then it is a signed value.
        private List<long> mVehicleUsingGPSOdometer;
        Dictionary<long, CardReaderConfiguration> mVehicleCardReaderConfigs;
        private DataSet mReasonCodes;
        private DataSet mConfigDataSet;
        private DataSet mRouteGroupDataSet;
        private DataSet mRoutePointDataSet;
        private DataSet mSetGroupDataSet;
        private DataSet mSetPointDataSet;
        private DataSet mSetPointsToSendToUnitDataSet;
        private DataSet mTrailerDataSet;
        private DataSet mSetPointNodesDataSet;
        private DataSet mMassDataSet;
        private DataSet mDriverPointsDataSet;
        private DataSet mInterfaceChannelsDataSet;
        private DataSet mDriverPointsServerTrackingDataSet;
        private DataSet mScheduledTasksDataSet;
        private DataSet mExtendedHistoryDataSet;
        private int iRefreshCounter = 0;

        // This set of vars is used to store an re-apply unit states.
        private Hashtable oUnitStates = null;					// A hashtable to store unit statuses in memory
        private Hashtable oUnitToSSWPGroup = null;
        private Hashtable mWPGroupToUnitSideWPs = new Hashtable();
        private ArrayList mServerSideWPGroupList = new ArrayList();
        private ArrayList mPolygonWPGroupList = new ArrayList();
        private EnhancedThread thRefreshWPList = null;
        private string _userDefinedDateFormat = null;
        private string _sUserInfoDefaultField = "";
        private int _setPointFlagsOrPattern = 0;
        private int _iConcreteStationaryMinutes = 0;
        private RouteManagement _routeManagement = null;
        private bool bRefreshInProgress = false;
        private bool bRecoverableMSMQ = false;
        private WaypointLookupTree oWPLookupTree = null;
        private object oWPLookupTreeSync = new object();
        private double dRad = 0;
        private Dictionary<int, TemporalWayPoint> _temporalWaypoints;
        private DataTable _vehicleTemporalWayPoints;
        private DataTable _vehicleWayPoints;
        private Dictionary<int, TemporalObject> _configStationaryPeriods;
        private Dictionary<int, TemporalObject> _configOutOfHoursPeriods;
        private List<Tuple<int, int>> _vehiclesWpServerSideOnly;

        private ServerLoadLevel _currentServerLoad;
        private List<ServerLoadLevel> _serverLoadLevels;
        private IEmailHelper emailHelper;

        public List<byte> ConcreteFleets { get; set; }
        private bool IsConcreteFleet(byte fleet)
        {
            if (ConcreteFleets != null && ConcreteFleets.Contains(fleet))
            {
                return true;
            }
            return false;
        }
        #endregion
        #region Create and Dispose functions
        /// <summary>
        /// This method will be used to dispose of the DatabaseInterface object
        /// </summary>
        /// <returns>void</returns>
        public void Dispose()
        {
            try
            {
                if (_dashInterface != null)
                    _dashInterface = null;
            }
            catch (System.Exception)
            {
            }
            #region Dispose of the local objects.
            try
            {
                if (thRefreshWPList != null)
                {
                    thRefreshWPList.Stop();
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (_msQueue != null)
                {
                    lock (MSQSyncRoot)
                    {
                        _msQueue.Q.Dispose();
                        _msQueue = null;
                    }
                }
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (mSqlConnection != null)
                {
                    mSqlConnection.Close();
                }
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (mSqlConnection != null)
                {
                    mSqlConnection.Dispose();
                    mSqlConnection = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mVehicleDataSet != null)
                {
                    mVehicleDataSet.Clear();
                    mVehicleDataSet.Dispose();
                    mVehicleDataSet = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mReasonCodes != null)
                {
                    mReasonCodes.Clear();
                    mReasonCodes.Dispose();
                    mReasonCodes = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mConfigDataSet != null)
                {
                    lock (oConfigDataSetSync)
                    {
                        mConfigDataSet.Clear();
                        mConfigDataSet.Dispose();
                        mConfigDataSet = null;
                    }
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mRouteGroupDataSet != null)
                {
                    lock (oRouteGroupDataSetSync)
                    {
                        mRouteGroupDataSet.Clear();
                        mRouteGroupDataSet.Dispose();
                        mRouteGroupDataSet = null;
                    }
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mSetGroupDataSet != null || mSetPointDataSet != null)
                {
                    lock (oSetGroupDataSetSync)
                    {
                        lock (oSetPointDataSetSync)
                        {
                            if (mSetPointDataSet != null)
                            {
                                mSetPointDataSet.Clear();
                                mSetPointDataSet.Dispose();
                                mSetPointDataSet = null;
                            }
                            if (mSetGroupDataSet != null)
                            {
                                mSetGroupDataSet.Clear();
                                mSetGroupDataSet.Dispose();
                                mSetGroupDataSet = null;
                            }
                        }
                    }
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mRoutePointDataSet != null)
                {
                    lock (oRoutePointDataSetSync)
                    {
                        mRoutePointDataSet.Clear();
                        mRoutePointDataSet.Dispose();
                        mRoutePointDataSet = null;
                    }
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mSetPointNodesDataSet != null)
                {
                    mSetPointNodesDataSet.Clear();
                    mSetPointNodesDataSet.Dispose();
                    mSetPointNodesDataSet = null;
                }
            }
            catch (System.Exception)
            {
            }


            if (cmdGatewayUnitCredentialsIMEI != null)
                cmdGatewayUnitCredentialsIMEI.Dispose();
            cmdGatewayUnitCredentialsIMEI = null;

            if (cmdGatewayUnitCredentialsCDMAESN != null)
                cmdGatewayUnitCredentialsCDMAESN.Dispose();
            cmdGatewayUnitCredentialsCDMAESN = null;

            if (cmdGatewayUnitCredentialsSerialNumber != null)
                cmdGatewayUnitCredentialsSerialNumber.Dispose();
            cmdGatewayUnitCredentialsSerialNumber = null;

            if (cmdGatewayUnitUpdateWithCredentialsIMEI != null)
                cmdGatewayUnitUpdateWithCredentialsIMEI.Dispose();
            cmdGatewayUnitUpdateWithCredentialsIMEI = null;

            if (cmdGatewayUnitUpdateWithCredentialsCDMAESN != null)
                cmdGatewayUnitUpdateWithCredentialsCDMAESN.Dispose();
            cmdGatewayUnitUpdateWithCredentialsCDMAESN = null;

            if (cmdGatewayUnitUpdateWithIMEIandCDMAESN != null)
                cmdGatewayUnitUpdateWithIMEIandCDMAESN.Dispose();
            cmdGatewayUnitUpdateWithIMEIandCDMAESN = null;

            if (oUnitStates != null)
            {
                lock (oUnitStates.SyncRoot)
                    oUnitStates.Clear();
                oUnitStates = null;
            }
            #endregion
        }

        private DashboardProcessing.DashInterfaceDelegate _dashInterface;

        /// <summary>
        /// This method will be create the DatabaseInterface object.
        /// </summary>
        /// <param name="ref cLogThread oLog"></param>
        /// <param name="string connString"></param>
        /// <param name="bool bTransport"></param>
        /// <param name="int commandTimeout"></param>
        /// <param name="string userDefinedDateFormat"></param>
        /// <param name="string serverTime_DateFormat"></param>
        /// <param name="string sUserInfoDefaultField"></param>
        /// <param name="RouteManagement routeManagement"></param>
        public DatabaseInterface(string connString, int commandTimeout, 
            string userDefinedDateFormat, string serverTime_DateFormat, string sUserInfoDefaultField, RouteManagement routeManagement,
            DashboardProcessing.DashInterfaceDelegate dashInterface, GatewayAdministrator.SetAcceptUnitTrafficFlagDel delAcceptUnitTraffic, IEmailHelper emailHelper)
        {
            string sDefaultSetPointFlags = "";
            _delAcceptUnitTraffic = delAcceptUnitTraffic;
            this.emailHelper = emailHelper;
            _dashInterface = dashInterface;
            dRad = System.Math.PI / 180;
            _routeManagement = routeManagement;
            _serverTime_DateFormat = serverTime_DateFormat;
            _sUserInfoDefaultField = sUserInfoDefaultField;
            sDSN = connString;
            sDefaultSetPointFlags = System.Configuration.ConfigurationManager.AppSettings["DefaultSetPointFlagPattern"];
            _setPointFlagsOrPattern = 0;
            if (sDefaultSetPointFlags != null)
                _setPointFlagsOrPattern = Int32.Parse(sDefaultSetPointFlags);

            InitialisePendantAlarms();

            _temporalWaypoints = new Dictionary<int, TemporalWayPoint>();
            _configOutOfHoursPeriods = new Dictionary<int, TemporalObject>();
            _configStationaryPeriods = new Dictionary<int, TemporalObject>();
            _fleetPossibleAccident = new Dictionary<int, bool>();
            _vehiclesWpServerSideOnly = new List<Tuple<int, int>>();
            _serverLoadLevels = ConfigurationManager.GetSection("ServerLoadLevels") as List<ServerLoadLevel>;

            try
            {
                _iConcreteStationaryMinutes = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["ConstantPositionTimeMinutes"]);
            }
            catch (System.Exception)
            {
                _iConcreteStationaryMinutes = 2;
            }
            try
            {
                if (System.Configuration.ConfigurationManager.AppSettings["MessageQueueIsRecoverable"] == "true")
                    bRecoverableMSMQ = true;
            }
            catch (System.Exception)
            {
            }
            try
            {
                iFailedToInsertToMSMQWaitPeriod = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["PauseOnFailedMSMQInsert"]);
            }
            catch (System.Exception)
            {
                iFailedToInsertToMSMQWaitPeriod = 10000;
            }

            try
            {
                string sDBVer = System.Configuration.ConfigurationManager.AppSettings["DatabaseVer"];
                dbVer = Convert.ToDouble(sDBVer);
                DatabaseInterface.sdbVer = dbVer;
                if (System.Configuration.ConfigurationManager.AppSettings["ProcessServerWPsWhilstVehicleIsMoving"] == "true")
                    bUseSSWPsWhilstUnitIsMoving = true;
            }
            catch (System.Exception ex)
            {
                dbVer = 5.7;
                DatabaseInterface.sdbVer = dbVer;
                _log.Error(sClassName + "DatabaseInterface(string connString, bool bTransport, int commandTimeout, NotificationInsertionMode notificationInsertionMode, string userDefinedDateFormat, string serverTime_DateFormat, string sUserInfoDefaultField, RouteManagement routeManagement, DashboardProcessing.DashInterfaceDelegate dashInterface)", ex);
            }

            try
            {
                // Create the sync objects for the datasets.
                _userDefinedDateFormat = userDefinedDateFormat;

                mSqlConnection = new SqlConnection(connString);
                mSqlConnection.Open();

                #region Load a new WP and Melways lookup tree
                oSuburbTree = new PlaceLookupTree(sDSN);
                #endregion
                string gatewayUnitPortUpdateCommand = "UPDATE T_GatewayUnit SET SIMCardNumber = @SIMCardNumber, IPAddress = @IPAddress, ListenPort = @ListenPort WHERE ";

                try
                {
                    #region Create the message queue
                    if (_msQueue == null)
                    {
                        lock (MSQSyncRoot)
                        {
                            _msQueue = new MSQueue(System.Configuration.ConfigurationManager.AppSettings["MessageQueueName"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MessageQueueMaxSize"]));
                        }
                        //update server load level
                        InitialiseServerLoadLevel();
                    }
                    #endregion
                }
                catch (System.Exception exMSMQ)
                {
                    _log.Error(sClassName + "DatabaseInterface(string connString, bool bTransport, int commandTimeout, NotificationInsertionMode notificationInsertionMode, string userDefinedDateFormat, string serverTime_DateFormat, string sUserInfoDefaultField, RouteManagement routeManagement, DashboardProcessing.DashInterfaceDelegate dashInterface) - Error starting Message Queue Thread.", exMSMQ);
                }

                #region Commands for GateWayUnit Interrogation
                string credentialsQuery = "SELECT SerialNumber, IMIE, CDMAESN, FleetID, VehicleID FROM T_GatewayUnit WHERE {0} = @TestValue";
                cmdGatewayUnitCredentialsSerialNumber = new SqlCommand(string.Format(credentialsQuery, "SerialNumber"), mSqlConnection);
                cmdGatewayUnitCredentialsSerialNumber.Parameters.Add(new SqlParameter("@TestValue", SqlDbType.BigInt));

                cmdGatewayUnitCredentialsIMEI = new SqlCommand(string.Format(credentialsQuery, "LTRIM(RTRIM(REPLACE(IMIE, CHAR(13) + CHAR(10), '')))"), mSqlConnection);
                cmdGatewayUnitCredentialsIMEI.Parameters.Add(new SqlParameter("@TestValue", SqlDbType.VarChar, 40));

                cmdGatewayUnitCredentialsCDMAESN = new SqlCommand(string.Format(credentialsQuery, "LTRIM(RTRIM(REPLACE(CDMAESN, CHAR(13) + CHAR(10), '')))"), mSqlConnection);
                cmdGatewayUnitCredentialsCDMAESN.Parameters.Add(new SqlParameter("@TestValue", SqlDbType.VarChar, 20));

                cmdGatewayUnitUpdateWithCredentialsIMEI = new SqlCommand(gatewayUnitPortUpdateCommand + "LTRIM(RTRIM(REPLACE(IMIE, CHAR(13) + CHAR(10), ''))) = @TestValue", mSqlConnection);
                cmdGatewayUnitUpdateWithCredentialsIMEI.Parameters.Add(new SqlParameter("@TestValue", SqlDbType.VarChar, 40));
                cmdGatewayUnitUpdateWithCredentialsIMEI.Parameters.Add(new SqlParameter("@SIMCardNumber", SqlDbType.VarChar, 32));
                cmdGatewayUnitUpdateWithCredentialsIMEI.Parameters.Add(new SqlParameter("@IPAddress", SqlDbType.VarChar, 16));
                cmdGatewayUnitUpdateWithCredentialsIMEI.Parameters.Add(new SqlParameter("@ListenPort", SqlDbType.VarChar, 10));

                cmdGatewayUnitUpdateWithCredentialsCDMAESN = new SqlCommand(gatewayUnitPortUpdateCommand + "LTRIM(RTRIM(REPLACE(CDMAESN, CHAR(13) + CHAR(10), ''))) = @TestValue", mSqlConnection);
                cmdGatewayUnitUpdateWithCredentialsCDMAESN.Parameters.Add(new SqlParameter("@TestValue", SqlDbType.VarChar, 40));
                cmdGatewayUnitUpdateWithCredentialsCDMAESN.Parameters.Add(new SqlParameter("@SIMCardNumber", SqlDbType.VarChar, 32));
                cmdGatewayUnitUpdateWithCredentialsCDMAESN.Parameters.Add(new SqlParameter("@IPAddress", SqlDbType.VarChar, 16));
                cmdGatewayUnitUpdateWithCredentialsCDMAESN.Parameters.Add(new SqlParameter("@ListenPort", SqlDbType.VarChar, 10));

                cmdGatewayUnitUpdateWithIMEIandCDMAESN = new SqlCommand("UPDATE T_GatewayUnit SET IMIE = @IMEI, CDMAESN = @CDMAESN WHERE SerialNumber = @SerialNumber", mSqlConnection);
                cmdGatewayUnitUpdateWithIMEIandCDMAESN.Parameters.Add(new SqlParameter("@IMEI", SqlDbType.VarChar, 40));
                cmdGatewayUnitUpdateWithIMEIandCDMAESN.Parameters.Add(new SqlParameter("@CDMAESN", SqlDbType.VarChar, 20));
                cmdGatewayUnitUpdateWithIMEIandCDMAESN.Parameters.Add(new SqlParameter("@SerialNumber", SqlDbType.BigInt));


                cmdPositionGetForLatLong = new SqlCommand("EXEC dbo.CalculateNearestPlaceAndMapRef @Latitude, @Longitude", mSqlConnection);
                cmdPositionGetForLatLong.Parameters.Add(new SqlParameter("@Latitude", SqlDbType.Float, 8));
                cmdPositionGetForLatLong.Parameters.Add(new SqlParameter("@Longitude", SqlDbType.Float, 8));
                #endregion

                _log.Info("Refreshing Datasets on startup - Full forced reload.");
                RefreshSPNIsSignedDataTypeLookup();
                RefreshDataSets();
                RefreshDriverPointsServerTrackingRules();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DatabaseInterface(string connString, bool bTransport, int commandTimeout, NotificationInsertionMode notificationInsertionMode, string userDefinedDateFormat, string serverTime_DateFormat, string sUserInfoDefaultField, RouteManagement routeManagement, DashboardProcessing.DashInterfaceDelegate dashInterface)", ex);
            }
            #region Load the suburb places tree
            try
            {
                oSuburbTree = new PlaceLookupTree(connString);
            }
            catch (System.Exception exLoadPlaces)
            {
                _log.Error(sClassName + "DatabaseInterface(string connString, bool bTransport, int commandTimeout, NotificationInsertionMode notificationInsertionMode, string userDefinedDateFormat, string serverTime_DateFormat, string sUserInfoDefaultField, RouteManagement routeManagement, DashboardProcessing.DashInterfaceDelegate dashInterface) - Failed to load Places tree.", exLoadPlaces);
            }
            #endregion
            #region Start the thread to periodically refresh the datasets
            thRefreshWPList = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(RefreshDataSetTimerThread), null);
            thRefreshWPList.Start();
            #endregion
            // Tell the listener thread to start accepting packets.
            _delAcceptUnitTraffic(true);
        }

        private void InitialisePendantAlarms()
        {
            var pendantAlarms = new List<PendantAlarmListItem>();
            var lastAlarms = new Dictionary<FleetVehicle, DateTime>();
            _log.Info("Initialise Pendant Alarm Cache");
            try
            {
                using (SqlConnection connection = new SqlConnection(sDSN))
                using (SqlCommand command = connection.CreateCommand())
                {
                    command.CommandText = "usp_AcknowledgeAlarmsForListenerLoad";
                    command.CommandType = CommandType.StoredProcedure;
                    connection.Open();
                    var reader = command.ExecuteReader();
                    while (reader.Read())
                    {
                        PendantAlarmListItem item = new PendantAlarmListItem(Convert.ToByte(reader["FleetID"]), Convert.ToUInt32(reader["VehicleID"]), (DateTime)reader["DeviceTime"], (bool)reader["AckByUser"]);
                        pendantAlarms.Add(item);
                    }
                    reader.NextResult();
                    while (reader.Read())
                    {
                        FleetVehicle fleetVehicle = new FleetVehicle((int)reader["FleetID"], (int)reader["VehicleID"]);
                        lastAlarms[fleetVehicle] = (DateTime)reader["DeviceTime"];
                    }
                    _pendantActiveAlarmList = new PendantAlarmList(pendantAlarms, lastAlarms);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
                throw;
            }
        }

        #endregion

        #region Database connection functions
        /// <summary>
        /// This method will ensure that the database connection is available prior to uploading vehicle details.
        /// </summary>
        /// <param name="cLogThread logging"></param>
        /// <param name="string connectString"></param>
        /// <returns>bool</returns>
        public static bool ConfirmConnection(string connectString)
        {
            bool bRet = false;
            try
            {
                _log.Info("Confirming Database Connection");
                SqlConnection connection = new SqlConnection(connectString);
                connection.Open();
                connection.Close();
                connection.Dispose();
                bRet = true;
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "ConfirmConnection(string connectString = '" + connectString + "')", ex);
                bRet = false;
            }
            return bRet;
        }
        public static bool ConfirmMSMQ()
        {
            bool bRet = false;
            try
            {
                #region Try to create the message queues.
                string queueName = System.Configuration.ConfigurationManager.AppSettings["MessageQueueName"];
                if (queueName.StartsWith("FormatName:direct=OS:"))
                    queueName = string.Concat("FormatName:direct=OS:", queueName);
                MessageQueue queue = new MessageQueue(queueName);
                queue.Close();
                queue.Dispose();

                bRet = true;
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ConfirmMSMQ()", ex);
                bRet = false;
            }
            return bRet;
        }

        public string InsertListenerMessage(ListenerLogItem logItem)
        {

            TrackingQueueItem item = new TrackingQueueItem();
            ListenerLog log = new ListenerLog();
            log.Direction = (int)logItem.Direction;
            log.FleetID = logItem.FleetID;
            log.IpAddress = logItem.IPAddress;
            log.ListenerMessage = logItem.ListenerMessage;
            log.LogSizeMinutes = 30;
            try
            {
                log.LogSizeMinutes = int.Parse(ConfigurationManager.AppSettings["LogSizeMinutes"]);
            }
            catch { }
            log.MessageType = logItem.MessageType;
            log.VehicleID = logItem.VehicleID;
            item.SubCommands.Add(log);
            return this.AddToMSMQ(item);
        }


        /// <summary>
        /// Gets a SqlConnection to the TransportApp database.
        /// </summary>
        /// <returns>SqlConnection</returns>
        public SqlConnection GetOpenConnection()
        {
            SqlConnection oConn = null;
            bool bConnectionError = false;
            int iRetryCounter = 0;
            #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetOpenConnection() - Unable to connect to database - Will reconnect in 5 seconds.", ex);
                bConnectionError = true;
            }
            while (bConnectionError)
            {
                try
                {
                    if (iRetryCounter > 20)
                    {
                        _log.Info("Connection has failed 20 times, giving up on trying to connect.");
                        return null;
                    }
                    Thread.Sleep(5000);
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                    bConnectionError = false;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetOpenConnection() - Unable to connect to database - Will reconnect in 5 seconds.", ex);
                    bConnectionError = true;
                    iRetryCounter++;
                }
            }
            #endregion
            return oConn;
        }
        #endregion
        /// <summary>
        /// Returns a list of units in the given fleet which are running the config types specified in configType
        /// with the specified number (note - NOT ID)
        /// </summary>
        /// <param name="byte fleet"></param>
        /// <param name="string configType"></param>
        /// <param name="int configNumber"></param>
        /// <returns>uint[]</returns>
        public uint[] GetVehicleIdsRunningThis(byte fleet, string configType, int configNumber)
        {
            DataRow[] dra = null;
            // Procedure - find the ID of the schedule/config/route/set with that "number"
            // find any vehicles in T_Vehicle which use one of those 
            ArrayList units = new ArrayList();
            DataRow dr;
            uint id = 0;
            string sVehicleColumnHeader;

            #region Get Schedule/Config/Route/Set ID:
            if (configType.Equals("Schedule"))
            {
                // The Schedule page actually passes the ID directly. Handy - but inconsistent.
                id = Convert.ToUInt16(Convert.ToUInt32(configNumber) & 0xFFFF);
                sVehicleColumnHeader = "ScheduleGroupID";
            }
            else if (configType.Equals("DriverPointsRules"))
            {
                // The Driver Points actually passes the ID directly.
                id = Convert.ToUInt16(Convert.ToUInt32(configNumber) & 0xFFFF);
                sVehicleColumnHeader = "DriverPointsRuleGroupID";
            }
            else if (configType.Equals("DriverPointsGraphs"))
            {
                id = Convert.ToUInt16(Convert.ToUInt32(configNumber) & 0xFFFF);
                sVehicleColumnHeader = "DriverPointsGraphGroupID";
            }
            else
            {
                if (configType.Equals("Config"))
                {
                    dr = GetConfigRowFromConfigNumber(fleet, configNumber);
                    sVehicleColumnHeader = "ConfigID";
                }
                else if (configType.Equals("Route"))
                {
                    dr = GetRoutePointRowFromRoutePointNumber(fleet, configNumber);
                    sVehicleColumnHeader = "RoutePointGroupID";
                }
                else // Set point group:
                {
                    dr = GetSetPointRowFromSetPointNumber(fleet, configNumber);
                    sVehicleColumnHeader = "SetPointGroupID";
                }

                if (dr == null) return null;
                id = Convert.ToUInt16(Convert.ToUInt32(dr["ID"]) & 0xFFFF);
            }
            #endregion
            #region Find Vehicles using this id

            // Because we now hold the car's current settings in the top part, 
            // we must mask it. But there is no mask in SQL, so we'll just use
            // a MOD with 0xFFFF (65536) to find it
            lock (oVehicleDataSetSync)
            {
                if (mVehicleDataSet != null)
                {
                    dra = mVehicleDataSet.Tables["Vehicle"].Select(sVehicleColumnHeader + " % 65536 = " + id + " AND FleetID = " + fleet);
                }
            }
            if (dra == null) return null;
            if (dra.Length == 0) return null;
            foreach (DataRow drv in dra)
            {
                units.Add(Convert.ToUInt16(drv["ID"]));
            }
            #endregion
            uint[] unitList = new uint[units.Count];
            for (int pos = 0; pos < units.Count; pos++)
            {
                unitList[pos] = Convert.ToUInt16(units[pos]);
            }
            return unitList;
        }

        #region Update Vehicle Status functions
        /// <summary>
        /// This method will update the vehicle entry in the oUnitStates hashtable with the status value in the Vehicle object.
        /// </summary>
        /// <param name="Vehicle v"></param>
        /// <returns>void</returns>
        public void UpdateVehilcleStatus(Vehicle v)
        {
            try
            {
                UpdateVehilcleStatus(v, false);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehilcleStatus(Vehicle v)", ex);
            }
        }
        /// <summary>
        /// This method will update the vehicle entry in the oUnitStates hashtable with the status value in the Vehicle object.
        /// </summary>
        /// <param name="Vehicle v"></param>
        /// <param name="bool bForceUpdate"></param>
        /// <returns>void</returns>
        public void UpdateVehilcleStatus(Vehicle v, bool bForceUpdate)
        {
            try
            {
                if (v.iUnit > 0 && Convert.ToInt32(v.cFleet) > 0)
                {
                    UnitState oUnitState = v.State;
                    // Store the current Aux/MDT/Concrete state.
                    if (oUnitState >= UnitState.MDTState1)
                    {
                        string sUnitKey = Convert.ToString(v.iUnit).PadLeft(4, '0') + Convert.ToString(Convert.ToInt32(v.cFleet)).PadLeft(4, '0');
                        if (oUnitStates == null) oUnitStates = Hashtable.Synchronized(new Hashtable());
                        lock (oUnitStates.SyncRoot)
                        {
                            if (oUnitStates.ContainsKey(sUnitKey))
                                oUnitStates.Remove(sUnitKey);
                            oUnitStates.Add(sUnitKey, oUnitState);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehilcleStatus(Vehicle v, bool bForceUpdate)", ex);
            }
        }
        /// <summary>
        /// This method will update the vehicle entry in the oUnitStates hashtable with the status value in the Vehicle object.
        /// </summary>
        /// <param name="string sUnitKey"></param>
        /// <param name="UnitState oUnitState"></param>
        /// <param name="bool bForceUpdate"></param>
        /// <returns>void</returns>
        public void UpdateVehilcleStatus(string sUnitKey, UnitState oUnitState, bool bForceUpdate)
        {
            try
            {
                if (oUnitState >= UnitState.MDTState1)
                {
                    lock (oUnitStates.SyncRoot)
                    {
                        if (oUnitStates.ContainsKey(sUnitKey))
                            oUnitStates.Remove(sUnitKey);
                        oUnitStates.Add(sUnitKey, oUnitState);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehilcleStatus(string sUnitKey, UnitState oUnitState, bool bForceUpdate)", ex);
            }
        }
        /// <summary>
        /// This function updates the Vehicle objects state from the state stored in the oUnitStates hashtable
        /// </summary>
        /// <param name="Vehicle v"></param>
        /// <returns>void</returns>
        public void UpdateRestoreStoreStatus(Vehicle v)
        {
            #region Reapply/Store "State" info

            if (v == null) return;

            if (oUnitStates == null)
                oUnitStates = Hashtable.Synchronized(new Hashtable());

            try
            {
                UnitState oUnitState = v.GetUnitState();

                if (v.iUnit > 0 && Convert.ToInt32(v.cFleet) > 0)
                {
                    string sUnitKey = Convert.ToString(v.iUnit).PadLeft(4, '0') + Convert.ToString(Convert.ToInt32(v.cFleet)).PadLeft(4, '0');

                    if (oUnitState < UnitState.MDTState1)
                    {
                        // Reapply an previous Aux/MDT/Concrete state.
                        lock (oUnitStates.SyncRoot)
                        {
                            if (oUnitStates.ContainsKey(sUnitKey))
                            {
                                UnitState oOldUnitState = (UnitState)oUnitStates[sUnitKey];
                                v.SetStateBit(oOldUnitState);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateRestoreStoreStatus(Vehicle v)", ex);
            }
            #endregion
        }
        /// <summary>
        /// This function checks if the state (oState) is set on the vehicle object
        /// </summary>
        /// <param name="Vehicle v"></param>
        /// <param name="UnitState oState"></param>
        /// <returns>bool</returns>
        private bool IsStateSet(Vehicle v, UnitState oState)
        {
            if ((v.State & oState) == oState) return true;
            return false;
        }
        #endregion

        private void RefreshSPNIsSignedDataTypeLookup()
        {
            SqlConnection oConn = null;
            SqlDataAdapter objSQLDA = null;
            SqlCommand objSQLCmd = null;
            DataSet oDS = null;
            bool bConnectionError = false;
            string sSQL = "";
            try
            {
                mSPNSignedDataType = new Dictionary<int, Dictionary<int, int>>();
                sSQL = "SELECT spn.[SourceTypeID], spn.[SPNCode], sae.DataLength  FROM T_SPN_Codes spn INNER JOIN T_SPN_Code_SAE sae on sae.SAE_ID = spn.SAE_ID WHERE spn.IsSignedValue = 1";
                #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
                try
                {
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "RefreshSPNIsSignedDataTypeLookup() - Unable to connect to database - Will reconnect in 5 seconds.", ex);
                    bConnectionError = true;
                }
                while (bConnectionError)
                {
                    try
                    {
                        Thread.Sleep(5000);
                        oConn = new SqlConnection(sDSN);
                        oConn.Open();
                        bConnectionError = false;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "RefreshSPNIsSignedDataTypeLookup() - Unable to connect to database - Will reconnect in 5 seconds.", ex);
                        bConnectionError = true;
                    }
                }
                #endregion
                #region Select the DataSet
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand(sSQL, oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                oDS = new DataSet();
                objSQLDA.Fill(oDS);
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion

                if (oDS.Tables.Count > 0 && oDS.Tables[0].Rows.Count > 0)
                {
                    for (int X = 0; X < oDS.Tables[0].Rows.Count; X++)
                    {
                        DataRow dr = oDS.Tables[0].Rows[X];
                        int iSourceType = Convert.ToInt32(dr["SourceTypeID"]);
                        int iSPNCode = Convert.ToInt32(dr["SPNCode"]);
                        int iDataLength = Convert.ToInt32(dr["DataLength"]);


                        if (!mSPNSignedDataType.ContainsKey(iSourceType))
                        {
                            mSPNSignedDataType.Add(iSourceType, new Dictionary<int, int>());
                        }
                        if (mSPNSignedDataType.ContainsKey(iSourceType))
                        {
                            if (!mSPNSignedDataType[iSourceType].ContainsKey(iSPNCode))
                            {
                                mSPNSignedDataType[iSourceType].Add(iSPNCode, iDataLength);
                            }
                        }
                    }
                }

            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshSPNIsSignedDataTypeLookup()", ex);
            }

        }
        /// <summary>
        /// This function contains the code for the automatic timed refresh of the cached dataaset.
        /// </summary>
        /// <param name="EnhancedThread sender"></param>
        /// <param name="object data"></param>
        /// <returns>object</returns>
        private object RefreshDataSetTimerThread(EnhancedThread sender, object data)
        {
            iRefreshCounter = 6000; // 10 minutes in 100ms increments.
            while (!sender.Stopping)
            {
                iRefreshCounter--;
                if (iRefreshCounter <= 0)
                {
                    Action refeshThread = () => { RefreshDataSets(); };
                    refeshThread.BeginInvoke(AsyncRefreshDatabaseCallBack, refeshThread);
                    iRefreshCounter = 6000;
                }
                Thread.Sleep(100);
            }
            return null;
        }

        /// <summary>
        /// This method is called when the SendSMSEmail(LiveUpdate oLiveUpdate) calls BeginInvoke is completed.
        /// This function cleans up memory for the async call delegate.
        /// </summary>
        /// <param name="IAsyncResult ar">This is result of the delegate run.</param>
        private void AsyncRefreshDatabaseCallBack(IAsyncResult ar)
        {
            try
            {
                if (ar.AsyncState is Action)
                    (ar.AsyncState as Action).EndInvoke(ar);
                _log.Info("Database Update complete");
            }
            catch (System.Exception ex)
            {
                _log.Error(this.GetType().FullName + ".AsyncRefreshDatabaseCallBack(IAsyncResult ar)", ex);
            }
        }

        /// <summary>
        /// This method will cause the automatic timed refresh of the cached dataaset thread to update the dataset straight away.
        /// </summary>
        /// <returns>void</returns>
        private bool bForceRefeshComplete = true;
        public void ForceRefreshDataSets()
        {
            ForceRefreshDataSets(true);
        }
        public void ForceRefreshDataSets(bool bWaitForRefresh)
        {
            int iWaitCounter = 0;
            try
            {
                iRefreshCounter = 0;
                bForceRefeshComplete = false;
                while (!bForceRefeshComplete && bWaitForRefresh)
                {
                    Thread.Sleep(100);
                    iWaitCounter++;
                    if (iWaitCounter >= 600)
                    {
                        _log.Info("ForceRefreshDataSets : Waited for 60 seconds for refresh to complete, returning to normal processing.");
                        break;
                    }
                }
            }
            catch (System.Exception ex)
            {

                _log.Error(sClassName + "ForceRefreshDataSets(bool bWaitForRefresh)", ex);
            }
        }
        /// <summary>
        /// This method will cause refresh the cached dataasets
        /// </summary>
        /// <returns>void</returns>
        public void RefreshDataSets()
        {
            Thread tWPLookupTreeLoad = new Thread(new ThreadStart(LoadWPLookupTree));
            tWPLookupTreeLoad.Start();

            bool bConnectionError = false;
            int iWaitCounter = 0;
            System.Data.SqlClient.SqlConnection oConn = null;

            #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshDataSets() - Unable to connect to database - Will reconnect in 5 seconds", ex);
                bConnectionError = true;
            }
            while (bConnectionError)
            {
                try
                {
                    Thread.Sleep(5000);
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                    bConnectionError = false;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "RefreshDataSets() - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    bConnectionError = true;
                }
            }
            #endregion

            #region If there is already a refresh in progress, wait for up to 20 seconds for it to complete.
            while (bRefreshInProgress)
            {
                Thread.Sleep(100);
                iWaitCounter++;
                if (iWaitCounter >= 200)
                {
                    _log.Info("Waited 20 seconds for a previous refresh to complete - returned without refreshing datasets.");
                    return;
                }
            }
            #endregion

            try
            {
                _log.Info("Refreshing cached datasets.");
                bRefreshInProgress = true;
                RefreshVehicleDataSet(oConn);
                RefreshConfigDataSet(oConn);
                RefreshRouteGroupDataSet(oConn);
                RefreshSetPointGroupDataSet(oConn);
                RefreshTrailersDataSet(oConn);
                RefreshMassDeclarationDataSet(oConn);
                RefreshDriverPointsDataSet(oConn);
                RefreshInterfaceChannelsDataSet(oConn);
                RefreshScheduledTasksDataSet(oConn);
                RefreshExtendedHistoryDataSet(oConn);
                RefreshPendantData(oConn);
                if (_routeManagement != null)
                    _routeManagement.ReloadDataSets(oConn);

                // Send a message to all object that are attached to the database object that a refresh is required.
                if (eRefreshDatasets != null)
                    eRefreshDatasets();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshDataSets()", ex);
            }
            finally
            {
                bRefreshInProgress = false;
                bForceRefeshComplete = true;
                iRefreshCounter = 6000;
                if (oConn != null)
                {
                    oConn.Dispose();
                }
            }
        }

        // Checks if any of the supplied pendant alarms have been acknowledged.
        private void RefreshPendantData(SqlConnection connection)
        {
            _log.Info("Refreshing Pendant Data");
            DataTable dt = new DataTable();
            dt.Columns.Add("FleetID", typeof(int));
            dt.Columns.Add("VehicleID", typeof(int));
            dt.Columns.Add("AlarmTimestamp", typeof(DateTime));
            foreach (var activeAlarm in PendentActiveAlarmList.GetUnacknowledgedAlarms())
            {
                dt.Rows.Add((int)activeAlarm.Fleet, (int)activeAlarm.VehicleId, activeAlarm.AlarmTime);
            }
            if (dt.Rows.Count <= 0)
            {
                // No unacknowledged active pendant alarms to check
                _log.Debug("No unacknowledged active pendant alarms to check");
                return;
            }
            else
            {
                var alarmStr = dt.AsEnumerable().Select(x => "(" + x.Field<int>("FleetID").ToString() + "/" + x.Field<int>("VehicleID").ToString() + "/" + x.Field<DateTime>("AlarmTimestamp").ToString() + ")");
                _log.Debug("Checking alarms: (F/V/AlarmTimestamp)" + string.Join(", ", alarmStr));
            }
            using (SqlCommand command = new SqlCommand("isp_PendantRefresh", connection))
            {
                command.CommandType = CommandType.StoredProcedure;
                SqlParameter p = new SqlParameter() { ParameterName = "@Alarms", SqlDbType = SqlDbType.Structured, Value = dt, TypeName = "PendantAlarm" };
                command.Parameters.Add(p);
                using (var reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        int fleetId = (int)reader["FleetID"];
                        int vehicleId = (int)reader["VehicleID"];
                        DateTime alarmTimestamp = (DateTime)reader["AlarmTimestamp"];
                        bool acknowledged = PendentActiveAlarmList.AcknowledgeAlarm((byte)fleetId, (uint)vehicleId);
                        if (acknowledged)
                        {
                            _log.Info("Pendant Alarm acknowledgment detected - Updating cache (FleetID: " +
                                fleetId + " VehicleID: " + vehicleId + " Time: " + alarmTimestamp);
                        }
                    }
                }
            }
        }

        /// <summary>
        /// This method will be used to find the last serial number assigned to an IO Box and increment it by one.
        /// </summary>
        /// <returns>uint</returns>
        public uint GetNextIOBoxSerialNumber()
        {
            SqlConnection oConn = null;
            SqlDataAdapter objSQLDA = null;
            SqlCommand objSQLCmd = null;
            DataSet objDS = null;
            uint iResultID = 0;

            try
            {
                #region Create the SQL connection and select commands.
                oConn = new SqlConnection(sDSN);
                // We use an adapter on the Gateway table because
                // we need fast and frequent access to it.
                oConn.Open();
                #endregion
                #region Get the maximum IOBox ID
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("EXEC usp_GetNext1035ID", oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                if (objDS != null)
                {
                    if (objDS.Tables.Count > 0)
                    {
                        if (objDS.Tables[0].Rows.Count > 0)
                        {
                            if (objDS.Tables[0].Rows[0]["NextID"] != System.DBNull.Value)
                            {
                                iResultID = Convert.ToUInt32(objDS.Tables[0].Rows[0]["NextID"]);
                            }
                        }
                    }
                }
                objDS = null;
                objSQLCmd.Dispose();
                objSQLCmd = null;
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetNextIOBoxSerialNumber", ex);
            }
            return iResultID;
        }
        #region T_Vehicle table functions

        private PendantAlarmList _pendantActiveAlarmList;
        public PendantAlarmList PendentActiveAlarmList
        {
            get
            {
                return _pendantActiveAlarmList;
            }
        }

        /// <summary>
        /// This method will be used to refresh the vehicle datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshVehicleDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objLocalVehicleDS = null;
            Dictionary<long, int> objVehicleOdometerOffsets = new Dictionary<long, int>();
            Dictionary<long, int> objVehicleEngineHourOffsets = new Dictionary<long, int>();
            List<long> objVehicleUsingGPSOdometer = new List<long>();
            Dictionary<long, CardReaderConfiguration> objCardReaderConfigs = new Dictionary<long, CardReaderConfiguration>();

            try
            {
                lock (oVehicleDataSetSync)
                {
                    #region Retrieve the data from the T_Vehicle table
                    objSQLDA = new SqlDataAdapter();
                    objSQLCmd = new SqlCommand("EXEC isp_ListenerVehicleDetails", oConn);
                    objSQLCmd.CommandTimeout = 3600;
                    objSQLDA.SelectCommand = objSQLCmd;
                    objLocalVehicleDS = new DataSet();
                    objSQLDA.Fill(objLocalVehicleDS);
                    objLocalVehicleDS.Tables[0].TableName = "Vehicle";
                    objLocalVehicleDS.Tables[1].TableName = "CurrentAlarms";
                    objLocalVehicleDS.Tables[2].TableName = "AcknowledgedAlarms";
                    objLocalVehicleDS.Tables[3].TableName = "LastAlarms";
                    objLocalVehicleDS.Tables[4].TableName = "OdometerOffsets";
                    objLocalVehicleDS.Tables[5].TableName = "VehicleWatchList";
                    objLocalVehicleDS.Tables[6].TableName = "CardReaderConfigs";
                    objLocalVehicleDS.Tables[7].TableName = "Fleet";
                    objLocalVehicleDS.Tables[8].TableName = "EngineHourOffsets";
                    objSQLCmd.Dispose();
                    objSQLDA.Dispose();
                    objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                    if (dbVer > 5.9)
                    {
                        #region Odometer Offsets
                        if (objLocalVehicleDS.Tables.Contains("OdometerOffsets"))
                        {
                            foreach (DataRow dr in objLocalVehicleDS.Tables["OdometerOffsets"].Rows)
                            {
                                try
                                {
                                    int fleetId = Convert.ToInt32(dr["FleetID"]);
                                    int vehicleId = Convert.ToInt32(dr["VehicleID"]);
                                    int offsetValue = Convert.ToInt32(dr["Offset"]);
                                    long key = GetVehicleHashKey(fleetId, vehicleId);
                                    if (!objVehicleOdometerOffsets.ContainsKey(key))
                                    {
                                        objVehicleOdometerOffsets.Add(key, offsetValue);
                                    }
                                }
                                catch (System.Exception exGPSOdo)
                                {
                                    _log.Error(sClassName + "RefreshVehicleDataSet(System.Data.SqlClient.SqlConnection oConn)", exGPSOdo);
                                }
                            }
                        }
                        #endregion
                        #region Engine Hour Offsets
                        if (objLocalVehicleDS.Tables.Contains("EngineHourOffsets"))
                        {
                            foreach (DataRow dr in objLocalVehicleDS.Tables["EngineHourOffsets"].Rows)
                            {
                                try
                                {
                                    long key = GetVehicleHashKey(Convert.ToInt32(dr["FleetID"]), Convert.ToInt32(dr["VehicleID"]));
                                    if (!objVehicleEngineHourOffsets.ContainsKey(key))
                                    {
                                        objVehicleEngineHourOffsets.Add(key, Convert.ToInt32(dr["Offset"]));
                                    }
                                }
                                catch (System.Exception exEngineHourOffset)
                                {
                                    _log.Error(sClassName + "RefreshVehicleDataSet(System.Data.SqlClient.SqlConnection oConn)", exEngineHourOffset);
                                }
                            }
                        }
                        #endregion
                        #region Units using GPS odometer
                        if (objLocalVehicleDS.Tables.Contains("Vehicle"))
                        {
                            foreach (DataRow dr in objLocalVehicleDS.Tables["Vehicle"].Rows)
                            {
                                try
                                {
                                    if (dr["UseGPSOdometer"] != DBNull.Value && Convert.ToBoolean(dr["UseGPSOdometer"]))
                                    {
                                        long key = GetVehicleHashKey(Convert.ToInt32(dr["FleetID"]), Convert.ToInt32(dr["ID"]));
                                        if (!objVehicleUsingGPSOdometer.Contains(key))
                                            objVehicleUsingGPSOdometer.Add(key);
                                    }
                                }
                                catch (System.Exception exGPSOdo)
                                {
                                    _log.Error(sClassName + "RefreshVehicleDataSet(System.Data.SqlClient.SqlConnection oConn)", exGPSOdo);
                                }
                            }
                        }
                        #endregion
                        #region Units using card readers need additional config information
                        if (objLocalVehicleDS.Tables.Contains("CardReaderConfigs"))
                        {
                            foreach (DataRow dr in objLocalVehicleDS.Tables["CardReaderConfigs"].Rows)
                            {
                                try
                                {
                                    long key = GetVehicleHashKey(Convert.ToInt32(dr["FleetID"]), Convert.ToInt32(dr["VehicleID"]));
                                    if (!objCardReaderConfigs.ContainsKey(key))
                                    {
                                        CardReaderConfiguration config = new CardReaderConfiguration(dr);
                                        objCardReaderConfigs.Add(key, config);
                                    }
                                }
                                catch (System.Exception exCardReader)
                                {
                                    _log.Error(sClassName + "RefreshVehicleDataSet(System.Data.SqlClient.SqlConnection oConn)", exCardReader);
                                }
                            }
                        }
                        #endregion
                    }
                    mVehicleDataSet = new DataSet();
                    mVehicleDataSet.Tables.Add(objLocalVehicleDS.Tables["Vehicle"].Copy());
                    mVehicleOffsets = objVehicleOdometerOffsets;
                    mVehicleEngineHourOffsets = objVehicleEngineHourOffsets;
                    mVehicleWatchList = new DataSet();
                    mVehicleWatchList.Tables.Add(objLocalVehicleDS.Tables["VehicleWatchList"].Copy());
                    mVehicleUsingGPSOdometer = objVehicleUsingGPSOdometer;
                    mVehicleCardReaderConfigs = objCardReaderConfigs;
                    _fleetPossibleAccident.Clear();
                    foreach (DataRow r in objLocalVehicleDS.Tables["Fleet"].Rows)
                    {
                        _fleetPossibleAccident.Add(r.Field<int>("ID"), r.Field<bool>("PossibleAccidentTrigger"));
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshVehicleDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        /// <summary>
        /// This method will return the datarow for the given fleet/vehicle
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <returns>void</returns>
        public DataRow GetVehicleRow(byte cFleetId, uint iUnitID)
        {
            DataRow[] drVehicles = null;
            DataRow drRet = null;

            try
            {
                #region Get the data row for this vehicle
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleDataSet != null)
                    {
                        if (mVehicleDataSet.Tables.Count > 0)
                        {
                            if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                            {
                                drVehicles = mVehicleDataSet.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetId) + " AND ID = " + Convert.ToString(iUnitID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drVehicles != null)
                {
                    if (drVehicles.Length > 0)
                    {
                        drRet = drVehicles[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetUserInfoTag(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
            }
            return drRet;
        }
        /// <summary>
        /// This method will return the unit type (Tracking or Refrigeration) for the given fleet/vehicle
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <param name="ref bool bIsTrackingUnit"></param>
        /// <param name="ref bool bIsRefrigerationUnit"></param>
        /// <returns>void</returns>
        public void GetUnitType(byte cFleetID, uint iVehicleID, ref bool bIsTrackingUnit, ref bool bIsRefrigerationUnit)
        {
            DataRow[] drVehicles = null;
            try
            {
                // Default value.
                bIsTrackingUnit = true;
                bIsRefrigerationUnit = false;

                #region Get the data row for this vehicle
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleDataSet != null)
                    {
                        if (mVehicleDataSet.Tables.Count > 0)
                        {
                            if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                            {
                                drVehicles = mVehicleDataSet.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetID) + " AND ID = " + Convert.ToString(iVehicleID));
                            }
                        }
                    }
                }
                #endregion
                if (drVehicles != null)
                {
                    if (drVehicles.Length > 0)
                    {
                        #region Read the results
                        if (drVehicles[0].Table.Columns.Contains("IsTrackingUnit"))
                        {
                            if (drVehicles[0]["IsTrackingUnit"] != System.DBNull.Value)
                            {
                                if (Convert.ToInt32(drVehicles[0]["IsTrackingUnit"]) == 1)
                                    bIsTrackingUnit = true;
                                else
                                    bIsTrackingUnit = false;
                            }
                            if (drVehicles[0]["IsRefrigerationUnit"] != System.DBNull.Value)
                            {
                                if (Convert.ToInt32(drVehicles[0]["IsRefrigerationUnit"]) == 1)
                                    bIsRefrigerationUnit = true;
                                else
                                    bIsRefrigerationUnit = false;
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DatabaseInterface.GetUnitType(byte cFleetID, uint iUnitID, ref bool bIsTrackingUnit, ref bool bIsRefrigerationUnit)", ex);
            }
        }

        /// <summary>
        /// This method will return the current active odometer for the given fleet/vehicle.  This value is taken from a field in the
        /// T_OdometerOffset table
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <returns>void</returns>
        public int GetOdometerOffset(byte cFleetId, uint iUnitID)
        {
            try
            {
                long key = GetVehicleHashKey(cFleetId, (int)iUnitID);
                if (key > 0)
                {
                    lock (oVehicleDataSetSync)
                    {
                        if (mVehicleOffsets.ContainsKey(key))
                        {
                            return mVehicleOffsets[key];
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetOdometerOffset(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
            }
            return 0;
        }

        /// <summary>
        /// This method will return the current engine hour for the given fleet/vehicle.  This value is taken from a field in the T_EngineHourOffset table
        /// </summary>
        /// <returns>Engine hour offset value</returns>
        public int GetEngineHourOffset(byte cFleetId, uint iUnitID)
        {
            try
            {
                long key = GetVehicleHashKey(cFleetId, (int)iUnitID);
                if (key > 0)
                {
                    lock (oVehicleDataSetSync)
                    {
                        if (mVehicleEngineHourOffsets.ContainsKey(key))
                        {
                            return mVehicleEngineHourOffsets[key];
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetEngineHourOffset(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
            }
            return 0;
        }

        /// <summary>
        /// This method will return the current UseGPSOdometer for the given fleet/vehicle.
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <returns>void</returns>
        public bool UsingGPSOdometer(byte cFleetId, uint iUnitID)
        {
            try
            {
                long key = GetVehicleHashKey(cFleetId, (int)iUnitID);
                if (key > 0)
                {
                    if (mVehicleUsingGPSOdometer.Contains(key))
                    {
                        return true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UsingGPSOdometer(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
            }
            return false;
        }

        /// <summary>
        /// This method will return the current card reader configuration for the given fleet/vehicle's card reader asset.
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <returns>void</returns>
        public CardReaderConfiguration GetCardReaderConfig(byte cFleetId, uint iUnitID)
        {
            CardReaderConfiguration res = new CardReaderConfiguration();
            try
            {
                long key = GetVehicleHashKey(cFleetId, (int)iUnitID);
                if (key > 0)
                {
                    lock (oVehicleDataSetSync)
                    {
                        if (mVehicleCardReaderConfigs != null && mVehicleCardReaderConfigs.ContainsKey(key))
                        {
                            res = mVehicleCardReaderConfigs[key];
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetCardReaderConfig(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
            }
            return res;
        }

        /// <summary>
        /// This method will return the user info tag for the given fleet/vehicle.  This value is taken from a field in the
        /// T_Vehicle table that is specified by the DefaultUserInfoTagValue configuration item.
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <returns>void</returns>
        public string GetUserInfoTag(byte cFleetId, uint iUnitID)
        {
            DataRow[] drVehicles = null;
            string sRet = "";

            if (_sUserInfoDefaultField != "")
            {
                try
                {
                    #region Get the data row for this vehicle
                    lock (oVehicleDataSetSync)
                    {
                        if (mVehicleDataSet != null)
                        {
                            if (mVehicleDataSet.Tables.Count > 0)
                            {
                                if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                                {
                                    drVehicles = mVehicleDataSet.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetId) + " AND ID = " + Convert.ToString(iUnitID));
                                }
                            }
                        }
                    }
                    #endregion
                    if (drVehicles != null)
                    {
                        if (drVehicles.Length > 0)
                        {
                            #region Read the results
                            if (drVehicles[0].Table.Columns.Contains(_sUserInfoDefaultField))
                            {
                                if (drVehicles[0][_sUserInfoDefaultField] != System.DBNull.Value)
                                {
                                    sRet = Convert.ToString(drVehicles[0][_sUserInfoDefaultField]);
                                }
                            }
                            #endregion
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetUserInfoTag(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
                }
            }
            if (sRet == "")
                sRet = " ";
            return sRet;
        }
        /// <summary>
        /// This method will return the vehicle tag for the given fleet/vehicle.  This value is taken from the Spare3 field in the
        /// T_Vehicle table.
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <returns>void</returns>
        public string GetVehicleTag(byte cFleetId, uint iUnitID)
        {
            DataRow[] drVehicles = null;
            string sRet = "";

            try
            {
                #region Get the data row for this vehicle
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleDataSet != null)
                    {
                        if (mVehicleDataSet.Tables.Count > 0)
                        {
                            if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                            {
                                drVehicles = mVehicleDataSet.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetId) + " AND ID = " + Convert.ToString(iUnitID));
                            }
                        }
                    }
                }
                #endregion
                if (drVehicles != null)
                {
                    if (drVehicles.Length > 0)
                    {
                        #region Read the results
                        if (drVehicles[0].Table.Columns.Contains("Spare3"))
                        {
                            if (drVehicles[0]["Spare3"] != System.DBNull.Value)
                            {
                                sRet = Convert.ToString(drVehicles[0]["Spare3"]);
                            }
                        }
                        #endregion
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetVehicleTag(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ")", ex);
            }
            return sRet;
        }
        /// <summary>
        /// This method will update the UserDefinedValue9 column T_Vehicle table with the value in the sDetails string.
        /// </summary>
        /// <param name="byte cFleetId"></param>
        /// <param name="uint iUnitID"></param>
        /// <param name="string sDetails"></param>
        /// <returns>string</returns>
        private string UpdateVehicleConfigurationDetails(byte cFleetId, uint iUnitID, string sDetails)
        {
            System.Data.SqlClient.SqlConnection oConn = null;
            System.Data.SqlClient.SqlCommand oCmd = null;
            string sSQL = "";
            string sRet = "";
            int iRet = 0;
            bool bConnectionError = false;

            try
            {
                #region Update the mVehicleDataSet table with the new value.
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleDataSet != null)
                    {
                        if (mVehicleDataSet.Tables.Count > 0)
                        {
                            if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                            {
                                for (int X = 0; X < mVehicleDataSet.Tables[0].Rows.Count; X++)
                                {
                                    if (mVehicleDataSet.Tables[0].Rows[X]["FleetID"] != System.DBNull.Value && mVehicleDataSet.Tables[0].Rows[X]["ID"] != System.DBNull.Value)
                                    {
                                        if (Convert.ToInt32(mVehicleDataSet.Tables[0].Rows[X]["FleetID"]) == (int)cFleetId && Convert.ToInt32(mVehicleDataSet.Tables[0].Rows[X]["ID"]) == Convert.ToInt32(iUnitID))
                                        {
                                            mVehicleDataSet.Tables[0].Rows[X]["UserDefinedValue9"] = sDetails;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehicleConfigurationDetails(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ", string sDetails = '" + sDetails + "')", ex);
            }

            #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshDataSets() - Unable to connect to database - Will reconnect in 5 seconds", ex);
                bConnectionError = true;
            }
            while (bConnectionError)
            {
                try
                {
                    Thread.Sleep(5000);
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                    bConnectionError = false;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "RefreshDataSets() - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    bConnectionError = true;
                }
            }
            #endregion

            try
            {
                sSQL = "UPDATE T_Vehicle SET UserDefinedValue9 = '" + sDetails.Replace("'", "''") + "' WHERE FleetID = " + Convert.ToString((int)cFleetId) + " AND ID = " + Convert.ToString(iUnitID);
                oCmd = oConn.CreateCommand();
                oCmd.CommandText = sSQL;
                iRet = oCmd.ExecuteNonQuery();
                oCmd.Dispose();
                oCmd = null;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehicleConfigurationDetails(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ", string sDetails = '" + sDetails + "')", ex);
                sRet = "Error Updating Vehicle Configuration Details - " + ex.Message;
            }
            try
            {
                if (oConn != null)
                {
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehicleConfigurationDetails(byte cFleetId = " + Convert.ToString((int)cFleetId) + ", iUnitID = " + Convert.ToString(iUnitID) + ", string sDetails = '" + sDetails + "')", ex);
                sRet += "Error Updating Vehicle Configuration Details - " + ex.Message;
            }

            if (sRet == "")
                return null;
            else
                return sRet;
        }
        /// <summary>
        /// This method will get the current running configuration from the ConfigTotalGPPacket object and pass it to the UpdateVehicleConfigurationDetails method.
        /// </summary>
        /// <param name="ConfigTotalGPPacket confPacket"></param>
        /// <returns>string</returns>
        public string UpdateVehicleCurrentlyRunning(ConfigTotalGPPacket confPacket)
        {
            uint iScheduleGroupID = 0;
            uint iConfigID = 0;
            uint iRoutePointGroupID = 0;
            uint iSetPointGroupID = 0;
            DataRow[] drConfigs = null;
            DataRow[] drRoutePointGroups = null;
            DataRow[] drSetPoints = null;
            string sDetails = "";

            try
            {
                #region Config ID:
                if ((int)confPacket.cConfigFileActive != 0)
                {
                    drConfigs = null;
                    lock (oConfigDataSetSync)
                    {
                        if (mConfigDataSet != null)
                        {
                            if (mConfigDataSet.Tables.Count > 0)
                            {
                                if (mConfigDataSet.Tables[0].Rows.Count > 0)
                                {
                                    drConfigs = mConfigDataSet.Tables[0].Select("ConfigFileNo = " + Convert.ToString((int)confPacket.cConfigFileActive) + " AND FleetId = " + Convert.ToString((int)confPacket.cFleetId));
                                }
                            }
                        }
                    }
                    if (drConfigs == null)
                        return "UpdateVehicleSetup: Couldn't find a config for ConfigFileNo = " + Convert.ToString((int)confPacket.cConfigFileActive) + " AND FleetId = " + Convert.ToString((int)confPacket.cFleetId);
                    if (drConfigs.Length == 0)
                        return "UpdateVehicleSetup: Couldn't find a config for ConfigFileNo = " + Convert.ToString((int)confPacket.cConfigFileActive) + " AND FleetId = " + Convert.ToString((int)confPacket.cFleetId);
                    iConfigID = Convert.ToUInt32(drConfigs[0]["ID"]);
                }
                #endregion
                #region RoutePointGroup ID:
                if (confPacket.cRouteSetActive != 0)
                {
                    drRoutePointGroups = null;
                    lock (oRouteGroupDataSetSync)
                    {
                        if (mRouteGroupDataSet != null)
                        {
                            if (mRouteGroupDataSet.Tables.Count > 0)
                            {
                                if (mRouteGroupDataSet.Tables[0].Rows.Count > 0)
                                {
                                    drRoutePointGroups = mRouteGroupDataSet.Tables[0].Select("RoutePointNumber = " + Convert.ToString((int)confPacket.cRouteSetActive) + " AND (FleetID = " + Convert.ToString((int)confPacket.cFleetId) + " OR FleetID = 0)");
                                }
                            }
                        }
                    }
                    if (drRoutePointGroups == null)
                        return "UpdateVehicleSetup: Couldn't find a config for RoutePointNumber = " + Convert.ToString((int)confPacket.cRouteSetActive) + " AND (FleetID = " + Convert.ToString((int)confPacket.cFleetId) + " OR FleetID = 0)";
                    if (drRoutePointGroups.Length == 0)
                        return "UpdateVehicleSetup: Couldn't find a config for RoutePointNumber = " + Convert.ToString((int)confPacket.cRouteSetActive) + " AND (FleetID = " + Convert.ToString((int)confPacket.cFleetId) + " OR FleetID = 0)";
                    iRoutePointGroupID = Convert.ToUInt32(drRoutePointGroups[0]["ID"]);
                }
                #endregion
                #region SetPointGroup ID:
                if (confPacket.cSetPointSetActive != 0)
                {
                    drSetPoints = null;
                    lock (oSetGroupDataSetSync)
                    {
                        if (mSetGroupDataSet != null)
                        {
                            if (mSetGroupDataSet.Tables.Count > 0)
                            {
                                if (mSetGroupDataSet.Tables[0].Rows.Count > 0)
                                {
                                    drSetPoints = mSetGroupDataSet.Tables[0].Select("SetPointNumber = " + Convert.ToString((int)confPacket.cSetPointSetActive) + " AND (FleetId = " + Convert.ToString((int)confPacket.cFleetId) + " OR FleetID = 0)");
                                }
                            }
                        }
                    }
                    if (drSetPoints == null)
                        return "UpdateVehicleSetup: Couldn't find a config for SetPointNumber = " + Convert.ToString((int)confPacket.cSetPointSetActive) + " AND (FleetId = " + Convert.ToString((int)confPacket.cFleetId) + " OR FleetID = 0)";
                    if (drSetPoints.Length == 0)
                        return "UpdateVehicleSetup: Couldn't find a config for SetPointNumber = " + Convert.ToString((int)confPacket.cSetPointSetActive) + " AND (FleetId = " + Convert.ToString((int)confPacket.cFleetId) + " OR FleetID = 0)";
                    iSetPointGroupID = Convert.ToUInt32(drSetPoints[0]["ID"]);
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateVehicleCurrentlyRunning(ConfigTotalGPPacket confPacket)", ex);
                return "Exception raised while updating current settings:" + confPacket.cFleetId + " Unit " + confPacket.iVehicleId;
            }

            // Now we have the current IDs and versions, stick them together and write them 
            // back to the database in the format:
            // ConfigID,vM,vm,RouteID,vM,vm,SetId,vM,vm,SchedId,vM,vm
            sDetails =
                iConfigID + "," +
                confPacket.cConfigFileActiveVersionMajor + "," +
                confPacket.cConfigFileActiveVersionMinor + "," +
                iRoutePointGroupID + "," +
                confPacket.cRouteSetActiveVersionMajor + "," +
                confPacket.cRouteSetActiveVersionMinor + "," +
                iSetPointGroupID + "," +
                confPacket.cSetPointSetActiveVersionMajor + "," +
                confPacket.cSetPointSetActiveVersionMinor + "," +
                iScheduleGroupID + "," +
                confPacket.cScheduleActiveVersionMajor + "," +
                confPacket.cScheduleActiveVersionMinor;

            return UpdateVehicleConfigurationDetails(confPacket.cFleetId, confPacket.iVehicleId, sDetails);
        }
        /// <summary>
        /// This method will get the total mass and weight limit for a give fleet and vehicle.  The fTotalMass and iWeightLimit values will be populated with the result values.
        /// </summary>
        /// <param name="byte cFleetID"></param>
        /// <param name="uint iVehicleID"></param>
        /// <param name="float fTotalMass"></param>
        /// <param name="int iWeightLimit"></param>
        /// <returns>void</returns>
        public void GetMassForUnit(byte cFleetID, uint iVehicleID, ref float fTotalMass, ref float fWeightLimit)
        {
            DataRow[] drVehicles = null;
            try
            {
                // Default value.
                lock (oMassDataSetSync)
                {
                    if (mMassDataSet != null)
                    {
                        DataRow[] rows = mMassDataSet.Tables[0].Select(string.Format("FleetID = {0} AND VehicleID = {1}", (int)cFleetID, iVehicleID));
                        if ((rows != null) && (rows.Length > 0))
                        {
                            if (!rows[0].IsNull("TotalMass"))
                            {
                                fTotalMass = float.Parse(rows[0]["TotalMass"].ToString());
                            }
                        }
                    }
                }
                #region Get the data row for this vehicle
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleDataSet != null)
                    {
                        if (mVehicleDataSet.Tables.Count > 0)
                        {
                            if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                            {
                                drVehicles = mVehicleDataSet.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetID) + " AND ID = " + Convert.ToString(iVehicleID));
                            }
                        }
                    }
                }
                #endregion
                #region If a vehicle record was found, read the VehicleWeightLimit field
                if (drVehicles != null)
                {
                    if (drVehicles.Length > 0)
                    {
                        if (drVehicles[0].Table.Columns.Contains("VehicleWeightLimit"))
                        {
                            if (drVehicles[0]["VehicleWeightLimit"] != System.DBNull.Value)
                            {
                                int weightKg = Convert.ToInt32(drVehicles[0]["VehicleWeightLimit"]);
                                fWeightLimit = weightKg / 1000f;
                            }
                        }
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DatabaseInterface.GetMassForUnit(byte cFleetID = " + Convert.ToString((int)cFleetID) + ", uint iVehicleID = " + Convert.ToString(iVehicleID) + ", ref float fTotalMass = " + Convert.ToString(fTotalMass) + ", ref int iWeightLimit = " + Convert.ToString(fWeightLimit) + ")", ex);
            }
        }
        public ConfigWatchList GetWatchListState(byte cFleetID, uint iVehicleID)
        {
            ConfigWatchList oRet = null;
            DataRow[] oItems = null;
            try
            {
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleWatchList != null && mVehicleWatchList.Tables.Count > 0)
                    {
                        oItems = mVehicleWatchList.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetID) + " AND VehicleID = " + Convert.ToString(iVehicleID));
                        if (oItems != null && oItems.Length > 0)
                        {
                            oRet = new ConfigWatchList(_serverTime_DateFormat);
                            oRet.AddToWatchList = true;
                            oRet.WatchListFlags = Convert.ToInt32(oItems[0]["ListFlags"]);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DatabaseInterface.GetWatchListState(byte cFleetID = " + Convert.ToString((int)cFleetID) + ", uint iVehicleID = " + Convert.ToString(iVehicleID) + ")", ex);
            }
            return oRet;
        }
        public void SetWatchListState(byte cFleetID, uint iVehicleID, bool bAddToWatchList, int iListFlags)
        {
            DataRow[] oItems = null;
            try
            {
                lock (oVehicleDataSetSync)
                {
                    oItems = mVehicleWatchList.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetID) + " AND VehicleID = " + Convert.ToString(iVehicleID));
                    if (oItems != null && oItems.Length > 0)
                    {
                        if (!bAddToWatchList)
                        {
                            oItems[0].Delete();
                            mVehicleWatchList.AcceptChanges();
                        }
                    }
                    else
                    {
                        if (bAddToWatchList)
                        {
                            DataRow dr = mVehicleWatchList.Tables[0].NewRow();
                            dr["FleetID"] = (int)cFleetID;
                            dr["VehicleID"] = iVehicleID;
                            dr["ListFlags"] = iListFlags;
                            mVehicleWatchList.Tables[0].Rows.Add(dr);
                            mVehicleWatchList.Tables[0].AcceptChanges();
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DatabaseInterface.SetWatchListState(byte cFleetID, uint iVehicleID, bool bAddToWatchList, int iListFlags)", ex);
            }
        }
        public DateTime GetVehicleLocalTime(int fleetId, int vehicleId, DateTime dateTime)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = string.Format("select dbo.ufnVehicleLocalTime({0}, {1}, '{2}') as LocalTime",
                        fleetId, vehicleId, dateTime.ToString("MM/dd/yyyy HH:mm:ss"));
                    cmd.Connection = oConn;
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                    {
                        return Convert.ToDateTime(data.Tables[0].Rows[0]["LocalTime"]);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetVehicleLocalTime()", ex);
            }
            return DateTime.MinValue;
        }
        public int GetConfigIdForUnit(byte cFleetID, uint iVehicleID)
        {
            DataRow[] drVehicles = null;
            try
            {
                #region Get the data row for this vehicle
                lock (oVehicleDataSetSync)
                {
                    if (mVehicleDataSet != null)
                    {
                        if (mVehicleDataSet.Tables.Count > 0)
                        {
                            if (mVehicleDataSet.Tables[0].Rows.Count > 0)
                            {
                                drVehicles = mVehicleDataSet.Tables[0].Select("FleetID = " + Convert.ToString((int)cFleetID) + " AND ID = " + Convert.ToString(iVehicleID));
                            }
                        }
                    }
                }
                #endregion
                #region If a vehicle record was found, read the ConfigID field
                if (drVehicles != null)
                {
                    if (drVehicles.Length > 0)
                    {
                        if (drVehicles[0].Table.Columns.Contains("ConfigID"))
                        {
                            if (drVehicles[0]["ConfigID"] != System.DBNull.Value)
                            {
                                return Convert.ToInt32(drVehicles[0]["ConfigID"]);
                            }
                        }
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DatabaseInterface.GetConfigIdForUnit(byte cFleetID = " + Convert.ToString((int)cFleetID) + ", uint iVehicleID = " + Convert.ToString(iVehicleID) + ")", ex);
            }
            return -1;
        }
        public bool IsFleetPossibleAccident(int fleetid)
        {
            if (_fleetPossibleAccident.ContainsKey(fleetid))
            {
                return _fleetPossibleAccident[fleetid];
            }
            return true;
        }
        #endregion
        #region T_Config table functions
        /// <summary>
        /// This method will be used to refresh the configuration datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshConfigDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objDS = null;

            try
            {
                Dictionary<int, Dictionary<int, string>> _reasonCodeOverides = new Dictionary<int, Dictionary<int, string>>();
                Dictionary<int, Dictionary<int, int>> _reasonCodeOverideAssignments = new Dictionary<int, Dictionary<int, int>>();

                #region Retrieve the data from the T_Config table
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("EXEC usp_GetConfigForListenerLoad", oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;

                objDS.Tables[0].TableName = "Config";
                objDS.Tables[1].TableName = "ReasonCodeOverides";
                objDS.Tables[2].TableName = "ReasonCodeOverideAssignments";
                objDS.Tables[3].TableName = "ConfigTimePeriods";
                objDS.Tables[4].TableName = "ActiveStationayPeriods";
                objDS.Tables[5].TableName = "ActiveOutOfHoursUsage";
                objDS.Tables[6].TableName = "DefaultEcmConfig";
                objDS.Tables[7].TableName = "EcmConfig";
                lock (oConfigDataSetSync)
                {
                    mConfigDataSet = new DataSet();
                    mConfigDataSet.Tables.Add(objDS.Tables["Config"].Copy());
                    mConfigDataSet.Tables.Add(objDS.Tables["ActiveStationayPeriods"].Copy());
                    mConfigDataSet.Tables.Add(objDS.Tables["ActiveOutOfHoursUsage"].Copy());
                    mConfigDataSet.Tables.Add(objDS.Tables["DefaultEcmConfig"].Copy());
                    mConfigDataSet.Tables.Add(objDS.Tables["EcmConfig"].Copy());
                    mConfigDataSet.AcceptChanges();

                    _configStationaryPeriods.Clear();
                    _configOutOfHoursPeriods.Clear();
                    foreach (DataRow row in objDS.Tables["ConfigTimePeriods"].Rows)
                    {
                        int configId = Convert.ToInt32(row["ConfigID"]);
                        int periodType = Convert.ToInt32(row["ConfigTimeType"]);
                        DayOfWeek day = (DayOfWeek)Convert.ToInt32(row["DayOfTheWeek"]);
                        int start = Convert.ToInt32(row["StartTime"]);
                        int duration = Convert.ToInt32(row["Duration"]);
                        TimePeriod p = new TimePeriod(day, start, duration);
                        if (periodType == 0)
                        {
                            if (!_configStationaryPeriods.ContainsKey(configId))
                            {
                                _configStationaryPeriods.Add(configId, new TemporalObject(configId));
                            }
                            _configStationaryPeriods[configId].Times.Add(p);
                        }
                        else
                        {
                            if (!_configOutOfHoursPeriods.ContainsKey(configId))
                            {
                                _configOutOfHoursPeriods.Add(configId, new TemporalObject(configId));
                            }
                            _configOutOfHoursPeriods[configId].Times.Add(p);
                        }
                    }
                }

                #endregion
                #region Refresh the reason code overide groups
                foreach (DataRow drOveride in objDS.Tables[1].Rows)
                {
                    int groupId = Convert.ToInt32(drOveride["GroupID"]);
                    int reasonId = Convert.ToInt32(drOveride["ReasonID"]);
                    string name = Convert.ToString(drOveride["Name"]);
                    if (_reasonCodeOverides.ContainsKey(groupId))
                    {
                        if (_reasonCodeOverides[groupId].ContainsKey(reasonId))
                        {
                            _reasonCodeOverides[groupId].Remove(reasonId);
                        }
                        _reasonCodeOverides[groupId].Add(reasonId, name);
                    }
                    else
                    {
                        Dictionary<int, string> overides = new Dictionary<int, string>();
                        overides.Add(reasonId, name);
                        _reasonCodeOverides.Add(groupId, overides);
                    }
                }
                foreach (DataRow drAssignment in objDS.Tables[2].Rows)
                {
                    int groupId = Convert.ToInt32(drAssignment["GroupID"]);
                    int vehicleId = Convert.ToInt32(drAssignment["VehicleID"]);
                    int fleetId = Convert.ToInt32(drAssignment["FleetID"]);
                    if (_reasonCodeOverideAssignments.ContainsKey(fleetId))
                    {
                        if (_reasonCodeOverideAssignments[fleetId].ContainsKey(vehicleId))
                        {
                            _reasonCodeOverideAssignments[fleetId].Remove(vehicleId);
                        }
                        _reasonCodeOverideAssignments[fleetId].Add(vehicleId, groupId);
                    }
                    else
                    {
                        Dictionary<int, int> assignments = new Dictionary<int, int>();
                        assignments.Add(vehicleId, groupId);
                        _reasonCodeOverideAssignments.Add(fleetId, assignments);
                    }
                }
                lock (oReasonCodeOveridesSync)
                    mReasonCodeOverides = _reasonCodeOverides;
                lock (oReasonCodeOverideAssignmentsSync)
                    mReasonCodeOverideAssignments = _reasonCodeOverideAssignments;

                objDS.Dispose();
                objDS = null;
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshConfigDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }

        /// <summary>
        /// This method will be return the configuration data from for the given fleet and config ID
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int configID"></param>
        /// <returns>DataRow</returns>
        public string GetReasonName(int fleetId, int vehicleId, int reasonId)
        {
            string sRet = "";
            try
            {
                int groupId = 0;
                lock (oReasonCodeOveridesSync)
                    if (mReasonCodeOverideAssignments != null && mReasonCodeOverideAssignments.ContainsKey(fleetId))
                        if (mReasonCodeOverideAssignments[fleetId].ContainsKey(vehicleId))
                            groupId = mReasonCodeOverideAssignments[fleetId][vehicleId];
                lock (oReasonCodeOverideAssignmentsSync)
                    if (mReasonCodeOverides.ContainsKey(groupId))
                        if (mReasonCodeOverides[groupId].ContainsKey(reasonId))
                            sRet = mReasonCodeOverides[groupId][reasonId];
            }
            catch (Exception)
            {

            }
            return sRet;
        }
        /// <summary>
        /// This method will be return the configuration data from for the given fleet and config ID
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int configID"></param>
        /// <returns>DataRow</returns>
        public DataRow GetConfigRowByID(int fleet, int configID)
        {
            DataRow[] drConfigs = null;
            DataRow drResult = null;
            try
            {
                #region Get the data row for this fleet/config number
                lock (oConfigDataSetSync)
                {
                    if (mConfigDataSet != null)
                    {
                        if (mConfigDataSet.Tables.Count > 0)
                        {
                            if (mConfigDataSet.Tables[0].Rows.Count > 0)
                            {
                                drConfigs = mConfigDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND ID = " + Convert.ToString(configID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drConfigs != null)
                {
                    if (drConfigs.Length > 0)
                    {
                        drResult = drConfigs[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetConfigRowByID(int fleet = " + Convert.ToString(fleet) + ", int configID = " + Convert.ToString(configID) + ")", ex);
            }
            return drResult;
        }
        /// <summary>
        /// This method will be return the configuration data from for the given fleet and config file number (Not ID)
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int configNumber"></param>
        /// <returns>DataRow</returns>
        public DataRow GetConfigRowFromConfigNumber(int fleet, int configNumber)
        {
            DataRow[] drConfigs = null;
            DataRow drResult = null;
            try
            {
                #region Get the data row for this fleet/config number
                lock (oConfigDataSetSync)
                {
                    if (mConfigDataSet != null)
                    {
                        if (mConfigDataSet.Tables.Count > 0)
                        {
                            if (mConfigDataSet.Tables[0].Rows.Count > 0)
                            {
                                drConfigs = mConfigDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND ConfigFileNo = " + Convert.ToString(configNumber));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drConfigs != null)
                {
                    if (drConfigs.Length > 0)
                    {
                        drResult = drConfigs[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetConfigRowFromConfigNumber(int fleet = " + Convert.ToString(fleet) + ", int configNumber = " + Convert.ToString(configNumber) + ")", ex);
            }
            return drResult;
        }
        /// <summary>
        /// This method will be return if the configuration passed in in the ConfigTotalGPPacket is the current configuration for the vehicle.
        /// </summary>
        /// <param name="ConfigTotalGPPacket cfPacket"></param>
        /// <returns>bool</returns>
        public bool DoesANewerConfigVersionExist(ConfigTotalGPPacket cfPacket)
        {
            bool bRet = false;
            DataRow drRow = null;
            byte bVerMajor = (byte)0x00;
            byte bVerMinor = (byte)0x00;

            try
            {
                drRow = GetConfigRowFromConfigNumber((int)cfPacket.cFleetId, (int)cfPacket.cConfigFileActive);
                if (drRow != null)
                {
                    bVerMajor = Convert.ToByte(Convert.ToInt16(drRow["VersionMajor"]) & 0xFF);
                    bVerMinor = Convert.ToByte(Convert.ToInt16(drRow["VersionMinor"]) & 0xFF);
                    if ((bVerMajor == cfPacket.cConfigFileActiveVersionMajor) && (bVerMinor == cfPacket.cConfigFileActiveVersionMinor))
                        bRet = false;
                    else
                        bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DoesANewerConfigVersionExist(ConfigTotalGPPacket cfPacket)", ex);
                bRet = false;
            }
            return bRet;
        }
        /// <summary>
        /// This method will populate the ConfigTotalGPPacket object with the configuration version values for the specified vehicle.
        /// </summary>
        /// <param name="ref ConfigTotalGPPacket aPacket"></param>
        /// <returns>string</returns>
        public string GetVehicleConfigTotal(ref ConfigTotalGPPacket aPacket)
        {
            DataRow drVehicle = null;
            DataRow drConfig = null;
            DataRow drSetPointGroup = null;
            DataRow drRoutePointGroup = null;
            int scId = 0, cfId = 0, rgId = 0, spId = 0, iVehicleMass = 0;

            try
            {
                _log.Info("Performing Vehicle Config Check for " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId));
                #region Initialise the packet so that if there is a problem, something is there:
                aPacket.cConfigFileActive = 0;
                aPacket.cConfigFileActiveVersionMajor = 0;
                aPacket.cConfigFileActiveVersionMinor = 0;
                aPacket.cRouteSetActive = 0;
                aPacket.cRouteSetActiveVersionMajor = 0;
                aPacket.cRouteSetActiveVersionMinor = 0;
                aPacket.cRoutePointNumber = 0;
                aPacket.cSetPointSetActive = 0;
                aPacket.cSetPointSetActiveVersionMajor = 0;
                aPacket.cSetPointSetActiveVersionMinor = 0;
                aPacket.cScheduleActive = 0;
                aPacket.cScheduleActiveVersionMajor = 0;
                aPacket.cScheduleActiveVersionMinor = 0;
                aPacket.uSetPointGroup_Flags = 1;
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetVehicleConfigTotal(ref ConfigTotalGPPacket aPacket) (1)", ex);
            }
            try
            {
                #region Populate the packet
                drVehicle = GetVehicleRow(aPacket.cFleetId, aPacket.iVehicleId);
                if (drVehicle == null)
                    return "Couldn't locate vehicle entry for " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId);
                if (drVehicle["VehicleWeightLimit"] != System.DBNull.Value)
                    iVehicleMass = Convert.ToInt32(drVehicle["VehicleWeightLimit"]);

                cfId = System.Convert.ToInt32(drVehicle["ConfigID"]) & 0xFFFF;
                spId = System.Convert.ToInt32(drVehicle["SetPointGroupID"]) & 0xFFFF;
                rgId = System.Convert.ToInt32(drVehicle["RoutePointGroupID"]) & 0xFFFF;
                scId = System.Convert.ToInt32(drVehicle["ScheduleGroupID"]) & 0xFFFF;

                #region Load the config details
                if (cfId == 0)
                {
                    aPacket.cConfigFileActive = (byte)0x00;
                    aPacket.cConfigFileActiveVersionMajor = (byte)0x00;
                    aPacket.cConfigFileActiveVersionMinor = (byte)0x00;
                }
                else
                {
                    drConfig = GetConfigRowByID((int)aPacket.cFleetId, cfId);
                    if (drConfig != null)
                    {
                        aPacket.cConfigFileActive = Convert.ToByte(drConfig["ConfigFileNo"]);
                        aPacket.cConfigFileActiveVersionMajor = Convert.ToByte(drConfig["VersionMajor"]);
                        aPacket.cConfigFileActiveVersionMinor = Convert.ToByte(drConfig["VersionMinor"]);
                    }
                    else
                    {
                        _log.Info("Could not find Config ID " + Convert.ToString(cfId) + " for " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId));
                        aPacket.cConfigFileActive = (byte)0x00;
                        aPacket.cConfigFileActiveVersionMajor = (byte)0x00;
                        aPacket.cConfigFileActiveVersionMinor = (byte)0x00;
                    }
                }
                //set ecm config to same as config
                aPacket.cEcmConfigFileActive = aPacket.cConfigFileActive;
                aPacket.cEcmConfigFileActiveVersionMajor = aPacket.cConfigFileActiveVersionMajor;
                aPacket.cEcmConfigFileActiveVersionMinor = aPacket.cConfigFileActiveVersionMinor;
                #endregion
                #region Load the set point details
                if (spId == 0)
                {
                    aPacket.cSetPointSetActive = (byte)0x00;
                    aPacket.cSetPointSetActiveVersionMajor = (byte)0x00;
                    aPacket.cSetPointSetActiveVersionMinor = (byte)0x00;
                }
                else
                {
                    drSetPointGroup = GetSetPointRowByID((int)aPacket.cFleetId, spId);
                    if (drSetPointGroup != null)
                    {
                        aPacket.cSetPointSetActive = Convert.ToByte(drSetPointGroup["SetPointNumber"]);
                        aPacket.cSetPointSetActiveVersionMajor = Convert.ToByte(drSetPointGroup["VersionMajor"]);
                        aPacket.cSetPointSetActiveVersionMinor = Convert.ToByte(drSetPointGroup["VersionMinor"]);
                        //if (drSetPointGroup.Table.Columns.Contains("Spare1") && drSetPointGroup["Spare1"] != System.DBNull.Value)
                        //    aPacket.uSetPointGroup_Flags = Convert.ToUInt32(drSetPointGroup["Spare1"]);
                        //else
                        //    aPacket.uSetPointGroup_Flags = 0;
                    }
                    else
                    {
                        _log.Info("Could not find SetpointGroupID " + Convert.ToString(spId) + " for " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId));
                        aPacket.cSetPointSetActive = (byte)0x00;
                        aPacket.cSetPointSetActiveVersionMajor = (byte)0x00;
                        aPacket.cSetPointSetActiveVersionMinor = (byte)0x00;
                    }
                }
                #endregion
                #region Load the route point details
                if (rgId == 0)
                {
                    aPacket.cRouteSetActive = (byte)0x00;
                    aPacket.cRouteSetActiveVersionMajor = (byte)0x00;
                    aPacket.cRouteSetActiveVersionMinor = (byte)0x00;
                }
                else
                {
                    drRoutePointGroup = GetRoutePointRowByID((int)aPacket.cFleetId, rgId);
                    if (drRoutePointGroup != null)
                    {
                        aPacket.cRouteSetActive = Convert.ToByte(drRoutePointGroup["SetPointNumber"]);
                        aPacket.cRouteSetActiveVersionMajor = Convert.ToByte(drRoutePointGroup["VersionMajor"]);
                        aPacket.cRouteSetActiveVersionMinor = Convert.ToByte(drRoutePointGroup["VersionMinor"]);
                    }
                    else
                    {
                        _log.Info("Could not find RoutePointGroupID " + Convert.ToString(rgId) + " for " + Convert.ToString((int)aPacket.cFleetId) + "/" + Convert.ToString(aPacket.iVehicleId));
                        aPacket.cRouteSetActive = (byte)0x00;
                        aPacket.cRouteSetActiveVersionMajor = (byte)0x00;
                        aPacket.cRouteSetActiveVersionMinor = (byte)0x00;
                    }
                }
                #endregion
                #region Load the schedule details
                aPacket.cScheduleActive = (byte)0x00;
                aPacket.cScheduleActiveVersionMajor = (byte)0x00;
                aPacket.cScheduleActiveVersionMinor = (byte)0x00;
                #endregion
                #region Load the vehicle weight limit details
                aPacket.uVehicleMass = (uint)iVehicleMass;
                #endregion
                #region Load Listener version
                System.Version version = this.GetType().Assembly.GetName().Version;
                aPacket.cListenerVersionMajor = GetByteFromInt(version.Major);
                aPacket.cListenerVersionMinor = GetByteFromInt(version.Minor);
                aPacket.cListenerVersionRevison = GetByteFromInt(version.Revision);
                #endregion
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetVehicleConfigTotal(ref ConfigTotalGPPacket aPacket) (2)", ex);
            }
            return null;
        }
        /// <summary>
        /// This method will return the diagnostics level the vehicle is supposed to be running
        /// </summary>
        /// <param name="ref ConfigTotalGPPacket aPacket"></param>
        /// <returns>the diagnostics level 0 = Error, 1 = Warning, 2 = Info, 3 = Debug</returns>
        public short GetVehicleDiagnosticsLevel(ConfigTotalGPPacket aPacket)
        {
            DataRow drVehicle = null;
            try
            {
                drVehicle = GetVehicleRow(aPacket.cFleetId, aPacket.iVehicleId);
                if (drVehicle == null)
                {
                    //could not find the vehicle so return the default of (Error)
                    return 0;
                }
                //diagnostics level is stored in 
                if (drVehicle["UserDefinedValue7"] != System.DBNull.Value)
                {
                    return Convert.ToInt16(drVehicle["UserDefinedValue7"]);
                }
            }
            catch
            {
            }
            return 0;
        }
        public void SaveDiagnosticsLevelRunning(Vehicle v, short diagnosticLevel)
        {
            try
            {
                DataRow drVehicle = GetVehicleRow(v.cFleet, v.iUnit);
                if (drVehicle != null)
                {
                    //diagnostics level is stored in 
                    drVehicle["UserDefinedValue8"] = diagnosticLevel;
                }
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleUpdateDiagnosticLevelRunning {0}, {1}, {2}", (int)v.cFleet, v.iUnit, diagnosticLevel);
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                }
            }
            catch
            {
            }
        }
        #region Load Confifguration Packet Functions
        /// <summary>
        /// This method will populate the ConfigFileGPPacket object with the configuration values for the specified vehicle.
        /// </summary>
        /// <param name="ref ConfigTotalGPPacket aPacket"></param>
        /// <returns>string</returns>
        public string GetConfigFile(ref ConfigFileGPPacket cfPacket)
        {
            DataRow drConfig = null;
            DataRow drVehicle = null;
            int iConfigID = 0;
            try
            {
                drConfig = GetConfigRowFromConfigNumber((int)cfPacket.cFleetId, (int)cfPacket.cConfigFileNo);
                if (drConfig == null)
                {
                    drVehicle = GetVehicleRow(cfPacket.cFleetId, cfPacket.iVehicleId);
                    if (drVehicle == null)
                        return "Could not find a configuration or vehicle record for " + Convert.ToString(cfPacket.cFleetId) + "/" + Convert.ToString(cfPacket.iVehicleId);

                    iConfigID = Convert.ToInt32(drVehicle["ConfigID"]);
                    drConfig = GetConfigRowByID((int)cfPacket.cFleetId, iConfigID);
                    if (drConfig == null)
                        return "Could not find a configuration record for " + Convert.ToString(cfPacket.cFleetId) + "/" + Convert.ToString(cfPacket.iVehicleId);

                    cfPacket.cConfigFileNo = (byte)Convert.ToInt32(drConfig["ConfigFileNo"]);
                }
                #region Populate the configuration packet values
                #region Populate the configuration file and listener version values
                cfPacket.cVersionMajor = (byte)Convert.ToInt32(drConfig["VersionMajor"]);
                cfPacket.cVersionMinor = (byte)Convert.ToInt32(drConfig["VersionMinor"]);
                System.Version version = this.GetType().Assembly.GetName().Version;
                cfPacket.cListenerVersionMajor = GetByteFromInt(version.Major);
                cfPacket.cListenerVersionMinor = GetByteFromInt(version.Minor);
                cfPacket.cListenerVersionRevison = GetByteFromInt(version.Revision);
                #endregion
                #region Populate the GPS Configuration Values
                cfPacket.GpsTuningActive = Convert.ToByte(VerifyConfigField(drConfig, "GpsTuningActive", (int)0));
                cfPacket.Low_Speed_Delta_Threshold_In_Knots = Convert.ToByte(VerifyConfigField(drConfig, "GPS_LowSpeedDeltaThresholdInKnots", (int)7));
                cfPacket.High_Speed_Delta_Threshold_In_Knots = Convert.ToByte(VerifyConfigField(drConfig, "GPS_HighSpeedDeltaThresholdInKnots", (int)4));
                cfPacket.Hdop_Hysteresis_Threshold = Convert.ToByte(VerifyConfigField(drConfig, "GPS_HdopHysteresisThreshold", (int)40));
                cfPacket.GPS_CancelHysteresisTime = Convert.ToByte(VerifyConfigField(drConfig, "GPS_CancelHysteresisTime", (int)30));
                cfPacket.Seconds_For_Averaging_Speed = Convert.ToByte(VerifyConfigField(drConfig, "GPS_SecondsForAveragingSpeed", (int)0));
                cfPacket.Hysterisis_recovery_good_values = Convert.ToByte(VerifyConfigField(drConfig, "GPS_HysterisisRecoveryGoodValues", (int)3));
                cfPacket.NumberOfSatellitesThreshold = Convert.ToByte(VerifyConfigField(drConfig, "GPS_NumberOfSatellitesThreshold", (int)4));
                #endregion
                #region Populate the MDT Settings
                if (System.Configuration.ConfigurationManager.AppSettings["LoadConfigDataSettings"] == "true")
                {
                    cfPacket.sMDTIPAddress = VerifyConfigField(drConfig, "MDTIPAddress", "0.0.0.0");
                    cfPacket.iMDTPortAddress = VerifyConfigField(drConfig, "MDTPort", (int)0);
                    cfPacket.iMDTKeepAliveFreq = VerifyConfigField(drConfig, "MDTKeepAliveFreq", (int)300);
                }
                else
                {
                    cfPacket.sMDTIPAddress = "0.0.0.0";
                    cfPacket.iMDTPortAddress = 0;
                    cfPacket.iMDTKeepAliveFreq = 300;
                }
                #endregion
                #region Overspeed, Vehicle Speed, RPM Speed Zones
                if (dbVer > 5.7)
                {
                    try
                    {
                        cfPacket.cOverspeedZone1 = GetFloorByteFromFloat(VerifyConfigField(drConfig, "OverspeedZone1", (double)0d));
                        cfPacket.cOverspeedZone2 = GetFloorByteFromFloat(VerifyConfigField(drConfig, "OverspeedZone2", (double)0d));
                        cfPacket.cOverspeedZone3 = GetFloorByteFromFloat(VerifyConfigField(drConfig, "OverspeedZone3", (double)0d));
                        cfPacket.cOverspeedZone4 = GetFloorByteFromFloat(VerifyConfigField(drConfig, "OverspeedZone4", (double)0d));

                        cfPacket.cOverspeedZone1DecimalPlaces = GetByteFromFloatDecimalValue(VerifyConfigField(drConfig, "OverspeedZone1", (double)0));
                        cfPacket.cOverspeedZone2DecimalPlaces = GetByteFromFloatDecimalValue(VerifyConfigField(drConfig, "OverspeedZone2", (double)0));
                        cfPacket.cOverspeedZone3DecimalPlaces = GetByteFromFloatDecimalValue(VerifyConfigField(drConfig, "OverspeedZone3", (double)0));
                        cfPacket.cOverspeedZone4DecimalPlaces = GetByteFromFloatDecimalValue(VerifyConfigField(drConfig, "OverspeedZone4", (double)0));
                        cfPacket.cVehicleSpeedZone1 = GetByteFromFloat(VerifyConfigField(drConfig, "VehicleSpeedZone1", (double)0d));
                        cfPacket.cVehicleSpeedZone2 = GetByteFromFloat(VerifyConfigField(drConfig, "VehicleSpeedZone2", (double)0d));
                        cfPacket.cVehicleSpeedZone3 = GetByteFromFloat(VerifyConfigField(drConfig, "VehicleSpeedZone3", (double)0d));
                        cfPacket.cRPMSpeedZone1 = GetByteFromInt(VerifyConfigField(drConfig, "RPMSpeedZone1", (int)0));
                        cfPacket.cRPMSpeedZone2 = GetByteFromInt(VerifyConfigField(drConfig, "RPMSpeedZone2", (int)0));
                        cfPacket.cRPMSpeedZone3 = GetByteFromInt(VerifyConfigField(drConfig, "RPMSpeedZone3", (int)0));
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching Overspeed, Vehicle Speed, RPM Speed Zones Settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                else
                {
                    try
                    {
                        cfPacket.cOverspeedZone1 = (byte)0x00;
                        cfPacket.cOverspeedZone2 = (byte)0x00;
                        cfPacket.cOverspeedZone3 = (byte)0x00;
                        cfPacket.cOverspeedZone4 = (byte)0x00;
                        cfPacket.cVehicleSpeedZone1 = (byte)0x00;
                        cfPacket.cVehicleSpeedZone2 = (byte)0x00;
                        cfPacket.cVehicleSpeedZone3 = (byte)0x00;
                        cfPacket.cRPMSpeedZone1 = (byte)0x00;
                        cfPacket.cRPMSpeedZone2 = (byte)0x00;
                        cfPacket.cRPMSpeedZone3 = (byte)0x00;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching Overspeed, Vehicle Speed, RPM Speed Zones Settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                #endregion
                #region Angel Gear Settings
                if (dbVer > 5.7)
                {
                    try
                    {
                        cfPacket.cAngelGearSpeed = GetByteFromFloat(VerifyConfigField(drConfig, "AngelGearSpeed", (double)0));
                        cfPacket.cAngelGearRPM = Get2BytesFromInt(VerifyConfigField(drConfig, "AngelGearRPM", (int)0));
                        cfPacket.cAngelGearTime = GetByteFromInt(VerifyConfigField(drConfig, "AngelGearTime", (int)0));
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching Angel Gear Settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                else
                {
                    try
                    {
                        cfPacket.cAngelGearSpeed = (byte)0x00;
                        cfPacket.cAngelGearRPM = Get2BytesFromInt(0);
                        cfPacket.cAngelGearTime = (byte)0x00;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching Angel Gear Settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                #endregion
                #region Fatigue Reporting Rules
                // These rules have been superseeded by the fatigue managment plugin.
                if (dbVer >= 5.9)
                {
                    cfPacket.iFatigueThresholdMinutesContinuousIgnOn = VerifyConfigField(drConfig, "IgOnFatigueLimitMinutes", (int)0);
                    cfPacket.iFatigueThresholdMinutes24Hrs = VerifyConfigField(drConfig, "Hrs24FatigueLimitMinutes", (int)0);
                }
                else
                {
                    cfPacket.iFatigueThresholdMinutesContinuousIgnOn = 0;
                    cfPacket.iFatigueThresholdMinutes24Hrs = 0;
                }

                #endregion
                #region Refrigeration Reporting Rules
                if (dbVer >= 6)
                {
                    cfPacket.iConcreteStationaryMinutes = _iConcreteStationaryMinutes;
                    cfPacket.iRefrigerationFlags = VerifyConfigField(drConfig, "RefrigerationFlags", (int)0);
                    cfPacket.iRefrigerationMinVolts = VerifyConfigField(drConfig, "RefrigerationMinVolts", (int)0);
                    cfPacket.iRefrigerationFuelAlert = VerifyConfigField(drConfig, "RefrigerationFuelAlert", (int)0);
                    cfPacket.iRefrigerationDwellSetPoint = VerifyConfigField(drConfig, "RefrigerationDwellSetPoint", (int)0);
                    cfPacket.iRefrigerationDwellSupply = VerifyConfigField(drConfig, "RefrigerationDwellSupply", (int)0);
                    cfPacket.iRefrigerationDwellReturn = VerifyConfigField(drConfig, "RefrigerationDwellReturn", (int)0);
                    cfPacket.iRefrigerationDwellEvap = VerifyConfigField(drConfig, "RefrigerationDwellEvap", (int)0);
                    cfPacket.iRefrigerationZone1SetPoint = VerifyConfigField(drConfig, "RefrigerationZone1SetPoint", (short)0) / 10;
                    cfPacket.iRefrigerationZone1SetPointTol = VerifyConfigField(drConfig, "RefrigerationZone1SetPointTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone1Supply = VerifyConfigField(drConfig, "RefrigerationZone1Supply", (short)0) / 10;
                    cfPacket.iRefrigerationZone1SupplyTol = VerifyConfigField(drConfig, "RefrigerationZone1SupplyTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone1Return = VerifyConfigField(drConfig, "RefrigerationZone1Return", (short)0) / 10;
                    cfPacket.iRefrigerationZone1ReturnTol = VerifyConfigField(drConfig, "RefrigerationZone1ReturnTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone1Evap = VerifyConfigField(drConfig, "RefrigerationZone1Evap", (short)0) / 10;
                    cfPacket.iRefrigerationZone1EvapTol = VerifyConfigField(drConfig, "RefrigerationZone1EvapTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone2SetPoint = VerifyConfigField(drConfig, "RefrigerationZone2SetPoint", (short)0) / 10;
                    cfPacket.iRefrigerationZone2SetPointTol = VerifyConfigField(drConfig, "RefrigerationZone2SetPointTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone2Supply = VerifyConfigField(drConfig, "RefrigerationZone2Supply", (short)0) / 10;
                    cfPacket.iRefrigerationZone2SupplyTol = VerifyConfigField(drConfig, "RefrigerationZone2SupplyTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone2Return = VerifyConfigField(drConfig, "RefrigerationZone2Return", (short)0) / 10;
                    cfPacket.iRefrigerationZone2ReturnTol = VerifyConfigField(drConfig, "RefrigerationZone2ReturnTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone2Evap = VerifyConfigField(drConfig, "RefrigerationZone2Evap", (short)0) / 10;
                    cfPacket.iRefrigerationZone2EvapTol = VerifyConfigField(drConfig, "RefrigerationZone2EvapTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone3SetPoint = VerifyConfigField(drConfig, "RefrigerationZone3SetPoint", (short)0) / 10;
                    cfPacket.iRefrigerationZone3SetPointTol = VerifyConfigField(drConfig, "RefrigerationZone3SetPointTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone3Supply = VerifyConfigField(drConfig, "RefrigerationZone3Supply", (short)0) / 10;
                    cfPacket.iRefrigerationZone3SupplyTol = VerifyConfigField(drConfig, "RefrigerationZone3SupplyTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone3Return = VerifyConfigField(drConfig, "RefrigerationZone3Return", (short)0) / 10;
                    cfPacket.iRefrigerationZone3ReturnTol = VerifyConfigField(drConfig, "RefrigerationZone3ReturnTol", (short)0) / 10;
                    cfPacket.iRefrigerationZone3Evap = VerifyConfigField(drConfig, "RefrigerationZone3Evap", (short)0) / 10;
                    cfPacket.iRefrigerationZone3EvapTol = VerifyConfigField(drConfig, "RefrigerationZone3EvapTol", (short)0) / 10;
                }
                else
                {
                    cfPacket.iConcreteStationaryMinutes = _iConcreteStationaryMinutes;
                    cfPacket.iRefrigerationFlags = 0;
                    cfPacket.iRefrigerationMinVolts = 0;
                    cfPacket.iRefrigerationFuelAlert = 0;
                    cfPacket.iRefrigerationDwellSetPoint = 0;
                    cfPacket.iRefrigerationDwellSupply = 0;
                    cfPacket.iRefrigerationDwellReturn = 0;
                    cfPacket.iRefrigerationDwellEvap = 0;
                    cfPacket.iRefrigerationZone1SetPoint = 0;
                    cfPacket.iRefrigerationZone1SetPointTol = 0;
                    cfPacket.iRefrigerationZone1Supply = 0;
                    cfPacket.iRefrigerationZone1SupplyTol = 0;
                    cfPacket.iRefrigerationZone1Return = 0;
                    cfPacket.iRefrigerationZone1ReturnTol = 0;
                    cfPacket.iRefrigerationZone1Evap = 0;
                    cfPacket.iRefrigerationZone1EvapTol = 0;
                    cfPacket.iRefrigerationZone2SetPoint = 0;
                    cfPacket.iRefrigerationZone2SetPointTol = 0;
                    cfPacket.iRefrigerationZone2Supply = 0;
                    cfPacket.iRefrigerationZone2SupplyTol = 0;
                    cfPacket.iRefrigerationZone2Return = 0;
                    cfPacket.iRefrigerationZone2ReturnTol = 0;
                    cfPacket.iRefrigerationZone2Evap = 0;
                    cfPacket.iRefrigerationZone2EvapTol = 0;
                    cfPacket.iRefrigerationZone3SetPoint = 0;
                    cfPacket.iRefrigerationZone3SetPointTol = 0;
                    cfPacket.iRefrigerationZone3Supply = 0;
                    cfPacket.iRefrigerationZone3SupplyTol = 0;
                    cfPacket.iRefrigerationZone3Return = 0;
                    cfPacket.iRefrigerationZone3ReturnTol = 0;
                    cfPacket.iRefrigerationZone3Evap = 0;
                    cfPacket.iRefrigerationZone3EvapTol = 0;
                }
                #endregion
                #region Input 1 and 2 Divide factors
                if (dbVer > 5.7)
                {
                    try
                    {
                        cfPacket.cInput1RPMCounterDivideFactor = GetByteFromInt(VerifyConfigField(drConfig, "Input1RPMCounterDivideFactor", (int)0));
                        cfPacket.cInput2VehicleSpeedCounterDivideFactor = GetByteFromInt(VerifyConfigField(drConfig, "Input2VehicleSpeedCounterDivideFactor", (int)0));
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching config Input 1 and 2 Divide factors: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                else
                {
                    try
                    {
                        cfPacket.cInput1RPMCounterDivideFactor = (byte)0x00;
                        cfPacket.cInput2VehicleSpeedCounterDivideFactor = (byte)0x00;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching config Input 1 and 2 Divide factors: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                #endregion
                #region Reporting
                try
                {
                    cfPacket.iUnitFreqReportSecs = Convert.ToInt16(VerifyConfigField(drConfig, "UnitFreqReportSecs", (int)0));
                    cfPacket.iUnitFreqReportMins = Convert.ToInt16(VerifyConfigField(drConfig, "UnitFreqReportMins", (int)2));
                    cfPacket.iUnitFreqReportExcpDist = Convert.ToInt16(VerifyConfigField(drConfig, "UnitFreqReportExcpDist", (int)0));
                    cfPacket.iUnitReportDistance = Convert.ToInt16(VerifyConfigField(drConfig, "UnitReportDistance", (int)0));
                    cfPacket.iGPSNotValidReport = Convert.ToInt16(VerifyConfigField(drConfig, "GPSNotValidReport", (int)0));
                    cfPacket.cLastSecGPSStores = Convert.ToByte(VerifyConfigField(drConfig, "LastSecGPSStores", (int)0));
                    cfPacket.cLastMinGPSStores = Convert.ToByte(VerifyConfigField(drConfig, "LastMinGPSStores", (int)0));
                    cfPacket.cLastHourGPSStores = Convert.ToByte(VerifyConfigField(drConfig, "LastHourGPSStores", (int)0));
                    cfPacket.cLastXMinGPSStores = Convert.ToByte(VerifyConfigField(drConfig, "LastXMinGPSStores", (int)0));
                    cfPacket.cLastXMinPeriod = Convert.ToByte(VerifyConfigField(drConfig, "LastXMinPeriod", (int)0));
                    cfPacket.cMaxSpeed = Convert.ToByte(VerifyConfigField(drConfig, "MaxSpeed", (int)0));
                    cfPacket.cMinSpeed = Convert.ToByte(VerifyConfigField(drConfig, "MinSpeed", (int)0));
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Reporting settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Simple Rules Fields
                try
                {
                    //POD cfPacket.iGPSRules = VerifyConfigField(drConfig, "GPSRules", (int)0);
                    try
                    {
                        string defaultGPSRules = System.Configuration.ConfigurationManager.AppSettings["DefaultGPSRules"];
                        if ((defaultGPSRules != null) && (defaultGPSRules != ""))
                            cfPacket.iGPSRules = Convert.ToInt32(defaultGPSRules);
                        else
                            cfPacket.iGPSRules = 2;
                    }
                    catch
                    {
                        cfPacket.iGPSRules = 2;
                    }

                    cfPacket.iGPSRules2 = VerifyConfigField(drConfig, "GPSRules2", (int)0);
                    cfPacket.iGPSRules3 = VerifyConfigField(drConfig, "GPSRules3", (int)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config GPS Rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                try
                {
                    cfPacket.iSpeedRules = VerifyConfigField(drConfig, "SpeedRules", (int)0);
                    cfPacket.iSpeedRules2 = VerifyConfigField(drConfig, "SpeedRules2", (int)0);
                    cfPacket.iSpeedRules3 = VerifyConfigField(drConfig, "SpeedRules3", (int)0);
                    cfPacket.iSpeedRules4 = VerifyConfigField(drConfig, "SpeedRules4", (int)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Speed rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }

                try
                {
                    cfPacket.cSpeedZone1Tolerace = GetByteFromInt(VerifyConfigField(drConfig, "OverspeedSecTol1", (int)0));
                    cfPacket.cSpeedZone2Tolerace = GetByteFromInt(VerifyConfigField(drConfig, "OverspeedSecTol2", (int)0));
                    cfPacket.cSpeedZone3Tolerace = GetByteFromInt(VerifyConfigField(drConfig, "OverspeedSecTol3", (int)0));
                    cfPacket.cSpeedZone4Tolerace = GetByteFromInt(VerifyConfigField(drConfig, "OverspeedSecTol4", (int)0));
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Speed rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }

                try
                {
                    // OR on the lowest 9 bits - they are not editable but must be ON
                    cfPacket.iSendingRules = VerifyConfigField(drConfig, "SendingRules", (int)0) | 0x1FF;
                    cfPacket.iSendingRules2 = VerifyConfigField(drConfig, "SendingRules2", (int)327720);
                    cfPacket.iSendingRules3 = VerifyConfigField(drConfig, "SendingRules3", (int)30);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Sending rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                try
                {
                    cfPacket.iSetPointRules = VerifyConfigField(drConfig, "SetPointRules", (int)0);
                    cfPacket.iSetPointRules2 = VerifyConfigField(drConfig, "SetPointRules2", (int)0);
                    cfPacket.iSetPointRules3 = VerifyConfigField(drConfig, "SetPointRules3", (int)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Set point rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                try
                {
                    cfPacket.iRoutePointRules = VerifyConfigField(drConfig, "RoutePointRules", (int)0);
                    cfPacket.iRoutePointRules2 = VerifyConfigField(drConfig, "RoutePointRules2", (int)0);
                    cfPacket.iRoutePointRules3 = VerifyConfigField(drConfig, "RoutePointRules3", (int)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Route point rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                try
                {
                    cfPacket.iStatusRules = VerifyConfigField(drConfig, "StatusRules", (int)0);
                    cfPacket.iStatusRules2 = VerifyConfigField(drConfig, "StatusRules2", (int)0);
                    cfPacket.iStatusRules3 = VerifyConfigField(drConfig, "StatusRules3", (int)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Status rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                try
                {
                    cfPacket.iSMSRules = VerifyConfigField(drConfig, "SMSRules", (int)0);
                    cfPacket.iSMSRules2 = VerifyConfigField(drConfig, "SMSRules2", (int)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config SMS rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Get the "G-Force by change in speed" settings
                if (dbVer > 5.7)
                {
                    try
                    {
                        cfPacket.iSMSRules3 = VerifyConfigField(drConfig, "SMSRules3", (int)0);
                        // Byte 0 = Time
                        // Byte 2 = Speed
                        //int iGForceSpeed = VerifyConfigField(drConfig, "SMSRules3", (int)0);
                        //if (iGForceSpeed > 0)
                        //{
                        //    byte[] bGForceSpeed = BitConverter.GetBytes(iGForceSpeed);
                        //    byte[] bConv = new byte[4];
                        //    byte[] bResult = new byte[4];
                        //    bResult[0] = bGForceSpeed[0];  // Copy the time byte

                        //    bConv[0] = bGForceSpeed[1];
                        //    double dGForceSpeed = Convert.ToDouble(BitConverter.ToInt32(bConv, 0));
                        //    dGForceSpeed = dGForceSpeed * 10;
                        //    dGForceSpeed = (dGForceSpeed / 36);
                        //    iGForceSpeed = Convert.ToInt32(Math.Round(dGForceSpeed, 0));
                        //    bGForceSpeed = BitConverter.GetBytes(iGForceSpeed);
                        //    bResult[1] = bGForceSpeed[0];	 // Copy the speed byte

                        //    iGForceSpeed = BitConverter.ToInt32(bResult, 0);
                        //}
                        //cfPacket.iSMSRules3 = iGForceSpeed;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching config Get the 'G-Force by change in speed' settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                else
                {
                    try
                    {
                        cfPacket.iSMSRules3 = 0;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                        return "Exception fetching config Get the 'G-Force by change in speed' settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                    }
                }
                #endregion
                #region Ignition Rules
                try
                {
                    cfPacket.iIgnitionOnRules = VerifyConfigField(drConfig, "IgnitionOnRules", (int)32);		//	Default to send ignition on reports
                    cfPacket.iIgnitionOffRules = VerifyConfigField(drConfig, "IgnitionOffRules", (int)32);	//	Default to send ignition off reports
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Ignition rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Channels
                try
                {
                    cfPacket.Channels = GetChannels(cfPacket.sMDTIPAddress, cfPacket.iMDTPortAddress);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Channels: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Inputs - On rules are stored in the lower 32 bits of a 64-bit long. Off rules in the top 32
                try
                {
                    cfPacket.iInputRules = Convert.ToInt32(VerifyConfigField(drConfig, "InputRules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput1Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input1Rules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput2Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input2Rules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput3Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input3Rules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput4Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input4Rules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput5Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input5Rules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput6Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input6Rules", (Int64)0) & 0xFFFFFFFF);
                    cfPacket.iInput7Rules = Convert.ToInt32(VerifyConfigField(drConfig, "Input7Rules", (Int64)0) & 0xFFFFFFFF);
                    if (drConfig["Input8Rules"] != System.DBNull.Value) cfPacket.iInput8Rules = (ulong)Convert.ToInt64(drConfig["Input8Rules"]);
                    if (drConfig["Input9Rules"] != System.DBNull.Value) cfPacket.iInput9Rules = (ulong)Convert.ToInt64(drConfig["Input9Rules"]);
                    if (drConfig["Input10Rules"] != System.DBNull.Value) cfPacket.iInput10Rules = (ulong)Convert.ToInt64(drConfig["Input10Rules"]);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Input On rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                try
                {
                    cfPacket.iInput1OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input1Rules", (Int64)0) >> 32);
                    cfPacket.iInput2OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input2Rules", (Int64)0) >> 32);
                    cfPacket.iInput3OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input3Rules", (Int64)0) >> 32);
                    cfPacket.iInput4OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input4Rules", (Int64)0) >> 32);
                    // Bytes are now being used by the Card Read Configuration
                    // cfPacket.iInput5OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input5Rules", (Int64)0) >> 32);
                    // cfPacket.iInput6OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input6Rules", (Int64)0) >> 32);
                    // cfPacket.iInput7OffRules = Convert.ToInt32(VerifyConfigField(drConfig, "Input7Rules", (Int64)0) >> 32);
                    cfPacket.CardReaderConfiguration = GetCardReaderConfig(cfPacket.cFleetId, cfPacket.iVehicleId);
                    if (drConfig["Input8Rules"] != System.DBNull.Value) cfPacket.iInput8OffRules = (ulong)Convert.ToInt64(drConfig["Input8Rules"]);
                    if (drConfig["Input9Rules"] != System.DBNull.Value) cfPacket.iInput9OffRules = (ulong)Convert.ToInt64(drConfig["Input9Rules"]);
                    if (drConfig["Input10Rules"] != System.DBNull.Value) cfPacket.iInput10OffRules = (ulong)Convert.ToInt64(drConfig["Input10Rules"]);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Input Off rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Outputs (for MobileApp) or Limits (for TransportApp)
                try
                {
                    cfPacket.iMaxCoolantTempLimit = VerifyConfigField(drConfig, "MaxCoolantTempLimit", (int)0);
                    cfPacket.iMaxOilTempLimit = VerifyConfigField(drConfig, "MaxOilTempLimit", (int)0);
                    cfPacket.iMinOilPressLow = VerifyConfigField(drConfig, "MinOilPressLow", (int)0);
                    cfPacket.iMinOilPressHigh = VerifyConfigField(drConfig, "MinOilPressHigh", (int)0);
                    cfPacket.iMinOilPressLowRPM = VerifyConfigField(drConfig, "MinOilPressLowRPM", (int)0);
                    cfPacket.iMinOilPressHighRPM = VerifyConfigField(drConfig, "MinOilPressHighRPM", (int)0);
                    cfPacket.iMaxRPM = VerifyConfigField(drConfig, "MaxRPM", (int)0);
                    cfPacket.fGForceAccelerationLimit = VerifyConfigField(drConfig, "GForceAccelerationLimit", (Single)0);
                    cfPacket.fGForceBrakingLimit = VerifyConfigField(drConfig, "GForceBrakingLimit", (Single)0);
                    cfPacket.fGForceLRLimit = VerifyConfigField(drConfig, "GForceLRLimit", (Single)0);
                    cfPacket.fGForceAccident = VerifyConfigField(drConfig, "GForceAccident", (Single)0);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Vehicle limit rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Output Data
                try
                {
                    cfPacket.iOutput1Data = VerifyConfigField(drConfig, "Output1Data", (int)0);
                    cfPacket.iOutput2Data = VerifyConfigField(drConfig, "Output2Data", (int)0);
                    cfPacket.iOutput3Data = VerifyConfigField(drConfig, "Output3Data", (int)0);
                    cfPacket.iOutput4Data = VerifyConfigField(drConfig, "Output4Data", (int)0);
                    cfPacket.iOutput5Data = VerifyConfigField(drConfig, "Output5Data", (int)0);
                    cfPacket.iOutput6Data = VerifyConfigField(drConfig, "Output6Data", (int)0);
                    cfPacket.iOutput7Data = VerifyConfigField(drConfig, "Output7Data", (int)0);
                    cfPacket.iOutput8Data = VerifyConfigField(drConfig, "Output8Data", (int)0);
                    if (drConfig.IsNull("Output9Data")) cfPacket.iOutput9Data = 0;
                    else cfPacket.iOutput9Data = Convert.ToInt64(VerifyConfigField(drConfig, "Output9Data", (Int64)0));

                    if (drConfig.IsNull("Output10Data")) cfPacket.iOutput10Data = 0;
                    else cfPacket.iOutput10Data = Convert.ToInt64(VerifyConfigField(drConfig, "Output10Data", (Int64)0)); // Concrete Barrel RPM limit
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Output rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region SMS notification numbers:
                try
                {
                    cfPacket.lSMSNetworkNo = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNetworkNumber", ""));
                    cfPacket.lSMSNotifyNo1 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNotifyNumber1", ""));
                    cfPacket.lSMSNotifyNo2 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNotifyNumber2", ""));
                    cfPacket.lSMSNotifyNo3 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNotifyNumber3", ""));
                    cfPacket.lSMSNotifyNo4 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNotifyNumber4", ""));
                    cfPacket.lSMSNotifyNo5 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNotifyNumber5", ""));
                    cfPacket.lSMSNotifyNo6 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "SMSNotifyNumber6", ""));
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config SMS rules: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region Phone Numbers
                try
                {
                    cfPacket.lPhoneNumber1 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "PhoneNumber", ""));
                    cfPacket.lPhoneNumber2 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "PhoneNumber2", ""));
                    cfPacket.lPhoneNumber3 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "PhoneNumber3", ""));
                    cfPacket.lPhoneNumber4 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "PhoneNumber4", ""));
                    cfPacket.lPhoneNumber5 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "PhoneNumber5", ""));
                    cfPacket.lPhoneNumber6 = ConfigFileGPPacket.StringToBCDLong(VerifyConfigField(drConfig, "PhoneNumber6", ""));
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching config Phopne number settings: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion

                #region ECM Alerts  (ECM Vehicle Speed)
                try
                {
                    int iECMVehicleSpeed = VerifyConfigField(drConfig, "ECMVehicleSpeed", (int)0);
                    if (iECMVehicleSpeed > 0)
                    {
                        cfPacket.AddCustomECMAlert((byte)GeneralGPPacket.GEN_ECM_OVERSPEED, (ushort)GPECMAdvancedItem.SourceTypes.J1939, 84, Convert.ToUInt32(iECMVehicleSpeed * 256));
                        cfPacket.AddCustomECMAlert((byte)GeneralGPPacket.GEN_ECM_OVERSPEED, (ushort)GPECMAdvancedItem.SourceTypes.J1708, 84, Convert.ToUInt32(Convert.ToDouble(iECMVehicleSpeed) / Convert.ToDouble(0.805)));
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
                    return "Exception fetching dynamic ECM alerts: " + cfPacket.cConfigFileNo + "v" + cfPacket.cVersionMajor + "." + cfPacket.cVersionMinor + " for fleet " + cfPacket.cFleetId + ", unit " + cfPacket.iVehicleId + "\nError  " + ex.Message + "\nSource : " + ex.Source;
                }
                #endregion
                #region trailer Track
                cfPacket.TrailerReportFreqPower = VerifyConfigField(drConfig, "TrailerTrackReportFreqPower", (short)60);
                cfPacket.TrailerReportFreqBattery = VerifyConfigField(drConfig, "TrailerTrackReportFreqBattery", (short)120);
                cfPacket.TrailerReportFreqLowBattery = VerifyConfigField(drConfig, "TrailerTrackReportFreqLowBattery", (short)300);
                cfPacket.TrailerReportFreqStationary = VerifyConfigField(drConfig, "TrailerTrackReportFreqStationary", (short)14400);
                cfPacket.TrailerBatteryLow = VerifyConfigField(drConfig, "TrailerTrackBatteryLevelLow", (short)3650);
                cfPacket.TrailerBatteryCritical = VerifyConfigField(drConfig, "TrailerTrackBatteryLevelCritical", (short)3550);
                cfPacket.TrailerBatteryShutdown = VerifyConfigField(drConfig, "TrailerTrackBatteryLevelShutdown", (short)3450);
                #endregion
                #endregion
            }
            catch (System.Exception ex)
            {
                if (cfPacket != null)
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket) - Unit " + Convert.ToString((int)cfPacket.cFleetId) + "/" + Convert.ToString((int)cfPacket.iVehicleId) + ", Config File No " + Convert.ToString((int)cfPacket.cConfigFileNo) + " (" + Convert.ToString((int)cfPacket.cVersionMajor) + "." + Convert.ToString((int)cfPacket.cVersionMinor) + ")", ex);
                else
                    _log.Error(sClassName + "GetConfigFile(ref ConfigFileGPPacket cfPacket)", ex);
            }
            return null;
        }
        /// <summary>
        /// This method will populate the ConfigEcmPacket object with the configuration values for the specified vehicle.
        /// </summary>
        /// <param name="ref ConfigEcmPacket aPacket"></param>
        /// <returns>string</returns>
        public string GetEcmConfigFile(ref ConfigEcmPacket packet)
        {
            try
            {
                DataRow drConfig = GetConfigRowFromConfigNumber((int)packet.cFleetId, (int)packet.ConfigId);
                if (drConfig != null)
                {
                    DataRow drVehicle = GetVehicleRow(packet.cFleetId, packet.iVehicleId);
                    if (drVehicle == null)
                        return "Could not find a configuration or vehicle record for " + Convert.ToString(packet.cFleetId) + "/" + Convert.ToString(packet.iVehicleId);

                    int iConfigID = Convert.ToInt32(drVehicle["ConfigID"]);

                    //check if ecm config defined
                    DataRow[] ecmConfig = mConfigDataSet.Tables["EcmConfig"].Select(string.Format("ConfigID = {0}", iConfigID));
                    if (ecmConfig.Length == 0)
                    {
                        //load default config
                        ecmConfig = mConfigDataSet.Tables["DefaultEcmConfig"].Select();
                    }

                    foreach (DataRow r in ecmConfig)
                    {
                        ConfigEcmPacket.EcmRule rule = new ConfigEcmPacket.EcmRule();
                        rule.SourceType = Convert.ToInt32(r["SourceType"]);
                        rule.SpnCode = Convert.ToInt32(r["SPNCode"]);
                        rule.RuleType = Convert.ToInt32(r["RuleType"]);
                        rule.LowValue = Convert.ToInt32(r["LowValue"]);
                        rule.HighValue = Convert.ToInt32(r["HighValue"]);
                        rule.ReportDuring = Convert.ToInt32(r["ReportDuring"]);
                        rule.NumberOfReportsToBreakOut = Convert.ToInt32(r["NumberOfRecordsToBreakOut"]);
                        rule.RuleId = Convert.ToInt32(r["ID"]);
                        rule.PgnCode = Convert.ToInt32(r["PGNCode"]);
                        rule.TransmissionRate = Convert.ToInt32(r["TransmissionRate"]);
                        rule.StartByte = Convert.ToInt32(r["StartByte"]);
                        rule.StartBit = Convert.ToInt32(r["StartBit"]);
                        rule.LengthBits = Convert.ToInt32(r["LengthBits"]);
                        rule.Multipler = Convert.ToSingle(r["Multiplier"]);
                        rule.Offset = Convert.ToSingle(r["Offset"]);
                        rule.ValueType = 0;
                        rule.WellKnownType = 0;
                        packet.Rules.Add(rule);
                    }

                }
            }
            catch (System.Exception ex)
            {
                if (packet != null)
                    _log.Error(sClassName + "GetEcmConfigFile() - Unit " + Convert.ToString((int)packet.cFleetId) + "/" + Convert.ToString((int)packet.iVehicleId) + ", Config File No " + Convert.ToString((int)packet.ConfigId) + " (" + Convert.ToString((int)packet.ConfigMajor) + "." + Convert.ToString((int)packet.ConfigMinor) + ")", ex);
                else
                    _log.Error(sClassName + "GetEcmConfigFile()", ex);
            }
            return null;
        }

        /// <summary>
        /// This method is used by the GetConfigFile to populate a default value when none is supplied in the dataset.
        /// </summary>
        /// <param name="DataRow row"></param>
        /// <param name="string fieldName"></param>
        /// <param name="string defaultValue"></param>
        /// <returns>string</returns>
        private string VerifyConfigField(DataRow row, string fieldName, string defaultValue)
        {
            if (row.IsNull(fieldName))
            {
                _log.Info(string.Format("VerifyConfigField : ERROR : Field '{0}' is NULL : Using Default Value '{1}'", fieldName, defaultValue));
                return defaultValue;
            }
            else
                return Convert.ToString(row[fieldName]);
        }
        /// <summary>
        /// This method is used by the GetConfigFile to populate a default value when none is supplied in the dataset.
        /// </summary>
        /// <param name="DataRow row"></param>
        /// <param name="string fieldName"></param>
        /// <param name="double defaultValue"></param>
        /// <returns>double</returns>
        private double VerifyConfigField(DataRow row, string fieldName, double defaultValue)
        {
            if (row.IsNull(fieldName))
            {
                _log.Info(string.Format("VerifyConfigField : ERROR : Field '{0}' is NULL : Using Default Value '{1}'", fieldName, defaultValue));
                return defaultValue;
            }
            else
                return Convert.ToDouble(row[fieldName]);
        }
        /// <summary>
        /// This method is used by the GetConfigFile to populate a default value when none is supplied in the dataset.
        /// </summary>
        /// <param name="DataRow row"></param>
        /// <param name="string fieldName"></param>
        /// <param name="Single defaultValue"></param>
        /// <returns>Single</returns>
        private Single VerifyConfigField(DataRow row, string fieldName, Single defaultValue)
        {
            if (row.IsNull(fieldName))
            {
                _log.Info(string.Format("VerifyConfigField : ERROR : Field '{0}' is NULL : Using Default Value '{1}'", fieldName, defaultValue));
                return defaultValue;
            }
            else
                return Convert.ToSingle(row[fieldName]);
        }
        /// <summary>
        /// This method is used by the GetConfigFile to populate a default value when none is supplied in the dataset.
        /// </summary>
        /// <param name="DataRow row"></param>
        /// <param name="string fieldName"></param>
        /// <param name="int defaultValue"></param>
        /// <returns>int</returns>
        private int VerifyConfigField(DataRow row, string fieldName, int defaultValue)
        {
            if (row.IsNull(fieldName))
            {
                _log.Info(string.Format("VerifyConfigField : ERROR : Field '{0}' is NULL : Using Default Value '{1}'", fieldName, defaultValue));
                return defaultValue;
            }
            else
                return Convert.ToInt32(row[fieldName]);
        }
        /// <summary>
        /// This method is used by the GetConfigFile to populate a default value when none is supplied in the dataset.
        /// </summary>
        /// <param name="DataRow row"></param>
        /// <param name="string fieldName"></param>
        /// <param name="short defaultValue"></param>
        /// <returns>short</returns>
        private short VerifyConfigField(DataRow row, string fieldName, short defaultValue)
        {
            if (row.IsNull(fieldName))
            {
                _log.Info(string.Format("VerifyConfigField : ERROR : Field '{0}' is NULL : Using Default Value '{1}'", fieldName, defaultValue));
                return defaultValue;
            }
            else
                return Convert.ToInt16(row[fieldName]);
        }
        /// <summary>
        /// This method is used by the GetConfigFile to populate a default value when none is supplied in the dataset.
        /// </summary>
        /// <param name="DataRow row"></param>
        /// <param name="string fieldName"></param>
        /// <param name="Int64 defaultValue"></param>
        /// <returns>Int64</returns>
        private Int64 VerifyConfigField(DataRow row, string fieldName, Int64 defaultValue)
        {
            if (row.IsNull(fieldName))
            {
                _log.Info(string.Format("VerifyConfigField : ERROR : Field '{0}' is NULL : Using Default Value '{1}'", fieldName, defaultValue));
                return defaultValue;
            }
            else
                return Convert.ToInt64(row[fieldName]);
        }
        /// <summary>
        /// This method is used to return a two byte field from a signed int value
        /// </summary>
        /// <param name="int iValue"></param>
        /// <returns>byte[]</returns>
        private byte[] Get2BytesFromInt(int iValue)
        {
            byte[] bRet = new byte[2];
            try
            {
                byte[] bConvert = BitConverter.GetBytes(iValue);
                bRet[0] = bConvert[0];
                bRet[1] = bConvert[1];
                return bRet;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Get2BytesFromInt(int iValue)", ex);
                return bRet;
            }
        }
        /// <summary>
        /// This method is used to return a byte field from a double value
        /// </summary>
        /// <param name="double iValue"></param>
        /// <returns>byte</returns>
        private byte GetByteFromFloat(double iValue)
        {
            int iConvert = Convert.ToInt32(Math.Round(iValue, 0));
            return GetByteFromInt(iConvert);
        }
        /// <summary>
        /// This method is used to return a byte field from a double value
        /// </summary>
        /// <param name="double iValue"></param>
        /// <returns>byte</returns>
        private byte GetFloorByteFromFloat(double iValue)
        {
            int iConvert = Convert.ToInt32(Math.Floor(iValue));
            return GetByteFromInt(iConvert);
        }
        /// <summary>
        /// This method is used to the first 2 DPs of a float as a byte.
        /// </summary>
        /// <param name="double iValue"></param>
        /// <returns>byte</returns>
        private byte GetByteFromFloatDecimalValue(double iValue)
        {
            int iConvert = Convert.ToInt32(Math.Floor(iValue));
            iValue = iValue - (double)iConvert;
            iValue = iValue * 100;
            iConvert = Convert.ToInt32(Math.Round(iValue, 0));
            return GetByteFromInt(iConvert);
        }
        
        /// This method is used to return a byte field from a signed int value
        /// </summary>
        /// <param name="int iValue"></param>
        /// <returns>byte</returns>
        private byte GetByteFromInt(int iValue)
        {
            try
            {
                byte[] bConvert = BitConverter.GetBytes(iValue);
                return bConvert[0];
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetByteFromInt(int iValue)", ex);
                return (byte)0x00;
            }
        }
        #endregion

        #region stationary alerts and Out of hours usage
        public DataRow[] GetScheduledStationaryAlerts(DateTime time) 
        {
            lock (oConfigDataSetSync)
            {
                return mConfigDataSet.Tables["ActiveStationayPeriods"].AsEnumerable()
                .Where(r => r.Field<bool>("Active") == false && r.Field<DateTime>("ScheduledStartTime") < time)
                .ToArray();
            }
        }
        public DataRow[] GetScheduledOutOfHoursFinished(DateTime time)
        {
            lock (oConfigDataSetSync)
            {
                return mConfigDataSet.Tables["ActiveOutOfHoursUsage"].AsEnumerable()
                .Where(r => r.Field<DateTime>("ScheduledEndTime") < time)
                .ToArray();
            }
        }
        public TemporalObject GetStationaryTimePeriods(int configId)
        {
            lock (oConfigDataSetSync)
            {
                if (_configStationaryPeriods.ContainsKey(configId))
                {
                    return _configStationaryPeriods[configId];
                }
            }
            return null;
        }
        public TemporalObject GetOutOfHoursTimePeriods(int configId)
        {
            lock (oConfigDataSetSync)
            {
                if (_configOutOfHoursPeriods.ContainsKey(configId))
                {
                    return _configOutOfHoursPeriods[configId];
                }
            }
            return null;
        }
        
        public void StartActiveOutOfHoursUsage(int fleetId, int vehicleId, DateTime endTime)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleOutOfHoursUsageInsert {0}, {1}, '{2}'",
                        fleetId, vehicleId, endTime.ToString("MM/dd/yyyy HH:mm:ss"));
                    cmd.Connection = oConn;
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(data.Tables[0].Rows[0]["ID"]);
                        lock (oConfigDataSetSync)
                        {
                            if (mConfigDataSet != null)
                            {
                                DataRow r = mConfigDataSet.Tables["ActiveOutOfHoursUsage"].NewRow();
                                r["ID"] = id;
                                r["FleetId"] = fleetId;
                                r["VehicleId"] = vehicleId;
                                r["ScheduledEndTime"] = endTime;
                                mConfigDataSet.Tables["ActiveOutOfHoursUsage"].Rows.Add(r);
                            }
                        }

                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "SaveStartOutOfUsage()", ex);
            }
        }
        public int FindActiveOutOfHoursUsage(int fleetId, int vehicleId)
        {
            lock (oConfigDataSetSync)
            {
                if (mConfigDataSet != null)
                {
                    DataRow[] rows = mConfigDataSet.Tables["ActiveOutOfHoursUsage"].Select(string.Format("FleetId = {0} and VehicleId = {1}", fleetId, vehicleId));
                    if (rows.Length > 0)
                    {
                        return Convert.ToInt32(rows[0]["Id"]);
                    }
                }
            }
            return -1;
        }
        public void EndActiveOutOfHoursUsage(int fleetId, int vehicleId)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleOutOfHoursUsageDelete {0}, {1}", fleetId, vehicleId);
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    lock (oConfigDataSetSync)
                    {
                        if (mConfigDataSet != null)
                        {
                            DataRow[] rows = mConfigDataSet.Tables["ActiveOutOfHoursUsage"].Select(string.Format("FleetId = {0} and VehicleId = {1}", fleetId, vehicleId));
                            foreach (DataRow r in rows)
                            {
                                mConfigDataSet.Tables["ActiveOutOfHoursUsage"].Rows.Remove(r);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "EndActiveOutOfHoursUsage()", ex);
            }
        }

        public int GetStationaryAlertPeriod(int configId)
        {
            try
            {
                DataRow[] drConfigs = null;
                lock (oConfigDataSetSync)
                {
                    if (mConfigDataSet != null && mConfigDataSet.Tables.Contains("Config"))
                    {
                        drConfigs = mConfigDataSet.Tables["Config"].Select(string.Format("ID = {0}", configId));
                    }
                }
                if (drConfigs.Length > 0)
                {
                    return Convert.ToInt32(drConfigs[0]["StationaryAlertPeriod"]);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetStationaryAlertPeriod(int configID = " + Convert.ToString(configId) + ")", ex);
            }
            return -1;
        }
        public void StartStationaryAlert(int fleetId, int vehicleId, DateTime startTime)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                using (SqlCommand cmd = new SqlCommand())
                {
                    _log.DebugFormat("Stationary Alert - Initialising alert for {0}/{1} to start at {2} UTC", fleetId, vehicleId, startTime.ToString("HH:mm:ss dd/MM/yyyy"));
                    cmd.CommandText = string.Format("exec usp_VehicleStationaryAlertInsert {0}, {1}, '{2}'",
                        fleetId, vehicleId, startTime.ToString("MM/dd/yyyy HH:mm:ss"));
                    cmd.Connection = oConn;
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(data.Tables[0].Rows[0]["ID"]);
                        lock (oConfigDataSetSync)
                        {
                            if (mConfigDataSet != null)
                            {
                                DataRow r = mConfigDataSet.Tables["ActiveStationayPeriods"].NewRow();
                                r["ID"] = id;
                                r["FleetId"] = fleetId;
                                r["VehicleId"] = vehicleId;
                                r["ScheduledStartTime"] = startTime;
                                r["Active"] = 0;
                                mConfigDataSet.Tables["ActiveStationayPeriods"].Rows.Add(r);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "StartStationaryAlert()", ex);
            }
        }
        public DataRow FindStationaryAlert(int fleetId, int vehicleId)
        {
            lock (oConfigDataSetSync)
            {
                if (mConfigDataSet != null)
                {
                    DataRow[] rows = mConfigDataSet.Tables["ActiveStationayPeriods"].Select(string.Format("FleetId = {0} and VehicleId = {1}", fleetId, vehicleId));
                    if (rows.Length > 0)
                    {
                        return rows[0];
                    }
                }
            }
            return null;
        }
        public void StationaryAlertActive(int id)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    _log.DebugFormat("Stationary Alert - Setting alert {0} to active", id);
                    cmd.CommandText = string.Format("exec usp_VehicleStationaryAlertActive {0}", id);
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    lock (oConfigDataSetSync)
                    {
                        if (mConfigDataSet != null)
                        {
                            DataRow[] rows = mConfigDataSet.Tables["ActiveStationayPeriods"].Select(string.Format("ID = {0}", id));
                            if (rows.Length > 0)
                            {
                                rows[0]["Active"] = 1;
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateTemporalWaypointState()", ex);
            }
        }
        public void EndStationaryAlert(int fleetId, int vehicleId)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    _log.DebugFormat("Stationary Alert - Ending alert for {0}/{1}", fleetId, vehicleId);
                    cmd.CommandText = string.Format("exec usp_VehicleStationaryAlertDelete {0}, {1}", fleetId, vehicleId);
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    lock (oConfigDataSetSync)
                    {
                        if (mConfigDataSet != null)
                        {
                            DataRow[] rows = mConfigDataSet.Tables["ActiveStationayPeriods"].Select(string.Format("FleetId = {0} and VehicleId = {1}", fleetId, vehicleId));
                            foreach (DataRow r in rows)
                            {
                                mConfigDataSet.Tables["ActiveStationayPeriods"].Rows.Remove(r);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "EndStationaryAlert()", ex);
            }
        }
        #endregion
        #endregion
        #region T_ReasonCodeOverideGroup table functions
        /// <summary>
        /// This method will be used to return the results of usp_GetReasonCodeOverideByGroupIDandVer [FleetID], [ListID],[Version]
        /// </summary>
        /// <param name="int iFleetID"></param>
        /// <param name="int iListID"></param>
        /// <param name="int iVersion"></param>
        /// <returns>DataSet</returns>
        public DataSet GetReasonCodeListChange(int iFleetID, int iListID, int iVersion)
        {
            DataSet oDS = null;
            SqlConnection oConn = null;
            SqlCommand oCmd = null;
            SqlDataAdapter oDA = null;
            string sSQLCmd = "";

            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
                sSQLCmd = "EXEC usp_GetReasonCodeOverideByGroupIDandVer [UserID], [GroupID], [FleetID], [Version]";
                sSQLCmd = sSQLCmd.Replace("[UserID]", "1");
                sSQLCmd = sSQLCmd.Replace("[GroupID]", Convert.ToString(iListID));
                sSQLCmd = sSQLCmd.Replace("[FleetID]", Convert.ToString(iFleetID));
                sSQLCmd = sSQLCmd.Replace("[Version]", Convert.ToString(iVersion));
                oCmd = oConn.CreateCommand();
                oCmd.CommandText = sSQLCmd;
                oDS = new DataSet();
                oDA = new SqlDataAdapter(oCmd);
                oDA.Fill(oDS);
                oDA.Dispose();
                oCmd.Dispose();
                oConn.Close();
                oConn.Dispose();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetReasonCodeListChange(int iFleetID = " + Convert.ToString(iFleetID) + ", int iListID = " + Convert.ToString(iListID) + ", int iVersion = " + Convert.ToString(iVersion) + ")", ex);
            }
            return oDS;
        }
        #endregion
        #region T_DriverPoints
        public void RefreshDriverPointsDataSet(SqlConnection connection)
        {
            try
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "EXEC usp_DriverPointsGetRuleGroups";
                command.CommandTimeout = 0;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataSet data = new DataSet();
                adapter.Fill(data);
                adapter.Dispose();
                adapter = null;

                command.Dispose();
                command = null;

                data.Tables[0].TableName = "T_DriverPointsRuleGroups";
                data.Tables[1].TableName = "T_DriverPointsRules";
                data.Tables[2].TableName = "T_DriverPointsRulesTrackingEvent";
                data.Tables[3].TableName = "T_DriverPointsRulesECM";
                data.Tables[4].TableName = "T_DriverPointsRulesIO";
                data.Tables[5].TableName = "T_DriverPointsRulesMdt";
                data.Tables[6].TableName = "T_DriverPointsTrackingRules";
                lock (oDriverPointsDataSetSync)
                {
                    mDriverPointsDataSet = data.Copy();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshDriverPointsDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        /// <summary>
        /// This method will populate the ConfigFileGPPacket object with the configuration values for the specified vehicle.
        /// </summary>
        /// <param name="ref ConfigTotalGPPacket aPacket"></param>
        /// <returns>string error</returns>
        public string GetDriverPointsConfig(ref ConfigDriverPointsGPPacket packet, int fleetId, uint vehicleId)
        {
            string error = null;
            try
            {
                DataRow vehicleRow = GetVehicleRow((byte)fleetId, vehicleId);
                int ruleGroupId = 0;
                if (vehicleRow != null && vehicleRow.Table.Columns.Contains("DriverPointsRuleGroupID")
                    && vehicleRow["DriverPointsRuleGroupID"] != DBNull.Value)
                {
                    ruleGroupId = Convert.ToInt32(vehicleRow["DriverPointsRuleGroupID"]);
                }
                if (ruleGroupId == 0)
                {
                    packet.GroupID = 0;
                    packet.GroupVerMajor = 0;
                    packet.GroupVerMinor = 0;
                }
                else
                {
                    DataRow ruleGroupRow = GetRuleGroupRow(ruleGroupId);
                    if (ruleGroupRow == null)
                    {
                        packet.GroupID = 0;
                        packet.GroupVerMajor = 0;
                        packet.GroupVerMinor = 0;
                    }
                    else
                    {
                        packet.GroupID = ruleGroupId;
                        packet.GroupVerMajor = (byte)Convert.ToInt32(ruleGroupRow["VersionMajor"]);
                        packet.GroupVerMinor = (byte)Convert.ToInt32(ruleGroupRow["VersionMinor"]);

                        DataRow[] rules = GetRulesForGroup(ruleGroupId);
                        if (rules != null && rules.Length > 0)
                        {
                            foreach (DataRow rule in rules)
                            {
                                int ruleType = Convert.ToInt32(rule["Type"]);
                                int ruleId = Convert.ToInt32(rule["Id"]);
                                int points = Convert.ToInt32(rule["Points"]);
                                int output = Convert.ToInt32(rule["Output1"]);
                                if (output > 0)
                                {
                                    output = output / 100; //stored in milli secs in databse, sent as deci secs
                                }
                                short output1 = (short)output;
                                output = Convert.ToInt32(rule["Output2"]);
                                if (output > 0)
                                {
                                    output = output / 100; //stored in milli secs in databse, sent as deci secs
                                }
                                short output2 = (short)output;
                                DataRow ruleRow;
                                switch (ruleType)
                                {
                                    case 0:
                                        ruleRow = GetTrackingRule(ruleId);
                                        if (ruleRow != null)
                                        {
                                            ushort reasonCode = (ushort)Convert.ToInt32(ruleRow["ReasonCode"]);
                                            if (IsUnitTrackingRule(reasonCode))
                                            {
                                                ConfigDriverPointsTrkRules t = new ConfigDriverPointsTrkRules();
                                                t.PacketVer = packet.PacketVer;
                                                t.RuleID = ruleId;
                                                t.Points = (byte)points;
                                                t.Output1 = output1;
                                                t.Output2 = output2;
                                                t.ReasonCode = reasonCode;
                                                packet.TrackingRules.Add(t);
                                            }
                                        }
                                        break;
                                    case 1:
                                        ruleRow = GetEcmRule(ruleId);
                                        if (ruleRow != null)
                                        {
                                            ConfigDriverPointsECMRules e = new ConfigDriverPointsECMRules();
                                            e.PacketVer = packet.PacketVer;
                                            e.RuleID = ruleId;
                                            e.Points = (byte)points;
                                            e.Output1 = output1;
                                            e.Output2 = output2;
                                            e.SourceType = (ushort)Convert.ToInt32(ruleRow["SourceType"]);
                                            int dataLength = Convert.ToInt16(ruleRow["DataLength"]);
                                            switch (dataLength)
                                            {
                                                case 0:
                                                    e.EcmValyeType = GPECMAdvanced.ECMValueType.BinaryValue & GPECMAdvanced.ECMValueType.DataLength1;
                                                    break;
                                                case 1:
                                                    e.EcmValyeType = GPECMAdvanced.ECMValueType.DataLength1;
                                                    break;
                                                case 2:
                                                    e.EcmValyeType = GPECMAdvanced.ECMValueType.DataLength2;
                                                    break;
                                                case 4:
                                                    e.EcmValyeType = GPECMAdvanced.ECMValueType.DataLength4;
                                                    break;
                                            }
                                            e.PGN = (ushort)Convert.ToInt32(ruleRow["PGN"]);
                                            e.SPN = (ushort)Convert.ToInt32(ruleRow["SPN"]);
                                            e.ByteLocation = (byte)Convert.ToInt16(ruleRow["StartBytePos"]);
                                            e.BitLocation = (byte)Convert.ToInt16(ruleRow["StartBitPos"]);
                                            e.EcmValue = (uint)Convert.ToInt32(ruleRow["Value"]);
                                            e.Condition = (byte)Convert.ToInt32(ruleRow["Condition"]);
                                            e.Duration = (uint)Convert.ToInt32(ruleRow["Duration"]);
                                            e.ResetDuration = (uint)Convert.ToInt32(ruleRow["Reset"]);
                                            packet.ECMRules.Add(e);
                                        }
                                        break;
                                    case 2:
                                        ruleRow = GetIoRule(ruleId);
                                        if (ruleRow != null)
                                        {
                                            ConfigDriverPointsIORules i = new ConfigDriverPointsIORules();
                                            i.PacketVer = packet.PacketVer;
                                            i.RuleID = ruleId;
                                            i.Points = (byte)points;
                                            i.Output1 = output1;
                                            i.Output2 = output2;

                                            i.Input = (byte)Convert.ToInt32(ruleRow["InputNumber"]);
                                            i.State = (byte)Convert.ToByte(ruleRow["State"]);
                                            i.VehicleSpeed = (byte)Convert.ToInt32(ruleRow["VehicleSpeed"]);
                                            i.Duration = (uint)Convert.ToInt32(ruleRow["Duration"]);
                                            i.ResetDuration = (uint)Convert.ToInt32(ruleRow["Reset"]);
                                            packet.IORules.Add(i);
                                        }
                                        break;
                                    case 3:
                                        ruleRow = GetMdtRule(ruleId);
                                        if (ruleRow != null)
                                        {
                                            ConfigDriverPointsMDTRules m = new ConfigDriverPointsMDTRules();
                                            m.PacketVer = packet.PacketVer;
                                            m.RuleID = ruleId;
                                            m.Points = (byte)points;
                                            m.Output1 = output1;
                                            m.Output2 = output2;
                                            m.MdtType = (byte)Convert.ToInt32(ruleRow["Type"]);
                                            m.VehicleSpeed = (byte)Convert.ToInt32(ruleRow["VehicleSpeed"]);
                                            m.Duration = (uint)Convert.ToInt32(ruleRow["Duration"]);
                                            m.ResetDuration = (uint)Convert.ToInt32(ruleRow["Reset"]);
                                            packet.MDTRules.Add(m);
                                        }
                                        break;
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetDriverPointsConfig(ref ConfigDriverPointsGPPacket packet, int fleetId, uint vehicleId)", ex);
                error = ex.Message;
            }
            return error;
        }

        public string UpdateDriverPointsCurrentlyRunning(DriverPointsRuleRequest configPacket, PacketDatabaseInterface.DriverPointsCurrentRuleGroupChangedEvent ruleGroupChanged, ref bool updated)
        {
            string error = null;
            try
            {
                //get the stored values of what is currently running
                DataRow vehicleRow = GetVehicleRow(configPacket.cFleetId, configPacket.iVehicleId);
                if (vehicleRow == null)
                {
                    _log.Info(string.Format("Update DriverPoints Config currently Running, fleet {0} vehicle {1} not found", configPacket.cFleetId, configPacket.iVehicleId));
                    return error;
                }
                int ruleGroupId = 0;
                int major = 0;
                int minor = 0;
                if (vehicleRow["DriverPointsRuleCurrentID"] != DBNull.Value)
                {
                    ruleGroupId = Convert.ToInt32(vehicleRow["DriverPointsRuleCurrentID"]);
                }
                if (vehicleRow["DriverPointsRuleCurrentVersionMajor"] != DBNull.Value)
                {
                    major = Convert.ToInt32(vehicleRow["DriverPointsRuleCurrentVersionMajor"]);
                }
                if (vehicleRow["DriverPointsRuleCurrentVersionMinor"] != DBNull.Value)
                {
                    minor = Convert.ToInt32(vehicleRow["DriverPointsRuleCurrentVersionMinor"]);
                }

                //check them against what the vehicle is currently running
                if (configPacket.RuleGroupId != ruleGroupId ||
                    configPacket.VersionMajor != major ||
                    configPacket.VersionMinor != minor)
                {
                    //save them in the data set
                    lock (oVehicleDataSetSync)
                    {
                        vehicleRow["DriverPointsRuleCurrentID"] = configPacket.RuleGroupId;
                        vehicleRow["DriverPointsRuleCurrentVersionMajor"] = configPacket.VersionMajor;
                        vehicleRow["DriverPointsRuleCurrentVersionMinor"] = configPacket.VersionMinor;
                    }

                    //save in the database
                    #region save in database
                    #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
                    SqlConnection oConn = null;
                    bool bConnectionError = false;
                    try
                    {
                        oConn = new SqlConnection(sDSN);
                        oConn.Open();
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "UpdateDriverPointsCurrentlyRunning(DriverPointsRuleRequest configPacket, PacketDatabaseInterface.DriverPointsCurrentRuleGroupChangedEvent ruleGroupChanged, ref bool updated) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                        bConnectionError = true;
                    }
                    while (bConnectionError)
                    {
                        try
                        {
                            Thread.Sleep(5000);
                            oConn = new SqlConnection(sDSN);
                            oConn.Open();
                            bConnectionError = false;
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "UpdateDriverPointsCurrentlyRunning(DriverPointsRuleRequest configPacket, PacketDatabaseInterface.DriverPointsCurrentRuleGroupChangedEvent ruleGroupChanged, ref bool updated) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                            bConnectionError = true;
                        }
                    }
                    #endregion

                    try
                    {
                        string sql = "EXEC usp_DriverPointsUpdateRuleGroupCurrent [VehicleId], [FleetId], [GroupId], [Major], [Minor]";
                        sql = sql.Replace("[VehicleId]", vehicleRow["Id"].ToString());
                        sql = sql.Replace("[FleetId]", vehicleRow["FleetID"].ToString());
                        sql = sql.Replace("[GroupId]", vehicleRow["DriverPointsRuleCurrentID"].ToString());
                        sql = sql.Replace("[Major]", vehicleRow["DriverPointsRuleCurrentVersionMajor"].ToString());
                        sql = sql.Replace("[Minor]", vehicleRow["DriverPointsRuleCurrentVersionMinor"].ToString());
                        SqlCommand oCmd = oConn.CreateCommand();
                        oCmd.CommandText = sql;
                        oCmd.ExecuteNonQuery();
                        oCmd.Dispose();
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "UpdateDriverPointsCurrentlyRunning", ex);
                        error = "Error Updating Driver Points Currently Running - " + ex.Message;
                    }
                    try
                    {
                        if (oConn != null)
                        {
                            oConn.Close();
                            oConn.Dispose();
                            oConn = null;
                        }
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "UpdateDriverPointsCurrentlyRunning", ex);
                        error = "Error Updating Driver Points Currently Running - " + ex.Message;
                    }
                    #endregion
                    updated = true;

                    //send update message to the client
                    XMLUpdate xmlUpdate = new XMLUpdate();
                    xmlUpdate.cSubCmd = 'D';
                    xmlUpdate.iItemID = 0;
                    xmlUpdate.sXMLData = string.Format("Rule,{0},{1},{2},{3},{4}", vehicleRow["FleetID"], vehicleRow["Id"],
                        vehicleRow["DriverPointsRuleCurrentID"], vehicleRow["DriverPointsRuleCurrentVersionMajor"],
                        vehicleRow["DriverPointsRuleCurrentVersionMinor"]);
                    xmlUpdate.iPacketVersion = 1;
                    byte[] data = xmlUpdate.Encode(ref error);
                    if (error == null || error.Equals(string.Empty))
                    {
                        if (ruleGroupChanged != null)
                        {
                            ruleGroupChanged((int)configPacket.cFleetId, data);
                        }
                    }
                    else
                    {
                        _log.Info("Failed to encode DriverPoints current running rule update" + error);
                    }
                }
            }
            catch (Exception exp)
            {
                _log.Error(sClassName + "UpdateDriverPointsCurrentlyRunning", exp);
            }
            return error;
        }

        private DataRow GetRuleGroupRow(int ruleGroupId)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsRuleGroups") && mDriverPointsDataSet.Tables["T_DriverPointsRuleGroups"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsRuleGroups"].Select(string.Format("Id = {0}", ruleGroupId));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return rows[0];
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetRuleGroupRow", ex);
            }
            return null;
        }
        private DataRow[] GetRulesForGroup(int ruleGroupId)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsRules") && mDriverPointsDataSet.Tables["T_DriverPointsRules"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsRules"].Select(string.Format("GroupID = {0}", ruleGroupId));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return rows;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetRulesForGroup", ex);
            }
            return null;
        }
        private DataRow GetTrackingRule(int ruleId)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsRulesTrackingEvent") && mDriverPointsDataSet.Tables["T_DriverPointsRulesTrackingEvent"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsRulesTrackingEvent"].Select(string.Format("Id = {0}", ruleId));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return rows[0];
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetTrackingRule", ex);
            }
            return null;
        }
        private bool IsUnitTrackingRule(ushort reasonCode)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsTrackingRules") && mDriverPointsDataSet.Tables["T_DriverPointsTrackingRules"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsTrackingRules"].Select(string.Format("Id = {0}", reasonCode));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return Convert.ToBoolean(rows[0]["Unit"]);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "IsUnitTrackingRule", ex);
            }
            return false;
        }
        private DataRow GetEcmRule(int ruleId)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsRulesECM") && mDriverPointsDataSet.Tables["T_DriverPointsRulesECM"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsRulesECM"].Select(string.Format("Id = {0}", ruleId));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return rows[0];
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetEcmRule", ex);
            }
            return null;
        }
        private DataRow GetIoRule(int ruleId)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsRulesIO") && mDriverPointsDataSet.Tables["T_DriverPointsRulesIO"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsRulesIO"].Select(string.Format("Id = {0}", ruleId));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return rows[0];
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetIoRule", ex);
            }
            return null;
        }
        private DataRow GetMdtRule(int ruleId)
        {
            try
            {
                DataRow[] rows = null;
                lock (oDriverPointsDataSetSync)
                {
                    if (mDriverPointsDataSet != null && mDriverPointsDataSet.Tables.Contains("T_DriverPointsRulesMdt") && mDriverPointsDataSet.Tables["T_DriverPointsRulesMdt"].Rows.Count > 0)
                    {
                        rows = mDriverPointsDataSet.Tables["T_DriverPointsRulesMdt"].Select(string.Format("Id = {0}", ruleId));
                    }
                }
                if (rows != null && rows.Length > 0)
                {
                    return rows[0];
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetMdtRule", ex);
            }
            return null;
        }

        private void RefreshDriverPointsServerTrackingRules()
        {
            try
            {
                SqlConnection connection = new SqlConnection(sDSN);
                connection.Open();

                SqlCommand command = connection.CreateCommand();
                command.CommandText = "EXEC usp_DriverPointsRunningServerTrackingRules";
                command.CommandTimeout = 0;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataSet data = new DataSet();
                adapter.Fill(data);
                adapter.Dispose();
                adapter = null;

                command.Dispose();
                command = null;

                data.Tables[0].TableName = "T_DriverPointsRuleGroups";
                data.Tables[1].TableName = "T_DriverPointsServerTrackingRules";
                lock (oDriverPointsServerTrackingDataSetSync)
                {
                    mDriverPointsServerTrackingDataSet = data.Copy();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshDriverPointsServerTrackingRules(SqlConnection oConn)", ex);
            }
        }
        public List<ServerTrackingRule> GetServerRuleBreaksForVehicle(int fleetId, uint vehicleId)
        {
            List<ServerTrackingRule> rules = new List<ServerTrackingRule>();
            try
            {
                //get vehicle row for this vehicle
                DataRow vehicleRow = GetVehicleRow((byte)fleetId, vehicleId);

                //check if it is running a rule group
                int ruleGroupId = 0;
                if (vehicleRow != null && vehicleRow.Table.Columns.Contains("DriverPointsRuleCurrentID")
                    && vehicleRow["DriverPointsRuleCurrentID"] != DBNull.Value)
                {
                    ruleGroupId = Convert.ToInt32(vehicleRow["DriverPointsRuleCurrentID"]);
                }

                if (ruleGroupId > 0)
                {
                    //it is runnign a rule group, get the version
                    int major = 0;
                    int minor = 0;
                    if (vehicleRow.Table.Columns.Contains("DriverPointsRuleCurrentVersionMajor")
                        && vehicleRow["DriverPointsRuleCurrentVersionMajor"] != DBNull.Value)
                    {
                        major = Convert.ToInt32(vehicleRow["DriverPointsRuleCurrentVersionMajor"]);
                    }
                    if (vehicleRow.Table.Columns.Contains("DriverPointsRuleCurrentVersionMinor")
                        && vehicleRow["DriverPointsRuleCurrentVersionMinor"] != DBNull.Value)
                    {
                        minor = Convert.ToInt32(vehicleRow["DriverPointsRuleCurrentVersionMinor"]);
                    }

                    //check that this group is in the dataset
                    DataRow[] rows = null;
                    DataSet dsCopy = null;
                    lock (oDriverPointsServerTrackingDataSetSync)
                    {
                        if (mDriverPointsServerTrackingDataSet != null)
                        {
                            dsCopy = mDriverPointsServerTrackingDataSet.Copy();
                        }
                    }
                    if (dsCopy != null)
                    {
                        if (dsCopy.Tables.Contains("T_DriverPointsRuleGroups") &&
                            dsCopy.Tables["T_DriverPointsRuleGroups"].Rows.Count > 0)
                        {
                            rows = dsCopy.Tables["T_DriverPointsRuleGroups"].Select(
                                string.Format("Id = {0} and VersionMajor = {1} and VersionMinor = {2}", ruleGroupId, major, minor));
                        }
                    }
                    if (rows == null || rows.Length == 0)
                    {
                        //group not in dataset, refresh
                        RefreshDriverPointsServerTrackingRules();
                    }

                    //get the server tracking rules for this group
                    rows = null;
                    if (dsCopy != null)
                    {
                        dsCopy.Dispose();
                        dsCopy = null;
                    }
                    lock (oDriverPointsServerTrackingDataSetSync)
                    {
                        if (mDriverPointsServerTrackingDataSet != null)
                        {
                            dsCopy = mDriverPointsServerTrackingDataSet.Copy();
                        }
                    }
                    if (dsCopy != null)
                    {
                        if (dsCopy.Tables.Contains("T_DriverPointsServerTrackingRules") &&
                            dsCopy.Tables["T_DriverPointsServerTrackingRules"].Rows.Count > 0)
                        {
                            rows = dsCopy.Tables["T_DriverPointsServerTrackingRules"].Select(
                                string.Format("GroupId = {0} and VersionMajor = {1} and VersionMinor = {2}", ruleGroupId, major, minor));
                        }
                    }
                    if (dsCopy != null)
                    {
                        dsCopy.Dispose();
                        dsCopy = null;
                    }
                    if (rows != null && rows.Length > 0)
                    {
                        foreach (DataRow row in rows)
                        {
                            rules.Add(new ServerTrackingRule(row));
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetServerRuleBreaksForVehicle(int fleetId, uint vehicleId)GetServerRuleBreaksForVehicle(int fleetId, uint vehicleId)", ex);
            }
            return rules;
        }
        #endregion
        #region T_InterfaceChannels
        public void RefreshInterfaceChannelsDataSet(SqlConnection connection)
        {
            try
            {
                SqlCommand command = connection.CreateCommand();
                command.CommandText = "EXEC usp_InterfaceChannels";
                command.CommandTimeout = 0;

                SqlDataAdapter adapter = new SqlDataAdapter(command);
                DataSet data = new DataSet();
                adapter.Fill(data);
                adapter.Dispose();
                adapter = null;

                command.Dispose();
                command = null;

                data.Tables[0].TableName = "T_InterfaceServers";
                data.Tables[1].TableName = "T_InterfaceServerChannels";
                lock (oInterfaceChannelsDataSetSync)
                {
                    mInterfaceChannelsDataSet = data.Copy();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshInterfaceChannelsDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        private List<ConfigFileGPPacket.Channel> GetChannels(string ipAddress, int port)
        {
            List<ConfigFileGPPacket.Channel> channels = new List<ConfigFileGPPacket.Channel>();
            lock (oInterfaceChannelsDataSetSync)
            {
                if (mInterfaceChannelsDataSet != null && mInterfaceChannelsDataSet.Tables.Contains("T_InterfaceServers"))
                {
                    DataRow[] rows = mInterfaceChannelsDataSet.Tables["T_InterfaceServers"].Select(string.Format("IPAddress = '{0}' and Port = {1}", ipAddress, port));
                    if (rows.Length > 0 && mInterfaceChannelsDataSet.Tables.Contains("T_InterfaceServerChannels"))
                    {
                        DataRow[] channelRows = mInterfaceChannelsDataSet.Tables["T_InterfaceServerChannels"].Select(string.Format("InterfaceServerID = {0} ", rows[0]["InterfaceServerID"]));
                        foreach (DataRow r in channelRows)
                        {
                            int number = Convert.ToInt32(r["ChannelNumber"]);
                            //if channel number = -1 (default channel), set to 255 (sent as a single byte)
                            if (number == -1)
                            {
                                number = 255;
                            }
                            int type = Convert.ToInt32(r["ChannelType"]);
                            channels.Add(new ConfigFileGPPacket.Channel(number, type));
                        }
                    }
                }
            }
            return channels;
        }
        #endregion
        #region T_RoutePointGroup and T_RoutePoint table functions
        /// <summary>
        /// This method will be used to refresh the Route group datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshRouteGroupDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objDS = null;

            try
            {
                #region Retrieve the data from the T_RoutePointGroup table
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("EXEC usp_GetRoutesForListenerLoad", oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                objDS.Tables[0].TableName = "RoutePointGroup";
                objDS.Tables[1].TableName = "RoutePoint";
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;

                lock (oRouteGroupDataSetSync)
                {
                    mRouteGroupDataSet = new DataSet();
                    mRouteGroupDataSet.Tables.Add(objDS.Tables["RoutePointGroup"].Copy());
                    mRouteGroupDataSet.AcceptChanges();
                }
                lock (oRoutePointDataSetSync)
                {
                    mRoutePointDataSet = new DataSet();
                    mRoutePointDataSet.Tables.Add(objDS.Tables["RoutePoint"].Copy());
                    mRoutePointDataSet.AcceptChanges();
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshRouteGroupDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        /// <summary>
        /// This method will be return the route group data from for the given fleet and RoutePointGroupID
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int RoutePointGroupID"></param>
        /// <returns>DataRow</returns>
        public DataRow GetRoutePointRowByID(int fleet, int RoutePointGroupID)
        {
            DataRow[] drRoutePointGroups = null;
            DataRow drResult = null;
            try
            {
                #region Get the data row for this fleet/route number
                lock (oRouteGroupDataSetSync)
                {
                    if (mRouteGroupDataSet != null)
                    {
                        if (mRouteGroupDataSet.Tables.Count > 0)
                        {
                            if (mRouteGroupDataSet.Tables[0].Rows.Count > 0)
                            {
                                drRoutePointGroups = mRouteGroupDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND ID = " + Convert.ToString(RoutePointGroupID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drRoutePointGroups != null)
                {
                    if (drRoutePointGroups.Length > 0)
                    {
                        drResult = drRoutePointGroups[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetRoutePointRowByID(int fleet = " + Convert.ToString(fleet) + ", int RoutePointGroupID = " + Convert.ToString(RoutePointGroupID) + ")", ex);
            }
            return drResult;
        }
        /// <summary>
        /// This method will be return the route group data from for the given fleet and routeNumber
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int routeNumber"></param>
        /// <returns>DataRow</returns>
        public DataRow GetRoutePointRowFromRoutePointNumber(int fleet, int routeNumber)
        {
            DataRow[] drRoutePointGroups = null;
            DataRow drResult = null;
            try
            {
                #region Get the data row for this fleet/route number
                lock (oRouteGroupDataSetSync)
                {
                    if (mRouteGroupDataSet != null)
                    {
                        if (mRouteGroupDataSet.Tables.Count > 0)
                        {
                            if (mRouteGroupDataSet.Tables[0].Rows.Count > 0)
                            {
                                drRoutePointGroups = mRouteGroupDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND RoutePointNumber = " + Convert.ToString(routeNumber));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drRoutePointGroups != null)
                {
                    if (drRoutePointGroups.Length > 0)
                    {
                        drResult = drRoutePointGroups[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetRoutePointRowFromRoutePointNumber(int fleet = " + Convert.ToString(fleet) + ", int routeNumber = " + Convert.ToString(routeNumber) + ")", ex);
            }
            return drResult;
        }
        /// <summary>
        /// This method will be return if the route group passed in in the ConfigTotalGPPacket is the current route group for the vehicle.
        /// </summary>
        /// <param name="ConfigTotalGPPacket cfPacket"></param>
        /// <returns>bool</returns>
        public bool DoesANewerRoutePointVersionExist(ConfigTotalGPPacket cfPacket)
        {
            bool bRet = false;
            DataRow drRow = null;
            byte bVerMajor = (byte)0x00;
            byte bVerMinor = (byte)0x00;

            try
            {
                drRow = GetRoutePointRowFromRoutePointNumber((int)cfPacket.cFleetId, (int)cfPacket.cRouteSetActive);
                if (drRow != null)
                {
                    bVerMajor = Convert.ToByte(Convert.ToInt16(drRow["VersionMajor"]) & 0xFF);
                    bVerMinor = Convert.ToByte(Convert.ToInt16(drRow["VersionMinor"]) & 0xFF);
                    if ((bVerMajor == cfPacket.cRouteSetActiveVersionMajor) && (bVerMinor == cfPacket.cRouteSetActiveVersionMinor))
                        bRet = false;
                    else
                        bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DoesANewerRoutePointVersionExist(ConfigTotalGPPacket cfPacket)", ex);
                bRet = false;
            }
            return bRet;
        }
        /// <summary>
        /// This method will be return the rouet group data from for the given fleet and route file number (Not ID)
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int configNumber"></param>
        /// <returns>DataRow</returns>
        public string GetRoutePointGroup(ref ConfigNewRoutePtGPPacket rpPacket, int iRPGroupID)
        {
            DataRow drRoutePointGroup = null;
            DataRow[] drRoutePoints = null;
            DataRow drRoutePoint = null;
            int iRoutePointGroupID = 0;
            GPRoutePoint myPoint = null;

            try
            {
                #region Retrieve the data from the T_RoutePointGroup table
                drRoutePointGroup = GetRoutePointRowFromRoutePointNumber((int)rpPacket.cFleetId, (int)rpPacket.cRouteSetNumber);
                if (drRoutePointGroup == null)
                    return "Couldn't locate Route Point Group Number " + Convert.ToString((int)rpPacket.cRouteSetNumber) + " for fleet " + Convert.ToString((int)rpPacket.cFleetId) + ", unit " + Convert.ToString(rpPacket.iVehicleId);
                else
                {
                    iRoutePointGroupID = Convert.ToInt32(drRoutePointGroup["ID"]);
                    iRPGroupID = iRoutePointGroupID;
                }
                #endregion
                #region Popualte the RP packet
                rpPacket.cPageAdjust = 0; // ?
                rpPacket.iRouteVersion = (Convert.ToInt16(drRoutePointGroup["VersionMajor"]) << 8);
                rpPacket.iRouteVersion += (Convert.ToInt16(drRoutePointGroup["VersionMinor"]));
                #region Get the data rows for the iRoutePointGroupID
                lock (oRoutePointDataSetSync)
                {
                    if (mRoutePointDataSet != null)
                    {
                        if (mRoutePointDataSet.Tables.Count > 0)
                        {
                            if (mRoutePointDataSet.Tables[0].Rows.Count > 0)
                            {
                                drRoutePoints = mRoutePointDataSet.Tables[0].Select("RoutePointGroupID = " + Convert.ToString(iRoutePointGroupID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drRoutePoints != null)
                {
                    if (drRoutePoints.Length > 0)
                    {
                        for (int X = 0; X < drRoutePoints.Length; X++)
                        {
                            drRoutePoint = drRoutePoints[X];
                            myPoint = new GPRoutePoint();
                            // Populate it:
                            myPoint.cPointNumber = Convert.ToByte(Convert.ToInt16(drRoutePoint["RoutePointNumber"]) & 0xFF);
                            myPoint.cNextPointNumber = Convert.ToByte(Convert.ToInt16(drRoutePoint["NextPointNumber"]) & 0xFF);
                            myPoint.iFlags = Convert.ToInt16(Convert.ToInt32(drRoutePoint["Flags"]) & 0xFFFF);
                            myPoint.fBinLat = Convert.ToSingle(drRoutePoint["Latitude"]);
                            myPoint.fBinLong = Convert.ToSingle(drRoutePoint["Longitude"]);
                            myPoint.iLatTolerance = Convert.ToInt16(Convert.ToInt32(drRoutePoint["XTolerance"]) & 0xFFFF);
                            myPoint.iLongTolerance = Convert.ToInt16(Convert.ToInt32(drRoutePoint["YTolerance"]) & 0xFFFF);
                            myPoint.cArriveHour = Convert.ToByte((Convert.ToInt16(drRoutePoint["ArriveTime"]) / 60) & 0xFF);
                            myPoint.cArriveMinute = Convert.ToByte((Convert.ToInt16(drRoutePoint["ArriveTime"]) % 60) & 0xFF);
                            myPoint.cArriveTolerance = Convert.ToByte(Convert.ToInt16(drRoutePoint["ArriveTimeToleranceMinute"]) & 0xFF);
                            myPoint.cDepartHour = Convert.ToByte((Convert.ToInt16(drRoutePoint["DepartureTime"]) / 60) & 0xFF);
                            myPoint.cDepartMinute = Convert.ToByte((Convert.ToInt16(drRoutePoint["DepartureTime"]) % 60) & 0xFF);
                            myPoint.cDepartTolerance = Convert.ToByte(Convert.ToInt16(drRoutePoint["DepartureTimeToleranceMinute"]) & 0xFF);
                            myPoint.cDurationTime = Convert.ToByte(Convert.ToInt16(drRoutePoint["DurationTime"]) & 0xFF);
                            myPoint.iDistanceToNext = Convert.ToInt16(Convert.ToInt32(drRoutePoint["DistanceToNextPoint"]) & 0xFFFF);
                            myPoint.cTimeToNext = Convert.ToByte(Convert.ToInt16(drRoutePoint["TimeToNextPoint"]) & 0xFF);
                            // Add it to the group - this will also set mNumPoints correctly.
                            rpPacket.AddPoint(myPoint);
                        }
                    }
                }
                #endregion
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetRoutePointGroup(ref ConfigNewRoutePtGPPacket rpPacket, int iRPGroupID)", ex);
            }
            return null;
        }
        public DataRow GetRouteUpdateValues(long tableId)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objDS = null;
            System.Data.SqlClient.SqlConnection oConn = null;
            try
            {
                #region Retrieve the data from the T_RoutePointGroup table
                oConn = GetOpenConnection();
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("EXEC [usp_Route2GetRouteStatusUpdateValues] " + Convert.ToString(tableId), oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion
                if (objDS != null && objDS.Tables.Count > 0 && objDS.Tables[0].Rows.Count > 0)
                    return objDS.Tables[0].Rows[0];
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetRouteUpdateValues(long tableId = " + Convert.ToString(tableId) + ")", ex);
            }
            return null;
        }
        #endregion
        #region T_SetPointGroup and T_SetPoint table and Server Side Waypoint functions
        private void LoadWPLookupTree()
        {
            try
            {
                #region Load WP data into a lookup tree
                WaypointLookupTree oTempWPLookupTree = new WaypointLookupTree(sDSN, 6);
                lock (oWPLookupTreeSync)
                {
                    if (oWPLookupTree != null)
                    {
                        oWPLookupTree.Dispose();
                        oWPLookupTree = null;
                    }
                    oWPLookupTree = oTempWPLookupTree;
                    oMelwayTree = oWPLookupTree.oMelwaysRefLookup;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadWPLookupTree()", ex);
            }
        }
        /// <summary>
        /// This method will be used to refresh the WP group datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshSetPointGroupDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            SqlDataAdapter objSQLDA = null;
            SqlCommand objSQLCmd = null;
            DataSet dsResult = null;
            DataTable setpointGroups = null;
            DataTable setpoints = null;
            DataTable setpointNodes = null;

            DataSet objSetPointsToSendToUnit = null;

            ArrayList oGroupsWithPolyWPs = null;
            DataSet oVehicleDataSet = null;
            Hashtable oTempUnitToSSWPGroup = new Hashtable();
            Hashtable oWPGroupToUnitSideWPs = new Hashtable();
            ArrayList oUnitSideWPs = new ArrayList();
            ArrayList oServerSideWPGroups = new ArrayList();
            //MelwaysLookup oTempMelwaysLookup = null;
            List<Tuple<int, int>> vehiclesWpServerSideOnly = new List<Tuple<int, int>>();
            try
            {
                #region Retrieve the data from the T_SetPointGroup table
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("EXEC usp_GetSetPointsForListenerLoad", oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                dsResult = new DataSet();
                objSQLDA.Fill(dsResult);
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion

                setpointGroups = dsResult.Tables[0];
                setpoints = dsResult.Tables[1];
                setpointNodes = dsResult.Tables[2];
                objSetPointsToSendToUnit = new DataSet();
                objSetPointsToSendToUnit.Tables.Add(setpoints.Clone());


                #region Update the polygon and server side group flags for each objSetpointGroup and objSetpoints record and create unit side WP lists (oWPGroupToUnitSideWPs and objSetPointsToSendToUnit)
                oServerSideWPGroups = new ArrayList();
                oGroupsWithPolyWPs = new ArrayList();
                foreach (DataRow setpointGroup in setpointGroups.Rows)
                {
                    int spGroupId = Convert.ToInt32(setpointGroup["ID"]);
                    bool isServerSideGroup = Convert.ToBoolean(setpointGroup["IsServerSideGroup"]);
                    bool isPolygonGroup = Convert.ToBoolean(setpointGroup["IsPolygonGroup"]);
                    if (isPolygonGroup && !oGroupsWithPolyWPs.Contains(spGroupId))
                        oGroupsWithPolyWPs.Add(spGroupId);
                    oUnitSideWPs = new ArrayList();
                    DataRow[] drSetpoints = null;
                    if (isPolygonGroup)
                    {
                        #region If this is a polygon WP group then the unit supports 300 points with a max of 20 nodes in each point
                        if (!isServerSideGroup)
                        {
                            #region If this is not flaged as a server side group, copy all the points to objSetPointsToSendToUnit and oUnitSideWPs
                            drSetpoints = setpoints.Select("SetPointGroupID = " + spGroupId);
                            foreach (DataRow setpoint in drSetpoints)
                            {
                                int setPointId = Convert.ToInt32(setpoint["ID"]);
                                DataRow drNew = objSetPointsToSendToUnit.Tables[0].NewRow();
                                for (int col = 0; col < setpoint.Table.Columns.Count; col++)
                                    drNew[col] = setpoint[col];
                                objSetPointsToSendToUnit.Tables[0].Rows.Add(drNew);

                                if (!oUnitSideWPs.Contains(setPointId))
                                    oUnitSideWPs.Add(setPointId);
                            }
                            #endregion
                        }
                        else
                        {
                            // This is a server side wp group, add this to the list
                            if (!oServerSideWPGroups.Contains(spGroupId))
                                oServerSideWPGroups.Add(spGroupId);

                            #region If this is flaged as a server side group, copy all the points marked as UnitSideWP = 1 to objSetPointsToSendToUnit and oUnitSideWPs
                            drSetpoints = setpoints.Select("SetPointGroupID = " + spGroupId + " AND UnitSideWP = 1");
                            int spCount = 0;
                            foreach (DataRow setpoint in drSetpoints)
                            {
                                spCount++;
                                if (spCount > 300)
                                {
                                    setpoint["UnitSideWP"] = 0;
                                    setpoint.AcceptChanges();
                                }
                                else
                                {
                                    DataRow drNew = objSetPointsToSendToUnit.Tables[0].NewRow();
                                    for (int col = 0; col < setpoint.Table.Columns.Count; col++)
                                        drNew[col] = setpoint[col];
                                    objSetPointsToSendToUnit.Tables[0].Rows.Add(drNew);
                                    int setPointId = Convert.ToInt32(setpoint["ID"]);
                                    if (!oUnitSideWPs.Contains(setPointId))
                                        oUnitSideWPs.Add(setPointId);
                                }
                            }
                            #endregion
                            #region If we have not copied out 300 WPs to the unit, add any other WP in this group that CAN be processed on the unit
                            if (drSetpoints.Length < 300)
                            {
                                int addAdditionalPoints = 300 - drSetpoints.Length;
                                drSetpoints = setpoints.Select("SetPointGroupID = " + spGroupId + " AND UnitSideWP = 0 AND NodeCount < 20");
                                foreach (DataRow setpoint in drSetpoints)
                                {
                                    DataRow drNew = objSetPointsToSendToUnit.Tables[0].NewRow();
                                    for (int col = 0; col < setpoint.Table.Columns.Count; col++)
                                        drNew[col] = setpoint[col];
                                    objSetPointsToSendToUnit.Tables[0].Rows.Add(drNew);
                                    int setPointId = Convert.ToInt32(setpoint["ID"]);
                                    if (!oUnitSideWPs.Contains(setPointId))
                                        oUnitSideWPs.Add(setPointId);

                                    setpoint["UnitSideWP"] = 1;
                                    setpoint.AcceptChanges();

                                    addAdditionalPoints--;
                                    if (addAdditionalPoints <= 0)
                                        break;
                                }
                            }
                            #endregion
                        }
                        // Assosiate the list of unit side WPs with the setpointgroupID.
                        oWPGroupToUnitSideWPs.Add(spGroupId, oUnitSideWPs);
                        #endregion
                    }
                    else
                    {
                        #region If this is a non polygon group, then the unit supports 39 points (Legacy Firmware support)
                        if (!isServerSideGroup)
                        {
                            #region If this is not flaged as a server side group, copy all the points to objSetPointsToSendToUnit and oUnitSideWPs
                            drSetpoints = setpoints.Select("SetPointGroupID = " + spGroupId);
                            foreach (DataRow setpoint in drSetpoints)
                            {
                                DataRow drNew = objSetPointsToSendToUnit.Tables[0].NewRow();
                                for (int col = 0; col < setpoint.Table.Columns.Count; col++)
                                    drNew[col] = setpoint[col];
                                objSetPointsToSendToUnit.Tables[0].Rows.Add(drNew);
                                int setPointId = Convert.ToInt32(setpoint["ID"]);
                                if (!oUnitSideWPs.Contains(setPointId))
                                    oUnitSideWPs.Add(setPointId);
                            }
                            #endregion
                        }
                        else
                        {
                            // This is a server side wp group, add this to the list
                            if (!oServerSideWPGroups.Contains(spGroupId))
                                oServerSideWPGroups.Add(spGroupId);
                            #region If this is flaged as a server side group, copy up to 39 points marked as UnitSideWP = 1 to objSetPointsToSendToUnit and oUnitSideWPs
                            drSetpoints = setpoints.Select("SetPointGroupID = " + spGroupId + " AND UnitSideWP = 1");
                            int iAddedPoints = 0;
                            foreach (DataRow setpoint in drSetpoints)
                            {
                                DataRow drNew = objSetPointsToSendToUnit.Tables[0].NewRow();
                                for (int col = 0; col < setpoint.Table.Columns.Count; col++)
                                    drNew[col] = setpoint[col];
                                objSetPointsToSendToUnit.Tables[0].Rows.Add(drNew);
                                int setPointId = Convert.ToInt32(setpoint["ID"]);
                                if (!oUnitSideWPs.Contains(setPointId))
                                    oUnitSideWPs.Add(setPointId);
                                iAddedPoints++;
                                if (iAddedPoints >= 39)
                                    break;
                            }
                            #endregion
                            #region If we have not copied out 39 WPs to the unit, add any other WP in this group that CAN be processed on the unit
                            if (oUnitSideWPs.Count < 39)
                            {
                                int addAdditionalPoints = 39 - oUnitSideWPs.Count;
                                drSetpoints = setpoints.Select("SetPointGroupID = " + spGroupId + " AND UnitSideWP = 0");
                                foreach (DataRow setpoint in drSetpoints)
                                {
                                    DataRow drNew = objSetPointsToSendToUnit.Tables[0].NewRow();
                                    for (int col = 0; col < setpoint.Table.Columns.Count; col++)
                                        drNew[col] = setpoint[col];
                                    objSetPointsToSendToUnit.Tables[0].Rows.Add(drNew);
                                    int setPointId = Convert.ToInt32(setpoint["ID"]);
                                    if (!oUnitSideWPs.Contains(setPointId))
                                        oUnitSideWPs.Add(setPointId);
                                    addAdditionalPoints--;
                                    if (addAdditionalPoints <= 0)
                                        break;
                                }
                            }
                            #endregion
                        }
                        #endregion
                    }
                }
                #endregion
                #region Setup the hash table to map units to server side WPs
                lock (oVehicleDataSetSync)
                    oVehicleDataSet = mVehicleDataSet.Copy();

                for (int X = 0; X < oVehicleDataSet.Tables[0].Rows.Count; X++)
                {
                    int iFleetID = Convert.ToInt32(oVehicleDataSet.Tables[0].Rows[X]["FleetID"]);
                    int iVehicleID = Convert.ToInt32(oVehicleDataSet.Tables[0].Rows[X]["ID"]);
                    int iSetPointGroupID = Convert.ToInt32(oVehicleDataSet.Tables[0].Rows[X]["SetPointGroupID"]);
                    bool serverSideOnly = oVehicleDataSet.Tables[0].Rows[X].Field<bool>("WPServerSideOnly");
                    if (serverSideOnly)
                    {
                        Tuple<int, int> t = new Tuple<int, int>(iFleetID, iVehicleID);
                        if (!vehiclesWpServerSideOnly.Contains(t))
                        {
                            vehiclesWpServerSideOnly.Add(t);
                        }
                    }
                    else if (oServerSideWPGroups.Contains(iSetPointGroupID))
                    {
                        long lKey = GetVehicleHashKey(iFleetID, iVehicleID);
                        if (!oTempUnitToSSWPGroup.ContainsKey(lKey))
                            oTempUnitToSSWPGroup.Add(lKey, iSetPointGroupID);
                    }
                }
                #endregion

                #region Update the mSetGroupDataSet and mSetPointDataSet at the same time
                lock (oSetGroupDataSetSync)
                {
                    lock (oSetPointDataSetSync)
                    {
                        if (mSetGroupDataSet != null)
                        {
                            mSetGroupDataSet.Dispose();
                            mSetGroupDataSet = null;
                        }
                        mSetGroupDataSet = new DataSet();
                        mSetGroupDataSet.Tables.Add(setpointGroups.Copy());
                        mSetGroupDataSet.Tables[0].TableName = "SetPointGroups";
                        mSetGroupDataSet.AcceptChanges();

                        if (mSetPointDataSet != null)
                        {
                            mSetPointDataSet.Dispose();
                            mSetPointDataSet = null;
                        }
                        mSetPointDataSet = new DataSet();
                        mSetPointDataSet.Tables.Add(setpoints.Copy());
                        mSetPointDataSet.Tables[0].TableName = "SetPoints";
                        mSetPointDataSet.AcceptChanges();

                        if (mSetPointNodesDataSet != null)
                        {
                            mSetPointNodesDataSet.Dispose();
                            mSetPointNodesDataSet = null;
                        }
                        mSetPointNodesDataSet = new DataSet();
                        mSetPointNodesDataSet.Tables.Add(setpointNodes.Copy());
                        mSetPointNodesDataSet.Tables[0].TableName = "SetPointNodes";
                        mSetPointNodesDataSet.AcceptChanges();


                        if (mSetPointsToSendToUnitDataSet != null)
                        {
                            mSetPointsToSendToUnitDataSet.Dispose();
                            mSetPointsToSendToUnitDataSet = null;
                        }
                        mSetPointsToSendToUnitDataSet = objSetPointsToSendToUnit.Copy();

                        if (oUnitToSSWPGroup != null)
                        {
                            oUnitToSSWPGroup.Clear();
                            oUnitToSSWPGroup = null;
                        }
                        oUnitToSSWPGroup = oTempUnitToSSWPGroup;
                        if (mWPGroupToUnitSideWPs != null)
                        {
                            mWPGroupToUnitSideWPs.Clear();
                            mWPGroupToUnitSideWPs = null;
                        }
                        mWPGroupToUnitSideWPs = oWPGroupToUnitSideWPs;

                        if (mServerSideWPGroupList != null)
                        {
                            mServerSideWPGroupList.Clear();
                            mServerSideWPGroupList = null;
                        }
                        mServerSideWPGroupList = oServerSideWPGroups;
                        if (mPolygonWPGroupList != null)
                        {
                            mPolygonWPGroupList.Clear();
                            mPolygonWPGroupList = null;
                        }
                        mPolygonWPGroupList = oGroupsWithPolyWPs;
                        _vehiclesWpServerSideOnly = vehiclesWpServerSideOnly;
                    }
                }
                #endregion

                lock (oTemporalWaypointsSync)
                {
                    _temporalWaypoints.Clear();
                    if (dsResult.Tables.Count > 3)
                    {
                        foreach (DataRow row in dsResult.Tables[3].Rows)
                        {
                            int setPoint = Convert.ToInt32(row["SetPointId"]);
                            if (!_temporalWaypoints.ContainsKey(setPoint))
                            {
                                _temporalWaypoints.Add(setPoint, new TemporalWayPoint(setPoint));
                            }
                            DayOfWeek day = (DayOfWeek)Convert.ToInt32(row["DayOfTheWeek"]);
                            int start = Convert.ToInt32(row["StartTime"]);
                            int duration = Convert.ToInt32(row["Duration"]);
                            _temporalWaypoints[setPoint].Times.Add(new TimePeriod(day, start, duration));
                        }
                    }
                    if (dsResult.Tables.Count > 4)
                    {
                        _vehicleTemporalWayPoints = dsResult.Tables[4];
                    }
                    if (dsResult.Tables.Count > 5)
                    {
                        _vehicleWayPoints = dsResult.Tables[5];
                    }
                }

            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshSetPointGroupDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        /// <summary>
        /// This method will be used to return a SetPointGroup data row for the given fleet and setPointGroupID
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int setPointGroupID"></param>
        /// <returns>DataRow</returns>
        public DataRow GetSetPointRowByID(int fleet, int setPointGroupID)
        {
            DataRow[] drSetPointGroups = null;
            DataRow drResult = null;
            try
            {
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {
                    if (mSetGroupDataSet != null)
                    {
                        if (mSetGroupDataSet.Tables.Count > 0)
                        {
                            if (mSetGroupDataSet.Tables[0].Rows.Count > 0)
                            {
                                drSetPointGroups = mSetGroupDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND ID = " + Convert.ToString(setPointGroupID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drSetPointGroups != null)
                {
                    if (drSetPointGroups.Length > 0)
                    {
                        drResult = drSetPointGroups[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSetPointRowByID(int fleet = " + Convert.ToString(fleet) + ", int setPointGroupID = " + Convert.ToString(setPointGroupID) + ")", ex);
            }
            return drResult;
        }
        /// <summary>
        /// This method will be used to return a SetPointGroup data row for the given fleet and setPointGroupID
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int setPointGroupID"></param>
        /// <returns>DataRow</returns>
        public DataSet GetSetPointUpdateDataset(int iSetPointID)
        {
            DataSet dsRet = null;
            DataTable dtTemp = null;
            DataRow[] drSetPoints = null;
            DataRow[] drSetPointGroups = null;
            DataRow[] drSetPointNodes = null;
            DataRow drTemp = null;
            DataRow drSetPoint = null;
            DataRow drSetPointGroup = null;
            int iSetPointGroupID = 0;

            try
            {
                #region Create a result data set with a T_SetPointGroup, T_SetPoint and T_SetPointNode table
                dsRet = new DataSet();
                lock (oSetGroupDataSetSync)
                {
                    if (mSetGroupDataSet != null && mSetGroupDataSet.Tables.Count > 0)
                    {
                        dtTemp = mSetGroupDataSet.Tables[0].Clone();
                        dtTemp.TableName = "T_SetPointGroup";
                        dsRet.Tables.Add(dtTemp);
                    }
                    if (mSetPointDataSet != null && mSetPointDataSet.Tables.Count > 0)
                    {
                        dtTemp = mSetPointDataSet.Tables[0].Clone();
                        dtTemp.TableName = "T_SetPoint";
                        dsRet.Tables.Add(dtTemp);
                    }
                    if (mSetPointNodesDataSet != null && mSetPointNodesDataSet.Tables.Count > 0)
                    {
                        dtTemp = mSetPointNodesDataSet.Tables[0].Clone();
                        dtTemp.TableName = "T_SetPointNode";
                        dsRet.Tables.Add(dtTemp);
                    }
                }
                #endregion
                #region Get the data row for the given setpoint ID
                lock (oSetGroupDataSetSync)
                {
                    if (mSetPointDataSet != null && mSetPointDataSet.Tables.Count > 0 && mSetPointDataSet.Tables[0].Rows.Count > 0)
                        drSetPoints = mSetPointDataSet.Tables[0].Select("ID = " + Convert.ToString(iSetPointID));
                }
                #endregion
                #region Read the results
                if (drSetPoints != null && drSetPoints.Length > 0)
                {
                    drSetPoint = drSetPoints[0];
                    iSetPointGroupID = Convert.ToInt32(drSetPoint["SetPointGroupID"]);
                    lock (oSetGroupDataSetSync)
                    {
                        if (mSetGroupDataSet != null && mSetGroupDataSet.Tables.Count > 0 && mSetGroupDataSet.Tables[0].Rows.Count > 0)
                            drSetPointGroups = mSetGroupDataSet.Tables[0].Select("ID = " + Convert.ToString(iSetPointGroupID));
                    }
                    if (drSetPoints != null && drSetPoints.Length > 0)
                    {
                        #region Add the setpoint group record to the result dataset
                        drSetPointGroup = drSetPointGroups[0];
                        drTemp = dsRet.Tables[0].NewRow();
                        for (int iCol = 0; iCol < drTemp.Table.Columns.Count; iCol++)
                            drTemp[iCol] = drSetPointGroup[iCol];
                        dsRet.Tables[0].Rows.Add(drTemp);
                        #endregion
                        #region Add the setpoint record to the result dataset
                        drTemp = dsRet.Tables[1].NewRow();
                        for (int iCol = 0; iCol < drTemp.Table.Columns.Count; iCol++)
                            drTemp[iCol] = drSetPoint[iCol];
                        dsRet.Tables[1].Rows.Add(drTemp);
                        #endregion
                        #region Add any linked set point node records to the result dataset
                        lock (oSetGroupDataSetSync)
                        {
                            if (mSetPointNodesDataSet != null && mSetPointNodesDataSet.Tables.Count > 0 && mSetPointNodesDataSet.Tables[0].Rows.Count > 0)
                                drSetPointNodes = mSetPointNodesDataSet.Tables[0].Select("SetPointID = " + Convert.ToString(iSetPointID));
                        }
                        if (drSetPointNodes != null && drSetPointNodes.Length > 0)
                        {
                            for (int iRow = 0; iRow < drSetPointNodes.Length; iRow++)
                            {
                                drTemp = dsRet.Tables[2].NewRow();
                                for (int iCol = 0; iCol < drTemp.Table.Columns.Count; iCol++)
                                    drTemp[iCol] = drSetPointNodes[iRow][iCol];
                                dsRet.Tables[2].Rows.Add(drTemp);
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSetPointUpdateDataset(int iSetPointID = " + Convert.ToString(iSetPointID) + ")", ex);
            }
            return dsRet;
        }
        /// <summary>
        /// This method will be used to return a SetPointGroup data row for the given fleet and setPointGroupNumber
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int setPointGroupNumber"></param>
        /// <returns>DataRow</returns>
        public DataRow GetSetPointRowFromSetPointNumber(int fleet, int setPointGroupNumber)
        {
            DataRow[] drSetPointGroups = null;
            DataRow drResult = null;
            try
            {
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {
                    if (mSetGroupDataSet != null)
                    {
                        if (mSetGroupDataSet.Tables.Count > 0)
                        {
                            if (mSetGroupDataSet.Tables[0].Rows.Count > 0)
                            {
                                drSetPointGroups = mSetGroupDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND SetPointNumber = " + Convert.ToString(setPointGroupNumber));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drSetPointGroups != null)
                {
                    if (drSetPointGroups.Length > 0)
                    {
                        drResult = drSetPointGroups[0];
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSetPointRowFromSetPointNumber(int fleet = " + Convert.ToString(fleet) + ", int setPointGroupNumber = " + Convert.ToString(setPointGroupNumber) + ")", ex);
            }
            return drResult;
        }
        /// <summary>
        /// This method will be used to check if the values in the ConfigTotalGPPacket are the correct values assigned to the unit
        /// </summary>
        /// <param name="ConfigTotalGPPacket cfPacket"></param>
        /// <returns>bool</returns>
        public bool DoesANewerSetPointVersionExist(ConfigTotalGPPacket cfPacket)
        {
            bool bRet = false;
            DataRow drRow = null;
            byte bVerMajor = (byte)0x00;
            byte bVerMinor = (byte)0x00;

            try
            {
                drRow = GetSetPointRowFromSetPointNumber((int)cfPacket.cFleetId, (int)cfPacket.cSetPointSetActive);
                if (drRow != null)
                {
                    bVerMajor = Convert.ToByte(Convert.ToInt16(drRow["VersionMajor"]) & 0xFF);
                    bVerMinor = Convert.ToByte(Convert.ToInt16(drRow["VersionMinor"]) & 0xFF);
                    if ((bVerMajor == cfPacket.cSetPointSetActiveVersionMajor) && (bVerMinor == cfPacket.cSetPointSetActiveVersionMinor))
                        bRet = false;
                    else
                        bRet = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DoesANewerSetPointVersionExist(ConfigTotalGPPacket cfPacket)", ex);
                bRet = false;
            }
            return bRet;
        }
        /// <summary>
        /// This method will be used to retrieve the waypoint group that is currently assigned to a fleet/vehicle
        /// </summary>
        /// <param name="int fleetID"></param>
        /// <param name="uint unitID"></param>
        /// <returns>int</returns>
        public int GetSetPointGroupIDForVehicle(int fleetID, uint unitID)
        {
            int iSetPointGroupID = 0;
            DataRow[] rows = null;

            try
            {
                lock (oVehicleDataSetSync)
                    if (mVehicleDataSet != null)
                        rows = mVehicleDataSet.Tables[0].Select(string.Format("FleetID = {0} AND ID = {1}", fleetID, unitID));

                if ((rows != null) && (rows.Length > 0))
                    if (rows[0]["SetPointGroupID"] != System.DBNull.Value)
                        iSetPointGroupID = Convert.ToInt32(rows[0]["SetPointGroupID"]);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSetPointGroupIDForVehicle(int fleetID = " + Convert.ToString(fleetID) + ", uint unitID = " + Convert.ToString(unitID) + ")", ex);
            }
            return iSetPointGroupID;
        }
        /// <summary>
        /// This method will be used to return a SetPointGroupNumber for the given fleet and setPointGroupID
        /// </summary>
        /// <param name="int fleet"></param>
        /// <param name="int setPointGroupID"></param>
        /// <returns>int</returns>
        public int GetSetPointNumberFromID(int fleet, int setPointGroupID)
        {
            int iRet = 0;
            DataRow[] drSetPointGroups = null;

            try
            {
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {
                    if (mSetGroupDataSet != null)
                    {
                        if (mSetGroupDataSet.Tables.Count > 0)
                        {
                            if (mSetGroupDataSet.Tables[0].Rows.Count > 0)
                            {
                                drSetPointGroups = mSetGroupDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleet) + " AND ID = " + Convert.ToString(setPointGroupID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drSetPointGroups != null)
                {
                    if (drSetPointGroups.Length > 0)
                    {
                        if (drSetPointGroups[0]["SetPointNumber"] != System.DBNull.Value)
                        {
                            iRet = Convert.ToInt32(drSetPointGroups[0]["SetPointNumber"]);
                        }
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSetPointNumberFromID(int fleet = " + Convert.ToString(fleet) + ", int setPointGroupID = " + Convert.ToString(setPointGroupID) + ")", ex);
            }
            return iRet;
        }
        /// <summary>
        /// This method will be used to populate the spPacket with the setpoint details based on the fleet/vehicle ID contained in the spPacket entry
        /// </summary>
        /// <param name="ref ConfigNewSetPtGPPacket spPacket"></param>
        /// <param name="ref int iSPGroupdID"></param>
        /// <returns>string</returns>
        public string GetSetPointGroup(ref ConfigNewSetPtGPPacket spPacket, ref int iSPGroupdID, int iPolygonUnit, bool bCreateDummyPacket)
        {
            int iSetPointGroupID = 0;
            DataRow drSetPointGroup = null;
            DataRow[] drSetPoints = null;
            DataRow[] dSSWPtoSendToUnit = null;
            DataRow[] drSetPointNodes = null;
            int iSetPointID = 0;
            int iSetPointNumberCounter = 1;
            GPSetPoint myPoint = null;
            double dLatitude = 0;
            double dLongitude = 0;
            double dLongToleranceDegrees = 0;
            double dLatToleranceDegrees = 0;
            int iLatLookup = 0;
            PolygonNode oPolyNode = null;
            bool bIsPolygonWPGroup = false;
            int iNumOfPointsAdded = 0;
            try
            {
                if ((int)spPacket.cSetPointSetNumber == 0 || bCreateDummyPacket)
                {
                    #region Setup the packet header
                    if ((int)spPacket.cSetPointSetNumber == 0)
                    {
                        // Set the packet vars too zero.
                        spPacket.cSetPointSetNumber = 0;
                        spPacket.cSetPointSetActiveVersionMajor = 0;
                        spPacket.cSetPointSetActiveVersionMinor = 0;
                        spPacket.cPageAdjust = 0;
                        spPacket.iSetPointVersion = 0;
                        _log.Info("Sending 'NONE' set point group to " + Convert.ToString((int)spPacket.cFleetId) + "/" + Convert.ToString((int)spPacket.iVehicleId));
                    }
                    else
                    {
                        drSetPointGroup = GetSetPointRowFromSetPointNumber((int)spPacket.cFleetId, (int)spPacket.cSetPointSetNumber);
                        if (drSetPointGroup == null)
                            return "Couldn't locate Set Point Group Number " + Convert.ToString(spPacket.cSetPointSetNumber);
                        iSetPointGroupID = Convert.ToInt32(drSetPointGroup["ID"]);
                        iSPGroupdID = iSetPointGroupID;
                        spPacket.iSetPointVersion = (Convert.ToInt16(drSetPointGroup["VersionMajor"]) << 8);
                        spPacket.iSetPointVersion += (Convert.ToInt16(drSetPointGroup["VersionMinor"]));
                        spPacket.cPageAdjust = 0;
                        _log.Info("Sending dummy set point group to " + Convert.ToString((int)spPacket.cFleetId) + "/" + Convert.ToString((int)spPacket.iVehicleId));
                    }
                    #endregion
                    #region Populate the packet with dummy points
                    if (iPolygonUnit == 1)
                        bIsPolygonWPGroup = true;
                    if (bIsPolygonWPGroup)
                    {
                        myPoint = new GPSetPoint(4);
                        myPoint.iSetPointID = 0;
                        myPoint.PolySideNodes = 0;
                        myPoint.fBinLat = -1;
                        myPoint.fBinLong = -1;
                        myPoint.iLatTolerance = 1;
                        myPoint.iLongTolerance = 1;
                        myPoint.cDurationTime = 0;
                        myPoint.cMaxSpeedNmh = (byte)0x00;

                        oPolyNode = new PolygonNode();
                        oPolyNode.Longitude = (float)-1;
                        oPolyNode.Latitude = (float)-1;
                        myPoint.AddPointNode(oPolyNode);
                        oPolyNode = new PolygonNode();
                        oPolyNode.Longitude = (float)-1;
                        oPolyNode.Latitude = (float)-1;
                        myPoint.AddPointNode(oPolyNode);
                        oPolyNode = new PolygonNode();
                        oPolyNode.Longitude = (float)-1;
                        oPolyNode.Latitude = (float)-1;
                        myPoint.AddPointNode(oPolyNode);
                        oPolyNode = new PolygonNode();
                        oPolyNode.Longitude = (float)-1;
                        oPolyNode.Latitude = (float)-1;
                        myPoint.AddPointNode(oPolyNode);
                        spPacket.AddPoint(myPoint);
                    }
                    else
                    {
                        for (int iPadPoint = 1; iPadPoint < 39; iPadPoint++)
                        {
                            myPoint = new GPSetPoint();
                            float fLocation = Convert.ToSingle(-1) - (Convert.ToSingle(iPadPoint) / Convert.ToSingle(100));
                            // Populate it:
                            myPoint.cPointNumberOld = (byte)iPadPoint;
                            myPoint.cFlagsOld = (byte)0x00;
                            myPoint.fBinLat = fLocation;
                            myPoint.fBinLong = fLocation;
                            myPoint.iLatTolerance = 10;
                            myPoint.iLongTolerance = 10;
                            myPoint.cDurationTime = (byte)0x00;
                            myPoint.cMaxSpeedNmh = (byte)0x00;
                            myPoint.uPointNumber = (ushort)iPadPoint;
                            myPoint.uFlags = (ushort)0x00;
                            // Add it to the group, and set mNumPoints correctly:
                            spPacket.AddPoint(myPoint);
                        }
                    }
                    #endregion
                }
                else
                {
                    #region Retrieve the Setpoint Group record
                    drSetPointGroup = GetSetPointRowFromSetPointNumber((int)spPacket.cFleetId, (int)spPacket.cSetPointSetNumber);
                    if (drSetPointGroup == null)
                        return "Couldn't locate Set Point Group Number " + Convert.ToString(spPacket.cSetPointSetNumber);
                    iSetPointGroupID = Convert.ToInt32(drSetPointGroup["ID"]);
                    iSPGroupdID = iSetPointGroupID;
                    bIsPolygonWPGroup = IsPolygonWPGroup(iSetPointGroupID);
                    if (iPolygonUnit == 1)
                        bIsPolygonWPGroup = true;
                    #endregion
                    #region Populate the packet
                    spPacket.cPageAdjust = 0; // ?
                    spPacket.iSetPointVersion = (Convert.ToInt16(drSetPointGroup["VersionMajor"]) << 8);
                    spPacket.iSetPointVersion += (Convert.ToInt16(drSetPointGroup["VersionMinor"]));
                    if (IsServerSideWP(iSetPointGroupID))
                    {
                        #region Server side processed way point group.
                        try
                        {
                            lock (oSetPointDataSetSync)
                            {
                                dSSWPtoSendToUnit = mSetPointsToSendToUnitDataSet.Tables[0].Select("SetPointGroupID = " + Convert.ToString(iSetPointGroupID), "SetPointNumber");
                            }
                            iSetPointNumberCounter = 1;
                            if ((dSSWPtoSendToUnit != null) && (dSSWPtoSendToUnit.Length > 0))
                            {
                                #region If there are unit side WP to send
                                foreach (DataRow dRow in dSSWPtoSendToUnit)
                                {
                                    //	Only send active packets
                                    if ((dbVer < 5.9) || ((dbVer >= 5.9) && (bool)dRow["Active"]))
                                    {
                                        if (bIsPolygonWPGroup)
                                        {
                                            #region Encode a polygon WP
                                            iSetPointID = Convert.ToInt32(dRow["ID"]);
                                            lock (oSetGroupDataSetSync)
                                                drSetPointNodes = (mSetPointNodesDataSet.Tables["SetPointNodes"].Select("SetPointID = " + iSetPointID, "Sequence"));

                                            if (drSetPointNodes.Length == 2 || drSetPointNodes.Length == 0)
                                                myPoint = new GPSetPoint(4);
                                            else
                                                myPoint = new GPSetPoint(drSetPointNodes.Length);
                                            myPoint.iSetPointID = iSetPointID;
                                            myPoint.PolySideNodes = 0;
                                            myPoint.fBinLat = Convert.ToSingle(dRow["Latitude"]);
                                            myPoint.fBinLong = Convert.ToSingle(dRow["Longitude"]);
                                            int coord = Convert.ToInt32(dRow["XTolerance"]) & 0xFFFF;
                                            if (coord > Int16.MaxValue)
                                            {
                                                coord = Int16.MaxValue;
                                            }
                                            myPoint.iLatTolerance = Convert.ToInt16(coord);
                                            coord = Convert.ToInt32(dRow["YTolerance"]) & 0xFFFF;
                                            if (coord > Int16.MaxValue)
                                            {
                                                coord = Int16.MaxValue;
                                            }
                                            myPoint.iLongTolerance = Convert.ToInt16(coord);
                                            myPoint.cDurationTime = Convert.ToByte(Convert.ToInt16(dRow["DurationTime"]) & 0xFF);
                                            myPoint.cMaxSpeedNmh = (byte)0x00;
                                            if (this.dbVer >= 5.9)
                                            {
                                                if (!dRow.IsNull("MaxSpeedNmH"))
                                                {
                                                    myPoint.cMaxSpeedNmh = GetFloorByteFromFloat(Convert.ToDouble(dRow["MaxSpeedNmh"]));
                                                    myPoint.cMaxSpeedNmhDecimalPlaces = GetByteFromFloatDecimalValue(Convert.ToDouble(dRow["MaxSpeedNmh"]));
                                                }
                                                else
                                                {
                                                    myPoint.cMaxSpeedNmh = 0;
                                                    myPoint.cMaxSpeedNmhDecimalPlaces = 0;
                                                }
                                                myPoint.uPointNumber = Convert.ToUInt16(Convert.ToInt32(dRow["SetPointNumber"]) & 0xFFFF);
                                                myPoint.uFlags = Convert.ToUInt16(Convert.ToInt32(dRow["Flags"]) & 0xFFFF);
                                            }
                                            else
                                            {
                                                myPoint.cMaxSpeedNmh = (byte)0x00;
                                                myPoint.uPointNumber = 0;
                                                myPoint.uFlags = 0;
                                            }
                                            if (drSetPointNodes.Length == 2 || drSetPointNodes.Length == 0)
                                            {
                                                #region Create a square polygon for this point, base on the Latitude/Longitude and Tolerance values
                                                dLatitude = Convert.ToDouble(dRow["Latitude"]);
                                                dLongitude = Convert.ToDouble(dRow["Longitude"]);
                                                dLatToleranceDegrees = (((Convert.ToDouble(dRow["ToleranceNSMetres"]))) / 1.852) / (double)60000;
                                                iLatLookup = (int)dLatitude;
                                                if (iLatLookup < 0)
                                                    iLatLookup *= -1;
                                                dLongToleranceDegrees = (((Convert.ToDouble(dRow["ToleranceEWMetres"]) / 1.852) / CosLatTable[iLatLookup]) / (double)60000);

                                                for (int i = 0; i < 4; i++)
                                                {
                                                    oPolyNode = new PolygonNode();
                                                    switch (i)
                                                    {
                                                        case 0:
                                                            oPolyNode.Longitude = (float)(dLongitude + dLongToleranceDegrees);
                                                            oPolyNode.Latitude = (float)(dLatitude - dLatToleranceDegrees);
                                                            break;
                                                        case 1:
                                                            oPolyNode.Longitude = (float)(dLongitude + dLongToleranceDegrees);
                                                            oPolyNode.Latitude = (float)(dLatitude + dLatToleranceDegrees);
                                                            break;
                                                        case 2:
                                                            oPolyNode.Longitude = (float)(dLongitude - dLongToleranceDegrees);
                                                            oPolyNode.Latitude = (float)(dLatitude + dLatToleranceDegrees);
                                                            break;
                                                        case 3:
                                                            oPolyNode.Longitude = (float)(dLongitude - dLongToleranceDegrees);
                                                            oPolyNode.Latitude = (float)(dLatitude - dLatToleranceDegrees);
                                                            break;
                                                        default:
                                                            break;
                                                    }
                                                    myPoint.AddPointNode(oPolyNode);
                                                    iNumOfPointsAdded++;
                                                }
                                                #endregion
                                            }
                                            else
                                            {
                                                #region Add the nodes to the waypoint
                                                if (drSetPointNodes.Length == 1)
                                                {
                                                    oPolyNode = new PolygonNode();
                                                    oPolyNode.Latitude = Convert.ToSingle(dRow["Latitude"]);
                                                    oPolyNode.Longitude = Convert.ToSingle(dRow["Longitude"]);
                                                    oPolyNode.Radius = Convert.ToInt32(Math.Round(Convert.ToDouble(dRow["ToleranceNSMetres"]) / Convert.ToDouble(1.852), 0));
                                                    myPoint.AddPointNode(oPolyNode);
                                                    iNumOfPointsAdded++;
                                                }
                                                else
                                                {
                                                    #region Encode a polygon
                                                    for (int X = 0; X < drSetPointNodes.Length; X++)
                                                    {
                                                        oPolyNode = new PolygonNode();
                                                        oPolyNode.Latitude = Convert.ToSingle(drSetPointNodes[X]["Latitude"]);
                                                        oPolyNode.Longitude = Convert.ToSingle(drSetPointNodes[X]["Longitude"]);
                                                        myPoint.AddPointNode(oPolyNode);
                                                        iNumOfPointsAdded++;
                                                    }
                                                    #endregion
                                                }
                                                #endregion
                                            }
                                            spPacket.AddPoint(myPoint);
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Encode a normal WP
                                            myPoint = new GPSetPoint();
                                            if (dbVer >= 5.9)
                                                myPoint.cPointNumberOld = Convert.ToByte(iSetPointNumberCounter++);
                                            else
                                                myPoint.cPointNumberOld = (byte)Convert.ToInt32(dRow["SetPointNumber"]);

                                            myPoint.cFlagsOld = Convert.ToByte(Convert.ToInt32(dRow["Flags"]) & 0xFF);
                                            myPoint.fBinLat = Convert.ToSingle(dRow["Latitude"]);
                                            myPoint.fBinLong = Convert.ToSingle(dRow["Longitude"]);
                                            int coord = Convert.ToInt32(dRow["XTolerance"]) & 0xFFFF;
                                            if (coord > Int16.MaxValue)
                                            {
                                                coord = Int16.MaxValue;
                                            }
                                            myPoint.iLatTolerance = Convert.ToInt16(coord);
                                            coord = Convert.ToInt32(dRow["YTolerance"]) & 0xFFFF;
                                            if (coord > Int16.MaxValue)
                                            {
                                                coord = Int16.MaxValue;
                                            }
                                            myPoint.iLongTolerance = Convert.ToInt16(coord);
                                            myPoint.cDurationTime = Convert.ToByte(Convert.ToInt16(dRow["DurationTime"]) & 0xFF);
                                            myPoint.cMaxSpeedNmh = (byte)0x00;
                                            if (this.dbVer >= 5.9)
                                            {
                                                if (!dRow.IsNull("MaxSpeedNmH"))
                                                {
                                                    myPoint.cMaxSpeedNmh = GetFloorByteFromFloat(Convert.ToDouble(dRow["MaxSpeedNmh"]));
                                                    myPoint.cMaxSpeedNmhDecimalPlaces = GetByteFromFloatDecimalValue(Convert.ToDouble(dRow["MaxSpeedNmh"]));
                                                }
                                                else
                                                {
                                                    myPoint.cMaxSpeedNmh = 0;
                                                    myPoint.cMaxSpeedNmhDecimalPlaces = 0;
                                                }
                                                myPoint.uPointNumber = Convert.ToUInt16(Convert.ToInt32(dRow["SetPointNumber"]) & 0xFFFF);
                                                myPoint.uFlags = Convert.ToUInt16(Convert.ToInt32(dRow["Flags"]) & 0xFFFF);
                                            }
                                            else
                                            {
                                                myPoint.uPointNumber = 0;
                                                myPoint.uFlags = 0;
                                            }
                                            // Add it to the group, and set mNumPoints correctly:
                                            spPacket.AddPoint(myPoint);
                                            iNumOfPointsAdded++;
                                            #endregion
                                        }
                                    }
                                }

                                if (iNumOfPointsAdded == 0)
                                {
                                    #region If no points where added to the setpoint packet, add one dummy point.
                                    if (bIsPolygonWPGroup)
                                    {
                                        _log.Info("Couldn't locate any Polygon Set Points with Group ID " + Convert.ToString(iSetPointGroupID));
                                        myPoint = new GPSetPoint(4);
                                        myPoint.PolySideNodes = 0;
                                        oPolyNode = new PolygonNode();
                                        oPolyNode.Longitude = (float)-1;
                                        oPolyNode.Latitude = (float)-1;
                                        myPoint.AddPointNode(oPolyNode);
                                        oPolyNode = new PolygonNode();
                                        oPolyNode.Longitude = (float)-1;
                                        oPolyNode.Latitude = (float)-1;
                                        myPoint.AddPointNode(oPolyNode);
                                        oPolyNode = new PolygonNode();
                                        oPolyNode.Longitude = (float)-1;
                                        oPolyNode.Latitude = (float)-1;
                                        myPoint.AddPointNode(oPolyNode);
                                        oPolyNode = new PolygonNode();
                                        oPolyNode.Longitude = (float)-1;
                                        oPolyNode.Latitude = (float)-1;
                                        myPoint.AddPointNode(oPolyNode);
                                        spPacket.AddPoint(myPoint);
                                    }
                                    else
                                    {
                                        _log.Info("Couldn't locate any Set Points with Group ID " + Convert.ToString(iSetPointGroupID));
                                        myPoint = new GPSetPoint();
                                        // Populate it:
                                        myPoint.cPointNumberOld = 1;
                                        myPoint.cFlagsOld = (byte)0x00;
                                        myPoint.fBinLat = -1;
                                        myPoint.fBinLong = -1;
                                        myPoint.iLatTolerance = 1;
                                        myPoint.iLongTolerance = 1;
                                        myPoint.cDurationTime = (byte)0x00;
                                        myPoint.cMaxSpeedNmh = (byte)0x00;
                                        myPoint.uPointNumber = 1;
                                        myPoint.uFlags = (ushort)0x00;
                                        // Add it to the group, and set mNumPoints correctly:
                                        spPacket.AddPoint(myPoint);
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            else
                            {
                                #region If no points where added to the setpoint packet, add one dummy point.
                                if (bIsPolygonWPGroup)
                                {
                                    _log.Info("Couldn't locate any Polygon Set Points with Group ID " + Convert.ToString(iSetPointGroupID));
                                    myPoint = new GPSetPoint(4);
                                    myPoint.iSetPointID = 0;
                                    myPoint.PolySideNodes = 0;
                                    myPoint.fBinLat = -1;
                                    myPoint.fBinLong = -1;
                                    myPoint.iLatTolerance = 1;
                                    myPoint.iLongTolerance = 1;
                                    myPoint.cDurationTime = 0;
                                    myPoint.cMaxSpeedNmh = (byte)0x00;

                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1;
                                    oPolyNode.Latitude = (float)-1;
                                    myPoint.AddPointNode(oPolyNode);
                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1;
                                    oPolyNode.Latitude = (float)-1;
                                    myPoint.AddPointNode(oPolyNode);
                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1;
                                    oPolyNode.Latitude = (float)-1;
                                    myPoint.AddPointNode(oPolyNode);
                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1;
                                    oPolyNode.Latitude = (float)-1;
                                    myPoint.AddPointNode(oPolyNode);
                                    spPacket.AddPoint(myPoint);
                                }
                                else
                                {
                                    _log.Info("Couldn't locate any Set Points with Group ID " + Convert.ToString(iSetPointGroupID));
                                    for (int iPadPoint = 1; iPadPoint < 39; iPadPoint++)
                                    {
                                        myPoint = new GPSetPoint();
                                        float fLocation = Convert.ToSingle(-1) - (Convert.ToSingle(iPadPoint) / Convert.ToSingle(100));
                                        // Populate it:
                                        myPoint.cPointNumberOld = (byte)iPadPoint;
                                        myPoint.cFlagsOld = (byte)0x00;
                                        myPoint.fBinLat = fLocation;
                                        myPoint.fBinLong = fLocation;
                                        myPoint.iLatTolerance = 10;
                                        myPoint.iLongTolerance = 10;
                                        myPoint.cDurationTime = (byte)0x00;
                                        myPoint.cMaxSpeedNmh = (byte)0x00;
                                        myPoint.uPointNumber = (ushort)iPadPoint;
                                        myPoint.uFlags = (ushort)0x00;
                                        // Add it to the group, and set mNumPoints correctly:
                                        spPacket.AddPoint(myPoint);
                                    }
                                }
                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            // There was probably a DBNull in there.
                            return "Exception fetching SetPointGroup " + spPacket.cSetPointSetNumber + " v" +
                                spPacket.iSetPointVersion +
                                " Error : " + ex.Message + " Source : " + ex.Source + " for fleet " +
                                spPacket.cFleetId + ", unit " +
                                spPacket.iVehicleId;
                        }
                        #endregion
                        return null;
                    }
                    else
                    {
                        #region Unit side only Waypoint group.
                        try
                        {
                            #region Get the set point records for the given setpoint group ID
                            lock (oSetPointDataSetSync)
                            {
                                if (mSetPointDataSet != null)
                                {
                                    if (mSetPointDataSet.Tables.Count > 0)
                                    {
                                        if (mSetPointDataSet.Tables[0].Rows.Count > 0)
                                        {
                                            drSetPoints = mSetPointDataSet.Tables[0].Select("SetPointGroupID = " + Convert.ToString(iSetPointGroupID));
                                        }
                                    }
                                }
                            }
                            #endregion
                            if (drSetPoints != null && drSetPoints.Length > 0)
                            {
                                iSetPointNumberCounter = 1;
                                foreach (DataRow drSetPoint in drSetPoints)
                                {
                                    if (bIsPolygonWPGroup)
                                    {
                                        #region Encode a polygon WP
                                        iSetPointID = Convert.ToInt32(drSetPoint["ID"]);
                                        lock (oSetGroupDataSetSync)
                                            drSetPointNodes = (mSetPointNodesDataSet.Tables["SetPointNodes"].Select("SetPointID = " + iSetPointID, "Sequence"));

                                        if (drSetPointNodes.Length == 2 || drSetPointNodes.Length == 0)
                                            myPoint = new GPSetPoint(4);
                                        else
                                            myPoint = new GPSetPoint(drSetPointNodes.Length);
                                        myPoint.iSetPointID = iSetPointID;
                                        myPoint.PolySideNodes = 0;
                                        myPoint.fBinLat = Convert.ToSingle(drSetPoint["Latitude"]);
                                        myPoint.fBinLong = Convert.ToSingle(drSetPoint["Longitude"]);
                                        int coord = Convert.ToInt32(drSetPoint["XTolerance"]);
                                        if (coord > Int16.MaxValue)
                                        {
                                            coord = Int16.MaxValue;
                                        }
                                        myPoint.iLatTolerance = Convert.ToInt16(coord);
                                        coord = Convert.ToInt32(drSetPoint["YTolerance"]);
                                        if (coord > Int16.MaxValue)
                                        {
                                            coord = Int16.MaxValue;
                                        }
                                        myPoint.iLongTolerance = Convert.ToInt16(coord);
                                        myPoint.cDurationTime = Convert.ToByte(Convert.ToInt16(drSetPoint["DurationTime"]) & 0xFF);
                                        myPoint.cMaxSpeedNmh = (byte)0x00;
                                        if (this.dbVer >= 5.9)
                                        {
                                            if (!drSetPoint.IsNull("MaxSpeedNmH"))
                                            {
                                                myPoint.cMaxSpeedNmh = GetFloorByteFromFloat(Convert.ToDouble(drSetPoint["MaxSpeedNmh"]));
                                                myPoint.cMaxSpeedNmhDecimalPlaces = GetByteFromFloatDecimalValue(Convert.ToDouble(drSetPoint["MaxSpeedNmh"]));
                                            }
                                            else
                                            {
                                                myPoint.cMaxSpeedNmh = 0;
                                                myPoint.cMaxSpeedNmhDecimalPlaces = 0;
                                            }
                                            myPoint.uPointNumber = Convert.ToUInt16(Convert.ToInt32(drSetPoint["SetPointNumber"]) & 0xFFFF);
                                            myPoint.uFlags = Convert.ToUInt16(Convert.ToInt32(drSetPoint["Flags"]) & 0xFFFF);
                                        }
                                        else
                                        {
                                            myPoint.cMaxSpeedNmh = (byte)0x00;
                                            myPoint.uPointNumber = 0;
                                            myPoint.uFlags = 0;
                                        }
                                        if (drSetPointNodes.Length == 2 || drSetPointNodes.Length == 0)
                                        {
                                            #region Encode the xy tolerance waypoint as a polygon
                                            dLatitude = Convert.ToDouble(drSetPoint["Latitude"]);
                                            dLongitude = Convert.ToDouble(drSetPoint["Longitude"]);
                                            dLatToleranceDegrees = (((Convert.ToDouble(drSetPoint["ToleranceNSMetres"]))) / 1.852) / (double)60000;
                                            iLatLookup = (int)dLatitude;
                                            if (iLatLookup < 0)
                                                iLatLookup *= -1;
                                            dLongToleranceDegrees = (((Convert.ToDouble(drSetPoint["ToleranceEWMetres"]) / 1.852) / CosLatTable[iLatLookup]) / (double)60000);

                                            for (int i = 0; i < 4; i++)
                                            {
                                                oPolyNode = new PolygonNode();
                                                switch (i)
                                                {
                                                    case 0:
                                                        oPolyNode.Longitude = (float)(dLongitude + dLongToleranceDegrees);
                                                        oPolyNode.Latitude = (float)(dLatitude - dLatToleranceDegrees);
                                                        break;
                                                    case 1:
                                                        oPolyNode.Longitude = (float)(dLongitude + dLongToleranceDegrees);
                                                        oPolyNode.Latitude = (float)(dLatitude + dLatToleranceDegrees);
                                                        break;
                                                    case 2:
                                                        oPolyNode.Longitude = (float)(dLongitude - dLongToleranceDegrees);
                                                        oPolyNode.Latitude = (float)(dLatitude + dLatToleranceDegrees);
                                                        break;
                                                    case 3:
                                                        oPolyNode.Longitude = (float)(dLongitude - dLongToleranceDegrees);
                                                        oPolyNode.Latitude = (float)(dLatitude - dLatToleranceDegrees);
                                                        break;
                                                    default:
                                                        break;
                                                }
                                                myPoint.AddPointNode(oPolyNode);
                                                iNumOfPointsAdded++;
                                            }
                                            #endregion
                                        }
                                        else
                                        {
                                            #region Add the nodes to the waypoint
                                            if (drSetPointNodes.Length == 1)
                                            {
                                                oPolyNode = new PolygonNode();
                                                oPolyNode.Latitude = Convert.ToSingle(drSetPoint["Latitude"]);
                                                oPolyNode.Longitude = Convert.ToSingle(drSetPoint["Longitude"]);
                                                oPolyNode.Radius = Convert.ToInt32(Math.Round(Convert.ToDouble(drSetPoint["ToleranceNSMetres"]) / Convert.ToDouble(1.852), 0));
                                                myPoint.AddPointNode(oPolyNode);
                                                iNumOfPointsAdded++;
                                            }
                                            else
                                            {
                                                #region Encode a polygon
                                                for (int X = 0; X < drSetPointNodes.Length; X++)
                                                {
                                                    oPolyNode = new PolygonNode();
                                                    oPolyNode.Latitude = Convert.ToSingle(drSetPointNodes[X]["Latitude"]);
                                                    oPolyNode.Longitude = Convert.ToSingle(drSetPointNodes[X]["Longitude"]);
                                                    myPoint.AddPointNode(oPolyNode);
                                                    iNumOfPointsAdded++;
                                                }
                                                #endregion
                                            }
                                            #endregion
                                        }
                                        spPacket.AddPoint(myPoint);
                                        #endregion
                                    }
                                    else
                                    {
                                        #region If there are set points in the group, add them to the packet
                                        if ((dbVer < 5.9) || ((dbVer >= 5.9) && (bool)drSetPoint["Active"]))
                                        {
                                            myPoint = new GPSetPoint();
                                            myPoint.cPointNumberOld = Convert.ToByte(iSetPointNumberCounter++);
                                            myPoint.cFlagsOld = Convert.ToByte(Convert.ToInt32(drSetPoint["Flags"]) & 0xFF);
                                            myPoint.fBinLat = Convert.ToSingle(drSetPoint["Latitude"]);
                                            myPoint.fBinLong = Convert.ToSingle(drSetPoint["Longitude"]);
                                            //---------------------------------------------------------------------------------------
                                            //	POD : Due to a mistake in the client, the following mapping will now apply..
                                            //		XTolerance will be Latitude Tolerance
                                            //		YTolerance will be Longitude Tolerance
                                            //	This is to counteract an error in all clients.
                                            int coord = Convert.ToInt32(drSetPoint["XTolerance"]) & 0xFFFF;
                                            if (coord > Int16.MaxValue)
                                            {
                                                coord = Int16.MaxValue;
                                            }
                                            myPoint.iLatTolerance = Convert.ToInt16(coord);
                                            coord = Convert.ToInt32(drSetPoint["YTolerance"]) & 0xFFFF;
                                            if (coord > Int16.MaxValue)
                                            {
                                                coord = Int16.MaxValue;
                                            }
                                            myPoint.iLongTolerance = Convert.ToInt16(coord);
                                            //---------------------------------------------------------------------------------------
                                            myPoint.cDurationTime = Convert.ToByte(Convert.ToInt16(drSetPoint["DurationTime"]) & 0xFF);
                                            myPoint.cMaxSpeedNmh = (byte)0x00;
                                            if (this.dbVer >= 5.9)
                                            {
                                                if (!drSetPoint.IsNull("MaxSpeedNmH"))
                                                {
                                                    myPoint.cMaxSpeedNmh = GetFloorByteFromFloat(Convert.ToDouble(drSetPoint["MaxSpeedNmh"]));
                                                    myPoint.cMaxSpeedNmhDecimalPlaces = GetByteFromFloatDecimalValue(Convert.ToDouble(drSetPoint["MaxSpeedNmh"]));
                                                }
                                                else
                                                {
                                                    myPoint.cMaxSpeedNmh = 0;
                                                    myPoint.cMaxSpeedNmhDecimalPlaces = 0;
                                                }
                                                myPoint.uPointNumber = Convert.ToUInt16(Convert.ToInt32(drSetPoint["SetPointNumber"]) & 0xFFFF);
                                                myPoint.uFlags = Convert.ToUInt16(Convert.ToInt32(drSetPoint["Flags"]) & 0xFFFF);
                                            }
                                            else
                                            {
                                                myPoint.cMaxSpeedNmh = (byte)0x00;
                                                myPoint.uPointNumber = 0;
                                                myPoint.uFlags = 0;
                                            }
                                            // Add it to the group, and set mNumPoints correctly:
                                            spPacket.AddPoint(myPoint);
                                            iNumOfPointsAdded++;
                                        }
                                        #endregion
                                    }
                                }
                            }
                            else
                            {
                                iNumOfPointsAdded = 0;
                            }
                            if (iNumOfPointsAdded == 0)
                            {
                                #region If no points where added to the setpoint packet, add one dummy point.
                                if (bIsPolygonWPGroup)
                                {
                                    _log.Info("Couldn't locate any Polygon Set Points with Group ID " + Convert.ToString(iSetPointGroupID));
                                    myPoint = new GPSetPoint(4);

                                    myPoint.iSetPointID = 0;
                                    myPoint.PolySideNodes = 0;
                                    myPoint.fBinLat = -1;
                                    myPoint.fBinLong = -1;
                                    myPoint.iLatTolerance = 1;
                                    myPoint.iLongTolerance = 1;
                                    myPoint.cDurationTime = 0;
                                    myPoint.cMaxSpeedNmh = (byte)0x00;

                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1;
                                    oPolyNode.Latitude = (float)-1;
                                    myPoint.AddPointNode(oPolyNode);
                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1.00001;
                                    oPolyNode.Latitude = (float)-1;
                                    myPoint.AddPointNode(oPolyNode);
                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1.1;
                                    oPolyNode.Latitude = (float)-1.1;
                                    myPoint.AddPointNode(oPolyNode);
                                    oPolyNode = new PolygonNode();
                                    oPolyNode.Longitude = (float)-1;
                                    oPolyNode.Latitude = (float)-1.1;
                                    myPoint.AddPointNode(oPolyNode);
                                    spPacket.AddPoint(myPoint);
                                }
                                else
                                {
                                    _log.Info("Couldn't locate any Set Points with Group ID " + Convert.ToString(iSetPointGroupID));
                                    for (int iPadPoint = 1; iPadPoint < 39; iPadPoint++)
                                    {
                                        myPoint = new GPSetPoint();
                                        float fLocation = Convert.ToSingle(-1) - (Convert.ToSingle(iPadPoint) / Convert.ToSingle(100));
                                        // Populate it:
                                        myPoint.cPointNumberOld = (byte)iPadPoint;
                                        myPoint.cFlagsOld = (byte)0x00;
                                        myPoint.fBinLat = fLocation;
                                        myPoint.fBinLong = fLocation;
                                        myPoint.iLatTolerance = 10;
                                        myPoint.iLongTolerance = 10;
                                        myPoint.cDurationTime = (byte)0x00;
                                        myPoint.cMaxSpeedNmh = (byte)0x00;
                                        myPoint.uPointNumber = (ushort)iPadPoint;
                                        myPoint.uFlags = (ushort)0x00;
                                        // Add it to the group, and set mNumPoints correctly:
                                        spPacket.AddPoint(myPoint);
                                    }
                                }
                                #endregion
                            }
                        }
                        catch (Exception ex)
                        {
                            // There was probably a DBNull in there.
                            return "Exception fetching SetPointGroup " + spPacket.cSetPointSetNumber + " v" +
                                spPacket.iSetPointVersion +
                                " Error : " + ex.Message + " Source : " + ex.Source + " for fleet " +
                                spPacket.cFleetId + ", unit " +
                                spPacket.iVehicleId;
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSetPointGroup(ref ConfigNewSetPtGPPacket spPacket, ref int iSPGroupdID)", ex);
            }
            return null;
        }
        /// <summary>
        /// This method will be used to retrieve the waypoint name for the current setpointID sent by the unit
        /// </summary>
        /// <param name="int fleetID"></param>
        /// <param name="int setPointGroupNumber"></param>
        /// <param name="int setPointNumber"></param>
        /// <param name="MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails positionDetails"></param>
        /// <returns>string</returns>
        private string GetWPNameForID(int fleetID, decimal dUnitLat, decimal dUnitLong, int iSetPointID, MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails positionDetails, ref cWPItem oWPItem)
        {
            string result = null;
            int iSetPointGroupID = 0;
            int iSetPointNumber = 0;
            int iSetPointGroupNumber = 0;
            DataRow[] drSetPoints = null;
            DataRow[] drSetPointGroups = null;
            DataRow drResult = null;
            cWPItem oWPPos = null;
            try
            {
                #region Setup a blank cWPItem object
                oWPPos = new cWPItem();
                positionDetails = new PositionDetails();
                positionDetails.PlaceName = "";
                positionDetails.Distance = 0;
                positionDetails.Position = "";
                positionDetails.MapRef = "";
                positionDetails.SetPointGroupID = -1;
                positionDetails.SetPointGroupNumber = -1;
                positionDetails.SetPointID = -1;
                positionDetails.SetPointNumber = -1;
                positionDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                oWPPos.oPos = positionDetails;
                #endregion
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {
                    if (mSetPointDataSet != null)
                    {
                        if (mSetPointDataSet.Tables.Count > 0)
                        {
                            if (mSetPointDataSet.Tables[0].Rows.Count > 0)
                            {
                                drSetPoints = mSetPointDataSet.Tables[0].Select("ID = " + Convert.ToString(iSetPointID));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                drResult = null;
                if (drSetPoints != null)
                {
                    if (drSetPoints.Length > 0)
                    {
                        iSetPointGroupID = Convert.ToInt32(drSetPoints[0]["SetPointGroupID"]);
                        iSetPointNumber = Convert.ToInt32(drSetPoints[0]["SetPointNumber"]);
                        drResult = drSetPoints[0];
                    }
                    else
                    {
                        iSetPointID = 0;
                        iSetPointGroupID = 0;
                        iSetPointNumber = 0;
                    }
                }
                #endregion
                #region Populate the cWPItem object
                if (drResult != null)
                {
                    #region Get the data row for this fleet/SetPointNumber number
                    drSetPointGroups = null;
                    lock (oSetGroupDataSetSync)
                    {
                        if (mSetGroupDataSet != null)
                        {
                            if (mSetGroupDataSet.Tables.Count > 0)
                            {
                                if (mSetGroupDataSet.Tables[0].Rows.Count > 0)
                                {
                                    drSetPointGroups = mSetGroupDataSet.Tables[0].Select("ID = " + Convert.ToString(iSetPointGroupID));
                                }
                            }
                        }
                    }
                    #endregion
                    if (drSetPointGroups != null)
                    {
                        if (drSetPointGroups.Length > 0)
                        {
                            iSetPointGroupNumber = Convert.ToInt32(drSetPointGroups[0]["SetPointNumber"]);
                        }
                    }
                    oWPPos.dLatitude = Convert.ToDecimal(drResult["Latitude"]);
                    oWPPos.dLongitude = Convert.ToDecimal(drResult["Longitude"]);
                    oWPPos.iWP_Flags = Convert.ToInt32(drResult["Flags"]) | _setPointFlagsOrPattern;
                    oWPPos.MapRef = GetMelwaysRef(oWPPos.dLatitude, oWPPos.dLongitude);
                    oWPPos.Name = Convert.ToString(drResult["Name"]);
                    oWPPos.SetPointGroupID = iSetPointGroupID;
                    oWPPos.SetPointGroupNumber = iSetPointGroupNumber;
                    oWPPos.SetPointID = iSetPointID;
                    oWPPos.SetPointNumber = iSetPointNumber;
                    oWPPos.dMaxLatitude = Convert.ToDecimal(drResult["Latitude"]) + (Convert.ToDecimal(drResult["XTolerance"]) / 60000);
                    oWPPos.dMinLatitude = Convert.ToDecimal(drResult["Latitude"]) - (Convert.ToDecimal(drResult["XTolerance"]) / 60000);
                    oWPPos.dMaxLongitude = Convert.ToDecimal(drResult["Longitude"]) - (Convert.ToDecimal(drResult["YTolerance"]) / 60000);
                    oWPPos.dMinLongitude = Convert.ToDecimal(drResult["Longitude"]) - (Convert.ToDecimal(drResult["YTolerance"]) / 60000);
                    oWPPos.oPos = new PositionDetails();
                    oWPPos.oPos.PlaceName = Convert.ToString(drResult["Name"]);
                    oWPPos.oPos.Distance = GetPostionDistance(dUnitLat, dUnitLong, oWPPos.dLatitude, oWPPos.dLongitude);
                    oWPPos.oPos.Position = GetPostionString(dUnitLat, dUnitLong, oWPPos.dLatitude, oWPPos.dLongitude);
                    oWPPos.oPos.MapRef = oWPPos.MapRef;
                    oWPPos.oPos.SetPointGroupID = iSetPointGroupID;
                    oWPPos.oPos.SetPointGroupNumber = iSetPointGroupNumber;
                    oWPPos.oPos.SetPointID = iSetPointID;
                    oWPPos.oPos.SetPointNumber = iSetPointNumber;
                    if ((oWPPos.iWP_Flags & 64) == 64)
                    {
                        oWPPos.bIsSpecial1 = true;
                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.Special1;
                    }
                    else if ((oWPPos.iWP_Flags & 128) == 128)
                    {
                        oWPPos.bIsSpecial2 = true;
                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.Special2;
                    }
                    else if ((oWPPos.iWP_Flags & 256) == 256)
                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.NoGoZone;
                    else
                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.WayPoint;
                }
                #endregion
                oWPItem = oWPPos;
                positionDetails = new PositionDetails();
                positionDetails.PlaceName = oWPPos.oPos.PlaceName;
                positionDetails.Distance = oWPPos.oPos.Distance;
                positionDetails.Position = oWPPos.oPos.Position;
                positionDetails.MapRef = oWPPos.oPos.MapRef;
                positionDetails.SetPointGroupID = oWPPos.oPos.SetPointGroupID;
                positionDetails.SetPointGroupNumber = oWPPos.oPos.SetPointGroupNumber;
                positionDetails.SetPointID = oWPPos.oPos.SetPointID;
                positionDetails.SetPointNumber = oWPPos.oPos.SetPointNumber;
                positionDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                result = oWPPos.oPos.PlaceName;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetWPNameForID(int fleetID = " + Convert.ToString(fleetID) + ", int iSetPointID = " + Convert.ToString(iSetPointID) + ", MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails positionDetails)", ex);
            }
            return (result == null) ? "" : result;
        }

        /// <summary>
        /// This method will be used by the 5.9 code to retrieve the waypoint name for the current waypoint accoridng to the unit.
        /// </summary>
        /// <param name="int fleetID"></param>
        /// <param name="int setPointGroupNumber"></param>
        /// <param name="int setPointNumber"></param>
        /// <param name="MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails positionDetails"></param>
        /// <returns>string</returns>
        private string GetWPNameForNumber(int fleetID, decimal dUnitLat, decimal dUnitLong, int setPointGroupNumber, int setPointNumber, MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails positionDetails, ref cWPItem oWPItem)
        {
            string result = null;
            int iSetPointGroupID = 0;
            DataRow[] drSetPointGroups = null;
            DataRow[] drSetPoints = null;
            DataRow drResult = null;
            cWPItem oWPPos = null;
            try
            {
                #region Setup a blank cWPItem object
                oWPPos = new cWPItem();
                positionDetails = new PositionDetails();
                positionDetails.PlaceName = "";
                positionDetails.Distance = 0;
                positionDetails.Position = "";
                positionDetails.MapRef = "";
                positionDetails.SetPointGroupID = -1;
                positionDetails.SetPointGroupNumber = -1;
                positionDetails.SetPointID = -1;
                positionDetails.SetPointNumber = -1;
                positionDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                oWPPos.oPos = positionDetails;
                #endregion
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {
                    if (mSetGroupDataSet != null)
                    {
                        if (mSetGroupDataSet.Tables.Count > 0)
                        {
                            if (mSetGroupDataSet.Tables[0].Rows.Count > 0)
                            {
                                drSetPointGroups = mSetGroupDataSet.Tables[0].Select("FleetID = " + Convert.ToString(fleetID) + " AND SetPointNumber = " + Convert.ToString(setPointGroupNumber));
                            }
                        }
                    }
                }
                #endregion
                #region Read the results
                if (drSetPointGroups != null)
                {
                    if (drSetPointGroups.Length > 0)
                    {
                        iSetPointGroupID = Convert.ToInt32(drSetPointGroups[0]["ID"]);
                    }
                }
                #endregion
                if (iSetPointGroupID > 0)
                {
                    #region Get the data row for this fleet/SetPointNumber number
                    lock (oSetPointDataSetSync)
                    {
                        if (mSetPointDataSet != null)
                        {
                            if (mSetPointDataSet.Tables.Count > 0)
                            {
                                if (mSetPointDataSet.Tables[0].Rows.Count > 0)
                                {
                                    drSetPoints = mSetPointDataSet.Tables[0].Select("SetPointGroupID = " + Convert.ToString(iSetPointGroupID) + " AND SetPointNumber = " + Convert.ToString(setPointNumber));
                                }
                            }
                        }
                    }
                    #endregion
                    #region Read the results
                    if (drSetPoints != null)
                    {
                        if (drSetPoints.Length > 0)
                        {
                            drResult = drSetPoints[0];
                        }
                    }
                    #endregion
                    #region Populate the cWPItem object
                    if (drResult != null)
                    {
                        oWPPos.dLatitude = Convert.ToDecimal(drResult["Latitude"]);
                        oWPPos.dLongitude = Convert.ToDecimal(drResult["Longitude"]);
                        oWPPos.iWP_Flags = Convert.ToInt32(drResult["Flags"]) | _setPointFlagsOrPattern;
                        oWPPos.MapRef = GetMelwaysRef(oWPPos.dLatitude, oWPPos.dLongitude);
                        oWPPos.Name = Convert.ToString(drResult["Name"]);
                        oWPPos.SetPointGroupID = iSetPointGroupID;
                        oWPPos.SetPointGroupNumber = setPointGroupNumber;
                        oWPPos.SetPointID = Convert.ToInt32(drResult["ID"]);
                        oWPPos.SetPointNumber = Convert.ToInt32(drResult["SetPointNumber"]);
                        oWPPos.dMaxLatitude = Convert.ToDecimal(drResult["Latitude"]) + (Convert.ToDecimal(drResult["XTolerance"]) / 60000);
                        oWPPos.dMinLatitude = Convert.ToDecimal(drResult["Latitude"]) - (Convert.ToDecimal(drResult["XTolerance"]) / 60000);
                        oWPPos.dMaxLongitude = Convert.ToDecimal(drResult["Longitude"]) - (Convert.ToDecimal(drResult["YTolerance"]) / 60000);
                        oWPPos.dMinLongitude = Convert.ToDecimal(drResult["Longitude"]) - (Convert.ToDecimal(drResult["YTolerance"]) / 60000);
                        oWPPos.oPos.PlaceName = Convert.ToString(drResult["Name"]);
                        oWPPos.oPos.Distance = GetPostionDistance(dUnitLat, dUnitLong, oWPPos.dLatitude, oWPPos.dLongitude);
                        oWPPos.oPos.Position = GetPostionString(dUnitLat, dUnitLong, oWPPos.dLatitude, oWPPos.dLongitude);
                        oWPPos.oPos.MapRef = oWPPos.MapRef;
                        oWPPos.oPos.SetPointGroupID = iSetPointGroupID;
                        oWPPos.oPos.SetPointGroupNumber = setPointGroupNumber;
                        oWPPos.oPos.SetPointID = Convert.ToInt32(drResult["ID"]);
                        oWPPos.oPos.SetPointNumber = Convert.ToInt32(drResult["SetPointNumber"]);
                        if ((oWPPos.iWP_Flags & 64) == 64)
                        {
                            oWPPos.bIsSpecial1 = true;
                            oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.Special1;
                        }
                        else if ((oWPPos.iWP_Flags & 128) == 128)
                        {
                            oWPPos.bIsSpecial2 = true;
                            oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.Special2;
                        }
                        else if ((oWPPos.iWP_Flags & 256) == 256)
                            oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.NoGoZone;
                        else
                            oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.WayPoint;
                    }
                    #endregion
                }
                oWPItem = oWPPos;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetWPNameForNumber(int fleetID = " + Convert.ToString(fleetID) + ", int setPointGroupNumber = " + Convert.ToString(setPointGroupNumber) + ", int setPointNumber = " + Convert.ToString(setPointNumber) + ", MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails positionDetails)", ex);
            }
            return (result == null) ? "" : result;
        }
        /// <summary>
        /// This method will be used to retrieve the waypoint name for a fleet vehicle and location.
        /// </summary>
        /// <param name="int fleetID"></param>
        /// <param name="uint unitID"></param>
        /// <param name="decimal latitude"></param>
        /// <param name="decimal longitude"></param>
        /// <param name="MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails"></param>
        /// <returns>string</returns>
        private string GetWPName(int fleetID, uint unitID, decimal latitude, decimal longitude, MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails oPosDetails)
        {
            int iSetPointGroupID = 0;
            int iSetPointGroupNumber = 0;
            ArrayList oWPItems = null;
            WayPoint oWPItem = null;
            string sRet = "";
            try
            {
                #region Setup the default return value
                oPosDetails.PlaceName = "";
                oPosDetails.Distance = 0;
                oPosDetails.Position = "";
                oPosDetails.MapRef = "";
                oPosDetails.SetPointGroupID = -1;
                oPosDetails.SetPointGroupNumber = -1;
                oPosDetails.SetPointID = -1;
                oPosDetails.SetPointNumber = -1;
                oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                #endregion

                if (oWPLookupTree != null)
                {
                    #region Lookup the WPs the unit is currently at, returning the smallest WP the unit is currently within.
                    iSetPointGroupID = GetSetPointGroupIDForVehicle(fleetID, unitID);
                    iSetPointGroupNumber = GetSetPointNumberFromID(fleetID, iSetPointGroupID);
                    lock (oWPLookupTreeSync)
                        oWPItems = oWPLookupTree.Query(iSetPointGroupID, Convert.ToDouble(latitude), Convert.ToDouble(longitude), false);
                    if (oWPItems.Count > 0)
                    {
                        oWPItem = (WayPoint)oWPItems[0];
                        sRet = oWPItem.sPlaceName;
                        oPosDetails.PlaceName = oWPItem.sPlaceName;
                        oPosDetails.Distance = Convert.ToDecimal(oWPItem.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude)));
                        oPosDetails.Position = oWPItem.GetDirectionFromPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                        oPosDetails.MapRef = oWPItem.sMapRef;
                        oPosDetails.SetPointGroupID = iSetPointGroupID;
                        oPosDetails.SetPointGroupNumber = iSetPointGroupNumber;
                        oPosDetails.SetPointID = oWPItem.iID;
                        oPosDetails.SetPointNumber = oWPItem.iSetPointNumber;

                        if ((oWPItem.iFlags & 64) == 64)
                            oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.Special1;
                        else if ((oWPItem.iFlags & 128) == 128)
                            oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.Special2;
                        else if ((oWPItem.iFlags & 256) == 256)
                            oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.NoGoZone;
                        else
                            oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.WayPoint;
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetWPName(int fleetID = " + Convert.ToString(fleetID) + ", uint unitID = " + Convert.ToString(unitID) + ", decimal latitude = " + Convert.ToString(latitude) + ", decimal longitude = " + Convert.ToString(longitude) + ", MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails)", ex);
            }
            return sRet;
        }
        /// <summary>
        /// This method will be used to get a list of special 1 server side way point based on a vehicle identity and location
        /// This method uses the WP lookup tree as the data source.
        /// </summary>
        /// <param name="int fleetID"></param>
        /// <param name="uint unitID"></param>
        /// <param name="decimal latitude"></param>
        /// <param name="decimal longitude"></param>
        /// <param name="MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails"></param>
        /// <returns>string</returns>
        private string GetClosestSpecial1WPName(int fleetID, uint unitID, decimal latitude, decimal longitude, MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails oPosDetails)
        {
            int iSetPointGroupID = 0;
            int iSetPointGroupNumber = 0;
            ArrayList oWPItems = null;
            WayPoint oWPItem = null;
            string sRet = "";
            try
            {
                #region Setup the default return value
                oPosDetails.PlaceName = "";
                oPosDetails.Distance = 0;
                oPosDetails.Position = "";
                oPosDetails.MapRef = "";
                oPosDetails.SetPointGroupID = -1;
                oPosDetails.SetPointGroupNumber = -1;
                oPosDetails.SetPointID = -1;
                oPosDetails.SetPointNumber = -1;
                oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                #endregion

                if (oWPLookupTree != null)
                {
                    #region Lookup the WPs the unit is currently at, returning the smallest WP the unit is currently within.
                    iSetPointGroupID = GetSetPointGroupIDForVehicle(fleetID, unitID);
                    iSetPointGroupNumber = GetSetPointNumberFromID(fleetID, iSetPointGroupID);
                    lock (oWPLookupTreeSync)
                        oWPItems = oWPLookupTree.Query(iSetPointGroupID, Convert.ToDouble(latitude), Convert.ToDouble(longitude), true);
                    if (oWPItems.Count > 0)
                    {
                        for (int X = 0; X < oWPItems.Count; X++)
                        {
                            oWPItem = (WayPoint)oWPItems[X];
                            if ((oWPItem.iFlags & 64) == 64)
                            {
                                sRet = oWPItem.sPlaceName;
                                oPosDetails.PlaceName = oWPItem.sPlaceName;
                                oPosDetails.Distance = Convert.ToDecimal(oWPItem.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude)));
                                oPosDetails.Position = oWPItem.GetDirectionFromPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                                oPosDetails.MapRef = oWPItem.sMapRef;
                                oPosDetails.SetPointGroupID = iSetPointGroupID;
                                oPosDetails.SetPointGroupNumber = iSetPointGroupNumber;
                                oPosDetails.SetPointID = oWPItem.iID;
                                oPosDetails.SetPointNumber = oWPItem.iSetPointNumber;
                                oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.Special1;
                                break;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetClosestSpecial1WPName(int fleetID = " + Convert.ToString(fleetID) + ", uint unitID = " + Convert.ToString(unitID) + ", decimal latitude = " + Convert.ToString(latitude) + ", decimal longitude = " + Convert.ToString(longitude) + ", MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails)", ex);
            }
            return sRet;
        }
        /// <summary>
        /// This method will be used to get a list of special 2 server side way point based on a vehicle identity and location
        /// This method uses the WP lookup tree as the data source.
        /// </summary>
        /// <param name="int fleetID"></param>
        /// <param name="uint unitID"></param>
        /// <param name="decimal latitude"></param>
        /// <param name="decimal longitude"></param>
        /// <param name="MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails"></param>
        /// <returns>string</returns>
        private string GetClosestSpecial2WPName(int fleetID, uint unitID, decimal latitude, decimal longitude, MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails oPosDetails)
        {
            int iSetPointGroupID = 0;
            int iSetPointGroupNumber = 0;
            ArrayList oWPItems = null;
            WayPoint oWPItem = null;
            string sRet = "";
            try
            {
                #region Setup the default return value
                oPosDetails.PlaceName = "";
                oPosDetails.Distance = 0;
                oPosDetails.Position = "";
                oPosDetails.MapRef = "";
                oPosDetails.SetPointGroupID = -1;
                oPosDetails.SetPointGroupNumber = -1;
                oPosDetails.SetPointID = -1;
                oPosDetails.SetPointNumber = -1;
                oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                #endregion

                if (oWPLookupTree != null)
                {
                    #region Lookup the WPs the unit is currently at, returning the smallest WP the unit is currently within.
                    iSetPointGroupID = GetSetPointGroupIDForVehicle(fleetID, unitID);
                    iSetPointGroupNumber = GetSetPointNumberFromID(fleetID, iSetPointGroupID);
                    lock (oWPLookupTreeSync)
                        oWPItems = oWPLookupTree.Query(iSetPointGroupID, Convert.ToDouble(latitude), Convert.ToDouble(longitude), true);
                    if (oWPItems.Count > 0)
                    {
                        for (int X = 0; X < oWPItems.Count; X++)
                        {
                            oWPItem = (WayPoint)oWPItems[X];
                            if ((oWPItem.iFlags & 128) == 128)
                            {
                                sRet = oWPItem.sPlaceName;
                                oPosDetails.PlaceName = oWPItem.sPlaceName;
                                oPosDetails.Distance = Convert.ToDecimal(oWPItem.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude)));
                                oPosDetails.Position = oWPItem.GetDirectionFromPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                                oPosDetails.MapRef = oWPItem.sMapRef;
                                oPosDetails.SetPointGroupID = iSetPointGroupID;
                                oPosDetails.SetPointGroupNumber = iSetPointGroupNumber;
                                oPosDetails.SetPointID = oWPItem.iID;
                                oPosDetails.SetPointNumber = oWPItem.iSetPointNumber;
                                oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.Special2;
                                break;
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetClosestSpecial2WPName(int fleetID = " + Convert.ToString(fleetID) + ", uint unitID = " + Convert.ToString(unitID) + ", decimal latitude = " + Convert.ToString(latitude) + ", decimal longitude = " + Convert.ToString(longitude) + ", MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails)", ex);
            }
            return sRet;
        }
        /// <summary>
        /// This method will be used to check if the given setpoint group contains server side WPs
        /// </summary>
        /// <param name="int iWPGroupID"></param>
        /// <returns>bool</returns>
        public bool IsServerSideWP(int iWPGroupID)
        {
            bool bRet = false;
            try
            {
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {

                    if (mServerSideWPGroupList != null)
                        if (mServerSideWPGroupList.Contains(iWPGroupID))
                            bRet = true;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "IsServerSideWP(int iWPGroupID = " + Convert.ToString(iWPGroupID) + ")", ex);
            }
            return bRet;
        }
        /// <summary>
        /// This method will be used to check if the given setpoint group contains polygon WPs
        /// </summary>
        /// <param name="int iWPGroupID"></param>
        /// <returns>bool</returns>
        public bool IsPolygonWPGroup(int iWPGroupID)
        {

            bool bRet = false;
            try
            {
                #region Get the data row for this fleet/SetPointNumber number
                lock (oSetGroupDataSetSync)
                {
                    if (mPolygonWPGroupList != null)
                        if (mPolygonWPGroupList.Contains(iWPGroupID))
                            bRet = true;
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "IsPolygonWPGroup(int iWPGroupID = " + Convert.ToString(iWPGroupID) + ")", ex);
            }
            return bRet;
        }
        /// <summary>
        /// This method will be used to check if the given fleet/vehicle is current assigned to a set point group that contains server side WPs
        /// </summary>
        /// <param name="string sFleetID"></param>
        /// <param name="string sVehicleID"></param>
        /// <returns>bool</returns>
        public bool IsServerSideWPUnit(int fleetID, int vehicleID)
        {
            try
            {
                //check if unit is server side only before server side waypoint groups
                Tuple<int, int> t = new Tuple<int, int>(fleetID, vehicleID);
                long lKey = GetVehicleHashKey(fleetID, vehicleID);
                lock (oSetGroupDataSetSync)
                {
                    if (_vehiclesWpServerSideOnly.Contains(t))
                    {
                        return true;
                    }
                    else if (oUnitToSSWPGroup != null && oUnitToSSWPGroup.ContainsKey(lKey))
                    {
                        return true;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "IsServerSideWPUnit(string sFleetID = " + fleetID + ", string sVehicleID = " + vehicleID + ")", ex);
            }
            return false;
        }
        public ArrayList GetServerSideWaypoint(int fleetId, int vehicleId, decimal latitude, decimal longitude, ref PositionDetails oDefaultLocation)
        {
            int iSetPointGroupID = 0;
            int iSetPointGroupNumber = 0;
            ArrayList oWPItems = null;
            WayPoint oWPItem = null;
            WayPoint oDefWP = null;
            cWPItem oWPPos = null;
            ArrayList oRet = null;
            ArrayList oUnitSideWPs = null;
            double dTemp = 0;
            double dSize = double.MaxValue;
            double dDistance = double.MaxValue;

            try
            {
                oRet = new ArrayList();
                if (oWPLookupTree != null)
                {
                    #region Lookup the WPs the unit is currently at, returning a list of all the WPs the unit is currently within.
                    iSetPointGroupID = GetSetPointGroupIDForVehicle(fleetId, (uint)vehicleId);
                    iSetPointGroupNumber = GetSetPointNumberFromID(fleetId, iSetPointGroupID);
                    Tuple<int, int> t = new Tuple<int, int>(fleetId, vehicleId);

                    lock (oWPLookupTreeSync)
                        oWPItems = oWPLookupTree.Query(iSetPointGroupID, Convert.ToDouble(latitude), Convert.ToDouble(longitude), true);
                    lock (oSetPointDataSetSync)
                    {
                        if (_vehiclesWpServerSideOnly.Contains(t))
                        {
                            oUnitSideWPs = new ArrayList();
                        }
                        else if (mWPGroupToUnitSideWPs.ContainsKey(iSetPointGroupID))
                        {
                            oUnitSideWPs = (ArrayList)mWPGroupToUnitSideWPs[iSetPointGroupID];
                        }
                    }

                    #endregion
                    #region Support for array of WayPoint objects
                    if (oWPItems.Count > 0)
                    {
                        for (int X = 0; X < oWPItems.Count; X++)
                        {
                            oWPItem = (WayPoint)oWPItems[X];
                            // Only add server side WPs to the result list.
                            if (oUnitSideWPs != null)
                            {
                                if (!oUnitSideWPs.Contains(oWPItem.iID))
                                {
                                    #region Find the default WP
                                    if (oDefWP == null)
                                    {
                                        oDefWP = oWPItem;
                                        dSize = oWPItem.oTestArea.AreaSize;
                                    }
                                    else
                                    {
                                        dTemp = oWPItem.oTestArea.AreaSize;
                                        if (dTemp < dSize)
                                        {
                                            oDefWP = oWPItem;
                                            dSize = dTemp;
                                        }
                                        else if (dTemp == dSize)
                                        {
                                            #region If this point is the same size as the current default, find which one we are closer to the centre of.
                                            dDistance = oDefWP.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                                            dTemp = oWPItem.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                                            if (dTemp < dDistance)
                                                oDefWP = oWPItem;
                                            #endregion
                                        }
                                    }
                                    #endregion
                                    oWPPos = new cWPItem();
                                    #region Populate the cWPItem object
                                    oWPPos.dLatitude = Convert.ToDecimal(oWPItem.dLat);
                                    oWPPos.dLongitude = Convert.ToDecimal(oWPItem.dLong);
                                    oWPPos.iWP_Flags = oWPItem.iFlags;
                                    oWPPos.MapRef = oWPItem.sMapRef;
                                    oWPPos.Name = oWPItem.sPlaceName;
                                    oWPPos.SetPointGroupID = iSetPointGroupID;
                                    oWPPos.SetPointGroupNumber = iSetPointGroupNumber;
                                    oWPPos.SetPointID = oWPItem.iID;
                                    oWPPos.SetPointNumber = oWPItem.iSetPointNumber;
                                    oWPPos.dMaxLatitude = Convert.ToDecimal(oWPItem.oTestArea.dMaxLat);
                                    oWPPos.dMinLatitude = Convert.ToDecimal(oWPItem.oTestArea.dMinLat);
                                    oWPPos.dMaxLongitude = Convert.ToDecimal(oWPItem.oTestArea.dMaxLong);
                                    oWPPos.dMinLongitude = Convert.ToDecimal(oWPItem.oTestArea.dMinLong);

                                    #region Populate cWPItem.PositionDetails object
                                    oWPPos.oPos = new PositionDetails();
                                    oWPPos.oPos.PlaceName = oWPItem.sPlaceName;
                                    oWPPos.oPos.Distance = Convert.ToDecimal(oWPItem.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude)));
                                    oWPPos.oPos.Position = oWPItem.GetDirectionFromPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                                    oWPPos.oPos.MapRef = oWPItem.sMapRef;
                                    oWPPos.oPos.SetPointGroupID = iSetPointGroupID;
                                    oWPPos.oPos.SetPointGroupNumber = iSetPointGroupNumber;
                                    oWPPos.oPos.SetPointID = oWPItem.iID;
                                    oWPPos.oPos.SetPointNumber = oWPItem.iSetPointNumber;
                                    #endregion
                                    #region Populate the WP type vars
                                    if ((oWPItem.iFlags & 64) == 64)
                                    {
                                        oWPPos.bIsSpecial1 = true;
                                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.Special1;
                                    }
                                    else if ((oWPItem.iFlags & 128) == 128)
                                    {
                                        oWPPos.bIsSpecial2 = true;
                                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.Special2;
                                    }
                                    else if ((oWPItem.iFlags & 256) == 256)
                                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.NoGoZone;
                                    else
                                        oWPPos.oPos.PositionType = DatabaseAccess.PositionDetails.Category.WayPoint;
                                    #endregion
                                    #endregion
                                    oRet.Add(oWPPos);
                                }
                            }
                        }
                    }
                    #endregion
                    #region Populate the oDefaultLocation object
                    if (oDefWP != null)
                    {
                        #region Populate cWPItem.PositionDetails object
                        oDefaultLocation = new PositionDetails();
                        oDefaultLocation.PlaceName = oDefWP.sPlaceName;
                        oDefaultLocation.Distance = Convert.ToDecimal(oDefWP.GetDistanceToPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude)));
                        oDefaultLocation.Position = oDefWP.GetDirectionFromPoint(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                        oDefaultLocation.MapRef = oDefWP.sMapRef;
                        oDefaultLocation.SetPointGroupID = iSetPointGroupID;
                        oDefaultLocation.SetPointGroupNumber = iSetPointGroupNumber;
                        oDefaultLocation.SetPointID = oDefWP.iID;
                        oDefaultLocation.SetPointNumber = oDefWP.iSetPointNumber;
                        #endregion
                        #region Populate the WP type vars
                        if ((oDefWP.iFlags & 64) == 64)
                        {
                            oDefaultLocation.PositionType = DatabaseAccess.PositionDetails.Category.Special1;
                        }
                        else if ((oDefWP.iFlags & 128) == 128)
                        {
                            oDefaultLocation.PositionType = DatabaseAccess.PositionDetails.Category.Special2;
                        }
                        else if ((oDefWP.iFlags & 256) == 256)
                            oDefaultLocation.PositionType = DatabaseAccess.PositionDetails.Category.NoGoZone;
                        else
                            oDefaultLocation.PositionType = DatabaseAccess.PositionDetails.Category.WayPoint;
                        #endregion
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetServerSideWaypoint(int fleetId = " + fleetId + ", int vehicleId = " + vehicleId + ", decimal latitude = " + Convert.ToString(latitude) + ", decimal longitude = " + Convert.ToString(longitude) + ", byte bSpeed)", ex);
            }
            return oRet;
        }

        private GBVariablePacketExtended59 GetExtendedDetails(object packet)
        {
            GBVariablePacketExtended59 ret = null;
            try
            {
                if (packet is RoutePtSetPtGPPacket)
                {
                    ret = ((RoutePtSetPtGPPacket)packet).mExtendedVariableDetails;
                }
                else if (packet is RoutingModuleGPPacket)
                {
                    ret = ((RoutingModuleGPPacket)packet).mExtendedVariableDetails;
                }
                else if (packet is ConfigCustomerInfoGPPacket || packet is ConfigDriverPointsGPPacket
               || packet is ConfigFileGPPacket || packet is ConfigNetworkInfoGPPacket
               || packet is ConfigNewRoutePtGPPacket || packet is ConfigNewSetPtGPPacket
               || packet is ConfigRequestGPPacket || packet is ConfigRouteUpdateGPPacket
               || packet is ConfigScheduleClearGPPacket || packet is ConfigScheduleGPPacket
               || packet is ConfigScheduleInfoGPPacket || packet is ConfigTotalGPPacket
               || packet is ConfigWatchList || packet is DriverPointsConfigError
               || packet is DriverPointsRuleRequest
               || packet is DriverPointsServerTrackingRuleBroken || packet is FirmwareDriverLoginReply
               || packet is GeneralGPPacket || packet is GeneralOutputGPPacket
               || packet is GPSendScriptFile || packet is MailMessagePacket
               || packet is ProgramDownloadPacket || packet is ProgramVerifyPacket
               || packet is UserAckForUnitAlarm)
                {
                    ret = ((GeneralGPPacket)packet).mExtendedVariableDetails;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetExtendedDetails(object packet)", ex);
            }
            return ret;
        }
        private object[] GetFleetVehicleMsgTypeReportTimeLatLong(object packet)
        {
            object[] ret = new object[6];
            try
            {
                ret[0] = 0;
                ret[1] = 0;
                ret[2] = 0;
                ret[3] = null;
                ret[4] = (decimal)0;
                ret[5] = (decimal)0;
                if (packet is RoutePtSetPtGPPacket)
                {
                    ret[0] = ((RoutePtSetPtGPPacket)packet).cFleetId;
                    ret[1] = ((RoutePtSetPtGPPacket)packet).iVehicleId;
                    ret[2] = ((RoutePtSetPtGPPacket)packet).cMsgType;
                    ret[3] = ((RoutePtSetPtGPPacket)packet).mCurrentClock.ToDateTime();
                    if (((RoutePtSetPtGPPacket)packet).mFix != null)
                    {
                        ret[4] = ((RoutePtSetPtGPPacket)packet).mFix.dLatitude;
                        ret[5] = ((RoutePtSetPtGPPacket)packet).mFix.dLongitude;
                    }
                }
                else if (packet is RoutingModuleGPPacket)
                {
                    ret[0] = ((RoutingModuleGPPacket)packet).cFleetId;
                    ret[1] = ((RoutingModuleGPPacket)packet).iVehicleId;
                    ret[2] = ((RoutingModuleGPPacket)packet).cMsgType;
                    ret[3] = ((RoutingModuleGPPacket)packet).mCurrentClock.ToDateTime();
                    if (((RoutingModuleGPPacket)packet).mFix != null)
                    {
                        ret[4] = ((RoutingModuleGPPacket)packet).mFix.dLatitude;
                        ret[5] = ((RoutingModuleGPPacket)packet).mFix.dLongitude;
                    }
                }
                else if (packet is GeneralGPPacket)
                {
                    ret[0] = ((GeneralGPPacket)packet).cFleetId;
                    ret[1] = ((GeneralGPPacket)packet).iVehicleId;
                    ret[2] = ((GeneralGPPacket)packet).cMsgType;
                    ret[3] = ((GeneralGPPacket)packet).mCurrentClock.ToDateTime();
                    if (((GeneralGPPacket)packet).mFix != null)
                    {
                        ret[4] = ((GeneralGPPacket)packet).mFix.dLatitude;
                        ret[5] = ((GeneralGPPacket)packet).mFix.dLongitude;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetFleetVehicleMsgTypeReportTime(object packet)", ex);
            }
            return ret;
        }
        private bool IsPacketFromFlash(object packet)
        {
            bool ret = false;
            try
            {
                if (packet is RoutePtSetPtGPPacket)
                {
                    ret = ((RoutePtSetPtGPPacket)packet).bArchivalData;
                }
                else if (packet is RoutingModuleGPPacket)
                {
                    ret = ((RoutingModuleGPPacket)packet).bArchivalData;
                }
                else if (packet is GeneralGPPacket)
                {
                    ret = ((GeneralGPPacket)packet).bArchivalData;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "IsPacketFromFlash(object packet)", ex);
            }
            return ret;
        }

        private void SetStateFlagsOnCurrentPacket(object packet, Vehicle v)
        {
            //try
            //{
            //    if (packet is RoutePtSetPtGPPacket)
            //    {
            //    }
            //    else if (packet is RoutingModuleGPPacket)
            //    {
            //    }
            //    else if (packet is GeneralGPPacket)
            //    {
            //    }
            //}
            //catch (System.Exception ex)
            //{
            //    _log.Error(sClassName + "SetStateFlagsOnCurrentPacket(object packet, UnitState unitState)", ex);
            //}
        }
        /// <summary>
        /// This method will be used to get the position details for a vehicle based on the vehicles identity and the current location of the vehicle.
        /// </summary>
        /// <param name="Vehicle v"></param>
        /// <param name="object oGPPacket"></param>
        /// <param name="decimal latitude"></param>
        /// <param name="decimal longitude"></param>
        /// <returns>MTData.Transport.GatewayListener.Database_Access.PositionDetails</returns>
        public MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails GetPositionForVehicle(Vehicle v, object oGPPacket, decimal latitude, decimal longitude)
        {
            GBVariablePacketExtended59 mExtendedVariableDetails = null;
            byte bMsgType = 0;
            bool bIsFlashPacket = false;
            bool bHasFlashToUnload = false;
            uint iRouteScheduleID = 0;
            int iCheckPointIndex = 0;
            string sWPName = "";
            string sRouteCheckPointName = "";
            string sResult = "";
            int fleetId = 0;
            int vehicleId = 0;
            decimal oFlashLatitude = 0;
            decimal oFlashLongitude = 0;
            PositionDetails oVehiclePosition = null;
            PositionDetails oRoutePosResult = null;
            ArrayList oCurrentWPList = new ArrayList();
            ArrayList oCheckCurrentWPList = new ArrayList();
            ArrayList oDepartedWPList = new ArrayList();
            ArrayList oFlashWayPointIDs = null;
            cWPItem oWPItem = null;
            cWPItem oCurItem = null;
            cWPItem oVehItem = null;
            PositionDetails oDefaultLocation = new PositionDetails();
            DateTime dtGPSTime = DateTime.MinValue;
            DateTime dtDeviceTime = DateTime.MinValue;
            MTData.Transport.Gateway.Packet.GeneralGPPacket oGP = null;
            MTData.Transport.Gateway.Packet.GeneralGPPacket oLiveUpdatePacket = null;
            PositionDetails oRet = null;

            try
            {
                if (dbVer >= 5.9)
                {
                    #region Read the message type and extended details from the packet
                    if (oGPPacket is MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)
                    {
                        bMsgType = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).cMsgType;
                        fleetId = (int)((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).cFleetId;
                        vehicleId = (int)((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).iVehicleId;
                        mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mExtendedVariableDetails;
                        bIsFlashPacket = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).bArchivalData;
                        bHasFlashToUnload = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket)._receivedFlashAvailable;

                        GatewayProtocolPacket oGPP = (GatewayProtocolPacket)oGPPacket;
                        oGP = new GeneralGPPacket(oGPP, _serverTime_DateFormat);
                        oGP.mCurrentClock = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mCurrentClock;
                        oGP.mFixClock = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mFixClock;
                        oGP.mFix = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mFix;
                        oGP.mStatus = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mStatus;
                        oGP.mDistance = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mDistance;
                        oGP.mExtendedValues = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mExtendedValues;
                        oGP.mEngineData = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mEngineData;
                        oGP.mTransportExtraDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mTransportExtraDetails;
                        oGP.mTrailerTrack = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mTrailerTrack;
                        oGP.mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutePtSetPtGPPacket)oGPPacket).mExtendedVariableDetails;
                    }
                    else if (oGPPacket is MTData.Transport.Gateway.Packet.GeneralGPPacket)
                    {
                        bMsgType = ((MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket).cMsgType;
                        fleetId = (int)((MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket).cFleetId;
                        vehicleId = (int)((MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket).iVehicleId;
                        mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket).mExtendedVariableDetails;
                        bIsFlashPacket = ((MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket).bArchivalData;
                        bHasFlashToUnload = ((MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket)._receivedFlashAvailable;
                        oGP = (MTData.Transport.Gateway.Packet.GeneralGPPacket)oGPPacket;
                    }
                    else if (oGPPacket is MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)
                    {
                        bMsgType = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).cMsgType;
                        fleetId = (int)((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).cFleetId;
                        vehicleId = (int)((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).iVehicleId;
                        mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mExtendedVariableDetails;
                        bIsFlashPacket = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).bArchivalData;
                        bHasFlashToUnload = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket)._receivedFlashAvailable;

                        GatewayProtocolPacket oGPP = (GatewayProtocolPacket)oGPPacket;
                        oGP = new GeneralGPPacket(oGPP, _serverTime_DateFormat);
                        oGP.mCurrentClock = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mCurrentClock;
                        oGP.mFixClock = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mFixClock;
                        oGP.mFix = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mFix;
                        oGP.mStatus = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mStatus;
                        oGP.mDistance = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mDistance;
                        oGP.mExtendedValues = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mExtendedValues;
                        oGP.mEngineData = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mEngineData;
                        oGP.mTransportExtraDetails = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mTransportExtraDetails;
                        oGP.mTrailerTrack = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mTrailerTrack;
                        oGP.mExtendedVariableDetails = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).mExtendedVariableDetails;
                    }
                    else if (oGPPacket is GPSHistoryGPPacket)
                    {
                        GPSHistoryGPPacket history = oGPPacket as GPSHistoryGPPacket;
                        bMsgType = history.cMsgType;
                        fleetId = (int)history.cFleetId;
                        vehicleId = (int)history.iVehicleId;
                        mExtendedVariableDetails = new GBVariablePacketExtended59();
                        bIsFlashPacket = history.bArchivalData;
                        bHasFlashToUnload = history._receivedFlashAvailable;

                        oGP = new GeneralGPPacket(history, _serverTime_DateFormat);
                        oGP.mCurrentClock = history.mClockList;
                        oGP.mFixClock = history.mClockList;
                        oGP.mFix = history.mFixList;
                        oGP.mStatus = history.mStatusList;
                        oGP.mDistance = history.mDistance;
                        oGP.mExtendedValues = history.mExtendedValues;
                        oGP.mEngineData = history.mEngineData;
                    }
                    else
                    {
                        if (oGPPacket != null)
                        {
                            bMsgType = ((MTData.Transport.Gateway.Packet.GatewayProtocolPacket)oGPPacket).cMsgType;
                            fleetId = (int)((MTData.Transport.Gateway.Packet.GatewayProtocolPacket)oGPPacket).cFleetId;
                            vehicleId = (int)((MTData.Transport.Gateway.Packet.GatewayProtocolPacket)oGPPacket).iVehicleId;
                            bIsFlashPacket = ((MTData.Transport.Gateway.Packet.GatewayProtocolPacket)oGPPacket).bArchivalData;
                            bHasFlashToUnload = ((MTData.Transport.Gateway.Packet.GatewayProtocolPacket)oGPPacket)._receivedFlashAvailable;
                            mExtendedVariableDetails = new GBVariablePacketExtended59();
                        }
                        else
                        {
                            bMsgType = (byte)0xC1;  // Status Report.
                            fleetId = (int)v.cFleet;
                            vehicleId = (int)v.iUnit;
                            mExtendedVariableDetails = new GBVariablePacketExtended59();
                            bIsFlashPacket = false;
                            bHasFlashToUnload = false;
                            dtDeviceTime = DateTime.Now.ToUniversalTime();
                            dtGPSTime = DateTime.Now.ToUniversalTime();
                        }
                    }
                    #endregion
                    if (mExtendedVariableDetails.Populated)
                    {
                        #region Check if the unit is currently running a route, if so check if it is at a route check point.
                        if (_routeManagement != null && oGPPacket is MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)
                        {
                            iRouteScheduleID = mExtendedVariableDetails.CurrentVehicleRouteScheduleID;
                            iCheckPointIndex = mExtendedVariableDetails.CurrentRouteCheckPointIndex;
                            if ((bMsgType != (byte)RoutingModuleGPPacket.ROUTES_OFF_ROUTE) && (bMsgType != (byte)RoutingModuleGPPacket.ROUTES_OFF_ROUTE_END))
                            {
                                iRouteScheduleID = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).RouteNumber;
                                iCheckPointIndex = ((MTData.Transport.Gateway.Packet.RoutingModuleGPPacket)oGPPacket).CheckPointIndex;
                            }
                            if (iRouteScheduleID > 0)
                            {
                                oRoutePosResult = new PositionDetails();
                                sRouteCheckPointName = _routeManagement.GetRouteNameForRouteScheduleID(bMsgType, iRouteScheduleID, iCheckPointIndex, oRoutePosResult);
                            }
                        }
                        #endregion
                    }
                }

                if (IsServerSideWPUnit(fleetId, vehicleId))
                {
                    #region Get a list of points the unit is currently at, find the points it is no longer at.
                    if (v != null && oGP != null)
                    {
                        if (bIsFlashPacket || bHasFlashToUnload)
                        {
                            #region If this report is from flash or there is still flash to unload
                            #region If this packet is not a Set point packet and the time on the packet is within the last 60 days, add it to the list to be processed after flash has been unloaded.
                            if (Math.Abs(((TimeSpan)DateTime.Now.ToUniversalTime().Subtract(dtDeviceTime)).Days) < 60)
                            {
                                if (!v.oFlashReportDictionary.ContainsKey(dtDeviceTime))
                                    v.oFlashReportDictionary.Add(dtDeviceTime, oGP);
                            }
                            #endregion
                            #region At this point we can not create any arrive or depart events, but we can set the location name and status on the packet that is inserted into the DB
                            #region Get a list of matched WPs for the unit
                            oCurrentWPList = GetServerSideWaypoint(fleetId, vehicleId, latitude, longitude, ref oDefaultLocation);
                            #endregion
                            #region This packet was from flash or there is still flash data to unit, so all we can do is determine the status by the WPs it is currently at
                            v.ClearStateBit(UnitState.AtWPSpecial1);
                            v.ClearStateBit(UnitState.AtWPSpecial2);
                            v.ClearStateBit(UnitState.AtWayPoint);
                            for (int iVehWPs = 0; iVehWPs < oCurrentWPList.Count; iVehWPs++)
                            {
                                oVehItem = (cWPItem)oCurrentWPList[iVehWPs];
                                if (iVehWPs == 0)
                                {
                                    v.oPos = oVehItem.oPos;
                                    sWPName = oVehItem.oPos.PlaceName;
                                }
                                if ((oVehItem.iWP_Flags & 64) == 64)
                                    v.SetStateBit(UnitState.AtWPSpecial1);
                                else if ((oVehItem.iWP_Flags & 128) == 128)
                                    v.SetStateBit(UnitState.AtWPSpecial2);
                                else
                                    v.SetStateBit(UnitState.AtWayPoint);
                            }
                            #endregion
                            #region If the unit is indicating that it is at a WP, then check the vehicles WP list is correct.
                            sWPName = "";
                            if (oDefaultLocation.PlaceName != "")
                            {
                                v.oPos = oDefaultLocation;
                                sWPName = oDefaultLocation.PlaceName;
                            }
                            if (mExtendedVariableDetails != null && mExtendedVariableDetails.Populated)
                            {
                                if ((mExtendedVariableDetails.UnitStatus & (long)1) == (long)StatusVehicle59.STATUS_WP_AT && (mExtendedVariableDetails.CurrentSetPointID > 0 || mExtendedVariableDetails.CurrentSetPointGroup > 0 || mExtendedVariableDetails.CurrentSetPointNumber > 0))
                                {
                                    oVehItem = new cWPItem();
                                    if (oGP.mExtendedVariableDetails.CurrentSetPointID > 0 && !IsVehicleAtTemporalWaypoint((int)v.cFleet, (int)v.iUnit, oGP.mExtendedVariableDetails.CurrentSetPointID))
                                        sWPName = GetWPNameForID((int)v.cFleet, oGP.mFix.dLatitude, oGP.mFix.dLongitude, mExtendedVariableDetails.CurrentSetPointID, oVehiclePosition, ref oVehItem);
                                    else
                                        sWPName = GetWPNameForNumber((int)v.cFleet, oGP.mFix.dLatitude, oGP.mFix.dLongitude, mExtendedVariableDetails.CurrentSetPointGroup, mExtendedVariableDetails.CurrentSetPointNumber, oVehiclePosition, ref oVehItem);
                                    if (oVehItem != null)
                                    {
                                        if (oVehItem.Name != "")
                                        {
                                            v.oPos = oVehItem.oPos;
                                            sWPName = oVehItem.oPos.PlaceName;
                                            if ((oVehItem.iWP_Flags & 64) == 64)
                                                v.SetStateBit(UnitState.AtWPSpecial1);
                                            else if ((oVehItem.iWP_Flags & 128) == 128)
                                                v.SetStateBit(UnitState.AtWPSpecial2);
                                            else
                                                v.SetStateBit(UnitState.AtWayPoint);
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region If the vehicle is not at any WPs, get the closest suburb name
                            if (sWPName == "")
                            {
                                oVehiclePosition = new PositionDetails();
                                GetClosestSuburbName(latitude, longitude, ref oVehiclePosition);
                                v.oPos = oVehiclePosition;
                            }
                            #endregion
                            #endregion
                            #endregion
                        }
                        else
                        {
                            #region If this is a live report
                            #region Process any flash packets first
                            if (v.oFlashReportDictionary.Count > 0)
                            {
                                oFlashWayPointIDs = v.oWayPointIDs;
                                foreach (GeneralGPPacket oFlashReport in v.oFlashReportDictionary.Values)
                                {
                                    #region Process each flash packet report
                                    #region Get a list of matched WPs for the unit
                                    oFlashLatitude = oFlashReport.mFix.dLatitude;
                                    oFlashLongitude = oFlashReport.mFix.dLongitude;
                                    oCurrentWPList = GetServerSideWaypoint(fleetId, vehicleId, oFlashLatitude, oFlashLongitude, ref oDefaultLocation);
                                    for (int iCurWPs = 0; iCurWPs < oCurrentWPList.Count; iCurWPs++)
                                    {
                                        oCheckCurrentWPList.Add(oCurrentWPList[iCurWPs]);
                                    }
                                    #endregion
                                    #region Compare the oCurrentWPList with the oFlashWayPointIDs and generate arrive and depart events.
                                    if (oFlashWayPointIDs.Count > 0)
                                    {
                                        #region Compare the oCurrentWPList with oCheckCurrentWPList and remove any entries that are already in oCurrentWPList
                                        for (int iCurWPs = oCheckCurrentWPList.Count - 1; iCurWPs >= 0; iCurWPs--)
                                        {
                                            oCurItem = (cWPItem)oCheckCurrentWPList[iCurWPs];
                                            for (int iVehWPs = 0; iVehWPs < oFlashWayPointIDs.Count; iVehWPs++)
                                            {
                                                oVehItem = (cWPItem)oFlashWayPointIDs[iVehWPs];
                                                if (oVehItem.SetPointGroupID == oCurItem.SetPointGroupID && oVehItem.SetPointID == oCurItem.SetPointID)
                                                {
                                                    oCheckCurrentWPList.RemoveAt(iCurWPs);
                                                    break;
                                                }
                                            }
                                        }
                                        #endregion
                                        #region Compare the oFlashWayPointIDs with oCurrentWPList, any items that are in the oFlashWayPointIDs and not in the oCurrentWPList add to oDepartedWPList
                                        for (int iVehWPs = oFlashWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                        {
                                            bool bMatched = false;
                                            oVehItem = (cWPItem)oFlashWayPointIDs[iVehWPs];
                                            for (int iCurWPs = 0; iCurWPs < oCurrentWPList.Count; iCurWPs++)
                                            {
                                                oCurItem = (cWPItem)oCurrentWPList[iCurWPs];
                                                if (oVehItem.SetPointGroupID == oCurItem.SetPointGroupID && oVehItem.SetPointID == oCurItem.SetPointID)
                                                {
                                                    bMatched = true;
                                                    break;
                                                }
                                            }
                                            if (!bMatched)
                                                oDepartedWPList.Add(oVehItem);
                                        }
                                        #endregion
                                    }
                                    if (oDepartedWPList.Count > 0)
                                    {
                                        #region If we are no longer at these WPs, create a depart message for each entry.
                                        for (int X = 0; X <= oDepartedWPList.Count; X++)
                                        {
                                            oWPItem = (cWPItem)oDepartedWPList[X];
                                            #region Create a datetime for the depart WP report
                                            // Move the report time back 1 second for each depart report.
                                            dtGPSTime = oFlashReport.mFixClock.ToDateTime();
                                            dtDeviceTime = oFlashReport.mCurrentClock.ToDateTime();
                                            dtGPSTime = dtGPSTime.AddSeconds(-1 * (X + 1));
                                            dtDeviceTime = dtDeviceTime.AddSeconds(-1 * (X + 1));
                                            oFlashReport.mFixClock.iSecond = (uint)dtGPSTime.Second;
                                            oFlashReport.mFixClock.iMinute = (uint)dtGPSTime.Minute;
                                            oFlashReport.mFixClock.iHour = (uint)dtGPSTime.Hour;
                                            oFlashReport.mFixClock.iDay = (uint)dtGPSTime.Day;
                                            oFlashReport.mFixClock.iMonth = (uint)dtGPSTime.Month;
                                            oFlashReport.mFixClock.iYear = (uint)dtGPSTime.Year - 2000;
                                            oFlashReport.mCurrentClock.iSecond = (uint)dtDeviceTime.Second;
                                            oFlashReport.mCurrentClock.iMinute = (uint)dtDeviceTime.Minute;
                                            oFlashReport.mCurrentClock.iHour = (uint)dtDeviceTime.Hour;
                                            oFlashReport.mCurrentClock.iDay = (uint)dtDeviceTime.Day;
                                            oFlashReport.mCurrentClock.iMonth = (uint)dtDeviceTime.Month;
                                            oFlashReport.mCurrentClock.iYear = (uint)dtDeviceTime.Year - 2000;
                                            #endregion
                                            #region Set the reason Code to WP_Depart
                                            if ((oWPItem.iWP_Flags & 64) == 64)
                                                oFlashReport.cMsgType = (byte)0xA7;  // 167	Departed Special 1
                                            else if ((oWPItem.iWP_Flags & 128) == 128)
                                                oFlashReport.cMsgType = (byte)0xA9;  // 169 - Departed Special 2
                                            else if ((oWPItem.iWP_Flags & 128) == 128)
                                                oFlashReport.cMsgType = (byte)0xAE;  // 174 - Departed No-Go Zone
                                            else
                                                oFlashReport.cMsgType = (byte)0xA2;  // 162 - Departed WP
                                            #endregion
                                            #region Set the vehicle position and state
                                            v.oPos = oWPItem.oPos;
                                            v.ClearStateBit(UnitState.AtWPSpecial1);
                                            v.ClearStateBit(UnitState.AtWPSpecial2);
                                            v.ClearStateBit(UnitState.AtWayPoint);
                                            for (int iVehWPs = oFlashWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                            {
                                                oVehItem = (cWPItem)oFlashWayPointIDs[iVehWPs];
                                                if ((oVehItem.iWP_Flags & 64) == 64)
                                                    v.SetStateBit(UnitState.AtWPSpecial1);
                                                else if ((oVehItem.iWP_Flags & 128) == 128)
                                                    v.SetStateBit(UnitState.AtWPSpecial2);
                                                else
                                                    v.SetStateBit(UnitState.AtWayPoint);
                                            }
                                            #endregion
                                            #region Insert the record into the database and send an update out to the clients.
                                            TrackingQueueItem item = PopulateInsertCommand(oFlashReport, v, oWPItem.SetPointGroupID, oWPItem.SetPointID, oWPItem.SetPointNumber);
                                            PrepareAdvancedECMInsert(oFlashReport.mECMAdvanced, item);
                                            PrepareRoadTypeInsert(oFlashReport.RoadTypeData, item);
                                            sResult = AddToMSMQ(item);
                                            if (sResult != null) _log.Info(sResult);
                                            oLiveUpdatePacket = ((GeneralGPPacket)oFlashReport).CreateCopy();
                                            v.AddAdditionalLiveUpdate(oLiveUpdatePacket);
                                            #endregion
                                            #region Remove the WP entry from the vehicles WP list
                                            for (int iVehWPs = oFlashWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                            {
                                                if (((cWPItem)oFlashWayPointIDs[iVehWPs]).SetPointGroupID == oWPItem.SetPointGroupID && ((cWPItem)oFlashWayPointIDs[iVehWPs]).SetPointID == oWPItem.SetPointID)
                                                {
                                                    oFlashWayPointIDs.RemoveAt(iVehWPs);
                                                }
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    if (((!bUseSSWPsWhilstUnitIsMoving && oFlashReport.mFix.cSpeed == (byte)0x00) || bUseSSWPsWhilstUnitIsMoving) && oCheckCurrentWPList.Count > 0)
                                    {
                                        #region If there are entries in the oCheckCurrentWPList, create an arrive message for each entry.
                                        for (int X = 0; X < oCheckCurrentWPList.Count; X++)
                                        {
                                            oWPItem = (cWPItem)oCheckCurrentWPList[X];
                                            #region Create a datetime for the depart WP report
                                            // Move the report time back 1 second for each depart report.
                                            dtGPSTime = oFlashReport.mFixClock.ToDateTime();
                                            dtDeviceTime = oFlashReport.mCurrentClock.ToDateTime();
                                            dtGPSTime = dtGPSTime.AddSeconds((X + 1));
                                            dtDeviceTime = dtDeviceTime.AddSeconds((X + 1));
                                            oFlashReport.mFixClock.iSecond = (uint)dtGPSTime.Second;
                                            oFlashReport.mFixClock.iMinute = (uint)dtGPSTime.Minute;
                                            oFlashReport.mFixClock.iHour = (uint)dtGPSTime.Hour;
                                            oFlashReport.mFixClock.iDay = (uint)dtGPSTime.Day;
                                            oFlashReport.mFixClock.iMonth = (uint)dtGPSTime.Month;
                                            oFlashReport.mFixClock.iYear = (uint)dtGPSTime.Year - 2000;
                                            oFlashReport.mCurrentClock.iSecond = (uint)dtDeviceTime.Second;
                                            oFlashReport.mCurrentClock.iMinute = (uint)dtDeviceTime.Minute;
                                            oFlashReport.mCurrentClock.iHour = (uint)dtDeviceTime.Hour;
                                            oFlashReport.mCurrentClock.iDay = (uint)dtDeviceTime.Day;
                                            oFlashReport.mCurrentClock.iMonth = (uint)dtDeviceTime.Month;
                                            oFlashReport.mCurrentClock.iYear = (uint)dtDeviceTime.Year - 2000;
                                            #endregion
                                            #region Set the reason Code to WP_Arrive
                                            if ((oWPItem.iWP_Flags & 64) == 64)
                                                oFlashReport.cMsgType = (byte)0xA6;  // 166	Arrive Special 1
                                            else if ((oWPItem.iWP_Flags & 128) == 128)
                                                oFlashReport.cMsgType = (byte)0xA8;  // 168 - Arrive Special 2
                                            else if ((oWPItem.iWP_Flags & 128) == 128)
                                                oFlashReport.cMsgType = (byte)0xAD;  // 173 - Arrive No-Go Zone
                                            else
                                                oFlashReport.cMsgType = (byte)0xA1;  // 161 - Arrive WP
                                            #endregion
                                            #region Set the vehicle position and state
                                            v.oPos = oWPItem.oPos;
                                            v.ClearStateBit(UnitState.AtWPSpecial1);
                                            v.ClearStateBit(UnitState.AtWPSpecial2);
                                            v.ClearStateBit(UnitState.AtWayPoint);
                                            for (int iVehWPs = oFlashWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                            {
                                                oVehItem = (cWPItem)oFlashWayPointIDs[iVehWPs];
                                                if ((oVehItem.iWP_Flags & 64) == 64)
                                                    v.SetStateBit(UnitState.AtWPSpecial1);
                                                else if ((oVehItem.iWP_Flags & 128) == 128)
                                                    v.SetStateBit(UnitState.AtWPSpecial2);
                                                else
                                                    v.SetStateBit(UnitState.AtWayPoint);
                                            }
                                            #endregion
                                            #region Insert the record into the database and send an update out to the clients.
                                            TrackingQueueItem item = PopulateInsertCommand(oFlashReport, v, oWPItem.SetPointGroupID, oWPItem.SetPointID, oWPItem.SetPointNumber);
                                            PrepareAdvancedECMInsert(oFlashReport.mECMAdvanced, item);
                                            PrepareRoadTypeInsert(oFlashReport.RoadTypeData, item);
                                            sResult = AddToMSMQ(item);
                                            if (sResult != null) _log.Info(sResult);
                                            oLiveUpdatePacket = ((GeneralGPPacket)oFlashReport).CreateCopy();
                                            v.AddAdditionalLiveUpdate(oLiveUpdatePacket);
                                            #endregion
                                            #region Add the WP entry from the vehicles WP list
                                            bool bMatch = false;
                                            for (int iVehWPs = 0; iVehWPs < oFlashWayPointIDs.Count; iVehWPs++)
                                            {
                                                if (((cWPItem)oFlashWayPointIDs[iVehWPs]).SetPointGroupID == oWPItem.SetPointGroupID && ((cWPItem)oFlashWayPointIDs[iVehWPs]).SetPointID == oWPItem.SetPointID)
                                                {
                                                    bMatch = true;
                                                }
                                            }
                                            if (!bMatch)
                                                oFlashWayPointIDs.Add(oWPItem);
                                            #endregion
                                        }
                                        #endregion
                                    }
                                    #endregion
                                    #endregion
                                }
                                // Udpate the vehicle object with the current list of locations after processing the flash records.
                                v.oFlashReportDictionary.Clear();
                                v.oWayPointIDs = oFlashWayPointIDs;
                            }
                            #endregion

                            #region Get a list of matched WPs for the unit
                            oCurrentWPList = GetServerSideWaypoint(fleetId, vehicleId, latitude, longitude, ref oDefaultLocation);
                            for (int iCurWPs = 0; iCurWPs < oCurrentWPList.Count; iCurWPs++)
                            {
                                oCheckCurrentWPList.Add(oCurrentWPList[iCurWPs]);
                            }
                            #endregion
                            #region Compare the oCurrentWPList with the v.oWayPointIDs and generate arrive and depart events.
                            if (v.oWayPointIDs.Count > 0)
                            {
                                #region Compare the v.oWayPointIDs with oCheckCurrentWPList and remove any entries from oCheckCurrentWPList that are already in v.oWayPointIDs
                                for (int iCurWPs = oCheckCurrentWPList.Count - 1; iCurWPs >= 0; iCurWPs--)
                                {
                                    oCurItem = (cWPItem)oCheckCurrentWPList[iCurWPs];
                                    for (int iVehWPs = 0; iVehWPs < v.oWayPointIDs.Count; iVehWPs++)
                                    {
                                        oVehItem = (cWPItem)v.oWayPointIDs[iVehWPs];
                                        if (oVehItem.SetPointGroupID == oCurItem.SetPointGroupID && oVehItem.SetPointID == oCurItem.SetPointID)
                                        {
                                            oCheckCurrentWPList.RemoveAt(iCurWPs);
                                            break;
                                        }
                                    }
                                }
                                #endregion
                                #region Compare the v.oWayPointIDs with oCurrentWPList, any items that are in the v.oWayPointIDs and not in the oCurrentWPList add to oDepartedWPList
                                for (int iVehWPs = v.oWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                {
                                    bool bMatched = false;
                                    oVehItem = (cWPItem)v.oWayPointIDs[iVehWPs];
                                    for (int iCurWPs = 0; iCurWPs < oCurrentWPList.Count; iCurWPs++)
                                    {
                                        oCurItem = (cWPItem)oCurrentWPList[iCurWPs];
                                        if (oVehItem.SetPointGroupID == oCurItem.SetPointGroupID && oVehItem.SetPointID == oCurItem.SetPointID)
                                        {
                                            bMatched = true;
                                            break;
                                        }
                                    }
                                    if (!bMatched)
                                        oDepartedWPList.Add(oVehItem);
                                }
                                #endregion
                            }
                            if (oDepartedWPList.Count > 0)
                            {
                                #region For each entry in the oDepartedWPList, create a depart record
                                for (int X = 0; X < oDepartedWPList.Count; X++)
                                {
                                    oWPItem = (cWPItem)oDepartedWPList[X];
                                    TemporalWayPoint wp = GetTemporalWaypoint(oWPItem.SetPointID);
                                    bool valid = true;
                                    if (wp != null)
                                    {
                                        //if we are an arrive packet, check times
                                        valid = wp.CheckTimes(oGP.mCurrentClock.ToDateTime(), this, v, RoutePtSetPtGPPacket.SETPT_DEPART);
                                    }
                                    if (valid)
                                    {
                                        #region Create a datetime for the depart WP report
                                        // Move the report time back 1 second for each depart report.
                                        dtGPSTime = oGP.mFixClock.ToDateTime();
                                        dtDeviceTime = oGP.mCurrentClock.ToDateTime();
                                        dtGPSTime = dtGPSTime.AddSeconds(-1 * (X + 1));
                                        dtDeviceTime = dtDeviceTime.AddSeconds(-1 * (X + 1));
                                        oGP.mFixClock.iSecond = (uint)dtGPSTime.Second;
                                        oGP.mFixClock.iMinute = (uint)dtGPSTime.Minute;
                                        oGP.mFixClock.iHour = (uint)dtGPSTime.Hour;
                                        oGP.mFixClock.iDay = (uint)dtGPSTime.Day;
                                        oGP.mFixClock.iMonth = (uint)dtGPSTime.Month;
                                        oGP.mFixClock.iYear = (uint)dtGPSTime.Year - 2000;
                                        oGP.mCurrentClock.iSecond = (uint)dtDeviceTime.Second;
                                        oGP.mCurrentClock.iMinute = (uint)dtDeviceTime.Minute;
                                        oGP.mCurrentClock.iHour = (uint)dtDeviceTime.Hour;
                                        oGP.mCurrentClock.iDay = (uint)dtDeviceTime.Day;
                                        oGP.mCurrentClock.iMonth = (uint)dtDeviceTime.Month;
                                        oGP.mCurrentClock.iYear = (uint)dtDeviceTime.Year - 2000;
                                        #endregion
                                        #region Set the reason Code to WP_Depart
                                        if ((oWPItem.iWP_Flags & 64) == 64)
                                            oGP.cMsgType = (byte)0xA7;  // 167	Departed Special 1
                                        else if ((oWPItem.iWP_Flags & 128) == 128)
                                            oGP.cMsgType = (byte)0xA9;  // 169 - Departed Special 2
                                        else if ((oWPItem.iWP_Flags & 256) == 256)
                                            oGP.cMsgType = (byte)0xAE;  // 174 - Departed No-Go Zone
                                        else
                                            oGP.cMsgType = (byte)0xA2;  // 162 - Departed WP
                                        #endregion
                                        #region Set the vehicle position and state
                                        v.oPos = oWPItem.oPos;
                                        v.ClearStateBit(UnitState.AtWPSpecial1);
                                        v.ClearStateBit(UnitState.AtWPSpecial2);
                                        v.ClearStateBit(UnitState.AtWayPoint);
                                        for (int iVehWPs = v.oWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                        {
                                            oVehItem = (cWPItem)v.oWayPointIDs[iVehWPs];
                                            if ((oVehItem.iWP_Flags & 64) == 64)
                                                v.SetStateBit(UnitState.AtWPSpecial1);
                                            else if ((oVehItem.iWP_Flags & 128) == 128)
                                                v.SetStateBit(UnitState.AtWPSpecial2);
                                            else
                                                v.SetStateBit(UnitState.AtWayPoint);
                                        }
                                        #endregion
                                        #region Insert the record into the database and send an update out to the clients.
                                        TrackingQueueItem item = PopulateInsertCommand(oGP, v, oWPItem.SetPointGroupID, oWPItem.SetPointID, oWPItem.SetPointNumber);
                                        PrepareAdvancedECMInsert(oGP.mECMAdvanced, item);
                                        PrepareRoadTypeInsert(oGP.RoadTypeData, item);
                                        sResult = AddToMSMQ(item);
                                        if (sResult != null) _log.Info(sResult);
                                        oLiveUpdatePacket = ((GeneralGPPacket)oGP).CreateCopy();
                                        v.AddAdditionalLiveUpdate(oLiveUpdatePacket);
                                        #endregion
                                    }
                                    #region Remove the WP entry from the vehicles WP list
                                    for (int iVehWPs = v.oWayPointIDs.Count - 1; iVehWPs >= 0; iVehWPs--)
                                    {
                                        if (((cWPItem)v.oWayPointIDs[iVehWPs]).SetPointGroupID == oWPItem.SetPointGroupID && ((cWPItem)v.oWayPointIDs[iVehWPs]).SetPointID == oWPItem.SetPointID)
                                        {
                                            v.oWayPointIDs.RemoveAt(iVehWPs);
                                        }
                                    }
                                    #endregion
                                }
                                #endregion
                            }
                            if (((!bUseSSWPsWhilstUnitIsMoving && oGP.mFix.cSpeed == (byte)0x00) || bUseSSWPsWhilstUnitIsMoving) && oCheckCurrentWPList.Count > 0)
                            {
                                #region If there are entries in the oCheckCurrentWPList, create an arrive message for each entry.
                                for (int X = 0; X < oCheckCurrentWPList.Count; X++)
                                {
                                    oWPItem = (cWPItem)oCheckCurrentWPList[X];
                                    TemporalWayPoint wp = GetTemporalWaypoint(oWPItem.SetPointID);
                                    bool valid = true;
                                    if (wp != null)
                                    {
                                        //if we are an arrive packet, check times
                                        valid = wp.CheckTimes(oGP.mCurrentClock.ToDateTime(), this, v, RoutePtSetPtGPPacket.SETPT_ARRIVE);
                                    }
                                    if (valid)
                                    {
                                        #region Create a datetime for the depart WP report
                                        // Move the report time back 1 second for each depart report.
                                        dtGPSTime = oGP.mFixClock.ToDateTime();
                                        dtDeviceTime = oGP.mCurrentClock.ToDateTime();
                                        dtGPSTime = dtGPSTime.AddSeconds((X + 1));
                                        dtDeviceTime = dtDeviceTime.AddSeconds((X + 1));
                                        oGP.mFixClock.iSecond = (uint)dtGPSTime.Second;
                                        oGP.mFixClock.iMinute = (uint)dtGPSTime.Minute;
                                        oGP.mFixClock.iHour = (uint)dtGPSTime.Hour;
                                        oGP.mFixClock.iDay = (uint)dtGPSTime.Day;
                                        oGP.mFixClock.iMonth = (uint)dtGPSTime.Month;
                                        oGP.mFixClock.iYear = (uint)dtGPSTime.Year - 2000;
                                        oGP.mCurrentClock.iSecond = (uint)dtDeviceTime.Second;
                                        oGP.mCurrentClock.iMinute = (uint)dtDeviceTime.Minute;
                                        oGP.mCurrentClock.iHour = (uint)dtDeviceTime.Hour;
                                        oGP.mCurrentClock.iDay = (uint)dtDeviceTime.Day;
                                        oGP.mCurrentClock.iMonth = (uint)dtDeviceTime.Month;
                                        oGP.mCurrentClock.iYear = (uint)dtDeviceTime.Year - 2000;
                                        #endregion
                                        #region Set the reason Code to WP_Arrive
                                        if ((oWPItem.iWP_Flags & 64) == 64)
                                            oGP.cMsgType = (byte)0xA6;  // 166	Arrive Special 1
                                        else if ((oWPItem.iWP_Flags & 128) == 128)
                                            oGP.cMsgType = (byte)0xA8;  // 168 - Arrive Special 2
                                        else if ((oWPItem.iWP_Flags & 256) == 256)
                                            oGP.cMsgType = (byte)0xAD;  // 173 - Arrive No-Go Zone
                                        else
                                            oGP.cMsgType = (byte)0xA1;  // 161 - Arrive WP
                                        #endregion
                                        #region Set the vehicle position and state
                                        v.oPos = oWPItem.oPos;
                                        sWPName = v.oPos.PlaceName;
                                        v.ClearStateBit(UnitState.AtWPSpecial1);
                                        v.ClearStateBit(UnitState.AtWPSpecial2);
                                        v.ClearStateBit(UnitState.AtWayPoint);
                                        for (int iVehWPs = 0; iVehWPs < v.oWayPointIDs.Count; iVehWPs++)
                                        {
                                            oVehItem = (cWPItem)v.oWayPointIDs[iVehWPs];
                                            if ((oVehItem.iWP_Flags & 64) == 64)
                                                v.SetStateBit(UnitState.AtWPSpecial1);
                                            else if ((oVehItem.iWP_Flags & 128) == 128)
                                                v.SetStateBit(UnitState.AtWPSpecial2);
                                            else
                                                v.SetStateBit(UnitState.AtWayPoint);
                                        }
                                        if ((oWPItem.iWP_Flags & 64) == 64)
                                            v.SetStateBit(UnitState.AtWPSpecial1);
                                        else if ((oWPItem.iWP_Flags & 128) == 128)
                                            v.SetStateBit(UnitState.AtWPSpecial2);
                                        else
                                            v.SetStateBit(UnitState.AtWayPoint);
                                        #endregion
                                        #region Insert the record into the database and send an update out to the clients.
                                        TrackingQueueItem item = PopulateInsertCommand(oGP, v, oWPItem.SetPointGroupID, oWPItem.SetPointID, oWPItem.SetPointNumber);
                                        PrepareAdvancedECMInsert(oGP.mECMAdvanced, item);
                                        PrepareRoadTypeInsert(oGP.RoadTypeData, item);
                                        sResult = AddToMSMQ(item);
                                        if (sResult != null) _log.Info(sResult);
                                        oLiveUpdatePacket = ((GeneralGPPacket)oGP).CreateCopy();
                                        v.AddAdditionalLiveUpdate(oLiveUpdatePacket);
                                        #endregion
                                    }
                                    #region Add the WP entry from the vehicles WP list
                                    bool bMatch = false;
                                    for (int iVehWPs = 0; iVehWPs < v.oWayPointIDs.Count; iVehWPs++)
                                    {
                                        if (((cWPItem)v.oWayPointIDs[iVehWPs]).SetPointGroupID == oWPItem.SetPointGroupID && ((cWPItem)v.oWayPointIDs[iVehWPs]).SetPointID == oWPItem.SetPointID)
                                        {
                                            bMatch = true;
                                        }
                                    }
                                    if (!bMatch)
                                        v.oWayPointIDs.Add(oWPItem);
                                    #endregion
                                }
                                #endregion
                            }
                            #region check temperoal waypoint
                            Dictionary<PositionDetails, DatabaseInterface.TemporalWaypointState> waypoints = IsVehicleAtTemporalWaypoints((int)v.cFleet, (int)v.iUnit, oGP.mCurrentClock.ToDateTime());
                            int departIndex = 1;
                            int arriveIndex = -1;
                            bool useWaypointName = true;
                            Dictionary<int, bool> useWaypointFlags = new Dictionary<int, bool>();
                            if (waypoints.Count > 0)
                            {
                                foreach (PositionDetails pos in waypoints.Keys)
                                {
                                    bool clearFlags = true;
                                    switch (waypoints[pos])
                                    {
                                        case DatabaseInterface.TemporalWaypointState.Arrived:
                                            clearFlags = false;
                                            //create arrive packet
                                            if (pos.PositionType == PositionDetails.Category.Special1)
                                                v.SetStateBit(UnitState.AtWPSpecial1);
                                            else if (pos.PositionType == PositionDetails.Category.Special2)
                                                v.SetStateBit(UnitState.AtWPSpecial2);
                                            else
                                                v.SetStateBit(UnitState.AtWayPoint);
                                            CreateTemporalPacket(true, oGP, pos, arriveIndex--, v);
                                            break;
                                        case DatabaseInterface.TemporalWaypointState.Departed:
                                            clearFlags = false;
                                            //create depart packet
                                            CreateTemporalPacket(false, oGP, pos, departIndex++, v);
                                            break;
                                        case TemporalWaypointState.BeforeArriveTime:
                                        case TemporalWaypointState.ScheduledDepartedTimePast:
                                            for (int iVehWPs = 0; iVehWPs < v.oWayPointIDs.Count; iVehWPs++)
                                            {
                                                if (((cWPItem)v.oWayPointIDs[iVehWPs]).SetPointID == pos.SetPointID)
                                                {
                                                    useWaypointName = false;
                                                }
                                            }
                                            break;
                                        case DatabaseInterface.TemporalWaypointState.AtWaypoint:
                                            clearFlags = false;
                                            break;
                                    }
                                    if (!useWaypointFlags.ContainsKey(pos.SetPointID))
                                    {
                                        useWaypointFlags.Add(pos.SetPointID, !clearFlags);
                                    }
                                }
                            }
                            #endregion
                    
                            
                            #region After processing the arrive and depart, determine vehicle state from the remaining entries in v.oWayPointIDs
                            v.ClearStateBit(UnitState.AtWPSpecial1);
                            v.ClearStateBit(UnitState.AtWPSpecial2);
                            v.ClearStateBit(UnitState.AtWayPoint);
                            for (int iVehWPs = 0; iVehWPs < v.oWayPointIDs.Count; iVehWPs++)
                            {
                                oVehItem = (cWPItem)v.oWayPointIDs[iVehWPs];
                                bool addFlag = true;
                                if (useWaypointFlags.ContainsKey(oVehItem.SetPointID))
                                {
                                    addFlag = useWaypointFlags[oVehItem.SetPointID];
                                }
                                if (addFlag)
                                {
                                    if ((oVehItem.iWP_Flags & 64) == 64)
                                        v.SetStateBit(UnitState.AtWPSpecial1);
                                    else if ((oVehItem.iWP_Flags & 128) == 128)
                                        v.SetStateBit(UnitState.AtWPSpecial2);
                                    else
                                        v.SetStateBit(UnitState.AtWayPoint);
                                }
                            }
                            #endregion
                            #endregion
                            sWPName = "";
                            if (oDefaultLocation != null && oDefaultLocation.PlaceName != "" && useWaypointName)
                            {
                                v.oPos = oDefaultLocation;
                                sWPName = oDefaultLocation.PlaceName;
                            }
                            #region If the unit is indicating that it is at a WP, show the vehicle WP as the current location of the vehicle and set the status flags accordingly.
                            if (mExtendedVariableDetails != null && mExtendedVariableDetails.Populated)
                            {
                                if ((mExtendedVariableDetails.UnitStatus & (long)1) == (long)StatusVehicle59.STATUS_WP_AT && (mExtendedVariableDetails.CurrentSetPointID > 0 || mExtendedVariableDetails.CurrentSetPointGroup > 0 || mExtendedVariableDetails.CurrentSetPointNumber > 0))
                                {
                                    oVehItem = new cWPItem();
                                    if (oGP.mExtendedVariableDetails.CurrentSetPointID > 0 && !IsVehicleAtTemporalWaypoint((int)v.cFleet, (int)v.iUnit, oGP.mExtendedVariableDetails.CurrentSetPointID))
                                        sWPName = GetWPNameForID((int)v.cFleet, oGP.mFix.dLatitude, oGP.mFix.dLongitude, mExtendedVariableDetails.CurrentSetPointID, oVehiclePosition, ref oVehItem);
                                    else
                                        sWPName = GetWPNameForNumber((int)v.cFleet, oGP.mFix.dLatitude, oGP.mFix.dLongitude, mExtendedVariableDetails.CurrentSetPointGroup, mExtendedVariableDetails.CurrentSetPointNumber, oVehiclePosition, ref oVehItem);
                                    if (oVehItem != null)
                                    {
                                        if (oVehItem.Name != "")
                                        {
                                            v.oPos = oVehItem.oPos;
                                            sWPName = oVehItem.oPos.PlaceName;
                                            if ((oVehItem.iWP_Flags & 64) == 64)
                                                v.SetStateBit(UnitState.AtWPSpecial1);
                                            else if ((oVehItem.iWP_Flags & 128) == 128)
                                                v.SetStateBit(UnitState.AtWPSpecial2);
                                            else
                                                v.SetStateBit(UnitState.AtWayPoint);
                                        }
                                    }
                                }
                            }
                            #endregion
                            #region If the vehicle is not currently at any WPs, get the closest suburb name
                            if (sWPName == "")
                            {
                                oVehiclePosition = new PositionDetails();
                                GetClosestSuburbName(latitude, longitude, ref oVehiclePosition);
                                v.oPos = oVehiclePosition;
                            }
                            #endregion
                            // Reset the message type back to it's original value.
                            oGP.cMsgType = bMsgType;
                            #endregion
                        }
                    }
                    #endregion
                }
                else
                {
                    #region If the unit is running only unit side WPs
                    #region If the unit was previously running a server side WP Group
                    try
                    {
                        if (v != null)
                        {
                            if (v.oFlashReportDictionary != null)
                            {
                                v.oFlashReportDictionary.Clear();
                            }
                            if (v.oWayPointIDs != null)
                            {
                                if (v.oWayPointIDs.Count > 0)
                                {
                                    #region Create a new depart packet
                                    GeneralGPPacket oTempGP = ((GeneralGPPacket)oGP).CreateCopy();
                                    #endregion
                                    #region For each entry in the oDepartedWPList, create a depart record
                                    for (int X = v.oWayPointIDs.Count - 1; X >= 0; X--)
                                    {
                                        oWPItem = (cWPItem)v.oWayPointIDs[X];
                                        TemporalWayPoint wp = GetTemporalWaypoint(oWPItem.SetPointID);
                                        bool valid = true;
                                        if (wp != null)
                                        {
                                            //if we are an arrive packet, check times
                                            valid = wp.CheckTimes(oGP.mCurrentClock.ToDateTime(), this, v, RoutePtSetPtGPPacket.SETPT_ARRIVE);
                                        }
                                        if (valid)
                                        {
                                            #region Create a datetime for the depart WP report
                                            // Move the report time back 1 second for each depart report.
                                            dtGPSTime = oTempGP.mFixClock.ToDateTime();
                                            dtDeviceTime = oTempGP.mCurrentClock.ToDateTime();
                                            dtGPSTime = dtGPSTime.AddSeconds(-1 * (X + 1));
                                            dtDeviceTime = dtDeviceTime.AddSeconds(-1 * (X + 1));
                                            oTempGP.mFixClock.iSecond = (uint)dtGPSTime.Second;
                                            oTempGP.mFixClock.iMinute = (uint)dtGPSTime.Minute;
                                            oTempGP.mFixClock.iHour = (uint)dtGPSTime.Hour;
                                            oTempGP.mFixClock.iDay = (uint)dtGPSTime.Day;
                                            oTempGP.mFixClock.iMonth = (uint)dtGPSTime.Month;
                                            oTempGP.mFixClock.iYear = (uint)dtGPSTime.Year - 2000;
                                            oTempGP.mCurrentClock.iSecond = (uint)dtDeviceTime.Second;
                                            oTempGP.mCurrentClock.iMinute = (uint)dtDeviceTime.Minute;
                                            oTempGP.mCurrentClock.iHour = (uint)dtDeviceTime.Hour;
                                            oTempGP.mCurrentClock.iDay = (uint)dtDeviceTime.Day;
                                            oTempGP.mCurrentClock.iMonth = (uint)dtDeviceTime.Month;
                                            oTempGP.mCurrentClock.iYear = (uint)dtDeviceTime.Year - 2000;
                                            #endregion
                                            #region Set the reason Code to WP_Depart
                                            if ((oWPItem.iWP_Flags & 64) == 64)
                                                oTempGP.cMsgType = (byte)0xA7;  // 167	Departed Special 1
                                            else if ((oWPItem.iWP_Flags & 128) == 128)
                                                oTempGP.cMsgType = (byte)0xA9;  // 169 - Departed Special 2
                                            else if ((oWPItem.iWP_Flags & 256) == 256)
                                                oTempGP.cMsgType = (byte)0xAE;  // 174 - Departed No-Go Zone
                                            else
                                                oTempGP.cMsgType = (byte)0xA2;  // 162 - Departed WP
                                            #endregion
                                            #region Set the vehicle position and state
                                            v.oPos = oWPItem.oPos;
                                            v.ClearStateBit(UnitState.AtWPSpecial1);
                                            v.ClearStateBit(UnitState.AtWPSpecial2);
                                            v.ClearStateBit(UnitState.AtWayPoint);
                                            for (int iVehWPs = v.oWayPointIDs.Count - 2; iVehWPs >= 0; iVehWPs--)
                                            {
                                                oVehItem = (cWPItem)v.oWayPointIDs[iVehWPs];
                                                if ((oVehItem.iWP_Flags & 64) == 64)
                                                    v.SetStateBit(UnitState.AtWPSpecial1);
                                                else if ((oVehItem.iWP_Flags & 128) == 128)
                                                    v.SetStateBit(UnitState.AtWPSpecial2);
                                                else
                                                    v.SetStateBit(UnitState.AtWayPoint);
                                            }
                                            #endregion
                                            #region Insert the record into the database and send an update out to the clients.
                                            TrackingQueueItem item = PopulateInsertCommand(oTempGP, v, oWPItem.SetPointGroupID, oWPItem.SetPointID, oWPItem.SetPointNumber);
                                            PrepareAdvancedECMInsert(oTempGP.mECMAdvanced, item);
                                            PrepareRoadTypeInsert(oTempGP.RoadTypeData, item);
                                            sResult = AddToMSMQ(item);
                                            if (sResult != null) _log.Info(sResult);
                                            oLiveUpdatePacket = ((GeneralGPPacket)oTempGP).CreateCopy();
                                            v.AddAdditionalLiveUpdate(oLiveUpdatePacket);
                                            #endregion
                                        }
                                        #region Remove the WP entry from the vehicles WP list
                                        v.oWayPointIDs.RemoveAt(X);
                                        #endregion
                                    }
                                    #endregion
                                }
                            }
                        }
                    }
                    catch (System.Exception exCleanServerSide)
                    {
                        _log.Error(sClassName + "GetPositionForVehicle(Vehicle v, object oGPPacket, decimal latitude = " + Convert.ToString(latitude) + ", decimal longitude = " + Convert.ToString(longitude) + ")", exCleanServerSide);
                    }
                    #endregion

                    #region check temperoal waypoint
                    bool useWaypointName = true;
                    if (oGP != null)
                    {
                    Dictionary<PositionDetails, DatabaseInterface.TemporalWaypointState> waypoints = IsVehicleAtTemporalWaypoints((int)v.cFleet, (int)v.iUnit, oGP.mCurrentClock.ToDateTime());
                    int departIndex = 1;
                    int arriveIndex = -1;
                    if (waypoints.Count > 0)
                    {
                        bool clearFlags = true;
                        foreach (PositionDetails pos in waypoints.Keys)
                        {
                            switch (waypoints[pos])
                            {
                                case DatabaseInterface.TemporalWaypointState.Arrived:
                                    clearFlags = false;
                                    //create arrive packet
                                    CreateTemporalPacket(true, oGP, pos, arriveIndex--, v);
                                    break;
                                case DatabaseInterface.TemporalWaypointState.Departed:
                                    clearFlags = false;
                                    //create depart packet
                                    CreateTemporalPacket(false, oGP, pos, departIndex++, v);
                                    break;
                                case TemporalWaypointState.BeforeArriveTime:
                                case TemporalWaypointState.ScheduledDepartedTimePast:
                                    if (oGP.mExtendedVariableDetails.CurrentSetPointID == pos.SetPointID)
                                    {
                                        useWaypointName = false;
                                    }
                                    break;
                                case DatabaseInterface.TemporalWaypointState.AtWaypoint:
                                    clearFlags = false;
                                    break;
                            }
                        }
                        if (clearFlags && !IsAtWaypoint((int)v.cFleet, (int)v.iUnit))
                        {
                            v.ClearStateBit(UnitState.AtWayPoint);
                            v.ClearStateBit(UnitState.AtWPSpecial1);
                            v.ClearStateBit(UnitState.AtWPSpecial2);
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_AT;
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_DOCK;
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_HOLDING;
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_NOGO;
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_SPEEDING;
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_TOO_LONG;
                            v.ServerUnitStateEx &= ~(long)StatusVehicle59.STATUS_WP_WORKSHOP;
                            GeneralGPPacket gen = oGPPacket as GeneralGPPacket;
                            if (gen != null)
                            {
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_AT;
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_DOCK;
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_HOLDING;
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_NOGO;
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_SPEEDING;
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_TOO_LONG;
                                gen.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_WORKSHOP;
                            }
                            else
                            {
                                RoutePtSetPtGPPacket set = oGPPacket as RoutePtSetPtGPPacket;
                                if (set != null)
                                {
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_AT;
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_DOCK;
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_HOLDING;
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_NOGO;
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_SPEEDING;
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_TOO_LONG;
                                    set.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_WORKSHOP;
                                }
                                else
                                {
                                    RoutingModuleGPPacket r = oGPPacket as RoutingModuleGPPacket;
                                    if (r != null)
                                    {
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_AT;
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_DOCK;
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_HOLDING;
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_NOGO;
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_SPEEDING;
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_TOO_LONG;
                                        r.mExtendedVariableDetails.UnitStatus &= ~(long)StatusVehicle59.STATUS_WP_WORKSHOP;
                                    }
                                }
                            }
                        }
                    }
                    }
                    #endregion

                    #region If the unit is not on a route or not currently at a check point, see if the unit is indicating a location from a unit side WP.
                    if ((mExtendedVariableDetails.UnitStatus & (long)1) == (long)StatusVehicle59.STATUS_WP_AT && (mExtendedVariableDetails.CurrentSetPointID > 0 || mExtendedVariableDetails.CurrentSetPointGroup > 0 || mExtendedVariableDetails.CurrentSetPointNumber > 0))
                    {
                        if (v != null)
                        {
                            oVehItem = new cWPItem();
                            if (oGP.mExtendedVariableDetails.CurrentSetPointID > 0 && useWaypointName)
                                sWPName = GetWPNameForID((int)v.cFleet, oGP.mFix.dLatitude, oGP.mFix.dLongitude, mExtendedVariableDetails.CurrentSetPointID, oVehiclePosition, ref oVehItem);
                            else
                                sWPName = GetWPNameForNumber((int)v.cFleet, oGP.mFix.dLatitude, oGP.mFix.dLongitude, mExtendedVariableDetails.CurrentSetPointGroup, mExtendedVariableDetails.CurrentSetPointNumber, oVehiclePosition, ref oVehItem);
                            if (oVehItem != null)
                            {
                                if (oVehItem.Name != "")
                                {
                                    v.oPos = oVehItem.oPos;
                                    sWPName = v.oPos.PlaceName;
                                }
                            }
                        }
                    }
                    #endregion
                    #region If the vehicle is not indicating that it is at a WP or no match could be found, populate the location with the nearest suburb name
                    if ((sWPName == "") || (sWPName == null))
                    {
                        oVehiclePosition = new PositionDetails();
                        GetClosestSuburbName(latitude, longitude, ref oVehiclePosition);
                        if (v != null)
                        {
                            v.oPos = oVehiclePosition;
                        }
                    }
                    #endregion
                    #endregion
                }
                #region If we have a name and position for the current route point, set that on the oVehiclePosition
                if (sRouteCheckPointName != "" && v != null)
                {
                    v.oPos.PlaceName = sRouteCheckPointName;
                }
                #endregion
                if (v != null)
                    oRet = v.oPos;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetPositionForVehicle(Vehicle v, object oGPPacket, decimal latitude = " + Convert.ToString(latitude) + ", decimal longitude = " + Convert.ToString(longitude) + ")", ex);
            }
            return oRet;
        }

        private void CreateTemporalPacket(bool arrivePacket, GeneralGPPacket oGP, PositionDetails pos, int index, Vehicle v)
        {
            #region Create a datetime for the depart WP report
            GeneralGPPacket newPacket = oGP.CreateCopy();

            // Move the report time back 1 second for each depart report.
            DateTime gpsTime = newPacket.mFixClock.ToDateTime().AddSeconds(index);
            DateTime deviceTime = newPacket.mCurrentClock.ToDateTime().AddSeconds(index);
            newPacket.mFixClock.iSecond = (uint)gpsTime.Second;
            newPacket.mFixClock.iMinute = (uint)gpsTime.Minute;
            newPacket.mFixClock.iHour = (uint)gpsTime.Hour;
            newPacket.mFixClock.iDay = (uint)gpsTime.Day;
            newPacket.mFixClock.iMonth = (uint)gpsTime.Month;
            newPacket.mFixClock.iYear = (uint)gpsTime.Year - 2000;
            newPacket.mCurrentClock.iSecond = (uint)deviceTime.Second;
            newPacket.mCurrentClock.iMinute = (uint)deviceTime.Minute;
            newPacket.mCurrentClock.iHour = (uint)deviceTime.Hour;
            newPacket.mCurrentClock.iDay = (uint)deviceTime.Day;
            newPacket.mCurrentClock.iMonth = (uint)deviceTime.Month;
            newPacket.mCurrentClock.iYear = (uint)deviceTime.Year - 2000;
            #endregion
            #region Set the reason Code to WP_Arrive
            switch (pos.PositionType)
            {
                case PositionDetails.Category.Special1:
                    if (arrivePacket)
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_SPECIAL_1_ARRIVE;
                    }
                    else
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_SPECIAL_1_DEPART;
                    }
                    break;
                case PositionDetails.Category.Special2:
                    if (arrivePacket)
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK;
                    }
                    else
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_DEPART_DOCK;
                    }
                    break;
                case PositionDetails.Category.NoGoZone:
                    if (arrivePacket)
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO;
                    }
                    else
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_DEPART_NOGO;
                    }
                    break;
                default:
                    if (arrivePacket)
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_ARRIVE;
                    }
                    else
                    {
                        newPacket.cMsgType = RoutePtSetPtGPPacket.SETPT_DEPART;
                    }
                    break;
            }
            #endregion
            #region Set the vehicle position and state
            v.oPos = pos;
            #endregion
            #region Insert the record into the database and send an update out to the clients.
            TrackingQueueItem item = PopulateInsertCommand(newPacket, v, 0, 0, 0);
            PrepareAdvancedECMInsert(newPacket.mECMAdvanced, item);
            PrepareRoadTypeInsert(newPacket.RoadTypeData, item);
            string sResult = AddToMSMQ(item);
            if (sResult != null) _log.Info(sResult);
            GeneralGPPacket oLiveUpdatePacket = ((GeneralGPPacket)newPacket).CreateCopy();
            v.AddAdditionalLiveUpdate(oLiveUpdatePacket);
            #endregion
        }

        public void ArrivedTemporalWaypoint(TemporalWayPoint waypoint, Vehicle vehicle, DateTime arriveTime, DateTime scheduledArriveTime, DateTime scheduledDepartTime, int state)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleTemporalSetPointInsert {0}, {1}, {2}, '{3}', '{4}', '{5}', {6}", 
                        (int)vehicle.cFleet, (int)vehicle.iUnit, waypoint.Id, arriveTime.ToString("MM/dd/yyyy HH:mm:ss"),
                        scheduledArriveTime.ToString("MM/dd/yyyy HH:mm:ss"), scheduledDepartTime.ToString("MM/dd/yyyy HH:mm:ss"), state);
                    cmd.Connection = oConn;
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(data.Tables[0].Rows[0]["ID"]);
                        lock(oTemporalWaypointsSync)
                        {
                            if (_vehicleTemporalWayPoints != null)
                            {
                                DataRow r = _vehicleTemporalWayPoints.NewRow();
                                r["ID"] = id;
                                r["FleetId"] = (int)vehicle.cFleet;
                                r["VehicleId"] = (int)vehicle.iUnit;
                                r["SetpointID"] = waypoint.Id;
                                r["ArriveTime"] = arriveTime;
                                r["ScheduledArriveTime"] = scheduledArriveTime;
                                r["ScheduledDepartTime"] = scheduledDepartTime;
                                r["State"] = state;
                                _vehicleTemporalWayPoints.Rows.Add(r);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ArrivedTemporalWaypoint()", ex);
            }
        }
        public void EndTemporalWaypoint(int fleetId, int vehicleId, int setPointId)
        {
            lock (oTemporalWaypointsSync)
            {
                if (_vehicleTemporalWayPoints != null)
                {
                    var row = (from p in _vehicleTemporalWayPoints.AsEnumerable()
                               where p.Field<int>("FleetId") == fleetId && p.Field<int>("VehicleId") == vehicleId && p.Field<int>("SetPointId") == setPointId
                               select p).FirstOrDefault(); 
                    if (row != null)
                    {
                        int id = row.Field<int>("Id");
                        EndTemporalWaypoint(id);
                    }
                }
            }
        }
        public void EndTemporalWaypoint(int temporalWaypointId)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleTemporalSetPointDelete {0}", temporalWaypointId);
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    lock (oTemporalWaypointsSync)
                    {
                        if (_vehicleTemporalWayPoints != null)
                        {
                            var row = (from p in _vehicleTemporalWayPoints.AsEnumerable()
                                       where p.Field<int>("ID") == temporalWaypointId
                                       select p).FirstOrDefault();
                            if (row != null)
                            {
                                _vehicleTemporalWayPoints.Rows.Remove(row);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "EndTemporalWaypoint()", ex);
            }
        }
        public void UpdateTemporalWaypointState(int temporalWaypointId, int state)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleTemporalSetPointSetState {0}, {1}", temporalWaypointId, state);
                    cmd.CommandTimeout = 3600;
                    cmd.ExecuteNonQuery();
                    lock (oTemporalWaypointsSync)
                    {
                        if (_vehicleTemporalWayPoints != null)
                        {
                            var row = (from p in _vehicleTemporalWayPoints.AsEnumerable()
                                       where p.Field<int>("ID") == temporalWaypointId
                                       select p).FirstOrDefault();
                            if (row != null)
                            {
                                row.SetField<int>("State", state);
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateTemporalWaypointState()", ex);
            }
        }

        public TemporalWayPoint GetTemporalWaypoint(int setPointId)
        {
            lock(oTemporalWaypointsSync)
            {
                if (_temporalWaypoints.ContainsKey(setPointId))
                {
                    return _temporalWaypoints[setPointId];
                }
            }
            return null;
        }

        public enum TemporalWaypointState
        {
            None,
            BeforeArriveTime,
            Arrived,
            AtWaypoint,
            Departed,
            ScheduledDepartedTimePast,
        }
        public Dictionary<PositionDetails, TemporalWaypointState> IsVehicleAtTemporalWaypoints(int fleetId, int vehicleId, DateTime reportTime)
        {
            Dictionary<PositionDetails, TemporalWaypointState> waypoints = new Dictionary<PositionDetails, TemporalWaypointState>();
            EnumerableRowCollection<DataRow> rows = null;
            lock (oTemporalWaypointsSync)
            {
                if (_vehicleTemporalWayPoints != null)
                {
                    rows = (from p in _vehicleTemporalWayPoints.AsEnumerable()
                            where p.Field<int>("FleetId") == fleetId && p.Field<int>("VehicleId") == vehicleId
                            select p);
                }
            }

            if (rows != null)
            {
                foreach (DataRow r in rows)
                {
                    PositionDetails pos = new PositionDetails();
                    pos.SetPointID = r.Field<int>("SetPointId");
                    DataRow setPoint = null;
                    lock (oSetPointDataSetSync)
                    {
                        setPoint = (from p in mSetPointDataSet.Tables[0].AsEnumerable()
                                    where p.Field<int>("ID") == pos.SetPointID
                                    select p).FirstOrDefault();
                    }
                    if (setPoint != null)
                    {
                        pos.PlaceName = setPoint.Field<string>("Name");
                        pos.SetPointGroupID = setPoint.Field<int>("SetPointGroupID");
                        pos.SetPointNumber = setPoint.Field<int>("SetPointNumber");
                        int flags = setPoint.Field<int>("Flags");
                        if ((flags & 64) == 64)
                        {
                            pos.PositionType = PositionDetails.Category.Special1;
                        }
                        else if ((flags & 128) == 128)
                        {
                            pos.PositionType = PositionDetails.Category.Special2;
                        }
                        else if ((flags & 256) == 256)
                        {
                            pos.PositionType = PositionDetails.Category.NoGoZone;
                        }
                        else 
                        {
                            pos.PositionType = PositionDetails.Category.WayPoint;
                        }
                    }
                    waypoints.Add(pos, GetWaypointsState(r, reportTime));
                }
             
            }
            return waypoints;
        }
        public bool IsVehicleAtTemporalWaypoint(int fleetId, int vehicleId, int setPointId)
        {
            lock (oTemporalWaypointsSync)
            {
                if (_vehicleTemporalWayPoints != null)
                {
                    var row = (from p in _vehicleTemporalWayPoints.AsEnumerable()
                               where p.Field<int>("FleetId") == fleetId && p.Field<int>("VehicleId") == vehicleId && p.Field<int>("SetPointId") == setPointId
                               select p).FirstOrDefault();
                    if (row != null)
                    {
                        int oldState = row.Field<int>("State");
                        //state 0 = at waypoint but before time, state 2 = at waypoint but after time
                        if (oldState == 0 || oldState == 2)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        public bool IsTemporalWaypointArrived(int fleetId, int vehicleId, int setPointId)
        {
            lock (oTemporalWaypointsSync)
            {
                if (_vehicleTemporalWayPoints != null)
                {
                    var row = (from p in _vehicleTemporalWayPoints.AsEnumerable()
                               where p.Field<int>("FleetId") == fleetId && p.Field<int>("VehicleId") == vehicleId && p.Field<int>("SetPointId") == setPointId
                               select p).FirstOrDefault();
                    if (row != null)
                    {
                        int oldState = row.Field<int>("State");
                        if (oldState == 1)
                        {
                            return true;
                        }
                    }
                }
            }
            return false;
        }
        private TemporalWaypointState GetWaypointsState(DataRow r, DateTime reportTime)
        {
            TemporalWaypointState state = TemporalWaypointState.None;
            DateTime arriveTime = r.Field<DateTime>("ScheduledArriveTime");
            DateTime endTime = r.Field<DateTime>("ScheduledDepartTime");
            int oldState = r.Field<int>("State");
            int id = r.Field<int>("Id");
            if (endTime < reportTime)
            {
                if (oldState == 1)
                {
                    UpdateTemporalWaypointState(id, 2);
                    state = TemporalWaypointState.Departed;
                }
                else
                {
                    state = TemporalWaypointState.ScheduledDepartedTimePast;
                }
            }
            else if (reportTime >= arriveTime)
            {
                if (oldState == 0)
                {
                    state = TemporalWaypointState.Arrived;
                    UpdateTemporalWaypointState(id, 1);
                }
                else
                {
                    state = TemporalWaypointState.AtWaypoint;
                }
            }
            else if (reportTime < arriveTime)
            {
                state = TemporalWaypointState.BeforeArriveTime;
            }
            return state;
        }

        public void ArrivedWaypoint(int fleetId, int vehicleId, int setPointId)
        {
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                using (SqlCommand cmd = new SqlCommand())
                {
                    cmd.CommandText = string.Format("exec usp_VehicleSetPointInsert {0}, {1}, {2}",
                        fleetId, vehicleId, setPointId);
                    cmd.Connection = oConn;
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                    {
                        int id = Convert.ToInt32(data.Tables[0].Rows[0]["ID"]);
                        if (id >= 0)
                        {
                            lock (oTemporalWaypointsSync)
                            {
                                if (_vehicleWayPoints != null)
                                {
                                    DataRow r = _vehicleWayPoints.NewRow();
                                    r["ID"] = id;
                                    r["FleetId"] = fleetId;
                                    r["VehicleId"] = vehicleId;
                                    r["SetpointID"] = setPointId;
                                    _vehicleWayPoints.Rows.Add(r);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ArrivedWaypoint()", ex);
            }
        }
        public void EndWaypoint(int fleetId, int vehicleId, int setPointId)
        {
            List<int> ids = new List<int>();
            lock (oTemporalWaypointsSync)
            {
                if (_vehicleWayPoints != null)
                {
                    var row = (from p in _vehicleWayPoints.AsEnumerable()
                               where p.Field<int>("FleetId") == fleetId && p.Field<int>("VehicleId") == vehicleId && p.Field<int>("SetPointId") == setPointId
                               select p);
                    if (row != null)
                    {
                        foreach (var id in row)
                        {
                            ids.Add(id.Field<int>("Id"));
                        }
                    }
                }
            }
 
            if (ids.Count == 0)
            {
                return;
            }
            
            try
            {
                using (SqlConnection oConn = GetOpenConnection())
                using (SqlCommand cmd = oConn.CreateCommand())
                {
                    foreach (int id in ids)
                    {
                        cmd.CommandText = string.Format("exec usp_VehicleSetPointDelete {0}", id);
                        cmd.CommandTimeout = 3600;
                        cmd.ExecuteNonQuery();
                        lock (oTemporalWaypointsSync)
                        {
                            if (_vehicleWayPoints != null)
                            {
                                var row = (from p in _vehicleWayPoints.AsEnumerable()
                                           where p.Field<int>("ID") == id
                                           select p).FirstOrDefault();
                                if (row != null)
                                {
                                    _vehicleWayPoints.Rows.Remove(row);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "EndWaypoint()", ex);
            }
        }
        public bool IsAtWaypoint(int fleetId, int vehicleId)
        {
            lock (oTemporalWaypointsSync)
            {
                if (_vehicleWayPoints != null)
                {
                    var row = (from p in _vehicleWayPoints.AsEnumerable()
                               where p.Field<int>("FleetId") == fleetId && p.Field<int>("VehicleId") == vehicleId
                               select p).FirstOrDefault();
                    if (row != null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
        #endregion
        #region T_Trailers table functions
        /// <summary>
        /// This method will be used to refresh the trailer dataset
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshTrailersDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objDS = null;

            try
            {
                #region Retrieve the data from the T_Trailers table
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("SELECT DISTINCT ID, FleetID, Name, ESN from T_Trailers", oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                objDS.Tables[0].TableName = "Trailers";
                lock (oTrailerDataSetSync)
                {
                    mTrailerDataSet = objDS.Copy();
                }
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshTrailersDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        /// <summary>
        /// This method will pass a copy of the mTrailerDataSet dataset.
        /// </summary>
        /// <returns>DataSet</returns>
        public DataSet GetTrailerESNs()
        {
            DataSet dsRet = null;
            try
            {
                lock (oTrailerDataSetSync)
                {
                    if (mTrailerDataSet != null)
                        dsRet = mTrailerDataSet.Copy();
                }
            }
            catch (System.Exception)
            {
                dsRet = null;
            }
            return dsRet;
        }
        #endregion
        #region T_InterfaceServer and T_InterfaceServerToFleet functions
        /// <summary>
        /// This method will return a hash table of interface server IP and Port
        /// </summary>
        /// <returns>Hashtable</returns>
        public Hashtable GetInterfaceServersTable()
        {
            Hashtable oFleetInterfaceServers = null;
            try
            {
                oFleetInterfaceServers = Hashtable.Synchronized(new Hashtable());

                if (mSqlConnection != null)
                {
                    SqlDataAdapter oDA = new SqlDataAdapter();
                    oDA.SelectCommand = new SqlCommand("select Distinct FleetID FROM T_InterfaceServerToFleet", mSqlConnection);
                    DataSet oDS_Fleets = new DataSet();
                    oDA.Fill(oDS_Fleets);
                    if (oDS_Fleets.Tables.Count > 0)
                    {
                        if (oDS_Fleets.Tables[0].Rows.Count > 0)
                        {
                            for (int X = 0; X < oDS_Fleets.Tables[0].Rows.Count; X++)
                            {
                                int iFleetID = Convert.ToInt32(oDS_Fleets.Tables[0].Rows[X]["FleetID"]);
                                oDA = new SqlDataAdapter();
                                oDA.SelectCommand = new SqlCommand("select DISTINCT isnull(isrv.IPAddress, '') as IPAddress, isnull(isrv.Port, 0) as Port FROM T_InterfaceServers isrv INNER JOIN T_InterfaceServerToFleet iflt ON isrv.InterfaceServerID = iflt.InterfaceServerID WHERE iflt.FleetID = " + Convert.ToString(iFleetID), mSqlConnection);
                                DataSet oDS = new DataSet();
                                oDA.Fill(oDS);
                                if (oDS.Tables.Count > 0)
                                {
                                    if (oDS.Tables[0].Rows.Count > 0)
                                    {
                                        IPEndPoint[] oEndPoints = new IPEndPoint[oDS.Tables[0].Rows.Count];
                                        for (int Y = 0; Y < oDS.Tables[0].Rows.Count; Y++)
                                        {
                                            string sAddress = Convert.ToString(oDS.Tables[0].Rows[Y]["IPAddress"]);
                                            int iPort = Convert.ToInt32(oDS.Tables[0].Rows[Y]["Port"]);
                                            if (sAddress != "" && iPort > 0)
                                            {
                                                try
                                                {
                                                    IPEndPoint oEP = new IPEndPoint(System.Net.IPAddress.Parse(sAddress), iPort);
                                                    oEndPoints[Y] = oEP;
                                                }
                                                catch (System.Exception ex)
                                                {
                                                    _log.Error(sClassName + "XXX()", ex);
                                                    oEndPoints[Y] = null;
                                                }
                                            }
                                        }
                                        oFleetInterfaceServers.Add(iFleetID, oEndPoints);
                                    }
                                }
                            }	// End For
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetInterfaceServersTable()", ex);
            }
            return oFleetInterfaceServers;
        }
        #endregion
        #region T_MassDeclaration table functions
        /// <summary>
        /// This method will be used to refresh the mass declaration datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshMassDeclarationDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objDS = null;
            string sSQL = "";
            try
            {
                #region Retrieve the data from the T_MassDeclaration table
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                sSQL = "EXEC [usp_GetMassDeclarationDataForListenerLoad]";
                objSQLCmd = new SqlCommand(sSQL, oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                objDS.Tables[0].TableName = "TotalMass";
                lock (oMassDataSetSync)
                {
                    mMassDataSet = objDS.Copy();
                }
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshMassDeclarationDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        #endregion
        #region T_ScheduledTasks table functions
        /// <summary>
        /// This method will be used to refresh the scheduled task datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshScheduledTasksDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            SqlCommand cmd = null;
            DataSet data = null;
            try
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    cmd = new SqlCommand("exec usp_GetScheduledTasksDataForListenerLoad", oConn);
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    data = new DataSet();
                    adapter.Fill(data);
                    data.Tables[0].TableName = "Tasks";
                    lock (oScheduledTasksDataSetSync)
                    {
                        mScheduledTasksDataSet = data.Copy();
                    }
                    cmd.Dispose();
                    cmd = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshMassDeclarationDataSet(System.Data.SqlClient.SqlConnection oConn)", ex);
            }
        }
        public List<ScheduledTask> GetScheduledTasksForVehicle(int fleetId, uint vehicleId)
        {
            List<ScheduledTask> tasks = new List<ScheduledTask>();
            try
            {
                DataSet dsCopy = null;
                lock (oScheduledTasksDataSetSync)
                {
                    if (mScheduledTasksDataSet != null)
                    {
                        dsCopy = mScheduledTasksDataSet.Copy();
                    }
                }
                if (dsCopy != null)
                {
                    if (dsCopy.Tables.Contains("Tasks") && dsCopy.Tables["Tasks"].Rows.Count > 0)
                    {
                        DataRow[] rows = dsCopy.Tables["Tasks"].Select(string.Format("FleetId = {0} and VehicleId = {1}", fleetId, vehicleId));
                        foreach (DataRow row in rows)
                        {
                            tasks.Add(new ScheduledTask(row, this.emailHelper));
                        }
                    }
                    dsCopy.Dispose();
                    dsCopy = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + ".GetScheduledTasksForVehicle(int fleetId, uint vehicleId)", ex);
            }
            return tasks;
        }
        public DataSet UpdateTaskState(ScheduledTask t, string notes, TriggerTypes trigger)
        {
            SqlConnection oConn = GetOpenConnection();
            if (oConn == null)
            {
                _log.Error("unable to update task state as can't open database connection");
                return null;
            }

            SqlCommand cmd = null;
            DataSet data = null;
            try
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    cmd = new SqlCommand(string.Format("exec usp_UpdateScheduleTaskState -1, {0}, {1}, '{2}', {3}", t.Id, (int)t.State, notes, (int)trigger), oConn);
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    data = new DataSet();
                    adapter.Fill(data);
                    data.Tables[0].TableName = "notifications";
                    data.Tables[1].TableName = "emails";
                    cmd.Dispose();
                    cmd = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateTaskState()", ex);
            }
            finally
            {
                if (oConn != null)
                {
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                }
            }
            return data;
        }
        public void CheckScheduledTasksAgainstDate(DateTime time)
        {
            SqlConnection connection = GetOpenConnection();
            if (connection == null)
            {
                _log.Error("unable to get all schedule tasks as can't open database connection");
                return;
            }

            try
            {
                //get list of all tasks in system
                List<ScheduledTask> tasks = GetScheduledTasks(connection);

                //iterate through each task and check date
                bool updated = false;
                foreach (ScheduledTask t in tasks)
                {
                    if (t.CheckDate(time, this))
                    {
                        updated = true;
                    }
                }

                //if any task has been updated, refresh the data
                if (updated)
                {
                    RefreshScheduledTasksDataSet(connection);
                }

                //fire event to get vehicles to use latest rules
                if (OnRefreshScheduledTaskDataset != null)
                {
                    OnRefreshScheduledTaskDataset();
                }
            }
            finally
            {
                if (connection != null)
                {
                    connection.Close();
                    connection = null;
                }
            }
        }
        private List<ScheduledTask> GetScheduledTasks(SqlConnection connection)
        {
            List<ScheduledTask> tasks = new List<ScheduledTask>();
            try
            {
                //reload the data so we are using latest rules
                RefreshScheduledTasksDataSet(connection);

                DataSet dsCopy = null;
                lock (oScheduledTasksDataSetSync)
                {
                    if (mScheduledTasksDataSet != null)
                    {
                        dsCopy = mScheduledTasksDataSet.Copy();
                    }
                }
                if (dsCopy != null)
                {
                    if (dsCopy.Tables.Contains("Tasks") && dsCopy.Tables["Tasks"].Rows.Count > 0)
                    {
                        foreach (DataRow row in dsCopy.Tables["Tasks"].Rows)
                        {
                            tasks.Add(new ScheduledTask(row, this.emailHelper));
                        }
                    }
                    dsCopy.Dispose();
                    dsCopy = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + ".GetScheduledTasksForVehicle(int fleetId, uint vehicleId)", ex);
            }
            return tasks;
        }
        #endregion
        #region T_ExtendedHistoryRequests table functions
        /// <summary>
        /// This method will be used to refresh the extend history requests datasets
        /// </summary>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>void</returns>
        public void RefreshExtendedHistoryDataSet(System.Data.SqlClient.SqlConnection oConn)
        {
            bool closeConnection = false;
            SqlConnection connection = oConn;
            if (oConn == null)
            {
                closeConnection = true;
                connection = GetOpenConnection();
            }

            try
            {
                SqlCommand cmd = null;
                DataSet data = null;

                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    cmd = new SqlCommand("exec usp_GetExtendedHistoryDataForListenerLoad", connection);
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    data = new DataSet();
                    adapter.Fill(data);
                    data.Tables[0].TableName = "ExtendedHistory";
                    lock (oExtendedHistoryDataSetSync)
                    {
                        mExtendedHistoryDataSet = data.Copy();
                    }
                    cmd.Dispose();
                    cmd = null;
                }
            }
            finally
            {
                if (closeConnection && connection != null)
                {
                    connection.Close();
                    connection = null;
                }
            } 
        }
        public List<RequestExtendedHistory> GetExtendedHistoryForVehicle(int fleetId, uint vehicleId)
        {
            List<RequestExtendedHistory> requests = new List<RequestExtendedHistory>();
            try
            {
                DataSet dsCopy = null;
                lock (oExtendedHistoryDataSetSync)
                {
                    if (mExtendedHistoryDataSet!= null)
                    {
                        dsCopy = mExtendedHistoryDataSet.Copy();
                    }
                }
                if (dsCopy != null)
                {
                    if (dsCopy.Tables.Contains("ExtendedHistory") && dsCopy.Tables["ExtendedHistory"].Rows.Count > 0)
                    {
                        DataRow[] rows = dsCopy.Tables["ExtendedHistory"].Select(string.Format("FleetId = {0} and VehicleId = {1}", fleetId, vehicleId));
                        foreach (DataRow row in rows)
                        {
                            requests.Add(new RequestExtendedHistory(row));
                        }
                    }
                    dsCopy.Dispose();
                    dsCopy = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + ".GetExtendedHistoryForVehicle(int fleetId, uint vehicleId)", ex);
            }
            return requests;
        }
        public void UpdateExtendedHistoryRequestSent(RequestExtendedHistory request)
        {
            SqlConnection oConn = GetOpenConnection();
            if (oConn == null)
            {
                _log.Error("unable to update extended history request start as can't open database connection");
                return;
            }

            try
            {
                using (SqlCommand oCmd = new SqlCommand(string.Format("exec usp_ExtendedHistoryRequestSent {0}", request.Id), oConn))
                {
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateExtendedHistoryRequestStart()", ex);
            }
            finally
            {
                if (oConn != null)
                {
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                }
            }
        }
        public void UpdateExtendedHistoryRequestComplete(RequestExtendedHistory request, Vehicle v)
        {
            SqlConnection oConn = GetOpenConnection();
            if (oConn == null)
            {
                _log.Error("unable to update extended history request complete as can't open database connection");
                return;
            }

            try
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter())
                {
                    SqlCommand cmd = new SqlCommand(string.Format("exec usp_ExtendedHistoryRequestComplete {0}", request.Id), oConn);
                    cmd.CommandTimeout = 3600;
                    adapter.SelectCommand = cmd;
                    DataSet data = new DataSet();
                    adapter.Fill(data);
                    if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                    {
                        v.CompletedExtenededHistoryRequestStart = Convert.ToDateTime(data.Tables[0].Rows[0]["StartDate"]);
                        v.CompletedExtenededHistoryRequestEnd = Convert.ToDateTime(data.Tables[0].Rows[0]["EndDate"]);
                    }
                    
                    cmd.Dispose();
                    cmd = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateExtendedHistoryRequestStart()", ex);
            }
            finally
            {
                if (oConn != null)
                {
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                }
            }
        }
        #endregion
        #region Search/Update T_GatewayUnit functions
        /// <summary>
        /// This method will retrieve the record for the Gateway Unit based on the criteria passed in.
        /// It will be called by the GEN_IM_ALIVE_IMEI_ESN message to establish the credentials of the 
        /// gateway unit.
        /// </summary>
        /// <param name="GeneralGPPacket aPacket"></param>
        /// <param name="System.Data.SqlClient.SqlConnection oConn"></param>
        /// <returns>DataSet</returns>
        public DataSet GetGatewayUnitCredentials(GeneralGPPacket aPacket, System.Data.SqlClient.SqlConnection oConn)
        {
            bool bCreateNewEntry = false;
            SqlDataAdapter oSQLDA = null;
            SqlCommand oSQLCmd = null;
            DataSet oResult = null;
            string sSQL = "";
            int iRes = 0;
            bool bCheckBySerialNumber = false;

            lock (this)
            {
                #region Create the select string base on the info provided
                try
                {
                    if (aPacket.lSerialNumber > 0)
                    {
                        sSQL = "SELECT SerialNumber, IMIE, CDMAESN, FleetID, VehicleID FROM T_GatewayUnit WHERE SerialNumber = [SerialNumber]";
                        sSQL = sSQL.Replace("[SerialNumber]", Convert.ToString(aPacket.lSerialNumber));
                        bCheckBySerialNumber = true;
                    }
                    else if (aPacket.sIMEIInbound != "")
                    {
                        sSQL = "SELECT SerialNumber, IMIE, CDMAESN, FleetID, VehicleID FROM T_GatewayUnit WHERE LTRIM(RTRIM(REPLACE(IMIE, CHAR(13) + CHAR(10), ''))) = '[IMIE]'";
                        sSQL = sSQL.Replace("[IMIE]", aPacket.sIMEIInbound);
                    }
                    else if (aPacket.sCDMAESNInbound != "")
                    {

                        sSQL = "SELECT SerialNumber, IMIE, CDMAESN, FleetID, VehicleID FROM T_GatewayUnit WHERE LTRIM(RTRIM(REPLACE(CDMAESN, CHAR(13) + CHAR(10), ''))) = '[CDMAESN]'";
                        sSQL = sSQL.Replace("[CDMAESN]", aPacket.sCDMAESNInbound);

                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetGatewayUnitCredentials(GeneralGPPacket aPacket) (1)", ex);
                }
                #endregion
                #region Select the data from the DB
                try
                {
                    oSQLCmd = oConn.CreateCommand();
                    oSQLCmd.CommandText = sSQL;
                    oSQLDA = new SqlDataAdapter(oSQLCmd);
                    oResult = new DataSet();
                    oSQLDA.Fill(oResult);
                    oSQLDA.Dispose();
                    oSQLDA = null;
                    oSQLCmd.Dispose();
                    oSQLCmd = null;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetGatewayUnitCredentials(GeneralGPPacket aPacket) (2)", ex);
                    oResult = null;
                    return oResult;
                }
                #endregion
                #region In no result was retruned, check if we can create a new entry for this unit.
                try
                {
                    bCreateNewEntry = true;
                    if (oResult != null)
                    {
                        if (oResult.Tables.Count > 0)
                        {
                            if (oResult.Tables[0].Rows.Count > 0)
                            {
                                aPacket.iVehicleId = Convert.ToUInt16(oResult.Tables[0].Rows[0]["VehicleID"]);
                                aPacket.cFleetId = Convert.ToByte(oResult.Tables[0].Rows[0]["FleetID"]);
                                // 508x do not send a serial number, we need to update the packet for these units so the 
                                // T_GatewayUnit table is updated correctly.
                                if (aPacket.lSerialNumber == 0)
                                    aPacket.lSerialNumber = Convert.ToInt32(oResult.Tables[0].Rows[0]["SerialNumber"]);
                                bCreateNewEntry = false;
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetGatewayUnitCredentials(GeneralGPPacket aPacket) (3)", ex);
                    oResult = null;
                    return oResult;
                }
                #endregion
                if (bCreateNewEntry)
                {
                    #region Create a new vehicle record for this unit.
                    if (!bCheckBySerialNumber)
                    {
                        // If we couldn't find a match on CDMA ESN or IMIE, check via serial number
                        sSQL = "SELECT SerialNumber, IMIE, CDMAESN, FleetID, VehicleID FROM T_GatewayUnit WHERE SerialNumber = [SerialNumber]";
                        sSQL = sSQL.Replace("[SerialNumber]", Convert.ToString(aPacket.lSerialNumber));
                        oSQLCmd = oConn.CreateCommand();
                        oSQLCmd.CommandText = sSQL;
                        oSQLDA = new SqlDataAdapter(oSQLCmd);
                        oResult = new DataSet();
                        oSQLDA.Fill(oResult);
                        oSQLDA.Dispose();
                        oSQLDA = null;
                        oSQLCmd.Dispose();
                        oSQLCmd = null;
                        if (oResult != null)
                        {
                            if (oResult.Tables.Count > 0)
                            {
                                if (oResult.Tables[0].Rows.Count > 0)
                                {
                                    aPacket.iVehicleId = Convert.ToUInt16(oResult.Tables[0].Rows[0]["VehicleID"]);
                                    aPacket.cFleetId = Convert.ToByte(oResult.Tables[0].Rows[0]["FleetID"]);
                                    bCreateNewEntry = false;
                                }
                            }
                        }
                    }

                    if (bCreateNewEntry)
                    {
                        #region Create a new entry for the unit
                        try
                        {
                            sSQL = "EXEC usp_CreateEntryForUnknownUnit @SerialNumber, '@IMIE', '@CDMAESN'";
                            sSQL = sSQL.Replace("@SerialNumber", Convert.ToString(aPacket.lSerialNumber));
                            sSQL = sSQL.Replace("@IMIE", aPacket.sIMEIInbound);
                            sSQL = sSQL.Replace("@CDMAESN", aPacket.sCDMAESNInbound);
                            oSQLCmd = oConn.CreateCommand();
                            oSQLCmd.CommandText = sSQL;
                            iRes = oSQLCmd.ExecuteNonQuery();

                            if (iRes >= 0)
                            {
                                sSQL = "SELECT SerialNumber, IMIE, CDMAESN, FleetID, VehicleID FROM T_GatewayUnit WHERE SerialNumber = [SerialNumber]";
                                sSQL = sSQL.Replace("[SerialNumber]", Convert.ToString(aPacket.lSerialNumber));
                            }
                            else
                            {
                                _log.Info("GetGatewayUnitCredentials(GeneralGPPacket aPacket) (4) - Failed to auto create unit.");
                                oResult = null;
                                return oResult;
                            }
                            ForceRefreshDataSets(true);
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "GetGatewayUnitCredentials(GeneralGPPacket aPacket) (5)", ex);
                            oResult = null;
                            return oResult;
                        }
                        #endregion
                        #region Select back the details for the newly created entry
                        try
                        {
                            oSQLCmd = oConn.CreateCommand();
                            oSQLCmd.CommandText = sSQL;
                            oSQLDA = new SqlDataAdapter(oSQLCmd);
                            oResult = new DataSet();
                            oSQLDA.Fill(oResult);
                            oSQLDA.Dispose();
                            oSQLDA = null;
                            oSQLCmd.Dispose();
                            oSQLCmd = null;
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "GetGatewayUnitCredentials(GeneralGPPacket aPacket) (6)", ex);
                            oResult = null;
                            return oResult;
                        }
                        #endregion
                        #region Read the fleet/vehicle ID and set them back to the packet
                        try
                        {
                            if (oResult != null)
                            {
                                if (oResult.Tables.Count > 0)
                                {
                                    if (oResult.Tables[0].Rows.Count > 0)
                                    {
                                        aPacket.iVehicleId = Convert.ToUInt16(oResult.Tables[0].Rows[0]["VehicleID"]);
                                        aPacket.cFleetId = Convert.ToByte(oResult.Tables[0].Rows[0]["FleetID"]);
                                    }
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "GetGatewayUnitCredentials(GeneralGPPacket aPacket) (7)", ex);
                            oResult = null;
                            return oResult;
                        }
                        #endregion
                    }
                    #endregion
                }
            }
            return oResult;
        }
        /// <summary>
        /// This method will update the port numbers etc.. for the GatewayUnit on logon.. IM_ALIVE
        /// </summary>
        /// <param name="GeneralGPPacket aPacket"></param>
        /// <param name="DataSet dsCredentials"></param>
        /// <param name="SqlConnection oConn"></param>
        /// <returns>string</returns>
        public string UpdateGatewayUnitWithCredentials(GeneralGPPacket aPacket, DataSet dsCredentials, SqlConnection oConn)
        {
            string sRet = null;
            string sSQL = "";
            string sListenPort = "";
            SqlCommand oSQLCmd = null;
            int iRes = 0;
            string sLogEntry = "";

            if (aPacket != null)
            {
                #region Create a log entry for the Gen_Im_Alive
                try
                {
                    sLogEntry = "[MsgType] from [FleetID]/[VehicleID] - S/N : [SerialNumber], SW Ver : [ProgramMajor].[ProgramMinor], IO Box : [IOBoxProgramMajor].[IOBoxProgramMinor], MDT : [MDTProgramMajor].[MDTProgramMinor]";
                    if (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_FULL)
                        sLogEntry = sLogEntry.Replace("[MsgType]", "GEN_IM_ALIVE_FULL");
                    else if (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN)
                        sLogEntry = sLogEntry.Replace("[MsgType]", "GEN_IM_ALIVE_IMEI_ESN");
                    else if (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMIE)
                        sLogEntry = sLogEntry.Replace("[MsgType]", "GEN_IM_ALIVE_IMIE");
                    else
                        sLogEntry = sLogEntry.Replace("[MsgType]", "GEN_IM_ALIVE");
                    sLogEntry = sLogEntry.Replace("[FleetID]", Convert.ToString((int)aPacket.cFleetId));
                    sLogEntry = sLogEntry.Replace("[VehicleID]", Convert.ToString((int)aPacket.iVehicleId));
                    sLogEntry = sLogEntry.Replace("[SerialNumber]", Convert.ToString(aPacket.lSerialNumber));
                    sLogEntry = sLogEntry.Replace("[ProgramMajor]", Convert.ToString((int)aPacket._cProgramMajor));
                    sLogEntry = sLogEntry.Replace("[ProgramMinor]", Convert.ToString(aPacket._iProgramMinor));
                    sLogEntry = sLogEntry.Replace("[IOBoxProgramMajor]", Convert.ToString((int)aPacket.cIOBoxProgramMajor));
                    sLogEntry = sLogEntry.Replace("[IOBoxProgramMinor]", Convert.ToString((int)aPacket.cIOBoxProgramMinor));
                    sLogEntry = sLogEntry.Replace("[MDTProgramMajor]", Convert.ToString(aPacket.iMDTProgramMajor));
                    sLogEntry = sLogEntry.Replace("[MDTProgramMinor]", Convert.ToString(aPacket.iMDTProgramMinor));
                    _log.Info(sLogEntry);
                }
                catch (System.Exception)
                {
                }
                #endregion

                #region Update the T_GatewayUnit table with the values on the login packet
                try
                {
                    sListenPort = aPacket.cProgramMajor + "," + (aPacket.iProgramMinor >> 8) + (aPacket.iProgramMinor & 0xFF) + ":" + (aPacket.cIOBoxProgramMajor & 0xFF) + (aPacket.cIOBoxProgramMinor & 0xFF);
                    if (aPacket.SoftwareBuild > 0 || aPacket.SoftwareBranch > 0 || aPacket.SoftwareBuildOptions > 0)
                    {
                        sListenPort = string.Format("{0}|{1}|{2}|{3}", sListenPort, aPacket.SoftwareBuild, aPacket.SoftwareBranch, aPacket.SoftwareBuildOptions);
                    }
                    //sSQL = "UPDATE T_GatewayUnit SET IPAddress = '[IPAddress]', ListenPort = '[ListenPort]', SIMCardNumber = '[SIMCardNumber]', IMIE = '[IMIE]', CDMAESN = '[CDMAESN]' WHERE SerialNumber = [SerialNumber]";
                    sSQL = "usp_Update_GatewayUnit [SerialNumber], '[IPAddress]', '[ListenPort]', '[SIMCardNumber]', '[IMIE]', '[CDMAESN]'";
                    sSQL = sSQL.Replace("[IPAddress]", aPacket.mSenderIP.Address.ToString());
                    sSQL = sSQL.Replace("[ListenPort]", sListenPort);
                    sSQL = sSQL.Replace("[SIMCardNumber]", aPacket.sSimNumber);
                    sSQL = sSQL.Replace("[SerialNumber]", Convert.ToString(aPacket.lSerialNumber));
                    if (dsCredentials != null)
                    {
                        if (dsCredentials.Tables.Count > 0)
                        {
                            if (dsCredentials.Tables[0].Rows.Count > 0)
                            {
                                if (dsCredentials.Tables[0].Rows[0]["IMIE"] != System.DBNull.Value)
                                    sSQL = sSQL.Replace("[IMIE]", aPacket.sIMEIInbound);
                                if (dsCredentials.Tables[0].Rows[0]["IMIE"] != System.DBNull.Value)
                                    sSQL = sSQL.Replace("[CDMAESN]", aPacket.sCDMAESNInbound);
                            }
                        }
                    }
                    sSQL = sSQL.Replace("[IMIE]", "");
                    sSQL = sSQL.Replace("[CDMAESN]", "");
                    oSQLCmd = oConn.CreateCommand();
                    oSQLCmd.CommandText = sSQL;
                    iRes = Convert.ToInt32(oSQLCmd.ExecuteScalar());
                    if (iRes < 0)
                        sRet = "Failed to create asset register entry.";
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "UpdateGatewayUnitWithCredentials(GeneralGPPacket aPacket, DataSet dsCredentials)", ex);
                    sRet = "Failed to create asset register entry.";
                }
                #endregion
                #region If this is a GEN_IM_ALIVE_FULL packet, create the T_AssetRegister entry
                if (aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_FULL)
                {
                    try
                    {
                        TrackingQueueItem item = new TrackingQueueItem();
                        UpdateAssetRegister updateAsset = new UpdateAssetRegister();
                        updateAsset.FleetID = (int)aPacket.cFleetId;
                        updateAsset.VehicleID = (int)aPacket.iVehicleId;
                        updateAsset.HardwareType = aPacket.iHWTypeInbound;
                        updateAsset.SimCard = aPacket.sSimNumber;
                        updateAsset.SlotNumber = aPacket.iSlotNumber;
                        updateAsset.PreferedSlotNumber = aPacket.iPreferedSlotNumber;
                        if (aPacket.sNetworkName.Length > 30)
                            updateAsset.NetworkName = aPacket.sNetworkName.Substring(0, 30);
                        else
                            updateAsset.NetworkName = aPacket.sNetworkName;
                        updateAsset.IMEI = aPacket.sIMEIInbound;
                        updateAsset.CDMAESN = aPacket.sCDMAESNInbound;
                        updateAsset.SerialNumber = aPacket.lSerialNumber;
                        updateAsset.SoftwareVersion = aPacket.iSoftwareVersion;
                        updateAsset.ProgramMajor = (int)aPacket._cProgramMajor;
                        updateAsset.ProgramMinor = aPacket._iProgramMinor;
                        updateAsset.HardwareVersionNumber = aPacket.iHardwareVersionNumber;
                        updateAsset.IOBoxSerialNumber = aPacket.lIOBoxSerialNumber;
                        updateAsset.IOBoxProgramMajor = (int)aPacket.cIOBoxProgramMajor;
                        updateAsset.IOBoxProgramMajor = (int)aPacket.cIOBoxProgramMinor;
                        updateAsset.IOBoxHardwareVersionNumber = aPacket.iIOBoxHardwareVersionNumber;
                        updateAsset.MDTSerialNumber = aPacket.lMDTSerialNumber;
                        updateAsset.MDTProgramMajor = aPacket.iMDTProgramMajor;
                        updateAsset.MDTProgramMinor = aPacket.iMDTProgramMinor;
                        updateAsset.MDTHardwareVer = aPacket.iMDTHardwareVer;
                        updateAsset.LoginFlags = aPacket.iLoginFlags;
                        item.SubCommands.Add(updateAsset);
                        string temp = this.AddToMSMQ(item);
                        if (!string.IsNullOrEmpty(temp))
                        {
                            if (sRet.Length > 0)
                                sRet += "\r\n" + temp;
                            else
                                sRet = temp;
                        }

                    }
                    catch (System.Exception ex)
                    {
                        if (!ex.Message.Contains("Violation of PRIMARY KEY constraint"))
                        {
                            _log.Error(sClassName + "UpdateGatewayUnitWithCredentials(GeneralGPPacket aPacket, DataSet dsCredentials)", ex);
                            if (sRet.Length > 0)
                                sRet += "\r\nFailed to create asset register entry.";
                            else
                                sRet = "Failed to create asset register entry.";
                        }
                    }
                }
                #endregion
            }
            return sRet;
        }
        #endregion
        #region Route table loading.
        /// <summary>
        /// This method will cause the automatic timed refresh of the cached dataaset thread to update the dataset straight away.
        /// </summary>
        /// <returns>void</returns>
        public void RefreshRouteData()
        {
            bool bConnectionError = false;
            SqlConnection oConn = null;
            int iRetryCount = 0;
            if (_routeManagement != null)
            {
                #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
                try
                {
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "RefreshRouteData() - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    bConnectionError = true;
                }
                while (bConnectionError)
                {
                    try
                    {
                        if (iRetryCount >= 24)
                        {
                            _log.Info("Giving up on database connection for updating routes after 24 retries at 5 second intervals.");
                            oConn = null;
                            break;
                        }
                        Thread.Sleep(5000);
                        oConn = new SqlConnection(sDSN);
                        oConn.Open();
                        bConnectionError = false;
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "RefreshRouteData() - Unable to connect to database to refresh routes - Will try again in 5 seconds", ex);
                        bConnectionError = true;
                        iRetryCount++;
                    }
                }
                #endregion
                if (oConn != null)
                {
                    try
                    {
                        _routeManagement.ReloadDataSets(oConn);
                    }
                    finally
                    {
                        oConn.Close();
                        oConn.Dispose();
                        oConn = null;
                    }
                }
            }
        }

        #endregion
        #region Driver Fatigue
        public DataTable GetNextDriverBreaks(int driverId)
        {
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.DataSet objDS = null;
            System.Data.SqlClient.SqlConnection oConn = null;
            try
            {
                #region Retrieve the next driver break data from the DB
                oConn = GetOpenConnection();
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                objSQLCmd = new SqlCommand("EXEC [fsp_NextBreakByDriverID] " + Convert.ToString(driverId), oConn);
                objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                objDS = new DataSet();
                objSQLDA.Fill(objDS);
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion
                if (objDS != null && objDS.Tables.Count > 0 && objDS.Tables[0].Rows.Count > 0)
                    return objDS.Tables[0];
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetNextDriverBreaks(int driverId = " + Convert.ToString(driverId) + ")", ex);
            }
            return null;
        }
        #endregion
        #region Place Lookup Tree and Map Ref Functions
        /// <summary>
        /// This method will be used to get a the nearest suburb name based on the vehicles location
        /// </summary>
        /// <param name="decimal latitude"></param>
        /// <param name="decimal longitude"></param>
        /// <param name="MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails"></param>
        /// <returns>void</returns>
        private void GetClosestSuburbName(decimal latitude, decimal longitude, ref MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess.PositionDetails oPosDetails)
        {
            PlaceName oResult = null;

            try
            {
                if (oPosDetails == null)
                    oPosDetails = new PositionDetails();
                if (oSuburbTree != null)
                {
                    //if lat and long are not zero then lookup suburb
                    if (latitude != 0 || longitude != 0)
                    {
                        oResult = oSuburbTree.Query(Convert.ToDouble(latitude), Convert.ToDouble(longitude));
                    }

                    if (oResult != null)
                    {
                        oPosDetails.MapRef = oResult.sMapRef;
                        oPosDetails.PlaceName = oResult.sPlaceName;
                        oPosDetails.Position = oResult.sPosition;
                        oPosDetails.Distance = Convert.ToDecimal(oResult.dDistance);
                        oPosDetails.SetPointGroupID = -1;
                        oPosDetails.SetPointGroupNumber = -1;
                        oPosDetails.SetPointID = -1;
                        oPosDetails.SetPointNumber = -1;
                        oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.None;
                    }
                    else
                    {
                        oPosDetails.PlaceName = "";
                        oPosDetails.Distance = 0;
                        oPosDetails.Position = "N";
                        oPosDetails.MapRef = "";
                        oPosDetails.SetPointGroupID = -1;
                        oPosDetails.SetPointGroupNumber = -1;
                        oPosDetails.SetPointID = -1;
                        oPosDetails.SetPointNumber = -1;
                        oPosDetails.PositionType = DatabaseAccess.PositionDetails.Category.Suburb;
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetClosestSuburbName(decimal latitude, decimal longitude, ref MTData.Transport.Service.Listener.GatewayListener.Database_Access.PositionDetails oPosDetails)", ex);
            }
        }
        /// <summary>
        /// This method will be used to get a the nearest map reference based on the vehicles location
        /// </summary>
        /// <param name="decimal dLatUnit"></param>
        /// <param name="decimal dLongUnit"></param>
        /// <returns>string</returns>
        public string GetMelwaysRef(decimal dLatUnit, decimal dLongUnit)
        {
            string sResult = "";

            try
            {
                if (oMelwayTree != null)
                {
                    lock (oSetGroupDataSetSync)
                    {
                        sResult = oMelwayTree.Query(Convert.ToDouble(dLatUnit), Convert.ToDouble(dLongUnit));
                    }
                }
                else
                {
                    _log.Info("Melways Ref List not loaded.");
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetMelwaysRef(decimal dLatUnit, decimal dLongUnit)", ex);
                sResult = "";
            }
            return sResult;
        }
        #endregion
        #region Insertion functions
        /// <summary>
        /// This method will be used to populate an insert command from the provided status packet and vehicle object
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <returns>string</returns>
        public string InsertNotificationRecord(GeneralGPPacket statusPacket, Vehicle v)
        {
            return InsertNotificationRecord(statusPacket, v, "", "");
        }
        /// This method will be used to populate an insert command from the provided status packet and vehicle object.  This call provides parameters
        /// to override the positions details.
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <param name="string sSuburbOverride"></param>
        /// <param name="string sSpare2Value"></param>
        /// <returns>string</returns>
        public string InsertNotificationRecord(GeneralGPPacket statusPacket, Vehicle v, string sSuburbOverride, string sSpare2Value)
        {
            string result = null;
            // Check it's worth inserting - is it from a reasonable unit?:
            // Swallow messages if required:
            if (!((statusPacket.iVehicleId == 0) || (statusPacket.cFleetId == 0) || (v.SwallowThisPacket) || (!statusPacket.mCurrentClock.IsDate(null)) || (!statusPacket.mFixClock.IsDate(null)) || (statusPacket.mFix.iDirection != GPPositionFix.INVALID_SPEED && (statusPacket.mFix.iDirection > 359 || statusPacket.mFix.iDirection < 0))))
            {
                lock (this)
                {
                    if (statusPacket.cMsgType == GeneralGPPacket.ALRM_UNIT_POWERUP)
                    {
                        #region GeneralGPPacket.ALRM_UNIT_POWERUP
                        if (statusPacket.mUnitStartupReport.SecondsSinceFirstIgnition > 0)
                        {
                            // If we have a missed ignition on event, then create a report from the data provided.
                            GeneralGPPacket oFlashReport = statusPacket.CreateCopy();
                            oFlashReport.cMsgType = GeneralGPPacket.STATUS_IGNITION_ON;
                            //oFlashReport.cInputNumber = (byte)0x01;
                            oFlashReport.mFix.dLatitude = Convert.ToDecimal(statusPacket.mUnitStartupReport.Latitude);
                            oFlashReport.mFix.dLongitude = Convert.ToDecimal(statusPacket.mUnitStartupReport.Longitude);
                            oFlashReport.mFix.cFlags = statusPacket.mUnitStartupReport.GPSFlags;
                            oFlashReport.mFixClock.FromDateTime(statusPacket.mUnitStartupReport.IgnitionOnGPSTime);
                            oFlashReport.mCurrentClock.FromDateTime(statusPacket.mUnitStartupReport.IgnitionOnDeviceTime);
                            oFlashReport.mExtendedVariableDetails.Populated = true;
                            oFlashReport.mExtendedVariableDetails.UnitStatus = (long) statusPacket.mUnitStartupReport.StatusFlags;
                            if (statusPacket.mUnitStartupReport.RefStatusFlags > 0)
                                oFlashReport.mExtendedVariableDetails.Refrigeration = new RefrigerationInfo((int) statusPacket.mUnitStartupReport.RefStatusFlags, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, new RefrigerationZoneInfo[0]);
                            oFlashReport.mExtendedVariableDetails.CurrentSetPointID = (int)statusPacket.mUnitStartupReport.CurrentSetPointID;
                            TrackingQueueItem missingItem = UnsafePrepareNotificationRecord(oFlashReport, v);
                            result = AddToMSMQ(missingItem);
                        }

                        statusPacket.mExtendedVariableDetails.Populated = true;
                        statusPacket.mExtendedVariableDetails.UnitStatus = (long)statusPacket.mUnitStartupReport.StatusFlags;
                        if (statusPacket.mUnitStartupReport.RefStatusFlags > 0)
                            statusPacket.mExtendedVariableDetails.Refrigeration = new RefrigerationInfo((int)statusPacket.mUnitStartupReport.RefStatusFlags, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, new RefrigerationZoneInfo[0]);
                        statusPacket.mExtendedVariableDetails.CurrentSetPointID = (int)statusPacket.mUnitStartupReport.CurrentSetPointID;

                        TrackingQueueItem item = UnsafePrepareNotificationRecord(statusPacket, v, sSuburbOverride, sSpare2Value);
                        if (item != null)
                        {
                            item.SubCommands.Add(PrepareVehicleStatupRecord(statusPacket));
                            result = AddToMSMQ(item);
                        }
                        #endregion
                    }
                    else
                    {
                        TrackingQueueItem item = UnsafePrepareNotificationRecord(statusPacket, v, sSuburbOverride, sSpare2Value);
                        if (item != null)
                        {
                            PrepareAdvancedECMInsert(statusPacket.mECMAdvanced, item);
                            PrepareRoadTypeInsert(statusPacket.RoadTypeData, item);
                            result = AddToMSMQ(item);
                        }
                    }
                }
            }
            else
            {
                if (!v.SwallowThisPacket)
                {
                    if (statusPacket.iVehicleId == 0)
                        result = "Packet Data does not have a valid vehicle ID - (F/V/Reason) " + ((int)statusPacket.cFleetId).ToString() + "/" + statusPacket.iVehicleId.ToString() + "/" + statusPacket.cMsgType.ToString();
                    if(statusPacket.cFleetId == 0)
                        result = "Packet Data does not have a valid fleet ID - (F/V/Reason) " + ((int)statusPacket.cFleetId).ToString() + "/" + statusPacket.iVehicleId.ToString() + "/" + statusPacket.cMsgType.ToString();
                    if(!statusPacket.mFixClock.IsDate(null))
                        result = "Packet Data does not have a valid GPS Fix - (F/V/Reason) " + ((int)statusPacket.cFleetId).ToString() + "/" + statusPacket.iVehicleId.ToString() + "/" + statusPacket.cMsgType.ToString();
                    if (statusPacket.mFix.iDirection != GPPositionFix.INVALID_SPEED && (statusPacket.mFix.iDirection > 359 || statusPacket.mFix.iDirection < 0))
                        result = "Packet Data does not have a valid Direction (" + statusPacket.mFix.iDirection.ToString() + ") - (F/V/Reason) " + ((int)statusPacket.cFleetId).ToString() + "/" + statusPacket.iVehicleId.ToString() + "/" + statusPacket.cMsgType.ToString();
                    if (!statusPacket.mCurrentClock.IsDate(null))
                        result = "Packet Data does not have a valid device time - (F/V/Reason) " + ((int)statusPacket.cFleetId).ToString() + "/" + statusPacket.iVehicleId.ToString() + "/" + statusPacket.cMsgType.ToString();
                }
            }
            return result;
        }
        
        public string InsertDriverPerformanceMetricRecord(GPDriverPerformanceMetrics dpmPacket)
        {
            string sErr = null;
            try
            {
                if (dpmPacket != null)
                {
                    DriverPerformanceMetrics metrics = new DriverPerformanceMetrics();
                    metrics.FleetID = (int)dpmPacket.cFleetId;
                    metrics.VehicleID = (int)dpmPacket.iVehicleId;
                    metrics.DeviceTime = dpmPacket.mCurrentClock.ToString();
                    metrics.DrivingTime = dpmPacket.DrivingTime;
                    metrics.IdleTime = dpmPacket.IdleTime;
                    metrics.ExcessiveIdleTime = dpmPacket.ExcessiveIdleTime;
                    metrics.IgnitionOnTime = dpmPacket.IgnitionOnTime;
                    metrics.WarmUpTime = dpmPacket.WarmUpTime;
                    metrics.BrakeCount = dpmPacket.BrakeCount;
                    metrics.BrakeRoll = dpmPacket.BrakeRoll;
                    metrics.HardBrakingCounts = dpmPacket.HardBrakingCount;
                    metrics.HardCorneringCounts = dpmPacket.HardCorneringCount;
                    metrics.HardAccelerationCounts = dpmPacket.HardAccelerationCount;
                    metrics.SustainedGForceEvents = dpmPacket.SustainedGForceEvents;
                    metrics.AccidentBufferEvents = dpmPacket.AccidentBufferEvents;
                    metrics.BrakeOnTime = dpmPacket.BrakeOnTime;
                    TrackingQueueItem item = new TrackingQueueItem();
                    item.SubCommands.Add(metrics);
                    sErr = AddToMSMQ(item);
                    if (sErr != null)
                    {
                        _log.Info(sClassName + "InsertDriverPerformanceMetricRecord(GPDriverPerformanceMetrics dpmPacket) Error : " + sErr);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "InsertDriverPerformanceMetricRecord(GPDriverPerformanceMetrics dpmPacket)", ex);
                sErr = ex.Message;
            }
            return sErr;
        }

        public string InsertDriverDataAccumlatorRecord(GPDriverDataAccumulator ddaPacket)
        {
            string sErr = null;
            try
            {
                if (ddaPacket.MetricData != null && ddaPacket.MetricData.Length > 0)
                {
                    DriverDataAccumulator data = new DriverDataAccumulator();
                    data.FleetID = (int)ddaPacket.cFleetId;
                    data.VehicleID = (int)ddaPacket.iVehicleId;
                    data.DeviceTime = ddaPacket.mCurrentClock.ToString();
                    data.SPNType = ddaPacket.SPNType;
                    data.SPNValue = ddaPacket.SPNValue;
                    data.DataAccumulatorTypeID = ddaPacket.AccumulatorType;
                    string sData = BitConverter.ToString(ddaPacket.MetricData, 0, ddaPacket.MetricData.Length);
                    sData = sData.Replace("-", "");
                    data.Data = sData;

                    TrackingQueueItem item = new TrackingQueueItem();
                    item.SubCommands.Add(data);
                    sErr = AddToMSMQ(item);
                    if (sErr != null)
                    {
                        _log.Info(sClassName + "InsertDriverDataAccumlatorRecord(GPDriverDataAccumulator statusPacket) Error : " + sErr);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "InsertDriverDataAccumlatorRecord(GPDriverDataAccumulator statusPacket)", ex);
                sErr = ex.Message;
            }
            return sErr;
        }

        private VehicleStartup PrepareVehicleStatupRecord(GeneralGPPacket statusPacket)
        {
            VehicleStartup vs = new VehicleStartup();
            try
            {
                vs.LastKnownGPSTime = statusPacket.mUnitStartupReport.GPSTime.ToString("MM/dd/yyyy HH:mm:ss");
                vs.LastKnownDeviceTime = statusPacket.mUnitStartupReport.DeviceTime.ToString("MM/dd/yyyy HH:mm:ss");
                vs.StartupReportTime = statusPacket.mCurrentClock.ToDateTime().ToString("MM/dd/yyyy HH:mm:ss");
                vs.LastKnownLat = statusPacket.mUnitStartupReport.Latitude;
                vs.LastKnownLong = statusPacket.mUnitStartupReport.Longitude;
                vs.Startup = statusPacket.mUnitStartupReport.SecondsSincePowerup;
                vs.FirstIgnition = statusPacket.mUnitStartupReport.SecondsSinceFirstIgnition;
                vs.GPRSActive = statusPacket.mUnitStartupReport.SecondsSinceGPRSActive;
                vs.GPSAquired = statusPacket.mUnitStartupReport.SecondsSinceGPSAquired;
                vs.FirstCTA = statusPacket.mUnitStartupReport.SecondsSinceFirstCTA;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "PrepareVehicleStatupRecord(GeneralGPPacket statusPacket)", ex);
                vs = null;
            }
            return vs;
        }

        private DriverPointsRuleBreak PrepareDriverPointsRuleBreak(DriverPointsRuleBreakPacket packet)
        {
            DriverPointsRuleBreak dp = new DriverPointsRuleBreak();
            try
            {
                dp.RuleId = packet.RuleId;
                if (packet.mCurrentClock.IsDate(null))
                    dp.RuleBreakTime = packet.mCurrentClock.ToString();
                else
                    dp.RuleBreakTime = DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss");
                dp.VehicleID = (int)packet.iVehicleId;
                dp.FleetID = (int)packet.cFleetId;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "PrepareDriverPointsRuleBreak(DriverPointsRuleBreakPacket packet)", ex);
                dp = null;
            }
            return dp;
        }
        private RuleBreach PrepareRuleBreachInsert(GenericPacket packet)
        {
            RuleBreach rb = new RuleBreach();
            try
            {
                rb.RuleSetId = packet.RuleSetId;
                rb.RuleSetVersion = packet.RuleSetVersion;
                rb.RuleId = packet.RuleId;
                rb.LocationFileId = packet.LocationFileId;
                rb.LocationFileVersion = packet.LocationFileVersion;
                rb.LocationId = packet.LocationId;
                rb.Generic = packet.Generic;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "PrepareDriverPointsRuleBreak(DriverPointsRuleBreakPacket packet)", ex);
                rb = null;
            }
            return rb;
        }

        /// <summary>
        /// This method will be used to populate an insert command from the provided status packet and vehicle object.
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <returns>string</returns>
        private TrackingQueueItem UnsafePrepareNotificationRecord(GeneralGPPacket statusPacket, Vehicle v)
        {
            return UnsafePrepareNotificationRecord(statusPacket, v, "", "");
        }
        /// <summary>
        /// This method will be used to populate an insert command from the provided status packet and vehicle object.  This call provides parameters
        /// to override the positions details.
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <param name="string sSuburbOverride"></param>
        /// <param name="string sSpare2Value"></param>
        /// <returns>string</returns>
        private TrackingQueueItem UnsafePrepareNotificationRecord(GeneralGPPacket statusPacket, Vehicle v, string sSuburbOverride, string sSpare2Value)
        {
            PositionDetails position = null;
            if (sSuburbOverride == "")
            {
                position = GetPositionForVehicle(v, statusPacket, statusPacket.mFix.dLatitude, statusPacket.mFix.dLongitude);
            }
            return PopulateInsertCommand(statusPacket, v, 0, 0, 0, sSpare2Value);
        }
        /// <summary>
        /// This method will be used to populate an insert command from the provided status packet and vehicle object. 
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <param name="int setPointGroupID"></param>
        /// <param name="int setPointID"></param>
        /// <param name="int setPointNumber"></param>
        /// <returns>string</returns>
        private TrackingQueueItem PopulateInsertCommand(GeneralGPPacket statusPacket, Vehicle v, int setPointGroupID, int setPointID, int setPointNumber)
        {
            return PopulateInsertCommand(statusPacket, v, setPointGroupID, setPointID, setPointNumber, "");
        }
        /// <summary>
        /// This method will be used to populate an insert command from the provided status packet and vehicle object. 
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <param name="int setPointGroupID"></param>
        /// <param name="int setPointID"></param>
        /// <param name="int setPointNumber"></param>
        /// <param name="string sSpare2Value"></param>
        /// <returns>string</returns>
        private TrackingQueueItem PopulateInsertCommand(GeneralGPPacket statusPacket, Vehicle v, int setPointGroupID, int setPointID, int setPointNumber, string sSpare2Value)
        {
            VehicleNotification vn = new VehicleNotification();
            string sUserDefinedValue = "";
            PositionDetails position = v.oPos;
            string sUserInfoTag = v.sUserInfo;
            string sUserDesc = v.VehicleTag;
            RefrigerationInfo oRefrigerationInfo = null;
            RefrigerationZoneInfo[] oRefrigerationZones = null;
            RefrigerationZoneInfo oRefZone1Info = null;
            RefrigerationZoneInfo oRefZone2Info = null;
            RefrigerationZoneInfo oRefZone3Info = null;

            #region Insert the time and position values
            vn.VehicleID = (int)statusPacket.iVehicleId;
            vn.FleetID = statusPacket.cFleetId;
            if (statusPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT)
            {
                vn.ReasonID = Convert.ToInt32((statusPacket.cRefrigerationZone << 16) + (statusPacket.cInputNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || statusPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END || 
                statusPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT || statusPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN || 
                statusPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT || statusPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART || 
                statusPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT || statusPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY ||
                statusPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION || statusPacket.cMsgType == GeneralGPPacket.TAMPER || statusPacket.cMsgType == GeneralGPPacket.IAP_UPDATE)
            {
                vn.ReasonID = Convert.ToInt32((statusPacket.cInputNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS)
            {
                // The cInputNumber field holds the flag for this packet type, 0 = Start, 1 = End
                vn.ReasonID = Convert.ToInt32((statusPacket.cInputNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
            {
                // The cInputNumber field holds the speed zone flag for this packet type, 
                vn.ReasonID = Convert.ToInt32(((statusPacket.bSpeedZoneID & 0x0F) << 12) + ((statusPacket.bSpeedZoneFlags & 0x0F) << 8) + (statusPacket.cMsgType & 0xFF));
                // Store the zone speed limit in the button status field
                vn.ButtonStatus = Convert.ToInt16(statusPacket.bSpeedZoneLimit);
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE)
            {
                // The cInputNumber field holds the GPRS Failure flags.  ReasonID = ((1 << [cInputNumber]) << 8) + 0xF6
                vn.ReasonID = Convert.ToInt32(((1 << ((int)statusPacket.cInputNumber)) << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF || statusPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON || statusPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES || statusPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD || statusPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD)
            {
                vn.ReasonID = Convert.ToInt32((statusPacket.cInputNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.STATUS_PHONECALL_PACKET || statusPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_START || statusPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_USAGE_END || statusPacket.cMsgType == GeneralGPPacket.STATUS_JOB_PACKET)
            {
                vn.ReasonID = Convert.ToInt32((statusPacket.cButtonNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.STATUS_GLOBAL_STAR)
            {
                vn.ReasonID = Convert.ToInt32((statusPacket.cButtonNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION && statusPacket.cButtonNumber == 0x01)
            {
                vn.ReasonID = Convert.ToInt32((statusPacket.cButtonNumber << 8) + (statusPacket.cMsgType & 0xFF));
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR && statusPacket.EcmError != null && statusPacket.EcmError.Cancelled)
            {
                vn.ReasonID = 295;
            }
            else if (statusPacket.cMsgType == GeneralGPPacket.WAYPT_OVER_SPEED)
            {
                vn.ReasonID = Convert.ToInt32(((statusPacket.bSpeedZoneID & 0x0F) << 12) + ((statusPacket.bSpeedZoneFlags & 0x0F) << 8) + (statusPacket.cMsgType & 0xFF));
				vn.ButtonStatus = Convert.ToInt16(statusPacket.bSpeedZoneLimit);
            }
            else
            {
                vn.ReasonID = ((statusPacket.mStatus.cLightStatus << 12) + (statusPacket.cMsgType & 0xFF));
            }

            vn.GPSTime = statusPacket.mFixClock.ToNullableDateTime();
            vn.DeviceTime = statusPacket.mCurrentClock.ToNullableDateTime();
            vn.Latitude = (double)statusPacket.mFix.dLatitude;
            vn.Longitude = (double)statusPacket.mFix.dLongitude;
            vn.Direction = statusPacket.mFix.iDirection;
            vn.Speed = statusPacket.mFix.cSpeed;
            vn.RawGPS = statusPacket.sRawGPS;
            #endregion
            #region Insert the status values
            vn.InputStatus = statusPacket.mStatus.cInputStatus;
            vn.OutputStatus = statusPacket.mStatus.cOutputStatus;
            vn.LightStatus = statusPacket.mStatus.cLightStatus;
            if (statusPacket.cMsgType != GeneralGPPacket.DRIVER_OVERSPEED_REPORT)
                vn.ButtonStatus = statusPacket.mStatus.cButtonStatus;
            vn.MaxSpeed = statusPacket.mDistance.cMaxSpeed;
            vn.SpeedAcc = (int)statusPacket.mDistance.lSpeedAccumulator;
            vn.Samples = statusPacket.mDistance.iSamples;
            vn.Spare1 = statusPacket.mExtendedValues.ToDatabaseFormatString();
            #endregion
            #region Insert the Waypoint details
            if (setPointGroupID > 0 && setPointID > 0)
            {
                vn.Status = setPointGroupID;
                vn.Flags = setPointNumber;
            }
            else
            {
                vn.Status = 0;
                vn.Flags = Convert.ToInt16(statusPacket.mFix.cFlags); // Can be used to detect invalid GPS
            }
            #endregion
            #region Insert the user info tag.  This only applies to reports that are not from flash
            if (!statusPacket.bArchivalData)
            {
                vn.Spare3 = sUserInfoTag;
            }
            else
            {
                vn.Spare3 = "";
            }
            #endregion
            #region Insert the vehicle description
            if (sUserDesc != "")
                vn.Spare4 = sUserDesc;
            else
                vn.Spare4 = "";
            #endregion
            #region set SPARE 6
            if (statusPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY && statusPacket.cInputNumber >= 5 && statusPacket.cInputNumber <= 8)
                vn.Spare6 = string.Format("{0}|{1}", statusPacket.OldReportingFrequency, statusPacket.NewReportingFrequency);
            else if (statusPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION && statusPacket.cInputNumber == 0)
                vn.Spare6 = statusPacket.GFCalibrationReason.ToString();
            else if (statusPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION && statusPacket.cInputNumber == 1)
                vn.Spare6 = string.Format("{0}|{1}|{2}|{3}|{4}", statusPacket.GFCalibrationReason, statusPacket.OldGFCalibration1, statusPacket.OldGFCalibration2, statusPacket.NewGFCalibration1, statusPacket.NewGFCalibration2);
            else if (statusPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION && statusPacket.cInputNumber == 2)
                vn.Spare6 = string.Format("{0}|{1}|{2}", statusPacket.GFCalibrationReason, statusPacket.OldGFCalibration1, statusPacket.NewGFCalibration1);
            else if (statusPacket.cMsgType == GeneralGPPacket.GPS_SETTINGS)
                vn.Spare6 = statusPacket.GPSSettingsString;
            else if (statusPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC_LEVEL)
                vn.Spare6 = statusPacket.DiagnosticsLevel.ToString();
            else if (statusPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC)
                vn.Spare6 = string.Format("{0}|{1}|{2}", statusPacket.DiagnosticsHardwareType, statusPacket.DiagnosticsLevel, statusPacket.Diagnostics);
            else
                vn.Spare6 = "";
            #endregion
            #region Insert the job reference number
            if (sSpare2Value != "")
                vn.Spare2 = sSpare2Value;
            else
                vn.Spare2 = "0";
            #endregion
            #region Spare10 holds the index of the cached Accident..
            if (statusPacket.cMsgType == GeneralGPPacket.STATUS_ACCIDENT_NEW || statusPacket.cMsgType == GeneralGPPacket.POSSIBLE_ACCIDENT)
                vn.Spare10 = statusPacket.cAccidentIndex.ToString();
            else
                vn.Spare10 = null;
            #endregion
            #region Engine details for Transport Solution
            // We are working with a Transport database -
            // insert the additional data:
            // Engine operational parameters:
            vn.MaxEngCoolTemp = statusPacket.mEngineData.iCoolantTemperature;
            vn.MaxEngOilTemp = statusPacket.mEngineData.iOilTemperature;
            vn.MinOilPressure = Convert.ToSingle(statusPacket.mEngineData.iOilPressure);

            vn.GearPosition = statusPacket.mEngineData.iGear;
            vn.MaxEngSpeed = statusPacket.mEngineData.iMaxRPM;
            vn.BrakeHits = statusPacket.mEngineData.iBrakeApplications;

            // G Forces:
            vn.MaxF = statusPacket.mEngineData.fGForceFront;
            vn.MaxB = statusPacket.mEngineData.fGForceBack;
            vn.MaxLR = statusPacket.mEngineData.fGForceLeftRight;

            // Not in standard status report:
            vn.TotalEngHours = 0;
            vn.TotalFuelUsed = statusPacket.mTransportExtraDetails.iTotalFuelUsed;
            vn.TripFuel = 0F;
            if (statusPacket.mEngineSummaryData != null)
                vn.FuelEconomy = statusPacket.mEngineSummaryData.fFuelEconomy;
            else
                vn.FuelEconomy = 0F;

            if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
            {
                vn.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
            }
            else
            {
                vn.Odometer = (int)statusPacket.mTransportExtraDetails.iOdometer;
            }


            if (statusPacket.iEngineValue != 0)
            {
                // Only the case if there is an Engine Error Code
                // Insert the value into Status:
                vn.Status = (int)statusPacket.iEngineValue;
            }
            #endregion
            #region Insert the units position details
			vn.StreetName = "";  // Not Implemented
            if (statusPacket.mFix.dLatitude == 0 && statusPacket.mFix.dLongitude == 0)
            {
                vn.DistanceFromNearest = 0;
                vn.PositionFromNearest = "N";
                vn.Suburb = "";
                vn.MapReference = "";
            }
            else
            {
                vn.DistanceFromNearest = (double)v.oPos.Distance;
                vn.PositionFromNearest = v.oPos.Position;
                if (!String.IsNullOrEmpty(statusPacket.StreetName))
                    vn.StreetName = statusPacket.StreetName;
                else
                    vn.StreetName = position.PlaceName;
                vn.Suburb = v.oPos.PlaceName;
                vn.MapReference = v.oPos.MapRef;
            }

            if (statusPacket.RoadTypeData != null)
            {
                vn.StreetName = string.IsNullOrEmpty(statusPacket.RoadTypeData.Street) ? "" : statusPacket.RoadTypeData.Street;
                vn.Suburb = string.IsNullOrEmpty(statusPacket.RoadTypeData.Suburb) ? "" : statusPacket.RoadTypeData.Suburb;
                vn.MapReference = "";
                vn.DistanceFromNearest = 0;
                vn.PositionFromNearest = "N";
                vn.SpeedLimit = statusPacket.RoadTypeData.OverrideSpeedLimit > 0 ? statusPacket.RoadTypeData.OverrideSpeedLimit : statusPacket.RoadTypeData.SpeedLimit;
            }

            #endregion
            #region New "State" info
            // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
            if (_userDefinedDateFormat != null)
                sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
            else
                sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');
            // Insert the count if necessary into the UserDefined field
            if (statusPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES)
                sUserDefinedValue += "%" + statusPacket.iPulseCount.ToString();
            else
                sUserDefinedValue += "%0";
            vn.UserDefined = sUserDefinedValue;
            #endregion
            if (dbVer > 5.7)
            {
                #region Insert the 5.8+ database field values
                //Speed Zone Details
                vn.SpeedZone1 = statusPacket.mTransportExtraDetails.iSpeedZone1;
                vn.SpeedZone2 = statusPacket.mTransportExtraDetails.iSpeedZone2;
                vn.SpeedZone3 = statusPacket.mTransportExtraDetails.iSpeedZone3;
                vn.SpeedZone4 = statusPacket.mTransportExtraDetails.iSpeedZone4;

                //RPM Zone Details
                vn.RPMZone1 = statusPacket.mTransportExtraDetails.iRPMZone1;
                vn.RPMZone2 = statusPacket.mTransportExtraDetails.iRPMZone2;
                vn.RPMZone3 = statusPacket.mTransportExtraDetails.iRPMZone3;
                vn.RPMZone4 = statusPacket.mTransportExtraDetails.iRPMZone4;

                vn.BatteryVolts = statusPacket.mTransportExtraDetails.iBatteryVolts;
                vn.BrakeUsageSeconds = statusPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                vn.CANVehicleSpeed = statusPacket.mTransportExtraDetails.iCANVehicleSpeed;

                byte[] bNoTrailer = new byte[6];

                if (statusPacket.mTrailerTrack.oTrailers.Count >= 1)
                    vn.Trailer1 = (byte[])statusPacket.mTrailerTrack.oTrailers[0];
                else
                    vn.Trailer1 = bNoTrailer;
                if (statusPacket.mTrailerTrack.oTrailers.Count >= 2)
                    vn.Trailer2 = (byte[])statusPacket.mTrailerTrack.oTrailers[1];
                else
                    vn.Trailer2 = bNoTrailer;
                if (statusPacket.mTrailerTrack.oTrailers.Count >= 3)
                    vn.Trailer3 = (byte[])statusPacket.mTrailerTrack.oTrailers[2];
                else
                    vn.Trailer3 = bNoTrailer;
                if (statusPacket.mTrailerTrack.oTrailers.Count >= 4)
                    vn.Trailer4 = (byte[])statusPacket.mTrailerTrack.oTrailers[3];
                else
                    vn.Trailer4 = bNoTrailer;
                if (statusPacket.mTrailerTrack.oTrailers.Count >= 5)
                    vn.Trailer5 = (byte[])statusPacket.mTrailerTrack.oTrailers[4];
                else
                    vn.Trailer5 = bNoTrailer;
                #endregion
            }
            if (dbVer >= 5.9)
            {
                #region Populate the v5.9 fields
                vn.StatusVehicle = v.GetUnitStateForReport(statusPacket);
                vn.StatusAuxiliary = v.GetAuxiliaryStateForReport(statusPacket);
                vn.SetPointGroupID = (setPointGroupID > 0) ? setPointNumber : v.oPos.SetPointGroupID;
                vn.SetPointID = (setPointID > 0) ? setPointID : v.oPos.SetPointID;
                vn.VehicleScheduleID = statusPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID;
                vn.CheckPointIndex = statusPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex;
                #endregion
            }
            if (dbVer >= 6.0)
            {
                #region Refrigeration Values
                if (statusPacket.mExtendedVariableDetails.Refrigeration != null)
                {
                    oRefrigerationInfo = statusPacket.mExtendedVariableDetails.Refrigeration;
                    oRefrigerationZones = null;
                    oRefZone1Info = null;
                    oRefZone2Info = null;
                    oRefZone3Info = null;
                    if (oRefrigerationInfo.RefrigerationZones != null)
                    {
                        oRefrigerationZones = oRefrigerationInfo.RefrigerationZones;
                        if (oRefrigerationZones.Length >= 1)
                            oRefZone1Info = oRefrigerationZones[0];
                        if (oRefrigerationZones.Length >= 2)
                            oRefZone2Info = oRefrigerationZones[1];
                        if (oRefrigerationZones.Length >= 3)
                            oRefZone3Info = oRefrigerationZones[2];
                    }

                    vn.ReferFlags = oRefrigerationInfo.RefrigerationFlags;
                    vn.ReferFuelPercentage = (double)oRefrigerationInfo.FuelPercentage;
                    vn.ReferBatteryVolts = (double)oRefrigerationInfo.BatteryVolts;
                    vn.ReferInput1 = oRefrigerationInfo.Input1On;
                    vn.ReferInput2 = oRefrigerationInfo.Input2On;
                    vn.ReferInput3 = oRefrigerationInfo.Input3On;
                    vn.ReferInput4 = oRefrigerationInfo.Input4On;
                    vn.ReferSensorsAvailable = oRefrigerationInfo.SensorsAvailable;
                    vn.ReferHumidity = (double)oRefrigerationInfo.Humidity;
                    vn.ReferSensor1 = oRefrigerationInfo.Sensor1Temperature;
                    vn.ReferSensor2 = oRefrigerationInfo.Sensor2Temperature;
                    vn.ReferSensor3 = oRefrigerationInfo.Sensor3Temperature;
                    vn.ReferSensor4 = oRefrigerationInfo.Sensor4Temperature;
                    vn.ReferSensor5 = oRefrigerationInfo.Sensor5Temperature;
                    vn.ReferSensor6 = oRefrigerationInfo.Sensor6Temperature;

                    if (oRefrigerationZones != null)
                    {
                        if (oRefZone1Info != null)
                        {
                            vn.ReferZ1Zone = oRefZone1Info.ZoneNumber;
                            vn.ReferZ1OperatingMode = (int)oRefZone1Info.OperatingMode;
                            vn.ReferZ1Alarms = (int)oRefZone1Info.ZoneAlarms;
                            vn.ReferZ1SensorsAvailable = oRefZone1Info.SensorsAvailable;
                            vn.ReferZ1ReturnAir1 = oRefZone1Info.ReturnAir1;
                            vn.ReferZ1ReturnAir2 = oRefZone1Info.ReturnAir2;
                            vn.ReferZ1SupplyAir1 = oRefZone1Info.SupplyAir1;
                            vn.ReferZ1SupplyAir2 = oRefZone1Info.SupplyAir2;
                            vn.ReferZ1Setpoint = oRefZone1Info.Setpoint;
                            vn.ReferZ1Evap = oRefZone1Info.EvaporatorCoil;
                        }
                        if (oRefZone2Info != null)
                        {
                            vn.ReferZ2Zone = oRefZone2Info.ZoneNumber;
                            vn.ReferZ2OperatingMode = (int)oRefZone2Info.OperatingMode;
                            vn.ReferZ2Alarms = (int)oRefZone2Info.ZoneAlarms;
                            vn.ReferZ2SensorsAvailable = oRefZone2Info.SensorsAvailable;
                            vn.ReferZ2ReturnAir1 = oRefZone2Info.ReturnAir1;
                            vn.ReferZ2ReturnAir2 = oRefZone2Info.ReturnAir2;
                            vn.ReferZ2SupplyAir1 = oRefZone2Info.SupplyAir1;
                            vn.ReferZ2SupplyAir2 = oRefZone2Info.SupplyAir2;
                            vn.ReferZ2Setpoint = oRefZone2Info.Setpoint;
                            vn.ReferZ2Evap = oRefZone2Info.EvaporatorCoil;
                        }
                        if (oRefZone3Info != null)
                        {
                            vn.ReferZ3Zone = oRefZone3Info.ZoneNumber;
                            vn.ReferZ3OperatingMode = (int)oRefZone3Info.OperatingMode;
                            vn.ReferZ3Alarms = (int)oRefZone3Info.ZoneAlarms;
                            vn.ReferZ3SensorsAvailable = oRefZone3Info.SensorsAvailable;
                            vn.ReferZ3ReturnAir1 = oRefZone3Info.ReturnAir1;
                            vn.ReferZ3ReturnAir2 = oRefZone3Info.ReturnAir2;
                            vn.ReferZ3SupplyAir1 = oRefZone3Info.SupplyAir1;
                            vn.ReferZ3SupplyAir2 = oRefZone3Info.SupplyAir2;
                            vn.ReferZ3Setpoint = oRefZone3Info.Setpoint;
                            vn.ReferZ3Evap = oRefZone3Info.EvaporatorCoil;
                        }
                    }
                }
                #endregion
            }
            if (dbVer >= 6.16)
            {
                #region Additional Advanced ECM fields
                if (eSendECMLiveUpdate != null && statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.ECMAdvancedItems != null && statusPacket.mECMAdvanced.ECMAdvancedItems.Length > 0 && v != null && v.oPos != null)
                {
                    eSendECMLiveUpdate((int) statusPacket.cFleetId, (int) statusPacket.iVehicleId, statusPacket.mCurrentClock.ToDateTime(), v.oPos.PlaceName, Convert.ToSingle(statusPacket.mFix.dLatitude), Convert.ToSingle(statusPacket.mFix.dLongitude), statusPacket.mECMAdvanced.ECMAdvancedItems);
                }
                vn.HDOP = statusPacket.mECMAdvanced.SatellitesHDOP();
                vn.NumOfSatellites = statusPacket.mECMAdvanced.SatellitesInView();
                vn.MaxZ = statusPacket.mECMAdvanced.GForceZAxis();
                vn.Altitude = statusPacket.mECMAdvanced.Altitude();
                vn.GForceTemperature = statusPacket.mECMAdvanced.GForceTemperature();
                vn.ReportFromFlash = statusPacket.bArchivalData;
                // Override previous values if the are available in the advanced data.
                // Common ECM values
                if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                {
                    vn.GPSOdometer = true;
                    vn.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                }
                else
                {
                    vn.GPSOdometer = false;
                    if (statusPacket.mECMAdvanced.Odometer() > 0) vn.Odometer = (int)statusPacket.mECMAdvanced.Odometer();
                }
                if (statusPacket.mECMAdvanced.TotalEngHours() > 0) vn.TotalEngHours = (int)statusPacket.mECMAdvanced.TotalEngHours();
                if (statusPacket.mECMAdvanced.TotalFuelUsed() > 0) vn.TotalFuelUsed = statusPacket.mECMAdvanced.TotalFuelUsed();
                if (statusPacket.mECMAdvanced.TripFuel() > 0) vn.TripFuel = statusPacket.mECMAdvanced.TripFuel();
                if (statusPacket.mECMAdvanced.FuelEconomy() > 0) vn.FuelEconomy = statusPacket.mECMAdvanced.FuelEconomy();
                if (statusPacket.mECMAdvanced.MaxEngCoolTemp() > 0) vn.MaxEngCoolTemp = statusPacket.mECMAdvanced.MaxEngCoolTemp();
                if (statusPacket.mECMAdvanced.MaxEngOilTemp() > 0) vn.MaxEngOilTemp = statusPacket.mECMAdvanced.MaxEngOilTemp();
                if (statusPacket.mECMAdvanced.MinOilPressure() > 0) vn.MinOilPressure = statusPacket.mECMAdvanced.MinOilPressure();
                if (statusPacket.mECMAdvanced.GearPosition() > 0) vn.GearPosition = statusPacket.mECMAdvanced.GearPosition();
                if (statusPacket.mECMAdvanced.MaxEngSpeed() > 0) vn.MaxEngSpeed = statusPacket.mECMAdvanced.MaxEngSpeed();
                if (statusPacket.mECMAdvanced.BrakeHits() > 0) vn.BrakeHits = statusPacket.mECMAdvanced.BrakeHits();
                if (statusPacket.mECMAdvanced.BatteryVolts() > 0) vn.BatteryVolts = statusPacket.mECMAdvanced.BatteryVolts();
                if (statusPacket.mECMAdvanced.BrakeUsageSeconds() > 0) vn.BrakeUsageSeconds = statusPacket.mECMAdvanced.BrakeUsageSeconds();
                if (statusPacket.mECMAdvanced.ECMVehicleSpeed(true) > 0) vn.CANVehicleSpeed = statusPacket.mECMAdvanced.ECMVehicleSpeed(true);
                // G-Force Readings
                if (statusPacket.mECMAdvanced.HasGForce)
                {
                    vn.MaxF = statusPacket.mECMAdvanced.GForceFront();
                    vn.MaxB = statusPacket.mECMAdvanced.GForceBack();
                    vn.MaxLR = statusPacket.mECMAdvanced.GForceLeftRight();
                }
                // RPM Zone Readings
                if (statusPacket.mECMAdvanced.RPMZone1() > 0) vn.RPMZone1 = statusPacket.mECMAdvanced.RPMZone1();
                if (statusPacket.mECMAdvanced.RPMZone2() > 0) vn.RPMZone2 = statusPacket.mECMAdvanced.RPMZone2();
                if (statusPacket.mECMAdvanced.RPMZone3() > 0) vn.RPMZone3 = statusPacket.mECMAdvanced.RPMZone3();
                if (statusPacket.mECMAdvanced.RPMZone4() > 0) vn.RPMZone4 = statusPacket.mECMAdvanced.RPMZone4();
                // Speed Zone Readings
                if (statusPacket.mECMAdvanced.SpeedZone1() > 0) vn.SpeedZone1 = statusPacket.mECMAdvanced.SpeedZone1();
                if (statusPacket.mECMAdvanced.SpeedZone2() > 0) vn.SpeedZone2 = statusPacket.mECMAdvanced.SpeedZone2();
                if (statusPacket.mECMAdvanced.SpeedZone3() > 0) vn.SpeedZone3 = statusPacket.mECMAdvanced.SpeedZone3();
                if (statusPacket.mECMAdvanced.SpeedZone4() > 0) vn.SpeedZone4 = statusPacket.mECMAdvanced.SpeedZone4();
                // Extended I/O
                if (statusPacket.mECMAdvanced.ExtendedIO1() > 0 || statusPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(statusPacket.cFleetId)) > 0 || statusPacket.mECMAdvanced.ExtendedIO3() > 0 || statusPacket.mECMAdvanced.ExtendedIO4() > 0)
                    vn.Spare1 = statusPacket.mECMAdvanced.ExtendedIO1() + "%" + statusPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(statusPacket.cFleetId)) + "%" + statusPacket.mECMAdvanced.ExtendedIO3() + "%" + statusPacket.mECMAdvanced.ExtendedIO4();
                #endregion
            }

            TrackingQueueItem item = new TrackingQueueItem();
            item.InsertIntoVehicleNotification = vn;
            return item;
        }

        private void PrepareAdvancedECMInsert(GPECMAdvanced ecmData, TrackingQueueItem item)
        {
            try
            {
                if (ecmData != null && ecmData.Count > 0)
                {
                    for (int X = 0; X < ecmData.Count; X++)
                    {
                        AdvancedEcm ecm = new AdvancedEcm();
                        GPECMAdvancedItem oItem = ecmData[X];
                        ecm.SourceType = (int)oItem.SourceType;
                        ecm.SpnId = (int)oItem.SPN_ID;
                        ecm.Flags = (int)oItem.Flags;
                        ecm.Value = oItem.RawData;
                        if (mSPNSignedDataType != null && mSPNSignedDataType.ContainsKey((int)oItem.SourceType) && mSPNSignedDataType[(int)oItem.SourceType].ContainsKey((int)oItem.SPN_ID))
                            ecm.ValueFloat = oItem.SignedRawValue(mSPNSignedDataType[(int)oItem.SourceType][(int)oItem.SPN_ID]);
                        else
                            ecm.ValueFloat = oItem.RawValue;

                        item.SubCommands.Add(ecm);
                    }
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "InsertAdvancedECMInsert(GeneralGPPacket statusPacket)", ex);
            }
        }

        private void PrepareDvrStatusInsert(GenericPacket genericPacket, TrackingQueueItem item)
        {
            try
            {
                if (genericPacket.DvrStatusVersion == 1)
                {
                    DvrStatusItem statusItem = new DvrStatusItem
                    {
                        FleetID = genericPacket.cFleetId,
                        VehicleID = genericPacket.iVehicleId,
                        DeviceName = genericPacket.DvrStatusDeviceName,
                        DeviceType = 1, // Default to AutoIT DVR device
                        DateTime = item.InsertIntoVehicleNotification.DeviceTime ?? DateTime.UtcNow,

                        ErrorCode = genericPacket.DvrErrorCode,
                        Connected = genericPacket.DvrStatusConnected,

                        RecordOn = genericPacket.DvrStatusRecording,
                        VideoConnectedChannel = Convert.ToInt16(genericPacket.DvrStatusVideoConnectedChannel),
                        VideoRecordChannel = Convert.ToInt16(genericPacket.DvrStatusVideoRecordChannel),

                        HddOk = genericPacket.DvrStatusHddOk,
                        HddSize = Convert.ToInt16(genericPacket.DvrStatusHddSize),
                        HddFree = Convert.ToInt16(genericPacket.DvrStatusHddFree),
                        TimeOffset = genericPacket.DvrStatusTimeOffset,
                        FirmwareVersion = genericPacket.DvrStatusFirmwareVersion,
                        Model = genericPacket.DvrStatusModel,
                        VideoFormat = genericPacket.DvrStatusVideoFormat,
                        IsHealthStatus = genericPacket.DvrStatusIsHealthStatus
                    };

                    item.SubCommands.Add(statusItem);
                }
                else
                {
                    _log.ErrorFormat("PrepareDvrStatusInsert. Received unknown version number ({0})", genericPacket.DvrStatusVersion);
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "PrepareDvrStatusInsert(GenericPacket genericPacket, TrackingQueueItem item)", ex);
            }
        }

        private void PrepareRoadTypeInsert(RoadTypeData roadTypeData, TrackingQueueItem item)
        {
            try
            {
                if (roadTypeData != null)
                {
                    VehicleNotificationRoadData vehicleNotificationRoadData = new VehicleNotificationRoadData();
                    vehicleNotificationRoadData.SegmentId = roadTypeData.SegmentId;
                    vehicleNotificationRoadData.SpeedLimit = roadTypeData.SpeedLimit;
                    vehicleNotificationRoadData.OverrideSpeedLimit = roadTypeData.OverrideSpeedLimit;                    
                    vehicleNotificationRoadData.SchoolZone = roadTypeData.SchoolZone;
                    vehicleNotificationRoadData.IsPrivate = roadTypeData.IsPrivate;
                    vehicleNotificationRoadData.RoadType = roadTypeData.RoadType;
                    vehicleNotificationRoadData.StreetNumber = roadTypeData.StreetNumber ?? null;
                    vehicleNotificationRoadData.Street = roadTypeData.Street ?? null;
                    vehicleNotificationRoadData.Suburb = roadTypeData.Suburb ?? null;
                    vehicleNotificationRoadData.City = roadTypeData.City ?? null;
                    vehicleNotificationRoadData.State = roadTypeData.State ?? null;
                    vehicleNotificationRoadData.Country = roadTypeData.Country ?? null;
                    vehicleNotificationRoadData.PostCode = roadTypeData.PostCode ?? null;

                    item.SubCommands.Add(vehicleNotificationRoadData);
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "PrepareRoadTypeInsert(GeneralGPPacket statusPacket)", ex);                
            }
        }

        /// <summary>
        /// This method will be used to populate an insert command for a packet that is in error.
        /// </summary>
        /// <param name="GatewayProtocolPacket statusPacket"></param>
        /// <returns>string</returns>
        public string InsertSystemErrorRecord(GatewayProtocolPacket statusPacket)
        {
            if ((statusPacket.iVehicleId == 0) || (statusPacket.cFleetId == 0)) return null;

            lock (this)
            {
                VehicleNotification vn = new VehicleNotification();
                vn.VehicleID = (int)statusPacket.iVehicleId;
                vn.FleetID = statusPacket.cFleetId;
                vn.ReasonID = statusPacket.cMsgType;
                vn.GPSTime = null;
                vn.DeviceTime = null;
                vn.RawGPS = "";
                vn.UserDefined = "";
                vn.Spare1 = GPExtendedValues.BlankDatabaseString;
                byte[] bNoTrailer = new byte[6];
                vn.Trailer1 = bNoTrailer;
                vn.Trailer2 = bNoTrailer;
                vn.Trailer3 = bNoTrailer;
                vn.Trailer4 = bNoTrailer;
                vn.Trailer5 = bNoTrailer;
                TrackingQueueItem item = new TrackingQueueItem(){ InsertIntoVehicleNotification = vn };
                return AddToMSMQ(item);
            }
        }
        /// <summary>
        /// This method will be used to populate an insert command for a routing packet.
        /// </summary>
        /// <param name="RoutingModuleGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <returns>string</returns>
        public string InsertRoutingRecord(RoutingModuleGPPacket statusPacket, Vehicle v)
        {
            string sUserDefinedValue = "";
            string sUserDesc = "";

            if ((statusPacket.iVehicleId == 0) || (statusPacket.cFleetId == 0)) return null;
            // Swallow messages if required:
            if (v.SwallowThisPacket) return null;

            PositionDetails position = null;
            position = GetPositionForVehicle(v, statusPacket, statusPacket.mFix.dLatitude, statusPacket.mFix.dLongitude);

            sUserDesc = v.VehicleTag;

            lock (this)
            {
                VehicleNotification vn = new VehicleNotification();                
                vn.VehicleID = (int)statusPacket.iVehicleId;
                vn.FleetID = statusPacket.cFleetId;
                vn.ReasonID = statusPacket.cMsgType;
                vn.GPSTime = statusPacket.mFixClock.ToNullableDateTime();
                vn.DeviceTime = statusPacket.mCurrentClock.ToNullableDateTime();
                vn.Latitude = (double)statusPacket.mFix.dLatitude;
                vn.Longitude = (double)statusPacket.mFix.dLongitude;
                vn.Direction = statusPacket.mFix.iDirection;
                vn.Speed = statusPacket.mFix.cSpeed;
                vn.RawGPS = "";
                // FIXME - where should the point which caused the message be stored??? (See below)
                vn.Status = (int)statusPacket.RouteNumber;
                vn.Flags = statusPacket.CheckPointIndex;
                vn.UserDefined = "";
                vn.InputStatus = statusPacket.mStatus.cInputStatus;
                vn.OutputStatus = statusPacket.mStatus.cOutputStatus;
                vn.LightStatus = statusPacket.mStatus.cLightStatus;
                vn.ButtonStatus = statusPacket.mStatus.cButtonStatus;
                vn.MaxSpeed = statusPacket.mDistance.cMaxSpeed;
                vn.SpeedAcc = (int)statusPacket.mDistance.lSpeedAccumulator;
                vn.Samples = statusPacket.mDistance.iSamples;
                vn.Spare1 = statusPacket.mExtendedValues.ToDatabaseFormatString();
                if (!statusPacket.bArchivalData)
                {
                    vn.Spare3 = v.sUserInfo;
                }
                else
                {
                    vn.Spare3 = "";
                }

                #region Insert the vehicle description

                if (sUserDesc != "")
                    vn.Spare4 = sUserDesc;
                else
                    vn.Spare4 = "";

                #endregion
                #region set trailer reporting frequencies
                vn.Spare6 = "";
                #endregion

                #region Engine details for Transport Solution

                vn.Spare2 = "0";

                // We are working with a Transport database -
                // insert the additional data:
                // Engine operational parameters:
                vn.MaxEngCoolTemp = statusPacket.mEngineData.iCoolantTemperature;
                vn.MaxEngOilTemp = statusPacket.mEngineData.iOilTemperature;
                vn.MinOilPressure = Convert.ToSingle(statusPacket.mEngineData.iOilPressure);
                vn.GearPosition = statusPacket.mEngineData.iGear;
                vn.MaxEngSpeed = statusPacket.mEngineData.iMaxRPM;
                vn.BrakeHits = statusPacket.mEngineData.iBrakeApplications;


                // G Forces:
                vn.MaxF = statusPacket.mEngineData.fGForceFront;
                vn.MaxB = statusPacket.mEngineData.fGForceBack;
                vn.MaxLR = statusPacket.mEngineData.fGForceLeftRight;

                // Not in standard status report:
                vn.TotalEngHours = 0;
                vn.TotalFuelUsed = statusPacket.mTransportExtraDetails.iTotalFuelUsed;
                vn.TripFuel = 0F;
                vn.FuelEconomy = 0F;
                if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                {
                    vn.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                }
                else
                {
                    if (statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.Odometer() > 0)
                        vn.Odometer = (int)statusPacket.mECMAdvanced.Odometer();
                    else if (statusPacket.mTransportExtraDetails != null)
                        vn.Odometer = (int)statusPacket.mTransportExtraDetails.iOdometer;
                    else
                        vn.Odometer = 0;
                }


                if (dbVer > 5.7)
                {
                    //Speed Zone Details
                    vn.SpeedZone1 = statusPacket.mTransportExtraDetails.iSpeedZone1;
                    vn.SpeedZone2 = statusPacket.mTransportExtraDetails.iSpeedZone2;
                    vn.SpeedZone3 = statusPacket.mTransportExtraDetails.iSpeedZone3;
                    vn.SpeedZone4 = statusPacket.mTransportExtraDetails.iSpeedZone4;

                    //RPM Zone Details
                    vn.RPMZone1 = statusPacket.mTransportExtraDetails.iRPMZone1;
                    vn.RPMZone2 = statusPacket.mTransportExtraDetails.iRPMZone2;
                    vn.RPMZone3 = statusPacket.mTransportExtraDetails.iRPMZone3;
                    vn.RPMZone4 = statusPacket.mTransportExtraDetails.iRPMZone4;

                    vn.BatteryVolts = statusPacket.mTransportExtraDetails.iBatteryVolts;
                    vn.BrakeUsageSeconds = statusPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                    vn.CANVehicleSpeed = statusPacket.mTransportExtraDetails.iCANVehicleSpeed;

                    byte[] bNoTrailer = new byte[6];

                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 1)
                        vn.Trailer1 = (byte[])statusPacket.mTrailerTrack.oTrailers[0];
                    else
                        vn.Trailer1 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 2)
                        vn.Trailer2 = (byte[])statusPacket.mTrailerTrack.oTrailers[1];
                    else
                        vn.Trailer2 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 3)
                        vn.Trailer3 = (byte[])statusPacket.mTrailerTrack.oTrailers[2];
                    else
                        vn.Trailer3 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 4)
                        vn.Trailer4 = (byte[])statusPacket.mTrailerTrack.oTrailers[3];
                    else
                        vn.Trailer4 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 5)
                        vn.Trailer5 = (byte[])statusPacket.mTrailerTrack.oTrailers[4];
                    else
                        vn.Trailer5 = bNoTrailer;
                }

                if (dbVer >= 5.9)
                {
                    vn.StatusVehicle = v.GetUnitStateForReport(statusPacket);
                    vn.StatusAuxiliary = v.GetAuxiliaryStateForReport(statusPacket);
                    vn.SetPointGroupID = v.oPos.SetPointGroupID;
                    vn.SetPointID = v.oPos.SetPointID;
                    vn.VehicleScheduleID = statusPacket.RouteNumber;
                    vn.CheckPointIndex = statusPacket.CheckPointIndex;
                }
                if (dbVer >= 6.0)
                {
                    #region Refrigeration Values

                    if (statusPacket.mExtendedVariableDetails.Refrigeration != null)
                    {
                        vn.ReferFlags = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                        vn.ReferFuelPercentage = (double)statusPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                        vn.ReferBatteryVolts = (double)statusPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                        vn.ReferInput1 = statusPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                        vn.ReferInput2 = statusPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                        vn.ReferInput3 = statusPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                        vn.ReferInput4 = statusPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                        vn.ReferSensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                        vn.ReferHumidity = (double)statusPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                        vn.ReferSensor1 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                        vn.ReferSensor2 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                        vn.ReferSensor3 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                        vn.ReferSensor4 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                        vn.ReferSensor5 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                        vn.ReferSensor6 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                        if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                        {
                            if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                            {
                                vn.ReferZ1Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                vn.ReferZ1OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                vn.ReferZ1Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                vn.ReferZ1SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                vn.ReferZ1ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                vn.ReferZ1ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                vn.ReferZ1SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                vn.ReferZ1SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                vn.ReferZ1Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                vn.ReferZ1Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;
                            }
                            if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                            {
                                vn.ReferZ2Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                vn.ReferZ2OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                vn.ReferZ2Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                vn.ReferZ2SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                vn.ReferZ2ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                vn.ReferZ2ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                vn.ReferZ2SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                vn.ReferZ2SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                vn.ReferZ2Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                vn.ReferZ2Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;
                            }
                            if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                            {
                                vn.ReferZ3Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                vn.ReferZ3OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                vn.ReferZ3Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                vn.ReferZ3SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                vn.ReferZ3ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                vn.ReferZ3ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                vn.ReferZ3SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                vn.ReferZ3SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                vn.ReferZ3Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                vn.ReferZ3Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;
                            }
                        }
                    }
                    #endregion
                }
                if (dbVer >= 6.16)
                {
                    #region Additional Advanced ECM fields
                    if (eSendECMLiveUpdate != null && statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.ECMAdvancedItems != null && statusPacket.mECMAdvanced.ECMAdvancedItems.Length > 0 && v != null && v.oPos != null)
                    {
                        eSendECMLiveUpdate((int)statusPacket.cFleetId, (int)statusPacket.iVehicleId, statusPacket.mCurrentClock.ToDateTime(), v.oPos.PlaceName, Convert.ToSingle(statusPacket.mFix.dLatitude), Convert.ToSingle(statusPacket.mFix.dLongitude), statusPacket.mECMAdvanced.ECMAdvancedItems);
                    }

                    vn.HDOP = statusPacket.mECMAdvanced.SatellitesHDOP();
                    vn.NumOfSatellites = statusPacket.mECMAdvanced.SatellitesInView();
                    vn.MaxZ = statusPacket.mECMAdvanced.GForceZAxis();
                    vn.Altitude = statusPacket.mECMAdvanced.Altitude();
                    vn.GForceTemperature = statusPacket.mECMAdvanced.GForceTemperature();
                    vn.ReportFromFlash = statusPacket.bArchivalData;

                    // Override previous values if the are available in the advanced data.
                    if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                    {
                        vn.GPSOdometer = true;
                        vn.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                    }
                    else
                    {
                        vn.GPSOdometer = false;
                        if (statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.Odometer() > 0)
                            vn.Odometer = (int)statusPacket.mECMAdvanced.Odometer();
                        else if (statusPacket.mTransportExtraDetails != null)
                            vn.Odometer = (int)statusPacket.mTransportExtraDetails.iOdometer;
                        else
                            vn.Odometer = 0;
                    }
                    if (statusPacket.mECMAdvanced.TotalEngHours() > 0) vn.TotalEngHours = (int)statusPacket.mECMAdvanced.TotalEngHours();
                    if (statusPacket.mECMAdvanced.TotalFuelUsed() > 0) vn.TotalFuelUsed = statusPacket.mECMAdvanced.TotalFuelUsed();
                    if (statusPacket.mECMAdvanced.TripFuel() > 0) vn.TripFuel = statusPacket.mECMAdvanced.TripFuel();
                    if (statusPacket.mECMAdvanced.FuelEconomy() > 0) vn.FuelEconomy = statusPacket.mECMAdvanced.FuelEconomy();
                    if (statusPacket.mECMAdvanced.MaxEngCoolTemp() > 0) vn.MaxEngCoolTemp = statusPacket.mECMAdvanced.MaxEngCoolTemp();
                    if (statusPacket.mECMAdvanced.MaxEngOilTemp() > 0) vn.MaxEngOilTemp = statusPacket.mECMAdvanced.MaxEngOilTemp();
                    if (statusPacket.mECMAdvanced.MinOilPressure() > 0) vn.MinOilPressure = statusPacket.mECMAdvanced.MinOilPressure();
                    if (statusPacket.mECMAdvanced.GearPosition() > 0) vn.GearPosition = statusPacket.mECMAdvanced.GearPosition();
                    if (statusPacket.mECMAdvanced.MaxEngSpeed() > 0) vn.MaxEngSpeed = statusPacket.mECMAdvanced.MaxEngSpeed();
                    if (statusPacket.mECMAdvanced.BrakeHits() > 0) vn.BrakeHits = statusPacket.mECMAdvanced.BrakeHits();
                    if (statusPacket.mECMAdvanced.BatteryVolts() > 0) vn.BatteryVolts = statusPacket.mECMAdvanced.BatteryVolts();
                    if (statusPacket.mECMAdvanced.BrakeUsageSeconds() > 0) vn.BrakeUsageSeconds = statusPacket.mECMAdvanced.BrakeUsageSeconds();
                    if (statusPacket.mECMAdvanced.ECMVehicleSpeed(true) > 0) vn.CANVehicleSpeed = statusPacket.mECMAdvanced.ECMVehicleSpeed(true);
                    // G-Force Readings
                    if (statusPacket.mECMAdvanced.HasGForce)
                    {
                        vn.MaxF = statusPacket.mECMAdvanced.GForceFront();
                        vn.MaxB = statusPacket.mECMAdvanced.GForceBack();
                        vn.MaxLR = statusPacket.mECMAdvanced.GForceLeftRight();
                    }
                    // RPM Zone Readings
                    if (statusPacket.mECMAdvanced.RPMZone1() > 0) vn.RPMZone1 = statusPacket.mECMAdvanced.RPMZone1();
                    if (statusPacket.mECMAdvanced.RPMZone2() > 0) vn.RPMZone2 = statusPacket.mECMAdvanced.RPMZone2();
                    if (statusPacket.mECMAdvanced.RPMZone3() > 0) vn.RPMZone3 = statusPacket.mECMAdvanced.RPMZone3();
                    if (statusPacket.mECMAdvanced.RPMZone4() > 0) vn.RPMZone4 = statusPacket.mECMAdvanced.RPMZone4();
                    // Speed Zone Readings
                    if (statusPacket.mECMAdvanced.SpeedZone1() > 0) vn.SpeedZone1 = statusPacket.mECMAdvanced.SpeedZone1();
                    if (statusPacket.mECMAdvanced.SpeedZone2() > 0) vn.SpeedZone2 = statusPacket.mECMAdvanced.SpeedZone2();
                    if (statusPacket.mECMAdvanced.SpeedZone3() > 0) vn.SpeedZone3 = statusPacket.mECMAdvanced.SpeedZone3();
                    if (statusPacket.mECMAdvanced.SpeedZone4() > 0) vn.SpeedZone4 = statusPacket.mECMAdvanced.SpeedZone4();
                    // Extended I/O
                    if (statusPacket.mECMAdvanced.ExtendedIO1() > 0 || statusPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(statusPacket.cFleetId)) > 0 || statusPacket.mECMAdvanced.ExtendedIO3() > 0 || statusPacket.mECMAdvanced.ExtendedIO4() > 0)
                        vn.Spare1 = statusPacket.mECMAdvanced.ExtendedIO1() + "%" + statusPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(statusPacket.cFleetId)) + "%" + statusPacket.mECMAdvanced.ExtendedIO3() + "%" + statusPacket.mECMAdvanced.ExtendedIO4();
                    #endregion
                }
                //	Update the location of the unit..
                if (statusPacket.mFix.dLatitude == 0 && statusPacket.mFix.dLongitude == 0)
                {
                    vn.DistanceFromNearest = 0;
                    vn.PositionFromNearest = "N";
                    vn.Suburb = "";
                    vn.StreetName = "";
                    vn.MapReference = "";
                }
                else
                {
                    vn.DistanceFromNearest = (double)v.oPos.Distance;
                    vn.PositionFromNearest = v.oPos.Position;
                    if (!String.IsNullOrEmpty(statusPacket.StreetName))
                        vn.StreetName = statusPacket.StreetName;
                    else
                        vn.StreetName = v.oPos.PlaceName;
                    vn.Suburb = v.oPos.PlaceName;
                    vn.MapReference = v.oPos.MapRef;
                }

                if (statusPacket.RoadTypeData != null)
                {                   
                    vn.StreetName = string.IsNullOrEmpty(statusPacket.RoadTypeData.Street) ? "" : statusPacket.RoadTypeData.Street;
                    vn.Suburb = string.IsNullOrEmpty(statusPacket.RoadTypeData.Suburb) ? "" : statusPacket.RoadTypeData.Suburb;
                    vn.MapReference = "";
                    vn.DistanceFromNearest = 0;
                    vn.PositionFromNearest = "N";
                    vn.SpeedLimit = statusPacket.RoadTypeData.OverrideSpeedLimit > 0 ? statusPacket.RoadTypeData.OverrideSpeedLimit : statusPacket.RoadTypeData.SpeedLimit;
                }

                #endregion

                #region New "State" info

                // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                if (_userDefinedDateFormat != null)
                    sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                else
                    sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');
                sUserDefinedValue += "%0";
                vn.UserDefined = sUserDefinedValue;

                #endregion

                TrackingQueueItem item = new TrackingQueueItem();
                item.InsertIntoVehicleNotification = vn;
                PrepareAdvancedECMInsert(statusPacket.mECMAdvanced, item);
                PrepareRoadTypeInsert(statusPacket.RoadTypeData, item);
                string sErr = AddToMSMQ(item);
                return sErr;
            }
        }
        /// <summary>
        /// This method will be used to populate an insert command for a setpoint/route point packet.
        /// </summary>
        /// <param name="RoutePtSetPtGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <returns>string</returns>
        public string InsertSetPointRoutePointRecord(RoutePtSetPtGPPacket statusPacket, Vehicle v)
        {

            string sUserDefinedValue = "";
            string sUserDesc = "";

            if ((statusPacket.iVehicleId == 0) || (statusPacket.cFleetId == 0)) return null;
            // Swallow messages if required:
            if (v.SwallowThisPacket) return null;

            PositionDetails position = null;
            position = GetPositionForVehicle(v, statusPacket, statusPacket.mFix.dLatitude, statusPacket.mFix.dLongitude);

            sUserDesc = v.VehicleTag;

            lock (this)
            {
                VehicleNotification vn = new VehicleNotification();
                vn.VehicleID = (int)statusPacket.iVehicleId;
                vn.FleetID = statusPacket.cFleetId;
                if (statusPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_SPEEDING_AT_WP)
                {
                    vn.ReasonID = Convert.ToInt32(((statusPacket.speedZoneID & 0x0F) << 12) + ((statusPacket.speedZoneFlags & 0x0F) << 8) + (statusPacket.cMsgType & 0xFF));
                }
                else
                {
                    vn.ReasonID = ((statusPacket.mStatus.cLightStatus << 12) + (statusPacket.cMsgType & 0xFF));
                }
                vn.GPSTime = statusPacket.mFixClock.ToNullableDateTime();
                vn.DeviceTime = statusPacket.mCurrentClock.ToNullableDateTime();
                vn.Latitude = (double)statusPacket.mFix.dLatitude;
                vn.Longitude = (double)statusPacket.mFix.dLongitude;
                vn.Direction = statusPacket.mFix.iDirection;
                vn.Speed = statusPacket.mFix.cSpeed;
                vn.RawGPS = "";
                // FIXME - where should the point which caused the message be stored??? (See below)
                vn.Status = statusPacket.cPointSetNumber;
                vn.Flags = statusPacket.cPointPointNumber;
                vn.UserDefined = "";
                vn.InputStatus = statusPacket.mStatus.cInputStatus;
                vn.OutputStatus = statusPacket.mStatus.cOutputStatus;
                vn.LightStatus = statusPacket.mStatus.cLightStatus;
                vn.ButtonStatus = statusPacket.mStatus.cButtonStatus;
                vn.MaxSpeed = statusPacket.mDistance.cMaxSpeed;
                vn.SpeedAcc = (int)statusPacket.mDistance.lSpeedAccumulator;
                vn.Samples = statusPacket.mDistance.iSamples;
                vn.Spare1 = statusPacket.mExtendedValues.ToDatabaseFormatString();
                if (!statusPacket.bArchivalData)
                {
                    vn.Spare3 = v.sUserInfo;
                }
                else
                {
                    vn.Spare3 = "";
                }
                #region Insert the vehicle description
                if (sUserDesc != "")
                    vn.Spare4 = sUserDesc;
                else
                    vn.Spare4 = "";
                #endregion
                #region set trailer reporting frequencies
                vn.Spare6 = "";
                #endregion
                #region Engine details for Transport Solution
                vn.Spare2 = "0";

                // We are working with a Transport database -
                // insert the additional data:
                // Engine operational parameters:
                vn.MaxEngCoolTemp = statusPacket.mEngineData.iCoolantTemperature;
                vn.MaxEngOilTemp = statusPacket.mEngineData.iOilTemperature;
                vn.MinOilPressure = Convert.ToSingle(statusPacket.mEngineData.iOilPressure);
                vn.GearPosition = statusPacket.mEngineData.iGear;
                vn.MaxEngSpeed = statusPacket.mEngineData.iMaxRPM;
                vn.BrakeHits = statusPacket.mEngineData.iBrakeApplications;


                // G Forces:
                vn.MaxF = statusPacket.mEngineData.fGForceFront;
                vn.MaxB = statusPacket.mEngineData.fGForceBack;
                vn.MaxLR = statusPacket.mEngineData.fGForceLeftRight;

                // Not in standard status report:
                vn.TotalEngHours = 0;
                vn.TotalFuelUsed = statusPacket.mTransportExtraDetails.iTotalFuelUsed;
                vn.TripFuel = 0F;
                vn.FuelEconomy = statusPacket.mTransportExtraDetails.fFuelEconomy;
                if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                {
                    vn.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                }
                else
                {
                    if (statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.Odometer() > 0)
                        vn.Odometer = (int)statusPacket.mECMAdvanced.Odometer();
                    else if (statusPacket.mTransportExtraDetails != null)
                        vn.Odometer = (int)statusPacket.mTransportExtraDetails.iOdometer;
                    else
                        vn.Odometer = 0;
                }

                if (dbVer > 5.7)
                {
                    //Speed Zone Details
                    vn.SpeedZone1 = statusPacket.mTransportExtraDetails.iSpeedZone1;
                    vn.SpeedZone2 = statusPacket.mTransportExtraDetails.iSpeedZone2;
                    vn.SpeedZone3 = statusPacket.mTransportExtraDetails.iSpeedZone3;
                    vn.SpeedZone4 = statusPacket.mTransportExtraDetails.iSpeedZone4;

                    //RPM Zone Details
                    vn.RPMZone1 = statusPacket.mTransportExtraDetails.iRPMZone1;
                    vn.RPMZone2 = statusPacket.mTransportExtraDetails.iRPMZone2;
                    vn.RPMZone3 = statusPacket.mTransportExtraDetails.iRPMZone3;
                    vn.RPMZone4 = statusPacket.mTransportExtraDetails.iRPMZone4;

                    vn.BatteryVolts = statusPacket.mTransportExtraDetails.iBatteryVolts;
                    vn.BrakeUsageSeconds = statusPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                    vn.CANVehicleSpeed = statusPacket.mTransportExtraDetails.iCANVehicleSpeed;

                    byte[] bNoTrailer = new byte[6];

                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 1)
                        vn.Trailer1 = (byte[])statusPacket.mTrailerTrack.oTrailers[0];
                    else
                        vn.Trailer1 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 2)
                        vn.Trailer2 = (byte[])statusPacket.mTrailerTrack.oTrailers[1];
                    else
                        vn.Trailer2 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 3)
                        vn.Trailer3 = (byte[])statusPacket.mTrailerTrack.oTrailers[2];
                    else
                        vn.Trailer3 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 4)
                        vn.Trailer4 = (byte[])statusPacket.mTrailerTrack.oTrailers[3];
                    else
                        vn.Trailer4 = bNoTrailer;
                    if (statusPacket.mTrailerTrack.oTrailers.Count >= 5)
                        vn.Trailer5 = (byte[])statusPacket.mTrailerTrack.oTrailers[4];
                    else
                        vn.Trailer5 = bNoTrailer;
                }

                if (dbVer >= 5.9)
                {
                    vn.StatusVehicle = v.GetUnitStateForReport(statusPacket);
                    vn.StatusAuxiliary = v.GetAuxiliaryStateForReport(statusPacket);
                    vn.SetPointGroupID = v.oPos.SetPointGroupID;
                    vn.SetPointID = v.oPos.SetPointID;
                    vn.VehicleScheduleID = statusPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID;
                    vn.CheckPointIndex = statusPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex;
                }
                if (dbVer >= 6.0)
                {
                    #region Refrigeration Values
                    if (statusPacket.mExtendedVariableDetails.Refrigeration != null)
                    {
                        vn.ReferFlags = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                        vn.ReferFuelPercentage = (double)statusPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                        vn.ReferBatteryVolts = (double)statusPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                        vn.ReferInput1 = statusPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                        vn.ReferInput2 = statusPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                        vn.ReferInput3 = statusPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                        vn.ReferInput4 = statusPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                        vn.ReferSensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                        vn.ReferHumidity = (double)statusPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                        vn.ReferSensor1 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                        vn.ReferSensor2 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                        vn.ReferSensor3 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                        vn.ReferSensor4 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                        vn.ReferSensor5 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                        vn.ReferSensor6 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                        if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                        {
                            if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                            {
                                vn.ReferZ1Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                vn.ReferZ1OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                vn.ReferZ1Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                vn.ReferZ1SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                vn.ReferZ1ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                vn.ReferZ1ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                vn.ReferZ1SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                vn.ReferZ1SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                vn.ReferZ1Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                vn.ReferZ1Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;
                            }
                            if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                            {
                                vn.ReferZ2Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                vn.ReferZ2OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                vn.ReferZ2Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                vn.ReferZ2SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                vn.ReferZ2ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                vn.ReferZ2ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                vn.ReferZ2SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                vn.ReferZ2SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                vn.ReferZ2Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                vn.ReferZ2Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;
                            }
                            if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                            {
                                vn.ReferZ3Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                vn.ReferZ3OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                vn.ReferZ3Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                vn.ReferZ3SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                vn.ReferZ3ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                vn.ReferZ3ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                vn.ReferZ3SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                vn.ReferZ3SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                vn.ReferZ3Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                vn.ReferZ3Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;
                            }
                        }
                    }
                    #endregion
                }
                if (dbVer >= 6.16)
                {
                    #region Additional Advanced ECM fields
                    if (eSendECMLiveUpdate != null && statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.ECMAdvancedItems != null && statusPacket.mECMAdvanced.ECMAdvancedItems.Length > 0 && v != null && v.oPos != null)
                    {
                        eSendECMLiveUpdate((int)statusPacket.cFleetId, (int)statusPacket.iVehicleId, statusPacket.mCurrentClock.ToDateTime(), v.oPos.PlaceName, Convert.ToSingle(statusPacket.mFix.dLatitude), Convert.ToSingle(statusPacket.mFix.dLongitude), statusPacket.mECMAdvanced.ECMAdvancedItems);
                    }

                    vn.HDOP = statusPacket.mECMAdvanced.SatellitesHDOP();
                    vn.NumOfSatellites = statusPacket.mECMAdvanced.SatellitesInView();
                    vn.MaxZ = statusPacket.mECMAdvanced.GForceZAxis();
                    vn.Altitude = statusPacket.mECMAdvanced.Altitude();
                    vn.GForceTemperature = statusPacket.mECMAdvanced.GForceTemperature();
                    vn.ReportFromFlash = statusPacket.bArchivalData;

                    // Override previous values if the are available in the advanced data.
                    if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                    {
                        vn.GPSOdometer = true;
                        vn.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                    }
                    else
                    {
                        vn.GPSOdometer = false;
                        if (statusPacket.mECMAdvanced != null && statusPacket.mECMAdvanced.Odometer() > 0)
                            vn.Odometer = (int)statusPacket.mECMAdvanced.Odometer();
                        else if (statusPacket.mTransportExtraDetails != null)
                            vn.Odometer = (int)statusPacket.mTransportExtraDetails.iOdometer;
                        else
                            vn.Odometer = 0;
                    }
                    if (statusPacket.mECMAdvanced.TotalEngHours() > 0) vn.TotalEngHours = (int)statusPacket.mECMAdvanced.TotalEngHours();
                    if (statusPacket.mECMAdvanced.TotalFuelUsed() > 0) vn.TotalFuelUsed = statusPacket.mECMAdvanced.TotalFuelUsed();
                    if (statusPacket.mECMAdvanced.TripFuel() > 0) vn.TripFuel = statusPacket.mECMAdvanced.TripFuel();
                    if (statusPacket.mECMAdvanced.FuelEconomy() > 0) vn.FuelEconomy = statusPacket.mECMAdvanced.FuelEconomy();
                    if (statusPacket.mECMAdvanced.MaxEngCoolTemp() > 0) vn.MaxEngCoolTemp = statusPacket.mECMAdvanced.MaxEngCoolTemp();
                    if (statusPacket.mECMAdvanced.MaxEngOilTemp() > 0) vn.MaxEngOilTemp = statusPacket.mECMAdvanced.MaxEngOilTemp();
                    if (statusPacket.mECMAdvanced.MinOilPressure() > 0) vn.MinOilPressure = statusPacket.mECMAdvanced.MinOilPressure();
                    if (statusPacket.mECMAdvanced.GearPosition() > 0) vn.GearPosition = statusPacket.mECMAdvanced.GearPosition();
                    if (statusPacket.mECMAdvanced.MaxEngSpeed() > 0) vn.MaxEngSpeed = statusPacket.mECMAdvanced.MaxEngSpeed();
                    if (statusPacket.mECMAdvanced.BrakeHits() > 0) vn.BrakeHits = statusPacket.mECMAdvanced.BrakeHits();
                    if (statusPacket.mECMAdvanced.BatteryVolts() > 0) vn.BatteryVolts = statusPacket.mECMAdvanced.BatteryVolts();
                    if (statusPacket.mECMAdvanced.BrakeUsageSeconds() > 0) vn.BrakeUsageSeconds = statusPacket.mECMAdvanced.BrakeUsageSeconds();
                    if (statusPacket.mECMAdvanced.ECMVehicleSpeed(true) > 0) vn.CANVehicleSpeed = statusPacket.mECMAdvanced.ECMVehicleSpeed(true);
                    // G-Force Readings
                    if (statusPacket.mECMAdvanced.HasGForce)
                    {
                        vn.MaxF = statusPacket.mECMAdvanced.GForceFront();
                        vn.MaxB = statusPacket.mECMAdvanced.GForceBack();
                        vn.MaxLR = statusPacket.mECMAdvanced.GForceLeftRight();
                    }
                    // RPM Zone Readings
                    if (statusPacket.mECMAdvanced.RPMZone1() > 0) vn.RPMZone1 = statusPacket.mECMAdvanced.RPMZone1();
                    if (statusPacket.mECMAdvanced.RPMZone2() > 0) vn.RPMZone2 = statusPacket.mECMAdvanced.RPMZone2();
                    if (statusPacket.mECMAdvanced.RPMZone3() > 0) vn.RPMZone3 = statusPacket.mECMAdvanced.RPMZone3();
                    if (statusPacket.mECMAdvanced.RPMZone4() > 0) vn.RPMZone4 = statusPacket.mECMAdvanced.RPMZone4();
                    // Speed Zone Readings
                    if (statusPacket.mECMAdvanced.SpeedZone1() > 0) vn.SpeedZone1 = statusPacket.mECMAdvanced.SpeedZone1();
                    if (statusPacket.mECMAdvanced.SpeedZone2() > 0) vn.SpeedZone2 = statusPacket.mECMAdvanced.SpeedZone2();
                    if (statusPacket.mECMAdvanced.SpeedZone3() > 0) vn.SpeedZone3 = statusPacket.mECMAdvanced.SpeedZone3();
                    if (statusPacket.mECMAdvanced.SpeedZone4() > 0) vn.SpeedZone4 = statusPacket.mECMAdvanced.SpeedZone4();
                    // Extended I/O
                    if (statusPacket.mECMAdvanced.ExtendedIO1() > 0 || statusPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(statusPacket.cFleetId)) > 0 || statusPacket.mECMAdvanced.ExtendedIO3() > 0 || statusPacket.mECMAdvanced.ExtendedIO4() > 0)
                        vn.Spare1 = statusPacket.mECMAdvanced.ExtendedIO1() + "%" + statusPacket.mECMAdvanced.ExtendedIO2(IsConcreteFleet(statusPacket.cFleetId)) + "%" + statusPacket.mECMAdvanced.ExtendedIO3() + "%" + statusPacket.mECMAdvanced.ExtendedIO4();
                    #endregion
                }
                //	Update the location of the unit..
                if (statusPacket.mFix.dLatitude == 0 && statusPacket.mFix.dLongitude == 0)
                {
                    vn.DistanceFromNearest = 0;
                    vn.PositionFromNearest = "N";
                    vn.Suburb = "";
                    vn.StreetName = "";
                    vn.MapReference = "";
                }
                else
                {
                    vn.DistanceFromNearest = (double)v.oPos.Distance;
                    vn.PositionFromNearest = v.oPos.Position;
                    if (!String.IsNullOrEmpty(statusPacket.StreetName))
                        vn.StreetName = statusPacket.StreetName;
                    else
                        vn.StreetName = v.oPos.PlaceName;

                    vn.Suburb = v.oPos.PlaceName;
                    vn.MapReference = v.oPos.MapRef;

                }

                if (statusPacket.RoadTypeData != null)
                {
                    vn.StreetName = string.IsNullOrEmpty(statusPacket.RoadTypeData.Street) ? "" : statusPacket.RoadTypeData.Street;
                    vn.Suburb = string.IsNullOrEmpty(statusPacket.RoadTypeData.Suburb) ? "" : statusPacket.RoadTypeData.Suburb;
                    vn.MapReference = "";
                    vn.DistanceFromNearest = 0;
                    vn.PositionFromNearest = "N";
                    vn.SpeedLimit = statusPacket.RoadTypeData.OverrideSpeedLimit > 0 ? statusPacket.RoadTypeData.OverrideSpeedLimit : statusPacket.RoadTypeData.SpeedLimit;
                }

                #endregion

                #region New "State" info
                // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                if (_userDefinedDateFormat != null)
                    sUserDefinedValue = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                else
                    sUserDefinedValue = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');
                sUserDefinedValue += "%0";
                vn.UserDefined = sUserDefinedValue;
                #endregion

                TrackingQueueItem item = new TrackingQueueItem();
                item.InsertIntoVehicleNotification = vn;
                PrepareAdvancedECMInsert(statusPacket.mECMAdvanced, item);
                PrepareRoadTypeInsert(statusPacket.RoadTypeData, item);
                string sErr = AddToMSMQ(item);
                return sErr;
            }
        }
        /// <summary>
        /// This method will be used to populate an insert command for a history position packet.
        /// </summary>
        /// <param name="GPSHistoryGPPacket historyPacket"></param>
        /// <returns>string</returns>
        public string InsertHistoryRecords(GPSHistoryGPPacket historyPacket, Vehicle v)
        {
            string error;
            if ((historyPacket.iVehicleId == 0) || (historyPacket.cFleetId == 0) || historyPacket.IsExtendedHistoryComplete) return null;
            PositionDetails position = null;
            lock (this)
            {
                VehicleNotification vn = new VehicleNotification();
                vn.VehicleID = (int)historyPacket.iVehicleId;
                vn.FleetID = historyPacket.cFleetId;
                if (historyPacket.mStatusList.cSpareStatus == 1)
                {
                    vn.ReasonID = 481; //Extended History
                }
                else
                {
                    vn.ReasonID = historyPacket.cMsgType;
                }

                #region Set all the fields that aren't supplied with a value.
                vn.UserDefined = "";
                vn.RawGPS = "";
                vn.Spare1 = GPExtendedValues.BlankDatabaseString;
                vn.Spare2 = "";
                vn.Spare4 = "";
                vn.Spare10 = "";
                vn.Spare6 = "";
                if (dbVer > 5.7)
                {
                    #region Trailer IDs
                    byte[] bNoTrailer = new byte[6];
                    vn.Trailer1 = bNoTrailer;
                    vn.Trailer2 = bNoTrailer;
                    vn.Trailer3 = bNoTrailer;
                    vn.Trailer4 = bNoTrailer;
                    vn.Trailer5 = bNoTrailer;
                    #endregion
                }
                #region Set GPSTime and DeviceTime
                vn.GPSTime = historyPacket.mClockList.ToNullableDateTime();
                vn.DeviceTime = historyPacket.mClockList.ToNullableDateTime();
                #endregion
                #region Set the input status
                vn.InputStatus = historyPacket.mStatusList.cInputStatus;
                #endregion
                #region Set the fix data (Lat/Long, direction, speed)
                vn.Latitude = (double)historyPacket.mFixList.dLatitude;
                vn.Longitude = (double)historyPacket.mFixList.dLongitude;
                vn.Direction = historyPacket.mFixList.iDirection;
                vn.Speed = historyPacket.mFixList.cSpeed;
                #endregion
                #region Set the distance data (MaxSpeed, SpeedAcc, Samples)
                vn.MaxSpeed = historyPacket.mDistance.cMaxSpeed;
                vn.SpeedAcc = (int)historyPacket.mDistance.lSpeedAccumulator;
                vn.Samples = historyPacket.mDistance.iSamples;
                #endregion
                #region Set the engine data values (MaxEngCoolTemp, MaxEngOilTemp, MinOilPressure, GearPosition, MaxEngSpeed, BrakeHits)
                vn.MaxEngCoolTemp = historyPacket.mEngineData.iCoolantTemperature;
                vn.MaxEngOilTemp = historyPacket.mEngineData.iOilTemperature;
                vn.MinOilPressure = Convert.ToSingle(historyPacket.mEngineData.iOilPressure);
                vn.GearPosition = historyPacket.mEngineData.iGear;
                vn.MaxEngSpeed = historyPacket.mEngineData.iMaxRPM;
                vn.BrakeHits = historyPacket.mEngineData.iBrakeApplications;
                #endregion
                #region Set the G-Force values
                vn.MaxF = historyPacket.mEngineData.fGForceFront;
                vn.MaxB = historyPacket.mEngineData.fGForceBack;
                vn.MaxLR = historyPacket.mEngineData.fGForceLeftRight;
                #endregion
                #region Get the position data for the report
                if (historyPacket.mFixList.dLatitude == 0 && historyPacket.mFixList.dLongitude == 0)
                {
                    vn.DistanceFromNearest = 0;
                    vn.PositionFromNearest = "N";
                    vn.Suburb = "";
                    vn.StreetName = "";
                    vn.MapReference = "";
                }
                else
                {
                    position = GetPositionForVehicle(v, historyPacket, historyPacket.mFixList.dLatitude, historyPacket.mFixList.dLongitude);
                    vn.DistanceFromNearest = (double)position.Distance;
                    vn.PositionFromNearest = position.Position;
                    if (!String.IsNullOrEmpty(historyPacket.StreetName))
                        vn.StreetName = historyPacket.StreetName;
                    else
                        vn.StreetName = v.oPos.PlaceName;
                    vn.Suburb = v.oPos.PlaceName;
                    vn.MapReference = position.MapRef;
                }

                if (historyPacket.RoadTypeData != null)
                {
                    vn.StreetName = string.IsNullOrEmpty(historyPacket.RoadTypeData.Street) ? "" : historyPacket.RoadTypeData.Street;
                    vn.Suburb = string.IsNullOrEmpty(historyPacket.RoadTypeData.Suburb) ? "" : historyPacket.RoadTypeData.Suburb;
                    vn.SpeedLimit = historyPacket.RoadTypeData.OverrideSpeedLimit > 0 ? historyPacket.RoadTypeData.OverrideSpeedLimit : historyPacket.RoadTypeData.SpeedLimit;
                    vn.DistanceFromNearest = 0;
                    vn.PositionFromNearest = "N";
                    vn.MapReference = "";
                }                

                #endregion
                #region Default the values that are not included in the report from the unit
                vn.ReportFromFlash = true;
                #endregion
                TrackingQueueItem item = new TrackingQueueItem();
                item.InsertIntoVehicleNotification = vn;
                error = AddToMSMQ(item);
                return error;
                #endregion
            }
        }
        /// <summary>
        /// This method will be used to save data about the bytes sent and recieved by a unit.
        /// </summary>
        /// <param name="GeneralGPPacket gPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <returns>string</returns>
        public string UpdateBytesSentAndRecieved(GeneralGPPacket gPacket, Vehicle v)
        {
            bool bConnectionError = false;
            System.Data.SqlClient.SqlConnection oConn = null;
            string sCmd = "";
            SqlCommand oUpdateBytes = null;

            #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateBytesSentAndRecieved(GeneralGPPacket gPacket, Vehicle v) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                bConnectionError = true;
            }
            while (bConnectionError)
            {
                try
                {
                    Thread.Sleep(5000);
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                    bConnectionError = false;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "UpdateBytesSentAndRecieved(GeneralGPPacket gPacket, Vehicle v) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    bConnectionError = true;
                }
            }
            #endregion

            try
            {
                oUpdateBytes = oConn.CreateCommand();
                if (dbVer > 5.9)
                {
                    #region Update the asset register
                    if (gPacket.iProtocolVer > 0)
                    {
                        sCmd = "EXEC [usp_UpdateAssetDataUsage] @FleetID, @VehicleID, '@GPSTime', @iUnitTX, @iUnitRX, @iServerTX, @iServerRX, @SlotNumber, '@NetworkName', '@SimNumber'";
                        sCmd = sCmd.Replace("@FleetID", Convert.ToString(Convert.ToInt32(v.cFleet)));
                        sCmd = sCmd.Replace("@VehicleID", Convert.ToString(v.iUnit));
                        sCmd = sCmd.Replace("@GPSTime", gPacket.dtGPSTime.ToString("MM/dd/yyyy HH:mm:ss"));
                        sCmd = sCmd.Replace("@iUnitTX", Convert.ToString(gPacket.iUnitTXBytes));
                        sCmd = sCmd.Replace("@iUnitRX", Convert.ToString(gPacket.iUnitRXBytes));
                        sCmd = sCmd.Replace("@iServerTX", Convert.ToString(v.iServerTXBytes));
                        sCmd = sCmd.Replace("@iServerRX", Convert.ToString(v.iServerRXBytes));
                        sCmd = sCmd.Replace("@SlotNumber", Convert.ToString(gPacket.iSlotNumber));
                        sCmd = sCmd.Replace("@NetworkName", gPacket.sNetworkName);
                        sCmd = sCmd.Replace("@SimNumber", gPacket.sSimNumber);
                    }
                    else
                    {
                        sCmd = "EXEC [usp_UpdateAssetDataUsage] @FleetID, @VehicleID, '@GPSTime', @iUnitTX, @iUnitRX, @iServerTX, @iServerRX, @SlotNumber, '@NetworkName', '@SimNumber'";
                        sCmd = sCmd.Replace("@FleetID", Convert.ToString(Convert.ToInt32(v.cFleet)));
                        sCmd = sCmd.Replace("@VehicleID", Convert.ToString(v.iUnit));
                        sCmd = sCmd.Replace("@GPSTime", DateTime.Now.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss"));
                        sCmd = sCmd.Replace("@iUnitTX", Convert.ToString(gPacket.iUnitTXBytes));
                        sCmd = sCmd.Replace("@iUnitRX", Convert.ToString(gPacket.iUnitRXBytes));
                        sCmd = sCmd.Replace("@iServerTX", "0");
                        sCmd = sCmd.Replace("@iServerRX", "0");
                        sCmd = sCmd.Replace("@SlotNumber", "1");
                        sCmd = sCmd.Replace("@NetworkName", "");
                        sCmd = sCmd.Replace("@SimNumber", "");
                    }
                    oUpdateBytes.CommandText = sCmd;
                    oUpdateBytes.ExecuteNonQuery();
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                if (ex.Message.IndexOf("duplicate key", 0) < 0)
                    _log.Error(sClassName + "UpdateBytesSentAndRecieved(GeneralGPPacket gPacket, Vehicle v)", ex);
            }
            finally
            {
                if (oConn != null)
                {
                    oConn.Close();
                    oConn.Dispose();
                    oConn = null;
                }
            }
            return null;
        }
        
        public string InsertConfigDownloadedPacket(GeneralGPPacket configDownloadPacket)
        {
            string sFleetID = "";
            string sVehicleID = "";
            string sSQL = "";
            
            try
            {
                List<String> sqlCommandList = new List<string>();

                sFleetID = Convert.ToString((int)configDownloadPacket.cFleetId);
                sVehicleID = Convert.ToString(configDownloadPacket.iVehicleId);

                if (configDownloadPacket.mConfigDownloaded != null && configDownloadPacket.mConfigDownloaded.ItemCount > 0)
                {
                    List<Tuple<string, string>> listValues = configDownloadPacket.mConfigDownloaded.ListValues;
                    foreach (Tuple<string, string> listItem in listValues)
                    {
                        sSQL = "EXEC usp_InsertConfigDownload [FleetID], [VehicleID], '[GPSTime]', '[ItemKey]', '[ItemValue]'";
                        sSQL = sSQL.Replace("[FleetID]", sFleetID);
                        sSQL = sSQL.Replace("[VehicleID]", sVehicleID);
                        sSQL = sSQL.Replace("[GPSTime]", configDownloadPacket.mConfigDownloaded.GPSTime.ToString("MM/dd/yyyy HH:mm:ss"));
                        sSQL = sSQL.Replace("[ItemKey]", listItem.Item1.Replace("'", "''"));
                        sSQL = sSQL.Replace("[ItemValue]", listItem.Item2.Replace("'", "''"));
                        sqlCommandList.Add(sSQL);
                    }
                }

                using (SqlConnection oConn = new SqlConnection(sDSN))
                {
                    oConn.Open();
                    foreach (string sql in sqlCommandList)
                    {
                        SqlCommand oCmd = oConn.CreateCommand();
                        // for error handling;
                        sSQL = sql;
                        oCmd.CommandText = sql;
                        oCmd.ExecuteNonQuery();
                    }
                    oConn.Close();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "InsertConfigDownloadedPacket(GeneralGPPacket configDownloadPacket) SQL : " + sSQL, ex);
            }
            return null;
        }

        public string InsertSatteliteDataUsage(GPSatteliteDataUsage oDataUsage, Vehicle v)
        {
            string sFleetID = "";
            string sVehicleID = "";
            string sSQL = "";
            SqlConnection oConn = null;
            SqlCommand oCmd = null;
            bool bConnectionError = false;
            int iConnectionRetries = 0;
            try
            {
                sFleetID = Convert.ToString((int)v.cFleet);
                sVehicleID = Convert.ToString(v.iUnit);

                sSQL = "EXEC usp_InsertSatelliteDataUsage [FleetID], [VehicleID], '[GPSTime]', '[IMIE]', [SatVersion], [RXBytes], [RXPackets], [TXBytes], [TXPackets], [MailBoxChecks], [MOMSN], [MTMSN]";
                sSQL = sSQL.Replace("[FleetID]", sFleetID);
                sSQL = sSQL.Replace("[VehicleID]", sVehicleID);
                sSQL = sSQL.Replace("[GPSTime]", oDataUsage.GPSTime.ToString("MM/dd/yyyy HH:mm:ss"));
                sSQL = sSQL.Replace("[IMIE]", oDataUsage.IMIE);
                sSQL = sSQL.Replace("[SatVersion]", Convert.ToString(oDataUsage.SatVersion));
                sSQL = sSQL.Replace("[RXBytes]", Convert.ToString(oDataUsage.RXBytes));
                sSQL = sSQL.Replace("[RXPackets]", Convert.ToString(oDataUsage.RXPackets));
                sSQL = sSQL.Replace("[TXBytes]", Convert.ToString(oDataUsage.TXBytes));
                sSQL = sSQL.Replace("[TXPackets]", Convert.ToString(oDataUsage.TXPackets));
                sSQL = sSQL.Replace("[MailBoxChecks]", Convert.ToString(oDataUsage.MailBoxChecks));
                sSQL = sSQL.Replace("[MOMSN]", Convert.ToString(oDataUsage.MOMSN));
                sSQL = sSQL.Replace("[MTMSN]", Convert.ToString(oDataUsage.MTMSN));

                #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
                try
                {
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "InsertSatteliteDataUsage(GPSatteliteDataUsage oDataUsage, Vehicle v) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    bConnectionError = true;
                }
                while (bConnectionError)
                {
                    try
                    {

                        Thread.Sleep(5000);
                        oConn = new SqlConnection(sDSN);
                        oConn.Open();
                        bConnectionError = false;
                    }
                    catch (System.Exception ex)
                    {
                        bConnectionError = true;
                        if (iConnectionRetries > 10)
                        {
                            _log.Error(sClassName + "InsertSatteliteDataUsage(GPSatteliteDataUsage oDataUsage, Vehicle v) - Unable to connect to database afger 10 retries. SQL Cmd : " + sSQL, ex);
                            return "InsertSatteliteDataUsage Failed.";
                        }
                        iConnectionRetries++;
                        _log.Error(sClassName + "InsertSatteliteDataUsage(GPSatteliteDataUsage oDataUsage, Vehicle v) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    }
                }
                #endregion

                if (!bConnectionError)
                {
                    oCmd = oConn.CreateCommand();
                    oCmd.CommandText = sSQL;
                    oCmd.ExecuteNonQuery();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "InsertSatteliteDataUsage(GPSatteliteDataUsage oDataUsage, Vehicle v) SQL : " + sSQL, ex);
            }
            return null;
        }
        /// <summary>
        /// This method will be used to populate an insert command for a I/O event packet.
        /// </summary>
        /// <param name="GeneralGPPacket statusPacket"></param>
        /// <param name="Vehicle v"></param>
        /// <returns>string</returns>
        public string InsertInputActiveDeactiveRecord(GeneralGPPacket statusPacket, Vehicle v)
        {
            // For these messages, we "fudge" the reasonID to indicate 
            // whether it's an input 0, 1 or 2
            // This makes the server's job a lot easier.
            // Basically the Message Type is either 0xC4 (Active), 0xC5 (DeActive) or 0xC6 (Pulses)
            // and we prepend the input NUMBER to that hex digit.
            // So input 0 ON would be 0x00C4 (looks up as Ignition ON)
            // input 2 OFF would be 0x02C5
            // a pulse on input 1 would be 0x01C6

            string result = null;
            string sUserDefined = null;
            string sUserDesc = "";

            // Check it's worth inserting - is it from a reasonable unit?:
            // Swallow messages if required:
            if (!((statusPacket.iVehicleId == 0) || (statusPacket.cFleetId == 0) || (v.SwallowThisPacket)))
            {
                lock (this)
                {
                    TrackingQueueItem item = UnsafePrepareNotificationRecord(statusPacket, v);
                    if (item != null)
                    {
                        //	Adjust the messagetype based on input
                        item.InsertIntoVehicleNotification.ReasonID = ((statusPacket.mStatus.cLightStatus << 12) + (statusPacket.cInputNumber << 8) + (statusPacket.cMsgType & 0xFF));

                        // If _userDefinedDateFormat is null, use dd/MM/yyyy HH:mm:ss format.
                        if (_userDefinedDateFormat != null)
                            sUserDefined = (int)v.GetUnitState() + "%" + (v.GetLastNewPositionTime()).ToString(_userDefinedDateFormat);
                        else
                            sUserDefined = (int)v.GetUnitState() + "%" + v.GetLastNewPositionTime().Day.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Month.ToString().PadLeft(2, '0') + "/" + v.GetLastNewPositionTime().Year.ToString().PadLeft(4, '0') + " " + v.GetLastNewPositionTime().Hour.ToString().PadLeft(2, '0') + ":" + v.GetLastNewPositionTime().Minute.ToString().PadLeft(2, '0') + v.GetLastNewPositionTime().Second.ToString().PadLeft(2, '0');

                        if (statusPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES)
                            sUserDefined += "%" + statusPacket.iPulseCount.ToString();
                        else
                            sUserDefined += "%0";

                        item.InsertIntoVehicleNotification.UserDefined = sUserDefined;
                        item.InsertIntoVehicleNotification.Spare2 = "0";

                        if ((statusPacket.cInputNumber == 0) &&
                            (statusPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF) &&
                            (statusPacket.mEngineSummaryData != null))
                        {	// this is an ignition off - there will be additional data:
                            try
                            {
                                if (item.InsertIntoVehicleNotification.TotalEngHours == 0)
                                {
                                    item.InsertIntoVehicleNotification.TotalEngHours = statusPacket.mEngineSummaryData.iTotalEngineHours;
                                }
                                if (item.InsertIntoVehicleNotification.TotalFuelUsed == 0)
                                {
                                    item.InsertIntoVehicleNotification.TotalFuelUsed = statusPacket.mEngineSummaryData.iTotalFuel;
                                }
                                if (item.InsertIntoVehicleNotification.TripFuel == 0)
                                {
                                    item.InsertIntoVehicleNotification.TripFuel = statusPacket.mEngineSummaryData.iTripFuel;
                                }
                                if (item.InsertIntoVehicleNotification.FuelEconomy == 0)
                                {
                                    item.InsertIntoVehicleNotification.FuelEconomy = statusPacket.mEngineSummaryData.fFuelEconomy;
                                }
                                //if odometer has not been set
                                if (item.InsertIntoVehicleNotification.Odometer == 0)
                                {
                                    if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                                    {
                                        item.InsertIntoVehicleNotification.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                                    }
                                    else
                                    {
                                        if (statusPacket.mEngineSummaryData != null)
                                            item.InsertIntoVehicleNotification.Odometer = statusPacket.mEngineSummaryData.iOdometer;
                                        else
                                            item.InsertIntoVehicleNotification.Odometer = 0;
                                    }
                                }

                                if (!statusPacket.bArchivalData)
                                    item.InsertIntoVehicleNotification.Spare3 = v.sUserInfo;
                                else
                                    item.InsertIntoVehicleNotification.Spare3 = "";
                            }
                            catch (System.Exception ex)
                            {
                                _log.Error(sClassName + "InsertInputActiveDeactiveRecord(GeneralGPPacket statusPacket, Vehicle v)", ex);
                                result = "Error processing engine data";
                            }
                        }
                        else
                        {
                            item.InsertIntoVehicleNotification.TotalEngHours = 0;
                            item.InsertIntoVehicleNotification.TotalFuelUsed = statusPacket.mTransportExtraDetails.iTotalFuelUsed;
                            item.InsertIntoVehicleNotification.TripFuel = 0;
                            if (statusPacket.mEngineSummaryData != null)
                                item.InsertIntoVehicleNotification.FuelEconomy = statusPacket.mEngineSummaryData.fFuelEconomy;
                            else
                                item.InsertIntoVehicleNotification.FuelEconomy = 0F;
                            if (v.UseGPSOdometer && statusPacket.mECMAdvanced != null)
                            {
                                item.InsertIntoVehicleNotification.Odometer = (int)statusPacket.mECMAdvanced.GPSOdometer();
                            }
                            else
                            {
                                if (statusPacket.mTransportExtraDetails != null)
                                    item.InsertIntoVehicleNotification.Odometer = (int)statusPacket.mTransportExtraDetails.iOdometer;
                                else
                                    item.InsertIntoVehicleNotification.Odometer = 0;
                            }
                            
                            item.InsertIntoVehicleNotification.Spare2 = "0"; // Note this is different to normal behaviour.
                            if (!statusPacket.bArchivalData)
                                item.InsertIntoVehicleNotification.Spare3 = v.sUserInfo;
                            else
                                item.InsertIntoVehicleNotification.Spare3 = "";
                        }

                        #region Insert the vehicle description
                        sUserDesc = v.VehicleTag;
                        if (sUserDesc != "")
                            item.InsertIntoVehicleNotification.Spare4 = sUserDesc;
                        else
                            item.InsertIntoVehicleNotification.Spare4 = "";
	                    #endregion
                        #region set trailer reporting frequencies
                        item.InsertIntoVehicleNotification.Spare6 = "";
                        #endregion

	                    if (statusPacket.mFix.dLatitude == 0 && statusPacket.mFix.dLongitude == 0)
	                    {
	                        item.InsertIntoVehicleNotification.DistanceFromNearest = 0;
	                        item.InsertIntoVehicleNotification.PositionFromNearest = "N";
	                        item.InsertIntoVehicleNotification.Suburb = "";
	                        item.InsertIntoVehicleNotification.StreetName = "";
	                        item.InsertIntoVehicleNotification.MapReference = "";
	                    }
	                    else
	                    {
	                        item.InsertIntoVehicleNotification.DistanceFromNearest = (double)v.oPos.Distance;
	                        item.InsertIntoVehicleNotification.PositionFromNearest = v.oPos.Position;

	                        if (!String.IsNullOrEmpty(statusPacket.StreetName))
	                            item.InsertIntoVehicleNotification.StreetName = statusPacket.StreetName;
	                        else
	                            item.InsertIntoVehicleNotification.StreetName = v.oPos.PlaceName;
	                        item.InsertIntoVehicleNotification.Suburb = v.oPos.PlaceName;
	                        item.InsertIntoVehicleNotification.MapReference = v.oPos.MapRef;
	                    }

                        if (statusPacket.RoadTypeData != null)
                        {                            
                            item.InsertIntoVehicleNotification.StreetName = string.IsNullOrEmpty(statusPacket.RoadTypeData.Street) ? "" : statusPacket.RoadTypeData.Street; 
                            item.InsertIntoVehicleNotification.Suburb = string.IsNullOrEmpty(statusPacket.RoadTypeData.Suburb) ? "" : statusPacket.RoadTypeData.Suburb; ;
                            item.InsertIntoVehicleNotification.MapReference = "";
                            item.InsertIntoVehicleNotification.DistanceFromNearest = 0;
                            item.InsertIntoVehicleNotification.PositionFromNearest = "N";
                            item.InsertIntoVehicleNotification.SpeedLimit = statusPacket.RoadTypeData.OverrideSpeedLimit > 0 ? statusPacket.RoadTypeData.OverrideSpeedLimit : statusPacket.RoadTypeData.SpeedLimit;
                        }

                        if (dbVer > 5.7)
                        {
                            //Speed Zone Details
                            item.InsertIntoVehicleNotification.SpeedZone1 = statusPacket.mTransportExtraDetails.iSpeedZone1;
                            item.InsertIntoVehicleNotification.SpeedZone2 = statusPacket.mTransportExtraDetails.iSpeedZone2;
                            item.InsertIntoVehicleNotification.SpeedZone3 = statusPacket.mTransportExtraDetails.iSpeedZone3;
                            item.InsertIntoVehicleNotification.SpeedZone4 = statusPacket.mTransportExtraDetails.iSpeedZone4;

                            //RPM Zone Details
                            item.InsertIntoVehicleNotification.RPMZone1 = statusPacket.mTransportExtraDetails.iRPMZone1;
                            item.InsertIntoVehicleNotification.RPMZone2 = statusPacket.mTransportExtraDetails.iRPMZone2;
                            item.InsertIntoVehicleNotification.RPMZone3 = statusPacket.mTransportExtraDetails.iRPMZone3;
                            item.InsertIntoVehicleNotification.RPMZone4 = statusPacket.mTransportExtraDetails.iRPMZone4;

                            item.InsertIntoVehicleNotification.BatteryVolts = statusPacket.mTransportExtraDetails.iBatteryVolts;
                            item.InsertIntoVehicleNotification.BrakeUsageSeconds = statusPacket.mTransportExtraDetails.iBrakeUsageSeconds;
                            item.InsertIntoVehicleNotification.CANVehicleSpeed = statusPacket.mTransportExtraDetails.iCANVehicleSpeed;

                            byte[] bNoTrailer = new byte[6];

                            if (statusPacket.mTrailerTrack.oTrailers.Count >= 1)
                                item.InsertIntoVehicleNotification.Trailer1 = (byte[])statusPacket.mTrailerTrack.oTrailers[0];
                            else
                                item.InsertIntoVehicleNotification.Trailer1 = bNoTrailer;
                            if (statusPacket.mTrailerTrack.oTrailers.Count >= 2)
                                item.InsertIntoVehicleNotification.Trailer2 = (byte[])statusPacket.mTrailerTrack.oTrailers[1];
                            else
                                item.InsertIntoVehicleNotification.Trailer2 = bNoTrailer;
                            if (statusPacket.mTrailerTrack.oTrailers.Count >= 3)
                                item.InsertIntoVehicleNotification.Trailer3 = (byte[])statusPacket.mTrailerTrack.oTrailers[2];
                            else
                                item.InsertIntoVehicleNotification.Trailer3 = bNoTrailer;
                            if (statusPacket.mTrailerTrack.oTrailers.Count >= 4)
                                item.InsertIntoVehicleNotification.Trailer4 = (byte[])statusPacket.mTrailerTrack.oTrailers[3];
                            else
                                item.InsertIntoVehicleNotification.Trailer4 = bNoTrailer;
                            if (statusPacket.mTrailerTrack.oTrailers.Count >= 5)
                                item.InsertIntoVehicleNotification.Trailer5 = (byte[])statusPacket.mTrailerTrack.oTrailers[4];
                            else
                                item.InsertIntoVehicleNotification.Trailer5 = bNoTrailer;
                        }
                        if (dbVer >= 5.9)
                        {
                            item.InsertIntoVehicleNotification.StatusVehicle = v.GetUnitStateForReport(statusPacket);
                            item.InsertIntoVehicleNotification.StatusAuxiliary = v.GetAuxiliaryStateForReport(statusPacket);
                            item.InsertIntoVehicleNotification.SetPointGroupID = v.oPos.SetPointGroupID;
                            item.InsertIntoVehicleNotification.SetPointID = v.oPos.SetPointID;
                            item.InsertIntoVehicleNotification.VehicleScheduleID = statusPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID;
                            item.InsertIntoVehicleNotification.CheckPointIndex = statusPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex;
                        }
                        if (dbVer >= 6.0)
                        {
                            #region Refrigeration Values
                            if (statusPacket.mExtendedVariableDetails.Refrigeration != null)
                            {
                                item.InsertIntoVehicleNotification.ReferFlags = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationFlags;
                                item.InsertIntoVehicleNotification.ReferFuelPercentage = (double)statusPacket.mExtendedVariableDetails.Refrigeration.FuelPercentage;
                                item.InsertIntoVehicleNotification.ReferBatteryVolts = (double)statusPacket.mExtendedVariableDetails.Refrigeration.BatteryVolts;
                                item.InsertIntoVehicleNotification.ReferInput1 = statusPacket.mExtendedVariableDetails.Refrigeration.Input1On;
                                item.InsertIntoVehicleNotification.ReferInput2 = statusPacket.mExtendedVariableDetails.Refrigeration.Input2On;
                                item.InsertIntoVehicleNotification.ReferInput3 = statusPacket.mExtendedVariableDetails.Refrigeration.Input3On;
                                item.InsertIntoVehicleNotification.ReferInput4 = statusPacket.mExtendedVariableDetails.Refrigeration.Input4On;
                                item.InsertIntoVehicleNotification.ReferSensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.SensorsAvailable;
                                item.InsertIntoVehicleNotification.ReferHumidity = (double)statusPacket.mExtendedVariableDetails.Refrigeration.Humidity;
                                item.InsertIntoVehicleNotification.ReferSensor1 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor1Temperature;
                                item.InsertIntoVehicleNotification.ReferSensor2 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor2Temperature;
                                item.InsertIntoVehicleNotification.ReferSensor3 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor3Temperature;
                                item.InsertIntoVehicleNotification.ReferSensor4 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor4Temperature;
                                item.InsertIntoVehicleNotification.ReferSensor5 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor5Temperature;
                                item.InsertIntoVehicleNotification.ReferSensor6 = statusPacket.mExtendedVariableDetails.Refrigeration.Sensor6Temperature;

                                if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones != null)
                                {
                                    if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 1)
                                    {
                                        item.InsertIntoVehicleNotification.ReferZ1Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneNumber;
                                        item.InsertIntoVehicleNotification.ReferZ1OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].OperatingMode;
                                        item.InsertIntoVehicleNotification.ReferZ1Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ZoneAlarms;
                                        item.InsertIntoVehicleNotification.ReferZ1SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SensorsAvailable;
                                        item.InsertIntoVehicleNotification.ReferZ1ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir1;
                                        item.InsertIntoVehicleNotification.ReferZ1ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].ReturnAir2;
                                        item.InsertIntoVehicleNotification.ReferZ1SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir1;
                                        item.InsertIntoVehicleNotification.ReferZ1SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].SupplyAir2;
                                        item.InsertIntoVehicleNotification.ReferZ1Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].Setpoint;
                                        item.InsertIntoVehicleNotification.ReferZ1Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[0].EvaporatorCoil;
                                    }
                                    if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 2)
                                    {
                                        item.InsertIntoVehicleNotification.ReferZ2Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneNumber;
                                        item.InsertIntoVehicleNotification.ReferZ2OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].OperatingMode;
                                        item.InsertIntoVehicleNotification.ReferZ2Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ZoneAlarms;
                                        item.InsertIntoVehicleNotification.ReferZ2SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SensorsAvailable;
                                        item.InsertIntoVehicleNotification.ReferZ2ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir1;
                                        item.InsertIntoVehicleNotification.ReferZ2ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].ReturnAir2;
                                        item.InsertIntoVehicleNotification.ReferZ2SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir1;
                                        item.InsertIntoVehicleNotification.ReferZ2SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].SupplyAir2;
                                        item.InsertIntoVehicleNotification.ReferZ2Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].Setpoint;
                                        item.InsertIntoVehicleNotification.ReferZ2Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[1].EvaporatorCoil;
                                    }
                                    if (statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones.Length >= 3)
                                    {
                                        item.InsertIntoVehicleNotification.ReferZ3Zone = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneNumber;
                                        item.InsertIntoVehicleNotification.ReferZ3OperatingMode = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].OperatingMode;
                                        item.InsertIntoVehicleNotification.ReferZ3Alarms = (int)statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ZoneAlarms;
                                        item.InsertIntoVehicleNotification.ReferZ3SensorsAvailable = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SensorsAvailable;
                                        item.InsertIntoVehicleNotification.ReferZ3ReturnAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir1;
                                        item.InsertIntoVehicleNotification.ReferZ3ReturnAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].ReturnAir2;
                                        item.InsertIntoVehicleNotification.ReferZ3SupplyAir1 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir1;
                                        item.InsertIntoVehicleNotification.ReferZ3SupplyAir2 = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].SupplyAir2;
                                        item.InsertIntoVehicleNotification.ReferZ3Setpoint = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].Setpoint;
                                        item.InsertIntoVehicleNotification.ReferZ3Evap = statusPacket.mExtendedVariableDetails.Refrigeration.RefrigerationZones[2].EvaporatorCoil;
                                    }
                                }
                            }
                            
                            #endregion
                        }
                        PrepareAdvancedECMInsert(statusPacket.mECMAdvanced, item);
                        PrepareRoadTypeInsert(statusPacket.RoadTypeData, item);
                        result = AddToMSMQ(item);
                    }
                }
            }
            return result;
        }
        /// <summary>
        /// This method will take a 4 byte FleetID and a 4 byte vehicleID and convert them to a 8 byte long so a hash table can be uniquely keyed for a fleet and vehicle
        /// </summary>
        /// <param name="int iFleetID"></param>
        /// <param name="int iVehicleID"></param>
        /// <returns>long</returns>
        private long GetVehicleHashKey(int iFleetID, int iVehicleID)
        {
            long lRet = 0;
            byte[] bData = null;
            byte[] bConvert = null;
            try
            {
                bConvert = new byte[8];
                bData = BitConverter.GetBytes(iFleetID);
                bConvert[0] = bData[0];
                bConvert[1] = bData[1];
                bConvert[2] = bData[2];
                bConvert[3] = bData[3];
                bData = BitConverter.GetBytes(iVehicleID);
                bConvert[4] = bData[0];
                bConvert[5] = bData[1];
                bConvert[6] = bData[2];
                bConvert[7] = bData[3];
                lRet = BitConverter.ToInt64(bConvert, 0);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetVehicleHashKey(int iFleetID = " + Convert.ToString(iFleetID) + ", int iVehicleID = " + Convert.ToString(iVehicleID) + ")", ex);
            }
            return lRet;
        }
        /// <summary>
        /// This method will insert a mass declaration records into the database.
        /// </summary>
        /// <param name="MassDeclarationPacket massPacket"></param>
        /// <returns>string</returns>
        public string InsertMassDeclaration(MassDeclarationPacket massPacket)
        {
            bool bConnectionError = false;
            bool schemeError = false;
            SqlConnection oConn = null;
            SqlCommand massInsertCommand = null;
            SqlDataAdapter oDA = null;
            DataSet oDS = null;
            string sqlCmd = "";
            #region Try to establish a connection to the DB.  If the connection fails, wait for 5 secs and try again.
            try
            {
                oConn = new SqlConnection(sDSN);
                oConn.Open();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "InsertMassDeclaration(MassDeclarationPacket massPacket) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                bConnectionError = true;
            }
            while (bConnectionError)
            {
                try
                {
                    Thread.Sleep(5000);
                    oConn = new SqlConnection(sDSN);
                    oConn.Open();
                    bConnectionError = false;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "InsertMassDeclaration(MassDeclarationPacket massPacket) - Unable to connect to database - Will reconnect in 5 seconds", ex);
                    bConnectionError = true;
                }
            }
            #endregion
            try
            {
                StringBuilder sSQL = new StringBuilder("EXEC usp_MassInsert ");
                sSQL.Append((int)massPacket.cFleetId);
                sSQL.Append(", ");
                sSQL.Append(massPacket.iVehicleId);
                sSQL.Append(", '");
                sSQL.Append(massPacket.mCurrentClock.ToDateTime().ToString("MM/dd/yyyy HH:mm:ss"));
                sSQL.Append("', ");
                sSQL.Append((int)massPacket.ScalesType);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.ScalesState);
                sSQL.Append(", ");
                sSQL.Append(massPacket.TotalMass);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup1);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup2);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup3);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup4);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup5);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup6);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassAxleGroup7);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType1);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType2);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType3);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType4);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType5);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType6);
                sSQL.Append(", ");
                sSQL.Append((int)massPacket.AxleGroupType7);
                sSQL.Append(", '");
                sSQL.Append(massPacket.TicketId.Replace("'", "''"));
                sSQL.Append("', ");
                sSQL.Append(massPacket.LegID);
                sSQL.Append(", ");
                sSQL.Append(massPacket.MassSchemeItemID);
                sqlCmd = sSQL.ToString();
                massInsertCommand = oConn.CreateCommand();
                massInsertCommand.CommandType = CommandType.Text;
                massInsertCommand.CommandText = sqlCmd;
                oDS = new DataSet();
                using (oDA = new SqlDataAdapter(massInsertCommand))
                {
                    oDA.Fill(oDS);
                    if (oDS.Tables.Count > 0 && oDS.Tables[0].Rows.Count > 0)
                    {
                        schemeError = Convert.ToBoolean(oDS.Tables[0].Rows[0]["SchemeError"]);
                    }
                }
                RefreshMassDeclarationDataSet(oConn);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "InsertMassDeclaration(MassDeclarationPacket massPacket = \r\n" + ((massPacket == null) ? "null" : massPacket.ToDisplayString()) + "\r\n)\r\n\r\n SQL Command = '" + sqlCmd + "'", ex);
                return "InsertMassDeclaration - Error : " + ex.Message;
            }

            if (schemeError)
            {
                return "SchemeError";
            } 
            return null;
        }

        public string InsertDriverPointsRuleBreak(DriverPointsRuleBreakPacket packet, Vehicle v)
        {
            return InsertDriverPointsRuleBreak(packet, v, false, packet.RuleId);
        }

        public string InsertDriverPointsRuleBreak(object packet, Vehicle v, bool IsServerSidePacket, int ruleId)
        {
            string result = null;
            lock (this)
            {
                GeneralGPPacket gpPacket = null;
                DriverPointsRuleBreakPacket dpPacket = null;

                if (packet is GeneralGPPacket)
                {
                    gpPacket = (GeneralGPPacket)packet;
                    dpPacket = new DriverPointsRuleBreakPacket((GatewayProtocolPacket)packet, "dd/MM/yyyy HH:mm:ss");
                    dpPacket.RuleId = ruleId;
                    gpPacket.cMsgType = dpPacket.cMsgType;
                }
                else if(packet is DriverPointsRuleBreakPacket)
                {
                    dpPacket = (DriverPointsRuleBreakPacket)packet;
                    gpPacket = dpPacket.CreateGeneralGPPacket();    
                }
                if (gpPacket != null && dpPacket != null)
                {
                    TrackingQueueItem item = null;
                    if (IsServerSidePacket)
                    {
                        item = PopulateInsertCommand(gpPacket, v, 0, 0, 0, "");
                    }
                    else
                    {
                        item = UnsafePrepareNotificationRecord(gpPacket, v, "", "");
                    }

                    //save any SPN data
                    PrepareAdvancedECMInsert(gpPacket.mECMAdvanced, item);

                    PrepareRoadTypeInsert(gpPacket.RoadTypeData, item);

                    if (item != null)
                    {
                        item.SubCommands.Add(PrepareDriverPointsRuleBreak(dpPacket));
                        result = AddToMSMQ(item);
                    }
                }
            }
            return result;
        }

        public string InsertGenericPacket(GenericPacket genericPacket, Vehicle v)
        {
            string result = null;
            lock (this)
            {
                GeneralGPPacket gp = genericPacket.CreateGeneralGPPacket();
                TrackingQueueItem item = UnsafePrepareNotificationRecord(gp, v, "", "");

                if (item != null)
                {
                    //update the reason code
                    item.InsertIntoVehicleNotification.ReasonID = Convert.ToInt32(((int)(genericPacket.SubCommand + 1) << 8) + (genericPacket.cMsgType & 0xFF));

                    if (genericPacket.SubCommand == GenericSubCommands.DvrAlarm || genericPacket.SubCommand == GenericSubCommands.DvrRequest)
                    {
                        _log.InfoFormat("GenericPacket:{0}  Device:{1}, Event:{2}, EventId:{3}", genericPacket.SubCommand, genericPacket.DvrDeviceId, genericPacket.DvrEventType, genericPacket.DvrEventId);

                        item.InsertIntoVehicleNotification.Spare6 = string.Format("{0}|{1}|{2}", genericPacket.DvrDeviceId, genericPacket.DvrEventType, genericPacket.DvrEventId);

                        // Add a sub command for the alarm(EventType=0) and request(EventType=1) to link to the vehicle notification entry
                        if (genericPacket.DvrEventType == 0)
                        {
                            item.SubCommands.Add(new DvrAlarmItem
                            {
                                DvrAlarmId = genericPacket.DvrEventId
                            });
                        }
                        else if (genericPacket.DvrEventType == 1)
                        {
                            item.SubCommands.Add(new DvrRequestItem
                            {
                                DvrRequestId = genericPacket.DvrEventId
                            });
                        }
                    }
                    else if (genericPacket.SubCommand == GenericSubCommands.DvrStatus ||
                             genericPacket.SubCommand == GenericSubCommands.DvrError ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorDiskStatus ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorDiskRecording ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorCamerasConnected ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorComms ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorDataUsageLimit ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorUploadAttemptLimit ||
                             genericPacket.SubCommand == GenericSubCommands.DvrErrorIncompatibleFirmware)
                    {
                        if (genericPacket.DvrStatusVersion == -1)
                        {
                            // Dealing with a error entry and not a status entry
                            genericPacket.ParseDvrErrorCode(genericPacket.DvrErrorCode);

                            // Set the reason id to the sub command and message type
                            item.InsertIntoVehicleNotification.ReasonID = Convert.ToInt32(((int)(genericPacket.SubCommand + 1) << 8) + (genericPacket.cMsgType & 0xFF));

                            _log.InfoFormat("GenericPacket:{0}  ErrorCode:{1}", genericPacket.SubCommand, genericPacket.DvrErrorCode);

                            // Save the status information into Spare6 as well
                            item.InsertIntoVehicleNotification.Spare6 = string.Format("{0}|{1}", genericPacket.DvrStatusDeviceName, genericPacket.DvrErrorCode);
                        }
                        else if (genericPacket.DvrStatusVersion == 1)
                        {
                            // If a DVR status event with the error code set, then change to the relevant DVR Error event
                            if (genericPacket.SubCommand == GenericSubCommands.DvrStatus && genericPacket.DvrErrorCode > 0)
                            {
                                genericPacket.ParseDvrErrorCode(genericPacket.DvrErrorCode);
                            }

                            // Set the reason id to the sub command and message type
                            item.InsertIntoVehicleNotification.ReasonID = Convert.ToInt32(((int)(genericPacket.SubCommand + 1) << 8) + (genericPacket.cMsgType & 0xFF));

                            _log.InfoFormat("GenericPacket:{10}  ErrorCode:{0}|Connected:{1}|Recording:{2}|VideoConnectedChannel:{3}|VideoRecordChannel:{4}|HddOk:{5}|HddSize:{6}|HddFree:{7}|Timeoffset:{8},Firmware:{9}",
                                genericPacket.DvrErrorCode, genericPacket.DvrStatusConnected, genericPacket.DvrStatusRecording, genericPacket.DvrStatusVideoConnectedChannel, genericPacket.DvrStatusVideoRecordChannel,
                                genericPacket.DvrStatusHddOk, genericPacket.DvrStatusHddSize, genericPacket.DvrStatusHddFree, genericPacket.DvrStatusTimeOffset, genericPacket.DvrStatusFirmwareVersion, genericPacket.SubCommand);

                            // Save the status information into Spare6 as well
                            item.InsertIntoVehicleNotification.Spare6 = string.Format("{0}|{1}|{2}|{3}|{4}|{5}|{6}|{7}|{8}|{9}|{10}",
                                genericPacket.DvrStatusDeviceName,
                                genericPacket.DvrErrorCode,
                                genericPacket.DvrStatusConnected ? 1 : 0,
                                genericPacket.DvrStatusRecording ? 1 : 0,
                                genericPacket.DvrStatusVideoConnectedChannel,
                                genericPacket.DvrStatusVideoRecordChannel,
                                genericPacket.DvrStatusHddOk ? 1 : 0,
                                genericPacket.DvrStatusHddSize,
                                genericPacket.DvrStatusHddFree,
                                genericPacket.DvrStatusTimeOffset,
                                genericPacket.DvrStatusFirmwareVersion);

                            // Save the DVR status information (ignore if a Health Status as the T_DvrStatus entry will have already been created)
                            if (!genericPacket.DvrStatusIsHealthStatus)
                            {
                                PrepareDvrStatusInsert(genericPacket, item);
                            }
                        }
                        else
                        {
                            _log.ErrorFormat("GenericPacket:{0}. Received unknown version number ({1})", genericPacket.SubCommand, genericPacket.DvrStatusVersion);
                        }
                    }

                    //save any SPN data
                    PrepareAdvancedECMInsert(gp.mECMAdvanced, item);

                    PrepareRoadTypeInsert(gp.RoadTypeData, item);

                    if (genericPacket.IsRuleBreachPacket)
                    {
                        item.SubCommands.Add(PrepareRuleBreachInsert(genericPacket));
                    }

                    result = AddToMSMQ(item);
                }
            }
            return result;
        }

        public string InsertEcmHysteresisError(GeneralGPPacket gp, Vehicle v)
        {
            string result = null;
            lock (this)
            {
                TrackingQueueItem item = UnsafePrepareNotificationRecord(gp, v, "", "");
                if (item != null)
                {
                    //save any SPN data
                    PrepareAdvancedECMInsert(gp.mECMAdvanced, item);

                    PrepareRoadTypeInsert(gp.RoadTypeData, item);

                    //save the ECM Error
                    ConfigEcmData ecm = new ConfigEcmData();
                    ecm.EcmId = gp.EcmError.RuleId;
                    ecm.Raw = gp.EcmError.RawValue;
                    ecm.Processed = gp.EcmError.ProcessedValue;
                    ecm.PreviousRaw = gp.EcmError.PreviousRaw;
                    ecm.PreviousProcessed = gp.EcmError.PreviousProcessed;
                    item.SubCommands.Add(ecm);

                    result = AddToMSMQ(item);
                }
            }
            return result;
        }
        #endregion
        #region Get Position Support Functions
        /// <summary>
        /// This method will be used to convert degrees to radians
        /// </summary>
        /// <param name="double degrees"></param>
        /// <returns>double</returns>
        private double DegreesToRadians(double degrees)
        {
            return degrees * Math.PI / 180.0;
        }
        /// <summary>
        /// This method will be used to convert radians to degrees
        /// </summary>
        /// <param name="double radians"></param>
        /// <returns>double</returns>
        private double RadiansToDegrees(double radians)
        {
            return radians * 180.0 / Math.PI;
        }
        /// <summary>
        /// This method will be used to convert radians to nautical miles
        /// </summary>
        /// <param name="double radians"></param>
        /// <returns>double</returns>
        private double RadiansToNauticalMiles(double radians)
        {
            // There are 60 nautical miles for each degree
            return radians * 60 * 180 / Math.PI;
        }
        /// <summary>
        /// This method will be used to convert radians to meters
        /// </summary>
        /// <param name="double radians"></param>
        /// <returns>double</returns>
        private double RadiansToMeters(double radians)
        {
            // there are 1852 meters in a nautical mile
            return 1852 * RadiansToNauticalMiles(radians);
        }
        /// <summary>
        /// This method will be used to calculate the direction that dLatWP/dLongWP is from dLatUnit/dLongUnit.
        /// The function will return one of N,NE,E,SE,S,SW,W,NW
        /// </summary>
        /// <param name="decimal dLatUnit"></param>
        /// <param name="decimal dLongUnit"></param>
        /// <param name="decimal dLatWP"></param>
        /// <param name="decimal dLongWP"></param>
        /// <returns>string</returns>
        private string GetPostionString(decimal dLatUnit, decimal dLongUnit, decimal dLatWP, decimal dLongWP)
        {
            string sRet = "N";
            double dCourse = 0;
            double dLat = 0;
            double dLon = 0;
            double dLat1 = 0;
            double dLon1 = 0;
            double dLat2 = 0;
            double dLon2 = 0;
            double distanceNorth = 0;
            double distanceEast = 0;

            try
            {
                dLat1 = DegreesToRadians(Convert.ToDouble(dLatUnit));
                dLon1 = DegreesToRadians(Convert.ToDouble(dLongUnit));
                dLat2 = DegreesToRadians(Convert.ToDouble(dLatWP));
                dLon2 = DegreesToRadians(Convert.ToDouble(dLongWP));
                dLat = dLat2 - dLat1;
                dLon = dLon2 - dLon1;
                distanceNorth = dLat;
                distanceEast = dLon * Math.Cos(dLat1);

                dCourse = Math.Atan2(distanceEast, distanceNorth) % (2 * Math.PI);
                dCourse = RadiansToDegrees(dCourse);
                if (dCourse < 0)
                    dCourse += 360;
                if (dCourse <= 22.5)
                    sRet = "N";
                if (dCourse > 22.5 && dCourse <= 67.5)
                    sRet = "NE";
                if (dCourse > 67.5 && dCourse <= 112.5)
                    sRet = "E";
                if (dCourse > 112.5 && dCourse <= 157.5)
                    sRet = "SE";
                if (dCourse > 157.5 && dCourse <= 202.5)
                    sRet = "S";
                if (dCourse > 202.5 && dCourse <= 247.5)
                    sRet = "SW";
                if (dCourse > 247.5 && dCourse <= 292.5)
                    sRet = "W";
                if (dCourse > 292.5 && dCourse <= 337.5)
                    sRet = "NW";
                if (dCourse > 337.5)
                    sRet = "N";
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetPostionString(decimal dLatUnit, decimal dLongUnit, decimal dLatWP, decimal dLongWP)", ex);
            }
            return sRet;
        }
        /// <summary>
        /// This method will be used to calculate the distance that dLatWP/dLongWP is from dLatUnit/dLongUnit.
        /// The function will return the distance in degrees.
        /// </summary>
        /// <param name="decimal dLatUnit"></param>
        /// <param name="decimal dLongUnit"></param>
        /// <param name="decimal dLatWP"></param>
        /// <param name="decimal dLongWP"></param>
        /// <returns>decimal</returns>
        private decimal GetPostionDistance(decimal dLatUnit, decimal dLongUnit, decimal dLatWP, decimal dLongWP)
        {
            double dRet = 0;
            double dResLat = 0;
            double dResLon = 0;
            try
            {
                dResLat = Math.Pow(Convert.ToDouble(dLatWP) - Convert.ToDouble(dLatUnit), 2);
                dResLon = Math.Pow((Convert.ToDouble(dLongWP) - Convert.ToDouble(dLongUnit)) * Math.Cos(Convert.ToDouble(dLatUnit) * dRad), 2);
                dRet = Math.Sqrt(dResLat + dResLon) * 60 * 1.852;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetPostionDistance(decimal dLatUnit, decimal dLongUnit, decimal dLatWP, decimal dLongWP)", ex);
            }
            return Convert.ToDecimal(dRet);
        }
        /// <summary>
        /// This method will be used to calculate which vertial cell your position is within on a Sydways or Melways map page.
        /// </summary>
        /// <param name="string sMapName"></param>
        /// <param name="double dLowerLeftLat"></param>
        /// <param name="double dUppRightLat"></param>
        /// <param name="double dLat"></param>
        /// <returns>string</returns>
        private string GetLatitudeSector(string sMapName, double dLowerLeftLat, double dUppRightLat, double dLat)
        {
            double dLatResSector = 0;
            double dRet = 0;
            string sRet = "";
            if (sMapName.Trim().StartsWith("M"))
            {
                dLatResSector = ((dLowerLeftLat - dUppRightLat) / 12) * -1;
                if (dLatResSector != 0)
                    dRet = Math.Floor((dLat - dUppRightLat) / dLatResSector) * -1;
                if (dRet <= 12 && dRet >= 1)
                    sRet = Convert.ToString(dRet);
            }
            if (sMapName.Trim().StartsWith("S"))
            {
                dLatResSector = ((dLowerLeftLat - dUppRightLat) / 20) * -1;
                if (dLatResSector != 0)
                    dRet = Math.Floor((dLat - dUppRightLat) / dLatResSector) * -1;
                if (dRet <= 20 && dRet >= 1)
                    sRet = Convert.ToString(dRet);
            }
            return sRet;
        }
        /// <summary>
        /// This method will be used to calculate which horizontal cell your position is within on a Sydways or Melways map page.
        /// </summary>
        /// <param name="string sMapName"></param>
        /// <param name="double dLowerLeftLon"></param>
        /// <param name="double dUppRightLon"></param>
        /// <param name="double dLongitude"></param>
        /// <returns>string</returns>
        private string GetLongitudeSector(string sMapName, double dLowerLeftLon, double dUppRightLon, double dLongitude)
        {
            string sRet = "";
            int dRet = 0;
            double dLonResSector = 0;

            if (sMapName.Trim().StartsWith("M"))
            {
                dLonResSector = (dUppRightLon - dLowerLeftLon) / 10;
                if (dLonResSector != 0)
                    dRet = Convert.ToInt16(Math.Ceiling((dLongitude - dLowerLeftLon) / dLonResSector));
                switch (dRet)
                {
                    case 0: sRet = "A"; break;
                    case 1: sRet = "A"; break;
                    case 2: sRet = "B"; break;
                    case 3: sRet = "C"; break;
                    case 4: sRet = "D"; break;
                    case 5: sRet = "E"; break;
                    case 6: sRet = "F"; break;
                    case 7: sRet = "G"; break;
                    case 8: sRet = "H"; break;
                    case 9: sRet = "J"; break;
                    case 10: sRet = "K"; break;
                }
            }

            if (sMapName.Trim().StartsWith("S"))
            {
                dLonResSector = (dUppRightLon - dLowerLeftLon) / 16;
                switch (dRet)
                {
                    case 0: sRet = "A"; break;
                    case 1: sRet = "A"; break;
                    case 2: sRet = "B"; break;
                    case 3: sRet = "C"; break;
                    case 4: sRet = "D"; break;
                    case 5: sRet = "E"; break;
                    case 6: sRet = "F"; break;
                    case 7: sRet = "G"; break;
                    case 8: sRet = "H"; break;
                    case 9: sRet = "J"; break;
                    case 10: sRet = "K"; break;
                    case 11: sRet = "L"; break;
                    case 12: sRet = "M"; break;
                    case 13: sRet = "N"; break;
                    case 14: sRet = "P"; break;
                    case 15: sRet = "Q"; break;
                    case 16: sRet = "R"; break;
                }
            }
            return sRet;
        }
        #endregion
        #region Load Vehicle Data From History
        /// <summary>
        /// This method will be used to load the last known position for the vehicle from the database.
        /// </summary>
        /// <param name="int iFleetID"></param>
        /// <param name="int iVehicleID"></param>
        /// <param name="DateTime dtDeviceTime"></param>
        /// <returns>DataRow</returns>
        public DataRow LoadVehicleDataFromHistory(int iFleetID, int iVehicleID, DateTime dtDeviceTime)
        {
            try
            {
                DataSet data = new DataSet();
                string sql = "EXEC [usp_GetLastStatusReport] " + Convert.ToString(iFleetID) + ", " + Convert.ToString(iVehicleID) + ", '" + dtDeviceTime.ToString("MM/dd/yyyy HH:mm:ss") + "'";
                using (SqlConnection oConn = new SqlConnection(sDSN))
                using (SqlCommand oCmd = new SqlCommand(sql, oConn))
                using (SqlDataAdapter oDA = new SqlDataAdapter(oCmd))
                {
                    oDA.Fill(data);
                }

                if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                {
                    return data.Tables[0].Rows[0];
                }
            }
            catch (Exception exp)
            {
                _log.InfoFormat("LoadVehicleDataFromHistory : Could find a history for {0}/{1}. {2}", iFleetID, iVehicleID, exp.Message);
            }
            return null;
        }
        /// <summary>
        /// This method will be used to load the last known position for the vehicle from the database.
        /// </summary>
        /// <param name="int iFleetID"></param>
        /// <param name="int iVehicleID"></param>
        /// <param name="DateTime dtDeviceTime"></param>
        /// <returns>GeneralGPPacket</returns>
        public GeneralGPPacket LoadVehicleDataFromHistory(int iFleetID, int iVehicleID, DateTime dtDeviceTime, Vehicle v)
        {
            GeneralGPPacket mPacket = null;

            try
            {
                DataRow dr = LoadVehicleDataFromHistory(iFleetID, iVehicleID, dtDeviceTime);
                if (dr["ReasonID"] != DBNull.Value)
                {
                    if (v.oPos == null)
                    {
                        v.oPos = new PositionDetails();
                    }
                    v.oPos.Distance = Convert.ToInt32((dr["Distance"] == DBNull.Value ? 0 : dr["Distance"]));
                    v.oPos.Position = Convert.ToString((dr["Position"] == DBNull.Value ? "" : dr["Position"]));
                    v.oPos.PlaceName = Convert.ToString((dr["PlaceName"] == DBNull.Value ? "" : dr["PlaceName"]));
                    v.oPos.MapRef = Convert.ToString((dr["MapRef"] == DBNull.Value ? "" : dr["MapRef"]));

                    v.oPos.SetPointID = Convert.ToInt32((dr["SetPointID"] == DBNull.Value ? 0 : dr["SetPointID"]));

                    if (v.oPos.SetPointID > 0)
                    {
                        DataRow[] drSetPoint = null;
                        lock (oSetPointDataSetSync)
                            drSetPoint = mSetPointDataSet.Tables[0].Select("ID = " + v.oPos.SetPointID);
                        if (drSetPoint.Length > 0)
                        {
                            v.oPos.SetPointGroupID = Convert.ToInt32((drSetPoint[0]["SetPointGroupID"] == DBNull.Value ? 0 : drSetPoint[0]["SetPointGroupID"]));
                            v.oPos.SetPointNumber = Convert.ToInt32((drSetPoint[0]["SetPointNumber"] == DBNull.Value ? 0 : drSetPoint[0]["SetPointNumber"]));
                        }
                        else
                        {
                            v.oPos.SetPointID = 0;
                            v.oPos.SetPointGroupID = 0;
                            v.oPos.SetPointNumber = 0;
                        }
                    }

                    v.oPos.RouteVehicleScheduleID = Convert.ToUInt32((dr["RouteVehicleScheduleID"] == DBNull.Value ? 0 : dr["RouteVehicleScheduleID"]));
                    v.oPos.RouteCheckPointIndex = Convert.ToInt32((dr["RouteCheckPointIndex"] == DBNull.Value ? 0 : dr["RouteCheckPointIndex"]));

                    mPacket = GeneratePacketData(iFleetID, iVehicleID, dr);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadVehicleDataFromHistory(int iFleetID, int iVehicleID, DateTime dtDeviceTime)", ex);
                mPacket = null;
            }
            if (mPacket == null)
            {
                _log.Info("LoadVehicleDataFromHistory : Could find a history for " + Convert.ToString(iFleetID) + "/" + Convert.ToString(iVehicleID));
            }
            return mPacket;
        }
        /// <summary>
        /// This method will be used to load the GeneralGPPacket from the datarow of last known position for the vehicle from the database.
        /// </summary>
        /// <param name="int iFleetID"></param>
        /// <param name="int iVehicleID"></param>
        /// <param name="DataRow drData"></param>
        /// <returns>GeneralGPPacket</returns>
        private GeneralGPPacket GeneratePacketData(int iFleetID, int iVehicleID, DataRow drData)
        {
            string sTemp = "";
            string[] sIOValues = null;
            GeneralGPPacket mPacket = null;

            try
            {
                if (drData["ReasonID"] != System.DBNull.Value)
                {
                    #region Create a new packet
                    mPacket = new GeneralGPPacket(_serverTime_DateFormat);
                    mPacket.cOurSequence = 0;
                    mPacket.cAckSequence = 0;
                    mPacket.cFleetId = (byte)iFleetID;
                    mPacket.iVehicleId = (uint)iVehicleID;
                    mPacket.bAckImmediately = true;
                    mPacket.bAckRegardless = true;
                    mPacket.bArchivalData = false;
                    mPacket.bSendFlash = true;
                    mPacket.cCannedMessageNumber = (byte)0x00;
                    mPacket._cProgramMajor = (byte)0x00;
                    mPacket._iProgramMinor = (byte)0x00;
                    mPacket.cIOBoxProgramMajor = (byte)0x00;
                    mPacket.cIOBoxProgramMinor = (byte)0x00;
                    mPacket.iHardwareTypeInbound = GeneralGPPacket.Alive_IMEIESN_HardwareType.Platform_3026_IMEI;
                    mPacket.iHardwareVersionNumber = 1;
                    mPacket.iLoginFlags = 0;
                    mPacket.iPreferedSlotNumber = 1;
                    mPacket.lSerialNumber = 1;
                    mPacket.sNetworkName = "";
                    mPacket.sSimNumber = "";
                    mPacket.cMsgType = (byte)Convert.ToInt32(drData["ReasonID"]);
                    mPacket.mImieData = new cIMIEData();
                    mPacket.mImieData.i_FleetID = iFleetID;
                    mPacket.mImieData.i_VehicleID = iVehicleID;
                    mPacket.mImieData.i_HardwareType = (int)GeneralGPPacket.Alive_IMEIESN_HardwareType.Platform_3026_IMEI;
                    mPacket.mImieData.i_IOBoxVerMajor = 0;
                    mPacket.mImieData.i_IOBoxVerMinor = 0;
                    mPacket.mImieData.i_SoftwareVer = 0;
                    mPacket.mImieData.i_SoftwareVerMajor = 0;
                    mPacket.mImieData.i_SoftwareVerMinor = 0;
                    mPacket.mImieData.s_CDMAESN = "";
                    mPacket.mImieData.s_IMIE = "";
                    mPacket.mImieData.s_SimNumber = "";
                    mPacket.mFixClock = new GPClock("GPSClock", Convert.ToDateTime(drData["GPSTime"]), _serverTime_DateFormat);
                    mPacket.mCurrentClock = new GPClock("GPSClock", Convert.ToDateTime(drData["GPSTime"]), _serverTime_DateFormat);
                    mPacket.mFix = new GPPositionFix();
                    mPacket.mFix.iDirection = Convert.ToInt32(drData["Direction"]);
                    mPacket.mFix.cSpeed = (byte)Convert.ToInt32(drData["Speed"]);
                    mPacket.mFix.dLatitude = Convert.ToDecimal(drData["Latitude"]);
                    mPacket.mFix.dLongitude = Convert.ToDecimal(drData["Longitude"]);
                    mPacket.mFix.cFlags = (byte)0x00;
                    mPacket.mStatus = new GPStatus();
                    mPacket.mStatus.cButtonStatus = (byte)Convert.ToInt32(drData["ButtonStatus"]);
                    mPacket.mStatus.cExtendedStatusFlag = (byte)0x00;
                    mPacket.mStatus.cInputStatus = (byte)Convert.ToInt32(drData["InputStatus"]);
                    mPacket.mStatus.cLightStatus = (byte)Convert.ToInt32(drData["LightStatus"]);
                    mPacket.mStatus.cOutputStatus = (byte)Convert.ToInt32(drData["OutputStatus"]);
                    mPacket.mStatus.cSpareStatus = (byte)0x00;
                    mPacket.mStatus.cStatusFlag = (byte)Convert.ToInt32(drData["Status"]);
                    mPacket.cButtonNumber = (byte)0x00;
                    mPacket.cInputNumber = (byte)0x00;
                    mPacket.mDistance = new GPDistance();
                    mPacket.mDistance.cMaxSpeed = (byte)Convert.ToInt32(drData["MaxSpeed"]);
                    mPacket.mDistance.iSamples = Convert.ToInt32(drData["Samples"]);
                    mPacket.mDistance.lSpeedAccumulator = Convert.ToInt64(drData["SpeedAcc"]);
                    mPacket.mEngineData = new GPEngineData();
                    mPacket.mEngineData.fGForceBack = (float)Convert.ToDouble(drData["MaxB"]);
                    mPacket.mEngineData.fGForceFront = (float)Convert.ToDouble(drData["MaxF"]);
                    mPacket.mEngineData.fGForceLeftRight = (float)Convert.ToDouble(drData["MaxLR"]);
                    mPacket.mEngineData.iBrakeApplications = Convert.ToInt32(drData["BrakeHits"]);
                    mPacket.mEngineData.iCoolantTemperature = Convert.ToInt32(drData["MaxEngCoolTemp"]);
                    mPacket.mEngineData.iGear = Convert.ToInt32(drData["GearPosition"]);
                    mPacket.mEngineData.iMaxRPM = Convert.ToInt32(drData["MaxEngSpeed"]);
                    mPacket.mEngineData.iOilPressure = Convert.ToInt32(drData["MinOilPressure"]);
                    mPacket.mEngineData.iOilTemperature = Convert.ToInt32(drData["MaxEngOilTemp"]);
                    mPacket.mEngineSummaryData = new GPEngineSummaryData();
                    mPacket.mEngineSummaryData.fFuelEconomy = Convert.ToDouble(drData["FuelEconomy"]);
                    mPacket.mEngineSummaryData.iBatteryVoltage = Convert.ToInt32(drData["BatteryVolts"]);
                    mPacket.mEngineSummaryData.iOdometer = Convert.ToInt32(drData["Odometer"]);
                    mPacket.mEngineSummaryData.iTotalEngineHours = Convert.ToInt32(drData["TotalEngHours"]);
                    mPacket.mEngineSummaryData.iTotalFuel = Convert.ToInt32(drData["TotalFuelUsed"]);
                    mPacket.mEngineSummaryData.iTripFuel = Convert.ToInt32(drData["TripFuel"]);
                    mPacket.mExtendedValues = new GPExtendedValues();
                    sTemp = Convert.ToString(drData["Spare1"]);
                    if (sTemp.Length > 0 && sTemp.IndexOf("%") > 0)
                    {
                        sIOValues = sTemp.Split("%".ToCharArray());
                        mPacket.mExtendedValues.iValue1 = Convert.ToInt32(sIOValues[0]);
                        mPacket.mExtendedValues.iValue2 = Convert.ToInt32(sIOValues[1]);
                        mPacket.mExtendedValues.iValue3 = Convert.ToInt32(sIOValues[2]);
                        mPacket.mExtendedValues.iValue4 = Convert.ToInt32(sIOValues[3]);
                    }
                    mPacket.mExtendedVariableDetails = new GBVariablePacketExtended59();
                    mPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex = 0;
                    mPacket.mExtendedVariableDetails.CurrentSetPointGroup = 0;
                    mPacket.mExtendedVariableDetails.CurrentSetPointNumber = 0;
                    mPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID = 0;
                    mPacket.mExtendedVariableDetails.UnitStatus = Convert.ToInt64(drData["StatusVehicle"]);
                    mPacket.mExtendedVariableDetails.Refrigeration = null;
                    mPacket.mTrailerTrack = new GPTrailerTrack();
                    mPacket.mTrailerTrack.iTrailerCount = 0;
                    mPacket.mTransportExtraDetails = new GPTransportExtraDetails();
                    mPacket.mTransportExtraDetails.fFuelEconomy = (float)Convert.ToDouble(drData["FuelEconomy"]);
                    mPacket.mTransportExtraDetails.iBatteryVolts = Convert.ToInt32(drData["BatteryVolts"]);
                    mPacket.mTransportExtraDetails.iBrakeUsageSeconds = Convert.ToInt32(drData["BrakeUsageSeconds"]);
                    mPacket.mTransportExtraDetails.iCANVehicleSpeed = 0;
                    mPacket.mTransportExtraDetails.iEngineHours = Convert.ToInt32(drData["TotalEngHours"]);
                    mPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(drData["Odometer"]);
                    mPacket.mTransportExtraDetails.iRPMZone1 = Convert.ToInt32(drData["RPMZone1"]);
                    mPacket.mTransportExtraDetails.iRPMZone2 = Convert.ToInt32(drData["RPMZone2"]);
                    mPacket.mTransportExtraDetails.iRPMZone3 = Convert.ToInt32(drData["RPMZone3"]);
                    mPacket.mTransportExtraDetails.iRPMZone4 = Convert.ToInt32(drData["RPMZone4"]);
                    mPacket.mTransportExtraDetails.iSpeedZone1 = Convert.ToInt32(drData["SpeedZone1"]);
                    mPacket.mTransportExtraDetails.iSpeedZone2 = Convert.ToInt32(drData["SpeedZone2"]);
                    mPacket.mTransportExtraDetails.iSpeedZone3 = Convert.ToInt32(drData["SpeedZone3"]);
                    mPacket.mTransportExtraDetails.iSpeedZone4 = Convert.ToInt32(drData["SpeedZone4"]);
                    mPacket.mTransportExtraDetails.iTotalFuelUsed = Convert.ToInt32(drData["TotalFuelUsed"]);
                    mPacket.bIsPopulated = true;
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GeneratePacketData(DataRow drData)", ex);
                mPacket = null;
            }
            return mPacket;
        }
        #endregion
        #region Firmware Logins
        public FirmwareDriverLoginReply ValidateFirmwareLogin(byte msgType, int fleetId, int vehicleId, DateTime gpsTime, uint driverLogin, int driverPin, string driverName, byte[] cardID, byte cardType, byte[] cardReaderSerial, List<object[]> sendLogoutsTo, decimal latitude, decimal longitude, string suburb, uint odometer, int totalFuelUsed)
        {
            FirmwareDriverLoginReply oReply = null;
            int driverId = 0;
            int driverLoginAsInt = 0;
            try
            {
                if (driverLogin < int.MaxValue)
                    driverLoginAsInt = (int)driverLogin;

                string sSQL = "EXEC isp_ListenerCardReaderValidation [FleetID], [VehicleID], '[GPSTime]', [LoginID], [PinNumber], '[CardSerial]', [CardType], '[DriverName]', '[CardReaderSerial]', [LogonType], [BreakType], [Latitude], [Longitude], '[Suburb]', [Odometer], [TotalFuelUsed]";
                sSQL = sSQL.Replace("[FleetID]", Convert.ToString(fleetId));
                sSQL = sSQL.Replace("[VehicleID]", Convert.ToString(vehicleId));
                sSQL = sSQL.Replace("[GPSTime]", gpsTime.ToString("MM/dd/yyyy HH:mm:ss"));
                sSQL = sSQL.Replace("[LoginID]", Convert.ToString(driverLoginAsInt));
                sSQL = sSQL.Replace("[PinNumber]", Convert.ToString(driverPin));
                if (cardID != null && cardID.Length > 0)
                    sSQL = sSQL.Replace("[CardSerial]", BitConverter.ToString(cardID));
                else
                    sSQL = sSQL.Replace("[CardSerial]", "");
                sSQL = sSQL.Replace("[CardType]", Convert.ToString((int)cardType));
                sSQL = sSQL.Replace("[DriverName]", driverName.Replace("'", "''"));
                if (cardReaderSerial != null && cardReaderSerial.Length > 0)
                    sSQL = sSQL.Replace("[CardReaderSerial]", BitConverter.ToString(cardReaderSerial));
                else
                    sSQL = sSQL.Replace("[CardReaderSerial]", "");
                if (msgType == GeneralGPPacket.STATUS_LOGIN)
                    sSQL = sSQL.Replace("[LogonType]", "1");
                else
                    sSQL = sSQL.Replace("[LogonType]", "0");
                if (msgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START || msgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END)
                    sSQL = sSQL.Replace("[BreakType]", "1");
                else
                    sSQL = sSQL.Replace("[BreakType]", "0");
                sSQL = sSQL.Replace("[Latitude]", Convert.ToString(latitude));
                sSQL = sSQL.Replace("[Longitude]", Convert.ToString(longitude));
                sSQL = sSQL.Replace("[Suburb]", suburb.Replace("'", "''"));
                sSQL = sSQL.Replace("[Odometer]", Convert.ToString(odometer));
                sSQL = sSQL.Replace("[TotalFuelUsed]", Convert.ToString(totalFuelUsed));
                _log.Info(sSQL);

                #region Retrieve the data from the database
                SqlConnection oConn = new SqlConnection(sDSN);
                oConn.OpenExtentionMethod(1); //use MTData user id
                SqlDataAdapter objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                SqlCommand objSQLCmd = new SqlCommand(sSQL, oConn);
                //objSQLCmd.CommandTimeout = 3600;
                objSQLDA.SelectCommand = objSQLCmd;
                DataSet oDS = new DataSet();
                objSQLDA.Fill(oDS);
                objSQLCmd.Dispose();
                objSQLCmd = null;
                objSQLDA.Dispose();
                objSQLDA = null;
                #endregion
                if (oDS != null && oDS.Tables.Count > 0 && oDS.Tables[0].Rows.Count > 0)
                {
                    DataRow dr = oDS.Tables[0].Rows[0];
                    if (dr["DriverID"] != DBNull.Value)
                        driverId = Convert.ToInt32(dr["DriverID"]);
                    // If the login was sucessful
                    if ((FirmwareDriverLoginReply.FirmwareLoginResult)Convert.ToInt32(dr["LoginReply"]) == FirmwareDriverLoginReply.FirmwareLoginResult.LoginSuccess)
                    {
                        // Send a firmware log out to any other units that this card is currently logged into.
                        if (oDS.Tables.Count > 1 && oDS.Tables[1].Rows.Count > 0)
                        {
                            foreach (DataRow drLogout in oDS.Tables[1].Rows)
                            {
                                oReply = new FirmwareDriverLoginReply(_serverTime_DateFormat);
                                oReply.LoginResult = FirmwareDriverLoginReply.FirmwareLoginResult.LoggedIntoAnotherVehicle;
                                oReply.DriverID = driverLoginAsInt;
                                oReply.DriverPin = driverPin;
                                oReply.CardID = cardID;
                                oReply.DriverName = "";
                                oReply.EncodeDataField();

                                object[] logout = new object[3];
                                logout[0] = (byte)Convert.ToInt32(drLogout["FleetID"]);
                                logout[1] = Convert.ToUInt32(drLogout["VehicleID"]);
                                logout[2] = oReply;
                                sendLogoutsTo.Add(logout);
                            }
                        }
                    }
                    // Create the login reply for this vehicle
                    oReply = new FirmwareDriverLoginReply(_serverTime_DateFormat);
                    oReply.LoginResult = (FirmwareDriverLoginReply.FirmwareLoginResult)Convert.ToInt32(dr["LoginReply"]);
                    oReply.DriverID = driverLoginAsInt;
                    oReply.DriverPin = driverPin;
                    oReply.CardID = cardID;
                    oReply.DriverName = Convert.ToString(dr["DriverName"]);
                    oReply.EncodeDataField();

                    if (oDS.Tables.Count > 2 && oDS.Tables[2].Rows.Count > 0)
                    {
                        foreach (DataRow drErr in oDS.Tables[2].Rows)
                        {
                            //Console.WriteLine(Convert.ToString(drErr["ErrMsg"]));
                            _log.Info(Convert.ToString(drErr["ErrMsg"]));

                        }
                    }

                }
                else
                {
                    oReply = new FirmwareDriverLoginReply(_serverTime_DateFormat);
                    oReply.LoginResult = FirmwareDriverLoginReply.FirmwareLoginResult.CredentailFail;
                    oReply.DriverID = driverLoginAsInt;
                    oReply.DriverPin = driverPin;
                    oReply.CardID = cardID;
                    oReply.DriverName = driverName;
                    oReply.EncodeDataField();
                }
            }
            catch (Exception ex)
            {
                _log.Error(sClassName + "ValidateFirmwareLogin(int fleetId, int vehicleId, uint driverLogin, int driverPin, string driverName, byte[] cardID, byte cardType, byte[] cardReaderSerial)", ex);
            }
            return oReply;
        }
        #endregion
        #region MSMQ

        private void InitialiseServerLoadLevel()
        {
            int queueSize = _msQueue.QSize;
            foreach (ServerLoadLevel level in _serverLoadLevels)
            {
                if (queueSize >= level.Lower)
                {
                    SetCurrentServerLoad(level);
                }
            }
        }
        private void UpdateServerLoadLevel()
        {
            int queueSize = _msQueue.QSize;
            if (queueSize > _currentServerLoad.Upper || queueSize < _currentServerLoad.Lower)
            {
                //queue size is outside the server level - as we are no longer removing packets from the queue, we have to manually re count the queue
                queueSize = _msQueue.ReCount();
            }

            if (queueSize > _currentServerLoad.Upper)
            {
                //check the next queue to see if we have moved up a level
                ServerLoadLevel level = _serverLoadLevels.Find(s => s.Level == _currentServerLoad.Level + 1);
                if (level != null)
                {
                    SetCurrentServerLoad(level);
                }
            }
            else if (queueSize < _currentServerLoad.Lower)
            {
                SetCurrentServerLoad(_serverLoadLevels.Find(s => s.Level == _currentServerLoad.Level - 1));
            }
        }
        public bool SendFlash { get { return _currentServerLoad.SendFlash; } }
        public int CurrentServerLoadLevel { get { return _currentServerLoad.Level; } }
        public int CurrentServerLoadStatus { get { return _currentServerLoad.StatusReports; } }
        private void SetCurrentServerLoad(ServerLoadLevel load)
        {
            if (load != null)
            {
                _currentServerLoad = load;
                _log.ErrorFormat("CurrentServerLoad set to {0}", _currentServerLoad.Level);
                _statsLog.ErrorFormat("CurrentServerLoad set to {0}", _currentServerLoad.Level);

                string wmiServiceName = "ListenerService";
                if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["WmiServiceName"]))
                {
                    wmiServiceName = ConfigurationManager.AppSettings["WmiServiceName"];
                }
                MTData.Common.WMI.MTDataMonitor monitor = MTData.Common.WMI.MTDataMonitor.GetInstance(wmiServiceName);
                if (monitor != null && monitor.Published)
                {
                    monitor.ServerLoadLevel = load.Level;
                }
            }
        }

        /// <summary>
        /// This method will be used to encode and enqueue a SQL command on the message queue for processing.
        /// </summary>
        /// <param name="SqlCommand oCmd"></param>
        /// <returns>string</returns>
        private string AddToMSMQ(TrackingQueueItem item)
        {
            int iTimeOutCounter = 0;
            string sRet = null;
            MemoryStream stream = null;
            DateTime dtTemp = DateTime.MinValue;
            bool bDataEnqueued = true;
            long iStartEnqueueTicks = 0;
            long iEndEnqueueTicks = 0;
            try
            {
                #region Setup the MSMQ if required
                if (_msQueue == null)
                {
                    lock (MSQSyncRoot)
                    {
                        _msQueue = new MSQueue(System.Configuration.ConfigurationManager.AppSettings["MessageQueueName"], Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["MessageQueueMaxSize"]));
                        if (bRecoverableMSMQ)
                            _msQueue.Q.DefaultPropertiesToSend.Recoverable = true;
                    }
                }
                #endregion
                stream = new MemoryStream();
                DataContractJsonSerializer ser = new DataContractJsonSerializer(typeof(TrackingQueueItem));
                item.TimePutOnQueue = DateTime.Now.Ticks;
                ser.WriteObject(stream, item);

                #region Insert the message on to the Q
                iEndEnqueueTicks = 0;
                iStartEnqueueTicks = DateTime.Now.Ticks;
                lock (MSQSyncRoot)
                {
                    try
                    {
                        
                        bDataEnqueued = _msQueue.InsertMessageToQueue(stream.ToArray(), MessagePriority.Normal, bRecoverableMSMQ);
                        item.AddToQueueTime = DateTime.Now.Ticks - item.TimePutOnQueue;
                        UpdateServerLoadLevel();
                    }
                    catch (System.Exception ex)
                    {
                        _log.Error(sClassName + "EnqueueInsertIntoVehicleNotification(SqlCommand oCmd) - Failed to insert into MSMQ", ex);
                        bDataEnqueued = false;
                    }
                }
                iEndEnqueueTicks = DateTime.Now.Ticks;

                if (bDataEnqueued)
                {
                    lock (oSyncUpdateCounter)
                    {
                        _transactionsAdded++;
                    }
                }
                #endregion
                #region If the data failed to enqueue, sleep for the specified period and try again.
                while (!bDataEnqueued && _msQueue != null)
                {
                    iTimeOutCounter = 0;
                    while (iTimeOutCounter < iFailedToInsertToMSMQWaitPeriod && _msQueue != null)
                    {
                        Thread.Sleep(100);
                        iTimeOutCounter += 100;
                    }
                    if (_msQueue != null)
                    {
                        #region Insert the message on to the Q
                        lock (MSQSyncRoot)
                        {
                            try
                            {
                                bDataEnqueued = _msQueue.InsertMessageToQueue(stream.ToArray(), MessagePriority.Normal, bRecoverableMSMQ);
                                UpdateServerLoadLevel();
                            }
                            catch (System.Exception ex)
                            {
                                _log.Error(sClassName + "EnqueueInsertIntoVehicleNotification(SqlCommand oCmd) - Failed to insert into MSMQ", ex);
                                bDataEnqueued = false;
                            }
                        }
                        if (bDataEnqueued)
                        {
                            lock (oSyncUpdateCounter)
                            {
                                _transactionsAdded++;
                            }
                        }
                        #endregion
                    }
                }
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "EnqueueInsertIntoVehicleNotification(SqlCommand oCmd)", ex);
                sRet = ex.Message;
            }
            return sRet;
        }
        /// <summary>
        /// This method is alternate thread code to dequeue and decode a SQL command from the message queue for processing.
        /// </summary>
        /// <param name="EnhancedThread sender"></param>
        /// <param name="object data"></param>
        /// <returns>string</returns>
        #region Performace Counters
        private object oSyncUpdateCounter = new object();
        private int _transactionsAdded = 0;

        /// <summary>
        /// This method is used to keep statistics about how the listener process is performing.
        /// </summary>
        /// <param name="EnhancedThread sender"></param>
        /// <param name="object data"></param>
        /// <returns>string</returns>
        public void GetAndResetUpdateCounters(ref int transactionsAdded, ref int currentTransactionQueueSize)
        {
            try
            {
                lock (oSyncUpdateCounter)
                {
                    // Retrrieve the values
                    transactionsAdded = _transactionsAdded;
                    currentTransactionQueueSize = _msQueue.ReCount();

                    // Reset the counters
                    _transactionsAdded = 0;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetAndResetUpdateCounter()", ex);
            }
        }
        #endregion
        #endregion


    }
}


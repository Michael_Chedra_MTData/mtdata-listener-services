#undef MySQLIntegration
using System;
#if MySQLIntegration
using System.Collections;
using System.Threading;
using MySql.Data.MySqlClient;
#endif

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
	/// <summary>
	/// Summary description for cMySQLInterface.
	/// </summary>
	public class MySQLInterface
	{
		/// <summary>
		/// This class is used to initialise the MySql interface, and the manner in which it will operate.
		/// </summary>
		public class cMySQLInterfaceParams
		{
			public const int DEFAULT_NOOP_DELAY = 10;
			public const int DEFAULT_NOCONN_DELAY = 1000;
			public const int DEFAULT_SUCC_DELAY = 5;

			public string ConnectionString = "";
			public int NoOperationDelay = DEFAULT_NOOP_DELAY;
			public int NoConnectionDelay = DEFAULT_NOCONN_DELAY;
			public int SuccessDelay = DEFAULT_SUCC_DELAY;

			/// <summary>
			/// This constructor can be called by code to initalise the parameters directly.
			/// </summary>
			/// <param name="connectionString"></param>
			/// <param name="noOperationDelay"></param>
			/// <param name="noConnectionDelay"></param>
			/// <param name="successDelay"></param>
			public cMySQLInterfaceParams(string connectionString, int noOperationDelay, int noConnectionDelay, int successDelay)
			{
				ConnectionString = connectionString;
				NoOperationDelay = noOperationDelay;
				NoConnectionDelay = noConnectionDelay;
				SuccessDelay = successDelay;
			}

			/// <summary>
			/// This constructor can be called when reading string values from a config file. It will allow defaults to be used
			/// if no valid value is passed in.
			/// </summary>
			/// <param name="connectionString"></param>
			/// <param name="noOperationDelay"></param>
			/// <param name="noConnectionDelay"></param>
			/// <param name="successDelay"></param>
			public cMySQLInterfaceParams(string connectionString, string noOperationDelay, string noConnectionDelay, string successDelay)
			{
				if (connectionString == null)
					ConnectionString = "";
				else
					ConnectionString = connectionString;

				NoOperationDelay = CheckValue(noOperationDelay, DEFAULT_NOOP_DELAY);
				NoConnectionDelay = CheckValue(noConnectionDelay, DEFAULT_NOCONN_DELAY);
				SuccessDelay = CheckValue(successDelay, DEFAULT_SUCC_DELAY);
			}

			/// <summary>
			/// Check the value to ensure we are using the correct one.
			/// </summary>
			/// <param name="newValue"></param>
			/// <param name="defaultValue"></param>
			/// <returns></returns>
			private int CheckValue(string newValue, int defaultValue)
			{
#if MySQLIntegration
				int result = defaultValue;
				if (newValue != null)
					try
					{
						result = Convert.ToInt32(newValue);
					}
					catch(Exception)
					{
						result = defaultValue;
					}
				return result;
#else
                return defaultValue;
#endif
			}
		}

		#region Private Vars
#if MySQLIntegration
		private Queue oQueue = null;
		private Thread tMySQLThread = null;
		private bool bMySQLThreadActive = false;

		private MySqlConnection oSqlConn = null; 

		private cMySQLInterfaceParams _params = null;
#endif
		#endregion

		#region Constructors and Threads
		public MySQLInterface(cMySQLInterfaceParams parameters)
		{
            #if MySQLIntegration
			_params = parameters;

			oQueue = Queue.Synchronized(new Queue());
			tMySQLThread = new Thread(new ThreadStart(ProcessMySQLQueue));
			tMySQLThread.Name = "cMySQLInterface.MySQLInterfaceThread";
			bMySQLThreadActive = false;
            #endif
		}

		public void Start()
		{
            #if MySQLIntegration
			WriteToConsole(String.Format("Start - Starting MySqlInterface with ConnectionString [{0}], NoOp [{1}], NoConn [{2}], Success [{3}]", _params.ConnectionString, _params.NoOperationDelay, _params.NoConnectionDelay, _params.SuccessDelay));
			try
			{
				if (bMySQLThreadActive) return;

				if (tMySQLThread == null)
				{
					tMySQLThread = new Thread(new ThreadStart(ProcessMySQLQueue));
					tMySQLThread.Name = "cMySQLInterface.MySQLInterfaceThread";
				}

				if (oQueue == null)
					oQueue = Queue.Synchronized(new Queue());
				else
					oQueue.Clear();

				bMySQLThreadActive = true;
				tMySQLThread.Start();
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole("Start - ", ex);
			}
            #endif
        }

		public void Stop()
        {
#if MySQLIntegration
            if (!bMySQLThreadActive) return;
			
			try
			{
				bMySQLThreadActive = false;
				lock(oQueue.SyncRoot)
					oQueue.Clear();
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole("Stop - ", ex);
			}
#endif
        }

		public void Dispose()
		{
#if MySQLIntegration
			bMySQLThreadActive = false;
			if (oQueue != null)
			{
				try
				{
					oQueue.Clear();
					oQueue = null;
				}
				catch(System.Exception ex)
				{
					WriteToErrorConsole("", ex);
				}
			}
#endif
        }

		/// <summary>
		/// This method will open the connection to mySql.
		/// </summary>
		/// <returns></returns>
		private bool OpenConnection()
		{
#if MySQLIntegration
			bool result = false;
			try
			{
				WriteToConsole("MySqlUpdate Handler - Opening New MySqlConnection");
				oSqlConn = new MySqlConnection(_params.ConnectionString);
				oSqlConn.Open();
				WriteToConsole("MySqlUpdate Handler - Opened New MySqlConnection");
				result = true;
			}
			catch(System.Exception ex2)
			{
				oSqlConn = null;
				WriteToErrorConsole("MySqlUpdate Handler - ", ex2);
			}
			return result;
#else
            return true;
#endif
		}

		/// <summary>
		/// This method will close the connection to mySql.
		/// </summary>
		private void CloseConnection()
		{
#if MySQLIntegration
			if (oSqlConn != null)
			{
				try
				{
					WriteToConsole("MySqlUpdate Handler - Closing Existing MySqlConnection");
					if (oSqlConn.State == System.Data.ConnectionState.Open)
						oSqlConn.Close();
					oSqlConn = null;
					WriteToConsole("MySqlUpdate Handler - Closed Existing MySqlConnection");
				}
				catch(System.Exception ex2)
				{
					oSqlConn = null;
					WriteToErrorConsole("MySqlUpdate Handler - ", ex2);
				}
			}
#endif
        }

		/// <summary>
		/// Indicates if there are entries queued or not.
		/// </summary>
		public bool HasQueuedEntries
		{
			get
			{
#if MySQLIntegration
				lock(oQueue.SyncRoot)
					return (oQueue.Count > 0);
#else
                return false;
#endif
			}
		}

		/// <summary>
		/// This method will only dequeue an item if it is the same as the item passed in.
		/// </summary>
		/// <param name="packet"></param>
		private void CheckedDequeue(MySqlUpdatePacket packet)
		{
#if MySQLIntegration
			lock(oQueue.SyncRoot)
				if (packet == (MySqlUpdatePacket)oQueue.Peek())
					oQueue.Dequeue();
#endif
        }

		/// <summary>
		/// This method will check the available conneciton prior to trying any commands.
		/// </summary>
		/// <returns></returns>
		private bool CheckConnection()
		{
#if MySQLIntegration
			if ((oSqlConn != null) && (oSqlConn.State == System.Data.ConnectionState.Open))
			{
				return true;
			}	
			else
			{
				WriteToConsole("MySqlUpdate Handler - Attempting Reconnect");
				CloseConnection();
				return OpenConnection();
			}
#else
            return true;
#endif
        }

		/// <summary>
		/// This method will process the update requests for the MySql database.
		/// </summary>
		private void ProcessMySQLQueue()
		{
#if MySQLIntegration
			//	SleepCode
			//	0 - No Operation				//	NoOperationDelay
			//	1 - No Connection				//	NoConnectionDelay
			//	2 - Successful Packet Persist	//	SuccessDelay
			int sleepCode = 0;

			WriteToConsole("MySqlUpdate Handler - Starting MySqlConnection Queue Handler");

			try
			{
				while(bMySQLThreadActive)
				{
					MySqlUpdatePacket packet = null;
					try
					{
						if (CheckConnection())
						{
							sleepCode = 0; //	No Operation
							lock(oQueue.SyncRoot)
							{
								if (oQueue.Count > 0)
									packet = (MySqlUpdatePacket)oQueue.Peek();
							}

							#region Execute the command.
							if (packet != null)
							{
								try
								{
									ProcessUpdate(packet);
									sleepCode = 2;	//	PAcket update succeeded
								}
								catch(Exception ex1)
								{
									//	If this is a connection error, close the connection.
									CloseConnection();
									sleepCode = 1; //	Lost connection
									throw ex1;
								}

								//	Dequeue the item once the job is done! Even if an error occurred
								//	as there may be an issue with the update..
								CheckedDequeue(packet);
							}
							#endregion
						}
						else
							sleepCode = 1;
					}
					catch(Exception ex)
					{
						WriteToErrorConsole("MySqlUpdate Handler - ", ex);
					}

					// Pause between groups of updates.
					int delay = 1000;
					switch(sleepCode)
					{
						case 0 : //	No Operation, check again in 10ms
							delay = _params.NoOperationDelay;
							break;
						case 1 : // No connection, attempt reconnect in 2 sec.
							delay = _params.NoConnectionDelay;
							break;
						default : 
							delay = _params.SuccessDelay;
							break;
					}
					if (delay > 0)
						Thread.Sleep(delay);
				}
			}
			finally
			{
				WriteToConsole("MySqlUpdate Handler - Stopping MySqlConnection Queue Handler");
			}
#endif
        }

		/// <summary>
		/// This method will update MySql with the data provided.
		/// It will firstly determine if the vehicle is already in 
		/// the target MySql database, and then update it if it is, or add it 
		/// if it isn't. IF there is a MySql error, then report it, but release 
		/// the queue to move on to the next update packet.
		/// </summary>
		/// <param name="packet"></param>
		private void ProcessUpdate(MySqlUpdatePacket packet)
		{
#if MySQLIntegration
			if ((oSqlConn == null) || ((oSqlConn != null) && (oSqlConn.State != System.Data.ConnectionState.Open)))
				throw new Exception("No connection to MySql is available");
			else
			{
				MySqlCommand command = oSqlConn.CreateCommand();
				try
				{
					//	Check to see if this item is already in the target database.
					command.CommandText = String.Format("SELECT mobileID FROM cmo WHERE mobileid = {0};", packet.VehicleID);
					object result = command.ExecuteScalar();
					if ((result != null) && (Convert.ToInt32(result) == packet.VehicleID))
					{
						//	Update the existing entry...
						command.CommandText = String.Format("UPDATE cmo SET Modified = {0}, Latitude = {1}, Longitude = {2}, Heading = {3}, Speed = {4} WHERE MobileID = {5}",
							Convert.ToString(packet.Modified),
							Convert.ToString(packet.Latitude),
							Convert.ToString(packet.Longitude),
							Convert.ToString(packet.Direction),
							packet.Speed,
							packet.VehicleID);
						command.ExecuteNonQuery();
					}
					else
					{
						//	Add a new entry...
						command.CommandText = String.Format("INSERT INTO cmo (mobileid, alias, vehicletype, logcode, GPSFlags, JobNumber, " + 
							"SubJobNumber, Channel, Latitude, Longitude, heading, information, status, speed, "+
							"OperatorNo1, OperatorNo2, OperatorNo3, OperatorNo4, OperatorNo5, OperatorNo6, "+
							"OperatorNo7, OperatorNo8, OperatorNo9, OperatorNo10, State, Modified, UserID) "+
							"Values({0}, '', '', 0, 0, '', 0, 1, {1}, {2}, {3}, '', 1, {4}, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, {5}, 'MTData')",
							packet.VehicleID,
							Convert.ToString(packet.Latitude), 
							Convert.ToString(packet.Longitude), 
							Convert.ToString(packet.Direction), 
							packet.Speed, 
							Convert.ToString(packet.Modified));
						command.ExecuteNonQuery();

						//	Update the driver details to point to the correct unit
						//	If there is no record there for this driver, then no update will be performed.. saves having to 
						//	do a check first..
						command.CommandText = String.Format("UPDATE driver_info Set mobileid = {0}  WHERE Drv_num = {1}", packet.VehicleID, packet.VehicleID);
						command.ExecuteNonQuery();
					}

					//	Update the log.
					command.CommandText = String.Format("INSERT INTO log (LogCode, GPSFlags, Mobileid, Category, JobNumber, SubJobNumber, Channel, " +
						"Status, Latitude, Longitude, Speed, Heading, Information, Modified, UserID) " +
						"Values (6, 0, {0}, 6, '', 0, 6, 1, {1}, {2}, {3}, {4}, '', '{5}', 'MTData')",
						Convert.ToString(packet.VehicleID),
						Convert.ToString(packet.Latitude),
						Convert.ToString(packet.Longitude), 
						packet.Speed, 
						Convert.ToString(packet.Direction),
						Convert.ToString(packet.Modified));
					command.ExecuteNonQuery();
				}
				finally
				{
					command.Dispose();
				}
			}
#endif
        }

		#endregion

		#region Public Interface

		/// <summary>
		/// This method will allow outside influences to queue a MySql
		/// update, disassociating the accessing of MySql from the rest of the
		/// operation.
		/// </summary>
		/// <param name="packet">This highlights the informationto be updated</param>
		public void AddUpdate(MySqlUpdatePacket packet)
		{
#if MySQLIntegration
			try
			{
				lock(oQueue.SyncRoot)
				{
					oQueue.Enqueue(packet);
				}
			}
			catch(System.Exception ex)
			{
				WriteToErrorConsole("AddUpdate - ", ex);
			}
#endif
        }

		#endregion

		#region Log Event Interface Support
		private void WriteToConsole(string sMsg)
		{
#if MySQLIntegration
			if(eConsoleEvent != null) eConsoleEvent("cMySQLInterface : " + sMsg);
#endif
        }

		private void WriteToErrorConsole(string message, System.Exception ex)
		{
#if MySQLIntegration
			string sMsg = ": Error : " + ex.Message + "/nSource : " + ex.Source + "/nStack : " + ex.StackTrace;
			WriteToConsole(message + sMsg);
#endif
        }
		#endregion


		/// <summary>
		/// This class is used to add an entry to the queue for processing
		/// It includes all items required by teh MySql Interface
		/// </summary>
		public class MySqlUpdatePacket
		{
			public int VehicleID; 
			public decimal Latitude;
			public decimal Longitude;
			public int Speed;
			public int Direction;
			public double Modified;

			public MySqlUpdatePacket(int vehicleID, decimal latitude, decimal longitude, byte speed, int direction, double modified)
			{
				this.VehicleID = vehicleID; 
				this.Latitude = latitude;
				this.Longitude = longitude;
				this.Speed = Convert.ToInt32(speed);
				this.Direction = direction;
				this.Modified = modified;
			}
		}

	}
}

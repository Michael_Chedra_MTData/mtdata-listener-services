﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class DBInsertQueueItem
    {
        public object Packet
        {
            get { return _packet; }
            set { _packet = value; }
        }
        public string Address
        {
            get { return _address; }
            set { _address = value; }
        }
        public int Speed
        {
            get { return _speed; }
            set { _speed = value; }
        }
        public string RoadType
        {
            get { return _roadType; }
            set { _roadType = value; }
        }
        public bool ProcessServerSideWPs
        {
            get { return _processServerSideWPs; }
            set { _processServerSideWPs = value; }
        }
        public object ExtendedData
        {
            get { return _extendedData; }
            set { _extendedData = value; }
        }
        public string UserInfo { get { return _userInfo; } set { _userInfo = value; } }
        public string VehicleTag { get { return _vehicleTag; } set { _vehicleTag = value; } }
        public bool UseGPSOdometer { get { return _useGPSOdometer; } set { _useGPSOdometer = value; } }
        public UnitState UnitState { get { return _unitState; } set { _unitState = value; } }
        public string UserDefinedValue { get { return string.Format("{0}%{1}", _unitState, (_lastNewPositionTime != DateTime.MinValue?_lastNewPositionTime.ToString("dd/MM/yyyy HH:mm:ss"):"")); } }
        public DateTime LastNewPositionTime { get { return _lastNewPositionTime; } set { _lastNewPositionTime = value; } }
        public long UnitStateForReport { get { return _unitStateForReport; } set { _unitStateForReport = value; } }
        public long AuxiliaryStateForReport { get { return _auxiliaryStateForReport; } set { _auxiliaryStateForReport = value; } }

        public PositionDetails Position
        {
            get { return _position; }
            set { _position = value; }
        }

        private object _packet;
        private string _address;
        private int _speed;
        private string _roadType;
        private string _userInfo;
        private string _vehicleTag;
        private bool _useGPSOdometer;
        private UnitState _unitState;
        private DateTime _lastNewPositionTime = DateTime.MinValue;
        private long _unitStateForReport;
        private long _auxiliaryStateForReport;
        private bool _processServerSideWPs;
        private object _extendedData;
        private PositionDetails _position;


        public DBInsertQueueItem(object packet, string address, int speed, string roadType, bool processServerSideWPs, object extendedData, Vehicle vehicle, PositionDetails position)
        {
            _packet = packet;
            _address = address;
            _speed = speed;
            _roadType = roadType;
            _processServerSideWPs = processServerSideWPs;
            _extendedData = extendedData;
            if (vehicle != null)
            {
                _userInfo = vehicle.sUserInfo;
                _vehicleTag = vehicle.VehicleTag;
                _useGPSOdometer = vehicle.UseGPSOdometer;
                _unitState = vehicle.GetUnitState();
                _lastNewPositionTime = vehicle.GetLastNewPositionTime();
                if (packet != null && packet is Gateway.Packet.GeneralGPPacket)
                {
                    _unitStateForReport = vehicle.GetUnitStateForReport((Gateway.Packet.GeneralGPPacket)packet);
                    _auxiliaryStateForReport = vehicle.GetAuxiliaryStateForReport((Gateway.Packet.GeneralGPPacket)packet);
                }
            }
            _position = position;
        }

        public DBInsertQueueItem(object packet, string address, int speed, string roadType, bool processServerSideWPs, object extendedData, DBInsertQueueItem vehicleQItem, PositionDetails position)
        {
            _packet = packet;
            _address = address;
            _speed = speed;
            _roadType = roadType;
            _processServerSideWPs = processServerSideWPs;
            _extendedData = extendedData;
            if (vehicleQItem != null)
            {
                _userInfo = vehicleQItem.UserInfo;
                _vehicleTag = vehicleQItem.VehicleTag;
                _useGPSOdometer = vehicleQItem.UseGPSOdometer;
                _unitState = vehicleQItem.UnitState;
                _lastNewPositionTime = vehicleQItem.LastNewPositionTime;
                _unitStateForReport = vehicleQItem.UnitStateForReport;
                _auxiliaryStateForReport = vehicleQItem.AuxiliaryStateForReport;
            }
            _position = position;
        }
    }
}

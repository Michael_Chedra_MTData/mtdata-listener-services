using System;
using System.Collections.Generic;
using System.Text;
using MTData.Transport.Gateway.Packet;
using System.Linq;
using MTData.Transport.GenericProtocol;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    // Thread safe wrapper for accessing the list of active pendant alarms.
    // Whilst this class provides thread safe access around the internal list
    // the items within the list are NOT thread safe.
    public class PendantAlarmList
    {
        private readonly object _syncLock = new object();
        private List<PendantAlarmListItem> _activeAlarms;
        // Stores the time of the last pendant event for each vehicle
        private Dictionary<FleetVehicle, DateTime> _lastAlarms;

        public PendantAlarmList(List<PendantAlarmListItem> activeAlarms, Dictionary<FleetVehicle, DateTime> lastAlarms)
        {
            lock (_syncLock)
            {
                _activeAlarms = activeAlarms;
                _lastAlarms = lastAlarms;
            }
        }

        /// <summary>
        /// Adds an alarm to the list provided it has not already been added.
        /// </summary>
        /// <returns>
        /// True if the alarm was successfully added.
        /// </returns>
        public bool AddAlarm(byte fleetId, uint vehicleId, DateTime time, GeneralGPPacket alarmPacket)
        {
            FleetVehicle fleetVehicle = new FleetVehicle(fleetId, (int)vehicleId);
            lock (_syncLock)
            {
                // Duplicate alarm identified 
                // Current design only allows a single alarm to be active at a time. 
                // Keep in mind it is possible that a unit could raise a new alarm before a previous alarm has been cleared.
                // If this occurs it is probably desirable to add a new alarm so that alerts are broadcast to Hawkeye again.
                // Unfortunately this design flaw is difficult to fix because it would require changes to the segments
                // (live update, vehicle ack) so that they include the alarm time so that each alarm is uniquely identifiable.
                if (_activeAlarms.Any(x => x.Fleet == fleetId && x.VehicleId == vehicleId))
                {
                    return false;
                }
                // If we have already received an alarm for this vehicle with a later time then ignore.
                if (!IsLatest(fleetVehicle, time))
                {
                    return false;
                }
                _activeAlarms.Add(new PendantAlarmListItem(fleetId, vehicleId, time, alarmPacket));
                _lastAlarms[fleetVehicle] = time;
                return true;
            }
        }

        /// <summary>
        /// Removes all alarms for the specified vehicle that occur on or before the supplied time.
        /// </summary>
        /// <returns>The number of alarms removed.</returns>
        /// <remarks>
        /// The reason that the time is checked is because there is a chance that the unit sends in a clear alarm
        /// from flash for an old alarm. If this occurs we do not want to clear any new alarms.
        /// </remarks>
        public int RemoveAlarms(byte fleetId, uint vehicleId, DateTime time)
        {
            lock (_syncLock)
            {
                return _activeAlarms.RemoveAll(x => x.Fleet == fleetId && x.VehicleId == vehicleId && x.AlarmTime <= time);
            }
        }

        /// <summary>
        /// Acknowledges all alarms for the supplied vehicle.
        /// The current implimentation only allows a single alarm to be active at a time.
        /// </summary>
        /// <returns>True if any alarms were successfully acknowledged.</returns>
        public bool AcknowledgeAlarm(byte fleetId, uint vehicleId)
        {
            lock (_syncLock)
            {
                var alarms = _activeAlarms.Where(x => x.Fleet == fleetId && x.VehicleId == vehicleId);
                foreach (var alarm in alarms) 
                {
                    alarm.Acknowledged = true;
                    return true;
                }
                return false;
            }
        }

        /// <summary>
        /// Returns a collection of alarms that have not been acknowledged by a user.  
        /// </summary>
        /// <remarks>
        /// The list is thread safe but the items within the last are not. 
        /// </remarks>
        public IEnumerable<PendantAlarmListItem> GetUnacknowledgedAlarms()
        {
            lock (_syncLock)
            {
                return _activeAlarms.Where(x => x.Acknowledged == false).ToList().AsReadOnly();
            }
        }

        /// <summary>
        /// Returns a collection of alarms for a specific vehicle that have been acknowledged by a user but not the vehicle.  
        /// </summary>
        /// <remarks>
        /// The list is thread safe but the items within the last are not. 
        /// </remarks>
        public IEnumerable<PendantAlarmListItem> GetAcknowledgedAlarms(byte fleetId, uint vehicleId)
        {
            lock (_syncLock)
            {
                return _activeAlarms.Where(x => x.Acknowledged == true && x.Fleet == fleetId && x.VehicleId == vehicleId).ToList().AsReadOnly();
            }
        }

        private bool IsLatest(FleetVehicle fleetVehicle, DateTime time)
        {
            DateTime existingTime;
            bool existing = _lastAlarms.TryGetValue(fleetVehicle, out existingTime);
            if (!existing || time > existingTime) 
            {
                return true;
            }
            return false;
        }
    }
}

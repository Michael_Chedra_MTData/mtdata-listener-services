﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Xml;

namespace MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess
{
    public class IgnorePackets : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            List<Vehicle> vehicles = new List<Vehicle>();

            XmlNodeList list = section.SelectNodes("Vehicle");
            foreach (XmlNode n in list)
            {
                int fleet = Convert.ToInt32(n.Attributes["fleet"].Value);
                int vId = Convert.ToInt32(n.Attributes["vehicle"].Value);
                Vehicle v = new Vehicle() { Fleet = fleet, VehicleId = vId, ReasonIds = new List<int>() };
                XmlNodeList reasons = n.SelectNodes("Packet");
                foreach (XmlNode r in reasons)
                {
                    v.ReasonIds.Add(Convert.ToInt32(r.Attributes["id"].Value));
                }
                vehicles.Add(v);
            }
            return vehicles;
        }


        public class Vehicle
        {
            public int Fleet { get; set; }
            public int VehicleId { get; set; }
            public List<int> ReasonIds { get; set; }
        }
    }
}

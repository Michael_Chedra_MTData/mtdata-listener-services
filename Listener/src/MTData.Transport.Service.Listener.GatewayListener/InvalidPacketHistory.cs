using System;
using System.Net;
using System.Collections;

namespace MTData.Transport.Service.Listener.GatewayListener
{
	/// <summary>
	/// This class provides details held against the invalid packet.
	/// </summary>
	public class InvalidPacketEntry
	{
		private byte[] _packet = null;
		private int _count = 0;

		public InvalidPacketEntry(byte[] packet)
		{
			_packet = packet;
			_count = 1;
		}

		public int Count
		{
			get{ return _count;}
		}

		public byte[] Packet
		{
			get{ return _packet; }
		}

		public int AddReference()
		{
			_count++;
			return _count;
		}
	}

	public delegate void InvalidPacketHistoryRequiresACKDelegate(InvalidPacketHistoryEntry sender, byte[] packet);

	/// <summary>
	/// This class will maintian the details for an invalid packet.
	/// </summary>
	public class InvalidPacketHistoryEntry
	{
		private IPEndPoint _endPoint = new IPEndPoint(IPAddress.Any, 0);

		//private Hashtable _receivedPackets = new Hashtable();
		private ArrayList _receivedPackets = new ArrayList();

		private int _fleetID = -1;
		private uint _vehicleID = 0;
		private byte _baseSequence = 0;
		private byte _mobileSequence = 0;

		public event InvalidPacketHistoryRequiresACKDelegate RequiresACK;

		/// <summary>
		/// Construct the ip endpoint
		/// </summary>
		public InvalidPacketHistoryEntry(IPEndPoint endPoint)
		{
			_endPoint = endPoint;
		}

		/// <summary>
		/// This is used to link an invalid packet form an IP address to the valid fleet 
		/// and vehicle for it. IF this is different from the existing details, then the 
		/// list of invalid packets is flushed, as it is belonged to a different 
		/// vehicle.
		/// </summary>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="baseSequence"></param>
		/// <param name="mobileSequence"></param>
		public void RegisterValidPacket(int fleetID, uint vehicleID, byte baseSequence, byte mobileSequence)
		{
			if ((fleetID != _fleetID) || (vehicleID != _vehicleID))
				_receivedPackets.Clear();

			_fleetID = fleetID;
			_vehicleID = vehicleID;
			_baseSequence = baseSequence;
			_mobileSequence = mobileSequence;

		}

		/// <summary>
		/// This method will take an invalid packet, and register it against the ip address.
		/// If the number of times it has been tried exceeds 5, it will attempt to send an ack
		/// by basing the sequence on the last valid sequence sent form the vehicle.
		/// </summary>
		/// <param name="buffer"></param>
		public void RegisterInvalidPacket(byte[] buffer)
		{
			int index = -1;
			//	Count backwards.. last one most likely to be errant one.
			for(int loop = _receivedPackets.Count -1; loop >= 0; loop--)
			{
				InvalidPacketEntry entry = (InvalidPacketEntry)_receivedPackets[loop];
				if (entry.Packet.Length == buffer.Length)
				{
					bool matched = true;
					for(int byteLoop = 0; byteLoop < buffer.Length; byteLoop++)
						if (buffer[byteLoop] != entry.Packet[byteLoop])
						{
							matched = false;
							break;
						}
					if (matched)
					{
						index = loop;
						break;
					}
				}
			}

			if (index == -1)
			{
				//	if there are more than 15 packets in the list, loose the oldest one
				while(_receivedPackets.Count > 15)
					_receivedPackets.RemoveAt(0);

				_receivedPackets.Add(new InvalidPacketEntry(buffer));
			}
			else
			{
				InvalidPacketEntry entry = (InvalidPacketEntry)_receivedPackets[index];
				if (entry.AddReference() >= 5)
				{
					//	Ack the packet, and remove this list entry..
					_receivedPackets.RemoveAt(index);
				
					if (RequiresACK != null)
						RequiresACK(this, buffer);
				}
			}
		}

		public IPEndPoint EndPoint { get{ return _endPoint; }}

		public int FleetID { get{ return _fleetID; }}
		public uint VehicleID { get{return _vehicleID;} }
		public byte BaseSequence { get{ return _baseSequence; } set{ _baseSequence = value; }}
		public byte MobileSequence { get{return _mobileSequence;} set{ _mobileSequence = value;}}

	}

	/// <summary>
	/// This class will provide a mechanism for checking the history of 
	/// invalid packets form a unit, and maintaining a count of them.
	/// If at any point, an invalid packet is repeated 3 times, then
	/// the unit will be acked to ensure that it will move on.
	/// </summary>
	public class InvalidPacketHistory
	{
		private Hashtable _entries = new Hashtable();

		public event InvalidPacketHistoryRequiresACKDelegate RequiresACK;

		public InvalidPacketHistory()
		{
			
		}

		/// <summary>
		/// This method will register an invalid packet from a mobile unit.
		/// </summary>
		/// <param name="endPoint"></param>
		/// <param name="packet"></param>
		public InvalidPacketHistoryEntry RegisterInvalidPacket(IPEndPoint endPoint, byte[] packet)
		{
			InvalidPacketHistoryEntry entry = null;

			if (_entries.Contains(endPoint))
			{
				entry = (InvalidPacketHistoryEntry)_entries[endPoint];
			}
			else
			{
				entry = AddEntry(endPoint);
			}
			entry.RegisterInvalidPacket(packet);
			return entry;
		}

		/// <summary>
		/// Add an entry for the ip address in a consistent manner.
		/// </summary>
		/// <param name="endPoint"></param>
		private InvalidPacketHistoryEntry AddEntry(IPEndPoint endPoint)
		{
			InvalidPacketHistoryEntry entry = new InvalidPacketHistoryEntry(endPoint);
			entry.RequiresACK += new InvalidPacketHistoryRequiresACKDelegate(entry_RequiresACK);
			_entries.Add(endPoint, entry);
			return entry;
		}

		/// <summary>
		/// Register a valid packet on this ip address.
		/// </summary>
		/// <param name="endPoint"></param>
		/// <param name="fleetID"></param>
		/// <param name="vehicleID"></param>
		/// <param name="baseSequence"></param>
		/// <param name="mobileSequence"></param>
		public void RegisterValidPacket(IPEndPoint endPoint, int fleetID, uint vehicleID, byte baseSequence, byte mobileSequence)
		{
			InvalidPacketHistoryEntry entry = null;

			if (_entries.Contains(endPoint))
			{
				entry = (InvalidPacketHistoryEntry)_entries[endPoint];
			}
			else
			{
				entry = AddEntry(endPoint);
			}
			entry.RegisterValidPacket(fleetID, vehicleID, baseSequence, mobileSequence);
		}

		/// <summary>
		/// Handle the requires ack message..
		/// </summary>
		/// <param name="sender"></param>
		private void entry_RequiresACK(InvalidPacketHistoryEntry sender, byte[] packet)
		{
			if (RequiresACK != null)
				RequiresACK(sender, packet);
		}
	}
}

using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using MTData.Transport.Application.Communication;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Service.Listener.GatewayListener
{
    public class SMSAlertItem
    {
        public short FleetId;
        public string EMailSourceAddress;
        public string EMailTargetAddress;
        public string EMailSubject;
        public string EMailBody;
        private List<int> _reasons;
        public SMSAlertItem()
        {
            FleetId = 0;
            EMailSourceAddress = "";
            EMailTargetAddress = "";
            EMailSubject = "";
            EMailBody = "";
        }

        public bool ContainsReasonId(int reasonId)
        {
            if(_reasons != null && _reasons.Contains(reasonId))
                return true;
            else
                return false;
        }

        public string ReasonIds
        {
            set
            {
                string temp = value;
                string[] sReasons = temp.Split(",".ToCharArray());
                _reasons = new List<int>();
                for(int X = 0; X < sReasons.Length; X++)
                {
                    int reasonId = 0;
                    try
                    {
                        reasonId = Convert.ToInt32(sReasons[X].Trim());
                    }
                    catch (Exception)
                    {
                        reasonId = 0;
                    }
                    if (reasonId > 0 && !_reasons.Contains(reasonId))
                        _reasons.Add(reasonId);
                }
            }
        }

        public string[] GenerateSubjectAndBody(LiveUpdate oLiveUpdate, DataRow drVehicle, string reasonName)
        {
            string[] sRet = new string[2];
            try
            {
                
                string fleetName = Convert.ToString(drVehicle["FleetName"]);
                string vehicleName = Convert.ToString(drVehicle["DisplayName"]);
                sRet[0] = ReplaceTags(EMailSubject, oLiveUpdate, fleetName, vehicleName, reasonName);
                sRet[1] = ReplaceTags(EMailBody, oLiveUpdate, fleetName, vehicleName, reasonName);
            }
            catch (Exception)
            {
            }
            return sRet;
        }

        private string ReplaceTags(string sTemp, LiveUpdate oLiveUpdate, string fleetName, string vehicleName, string reasonName)
        {
            sTemp = sTemp.Replace("[FleetName]", fleetName);
            sTemp = sTemp.Replace("[VehicleName]", vehicleName);
            sTemp = sTemp.Replace("[ReasonName]", reasonName);
            sTemp = sTemp.Replace("[Location]", oLiveUpdate.sPlaceName);
            sTemp = sTemp.Replace("[Latitude]", oLiveUpdate.dLat.ToString());
            sTemp = sTemp.Replace("[Longitude]", oLiveUpdate.dLong.ToString());
            sTemp = sTemp.Replace("[ReportTime]", oLiveUpdate.dtDeviceTime.ToLocalTime().ToString("dd/MM/yyyy HH:mm:ss"));
            return sTemp;
        }

    }
}

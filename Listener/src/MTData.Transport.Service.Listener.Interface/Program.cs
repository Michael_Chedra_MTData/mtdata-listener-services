﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;

using System.Configuration;

namespace MTData.Transport.Service.Listener.Interface
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            log4net.Config.XmlConfigurator.Configure();
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(CurrentDomain_UnhandledException);

            if (!Environment.UserInteractive)
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
            { 
                new ListenerInterfaceService() 
            };
                ServiceBase.Run(ServicesToRun);
            }
            else
            {
                List<CCS.CCSSetting> _CCSSettings = (List<CCS.CCSSetting>)ConfigurationManager.GetSection("CCSConfig");

                CCS.CCSSetting tmpSetting = _CCSSettings.Find(set => set.FleetID == 1 && set.VehicleID == 9151);
            }
        }


        static void CurrentDomain_UnhandledException(object sender, UnhandledExceptionEventArgs e)
        {
            Exception ex = e.ExceptionObject as Exception;
            string sMsg = "App Domain Error :\r\n" + ex.Message + "\r\n\r\nSource : \r\n" + ex.Source + "\r\n\r\nStack Trace : \r\n" + ex.StackTrace;

            try { LogManager.GetLogger(typeof(Program)).Error(sMsg, ex); }
            catch { }
            try
            {
                using (EventLog el = new EventLog())
                {
                    el.Source = "MTData Listener Service";
                    el.WriteEntry(sMsg, System.Diagnostics.EventLogEntryType.Error);
                    el.Close();
                }
            }
            catch { }

        }
    }
}

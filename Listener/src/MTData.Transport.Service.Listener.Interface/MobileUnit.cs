﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using log4net;
using MTData.Common.Network;
using MTData.Common.Queues;
using MTData.Common.Threading;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.GenericProtocol;
using MTData.Transport.GenericProtocol.Listener;

namespace MTData.Transport.Service.Listener.Interface
{
    public class MobileUnit
    {
        private ILog _log = LogManager.GetLogger(typeof(MobileUnit));
        private string _listenerIP;
        private int _listenerPort;

        private string _serialNumber;
        private int _fleetId;
        private int _vehicleId;
        private EnhancedThread _toListenerThread = null;
        private EnhancedThread _fromListenerThread = null;
        private UnitUdpComms _unitConnection = null;
        private StandardQueue _toListenerQ = null;
        private StandardQueue _fromListenerQ = null;
        private StandardQueue _toMccQ = null;
        private StandardQueue _fromMccQ = null;

        private object _syncRoot = new object();
        private bool _waitForAck = false;
        private int _sequenceNumber = 0;
        private byte _ackSequence = (byte)0x00;
        private byte[] _priorityPacket = null;
        private int _resendTimer = 0;

        public string SerialNumber { get { return _serialNumber; } }

        public MobileUnit(string serialNumber, int fleetId, int vehicleId, StandardQueue toMccQ)
        {
            _serialNumber = serialNumber;
            _fleetId = fleetId;
            _vehicleId = vehicleId;
            _toMccQ = toMccQ;
            _listenerIP = ConfigurationManager.AppSettings["ListenerBindIP"];
            _listenerPort = Convert.ToInt32(ConfigurationManager.AppSettings["ListenerTrackingPort"]);
        }

        public void Start()
        {
            try
            {
                Stop();

                _toListenerQ = new StandardQueue();
                _fromListenerQ = new StandardQueue();
                _fromMccQ = new StandardQueue();
                _unitConnection = new UnitUdpComms(_listenerIP, _listenerPort, _toListenerQ, _fromListenerQ);
                _toListenerThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ToListenerThread), null);
                _fromListenerThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(FromListenerThread), null);

                _unitConnection.Start();
                _fromListenerThread.Start();
                _toListenerThread.Start();
            }
            catch (System.Exception ex)
            {
                _log.Error("Start()", ex);
            }
        }

        public void Stop()
        {
            try
            {
                if (_toListenerThread != null)
                {
                    _toListenerThread.Stop();
                    _toListenerThread = null;
                }
                if (_fromListenerThread != null)
                {
                    _fromListenerThread.Stop();
                    _fromListenerThread = null;
                }
                if (_unitConnection != null)
                {
                    _unitConnection.Stop();
                    _unitConnection = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error("Stop()", ex);
            }
        }

        public void DevicePacketReceived(DeviceLayer packet)
        {
            lock (_fromMccQ.SyncRoot)
            {
                _fromMccQ.Enqueue(packet);
            }
        }

        private object ToListenerThread(EnhancedThread sender, object data)
        {
            byte[] bCurrentPacket = null;
            int iRetryCount = 0;
            int iLocalResendTimer = 0;
            bool localWaitFlag = false;
            bool isPriorityPacket = false;
            DeviceLayer packet;

            while (!sender.Stopping)
            {
                try
                {
                    lock (_syncRoot)
                    {
                        localWaitFlag = _waitForAck;
                    }

                    if (!localWaitFlag)
                    {
                        #region Packet sending processing
                        isPriorityPacket = false;
                        lock (_syncRoot)
                        {
                            if (_priorityPacket != null)
                            {
                                isPriorityPacket = true;
                            }
                        }

                        if (isPriorityPacket)
                        {
                            lock (_syncRoot)
                            {
                                _waitForAck = true;
                                _resendTimer = 0;
                                iRetryCount = 0;
                                bCurrentPacket = _priorityPacket;
                                _priorityPacket = null;
                            }
                            lock (_toListenerQ.SyncRoot)
                                _toListenerQ.Enqueue(bCurrentPacket);
                            LogMessage("Waiting for ACK on Priority Packet");
                        }
                        else
                        {
                            //check if any packets are in queue to be sent
                            packet = null;
                            lock (_fromMccQ.SyncRoot)
                            {
                                if (_fromMccQ.Count > 0)
                                {
                                    packet = _fromMccQ.Dequeue() as DeviceLayer;
                                }
                            }

                            //convert packet to a listener packet
                            if (packet != null)
                            {
                                bCurrentPacket = ConvertPacket(packet);
                                if (bCurrentPacket != null)
                                {
                                    lock (_syncRoot)
                                    {
                                        _waitForAck = true;
                                        _resendTimer = 0;
                                        iRetryCount = 0;
                                    }
                                    lock (_toListenerQ.SyncRoot)
                                        _toListenerQ.Enqueue(bCurrentPacket);
                                    LogMessage(string.Format("Waiting for ACK ackSeq {0}, seq {1}", bCurrentPacket[9], bCurrentPacket[8]));
                                }
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        #region Waiting for Ack processing
                        if (bCurrentPacket != null)
                        {
                            #region Resend processing.
                            lock (_syncRoot)
                            {
                                _resendTimer++;
                                iLocalResendTimer = _resendTimer;
                            }
                            if (iLocalResendTimer >= 20) // resend every 2 seconds
                            {
                                iRetryCount++;
                                LogMessage("Resending Data Packet (Retry " + Convert.ToString(iRetryCount) + ")");
                                lock (_syncRoot)
                                {
                                    _resendTimer = 0;
                                }
                                lock (_toListenerQ.SyncRoot)
                                    _toListenerQ.Enqueue(bCurrentPacket);
                            }
                            #endregion
                        }
                        else
                        {
                            #region There is no current packet, so let the process pickup the next one.
                            LogMessage("waiting for ack, but no current packet, skip to next one");
                            lock (_syncRoot)
                            {
                                _waitForAck = false;
                            }
                            #endregion
                        }

                        #endregion
                    }

                    Thread.Sleep(100);
                }
                catch (System.Exception ex)
                {
                    _log.Error("ToListenerThread()", ex);
                }
            }
            return null;
        }
        private object FromListenerThread(EnhancedThread sender, object data)
        {
            byte[] bytes = null;
            UDPPacket oListenerData = null;
            GatewayProtocolPacket oGPPacket = null;
            ConfigTotalGPPacket mConfigPacket = null;
            while (!sender.Stopping)
            {
                try
                {
                    #region Check for incomming data
                    lock (_fromListenerQ.SyncRoot)
                    {
                        if (_fromListenerQ.Count > 0)
                            bytes = (byte[])_fromListenerQ.Dequeue();
                        else
                            bytes = null;
                    }
                    #endregion
                    if (bytes != null)
                    {
                        #region Process the incomming data
                        oListenerData = new UDPPacket("");
                        oListenerData.Decode(bytes);
                        for (int X = 0; X < oListenerData.mNumPackets; X++)
                        {
                            oGPPacket = oListenerData.mPackets[X];
                            _ackSequence = oGPPacket.cOurSequence;
                            switch (oGPPacket.cMsgType)
                            {
                                case GeneralGPPacket.GEN_ACK:
                                    lock (_syncRoot)
                                    {
                                        LogMessage(string.Format("Ack Received - seq {0}", _ackSequence));
                                        _waitForAck = false;
                                    }
                                    break;
                                case GeneralGPPacket.GEN_RESET:
                                    _sequenceNumber = 0;
                                    _ackSequence = (byte)0x00;
                                    LogMessage("GenReset Received");
                                    lock (_syncRoot)
                                    {
                                        _resendTimer = 0;
                                        _waitForAck = false;
                                    }
                                    break;
                                case ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE:
                                    LogMessage(string.Format("ConfigTotalActive Received, send configActive - ackSeq {0}, seq {1}", _ackSequence, _sequenceNumber));
                                    ConfigTotalGPPacket sentConfig = new ConfigTotalGPPacket(oGPPacket, "");
                                    mConfigPacket = new ConfigTotalGPPacket("");
                                    mConfigPacket.cFleetId = (byte)_fleetId;
                                    mConfigPacket.iVehicleId = (uint)_vehicleId;
                                    mConfigPacket.cOurSequence = SequenceNumber;
                                    mConfigPacket.cAckSequence = _ackSequence;

                                    mConfigPacket.cConfigFileActive = sentConfig.cConfigFileActive;
                                    mConfigPacket.cConfigFileActiveVersionMajor = sentConfig.cConfigFileActiveVersionMajor;
                                    mConfigPacket.cConfigFileActiveVersionMinor = sentConfig.cConfigFileActiveVersionMinor;
                                    mConfigPacket.cEcmConfigFileActive = sentConfig.cEcmConfigFileActive;
                                    mConfigPacket.cEcmConfigFileActiveVersionMajor = sentConfig.cEcmConfigFileActiveVersionMajor;
                                    mConfigPacket.cEcmConfigFileActiveVersionMinor = sentConfig.cEcmConfigFileActiveVersionMinor;

                                    mConfigPacket.cSetPointSetActive = sentConfig.cSetPointSetActive;
                                    mConfigPacket.cSetPointSetActiveVersionMajor = sentConfig.cSetPointSetActiveVersionMajor;
                                    mConfigPacket.cSetPointSetActiveVersionMinor = sentConfig.cSetPointSetActiveVersionMinor;
                                    bytes = null;
                                    mConfigPacket.Encode(ref bytes);
                                    lock (_syncRoot)
                                    {
                                        _priorityPacket = bytes;
                                        _waitForAck = false;
                                    }
                                    break;
                                case ConfigFileGPPacket.CONFIG_NEW_FILE:
                                case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT:
                                    LogMessage(string.Format("ConfigNew Received. send ack - ackSeq {0}, seq {1}", _ackSequence, _sequenceNumber));
                                    SendAck();
                                    
                                    //update version numbers
                                    if (oGPPacket.cMsgType == ConfigFileGPPacket.CONFIG_NEW_FILE)
                                    {
                                        ConfigFileGPPacket filePacket = new ConfigFileGPPacket(oGPPacket, "");
                                        mConfigPacket.cConfigFileActive = filePacket.cConfigFileNo;
                                        mConfigPacket.cConfigFileActiveVersionMajor = filePacket.cVersionMajor;
                                        mConfigPacket.cConfigFileActiveVersionMinor = filePacket.cVersionMinor;
                                    }
                                    else
                                    {
                                        ConfigNewSetPtGPPacket setPointPacket = new ConfigNewSetPtGPPacket(oGPPacket, "");
                                        mConfigPacket.cSetPointSetActive = setPointPacket.cSetPointSetActive;
                                        mConfigPacket.cSetPointSetActiveVersionMajor = setPointPacket.cSetPointSetActiveVersionMajor;
                                        mConfigPacket.cSetPointSetActiveVersionMinor = setPointPacket.cSetPointSetActiveVersionMinor;
                                    }

                                    LogMessage(string.Format("resend configActive - ackSeq {0}, seq {1}", _ackSequence, _sequenceNumber));
                                    bytes = null;
                                    mConfigPacket.cOurSequence = SequenceNumber;
                                    mConfigPacket.cAckSequence = _ackSequence;
                                    mConfigPacket.Encode(ref bytes);
                                    lock (_syncRoot)
                                    {
                                        _priorityPacket = bytes;
                                        _waitForAck = false;
                                    }
                                    break;
                                default:
                                    LogMessage(string.Format("packet {1} Received, send ack - ackSeq {0}, seq {2}", _ackSequence, oGPPacket.cMsgType, _sequenceNumber));
                                    SendAck();
                                    break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error("FromListenerThread()", ex);
                }
            }
            return null;
        }

        private void SendAck()
        {
            byte[] bytes = null;
            GatewayProtocolPacket returnGPPacket = new GeneralGPPacket("");
            returnGPPacket.cMsgType = GeneralGPPacket.GEN_ACK;
            returnGPPacket.cFleetId = (byte)_fleetId;
            returnGPPacket.iVehicleId = (uint)_vehicleId;
            returnGPPacket.cOurSequence = SequenceNumber;
            returnGPPacket.cAckSequence = _ackSequence;
            returnGPPacket.Encode(ref bytes);
            bytes[2] = (byte)0x00;
            lock (_toListenerQ.SyncRoot)
                _toListenerQ.Enqueue(bytes);
        }

        private void LogMessage(string message)
        {
            _log.DebugFormat("Fleet {0}, Vehicle {1} - {2}", _fleetId, _vehicleId, message);
        }

        private byte[] ConvertPacket(DeviceLayer layer)
        {
            byte[] bytes = null;

            if (layer.Segments.Count > 1)
            {
                TrackingSegment tracking = new TrackingSegment(layer.Segments[0]);
                GpsSegment gps = new GpsSegment(layer.Segments[1]);

                if (tracking.ReasonCode == ReasonCodes.StatusReport)
                {
                    GeneralGPPacket mPacket = new GeneralGPPacket("");
                    mPacket.cOurSequence = SequenceNumber;
                    mPacket.cAckSequence = _ackSequence;
                    mPacket.cFleetId = (byte)_fleetId;
                    mPacket.iVehicleId = (uint)_vehicleId;
                    mPacket.cPacketTotal = 0x03;
                    mPacket.bAckImmediately = true;
                    mPacket.bAckRegardless = true;
                    mPacket.bArchivalData = false;
                    mPacket.bSendFlash = false;
                    mPacket.cCannedMessageNumber = 0x00;
                    mPacket._cProgramMajor = 0x00;
                    mPacket._iProgramMinor = 0x00;
                    mPacket.cIOBoxProgramMajor = 0x00;
                    mPacket.cIOBoxProgramMinor = 0x00;
                    mPacket.iHardwareTypeInbound = GeneralGPPacket.Alive_IMEIESN_HardwareType.Platform_3026_IMEI;
                    mPacket.iHardwareVersionNumber = 0x00;
                    mPacket.iLoginFlags = 0x00;
                    mPacket.iPreferedSlotNumber = 1;
                    mPacket.lSerialNumber = 1;
                    mPacket.sNetworkName = "";
                    mPacket.sSimNumber = "";

                    mPacket.cMsgType = (byte)193;
                    mPacket.mFixClock = new GPClock("GPSClock", gps.GpsTime, "");
                    mPacket.mCurrentClock = new GPClock("GPSClock", gps.GpsTime.AddSeconds(gps.DeviceTimer), "");

                    mPacket.mFix = new GPPositionFix();
                    mPacket.mFix.iDirection = gps.Heading;
                    mPacket.mFix.cSpeed = (byte)((int)gps.Speed);
                    mPacket.mFix.dLatitude = Convert.ToDecimal(gps.Latitude);
                    mPacket.mFix.dLongitude = Convert.ToDecimal(gps.Longitude);
                    mPacket.mFix.cFlags = (byte)0x00;

                    mPacket.mStatus = new GPStatus();
                    mPacket.mStatus.cButtonStatus = 0x00;
                    mPacket.mStatus.cExtendedStatusFlag = (byte)0x00;
                    mPacket.mStatus.cInputStatus = 0x00;
                    mPacket.mStatus.cLightStatus = 0x00;
                    mPacket.mStatus.cOutputStatus = 0x00;
                    mPacket.mStatus.cSpareStatus = 0x00;
                    mPacket.mStatus.cStatusFlag = 0x00;
                    mPacket.cButtonNumber = (byte)0x00;
                    mPacket.cInputNumber = (byte)0x00;
                    foreach (TrackingFlag flag in tracking.TrackingFlags)
                    {
                        if (flag.TrackingFlagType == TrackingFlagTypes.StatusFlags)
                        {
                            if (IsFlagSet(flag.Value, (uint)StatusFlags.LoggedIn))
                            {
                                mPacket.mStatus.cStatusFlag = 0x02;
                            }
                        }
                    }

                    mPacket.mDistance = new GPDistance();
                    mPacket.mDistance.cMaxSpeed = (byte)((int)gps.MaxSpeed);
                    mPacket.mDistance.iSamples = 0;
                    mPacket.mDistance.lSpeedAccumulator = 0;

                    mPacket.mExtendedValues = new GPExtendedValues();
                    mPacket.mExtendedVariableDetails = new GBVariablePacketExtended59();
                    mPacket.mImieData = new cIMIEData();
                    mPacket.mECMAdvanced = new GPECMAdvanced();
                    mPacket.mECMAdvanced.SetSpeedData(gps.Speed, gps.MaxSpeed);
                    mPacket.Encode(ref bytes);
                }
            }
            return bytes;
        }

        public bool IsFlagSet(uint value, uint flag)
        {
            if ((value & flag) != 0)
            {
                return true;
            }
            return false;
        }

        public byte SequenceNumber
        {
            get
            {
                byte seq = (byte)_sequenceNumber;
                _sequenceNumber++;
                if (_sequenceNumber > 31)
                {
                    _sequenceNumber = 0;
                }
                return seq;
            }
        }
    }
}

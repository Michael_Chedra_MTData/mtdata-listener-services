﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;

namespace MTData.Transport.Service.Listener.Interface.CCS
{
    public class CCSSetting
    {
        #region Private Variables
        private int _fleetID;
        private int _vehicleID;
        private int _FCTimeBeforeEventInSec;
        private int _FCTimeAfterEventInSec;
        private int _FCSampleRateInSec;
        private int _HBSampleRateInSec;
        private int _CustomerRef;
        private string _FileUploadUrl;
        #endregion

        #region Public Properties
        public int FleetID
        {
            get { return this._fleetID; }
        }

        public int VehicleID
        {
            get { return this._vehicleID; }
        }

        public int FCTimeBeforeEventInSec
        {
            get { return this._FCTimeBeforeEventInSec; }
        }

        public int FCTimeAfterEventInSec
        {
            get { return this._FCTimeAfterEventInSec; }
        }

        public int FCSampleRateInSec
        {
            get { return this._FCSampleRateInSec; }
        }

        public int HBSampleRateInSec
        {
            get { return this._HBSampleRateInSec; }
        }

        public int CustomerRef
        {
            get { return this._CustomerRef; }
        }

        public string FileUploadUrl
        {
            get { return this._FileUploadUrl; }
        }
        #endregion

        public CCSSetting(int fleetID, int vehicleID, int FCTimeBeforeEventInSec, int FCTimeAfterEventInSec, int FCSampleRateInSec, int HBSampleRateInSec,
            int CustomerRef, string FileUploadUrl)
        {
            this._fleetID = fleetID;
            this._vehicleID = vehicleID;
            this._FCTimeBeforeEventInSec = FCTimeBeforeEventInSec;
            this._FCTimeAfterEventInSec = FCTimeAfterEventInSec;
            this._FCSampleRateInSec = FCSampleRateInSec;
            this._HBSampleRateInSec = HBSampleRateInSec;
            this._CustomerRef = CustomerRef;
            this._FileUploadUrl = FileUploadUrl;
        }

        public CCSSetting(XmlNode node)
        {
            this._fleetID = Convert.ToInt32(node.Attributes["fleetID"].Value);
            this._vehicleID = Convert.ToInt32(node.Attributes["vehicleID"].Value);
            this._FCTimeBeforeEventInSec = Convert.ToInt32(node.Attributes["FCTimeBeforeEventInSec"].Value);
            this._FCTimeAfterEventInSec = Convert.ToInt32(node.Attributes["FCTimeAfterEventInSec"].Value);
            this._FCSampleRateInSec = Convert.ToInt32(node.Attributes["FCSampleRateInSec"].Value);
            this._HBSampleRateInSec = Convert.ToInt32(node.Attributes["HBSampleRateInSec"].Value);
            this._CustomerRef = Convert.ToInt32(node.Attributes["CustomerRef"].Value);
            this._FileUploadUrl = Convert.ToString(node.Attributes["FileUploadUrl"].Value);
        }
    }
}
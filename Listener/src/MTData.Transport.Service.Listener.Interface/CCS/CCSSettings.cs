﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;

namespace MTData.Transport.Service.Listener.Interface.CCS
{
    public class CCSSettings : IConfigurationSectionHandler
    {
        object IConfigurationSectionHandler.Create(object parent, object configContext, XmlNode section)
        {
            List<CCSSetting> settings = new List<CCSSetting>();
            XmlNodeList nodes = section.SelectNodes("CCSSetting");
            foreach (XmlNode node in nodes)
            {
                settings.Add(new CCSSetting(node));
            }
            return settings;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.Text;

namespace MTData.Transport.Service.Listener.Interface
{
    internal static class Util
    {
        private static string _serviceName = null;
        private static string _displayName = null;

        public static string ServiceName
        {
            get
            {
                if (_serviceName == null)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
                    _serviceName = config.AppSettings.Settings["ServiceName"].Value;
                }
                return _serviceName;
            }
        }

        public static string DisplayName
        {
            get
            {
                if (_displayName == null)
                {
                    Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);
                    _displayName = config.AppSettings.Settings["DisplayName"].Value;
                }
                return _displayName;
            }
        }
    }
}

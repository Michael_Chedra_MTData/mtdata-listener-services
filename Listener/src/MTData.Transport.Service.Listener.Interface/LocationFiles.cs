﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;

namespace MTData.Transport.Service.Listener.Interface
{
    public class LocationFiles : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            Dictionary<Tuple<int, int>, string> files = new Dictionary<Tuple<int, int>, string>();
            XmlNodeList nodes = section.SelectNodes("File");
            foreach (XmlNode node in nodes)
            {
                int id = Convert.ToInt32(node.Attributes["id"].Value);
                int version = Convert.ToInt32(node.Attributes["version"].Value);
                string path = node.Attributes["path"].Value;
                files.Add(new Tuple<int, int>(id, version), path);
            }
            return files;
        }
    }

}

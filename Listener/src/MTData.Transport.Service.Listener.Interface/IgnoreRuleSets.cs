﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Xml;

namespace MTData.Transport.Service.Listener.Interface
{
    public class IgnoreRuleSets : IConfigurationSectionHandler
    {
        public object Create(object parent, object configContext, XmlNode section)
        {
            Dictionary<int, IgnoreRuleSet> ruleSets = new Dictionary<int, IgnoreRuleSet>();
            XmlNodeList nodes = section.SelectNodes("RuleSets");
            foreach (XmlNode node in nodes)
            {
                int id = Convert.ToInt32(node.Attributes["id"].Value);
                IgnoreRuleSet ruleSet = new IgnoreRuleSet(id);

                XmlNodeList fleetNodes = node.SelectNodes("Fleet");
                foreach (XmlNode fleetNode in fleetNodes)
                {
                    List<int> vehicles = new List<int>();
                    XmlNodeList vehicleNodes = fleetNode.SelectNodes("Vehicle");
                    foreach (XmlNode vehicleNode in vehicleNodes)
                    {
                        vehicles.Add(Convert.ToInt32(vehicleNode.Attributes["id"].Value));
                    }

                    ruleSet.Fleets.Add(Convert.ToInt32(fleetNode.Attributes["id"].Value), vehicles);
                }
                ruleSets.Add(id, ruleSet);
            }
            return ruleSets;
        }
    }

    public class IgnoreRuleSet
    {
        public int RuleSetId { get; set; }

        public Dictionary<int, List<int>> Fleets { get; set; }

        public IgnoreRuleSet(int ruleSetId)
        {
            RuleSetId = ruleSetId;
            Fleets = new Dictionary<int, List<int>>();
        }
    }
}

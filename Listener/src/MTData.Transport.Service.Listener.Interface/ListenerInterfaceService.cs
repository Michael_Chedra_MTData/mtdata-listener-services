﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;
using log4net;
using MTData.Common.Config;
using MTData.Common.Queues;
using MTData.MDT.Interface;
using MTData.Common.Database;
using MTData.Common.Utilities;

namespace MTData.Transport.Service.Listener.Interface
{
    public partial class ListenerInterfaceService : ServiceBase
    {
        #region private fields
        /// <summary>
        /// For reading a standard 5040 interface config file.
        /// </summary>
        private GenConfigReader _config = null;
        /// <summary>
        /// For handling the MCC connection
        /// </summary>
        private Gen5040Administrator _admin;
        /// <summary>
        /// handles requests from MCC and sends back responses
        /// </summary>
        private ListenerInterface _listenerInterface;

        private ILog _logging = LogManager.GetLogger(typeof(ListenerInterfaceService));

        /// <summary>
        /// Updates sent from the the units
        /// </summary>
        private StandardQueue _fromMobileUnitsQ = null;
        /// <summary>
        /// Updates to be sent to the units
        /// </summary>
        private StandardQueue _toMobileUnitsQ = null;
        #endregion
        
        public ListenerInterfaceService()
        {
            InitializeComponent();

            this.ServiceName = Util.ServiceName;

            if (ConfigurationManager.AppSettings["DebugMode"] != null && ConfigurationManager.AppSettings["DebugMode"].ToUpper() == "TRUE")
                OnStart(null);
        }

        protected override void OnStart(string[] args)
        {
            //check database version
            CheckDatabaseVersion checkDB = new CheckDatabaseVersion(System.Configuration.ConfigurationManager.AppSettings["TransportDSN"]);
            DatabaseVersionErrorMessage errorMessage;
            if (checkDB.CheckDatabaseRequiredVersion(8, 2, 0, 1, out errorMessage) == false &&
                        errorMessage != null)
            {
                string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
                new EmailHelper(new EmailClient(emailServiceUrl)).SendDatabaseVersionErrorEmail(errorMessage);
            }

            InitialiseSettings();

            _listenerInterface = new ListenerInterface(_fromMobileUnitsQ, _toMobileUnitsQ, _config);
            _admin = new Gen5040Administrator(_config.TranslatorType, _fromMobileUnitsQ, _toMobileUnitsQ, _config);
        }

        protected override void OnStop()
        {
            if (_logging != null)
            {
                _logging.Info("Stopping Listener Interface");
            }

            if (_admin != null)
                _admin.Stop(true);

            if (_listenerInterface != null)
                _listenerInterface.Dispose();

            if (_fromMobileUnitsQ != null)
            {
                while (_fromMobileUnitsQ.Count > 0)
                    _fromMobileUnitsQ.Dequeue();
                _fromMobileUnitsQ = null;
            }
            if (_toMobileUnitsQ != null)
            {
                lock (_toMobileUnitsQ.SyncRoot)
                {
                    while (_toMobileUnitsQ.Count > 0)
                        _toMobileUnitsQ.Dequeue();
                }
                _toMobileUnitsQ = null;
            }
            // set objects to null
            if (_config != null)
                _config = null;
            if (_admin != null)
                _admin = null;
            if (_listenerInterface != null)
                _listenerInterface = null;
        }

        #region private methods
        private void InitialiseSettings()
        {
            // Create the queues to send and recieve from the service level
            _toMobileUnitsQ = new StandardQueue();
            _fromMobileUnitsQ = new StandardQueue();

            // Read the config files
            _config = new GenConfigReader();

        }
        #endregion
    }
}

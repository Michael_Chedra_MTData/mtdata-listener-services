﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Xml;
using log4net;
using MTData.Common.Config;
using MTData.Common.Queues;
using MTData.Common.Threading;
using MTData.Transport.GenericProtocol;
using MTData.Transport.GenericProtocol.Listener;
using MTData.Transport.GenericProtocol.Listener.IAP;
using MTData.Transport.GenericProtocol.Listener.InternalRequests;
using MTData.Transport.GenericProtocol.Listener.Rules;
using MTData.Transport.GenericProtocol.Logistics.Lists;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;

using KeepAliveSegment = MTData.Transport.GenericProtocol.Listener.KeepAliveSegment;
using Rule = MTData.Transport.GenericProtocol.Listener.Rules.Rule;
using Action = MTData.Transport.GenericProtocol.Listener.Rules.Action;
using MTData.Transport.GenericProtocol.Listener.Waypoints;
using MTData.Transport.GenericProtocol.Listener.Waypoint;

namespace MTData.Transport.Service.Listener.Interface
{
	public class ListenerInterface : IDisposable
	{
		#region Private Vars
		private static ILog _log = LogManager.GetLogger(typeof(ListenerInterface));
		private bool _threadRunning = false;									// Indicates that the processing thread is active.
		private StandardQueue _toMobileUnitsQ = null;							// The queue for sending to MCC
		private StandardQueue _fromMobileUnitsQ = null;							// The queue for recieving from MCC
		private EnhancedThread _incomingThread = null;									// The thread to monitor for incomming packets from Dispatched Work MDTs
		private GenConfigReader _config = null;	        // A class containing configuration values
		private Dictionary<string, MobileUnit> _units;

		private int _internalMessagesUdpCPort;
		private UdpClient _internalMessagesUdpClient; 
		private EnhancedThread _internalMessagesThread = null;
		private EnhancedThread _processInternalMessagesThread = null;
		private StandardQueue _internalMessagesQ = null;
		private object _syncObject;
		private Dictionary<Tuple<int, int>, KeyValuePair<DeviceLayer, DateTime>> _unitDeviceLayer;
	    private Dictionary<Tuple<int, int>, KeyValuePair<CachedHardwareConfigSegmentSummary, DateTime>> _unitHardwareConfigSegmentSummaries;
        private EnhancedThread _refreshDataThread = null;
		private string _uploadAccidentBufferUrl;
		private string _dvrServiceUrl;
		private Dictionary<Tuple<int, int>, string> _locationFiles;
		private Dictionary<int, IgnoreRuleSet> _ignoreRuleSets;

		private List<CCS.CCSSetting> _CCSSettings;

		private DateTime _lastQueuedMessageCheck = DateTime.MinValue;
		List<QueuedMessage> _messageList = null;
		private object _messageCheckSync = new object();
		private DateTime _removeOldUnitsTime;

		#endregion

		#region Constructor
		/// <summary>
		/// This is the creator function for the Logistics_DBInterface class.  This function starts the threads.
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="data"></param>
		/// <returns>object</returns>
		public ListenerInterface(StandardQueue fromMobileUnitsQ, StandardQueue toMobileUnitsQ, GenConfigReader config)
		{			
			_config = config;
			_toMobileUnitsQ = toMobileUnitsQ;
			_fromMobileUnitsQ = fromMobileUnitsQ;
			_units = new Dictionary<string, MobileUnit>();
			_syncObject = new object();
			_unitDeviceLayer = new Dictionary<Tuple<int, int>, KeyValuePair<DeviceLayer, DateTime>>();
		    _unitHardwareConfigSegmentSummaries = new Dictionary<Tuple<int, int>, KeyValuePair<CachedHardwareConfigSegmentSummary, DateTime>>();
            _locationFiles = (Dictionary<Tuple<int, int>, string>)ConfigurationManager.GetSection("LocationFiles");
			_ignoreRuleSets = (Dictionary<int, IgnoreRuleSet>)ConfigurationManager.GetSection("IgnoreRuleSets");
			_CCSSettings = (List<CCS.CCSSetting>)ConfigurationManager.GetSection("CCSConfig"); 
			_messageList = new List<QueuedMessage>();
			_log.Info("Started Listener Interface.");

			//Start the thread to process communications from the Dispatched MDTs
			if (_config.TranslatorType == "MTData.Listener")
			{
				_threadRunning = true;

				_incomingThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(IncomingMDTThread), null);
				_incomingThread.Name = "Process Messages From Terminals";
				_incomingThread.Start();
			}
			else
			{
				_log.Error("Unable to start MDT processing thread - Unknown TranslatorType specified", new System.Exception());
			}

			_internalMessagesUdpCPort = Convert.ToInt32(ConfigurationManager.AppSettings["InternalMessagesPort"]);
			_internalMessagesQ = new StandardQueue();
			_internalMessagesUdpClient = new UdpClient(_internalMessagesUdpCPort);
			_internalMessagesThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(InternalMessagesThread), null);
			_internalMessagesThread.Name = "Listen for Internal Messages";
			_internalMessagesThread.Start();
			_processInternalMessagesThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ProcesssInternalMessagesThread), null);
			_processInternalMessagesThread.Name = "Process Internal Messages";
			_processInternalMessagesThread.Start();

			_uploadAccidentBufferUrl = ConfigurationManager.AppSettings["UploadAccidentBufferUrl"];
			_dvrServiceUrl = ConfigurationManager.AppSettings["DvrServiceUrl"];

			_refreshDataThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(RefreshDataThread), null);
			_refreshDataThread.Name = "Refresh Data";
			_refreshDataThread.Start();

		}

		#endregion

		#region IDisposable Members

		public void Dispose()
		{
			//stop the thread
			_threadRunning = false;
			try
			{
				if (_incomingThread != null)
					_incomingThread.Stop();

				foreach (MobileUnit u in _units.Values)
				{
					u.Stop();
				}
			}
			catch (System.Exception ex)
			{
				_log.Error("Stopping DriverPoints_DBInterface Dispatched MDT communications thread.", ex);
			}

			try
			{
				if (_internalMessagesUdpClient != null)
				{
					_internalMessagesUdpClient.Close();
					_internalMessagesUdpClient = null;
				}
				if (_internalMessagesThread != null)
				{
					_internalMessagesThread.Stop();
				}
				if (_processInternalMessagesThread != null)
				{
					_processInternalMessagesThread.Stop();
				}
				if (_refreshDataThread != null)
				{
					_refreshDataThread.Stop();
				}
			}
			catch (System.Exception ex)
			{
				_log.Error("Stop internal message communications thread.", ex);
			}
		}

		#endregion

		#region private methods
		private object IncomingMDTThread(EnhancedThread sender, object oInput)
		{
			_log.Info("Incoiming MDTs thread started");

			while (_threadRunning)
			{
				//Check for new data from the units
				DeviceLayer deviceLayer = null;
				try
				{
					lock (_fromMobileUnitsQ.SyncRoot)
					{
						if (_fromMobileUnitsQ.Count > 0)
							deviceLayer = (DeviceLayer)_fromMobileUnitsQ.Dequeue();
					}
				}
				catch (Exception ex)
				{
					_log.Error("Error getting next MDT message from the queue", ex);
					deviceLayer = null;
				}
				try
				{
                    if (deviceLayer != null)
                    {
                        //List<SegmentLayer> segments = deviceLayer.Segments;

                        foreach (SegmentLayer segment in deviceLayer.Segments)
                        {
                            if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.TrackingSegment)
                            {
                                TrackingSegment seg = new TrackingSegment(segment);
                                if (!_units.ContainsKey(seg.SerialNumber))
                                {
                                    _log.InfoFormat("Adding new mobile unit {0}, Fleet {1}, Vehicle {2}", seg.SerialNumber, seg.Fleet, seg.Vehicle);
                                    MobileUnit u = new MobileUnit(seg.SerialNumber, seg.Fleet, seg.Vehicle, _toMobileUnitsQ);
                                    u.Start();
                                    _units.Add(seg.SerialNumber, u);
                                }
                                _units[seg.SerialNumber].DevicePacketReceived(deviceLayer);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestHardwareConfig)
                            {
                                RequestHardwareConfigSegment request = new RequestHardwareConfigSegment(deviceLayer.Segments[0]);
                                //Save the latest segment version in memory for later use when we want to push the hardware config.
                                Tuple<int, int> key = new Tuple<int, int>(request.Fleet, request.Vehicle);
                                lock (_syncObject)
                                {
                                    _unitHardwareConfigSegmentSummaries[key] = new KeyValuePair<CachedHardwareConfigSegmentSummary, DateTime>(new CachedHardwareConfigSegmentSummary(request.Version, request.ConfigId, request.ConfigVersion), DateTime.Now); ;
                                }

                                CheckHardwareConfig(deviceLayer);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.KeepAlive)
                            {
                                KeepAliveSegment seg = new KeepAliveSegment(segment);
                                Tuple<int, int> key = new Tuple<int, int>(seg.Fleet, seg.Vehicle);
                                lock (_syncObject)
                                {
                                    if (!_unitDeviceLayer.ContainsKey(key))
                                    {
                                        _unitDeviceLayer.Add(key, new KeyValuePair<DeviceLayer, DateTime>(deviceLayer, DateTime.Now));
                                    }
                                }
                                ProcessListenerMessages(key);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestRules)
                            {
                                HandleRulesRequest(deviceLayer, segment);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestLocationFiles)
                            {
                                HandleRequestLocationFiles(deviceLayer, segment);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.IAPDataBlocksUpdated)
                            {
                                HandleIAPDataBlockUpdated(deviceLayer, segment);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestDvrConfig)
                            {
                                CheckDvrConfig(deviceLayer);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestCCSConfig)
                            {
                                HandleCCSConfigRequest(deviceLayer, segment);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.DefaultHardwareConfig)
                            {
                                HandleReceiveDefaultConfig(deviceLayer, segment);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestOverSpeedConfig)
                            {
                                HandleOverSpeedConfigRequest(deviceLayer);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.AssetPacket)
                            {
                                HandleAssetPacket(deviceLayer, segment);
                            }
                            else if ((ListenerSegmentTypes)segment.Type == ListenerSegmentTypes.RequestWaypoints)
                            {
                                HandleWaypointsRequest(deviceLayer);
                            }
                            else
                            {
                                _log.InfoFormat("Segment 0 is not a listener segment, its a {0}", (ListenerSegmentTypes)segment.Type);
                            }
                        }
                    }
                    else
                    {
                        Thread.Sleep(100);
                    }
				}
				catch (Exception ex)
				{
					_log.Error("Error processing message from MDT.", ex);
				}
			}

			_log.Info("Incoiming MDTs thread stopped.");
			return null;
		}

		private void HandleRulesRequest(DeviceLayer deviceLayer, SegmentLayer segment)
		{
			RequestRulesSegment seg = new RequestRulesSegment(segment);
			_log.InfoFormat("Received Rules request From unit {0}, Fleet {1}, Vehicle {2}", deviceLayer.UnitId, seg.Fleet, seg.Vehicle);
			foreach (ListId id in seg.RuleIds)
			{
				_log.InfoFormat("unit currently running Rule set {0}.{1}", id.Id, id.Version);
			}

			//Find the list of rules this vehicle is running
			string sql = string.Format("exec lsp_GetListMultiDefault {0}, {1}, 0", seg.Fleet, seg.Vehicle);
			DataSet data = ExecuteSQL(sql);
			RequiredRulesSegment requiredRules = new RequiredRulesSegment();
			if (data.Tables.Count > 0)
			{
				foreach (DataRow row in data.Tables[0].Rows)
				{
					int listId = Convert.ToInt32(row["ListId"]);
					if (listId > 0 && !IgnoreRuleSet(seg.Fleet, seg.Vehicle, listId))
					{
						ListId ruleSetId = new ListId(listId, Convert.ToInt32(row["ListVersion"]));
						requiredRules.RuleIds.Add(ruleSetId);
						_log.InfoFormat("telling unit to run Rule set {0}.{1}", ruleSetId.Id, ruleSetId.Version);
					}
					else
					{
						_log.InfoFormat("ignoring Rule set {0}", listId);
					}
				}
			}
			SendToUnit(requiredRules, deviceLayer);

			//foreach rule set, check if the unit is already running it, if not send it
			foreach (ListId id in requiredRules.RuleIds)
			{
				if (!seg.RuleIds.Contains(id))
				{
					RulesSeqment rules = new RulesSeqment();
					rules.RuleSetId = id.Id;
					rules.RuleSetVersion = id.Version;

					//load list
					DataSet listData = GetListDataSet(id.Id, id.Version);
					foreach (DataRow row in listData.Tables["ListItemRule"].Rows)
					{
						Rule rule = new Rule();
						rule.RuleId = Convert.ToInt32(row["ListItemID"]);
						rule.Condition = row["Rule"].ToString();
						DataRow[] actions = listData.Tables["ListItemRuleAction"].Select(string.Format("ListItemID = {0}", rule.RuleId));
						foreach (DataRow actionRow in actions)
						{
							Action a = new Action();
							a.Arguments = actionRow["Action"].ToString();
							rule.Actions.Add(a);
						}
						rules.Rules.Add(rule);
					}

					_log.InfoFormat("sending unit Rule set {0}.{1}", rules.RuleSetId, rules.RuleSetVersion);
					SendToUnit(rules, deviceLayer);
				}
			}
		}

		private bool IgnoreRuleSet(int fleetId, int vehicleId, int ruleSetId)
		{
			if (_ignoreRuleSets != null)
			{
				if (_ignoreRuleSets.ContainsKey(ruleSetId))
				{
					//check if specfic fleet of vehicles are defined or is it a blanket ban
					if (_ignoreRuleSets[ruleSetId].Fleets.Count == 0)
					{
						//blanket ban
						return true;
					}

					if (_ignoreRuleSets[ruleSetId].Fleets.ContainsKey(fleetId))
					{
						if (_ignoreRuleSets[ruleSetId].Fleets[fleetId].Count == 0)
						{
							//all vehilces in fleet
							return true;
						}

						if (_ignoreRuleSets[ruleSetId].Fleets[fleetId].Contains(vehicleId))
						{
							//specfic vehicle in fleet
							return true;
						}
					}
				}
			}
			return false;
		}

		private void HandleRequestLocationFiles(DeviceLayer deviceLayer, SegmentLayer segment)
		{
			RequestLocationFiles seg = new RequestLocationFiles(segment);
			_log.InfoFormat("Received Location file request From unit {0}, Fleet {1}, Vehicle {2}", deviceLayer.UnitId, seg.Fleet, seg.Vehicle);
			for (int i = 0; i < seg.LocationIds.Count; i++)
			{
				ListId id = seg.LocationIds[i];
				_log.InfoFormat("location request {0}.{1}", id.Id, id.Version);

				Tuple<int, int> fileKey = new Tuple<int, int>(id.Id, id.Version);
				if (_locationFiles.ContainsKey(fileKey) && seg.LatestVersions[i])
				{
					LocationFilesSegment locationsFiles = new LocationFilesSegment();
					LocationFile file = new LocationFile();
					file.LocationId = new ListId(id.Id, id.Version);
					file.Url = _locationFiles[fileKey];
					locationsFiles.LocationFiles.Add(file);
					SendToUnit(locationsFiles, deviceLayer);
				}
			}

		}

	    private CachedHardwareConfigSegmentSummary GetCachedHardwareConfigSegmentSummary(int fleetId, int vehicleId)
	    {
	        try
	        {
	            var key = new Tuple<int, int>(fleetId, vehicleId);
	            lock (_syncObject)
	            {
	                return _unitHardwareConfigSegmentSummaries[key].Key;
	            }
	        }

	        catch (Exception ex)
	        {
	            _log.Error("GetCachedHardwareConfigSegmentSummary (May be Listener interface just started and not have cached information yet)" + ex);
	        }

            return null;
	    }

        private void CheckHardwareConfig(DeviceLayer deviceLayer, bool liveUpdate = false)
		{
			// As we now have a new method of working with hardware configs we need to test to see if type two is available
			// once we have determined that it is or isn't available then we can send back the appropriate type
			//
			// Dealing with new database hardware config version
			// Request Version 1 is an old package type, so we need to fetch from the new tables but package it up in the old way using the old sp, re-written
			//
			// Dealing with old database hardware config version
			// Request Version 2 that comes in and we only support the old hardare config means the listener will throw away the packet, the unit will have to keep track of sending
			// it in using the old method.
			RequestHardwareConfigSegment request = null;

			try
			{
				SegmentLayer segment = null;
				DataSet data = null;

			    // if it is live update push from clients via listener interface service. 
			    // the device layer is a keep alive segment and use it to create new request hardware config segment.
			    if (liveUpdate)
			    {
			        var kaSegment = new KeepAliveSegment(deviceLayer.Segments[0]);
			        request = new RequestHardwareConfigSegment
			        {
			            Fleet = kaSegment.Fleet,
			            Vehicle = kaSegment.Vehicle
			        };
			    }
			    else
			    {
			        request = new RequestHardwareConfigSegment(deviceLayer.Segments[0]);
			    }


                var hardwareConfigSegmentSummary = GetCachedHardwareConfigSegmentSummary(request.Fleet, request.Vehicle);
			    if (hardwareConfigSegmentSummary == null)
			    {
			        _log.Error($"Error GetCachedHardwareConfigSegmentSummary for fleet {request.Fleet}, vehicle {request.Vehicle}");
			        return;
			    }

			    // if it is live update push from clients via listener interface service. 
			    // initialize the newly created request hardware config segment from the cached values.
			    if (liveUpdate)
			    {
			        request.Version = hardwareConfigSegmentSummary.SegmentVersion;
			        request.ConfigId = hardwareConfigSegmentSummary.ConfigId;
			        request.ConfigVersion = hardwareConfigSegmentSummary.ConfigVersion;
			    }

			    _log.Debug($"Fleet ID: {request.Fleet}, Unit ID:{request.Vehicle} is running hardware config segment Version: {request.Version}, ConfigID: {request.ConfigId}, Config Version: {request.ConfigVersion} ");

                if (hardwareConfigSegmentSummary.SegmentVersion == 1)
                {
					string sql = string.Format("exec HardwareConfigGetForVehicle {0}, {1}", request.Fleet, request.Vehicle);

					data = ExecuteSQL(sql);

					if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
					{
						DataRow row = data.Tables[0].Rows[0];

						//check if id and version number are differnent to what is currently being run on the unit
						int configId = Convert.ToInt32(row["ID"]);
						int version = Convert.ToInt32(row["VersionNumber"]);

					    if (hardwareConfigSegmentSummary.ConfigId != configId || hardwareConfigSegmentSummary.ConfigVersion != version)
                        {
							segment = new HardwareConfigSegment();

							((HardwareConfigSegment)segment).ConfigId = configId;
							((HardwareConfigSegment)segment).ConfigVersion = version;
							((HardwareConfigSegment)segment).Name = Convert.ToString(row["Name"]);
							((HardwareConfigSegment)segment).SerialA = (SerialConnections)Convert.ToInt32(row["SerialA"]);
							((HardwareConfigSegment)segment).SerialAShort = (SerialConnections)Convert.ToInt32(row["SerialAShort"]);
							((HardwareConfigSegment)segment).SerialALong = (SerialConnections)Convert.ToInt32(row["SerialALong"]);
							((HardwareConfigSegment)segment).SerialB = (SerialConnections)Convert.ToInt32(row["SerialB"]);
							((HardwareConfigSegment)segment).SerialBShort = (SerialConnections)Convert.ToInt32(row["SerialBShort"]);
							((HardwareConfigSegment)segment).SerialBLong = (SerialConnections)Convert.ToInt32(row["SerialBLong"]);
							((HardwareConfigSegment)segment).Usb = (UsbConnections)Convert.ToInt32(row["USB"]);
							((HardwareConfigSegment)segment).Ethernet = (EthernetConnections)Convert.ToInt32(row["Ethernet"]);
							((HardwareConfigSegment)segment).Feature = (Features)Convert.ToInt32(row["Features"]);

                            _log.Debug($"Created HardwareConfigSegment version {segment.Version}");
                            //SendToUnit(segment, deviceLayer);
                        }
                    }
                }
                else
                {
                    Boolean sendConfigRequest = false;

                    // Save the unit's current hardware configuration id, version to the database t_vehicle table
                    string sql = string.Format("exec SaveCurrentHardwareConfigForVehicle {0}, {1}, {2}, {3}", request.Fleet, request.Vehicle, request.ConfigId, request.ConfigVersion);
                    data = ExecuteSQL(sql);

                    //  TypeID 1 is a Hardware Config
                    sql = string.Format("exec GetConfigForVehicleByFleetIDVehicleIDByType {0}, {1}, 1", request.Fleet, request.Vehicle);
                    data = ExecuteSQL(sql);

					if (data.Tables.Count > 0)
					{
						if (data.Tables[0].Rows.Count > 0)
						{
							// As all rows contain the same id and version, we can just grab the first row
							DataRow row = data.Tables[0].Rows[0];

							// We have an oldConfigID which was migrated over from old table, this will stop units from seeing a new config even when nothing has changed
							// we don't want them all downloading new ones as soon at the upgrade is done, any editing will not include the old number as it is now redundant.
							int configId = Convert.ToInt32(row["ID"]);
							int oldConfigId = Convert.ToInt32(row["OldConfigID"]);
							int version = Convert.ToInt32(row["VersionNumber"]);

                            _log.InfoFormat("HWConfig Request ConfigID {0}, Version {1}, Data Returned Old ConfigID {2}, ConfigID {3}, Evaluated ConfigID {4}, Version {5}",
                                request.ConfigId, request.ConfigVersion, oldConfigId, configId, (oldConfigId == -1 ? configId : oldConfigId), version);
                            // Save the unit's current hardware configuration id, version to the database t_vehicle table
                            sql =
                                $"exec SaveCurrentHardwareConfigForVehicle {request.Fleet}, {request.Vehicle}, {request.ConfigId}, {request.ConfigVersion}";
                            ExecuteSQL(sql);

                            // Only return something if we have a difference otherwise the unit continues with what it hasCom
                            if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
							{
							    if (hardwareConfigSegmentSummary.ConfigId != (oldConfigId == -1 ? configId : oldConfigId) || hardwareConfigSegmentSummary.ConfigVersion != version)
                                {
									segment = new HardwareConfigNameValueSegment();
                                    _log.Debug($"Created HardwareConfigNameValueSegment version {segment.Version}");

                                    ((INameValueConfig)segment).ParseDataset(data);
								}
							}
							else
							{
								sendConfigRequest = true;
							}
						}
						else
						{
							sendConfigRequest = true;
						}
					}
					else
					{
						sendConfigRequest = true;
					}

					if (sendConfigRequest)
					{
                        segment = new RequestDefaultHardwareConfigSegment()
                        {
                            FleetID = request.Fleet,
                            VehicleID = request.Vehicle
                        };
                    }
				}

				if (segment != null)
				{
				    _log.Debug("CheckHardwareConfig SendToUnit");
                    // Only send if we have an initialised segment
                    SendToUnit(segment, deviceLayer);
				}
			}
			catch (Exception exp)
			{
				if (request != null)
				{
					_log.Error(string.Format("Error handling Request Hardware Config for fleet {0}, vehicle {1}", request.Fleet, request.Vehicle), exp);
				}
				else
				{
					_log.Error("Error handling Request Hardware Config", exp);
				}
			}
		}

		private void CheckDvrConfig(DeviceLayer deviceLayer)
		{
			RequestDvrConfigSegment request = null;

			try
			{
				SegmentLayer segment = null;

				request = new RequestDvrConfigSegment(deviceLayer.Segments[0]);

				// Update the vehicle with the DVR configuration it is currently running
				ExecuteSQL(string.Format("exec isp_DvrConfigCurrent {0}, {1}, {2}, {3}, {4}", request.Fleet, request.Vehicle, request.ConfigNo, request.ConfigMajorVersion, request.ConfigMinorVersion));

				// Only response to the request for known versions
				if (request.Version == 1)
				{
					// Setup the detail with the default information (everything 0 by default)
					DvrConfigDetail detail = new DvrConfigDetail();

					DataSet data = ExecuteSQL(string.Format("exec usp_DvrConfigForVehicle {0}, {1}", request.Fleet, request.Vehicle));

					if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
					{
						DataRow row = data.Tables[0].Rows[0];

						// Setup the detail with the base information
						detail = new DvrConfigDetail
						{
							ConfigNo = Convert.ToInt32(row["ConfigNo"]),
							ConfigVersionMajor = Convert.ToInt32(row["VersionMajor"]),
							ConfigVersionMinor = Convert.ToInt32(row["VersionMinor"]),
							Url = _uploadAccidentBufferUrl,
							UrlStream = _dvrServiceUrl
						};

						// Check if config number or version number are differnent to what is currently being run on the unit
						if (request.ConfigNo != detail.ConfigNo || request.ConfigMajorVersion != detail.ConfigVersionMajor || request.ConfigMinorVersion != detail.ConfigVersionMinor)
						{
							// Populate the DVR segment with the DVR configuration
							detail.Events = new List<DvrConfigEvent>();
							detail.SendHealthStatus = Convert.ToBoolean(row["SendHealthStatus"]);
							detail.HealthStatusUtcOffset = Convert.ToInt32(row["HealthStatusUtcOffset"]);
                            detail.ContentStorageHours = new DvrConfigContentStorage
                            {
                                Image = Convert.ToInt32(row["ContentStorageImageHour"]),
                                VideoLowRes = Convert.ToInt32(row["ContentStorageVideoLowHour"]),
                                VideoHighRes = Convert.ToInt32(row["ContentStorageVideoHighHour"])
                            };

							if (data.Tables.Count > 1 && data.Tables[1].Rows.Count > 0)
							{
								// Add each of the configuration events
								foreach (DataRow eventRow in data.Tables[1].Rows)
								{
									try
									{
										detail.Events.Add(new DvrConfigEvent
										{
											ReasonId = Convert.ToInt32(eventRow["ReasonID"]),
											AutoImageUpload = Convert.ToInt32(eventRow["AutoImageUpload"]),
											AutoLowResVideoUpload = Convert.ToInt32(eventRow["AutoLowResVideoUpload"]),
											AutoLowResVideoDuration = Convert.ToInt32(eventRow["AutoLowResVideoDuration"])
										});
									}
									catch (Exception ex)
									{
										_log.Error("CheckDvrConfig. Unable to parse config events", ex);
									}
								}
							}
						}
					}

					// Always set the segment detail to handle the following cases:
					// - New configuration will send the entire structure
					// - Existing configuration will return the configNo, major version, minor version and url
					// - No configuration will return the configNo, major version, minor version all set to 0
					segment = new DvrConfigSegment();
					((DvrConfigSegment)segment).Detail = detail;
				}

				if (segment != null)
				{
					_log.Error(((DvrConfigSegment)segment).GetText());
				}

				// Only send if we have an initialised segment
				if (segment != null)
				{
					SendToUnit(segment, deviceLayer);
				}
			}
			catch (Exception exp)
			{
				if (request != null)
				{
					_log.Error(string.Format("Error handling Request DVR Config for fleet {0}, vehicle {1}", request.Fleet, request.Vehicle), exp);
				}
				else
				{
					_log.Error("Error handling Request DVR Config", exp);
				}
			}
		}

        /// <summary>
        /// Evaluate the request overspeed config and send the configuration to the unit
        /// </summary>
        private void HandleOverSpeedConfigRequest(DeviceLayer deviceLayer)
        {
            RequestOverSpeedConfigSegment request = null;
            SendOverSpeedConfigSegment segment = null;

            try
            {
                request = new RequestOverSpeedConfigSegment(deviceLayer.Segments[0]);

                _log.InfoFormat("Received overspeed config request. Unit:{0}, Config: {1} v{2}.{3}, WaypointGroup: {4} v{5}.{6}",
                    deviceLayer.SerialNumber, request.ConfigId, request.ConfigVersionMajor, request.ConfigVersionMinor, request.WaypointGroupId, request.WaypointGroupVersionMajor, request.WaypointGroupVersionMinor);

                // Only response to the request for known versions
                if (request.Version == 1)
                {
                    segment = new SendOverSpeedConfigSegment();

                    DataSet data = ExecuteSQL(string.Format("exec usp_GetOverspeedConfig {0}, {1}, {2}, {3}, {4}, {5}, {6}", request.ConfigId, request.ConfigVersionMajor, 
                        request.ConfigVersionMinor, request.WaypointGroupId, request.WaypointGroupVersionMajor, request.WaypointGroupVersionMinor, deviceLayer.SerialNumber));

                    if (data != null)
                    {
                        // Populate the configuration information if one is assignment to the configuration
                        if (data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
                        {
                            DataRow row = data.Tables[0].Rows[0];

                            segment.ConfigId = Convert.ToInt32(row["ConfigId"]);
                            segment.ConfigVersionMajor = Convert.ToInt32(row["ConfigVersionMajor"]);
                            segment.ConfigVersionMinor = Convert.ToInt32(row["ConfigVersionMinor"]);
                            segment.GazettedZone1 = ParseOverSpeedConfig(row, "Zone1Speed", "Zone1Tolerance", "Zone1Flags");
                            segment.GazettedZone2 = ParseOverSpeedConfig(row, "Zone2Speed", "Zone2Tolerance", "Zone2Flags");
                            segment.GazettedZone3 = ParseOverSpeedConfig(row, "Zone3Speed", "Zone3Tolerance", "Zone3Flags");
                            segment.GazettedZone4 = ParseOverSpeedConfig(row, "Zone4Speed", "Zone4Tolerance", "Zone4Flags");
                        }

                        // Populate the waypoint group information if one is assignment to the waypoint group
                        if (data.Tables.Count > 1 && data.Tables[1].Rows.Count > 0)
                        {
                            DataRow row = data.Tables[1].Rows[0];

                            segment.WaypointGroupId = Convert.ToInt32(row["SetPointGroupId"]);
                            segment.WaypointGroupVersionMajor = request.WaypointGroupVersionMajor;
                            segment.WaypointGroupVersionMinor = request.WaypointGroupVersionMinor;
                            segment.WaypointZone1 = ParseOverSpeedConfig(row, "Zone1Speed", "Zone1Tolerance", "Zone1Flags");
                            segment.WaypointZone2 = ParseOverSpeedConfig(row, "Zone2Speed", "Zone2Tolerance", "Zone2Flags");
                            segment.WaypointZone3 = ParseOverSpeedConfig(row, "Zone3Speed", "Zone3Tolerance", "Zone3Flags");
                            segment.WaypointZone4 = ParseOverSpeedConfig(row, "Zone4Speed", "Zone4Tolerance", "Zone4Flags");
                        }
                    }
                }

                // Only send if we have an initialised segment
                if (segment != null)
                {
                    SendToUnit(segment, deviceLayer);
                }
            }
            catch (Exception exp)
            {
                if (request != null)
                {
                    _log.Error(string.Format("Error handling Request Over Speed Config Segment for Unit:{0}, Config: {1} v{2}.{3}, WaypointGroup: {4} v{5}.{6}",
                        deviceLayer.UnitId, request.ConfigId, request.ConfigVersionMajor, request.ConfigVersionMinor, request.WaypointGroupId, request.WaypointGroupVersionMajor, request.WaypointGroupVersionMinor), exp);
                }
                else
                {
                    _log.Error("Error handling Request Over Speed Config Segment", exp);
                }
            }
        }

        /// <summary>
        /// Parse the data row to return the populated OverSpeedConfig
        /// </summary>
        /// <param name="row">Data row with configuration or waypoint group overspeed data</param>
        /// <param name="columnSpeed">Name of the column containing the speed information (Zone1Speed)</param>
        /// <param name="columnTolerance">Name of the column containing the tolerance information (Zone1Tolerance)</param>
        /// <param name="columnFlags">Name of the column containing the flag information (Zone1Flags)</param>
        /// <returns>Populated OverSpeedConfig</returns>
        private OverSpeedConfig ParseOverSpeedConfig(DataRow row, string columnSpeed, string columnTolerance, string columnFlags)
        {
            double? speed = row.Field<double?>(columnSpeed);

            // Zone is active if speed has a value and speed is >= 0
            if (speed.HasValue && speed.Value >= 0)
            {
                return new OverSpeedConfig(true, Convert.ToSingle(speed ?? 0), Convert.ToByte(row.Field<short?>(columnTolerance) ?? 0), row.Field<int?>(columnFlags) ?? 0);
            }

            // For inactive zone return empty OverSpeedConfig where active will default to false
            return new OverSpeedConfig();
        }

        private DataSet ExecuteSQL(string sql)
        {
            try
            {
                if (_config.LogSQLData) _log.Info("SQL Command : " + sql);

				DataSet dsRet = new DataSet();
				using (SqlConnection oConn = new SqlConnection(_config.TransportAppDSN))
				using (SqlCommand oCmd = new SqlCommand(sql, oConn))
				using (SqlDataAdapter oDA = new SqlDataAdapter(oCmd))
				{
					oDA.Fill(dsRet);
				}
				return dsRet;
			}
			catch (System.Exception ex)
			{
			   _log.Error("ExecuteSQL(sSQL = '" + sql + ")", ex);
				return null;
			}
		}
		/// <summary>
		/// send a list of segments to a unit
		/// </summary>
		/// <param name="segments">The segment list</param>
		/// <param name="deviceLayer">the device Layer received from the unit</param>
		private void SendToUnit(List<SegmentLayer> segments, DeviceLayer deviceLayer)
		{
			DeviceLayer packet = new DeviceLayer();
			packet.Type = deviceLayer.Type;
			packet.Version = deviceLayer.Version;
			packet.UnitId = deviceLayer.UnitId;
			packet.SerialNumber = deviceLayer.SerialNumber;
			packet.Hardware = deviceLayer.Hardware;
			packet.StandardDeviceFlag = true;
			packet.Segments = segments;
			lock (_toMobileUnitsQ.SyncRoot)
			{
				_toMobileUnitsQ.Enqueue(packet);
			}
		}
		/// <summary>
		/// send a segment to a unit
		/// </summary>
		/// <param name="segment">The segment to send</param>
		/// <param name="deviceLayer">the device Layer received from the unit</param>
		private void SendToUnit(SegmentLayer segment, DeviceLayer deviceLayer)
		{
			List<SegmentLayer> segments = new List<SegmentLayer>();
			segments.Add(segment);
			SendToUnit(segments, deviceLayer);
		}

		private DataSet GetListDataSet(int listId, int listVersion)
		{
			string sql = string.Format("exec lsp_GetList {0}, {1}", listId, listVersion);
			DataSet dsRet = ExecuteSQL(sql);
			if (dsRet != null && dsRet.Tables.Count > 0 && dsRet.Tables[0].Rows.Count > 0)
			{
				ListTypes listType = (ListTypes)Convert.ToInt32(dsRet.Tables[0].Rows[0]["ListType"]);
				//name standard tables
				bool addAddressTableNames = false;
				int index = 0;
				dsRet.Tables[index++].TableName = "List";
				dsRet.Tables[index++].TableName = "ListItem";
				switch (listType)
				{
					case ListTypes.Division:
						dsRet.Tables[index++].TableName = "ListItemDivision";
						break;
					case ListTypes.RejectReasons:
						dsRet.Tables[index++].TableName = "ListItemReject";
						break;
					case ListTypes.PalletTypes:
						dsRet.Tables[index++].TableName = "ListItemPallets";
						break;
					case ListTypes.Trailers:
						dsRet.Tables[index++].TableName = "ListItemTrailers";
						break;
					case ListTypes.FleetAddressBook:
					case ListTypes.FuelStations:
						addAddressTableNames = true;
						break;
					case ListTypes.DriverAddressBook:
						dsRet.Tables[index++].TableName = "DriverDetails";
						addAddressTableNames = true;
						break;
					case ListTypes.Questions:
						dsRet.Tables[index++].TableName = "ListQuestion";
						dsRet.Tables[index++].TableName = "ListItemQuestion";
						dsRet.Tables[index++].TableName = "ListItemQuestionList";
						dsRet.Tables[index++].TableName = "ListItemQuestionDefault";
						break;
					case ListTypes.FleetPhoneBook:
					case ListTypes.DriverPhoneBook:
						dsRet.Tables[index++].TableName = "Phonebook";
						dsRet.Tables[index++].TableName = "PhonebookEntries";
						break;
					case ListTypes.FleetEmailAddressBook:
						dsRet.Tables[index++].TableName = "ListItemEmailAddress";
						break;
					case ListTypes.DriverEmailAddressBook:
						dsRet.Tables[index++].TableName = "DriverDetails";
						dsRet.Tables[index++].TableName = "ListItemEmailAddress";
						break;
					case ListTypes.LoginTypes:
						dsRet.Tables[index++].TableName = "ListLoginTypes";
						break;
					case ListTypes.TransferStations:
						dsRet.Tables[index++].TableName = "ListItemTransferStation";
						addAddressTableNames = true;
						break;
					case ListTypes.Customers:
						dsRet.Tables[index++].TableName = "ListItemCustomer";
						dsRet.Tables[index++].TableName = "ListItemAddress";
						dsRet.Tables[index++].TableName = "SetPoint";
						dsRet.Tables[index++].TableName = "Poi";
						dsRet.Tables[index++].TableName = "AdditionalAddress";
						dsRet.Tables[index++].TableName = "SetPointNode";
						dsRet.Tables[index++].TableName = "PoiNode";
						dsRet.Tables[index++].TableName = "WaitTimeEmailAddress";
						dsRet.Tables[index++].TableName = "PodEmailAddress";
						dsRet.Tables[index++].TableName = "Attachments";
						break;
					case ListTypes.Parts:
						dsRet.Tables[index++].TableName = "ListItemPart";
						dsRet.Tables[index++].TableName = "Attachment";
						break;
					case ListTypes.Person:
						dsRet.Tables[index++].TableName = "ListItemPerson";
						addAddressTableNames = true;
						break;
					case ListTypes.PassengerStop:
						dsRet.Tables[index++].TableName = "ListItemPassengerStop";
						addAddressTableNames = true;
						break;
					case ListTypes.Organisation:
						dsRet.Tables[index++].TableName = "ListItemOrganisation";
						addAddressTableNames = true;
						break;
					case ListTypes.JobTemplate:
						dsRet.Tables[index++].TableName = "ListItemJobTemplate";
						dsRet.Tables[index++].TableName = "ListItemJobTemplateValues";
						dsRet.Tables[index++].TableName = "ItemJobTemplateFields";
						break;
					case ListTypes.MassScheme:
						dsRet.Tables[index++].TableName = "ListItemMassScheme";
						dsRet.Tables[index++].TableName = "ListItemMassVehicleSection";
						break;
					case ListTypes.RuleSets:
						dsRet.Tables[index++].TableName = "ListItemRule";
						dsRet.Tables[index++].TableName = "ListItemRuleAction";
						break;
				}

				if (addAddressTableNames)
				{
					dsRet.Tables[index++].TableName = "ListItemAddress";
					dsRet.Tables[index++].TableName = "SetPoint";
					dsRet.Tables[index++].TableName = "Poi";
					dsRet.Tables[index++].TableName = "SetPointNode";
					dsRet.Tables[index++].TableName = "PoiNode";
				}
			}
			return dsRet;
		}

		private object RefreshDataThread(EnhancedThread sender, object data)
		{
			int refreshCounter = 3000; // 5 minutes in 100ms increments.
			_removeOldUnitsTime = DateTime.Now.AddMinutes(60);

			while (!sender.Stopping)
			{
				try
				{
					refreshCounter--;
					if (refreshCounter <= 0)
					{
						refreshCounter = 3000;

						//load any outstanding accident buffer requests
						DataSet requests = ExecuteSQL("exec usp_GetUnSetAccidentBufferRequests");
						if (requests.Tables.Count > 0)
						{
							foreach (DataRow row in requests.Tables[0].Rows)
							{
								SendAccidentBufferRequest(row);
							}
						}

						//load any outstanding extended history requests
						requests = ExecuteSQL("exec usp_GetExtendedHistoryDataForListenerInterface");
						if (requests.Tables.Count > 0)
						{
							foreach (DataRow row in requests.Tables[0].Rows)
							{
								SendExtendedHistoryRequest(row);
							}
						}

						//load any outstanding dvr requests
						requests = ExecuteSQL("exec usp_GetDvrRequestDataForListenerInterface");
						if (requests.Tables.Count > 0)
						{
							foreach (DataRow row in requests.Tables[0].Rows)
							{
								SendDvrRequestRequest(row);
							}
						}
					}

					if (DateTime.Now > _removeOldUnitsTime)
					{
						DateTime t = DateTime.Now.AddHours(-1);
					    DateTime t2 = DateTime.Now.AddHours(-20);
                        lock (_syncObject)
						{
							var oldUnits = _unitDeviceLayer.Where(d => d.Value.Value < t).ToArray();
							foreach (var oldUnit in oldUnits)
							{
								_log.ErrorFormat("Removing old unit {0}", oldUnit.Key);
								_unitDeviceLayer.Remove(oldUnit.Key);
							}
						}

					    var oldUnitsHcss = _unitHardwareConfigSegmentSummaries.Where(d => d.Value.Value < t2).ToArray();
					    foreach (var oldUnit in oldUnitsHcss)
					    {
					        _log.ErrorFormat("Removing old unit hardware config segment summary {0}", oldUnit.Key);
					        _unitHardwareConfigSegmentSummaries.Remove(oldUnit.Key);
					    }

                        //set remove old units time for 1 minute
                        _removeOldUnitsTime = DateTime.Now.AddMinutes(1);
					}


					Thread.Sleep(100);
				}
				catch (Exception exp)
				{
					_log.Error(exp);
				}
			}

			_log.Info("Stopped refresh data thread");
			return null;
		}

		private void SendAccidentBufferRequest(DataRow row)
		{
			//get fleet and vehicle and see if we have heard from this unit
			int fleet = Convert.ToInt32(row["FleetId"]);
			int vehicle = Convert.ToInt32(row["VehicleId"]);
		
			Tuple<int, int> key = new Tuple<int, int>(fleet, vehicle);
			DeviceLayer device = null;
			lock (_syncObject)
			{
				if (_unitDeviceLayer.ContainsKey(key))
				{
					device = _unitDeviceLayer[key].Key;
				}
			}

			if (device != null)
			{
				RequestAccidentBufferSegment seg = new RequestAccidentBufferSegment();
				seg.UploadId = row["UploadGuid"].ToString();
				seg.Url = _uploadAccidentBufferUrl;
				seg.FileType = 1;
				seg.AccidentTime = Convert.ToDateTime(row["AccidentDate"]);
				_log.InfoFormat("Sending accident buffer request {0} to unit {1}, {2}", row["Id"], fleet, vehicle);
				SendToUnit(seg, device);

				//update request sent in database
				ExecuteSQL(string.Format("exec usp_AccidentBufferRequestSent {0}", row["Id"]));
			}
			else
			{
				_log.InfoFormat("unknown unit {1}, {2} - can't send accident buffer request {0}", row["Id"], fleet, vehicle);
			}
		}
		private void SendExtendedHistoryRequest(DataRow row)
		{
			//get fleet and vehicle and see if we have heard from this unit
			int fleet = Convert.ToInt32(row["FleetId"]);
			int vehicle = Convert.ToInt32(row["VehicleId"]);

			Tuple<int, int> key = new Tuple<int, int>(fleet, vehicle);
			DeviceLayer device = null;
			lock (_syncObject)
			{
				if (_unitDeviceLayer.ContainsKey(key))
				{
					device = _unitDeviceLayer[key].Key;
				}
			}

			if (device != null)
			{
				RequestExtendedHistorySegment seg = new RequestExtendedHistorySegment();
				seg.UploadId = row["UploadGuid"].ToString();
				seg.Url = _uploadAccidentBufferUrl;
				seg.FileType = 2;
				seg.StartTime = Convert.ToDateTime(row["StartDate"]);
				seg.EndTime = Convert.ToDateTime(row["EndDate"]);
				_log.InfoFormat("Sending extended history request {0} to unit {1}, {2}", row["Id"], fleet, vehicle);
				SendToUnit(seg, device);

				//update request sent in database
				ExecuteSQL(string.Format("exec usp_ExtendedHistoryRequestSent {0}", row["Id"]));
			}
			else
			{
				_log.InfoFormat("unknown unit {1}, {2} - can't send extened history request {0}", row["Id"], fleet, vehicle);
			}
		}

		/// <summary>
		/// Send the DVR content request to the unit
		/// </summary>
		private void SendDvrRequestRequest(DataRow row)
		{
			// Get fleet and vehicle and see if we have heard from this unit
			int fleet = Convert.ToInt32(row["FleetId"]);
			int vehicle = Convert.ToInt32(row["VehicleId"]);

			_log.InfoFormat("Attempt sending dvr request to unit {0}, {1}", fleet, vehicle);

			Tuple<int, int> key = new Tuple<int, int>(fleet, vehicle);
			DeviceLayer device = null;
			lock (_syncObject)
			{
				if (_unitDeviceLayer.ContainsKey(key))
				{
					device = _unitDeviceLayer[key].Key;
				}
			}

			if (device != null)
			{
				RequestDvrSegment seg = new RequestDvrSegment();
				seg.UploadId = row["UploadGuid"].ToString();
				seg.Url = _uploadAccidentBufferUrl;
				seg.RequestType = Convert.ToInt32(row["RequestType"]);
				seg.ContentType = Convert.ToInt32(row["ContentType"]);
				seg.ResponseChannel = Convert.ToInt32(row["ResponseChannel"]);
				seg.Duration = !DBNull.Value.Equals(row["Duration"]) ? Convert.ToInt32(row["Duration"]) : 0;

				if (!DBNull.Value.Equals(row["DateStart"]))
				{
					seg.DateStart = Convert.ToDateTime(row["DateStart"]);
				}

				if (!DBNull.Value.Equals(row["DateEnd"]))
				{
					seg.DateEnd = Convert.ToDateTime(row["DateEnd"]);
				}

				if (!DBNull.Value.Equals(row["AlarmId"]))
				{
					seg.AlarmId = Convert.ToInt32(row["AlarmId"]);
		}

				_log.InfoFormat("Sending dvr request {0} to unit {1}, {2}, UploadGuid:{3}", row["Id"], fleet, vehicle, row["UploadGuid"].ToString());

				SendToUnit(seg, device);

				// Update request sent in database
				ExecuteSQL(string.Format("exec usp_DvrRequestSent {0}", row["Id"]));
			}
			else
			{
				_log.InfoFormat("unknown unit {1}, {2} - can't send dvr request {0}", row["Id"], fleet, vehicle);
			}
		}

		/// <summary>
		/// Send the DVR configuration to the unit
		/// </summary>
		/// <param name="fleetId">Id of the fleet</param>
		/// <param name="vehicleId">Id of the vehicle, set to -1 to send to all vehicle assigned to the configNo</param>
		/// <param name="configNo">The number of the DVR configuration</param>
		private void SendDvrConfigRequest(int fleetId, int vehicleId, int configNo)
		{
			// Ensure the config no is 0 or above
			if (configNo < 0)
			{
				configNo = 0;
			}

			// Only support the clearing of the configuration for a single vehicle and not all vehicles
			if (configNo == 0 && vehicleId <= 0)
			{
				return;
			}

			DataSet requests = ExecuteSQL(string.Format("SELECT FleetId, Id FROM T_Vehicle WHERE FleetID={0} AND (ID={1} OR {1}=-1) AND ISNULL(DvrConfigNo, 0)={2}", fleetId, vehicleId, configNo));

			if (requests.Tables.Count > 0)
			{
				// Setup the detail with the default information (everything 0 by default)
				DvrConfigDetail detail = new DvrConfigDetail();

				// Only popuplate the configuration detail if a configNo is supplied
				if (configNo > 0)
				{
					DataSet data = ExecuteSQL(string.Format("exec usp_DvrConfigForVehicle {0}, {1}, {2}", fleetId, vehicleId, configNo));

					if (data != null && data.Tables.Count > 0 && data.Tables[0].Rows.Count > 0)
					{
						DataRow row = data.Tables[0].Rows[0];

                        // Setup the configuration detail
                        detail = new DvrConfigDetail
                        {
                            ConfigNo = Convert.ToInt32(row["ConfigNo"]),
                            ConfigVersionMajor = Convert.ToInt32(row["VersionMajor"]),
                            ConfigVersionMinor = Convert.ToInt32(row["VersionMinor"]),
                            Url = _uploadAccidentBufferUrl,
                            UrlStream = _dvrServiceUrl,
                            Events = new List<DvrConfigEvent>(),
                            SendHealthStatus = Convert.ToBoolean(row["SendHealthStatus"]),
                            HealthStatusUtcOffset = Convert.ToInt32(row["HealthStatusUtcOffset"]),
                            ContentStorageHours = new DvrConfigContentStorage
                            {
                                Image = Convert.ToInt32(row["ContentStorageImageHour"]),
                                VideoLowRes = Convert.ToInt32(row["ContentStorageVideoLowHour"]),
                                VideoHighRes = Convert.ToInt32(row["ContentStorageVideoHighHour"])
                            }
                        };

						if (data.Tables.Count > 1 && data.Tables[1].Rows.Count > 0)
						{
							// Add each of the configuration events
							foreach (DataRow eventRow in data.Tables[1].Rows)
							{
								try
								{
									detail.Events.Add(new DvrConfigEvent
									{
										ReasonId = Convert.ToInt32(eventRow["ReasonID"]),
										AutoImageUpload = Convert.ToInt32(eventRow["AutoImageUpload"]),
										AutoLowResVideoUpload = Convert.ToInt32(eventRow["AutoLowResVideoUpload"]),
										AutoLowResVideoDuration = Convert.ToInt32(eventRow["AutoLowResVideoDuration"])
									});
								}
								catch (Exception ex)
								{
									_log.Error("CheckDvrConfig. Unable to parse config events", ex);
								}
							}
						}
					}
				}

				// Attempt to send to each vehicle that is assigned to the configuration
				foreach (DataRow row in requests.Tables[0].Rows)
				{
					// Get fleet and vehicle and see if we have heard from this unit
					int data_fleetId = Convert.ToInt32(row["FleetId"]);
					int data_vehicleId = Convert.ToInt32(row["Id"]);

					_log.DebugFormat("Attempt sending dvr config to unit {0}, {1}", data_fleetId, data_vehicleId);

					Tuple<int, int> key = new Tuple<int, int>(data_fleetId, data_vehicleId);
					DeviceLayer device = null;
					lock (_syncObject)
					{
						if (_unitDeviceLayer.ContainsKey(key))
						{
							device = _unitDeviceLayer[key].Key;
						}
					}

					if (device != null)
					{
						SegmentLayer segment = new DvrConfigSegment();
						((DvrConfigSegment)segment).Detail = detail;

						_log.DebugFormat("Sending dvr config {0} to unit {1}, {2}", configNo, data_fleetId, data_vehicleId);
						_log.DebugFormat("Sending dvr config: {0}", ((DvrConfigSegment)segment).GetText());

						SendToUnit(segment, device);
					}
					else
					{
						_log.DebugFormat("Unknown unit {1}, {2} - can't send dvr config {0}", configNo, data_fleetId, data_vehicleId);
					}
				}
			}
		}

		private void ProcessListenerMessages(Tuple<int, int> key)
		{
			CheckForValidOBMessageList();

			List<QueuedMessage> unitMesages = null;
			lock (_messageCheckSync)
			{
				unitMesages = _messageList.FindAll(mess => mess.FleetId == key.Item1 && mess.VehicleId == key.Item2).OrderBy(mess => mess.Sequence).ToList();
			}

			foreach (QueuedMessage qm in unitMesages)
			{
				// Update the status of the message to "InProgress"
				_log.Info(String.Format("Sending {0} Message To Unit {1}", qm.MessageTypeId, key));
				Manager.UpdateMessageStatus(qm, MessageStatus.InProgress);

				try
				{
					switch (qm.MessageTypeId)
					{
						case ListenerMessageTypes.IAPDataBlocks:     // IAP data blocks
							UpdateIAPDataBlocksSegment blocks = new UpdateIAPDataBlocksSegment();
							blocks.PostionId = Convert.ToUInt32(qm.ExtraDataAsXmlNode.Attributes["position"].Value);
							blocks.SpeedId = Convert.ToUInt32(qm.ExtraDataAsXmlNode.Attributes["speed"].Value);
							blocks.BlockId = Convert.ToUInt32(qm.ExtraDataAsXmlNode.Attributes["block"].Value);
							blocks.UpdateId = qm.Id;

							SendToUnit(blocks, _unitDeviceLayer[key].Key);
							break;
						default:
							_log.Error(String.Format("Unknown MessageID {0}, F:{1},V:{2}", qm.MessageTypeId, qm.FleetId, qm.VehicleId));
							break;
					}
				}
				catch (Exception exp)
				{
					// Update the status to "Failed"
					_log.ErrorFormat("Failed to send message {0} to unit {1}, error - {2}", qm.MessageTypeId, key, exp.Message);
					Manager.UpdateMessageStatus(qm, MessageStatus.Failed);
				}

				_messageList.Remove(qm);
			}

		}
		private void CheckForValidOBMessageList()
		{
			lock (_messageCheckSync)
			{
				if (DateTime.Now.Subtract(_lastQueuedMessageCheck).TotalMinutes > 1)
				{
					_messageList.Clear();
					DataSet data = ExecuteSQL("exec GetListenerQueuedMessages 0, 0, NULL, NULL, 0, 0");

					foreach (DataRow row in data.Tables[0].Rows)
					{
						_messageList.Add(new QueuedMessage(row));
					}
					_lastQueuedMessageCheck = DateTime.Now;
				}
			}
		}

        private void HandleAssetPacket(DeviceLayer deviceLayer, SegmentLayer segment)
        {
            try
            {
                AssetSegment assetSegment = new AssetSegment(segment);
                _log.InfoFormat("Received Asset segment from Unit: {0}, SerialNumber: {1}", deviceLayer.UnitId, deviceLayer.SerialNumber);

                switch (assetSegment.AssetType)
                {
                    case AssetType.SatModem:
                        UpdateGatewayUnitIMEI(deviceLayer, assetSegment);
                        break;
                    case AssetType.VIN:
                        UpdateVIN(deviceLayer, assetSegment);
                        break;
                }
            }
            catch (Exception ex)
            {
                _log.Error("Error processing asset packet", ex);
            }
        }

        private void UpdateVIN(DeviceLayer deviceLayer, AssetSegment assetSegment)
        {
            if (deviceLayer != null)
            {
                if (assetSegment != null && assetSegment.AssetData != null)
                {
                    _log.InfoFormat("ListenerInterface/UpdateVIN - SerialNumber: " + deviceLayer.SerialNumber + " VIN: " + assetSegment.AssetData);
                    string sql = string.Format("exec usp_UpdateVehicleVIN '{0}', '{1}'", deviceLayer.SerialNumber, assetSegment.AssetData);

                    ExecuteSQL(sql);
                }
            }
        }

        private void UpdateGatewayUnitIMEI(DeviceLayer deviceLayer, AssetSegment assetSegment)
        {
            if (deviceLayer != null)
            {
                if (assetSegment.AssetData != null)
                {
                    _log.InfoFormat("ListenerInterface/UpdateGatewayUnitIMEI - SerialNumber: " + deviceLayer.SerialNumber + " IMEI: " + assetSegment.AssetData);
                    string sql = string.Format("exec usp_UpdateGatewayUnitIMEI '{0}', '{1}'", deviceLayer.SerialNumber, assetSegment.AssetData);

                    ExecuteSQL(sql);
                }
            }
        }

        private void HandleIAPDataBlockUpdated(DeviceLayer deviceLayer, SegmentLayer segment)
		{
			IAPDataBlocksUpdatedSegment seg = new IAPDataBlocksUpdatedSegment(segment);
			_log.Info(String.Format("Recieved IAP Data blocks updated message from Unit {0}", deviceLayer.SerialNumber));
			// Update the status to "Complete"
			Manager.UpdateMessageStatus(seg.UpdateId, MessageStatus.Complete);
		}

		private void HandleCCSConfigRequest(DeviceLayer deviceLayer, SegmentLayer segment)
		{
			RequestCCSConfigSegment CCSSegment = new RequestCCSConfigSegment(segment);

			// We need to fetch the config for this vehicle looking for both vehicle and fleet first, failing over to fleet only.
			CCS.CCSSetting setting = null;

			if (_CCSSettings.FindAll(settingToFind => settingToFind.FleetID == CCSSegment.FleetID && settingToFind.VehicleID == CCSSegment.VehicleID).Count != 0)
			{
				setting = _CCSSettings.Find(settingToFind => settingToFind.FleetID == CCSSegment.FleetID && settingToFind.VehicleID == CCSSegment.VehicleID);
			}
			else if (_CCSSettings.FindAll(settingToFind => settingToFind.FleetID == CCSSegment.FleetID).Count != 0)
			{
				setting = _CCSSettings.Find(settingToFind => settingToFind.FleetID == CCSSegment.FleetID);
			}

			// Only send something back if we match, otherwise the unit will run with the defaults.
			if (setting != null)
			{
				CCSConfigSegment outSeg = new CCSConfigSegment();

				outSeg.FleetID = CCSSegment.FleetID;
				outSeg.VehicleID = CCSSegment.VehicleID;

				outSeg.FCTimeBeforeEventInSec = setting.FCTimeBeforeEventInSec;
				outSeg.FCTimeAfterEventInSec = setting.FCTimeAfterEventInSec;
				outSeg.FCSampleRateInSec = setting.FCSampleRateInSec;
				outSeg.HBSampleRateInSec = setting.HBSampleRateInSec;
				outSeg.CustomerRef = setting.CustomerRef;
				outSeg.FileUploadUrl = setting.FileUploadUrl;

				SendToUnit(outSeg, deviceLayer);
			}
		}

		private void HandleReceiveDefaultConfig(DeviceLayer deviceLayer, SegmentLayer segment)
		{
			// TODO persist this data back into the T_Vehicle_Config tables
		}

        private void HandleWaypointsRequest(DeviceLayer deviceLayer)
        {
            RequestWaypointsSegment request = null;
            WaypointsSegment segment = null;
            DataTable waypointGroups = null;
            DataTable waypoints = null;
            DataTable waypointNodes = null;

            try
            {
                request = new RequestWaypointsSegment(deviceLayer.Segments[0]);

                _log.InfoFormat("Received waypoints request. Unit:{0}, FleetId: {1}, VehicleId: {2}, WaypointGroup: {3} v{4}.{5}",
                    deviceLayer.SerialNumber, request.FleetId, request.VehicleId, request.WaypointGroupNumber, request.WaypointGroupVersionMajor, request.WaypointGroupVersionMinor);

                segment = new WaypointsSegment();

                var data = ExecuteSQL(string.Format("exec usp_GetSetPointsForListenerByWaypointID {0}, {1}, {2}", request.FleetId, request.VehicleId,
                    request.WaypointGroupNumber));

                if (data == null)
                {
                    segment.WaypointGroupNumber = 0;
                    segment.WaypointGroupVersionMajor = 0;
                    segment.WaypointGroupVersionMinor = 0;
                    segment.IsUpToDate = false;
                }
                else
                {
                    waypointGroups = data.Tables[0];
                    waypoints = data.Tables[1];
                    waypointNodes = data.Tables[2];

                    // Populate the waypoint group information
                    if (waypointGroups.Rows.Count > 0)
                    {
                        var row = waypointGroups.Rows[0];
                        segment.WaypointGroupNumber = Convert.ToInt32(row["SetPointNumber"]);
                        segment.WaypointGroupVersionMajor = Convert.ToByte(row["VersionMajor"]);
                        segment.WaypointGroupVersionMinor = Convert.ToByte(row["VersionMinor"]);
                        segment.IsUpToDate = (request.WaypointGroupNumber == segment.WaypointGroupNumber) && (request.WaypointGroupVersionMajor == segment.WaypointGroupVersionMajor) && (request.WaypointGroupVersionMinor == segment.WaypointGroupVersionMinor);
                    }

                    if (!segment.IsUpToDate && waypoints.Rows.Count > 0)
                    {
                        var waypointList = new List<Waypoint>();
                        foreach (DataRow row in waypoints.Rows)
                        {
                            var setPointID = Convert.ToInt32(row["ID"]);
                            var nodePoints = new List<NodePoint>();
                            foreach (DataRow rowPoint in waypointNodes.Select("SetPointID=" + setPointID))
                            {
                                var nodePoint = new NodePoint
                                {
                                    Latitude = Convert.ToDouble(rowPoint["Latitude"]),
                                    Longitude = Convert.ToDouble(rowPoint["Longitude"])
                                };
                                nodePoints.Add(nodePoint);
                            }

                            var geofenceType = 0;
                            if (nodePoints.Count == 1)
                            {
                                geofenceType = (byte)GeofenceType.Circle;
                            }
                            else if (nodePoints.Count > 1)
                            {
                                geofenceType = (byte)GeofenceType.Polygon;
                            }

                            var wayPoint = new Waypoint
                            {
                                SetPointID = setPointID,
                                Flags = Convert.ToInt32(row["Flags"]),
                                DurationTime = Convert.ToByte(row["DurationTime"]),
                                Speed = (float)Convert.ToDouble(row["MaxSpeedNmH"]),
                                Latitude = Convert.ToDouble(row["Latitude"]),
                                Longitude = Convert.ToDouble(row["Longitude"]),
                                XTolerance = Convert.ToInt32(row["XTolerance"]),
                                YTolerance = Convert.ToInt32(row["YTolerance"]),
                                GeofenceType = (GeofenceType)geofenceType
                            };

                            if (wayPoint.GeofenceType == GeofenceType.Circle)
                            {
                                wayPoint.Radius = Convert.ToInt32(Math.Round(Convert.ToDouble(row["ToleranceNSMetres"]) / Convert.ToDouble(1.852), 0));
                            }
                            else if (wayPoint.GeofenceType == GeofenceType.Polygon)
                            {
                                wayPoint.NodePoints = nodePoints;
                            }
                            waypointList.Add(wayPoint);
                        }
                        segment.Waypoints = waypointList;
                    }
                }
                // Only send if we have an initialised segment
                if (segment != null)
                {
                    SendToUnit(segment, deviceLayer);
                }
            }
            catch (Exception exp)
            {
                if (request != null)
                {
                    _log.Error(string.Format("Error handling Request waypoint Segment for  Unit:{0}, FleetId: {1}, VehicleId: {2}, WaypointGroup: {3} v{4}.{5}",
                        deviceLayer.UnitId, request.FleetId, request.VehicleId, request.WaypointGroupNumber, request.WaypointGroupVersionMajor, request.WaypointGroupVersionMinor), exp);
                }
                else
                {
                    _log.Error("Error handling Request Waypoint Segment", exp);
                }
            }
        }
        #endregion

        #region internal messaging
        /// <summary>
        /// This thread controls the comms between the interface service and it's clients (Transport Web Sevice)
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public object InternalMessagesThread(EnhancedThread sender, object data)
		{
			byte[] receivedBytes = null;
			IPEndPoint remoteIP = new IPEndPoint(IPAddress.Any, 0);

			while (!sender.Stopping)
			{
				try
				{
					if (_internalMessagesUdpClient != null)
					{
						try
						{
							receivedBytes = _internalMessagesUdpClient.Receive(ref remoteIP);
							lock (_internalMessagesQ.SyncRoot)
								_internalMessagesQ.Enqueue(receivedBytes);
						}
						catch (Exception ex)
						{
							if (!sender.Stopping)
							{
								if (!ex.Message.StartsWith("An existing connection was forcibly closed by the remote host"))
								{
									_log.ErrorFormat("UDP Internal Message Error : {0}, [{1}]", ex.Message, ex.StackTrace);
								}
								_internalMessagesUdpClient = new UdpClient(_internalMessagesUdpCPort);
							}
						}
					}
					else
					{
						Thread.Sleep(5000);
						_internalMessagesUdpClient = new UdpClient(_internalMessagesUdpCPort);
					}
				}
				catch (Exception exp)
				{
					_log.Error(exp);
				}
			}

			_log.Info("Stopped listening for UDP on port " + Convert.ToString(_internalMessagesUdpCPort) + ".");
			return null;
		}

		public object ProcesssInternalMessagesThread(EnhancedThread sender, object data)
		{
			byte[] message = null;
			SegmentLayer segment = null;

			_log.Info("Processing Internal messages thread started.");
			while (!sender.Stopping)
			{
				try
				{
					message = null;
					lock (_internalMessagesQ.SyncRoot)
					{
						if (_internalMessagesQ.Count > 0)
							message = (byte[])_internalMessagesQ.Dequeue();
					}
					if (message != null)
					{
						segment = new SegmentLayer();
						segment.Decode(message);

						switch (segment.Type)
						{
							case (int)ListenerSegmentTypes.RequestFeature:
								HandleRequestFeature(segment);
								break;
						    case (int)ListenerSegmentTypes.UpdateHardwareConfig:
						        HandleUpdateHardwareConfig(segment);
						        break;
							default:
								_log.ErrorFormat("Received unkown internal message {0}", segment.Type);
								break;
						}
					}
					else
					{
						Thread.Sleep(100);
					}
				}
				catch (Exception ex)
				{
					_log.Error("Error processing internal message", ex);
				}
			}
			_log.Info("porcess internal message thread stopped.");
			return null;
		}

	    private void HandleUpdateHardwareConfig(SegmentLayer segment)
	    {
	        UpdateHardwareConfigSegment hs = new UpdateHardwareConfigSegment(segment);
	        _log.DebugFormat($"HandleUpdateHardwareConfig for fleet {hs.FleetId}, vehicle {hs.VehicleIds[0]}");

	        DeviceLayer deviceLayer = null;
	        lock (_syncObject)
	        {
	            var key = new Tuple<int, int>(hs.FleetId, hs.VehicleIds[0]);
	            if (_unitDeviceLayer.ContainsKey(key) == false)
	            {
	                _log.Error($"Error HandleUpdateHardwareConfig DeviceLayer not found for fleet {hs.FleetId}, vehicle {hs.VehicleIds[0]}");
	                return;
	            }

	            deviceLayer = _unitDeviceLayer[key].Key;
	        }

	        CheckHardwareConfig(deviceLayer, true);
	    }

        private void HandleRequestFeature(SegmentLayer segment)
		{
			RequestFeatureSegment featureSeg = new RequestFeatureSegment(segment);
			SegmentLayer sendSegment = null;
			string packet = null;

			_log.DebugFormat("HandleRequestFeature for fleet {0}, vehicle {1}, feature:{2}, featureId:{3}", featureSeg.Fleet, featureSeg.Vehicle, featureSeg.Feature, featureSeg.FeatureId);

			DataSet requests = null;
			switch (featureSeg.Feature)
			{
				case FeatureType.AccidentBuffer:
					requests = ExecuteSQL("exec usp_GetUnSetAccidentBufferRequests");
					if (requests.Tables.Count > 0)
					{
						DataRow[] rows = requests.Tables[0].Select(string.Format("FleetId = {0} and VehicleId = {1}", featureSeg.Fleet, featureSeg.Vehicle));
						foreach (DataRow row in rows)
						{
							SendAccidentBufferRequest(row);
						}
					}

					break;
				case FeatureType.ExtendedHistory:
					//not implemented yet
					_log.ErrorFormat("Received ExtendedHistory for fleet {0}, vehicle {1}", featureSeg.Fleet, featureSeg.Vehicle);
					requests = ExecuteSQL("exec usp_GetExtendedHistoryDataForListenerInterface");
					if (requests.Tables.Count > 0)
					{
						DataRow[] rows = requests.Tables[0].Select(string.Format("FleetId = {0} and VehicleId = {1}", featureSeg.Fleet, featureSeg.Vehicle));
						foreach (DataRow row in rows)
						{
							SendExtendedHistoryRequest(row);
						}
					}
					break;
				case FeatureType.ReportOnDemand:
					sendSegment = new RequestReportOnDemandSegment();
					packet = "Report On Demand";
					break;
				case FeatureType.DvrRequest:
					_log.DebugFormat("Received DvrRequest for fleet {0}, vehicle {1}", featureSeg.Fleet, featureSeg.Vehicle);
					requests = ExecuteSQL("exec usp_GetDvrRequestDataForListenerInterface");
					if (requests.Tables.Count > 0)
					{
						DataRow[] rows = requests.Tables[0].Select(string.Format("FleetId = {0} and VehicleId = {1}", featureSeg.Fleet, featureSeg.Vehicle));
						foreach (DataRow row in rows)
						{
							SendDvrRequestRequest(row);
						}
					}
					break;
				case FeatureType.DvrConfig:
					_log.DebugFormat("Received DvrConfig update for fleet {0}, vehicle {1}, featureId {2}", featureSeg.Fleet, featureSeg.Vehicle, featureSeg.FeatureId);

					SendDvrConfigRequest(featureSeg.Fleet, featureSeg.Vehicle, featureSeg.FeatureId);

					break;
				default:
					_log.ErrorFormat("Received unkown request feature {0} for fleet {1}, vehicle {2}", featureSeg.Feature, featureSeg.Fleet, featureSeg.Vehicle);
					break;
			}

			if (sendSegment != null)
			{
				Tuple<int, int> key = new Tuple<int, int>(featureSeg.Fleet, featureSeg.Vehicle);
				lock (_syncObject)
				{
					if (_unitDeviceLayer.ContainsKey(key))
					{
						_log.InfoFormat("Sending {0} to unit {1}, {2}", packet, featureSeg.Fleet, featureSeg.Vehicle);
						SendToUnit(sendSegment, _unitDeviceLayer[key].Key);
					}
					else
					{
						_log.InfoFormat("unknown unit {1}, {2} - can't send packet {0}", packet, featureSeg.Fleet, featureSeg.Vehicle);
					}
				}
			}
		}
        #endregion

	    /// <summary>
	    /// The cached hardware configuration segment summary class.
	    /// </summary>
	    private class CachedHardwareConfigSegmentSummary
	    {
	        /// <summary>
	        /// Initializes a new instance of the <see cref="CachedHardwareConfigSegmentSummary"/> class.
	        /// </summary>
	        /// <param name="segmentVersion">The segmentVersion.</param>
	        /// <param name="configId">The configId.</param>
	        /// <param name="configVersion">The configVersion.</param>
	        public CachedHardwareConfigSegmentSummary(int segmentVersion, int configId, int configVersion)
	        {
	            SegmentVersion = segmentVersion;
	            ConfigId = configId;
	            ConfigVersion = configVersion;
	        }

	        /// <summary>
	        /// Gets the segment version.
	        /// </summary>
	        public int SegmentVersion { get; }

	        /// <summary>
	        /// Gets the hardware configuration id.
	        /// </summary>
	        public int ConfigId { get; }

	        /// <summary>
	        /// Gets the hardware configuration version.
	        /// </summary>
	        public int ConfigVersion { get; }
	    }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Net.Sockets;
using log4net;

namespace MTData.Transport.Service.Listener.GatewayService
{
    public static class Util
    {
        private static ILog log = LogManager.GetLogger(typeof(GatewayService));

        private static UdpClient interfaceUdpClient;

        public static void SendLogisticsJobInterfaceMessage(byte[] data)
        {
            try
            {
                string interfaceServer = ConfigurationManager.AppSettings["LogisticsJobInterfaceIP"];
                int interfaceServerPort = Convert.ToInt32(ConfigurationManager.AppSettings["LogisticsJobInterfacePort"]);

                if (interfaceUdpClient == null)
                {
                    interfaceUdpClient = new UdpClient();
                }

                interfaceUdpClient.Send(data, data.Length, interfaceServer, interfaceServerPort);
            }
            catch (Exception ex)
            {
                interfaceUdpClient = null;
                log.Error("SendLogisticsJobInterfaceMessage - error sending message", ex);
            }
        }

        /// <summary>
        /// Send the messages to the listener interface.
        /// </summary>
        /// <param name="data">The message data to be sent.</param>
        public static void SendListenerInterfaceMessage(byte[] data)
        {
            try
            {
                string listenerAddress = ConfigurationManager.AppSettings["ListenerInterfaceIP"];
                int listenerPort = int.Parse(ConfigurationManager.AppSettings["ListenerInterfacePort"]);

                UdpClient udpClient = new UdpClient();
                    udpClient.Send(data, data.Length, listenerAddress, listenerPort);
            }
            catch (Exception ex)
            {
                log.Error("SendListenerInterfaceMessage - error sending message", ex);
            }
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ServiceModel;

namespace MTData.Transport.Service.Listener.GatewayService
{
    public interface IGatewayServiceCallback
    {
        [OperationContract(IsOneWay = true)]
        void ListenerMessage(MTData.Transport.Gateway.Packet.GatewayProtocolPacket packet);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using log4net;
using MTData.Transport.Application.Communication;
using MTData.Transport.GenericProtocol.LogisticsLiveUpdates;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;
using MTData.Transport.GenericProtocol.Listener.InternalRequests;

namespace MTData.Transport.Service.Listener.GatewayService
{
    // NOTE: In order to launch WCF Test Client for testing this service, please select Service1.svc or Service1.svc.cs at the Solution Explorer and start debugging.
    public class GatewayService : IGatewayServiceDefinition
    {
        private const int TRACKING_API_APP_VERSION_MAJOR = 8;
        private const int TRACKING_API_APP_VERSION_MINOR = 0;
        private const int TRACKING_API_APP_VERSION_BRANCH = 0;
        private const int TRACKING_API_APP_VERSION_BUILD = 0;

        private static ILog _log = LogManager.GetLogger(typeof(GatewayService));
        private Tracking.Service1 _tws;

        public GatewayService()
        {
            _tws = new Tracking.Service1();
            _tws.Url = System.Configuration.ConfigurationManager.AppSettings["TransportWebServiceURL"];
        }

        int IGatewayServiceDefinition.AppVersion()
        {
            // SS For now it is enough to return the major version due to some issues with returning the entire app version, 
            // so for now returns the major but in the future can return the full app version if required
            return TRACKING_API_APP_VERSION_MAJOR;
        }

        string IGatewayServiceDefinition.Sanity(string input)
        {
            return input;
        }

        int IGatewayServiceDefinition.CheckLogin(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            return WS.CheckUsernamePassword(appCredentials.Username, appCredentials.Password);
        }

        protected Tracking.Service1 WS
        {
            get
            {
                return _tws;
            }
        }

        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.ServerBinding IGatewayServiceDefinition.GetBindingDetails(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetBinding();
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> IGatewayServiceDefinition.GetQueuedMesages(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedMessages(0, 0, DateTime.MinValue, DateTime.MinValue, 0, false);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<GatewayServiceBL.Classes.QueuedMessage> IGatewayServiceDefinition.GetQueuedMesagesFiltered(int fleetID, int vehicleID, DateTime startDate, DateTime endDate, int statusID, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedMessages(fleetID, vehicleID, startDate, endDate, statusID, false);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> IGatewayServiceDefinition.GetMessageTypes(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetMessageTypes();
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.DownloadType> IGatewayServiceDefinition.GetDownloadTypes(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetDownloadTypes();
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> IGatewayServiceDefinition.GetFleetVehicles(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetFleetVehicles(appCredentials.UserID, false);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> IGatewayServiceDefinition.GetFleetVehiclesWithAttachments(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetFleetVehicles(appCredentials.UserID, true);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        int IGatewayServiceDefinition.SendMessageToQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage message, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.SendMessageToQueue(message, appCredentials);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        int IGatewayServiceDefinition.SendDownloadToQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload download, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.SendDownloadToQueue(download, appCredentials);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        int IGatewayServiceDefinition.AbortMessageInQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage message, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.AbortMessageInQueue(message, appCredentials);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        int IGatewayServiceDefinition.AbortDownloadInQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload download, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.AbortDownloadInQueue(download, appCredentials);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<ListenerLogItem> IGatewayServiceDefinition.GetListenerLog(DateTime lastChecked, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetListenerLogs(lastChecked);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<ListenerLogItem> IGatewayServiceDefinition.GetListenerLogForVehicle(int fleetId, int vehicleId, DateTime lastChecked, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetListenerLogs(fleetId, vehicleId, lastChecked);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> IGatewayServiceDefinition.GetQueuedDownloads(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedDownloads(false, true);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<GatewayServiceBL.Classes.QueuedDownload> IGatewayServiceDefinition.GetQueuedDownloadsFiltered(bool includeCompleted, bool isService, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                try
                {
                    return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedDownloads(includeCompleted, isService);
                }
                catch (Exception ex)
                {
                    _log.Error("Get Queued Downloads Filtered", ex);
                    return new List<GatewayServiceBL.Classes.QueuedDownload>();
                }
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<GatewayServiceBL.Classes.QueuedDownload> IGatewayServiceDefinition.GetQueuedDownloadsByCriteria(int fleetID, int vehicleID, DateTime startDate, DateTime endDate,
            bool includeCompleted, int statusID, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetCurrentQueuedDownloads(fleetID, vehicleID, startDate, endDate, includeCompleted, statusID);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        List<GatewayServiceBL.Classes.DownloadFileImage> IGatewayServiceDefinition.GetFileImagesForDownloadType(int downloadType, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                List<GatewayServiceBL.Classes.DownloadFileImage> tmpList = MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetFileImagesForDownloadType(downloadType);

                return tmpList;
            }
            else
            {
                throw this.CredentialException();
            }
        }

        /// <summary>
        /// Get the list of download definitions
        /// </summary>
        /// <param name="updateTypes">Type of updates to filter by</param>
        /// <param name="showInactive">Whether to include inactive definitions</param>
        /// <param name="fleetId">Id of the fleet (set to -1 for all fleets)</param>
        /// <param name="appCredentials">User credentials</param>
        /// <returns>The list of download definitions</returns>
        List<DownloadsDefinition> IGatewayServiceDefinition.GetDownloadsDefinition(List<DownloadsUpdateType> updateTypes, bool showInactive, int fleetId, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                List<DownloadsDefinition> tmpList = new List<DownloadsDefinition>();

                foreach (DownloadsUpdateType updateType in updateTypes)
                {
                    tmpList.AddRange(Manager.GetDownloadsDefinition(updateType, showInactive, fleetId));
                }

                return tmpList;
            }
            else
            {
                throw CredentialException();
            }
        }

        /// <summary>
        /// Assign a download to a vehicle
        /// </summary>
        /// <param name="fleetId">Id of the fleet</param>
        /// <param name="vehicleIds">Id of the vehicles</param>
        /// <param name="definitionIds">List of definition ids to assign</param>
        /// <param name="appCredentials">User credentials</param>
        /// <returns>Whether the assignment was successful</returns>
        bool IGatewayServiceDefinition.DownloadsAssignToVehicles(int fleetId, int[] vehicleIds, int[] definitionIds, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                bool result = Manager.DownloadsAssignToVehicle(fleetId, vehicleIds, definitionIds, true);

                if (result)
                {
                    // Send UDP message to logistics jobs interface to notify of terminal assignment change
                    TerminalUpdateSegment message = new TerminalUpdateSegment
                    {
                        FleetId = fleetId,
                        VehicleIds = vehicleIds.ToList()
                    };

                    Util.SendLogisticsJobInterfaceMessage(message.GetBytes());
                }

                return result;
            }
            else
            {
                throw CredentialException();
            }
        }

        /// <summary>
        /// Get all Hardware Configuration for a serial number
        /// </summary>
        /// <param name="serialNo">serial Number</param>
        /// <param name="appCredentials">user credentials</param>
        /// <returns>returns all Hardware Configuration for a serial number</returns>
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.HardwareConfig> IGatewayServiceDefinition.GetHardwareConfigsBySerialNo(long serialNo, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetHardwareConfigs((int)HardwareConfigType.HardwareConfig, (int)HardwareConfigItems.ShowAll, serialNo, false);
            }
            else
            {
                throw this.CredentialException();
            }
        }

        /// <summary>
        /// Get the detail of a specific config id
        /// </summary>
        /// <param name="configId">config Id</param>
        /// <param name="appCredentials">user credentials</param>
        /// <returns>returns the detail of a specific config id</returns>
        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.HardwareConfig IGatewayServiceDefinition.GetHardwareConfigById(int configId, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.HardwareConfig hardwareConfigItem = null;
                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.HardwareConfig> items = MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetHardwareConfigs((int)HardwareConfigType.HardwareConfig, configId);
                if (items.Count > 0)
                {
                    hardwareConfigItem = items[0];
                }

                return hardwareConfigItem;
            }
            else
            {
                throw this.CredentialException();
            }
        }

        /// <summary>
        /// Get the detail of a specific configuration Id from vehicle hardware configuration
        /// </summary>
        /// <param name="configId">Id of the configuration</param>
        /// <param name="appCredentials">user credentials</param>
        /// <returns>Returns the detail of a specific vehicle hardware configuration Id</returns>
        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.VehicleHardwareConfig IGatewayServiceDefinition.GetVehicleHardwareConfigById(int configId, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.VehicleHardwareConfig vehicleHardwareConfigItems = MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Manager.GetVehicleHardwareConfigDetails(configId);

                return vehicleHardwareConfigItems;
            }
            else
            {
                throw this.CredentialException();
            }
        }

        /// <summary>
        /// Get the hardware types
        /// </summary>
        /// <param name="appCredentials">User credentials</param>
        /// <returns>The list of hardware types</returns>
        List<HardwareType> IGatewayServiceDefinition.GetHardwareTypes(GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return Manager.GetHardwareTypes();
            }
            else
            {
                throw this.CredentialException();
            }
        }

        /// <summary>
        /// Assign a hardware config to a vehicle
        /// </summary>
        /// <param name="fleetId">Id of the fleet</param>
        /// <param name="vehicleIds">Id of the vehicles</param>
        /// <param name="configId">Config id to assign</param>
        /// <param name="appCredentials">User credentials</param>
        /// <returns>Whether the assignment was successful</returns>
        bool IGatewayServiceDefinition.AssignHardwareConfigToVehicles(int fleetId, int[] vehicleIds, int configId, GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                bool result = false;

                if (vehicleIds.Any())
                {
                    // Default the result to true and will set to false if any assignment fails
                    result = true;

                    foreach (int vehicleId in vehicleIds)
                    {
                        if (!Manager.AssignHardwareConfigToVehicle(fleetId, vehicleId, configId, appCredentials.UserID))
                        {
                            result = false;
                        }
                    }

                    if (result)
                    {
                        UpdateHardwareConfigSegment segment = new UpdateHardwareConfigSegment
                        {
                            FleetId = fleetId,
                            VehicleIds = vehicleIds.ToList()
                        };

                        Util.SendListenerInterfaceMessage(segment.GetBytes());
                    }
                }

                return result;
            }
            else
            {
                throw CredentialException();
            }
        }

        /// <summary>
        /// Get the asset hardware types
        /// </summary>
        /// <param name="appCredentials">User credentials</param>
        /// <returns>The list of asset hardware types</returns>
        List<HardwareType> IGatewayServiceDefinition.GetAssetHardwareTypes(GatewayServiceBL.Credentials appCredentials)
        {
            if (WS.GetDataSetSecurityRolesForUser(this.ConvertCredentials(appCredentials)).T_UserMenuItem.Select("userId = " + appCredentials.UserID).Length != 0)
            {
                return Manager.GetAssetHardwareTypes();
            }
            else
            {
                throw this.CredentialException();
            }
        }

        private Tracking.AppCredentials ConvertCredentials(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials)
        {
            Tracking.AppCredentials tmpCreds = new Tracking.AppCredentials();
            tmpCreds.UserID = appCredentials.UserID;
            tmpCreds.Username = appCredentials.Username;
            tmpCreds.Password = appCredentials.Password;

            var version = typeof(GatewayService).Assembly.GetName().Version;
            tmpCreds.AppVersion = new Tracking.AppVersion()
            {
                Major = version.Major,
                Minor = version.Minor,
                Build = version.Build,
                Branch = version.Revision
            };
            return tmpCreds;
        }

        private Exception CredentialException()
        {
            return new Exception("User details are not valid for this function");
        }
    }

}

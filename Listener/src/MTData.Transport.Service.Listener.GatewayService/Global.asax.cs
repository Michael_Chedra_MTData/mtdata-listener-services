﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.SessionState;
using log4net;
using log4net.Config;
using MTData.Transport.Service.Listener.GatewayServiceBL;

namespace MTData.Transport.Service.Listener.GatewayService
{
    public class Global : System.Web.HttpApplication
    {
        private static ILog _log = LogManager.GetLogger(typeof(Global));

        protected void Application_Start(object sender, EventArgs e)
        {
            // start log4net
            XmlConfigurator.Configure();
            _log.Info("Application_Start");

            // load the Listener messages from the message queue
            CurrentLogItems.Instance.Intialise();
        }

        protected void Session_Start(object sender, EventArgs e)
        {
        }

        protected void Application_BeginRequest(object sender, EventArgs e)
        {
        }

        protected void Application_AuthenticateRequest(object sender, EventArgs e)
        {
        }

        protected void Application_Error(object sender, EventArgs e)
        {
        }

        protected void Session_End(object sender, EventArgs e)
        {
        }

        protected void Application_End(object sender, EventArgs e)
        {
            // dispose the current log items
            CurrentLogItems.Instance.Dispose();

            _log.Info("Application_End");
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using MTData.Transport.Application.Communication;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;

namespace MTData.Transport.Service.Listener.GatewayService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IGatewayServiceDefinition
    {
        [OperationContract]
        string Sanity(string input);

        [OperationContract]
        int AppVersion();

        [OperationContract]
        int CheckLogin(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        MTData.Transport.Service.Listener.GatewayServiceBL.Classes.ServerBinding GetBindingDetails(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> GetMessageTypes(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.DownloadType> GetDownloadTypes(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.DownloadFileImage> GetFileImagesForDownloadType(int downloadType, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> GetQueuedMesages(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> GetQueuedMesagesFiltered(int fleetID, int vehicleID, DateTime startDate, 
            DateTime endDate, int statusID, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> GetQueuedDownloads(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> GetQueuedDownloadsFiltered(bool includeCompleted, bool isService, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<GatewayServiceBL.Classes.QueuedDownload> GetQueuedDownloadsByCriteria(int fleetID, int vehicleID, DateTime startDate, DateTime endDate,
            bool includeCompleted, int statusID, GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> GetFleetVehicles(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> GetFleetVehiclesWithAttachments(MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        int SendMessageToQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage message, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        int SendDownloadToQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload download, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        int AbortMessageInQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage message, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        int AbortDownloadInQueue(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload download, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<ListenerLogItem> GetListenerLog(DateTime lastChecked, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<ListenerLogItem> GetListenerLogForVehicle(int fleetId, int vehicleId, DateTime lastChecked, MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<DownloadsDefinition> GetDownloadsDefinition(List<DownloadsUpdateType> updateTypes, bool showInactive, int fleetId, GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        bool DownloadsAssignToVehicles(int fleetId, int[] vehicleIds, int[] definitionIds, GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<HardwareConfig> GetHardwareConfigsBySerialNo(long serialNumber, GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        HardwareConfig GetHardwareConfigById(int configId, GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        VehicleHardwareConfig GetVehicleHardwareConfigById(int configId, GatewayServiceBL.Credentials appCredentials);


        [OperationContract]
        List<HardwareType> GetHardwareTypes(GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        bool AssignHardwareConfigToVehicles(int fleetId, int[] vehicleIds, int configId, GatewayServiceBL.Credentials appCredentials);

        [OperationContract]
        List<HardwareType> GetAssetHardwareTypes(GatewayServiceBL.Credentials appCredentials);
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public class QueuedDownload
    {
        #region Private Fields
        private DateTime startDate;
        private DateTime expiryDate;
        private int abortTimeout;
        private bool continueWithoutWaitingForLogin;
        private string SMTPServerEmailAddress;
        private MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle vehicle;
        private MTData.Transport.Service.Listener.GatewayServiceBL.Classes.DownloadFileImage type;
        #endregion

        #region Constructor
        public QueuedDownload(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle vehicle, MTData.Transport.Service.Listener.GatewayServiceBL.Classes.DownloadFileImage fileImage, 
            DateTime startDate, DateTime expiryDate, int abortTimeout, bool continueNoLogin, string smtpEmailAddress)
        {
            this.vehicle = vehicle;
            this.type = fileImage;
            this.startDate = startDate;
            this.expiryDate = expiryDate;
            this.abortTimeout = abortTimeout;
            this.continueWithoutWaitingForLogin = continueNoLogin;
            this.SMTPServerEmailAddress = smtpEmailAddress;
        }
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        public MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle Vehicle
        {
            get { return this.vehicle; }
        }

        public MTData.Transport.Service.Listener.GatewayServiceBL.Classes.DownloadFileImage Type
        {
            get { return this.type; }
        }

        public DateTime StartDate
        {
            get { return this.startDate; }
        }

        public DateTime ExpiryDate
        {
            get { return this.expiryDate; }
        }

        public int AbortTimeout
        {
            get { return this.abortTimeout; }
        }

        public bool ContinueWithoutRelogin
        {
            get { return this.continueWithoutWaitingForLogin; }
        }

        public string SMTPEmailAddress
        {
            get { return this.SMTPServerEmailAddress; }
        }
        #endregion

        #region Methods

        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion

    }
}

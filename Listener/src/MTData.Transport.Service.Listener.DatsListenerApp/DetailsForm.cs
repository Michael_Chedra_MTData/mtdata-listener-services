using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MTData.Transport.Listener.DatsListenerApp
{
	/// <summary>
	/// Summary description for DetailsForm.
	/// </summary>
	public class DetailsForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.RichTextBox mRichText;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DetailsForm()
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
		}

		public void SetContents(string[] contentsList)
		{
			mRichText.Clear();
			foreach (string item in contentsList)
			{
				mRichText.AppendText(item + "\n");
			}
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.mRichText = new System.Windows.Forms.RichTextBox();
			this.SuspendLayout();
			// 
			// mRichText
			// 
			this.mRichText.Dock = System.Windows.Forms.DockStyle.Fill;
			this.mRichText.Name = "mRichText";
			this.mRichText.ReadOnly = true;
			this.mRichText.Size = new System.Drawing.Size(292, 261);
			this.mRichText.TabIndex = 0;
			this.mRichText.Text = "";
			// 
			// DetailsForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 261);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.mRichText});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "DetailsForm";
			this.Text = "Packet Details";
			this.ResumeLayout(false);

		}
		#endregion
	}
}

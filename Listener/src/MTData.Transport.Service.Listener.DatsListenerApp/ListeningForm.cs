//#region Transport Solution things
// Define this for the Transport Solution, which has the following 
// additional features:
// - Download to external I/O J-Bus unit
// - Additional packet types
// - Different Database design
#define TRANSPORT_SOLUTION
//#endregion

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Data;
using System.Text;
using Microsoft.Win32;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Gateway.Packet;
using System.IO;
using log4net;
using MTData.Transport.GatewayListener;


namespace MTData.Transport.Listener.DatsListenerApp
{
    /// <summary>
    /// Summary description for Form1.
    /// </summary>
    public class ListeningForm : System.Windows.Forms.Form, MTData.Transport.Service.Listener.GatewayListener.GatewayGUITrafficInterface, MTData.Transport.Service.Listener.GatewayListener.GatewayGUIInterface
    {
        private const string sClassName = "DATSListenerApp.ListeningForm.";
        private static ILog _log = LogManager.GetLogger(typeof(ListeningForm));
        #region Variables
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox mTextRxPacket;
        public System.Windows.Forms.StatusBar mStatusBar;
        private System.Windows.Forms.TextBox mTextLength;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox mCbMsgType;
        private System.Windows.Forms.TextBox mTextDataLength;
        private System.Windows.Forms.NumericUpDown mSbOurSeq;
        private System.Windows.Forms.NumericUpDown mSbAckSeq;
        private System.Windows.Forms.TextBox mTextPayload;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox mTextMsgName;
        private System.Windows.Forms.NumericUpDown mSpinPacketNum;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox mTextFleetId;
        private System.Windows.Forms.TextBox mTextVehicleId;
        private System.Windows.Forms.Button mButtonDetails;
        private DetailsForm mFormDetails;
        private System.Windows.Forms.Label mLabelTotalCount;
        private System.Windows.Forms.Button mButtonReset;
        private System.Windows.Forms.Button mButtonSendAlarm;
        private System.Windows.Forms.GroupBox groupBox4;
        private MTData.Transport.GatewayListener.OrderedTextBox mActivityList;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnClearStatus;
        private System.Windows.Forms.Button btnFlush;
        private System.Windows.Forms.Button btnQueueCommands;
        private System.Windows.Forms.Button btnListClients;
        private System.Windows.Forms.Button btnListUnits;
        private NetworkInfoSetupForm mNetworkForm;
        private System.Windows.Forms.CheckBox chkShowOutbound;

        private System.Windows.Forms.CheckBox chkResetFleet;
        private System.Windows.Forms.CheckBox chkRespondTo;
        private System.Windows.Forms.NumericUpDown spinRespondFleet;

        private System.Windows.Forms.CheckBox chkShowUnit;
        private System.Windows.Forms.NumericUpDown spinShowFleet;
        private System.Windows.Forms.NumericUpDown spinShowUnit;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.CheckBox chkLogErrorMessages;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtAFDState;
        private System.Windows.Forms.NumericUpDown spinAFDFleet;
        private System.Windows.Forms.CheckBox cbAFDEnabled;
        private System.Windows.Forms.Button btnAFDViewProgress;
        private System.Windows.Forms.CheckBox chkRevertToPrimary;
        private System.Windows.Forms.Button button1;

        private delegate void UpdatePacketCountDelegate(int iNewNumber);
        private UpdatePacketCountDelegate upcd;
        private delegate void AutoFleetDownloadProcessingDelegate(GatewayProtocolPacket p);
        private AutoFleetDownloadProcessingDelegate afdpd;
        private frmMultipleDownload oMultiDownloadForm = null;
        private frmQueueCommands oQueueCommandsForm = null;
        private frmChangeUnitID oChangeVehicleIDForm = null;
        private frmMonitorUnits oMonitorUnitReportsForm = null;
        private object oFormUpdateSyncRoot = null;
        private string _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
        #endregion

        // Things which the App does differently from the Windows Service:

        #region Constructor/Destructor

        /// <summary>
        /// This is the listener that controls the Gateway functionality
        /// </summary>
        private MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator _listener = null;
        private System.Windows.Forms.Button btnMultiDownload;
        private System.Windows.Forms.Button btnChangeUnitID;
        private System.Windows.Forms.Button btnMonitorReports;
        private System.Windows.Forms.Button btnIgnoreReports;

        private GroupBox gbSettings;
        private TextBox txtBindPort;
        private TextBox txtBindIP;
        private Label label14;
        private TextBox txtLiveUpdatesOn;
        private Label label15;
        private TextBox txtDSN;
        private Label label16;
        private TextBox txtRemoteStateUpdatePort;
        private Label label17;
        private Button btnMaintainWatchList;

        public ListeningForm()
        {

            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
            try
            {
                oFormUpdateSyncRoot = new object();
                mActivityList.AddMessageError += new OrderedTextBox.AddMessageErrorDelegate(mActivityList_AddMessageError);
                upcd = new UpdatePacketCountDelegate(UpdatePacketCountLabel);
                afdpd = new AutoFleetDownloadProcessingDelegate(DoAFDProcessing);
#if TRANSPORT_SOLUTION
                _listener = new MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator(true, this, this);
#else
			_listener = new MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator(false, this, this);
#endif
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ListeningForm()", ex);
            }
        }

        #endregion

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.mTextRxPacket = new System.Windows.Forms.TextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.mLabelTotalCount = new System.Windows.Forms.Label();
            this.mSpinPacketNum = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.mTextLength = new System.Windows.Forms.TextBox();
            this.mStatusBar = new System.Windows.Forms.StatusBar();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.mTextVehicleId = new System.Windows.Forms.TextBox();
            this.mTextFleetId = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.mTextPayload = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.mTextMsgName = new System.Windows.Forms.TextBox();
            this.mSbAckSeq = new System.Windows.Forms.NumericUpDown();
            this.mSbOurSeq = new System.Windows.Forms.NumericUpDown();
            this.mTextDataLength = new System.Windows.Forms.TextBox();
            this.mCbMsgType = new System.Windows.Forms.ComboBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.mButtonDetails = new System.Windows.Forms.Button();
            this.spinRespondFleet = new System.Windows.Forms.NumericUpDown();
            this.chkRespondTo = new System.Windows.Forms.CheckBox();
            this.chkResetFleet = new System.Windows.Forms.CheckBox();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.btnChangeUnitID = new System.Windows.Forms.Button();
            this.btnFlush = new System.Windows.Forms.Button();
            this.mButtonSendAlarm = new System.Windows.Forms.Button();
            this.mButtonReset = new System.Windows.Forms.Button();
            this.btnQueueCommands = new System.Windows.Forms.Button();
            this.btnListUnits = new System.Windows.Forms.Button();
            this.btnListClients = new System.Windows.Forms.Button();
            this.btnClearStatus = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.label10 = new System.Windows.Forms.Label();
            this.spinShowUnit = new System.Windows.Forms.NumericUpDown();
            this.spinShowFleet = new System.Windows.Forms.NumericUpDown();
            this.chkShowUnit = new System.Windows.Forms.CheckBox();
            this.label11 = new System.Windows.Forms.Label();
            this.chkShowOutbound = new System.Windows.Forms.CheckBox();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.mActivityList = new MTData.Transport.GatewayListener.OrderedTextBox(_serverTime_DateFormat);
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.btnMonitorReports = new System.Windows.Forms.Button();
            this.btnIgnoreReports = new System.Windows.Forms.Button();
            this.chkLogErrorMessages = new System.Windows.Forms.CheckBox();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.chkRevertToPrimary = new System.Windows.Forms.CheckBox();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.btnMultiDownload = new System.Windows.Forms.Button();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.btnAFDViewProgress = new System.Windows.Forms.Button();
            this.txtAFDState = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.spinAFDFleet = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.cbAFDEnabled = new System.Windows.Forms.CheckBox();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.txtRemoteStateUpdatePort = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.txtLiveUpdatesOn = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.txtBindPort = new System.Windows.Forms.TextBox();
            this.txtBindIP = new System.Windows.Forms.TextBox();
            this.label14 = new System.Windows.Forms.Label();
            this.btnMaintainWatchList = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mSpinPacketNum)).BeginInit();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mSbAckSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.mSbOurSeq)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinRespondFleet)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.groupBox5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinShowUnit)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinShowFleet)).BeginInit();
            this.groupBox4.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinAFDFleet)).BeginInit();
            this.gbSettings.SuspendLayout();
            this.SuspendLayout();
            // 
            // mTextRxPacket
            // 
            this.mTextRxPacket.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextRxPacket.Location = new System.Drawing.Point(64, 56);
            this.mTextRxPacket.Name = "mTextRxPacket";
            this.mTextRxPacket.Size = new System.Drawing.Size(493, 20);
            this.mTextRxPacket.TabIndex = 0;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.mLabelTotalCount);
            this.groupBox1.Controls.Add(this.mSpinPacketNum);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.mTextRxPacket);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.mTextLength);
            this.groupBox1.Location = new System.Drawing.Point(9, 88);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(565, 96);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Received Packet";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(272, 24);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(128, 24);
            this.button1.TabIndex = 6;
            this.button1.Text = "Goto Latest Packet";
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // mLabelTotalCount
            // 
            this.mLabelTotalCount.Location = new System.Drawing.Point(104, 24);
            this.mLabelTotalCount.Name = "mLabelTotalCount";
            this.mLabelTotalCount.Size = new System.Drawing.Size(48, 24);
            this.mLabelTotalCount.TabIndex = 5;
            this.mLabelTotalCount.Text = "of 0";
            // 
            // mSpinPacketNum
            // 
            this.mSpinPacketNum.Location = new System.Drawing.Point(64, 24);
            this.mSpinPacketNum.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.mSpinPacketNum.Name = "mSpinPacketNum";
            this.mSpinPacketNum.Size = new System.Drawing.Size(40, 20);
            this.mSpinPacketNum.TabIndex = 4;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(8, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 23);
            this.label3.TabIndex = 3;
            this.label3.Text = "Packet #";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(8, 56);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(56, 23);
            this.label2.TabIndex = 2;
            this.label2.Text = "Raw Data";
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(152, 24);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(40, 23);
            this.label1.TabIndex = 1;
            this.label1.Text = "Length";
            // 
            // mTextLength
            // 
            this.mTextLength.Location = new System.Drawing.Point(200, 24);
            this.mTextLength.Name = "mTextLength";
            this.mTextLength.Size = new System.Drawing.Size(40, 20);
            this.mTextLength.TabIndex = 0;
            // 
            // mStatusBar
            // 
            this.mStatusBar.Location = new System.Drawing.Point(0, 488);
            this.mStatusBar.Name = "mStatusBar";
            this.mStatusBar.Size = new System.Drawing.Size(845, 22);
            this.mStatusBar.TabIndex = 2;
            // 
            // groupBox2
            // 
            this.groupBox2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox2.Controls.Add(this.mTextVehicleId);
            this.groupBox2.Controls.Add(this.mTextFleetId);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.mTextPayload);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.mTextMsgName);
            this.groupBox2.Controls.Add(this.mSbAckSeq);
            this.groupBox2.Controls.Add(this.mSbOurSeq);
            this.groupBox2.Controls.Add(this.mTextDataLength);
            this.groupBox2.Controls.Add(this.mCbMsgType);
            this.groupBox2.Controls.Add(this.label7);
            this.groupBox2.Controls.Add(this.label6);
            this.groupBox2.Controls.Add(this.label5);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.mButtonDetails);
            this.groupBox2.Location = new System.Drawing.Point(9, 192);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(565, 160);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Decoded Data";
            // 
            // mTextVehicleId
            // 
            this.mTextVehicleId.Location = new System.Drawing.Point(232, 56);
            this.mTextVehicleId.Name = "mTextVehicleId";
            this.mTextVehicleId.Size = new System.Drawing.Size(48, 20);
            this.mTextVehicleId.TabIndex = 13;
            // 
            // mTextFleetId
            // 
            this.mTextFleetId.Location = new System.Drawing.Point(192, 56);
            this.mTextFleetId.Name = "mTextFleetId";
            this.mTextFleetId.Size = new System.Drawing.Size(32, 20);
            this.mTextFleetId.TabIndex = 12;
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(144, 56);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(48, 16);
            this.label9.TabIndex = 11;
            this.label9.Text = "F/Un ID";
            // 
            // mTextPayload
            // 
            this.mTextPayload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextPayload.Location = new System.Drawing.Point(72, 120);
            this.mTextPayload.Name = "mTextPayload";
            this.mTextPayload.Size = new System.Drawing.Size(485, 20);
            this.mTextPayload.TabIndex = 9;
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(16, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(100, 23);
            this.label8.TabIndex = 10;
            this.label8.Text = "Payload";
            // 
            // mTextMsgName
            // 
            this.mTextMsgName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mTextMsgName.Location = new System.Drawing.Point(152, 24);
            this.mTextMsgName.Name = "mTextMsgName";
            this.mTextMsgName.Size = new System.Drawing.Size(405, 20);
            this.mTextMsgName.TabIndex = 8;
            // 
            // mSbAckSeq
            // 
            this.mSbAckSeq.Location = new System.Drawing.Point(184, 88);
            this.mSbAckSeq.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mSbAckSeq.Name = "mSbAckSeq";
            this.mSbAckSeq.ReadOnly = true;
            this.mSbAckSeq.Size = new System.Drawing.Size(40, 20);
            this.mSbAckSeq.TabIndex = 7;
            // 
            // mSbOurSeq
            // 
            this.mSbOurSeq.Location = new System.Drawing.Point(72, 88);
            this.mSbOurSeq.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.mSbOurSeq.Name = "mSbOurSeq";
            this.mSbOurSeq.ReadOnly = true;
            this.mSbOurSeq.Size = new System.Drawing.Size(40, 20);
            this.mSbOurSeq.TabIndex = 6;
            // 
            // mTextDataLength
            // 
            this.mTextDataLength.Location = new System.Drawing.Point(72, 56);
            this.mTextDataLength.Name = "mTextDataLength";
            this.mTextDataLength.Size = new System.Drawing.Size(48, 20);
            this.mTextDataLength.TabIndex = 5;
            // 
            // mCbMsgType
            // 
            this.mCbMsgType.Location = new System.Drawing.Point(72, 24);
            this.mCbMsgType.Name = "mCbMsgType";
            this.mCbMsgType.Size = new System.Drawing.Size(72, 21);
            this.mCbMsgType.TabIndex = 4;
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(136, 88);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(100, 23);
            this.label7.TabIndex = 3;
            this.label7.Text = "Ack Seq";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(16, 88);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(100, 23);
            this.label6.TabIndex = 2;
            this.label6.Text = "Our Seq";
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(16, 56);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 23);
            this.label5.TabIndex = 1;
            this.label5.Text = "Length";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(16, 24);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(100, 23);
            this.label4.TabIndex = 0;
            this.label4.Text = "Msg Type";
            // 
            // mButtonDetails
            // 
            this.mButtonDetails.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mButtonDetails.Location = new System.Drawing.Point(240, 88);
            this.mButtonDetails.Name = "mButtonDetails";
            this.mButtonDetails.Size = new System.Drawing.Size(317, 23);
            this.mButtonDetails.TabIndex = 6;
            this.mButtonDetails.Text = "Details...";
            this.mButtonDetails.Click += new System.EventHandler(this.mButtonDetails_Click);
            // 
            // spinRespondFleet
            // 
            this.spinRespondFleet.Enabled = false;
            this.spinRespondFleet.Location = new System.Drawing.Point(176, 162);
            this.spinRespondFleet.Name = "spinRespondFleet";
            this.spinRespondFleet.Size = new System.Drawing.Size(48, 20);
            this.spinRespondFleet.TabIndex = 31;
            // 
            // chkRespondTo
            // 
            this.chkRespondTo.Enabled = false;
            this.chkRespondTo.Location = new System.Drawing.Point(16, 154);
            this.chkRespondTo.Name = "chkRespondTo";
            this.chkRespondTo.Size = new System.Drawing.Size(160, 24);
            this.chkRespondTo.TabIndex = 30;
            this.chkRespondTo.Text = "Only Respond To Fleet";
            this.chkRespondTo.CheckedChanged += new System.EventHandler(this.chkRespondTo_CheckedChanged);
            // 
            // chkResetFleet
            // 
            this.chkResetFleet.Location = new System.Drawing.Point(16, 178);
            this.chkResetFleet.Name = "chkResetFleet";
            this.chkResetFleet.Size = new System.Drawing.Size(136, 24);
            this.chkResetFleet.TabIndex = 28;
            this.chkResetFleet.Text = "Reset on Flash";
            this.chkResetFleet.CheckedChanged += new System.EventHandler(this.chkResetFleet_CheckedChanged);
            // 
            // groupBox7
            // 
            this.groupBox7.Controls.Add(this.btnMaintainWatchList);
            this.groupBox7.Controls.Add(this.btnChangeUnitID);
            this.groupBox7.Controls.Add(this.btnFlush);
            this.groupBox7.Controls.Add(this.mButtonSendAlarm);
            this.groupBox7.Controls.Add(this.mButtonReset);
            this.groupBox7.Controls.Add(this.btnQueueCommands);
            this.groupBox7.Location = new System.Drawing.Point(8, 8);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(232, 138);
            this.groupBox7.TabIndex = 26;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Request";
            // 
            // btnChangeUnitID
            // 
            this.btnChangeUnitID.Location = new System.Drawing.Point(8, 80);
            this.btnChangeUnitID.Name = "btnChangeUnitID";
            this.btnChangeUnitID.Size = new System.Drawing.Size(208, 24);
            this.btnChangeUnitID.TabIndex = 25;
            this.btnChangeUnitID.Text = "Change Unit ID";
            this.btnChangeUnitID.Click += new System.EventHandler(this.btnChangeUnitID_Click);
            // 
            // btnFlush
            // 
            this.btnFlush.Enabled = false;
            this.btnFlush.Location = new System.Drawing.Point(80, 16);
            this.btnFlush.Name = "btnFlush";
            this.btnFlush.Size = new System.Drawing.Size(64, 24);
            this.btnFlush.TabIndex = 24;
            this.btnFlush.Text = "Flush";
            this.btnFlush.Click += new System.EventHandler(this.btnFlush_Click);
            // 
            // mButtonSendAlarm
            // 
            this.mButtonSendAlarm.Enabled = false;
            this.mButtonSendAlarm.Location = new System.Drawing.Point(8, 16);
            this.mButtonSendAlarm.Name = "mButtonSendAlarm";
            this.mButtonSendAlarm.Size = new System.Drawing.Size(64, 24);
            this.mButtonSendAlarm.TabIndex = 10;
            this.mButtonSendAlarm.Text = "Alarm";
            this.mButtonSendAlarm.Click += new System.EventHandler(this.mButtonSendAlarm_Click);
            // 
            // mButtonReset
            // 
            this.mButtonReset.Enabled = false;
            this.mButtonReset.Location = new System.Drawing.Point(152, 16);
            this.mButtonReset.Name = "mButtonReset";
            this.mButtonReset.Size = new System.Drawing.Size(64, 24);
            this.mButtonReset.TabIndex = 9;
            this.mButtonReset.Text = "Reset";
            this.mButtonReset.Click += new System.EventHandler(this.mButtonReset_Click);
            // 
            // btnQueueCommands
            // 
            this.btnQueueCommands.Location = new System.Drawing.Point(8, 48);
            this.btnQueueCommands.Name = "btnQueueCommands";
            this.btnQueueCommands.Size = new System.Drawing.Size(208, 24);
            this.btnQueueCommands.TabIndex = 24;
            this.btnQueueCommands.Text = "Queue Commands";
            this.btnQueueCommands.Click += new System.EventHandler(this.btnQueueCommands_Click);
            // 
            // btnListUnits
            // 
            this.btnListUnits.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListUnits.Location = new System.Drawing.Point(8, 40);
            this.btnListUnits.Name = "btnListUnits";
            this.btnListUnits.Size = new System.Drawing.Size(232, 24);
            this.btnListUnits.TabIndex = 23;
            this.btnListUnits.Text = "Units...";
            this.btnListUnits.Click += new System.EventHandler(this.btnListUnits_Click);
            // 
            // btnListClients
            // 
            this.btnListClients.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnListClients.Location = new System.Drawing.Point(8, 8);
            this.btnListClients.Name = "btnListClients";
            this.btnListClients.Size = new System.Drawing.Size(232, 24);
            this.btnListClients.TabIndex = 22;
            this.btnListClients.Text = "Clients...";
            this.btnListClients.Click += new System.EventHandler(this.btnListClients_Click);
            // 
            // btnClearStatus
            // 
            this.btnClearStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnClearStatus.Location = new System.Drawing.Point(8, 8);
            this.btnClearStatus.Name = "btnClearStatus";
            this.btnClearStatus.Size = new System.Drawing.Size(232, 24);
            this.btnClearStatus.TabIndex = 11;
            this.btnClearStatus.Text = "Clear";
            this.btnClearStatus.Click += new System.EventHandler(this.btnClearStatus_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox5.Controls.Add(this.label10);
            this.groupBox5.Controls.Add(this.spinShowUnit);
            this.groupBox5.Controls.Add(this.spinShowFleet);
            this.groupBox5.Controls.Add(this.chkShowUnit);
            this.groupBox5.Controls.Add(this.label11);
            this.groupBox5.Location = new System.Drawing.Point(8, 128);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(232, 96);
            this.groupBox5.TabIndex = 24;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Filtered View";
            this.groupBox5.Visible = false;
            // 
            // label10
            // 
            this.label10.Location = new System.Drawing.Point(32, 48);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(48, 16);
            this.label10.TabIndex = 25;
            this.label10.Text = "Fleet";
            // 
            // spinShowUnit
            // 
            this.spinShowUnit.Location = new System.Drawing.Point(80, 72);
            this.spinShowUnit.Maximum = new decimal(new int[] {
            10000,
            0,
            0,
            0});
            this.spinShowUnit.Name = "spinShowUnit";
            this.spinShowUnit.Size = new System.Drawing.Size(56, 20);
            this.spinShowUnit.TabIndex = 24;
            // 
            // spinShowFleet
            // 
            this.spinShowFleet.Location = new System.Drawing.Point(80, 48);
            this.spinShowFleet.Name = "spinShowFleet";
            this.spinShowFleet.Size = new System.Drawing.Size(56, 20);
            this.spinShowFleet.TabIndex = 23;
            // 
            // chkShowUnit
            // 
            this.chkShowUnit.Location = new System.Drawing.Point(16, 24);
            this.chkShowUnit.Name = "chkShowUnit";
            this.chkShowUnit.Size = new System.Drawing.Size(112, 16);
            this.chkShowUnit.TabIndex = 22;
            this.chkShowUnit.Text = "Enable Filter";
            this.chkShowUnit.CheckedChanged += new System.EventHandler(this.chkShowUnit_CheckedChanged);
            // 
            // label11
            // 
            this.label11.Location = new System.Drawing.Point(32, 72);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(40, 16);
            this.label11.TabIndex = 26;
            this.label11.Text = "Unit";
            // 
            // chkShowOutbound
            // 
            this.chkShowOutbound.AutoCheck = false;
            this.chkShowOutbound.Location = new System.Drawing.Point(8, 108);
            this.chkShowOutbound.Name = "chkShowOutbound";
            this.chkShowOutbound.Size = new System.Drawing.Size(112, 32);
            this.chkShowOutbound.TabIndex = 21;
            this.chkShowOutbound.Text = "Show Outbound";
            this.chkShowOutbound.Click += new System.EventHandler(this.chkShowOutbound_CheckedChanged);
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox4.Controls.Add(this.mActivityList);
            this.groupBox4.Location = new System.Drawing.Point(4, 358);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(829, 124);
            this.groupBox4.TabIndex = 9;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Activity Snapshot";
            // 
            // mActivityList
            // 
            this.mActivityList.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.mActivityList.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.mActivityList.CausesValidation = false;
            this.mActivityList.DetectUrls = false;
            this.mActivityList.Font = new System.Drawing.Font("Courier New", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.mActivityList.Location = new System.Drawing.Point(8, 16);
            this.mActivityList.MaxLength = 500;
            this.mActivityList.Name = "mActivityList";
            this.mActivityList.ReadOnly = true;
            this.mActivityList.ScrollBars = System.Windows.Forms.RichTextBoxScrollBars.Vertical;
            this.mActivityList.Size = new System.Drawing.Size(813, 96);
            this.mActivityList.TabIndex = 0;
            this.mActivityList.Text = "";
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Location = new System.Drawing.Point(581, 16);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(256, 336);
            this.tabControl1.TabIndex = 28;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnMonitorReports);
            this.tabPage1.Controls.Add(this.chkLogErrorMessages);
            this.tabPage1.Controls.Add(this.groupBox5);
            this.tabPage1.Controls.Add(this.btnClearStatus);
            this.tabPage1.Controls.Add(this.btnIgnoreReports);
            this.tabPage1.Controls.Add(this.chkShowOutbound);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(248, 310);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Snapshot";
            // 
            // btnMonitorReports
            // 
            this.btnMonitorReports.Location = new System.Drawing.Point(8, 40);
            this.btnMonitorReports.Name = "btnMonitorReports";
            this.btnMonitorReports.Size = new System.Drawing.Size(232, 24);
            this.btnMonitorReports.TabIndex = 26;
            this.btnMonitorReports.Text = "Monitor For Unit Reports";
            this.btnMonitorReports.Click += new System.EventHandler(this.btnMonitorReports_Click);
            // 
            // btnIgnoreReports
            // 
            this.btnIgnoreReports.Location = new System.Drawing.Point(8, 76);
            this.btnIgnoreReports.Name = "btnIgnoreReports";
            this.btnIgnoreReports.Size = new System.Drawing.Size(232, 24);
            this.btnIgnoreReports.TabIndex = 26;
            this.btnIgnoreReports.Text = "Ignore Reports From Units";
            this.btnIgnoreReports.Click += new System.EventHandler(this.btnIgnoreReports_Click);
            // 
            // chkLogErrorMessages
            // 
            this.chkLogErrorMessages.Location = new System.Drawing.Point(8, 144);
            this.chkLogErrorMessages.Name = "chkLogErrorMessages";
            this.chkLogErrorMessages.Size = new System.Drawing.Size(208, 16);
            this.chkLogErrorMessages.TabIndex = 25;
            this.chkLogErrorMessages.Text = "Log Status Bar Messages";
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.btnListClients);
            this.tabPage2.Controls.Add(this.btnListUnits);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Size = new System.Drawing.Size(248, 310);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Lists";
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.chkRevertToPrimary);
            this.tabPage3.Controls.Add(this.groupBox7);
            this.tabPage3.Controls.Add(this.chkResetFleet);
            this.tabPage3.Controls.Add(this.chkRespondTo);
            this.tabPage3.Controls.Add(this.spinRespondFleet);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(248, 310);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Actions";
            // 
            // chkRevertToPrimary
            // 
            this.chkRevertToPrimary.Location = new System.Drawing.Point(16, 202);
            this.chkRevertToPrimary.Name = "chkRevertToPrimary";
            this.chkRevertToPrimary.Size = new System.Drawing.Size(224, 24);
            this.chkRevertToPrimary.TabIndex = 32;
            this.chkRevertToPrimary.Text = "Revert Foreign Units To Primary Server";
            this.chkRevertToPrimary.CheckedChanged += new System.EventHandler(this.chkRevertToPrimary_CheckedChanged);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.btnMultiDownload);
            this.tabPage4.Controls.Add(this.groupBox3);
            this.tabPage4.Location = new System.Drawing.Point(4, 22);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Size = new System.Drawing.Size(248, 310);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Downloads";
            // 
            // btnMultiDownload
            // 
            this.btnMultiDownload.Location = new System.Drawing.Point(8, 16);
            this.btnMultiDownload.Name = "btnMultiDownload";
            this.btnMultiDownload.Size = new System.Drawing.Size(232, 24);
            this.btnMultiDownload.TabIndex = 31;
            this.btnMultiDownload.Text = "Download to Multiple Units";
            this.btnMultiDownload.Click += new System.EventHandler(this.btnMultiDownload_Click);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.btnAFDViewProgress);
            this.groupBox3.Controls.Add(this.txtAFDState);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.spinAFDFleet);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.cbAFDEnabled);
            this.groupBox3.Location = new System.Drawing.Point(224, 208);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(16, 16);
            this.groupBox3.TabIndex = 29;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Automated Fleet Download";
            this.groupBox3.Visible = false;
            // 
            // btnAFDViewProgress
            // 
            this.btnAFDViewProgress.Location = new System.Drawing.Point(8, 80);
            this.btnAFDViewProgress.Name = "btnAFDViewProgress";
            this.btnAFDViewProgress.Size = new System.Drawing.Size(216, 23);
            this.btnAFDViewProgress.TabIndex = 5;
            this.btnAFDViewProgress.Text = "View Fleet Progress";
            this.btnAFDViewProgress.Click += new System.EventHandler(this.btnAFDViewProgress_Click);
            // 
            // txtAFDState
            // 
            this.txtAFDState.Enabled = false;
            this.txtAFDState.Location = new System.Drawing.Point(56, 40);
            this.txtAFDState.Multiline = true;
            this.txtAFDState.Name = "txtAFDState";
            this.txtAFDState.Size = new System.Drawing.Size(168, 40);
            this.txtAFDState.TabIndex = 4;
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(8, 56);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(40, 23);
            this.label13.TabIndex = 3;
            this.label13.Text = "State:";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // spinAFDFleet
            // 
            this.spinAFDFleet.Location = new System.Drawing.Point(128, 16);
            this.spinAFDFleet.Name = "spinAFDFleet";
            this.spinAFDFleet.Size = new System.Drawing.Size(48, 20);
            this.spinAFDFleet.TabIndex = 2;
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(88, 16);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(40, 24);
            this.label12.TabIndex = 1;
            this.label12.Text = "Fleet:";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // cbAFDEnabled
            // 
            this.cbAFDEnabled.Location = new System.Drawing.Point(16, 16);
            this.cbAFDEnabled.Name = "cbAFDEnabled";
            this.cbAFDEnabled.Size = new System.Drawing.Size(72, 24);
            this.cbAFDEnabled.TabIndex = 0;
            this.cbAFDEnabled.Text = "Enabled";
            this.cbAFDEnabled.CheckedChanged += new System.EventHandler(this.cbAFDEnabled_CheckedChanged);
            // 
            // gbSettings
            // 
            this.gbSettings.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.gbSettings.Controls.Add(this.txtRemoteStateUpdatePort);
            this.gbSettings.Controls.Add(this.label17);
            this.gbSettings.Controls.Add(this.txtDSN);
            this.gbSettings.Controls.Add(this.label16);
            this.gbSettings.Controls.Add(this.txtLiveUpdatesOn);
            this.gbSettings.Controls.Add(this.label15);
            this.gbSettings.Controls.Add(this.txtBindPort);
            this.gbSettings.Controls.Add(this.txtBindIP);
            this.gbSettings.Controls.Add(this.label14);
            this.gbSettings.Location = new System.Drawing.Point(12, 8);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(562, 74);
            this.gbSettings.TabIndex = 29;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Settings";
            // 
            // txtRemoteStateUpdatePort
            // 
            this.txtRemoteStateUpdatePort.Enabled = false;
            this.txtRemoteStateUpdatePort.Location = new System.Drawing.Point(504, 15);
            this.txtRemoteStateUpdatePort.Name = "txtRemoteStateUpdatePort";
            this.txtRemoteStateUpdatePort.Size = new System.Drawing.Size(48, 20);
            this.txtRemoteStateUpdatePort.TabIndex = 21;
            this.txtRemoteStateUpdatePort.TabStop = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(368, 18);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(130, 13);
            this.label17.TabIndex = 20;
            this.label17.Text = "Remote State Updates on";
            // 
            // txtDSN
            // 
            this.txtDSN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDSN.Enabled = false;
            this.txtDSN.Location = new System.Drawing.Point(64, 41);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.Size = new System.Drawing.Size(490, 20);
            this.txtDSN.TabIndex = 19;
            this.txtDSN.TabStop = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(14, 44);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(30, 13);
            this.label16.TabIndex = 18;
            this.label16.Text = "DSN";
            // 
            // txtLiveUpdatesOn
            // 
            this.txtLiveUpdatesOn.Enabled = false;
            this.txtLiveUpdatesOn.Location = new System.Drawing.Point(311, 15);
            this.txtLiveUpdatesOn.Name = "txtLiveUpdatesOn";
            this.txtLiveUpdatesOn.Size = new System.Drawing.Size(48, 20);
            this.txtLiveUpdatesOn.TabIndex = 17;
            this.txtLiveUpdatesOn.TabStop = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(220, 18);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(85, 13);
            this.label15.TabIndex = 16;
            this.label15.Text = "Live Updates on";
            // 
            // txtBindPort
            // 
            this.txtBindPort.Enabled = false;
            this.txtBindPort.Location = new System.Drawing.Point(164, 15);
            this.txtBindPort.Name = "txtBindPort";
            this.txtBindPort.Size = new System.Drawing.Size(48, 20);
            this.txtBindPort.TabIndex = 15;
            this.txtBindPort.TabStop = false;
            // 
            // txtBindIP
            // 
            this.txtBindIP.Enabled = false;
            this.txtBindIP.Location = new System.Drawing.Point(64, 15);
            this.txtBindIP.Name = "txtBindIP";
            this.txtBindIP.Size = new System.Drawing.Size(94, 20);
            this.txtBindIP.TabIndex = 14;
            this.txtBindIP.TabStop = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(14, 18);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(44, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "Bind To";
            // 
            // btnMaintainWatchList
            // 
            this.btnMaintainWatchList.Location = new System.Drawing.Point(8, 110);
            this.btnMaintainWatchList.Name = "btnMaintainWatchList";
            this.btnMaintainWatchList.Size = new System.Drawing.Size(208, 24);
            this.btnMaintainWatchList.TabIndex = 26;
            this.btnMaintainWatchList.Text = "Maintain Watch List";
            this.btnMaintainWatchList.Click += new System.EventHandler(this.btnMaintainWatchList_Click);
            // 
            // ListeningForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(845, 510);
            this.Controls.Add(this.gbSettings);
            this.Controls.Add(this.groupBox4);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.mStatusBar);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tabControl1);
            this.Name = "ListeningForm";
            this.Text = "MTData DATS Listener";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Closed += new System.EventHandler(this.ListeningForm_Closed);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.ListeningForm_Closing);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mSpinPacketNum)).EndInit();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.mSbAckSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.mSbOurSeq)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinRespondFleet)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.groupBox5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.spinShowUnit)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.spinShowFleet)).EndInit();
            this.groupBox4.ResumeLayout(false);
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tabPage4.ResumeLayout(false);
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.spinAFDFleet)).EndInit();
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            this.ResumeLayout(false);

        }
        #endregion

        #region Windows Event Handlers
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]		
		static void Main() 
		{
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new ListeningForm());
		}

        private void Form1_Load(object sender, System.EventArgs e)
        {
            try
            {
                _serverTime_DateFormat = System.Configuration.ConfigurationManager.AppSettings["ServerTime_DateFormat"];
                txtBindIP.Text = System.Configuration.ConfigurationManager.AppSettings["BindAddress"];
                txtBindPort.Text = System.Configuration.ConfigurationManager.AppSettings["ConnectPort"];
                txtLiveUpdatesOn.Text = System.Configuration.ConfigurationManager.AppSettings["ClientNotifyPort"];
                txtDSN.Text = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                txtRemoteStateUpdatePort.Text = System.Configuration.ConfigurationManager.AppSettings["RemoteStateUpdatePort"];
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Form_Load()", ex);
                _serverTime_DateFormat = "dd/MM/yyyy HH:mm:ss";
            }
            mTextRxPacket.Text = "<No packet received yet>";
            _log.Info("Confirming Pre-Requisites");
            if (!GatewayAdministrator.ConfirmPreRequisites())
            {
                _log.Info("Confirming Pre-Requisites - Pre-Requisites Unavailable");
                MessageBox.Show(this, "An Error Occurred during initialisation\r\nThe Prerequisites for the service to run were not avialable\r\nPlease contact Support");
                this.Text += " - Inactive due to Initialisation error";
                this.Close();
            }
            else
            {
                string error = _listener.Start();
                if (error != null)
                {
                    MessageBox.Show(this, "An Error Occurred during initialisation\r\n" + error + "\r\nPlease contact Support");
                    this.Text += " - Inactive due to Initialisation error";
                    _log.Info("An Error Occurred during initialisation\r\n" + error + "\r\nPlease contact Support");
                    this.Close();
                }
                else
                {
                    // Attach the handler for the spin box
                    mSpinPacketNum.ValueChanged += new EventHandler(this.mSpinPacketNum_ValueChanged);
                }
            }
        }

        private void ListeningForm_Closed(object sender, System.EventArgs e)
        {
            try
            {
                if (oSendVerify != null)
                {
                    oSendVerify.Close();
                    oSendVerify = null;
                }
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (mFormDetails != null) mFormDetails.Close();
            }
            catch (System.Exception)
            {
            }

            _listener.Stop();
            _listener.Dispose();
            try
            {
                oFormUpdateSyncRoot = null;
            }
            catch (System.Exception)
            {
            }
        }

        private void chkLogClientMessages_CheckedChanged(object sender, System.EventArgs e)
        {
            _listener.bLogExceptions = chkLogErrorMessages.Checked;

        }

        private void mSpinPacketNum_ValueChanged(object sender, System.EventArgs e)
        {
            try
            {
                lock (_listener.SyncRoot)
                {
                    // Switch the display to the desired packet
                    if (mSpinPacketNum.Value == 0)
                    {	// Show the first
                        _listener.mViewedPacket = 1;
                        mSpinPacketNum.Value = _listener.mViewedPacket;
                    }
                    else if (mSpinPacketNum.Value <= _listener.mRxedPacketCount)
                    {
                        _listener.mViewedPacket = System.Convert.ToInt16(mSpinPacketNum.Value);
                    }
                    else
                    {
                        // Show the last available:
                        _listener.mViewedPacket = _listener.mRxedPacketCount;
                        mSpinPacketNum.Value = _listener.mViewedPacket;

                    }
                    GatewayProtocolPacket packet = _listener.GetViewedPacket();
                    if ((packet != null) && (packet.IsPopulated()))
                    {
                        SafeDisplayPacketDetails(packet);
                    }
                    else
                    {
                        mStatusBar.Text = "Cannot decode packet " + _listener.mViewedPacket;
                    }
                }
            }
            catch (System.Exception)
            {
            }
        }

        private void mButtonDetails_Click(object sender, System.EventArgs e)
        {
            _listener.ShowDetails();

        }

        public void ShowPacketDetails(GatewayProtocolPacket aPacket)
        {
            mFormDetails = new DetailsForm();
            mFormDetails.Show();
            try
            {
                if (
                    aPacket.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_BEING_USED ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_LOGGED_IN ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_CONCETE_SENSOR_ALERT ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_G_FORCE_OUT_OF_CALIBRATION ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_G_FORCE_MISSING ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_SUSPEC_ENGINE_DATA ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_ENGINE_DATA_MISSING ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_ENGINE_DATA_RE_CONNECT ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_IGNITION_DISCONNECT ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_SUSPECT_GPS ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_G_FORCE_MISSING ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_G_FORCE_OUT_OF_CALIBRATION ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_CONCETE_SENSOR_ALERT ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_LOGGED_IN ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_BEING_USED ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_VEHICLE_GPS_SPEED_DIFF ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_ENGINE_DATA_RE_CONNECT ||
                    aPacket.cMsgType == GeneralGPPacket.ANGEL_GEAR_ALERT_START ||
                    aPacket.cMsgType == GeneralGPPacket.ANGEL_GEAR_ALERT_END ||
                    aPacket.cMsgType == GeneralGPPacket.ALRM_UNIT_POWERUP ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_ABOVE_ONSITE_SPEED ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_CONCRETE_AGE_MINS ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_CONCRETE_AGE_ROTATIONS ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_DRIVER_ERROR_1 ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_DRIVER_ERROR_2 ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_LEFT_LOADER ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_LOADING_STARTED ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_MIX_STARTED ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_NOT_MIXED ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_RPM_FORWARD ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_RPM_REVERSED ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_RPM_STOPPED ||
                    aPacket.cMsgType == GeneralGPPacket.BARREL_RPM_UP_TO_SPEED ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_FUEL_ECONOMY_ALERT ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_OVER_RPM ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_OVER_COOLANT_TEMP ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_OVER_OIL_TEMP ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_LOW ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_HIGH ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_G_FORCES_HIGH ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_G_FORCE_CALIBRATED_RPT ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_COOLANT_LEVEL_LOW ||
                    aPacket.cMsgType == GeneralGPPacket.ENGINE_ERROR_CODE ||
                    aPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD ||
                    aPacket.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD ||
                    aPacket.cMsgType == GeneralGPPacket.FATIGUE_REPORT_IGNON ||
                    aPacket.cMsgType == GeneralGPPacket.FATIGUE_REPORT_24 ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_STARTUP ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_FULL ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_OVERSPEED ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_EXCESSIVE_IDLE ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_EXCESSIVE_IDLE_END ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END ||
                    aPacket.cMsgType == GeneralGPPacket.GPS_HISTORY ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_IOONOFF ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_NO_GPS_ANT_OPEN ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_NO_GPS_ANT_SHORT ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_NOGPS ||
                    aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_1_START ||
                    aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_2_START ||
                    aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_3_START ||
                    aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_1_END ||
                    aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_2_END ||
                    aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_3_END ||
                    aPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_REPORT ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_TRAILER_HITCH ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_TRAILER_DEHITCH ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_POS_REPORT ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_DATA_USAGE_ALERT ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_IO_PULSES ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_ACCIDENT_NEW ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_RX_TX ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_RX_TX_SATTELITE ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_DATA_USAGE_ALERT_SATELLITE ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                    aPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS ||
                    aPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT ||
                    aPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT ||
                    aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST ||
                    aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM ||
                    aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED ||
                    aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER ||
                    aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_NOT_ACKD_BY_USER ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                    aPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                    aPacket.cMsgType == GeneralGPPacket.GEN_ZERO_BYTES_RECEIVED_RESTART ||
                    aPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT ||
                    aPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR ||
                    aPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY ||
                    aPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION ||
                    aPacket.cMsgType == GeneralGPPacket.GPS_SETTINGS ||
                    aPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC_LEVEL ||
                    aPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC
                    )
                {
                    GeneralGPPacket packet = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(packet.ToStringArray());
                }
                else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_PERFORMANCE_METRICS)
                {
                    GPDriverPerformanceMetrics packet = new GPDriverPerformanceMetrics(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_DATAACCUMLATOR_METRICS)
                {
                    GPDriverDataAccumulator packet = new GPDriverDataAccumulator(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (aPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION)
                {
                    MassDeclarationPacket massPacket = new MassDeclarationPacket(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { massPacket.ToDisplayString() });
                }
                else if (aPacket.cMsgType == DriverPointsRuleRequest.DRIVER_POINTS_RULE_REQUEST)
                {
                    DriverPointsRuleRequest packet = new DriverPointsRuleRequest(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (aPacket.cMsgType == DriverPointsRuleBreakPacket.DRIVER_POINTS_RULE_BROKEN)
                {
                    DriverPointsRuleBreakPacket packet = new DriverPointsRuleBreakPacket(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (aPacket.cMsgType == GenericPacket.GENERIC_PACKET)
                {
                    GenericPacket packet = new GenericPacket(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (aPacket.cMsgType == DriverPointsConfigError.DRIVER_POINTS_CONFIG_ERROR)
                {
                    DriverPointsConfigError packet = new DriverPointsConfigError(aPacket, _serverTime_DateFormat);
                    mFormDetails.SetContents(new String[] { packet.ToDisplayString() });
                }

            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ShowPacketDetails(GatewayProtocolPacket aPacket)", ex);
            }
        }

        private void mButtonReset_Click(object sender, System.EventArgs e)
        {
            _listener.ResetVehicle();
        }

        private void btnFlush_Click(object sender, System.EventArgs e)
        {
            _listener.SendFlush();
        }

        private void mButtonSendAlarm_Click(object sender, System.EventArgs e)
        {
            _listener.SendAlarm();
        }

        private void mButtonSaveSnapshot_Click(object sender, System.EventArgs e)
        {
            // Create a SaveFileDialog to request a path and file name to save to.
            SaveFileDialog saveFile1 = new SaveFileDialog();

            // Initialize the SaveFileDialog to specify the RTF extension for the file.
            saveFile1.DefaultExt = "*.rtf";
            saveFile1.Filter = "RTF Files|*.rtf";

            // Determine if the user selected a file name from the saveFileDialog.
            if (saveFile1.ShowDialog() == System.Windows.Forms.DialogResult.OK &&
                saveFile1.FileName.Length > 0)
            {
                mActivityList.SaveFile(saveFile1.FileName,
                    System.Windows.Forms.RichTextBoxStreamType.RichText);
                mStatusBar.Text = "Saved file to " + saveFile1.FileName;
            }
        }

        private void btnOutputOn_Click(object sender, System.EventArgs e)
        {
            _listener.OutputOn();
        }

        private void btnOutputOff_Click(object sender, System.EventArgs e)
        {
            _listener.OutputOff();
        }
        private void btnOutputPattern_Click(object sender, System.EventArgs e)
        {
            _listener.OutputPattern();
        }

        private void btnSendSchedule_Click(object sender, System.EventArgs e)
        {
            _listener.SendSchedule();

        }


        private void btnListClients_Click(object sender, System.EventArgs e)
        {
            mFormDetails = new DetailsForm();
            string[] clientList = _listener.mClientRegistry.ListClients();
            mFormDetails.Text = "List of Clients - " + clientList.Length + " connected";
            mFormDetails.Show();
            mFormDetails.SetContents(clientList);
        }

        private void btnGPSHistory_Click(object sender, System.EventArgs e)
        {
            _listener.SendGPSHistoryRequest();
        }

        private void btnClearStatus_Click(object sender, System.EventArgs e)
        {
            mActivityList.Clear();
            mStatusBar.Text = "";
        }

        private void btnListUnits_Click(object sender, System.EventArgs e)
        {
            mFormDetails = new DetailsForm();
            mFormDetails.Width = 500;
            mFormDetails.Height = 500;
            string[] unitList = _listener.mDBInterface.ListUnits();
            mFormDetails.Text = "List of Units - " + unitList.Length + " known active";
            mFormDetails.Show();

            mFormDetails.SetContents(unitList);
        }

        private void btnSendNetworkInfo_Click(object sender, System.EventArgs e)
        {
            GatewayProtocolPacket packet = _listener.GetViewedPacket();
            if (packet != null)
            {
                if (mNetworkForm == null)
                {
                    mNetworkForm = new NetworkInfoSetupForm(packet,
                        _listener.mDBInterface, _serverTime_DateFormat);
                }
                else
                {
                    mNetworkForm.SetNewSourcePacket(packet);
                }
                mNetworkForm.Show();
            }
            else
                MessageBox.Show(this, "Please select a recent packet for the Target Vehicle");
        }

        private void btnNewProgramImage_Click(object sender, System.EventArgs e)
        {
            GatewayProtocolPacket packet = _listener.GetViewedPacket();
            if (packet != null)
            {
                DownloadProcessor dp = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.TrackingDevice, _serverTime_DateFormat);
                //seconds for data retry packet timer

                try
                {
                    _listener.iTrackingDeviceDownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
                }
                catch (System.Exception)
                {
                    _listener.iTrackingDeviceDownloadRetryInterval = 2;
                }
                if (_listener.iTrackingDeviceDownloadRetryInterval < 2) _listener.iTrackingDeviceDownloadRetryInterval = 2;
                if (_listener.iTrackingDeviceDownloadRetryInterval > 20) _listener.iTrackingDeviceDownloadRetryInterval = 20;

                try
                {
                    _listener.iTrackingDeviceDownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                }
                catch (System.Exception)
                {
                    _listener.iTrackingDeviceDownloadRetryCount = 3;
                }
                if (_listener.iTrackingDeviceDownloadRetryCount < 3) _listener.iTrackingDeviceDownloadRetryCount = 3;
                if (_listener.iTrackingDeviceDownloadRetryCount > 20) _listener.iTrackingDeviceDownloadRetryCount = 20;

                // Download v6, 5 retries, n seconds between each retry
                dp.BeginDownload(6, _listener.iTrackingDeviceDownloadRetryCount, _listener.iTrackingDeviceDownloadRetryInterval);
            }
            else
                MessageBox.Show(this, "Please select a recent packet for the Target Vehicle");
        }

        private void btnNewIOBoxImage_Click(object sender, System.EventArgs e)
        {
            GatewayProtocolPacket packet = _listener.GetViewedPacket();
            if (packet != null)
            {
                DownloadProcessor dp = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.InterfaceBox, _serverTime_DateFormat);
                //seconds for data retry packet timer

                try
                {
                    _listener.iInterfaceBoxDownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["InterfaceBoxDownloadRetryInterval"]);
                }
                catch (System.Exception)
                {
                    _listener.iInterfaceBoxDownloadRetryInterval = 2;
                }
                if (_listener.iInterfaceBoxDownloadRetryInterval < 2) _listener.iInterfaceBoxDownloadRetryInterval = 2;
                if (_listener.iInterfaceBoxDownloadRetryInterval > 10) _listener.iInterfaceBoxDownloadRetryInterval = 10;

                try
                {
                    _listener.iInterfaceBoxDownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["InterfaceBoxDownloadRetryCount"]);
                }
                catch (System.Exception)
                {
                    _listener.iInterfaceBoxDownloadRetryCount = 3;
                }
                if (_listener.iInterfaceBoxDownloadRetryCount < 3) _listener.iInterfaceBoxDownloadRetryCount = 3;
                if (_listener.iInterfaceBoxDownloadRetryCount > 10) _listener.iInterfaceBoxDownloadRetryCount = 10;

                // Download v6, 5 retries, 5 seconds between each retry
                dp.BeginDownload(6, _listener.iInterfaceBoxDownloadRetryCount, _listener.iInterfaceBoxDownloadRetryInterval);
            }
            else
                MessageBox.Show(this, "Please select a recent packet for the Target Vehicle");
        }

        private void btnNewMDTImage_Click(object sender, System.EventArgs e)
        {
            GatewayProtocolPacket packet = _listener.GetViewedPacket();
            if (packet != null)
            {
                DownloadProcessor dp = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.MobileDataTerminal, _serverTime_DateFormat);
                //seconds for data retry packet timer

                try
                {
                    _listener.iDataTerminalDownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DataTerminalDownloadRetryInterval"]);
                }
                catch (System.Exception)
                {
                    _listener.iDataTerminalDownloadRetryInterval = 5;
                }

                if (_listener.iDataTerminalDownloadRetryInterval < 5) _listener.iDataTerminalDownloadRetryInterval = 5;
                if (_listener.iDataTerminalDownloadRetryInterval > 10) _listener.iDataTerminalDownloadRetryInterval = 10;

                try
                {
                    _listener.iDataTerminalDownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DataTerminalDownloadRetryCount"]);
                }
                catch (System.Exception)
                {
                    _listener.iDataTerminalDownloadRetryCount = 3;
                }
                if (_listener.iDataTerminalDownloadRetryCount < 3) _listener.iDataTerminalDownloadRetryCount = 3;
                if (_listener.iDataTerminalDownloadRetryCount > 10) _listener.iDataTerminalDownloadRetryCount = 10;

                // Download v6, 5 retries, 5 seconds between each retry
                dp.BeginDownload(6, _listener.iDataTerminalDownloadRetryCount, _listener.iDataTerminalDownloadRetryInterval);
            }
            else
                MessageBox.Show(this, "Please select a recent packet for the Target Vehicle");
        }

        private frmSendVerify oSendVerify = null;
        private void btnSendVerifyPacket_Click(object sender, System.EventArgs e)
        {
            GatewayProtocolPacket packet = _listener.GetViewedPacket();
            if (oSendVerify != null)
            {
                oSendVerify.Close();
                oSendVerify = null;
            }
            oSendVerify = new frmSendVerify(packet, _listener.mDBInterface, _serverTime_DateFormat);
            oSendVerify.Show();

        }

        private void chkResetFleet_CheckedChanged(object sender, System.EventArgs e)
        {

        }

        private void chkRespondTo_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkRespondTo.Checked)
            {
                _listener.mDBInterface.cFleetToRespondTo = (byte)spinRespondFleet.Value;
                _listener.mDBInterface.bRespondToFleets = true;
            }
            else
            {
                _listener.mDBInterface.bRespondToFleets = false;
            }
        }

        private void chkShowUnit_CheckedChanged(object sender, System.EventArgs e)
        {
            if (chkShowUnit.Checked)
            {
                _listener.mDBInterface.SetOutboundDisplayUnit(Convert.ToByte(spinShowFleet.Value),
                    Convert.ToUInt16(spinShowUnit.Value));
            }
            else
            {
                _listener.mDBInterface.SetOutboundDisplayUnit(0, 0);
            }

        }

        private void chkShowOutbound_CheckedChanged(object sender, System.EventArgs e)
        {
            if (!chkShowOutbound.Checked)
            {
                if (MessageBox.Show("Enabling outbound message display\n" +
                    "SEVERELY affects Listener performance\n" +
                    "and should only be enabled in critical\n" +
                    "situations and for short periods.\n\n" +
                    "Are you sure you want to enable this?",
                    "Confirm Outbound Display",
                    MessageBoxButtons.YesNo,
                    MessageBoxIcon.Warning,
                    MessageBoxDefaultButton.Button2) ==
                    DialogResult.Yes)
                {
                    chkShowOutbound.Checked = true;
                    _listener.mDBInterface.SetOutboundDisplayState(true);
                    return;
                }
            }
            _listener.mDBInterface.SetOutboundDisplayState(false);
            chkShowOutbound.Checked = false;
        }

        private void ListeningForm_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
        }

        private void cbAFDEnabled_CheckedChanged(object sender, System.EventArgs e)
        {

        }

        private void btnAFDViewProgress_Click(object sender, System.EventArgs e)
        {

        }

        private void chkRevertToPrimary_CheckedChanged(object sender, System.EventArgs e)
        {
            _listener.mDBInterface.bRevertUnitsToPrimary = chkRevertToPrimary.Checked;
        }

        #endregion

        #region Cross-Thread delegates

        public void UpdatePacketCountLabel(int iNewNumber)
        {
            // Update the packet count label:
            mLabelTotalCount.Text = " of " + iNewNumber;
        }

        public void UpdateAFDStatusText(string s)
        {
            txtAFDState.Text = s;
        }

        public void DoAFDProcessing(GatewayProtocolPacket p)
        {
            // This delegate is called by MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator.ProcessPacket() -> _guiInterface.UpdateAFDState(latestPacket);
            // This function is fired after the listener has done all it's processing of the packet.
        }
        #endregion

        private void button1_Click(object sender, System.EventArgs e)
        {
            try
            {
                mSpinPacketNum.Value = 300;
                mSpinPacketNum_ValueChanged(this, new System.EventArgs());
            }
            catch (System.Exception)
            {
            }
        }

        #region GUI Updater Interface Implementation

        private delegate void InternalTextDelegate(string newValue);

        private void SafeSetStatusText(string newValue)
        {
            mStatusBar.Text = newValue;
        }

        public void SetStatusText(string newValue)
        {
            mStatusBar.BeginInvoke(new InternalTextDelegate(SafeSetStatusText), new object[] { newValue });
        }

        private void SafeSetTitleText(string newValue)
        {
            this.Text = newValue;
        }

        public void SetTitleText(string newValue)
        {
            this.BeginInvoke(new InternalTextDelegate(SafeSetTitleText), new object[] { newValue });
        }

        public int GetActivityListMaxLength()
        {
            return mActivityList.MaxLength;
        }

        public void SetActivityListMaxLength(int newValue)
        {
            mActivityList.MaxLength = newValue;
        }

        public void SetIOBoxImageEnable(bool newValue)
        {
        }

        public void SetLogErrorMessagesChecked(bool newValue)
        {
            chkLogErrorMessages.Checked = newValue;
        }

        public void UpdatePacketCount(int newNumber)
        {
            mLabelTotalCount.BeginInvoke(upcd, new object[] { newNumber });
        }

        public void UpdateAFDState(GatewayProtocolPacket p)
        {
            // This event is fired by MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator.ProcessPacket() -> _guiInterface.UpdateAFDState(latestPacket);
        }

        private delegate void DisplayPacketDetailsDelegate(GatewayProtocolPacket aPacket);
        public void DisplayPacketDetails(GatewayProtocolPacket aPacket)
        {
            this.BeginInvoke(new DisplayPacketDetailsDelegate(SafeDisplayPacketDetails), new object[] { aPacket });
        }

        // Tell all clients who have registered an interest in action
        // from this fleet
        private void SafeDisplayPacketDetails(GatewayProtocolPacket aPacket)
        {
            Byte[] myBytes = aPacket.mRawBytes;
            // If we get here, we've read some stuff and
            // it is valid to display it
            mTextLength.Text = myBytes.Length.ToString();
            mTextRxPacket.Text = System.BitConverter.ToString(myBytes, 0);
            mCbMsgType.Text = "0x" + aPacket.cMsgType.ToString("X");
            mTextMsgName.Text = GatewayProtocolLookup.GetMessageType(aPacket.cMsgType);
            mTextDataLength.Text = aPacket.iLength.ToString();
            mTextFleetId.Text = aPacket.cFleetId.ToString();
            mTextVehicleId.Text = aPacket.iVehicleId.ToString();
            mSbOurSeq.Value = aPacket.cOurSequence;
            mSbAckSeq.Value = aPacket.cAckSequence;
            if (aPacket.iLength > 0)
            {
                mTextPayload.Text = System.BitConverter.ToString(aPacket.mDataField);
            }
        }

        private delegate void EnableSpinControlsDelegate();

        public void EnableSpinControls()
        {
            this.BeginInvoke(new EnableSpinControlsDelegate(SafeEnableSpinControls), null);
        }

        public void SafeEnableSpinControls()
        {
            mSpinPacketNum.Value = 1;
            mButtonSendAlarm.Enabled = true;
            btnFlush.Enabled = true;
            mButtonReset.Enabled = true;
        }

        public bool GetShowUnitChecked()
        {
            return chkShowUnit.Checked;
        }

        public ushort GetShowUnitValue()
        {
            return Convert.ToUInt16(spinShowUnit.Value);
        }

        public byte GetShowFleetValue()
        {
            return Convert.ToByte(spinShowFleet.Value);
        }

        public void AddInboundMessage(GatewayProtocolPacket aPacket)
        {
            lock (oFormUpdateSyncRoot)
            {
                if (oMultiDownloadForm != null || oQueueCommandsForm != null || oChangeVehicleIDForm != null)
                {
                    if (aPacket.cMsgType == (byte)0xC1 || aPacket.cMsgType == (byte)0xD0)
                    {
                        if (oMultiDownloadForm != null)
                        {
                            oMultiDownloadForm.AddPacketToCheckList(aPacket);
                        }
                        if (oQueueCommandsForm != null)
                        {
                            oQueueCommandsForm.AddPacketToCheckList(aPacket);
                        }
                        if (oChangeVehicleIDForm != null)
                        {
                            oChangeVehicleIDForm.AddPacketToCheckList(aPacket);
                        }
                    }
                }
                if (oMonitorUnitReportsForm != null)
                {
                    oMonitorUnitReportsForm.AddPacketToCheckList(aPacket);
                }
            }
            mActivityList.AddInboundMessage(aPacket);
        }

        public void AddOutboundMessage(GatewayProtocolPacket aPacket)
        {
            if (oMonitorUnitReportsForm != null)
            {
                oMonitorUnitReportsForm.AddOutboundPacketToCheckList(aPacket);
            }
            if (chkShowOutbound.Checked)
                mActivityList.AddOutboundMessage(aPacket);
        }

        #endregion

        private void btnMultiDownload_Click(object sender, System.EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                oMultiDownloadForm = new frmMultipleDownload(_listener, _serverTime_DateFormat);
                oMultiDownloadForm.Closed += new EventHandler(oMultiDownloadForm_Closed);
            }
            oMultiDownloadForm.Show();
        }

        private void btnQueueCommands_Click(object sender, System.EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                oQueueCommandsForm = new frmQueueCommands(_listener);
                oQueueCommandsForm.Closed += new EventHandler(oQueueCommandsForm_Closed);
            }
            oQueueCommandsForm.Show();
        }

        private void btnChangeUnitID_Click(object sender, System.EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                oChangeVehicleIDForm = new frmChangeUnitID(_listener);
                oChangeVehicleIDForm.Closed += new EventHandler(oChangeVehicleIDForm_Closed);
            }
            oChangeVehicleIDForm.Show();
        }

        private void btnMonitorReports_Click(object sender, EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                oMonitorUnitReportsForm = new frmMonitorUnits(_listener);
                oMonitorUnitReportsForm.Closed += new EventHandler(oMonitorUnitReportsForm_Closed);
            }
            _listener.mDBInterface.SetOutboundDisplayState(true);
            oMonitorUnitReportsForm.Show();
        }

        private frmIgnoreUnits _ignoreUnitsForm = null;

        private void btnIgnoreReports_Click(object sender, EventArgs e)
        {
            try
            {
                _listener.IgnoreListClearList();
                if (_ignoreUnitsForm != null)
                    _ignoreUnitsForm.Focus();
                else
                {
                    _ignoreUnitsForm = new frmIgnoreUnits(_listener);
                    _ignoreUnitsForm.FormClosed += new FormClosedEventHandler(_ignoreUnitsForm_FormClosed);
                    _ignoreUnitsForm.Show();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnIgnoreReports_Click(object sender, EventArgs e)", ex);
                MessageBox.Show(this, "An error has occured : \r\n\r\n" + sClassName + "btnIgnoreReports_Click(object sender, EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        void _ignoreUnitsForm_FormClosed(object sender, FormClosedEventArgs e)
        {
            try
            {
                _listener.IgnoreListClearList();
                _ignoreUnitsForm.Dispose();
                _ignoreUnitsForm = null;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "_ignoreUnitsForm_FormClosed(object sender, FormClosedEventArgs e)", ex);
            }
        }

        private void oMultiDownloadForm_Closed(object sender, EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                if (oMultiDownloadForm != null)
                    oMultiDownloadForm.Closed -= new EventHandler(oMultiDownloadForm_Closed);
                oMultiDownloadForm = null;
            }
        }

        private void oQueueCommandsForm_Closed(object sender, EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                if (oQueueCommandsForm != null)
                    oQueueCommandsForm.Closed -= new EventHandler(oQueueCommandsForm_Closed);
                oQueueCommandsForm = null;
            }
        }

        private void oChangeVehicleIDForm_Closed(object sender, EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                if (oChangeVehicleIDForm != null)
                    oChangeVehicleIDForm.Closed -= new EventHandler(oChangeVehicleIDForm_Closed);
                oChangeVehicleIDForm = null;
            }
        }

        private void oMonitorUnitReportsForm_Closed(object sender, EventArgs e)
        {
            lock (oFormUpdateSyncRoot)
            {
                if (oMonitorUnitReportsForm != null)
                    oMonitorUnitReportsForm.Closed -= new EventHandler(oMonitorUnitReportsForm_Closed);
                oMonitorUnitReportsForm = null;
            }
        }

        private void mActivityList_AddMessageError(OrderedTextBox textBox, Exception ex, GatewayProtocolPacket packet, string direction)
        {
            try
            {
                _log.Error(sClassName + "mActivityList_AddMessageError(OrderedTextBox textBox, Exception ex, GatewayProtocolPacket packet, string direction = '" + direction + "')", ex);
            }
            catch
            {
            }
        }

        private void btnMaintainWatchList_Click(object sender, EventArgs e)
        {
            MessageBox.Show("No Watch lists have been implemented.", "MTData");
        }
    }
}

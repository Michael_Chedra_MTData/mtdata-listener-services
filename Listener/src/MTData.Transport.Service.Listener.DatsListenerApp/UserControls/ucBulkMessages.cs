﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using log4net;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;

namespace MTData.Transport.Listener.DatsListenerApp.UserControls
{
    public partial class ucBulkMessages : UserControl
    {
        private List<KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>> fleetVehicles
            = new List<KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>>();

        private List<QueuedMessage> queuedMessages = new List<QueuedMessage>();

        private Timer messageQueueTimer;

        private int refreshCount = 60;

        private ILog log = LogManager.GetLogger(typeof(ucBulkMessages));

        public ucBulkMessages()
        {
            InitializeComponent();

            this.LoadListViewColumns();

            this.LoadFilterCombos();

            this.LoadMessageTypes();

            this.cmbQueueFilter1.SelectedIndex = 4;

            this.cmbQueuedStatus1.SelectedIndex = 0;

            this.LoadExistingQueuedMessages();

            this.LoadFleetVehicles();

            splitMain.SplitterDistance = splitMain.Width / 2;

            this.StartQueueTimer();

            dtpStartDate.MinDate = DateTime.Now;
            dtpStartDate.Value = DateTime.Now;

            dtpExpiryDate.MinDate = DateTime.Now;
            dtpExpiryDate.Value = DateTime.Now.AddDays(7);

            PositionDataBlocksNumericUpDown.Maximum = uint.MaxValue;
            SpeedDataBlockNumericUpDown.Maximum = uint.MaxValue;
            BlockDataBlockNumericUpDown.Maximum = uint.MaxValue;

        }

        private void StartQueueTimer()
        {
            messageQueueTimer = new Timer();

            messageQueueTimer.Tick += messageQueueTimer_Tick;

            messageQueueTimer.Interval = 1000;

            messageQueueTimer.Start();
        }

        void messageQueueTimer_Tick(object sender, EventArgs e)
        {
            messageQueueTimer.Stop();

            if (lblAutoRefresh.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { messageQueueTimer_Tick(sender, e); }));
            }
            else
            {
                if (refreshCount == 0)
                {
                    this.LoadExistingQueuedMessages();
                    refreshCount = 60;
                }
                else
                {
                    refreshCount--;
                    this.lblAutoRefresh.Text = "Auto Refresh in " + refreshCount.ToString();
                }
            }

            messageQueueTimer.Start();
        }

        private void LoadMessageTypes()
        {
            cmbCommand.DataSource = null;

            cmbCommand.DisplayMember = "Description";
            cmbCommand.ValueMember = "Id";

            try
            {
                List<CustomListItem> items = Helpers.ListenerGateway().GetMessageTypes(Helpers.UserCredentials);
                log.InfoFormat("GetMessageTypes called, returned {0} items", items.Count);

                cmbCommand.DataSource = items;

                if (cmbCommand.Items.Count > 0)
                {
                    try
                    {
                        cmbCommand.SelectedIndex = 4;
                    }
                    catch (Exception)
                    {
                        cmbCommand.SelectedIndex = 0;
                    }
                }
            }
            catch (Exception)
            { }
        }

        private void LoadFilterCombos()
        {
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> availFilters = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> availFilter2 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();

            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> queuedFilter1 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> queuedFilter2 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();

            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> statusFilter1 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> statusFilter2 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();


            MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem tmpItem
                = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "All");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Fleet");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(2, "Vehicle");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(3, "HW Version");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(4, "SW Version");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(5, "CDMASupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(6, "GPRS");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(7, "MDTSupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(8, "DetachMDTSupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(9, "PhoneKit");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(10, "RS232");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(11, "ConcreteSupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(12, "Refrigeration");
            availFilters.Add(tmpItem);

            availFilter2 = availFilters.ToList();

            cmbAvailFilter1.DisplayMember = "Description";
            cmbAvailFilter1.ValueMember = "Id";

            cmbAvailFilter1.DataSource = availFilters;

            cmbAvailFilter2.DisplayMember = "Description";
            cmbAvailFilter2.ValueMember = "Id";

            cmbAvailFilter2.DataSource = availFilter2;

            cmbAvailFilter1.SelectedIndex = 0;
            cmbAvailFilter2.SelectedIndex = 0;

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "No");
            cmbBoolFilter1.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Yes");
            cmbBoolFilter1.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "No");
            cmbBoolFilter2.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Yes");
            cmbBoolFilter2.Items.Add(tmpItem);

            cmbBoolFilter1.DisplayMember = "Description";
            cmbBoolFilter2.DisplayMember = "Description";

            cmbBoolFilter1.ValueMember = "Id";
            cmbBoolFilter2.ValueMember = "Id";

            cmbBoolFilter1.SelectedIndex = 0;
            cmbBoolFilter2.SelectedIndex = 0;

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "All");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Fleet");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(2, "Vehicle");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(3, "Type");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(4, "Status");
            queuedFilter1.Add(tmpItem);

            //tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(5, "Extra");
            //queuedFilter1.Add(tmpItem);

            //tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(6, "Start Date");
            //queuedFilter1.Add(tmpItem);

            //tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(7, "Expiry Date");
            //queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(5, "Owner");
            queuedFilter1.Add(tmpItem);

            queuedFilter2 = queuedFilter1.ToList();

            cmbQueueFilter1.DisplayMember = "Description";
            cmbQueueFilter1.ValueMember = "Id";

            cmbQueueFilter1.DataSource = queuedFilter1;

            cmbQueueFilter2.DisplayMember = "Description";
            cmbQueueFilter2.ValueMember = "Id";

            cmbQueueFilter2.DataSource = queuedFilter2;

            cmbQueueFilter1.SelectedIndex = 0;
            cmbQueueFilter2.SelectedIndex = 0;

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "Pending");
            statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "In Progress");
            statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(2, "Failed");
            statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(3, "Aborted");
            statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(4, "Complete");
            statusFilter1.Add(tmpItem);

            statusFilter2 = statusFilter1.ToList();

            cmbQueuedStatus1.DisplayMember = "Description";
            cmbQueuedStatus2.DisplayMember = "Description";

            cmbQueuedStatus1.ValueMember = "Id";
            cmbQueuedStatus2.ValueMember = "Id";

            cmbQueuedStatus1.DataSource = statusFilter1;
            cmbQueuedStatus2.DataSource = statusFilter2;
        }

        private void LoadListViewColumns()
        {
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("SerialNumber", "Serial Number"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("HW Version", "HW Version"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("SW Version", "SW Version"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("CDMASupport", "CDMA Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("GPRS", "GPRS"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("MDTSupport", "MDT Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("DetachMDTSupport", "Detach MDT Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("PhoneKit", "Phone Kit"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("RS232", "RS232"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("ConcreteSupport", "Concrete Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("Refrigeration", "Refrigeration Support"));

            foreach (ColumnHeader ch in lvAvailableUnits.Columns)
            {
                ch.Width = -2;
            }

            lvQueued.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            lvQueued.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            lvQueued.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            lvQueued.Columns.Add(Helpers.CreateHeader("SerialNumber", "Serial Number"));
            lvQueued.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            lvQueued.Columns.Add(Helpers.CreateHeader("Type", "Type"));
            lvQueued.Columns.Add(Helpers.CreateHeader("Extra", "Extra"));
            //lvQueued.Columns.Add(Helpers.CreateHeader("StartDate", "Start Date"));
            lvQueued.Columns.Add(Helpers.CreateHeader("ExpiryDate", "Expiry Date"));

            foreach (ColumnHeader ch in lvQueued.Columns)
            {
                ch.Width = -2;
            }

            lvExistingQueue.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Type", "Type"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Status", "Status"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Extra", "Extra"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("StartDate", "Start Date"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("ExpiryDate", "Expiry Date"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Owner", "Owner"));

            foreach (ColumnHeader ch in lvExistingQueue.Columns)
            {
                ch.Width = -2;
            }
        }

        private void LoadExistingQueuedMessages()
        {
            if (lvExistingQueue.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { LoadExistingQueuedMessages(); }));
            }
            else
            {
                lvExistingQueue.Items.Clear();

                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> messages = null;

                if (cmbQueueFilter1.SelectedIndex == 0 && cmbQueueFilter2.SelectedIndex == 0)
                {
                    messages = Helpers.ListenerGateway().GetQueuedMesagesFiltered(0, 0, DateTime.MinValue, DateTime.MinValue, -1, Helpers.UserCredentials);
                    log.InfoFormat("GetQueuedMesagesFiltered All called, returned {0} messages", messages.Count);
                }
                else
                {
                    try
                    {
                        messages = Helpers.ListenerGateway().GetQueuedMesagesFiltered(0, 0, DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)), DateTime.Now, -1, Helpers.UserCredentials);
                        log.InfoFormat("GetQueuedMesagesFiltered last 7 days called, returned {0} messages", messages.Count);
                    }
                    catch (Exception)
                    {
                        System.Windows.Forms.Application.DoEvents();
                        //messages = Helpers.ListenerGateway().GetQueuedMesagesFiltered(0, 0, DateTime.Now.Subtract(new TimeSpan(7, 0, 0, 0)), DateTime.Now, -1, Helpers.UserCredentials);
                    }
                }

                //if (filtered)
                //{
                try
                {
                    if (cmbQueueFilter1.SelectedIndex != 0 || cmbQueueFilter2.SelectedIndex != 0)
                    {
                        if (cmbQueueFilter1.SelectedIndex != 0)
                        {
                            this.FilterQueuedMessageListBy(ref messages,
                                ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueueFilter1.SelectedItem).ID,
                                txtQueueFilter1.Text, true,
                                ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueuedStatus1.SelectedItem).ID);
                        }

                        if (cmbQueueFilter2.SelectedIndex != 0)
                        {
                            this.FilterQueuedMessageListBy(ref messages,
                                ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueueFilter2.SelectedItem).ID,
                                txtQueueFilter2.Text, true,
                                ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueuedStatus2.SelectedItem).ID);
                        }
                    }
                }
                catch (Exception)
                { }
                //}

                foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage qm in messages)
                {
                    AddVehicleMessageToExistingList(qm);
                }

                foreach (ColumnHeader ch in lvExistingQueue.Columns)
                {
                    ch.Width = -2;
                }
            }
        }

        private void FilterQueuedMessageListBy(ref List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> messages, int filterType, string filterString, bool useEqual, int downloadStatus)
        {
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage> tmpMessages;

            switch (filterType)
            {
                case 2: //Vehicle
                    try
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh)
                        {
                            return useEqual ?
                                veh.VehicleId == int.Parse(filterString) || veh.VehicleDisplayName == filterString || veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower())
                                : veh.VehicleId != int.Parse(filterString) && veh.VehicleDisplayName != filterString && !veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower());
                        });
                    }
                    catch (Exception)
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh)
                        {
                            return useEqual ?
                                veh.VehicleDisplayName == filterString || veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower())
                                : veh.VehicleDisplayName != filterString && !veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower());
                        });
                    }
                    break;
                case 3: //Type
                    try
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh) { return useEqual ? veh.MessageTypeDescription.Contains(filterString) : !veh.MessageTypeDescription.Contains(filterString); });
                    }
                    catch (Exception)
                    {
                        // Catch the issue here an just return an empty list
                        // TODO work this code better
                        tmpMessages = new List<Service.Listener.GatewayServiceBL.Classes.QueuedMessage>();
                    }
                    break;
                case 4: //Status
                    try
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh) { return useEqual ? veh.StatusId == downloadStatus : veh.StatusId != downloadStatus; });
                    }
                    catch (Exception)
                    {
                        // Catch the issue here an just return an empty list
                        // TODO work this code better
                        tmpMessages = new List<Service.Listener.GatewayServiceBL.Classes.QueuedMessage>();
                    }
                    break;
                case 5: //Owner
                    try
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh) { return useEqual ? veh.CreatedUserName.Contains(filterString) : !veh.CreatedUserName.Contains(filterString); });
                    }
                    catch (Exception)
                    {
                        // Catch the issue here an just return an empty list
                        // TODO work this code better
                        tmpMessages = new List<Service.Listener.GatewayServiceBL.Classes.QueuedMessage>();
                    }
                    break;
                case 1: //Fleet
                default:
                    try
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh)
                        {
                            return useEqual ? veh.FleetId == int.Parse(filterString) || veh.FleetName == filterString
                                : veh.FleetId != int.Parse(filterString) && veh.FleetName != filterString;
                        });
                    }
                    catch (Exception)
                    {
                        tmpMessages = messages.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage veh)
                        {
                            return useEqual ? veh.FleetName == filterString || veh.FleetName.ToLower().Contains(filterString.ToLower())
                                : veh.FleetName != filterString && !veh.FleetName.ToLower().Contains(filterString.ToLower());
                        });
                    }
                    break;
            }

            messages = tmpMessages;
        }

        private void LoadFleetVehicles()
        {
            this.fleetVehicles = new List<KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>>();

            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> currentVehicles = Helpers.ListenerGateway().GetFleetVehicles(Helpers.UserCredentials);
            log.InfoFormat("Bulk - GetFleetVehicles called, returned {0} vehicle objects", currentVehicles.Count);

            foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in currentVehicles)
            {
                KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet;

                try
                {
                    fleet = this.FindFleetInList(veh.FleetID + "-" + veh.FleetName);

                }
                catch (IndexOutOfRangeException)
                {
                    fleet = new KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>(veh.FleetID + "-" + veh.FleetName, new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>());
                    this.fleetVehicles.Add(fleet);
                }

                ((List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value).Add(veh);
            }

            // Now we need to build a list of vehicles that are not filtered we will do this by 
            this.FilterList();

            this.EnableMoveButtons();
        }

        private void FilterList()
        {
            this.lvAvailableUnits.Items.Clear();

            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> availList = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>();

            if (cmbAvailFilter1.SelectedIndex == 0 && cmbAvailFilter2.SelectedIndex == 0)
            {
                // Show all

                foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet in this.fleetVehicles)
                {
                    foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in (List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value)
                    {
                        AddVehicleToAvailableList(veh);
                    }
                }
            }
            else
            {
                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> filteredList = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>();

                foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet in this.fleetVehicles)
                {
                    foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in (List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value)
                    {
                        filteredList.Add(veh);
                    }
                }


                if (cmbAvailFilter1.SelectedIndex != 0)
                {
                    this.FilterListBy(ref filteredList, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbAvailFilter1.SelectedItem).ID, txtFilter1.Text, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbBoolFilter1.SelectedItem).ID);
                }

                if (cmbAvailFilter2.SelectedIndex != 0)
                {
                    this.FilterListBy(ref filteredList, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbAvailFilter2.SelectedItem).ID, txtFilter2.Text, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbBoolFilter2.SelectedItem).ID);
                }


                foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in filteredList)
                {
                    AddVehicleToAvailableList(veh);
                }
            }
        }

        private void FilterListBy(ref List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> vehicles, int filterType, string filterString, int boolValue)
        {
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> tmpVehicles;

            switch (filterType)
            {
                case 2: //Vehicle
                    try
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.VehicleID == int.Parse(filterString) || veh.DisplayName.ToLower().Contains(filterString.ToLower()); });
                    }
                    catch (Exception)
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.DisplayName == filterString || veh.DisplayName.ToLower().Contains(filterString.ToLower()); });
                    }
                    break;
                case 3: //HW Version
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.HWVersion == filterString; });
                    break;
                case 4: //SW Version
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.SWVersion == filterString; });
                    break;
                case 5: //CDMASupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.CDMASupport == Convert.ToBoolean(boolValue); });
                    break;
                case 6: //GPRS
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.GPRS == Convert.ToBoolean(boolValue); });
                    break;
                case 7: //MDTSupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.MDTSupport == Convert.ToBoolean(boolValue); });
                    break;
                case 8: //DetachMDTSupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.DetatchMDTSupport == Convert.ToBoolean(boolValue); });
                    break;
                case 9: //PhoneKit
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.PhoneKit == Convert.ToBoolean(boolValue); });
                    break;
                case 10: //RS232
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.RS232 == Convert.ToBoolean(boolValue); });
                    break;
                case 11: //ConcreteSupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.ConcreteSupport == Convert.ToBoolean(boolValue); });
                    break;
                case 12: //Refrigeration
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.Refrigeration == Convert.ToBoolean(boolValue); });
                    break;
                case 1: //Fleet
                default:
                    try
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.FleetID == int.Parse(filterString) || veh.FleetName == filterString; });
                    }
                    catch (Exception)
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return veh.FleetName == filterString || veh.FleetName.ToLower().Contains(filterString.ToLower()); });
                    }
                    break;
            }

            vehicles = tmpVehicles;
        }

        private void AddVehicleToAvailableList(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh)
        {
            ListViewItem vehItem = new ListViewItem(veh.FleetID.ToString());

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.FleetName));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.VehicleID.ToString()));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.SerialNumber == null ? "Unavailable" : veh.SerialNumber));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.DisplayName));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.HWVersion));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.SWVersion));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.CDMASupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.GPRS ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.MDTSupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.DetatchMDTSupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.PhoneKit ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.RS232 ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.ConcreteSupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.Refrigeration ? "Yes" : "No"));

            vehItem.Tag = veh;

            lvAvailableUnits.Items.Add(vehItem);
        }

        private KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> FindFleetInList(string fleet)
        {
            foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleetItem in this.fleetVehicles)
            {
                if (fleetItem.Key == fleet)
                {
                    return fleetItem;
                }
            }

            throw new IndexOutOfRangeException();
        }

        private void cmbCommand_SelectedIndexChanged(object sender, EventArgs e)
        {
            MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem selItem = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem;

            switch ((ListenerMessageTypes)selItem.ID)
            {
                case ListenerMessageTypes.ChangeIPAddress:
                    gbNetworkInfo.Visible = true;
                    IapDataBlocksGroupBox.Visible = false;
                    break;
                case ListenerMessageTypes.IAPDataBlocks:
                    gbNetworkInfo.Visible = false;
                    IapDataBlocksGroupBox.Visible = true;
                    break;
                default:
                    gbNetworkInfo.Visible = false;
                    IapDataBlocksGroupBox.Visible = false;
                    break;
            }

            this.EnableMoveButtons();
        }

        private void btnRefershAvaliableList_Click(object sender, EventArgs e)
        {
            this.LoadFleetVehicles();
        }

        private void lvAvailableUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void EnableMoveButtons()
        {
            btnAddAll.Enabled = lvAvailableUnits.Items.Count > 0
                && ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem).ID == 1 ? this.ValidateIPDetails() : true;
            btnRemoveAll.Enabled = lvQueued.Items.Count > 0;
            btnAddSelected.Enabled = lvAvailableUnits.SelectedIndices.Count > 0
                && (((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem).ID == 1 ? this.ValidateIPDetails() : true);
            btnRemoveSelected.Enabled = lvQueued.SelectedIndices.Count > 0;

            btnAddToQueue.Enabled = this.queuedMessages.Count > 0;
        }

        private bool ValidateIPDetails()
        {
            bool ret = true;

            if (!chkUseHostNameLookup.Checked)
            {
                try
                {
                    System.Net.IPAddress.Parse(txtNewIP.Text);
                }
                catch (System.Exception)
                {
                    ret &= false;
                }
            }
            else
            {
                ret &= txtHostName.Text.Length > 0;
            }

            try
            {
                Convert.ToInt16(numNewIPPort.Value);
            }
            catch (Exception)
            {
                ret &= false;
            }

            if (!string.IsNullOrEmpty(txtSecIP.Text))
            {
                try
                {
                    System.Net.IPAddress.Parse(txtSecIP.Text);
                }
                catch (System.Exception)
                {
                    ret &= false;
                }

                try
                {
                    Convert.ToInt16(numSecIPPort.Value);
                }
                catch (Exception)
                {
                    ret &= false;
                }
            }

            if (!string.IsNullOrEmpty(txtPingIP.Text))
            {
                try
                {
                    System.Net.IPAddress.Parse(txtPingIP.Text);
                }
                catch (System.Exception)
                {
                    ret &= false;
                }

                try
                {
                    Convert.ToByte(numNewPingRetries.Value);
                }
                catch (Exception)
                {
                    ret &= false;
                }
            }

            return ret;
        }

        private void btnAddSelected_Click(object sender, EventArgs e)
        {
            if (this.queuedMessages.Count == 0 && this.lvQueued.Items.Count > 0)
            {
                this.lvQueued.Items.Clear();
            }

            foreach (ListViewItem item in lvAvailableUnits.SelectedItems)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle)item.Tag;

                QueuedMessage qm = queuedMessages.Find(delegate(QueuedMessage m)
                {
                    return m.Vehicle.VehicleID == veh.VehicleID && m.Vehicle.FleetID == veh.FleetID
                        && ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem).ID == m.Type.ID;
                });

                if (qm == null)
                {
                    string extraData = string.Empty;
                    if (gbNetworkInfo.Visible)
                    {
                        extraData = CreateNetworkSettings();
                    }
                    else if (IapDataBlocksGroupBox.Visible)
                    {
                        extraData = CreateIAPDataBlockSettings();
                    }
                    qm = new QueuedMessage(veh, (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem, 
                        dtpStartDate.Value, dtpExpiryDate.Value, extraData);
                    queuedMessages.Add(qm);
                    AddVehicleMessageToList(qm, System.Drawing.Color.White);
                }
            }

            foreach (ColumnHeader ch in lvQueued.Columns)
            {
                ch.Width = -2;
            }

            this.EnableMoveButtons();
        }

        private string CreateNetworkSettings()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            System.Xml.XmlElement extraData = doc.CreateElement("extraData");

            extraData.SetAttribute("hostName", this.chkUseHostNameLookup.Checked ? "True" : "False");

            extraData.SetAttribute("hostIP", this.chkUseHostNameLookup.Checked ? this.txtHostName.Text : this.txtNewIP.Text);

            extraData.SetAttribute("ipPort", this.numNewIPPort.Value.ToString());

            extraData.SetAttribute("secIPPort", this.numSecIPPort.Value.ToString());

            extraData.SetAttribute("secIP", this.txtSecIP.Text);

            extraData.SetAttribute("pingRetries", this.numNewPingRetries.Value.ToString());

            extraData.SetAttribute("pingIP", txtPingIP.Text);

            doc.AppendChild(extraData);

            return doc.OuterXml;
        }

        private string CreateIAPDataBlockSettings()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();
            System.Xml.XmlElement extraData = doc.CreateElement("extraData");
            extraData.SetAttribute("position", this.PositionDataBlocksNumericUpDown.Value.ToString());
            extraData.SetAttribute("speed", this.SpeedDataBlockNumericUpDown.Value.ToString());
            extraData.SetAttribute("block", this.BlockDataBlockNumericUpDown.Value.ToString());

            doc.AppendChild(extraData);
            return doc.OuterXml;
        }

        private void AddVehicleMessageToList(QueuedMessage message, System.Drawing.Color highlighColor)
        {
            ListViewItem messageItem = new ListViewItem(message.Vehicle.FleetID.ToString());

            messageItem.BackColor = highlighColor;

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.Vehicle.FleetName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.Vehicle.VehicleID.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.Vehicle.SerialNumber == null ? "Unavailable" : message.Vehicle.SerialNumber));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.Vehicle.DisplayName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.Type.Description));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.ExtraData));

            //messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.StartDate.ToUniversalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.ExpiryDate.ToString()));

            messageItem.Tag = message;

            lvQueued.Items.Add(messageItem);
        }

        private void AddVehicleMessageToExistingList(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage message)
        {
            ListViewItem messageItem = new ListViewItem(message.FleetId.ToString());

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.FleetName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.VehicleId.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.VehicleDisplayName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.MessageTypeDescription));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.StatusDescription));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.ExtraData));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.StartDate.ToLocalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.ExpiryDate.ToUniversalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.CreatedUserName));

            messageItem.Tag = message;

            lvExistingQueue.Items.Add(messageItem);
        }

        private void btnAddAll_Click(object sender, EventArgs e)
        {
            if (this.queuedMessages.Count == 0 && this.lvQueued.Items.Count > 0)
            {
                this.lvQueued.Items.Clear();
            }

            foreach (ListViewItem item in lvAvailableUnits.Items)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle)item.Tag;

                QueuedMessage qm = queuedMessages.Find(delegate(QueuedMessage m)
                {
                    return m.Vehicle.VehicleID == veh.VehicleID
                        && ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem).ID == m.Type.ID;
                });

                if (qm == null)
                {
                    qm = new QueuedMessage(veh, (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbCommand.SelectedItem, DateTime.Now, DateTime.Now.AddDays(7), "");
                    queuedMessages.Add(qm);
                    AddVehicleMessageToList(qm, System.Drawing.Color.White);
                }
            }

            foreach (ColumnHeader ch in lvQueued.Columns)
            {
                ch.Width = -2;
            }

            this.EnableMoveButtons();
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            queuedMessages.Clear();
            lvQueued.Items.Clear();

            this.EnableMoveButtons();
        }

        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvQueued.SelectedItems)
            {
                queuedMessages.Remove((QueuedMessage)item.Tag);
                lvQueued.Items.Remove(item);
            }

            this.EnableMoveButtons();
        }

        private void btnRefreshExistingQueue_Click(object sender, EventArgs e)
        {
            this.messageQueueTimer.Stop();

            this.refreshCount = 60;

            this.LoadExistingQueuedMessages();

            this.EnableAbortButton();

            this.messageQueueTimer.Start();
        }

        private void btnAddToQueue_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Send all messages to queue?", "Send Messages To Queue", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.messageQueueTimer.Stop();
                this.refreshCount = 60;
                MTData.Common.Threading.EnhancedThread uploadThread = new MTData.Common.Threading.EnhancedThread(new MTData.Common.Threading.EnhancedThread.EnhancedThreadStart(UploadMessagesToQueue), null);
                uploadThread.Start();
                this.messageQueueTimer.Start();
            }
        }

        private object UploadMessagesToQueue(MTData.Common.Threading.EnhancedThread sender, object data)
        {
            List<QueuedMessage> nonSend = new List<QueuedMessage>();

            foreach (QueuedMessage qm in this.queuedMessages)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage qMessage
                    = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage();

                qMessage.FleetId = qm.Vehicle.FleetID;
                qMessage.VehicleId = qm.Vehicle.VehicleID;
                qMessage.StartDate = qm.StartDate;
                qMessage.ExpiryDate = qm.ExpiryDate;
                qMessage.MessageTypeId = (ListenerMessageTypes)qm.Type.ID;
                qMessage.CreatedDate = DateTime.Now.ToUniversalTime();
                qMessage.ExtraData = this.BuildExtraData(qm);

                int response = Helpers.ListenerGateway().SendMessageToQueue(qMessage, Helpers.UserCredentials);
                log.Info("SendMessageToQueue called");

                if (response != 0)
                {
                    nonSend.Add(qm);
                }
            }

            // Now clear both the queued list and the lvQueued and then load the nonSend color coded
            this.queuedMessages.Clear();

            this.ClearQueuedList(nonSend);

            this.LoadExistingQueuedMessages();

            return null;
        }

        private void ClearQueuedList(List<QueuedMessage> nonSend)
        {
            if (this.lvQueued.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ClearQueuedList(nonSend); }));
            }
            else
            {
                this.lvQueued.Items.Clear();

                foreach (QueuedMessage qm in nonSend)
                {
                    AddVehicleMessageToList(qm, System.Drawing.Color.Orange);
                }

                this.LoadExistingQueuedMessages();

                this.EnableMoveButtons();
            }
        }

        private string BuildExtraData(QueuedMessage qm)
        {
            ListenerMessageTypes mt = (ListenerMessageTypes)qm.Type.ID;
            if (mt == ListenerMessageTypes.ChangeIPAddress || mt == ListenerMessageTypes.IAPDataBlocks)
            {
                return qm.ExtraData;
            }
            else
            {
                return string.Empty;
            }
        }

        private void btnAvailableFilter_Click(object sender, EventArgs e)
        {
            this.FilterList();

            this.EnableMoveButtons();
        }

        private void cmbAvailFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAvailFilter2.SelectedIndex == 0)
            {
                txtFilter2.Text = string.Empty;
            }

            txtFilter2.Visible = cmbAvailFilter2.SelectedIndex < 5;
            cmbBoolFilter2.Visible = cmbAvailFilter2.SelectedIndex >= 5;
            txtFilter2.Enabled = cmbAvailFilter2.SelectedIndex != 0;
        }

        private void cmbAvailFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAvailFilter1.SelectedIndex == 0)
            {
                txtFilter1.Text = string.Empty;
            }

            txtFilter1.Visible = cmbAvailFilter1.SelectedIndex < 5;
            cmbBoolFilter1.Visible = cmbAvailFilter1.SelectedIndex >= 5;
            txtFilter1.Enabled = cmbAvailFilter1.SelectedIndex != 0;
        }

        private void lvQueued_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void lvExistingQueue_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableAbortButton();
        }

        private void EnableAbortButton()
        {
            if (this.lvExistingQueue.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EnableAbortButton(); }));
            }
            else
            {

                btnAbortMessage.Enabled = lvExistingQueue.SelectedItems.Count > 0;
            }
        }

        private void btnAbortMessage_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Abort Messages?", "Abort", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.messageQueueTimer.Stop();
                MTData.Common.Threading.EnhancedThread abortThread = new MTData.Common.Threading.EnhancedThread(new MTData.Common.Threading.EnhancedThread.EnhancedThreadStart(AbortMessagesInQueue), null);
                abortThread.Start();
                this.refreshCount = 60;
                this.messageQueueTimer.Start();
            }
        }

        private object AbortMessagesInQueue(MTData.Common.Threading.EnhancedThread sender, object data)
        {
            if (lvExistingQueue.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { AbortMessagesInQueue(sender, data); }));
            }
            else
            {
                foreach (ListViewItem qm in lvExistingQueue.SelectedItems)
                {
                    MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage qMessage = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedMessage)qm.Tag;

                    log.Info("AbortMessageInQueue called");
                    if (Helpers.ListenerGateway().AbortMessageInQueue(qMessage, Helpers.UserCredentials) == 0)
                    {
                        lvExistingQueue.Items.Remove(qm);
                    }
                }

                this.EnableAbortButton();
            }

            return null;
        }

        private void lvAvailableUnits_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                foreach (ListViewItem item in lvAvailableUnits.Items)
                {
                    item.Selected = true;
                }
            }

            e.SuppressKeyPress = true;

            this.EnableMoveButtons();
        }

        private void lvExistingQueue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                foreach (ListViewItem item in lvExistingQueue.Items)
                {
                    item.Selected = true;
                }
            }

            e.SuppressKeyPress = true;

            this.EnableAbortButton();
        }

        private void btnQueueFilter_Click(object sender, EventArgs e)
        {
            this.messageQueueTimer.Stop();

            this.lblAutoRefresh.Text = "Click to start auto timer";

            this.refreshCount = 60;

            this.LoadExistingQueuedMessages();

            this.EnableAbortButton();
        }

        private void chkUseHostNameLookup_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                txtHostName.Visible = chkUseHostNameLookup.Checked;
                txtNewIP.Visible = !chkUseHostNameLookup.Checked;

                txtHostName.Text = chkUseHostNameLookup.Checked ? txtHostName.Text : string.Empty;

                this.EnableMoveButtons();
            }
            catch (System.Exception ex)
            {
                log.Error("ucBulkMessages.checkBox1_CheckedChanged(object sender, EventArgs e)", ex);
            }
        }

        private void splitMain_SplitterMoved(object sender, SplitterEventArgs e)
        {
            splitMain.Panel1MinSize = splitMain.Width / 2 < 850 ? 850 : splitMain.Width / 2;
        }

        private void txtHostName_TextChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtSecIP_TextChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtPingIP_TextChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void numNewIPPort_ValueChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void numSecIPPort_ValueChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void numNewPingRetries_ValueChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtHostName_Enter(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtHostName_Leave(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtSecIP_Leave(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtSecIP_Enter(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtPingIP_Leave(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtPingIP_Enter(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtNewIP_TextChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtNewIP_Leave(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void txtNewIP_Enter(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void cmbQueueFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cmbBoolQueuedFilter1.Visible = cmbQueueFilter1.SelectedIndex == 4;

            txtQueueFilter1.Visible = cmbQueueFilter1.SelectedIndex != 0 && cmbQueueFilter1.SelectedIndex != 4;

            cmbQueuedStatus1.Visible = cmbQueueFilter1.SelectedIndex == 4;

            if (cmbQueueFilter1.SelectedIndex == 0)
            {
                txtQueueFilter1.Text = string.Empty;
            }
        }

        private void cmbQueueFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            //cmbBoolQueuedFilter2.Visible = cmbQueueFilter2.SelectedIndex == 4;

            txtQueueFilter2.Visible = cmbQueueFilter2.SelectedIndex != 0 && cmbQueueFilter2.SelectedIndex != 4;

            cmbQueuedStatus2.Visible = cmbQueueFilter2.SelectedIndex == 4;

            if (cmbQueueFilter2.SelectedIndex == 0)
            {
                txtQueueFilter2.Text = string.Empty;
            }
        }

        private void txtFilter1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                btnAvailableFilter_Click(sender, null);
            }
        }
    }
}
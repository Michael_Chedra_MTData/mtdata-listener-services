﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Text;
using System.Windows.Forms;
using log4net;

namespace MTData.Transport.Listener.DatsListenerApp.UserControls
{
    public partial class ucDownloads : UserControl
    {
        private List<KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>> fleetVehicles
            = new List<KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>>();

        private List<QueuedDownload> queuedDownloads = new List<QueuedDownload>();

        private Timer messageQueueTimer;

        private int refreshCount = 60;

        private ILog log = LogManager.GetLogger(typeof(ucDownloads));

        public ucDownloads()
        {
            InitializeComponent();

            this.LoadListViewColumns();

            this.LoadFilterCombos();

            this.cmbQueueFilter1.SelectedIndex = 6;

            this.cmbQueuedStatus1.SelectedIndex = 0;


            this.LoadExistingQueuedDownloads();

            this.LoadFleetVehicles();

            this.LoadDownloadTypes();

            splitMain.SplitterDistance = splitMain.Width / 2;

            this.StartQueueTimer();

            dtpStartDate.MinDate = DateTime.Now;
            dtpStartDate.Value = DateTime.Now;

            dtpExpiryDate.MinDate = DateTime.Now;
            dtpExpiryDate.Value = DateTime.Now.AddDays(7);

            gbAvailableFilter.Enabled = false;
        }

        private void LoadDownloadTypes()
        {
            try
            {
                cmbDownloadType.DataSource = null;

                cmbDownloadType.DisplayMember = "Description";
                cmbDownloadType.ValueMember = "Id";

                cmbDownloadType.DataSource = Helpers.ListenerGateway().GetDownloadTypes(Helpers.UserCredentials);
                log.Info("GetDownloadTypes called");

                if (cmbDownloadType.Items.Count > 0)
                {
                    cmbDownloadType.SelectedIndex = 0;
                }
                else
                {
                    this.FilterList();
                }
            }
            catch (Exception ex)
            { }
        }

        private bool ValidateSMTPDetails()
        {
            string ipRegEx = @"\b(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b";
            string fqdnRegEx = @"(?=^.{4,255}$)(^((?!-)[a-zA-Z0-9-]{1,63}(?<!-)\.)+[a-zA-Z]{2,63}$)";
            string emailAddresRegEx = @"\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}\b";
            bool ret = true;

            Regex regExTest;

            if (txtEmailTo.Text.Length > 0)
            {
                regExTest = new Regex(emailAddresRegEx);

                ret &= regExTest.Match(txtEmailTo.Text).Success;
            }

            return ret;
        }

        private void FilterMessageListBy(ref List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> vehicles, int filterType, string filterString, int boolValue, bool useEqual)
        {
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> tmpVehicles;

            switch (filterType)
            {
                case 2: //Vehicle
                    try
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh)
                        {
                            return useEqual ?                                
                                veh.VehicleID == int.Parse(filterString) || veh.DisplayName == filterString || veh.DisplayName.ToLower().Contains(filterString.ToLower())
                                : veh.VehicleID != int.Parse(filterString) && veh.DisplayName != filterString && !veh.DisplayName.ToLower().Contains(filterString.ToLower());
                        });
                    }
                    catch (Exception)
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh)
                        {
                            return useEqual ?
                                veh.DisplayName == filterString || veh.DisplayName.ToLower().Contains(filterString.ToLower())
                                : veh.DisplayName != filterString && !veh.DisplayName.ToLower().Contains(filterString.ToLower()); 
                        });
                    }
                    break;
                case 3: //HW Version
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return useEqual ? veh.HWVersion == filterString : veh.HWVersion != filterString; });
                    break;
                case 4: //SW Version
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return useEqual ? veh.SWVersion == filterString : veh.SWVersion != filterString; });
                    break;
                case 5: //CDMASupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return useEqual ? veh.CDMASupport == Convert.ToBoolean(boolValue) : veh.CDMASupport != Convert.ToBoolean(boolValue); });
                    break;
                case 6: //GPRS
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return  useEqual ? veh.GPRS == Convert.ToBoolean(boolValue) : veh.GPRS != Convert.ToBoolean(boolValue); });
                    break;
                case 7: //MDTSupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return  useEqual ? veh.MDTSupport == Convert.ToBoolean(boolValue) : veh.MDTSupport != Convert.ToBoolean(boolValue); });
                    break;
                case 8: //DetachMDTSupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return  useEqual ? veh.DetatchMDTSupport == Convert.ToBoolean(boolValue) : veh.DetatchMDTSupport != Convert.ToBoolean(boolValue); });
                    break;
                case 9: //PhoneKit
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return  useEqual ? veh.PhoneKit == Convert.ToBoolean(boolValue) : veh.PhoneKit != Convert.ToBoolean(boolValue); });
                    break;
                case 10: //RS232
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return  useEqual ? veh.RS232 == Convert.ToBoolean(boolValue) : veh.RS232 != Convert.ToBoolean(boolValue); });
                    break;
                case 11: //ConcreteSupport
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return  useEqual ? veh.ConcreteSupport == Convert.ToBoolean(boolValue) : veh.ConcreteSupport != Convert.ToBoolean(boolValue); });
                    break;
                case 12: //Refrigeration
                    tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh) { return useEqual ? veh.Refrigeration == Convert.ToBoolean(boolValue) : veh.Refrigeration != Convert.ToBoolean(boolValue); });
                    break;
                case 1: //Fleet
                default:
                    try
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh)
                        {
                            return useEqual ? veh.FleetID == int.Parse(filterString) || veh.FleetName == filterString
                                : veh.FleetID != int.Parse(filterString) && veh.FleetName != filterString;
                        });
                    }
                    catch (Exception)
                    {
                        tmpVehicles = vehicles.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh)
                        {
                            return useEqual ? veh.FleetName == filterString || veh.FleetName.ToLower().Contains(filterString.ToLower())
                                : veh.FleetName != filterString && !veh.FleetName.ToLower().Contains(filterString.ToLower());
                        });
                    }
                    break;
            }

            vehicles = tmpVehicles;
        }

        private void FilterQueuedDownListBy(ref List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> downloads, int filterType, string filterString, int boolValue, bool useEqual, int downloadStatus)
        {
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> tmpDownloads;

            try
            {
                switch (filterType)
                {
                    case 2: //Vehicle
                        try
                        {
                            tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh)
                            {
                                return useEqual ?
                                    veh.VehicleId == int.Parse(filterString) || veh.VehicleDisplayName == filterString || veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower())
                                    : veh.VehicleId != int.Parse(filterString) && veh.VehicleDisplayName != filterString && !veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower());
                            });
                        }
                        catch (Exception)
                        {
                            tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh)
                            {
                                return useEqual ?
                                    veh.VehicleDisplayName == filterString || veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower())
                                    : veh.VehicleDisplayName != filterString && !veh.VehicleDisplayName.ToLower().Contains(filterString.ToLower());
                            });
                        }
                        break;
                    case 3: //Type
                        tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh) { return useEqual ? veh.DownloadType.Contains(filterString) : !veh.DownloadType.Contains(filterString); });
                        break;
                    case 4: //Relogin
                        tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh) { return useEqual ? veh.ContinueWithoutRelogin == Convert.ToBoolean(boolValue) : veh.ContinueWithoutRelogin != Convert.ToBoolean(boolValue); });
                        break;
                    case 5: //Owner
                        tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh) { return useEqual ? veh.CreatedUserName.Contains(filterString) : !veh.CreatedUserName.Contains(filterString); });
                        break;
                    case 6: //Status
                        tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh) { return useEqual ? veh.DownloadStatusId == downloadStatus : veh.DownloadStatusId != downloadStatus; });
                        break;
                    case 1: //Fleet
                    default:
                        try
                        {
                            tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh)
                            {
                                return useEqual ? veh.FleetID == int.Parse(filterString) || veh.FleetName == filterString
                                    : veh.FleetID != int.Parse(filterString) && veh.FleetName != filterString;
                            });
                        }
                        catch (Exception)
                        {
                            tmpDownloads = downloads.FindAll(delegate(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload veh)
                            {
                                return useEqual ? veh.FleetName == filterString || veh.FleetName.ToLower().Contains(filterString.ToLower())
                                    : veh.FleetName != filterString && !veh.FleetName.ToLower().Contains(filterString.ToLower());
                            });
                        }
                        break;
                }
                downloads = tmpDownloads;
            }
            catch (Exception ex)
            { }
        }

        void messageQueueTimer_Tick(object sender, EventArgs e)
        {
            messageQueueTimer.Stop();

            if (lblAutoRefresh.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { messageQueueTimer_Tick(sender, e); }));
            }
            else
            {
                if (refreshCount == 0)
                {
                    this.LoadExistingQueuedDownloads();
                    refreshCount = 60;
                }
                else
                {
                    refreshCount--;
                    this.lblAutoRefresh.Text = "Auto Refresh in " + refreshCount.ToString();
                }
            }

            messageQueueTimer.Start();
        }

        private void StartQueueTimer()
        {
            messageQueueTimer = new Timer();

            messageQueueTimer.Tick += messageQueueTimer_Tick;

            messageQueueTimer.Interval = 1000;

            messageQueueTimer.Start();
        }

        private void AddVehicleToAvailableList(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh)
        {
            ListViewItem vehItem = new ListViewItem(veh.FleetID.ToString());

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.FleetName));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.VehicleID.ToString()));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.SerialNumber == null ? "Unavailable" : veh.SerialNumber));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.DisplayName));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.HWVersion));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.SWVersion));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.CDMASupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.GPRS ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.MDTSupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.DetatchMDTSupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.PhoneKit ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.RS232 ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.ConcreteSupport ? "Yes" : "No"));

            vehItem.SubItems.Add(new ListViewItem.ListViewSubItem(vehItem, veh.Refrigeration ? "Yes" : "No"));

            vehItem.Tag = veh;

            lvAvailableUnits.Items.Add(vehItem);
        }

        private void EnableMoveButtons()
        {
            btnAddAll.Enabled = lvAvailableUnits.Items.Count > 0 && this.ValidateSMTPDetails() && this.cmbDownloadFile.SelectedIndex != -1;
            btnRemoveAll.Enabled = lvQueued.Items.Count > 0;
            btnAddSelected.Enabled = lvAvailableUnits.SelectedIndices.Count > 0 && this.ValidateSMTPDetails() && this.cmbDownloadFile.SelectedIndex != -1;
            btnRemoveSelected.Enabled = lvQueued.SelectedIndices.Count > 0;

            btnSendToUnits.Enabled = this.queuedDownloads.Count > 0;
        }

        private KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> FindFleetInList(string fleet)
        {
            foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleetItem in this.fleetVehicles)
            {
                if (fleetItem.Key == fleet)
                {
                    return fleetItem;
                }
            }

            throw new IndexOutOfRangeException();
        }

        private void FilterList()
        {
            this.lvAvailableUnits.Items.Clear();

            if (cmbDownloadFile.SelectedIndex != -1)
            {
                Service.Listener.GatewayServiceBL.Classes.DownloadFileImage dt = (Service.Listener.GatewayServiceBL.Classes.DownloadFileImage)cmbDownloadFile.SelectedItem;

                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> availList = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>();

                if (cmbAvailFilter1.SelectedIndex == 0 && cmbAvailFilter2.SelectedIndex == 0)
                {
                    // Show all

                    foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet in this.fleetVehicles)
                    {
                        foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in (List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value)
                        {
                            if (dt.AssetTypeID == veh.AssetTypeID)
                            {
                                AddVehicleToAvailableList(veh);
                            }
                        }
                    }
                }
                else
                {
                    List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> filteredList = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>();

                    foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet in this.fleetVehicles)
                    {
                        foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in (List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value)
                        {
                            filteredList.Add(veh);
                        }
                    }


                    if (cmbAvailFilter1.SelectedIndex != 0)
                    {
                        this.FilterMessageListBy(ref filteredList, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbAvailFilter1.SelectedItem).ID, txtFilter1.Text, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbBoolFilter1.SelectedItem).ID, !chkFilter1EqNEq.Checked);
                    }

                    if (cmbAvailFilter2.SelectedIndex != 0)
                    {
                        this.FilterMessageListBy(ref filteredList, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbAvailFilter2.SelectedItem).ID, txtFilter2.Text, ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbBoolFilter2.SelectedItem).ID, !chkFilter2EqNEq.Checked);
                    }


                    foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in filteredList)
                    {
                        if (dt.AssetTypeID == veh.AssetTypeID)
                        {
                            AddVehicleToAvailableList(veh);
                        }
                    }
                }
            }
            else
            {
                // As we can't download just list everything

                foreach (KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet in this.fleetVehicles)
                {
                    foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in (List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value)
                    {
                        AddVehicleToAvailableList(veh);
                    }
                }
            }
        }

        private void LoadFleetVehicles()
        {
            try
            {
                this.fleetVehicles = new List<KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>>();

                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle> currentVehicles = Helpers.ListenerGateway().GetFleetVehiclesWithAttachments(Helpers.UserCredentials);
                log.InfoFormat("Downloads - GetFleetVehicles called, returned {0} vehicle objects", currentVehicles.Count);

                foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh in currentVehicles)
                {
                    KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>> fleet;

                    try
                    {
                        fleet = this.FindFleetInList(veh.FleetID + "-" + veh.FleetName);

                    }
                    catch (IndexOutOfRangeException)
                    {
                        fleet = new KeyValuePair<string, List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>>(veh.FleetID + "-" + veh.FleetName, new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>());
                        this.fleetVehicles.Add(fleet);
                    }

                    ((List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle>)fleet.Value).Add(veh);
                }

                // Now we need to build a list of vehicles that are not filtered we will do this by 
                this.FilterList();

                this.EnableMoveButtons();
            }
            catch (Exception)
            { }
        }
        private void AddDownloadToExistingList(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload message)
        {
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("Type", "Type"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("Status", "Status"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("StartDate", "Start Date"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("ExpiryDate", "Expiry Date"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("AbortTimeout", "Abort Timeout"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("Relogin", "Require Relogin"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("SMTPServer", "SMTP Server"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("SMTPEmail", "Email Address"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("Owner", "Owner"));

            ListViewItem messageItem = new ListViewItem(message.FleetID.ToString());

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.FleetName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.VehicleId.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.VehicleDisplayName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.DownloadType));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.SentSegment.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.TotalSegment.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.DownloadStatus));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.StartDate.ToLocalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.ExpiryDate.ToLocalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.AbortTimeout.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.ContinueWithoutRelogin.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.SMTPEmailAddress));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, message.CreatedUserName));

            messageItem.Tag = message;

            lvExistingQueue.Items.Add(messageItem);
        }
        private void LoadExistingQueuedDownloads()
        {
            if (cmbQueueFilter1.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { LoadExistingQueuedDownloads(); }));
            }
            else
            {
                lvExistingQueue.Items.Clear();

                List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload> downloads = null;


                //if (cmbQueueFilter1.SelectedIndex != 0 || cmbQueueFilter2.SelectedIndex != 0)
                //{
                //if((cmbQueueFilter1.SelectedIndex == 6 || cmbQueueFilter2.SelectedIndex == 6))
                //{
                //    if (cmbQueuedStatus1.SelectedIndex == 0 || cmbQueuedStatus2.SelectedIndex == 0)
                //    {
                try
                {
                    downloads = Helpers.ListenerGateway().GetQueuedDownloadsFiltered(true, false, Helpers.UserCredentials);
                    log.InfoFormat("GetQueuedDownloadsFiltered called, returned {0} vehicle objects", downloads.Count);
                }
                catch (Exception ex)
                {
                    System.Windows.Forms.Application.DoEvents();
                }
                //    }
                //}
                //}
                //else
                //{
                //    downloads = Helpers.ListenerGateway().GetQueuedDownloads(Helpers.UserCredentials);
                //}

                //if (filtered)
                //{
                if (cmbQueueFilter1.SelectedIndex != 0 || cmbQueueFilter2.SelectedIndex != 0)
                {
                    if (cmbQueueFilter1.SelectedIndex != 0)
                    {
                        this.FilterQueuedDownListBy(ref downloads,
                            ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueueFilter1.SelectedItem).ID,
                            txtQueueFilter1.Text, cmbBoolQueuedFilter1.SelectedIndex, true,
                            ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueuedStatus1.SelectedItem).ID);
                    }

                    if (cmbQueueFilter2.SelectedIndex != 0)
                    {
                        this.FilterQueuedDownListBy(ref downloads,
                            ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueueFilter2.SelectedItem).ID,
                            txtQueueFilter2.Text, cmbBoolQueuedFilter1.SelectedIndex, true,
                            ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueuedStatus2.SelectedItem).ID);
                    }
                }
                //}

                if (downloads != null)
                {
                    foreach (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qm in downloads)
                    {
                        AddDownloadToExistingList(qm);
                    }
                }
                foreach (ColumnHeader ch in lvExistingQueue.Columns)
                {
                    ch.Width = -2;
                }
            }
        }

        private bool FilterItem(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qd)
        {
            bool canAdd = true;

            if (cmbQueueFilter1.SelectedIndex != 0)
            {
                if (cmbQueueFilter1.SelectedIndex == 13)
                {
                    canAdd &= qd.DownloadStatusId == ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueueFilter1.SelectedItem).ID;
                }
                else
                {
                    switch (((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueuedStatus1.SelectedItem).ID)
                    {
                        // TODO add code here
                    }
                }
            }

            if (cmbQueueFilter2.SelectedIndex != 0)
            {
                if (cmbQueueFilter2.SelectedIndex == 13)
                {
                    canAdd &= qd.DownloadStatusId == ((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueuedStatus2.SelectedItem).ID;
                        
                }
                else
                {
                    switch (((MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem)cmbQueueFilter2.SelectedItem).ID)
                    {
                        // TODO add code here
                    }
                }
            }

            return canAdd;
        }

        private void LoadFilterCombos()
        {
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> availFilters = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> availFilter2 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> availFilter3 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> availFilter4 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();

            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> queuedFilter1 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> queuedFilter2 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();

            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> statusFilter1 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();
            List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem> statusFilter2 = new List<MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem>();

            MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem tmpItem
                = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "All");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Fleet");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(2, "Vehicle");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(3, "HW Version");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(4, "SW Version");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(5, "CDMASupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(6, "GPRS");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(7, "MDTSupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(8, "DetachMDTSupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(9, "PhoneKit");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(10, "RS232");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(11, "ConcreteSupport");
            availFilters.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(12, "Refrigeration");
            availFilters.Add(tmpItem);

            availFilter2 = availFilters.ToList();

            availFilter3 = availFilters.ToList();

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(13, "Status");
            availFilter3.Add(tmpItem);

            availFilter4 = availFilter3.ToList();

            cmbAvailFilter1.DisplayMember = "Description";
            cmbAvailFilter1.ValueMember = "Id";

            cmbAvailFilter1.DataSource = availFilters;

            cmbAvailFilter2.DisplayMember = "Description";
            cmbAvailFilter2.ValueMember = "Id";

            cmbAvailFilter2.DataSource = availFilter2;

            cmbAvailFilter1.SelectedIndex = 0;
            cmbAvailFilter2.SelectedIndex = 0;

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "All");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Fleet");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(2, "Vehicle");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(3, "Type");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(4, "Relogin");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(5, "Owner");
            queuedFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(6, "Status");
            queuedFilter1.Add(tmpItem);

            queuedFilter2 = queuedFilter1.ToList();

            cmbQueueFilter1.DisplayMember = "Description";
            cmbQueueFilter1.ValueMember = "Id";

            cmbQueueFilter1.DataSource = queuedFilter1;

            cmbQueueFilter2.DisplayMember = "Description";
            cmbQueueFilter2.ValueMember = "Id";

            cmbQueueFilter2.DataSource = queuedFilter2;

            cmbQueueFilter1.SelectedIndex = 0;
            cmbQueueFilter2.SelectedIndex = 0;

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "No");
            cmbBoolFilter1.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Yes");
            cmbBoolFilter1.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "No");
            cmbBoolFilter2.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Yes");
            cmbBoolFilter2.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "No");
            cmbBoolQueuedFilter1.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Yes");
            cmbBoolQueuedFilter1.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "No");
            cmbBoolQueuedFilter2.Items.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "Yes");
            cmbBoolQueuedFilter2.Items.Add(tmpItem);

            cmbBoolFilter1.DisplayMember = "Description";
            cmbBoolFilter2.DisplayMember = "Description";
            cmbBoolQueuedFilter1.DisplayMember = "Description";
            cmbBoolQueuedFilter2.DisplayMember = "Description";

            cmbBoolFilter1.ValueMember = "Id";
            cmbBoolFilter2.ValueMember = "Id";
            cmbBoolQueuedFilter1.ValueMember = "Id";
            cmbBoolQueuedFilter2.ValueMember = "Id";

            cmbBoolFilter1.SelectedIndex = 0;
            cmbBoolFilter2.SelectedIndex = 0;
            cmbBoolQueuedFilter1.SelectedIndex = 0;
            cmbBoolQueuedFilter2.SelectedIndex = 0;

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(0, "Pending");
            statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(1, "In Progress");
            statusFilter1.Add(tmpItem);

            //tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(2, "");
            //statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(3, "Aborted");
            statusFilter1.Add(tmpItem);

            tmpItem = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem(4, "Complete");
            statusFilter1.Add(tmpItem);

            statusFilter2 = statusFilter1.ToList();

            cmbQueuedStatus1.DisplayMember = "Description";
            cmbQueuedStatus2.DisplayMember = "Description";

            cmbQueuedStatus1.ValueMember = "Id";
            cmbQueuedStatus2.ValueMember = "Id";

            cmbQueuedStatus1.DataSource = statusFilter1;
            cmbQueuedStatus2.DataSource = statusFilter2;
        }

        private void LoadListViewColumns()
        {
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("SerialNumber", "Serial Number"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("HW Version", "HW Version"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("SW Version", "SW Version"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("CDMASupport", "CDMA Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("GPRS", "GPRS"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("MDTSupport", "MDT Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("DetachMDTSupport", "Detach MDT Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("PhoneKit", "Phone Kit"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("RS232", "RS232"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("ConcreteSupport", "Concrete Support"));
            lvAvailableUnits.Columns.Add(Helpers.CreateHeader("Refrigeration", "Refrigeration Support"));

            foreach (ColumnHeader ch in lvAvailableUnits.Columns)
            {
                ch.Width = -2;
            }

            lvQueued.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            lvQueued.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            lvQueued.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            lvQueued.Columns.Add(Helpers.CreateHeader("SerialNumber", "Serial Number"));
            lvQueued.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            lvQueued.Columns.Add(Helpers.CreateHeader("Type", "Type"));
            lvQueued.Columns.Add(Helpers.CreateHeader("StartDate", "Start Date"));
            lvQueued.Columns.Add(Helpers.CreateHeader("ExpiryDate", "Expiry Date"));
            lvQueued.Columns.Add(Helpers.CreateHeader("AbortTimeout", "Abort Timeout"));
            lvQueued.Columns.Add(Helpers.CreateHeader("Relogin", "Require Relogin"));
            lvQueued.Columns.Add(Helpers.CreateHeader("SMTPEmail", "Email Address"));

            foreach (ColumnHeader ch in lvQueued.Columns)
            {
                ch.Width = -2;
            }

            lvExistingQueue.Columns.Add(Helpers.CreateHeader("FleetID", "Fleet ID"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("FleetName", "Fleet Name"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("VehicleID", "Vehicle ID"));
            //lvExistingQueue.Columns.Add(Helpers.CreateHeader("SerialNumber", "Serial Number"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("VehicleName", "Vehicle Name"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Type", "Type"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("SentSegment", "Sent"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("TotalSegment", "Total"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Status", "Status"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("StartDate", "Start Date"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("ExpiryDate", "Expiry Date"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("AbortTimeout", "Abort Timeout"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Relogin", "Require Relogin"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("SMTPEmail", "Email Address"));
            lvExistingQueue.Columns.Add(Helpers.CreateHeader("Owner", "Owner"));

            foreach (ColumnHeader ch in lvExistingQueue.Columns)
            {
                ch.Width = -2;
            }
        }

        private void AddDownloadToList(QueuedDownload download, System.Drawing.Color highlighColor)
        {
            ListViewItem messageItem = new ListViewItem(download.Vehicle.FleetID.ToString());

            messageItem.BackColor = highlighColor;

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.Vehicle.FleetName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.Vehicle.VehicleID.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.Vehicle.SerialNumber == null ? "Unavailable" : download.Vehicle.SerialNumber));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.Vehicle.DisplayName));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.Type.Description));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.StartDate.ToUniversalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.ExpiryDate.ToUniversalTime().ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.AbortTimeout.ToString()));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.ContinueWithoutRelogin ? "Yes" : "No"));

            messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, download.SMTPEmailAddress));
            
            messageItem.Tag = download;

            lvQueued.Items.Add(messageItem);
        }

        private void btnAvailableFilter_Click(object sender, EventArgs e)
        {
            this.FilterList();

            this.EnableMoveButtons();
        }

        private void cmbAvailFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAvailFilter2.SelectedIndex == 0)
            {
                txtFilter2.Text = string.Empty;
            }

            txtFilter2.Visible = cmbAvailFilter2.SelectedIndex < 5;
            cmbBoolFilter2.Visible = cmbAvailFilter2.SelectedIndex >= 5;
            txtFilter2.Enabled = cmbAvailFilter2.SelectedIndex != 0;
        }

        private void cmbAvailFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cmbAvailFilter1.SelectedIndex == 0)
            {
                txtFilter1.Text = string.Empty;
            }

            txtFilter1.Visible = cmbAvailFilter1.SelectedIndex < 5;
            cmbBoolFilter1.Visible = cmbAvailFilter1.SelectedIndex >= 5;
            txtFilter1.Enabled = cmbAvailFilter1.SelectedIndex != 0;
        }

        private void lvQueued_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void lvExistingQueue_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableAbortButton();
        }

        private void EnableAbortButton()
        {
            if (this.lvExistingQueue.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { EnableAbortButton(); }));
            }
            else
            {
                btnAbortDownload.Enabled = lvExistingQueue.SelectedItems.Count > 0;
            }
        }

        private void btnAbortMessage_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Abort Messages?", "Abort", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.messageQueueTimer.Stop();
                MTData.Common.Threading.EnhancedThread abortThread = new MTData.Common.Threading.EnhancedThread(new MTData.Common.Threading.EnhancedThread.EnhancedThreadStart(AbortDownloadsInQueue), null);
                abortThread.Start();
                this.refreshCount = 60;
                this.messageQueueTimer.Start();
            }
        }

        private object AbortDownloadsInQueue(MTData.Common.Threading.EnhancedThread sender, object data)
        {
            if (lvExistingQueue.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { AbortDownloadsInQueue(sender, data); }));
            }
            else
            {
                foreach (ListViewItem dl in lvExistingQueue.SelectedItems)
                {
                    MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qDownload = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload)dl.Tag;

                    qDownload.FileData = null;

                    log.Info("AbortDownloadInQueue called");
                    if (Helpers.ListenerGateway().AbortDownloadInQueue(qDownload, Helpers.UserCredentials) == 0)
                    {
                        lvExistingQueue.Items.Remove(dl);
                    }
                }

                this.EnableAbortButton();
            }

            return null;
        }

        private void lvAvailableUnits_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                foreach (ListViewItem item in lvAvailableUnits.Items)
                {
                    item.Selected = true;
                }
            }

            e.SuppressKeyPress = true;

            this.EnableMoveButtons();
        }

        private void lvExistingQueue_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Control && e.KeyCode == Keys.A)
            {
                foreach (ListViewItem item in lvExistingQueue.Items)
                {
                    item.Selected = true;
                }
            }

            e.SuppressKeyPress = true;

            this.EnableAbortButton();
        }

        private void btnRefreshExistingQueue_Click(object sender, EventArgs e)
        {
            this.messageQueueTimer.Stop();

            this.refreshCount = 60;

            this.LoadExistingQueuedDownloads();

            this.EnableAbortButton();

            this.messageQueueTimer.Start();
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            queuedDownloads.Clear();
            lvQueued.Items.Clear();

            this.EnableMoveButtons();
        }

        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            foreach (ListViewItem item in lvQueued.SelectedItems)
            {
                queuedDownloads.Remove((QueuedDownload)item.Tag);
                lvQueued.Items.Remove(item);
            }

            this.EnableMoveButtons();
        }

        private void btnAddAll_Click(object sender, EventArgs e)
        {
            if (this.queuedDownloads.Count == 0 && this.lvQueued.Items.Count > 0)
            {
                this.lvQueued.Items.Clear();
            }

            foreach (ListViewItem item in lvAvailableUnits.Items)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle)item.Tag;

                QueuedDownload qm = queuedDownloads.Find(delegate(QueuedDownload m)
                {
                    return m.Vehicle.VehicleID == veh.VehicleID;
                });

                if (qm == null)
                {
                    qm = new QueuedDownload(veh,
                        (Service.Listener.GatewayServiceBL.Classes.DownloadFileImage)cmbDownloadFile.SelectedItem, DateTime.Now, DateTime.Now.AddDays(7),
                            Convert.ToInt32(numTimeoutDownloadMins.Value), chkContinueWithoutWaiting.Checked, txtEmailTo.Text);
                    queuedDownloads.Add(qm);
                    AddDownloadToList(qm, System.Drawing.Color.White);
                }
            }

            foreach (ColumnHeader ch in lvQueued.Columns)
            {
                ch.Width = -2;
            }

            this.EnableMoveButtons();
        }

        private void btnAddSelected_Click(object sender, EventArgs e)
        {
            if (this.queuedDownloads.Count == 0 && this.lvQueued.Items.Count > 0)
            {
                this.lvQueued.Items.Clear();
            }

            foreach (ListViewItem item in lvAvailableUnits.SelectedItems)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle veh = (MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle)item.Tag;

                QueuedDownload qm = queuedDownloads.Find(delegate(QueuedDownload m)
                {
                    return m.Vehicle.VehicleID == veh.VehicleID && m.Vehicle.FleetID == veh.FleetID;
                });

                if (qm == null)
                {
                    qm = new QueuedDownload(veh,
                        (Service.Listener.GatewayServiceBL.Classes.DownloadFileImage)cmbDownloadFile.SelectedItem, DateTime.Now, DateTime.Now.AddDays(7),
                            Convert.ToInt32(numTimeoutDownloadMins.Value), chkContinueWithoutWaiting.Checked, txtEmailTo.Text);
                    queuedDownloads.Add(qm);
                    AddDownloadToList(qm, System.Drawing.Color.White);
                }
            }

            foreach (ColumnHeader ch in lvQueued.Columns)
            {
                ch.Width = -2;
            }

            this.EnableMoveButtons();
        }

        private void btnRefershAvaliableList_Click(object sender, EventArgs e)
        {
            this.LoadFleetVehicles();
        }

        private void lvAvailableUnits_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void btnAddToQueue_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Send all downloads to queue?", "Send Downloads To Queue", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.messageQueueTimer.Stop();
                this.refreshCount = 60;
                MTData.Common.Threading.EnhancedThread uploadThread = new MTData.Common.Threading.EnhancedThread(new MTData.Common.Threading.EnhancedThread.EnhancedThreadStart(UploadDownloadsToQueue), null);
                uploadThread.Start();
                this.messageQueueTimer.Start();
            }
        }

        private object UploadDownloadsToQueue(MTData.Common.Threading.EnhancedThread sender, object data)
        {
            List<QueuedDownload> nonSend = new List<QueuedDownload>();

            foreach (QueuedDownload qm in this.queuedDownloads)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qDownload
                    = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload();

                qDownload.DefinitionID = 1;
                qDownload.FleetID = qm.Vehicle.FleetID;
                qDownload.VehicleId = qm.Vehicle.VehicleID;
                qDownload.StartDate = qm.StartDate;
                qDownload.ExpiryDate = qm.ExpiryDate;
                qDownload.ForceLogout = false;
                qDownload.ForceUpdate = false;

                log.Info("SendDownloadToQueue called");
                int response = Helpers.ListenerGateway().SendDownloadToQueue(qDownload, Helpers.UserCredentials);

                if (response != 0)
                {
                    nonSend.Add(qm);
                }
            }

            // Now clear both the queued list and the lvQueued and then load the nonSend color coded
            this.queuedDownloads.Clear();

            this.ClearQueuedList(nonSend);

            return null;
        }

        private void ClearQueuedList(List<QueuedDownload> nonSend)
        {
            if (this.lvQueued.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { ClearQueuedList(nonSend); }));
            }
            else
            {
                this.lvQueued.Items.Clear();

                foreach (QueuedDownload qm in nonSend)
                {
                    AddDownloadToList(qm, System.Drawing.Color.Orange);
                }

                this.LoadExistingQueuedDownloads();

                this.EnableMoveButtons();
            }
        }

        private void LoadDownloadFilesByType(int downloadType)
        {
            try
            {
                cmbDownloadFile.DataSource = null;

                cmbDownloadFile.DisplayMember = "Description";
                cmbDownloadFile.ValueMember = "Id";

                cmbDownloadFile.DataSource = Helpers.ListenerGateway().GetFileImagesForDownloadType(downloadType, Helpers.UserCredentials);
                log.Info("GetFileImagesForDownloadType called");

                if (cmbDownloadFile.Items.Count > 0)
                {
                    cmbDownloadFile.SelectedIndex = 0;
                }

                gbAvailableFilter.Enabled = cmbDownloadFile.Items.Count > 0;
            }
            catch (Exception)
            { }
        }

        private void cmbDownloadType_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.LoadDownloadFilesByType(((Service.Listener.GatewayServiceBL.Classes.DownloadType)cmbDownloadType.SelectedItem).ID);

            this.FilterList();

            this.EnableMoveButtons();
        }

        private void cmbDownloadFile_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.FilterList();

            this.EnableMoveButtons();
        }

        private void txtEmailTo_TextChanged(object sender, EventArgs e)
        {
            this.EnableMoveButtons();
        }

        private void btnSendToUnits_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Send all downloads to queue?", "Send Downloads To Queue", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.messageQueueTimer.Stop();
                this.refreshCount = 60;
                MTData.Common.Threading.EnhancedThread uploadThread = new MTData.Common.Threading.EnhancedThread(new MTData.Common.Threading.EnhancedThread.EnhancedThreadStart(UploadFilesToQueue), null);
                uploadThread.Start();
                this.messageQueueTimer.Start();
            }
        }

        private object UploadFilesToQueue(MTData.Common.Threading.EnhancedThread sender, object data)
        {
            List<QueuedDownload> nonSend = new List<QueuedDownload>();

            foreach (QueuedDownload qd in this.queuedDownloads)
            {
                MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload qDownload
                    = new MTData.Transport.Service.Listener.GatewayServiceBL.Classes.QueuedDownload();

                qDownload.FleetID = qd.Vehicle.FleetID;
                qDownload.VehicleId = qd.Vehicle.VehicleID;
                qDownload.StartDate = qd.StartDate;
                qDownload.ExpiryDate = qd.ExpiryDate;
                qDownload.DefinitionID = qd.Type.ID;
                qDownload.TimeCreated = DateTime.Now.ToUniversalTime();
                qDownload.AbortTimeout = qd.AbortTimeout;
                qDownload.ContinueWithoutRelogin = qd.ContinueWithoutRelogin;
                qDownload.SMTPEmailAddress = qd.SMTPEmailAddress;

                log.Info("SendDownloadToQueue called");
                int response = Helpers.ListenerGateway().SendDownloadToQueue(qDownload, Helpers.UserCredentials);

                if (response != 0)
                {
                    nonSend.Add(qd);
                }
            }

            // Now clear both the queued list and the lvQueued and then load the nonSend color coded
            this.queuedDownloads.Clear();

            this.ClearQueuedList(nonSend);

            this.LoadExistingQueuedDownloads();

            return null;
        }

        private void chkFilter1EqNEq_CheckedChanged(object sender, EventArgs e)
        {
            chkFilter1EqNEq.Text = chkFilter1EqNEq.Checked ? "!=" : "=";
        }

        private void chkFilter2EqNEq_CheckedChanged(object sender, EventArgs e)
        {
            chkFilter2EqNEq.Text = chkFilter2EqNEq.Checked ? "!=" : "=";
        }

        private void cmbQueueFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbBoolQueuedFilter1.Visible = cmbQueueFilter1.SelectedIndex == 4;

            txtQueueFilter1.Visible = cmbQueueFilter1.SelectedIndex != 0 && cmbQueueFilter1.SelectedIndex != 6 && cmbQueueFilter2.SelectedIndex != 4;

            cmbQueuedStatus1.Visible = cmbQueueFilter1.SelectedIndex == 6 && cmbQueueFilter2.SelectedIndex != 4;

            if (cmbQueueFilter1.SelectedIndex == 0)
            {
                txtQueueFilter1.Text = string.Empty;
            }
        }

        private void cmbQueueFilter2_SelectedIndexChanged(object sender, EventArgs e)
        {
            cmbBoolQueuedFilter2.Visible = cmbQueueFilter2.SelectedIndex == 4;

            txtQueueFilter2.Visible = cmbQueueFilter2.SelectedIndex != 0 && cmbQueueFilter2.SelectedIndex != 6 && cmbQueueFilter2.SelectedIndex != 4;

            cmbQueuedStatus2.Visible = cmbQueueFilter2.SelectedIndex == 6 && cmbQueueFilter2.SelectedIndex != 4;

            if (cmbQueueFilter2.SelectedIndex == 0)
            {
                txtQueueFilter2.Text = string.Empty;
            }
        }

        private void btnQueueFilter_Click(object sender, EventArgs e)
        {
            this.messageQueueTimer.Stop();

            this.lblAutoRefresh.Text = "Click to start auto timer";

            this.refreshCount = 60;

            this.LoadExistingQueuedDownloads();

            this.EnableAbortButton();
        }

        private void cmbBoolQueuedFilter1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void splitMain_SplitterMoved(object sender, SplitterEventArgs e)
        {
            splitMain.Panel1MinSize = splitMain.Width / 2 < 550 ? 550 : splitMain.Width / 2;
        }
    }
}

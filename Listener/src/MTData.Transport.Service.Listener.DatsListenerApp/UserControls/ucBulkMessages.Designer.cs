﻿namespace MTData.Transport.Listener.DatsListenerApp.UserControls
{
    partial class ucBulkMessages
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.IapDataBlocksGroupBox = new System.Windows.Forms.GroupBox();
            this.BlockDataBlockNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label12 = new System.Windows.Forms.Label();
            this.SpeedDataBlockNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.PositionDataBlocksNumericUpDown = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.gbMessageEndDate = new System.Windows.Forms.GroupBox();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.gbMessageStartDate = new System.Windows.Forms.GroupBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.gbAvailableFilter = new System.Windows.Forms.GroupBox();
            this.cmbBoolFilter2 = new System.Windows.Forms.ComboBox();
            this.btnAvailableFilter = new System.Windows.Forms.Button();
            this.txtFilter2 = new System.Windows.Forms.TextBox();
            this.cmbAvailFilter2 = new System.Windows.Forms.ComboBox();
            this.txtFilter1 = new System.Windows.Forms.TextBox();
            this.cmbAvailFilter1 = new System.Windows.Forms.ComboBox();
            this.cmbBoolFilter1 = new System.Windows.Forms.ComboBox();
            this.gbNetworkInfo = new System.Windows.Forms.GroupBox();
            this.txtPingIP = new MTData.Transport.Listener.DatsListenerApp.MaskedTextBox();
            this.txtSecIP = new MTData.Transport.Listener.DatsListenerApp.MaskedTextBox();
            this.numSecIPPort = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numNewPingRetries = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numNewIPPort = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.chkUseHostNameLookup = new System.Windows.Forms.CheckBox();
            this.txtHostName = new System.Windows.Forms.TextBox();
            this.txtNewIP = new MTData.Transport.Listener.DatsListenerApp.MaskedTextBox();
            this.btnRefershAvaliableList = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lvAvailableUnits = new System.Windows.Forms.ListView();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbCommand = new System.Windows.Forms.ComboBox();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.btnAddAll = new System.Windows.Forms.Button();
            this.btnAddSelected = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lvQueued = new System.Windows.Forms.ListView();
            this.btnAddToQueue = new System.Windows.Forms.Button();
            this.lblAutoRefresh = new System.Windows.Forms.Label();
            this.gbQueuedFilter = new System.Windows.Forms.GroupBox();
            this.btnQueueFilter = new System.Windows.Forms.Button();
            this.cmbQueueFilter2 = new System.Windows.Forms.ComboBox();
            this.cmbQueueFilter1 = new System.Windows.Forms.ComboBox();
            this.cmbQueuedStatus2 = new System.Windows.Forms.ComboBox();
            this.cmbQueuedStatus1 = new System.Windows.Forms.ComboBox();
            this.txtQueueFilter1 = new System.Windows.Forms.TextBox();
            this.txtQueueFilter2 = new System.Windows.Forms.TextBox();
            this.lvExistingQueue = new System.Windows.Forms.ListView();
            this.btnAbortMessage = new System.Windows.Forms.Button();
            this.btnRefreshExistingQueue = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.IapDataBlocksGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BlockDataBlockNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedDataBlockNumericUpDown)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PositionDataBlocksNumericUpDown)).BeginInit();
            this.gbMessageEndDate.SuspendLayout();
            this.gbMessageStartDate.SuspendLayout();
            this.gbAvailableFilter.SuspendLayout();
            this.gbNetworkInfo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSecIPPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewPingRetries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewIPPort)).BeginInit();
            this.gbQueuedFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.IapDataBlocksGroupBox);
            this.splitMain.Panel1.Controls.Add(this.gbMessageEndDate);
            this.splitMain.Panel1.Controls.Add(this.gbMessageStartDate);
            this.splitMain.Panel1.Controls.Add(this.gbAvailableFilter);
            this.splitMain.Panel1.Controls.Add(this.gbNetworkInfo);
            this.splitMain.Panel1.Controls.Add(this.btnRefershAvaliableList);
            this.splitMain.Panel1.Controls.Add(this.label1);
            this.splitMain.Panel1.Controls.Add(this.lvAvailableUnits);
            this.splitMain.Panel1.Controls.Add(this.label3);
            this.splitMain.Panel1.Controls.Add(this.cmbCommand);
            this.splitMain.Panel1.Controls.Add(this.btnRemoveSelected);
            this.splitMain.Panel1.Controls.Add(this.btnAddAll);
            this.splitMain.Panel1.Controls.Add(this.btnAddSelected);
            this.splitMain.Panel1.Controls.Add(this.btnRemoveAll);
            this.splitMain.Panel1.Controls.Add(this.label2);
            this.splitMain.Panel1.Controls.Add(this.lvQueued);
            this.splitMain.Panel1.Controls.Add(this.btnAddToQueue);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.lblAutoRefresh);
            this.splitMain.Panel2.Controls.Add(this.gbQueuedFilter);
            this.splitMain.Panel2.Controls.Add(this.lvExistingQueue);
            this.splitMain.Panel2.Controls.Add(this.btnAbortMessage);
            this.splitMain.Panel2.Controls.Add(this.btnRefreshExistingQueue);
            this.splitMain.Size = new System.Drawing.Size(1262, 488);
            this.splitMain.SplitterDistance = 901;
            this.splitMain.TabIndex = 0;
            this.splitMain.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitMain_SplitterMoved);
            // 
            // IapDataBlocksGroupBox
            // 
            this.IapDataBlocksGroupBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.IapDataBlocksGroupBox.Controls.Add(this.BlockDataBlockNumericUpDown);
            this.IapDataBlocksGroupBox.Controls.Add(this.label12);
            this.IapDataBlocksGroupBox.Controls.Add(this.SpeedDataBlockNumericUpDown);
            this.IapDataBlocksGroupBox.Controls.Add(this.label11);
            this.IapDataBlocksGroupBox.Controls.Add(this.PositionDataBlocksNumericUpDown);
            this.IapDataBlocksGroupBox.Controls.Add(this.label10);
            this.IapDataBlocksGroupBox.Location = new System.Drawing.Point(3, 343);
            this.IapDataBlocksGroupBox.Name = "IapDataBlocksGroupBox";
            this.IapDataBlocksGroupBox.Size = new System.Drawing.Size(221, 115);
            this.IapDataBlocksGroupBox.TabIndex = 4;
            this.IapDataBlocksGroupBox.TabStop = false;
            this.IapDataBlocksGroupBox.Text = "IAP Data Blocks";
            // 
            // BlockDataBlockNumericUpDown
            // 
            this.BlockDataBlockNumericUpDown.Location = new System.Drawing.Point(82, 81);
            this.BlockDataBlockNumericUpDown.Name = "BlockDataBlockNumericUpDown";
            this.BlockDataBlockNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.BlockDataBlockNumericUpDown.TabIndex = 5;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Location = new System.Drawing.Point(18, 83);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(34, 13);
            this.label12.TabIndex = 4;
            this.label12.Text = "Block";
            // 
            // SpeedDataBlockNumericUpDown
            // 
            this.SpeedDataBlockNumericUpDown.Location = new System.Drawing.Point(82, 55);
            this.SpeedDataBlockNumericUpDown.Name = "SpeedDataBlockNumericUpDown";
            this.SpeedDataBlockNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.SpeedDataBlockNumericUpDown.TabIndex = 3;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(18, 57);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(38, 13);
            this.label11.TabIndex = 2;
            this.label11.Text = "Speed";
            // 
            // PositionDataBlocksNumericUpDown
            // 
            this.PositionDataBlocksNumericUpDown.Location = new System.Drawing.Point(82, 29);
            this.PositionDataBlocksNumericUpDown.Name = "PositionDataBlocksNumericUpDown";
            this.PositionDataBlocksNumericUpDown.Size = new System.Drawing.Size(120, 20);
            this.PositionDataBlocksNumericUpDown.TabIndex = 1;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(18, 31);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(44, 13);
            this.label10.TabIndex = 0;
            this.label10.Text = "Position";
            // 
            // gbMessageEndDate
            // 
            this.gbMessageEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbMessageEndDate.Controls.Add(this.dtpExpiryDate);
            this.gbMessageEndDate.Location = new System.Drawing.Point(392, 240);
            this.gbMessageEndDate.Name = "gbMessageEndDate";
            this.gbMessageEndDate.Size = new System.Drawing.Size(149, 47);
            this.gbMessageEndDate.TabIndex = 12;
            this.gbMessageEndDate.TabStop = false;
            this.gbMessageEndDate.Text = "Expiry Date";
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpExpiryDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(4, 19);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(136, 20);
            this.dtpExpiryDate.TabIndex = 0;
            // 
            // gbMessageStartDate
            // 
            this.gbMessageStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbMessageStartDate.Controls.Add(this.dtpStartDate);
            this.gbMessageStartDate.Location = new System.Drawing.Point(392, 187);
            this.gbMessageStartDate.Name = "gbMessageStartDate";
            this.gbMessageStartDate.Size = new System.Drawing.Size(149, 47);
            this.gbMessageStartDate.TabIndex = 11;
            this.gbMessageStartDate.TabStop = false;
            this.gbMessageStartDate.Text = "Start Date";
            this.gbMessageStartDate.Visible = false;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpStartDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(7, 20);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(136, 20);
            this.dtpStartDate.TabIndex = 0;
            // 
            // gbAvailableFilter
            // 
            this.gbAvailableFilter.Controls.Add(this.cmbBoolFilter2);
            this.gbAvailableFilter.Controls.Add(this.btnAvailableFilter);
            this.gbAvailableFilter.Controls.Add(this.txtFilter2);
            this.gbAvailableFilter.Controls.Add(this.cmbAvailFilter2);
            this.gbAvailableFilter.Controls.Add(this.txtFilter1);
            this.gbAvailableFilter.Controls.Add(this.cmbAvailFilter1);
            this.gbAvailableFilter.Controls.Add(this.cmbBoolFilter1);
            this.gbAvailableFilter.Location = new System.Drawing.Point(4, 29);
            this.gbAvailableFilter.Name = "gbAvailableFilter";
            this.gbAvailableFilter.Size = new System.Drawing.Size(319, 81);
            this.gbAvailableFilter.TabIndex = 1;
            this.gbAvailableFilter.TabStop = false;
            this.gbAvailableFilter.Text = "Filter:";
            // 
            // cmbBoolFilter2
            // 
            this.cmbBoolFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoolFilter2.FormattingEnabled = true;
            this.cmbBoolFilter2.Location = new System.Drawing.Point(149, 47);
            this.cmbBoolFilter2.Name = "cmbBoolFilter2";
            this.cmbBoolFilter2.Size = new System.Drawing.Size(101, 21);
            this.cmbBoolFilter2.TabIndex = 4;
            // 
            // btnAvailableFilter
            // 
            this.btnAvailableFilter.Location = new System.Drawing.Point(256, 20);
            this.btnAvailableFilter.Name = "btnAvailableFilter";
            this.btnAvailableFilter.Size = new System.Drawing.Size(57, 23);
            this.btnAvailableFilter.TabIndex = 2;
            this.btnAvailableFilter.Text = "Apply";
            this.btnAvailableFilter.UseVisualStyleBackColor = true;
            this.btnAvailableFilter.Click += new System.EventHandler(this.btnAvailableFilter_Click);
            // 
            // txtFilter2
            // 
            this.txtFilter2.Location = new System.Drawing.Point(149, 46);
            this.txtFilter2.Name = "txtFilter2";
            this.txtFilter2.Size = new System.Drawing.Size(100, 20);
            this.txtFilter2.TabIndex = 3;
            // 
            // cmbAvailFilter2
            // 
            this.cmbAvailFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAvailFilter2.FormattingEnabled = true;
            this.cmbAvailFilter2.Location = new System.Drawing.Point(7, 47);
            this.cmbAvailFilter2.Name = "cmbAvailFilter2";
            this.cmbAvailFilter2.Size = new System.Drawing.Size(136, 21);
            this.cmbAvailFilter2.TabIndex = 3;
            this.cmbAvailFilter2.SelectedIndexChanged += new System.EventHandler(this.cmbAvailFilter2_SelectedIndexChanged);
            // 
            // txtFilter1
            // 
            this.txtFilter1.Location = new System.Drawing.Point(149, 19);
            this.txtFilter1.Name = "txtFilter1";
            this.txtFilter1.Size = new System.Drawing.Size(100, 20);
            this.txtFilter1.TabIndex = 1;
            this.txtFilter1.KeyUp += new System.Windows.Forms.KeyEventHandler(this.txtFilter1_KeyUp);
            // 
            // cmbAvailFilter1
            // 
            this.cmbAvailFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAvailFilter1.FormattingEnabled = true;
            this.cmbAvailFilter1.Location = new System.Drawing.Point(7, 20);
            this.cmbAvailFilter1.Name = "cmbAvailFilter1";
            this.cmbAvailFilter1.Size = new System.Drawing.Size(136, 21);
            this.cmbAvailFilter1.TabIndex = 0;
            this.cmbAvailFilter1.SelectedIndexChanged += new System.EventHandler(this.cmbAvailFilter1_SelectedIndexChanged);
            // 
            // cmbBoolFilter1
            // 
            this.cmbBoolFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoolFilter1.FormattingEnabled = true;
            this.cmbBoolFilter1.Location = new System.Drawing.Point(149, 18);
            this.cmbBoolFilter1.Name = "cmbBoolFilter1";
            this.cmbBoolFilter1.Size = new System.Drawing.Size(101, 21);
            this.cmbBoolFilter1.TabIndex = 1;
            // 
            // gbNetworkInfo
            // 
            this.gbNetworkInfo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbNetworkInfo.Controls.Add(this.txtPingIP);
            this.gbNetworkInfo.Controls.Add(this.txtSecIP);
            this.gbNetworkInfo.Controls.Add(this.numSecIPPort);
            this.gbNetworkInfo.Controls.Add(this.label8);
            this.gbNetworkInfo.Controls.Add(this.label9);
            this.gbNetworkInfo.Controls.Add(this.numNewPingRetries);
            this.gbNetworkInfo.Controls.Add(this.label7);
            this.gbNetworkInfo.Controls.Add(this.numNewIPPort);
            this.gbNetworkInfo.Controls.Add(this.label5);
            this.gbNetworkInfo.Controls.Add(this.label6);
            this.gbNetworkInfo.Controls.Add(this.label4);
            this.gbNetworkInfo.Controls.Add(this.chkUseHostNameLookup);
            this.gbNetworkInfo.Controls.Add(this.txtHostName);
            this.gbNetworkInfo.Controls.Add(this.txtNewIP);
            this.gbNetworkInfo.Location = new System.Drawing.Point(3, 343);
            this.gbNetworkInfo.Name = "gbNetworkInfo";
            this.gbNetworkInfo.Size = new System.Drawing.Size(341, 128);
            this.gbNetworkInfo.TabIndex = 41;
            this.gbNetworkInfo.TabStop = false;
            this.gbNetworkInfo.Text = "Network Info - Sent with Change IP Address Command";
            this.gbNetworkInfo.Visible = false;
            // 
            // txtPingIP
            // 
            this.txtPingIP.Location = new System.Drawing.Point(111, 81);
            this.txtPingIP.Masked = MTData.Transport.Listener.DatsListenerApp.Mask.None;
            this.txtPingIP.Name = "txtPingIP";
            this.txtPingIP.Size = new System.Drawing.Size(162, 20);
            this.txtPingIP.TabIndex = 6;
            this.txtPingIP.TextChanged += new System.EventHandler(this.txtPingIP_TextChanged);
            this.txtPingIP.Enter += new System.EventHandler(this.txtPingIP_Enter);
            this.txtPingIP.Leave += new System.EventHandler(this.txtPingIP_Leave);
            // 
            // txtSecIP
            // 
            this.txtSecIP.Location = new System.Drawing.Point(111, 60);
            this.txtSecIP.Masked = MTData.Transport.Listener.DatsListenerApp.Mask.None;
            this.txtSecIP.Name = "txtSecIP";
            this.txtSecIP.Size = new System.Drawing.Size(162, 20);
            this.txtSecIP.TabIndex = 4;
            this.txtSecIP.TextChanged += new System.EventHandler(this.txtSecIP_TextChanged);
            this.txtSecIP.Enter += new System.EventHandler(this.txtSecIP_Enter);
            this.txtSecIP.Leave += new System.EventHandler(this.txtSecIP_Leave);
            // 
            // numSecIPPort
            // 
            this.numSecIPPort.Location = new System.Drawing.Point(279, 60);
            this.numSecIPPort.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numSecIPPort.Name = "numSecIPPort";
            this.numSecIPPort.Size = new System.Drawing.Size(56, 20);
            this.numSecIPPort.TabIndex = 12;
            this.numSecIPPort.Value = new decimal(new int[] {
            3344,
            0,
            0,
            0});
            this.numSecIPPort.ValueChanged += new System.EventHandler(this.numSecIPPort_ValueChanged);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(271, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 16);
            this.label8.TabIndex = 11;
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(9, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 16);
            this.label9.TabIndex = 3;
            this.label9.Text = "Secondary IP";
            // 
            // numNewPingRetries
            // 
            this.numNewPingRetries.Location = new System.Drawing.Point(111, 104);
            this.numNewPingRetries.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numNewPingRetries.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNewPingRetries.Name = "numNewPingRetries";
            this.numNewPingRetries.Size = new System.Drawing.Size(40, 20);
            this.numNewPingRetries.TabIndex = 8;
            this.numNewPingRetries.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            this.numNewPingRetries.ValueChanged += new System.EventHandler(this.numNewPingRetries_ValueChanged);
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(9, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 16);
            this.label7.TabIndex = 7;
            this.label7.Text = "Ping Retries";
            // 
            // numNewIPPort
            // 
            this.numNewIPPort.Location = new System.Drawing.Point(279, 38);
            this.numNewIPPort.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numNewIPPort.Name = "numNewIPPort";
            this.numNewIPPort.Size = new System.Drawing.Size(56, 20);
            this.numNewIPPort.TabIndex = 10;
            this.numNewIPPort.Value = new decimal(new int[] {
            3344,
            0,
            0,
            0});
            this.numNewIPPort.ValueChanged += new System.EventHandler(this.numNewIPPort_ValueChanged);
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(271, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = ":";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(9, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 16);
            this.label6.TabIndex = 5;
            this.label6.Text = "Ping IP Address";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(9, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 18);
            this.label4.TabIndex = 1;
            this.label4.Text = "Listener IP Address";
            // 
            // chkUseHostNameLookup
            // 
            this.chkUseHostNameLookup.AutoSize = true;
            this.chkUseHostNameLookup.Location = new System.Drawing.Point(12, 19);
            this.chkUseHostNameLookup.Name = "chkUseHostNameLookup";
            this.chkUseHostNameLookup.Size = new System.Drawing.Size(140, 17);
            this.chkUseHostNameLookup.TabIndex = 0;
            this.chkUseHostNameLookup.Text = "Use Host Name Lookup";
            this.chkUseHostNameLookup.UseVisualStyleBackColor = true;
            this.chkUseHostNameLookup.CheckedChanged += new System.EventHandler(this.chkUseHostNameLookup_CheckedChanged);
            // 
            // txtHostName
            // 
            this.txtHostName.Location = new System.Drawing.Point(111, 38);
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.Size = new System.Drawing.Size(162, 20);
            this.txtHostName.TabIndex = 2;
            this.txtHostName.Visible = false;
            this.txtHostName.TextChanged += new System.EventHandler(this.txtHostName_TextChanged);
            this.txtHostName.Enter += new System.EventHandler(this.txtHostName_Enter);
            this.txtHostName.Leave += new System.EventHandler(this.txtHostName_Leave);
            // 
            // txtNewIP
            // 
            this.txtNewIP.Location = new System.Drawing.Point(111, 38);
            this.txtNewIP.Masked = MTData.Transport.Listener.DatsListenerApp.Mask.None;
            this.txtNewIP.Name = "txtNewIP";
            this.txtNewIP.Size = new System.Drawing.Size(162, 20);
            this.txtNewIP.TabIndex = 29;
            this.txtNewIP.TextChanged += new System.EventHandler(this.txtNewIP_TextChanged);
            this.txtNewIP.Enter += new System.EventHandler(this.txtNewIP_Enter);
            this.txtNewIP.Leave += new System.EventHandler(this.txtNewIP_Leave);
            // 
            // btnRefershAvaliableList
            // 
            this.btnRefershAvaliableList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefershAvaliableList.Location = new System.Drawing.Point(3, 313);
            this.btnRefershAvaliableList.Name = "btnRefershAvaliableList";
            this.btnRefershAvaliableList.Size = new System.Drawing.Size(382, 24);
            this.btnRefershAvaliableList.TabIndex = 3;
            this.btnRefershAvaliableList.Text = "Refresh Avaliable List";
            this.btnRefershAvaliableList.Click += new System.EventHandler(this.btnRefershAvaliableList_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 0;
            this.label1.Text = "Available Units";
            // 
            // lvAvailableUnits
            // 
            this.lvAvailableUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAvailableUnits.FullRowSelect = true;
            this.lvAvailableUnits.GridLines = true;
            this.lvAvailableUnits.HideSelection = false;
            this.lvAvailableUnits.Location = new System.Drawing.Point(3, 121);
            this.lvAvailableUnits.Name = "lvAvailableUnits";
            this.lvAvailableUnits.Size = new System.Drawing.Size(382, 186);
            this.lvAvailableUnits.TabIndex = 2;
            this.lvAvailableUnits.UseCompatibleStateImageBehavior = false;
            this.lvAvailableUnits.View = System.Windows.Forms.View.Details;
            this.lvAvailableUnits.SelectedIndexChanged += new System.EventHandler(this.lvAvailableUnits_SelectedIndexChanged);
            this.lvAvailableUnits.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvAvailableUnits_KeyDown);
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label3.Location = new System.Drawing.Point(389, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 16);
            this.label3.TabIndex = 5;
            this.label3.Text = "Command";
            // 
            // cmbCommand
            // 
            this.cmbCommand.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmbCommand.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbCommand.Location = new System.Drawing.Point(389, 25);
            this.cmbCommand.Name = "cmbCommand";
            this.cmbCommand.Size = new System.Drawing.Size(112, 21);
            this.cmbCommand.TabIndex = 6;
            this.cmbCommand.SelectedIndexChanged += new System.EventHandler(this.cmbCommand_SelectedIndexChanged);
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveSelected.Enabled = false;
            this.btnRemoveSelected.Location = new System.Drawing.Point(421, 153);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveSelected.TabIndex = 10;
            this.btnRemoveSelected.Text = "<";
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // btnAddAll
            // 
            this.btnAddAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAll.Enabled = false;
            this.btnAddAll.Location = new System.Drawing.Point(421, 89);
            this.btnAddAll.Name = "btnAddAll";
            this.btnAddAll.Size = new System.Drawing.Size(32, 24);
            this.btnAddAll.TabIndex = 8;
            this.btnAddAll.Text = ">>";
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            // 
            // btnAddSelected
            // 
            this.btnAddSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSelected.Enabled = false;
            this.btnAddSelected.Location = new System.Drawing.Point(421, 57);
            this.btnAddSelected.Name = "btnAddSelected";
            this.btnAddSelected.Size = new System.Drawing.Size(32, 24);
            this.btnAddSelected.TabIndex = 7;
            this.btnAddSelected.Text = ">";
            this.btnAddSelected.Click += new System.EventHandler(this.btnAddSelected_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveAll.Enabled = false;
            this.btnRemoveAll.Location = new System.Drawing.Point(421, 121);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveAll.TabIndex = 9;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(516, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 13;
            this.label2.Text = "Queued for Download";
            // 
            // lvQueued
            // 
            this.lvQueued.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvQueued.FullRowSelect = true;
            this.lvQueued.GridLines = true;
            this.lvQueued.HideSelection = false;
            this.lvQueued.Location = new System.Drawing.Point(547, 25);
            this.lvQueued.Name = "lvQueued";
            this.lvQueued.Size = new System.Drawing.Size(347, 425);
            this.lvQueued.TabIndex = 14;
            this.lvQueued.UseCompatibleStateImageBehavior = false;
            this.lvQueued.View = System.Windows.Forms.View.Details;
            this.lvQueued.SelectedIndexChanged += new System.EventHandler(this.lvQueued_SelectedIndexChanged);
            // 
            // btnAddToQueue
            // 
            this.btnAddToQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddToQueue.Enabled = false;
            this.btnAddToQueue.Location = new System.Drawing.Point(718, 457);
            this.btnAddToQueue.Name = "btnAddToQueue";
            this.btnAddToQueue.Size = new System.Drawing.Size(176, 24);
            this.btnAddToQueue.TabIndex = 15;
            this.btnAddToQueue.Text = "Add To Queue";
            this.btnAddToQueue.Click += new System.EventHandler(this.btnAddToQueue_Click);
            // 
            // lblAutoRefresh
            // 
            this.lblAutoRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAutoRefresh.Location = new System.Drawing.Point(151, 457);
            this.lblAutoRefresh.Name = "lblAutoRefresh";
            this.lblAutoRefresh.Size = new System.Drawing.Size(96, 18);
            this.lblAutoRefresh.TabIndex = 3;
            this.lblAutoRefresh.Text = "placeHolder";
            // 
            // gbQueuedFilter
            // 
            this.gbQueuedFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbQueuedFilter.Controls.Add(this.btnQueueFilter);
            this.gbQueuedFilter.Controls.Add(this.cmbQueueFilter2);
            this.gbQueuedFilter.Controls.Add(this.cmbQueueFilter1);
            this.gbQueuedFilter.Controls.Add(this.cmbQueuedStatus2);
            this.gbQueuedFilter.Controls.Add(this.cmbQueuedStatus1);
            this.gbQueuedFilter.Controls.Add(this.txtQueueFilter1);
            this.gbQueuedFilter.Controls.Add(this.txtQueueFilter2);
            this.gbQueuedFilter.Location = new System.Drawing.Point(3, 3);
            this.gbQueuedFilter.Name = "gbQueuedFilter";
            this.gbQueuedFilter.Size = new System.Drawing.Size(347, 78);
            this.gbQueuedFilter.TabIndex = 0;
            this.gbQueuedFilter.TabStop = false;
            this.gbQueuedFilter.Text = "Current Queue - Filter:";
            // 
            // btnQueueFilter
            // 
            this.btnQueueFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQueueFilter.Location = new System.Drawing.Point(284, 19);
            this.btnQueueFilter.Name = "btnQueueFilter";
            this.btnQueueFilter.Size = new System.Drawing.Size(57, 23);
            this.btnQueueFilter.TabIndex = 6;
            this.btnQueueFilter.Text = "Apply";
            this.btnQueueFilter.UseVisualStyleBackColor = true;
            this.btnQueueFilter.Click += new System.EventHandler(this.btnQueueFilter_Click);
            // 
            // cmbQueueFilter2
            // 
            this.cmbQueueFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueueFilter2.FormattingEnabled = true;
            this.cmbQueueFilter2.Location = new System.Drawing.Point(6, 51);
            this.cmbQueueFilter2.Name = "cmbQueueFilter2";
            this.cmbQueueFilter2.Size = new System.Drawing.Size(136, 21);
            this.cmbQueueFilter2.TabIndex = 3;
            this.cmbQueueFilter2.SelectedIndexChanged += new System.EventHandler(this.cmbQueueFilter2_SelectedIndexChanged);
            // 
            // cmbQueueFilter1
            // 
            this.cmbQueueFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueueFilter1.FormattingEnabled = true;
            this.cmbQueueFilter1.Location = new System.Drawing.Point(6, 22);
            this.cmbQueueFilter1.Name = "cmbQueueFilter1";
            this.cmbQueueFilter1.Size = new System.Drawing.Size(136, 21);
            this.cmbQueueFilter1.TabIndex = 0;
            this.cmbQueueFilter1.SelectedIndexChanged += new System.EventHandler(this.cmbQueueFilter1_SelectedIndexChanged);
            // 
            // cmbQueuedStatus2
            // 
            this.cmbQueuedStatus2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueuedStatus2.FormattingEnabled = true;
            this.cmbQueuedStatus2.Location = new System.Drawing.Point(148, 49);
            this.cmbQueuedStatus2.Name = "cmbQueuedStatus2";
            this.cmbQueuedStatus2.Size = new System.Drawing.Size(101, 21);
            this.cmbQueuedStatus2.TabIndex = 4;
            // 
            // cmbQueuedStatus1
            // 
            this.cmbQueuedStatus1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueuedStatus1.FormattingEnabled = true;
            this.cmbQueuedStatus1.Location = new System.Drawing.Point(148, 21);
            this.cmbQueuedStatus1.Name = "cmbQueuedStatus1";
            this.cmbQueuedStatus1.Size = new System.Drawing.Size(101, 21);
            this.cmbQueuedStatus1.TabIndex = 1;
            // 
            // txtQueueFilter1
            // 
            this.txtQueueFilter1.Location = new System.Drawing.Point(149, 21);
            this.txtQueueFilter1.Name = "txtQueueFilter1";
            this.txtQueueFilter1.Size = new System.Drawing.Size(100, 20);
            this.txtQueueFilter1.TabIndex = 2;
            // 
            // txtQueueFilter2
            // 
            this.txtQueueFilter2.Location = new System.Drawing.Point(148, 50);
            this.txtQueueFilter2.Name = "txtQueueFilter2";
            this.txtQueueFilter2.Size = new System.Drawing.Size(100, 20);
            this.txtQueueFilter2.TabIndex = 5;
            // 
            // lvExistingQueue
            // 
            this.lvExistingQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvExistingQueue.FullRowSelect = true;
            this.lvExistingQueue.GridLines = true;
            this.lvExistingQueue.HideSelection = false;
            this.lvExistingQueue.Location = new System.Drawing.Point(3, 89);
            this.lvExistingQueue.Name = "lvExistingQueue";
            this.lvExistingQueue.Size = new System.Drawing.Size(347, 356);
            this.lvExistingQueue.TabIndex = 1;
            this.lvExistingQueue.UseCompatibleStateImageBehavior = false;
            this.lvExistingQueue.View = System.Windows.Forms.View.Details;
            this.lvExistingQueue.SelectedIndexChanged += new System.EventHandler(this.lvExistingQueue_SelectedIndexChanged);
            this.lvExistingQueue.KeyDown += new System.Windows.Forms.KeyEventHandler(this.lvExistingQueue_KeyDown);
            // 
            // btnAbortMessage
            // 
            this.btnAbortMessage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAbortMessage.Enabled = false;
            this.btnAbortMessage.Location = new System.Drawing.Point(3, 457);
            this.btnAbortMessage.Name = "btnAbortMessage";
            this.btnAbortMessage.Size = new System.Drawing.Size(97, 24);
            this.btnAbortMessage.TabIndex = 2;
            this.btnAbortMessage.Text = "Abort Message";
            this.btnAbortMessage.Click += new System.EventHandler(this.btnAbortMessage_Click);
            // 
            // btnRefreshExistingQueue
            // 
            this.btnRefreshExistingQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshExistingQueue.Location = new System.Drawing.Point(253, 457);
            this.btnRefreshExistingQueue.Name = "btnRefreshExistingQueue";
            this.btnRefreshExistingQueue.Size = new System.Drawing.Size(97, 24);
            this.btnRefreshExistingQueue.TabIndex = 4;
            this.btnRefreshExistingQueue.Text = "Refresh Now";
            this.btnRefreshExistingQueue.Click += new System.EventHandler(this.btnRefreshExistingQueue_Click);
            // 
            // ucBulkMessages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitMain);
            this.Name = "ucBulkMessages";
            this.Size = new System.Drawing.Size(1262, 488);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.IapDataBlocksGroupBox.ResumeLayout(false);
            this.IapDataBlocksGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.BlockDataBlockNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SpeedDataBlockNumericUpDown)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PositionDataBlocksNumericUpDown)).EndInit();
            this.gbMessageEndDate.ResumeLayout(false);
            this.gbMessageStartDate.ResumeLayout(false);
            this.gbAvailableFilter.ResumeLayout(false);
            this.gbAvailableFilter.PerformLayout();
            this.gbNetworkInfo.ResumeLayout(false);
            this.gbNetworkInfo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSecIPPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewPingRetries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewIPPort)).EndInit();
            this.gbQueuedFilter.ResumeLayout(false);
            this.gbQueuedFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.GroupBox gbAvailableFilter;
        private System.Windows.Forms.ComboBox cmbBoolFilter2;
        private System.Windows.Forms.ComboBox cmbBoolFilter1;
        private System.Windows.Forms.Button btnAvailableFilter;
        private System.Windows.Forms.TextBox txtFilter2;
        private System.Windows.Forms.ComboBox cmbAvailFilter2;
        private System.Windows.Forms.TextBox txtFilter1;
        private System.Windows.Forms.ComboBox cmbAvailFilter1;
        private System.Windows.Forms.GroupBox gbNetworkInfo;
        private MaskedTextBox txtPingIP;
        private MaskedTextBox txtSecIP;
        private System.Windows.Forms.NumericUpDown numSecIPPort;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numNewPingRetries;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numNewIPPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.CheckBox chkUseHostNameLookup;
        private System.Windows.Forms.TextBox txtHostName;
        private MaskedTextBox txtNewIP;
        private System.Windows.Forms.Button btnRefershAvaliableList;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvAvailableUnits;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbCommand;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.Button btnAddAll;
        private System.Windows.Forms.Button btnAddSelected;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvQueued;
        private System.Windows.Forms.Button btnAddToQueue;
        private System.Windows.Forms.GroupBox gbQueuedFilter;
        private System.Windows.Forms.ListView lvExistingQueue;
        private System.Windows.Forms.Button btnAbortMessage;
        private System.Windows.Forms.Button btnRefreshExistingQueue;
        private System.Windows.Forms.Label lblAutoRefresh;
        private System.Windows.Forms.Button btnQueueFilter;
        private System.Windows.Forms.ComboBox cmbQueuedStatus2;
        private System.Windows.Forms.ComboBox cmbQueueFilter2;
        private System.Windows.Forms.ComboBox cmbQueuedStatus1;
        private System.Windows.Forms.ComboBox cmbQueueFilter1;
        private System.Windows.Forms.GroupBox gbMessageEndDate;
        private System.Windows.Forms.DateTimePicker dtpExpiryDate;
        private System.Windows.Forms.GroupBox gbMessageStartDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.TextBox txtQueueFilter1;
        private System.Windows.Forms.TextBox txtQueueFilter2;
        private System.Windows.Forms.GroupBox IapDataBlocksGroupBox;
        private System.Windows.Forms.NumericUpDown BlockDataBlockNumericUpDown;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.NumericUpDown SpeedDataBlockNumericUpDown;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown PositionDataBlocksNumericUpDown;
        private System.Windows.Forms.Label label10;

    }
}

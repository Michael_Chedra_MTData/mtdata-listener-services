﻿namespace MTData.Transport.Listener.DatsListenerApp.UserControls
{
    partial class ucDownloads
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.gbMessageEndDate = new System.Windows.Forms.GroupBox();
            this.dtpExpiryDate = new System.Windows.Forms.DateTimePicker();
            this.gbMessageStartDate = new System.Windows.Forms.GroupBox();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.gbDownloadFile = new System.Windows.Forms.GroupBox();
            this.cmbDownloadFile = new System.Windows.Forms.ComboBox();
            this.gbAvailableFilter = new System.Windows.Forms.GroupBox();
            this.chkFilter2EqNEq = new System.Windows.Forms.CheckBox();
            this.chkFilter1EqNEq = new System.Windows.Forms.CheckBox();
            this.cmbBoolFilter2 = new System.Windows.Forms.ComboBox();
            this.cmbBoolFilter1 = new System.Windows.Forms.ComboBox();
            this.btnAvailableFilter = new System.Windows.Forms.Button();
            this.txtFilter2 = new System.Windows.Forms.TextBox();
            this.cmbAvailFilter2 = new System.Windows.Forms.ComboBox();
            this.txtFilter1 = new System.Windows.Forms.TextBox();
            this.cmbAvailFilter1 = new System.Windows.Forms.ComboBox();
            this.chkContinueWithoutWaiting = new System.Windows.Forms.CheckBox();
            this.txtEmailTo = new System.Windows.Forms.TextBox();
            this.btnRefershAvaliableList = new System.Windows.Forms.Button();
            this.lblAbourtSuffix = new System.Windows.Forms.Label();
            this.numTimeoutDownloadMins = new System.Windows.Forms.NumericUpDown();
            this.lblAbortPrefix = new System.Windows.Forms.Label();
            this.btnSendToUnits = new System.Windows.Forms.Button();
            this.label8 = new System.Windows.Forms.Label();
            this.numSimultaneousDownloads = new System.Windows.Forms.NumericUpDown();
            this.gbDownloadType = new System.Windows.Forms.GroupBox();
            this.cmbDownloadType = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lvQueued = new System.Windows.Forms.ListView();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnAddAll = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddSelected = new System.Windows.Forms.Button();
            this.lvAvailableUnits = new System.Windows.Forms.ListView();
            this.lblAutoRefresh = new System.Windows.Forms.Label();
            this.btnAbortDownload = new System.Windows.Forms.Button();
            this.btnRefreshExistingQueue = new System.Windows.Forms.Button();
            this.gbQueuedFilter = new System.Windows.Forms.GroupBox();
            this.btnQueueFilter = new System.Windows.Forms.Button();
            this.cmbBoolQueuedFilter2 = new System.Windows.Forms.ComboBox();
            this.cmbQueueFilter2 = new System.Windows.Forms.ComboBox();
            this.cmbBoolQueuedFilter1 = new System.Windows.Forms.ComboBox();
            this.cmbQueueFilter1 = new System.Windows.Forms.ComboBox();
            this.cmbQueuedStatus2 = new System.Windows.Forms.ComboBox();
            this.cmbQueuedStatus1 = new System.Windows.Forms.ComboBox();
            this.txtQueueFilter2 = new System.Windows.Forms.TextBox();
            this.txtQueueFilter1 = new System.Windows.Forms.TextBox();
            this.lvExistingQueue = new System.Windows.Forms.ListView();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            this.gbMessageEndDate.SuspendLayout();
            this.gbMessageStartDate.SuspendLayout();
            this.gbDownloadFile.SuspendLayout();
            this.gbAvailableFilter.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeoutDownloadMins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSimultaneousDownloads)).BeginInit();
            this.gbDownloadType.SuspendLayout();
            this.gbQueuedFilter.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitMain.Location = new System.Drawing.Point(0, 0);
            this.splitMain.Name = "splitMain";
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.gbMessageEndDate);
            this.splitMain.Panel1.Controls.Add(this.gbMessageStartDate);
            this.splitMain.Panel1.Controls.Add(this.gbDownloadFile);
            this.splitMain.Panel1.Controls.Add(this.gbAvailableFilter);
            this.splitMain.Panel1.Controls.Add(this.chkContinueWithoutWaiting);
            this.splitMain.Panel1.Controls.Add(this.txtEmailTo);
            this.splitMain.Panel1.Controls.Add(this.btnRefershAvaliableList);
            this.splitMain.Panel1.Controls.Add(this.lblAbourtSuffix);
            this.splitMain.Panel1.Controls.Add(this.numTimeoutDownloadMins);
            this.splitMain.Panel1.Controls.Add(this.lblAbortPrefix);
            this.splitMain.Panel1.Controls.Add(this.btnSendToUnits);
            this.splitMain.Panel1.Controls.Add(this.label8);
            this.splitMain.Panel1.Controls.Add(this.numSimultaneousDownloads);
            this.splitMain.Panel1.Controls.Add(this.gbDownloadType);
            this.splitMain.Panel1.Controls.Add(this.label2);
            this.splitMain.Panel1.Controls.Add(this.lvQueued);
            this.splitMain.Panel1.Controls.Add(this.btnRemoveSelected);
            this.splitMain.Panel1.Controls.Add(this.btnRemoveAll);
            this.splitMain.Panel1.Controls.Add(this.btnAddAll);
            this.splitMain.Panel1.Controls.Add(this.label1);
            this.splitMain.Panel1.Controls.Add(this.btnAddSelected);
            this.splitMain.Panel1.Controls.Add(this.lvAvailableUnits);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.lblAutoRefresh);
            this.splitMain.Panel2.Controls.Add(this.btnAbortDownload);
            this.splitMain.Panel2.Controls.Add(this.btnRefreshExistingQueue);
            this.splitMain.Panel2.Controls.Add(this.gbQueuedFilter);
            this.splitMain.Panel2.Controls.Add(this.lvExistingQueue);
            this.splitMain.Size = new System.Drawing.Size(1183, 487);
            this.splitMain.SplitterDistance = 816;
            this.splitMain.TabIndex = 66;
            this.splitMain.SplitterMoved += new System.Windows.Forms.SplitterEventHandler(this.splitMain_SplitterMoved);
            // 
            // gbMessageEndDate
            // 
            this.gbMessageEndDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbMessageEndDate.Controls.Add(this.dtpExpiryDate);
            this.gbMessageEndDate.Location = new System.Drawing.Point(325, 219);
            this.gbMessageEndDate.Name = "gbMessageEndDate";
            this.gbMessageEndDate.Size = new System.Drawing.Size(138, 47);
            this.gbMessageEndDate.TabIndex = 89;
            this.gbMessageEndDate.TabStop = false;
            this.gbMessageEndDate.Text = "Expiry Date";
            // 
            // dtpExpiryDate
            // 
            this.dtpExpiryDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpExpiryDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpExpiryDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpExpiryDate.Location = new System.Drawing.Point(4, 19);
            this.dtpExpiryDate.Name = "dtpExpiryDate";
            this.dtpExpiryDate.Size = new System.Drawing.Size(125, 20);
            this.dtpExpiryDate.TabIndex = 1;
            // 
            // gbMessageStartDate
            // 
            this.gbMessageStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.gbMessageStartDate.Controls.Add(this.dtpStartDate);
            this.gbMessageStartDate.Location = new System.Drawing.Point(325, 166);
            this.gbMessageStartDate.Name = "gbMessageStartDate";
            this.gbMessageStartDate.Size = new System.Drawing.Size(138, 47);
            this.gbMessageStartDate.TabIndex = 88;
            this.gbMessageStartDate.TabStop = false;
            this.gbMessageStartDate.Text = "Start Date";
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dtpStartDate.CustomFormat = "dd/MM/yyyy HH:mm";
            this.dtpStartDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpStartDate.Location = new System.Drawing.Point(7, 20);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(125, 20);
            this.dtpStartDate.TabIndex = 0;
            // 
            // gbDownloadFile
            // 
            this.gbDownloadFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbDownloadFile.Controls.Add(this.cmbDownloadFile);
            this.gbDownloadFile.Location = new System.Drawing.Point(145, 360);
            this.gbDownloadFile.Name = "gbDownloadFile";
            this.gbDownloadFile.Size = new System.Drawing.Size(186, 47);
            this.gbDownloadFile.TabIndex = 87;
            this.gbDownloadFile.TabStop = false;
            this.gbDownloadFile.Text = "Download File";
            // 
            // cmbDownloadFile
            // 
            this.cmbDownloadFile.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbDownloadFile.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDownloadFile.FormattingEnabled = true;
            this.cmbDownloadFile.Location = new System.Drawing.Point(3, 16);
            this.cmbDownloadFile.Name = "cmbDownloadFile";
            this.cmbDownloadFile.Size = new System.Drawing.Size(180, 21);
            this.cmbDownloadFile.TabIndex = 1;
            this.cmbDownloadFile.SelectedIndexChanged += new System.EventHandler(this.cmbDownloadFile_SelectedIndexChanged);
            // 
            // gbAvailableFilter
            // 
            this.gbAvailableFilter.Controls.Add(this.chkFilter2EqNEq);
            this.gbAvailableFilter.Controls.Add(this.chkFilter1EqNEq);
            this.gbAvailableFilter.Controls.Add(this.cmbBoolFilter2);
            this.gbAvailableFilter.Controls.Add(this.cmbBoolFilter1);
            this.gbAvailableFilter.Controls.Add(this.btnAvailableFilter);
            this.gbAvailableFilter.Controls.Add(this.txtFilter2);
            this.gbAvailableFilter.Controls.Add(this.cmbAvailFilter2);
            this.gbAvailableFilter.Controls.Add(this.txtFilter1);
            this.gbAvailableFilter.Controls.Add(this.cmbAvailFilter1);
            this.gbAvailableFilter.Location = new System.Drawing.Point(6, 19);
            this.gbAvailableFilter.Name = "gbAvailableFilter";
            this.gbAvailableFilter.Size = new System.Drawing.Size(317, 81);
            this.gbAvailableFilter.TabIndex = 86;
            this.gbAvailableFilter.TabStop = false;
            this.gbAvailableFilter.Text = "Filter:";
            // 
            // chkFilter2EqNEq
            // 
            this.chkFilter2EqNEq.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkFilter2EqNEq.AutoSize = true;
            this.chkFilter2EqNEq.Location = new System.Drawing.Point(149, 47);
            this.chkFilter2EqNEq.Name = "chkFilter2EqNEq";
            this.chkFilter2EqNEq.Size = new System.Drawing.Size(23, 23);
            this.chkFilter2EqNEq.TabIndex = 8;
            this.chkFilter2EqNEq.Text = "=";
            this.chkFilter2EqNEq.UseVisualStyleBackColor = true;
            this.chkFilter2EqNEq.CheckedChanged += new System.EventHandler(this.chkFilter2EqNEq_CheckedChanged);
            // 
            // chkFilter1EqNEq
            // 
            this.chkFilter1EqNEq.Appearance = System.Windows.Forms.Appearance.Button;
            this.chkFilter1EqNEq.AutoSize = true;
            this.chkFilter1EqNEq.Location = new System.Drawing.Point(149, 20);
            this.chkFilter1EqNEq.Name = "chkFilter1EqNEq";
            this.chkFilter1EqNEq.Size = new System.Drawing.Size(23, 23);
            this.chkFilter1EqNEq.TabIndex = 7;
            this.chkFilter1EqNEq.Text = "=";
            this.chkFilter1EqNEq.UseVisualStyleBackColor = true;
            this.chkFilter1EqNEq.CheckedChanged += new System.EventHandler(this.chkFilter1EqNEq_CheckedChanged);
            // 
            // cmbBoolFilter2
            // 
            this.cmbBoolFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoolFilter2.FormattingEnabled = true;
            this.cmbBoolFilter2.Location = new System.Drawing.Point(182, 47);
            this.cmbBoolFilter2.Name = "cmbBoolFilter2";
            this.cmbBoolFilter2.Size = new System.Drawing.Size(68, 21);
            this.cmbBoolFilter2.TabIndex = 6;
            // 
            // cmbBoolFilter1
            // 
            this.cmbBoolFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoolFilter1.FormattingEnabled = true;
            this.cmbBoolFilter1.Location = new System.Drawing.Point(182, 18);
            this.cmbBoolFilter1.Name = "cmbBoolFilter1";
            this.cmbBoolFilter1.Size = new System.Drawing.Size(68, 21);
            this.cmbBoolFilter1.TabIndex = 5;
            // 
            // btnAvailableFilter
            // 
            this.btnAvailableFilter.Location = new System.Drawing.Point(256, 20);
            this.btnAvailableFilter.Name = "btnAvailableFilter";
            this.btnAvailableFilter.Size = new System.Drawing.Size(57, 23);
            this.btnAvailableFilter.TabIndex = 4;
            this.btnAvailableFilter.Text = "Apply";
            this.btnAvailableFilter.UseVisualStyleBackColor = true;
            this.btnAvailableFilter.Click += new System.EventHandler(this.btnAvailableFilter_Click);
            // 
            // txtFilter2
            // 
            this.txtFilter2.Location = new System.Drawing.Point(182, 46);
            this.txtFilter2.Name = "txtFilter2";
            this.txtFilter2.Size = new System.Drawing.Size(67, 20);
            this.txtFilter2.TabIndex = 3;
            // 
            // cmbAvailFilter2
            // 
            this.cmbAvailFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAvailFilter2.FormattingEnabled = true;
            this.cmbAvailFilter2.Location = new System.Drawing.Point(7, 47);
            this.cmbAvailFilter2.Name = "cmbAvailFilter2";
            this.cmbAvailFilter2.Size = new System.Drawing.Size(136, 21);
            this.cmbAvailFilter2.TabIndex = 2;
            this.cmbAvailFilter2.SelectedIndexChanged += new System.EventHandler(this.cmbAvailFilter2_SelectedIndexChanged);
            // 
            // txtFilter1
            // 
            this.txtFilter1.Location = new System.Drawing.Point(182, 19);
            this.txtFilter1.Name = "txtFilter1";
            this.txtFilter1.Size = new System.Drawing.Size(67, 20);
            this.txtFilter1.TabIndex = 1;
            // 
            // cmbAvailFilter1
            // 
            this.cmbAvailFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbAvailFilter1.FormattingEnabled = true;
            this.cmbAvailFilter1.Location = new System.Drawing.Point(7, 20);
            this.cmbAvailFilter1.Name = "cmbAvailFilter1";
            this.cmbAvailFilter1.Size = new System.Drawing.Size(136, 21);
            this.cmbAvailFilter1.TabIndex = 0;
            this.cmbAvailFilter1.SelectedIndexChanged += new System.EventHandler(this.cmbAvailFilter1_SelectedIndexChanged);
            // 
            // chkContinueWithoutWaiting
            // 
            this.chkContinueWithoutWaiting.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkContinueWithoutWaiting.Location = new System.Drawing.Point(340, 391);
            this.chkContinueWithoutWaiting.Name = "chkContinueWithoutWaiting";
            this.chkContinueWithoutWaiting.Size = new System.Drawing.Size(240, 16);
            this.chkContinueWithoutWaiting.TabIndex = 85;
            this.chkContinueWithoutWaiting.Text = "Continue without waiting for unit to re-login";
            // 
            // txtEmailTo
            // 
            this.txtEmailTo.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtEmailTo.Location = new System.Drawing.Point(88, 429);
            this.txtEmailTo.Name = "txtEmailTo";
            this.txtEmailTo.Size = new System.Drawing.Size(640, 20);
            this.txtEmailTo.TabIndex = 78;
            this.txtEmailTo.TextChanged += new System.EventHandler(this.txtEmailTo_TextChanged);
            // 
            // btnRefershAvaliableList
            // 
            this.btnRefershAvaliableList.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefershAvaliableList.Location = new System.Drawing.Point(3, 320);
            this.btnRefershAvaliableList.Name = "btnRefershAvaliableList";
            this.btnRefershAvaliableList.Size = new System.Drawing.Size(320, 24);
            this.btnRefershAvaliableList.TabIndex = 73;
            this.btnRefershAvaliableList.Text = "Refresh Avaliable List";
            this.btnRefershAvaliableList.Click += new System.EventHandler(this.btnRefershAvaliableList_Click);
            // 
            // lblAbourtSuffix
            // 
            this.lblAbourtSuffix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAbourtSuffix.Location = new System.Drawing.Point(575, 362);
            this.lblAbourtSuffix.Name = "lblAbourtSuffix";
            this.lblAbourtSuffix.Size = new System.Drawing.Size(32, 16);
            this.lblAbourtSuffix.TabIndex = 81;
            this.lblAbourtSuffix.Text = "mins.";
            // 
            // numTimeoutDownloadMins
            // 
            this.numTimeoutDownloadMins.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numTimeoutDownloadMins.Location = new System.Drawing.Point(513, 360);
            this.numTimeoutDownloadMins.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numTimeoutDownloadMins.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numTimeoutDownloadMins.Name = "numTimeoutDownloadMins";
            this.numTimeoutDownloadMins.Size = new System.Drawing.Size(56, 20);
            this.numTimeoutDownloadMins.TabIndex = 76;
            this.numTimeoutDownloadMins.Value = new decimal(new int[] {
            30,
            0,
            0,
            0});
            // 
            // lblAbortPrefix
            // 
            this.lblAbortPrefix.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.lblAbortPrefix.Location = new System.Drawing.Point(337, 360);
            this.lblAbortPrefix.Name = "lblAbortPrefix";
            this.lblAbortPrefix.Size = new System.Drawing.Size(176, 16);
            this.lblAbortPrefix.TabIndex = 79;
            this.lblAbortPrefix.Text = "Abort download if not complete in ";
            // 
            // btnSendToUnits
            // 
            this.btnSendToUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendToUnits.Location = new System.Drawing.Point(709, 346);
            this.btnSendToUnits.Name = "btnSendToUnits";
            this.btnSendToUnits.Size = new System.Drawing.Size(96, 24);
            this.btnSendToUnits.TabIndex = 82;
            this.btnSendToUnits.Text = "Send To Units";
            this.btnSendToUnits.Click += new System.EventHandler(this.btnSendToUnits_Click);
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.Location = new System.Drawing.Point(0, 429);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 83;
            this.label8.Text = "Email Status to";
            // 
            // numSimultaneousDownloads
            // 
            this.numSimultaneousDownloads.Location = new System.Drawing.Point(176, 429);
            this.numSimultaneousDownloads.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSimultaneousDownloads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSimultaneousDownloads.Name = "numSimultaneousDownloads";
            this.numSimultaneousDownloads.Size = new System.Drawing.Size(56, 20);
            this.numSimultaneousDownloads.TabIndex = 77;
            this.numSimultaneousDownloads.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSimultaneousDownloads.Visible = false;
            // 
            // gbDownloadType
            // 
            this.gbDownloadType.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.gbDownloadType.Controls.Add(this.cmbDownloadType);
            this.gbDownloadType.Location = new System.Drawing.Point(3, 360);
            this.gbDownloadType.Name = "gbDownloadType";
            this.gbDownloadType.Size = new System.Drawing.Size(136, 47);
            this.gbDownloadType.TabIndex = 75;
            this.gbDownloadType.TabStop = false;
            this.gbDownloadType.Text = "Download Type";
            // 
            // cmbDownloadType
            // 
            this.cmbDownloadType.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbDownloadType.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDownloadType.FormattingEnabled = true;
            this.cmbDownloadType.Location = new System.Drawing.Point(3, 16);
            this.cmbDownloadType.Name = "cmbDownloadType";
            this.cmbDownloadType.Size = new System.Drawing.Size(130, 21);
            this.cmbDownloadType.TabIndex = 0;
            this.cmbDownloadType.SelectedIndexChanged += new System.EventHandler(this.cmbDownloadType_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.label2.Location = new System.Drawing.Point(445, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 74;
            this.label2.Text = "Queued for Download";
            // 
            // lvQueued
            // 
            this.lvQueued.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvQueued.FullRowSelect = true;
            this.lvQueued.GridLines = true;
            this.lvQueued.HideSelection = false;
            this.lvQueued.Location = new System.Drawing.Point(491, 16);
            this.lvQueued.Name = "lvQueued";
            this.lvQueued.Size = new System.Drawing.Size(314, 324);
            this.lvQueued.TabIndex = 72;
            this.lvQueued.UseCompatibleStateImageBehavior = false;
            this.lvQueued.View = System.Windows.Forms.View.Details;
            this.lvQueued.SelectedIndexChanged += new System.EventHandler(this.lvQueued_SelectedIndexChanged);
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveSelected.Location = new System.Drawing.Point(367, 136);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveSelected.TabIndex = 71;
            this.btnRemoveSelected.Text = "<";
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRemoveAll.Location = new System.Drawing.Point(367, 104);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveAll.TabIndex = 70;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnAddAll
            // 
            this.btnAddAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddAll.Location = new System.Drawing.Point(367, 72);
            this.btnAddAll.Name = "btnAddAll";
            this.btnAddAll.Size = new System.Drawing.Size(32, 24);
            this.btnAddAll.TabIndex = 69;
            this.btnAddAll.Text = ">>";
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 68;
            this.label1.Text = "Available Units";
            // 
            // btnAddSelected
            // 
            this.btnAddSelected.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAddSelected.Location = new System.Drawing.Point(367, 40);
            this.btnAddSelected.Name = "btnAddSelected";
            this.btnAddSelected.Size = new System.Drawing.Size(32, 24);
            this.btnAddSelected.TabIndex = 67;
            this.btnAddSelected.Text = ">";
            this.btnAddSelected.Click += new System.EventHandler(this.btnAddSelected_Click);
            // 
            // lvAvailableUnits
            // 
            this.lvAvailableUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvAvailableUnits.FullRowSelect = true;
            this.lvAvailableUnits.GridLines = true;
            this.lvAvailableUnits.HideSelection = false;
            this.lvAvailableUnits.Location = new System.Drawing.Point(3, 104);
            this.lvAvailableUnits.Name = "lvAvailableUnits";
            this.lvAvailableUnits.Size = new System.Drawing.Size(320, 208);
            this.lvAvailableUnits.TabIndex = 66;
            this.lvAvailableUnits.UseCompatibleStateImageBehavior = false;
            this.lvAvailableUnits.View = System.Windows.Forms.View.Details;
            this.lvAvailableUnits.SelectedIndexChanged += new System.EventHandler(this.lvAvailableUnits_SelectedIndexChanged);
            // 
            // lblAutoRefresh
            // 
            this.lblAutoRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.lblAutoRefresh.Location = new System.Drawing.Point(147, 452);
            this.lblAutoRefresh.Name = "lblAutoRefresh";
            this.lblAutoRefresh.Size = new System.Drawing.Size(96, 18);
            this.lblAutoRefresh.TabIndex = 67;
            this.lblAutoRefresh.Text = "placeHolder";
            // 
            // btnAbortDownload
            // 
            this.btnAbortDownload.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnAbortDownload.Enabled = false;
            this.btnAbortDownload.Location = new System.Drawing.Point(3, 452);
            this.btnAbortDownload.Name = "btnAbortDownload";
            this.btnAbortDownload.Size = new System.Drawing.Size(97, 24);
            this.btnAbortDownload.TabIndex = 66;
            this.btnAbortDownload.Text = "Abort Download";
            this.btnAbortDownload.Click += new System.EventHandler(this.btnAbortMessage_Click);
            // 
            // btnRefreshExistingQueue
            // 
            this.btnRefreshExistingQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefreshExistingQueue.Location = new System.Drawing.Point(249, 452);
            this.btnRefreshExistingQueue.Name = "btnRefreshExistingQueue";
            this.btnRefreshExistingQueue.Size = new System.Drawing.Size(97, 24);
            this.btnRefreshExistingQueue.TabIndex = 65;
            this.btnRefreshExistingQueue.Text = "Refresh Now";
            this.btnRefreshExistingQueue.Click += new System.EventHandler(this.btnRefreshExistingQueue_Click);
            // 
            // gbQueuedFilter
            // 
            this.gbQueuedFilter.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbQueuedFilter.Controls.Add(this.btnQueueFilter);
            this.gbQueuedFilter.Controls.Add(this.cmbBoolQueuedFilter2);
            this.gbQueuedFilter.Controls.Add(this.cmbQueueFilter2);
            this.gbQueuedFilter.Controls.Add(this.cmbBoolQueuedFilter1);
            this.gbQueuedFilter.Controls.Add(this.cmbQueueFilter1);
            this.gbQueuedFilter.Controls.Add(this.cmbQueuedStatus2);
            this.gbQueuedFilter.Controls.Add(this.cmbQueuedStatus1);
            this.gbQueuedFilter.Controls.Add(this.txtQueueFilter2);
            this.gbQueuedFilter.Controls.Add(this.txtQueueFilter1);
            this.gbQueuedFilter.Location = new System.Drawing.Point(3, 3);
            this.gbQueuedFilter.Name = "gbQueuedFilter";
            this.gbQueuedFilter.Size = new System.Drawing.Size(343, 78);
            this.gbQueuedFilter.TabIndex = 64;
            this.gbQueuedFilter.TabStop = false;
            this.gbQueuedFilter.Text = "Current Queue - Filter:";
            // 
            // btnQueueFilter
            // 
            this.btnQueueFilter.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnQueueFilter.Location = new System.Drawing.Point(280, 19);
            this.btnQueueFilter.Name = "btnQueueFilter";
            this.btnQueueFilter.Size = new System.Drawing.Size(57, 23);
            this.btnQueueFilter.TabIndex = 10;
            this.btnQueueFilter.Text = "Apply";
            this.btnQueueFilter.UseVisualStyleBackColor = true;
            this.btnQueueFilter.Click += new System.EventHandler(this.btnQueueFilter_Click);
            // 
            // cmbBoolQueuedFilter2
            // 
            this.cmbBoolQueuedFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoolQueuedFilter2.FormattingEnabled = true;
            this.cmbBoolQueuedFilter2.Location = new System.Drawing.Point(148, 49);
            this.cmbBoolQueuedFilter2.Name = "cmbBoolQueuedFilter2";
            this.cmbBoolQueuedFilter2.Size = new System.Drawing.Size(101, 21);
            this.cmbBoolQueuedFilter2.TabIndex = 9;
            // 
            // cmbQueueFilter2
            // 
            this.cmbQueueFilter2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueueFilter2.FormattingEnabled = true;
            this.cmbQueueFilter2.Location = new System.Drawing.Point(6, 51);
            this.cmbQueueFilter2.Name = "cmbQueueFilter2";
            this.cmbQueueFilter2.Size = new System.Drawing.Size(136, 21);
            this.cmbQueueFilter2.TabIndex = 8;
            this.cmbQueueFilter2.SelectedIndexChanged += new System.EventHandler(this.cmbQueueFilter2_SelectedIndexChanged);
            // 
            // cmbBoolQueuedFilter1
            // 
            this.cmbBoolQueuedFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbBoolQueuedFilter1.FormattingEnabled = true;
            this.cmbBoolQueuedFilter1.Location = new System.Drawing.Point(148, 20);
            this.cmbBoolQueuedFilter1.Name = "cmbBoolQueuedFilter1";
            this.cmbBoolQueuedFilter1.Size = new System.Drawing.Size(101, 21);
            this.cmbBoolQueuedFilter1.TabIndex = 7;
            this.cmbBoolQueuedFilter1.SelectedIndexChanged += new System.EventHandler(this.cmbBoolQueuedFilter1_SelectedIndexChanged);
            // 
            // cmbQueueFilter1
            // 
            this.cmbQueueFilter1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueueFilter1.FormattingEnabled = true;
            this.cmbQueueFilter1.Location = new System.Drawing.Point(6, 22);
            this.cmbQueueFilter1.Name = "cmbQueueFilter1";
            this.cmbQueueFilter1.Size = new System.Drawing.Size(136, 21);
            this.cmbQueueFilter1.TabIndex = 6;
            this.cmbQueueFilter1.SelectedIndexChanged += new System.EventHandler(this.cmbQueueFilter1_SelectedIndexChanged);
            // 
            // cmbQueuedStatus2
            // 
            this.cmbQueuedStatus2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueuedStatus2.FormattingEnabled = true;
            this.cmbQueuedStatus2.Location = new System.Drawing.Point(148, 50);
            this.cmbQueuedStatus2.Name = "cmbQueuedStatus2";
            this.cmbQueuedStatus2.Size = new System.Drawing.Size(101, 21);
            this.cmbQueuedStatus2.TabIndex = 12;
            // 
            // cmbQueuedStatus1
            // 
            this.cmbQueuedStatus1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbQueuedStatus1.FormattingEnabled = true;
            this.cmbQueuedStatus1.Location = new System.Drawing.Point(148, 21);
            this.cmbQueuedStatus1.Name = "cmbQueuedStatus1";
            this.cmbQueuedStatus1.Size = new System.Drawing.Size(101, 21);
            this.cmbQueuedStatus1.TabIndex = 11;
            // 
            // txtQueueFilter2
            // 
            this.txtQueueFilter2.Location = new System.Drawing.Point(149, 50);
            this.txtQueueFilter2.Name = "txtQueueFilter2";
            this.txtQueueFilter2.Size = new System.Drawing.Size(100, 20);
            this.txtQueueFilter2.TabIndex = 14;
            // 
            // txtQueueFilter1
            // 
            this.txtQueueFilter1.Location = new System.Drawing.Point(149, 20);
            this.txtQueueFilter1.Name = "txtQueueFilter1";
            this.txtQueueFilter1.Size = new System.Drawing.Size(100, 20);
            this.txtQueueFilter1.TabIndex = 13;
            // 
            // lvExistingQueue
            // 
            this.lvExistingQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvExistingQueue.FullRowSelect = true;
            this.lvExistingQueue.GridLines = true;
            this.lvExistingQueue.HideSelection = false;
            this.lvExistingQueue.Location = new System.Drawing.Point(3, 89);
            this.lvExistingQueue.Name = "lvExistingQueue";
            this.lvExistingQueue.Size = new System.Drawing.Size(343, 352);
            this.lvExistingQueue.TabIndex = 63;
            this.lvExistingQueue.UseCompatibleStateImageBehavior = false;
            this.lvExistingQueue.View = System.Windows.Forms.View.Details;
            this.lvExistingQueue.SelectedIndexChanged += new System.EventHandler(this.lvExistingQueue_SelectedIndexChanged);
            // 
            // ucDownloads
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.splitMain);
            this.Name = "ucDownloads";
            this.Size = new System.Drawing.Size(1183, 487);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel1.PerformLayout();
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.gbMessageEndDate.ResumeLayout(false);
            this.gbMessageStartDate.ResumeLayout(false);
            this.gbDownloadFile.ResumeLayout(false);
            this.gbAvailableFilter.ResumeLayout(false);
            this.gbAvailableFilter.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeoutDownloadMins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSimultaneousDownloads)).EndInit();
            this.gbDownloadType.ResumeLayout(false);
            this.gbQueuedFilter.ResumeLayout(false);
            this.gbQueuedFilter.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.GroupBox gbMessageEndDate;
        private System.Windows.Forms.DateTimePicker dtpExpiryDate;
        private System.Windows.Forms.GroupBox gbMessageStartDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.GroupBox gbDownloadFile;
        private System.Windows.Forms.ComboBox cmbDownloadFile;
        private System.Windows.Forms.GroupBox gbAvailableFilter;
        private System.Windows.Forms.ComboBox cmbBoolFilter2;
        private System.Windows.Forms.ComboBox cmbBoolFilter1;
        private System.Windows.Forms.Button btnAvailableFilter;
        private System.Windows.Forms.TextBox txtFilter2;
        private System.Windows.Forms.ComboBox cmbAvailFilter2;
        private System.Windows.Forms.TextBox txtFilter1;
        private System.Windows.Forms.ComboBox cmbAvailFilter1;
        private System.Windows.Forms.CheckBox chkContinueWithoutWaiting;
        private System.Windows.Forms.TextBox txtEmailTo;
        private System.Windows.Forms.Button btnRefershAvaliableList;
        private System.Windows.Forms.Label lblAbourtSuffix;
        private System.Windows.Forms.NumericUpDown numTimeoutDownloadMins;
        private System.Windows.Forms.Label lblAbortPrefix;
        private System.Windows.Forms.Button btnSendToUnits;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numSimultaneousDownloads;
        private System.Windows.Forms.GroupBox gbDownloadType;
        private System.Windows.Forms.ComboBox cmbDownloadType;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvQueued;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnAddAll;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnAddSelected;
        private System.Windows.Forms.ListView lvAvailableUnits;
        private System.Windows.Forms.Label lblAutoRefresh;
        private System.Windows.Forms.Button btnAbortDownload;
        private System.Windows.Forms.Button btnRefreshExistingQueue;
        private System.Windows.Forms.GroupBox gbQueuedFilter;
        private System.Windows.Forms.Button btnQueueFilter;
        private System.Windows.Forms.ComboBox cmbBoolQueuedFilter2;
        private System.Windows.Forms.ComboBox cmbQueueFilter2;
        private System.Windows.Forms.ComboBox cmbBoolQueuedFilter1;
        private System.Windows.Forms.ComboBox cmbQueueFilter1;
        private System.Windows.Forms.ListView lvExistingQueue;
        private System.Windows.Forms.CheckBox chkFilter2EqNEq;
        private System.Windows.Forms.CheckBox chkFilter1EqNEq;
        private System.Windows.Forms.ComboBox cmbQueuedStatus2;
        private System.Windows.Forms.ComboBox cmbQueuedStatus1;
        private System.Windows.Forms.TextBox txtQueueFilter2;
        private System.Windows.Forms.TextBox txtQueueFilter1;

    }
}

﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MTData.Transport.Application.Communication;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;

namespace MTData.Transport.Listener.DatsListenerApp.UserForms
{
    public partial class frmMultiFilter : Form
    {
        private Dictionary<Tuple<int, int>, Vehicle> _fleetVehicles = new Dictionary<Tuple<int,int>,Vehicle>();
        private Boolean _OK = false;
        private List<ListenerLogItem> _vehicleList = new List<ListenerLogItem>();

        public Boolean OK
        {
            get { return _OK; }
        }

        public List<ListenerLogItem> VehicleList
        {
            get { return _vehicleList; }
        }

        public frmMultiFilter(Dictionary<Tuple<int, int>, Vehicle> fleetVehicles)
        {
            InitializeComponent();

            foreach (KeyValuePair<Tuple<int, int>, Vehicle> entry in fleetVehicles)
            {
                this._fleetVehicles.Add(entry.Key, (Vehicle)entry.Value);
            }

            //this._fleetVehicles.OrderBy(veh => veh.Value.FleetName);

            this.LoadListView();
        }

        private void LoadListView()
        {
            foreach(KeyValuePair<Tuple<int,int>,Vehicle> veh in _fleetVehicles)
            {
                CheckedListItemWithTag tmpItem = new CheckedListItemWithTag(veh.Value);

                chkdListVehicles.Items.Add(tmpItem);
            }
        }

        private void btnApply_Click(object sender, EventArgs e)
        {
            this.CreateVehicleList();
            this._OK = true;
            this.Close();
        }

        private void CreateVehicleList()
        {

            foreach (CheckedListItemWithTag ci in chkdListVehicles.CheckedItems)
            {
                ListenerLogItem tmpItem = new ListenerLogItem();

                tmpItem.VehicleID = ((Vehicle)ci.Tag).VehicleID;
                tmpItem.FleetID = ((Vehicle)ci.Tag).FleetID;

                this._vehicleList.Add(tmpItem);
            }
        }

        public class CheckedListItemWithTag
        {
            private Vehicle _veh;

            public object Tag
            {
                get { return this._veh; }
            }

            public CheckedListItemWithTag(Vehicle veh)
            {
                this._veh = veh;
            }

            public override string ToString()
            {
                return this._veh.FleetName + ":" + this._veh.DisplayName;
            }
        }
    }
}

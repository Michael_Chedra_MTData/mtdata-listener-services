﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Application.Communication;

namespace MTData.Transport.Listener.DatsListenerApp.UserForms
{
    public partial class frmLogDetails : Form
    {
        private static log4net.ILog _log = log4net.LogManager.GetLogger(typeof(frmLogDetails));
        private ListenerLogItem mItem;

        public frmLogDetails(ListenerLogItem logItem)
        {
            InitializeComponent();

            this.mItem = logItem;

            this.LoadPacketDetails();
        }

        private void LoadPacketDetails()
        {
            GatewayProtocolPacket gpp = new GatewayProtocolPacket("dd/MM/yyyy HH:mm:ss");

            gpp.Decode(Helpers.StringToByteArray(this.mItem.ListenerMessage), 0);

            txtRawPacket.Text = mItem.ListenerMessage;
            txtPayload.Text = System.BitConverter.ToString(gpp.mDataField);

            txtPayload.Select(0, 0);

            try
            {
                if (
                    gpp.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_BEING_USED ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_LOGGED_IN ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_CONCETE_SENSOR_ALERT ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_G_FORCE_OUT_OF_CALIBRATION ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_G_FORCE_MISSING ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_SUSPEC_ENGINE_DATA ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_ENGINE_DATA_MISSING ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_ENGINE_DATA_RE_CONNECT ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_IGNITION_DISCONNECT ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_SUSPECT_GPS ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_G_FORCE_MISSING ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_G_FORCE_OUT_OF_CALIBRATION ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_CONCETE_SENSOR_ALERT ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_LOGGED_IN ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_MDT_NOT_BEING_USED ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_VEHICLE_GPS_SPEED_DIFF ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_ENGINE_DATA_RE_CONNECT ||
                    gpp.cMsgType == GeneralGPPacket.ANGEL_GEAR_ALERT_START ||
                    gpp.cMsgType == GeneralGPPacket.ANGEL_GEAR_ALERT_END ||
                    gpp.cMsgType == GeneralGPPacket.ALRM_UNIT_POWERUP ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_ABOVE_ONSITE_SPEED ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_CONCRETE_AGE_MINS ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_CONCRETE_AGE_ROTATIONS ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_DRIVER_ERROR_1 ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_DRIVER_ERROR_2 ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_LEFT_LOADER ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_LOADING_STARTED ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_MIX_STARTED ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_NOT_MIXED ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_RPM_FORWARD ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_RPM_REVERSED ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_RPM_STOPPED ||
                    gpp.cMsgType == GeneralGPPacket.BARREL_RPM_UP_TO_SPEED ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_FUEL_ECONOMY_ALERT ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_OVER_RPM ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_OVER_COOLANT_TEMP ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_OVER_OIL_TEMP ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_LOW ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_UNDER_OIL_PRESSURE_HIGH ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_G_FORCES_HIGH ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_G_FORCE_CALIBRATED_RPT ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_COOLANT_LEVEL_LOW ||
                    gpp.cMsgType == GeneralGPPacket.ENGINE_ERROR_CODE ||
                    gpp.cMsgType == GeneralGPPacket.EXTENDED_IO_BELOW_THRESHOLD ||
                    gpp.cMsgType == GeneralGPPacket.EXTENDED_IO_ABOVE_THRESHOLD ||
                    gpp.cMsgType == GeneralGPPacket.FATIGUE_REPORT_IGNON ||
                    gpp.cMsgType == GeneralGPPacket.FATIGUE_REPORT_24 ||
                    gpp.cMsgType == GeneralGPPacket.GEN_STARTUP ||
                    gpp.cMsgType == GeneralGPPacket.GEN_IM_ALIVE ||
                    gpp.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_FULL ||
                    gpp.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMEI_ESN ||
                    gpp.cMsgType == GeneralGPPacket.GEN_OVERSPEED ||
                    gpp.cMsgType == GeneralGPPacket.GEN_EXCESSIVE_IDLE ||
                    gpp.cMsgType == GeneralGPPacket.GEN_EXCESSIVE_IDLE_END ||
                    gpp.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START ||
                    gpp.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END ||
                    gpp.cMsgType == GeneralGPPacket.GPS_HISTORY ||
                    gpp.cMsgType == GeneralGPPacket.GEN_IOONOFF ||
                    gpp.cMsgType == GeneralGPPacket.GEN_NO_GPS_ANT_OPEN ||
                    gpp.cMsgType == GeneralGPPacket.GEN_NO_GPS_ANT_SHORT ||
                    gpp.cMsgType == GeneralGPPacket.GEN_NOGPS ||
                    gpp.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_1_START ||
                    gpp.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_2_START ||
                    gpp.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_3_START ||
                    gpp.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_1_END ||
                    gpp.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_2_END ||
                    gpp.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_3_END ||
                    gpp.cMsgType == GeneralGPPacket.REFRIG_REPORT ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_REPORT ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_TRAILER_HITCH ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_TRAILER_DEHITCH ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_POS_REPORT ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_DATA_USAGE_ALERT ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_IO_PULSES ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_ACCIDENT_NEW ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_RX_TX ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_RX_TX_SATTELITE ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_DATA_USAGE_ALERT_SATELLITE ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                    gpp.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS ||
                    gpp.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT ||
                    gpp.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE ||
                    gpp.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT ||
                    gpp.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST ||
                    gpp.cMsgType == GeneralGPPacket.PENDENT_ALARM ||
                    gpp.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED ||
                    gpp.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER ||
                    gpp.cMsgType == GeneralGPPacket.PENDENT_ALARM_NOT_ACKD_BY_USER ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                    gpp.cMsgType == GeneralGPPacket.STATUS_LOGOUT
                    )
                {
                    GeneralGPPacket packet = new GeneralGPPacket(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(packet.ToStringArray());
                }
                else if (gpp.cMsgType == GeneralGPPacket.DRIVER_PERFORMANCE_METRICS)
                {
                    GPDriverPerformanceMetrics packet = new GPDriverPerformanceMetrics(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (gpp.cMsgType == GeneralGPPacket.DRIVER_DATAACCUMLATOR_METRICS)
                {
                    GPDriverDataAccumulator packet = new GPDriverDataAccumulator(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (gpp.cMsgType == MassDeclarationPacket.MASS_DECLARATION)
                {
                    MassDeclarationPacket massPacket = new MassDeclarationPacket(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { massPacket.ToDisplayString() });
                }
                else if (gpp.cMsgType == DriverPointsRuleRequest.DRIVER_POINTS_RULE_REQUEST)
                {
                    DriverPointsRuleRequest packet = new DriverPointsRuleRequest(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (gpp.cMsgType == DriverPointsRuleBreakPacket.DRIVER_POINTS_RULE_BROKEN)
                {
                    DriverPointsRuleBreakPacket packet = new DriverPointsRuleBreakPacket(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (gpp.cMsgType == GenericPacket.GENERIC_PACKET)
                {
                    GenericPacket packet = new GenericPacket(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { packet.ToDisplayString() });
                }
                else if (gpp.cMsgType == DriverPointsConfigError.DRIVER_POINTS_CONFIG_ERROR)
                {
                    DriverPointsConfigError packet = new DriverPointsConfigError(gpp, "dd/MM/yyyy HH:mm:ss");
                    this.SetContents(new String[] { packet.ToDisplayString() });
                }
                else
                {
                    this.SetContents(new string[] { gpp.cMsgType.ToString() });
                }

            }
            catch (System.Exception ex)
            {
                _log.Error("ShowPacketDetails(GatewayProtocolPacket gpp)", ex);
            }
        }

        private void SetContents(string[] contentsList)
        {
            mRichText.Clear();
            foreach (string item in contentsList)
            {
                mRichText.AppendText(item + "\n");
            }

            mRichText.Select(0, 0);

            mRichText.ScrollToCaret();
        }
    }
}

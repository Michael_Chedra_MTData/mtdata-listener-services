﻿namespace MTData.Transport.Listener.DatsListenerApp.UserForms
{
    partial class frmLogDetails
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gbPayload = new System.Windows.Forms.GroupBox();
            this.txtPayload = new System.Windows.Forms.TextBox();
            this.mRichText = new System.Windows.Forms.RichTextBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtRawPacket = new System.Windows.Forms.TextBox();
            this.gbPayload.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // gbPayload
            // 
            this.gbPayload.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbPayload.Controls.Add(this.txtPayload);
            this.gbPayload.Location = new System.Drawing.Point(0, 0);
            this.gbPayload.Name = "gbPayload";
            this.gbPayload.Size = new System.Drawing.Size(493, 42);
            this.gbPayload.TabIndex = 8;
            this.gbPayload.TabStop = false;
            this.gbPayload.Text = "Payload";
            // 
            // txtPayload
            // 
            this.txtPayload.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPayload.Location = new System.Drawing.Point(3, 16);
            this.txtPayload.Name = "txtPayload";
            this.txtPayload.Size = new System.Drawing.Size(487, 20);
            this.txtPayload.TabIndex = 0;
            // 
            // mRichText
            // 
            this.mRichText.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.mRichText.Location = new System.Drawing.Point(0, 90);
            this.mRichText.Name = "mRichText";
            this.mRichText.ReadOnly = true;
            this.mRichText.Size = new System.Drawing.Size(493, 205);
            this.mRichText.TabIndex = 9;
            this.mRichText.Text = "";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.txtRawPacket);
            this.groupBox1.Location = new System.Drawing.Point(3, 48);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(493, 42);
            this.groupBox1.TabIndex = 9;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Raw Packet";
            // 
            // txtRawPacket
            // 
            this.txtRawPacket.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtRawPacket.Location = new System.Drawing.Point(3, 16);
            this.txtRawPacket.Name = "txtRawPacket";
            this.txtRawPacket.Size = new System.Drawing.Size(487, 20);
            this.txtRawPacket.TabIndex = 0;
            // 
            // frmLogDetails
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(493, 295);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.mRichText);
            this.Controls.Add(this.gbPayload);
            this.Name = "frmLogDetails";
            this.Text = "Listener Message Details";
            this.gbPayload.ResumeLayout(false);
            this.gbPayload.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox gbPayload;
        private System.Windows.Forms.TextBox txtPayload;
        private System.Windows.Forms.RichTextBox mRichText;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.TextBox txtRawPacket;
    }
}
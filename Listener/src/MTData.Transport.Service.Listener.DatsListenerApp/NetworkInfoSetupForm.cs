using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.Listener.DatsListenerApp
{
	/// <summary>
	/// Summary description for NetworkInfoSetupForm.
	/// </summary>
	public class NetworkInfoSetupForm : System.Windows.Forms.Form
	{
		#region Variables

		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.RadioButton rbNetworkTelstra;
		private System.Windows.Forms.RadioButton rbNetworkOther;
		private System.Windows.Forms.TextBox txtAPN;
		private System.Windows.Forms.GroupBox groupBox3;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label8;
		private System.Windows.Forms.Button btnDownload;
		private System.Windows.Forms.Button btnCancel;
		private ConfigNetworkInfoGPPacket netPacket;
		private PacketDatabaseInterface mDB;
		private System.Windows.Forms.NumericUpDown spinPrimaryRxPort;
		private System.Windows.Forms.NumericUpDown spinPrimaryServerIP4;
		private System.Windows.Forms.NumericUpDown spinPrimaryServerIP3;
		private System.Windows.Forms.NumericUpDown spinPrimaryServerIP2;
		private System.Windows.Forms.NumericUpDown spinPrimaryServerIP1;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.Label label9;
		private System.Windows.Forms.Label label10;
		private System.Windows.Forms.GroupBox groupBox4;
		private System.Windows.Forms.Label label11;
		private System.Windows.Forms.Label label12;
		private System.Windows.Forms.Label label13;
		private System.Windows.Forms.Label label14;
		private System.Windows.Forms.Label label15;
		private System.Windows.Forms.NumericUpDown spinSecondaryRxPort;
		private System.Windows.Forms.NumericUpDown spinSecondaryServerIP4;
		private System.Windows.Forms.NumericUpDown spinSecondaryServerIP3;
		private System.Windows.Forms.NumericUpDown spinSecondaryServerIP2;
		private System.Windows.Forms.NumericUpDown spinSecondaryServerIP1;
		private System.Windows.Forms.NumericUpDown spinPingMaxRetries;
		private System.Windows.Forms.NumericUpDown spinPingIP4;
		private System.Windows.Forms.NumericUpDown spinPingIP3;
		private System.Windows.Forms.NumericUpDown spinPingIP2;
		private System.Windows.Forms.NumericUpDown spinPingIP1;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private string _serverTime_DateFormat = "";
		#endregion

		public NetworkInfoSetupForm(GatewayProtocolPacket pkt, 
									PacketDatabaseInterface pdi,
									string serverTime_DateFormat)
		{
			//
			// Required for Windows Form Designer support
			//
			_serverTime_DateFormat = serverTime_DateFormat;
			InitializeComponent();
			netPacket = new ConfigNetworkInfoGPPacket(pkt, _serverTime_DateFormat);
			mDB = pdi;
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// 
		public void SetNewSourcePacket(GatewayProtocolPacket pkt)
		{
			netPacket = new ConfigNetworkInfoGPPacket(pkt, _serverTime_DateFormat);
		}

		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.btnDownload = new System.Windows.Forms.Button();
			this.btnCancel = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtAPN = new System.Windows.Forms.TextBox();
			this.rbNetworkOther = new System.Windows.Forms.RadioButton();
			this.rbNetworkTelstra = new System.Windows.Forms.RadioButton();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.spinPrimaryRxPort = new System.Windows.Forms.NumericUpDown();
			this.label8 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.spinPrimaryServerIP4 = new System.Windows.Forms.NumericUpDown();
			this.spinPrimaryServerIP3 = new System.Windows.Forms.NumericUpDown();
			this.spinPrimaryServerIP2 = new System.Windows.Forms.NumericUpDown();
			this.spinPrimaryServerIP1 = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.spinSecondaryRxPort = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.label9 = new System.Windows.Forms.Label();
			this.spinSecondaryServerIP4 = new System.Windows.Forms.NumericUpDown();
			this.spinSecondaryServerIP3 = new System.Windows.Forms.NumericUpDown();
			this.spinSecondaryServerIP2 = new System.Windows.Forms.NumericUpDown();
			this.spinSecondaryServerIP1 = new System.Windows.Forms.NumericUpDown();
			this.label10 = new System.Windows.Forms.Label();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.spinPingMaxRetries = new System.Windows.Forms.NumericUpDown();
			this.label11 = new System.Windows.Forms.Label();
			this.label12 = new System.Windows.Forms.Label();
			this.label13 = new System.Windows.Forms.Label();
			this.label14 = new System.Windows.Forms.Label();
			this.spinPingIP4 = new System.Windows.Forms.NumericUpDown();
			this.spinPingIP3 = new System.Windows.Forms.NumericUpDown();
			this.spinPingIP2 = new System.Windows.Forms.NumericUpDown();
			this.spinPingIP1 = new System.Windows.Forms.NumericUpDown();
			this.label15 = new System.Windows.Forms.Label();
			this.groupBox2.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryRxPort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP1)).BeginInit();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryRxPort)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP1)).BeginInit();
			this.groupBox4.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.spinPingMaxRetries)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP4)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP3)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP2)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP1)).BeginInit();
			this.SuspendLayout();
			// 
			// btnDownload
			// 
			this.btnDownload.Location = new System.Drawing.Point(32, 376);
			this.btnDownload.Name = "btnDownload";
			this.btnDownload.Size = new System.Drawing.Size(104, 23);
			this.btnDownload.TabIndex = 2;
			this.btnDownload.Text = "Download To Unit";
			this.btnDownload.Click += new System.EventHandler(this.btnDownload_Click);
			// 
			// btnCancel
			// 
			this.btnCancel.Location = new System.Drawing.Point(152, 376);
			this.btnCancel.Name = "btnCancel";
			this.btnCancel.Size = new System.Drawing.Size(104, 23);
			this.btnCancel.TabIndex = 3;
			this.btnCancel.Text = "Cancel";
			this.btnCancel.Click += new System.EventHandler(this.btnCancel_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.txtAPN,
																					this.rbNetworkOther,
																					this.rbNetworkTelstra});
			this.groupBox2.Location = new System.Drawing.Point(8, 8);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(280, 72);
			this.groupBox2.TabIndex = 5;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "GPRS Network";
			// 
			// txtAPN
			// 
			this.txtAPN.Enabled = false;
			this.txtAPN.Location = new System.Drawing.Point(104, 40);
			this.txtAPN.MaxLength = 80;
			this.txtAPN.Name = "txtAPN";
			this.txtAPN.Size = new System.Drawing.Size(160, 20);
			this.txtAPN.TabIndex = 2;
			this.txtAPN.Text = "";
			// 
			// rbNetworkOther
			// 
			this.rbNetworkOther.Location = new System.Drawing.Point(16, 40);
			this.rbNetworkOther.Name = "rbNetworkOther";
			this.rbNetworkOther.Size = new System.Drawing.Size(96, 24);
			this.rbNetworkOther.TabIndex = 1;
			this.rbNetworkOther.Text = "Other - APN:";
			this.rbNetworkOther.CheckedChanged += new System.EventHandler(this.rbNetworkOther_CheckedChanged);
			// 
			// rbNetworkTelstra
			// 
			this.rbNetworkTelstra.Checked = true;
			this.rbNetworkTelstra.Location = new System.Drawing.Point(16, 16);
			this.rbNetworkTelstra.Name = "rbNetworkTelstra";
			this.rbNetworkTelstra.TabIndex = 0;
			this.rbNetworkTelstra.TabStop = true;
			this.rbNetworkTelstra.Text = "Telstra (1)";
			this.rbNetworkTelstra.CheckedChanged += new System.EventHandler(this.rbNetworkTelstra_CheckedChanged);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.spinPrimaryRxPort,
																					this.label8,
																					this.label6,
																					this.label5,
																					this.label4,
																					this.spinPrimaryServerIP4,
																					this.spinPrimaryServerIP3,
																					this.spinPrimaryServerIP2,
																					this.spinPrimaryServerIP1,
																					this.label3});
			this.groupBox3.Location = new System.Drawing.Point(8, 88);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(280, 88);
			this.groupBox3.TabIndex = 6;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Primary DATS Server (REQUIRED)";
			// 
			// spinPrimaryRxPort
			// 
			this.spinPrimaryRxPort.Location = new System.Drawing.Point(176, 56);
			this.spinPrimaryRxPort.Maximum = new System.Decimal(new int[] {
																			  65535,
																			  0,
																			  0,
																			  0});
			this.spinPrimaryRxPort.Name = "spinPrimaryRxPort";
			this.spinPrimaryRxPort.Size = new System.Drawing.Size(64, 20);
			this.spinPrimaryRxPort.TabIndex = 12;
			// 
			// label8
			// 
			this.label8.Location = new System.Drawing.Point(16, 56);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(80, 16);
			this.label8.TabIndex = 10;
			this.label8.Text = "Receive Port:";
			this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(216, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(8, 23);
			this.label6.TabIndex = 8;
			this.label6.Text = ".";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(168, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(8, 23);
			this.label5.TabIndex = 7;
			this.label5.Text = ".";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(120, 16);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(8, 23);
			this.label4.TabIndex = 6;
			this.label4.Text = ".";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// spinPrimaryServerIP4
			// 
			this.spinPrimaryServerIP4.Location = new System.Drawing.Point(224, 24);
			this.spinPrimaryServerIP4.Maximum = new System.Decimal(new int[] {
																				 255,
																				 0,
																				 0,
																				 0});
			this.spinPrimaryServerIP4.Name = "spinPrimaryServerIP4";
			this.spinPrimaryServerIP4.Size = new System.Drawing.Size(40, 20);
			this.spinPrimaryServerIP4.TabIndex = 5;
			// 
			// spinPrimaryServerIP3
			// 
			this.spinPrimaryServerIP3.Location = new System.Drawing.Point(176, 24);
			this.spinPrimaryServerIP3.Maximum = new System.Decimal(new int[] {
																				 255,
																				 0,
																				 0,
																				 0});
			this.spinPrimaryServerIP3.Name = "spinPrimaryServerIP3";
			this.spinPrimaryServerIP3.Size = new System.Drawing.Size(40, 20);
			this.spinPrimaryServerIP3.TabIndex = 4;
			// 
			// spinPrimaryServerIP2
			// 
			this.spinPrimaryServerIP2.Location = new System.Drawing.Point(128, 24);
			this.spinPrimaryServerIP2.Maximum = new System.Decimal(new int[] {
																				 255,
																				 0,
																				 0,
																				 0});
			this.spinPrimaryServerIP2.Name = "spinPrimaryServerIP2";
			this.spinPrimaryServerIP2.Size = new System.Drawing.Size(40, 20);
			this.spinPrimaryServerIP2.TabIndex = 2;
			// 
			// spinPrimaryServerIP1
			// 
			this.spinPrimaryServerIP1.Location = new System.Drawing.Point(80, 24);
			this.spinPrimaryServerIP1.Maximum = new System.Decimal(new int[] {
																				 255,
																				 0,
																				 0,
																				 0});
			this.spinPrimaryServerIP1.Name = "spinPrimaryServerIP1";
			this.spinPrimaryServerIP1.Size = new System.Drawing.Size(40, 20);
			this.spinPrimaryServerIP1.TabIndex = 1;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(16, 24);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(56, 23);
			this.label3.TabIndex = 0;
			this.label3.Text = "Server IP:";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.spinSecondaryRxPort,
																					this.label1,
																					this.label2,
																					this.label7,
																					this.label9,
																					this.spinSecondaryServerIP4,
																					this.spinSecondaryServerIP3,
																					this.spinSecondaryServerIP2,
																					this.spinSecondaryServerIP1,
																					this.label10});
			this.groupBox1.Location = new System.Drawing.Point(8, 184);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(280, 88);
			this.groupBox1.TabIndex = 7;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Secondary DATS Server (Optional)";
			// 
			// spinSecondaryRxPort
			// 
			this.spinSecondaryRxPort.Location = new System.Drawing.Point(176, 56);
			this.spinSecondaryRxPort.Maximum = new System.Decimal(new int[] {
																				65535,
																				0,
																				0,
																				0});
			this.spinSecondaryRxPort.Name = "spinSecondaryRxPort";
			this.spinSecondaryRxPort.Size = new System.Drawing.Size(64, 20);
			this.spinSecondaryRxPort.TabIndex = 12;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 56);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(80, 16);
			this.label1.TabIndex = 10;
			this.label1.Text = "Receive Port:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(216, 16);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(8, 23);
			this.label2.TabIndex = 8;
			this.label2.Text = ".";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label7
			// 
			this.label7.Location = new System.Drawing.Point(168, 16);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(8, 23);
			this.label7.TabIndex = 7;
			this.label7.Text = ".";
			this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label9
			// 
			this.label9.Location = new System.Drawing.Point(120, 16);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(8, 23);
			this.label9.TabIndex = 6;
			this.label9.Text = ".";
			this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// spinSecondaryServerIP4
			// 
			this.spinSecondaryServerIP4.Location = new System.Drawing.Point(224, 24);
			this.spinSecondaryServerIP4.Maximum = new System.Decimal(new int[] {
																				   255,
																				   0,
																				   0,
																				   0});
			this.spinSecondaryServerIP4.Name = "spinSecondaryServerIP4";
			this.spinSecondaryServerIP4.Size = new System.Drawing.Size(40, 20);
			this.spinSecondaryServerIP4.TabIndex = 5;
			// 
			// spinSecondaryServerIP3
			// 
			this.spinSecondaryServerIP3.Location = new System.Drawing.Point(176, 24);
			this.spinSecondaryServerIP3.Maximum = new System.Decimal(new int[] {
																				   255,
																				   0,
																				   0,
																				   0});
			this.spinSecondaryServerIP3.Name = "spinSecondaryServerIP3";
			this.spinSecondaryServerIP3.Size = new System.Drawing.Size(40, 20);
			this.spinSecondaryServerIP3.TabIndex = 4;
			// 
			// spinSecondaryServerIP2
			// 
			this.spinSecondaryServerIP2.Location = new System.Drawing.Point(128, 24);
			this.spinSecondaryServerIP2.Maximum = new System.Decimal(new int[] {
																				   255,
																				   0,
																				   0,
																				   0});
			this.spinSecondaryServerIP2.Name = "spinSecondaryServerIP2";
			this.spinSecondaryServerIP2.Size = new System.Drawing.Size(40, 20);
			this.spinSecondaryServerIP2.TabIndex = 2;
			// 
			// spinSecondaryServerIP1
			// 
			this.spinSecondaryServerIP1.Location = new System.Drawing.Point(80, 24);
			this.spinSecondaryServerIP1.Maximum = new System.Decimal(new int[] {
																				   255,
																				   0,
																				   0,
																				   0});
			this.spinSecondaryServerIP1.Name = "spinSecondaryServerIP1";
			this.spinSecondaryServerIP1.Size = new System.Drawing.Size(40, 20);
			this.spinSecondaryServerIP1.TabIndex = 1;
			// 
			// label10
			// 
			this.label10.Location = new System.Drawing.Point(16, 24);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(56, 23);
			this.label10.TabIndex = 0;
			this.label10.Text = "Server IP:";
			this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.AddRange(new System.Windows.Forms.Control[] {
																					this.spinPingMaxRetries,
																					this.label11,
																					this.label12,
																					this.label13,
																					this.label14,
																					this.spinPingIP4,
																					this.spinPingIP3,
																					this.spinPingIP2,
																					this.spinPingIP1,
																					this.label15});
			this.groupBox4.Location = new System.Drawing.Point(8, 280);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(280, 88);
			this.groupBox4.TabIndex = 8;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Ping Target (Default = www.google.com)";
			// 
			// spinPingMaxRetries
			// 
			this.spinPingMaxRetries.Location = new System.Drawing.Point(176, 56);
			this.spinPingMaxRetries.Maximum = new System.Decimal(new int[] {
																			   255,
																			   0,
																			   0,
																			   0});
			this.spinPingMaxRetries.Name = "spinPingMaxRetries";
			this.spinPingMaxRetries.Size = new System.Drawing.Size(40, 20);
			this.spinPingMaxRetries.TabIndex = 12;
			this.spinPingMaxRetries.Value = new System.Decimal(new int[] {
																			 5,
																			 0,
																			 0,
																			 0});
			// 
			// label11
			// 
			this.label11.Location = new System.Drawing.Point(16, 56);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(152, 16);
			this.label11.TabIndex = 10;
			this.label11.Text = "Max Retries Before Reset:";
			this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label12
			// 
			this.label12.Location = new System.Drawing.Point(216, 16);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(8, 23);
			this.label12.TabIndex = 8;
			this.label12.Text = ".";
			this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label13
			// 
			this.label13.Location = new System.Drawing.Point(168, 16);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(8, 23);
			this.label13.TabIndex = 7;
			this.label13.Text = ".";
			this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label14
			// 
			this.label14.Location = new System.Drawing.Point(120, 16);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(8, 23);
			this.label14.TabIndex = 6;
			this.label14.Text = ".";
			this.label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// spinPingIP4
			// 
			this.spinPingIP4.Location = new System.Drawing.Point(224, 24);
			this.spinPingIP4.Maximum = new System.Decimal(new int[] {
																		255,
																		0,
																		0,
																		0});
			this.spinPingIP4.Name = "spinPingIP4";
			this.spinPingIP4.Size = new System.Drawing.Size(40, 20);
			this.spinPingIP4.TabIndex = 5;
			this.spinPingIP4.Value = new System.Decimal(new int[] {
																	  99,
																	  0,
																	  0,
																	  0});
			// 
			// spinPingIP3
			// 
			this.spinPingIP3.Location = new System.Drawing.Point(176, 24);
			this.spinPingIP3.Maximum = new System.Decimal(new int[] {
																		255,
																		0,
																		0,
																		0});
			this.spinPingIP3.Name = "spinPingIP3";
			this.spinPingIP3.Size = new System.Drawing.Size(40, 20);
			this.spinPingIP3.TabIndex = 4;
			this.spinPingIP3.Value = new System.Decimal(new int[] {
																	  57,
																	  0,
																	  0,
																	  0});
			// 
			// spinPingIP2
			// 
			this.spinPingIP2.Location = new System.Drawing.Point(128, 24);
			this.spinPingIP2.Maximum = new System.Decimal(new int[] {
																		255,
																		0,
																		0,
																		0});
			this.spinPingIP2.Name = "spinPingIP2";
			this.spinPingIP2.Size = new System.Drawing.Size(40, 20);
			this.spinPingIP2.TabIndex = 2;
			this.spinPingIP2.Value = new System.Decimal(new int[] {
																	  239,
																	  0,
																	  0,
																	  0});
			// 
			// spinPingIP1
			// 
			this.spinPingIP1.Location = new System.Drawing.Point(80, 24);
			this.spinPingIP1.Maximum = new System.Decimal(new int[] {
																		255,
																		0,
																		0,
																		0});
			this.spinPingIP1.Name = "spinPingIP1";
			this.spinPingIP1.Size = new System.Drawing.Size(40, 20);
			this.spinPingIP1.TabIndex = 1;
			this.spinPingIP1.Value = new System.Decimal(new int[] {
																	  216,
																	  0,
																	  0,
																	  0});
			// 
			// label15
			// 
			this.label15.Location = new System.Drawing.Point(16, 24);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(56, 23);
			this.label15.TabIndex = 0;
			this.label15.Text = "Server IP:";
			this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// NetworkInfoSetupForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(292, 405);
			this.Controls.AddRange(new System.Windows.Forms.Control[] {
																		  this.groupBox4,
																		  this.groupBox3,
																		  this.groupBox2,
																		  this.btnCancel,
																		  this.btnDownload,
																		  this.groupBox1});
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.Name = "NetworkInfoSetupForm";
			this.Text = "Network / Server Info Setup";
			this.groupBox2.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryRxPort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPrimaryServerIP1)).EndInit();
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryRxPort)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinSecondaryServerIP1)).EndInit();
			this.groupBox4.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.spinPingMaxRetries)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP4)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP3)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP2)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.spinPingIP1)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		#region Button handlers
		private void btnDownload_Click(object sender, System.EventArgs e)
		{
			#region Sanity Checks:
			if (((spinPrimaryServerIP1.Value == 0) &&
				(spinPrimaryServerIP2.Value == 0) &&
				(spinPrimaryServerIP3.Value == 0) &&
				(spinPrimaryServerIP4.Value == 0)) ||
				(spinPrimaryRxPort.Value == 0))
			{
				MessageBox.Show("Primary Server Address and Port MUST be set", "Validation Problem");
				return;
			}
			
			if ((rbNetworkOther.Checked) && (txtAPN.Text.Length == 0))
			{
				MessageBox.Show("If not using Telstra, the APN must be set", "Validation Problem");
				return;
			}

			if (spinPingMaxRetries.Value == 0)
			{
				MessageBox.Show("Ping Retries must be at least 1", "Validation Problem");
				return;
			}

			// Get here if we validate:
			if (MessageBox.Show("This will change the network settings for:\n" +
				"Fleet " + netPacket.cFleetId + 
				" Unit " + netPacket.iVehicleId + "\n" +
				"Are you sure you want to do this?",
				"Confirm Network Settings Change",
				MessageBoxButtons.YesNo) ==
				DialogResult.No)
			{
				return;
			}

			#endregion

			#region Defaults:
			netPacket.cNewFleetId = 0;
			netPacket.iNewUnitId= 0;
			#endregion
			#region GPRS network:
			if (rbNetworkTelstra.Checked)
			{
				netPacket.cNetwork = 1;
			}
			else
			{
				netPacket.cNetwork = 0;
				netPacket.sNetworkAPN = txtAPN.Text;
			}
			#endregion
			#region IP network:
			netPacket.mPrimaryServerIPAddress = 
				System.Net.IPAddress.Parse(	spinPrimaryServerIP1.Value + "." +
											spinPrimaryServerIP2.Value + "." +
											spinPrimaryServerIP3.Value + "." +
											spinPrimaryServerIP4.Value);
			netPacket.iPrimaryServerRxPort = Convert.ToInt16(spinPrimaryRxPort.Value);

			netPacket.mSecondaryServerIPAddress = 
				System.Net.IPAddress.Parse(	spinSecondaryServerIP1.Value + "." +
											spinSecondaryServerIP2.Value + "." +
											spinSecondaryServerIP3.Value + "." +
											spinSecondaryServerIP4.Value);
			netPacket.iSecondaryServerRxPort = Convert.ToInt16(spinSecondaryRxPort.Value);

			netPacket.mPingableIPAddress = 
				System.Net.IPAddress.Parse(	spinPingIP1.Value + "." +
											spinPingIP2.Value + "." +
											spinPingIP3.Value + "." +
											spinPingIP4.Value);
			netPacket.cMaxPingRetries = Convert.ToByte(spinPingMaxRetries.Value);

			#endregion
			#region Serial:
			netPacket.cPort1BaudRate = 0;
			#endregion

			mDB.SendNetworkInfo(netPacket);
		}

		private void btnCancel_Click(object sender, System.EventArgs e)
		{
			this.Hide();
		}
		#endregion

		#region Radio Button handlers
		private void rbNetworkTelstra_CheckedChanged(object sender, System.EventArgs e)
		{
			txtAPN.Enabled = false;
		}
		private void rbNetworkOther_CheckedChanged(object sender, System.EventArgs e)
		{
			txtAPN.Enabled = true;
		}

		#endregion

	}
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Microsoft.Win32;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public class Helpers
    {
        private static MTData.Transport.Service.Listener.GatewayServiceBL.Credentials userCreds;
        private static KeyValuePair<string, string> currentConnection;
        private static Boolean _LogLoading;

        public static Boolean LogLoading
        {
            get { return _LogLoading; }
            set { _LogLoading = value; }
        }

        public static ListenerGatewayWCF.GatewayServiceDefinitionClient ListenerGateway()
        {
            System.ServiceModel.EndpointAddress address = new System.ServiceModel.EndpointAddress(currentConnection.Value);

            if (currentConnection.Value.Contains("https"))
            {
                return new ListenerGatewayWCF.GatewayServiceDefinitionClient("BasicHttpsBinding_IGatewayServiceDefinition", address);
            }
            else
            {
                return new ListenerGatewayWCF.GatewayServiceDefinitionClient("BasicHttpBinding_IGatewayServiceDefinition", address);
            }
        }

        public static MTData.Transport.Service.Listener.GatewayServiceBL.Credentials UserCredentials
        {
            get { return userCreds; }
            set { userCreds = value; }
        }

        public static KeyValuePair<string, string> CurrentConnection
        {
            get { return currentConnection; }
            set { currentConnection = value; }
        }

        public static ColumnHeader CreateHeader(string columnName, string headerText)
        {
            ColumnHeader tmpHeader = new ColumnHeader(columnName);
            tmpHeader.Text = headerText;

            return tmpHeader;
        }

        public static byte[] StringToByteArray(String hex)
        {
            //int NumberChars = hex.Length;

            //byte[] bytes = new byte[(NumberChars / 3) + 1];

            string[] hexStrings = hex.Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);

            byte[] bytes = new byte[hexStrings.Length];

            int byteCount = 0;

            foreach (string hexVal in hexStrings)
            {
                bytes[byteCount] = Convert.ToByte(hexVal, 16);
                //for (int i = 0; i < NumberChars; i += 3)
                //    bytes[i / 3] = Convert.ToByte(hex.Substring(i, 2), 16);
                byteCount++;
            }

            return bytes;
        }

        public static string ReadRegistryKey(string subKey, string KeyName)
        {
            RegistryKey rk = Registry.CurrentUser.OpenSubKey(subKey);

            try
            {
                return rk.GetValue(KeyName).ToString();
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public static bool WriteRegistryKey(string subKey, string KeyName, object Value)
        {
            try
            {
                RegistryKey rk = Registry.CurrentUser.OpenSubKey(subKey, true);

                if (rk == null)
                {
                    rk = Registry.CurrentUser.CreateSubKey(subKey);
                }

                rk.SetValue(KeyName, Value);

                return true;
            }
            catch (Exception e)
            {
                return false;
            }
        }
    }
}

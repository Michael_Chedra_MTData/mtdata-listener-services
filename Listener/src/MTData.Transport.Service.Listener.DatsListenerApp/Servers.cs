﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Xml;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public class Servers : IConfigurationSectionHandler
    {
        private List<KeyValuePair<string, string>> servers;

        public List<KeyValuePair<string, string>> ServerList { get { return this.servers; } }

        public Servers()
        {
            this.servers = new List<KeyValuePair<string, string>>();
        }

        #region IConfigurationSectionHandler Members

        public object Create(object parent, object configContext, XmlNode section)
        {
            Servers servers = new Servers();

            XmlNodeList nodes = section.SelectNodes("server");

            foreach (XmlElement node in nodes)
            {
                servers.ServerList.Add(new KeyValuePair<string, string>(node.GetAttribute("name"), node.GetAttribute("value")));
            }

            return servers;
        }

        #endregion
    }
}

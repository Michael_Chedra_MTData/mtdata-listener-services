using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public class MonitorData
    {
        private string sDirection = "";
        private string sReportTime = "";
        private string sFleetID = "";
        private string sVehicleID = "";
        private string sMsgType = "";
        private string sIPAddress = "";
        private string sByteString = "";

        public MonitorData(string Direction, string ReportTime, string FleetID, string VehicleID, string MsgType, string _IPAddress, byte[] ByteData)
        {
            try
            {
                sDirection = Direction;
                sReportTime = ReportTime;
                sFleetID = FleetID;
                sVehicleID = VehicleID;
                sMsgType = MsgType;
                sIPAddress = _IPAddress;
                if(ByteData != null)
                    sByteString = System.BitConverter.ToString(ByteData, 0);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }

        //
        //
        public void PopulateInterface(ListView lvQueued, ListView lvInfo, int iMaxHistoryItems, bool bAutoScroll)
        {
            string[] sValue = new string[7];
            ListViewItem oLVI = null;
            try
            {
                #region Update the queued items list
                for (int X = 0; X < lvQueued.Items.Count; X++)
                {
                    oLVI = lvQueued.Items[X];
                    if (oLVI.SubItems[0].Text == sFleetID && oLVI.SubItems[1].Text == sVehicleID)
                    {
                        oLVI.SubItems[2].Text = sReportTime;
                        oLVI.SubItems[3].Text = sIPAddress;
                        oLVI.SubItems[4].Text = sDirection;
                        oLVI.SubItems[5].Text = sByteString;
                        break;
                    }
                }
                #endregion
                #region Update the history items list
                sValue[0] = sFleetID;
                sValue[1] = sVehicleID;
                sValue[2] = sReportTime;
                sValue[3] = sMsgType;
                sValue[4] = sIPAddress;
                sValue[5] = sDirection;
                sValue[6] = sByteString;
                oLVI = new ListViewItem(sValue);
                lvInfo.Items.Add(oLVI);
                if (lvInfo.Items.Count > iMaxHistoryItems)
                {
                    lvInfo.Items.RemoveAt(0);
                }
                if (bAutoScroll)
                    lvInfo.EnsureVisible(lvInfo.Items.Count - 1);
                #endregion
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using log4net;
using MTData.Common.Database;
using MTData.Common.Utilities;

namespace MTData.Transport.Listener.DatsListenerApp
{
    static class Program
    {


        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {

            log4net.Config.XmlConfigurator.Configure();

            //check database version
            CheckDatabaseVersion checkDB = new CheckDatabaseVersion(System.Configuration.ConfigurationManager.AppSettings["ConnectString"]);
            DatabaseVersionErrorMessage errorMessage;
            if (checkDB.CheckDatabaseRequiredVersion(8, 2, 0, 1, out errorMessage) == false &&
                        errorMessage != null)
            {
                var log = LogManager.GetLogger(typeof(Program));
                string emailServiceUrl = ConfigurationManager.AppSettings["EmailServiceUrl"];
                new EmailHelper(new EmailClient(emailServiceUrl)).SendDatabaseVersionErrorEmail(errorMessage);
            }

            System.Windows.Forms.Application.EnableVisualStyles();
            System.Windows.Forms.Application.SetCompatibleTextRenderingDefault(false);

            bool connectToServer = true;
            while (connectToServer)
            {
                LoginForm loginForm = new LoginForm();
                System.Windows.Forms.Application.Run(loginForm);

                if (loginForm.LoadMain)
                {
                    ListenerForm listen = new ListenerForm();
                    System.Windows.Forms.Application.Run(listen);
                    connectToServer = listen.ConnectToNewServer;
                    listen.Dispose();
                }
                else
                {
                    connectToServer = false;
                }
            }
        }

    }
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.IO;
using System.Windows.Forms;
using System.Threading;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Gateway.Packet;
using log4net;
using MTData.Transport.Listener.DatsListenerApp;
using MTData.Common.Threading;

namespace MTData.Transport.Listener.DatsListenerApp
{
    /// <summary>
    /// Summary description for frmQueueCommands.
    /// </summary>
    public class frmQueueCommands : System.Windows.Forms.Form
    {
        private static ILog _log = LogManager.GetLogger(typeof(frmQueueCommands));
        private const string sClassName = "DATSListenerApp.frmQueueCommands.";
        private System.Windows.Forms.Button btnRefershAvaliableList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvQueued;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnAddAll;
        private System.Windows.Forms.Button btnAddSelected;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvAvailableUnits;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button btnProcessQueue;
        private System.Windows.Forms.ComboBox cmbCommand;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private Hashtable oSendCmdToList = null;
        private ArrayList oUpdatePackets = null;
        private EnhancedThread tFindUnits = null;
        private MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator _listener = null;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.NumericUpDown numNewPingRetries;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numNewIPPort;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numSecIPPort;
        private MaskedTextBox txtSecIP;
        private MaskedTextBox txtPingIP;
        private CheckBox checkBox1;
        private TextBox txtHostName;
        private MaskedTextBox txtNewIP;

        public frmQueueCommands(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener)
        {
            try
            {
                InitializeComponent();
                oSendCmdToList = Hashtable.Synchronized(new Hashtable());
                oUpdatePackets = ArrayList.Synchronized(new ArrayList());
                _listener = oListener;
                LoadListView();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmMultipleDownload(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener, string serverTime_DateFormat)", ex);
                throw new System.Exception(sClassName + "frmMultipleDownload(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener, string serverTime_DateFormat)", ex);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }

            try
            {
                if (oSendCmdToList != null)
                {
                    lock (oSendCmdToList.SyncRoot)
                        oSendCmdToList.Clear();
                }
                oSendCmdToList = null;
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (oUpdatePackets != null)
                {
                    lock (oUpdatePackets.SyncRoot)
                        oUpdatePackets.Clear();
                }
                oUpdatePackets = null;
            }
            catch (System.Exception)
            {
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRefershAvaliableList = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lvQueued = new System.Windows.Forms.ListView();
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnAddAll = new System.Windows.Forms.Button();
            this.btnAddSelected = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lvAvailableUnits = new System.Windows.Forms.ListView();
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.cmbCommand = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.btnProcessQueue = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtPingIP = new MTData.Transport.Listener.DatsListenerApp.MaskedTextBox();
            this.txtSecIP = new MTData.Transport.Listener.DatsListenerApp.MaskedTextBox();
            this.numSecIPPort = new System.Windows.Forms.NumericUpDown();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.numNewPingRetries = new System.Windows.Forms.NumericUpDown();
            this.label7 = new System.Windows.Forms.Label();
            this.numNewIPPort = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.txtHostName = new System.Windows.Forms.TextBox();
            this.txtNewIP = new MTData.Transport.Listener.DatsListenerApp.MaskedTextBox();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSecIPPort)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewPingRetries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewIPPort)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRefershAvaliableList
            // 
            this.btnRefershAvaliableList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefershAvaliableList.Location = new System.Drawing.Point(8, 392);
            this.btnRefershAvaliableList.Name = "btnRefershAvaliableList";
            this.btnRefershAvaliableList.Size = new System.Drawing.Size(320, 24);
            this.btnRefershAvaliableList.TabIndex = 1;
            this.btnRefershAvaliableList.Text = "Refresh Avaliable List";
            this.btnRefershAvaliableList.Click += new System.EventHandler(this.btnRefershAvaliableList_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(456, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "Queued for Download";
            // 
            // lvQueued
            // 
            this.lvQueued.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvQueued.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader8,
            this.columnHeader5});
            this.lvQueued.FullRowSelect = true;
            this.lvQueued.GridLines = true;
            this.lvQueued.Location = new System.Drawing.Point(456, 24);
            this.lvQueued.Name = "lvQueued";
            this.lvQueued.Size = new System.Drawing.Size(352, 496);
            this.lvQueued.TabIndex = 7;
            this.lvQueued.UseCompatibleStateImageBehavior = false;
            this.lvQueued.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Fleet ID";
            this.columnHeader3.Width = 61;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vehicle ID";
            this.columnHeader4.Width = 68;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Command";
            this.columnHeader8.Width = 100;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Status";
            this.columnHeader5.Width = 204;
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Location = new System.Drawing.Point(368, 168);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveSelected.TabIndex = 6;
            this.btnRemoveSelected.Text = "<";
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(368, 136);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveAll.TabIndex = 5;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnAddAll
            // 
            this.btnAddAll.Location = new System.Drawing.Point(368, 104);
            this.btnAddAll.Name = "btnAddAll";
            this.btnAddAll.Size = new System.Drawing.Size(32, 24);
            this.btnAddAll.TabIndex = 4;
            this.btnAddAll.Text = ">>";
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            // 
            // btnAddSelected
            // 
            this.btnAddSelected.Location = new System.Drawing.Point(368, 72);
            this.btnAddSelected.Name = "btnAddSelected";
            this.btnAddSelected.Size = new System.Drawing.Size(32, 24);
            this.btnAddSelected.TabIndex = 3;
            this.btnAddSelected.Text = ">";
            this.btnAddSelected.Click += new System.EventHandler(this.btnAddSelected_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Available Units";
            // 
            // lvAvailableUnits
            // 
            this.lvAvailableUnits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.lvAvailableUnits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader6});
            this.lvAvailableUnits.FullRowSelect = true;
            this.lvAvailableUnits.GridLines = true;
            this.lvAvailableUnits.Location = new System.Drawing.Point(8, 24);
            this.lvAvailableUnits.Name = "lvAvailableUnits";
            this.lvAvailableUnits.Size = new System.Drawing.Size(320, 360);
            this.lvAvailableUnits.TabIndex = 0;
            this.lvAvailableUnits.UseCompatibleStateImageBehavior = false;
            this.lvAvailableUnits.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Fleet ID";
            this.columnHeader1.Width = 61;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vehicle ID";
            this.columnHeader2.Width = 68;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Hardware Type";
            this.columnHeader7.Width = 88;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Firmware Ver";
            this.columnHeader6.Width = 80;
            // 
            // cmbCommand
            // 
            this.cmbCommand.Items.AddRange(new object[] {
            "Change IP Address",
            "Flush",
            "Flush Then Reset",
            "G-Force Auto Calibrate",
            "Reset",
            "Output 1 On",
            "Output 1 Off",
            "Output 2 On",
            "Output 2 Off",
            "Accident Buffer 1",
            "Accident Buffer 2",
            "Accident Buffer 3",
            "Accident Buffer 4",
            "Send Script File"});
            this.cmbCommand.Location = new System.Drawing.Point(336, 40);
            this.cmbCommand.Name = "cmbCommand";
            this.cmbCommand.Size = new System.Drawing.Size(112, 21);
            this.cmbCommand.TabIndex = 2;
            this.cmbCommand.Text = "Reset";
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(336, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "Command";
            // 
            // btnProcessQueue
            // 
            this.btnProcessQueue.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcessQueue.Location = new System.Drawing.Point(632, 528);
            this.btnProcessQueue.Name = "btnProcessQueue";
            this.btnProcessQueue.Size = new System.Drawing.Size(176, 24);
            this.btnProcessQueue.TabIndex = 8;
            this.btnProcessQueue.Text = "Process Queue";
            this.btnProcessQueue.Click += new System.EventHandler(this.btnProcessQueue_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox1.Controls.Add(this.txtPingIP);
            this.groupBox1.Controls.Add(this.txtSecIP);
            this.groupBox1.Controls.Add(this.numSecIPPort);
            this.groupBox1.Controls.Add(this.label8);
            this.groupBox1.Controls.Add(this.label9);
            this.groupBox1.Controls.Add(this.numNewPingRetries);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Controls.Add(this.numNewIPPort);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.checkBox1);
            this.groupBox1.Controls.Add(this.txtHostName);
            this.groupBox1.Controls.Add(this.txtNewIP);
            this.groupBox1.Location = new System.Drawing.Point(0, 424);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(344, 128);
            this.groupBox1.TabIndex = 19;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Network Info - Sent with Change IP Address Command";
            // 
            // txtPingIP
            // 
            this.txtPingIP.Location = new System.Drawing.Point(111, 82);
            this.txtPingIP.Masked = MTData.Transport.Listener.DatsListenerApp.Mask.IpAddress;
            this.txtPingIP.Name = "txtPingIP";
            this.txtPingIP.Size = new System.Drawing.Size(162, 20);
            this.txtPingIP.TabIndex = 26;
            this.txtPingIP.Text = "173.194.38.152";
            // 
            // txtSecIP
            // 
            this.txtSecIP.Location = new System.Drawing.Point(111, 60);
            this.txtSecIP.Masked = MTData.Transport.Listener.DatsListenerApp.Mask.IpAddress;
            this.txtSecIP.Name = "txtSecIP";
            this.txtSecIP.Size = new System.Drawing.Size(162, 20);
            this.txtSecIP.TabIndex = 25;
            this.txtSecIP.Text = "202.141.210.77";
            // 
            // numSecIPPort
            // 
            this.numSecIPPort.Location = new System.Drawing.Point(279, 60);
            this.numSecIPPort.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numSecIPPort.Name = "numSecIPPort";
            this.numSecIPPort.Size = new System.Drawing.Size(56, 20);
            this.numSecIPPort.TabIndex = 9;
            this.numSecIPPort.Value = new decimal(new int[] {
            3344,
            0,
            0,
            0});
            this.numSecIPPort.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label8
            // 
            this.label8.Location = new System.Drawing.Point(271, 62);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(16, 16);
            this.label8.TabIndex = 23;
            this.label8.Text = ":";
            // 
            // label9
            // 
            this.label9.Location = new System.Drawing.Point(9, 62);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(128, 16);
            this.label9.TabIndex = 21;
            this.label9.Text = "Secondary IP";
            // 
            // numNewPingRetries
            // 
            this.numNewPingRetries.Location = new System.Drawing.Point(111, 104);
            this.numNewPingRetries.Maximum = new decimal(new int[] {
            15,
            0,
            0,
            0});
            this.numNewPingRetries.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numNewPingRetries.Name = "numNewPingRetries";
            this.numNewPingRetries.Size = new System.Drawing.Size(40, 20);
            this.numNewPingRetries.TabIndex = 14;
            this.numNewPingRetries.Value = new decimal(new int[] {
            5,
            0,
            0,
            0});
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(9, 106);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 16);
            this.label7.TabIndex = 16;
            this.label7.Text = "Ping Retries";
            // 
            // numNewIPPort
            // 
            this.numNewIPPort.Location = new System.Drawing.Point(279, 38);
            this.numNewIPPort.Maximum = new decimal(new int[] {
            2147483647,
            0,
            0,
            0});
            this.numNewIPPort.Name = "numNewIPPort";
            this.numNewIPPort.Size = new System.Drawing.Size(56, 20);
            this.numNewIPPort.TabIndex = 4;
            this.numNewIPPort.Value = new decimal(new int[] {
            3344,
            0,
            0,
            0});
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(271, 40);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(16, 16);
            this.label5.TabIndex = 9;
            this.label5.Text = ":";
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(9, 84);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(128, 16);
            this.label6.TabIndex = 14;
            this.label6.Text = "Ping IP Address";
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(9, 40);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(96, 18);
            this.label4.TabIndex = 4;
            this.label4.Text = "Listener IP Address";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Location = new System.Drawing.Point(12, 19);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(140, 17);
            this.checkBox1.TabIndex = 27;
            this.checkBox1.Text = "Use Host Name Lookup";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // txtHostName
            // 
            this.txtHostName.Location = new System.Drawing.Point(111, 39);
            this.txtHostName.Name = "txtHostName";
            this.txtHostName.Size = new System.Drawing.Size(162, 20);
            this.txtHostName.TabIndex = 28;
            this.txtHostName.Visible = false;
            this.txtHostName.Leave += new System.EventHandler(this.txtHostName_Leave);
            // 
            // txtNewIP
            // 
            this.txtNewIP.Location = new System.Drawing.Point(111, 38);
            this.txtNewIP.Masked = MTData.Transport.Listener.DatsListenerApp.Mask.IpAddress;
            this.txtNewIP.Name = "txtNewIP";
            this.txtNewIP.Size = new System.Drawing.Size(162, 20);
            this.txtNewIP.TabIndex = 24;
            this.txtNewIP.Text = "202.141.210.77";
            // 
            // frmQueueCommands
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(816, 554);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.btnProcessQueue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmbCommand);
            this.Controls.Add(this.btnRefershAvaliableList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lvQueued);
            this.Controls.Add(this.btnRemoveSelected);
            this.Controls.Add(this.btnRemoveAll);
            this.Controls.Add(this.btnAddAll);
            this.Controls.Add(this.btnAddSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvAvailableUnits);
            this.Name = "frmQueueCommands";
            this.Text = "Queue Commands";
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmQueueCommands_Closing);
            this.Load += new System.EventHandler(this.frmQueueCommands_Load);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numSecIPPort)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewPingRetries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNewIPPort)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private void btnProcessQueue_Click(object sender, System.EventArgs e)
        {
            if (btnProcessQueue.Text == "Process Queue")
            {
                try
                {
                    /*if(cmbCommand.Text == "Send Script File")
                    {
                        OpenFileDialog oFD = new OpenFileDialog();
                        oFD.AddExtension = false;
                        oFD.AutoUpgradeEnabled = true;
                        oFD.CheckFileExists = true;
                        oFD.DefaultExt = "sh";
                        oFD.Filter = "Shell Script (*.sh)|*.sh|Zipped Shell Script (*.gz)|*.gz|All Files (*.*)|*.*";
                        oFD.InitialDirectory = "c:\\";
                        DialogResult result = oFD.ShowDialog();
                        if (result == DialogResult.OK)
		                {
		                    try
		                    {
                                _scriptFileName = oFD.FileName;
                                _scriptFileBytes = File.ReadAllBytes(_scriptFileName);
		                    }
		                    catch (Exception ex)
		                    {
		                        MessageBox.Show("Error : "  + ex.Message);
                                return;
		                    }
                            if(_scriptFileBytes == null || _scriptFileBytes.Length == 0)
                            {
                                MessageBox.Show("Please select a valid script file.");
                                return;
                            }
		                }
                    }*/

                    if (tFindUnits != null)
                    {
                        tFindUnits.Stop();
                        int iCounter = 0;
                        while (tFindUnits.Running && iCounter < 1000)
                        {
                            Thread.Sleep(100);
                        }
                        tFindUnits = null;
                    }
                    tFindUnits = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(MatchUnitInQueuedList), null);
                    tFindUnits.Start();
                    btnProcessQueue.Text = "Stop Processing Queue";
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnProcessQueue_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnProcessQueue_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
            else
            {
                try
                {
                    if (tFindUnits != null)
                    {
                        tFindUnits.Stop();
                        int iCounter = 0;
                        while (tFindUnits.Running && iCounter < 1000)
                        {
                            Thread.Sleep(100);
                        }
                        tFindUnits = null;
                    }
                    btnProcessQueue.Text = "Process Queue";
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnProcessQueue_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "An error has occured : \r\n\r\n" + sClassName + "btnProcessQueue_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
        }

        #region List Load and maniplation functions
        private void LoadListView()
        {
            string SQLCmd = "SELECT Distinct FleetID, VehicleID, ListenPort from T_GatewayUnit Where FleetID > 0 and VehicleID > 0 ORDER BY FleetID, VehicleID";
            string prpDSN = "";
            string[] sKnownUnits = null;
            string[] sUnknownUnitID = null;
            string sFleetID = "";
            string sUnitID = "";
            System.Data.DataSet objRS = null;
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.SqlClient.SqlConnection objSQLConn = null;
            System.Data.DataRow drUnkownUnit = null;
            System.Data.DataRow[] dResult = null;

            try
            {
                // Clear the list view
                lvAvailableUnits.Items.Clear();

                sKnownUnits = _listener.mDBInterface.KnownUnits();

                // Create the objects
                prpDSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                objSQLConn = new System.Data.SqlClient.SqlConnection(prpDSN);
                objRS = new System.Data.DataSet();
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                if (objSQLConn != null)
                {
                    // Connect to the SQL server
                    objSQLConn.Open();
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(objRS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    objSQLDA.Dispose();
                    objSQLDA = null;
                    objSQLConn.Close();
                    objSQLConn.Dispose();
                    objSQLConn = null;


                    // Retrieve the data value.
                    if (objRS.Tables.Count > 0)
                    {
                        if (objRS.Tables[0].Rows.Count > 0)
                        {
                            #region Add any units that are reporting to the listener but not in the database.
                            if (sKnownUnits != null)
                            {
                                for (int X = 0; X < sKnownUnits.Length; X++)
                                {
                                    sUnknownUnitID = sKnownUnits[X].Split("~".ToCharArray());
                                    if (sUnknownUnitID.Length == 2)
                                    {
                                        sFleetID = sUnknownUnitID[0];
                                        sUnitID = sUnknownUnitID[1];
                                        dResult = objRS.Tables[0].Select("FleetID = " + sFleetID + " AND VehicleID = " + sUnitID);
                                        if (dResult.Length == 0)
                                        {
                                            drUnkownUnit = objRS.Tables[0].NewRow();
                                            drUnkownUnit["FleetID"] = Convert.ToInt32(sFleetID);
                                            drUnkownUnit["VehicleID"] = Convert.ToInt32(sUnitID);
                                            drUnkownUnit["ListenPort"] = "Not In DB,Not In DB";
                                            objRS.Tables[0].Rows.Add(drUnkownUnit);
                                        }
                                    }
                                }
                            }
                            #endregion

                            dResult = objRS.Tables[0].Select("FleetID > 0 AND VehicleID > 0", "FleetID, VehicleID");

                            for (int X = 0; X < dResult.Length; X++)
                            {
                                string[] sItem = new string[4];
                                sItem[0] = Convert.ToString(dResult[X]["FleetID"]);
                                sItem[1] = Convert.ToString(dResult[X]["VehicleID"]);
                                if (dResult[X]["ListenPort"] == System.DBNull.Value)
                                {
                                    sItem[2] = "";
                                    sItem[3] = "";
                                }
                                else
                                {
                                    string sFirmware = Convert.ToString(dResult[X]["ListenPort"]);
                                    if (sFirmware.IndexOf(",") >= 0)
                                    {
                                        string[] sHWSplit = sFirmware.Split(",".ToCharArray());
                                        if (sHWSplit.Length == 2)
                                        {
                                            sItem[2] = sHWSplit[0];
                                            sItem[3] = sHWSplit[1];
                                        }
                                        else
                                        {
                                            sItem[2] = "";
                                            sItem[3] = "";
                                        }
                                    }
                                    else
                                    {
                                        sItem[2] = "";
                                        sItem[3] = "";
                                    }
                                }
                                ListViewItem oLVI = new ListViewItem(sItem);
                                lvAvailableUnits.Items.Add(oLVI);
                            }
                        }
                    }

                    // Clean up Dataset objects
                    dResult = null;
                    if (objRS != null)
                        objRS.Dispose();
                    objRS = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadListView()", ex);
                throw new System.Exception(sClassName + "LoadListView()", ex);
            }
        }


        private void btnRefershAvaliableList_Click(object sender, System.EventArgs e)
        {
            try
            {
                LoadListView();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnRefershAvaliableList_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show(this, "Exception Details : \r\n\r\n" + sClassName + "btnRefershAvaliableList_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }
        #endregion

        #region Processing Thread
        public void AddPacketToCheckList(GatewayProtocolPacket packet)
        {
            try
            {
                if (tFindUnits != null)
                {
                    if (tFindUnits.Running)
                    {
                        lock (oUpdatePackets.SyncRoot)
                        {
                            oUpdatePackets.Add(packet);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
                throw new System.Exception(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
            }
        }
        private object MatchUnitInQueuedList(EnhancedThread sender, object data)
        {
            int iCheckCount = 0;
            ArrayList oLocalList = null;
            ConfigNetworkInfoGPPacket netPacket = null;

            while (!sender.Stopping)
            {
                try
                {
                    lock (oSendCmdToList.SyncRoot)
                    {
                        iCheckCount = oSendCmdToList.Count;
                    }
                    if (iCheckCount > 0)
                    {
                        oLocalList = new ArrayList();
                        #region Copy the items waiting to be processed into a local array.
                        lock (oUpdatePackets.SyncRoot)
                        {
                            for (int X = oUpdatePackets.Count - 1; X >= 0; X--)
                            {
                                oLocalList.Add(oUpdatePackets[X]);
                                oUpdatePackets.RemoveAt(X);
                            }
                        }
                        #endregion

                        if (oLocalList.Count > 0)
                        {
                            for (int X = 0; X < oLocalList.Count; X++)
                            {
                                GatewayProtocolPacket packet = (GatewayProtocolPacket)oLocalList[X];
                                // Only start a new download as a response to a status packet.
                                if (packet.cMsgType == (byte)0xC1)
                                {
                                    byte[] bConvert = new byte[4];
                                    bConvert[0] = packet.cFleetId;
                                    string sFleetID = Convert.ToString(BitConverter.ToInt32(bConvert, 0));
                                    string sVehicleID = Convert.ToString(Convert.ToInt32(packet.iVehicleId));
                                    string sKey = sFleetID.PadLeft(4, '0') + "~" + sVehicleID.PadLeft(4, '0');
                                    lock (oSendCmdToList.SyncRoot)
                                    {
                                        if (oSendCmdToList.ContainsKey(sKey))
                                        {
                                            string sCmd = (string)oSendCmdToList[sKey];
                                            switch (sCmd)
                                            {
                                                case "Reset":
                                                    _listener.mDBInterface.SendResetRequest(packet);
                                                    UpdateStatus(sFleetID, sVehicleID, "Reset Sent To Unit.");
                                                    _log.Info("Send Queued Reset to Unit " + sFleetID + "/" + sVehicleID);
                                                    break;
                                                case "Flush":
                                                    _listener.mDBInterface.SendFlushRequest(packet);
                                                    UpdateStatus(sFleetID, sVehicleID, "Flush Sent To Unit.");
                                                    _log.Info("Send Queued Flush to Unit " + sFleetID + "/" + sVehicleID);
                                                    break;
                                                case "Flush Then Reset":
                                                    _listener.mDBInterface.SendFlushRequest(packet);
                                                    Thread.Sleep(1000);
                                                    _listener.mDBInterface.SendResetRequest(packet);
                                                    UpdateStatus(sFleetID, sVehicleID, "Flush and Reset Sent To Unit.");
                                                    _log.Info("Send Queued Flush and Reset to Unit " + sFleetID + "/" + sVehicleID);
                                                    break;
                                                case "G-Force Auto Calibrate":
                                                    _listener.mDBInterface.SendGForceAutoCalibrateRequest(packet);
                                                    UpdateStatus(sFleetID, sVehicleID, "G-Force Auto Calibrate To Unit.");
                                                    _log.Info("Sent G-Force Auto Calibrate to Unit " + sFleetID + "/" + sVehicleID);
                                                    break;
                                                case "Accident Buffer 1":
                                                case "Accident Buffer 2":
                                                case "Accident Buffer 3":
                                                case "Accident Buffer 4":
                                                    int index = Int32.Parse(sCmd.Substring(sCmd.Length - 1, 1));
                                                    //	Accidents are actually held as 0, 2, 4, 6..
                                                    _listener.mDBInterface.SendAccidentUploadRequest(packet, ((index - 1) * 2));
                                                    UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent To Unit.");
                                                    _log.Info("Sent Queued " + sCmd + " to Unit " + sFleetID + "/" + sVehicleID);
                                                    break;
                                                case "Change IP Address":
													if (string.IsNullOrEmpty(txtNewIP.Text))
													{
														UpdateStatus(sFleetID, sVehicleID, sCmd + " Bad IP Address specified.  No update sent.");
													}
													else
													{
                                                        if (string.IsNullOrEmpty(txtPingIP.Text))
														{
															UpdateStatus(sFleetID, sVehicleID, sCmd + " Bad IP Address specified for Ping Address.  No update sent.");
														}
														else
														{
															try
															{
																netPacket = new ConfigNetworkInfoGPPacket(packet, "");
																netPacket.cNewFleetId = 0;
																netPacket.iNewUnitId= 0;
																netPacket.cNetwork = 1;
                                                                if (checkBox1.Checked)
                                                                {
                                                                    try
                                                                    {
                                                                        netPacket.ServerHostName = txtHostName.Text;
                                                                    }
                                                                    catch 
                                                                    {
                                                                        UpdateStatus(sFleetID, sVehicleID, sCmd + " Bad IP Address specified.  No update sent.");
                                                                        return null;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    try
                                                                    {
                                                                        netPacket.mPrimaryServerIPAddress = System.Net.IPAddress.Parse(txtNewIP.Text);
                                                                    }
                                                                    catch 
                                                                    {
                                                                        UpdateStatus(sFleetID, sVehicleID, sCmd + " Bad IP Address specified.  No update sent.");
                                                                        return null;
                                                                    }
                                                                }
                                                                netPacket.iPrimaryServerRxPort = Convert.ToInt16(numNewIPPort.Value);
                                                                if (string.IsNullOrEmpty(txtSecIP.Text))
                                                                {
                                                                    netPacket.mSecondaryServerIPAddress = System.Net.IPAddress.Parse("0.0.0.0");
                                                                    netPacket.iSecondaryServerRxPort = 0;
                                                                }
                                                                else
                                                                {
                                                                    try
                                                                    {
                                                                        netPacket.mSecondaryServerIPAddress = System.Net.IPAddress.Parse(txtSecIP.Text);
                                                                    }
                                                                    catch 
                                                                    {
                                                                        UpdateStatus(sFleetID, sVehicleID, sCmd + " Bad Secondary server IP Address specified.  No update sent.");
                                                                        return null;
                                                                    }
                                                                    netPacket.iSecondaryServerRxPort = Convert.ToInt16(numSecIPPort.Value);
                                                                }
                                                                try
                                                                {
                                                                    netPacket.mPingableIPAddress = System.Net.IPAddress.Parse(txtPingIP.Text);
                                                                }
                                                                catch
                                                                {
                                                                    UpdateStatus(sFleetID, sVehicleID, sCmd + " Bad ping IP Address specified.  No update sent.");
                                                                    return null;
                                                                }
                                                                netPacket.cMaxPingRetries = Convert.ToByte(numNewPingRetries.Value);
                                                                netPacket.cPort1BaudRate = 0;
                                                                _listener.mDBInterface.SendNetworkInfo(netPacket);
                                                                UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Change IP Address Packet.");
                                                                _log.Info("Change of IP Address Sent to Unit " + sFleetID + "/" + sVehicleID + " : New IP = " + txtNewIP.Text + ":" + numNewIPPort.Value + ", Ping Address = " + txtPingIP.Text + ", Ping Retries = " + numNewPingRetries.Value);
                                                            }
                                                            catch (System.Exception exNetPacket)
                                                            {
                                                                _log.Error(sClassName + "MatchUnitInQueuedList(EnhancedThread sender, object data)", exNetPacket);
                                                            }
                                                        }
                                                    }
                                                    break;
                                                case "Output 1 On":
                                                    _listener.mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 1, true);
                                                    _log.Info("Sent Output 1 On to Unit " + sFleetID + "/" + sVehicleID);
                                                    UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 1 On Packet.");
                                                    break;
                                                case "Output 1 Off":
                                                    _listener.mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 1, false);
                                                    _log.Info("Sent Output 1 Off to Unit " + sFleetID + "/" + sVehicleID);
                                                    UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 1 Off Packet.");
                                                    break;
                                                case "Output 2 On":
                                                    _listener.mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 2, true);
                                                    _log.Info("Sent Output 2 On to Unit " + sFleetID + "/" + sVehicleID);
                                                    UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 2 On Packet.");
                                                    break;
                                                case "Output 2 Off":
                                                    _listener.mDBInterface.SendOutputRequestToUnit(packet.cFleetId, packet.iVehicleId, 2, false);
                                                    _log.Info("Sent Output 2 Off to Unit " + sFleetID + "/" + sVehicleID);
                                                    UpdateStatus(sFleetID, sVehicleID, sCmd + " Sent Output 2 Off Packet.");
                                                    break;
                                                default:
                                                    break;
                                            }
                                            lock (oSendCmdToList.SyncRoot)
                                            {
                                                oSendCmdToList.Remove(sKey);
                                            }
                                        }
                                    }
                                }
                            }
                            oLocalList.Clear();
                        }
                        else
                            Thread.Sleep(500);
                    }
                    else
                        Thread.Sleep(1000);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "MatchUnitInQueuedList(EnhancedThread sender, object data)", ex);
                    sender.Stop();
                    btnProcessQueue.Text = "Process Queue";
                }
            }
            return null;
        }

        private void UpdateStatus(string sFleetID, string sVehicleID, string sStatus)
        {
            if (lvQueued.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { UpdateStatus(sFleetID, sVehicleID, sStatus); }));
            else
            {
                ListViewItem oLVI = null;
                try
                {
                    for (int X = 0; X < lvQueued.Items.Count; X++)
                    {
                        oLVI = lvQueued.Items[X];
                        if (oLVI.SubItems[0].Text == sFleetID && oLVI.SubItems[1].Text == sVehicleID)
                        {
                            oLVI.SubItems[3].Text = sStatus;
                            break;
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "UpdateStatus(string sFleetID, string sVehicleID, string sStatus)", ex);
                }
            }
        }
        #endregion

        #region Add Remove Items from the queued list
        private void btnAddAll_Click(object sender, System.EventArgs e)
        {
            if (lvQueued.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnAddAll_Click(sender, e); }));
            else
            {
                bool bAddItem = true;
                ListViewItem oLVI = null;
                try
                {
                    lvQueued.Items.Clear();
                    for (int X = 0; X < lvAvailableUnits.Items.Count; X++)
                    {
                        oLVI = lvAvailableUnits.Items[X];
                        string sFleetID = oLVI.SubItems[0].Text;
                        string sVehicleID = oLVI.SubItems[1].Text;
                        string sCommand = cmbCommand.Text;
                        string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                        bAddItem = true;
                        // Check if this vehicle is already in the list.
                        for (int Y = 0; Y < lvQueued.Items.Count; Y++)
                        {
                            ListViewItem oLVIQueued = lvQueued.Items[Y];
                            string sQueuedFleetID = oLVIQueued.SubItems[0].Text;
                            string sQueuedVehicleID = oLVIQueued.SubItems[1].Text;
                            if (sQueuedFleetID == sFleetID && sQueuedVehicleID == sVehicleID)
                            {
                                bAddItem = false;
                                MessageBox.Show(this, "There is already a command queued for Vehicle " + sFleetID + " / " + sVehicleID + ".  Please remove this command and then add the new command.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                break;
                            }
                        }

                        if (bAddItem)
                        {
                            lock (oSendCmdToList.SyncRoot)
                            {
                                if (oSendCmdToList.ContainsKey(sKey))
                                {
                                    oSendCmdToList.Remove(sKey);
                                }
                                string[] sQueuedRecord = new string[4];
                                sQueuedRecord[0] = sFleetID;
                                sQueuedRecord[1] = sVehicleID;
                                sQueuedRecord[2] = sCommand;
                                sQueuedRecord[3] = "Queued.";
                                lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                                oSendCmdToList.Add(sKey, sCommand);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnAddAll_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAddAll_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnRemoveAll_Click(object sender, System.EventArgs e)
        {
            if (lvQueued.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnRemoveAll_Click(sender, e); }));
            else
            {
                try
                {
                    lock (oSendCmdToList.SyncRoot)
                    {
                        for (int X = 0; X < lvQueued.Items.Count; X++)
                        {
                            ListViewItem oLVI = lvQueued.Items[X];
                            string sFleetID = oLVI.SubItems[0].Text;
                            string sVehicleID = oLVI.SubItems[1].Text;
                            string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                            lock (oSendCmdToList.SyncRoot)
                            {
                                if (oSendCmdToList.ContainsKey(sKey))
                                {
                                    oSendCmdToList.Remove(sKey);
                                }
                            }
                            lvQueued.Items.RemoveAt(X);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnRemoveAll_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRemoveAll_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnAddSelected_Click(object sender, System.EventArgs e)
        {
            if (lvQueued.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnAddSelected_Click(sender, e); }));
            else
            {
                string sCommand = "";
                try
                {
                    for (int X = 0; X < lvAvailableUnits.Items.Count; X++)
                    {
                        if (lvAvailableUnits.Items[X].Selected)
                        {
                            ListViewItem oLVI = lvAvailableUnits.Items[X];
                            string sFleetID = oLVI.SubItems[0].Text;
                            string sVehicleID = oLVI.SubItems[1].Text;
                            sCommand = cmbCommand.Text;
                            string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');

                            // Check if this vehicle is already in the list.
                            for (int Y = 0; Y < lvQueued.Items.Count; Y++)
                            {
                                ListViewItem oLVIQueued = lvQueued.Items[Y];
                                string sQueuedFleetID = oLVIQueued.SubItems[0].Text;
                                string sQueuedVehicleID = oLVIQueued.SubItems[1].Text;
                                if (sQueuedFleetID == sFleetID && sQueuedVehicleID == sVehicleID)
                                {
                                    MessageBox.Show(this, "There is already a command queued for Vehicle " + sFleetID + " / " + sVehicleID + ".  Please remove this command and then add the new command.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                    return;
                                }
                            }

                            lock (oSendCmdToList.SyncRoot)
                            {
                                if (oSendCmdToList.ContainsKey(sKey))
                                {
                                    oSendCmdToList.Remove(sKey);
                                }
                                string[] sQueuedRecord = new string[4];
                                sQueuedRecord[0] = sFleetID;
                                sQueuedRecord[1] = sVehicleID;
                                sQueuedRecord[2] = sCommand;
                                sQueuedRecord[3] = "Queued.";
                                lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                                oSendCmdToList.Add(sKey, sCommand);
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnAddSelected_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAddSelected_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnRemoveSelected_Click(object sender, System.EventArgs e)
        {
            if (lvQueued.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnRemoveSelected_Click(sender, e); }));
            else
            {
                try
                {
                    for (int X = lvQueued.Items.Count - 1; X >= 0; X--)
                    {
                        if (lvQueued.Items[X].Selected)
                        {
                            ListViewItem oLVI = lvQueued.Items[X];
                            string sFleetID = oLVI.SubItems[0].Text;
                            string sVehicleID = oLVI.SubItems[1].Text;
                            string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                            lock (oSendCmdToList.SyncRoot)
                            {
                                if (oSendCmdToList.ContainsKey(sKey))
                                {
                                    oSendCmdToList.Remove(sKey);
                                }
                            }
                            lvQueued.Items.RemoveAt(X);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnRemoveSelected_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRemoveSelected_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }
        #endregion

        private void frmQueueCommands_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (tFindUnits != null)
                {
                    tFindUnits.Stop();
                    int iCounter = 0;
                    while (tFindUnits.Running && iCounter < 1000)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmQueueCommands_Closing(object sender, System.ComponentModel.CancelEventArgs e)", ex);
                throw new System.Exception(sClassName + "frmQueueCommands_Closing(object sender, System.ComponentModel.CancelEventArgs e)", ex);
            }
            try
            {
                tFindUnits = null;
            }
            catch (System.Exception)
            {
            }

        }

        private void frmQueueCommands_Load(object sender, System.EventArgs e)
        {
            try
            {
                this.cmbCommand.Items.Clear();
                this.cmbCommand.Items.AddRange(new object[] {
															"Accident Buffer 1", 
															"Accident Buffer 2",
															"Accident Buffer 3",
															"Accident Buffer 4",
															"Change IP Address",
															"Flush",
															"Flush Then Reset",
															"G-Force Auto Calibrate",
															"Reset",
															"Output 1 On",
															"Output 1 Off",
															"Output 2 On",
															"Output 2 Off"});
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmQueueCommands_Load(object sender, System.EventArgs e)", ex);
            }
        }

		private void numericUpDown1_ValueChanged(object sender, System.EventArgs e)
		{
		
		}

        private void checkBox1_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if(checkBox1.Checked)
                {
                    txtHostName.Visible = true;
                    txtNewIP.Visible = false;
                }
                else
                {
                    txtHostName.Visible = false;
                    txtNewIP.Visible = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "checkBox1_CheckedChanged(object sender, EventArgs e)", ex);
            }
        }

        private void txtHostName_Leave(object sender, EventArgs e)
        {
            //try parsing the address as an IP, if it is an IP then thrugh an error
            try
            {
                System.Net.IPAddress.Parse(txtHostName.Text);
                MessageBox.Show(this, string.Format("{0} is an IP should be a host name", txtHostName.Text));
                txtHostName.Text = "";
            }
            catch 
            {
            }

        }
    }
}

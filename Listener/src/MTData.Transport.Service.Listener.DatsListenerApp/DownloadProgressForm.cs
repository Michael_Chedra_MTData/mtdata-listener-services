using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;

namespace MTData.Transport.Listener.DatsListenerApp
{
	/// <summary>
	/// Summary description for DownloadProgress.
	/// </summary>
	public class DownloadProgressForm : System.Windows.Forms.Form
	{
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.GroupBox groupBox2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		public System.Windows.Forms.TextBox txtPacketCount;
		public System.Windows.Forms.TextBox txtPacketTotal;
		public System.Windows.Forms.ProgressBar pbProgress;
		public System.Windows.Forms.TextBox txtFleet;
		public System.Windows.Forms.TextBox txtSource;
		public System.Windows.Forms.TextBox txtUnit;
		public System.Windows.Forms.Button btnAbort;
		private DownloadProcessor mProcessor;
		private System.Windows.Forms.StatusBar sBar;
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;

		public DownloadProgressForm(DownloadProcessor mProcessor)
		{
			//
			// Required for Windows Form Designer support
			//
			InitializeComponent();

			//
			// TODO: Add any constructor code after InitializeComponent call
			//
			this.mProcessor = mProcessor;
			switch (mProcessor.DownloadType) 
			{
				case DownloadType.InterfaceBox:
					this.Text = "I/O Box Download";
					break;
				case DownloadType.MobileDataTerminal:
					this.Text = "MDT Download";
					break;
				default:
					break;
			}
			sStatusBarText = "Status : Waiting to start download.";
			this.Refresh();
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.txtUnit = new System.Windows.Forms.TextBox();
			this.label6 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.txtSource = new System.Windows.Forms.TextBox();
			this.txtFleet = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label1 = new System.Windows.Forms.Label();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.txtPacketTotal = new System.Windows.Forms.TextBox();
			this.txtPacketCount = new System.Windows.Forms.TextBox();
			this.label4 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.pbProgress = new System.Windows.Forms.ProgressBar();
			this.btnAbort = new System.Windows.Forms.Button();
			this.sBar = new System.Windows.Forms.StatusBar();
			this.groupBox1.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.txtUnit);
			this.groupBox1.Controls.Add(this.label6);
			this.groupBox1.Controls.Add(this.label5);
			this.groupBox1.Controls.Add(this.txtSource);
			this.groupBox1.Controls.Add(this.txtFleet);
			this.groupBox1.Controls.Add(this.label2);
			this.groupBox1.Controls.Add(this.label1);
			this.groupBox1.Location = new System.Drawing.Point(8, 8);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(296, 72);
			this.groupBox1.TabIndex = 0;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Download Info";
			// 
			// txtUnit
			// 
			this.txtUnit.Location = new System.Drawing.Point(232, 16);
			this.txtUnit.Name = "txtUnit";
			this.txtUnit.ReadOnly = true;
			this.txtUnit.Size = new System.Drawing.Size(56, 20);
			this.txtUnit.TabIndex = 6;
			this.txtUnit.Text = "";
			// 
			// label6
			// 
			this.label6.Location = new System.Drawing.Point(200, 16);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(28, 16);
			this.label6.TabIndex = 5;
			this.label6.Text = "Unit";
			this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(112, 16);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(40, 16);
			this.label5.TabIndex = 4;
			this.label5.Text = "Fleet";
			this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// txtSource
			// 
			this.txtSource.Location = new System.Drawing.Point(112, 40);
			this.txtSource.Name = "txtSource";
			this.txtSource.ReadOnly = true;
			this.txtSource.Size = new System.Drawing.Size(176, 20);
			this.txtSource.TabIndex = 3;
			this.txtSource.Text = "";
			// 
			// txtFleet
			// 
			this.txtFleet.Location = new System.Drawing.Point(152, 16);
			this.txtFleet.Name = "txtFleet";
			this.txtFleet.ReadOnly = true;
			this.txtFleet.Size = new System.Drawing.Size(40, 20);
			this.txtFleet.TabIndex = 2;
			this.txtFleet.Text = "";
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(16, 40);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(100, 16);
			this.label2.TabIndex = 1;
			this.label2.Text = "Source File:";
			this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(16, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(96, 16);
			this.label1.TabIndex = 0;
			this.label1.Text = "Downloading To:";
			this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.txtPacketTotal);
			this.groupBox2.Controls.Add(this.txtPacketCount);
			this.groupBox2.Controls.Add(this.label4);
			this.groupBox2.Controls.Add(this.label3);
			this.groupBox2.Controls.Add(this.pbProgress);
			this.groupBox2.Location = new System.Drawing.Point(8, 88);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(296, 80);
			this.groupBox2.TabIndex = 1;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Download Progress";
			// 
			// txtPacketTotal
			// 
			this.txtPacketTotal.Location = new System.Drawing.Point(190, 48);
			this.txtPacketTotal.Name = "txtPacketTotal";
			this.txtPacketTotal.ReadOnly = true;
			this.txtPacketTotal.Size = new System.Drawing.Size(34, 20);
			this.txtPacketTotal.TabIndex = 4;
			this.txtPacketTotal.Text = "";
			// 
			// txtPacketCount
			// 
			this.txtPacketCount.Location = new System.Drawing.Point(121, 48);
			this.txtPacketCount.Name = "txtPacketCount";
			this.txtPacketCount.ReadOnly = true;
			this.txtPacketCount.Size = new System.Drawing.Size(34, 20);
			this.txtPacketCount.TabIndex = 3;
			this.txtPacketCount.Text = "";
			// 
			// label4
			// 
			this.label4.Location = new System.Drawing.Point(166, 48);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(21, 23);
			this.label4.TabIndex = 2;
			this.label4.Text = "of";
			this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(73, 48);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(48, 23);
			this.label3.TabIndex = 1;
			this.label3.Text = "Packet";
			this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
			// 
			// pbProgress
			// 
			this.pbProgress.Location = new System.Drawing.Point(8, 16);
			this.pbProgress.Name = "pbProgress";
			this.pbProgress.Size = new System.Drawing.Size(280, 23);
			this.pbProgress.TabIndex = 0;
			// 
			// btnAbort
			// 
			this.btnAbort.Location = new System.Drawing.Point(119, 176);
			this.btnAbort.Name = "btnAbort";
			this.btnAbort.TabIndex = 2;
			this.btnAbort.Text = "Abort";
			this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
			// 
			// sBar
			// 
			this.sBar.Location = new System.Drawing.Point(0, 205);
			this.sBar.Name = "sBar";
			this.sBar.Size = new System.Drawing.Size(312, 16);
			this.sBar.TabIndex = 3;
			// 
			// DownloadProgressForm
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(312, 221);
			this.ControlBox = false;
			this.Controls.Add(this.sBar);
			this.Controls.Add(this.btnAbort);
			this.Controls.Add(this.groupBox2);
			this.Controls.Add(this.groupBox1);
			this.MaximizeBox = false;
			this.MinimizeBox = false;
			this.MinimumSize = new System.Drawing.Size(320, 232);
			this.Name = "DownloadProgressForm";
			this.Text = "Program Download";
			this.TopMost = true;
			this.Paint += new System.Windows.Forms.PaintEventHandler(this.DownloadProgressForm_Paint);
			this.groupBox1.ResumeLayout(false);
			this.groupBox2.ResumeLayout(false);
			this.ResumeLayout(false);

		}
		#endregion

		private void btnAbort_Click(object sender, System.EventArgs e)
		{

			mProcessor.bUserWantsToContinue = false;
			this.Close();

		}

		public string sStatusBarText = "";
		private void DownloadProgressForm_Paint(object sender, System.Windows.Forms.PaintEventArgs e)
		{
			if(sStatusBarText != "")
			{
				sBar.Text = sStatusBarText;
				sStatusBarText = "";
			}
		}
	}
}

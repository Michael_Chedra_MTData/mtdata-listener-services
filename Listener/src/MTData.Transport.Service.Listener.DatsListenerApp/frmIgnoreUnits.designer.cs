namespace MTData.Transport.Listener.DatsListenerApp
{
    partial class frmIgnoreUnits
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnIgnoreReports = new System.Windows.Forms.Button();
            this.btnRefershAvaliableList = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnAddAll = new System.Windows.Forms.Button();
            this.btnAddSelected = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lvAvailableUnits = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.lvQueued = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.chkDoNotReplyToPings = new System.Windows.Forms.CheckBox();
            this.chkDoNotReplyToUnknownUnitPing = new System.Windows.Forms.CheckBox();
            this.SuspendLayout();
            // 
            // btnIgnoreReports
            // 
            this.btnIgnoreReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnIgnoreReports.Location = new System.Drawing.Point(893, 366);
            this.btnIgnoreReports.Name = "btnIgnoreReports";
            this.btnIgnoreReports.Size = new System.Drawing.Size(150, 24);
            this.btnIgnoreReports.TabIndex = 24;
            this.btnIgnoreReports.Text = "Ignore Reports";
            this.btnIgnoreReports.Click += new System.EventHandler(this.btnIgnoreReports_Click);
            // 
            // btnRefershAvaliableList
            // 
            this.btnRefershAvaliableList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefershAvaliableList.Location = new System.Drawing.Point(4, 366);
            this.btnRefershAvaliableList.Name = "btnRefershAvaliableList";
            this.btnRefershAvaliableList.Size = new System.Drawing.Size(546, 24);
            this.btnRefershAvaliableList.TabIndex = 18;
            this.btnRefershAvaliableList.Text = "Refresh Avaliable List";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(540, -15);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 26;
            this.label2.Text = "Queued for Download";
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Location = new System.Drawing.Point(571, 131);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveSelected.TabIndex = 22;
            this.btnRemoveSelected.Text = "<";
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(571, 99);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveAll.TabIndex = 21;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnAddAll
            // 
            this.btnAddAll.Location = new System.Drawing.Point(571, 67);
            this.btnAddAll.Name = "btnAddAll";
            this.btnAddAll.Size = new System.Drawing.Size(32, 24);
            this.btnAddAll.TabIndex = 20;
            this.btnAddAll.Text = ">>";
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            // 
            // btnAddSelected
            // 
            this.btnAddSelected.Location = new System.Drawing.Point(571, 35);
            this.btnAddSelected.Name = "btnAddSelected";
            this.btnAddSelected.Size = new System.Drawing.Size(32, 24);
            this.btnAddSelected.TabIndex = 19;
            this.btnAddSelected.Text = ">";
            this.btnAddSelected.Click += new System.EventHandler(this.btnAddSelected_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(92, -15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 25;
            this.label1.Text = "Available Units";
            // 
            // lvAvailableUnits
            // 
            this.lvAvailableUnits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lvAvailableUnits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader6,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11});
            this.lvAvailableUnits.FullRowSelect = true;
            this.lvAvailableUnits.GridLines = true;
            this.lvAvailableUnits.Location = new System.Drawing.Point(4, 1);
            this.lvAvailableUnits.Name = "lvAvailableUnits";
            this.lvAvailableUnits.Size = new System.Drawing.Size(561, 359);
            this.lvAvailableUnits.TabIndex = 17;
            this.lvAvailableUnits.UseCompatibleStateImageBehavior = false;
            this.lvAvailableUnits.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Fleet ID";
            this.columnHeader1.Width = 61;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vehicle ID";
            this.columnHeader2.Width = 68;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Hardware Type";
            this.columnHeader7.Width = 88;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Firmware Ver";
            this.columnHeader6.Width = 80;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Serial Number";
            this.columnHeader9.Width = 80;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "IP Address";
            this.columnHeader10.Width = 90;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "IMEI";
            // 
            // lvQueued
            // 
            this.lvQueued.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvQueued.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader8,
            this.columnHeader12,
            this.columnHeader13,
            this.columnHeader14});
            this.lvQueued.FullRowSelect = true;
            this.lvQueued.GridLines = true;
            this.lvQueued.Location = new System.Drawing.Point(609, 1);
            this.lvQueued.Name = "lvQueued";
            this.lvQueued.Size = new System.Drawing.Size(430, 359);
            this.lvQueued.TabIndex = 27;
            this.lvQueued.UseCompatibleStateImageBehavior = false;
            this.lvQueued.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Fleet ID";
            this.columnHeader3.Width = 61;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vehicle ID";
            this.columnHeader4.Width = 68;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Hardware Type";
            this.columnHeader5.Width = 88;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Firmware Ver";
            this.columnHeader8.Width = 80;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Serial Number";
            this.columnHeader12.Width = 80;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "IP Address";
            this.columnHeader13.Width = 90;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "IMEI";
            // 
            // chkDoNotReplyToPings
            // 
            this.chkDoNotReplyToPings.AutoSize = true;
            this.chkDoNotReplyToPings.Location = new System.Drawing.Point(556, 371);
            this.chkDoNotReplyToPings.Name = "chkDoNotReplyToPings";
            this.chkDoNotReplyToPings.Size = new System.Drawing.Size(135, 17);
            this.chkDoNotReplyToPings.TabIndex = 28;
            this.chkDoNotReplyToPings.Text = "Do Not Reply To Pings";
            this.chkDoNotReplyToPings.UseVisualStyleBackColor = true;
            // 
            // chkDoNotReplyToUnknownUnitPing
            // 
            this.chkDoNotReplyToUnknownUnitPing.AutoSize = true;
            this.chkDoNotReplyToUnknownUnitPing.Location = new System.Drawing.Point(697, 371);
            this.chkDoNotReplyToUnknownUnitPing.Name = "chkDoNotReplyToUnknownUnitPing";
            this.chkDoNotReplyToUnknownUnitPing.Size = new System.Drawing.Size(178, 17);
            this.chkDoNotReplyToUnknownUnitPing.TabIndex = 29;
            this.chkDoNotReplyToUnknownUnitPing.Text = "Do Not Reply To Pings from 0/0";
            this.chkDoNotReplyToUnknownUnitPing.UseVisualStyleBackColor = true;
            // 
            // frmIgnoreUnits
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1045, 393);
            this.Controls.Add(this.chkDoNotReplyToUnknownUnitPing);
            this.Controls.Add(this.chkDoNotReplyToPings);
            this.Controls.Add(this.lvQueued);
            this.Controls.Add(this.btnIgnoreReports);
            this.Controls.Add(this.btnRefershAvaliableList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.btnRemoveSelected);
            this.Controls.Add(this.btnRemoveAll);
            this.Controls.Add(this.btnAddAll);
            this.Controls.Add(this.btnAddSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvAvailableUnits);
            this.Name = "frmIgnoreUnits";
            this.Text = "Ignore Report from these units..";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmIgnoreUnits_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnIgnoreReports;
        private System.Windows.Forms.Button btnRefershAvaliableList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnAddAll;
        private System.Windows.Forms.Button btnAddSelected;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvAvailableUnits;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ListView lvQueued;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.CheckBox chkDoNotReplyToPings;
        private System.Windows.Forms.CheckBox chkDoNotReplyToUnknownUnitPing;
    }
}
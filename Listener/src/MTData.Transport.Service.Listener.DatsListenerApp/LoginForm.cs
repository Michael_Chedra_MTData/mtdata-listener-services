﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.ServiceModel;
using System.Text;
using System.Windows.Forms;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public partial class LoginForm : Form
    {
        private bool canLoadMain = false;
        private int LastServerIndex;
        private string LastUserName;

        public LoginForm()
        {
            InitializeComponent();

            this.FetchRegistryValues();

            this.LoadServers();

            if (this.LastUserName != string.Empty)
            {
                this.txtUsername.Text = this.LastUserName;
            }

            this.EnableButtons();

            if (this.LastUserName == string.Empty)
            {
                this.txtUsername.Select();
            }
            else
            {
                this.txtPassword.Select();
            }

            this.Text = "Listener Endpoint Login " + System.Windows.Forms.Application.ProductVersion;
        }

        public bool LoadMain
        {
            get { return this.canLoadMain; }
        }

        private void txtUsername_TextChanged(object sender, EventArgs e)
        {
            this.EnableButtons();
        }

        private void txtPassword_TextChanged(object sender, EventArgs e)
        {
            this.EnableButtons();
        }

        private void btnLogin_Click(object sender, EventArgs e)
        {
            Cursor = Cursors.WaitCursor;

            Helpers.CurrentConnection = (KeyValuePair<string, string>)cmboServerList.SelectedItem;
            MTData.Transport.Service.Listener.GatewayServiceBL.Credentials appCred = new Service.Listener.GatewayServiceBL.Credentials();
            MTData.Common.Crypto.TextEncryption encryptor = new Common.Crypto.TextEncryption();
            ListenerGatewayWCF.GatewayServiceDefinitionClient listenerGatewayServiceCient = Helpers.ListenerGateway();
            int appVersion = 0;

            try
            { 
                appVersion = listenerGatewayServiceCient.AppVersion();
            }
            catch (ActionNotSupportedException exNS)
            {
                // Version 7 or below uses old encryption method
            }
            catch (Exception ex)
            {

            }
            appCred.Username = txtUsername.Text;
            // AppVersion is a new web method exists from Version 8, so version 7 or below it will be null
            if (appVersion <= 7)
            {
                appCred.Password = encryptor.Encrypt(encryptor.EncryptBase64(txtPassword.Text));
            }
            else 
            {
                // appVersion >= 8
                appCred.Password = encryptor.Encrypt(encryptor.EncryptSaltBase64(txtUsername.Text, txtPassword.Text));
            }

            try
            {
                int userID = 0;

                userID = Helpers.ListenerGateway().CheckLogin(appCred);

                if (userID > 0)
                {
                    appCred.UserID = userID;
                    Helpers.UserCredentials = appCred;

                    this.canLoadMain = true;

                    this.SaveRegistryValues();

                    Cursor = Cursors.Default;
                    this.Close();
                }
                else
                {
                    Cursor = Cursors.Default;
                    MessageBox.Show("Username/Password incorrect", "Login Error");
                }
            }
            catch (System.ServiceModel.EndpointNotFoundException)
            {
                Cursor = Cursors.Default;
                MessageBox.Show("Endpoint configuration incorrect or server offline", "Endpoint Error");
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Default;
                MessageBox.Show(ex.Message, "Login Error");
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cmboServerList_SelectedIndexChanged(object sender, EventArgs e)
        {
            this.EnableButtons();
        }

        private void FetchRegistryValues()
        {
            this.LastServerIndex = int.Parse(Helpers.ReadRegistryKey(@"MTDATA\ListenerTool", "LastServer") == null ? 0.ToString() : Helpers.ReadRegistryKey(@"MTDATA\ListenerTool", "LastServer"));
            this.LastUserName = Helpers.ReadRegistryKey(@"MTDATA\ListenerTool", "LastUser") == null ? string.Empty : Helpers.ReadRegistryKey(@"MTDATA\ListenerTool", "LastUser");
        }

        private void SaveRegistryValues()
        {
            Helpers.WriteRegistryKey(@"MTDATA\ListenerTool", "LastServer", this.cmboServerList.SelectedIndex);

            Helpers.WriteRegistryKey(@"MTDATA\ListenerTool", "LastUser", this.txtUsername.Text);

            if (this.LastServerIndex != this.cmboServerList.SelectedIndex)
            {
                Helpers.WriteRegistryKey(@"MTDATA\ListenerTool", "AutoLog", "False");
            }
        }

        private void LoadServers()
        {
            Servers servers = System.Configuration.ConfigurationManager.GetSection("servers") as Servers;

            if (servers != null)
            {
                cmboServerList.DataSource = servers.ServerList;
                cmboServerList.DisplayMember = "Key";
                cmboServerList.ValueMember = "Value";

                if (servers.ServerList.Count > 0 && servers.ServerList.Count > LastServerIndex)
                {
                    if (this.LastServerIndex > cmboServerList.Items.Count)
                    {
                        this.LastServerIndex = 0;
                    }

                    cmboServerList.SelectedIndex = this.LastServerIndex;
                }
            }

        }

        private void EnableButtons()
        {
            this.btnLogin.Enabled = this.cmboServerList.Items.Count > 0 && this.txtUsername.Text.Length > 0 && this.txtPassword.Text.Length > 0
                && this.txtUsername.Text.ToLower() != "mtdata";
        }

        private void txtUsername_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.txtUsername.Text.ToLower() != "mtdata")
            {
                txtPassword.Focus();
            }
        }

        private void txtPassword_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter && this.txtUsername.Text.ToLower() != "mtdata")
            {
                btnLogin_Click(sender, null);
            }
        }
    }
}

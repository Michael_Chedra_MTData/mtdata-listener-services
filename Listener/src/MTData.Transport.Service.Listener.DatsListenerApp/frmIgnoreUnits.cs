using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using log4net;
using MTData.Transport.Service.Listener.GatewayListener;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public partial class frmIgnoreUnits : Form
    {
        private static ILog _log = LogManager.GetLogger(typeof(frmIgnoreUnits));
        private const string sClassName = "DATSListener.frmIgnoreUnits.";
        private GatewayAdministrator _listener;
        private bool _ignoreModeActive;

        public frmIgnoreUnits()
        {
            _listener = null;
            InitializeComponent();
            LoadListView();
        }

        public frmIgnoreUnits(GatewayAdministrator listener)
        {
            _listener = listener;
            InitializeComponent();
            LoadListView();
        }

        private void btnIgnoreReports_Click(object sender, EventArgs e)
        {
            if (_listener != null)
            {
                _listener.IgnoreListClearList();
            }

            if(btnIgnoreReports.Text == "Ignore Reports")
            {
                _ignoreModeActive = true;
                btnIgnoreReports.Text = "Stop Ignoring Reports";

                if (_listener != null)
                {
                    _listener.IgnoreListReplyToPings = !chkDoNotReplyToPings.Checked;
                    _listener.IgnoreListReplyToPingsFromUnknownUnits = !chkDoNotReplyToUnknownUnitPing.Checked;
                    
                    foreach (ListViewItem lvi in lvQueued.Items)
                    {
                        int fleetID = Convert.ToInt32(lvi.SubItems[0].Text);
                        int vehicleID = Convert.ToInt32(lvi.SubItems[1].Text);
                        _listener.IgnoreListAddVehicle(fleetID, vehicleID);
                    }
                }
            }
            else
            {
                _ignoreModeActive = false;
                _listener.IgnoreListReplyToPings = true;
                _listener.IgnoreListReplyToPingsFromUnknownUnits = true;
                btnIgnoreReports.Text = "Ignore Reports";
            }
        }

        private void LoadListView()
        {
            string SQLCmd = "SELECT Distinct FleetID, VehicleID, ListenPort, SerialNumber, IPAddress, IMIE from T_GatewayUnit Where FleetID > 0 and VehicleID > 0 ORDER BY FleetID, VehicleID";
            string prpDSN = "";
            string[] sKnownUnits = null;
            string[] sUnknownUnitID = null;
            string sFleetID = "";
            string sUnitID = "";
            System.Data.DataSet objRS = null;
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.SqlClient.SqlConnection objSQLConn = null;
            System.Data.DataRow drUnkownUnit = null;
            System.Data.DataRow[] dResult = null;

            try
            {
                // Clear the list view
                lvAvailableUnits.Items.Clear();

                sKnownUnits = _listener.mDBInterface.KnownUnits();

                // Create the objects
                prpDSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                objSQLConn = new System.Data.SqlClient.SqlConnection(prpDSN);
                objRS = new System.Data.DataSet();
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                if (objSQLConn != null)
                {
                    // Connect to the SQL server
                    objSQLConn.Open();
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(objRS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    objSQLDA.Dispose();
                    objSQLDA = null;
                    objSQLConn.Close();
                    objSQLConn.Dispose();
                    objSQLConn = null;


                    // Retrieve the data value.
                    if (objRS.Tables.Count > 0)
                    {
                        if (objRS.Tables[0].Rows.Count > 0)
                        {
                            #region Add any units that are reporting to the listener but not in the database.
                            if (sKnownUnits != null)
                            {
                                for (int X = 0; X < sKnownUnits.Length; X++)
                                {
                                    sUnknownUnitID = sKnownUnits[X].Split("~".ToCharArray());
                                    if (sUnknownUnitID.Length == 2)
                                    {
                                        sFleetID = sUnknownUnitID[0];
                                        sUnitID = sUnknownUnitID[1];
                                        dResult = objRS.Tables[0].Select("FleetID = " + sFleetID + " AND VehicleID = " + sUnitID);
                                        if (dResult.Length == 0)
                                        {
                                            drUnkownUnit = objRS.Tables[0].NewRow();
                                            drUnkownUnit["FleetID"] = Convert.ToInt32(sFleetID);
                                            drUnkownUnit["VehicleID"] = Convert.ToInt32(sUnitID);
                                            drUnkownUnit["ListenPort"] = "Not In DB,Not In DB";
                                            objRS.Tables[0].Rows.Add(drUnkownUnit);
                                        }
                                    }
                                }
                            }
                            #endregion

                            dResult = objRS.Tables[0].Select("FleetID > 0 AND VehicleID > 0", "FleetID, VehicleID");

                            for (int X = 0; X < dResult.Length; X++)
                            {
                                string[] sItem = new string[7];

                                sItem[0] = Convert.ToString(dResult[X]["FleetID"]);
                                sItem[1] = Convert.ToString(dResult[X]["VehicleID"]);
                                sItem[4] = (dResult[X]["SerialNumber"] == DBNull.Value) ? "" : Convert.ToString(dResult[X]["SerialNumber"]);
                                sItem[5] = (dResult[X]["IPAddress"] == DBNull.Value) ? "" : Convert.ToString(dResult[X]["IPAddress"]);
                                sItem[6] = (dResult[X]["IMIE"] == DBNull.Value) ? "" : Convert.ToString(dResult[X]["IMIE"]);

                                if (dResult[X]["ListenPort"] == System.DBNull.Value)
                                {
                                    sItem[2] = "";
                                    sItem[3] = "";
                                }
                                else
                                {
                                    string sFirmware = Convert.ToString(dResult[X]["ListenPort"]);
                                    if (sFirmware.IndexOf(",") >= 0)
                                    {
                                        string[] sHWSplit = sFirmware.Split(",".ToCharArray());
                                        if (sHWSplit.Length == 2)
                                        {
                                            sItem[2] = sHWSplit[0];
                                            sItem[3] = sHWSplit[1];
                                        }
                                        else
                                        {
                                            sItem[2] = "";
                                            sItem[3] = "";
                                        }
                                    }
                                    else
                                    {
                                        sItem[2] = "";
                                        sItem[3] = "";
                                    }
                                }
                                ListViewItem oLVI = new ListViewItem(sItem);
                                lvAvailableUnits.Items.Add(oLVI);
                            }
                        }
                    }

                    // Clean up Dataset objects
                    dResult = null;
                    if (objRS != null)
                        objRS.Dispose();
                    objRS = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadListView()", ex);
            }
        }

        private void btnAddSelected_Click(object sender, EventArgs e)
        {
            try
            {
                lvQueued.BeginUpdate();
                int colCount = lvAvailableUnits.Columns.Count;
                foreach (ListViewItem lvi in lvAvailableUnits.SelectedItems)
                {
                    string[] newItem = new string[colCount];
                    for (int X = 0; X < colCount; X++)
                    {
                        newItem[X] = lvi.SubItems[X].Text;
                    }
                    bool alreadyAdded = false;
                    foreach(ListViewItem currentListItem in lvQueued.Items)
                    {
                        if (currentListItem.SubItems[0].Text == newItem[0] && currentListItem.SubItems[1].Text == newItem[1])
                        {
                            alreadyAdded = true;
                            break;
                        }
                    }
                    if (!alreadyAdded)
                    {
                        if(_ignoreModeActive && _listener != null)
                        {
                            int fleetID = Convert.ToInt32(newItem[0]);
                            int vehicleID = Convert.ToInt32(newItem[1]);
                            _listener.IgnoreListAddVehicle(fleetID, vehicleID);
                        }
                        lvQueued.Items.Add(new ListViewItem(newItem));
                    }
                }
                lvQueued.EndUpdate();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAddSelected_Click(object sender, EventArgs e)", ex);
                MessageBox.Show(this, "An error has occured : \r\n\r\n" + sClassName + "btnAddSelected_Click(object sender, EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnAddAll_Click(object sender, EventArgs e)
        {
            try
            {
                lvQueued.BeginUpdate();
                lvQueued.SelectedItems.Clear();
                lvQueued.Items.Clear();
                if (_ignoreModeActive && _listener != null)
                {
                    _listener.IgnoreListClearList();
                }

                int colCount = lvAvailableUnits.Columns.Count;
                foreach (ListViewItem lvi in lvAvailableUnits.Items)
                {
                    string[] newItem = new string[colCount];
                    for (int X = 0; X < colCount; X++)
                    {
                        newItem[X] = lvi.SubItems[X].Text;
                    }
                    lvQueued.Items.Add(new ListViewItem(newItem));
                    if (_ignoreModeActive && _listener != null)
                    {
                        int fleetID = Convert.ToInt32(newItem[0]);
                        int vehicleID = Convert.ToInt32(newItem[1]);
                        _listener.IgnoreListAddVehicle(fleetID, vehicleID);
                    }
                }
                lvQueued.EndUpdate();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAddSelected_Click(object sender, EventArgs e)", ex);
                MessageBox.Show(this, "An error has occured : \r\n\r\n" + sClassName + "btnAddAll_Click(object sender, EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemoveAll_Click(object sender, EventArgs e)
        {
            try
            {
                if (_ignoreModeActive && _listener != null)
                {
                    _listener.IgnoreListClearList();
                }
                lvQueued.BeginUpdate();
                lvQueued.SelectedItems.Clear();
                lvQueued.Items.Clear();
                lvQueued.EndUpdate();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAddSelected_Click(object sender, EventArgs e)", ex);
                MessageBox.Show(this, "An error has occured : \r\n\r\n" + sClassName + "btnRemoveAll_Click(object sender, EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnRemoveSelected_Click(object sender, EventArgs e)
        {
            try
            {
                lvQueued.BeginUpdate();
                foreach (ListViewItem lvi in lvQueued.SelectedItems)
                {
                    if (_ignoreModeActive && _listener != null)
                    {
                        int fleetID = Convert.ToInt32(lvi.SubItems[0].Text);
                        int vehicleID = Convert.ToInt32(lvi.SubItems[1].Text);
                        _listener.IgnoreListRemoveVehicle(fleetID, vehicleID);
                    }
                    lvi.Remove();
                }
                lvQueued.EndUpdate();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAddSelected_Click(object sender, EventArgs e)", ex);
                MessageBox.Show(this, "An error has occured : \r\n\r\n" + sClassName + "btnRemoveSelected_Click(object sender, EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmIgnoreUnits_FormClosing(object sender, FormClosingEventArgs e)
        {
            try
            {
                if (_ignoreModeActive && _listener != null)
                {
                    _listener.IgnoreListClearList();
                    _listener.IgnoreListReplyToPings = true;
                    _listener.IgnoreListReplyToPingsFromUnknownUnits = true;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmIgnoreUnits_FormClosing(object sender, FormClosingEventArgs e)", ex);
            }
        }
    }
}
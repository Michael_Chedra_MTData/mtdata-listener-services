using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using System.IO;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Gateway.Packet;
using log4net;
using MTData.Common.Threading;

namespace MTData.Transport.Listener.DatsListenerApp
{
    /// <summary>
    /// Summary description for frmMonitorUnits.
    /// </summary>
    public class frmMonitorUnits : System.Windows.Forms.Form
    {
        private static ILog _log = LogManager.GetLogger(typeof(frmMonitorUnits));
        private const string sClassName = "DATSListenerApp.frmMonitorUnits.";

        private System.Windows.Forms.Button btnRefershAvaliableList;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ListView lvQueued;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnAddAll;
        private System.Windows.Forms.Button btnAddSelected;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListView lvAvailableUnits;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ListView lvInfo;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtBytes;
        private System.Windows.Forms.Button btnMonitor;
        private System.Windows.Forms.Label label6;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private ArrayList oUpdatePackets = null;
        private Hashtable oMonitorForList = null;
        private EnhancedThread tFindUnits = null;
        private MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator _listener = null;
        private System.Windows.Forms.CheckBox chkLogToFile;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.NumericUpDown numMaxItemsToList;
        private System.Windows.Forms.CheckBox chkAutoScrollList;
        private bool bSettingInfoWidths = false;
        private System.Windows.Forms.Button btnDecodePacket;
        private bool bSettingQueuedWidths = false;
        private ColumnHeader columnHeader14;
        private ColumnHeader columnHeader15;
        private ColumnHeader columnHeader16;
        private ColumnHeader columnHeader17;
        private ArrayList oInterfaceUpdates = null;

        public frmMonitorUnits(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener)
        {
            try
            {
                InitializeComponent();
                oMonitorForList = Hashtable.Synchronized(new Hashtable());
                oUpdatePackets = ArrayList.Synchronized(new ArrayList());
                oInterfaceUpdates = ArrayList.Synchronized(new ArrayList());
                _listener = oListener;
                LoadListView();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmMonitorUnits(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener)", ex);
                throw new System.Exception(sClassName + "frmMonitorUnits(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener)", ex);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnRefershAvaliableList = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lvQueued = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader8 = new System.Windows.Forms.ColumnHeader();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnAddAll = new System.Windows.Forms.Button();
            this.btnAddSelected = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.lvAvailableUnits = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.lvInfo = new System.Windows.Forms.ListView();
            this.columnHeader9 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader10 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader11 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader12 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader13 = new System.Windows.Forms.ColumnHeader();
            this.label3 = new System.Windows.Forms.Label();
            this.numMaxItemsToList = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.btnMonitor = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.txtBytes = new System.Windows.Forms.TextBox();
            this.chkLogToFile = new System.Windows.Forms.CheckBox();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.chkAutoScrollList = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDecodePacket = new System.Windows.Forms.Button();
            this.columnHeader14 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader15 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader16 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader17 = new System.Windows.Forms.ColumnHeader();
            ((System.ComponentModel.ISupportInitialize)(this.numMaxItemsToList)).BeginInit();
            this.SuspendLayout();
            // 
            // btnRefershAvaliableList
            // 
            this.btnRefershAvaliableList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefershAvaliableList.Location = new System.Drawing.Point(8, 184);
            this.btnRefershAvaliableList.Name = "btnRefershAvaliableList";
            this.btnRefershAvaliableList.Size = new System.Drawing.Size(320, 24);
            this.btnRefershAvaliableList.TabIndex = 15;
            this.btnRefershAvaliableList.Text = "Refresh Avaliable List";
            this.btnRefershAvaliableList.Click += new System.EventHandler(this.btnRefershAvaliableList_Click);
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(376, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 16;
            this.label2.Text = "Monitor List";
            // 
            // lvQueued
            // 
            this.lvQueued.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvQueued.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5,
            this.columnHeader14,
            this.columnHeader16,
            this.columnHeader8});
            this.lvQueued.FullRowSelect = true;
            this.lvQueued.GridLines = true;
            this.lvQueued.Location = new System.Drawing.Point(376, 16);
            this.lvQueued.Name = "lvQueued";
            this.lvQueued.Size = new System.Drawing.Size(666, 192);
            this.lvQueued.TabIndex = 14;
            this.lvQueued.UseCompatibleStateImageBehavior = false;
            this.lvQueued.View = System.Windows.Forms.View.Details;
            this.lvQueued.Resize += new System.EventHandler(this.lvQueued_Resize);
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Fleet ID";
            this.columnHeader3.Width = 61;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vehicle ID";
            this.columnHeader4.Width = 68;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Last Reported";
            this.columnHeader5.Width = 145;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "Bytes";
            this.columnHeader8.Width = 81;
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Location = new System.Drawing.Point(336, 144);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveSelected.TabIndex = 13;
            this.btnRemoveSelected.Text = "<";
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(336, 112);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveAll.TabIndex = 12;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnAddAll
            // 
            this.btnAddAll.Location = new System.Drawing.Point(336, 80);
            this.btnAddAll.Name = "btnAddAll";
            this.btnAddAll.Size = new System.Drawing.Size(32, 24);
            this.btnAddAll.TabIndex = 11;
            this.btnAddAll.Text = ">>";
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            // 
            // btnAddSelected
            // 
            this.btnAddSelected.Location = new System.Drawing.Point(336, 48);
            this.btnAddSelected.Name = "btnAddSelected";
            this.btnAddSelected.Size = new System.Drawing.Size(32, 24);
            this.btnAddSelected.TabIndex = 9;
            this.btnAddSelected.Text = ">";
            this.btnAddSelected.Click += new System.EventHandler(this.btnAddSelected_Click);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 10;
            this.label1.Text = "Available Units";
            // 
            // lvAvailableUnits
            // 
            this.lvAvailableUnits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lvAvailableUnits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader6});
            this.lvAvailableUnits.FullRowSelect = true;
            this.lvAvailableUnits.GridLines = true;
            this.lvAvailableUnits.Location = new System.Drawing.Point(8, 24);
            this.lvAvailableUnits.Name = "lvAvailableUnits";
            this.lvAvailableUnits.Size = new System.Drawing.Size(320, 152);
            this.lvAvailableUnits.TabIndex = 8;
            this.lvAvailableUnits.UseCompatibleStateImageBehavior = false;
            this.lvAvailableUnits.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Fleet ID";
            this.columnHeader1.Width = 61;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vehicle ID";
            this.columnHeader2.Width = 68;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Hardware Type";
            this.columnHeader7.Width = 88;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Firmware Ver";
            this.columnHeader6.Width = 80;
            // 
            // lvInfo
            // 
            this.lvInfo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvInfo.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader15,
            this.columnHeader17,
            this.columnHeader13});
            this.lvInfo.FullRowSelect = true;
            this.lvInfo.GridLines = true;
            this.lvInfo.Location = new System.Drawing.Point(8, 248);
            this.lvInfo.Name = "lvInfo";
            this.lvInfo.Size = new System.Drawing.Size(1034, 208);
            this.lvInfo.TabIndex = 17;
            this.lvInfo.UseCompatibleStateImageBehavior = false;
            this.lvInfo.View = System.Windows.Forms.View.Details;
            this.lvInfo.Resize += new System.EventHandler(this.lvInfo_Resize);
            this.lvInfo.SelectedIndexChanged += new System.EventHandler(this.lvInfo_SelectedIndexChanged);
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "Fleet ID";
            this.columnHeader9.Width = 61;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "Vehicle ID";
            this.columnHeader10.Width = 68;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Report Time";
            this.columnHeader11.Width = 88;
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Msg Type";
            this.columnHeader12.Width = 64;
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Bytes";
            this.columnHeader13.Width = 444;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(8, 220);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 18;
            this.label3.Text = "Store last";
            // 
            // numMaxItemsToList
            // 
            this.numMaxItemsToList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numMaxItemsToList.Location = new System.Drawing.Point(64, 216);
            this.numMaxItemsToList.Maximum = new decimal(new int[] {
            1000,
            0,
            0,
            0});
            this.numMaxItemsToList.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numMaxItemsToList.Name = "numMaxItemsToList";
            this.numMaxItemsToList.Size = new System.Drawing.Size(48, 20);
            this.numMaxItemsToList.TabIndex = 19;
            this.numMaxItemsToList.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Location = new System.Drawing.Point(112, 220);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(56, 16);
            this.label4.TabIndex = 20;
            this.label4.Text = "entries.";
            // 
            // btnMonitor
            // 
            this.btnMonitor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMonitor.Location = new System.Drawing.Point(834, 528);
            this.btnMonitor.Name = "btnMonitor";
            this.btnMonitor.Size = new System.Drawing.Size(208, 24);
            this.btnMonitor.TabIndex = 21;
            this.btnMonitor.Text = "Start Monitoring";
            this.btnMonitor.Click += new System.EventHandler(this.btnMonitor_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Location = new System.Drawing.Point(8, 468);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 16);
            this.label5.TabIndex = 22;
            this.label5.Text = "Raw Bytes";
            // 
            // txtBytes
            // 
            this.txtBytes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtBytes.Location = new System.Drawing.Point(72, 464);
            this.txtBytes.Name = "txtBytes";
            this.txtBytes.Size = new System.Drawing.Size(970, 20);
            this.txtBytes.TabIndex = 23;
            // 
            // chkLogToFile
            // 
            this.chkLogToFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkLogToFile.Location = new System.Drawing.Point(8, 500);
            this.chkLogToFile.Name = "chkLogToFile";
            this.chkLogToFile.Size = new System.Drawing.Size(80, 16);
            this.chkLogToFile.TabIndex = 24;
            this.chkLogToFile.Text = "Log To File";
            this.chkLogToFile.CheckedChanged += new System.EventHandler(this.chkLogToFile_CheckedChanged);
            // 
            // txtFilename
            // 
            this.txtFilename.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtFilename.Location = new System.Drawing.Point(168, 496);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(152, 20);
            this.txtFilename.TabIndex = 25;
            this.txtFilename.Text = "Byte Data Log";
            // 
            // chkAutoScrollList
            // 
            this.chkAutoScrollList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkAutoScrollList.Checked = true;
            this.chkAutoScrollList.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkAutoScrollList.Location = new System.Drawing.Point(176, 220);
            this.chkAutoScrollList.Name = "chkAutoScrollList";
            this.chkAutoScrollList.Size = new System.Drawing.Size(104, 16);
            this.chkAutoScrollList.TabIndex = 26;
            this.chkAutoScrollList.Text = "Auto Scroll List";
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Location = new System.Drawing.Point(104, 500);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(64, 16);
            this.label6.TabIndex = 27;
            this.label6.Text = "Filenames";
            // 
            // btnDecodePacket
            // 
            this.btnDecodePacket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDecodePacket.Location = new System.Drawing.Point(610, 528);
            this.btnDecodePacket.Name = "btnDecodePacket";
            this.btnDecodePacket.Size = new System.Drawing.Size(208, 24);
            this.btnDecodePacket.TabIndex = 28;
            this.btnDecodePacket.Text = "Decode Selected Packet";
            this.btnDecodePacket.Click += new System.EventHandler(this.btnDecodePacket_Click);
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "IP Address";
            this.columnHeader14.Width = 76;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "IP Address";
            this.columnHeader15.Width = 90;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "Direction";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "Direction";
            // 
            // frmMonitorUnits
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(1050, 554);
            this.Controls.Add(this.btnDecodePacket);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.chkAutoScrollList);
            this.Controls.Add(this.txtFilename);
            this.Controls.Add(this.txtBytes);
            this.Controls.Add(this.chkLogToFile);
            this.Controls.Add(this.btnMonitor);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.numMaxItemsToList);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.lvInfo);
            this.Controls.Add(this.btnRefershAvaliableList);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lvQueued);
            this.Controls.Add(this.btnRemoveSelected);
            this.Controls.Add(this.btnRemoveAll);
            this.Controls.Add(this.btnAddAll);
            this.Controls.Add(this.btnAddSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvAvailableUnits);
            this.Controls.Add(this.label5);
            this.MinimumSize = new System.Drawing.Size(656, 544);
            this.Name = "frmMonitorUnits";
            this.Text = "Monitor Unit Reports";
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.frmMonitorUnits_Paint);
            this.Closing += new System.ComponentModel.CancelEventHandler(this.frmMonitorUnits_Closing);
            ((System.ComponentModel.ISupportInitialize)(this.numMaxItemsToList)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion

        #region Sping the last colum on the list views when they resize
        private void lvInfo_Resize(object sender, System.EventArgs e)
        {
            int iColWidths = 0;
            try
            {
                if (bSettingInfoWidths) return;
                bSettingInfoWidths = true;
                for (int X = 0; X < lvInfo.Columns.Count - 1; X++)
                {
                    iColWidths += lvInfo.Columns[X].Width;
                }
                iColWidths += 15;
                lvInfo.Columns[lvInfo.Columns.Count - 1].Width = lvInfo.Width - iColWidths;
                bSettingInfoWidths = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "lvInfo_Resize(object sender, System.EventArgs e)", ex);
                bSettingInfoWidths = false;
            }
        }

        private void lvQueued_Resize(object sender, System.EventArgs e)
        {
            int iColWidths = 0;
            try
            {
                if (bSettingQueuedWidths) return;
                bSettingQueuedWidths = true;
                for (int X = 0; X < lvQueued.Columns.Count - 1; X++)
                {
                    iColWidths += lvQueued.Columns[X].Width;
                }
                iColWidths += 15;
                lvQueued.Columns[lvQueued.Columns.Count - 1].Width = lvQueued.Width - iColWidths;
                bSettingQueuedWidths = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "lvQueued_Resize(object sender, System.EventArgs e)", ex);
                bSettingQueuedWidths = false;
            }
        }
        #endregion

        #region List Load and maniplation functions
        private void LoadListView()
        {
            string SQLCmd = "SELECT Distinct FleetID, VehicleID, ListenPort from T_GatewayUnit Where FleetID > 0 and VehicleID > 0 ORDER BY FleetID, VehicleID";
            string prpDSN = "";
            string[] sKnownUnits = null;
            string[] sUnknownUnitID = null;
            string sFleetID = "";
            string sUnitID = "";
            System.Data.DataSet objRS = null;
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.SqlClient.SqlConnection objSQLConn = null;
            System.Data.DataRow drUnkownUnit = null;
            System.Data.DataRow[] dResult = null;

            try
            {
                // Clear the list view
                lvAvailableUnits.Items.Clear();

                sKnownUnits = _listener.mDBInterface.KnownUnits();

                // Create the objects
                prpDSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                objSQLConn = new System.Data.SqlClient.SqlConnection(prpDSN);
                objRS = new System.Data.DataSet();
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                if (objSQLConn != null)
                {
                    // Connect to the SQL server
                    objSQLConn.Open();
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(objRS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    objSQLDA.Dispose();
                    objSQLDA = null;
                    objSQLConn.Close();
                    objSQLConn.Dispose();
                    objSQLConn = null;


                    // Retrieve the data value.
                    if (objRS.Tables.Count > 0)
                    {
                        if (objRS.Tables[0].Rows.Count > 0)
                        {
                            #region Add any units that are reporting to the listener but not in the database.
                            if (sKnownUnits != null)
                            {
                                for (int X = 0; X < sKnownUnits.Length; X++)
                                {
                                    sUnknownUnitID = sKnownUnits[X].Split("~".ToCharArray());
                                    if (sUnknownUnitID.Length == 2)
                                    {
                                        sFleetID = sUnknownUnitID[0];
                                        sUnitID = sUnknownUnitID[1];
                                        dResult = objRS.Tables[0].Select("FleetID = " + sFleetID + " AND VehicleID = " + sUnitID);
                                        if (dResult.Length == 0)
                                        {
                                            drUnkownUnit = objRS.Tables[0].NewRow();
                                            drUnkownUnit["FleetID"] = Convert.ToInt32(sFleetID);
                                            drUnkownUnit["VehicleID"] = Convert.ToInt32(sUnitID);
                                            drUnkownUnit["ListenPort"] = "Not In DB,Not In DB";
                                            objRS.Tables[0].Rows.Add(drUnkownUnit);
                                        }
                                    }
                                }
                            }
                            #endregion

                            dResult = objRS.Tables[0].Select("FleetID > 0 AND VehicleID > 0", "FleetID, VehicleID");

                            for (int X = 0; X < dResult.Length; X++)
                            {
                                string[] sItem = new string[4];
                                sItem[0] = Convert.ToString(dResult[X]["FleetID"]);
                                sItem[1] = Convert.ToString(dResult[X]["VehicleID"]);
                                if (dResult[X]["ListenPort"] == System.DBNull.Value)
                                {
                                    sItem[2] = "";
                                    sItem[3] = "";
                                }
                                else
                                {
                                    string sFirmware = Convert.ToString(dResult[X]["ListenPort"]);
                                    if (sFirmware.IndexOf(",") >= 0)
                                    {
                                        string[] sHWSplit = sFirmware.Split(",".ToCharArray());
                                        if (sHWSplit.Length == 2)
                                        {
                                            sItem[2] = sHWSplit[0];
                                            sItem[3] = sHWSplit[1];
                                        }
                                        else
                                        {
                                            sItem[2] = "";
                                            sItem[3] = "";
                                        }
                                    }
                                    else
                                    {
                                        sItem[2] = "";
                                        sItem[3] = "";
                                    }
                                }
                                ListViewItem oLVI = new ListViewItem(sItem);
                                lvAvailableUnits.Items.Add(oLVI);
                            }
                        }
                    }

                    // Clean up Dataset objects
                    dResult = null;
                    if (objRS != null)
                        objRS.Dispose();
                    objRS = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadListView()", ex);
            }
        }

        private void btnRefershAvaliableList_Click(object sender, System.EventArgs e)
        {
            try
            {
                LoadListView();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnRefershAvaliableList_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRefershAvaliableList_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }
        #endregion

        #region Add Remove Items from the queued list
        private void btnAddAll_Click(object sender, System.EventArgs e)
        {
            bool bAddItem = true;
            ListViewItem oLVI = null;
            try
            {
                lvQueued.Items.Clear();
                for (int X = 0; X < lvAvailableUnits.Items.Count; X++)
                {
                    oLVI = lvAvailableUnits.Items[X];
                    string sFleetID = oLVI.SubItems[0].Text;
                    string sVehicleID = oLVI.SubItems[1].Text;
                    string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                    bAddItem = true;
                    // Check if this vehicle is already in the list.
                    for (int Y = 0; Y < lvQueued.Items.Count; Y++)
                    {
                        ListViewItem oLVIQueued = lvQueued.Items[Y];
                        string sQueuedFleetID = oLVIQueued.SubItems[0].Text;
                        string sQueuedVehicleID = oLVIQueued.SubItems[1].Text;
                        if (sQueuedFleetID == sFleetID && sQueuedVehicleID == sVehicleID)
                        {
                            bAddItem = false;
                            break;
                        }
                    }

                    if (bAddItem)
                    {
                        lock (oMonitorForList.SyncRoot)
                        {
                            if (oMonitorForList.ContainsKey(sKey))
                            {
                                oMonitorForList.Remove(sKey);
                            }
                            string[] sQueuedRecord = new string[6];
                            sQueuedRecord[0] = sFleetID;
                            sQueuedRecord[1] = sVehicleID;
                            sQueuedRecord[2] = "";
                            sQueuedRecord[3] = "";
                            sQueuedRecord[4] = "";
                            sQueuedRecord[5] = "";
                            lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                            oMonitorForList.Add(sKey, " ");
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAddAll_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAddAll_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void btnRemoveAll_Click(object sender, System.EventArgs e)
        {
            try
            {
                lock (oMonitorForList.SyncRoot)
                {
                    oMonitorForList.Clear();
                }
                lvQueued.Items.Clear();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnRemoveAll_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRemoveAll_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void btnAddSelected_Click(object sender, System.EventArgs e)
        {
            try
            {
                for (int X = 0; X < lvAvailableUnits.Items.Count; X++)
                {
                    if (lvAvailableUnits.Items[X].Selected)
                    {
                        ListViewItem oLVI = lvAvailableUnits.Items[X];
                        string sFleetID = oLVI.SubItems[0].Text;
                        string sVehicleID = oLVI.SubItems[1].Text;
                        string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                        // Check if this vehicle is already in the list.
                        for (int Y = 0; Y < lvQueued.Items.Count; Y++)
                        {
                            ListViewItem oLVIQueued = lvQueued.Items[Y];
                            string sQueuedFleetID = oLVIQueued.SubItems[0].Text;
                            string sQueuedVehicleID = oLVIQueued.SubItems[1].Text;
                            if (sQueuedFleetID == sFleetID && sQueuedVehicleID == sVehicleID)
                            {
                                MessageBox.Show(this, "There is already in the list to be monitored.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }

                        lock (oMonitorForList.SyncRoot)
                        {
                            if (oMonitorForList.ContainsKey(sKey))
                            {
                                oMonitorForList.Remove(sKey);
                            }
                            string[] sQueuedRecord = new string[6];
                            sQueuedRecord[0] = sFleetID;
                            sQueuedRecord[1] = sVehicleID;
                            sQueuedRecord[2] = "";
                            sQueuedRecord[3] = "";
                            sQueuedRecord[4] = "";
                            sQueuedRecord[5] = ".";
                            lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                            oMonitorForList.Add(sKey, " ");
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAddSelected_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAddSelected_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void btnRemoveSelected_Click(object sender, System.EventArgs e)
        {
            try
            {
                for (int X = lvQueued.Items.Count - 1; X >= 0; X--)
                {
                    if (lvQueued.Items[X].Selected)
                    {
                        ListViewItem oLVI = lvQueued.Items[X];
                        string sFleetID = oLVI.SubItems[0].Text;
                        string sVehicleID = oLVI.SubItems[1].Text;
                        string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                        lock (oMonitorForList.SyncRoot)
                        {
                            if (oMonitorForList.ContainsKey(sKey))
                            {
                                oMonitorForList.Remove(sKey);
                            }
                        }
                        lvQueued.Items.RemoveAt(X);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnRemoveSelected_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRemoveSelected_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }
        #endregion

        #region Processing Thread
        private void btnMonitor_Click(object sender, System.EventArgs e)
        {
            if (btnMonitor.Text == "Start Monitoring")
            {
                try
                {
                    if (tFindUnits != null)
                    {
                        tFindUnits.Stop();
                        int iCounter = 0;
                        while (tFindUnits.Running && iCounter < 1000)
                        {
                            Thread.Sleep(100);
                        }
                        tFindUnits = null;
                    }
                    tFindUnits = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(MatchUnitInQueuedList), null);
                    tFindUnits.Start();
                    btnMonitor.Text = "Stop Monitoring";
                    _log.Info("Monitoring Reports Process Started.");
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMonitor_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnMonitor_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
            else
            {
                try
                {
                    if (tFindUnits != null)
                    {
                        tFindUnits.Stop();
                        int iCounter = 0;
                        while (tFindUnits.Running && iCounter < 1000)
                        {
                            Thread.Sleep(100);
                        }
                        tFindUnits = null;
                    }
           
                    _log.Info("Monitoring Reports Process Stopped.");
                    btnMonitor.Text = "Start Monitoring";
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMonitor_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnMonitor_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        public void AddOutboundPacketToCheckList(GatewayProtocolPacket packet)
        {
            object[] oItem = null;
            try
            {
                if (tFindUnits != null)
                {
                    if (tFindUnits.Running)
                    {
                        lock (oUpdatePackets.SyncRoot)
                        {
                            oItem = new object[2];
                            oItem[0] = "Out";
                            oItem[1] = packet;
                            oUpdatePackets.Add(oItem);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "AddOutboundPacketToCheckList(GatewayProtocolPacket packet)", ex);
            }
        }

        public void AddPacketToCheckList(GatewayProtocolPacket packet)
        {
            object[] oItem = null;
            try
            {
                if (tFindUnits != null)
                {
                    if (tFindUnits.Running)
                    {
                        lock (oUpdatePackets.SyncRoot)
                        {
                            oItem = new object[2];
                            oItem[0] = "In";
                            oItem[1] = packet;
                            oUpdatePackets.Add(oItem);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
            }
        }
        private object MatchUnitInQueuedList(EnhancedThread sender, object data)
        {
            int iCheckCount = 0;
            ArrayList oLocalList = null;
            string sDir = "";
            string sFleetID = "";
            string sVehicleID = "";
            string sKey = "";
            string sIPAddress = "";
            object[] oItem = null;

            while (!sender.Stopping)
            {
                try
                {
                    lock (oMonitorForList.SyncRoot)
                    {
                        iCheckCount = oMonitorForList.Count;
                    }
                    if (iCheckCount > 0)
                    {
                        oLocalList = new ArrayList();
                        #region Copy the items waiting to be processed into a local array.
                        lock (oUpdatePackets.SyncRoot)
                        {
                            for (int X = oUpdatePackets.Count - 1; X >= 0; X--)
                            {
                                oLocalList.Add(oUpdatePackets[X]);
                                oUpdatePackets.RemoveAt(X);
                            }
                        }
                        #endregion

                        if (oLocalList.Count > 0)
                        {
                            for (int X = 0; X < oLocalList.Count; X++)
                            {
                                oItem = (object[])oLocalList[X];
                                sDir = (string)oItem[0];
                                GatewayProtocolPacket packet = (GatewayProtocolPacket)oItem[1];

                                // Only start a new download as a response to a status packet.
                                byte[] bConvert = new byte[4];
                                bConvert[0] = packet.cFleetId;
                                sFleetID = Convert.ToString(BitConverter.ToInt32(bConvert, 0));
                                sVehicleID = Convert.ToString(Convert.ToInt32(packet.iVehicleId));
                                sKey = sFleetID.PadLeft(4, '0') + "~" + sVehicleID.PadLeft(4, '0');
                                if (packet.mSenderIP != null)
                                    sIPAddress = packet.mSenderIP.Address.ToString() + ":" + Convert.ToString(packet.mSenderIP.Port);
                                else
                                    sIPAddress = "0.0.0.0:0000";
                                lock (oMonitorForList.SyncRoot)
                                {
                                    if (oMonitorForList.ContainsKey(sKey))
                                    {
                                        UpdateStatus(sDir, sFleetID, sVehicleID, GatewayProtocolLookup.GetMessageType(packet.cMsgType), sIPAddress, packet.mRawBytes);
                                    }
                                }
                            }
                            oLocalList.Clear();
                        }
                        else
                            Thread.Sleep(500);
                    }
                    else
                        Thread.Sleep(1000);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "MatchUnitInQueuedList(EnhancedThread sender, object data)", ex);
                    sender.Stop();
                    btnMonitor.Text = "Process Queue";
                }
            }
            return null;
        }


        private void UpdateStatus(string sDir, string sFleetID, string sVehicleID, string sMsgType, string sIPAddress, byte[] oByteData)
        {
            MonitorData oData = null;
            string sReportTime = "";

            try
            {
                sReportTime = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss");
                #region Log Data to File
                if (chkLogToFile.Checked)
                {
                    _log.Info("Fleet : " + sFleetID + ", Vehicle : " + sVehicleID + ", Report Time : " + sReportTime + ", Direction : " + sDir + ", Msg Type : " + sMsgType + ", IP Address : " + sIPAddress + ", Byte Data : " + System.BitConverter.ToString(oByteData, 0));
                }
                #endregion

                #region Create an interface update object
                oData = new MonitorData(sDir, sReportTime, sFleetID, sVehicleID, sMsgType, sIPAddress, oByteData);
                lock (oInterfaceUpdates.SyncRoot)
                    oInterfaceUpdates.Add(oData);
                this.Invalidate();
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "UpdateStatus(string sDir, string sFleetID, string sVehicleID, string sMsgType, string sIPAddress, byte[] oByteData)", ex);
            }
        }
        #endregion

        private void chkLogToFile_CheckedChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (chkLogToFile.Checked)
                    txtFilename.Enabled = true;
                else
                    txtFilename.Enabled = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "chkLogToFile_CheckedChanged(object sender, System.EventArgs e)", ex);
            }
        }

        private void frmMonitorUnits_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            try
            {
                if (tFindUnits != null)
                {
                    tFindUnits.Stop();
                    int iCounter = 0;
                    while (tFindUnits.Running && iCounter < 1000)
                    {
                        Thread.Sleep(100);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmMonitorUnits_Closing(object sender, System.ComponentModel.CancelEventArgs e)", ex);
            }
            try
            {
                tFindUnits = null;
            }
            catch (System.Exception)
            {
            }

        }

        private void lvInfo_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            try
            {
                if (lvInfo.SelectedItems.Count > 0)
                {
                    txtBytes.Text = lvInfo.SelectedItems[0].SubItems[6].Text;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "lvInfo_SelectedIndexChanged(object sender, System.EventArgs e)", ex);
            }
        }

        private void btnDecodePacket_Click(object sender, System.EventArgs e)
        {
            DetailsForm oFormDetails = null;
            string sBytes = "";
            string[] sByteArray = null;
            byte[] bData = null;
            byte[] bConvert = new byte[4];
            int iState = 0;
            string sTemp = "";
            string sTempNum = "";
            MemoryStream oMS = null;
            string sResult = "";
            string[] sResults = new string[1];
            UDPPacket oUDPPacket = null;
            string sDateFormat = "dd/MM/yyyy hh:mm:ss";
            GatewayProtocolPacket oPacket = null;
            GeneralGPPacket gpPacket = null;
            RoutePtSetPtGPPacket pointPacket = null;
            RoutingModuleGPPacket routePacket = null;
            ConfigTotalGPPacket confPacket = null;

            try
            {
                if (lvInfo.SelectedItems.Count == 1)
                {
                    try
                    {
                        sDateFormat = System.Configuration.ConfigurationManager.AppSettings["ServerTime_DateFormat"];
                    }
                    catch (System.Exception)
                    {
                        sDateFormat = "dd/MM/yyyy hh:mm:ss";
                    }
                    sBytes = lvInfo.SelectedItems[0].SubItems[6].Text;
                    #region Convert the string back to a byte array
                    if (sBytes.Substring(0, 3) == "[1]")
                    {
                        sTemp = sBytes;
                        oMS = new MemoryStream();
                        for (int X = 0; X < sTemp.Length; X++)
                        {
                            switch (iState)
                            {
                                #region State Machine
                                case 0: //Looking for '[' or a char value
                                    if (sTemp.Substring(X, 1) == "[")
                                        iState = 1;
                                    else
                                    {
                                        oMS.WriteByte((byte)Convert.ToChar(sTemp.Substring(X, 1)));
                                        X++;
                                        iState = 3;
                                    }
                                    break;
                                case 1: // Looking for a number value or a space
                                    sTempNum = "";
                                    if (sTemp.Substring(X, 1) == " ")
                                    {
                                        oMS.WriteByte((byte)Convert.ToChar("["));
                                        iState = 3;
                                    }
                                    else
                                    {
                                        sTempNum += sTemp.Substring(X, 1);
                                        iState = 2;
                                    }
                                    break;
                                case 2: // Convert the char to byte value
                                    if (sTemp.Substring(X, 1) == "]")
                                    {
                                        oMS.WriteByte((byte)Convert.ToInt32(sTempNum));
                                        X++;
                                        iState = 3;
                                    }
                                    else
                                    {
                                        sTempNum += sTemp.Substring(X, 1);
                                    }
                                    break;
                                case 3: // Looking for the - seperating char
                                    if (sTemp.Substring(X, 1) == "-")
                                    {
                                        iState = 0;
                                        X++;
                                    }
                                    break;
                                #endregion
                            }
                        }
                        bData = oMS.ToArray();
                    }
                    else
                    {
                        sByteArray = sBytes.Split("-".ToCharArray());
                        bData = new byte[sByteArray.Length];
                        for (int X = 0; X < sByteArray.Length; X++)
                        {
                            bConvert = new byte[4];
                            bConvert = BitConverter.GetBytes(Convert.ToInt32(sByteArray[X], 16));
                            bData[X] = bConvert[0];
                        }
                    }
                    #endregion
                    oUDPPacket = new UDPPacket(sDateFormat);
                    string errMsg = oUDPPacket.Decode(bData);
                    if (errMsg != null)
                    {
                        sResult = errMsg;
                    }
                    else
                    {
                        oPacket = oUDPPacket.mPackets[0];
                        #region switch on packet type
                        // We have to do different things depending on what data we just got given
                        switch (oPacket.cMsgType & 0xF0)
                        {
                            case GeneralGPPacket.GEN_NO_GPS_PACKET_MASK:
                            case GeneralGPPacket.GEN_PACKET_MASK:
                            case GeneralGPPacket.STATUS_PACKET_MASK:
                            case GeneralGPPacket.ENGINE_PACKET_MASK:
                            case GeneralGPPacket.BARREL_PACKET_MASK:
                            case GeneralGPPacket.EXTENDED_IO_PACKET_MASK:
                            case GeneralGPPacket.ZONE_ALERT_MESSAGES:
                                gpPacket = new GeneralGPPacket(oPacket, sDateFormat);
                                sResult = gpPacket.ToDisplayString();
                                break;
                            case RoutePtSetPtGPPacket.SETPT_PACKET_MASK:
                                pointPacket = new RoutePtSetPtGPPacket(oPacket, sDateFormat);
                                sResult = pointPacket.ToDisplayString();
                                break;
                            case RoutingModuleGPPacket.ROUTES_PACKET_MASK:
                                routePacket = new RoutingModuleGPPacket(oPacket, sDateFormat);
                                sResult = routePacket.ToDisplayString();
                                break;
                            case ConfigTotalGPPacket.CONFIG_PACKET_MASK:
                                switch (oPacket.cMsgType)
                                {
                                    case ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE:
                                        confPacket = new ConfigTotalGPPacket(oPacket, sDateFormat);
                                        sResult = confPacket.ToDisplayString();
                                        break;
                                }
                                break;
                            case GPSHistoryGPPacket.GPS_HISTORY_PACKET_MASK:
                                gpPacket = new GeneralGPPacket(oPacket, sDateFormat);
                                sResult = gpPacket.ToDisplayString();
                                break;
                            case MassDeclarationPacket.MASS_PACKET_MASK:
                                if (oPacket.cMsgType == GenericPacket.GENERIC_PACKET)
                                {
                                    GenericPacket g = new GenericPacket(oPacket, sDateFormat);
                                    sResult = g.ToDisplayString();
                                }
                                else
                                {
                                    sResult = oPacket.ToDisplayString();
                                }
                                break;
                            default:
                                sResult = "Unknown Packet Type : " + Convert.ToString(oPacket.cMsgType);
                                break;
                        }
                        #endregion
                    }

                    oFormDetails = new DetailsForm();
                    oFormDetails.Show();
                    sResults[0] = sResult;
                    oFormDetails.SetContents(sResults);
                }
                else
                {
                    MessageBox.Show(this, "Please select one item from the list first.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (System.Exception ex)
            {
                MessageBox.Show(this, "Error Decoding Packet : " + ex.Message, "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void frmMonitorUnits_Paint(object sender, PaintEventArgs e)
        {
            MonitorData oData = null;
            try
            {
                lock (oInterfaceUpdates.SyncRoot)
                {
                    for (int X = 0; X < oInterfaceUpdates.Count; X++)
                    {
                        try
                        {
                            oData = (MonitorData)oInterfaceUpdates[X];
                            oData.PopulateInterface(lvQueued, lvInfo, Convert.ToInt32(numMaxItemsToList.Value), chkAutoScrollList.Checked);
                        }
                        catch (System.Exception exLoop)
                        {
                            _log.Error(sClassName + "frmMonitorUnits_Paint(object sender, PaintEventArgs e)", exLoop);
                        }
                    }
                    oInterfaceUpdates.Clear();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmMonitorUnits_Paint(object sender, PaintEventArgs e)", ex);
            }
        }

    }
}

﻿namespace MTData.Transport.Listener.DatsListenerApp
{
    partial class ListenerForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.splitMain = new System.Windows.Forms.SplitContainer();
            this.splitSub = new System.Windows.Forms.SplitContainer();
            this.gbSettings = new System.Windows.Forms.GroupBox();
            this.txtRemoteStateUpdatePort = new System.Windows.Forms.TextBox();
            this.lblHRemoteState = new System.Windows.Forms.Label();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.lblHDSN = new System.Windows.Forms.Label();
            this.txtLiveUpdatesOn = new System.Windows.Forms.TextBox();
            this.lblHLiveUpdate = new System.Windows.Forms.Label();
            this.txtBindPort = new System.Windows.Forms.TextBox();
            this.txtBindIP = new System.Windows.Forms.TextBox();
            this.lblHBindTo = new System.Windows.Forms.Label();
            this.gbActivity = new System.Windows.Forms.GroupBox();
            this.lvActivity = new System.Windows.Forms.ListView();
            this.mnuMain = new System.Windows.Forms.MenuStrip();
            this.fileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.connectToAnotherServerToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.bulkMessageToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.downloadToUnitsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exportLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cmItemDetails = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.showDetailsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.filterMultipleVehiclesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addThisVehicleToFilterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.clearFiltersToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveFileExportDialog = new System.Windows.Forms.SaveFileDialog();
            this.windowToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.AutoloadLogToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).BeginInit();
            this.splitMain.Panel1.SuspendLayout();
            this.splitMain.Panel2.SuspendLayout();
            this.splitMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitSub)).BeginInit();
            this.splitSub.Panel1.SuspendLayout();
            this.splitSub.SuspendLayout();
            this.gbSettings.SuspendLayout();
            this.gbActivity.SuspendLayout();
            this.mnuMain.SuspendLayout();
            this.cmItemDetails.SuspendLayout();
            this.SuspendLayout();
            // 
            // splitMain
            // 
            this.splitMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitMain.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            this.splitMain.Location = new System.Drawing.Point(0, 27);
            this.splitMain.Name = "splitMain";
            this.splitMain.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitMain.Panel1
            // 
            this.splitMain.Panel1.Controls.Add(this.splitSub);
            // 
            // splitMain.Panel2
            // 
            this.splitMain.Panel2.Controls.Add(this.gbActivity);
            this.splitMain.Size = new System.Drawing.Size(1534, 736);
            this.splitMain.SplitterDistance = 520;
            this.splitMain.TabIndex = 0;
            // 
            // splitSub
            // 
            this.splitSub.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitSub.Location = new System.Drawing.Point(0, 0);
            this.splitSub.Name = "splitSub";
            this.splitSub.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitSub.Panel1
            // 
            this.splitSub.Panel1.Controls.Add(this.gbSettings);
            this.splitSub.Size = new System.Drawing.Size(1530, 516);
            this.splitSub.SplitterDistance = 85;
            this.splitSub.TabIndex = 31;
            // 
            // gbSettings
            // 
            this.gbSettings.Controls.Add(this.txtRemoteStateUpdatePort);
            this.gbSettings.Controls.Add(this.lblHRemoteState);
            this.gbSettings.Controls.Add(this.txtDSN);
            this.gbSettings.Controls.Add(this.lblHDSN);
            this.gbSettings.Controls.Add(this.txtLiveUpdatesOn);
            this.gbSettings.Controls.Add(this.lblHLiveUpdate);
            this.gbSettings.Controls.Add(this.txtBindPort);
            this.gbSettings.Controls.Add(this.txtBindIP);
            this.gbSettings.Controls.Add(this.lblHBindTo);
            this.gbSettings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbSettings.Enabled = false;
            this.gbSettings.Location = new System.Drawing.Point(0, 0);
            this.gbSettings.Name = "gbSettings";
            this.gbSettings.Size = new System.Drawing.Size(1530, 85);
            this.gbSettings.TabIndex = 31;
            this.gbSettings.TabStop = false;
            this.gbSettings.Text = "Settings";
            // 
            // txtRemoteStateUpdatePort
            // 
            this.txtRemoteStateUpdatePort.Enabled = false;
            this.txtRemoteStateUpdatePort.Location = new System.Drawing.Point(504, 15);
            this.txtRemoteStateUpdatePort.Name = "txtRemoteStateUpdatePort";
            this.txtRemoteStateUpdatePort.Size = new System.Drawing.Size(48, 20);
            this.txtRemoteStateUpdatePort.TabIndex = 21;
            this.txtRemoteStateUpdatePort.TabStop = false;
            // 
            // lblHRemoteState
            // 
            this.lblHRemoteState.AutoSize = true;
            this.lblHRemoteState.Location = new System.Drawing.Point(368, 18);
            this.lblHRemoteState.Name = "lblHRemoteState";
            this.lblHRemoteState.Size = new System.Drawing.Size(130, 13);
            this.lblHRemoteState.TabIndex = 20;
            this.lblHRemoteState.Text = "Remote State Updates on";
            // 
            // txtDSN
            // 
            this.txtDSN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDSN.Enabled = false;
            this.txtDSN.Location = new System.Drawing.Point(64, 41);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.Size = new System.Drawing.Size(1458, 20);
            this.txtDSN.TabIndex = 19;
            this.txtDSN.TabStop = false;
            // 
            // lblHDSN
            // 
            this.lblHDSN.AutoSize = true;
            this.lblHDSN.Location = new System.Drawing.Point(14, 44);
            this.lblHDSN.Name = "lblHDSN";
            this.lblHDSN.Size = new System.Drawing.Size(30, 13);
            this.lblHDSN.TabIndex = 18;
            this.lblHDSN.Text = "DSN";
            // 
            // txtLiveUpdatesOn
            // 
            this.txtLiveUpdatesOn.Enabled = false;
            this.txtLiveUpdatesOn.Location = new System.Drawing.Point(311, 15);
            this.txtLiveUpdatesOn.Name = "txtLiveUpdatesOn";
            this.txtLiveUpdatesOn.Size = new System.Drawing.Size(48, 20);
            this.txtLiveUpdatesOn.TabIndex = 17;
            this.txtLiveUpdatesOn.TabStop = false;
            // 
            // lblHLiveUpdate
            // 
            this.lblHLiveUpdate.AutoSize = true;
            this.lblHLiveUpdate.Location = new System.Drawing.Point(220, 18);
            this.lblHLiveUpdate.Name = "lblHLiveUpdate";
            this.lblHLiveUpdate.Size = new System.Drawing.Size(85, 13);
            this.lblHLiveUpdate.TabIndex = 16;
            this.lblHLiveUpdate.Text = "Live Updates on";
            // 
            // txtBindPort
            // 
            this.txtBindPort.Enabled = false;
            this.txtBindPort.Location = new System.Drawing.Point(164, 15);
            this.txtBindPort.Name = "txtBindPort";
            this.txtBindPort.Size = new System.Drawing.Size(48, 20);
            this.txtBindPort.TabIndex = 15;
            this.txtBindPort.TabStop = false;
            // 
            // txtBindIP
            // 
            this.txtBindIP.Enabled = false;
            this.txtBindIP.Location = new System.Drawing.Point(64, 15);
            this.txtBindIP.Name = "txtBindIP";
            this.txtBindIP.Size = new System.Drawing.Size(94, 20);
            this.txtBindIP.TabIndex = 14;
            this.txtBindIP.TabStop = false;
            // 
            // lblHBindTo
            // 
            this.lblHBindTo.AutoSize = true;
            this.lblHBindTo.Location = new System.Drawing.Point(14, 18);
            this.lblHBindTo.Name = "lblHBindTo";
            this.lblHBindTo.Size = new System.Drawing.Size(44, 13);
            this.lblHBindTo.TabIndex = 0;
            this.lblHBindTo.Text = "Bind To";
            // 
            // gbActivity
            // 
            this.gbActivity.Controls.Add(this.lvActivity);
            this.gbActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gbActivity.Location = new System.Drawing.Point(0, 0);
            this.gbActivity.Name = "gbActivity";
            this.gbActivity.Size = new System.Drawing.Size(1530, 208);
            this.gbActivity.TabIndex = 0;
            this.gbActivity.TabStop = false;
            this.gbActivity.Text = "Activity Snapshot";
            // 
            // lvActivity
            // 
            this.lvActivity.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lvActivity.FullRowSelect = true;
            this.lvActivity.HideSelection = false;
            this.lvActivity.Location = new System.Drawing.Point(3, 16);
            this.lvActivity.MultiSelect = false;
            this.lvActivity.Name = "lvActivity";
            this.lvActivity.Size = new System.Drawing.Size(1524, 189);
            this.lvActivity.TabIndex = 0;
            this.lvActivity.UseCompatibleStateImageBehavior = false;
            this.lvActivity.View = System.Windows.Forms.View.Details;
            this.lvActivity.MouseClick += new System.Windows.Forms.MouseEventHandler(this.lvActivity_MouseClick);
            this.lvActivity.MouseDown += new System.Windows.Forms.MouseEventHandler(this.lvActivity_MouseDown);
            // 
            // mnuMain
            // 
            this.mnuMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileToolStripMenuItem,
            this.bulkMessageToolStripMenuItem,
            this.downloadToUnitsToolStripMenuItem,
            this.exportLogToolStripMenuItem,
            this.windowToolStripMenuItem});
            this.mnuMain.Location = new System.Drawing.Point(0, 0);
            this.mnuMain.Name = "mnuMain";
            this.mnuMain.Size = new System.Drawing.Size(1534, 24);
            this.mnuMain.TabIndex = 32;
            this.mnuMain.Text = "menuStrip1";
            // 
            // fileToolStripMenuItem
            // 
            this.fileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.connectToAnotherServerToolStripMenuItem,
            this.exitToolStripMenuItem});
            this.fileToolStripMenuItem.Name = "fileToolStripMenuItem";
            this.fileToolStripMenuItem.Size = new System.Drawing.Size(37, 20);
            this.fileToolStripMenuItem.Text = "&File";
            // 
            // connectToAnotherServerToolStripMenuItem
            // 
            this.connectToAnotherServerToolStripMenuItem.Name = "connectToAnotherServerToolStripMenuItem";
            this.connectToAnotherServerToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.connectToAnotherServerToolStripMenuItem.Text = "Connect To Another Server";
            this.connectToAnotherServerToolStripMenuItem.Click += new System.EventHandler(this.connectToAnotherServerToolStripMenuItem_Click);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(217, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // bulkMessageToolStripMenuItem
            // 
            this.bulkMessageToolStripMenuItem.Name = "bulkMessageToolStripMenuItem";
            this.bulkMessageToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.bulkMessageToolStripMenuItem.Text = "Bulk Message";
            this.bulkMessageToolStripMenuItem.Click += new System.EventHandler(this.bulkMessageToolStripMenuItem_Click);
            // 
            // downloadToUnitsToolStripMenuItem
            // 
            this.downloadToUnitsToolStripMenuItem.Name = "downloadToUnitsToolStripMenuItem";
            this.downloadToUnitsToolStripMenuItem.Size = new System.Drawing.Size(120, 20);
            this.downloadToUnitsToolStripMenuItem.Text = "Download To Units";
            this.downloadToUnitsToolStripMenuItem.Click += new System.EventHandler(this.downloadToUnitsToolStripMenuItem_Click);
            // 
            // exportLogToolStripMenuItem
            // 
            this.exportLogToolStripMenuItem.Name = "exportLogToolStripMenuItem";
            this.exportLogToolStripMenuItem.Size = new System.Drawing.Size(75, 20);
            this.exportLogToolStripMenuItem.Text = "Export Log";
            this.exportLogToolStripMenuItem.Click += new System.EventHandler(this.exportLogToolStripMenuItem_Click);
            // 
            // cmItemDetails
            // 
            this.cmItemDetails.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.showDetailsToolStripMenuItem,
            this.filterMultipleVehiclesToolStripMenuItem,
            this.addThisVehicleToFilterToolStripMenuItem,
            this.clearFiltersToolStripMenuItem});
            this.cmItemDetails.Name = "cmItemDetails";
            this.cmItemDetails.Size = new System.Drawing.Size(195, 92);
            // 
            // showDetailsToolStripMenuItem
            // 
            this.showDetailsToolStripMenuItem.Name = "showDetailsToolStripMenuItem";
            this.showDetailsToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.showDetailsToolStripMenuItem.Text = "Show Details";
            this.showDetailsToolStripMenuItem.Click += new System.EventHandler(this.showDetailsToolStripMenuItem_Click);
            // 
            // filterMultipleVehiclesToolStripMenuItem
            // 
            this.filterMultipleVehiclesToolStripMenuItem.Name = "filterMultipleVehiclesToolStripMenuItem";
            this.filterMultipleVehiclesToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.filterMultipleVehiclesToolStripMenuItem.Text = "Filter multiple vehicles";
            this.filterMultipleVehiclesToolStripMenuItem.Click += new System.EventHandler(this.filterMultipleVehiclesToolStripMenuItem_Click);
            // 
            // addThisVehicleToFilterToolStripMenuItem
            // 
            this.addThisVehicleToFilterToolStripMenuItem.Name = "addThisVehicleToFilterToolStripMenuItem";
            this.addThisVehicleToFilterToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.addThisVehicleToFilterToolStripMenuItem.Text = "Quick filter this vehicle";
            this.addThisVehicleToFilterToolStripMenuItem.Click += new System.EventHandler(this.addThisVehicleToFilterToolStripMenuItem_Click);
            // 
            // clearFiltersToolStripMenuItem
            // 
            this.clearFiltersToolStripMenuItem.Name = "clearFiltersToolStripMenuItem";
            this.clearFiltersToolStripMenuItem.Size = new System.Drawing.Size(194, 22);
            this.clearFiltersToolStripMenuItem.Text = "Clear Filters";
            this.clearFiltersToolStripMenuItem.Click += new System.EventHandler(this.clearFiltersToolStripMenuItem_Click);
            // 
            // windowToolStripMenuItem
            // 
            this.windowToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.AutoloadLogToolStripMenuItem});
            this.windowToolStripMenuItem.Name = "windowToolStripMenuItem";
            this.windowToolStripMenuItem.Size = new System.Drawing.Size(63, 20);
            this.windowToolStripMenuItem.Text = "Window";
            // 
            // AutoloadLogToolStripMenuItem
            // 
            this.AutoloadLogToolStripMenuItem.CheckOnClick = true;
            this.AutoloadLogToolStripMenuItem.Name = "AutoloadLogToolStripMenuItem";
            this.AutoloadLogToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.AutoloadLogToolStripMenuItem.Text = "Autoload Log";
            this.AutoloadLogToolStripMenuItem.CheckedChanged += new System.EventHandler(this.AutoloadLogToolStripMenuItem_CheckedChanged);
            // 
            // ListenerForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1534, 762);
            this.Controls.Add(this.mnuMain);
            this.Controls.Add(this.splitMain);
            this.IsMdiContainer = true;
            this.MinimumSize = new System.Drawing.Size(1550, 800);
            this.Name = "ListenerForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "MTData DATS Listener";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ListenerForm_Load);
            this.splitMain.Panel1.ResumeLayout(false);
            this.splitMain.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitMain)).EndInit();
            this.splitMain.ResumeLayout(false);
            this.splitSub.Panel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitSub)).EndInit();
            this.splitSub.ResumeLayout(false);
            this.gbSettings.ResumeLayout(false);
            this.gbSettings.PerformLayout();
            this.gbActivity.ResumeLayout(false);
            this.mnuMain.ResumeLayout(false);
            this.mnuMain.PerformLayout();
            this.cmItemDetails.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.SplitContainer splitMain;
        private System.Windows.Forms.GroupBox gbActivity;
        private System.Windows.Forms.ListView lvActivity;
        private System.Windows.Forms.MenuStrip mnuMain;
        private System.Windows.Forms.ToolStripMenuItem fileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem bulkMessageToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitSub;
        private System.Windows.Forms.GroupBox gbSettings;
        private System.Windows.Forms.TextBox txtRemoteStateUpdatePort;
        private System.Windows.Forms.Label lblHRemoteState;
        private System.Windows.Forms.TextBox txtDSN;
        private System.Windows.Forms.Label lblHDSN;
        private System.Windows.Forms.TextBox txtLiveUpdatesOn;
        private System.Windows.Forms.Label lblHLiveUpdate;
        private System.Windows.Forms.TextBox txtBindPort;
        private System.Windows.Forms.TextBox txtBindIP;
        private System.Windows.Forms.Label lblHBindTo;
        private System.Windows.Forms.ContextMenuStrip cmItemDetails;
        private System.Windows.Forms.ToolStripMenuItem showDetailsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem downloadToUnitsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem connectToAnotherServerToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem addThisVehicleToFilterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem clearFiltersToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem filterMultipleVehiclesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem exportLogToolStripMenuItem;
        private System.Windows.Forms.SaveFileDialog saveFileExportDialog;
        private System.Windows.Forms.ToolStripMenuItem windowToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem AutoloadLogToolStripMenuItem;
    }
}
﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using MTData.Transport.Application.Communication;
using MTData.Transport.Service.Listener.GatewayServiceBL.Classes;
using log4net;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public partial class ListenerForm : Form
    {
        #region private fields
        private List<ListenerLogItem> allList = new List<ListenerLogItem>();
        private static ILog _log = LogManager.GetLogger(typeof(ListenerForm));
        private int currentFunction = 0;
        private Timer logTimer;
        private DateTime lastChecked = DateTime.Now.Subtract(new TimeSpan(1,0,0)).ToUniversalTime();
        private DateTime lastUpdated;
        private int refreshCount = 30;
        private int refreshFleetVehicles = 600;
        private bool startLoad = true;
        private Boolean StartAutoLog;

        private Dictionary<Tuple<int, int>, Vehicle> _fleetVehicles;
        private object _dataSync = new object();

        private List<ListenerLogItem> logFilterList = new List<ListenerLogItem>();
        #endregion

        public bool ConnectToNewServer { get; set; }

        public ListenerForm()
        {
            InitializeComponent();

            ConnectToNewServer = false;

            _fleetVehicles = new Dictionary<Tuple<int, int>, Vehicle>();

            this.FetchRegistryValues();

            this.LoadVersion();
        }

        private void LoadVersion()
        {
            this.Text = "MTData DATS Listener " + System.Windows.Forms.Application.ProductVersion;
        }

        private void FetchRegistryValues()
        {
            this.StartAutoLog = Boolean.Parse(Helpers.ReadRegistryKey(@"MTDATA\ListenerTool", "AutoLog") == null ? "False" : Helpers.ReadRegistryKey(@"MTDATA\ListenerTool", "AutoLog"));
            this.AutoloadLogToolStripMenuItem.Checked = this.StartAutoLog;
        }


        private void StartListenerLog()
        {
            this.logTimer = new Timer();

            this.logTimer.Tick += logTimer_Tick;

            this.logTimer.Interval = 1000;

            this.logTimer.Start();
        }

        void logTimer_Tick(object sender, EventArgs e)
        {
            if (this.StartAutoLog)
            {
                this.logTimer.Stop();

                if (this.refreshCount == 0)
                {
                    this.LoadListenerLog(false);

                    refreshCount = 30;
                }
                else if (startLoad)
                {
                    this.LoadListenerLog(false);
                }
                else
                {
                    this.refreshCount--;
                }

                refreshFleetVehicles--;
                if (refreshFleetVehicles <= 0)
                {
                    refreshFleetVehicles = 600;
                    LoadFleetVehicles();
                }

                this.logTimer.Start();
            }
        }

        private void LoadListViewColumns()
        {
            lvActivity.Columns.Add(Helpers.CreateHeader("logid", "Log ID"));
            lvActivity.Columns.Add(Helpers.CreateHeader("fleetid", "Fleet ID"));
            lvActivity.Columns.Add(Helpers.CreateHeader("fleetname", "Fleet Name"));
            lvActivity.Columns.Add(Helpers.CreateHeader("vehcielid", "Vehicle ID"));
            lvActivity.Columns.Add(Helpers.CreateHeader("serialNumber", "Serial Number"));
            lvActivity.Columns.Add(Helpers.CreateHeader("vehiclename", "Display Name"));
            lvActivity.Columns.Add(Helpers.CreateHeader("direction", "Direction"));
            lvActivity.Columns.Add(Helpers.CreateHeader("messagetype", "Type"));
            lvActivity.Columns.Add(Helpers.CreateHeader("ipaddress", "IP Address"));
            lvActivity.Columns.Add(Helpers.CreateHeader("gpstime", "GPS Time"));
            lvActivity.Columns.Add(Helpers.CreateHeader("flags", "Flags"));
            lvActivity.Columns.Add(Helpers.CreateHeader("logdate", "Log Date"));

            foreach (ColumnHeader ch in lvActivity.Columns)
            {
                ch.Width = -2;
            }
        }

        private void LoadListenerLog(Boolean useAllList)
        {
            if (!Helpers.LogLoading)
            {
                Helpers.LogLoading = true;

                List<ListenerLogItem> tmpList = null;

                this.lastUpdated = DateTime.Now;
                try
                {
                    _log.InfoFormat("Call GetListenerLog with last checked time - {0}", lastChecked);
                    tmpList = Helpers.ListenerGateway().GetListenerLog(lastChecked, Helpers.UserCredentials);

                    while (tmpList.Count > 0 || useAllList)
                    {
                        allList.AddRange(tmpList);

                        DateTime removeDate = DateTime.Now.Subtract(new TimeSpan(0, 60, 0)).ToUniversalTime();

                        List<ListenerLogItem> removeList = new List<ListenerLogItem>();

                        // Remove from the all list the records older than 1 hr
                        foreach (ListenerLogItem li in allList)
                        {
                            if (li.LogDate < removeDate)
                            {
                                removeList.Add(li);
                            }
                        }

                        foreach (ListenerLogItem li in removeList)
                        {
                            allList.Remove(li);
                        }

                        if (useAllList)
                        {
                            lvActivity.Items.Clear();
                            tmpList = allList;
                            useAllList = false;
                        }

                        _log.InfoFormat("GetListenerLog returned with {0} items", tmpList.Count);

                        if (tmpList != null)
                        {
                            int rowCount = 0;
                            lvActivity.SuspendLayout();

                            int tmpTotal = tmpList.Count;
                            int tmpCount = 0;

                            UpdateLogStatusCount(tmpCount, tmpTotal);

                            foreach (ListenerLogItem item in tmpList)
                            {
                                if ((logFilterList.Count == 0) || (logFilterList.FindAll(li => li.FleetID == item.FleetID && li.VehicleID == item.VehicleID).Count > 0))
                                {
                                    try
                                    {
                                        AddMessageToList(item);
                                    }
                                    catch(Exception ex)
                                    {
                                        string exceptionMessage = ex.Message;
                                    }

                                    if (rowCount == 20)
                                    {
                                        lvActivity.ResumeLayout();
                                        lvActivity.SuspendLayout();
                                        //System.Windows.Forms.Application.DoEvents();
                                        this.Refresh();
                                        rowCount = 0;
                                    }
                                }

                                lastChecked = item.LogDate > lastChecked ? item.LogDate : lastChecked;
                                rowCount++;
                                tmpCount++;

                                UpdateLogStatusCount(tmpCount, tmpTotal);
                            }

                            //System.Threading.Thread.Sleep(1000);

                            tmpList = Helpers.ListenerGateway().GetListenerLog(lastChecked, Helpers.UserCredentials);
                        }
                    }
                    foreach (ColumnHeader ch in lvActivity.Columns)
                    {
                        ch.Width = -2;
                    }
                    lvActivity.ResumeLayout();
                    _log.InfoFormat("GetListenerLog set lastChecked {0}", lastChecked);

                    startLoad = false;
                }
                catch (Exception ex)
                {
                    _log.Error(ex);
                }

                this.UpdateLogStatus();
                Helpers.LogLoading = false;

                this.exportLogToolStripMenuItem.Visible = lvActivity.Items.Count > 0;
            }
        }

        private void UpdateLogStatusCount(int countAt, int countTotal)
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { UpdateLogStatusCount(countAt, countTotal); }));
            }
            else
            {
                gbActivity.Text = "Activity Snapshot - Remaining " + (countTotal - countAt).ToString() + " From " + countTotal.ToString();
            }
        }

        private void UpdateLogStatus()
        {
            if (this.InvokeRequired)
            {
                BeginInvoke(new MethodInvoker(delegate() { UpdateLogStatus(); }));
            }
            else
            {
                gbActivity.Text = "Activity Snapshot - Last Update " + this.lastUpdated.ToShortDateString() + " " + this.lastUpdated.ToShortTimeString()
                    + (refreshCount != 0 ? " - Auto Update in " + refreshCount.ToString() : string.Empty);
            }
        }

        private void AddMessageToList(ListenerLogItem item)
        {
            try
            {
                MTData.Transport.Gateway.Packet.GatewayProtocolPacket pkt = new Gateway.Packet.GatewayProtocolPacket("dd/MM/yyyy HH:mm:ss");
                pkt.Decode(Helpers.StringToByteArray(item.ListenerMessage), 0);
                MTData.Transport.Gateway.Packet.GeneralGPPacket pkt2 = new Gateway.Packet.GeneralGPPacket(pkt, "dd/MM/yyyy HH:mm:ss");


                ListViewItem messageItem = new ListViewItem(item.LogID == 0 ? (lvActivity.Items.Count + 1).ToString() : item.LogID.ToString());

                //find vehicle object
                Vehicle v = null;
                Tuple<int, int> t = new Tuple<int, int>(item.FleetID, item.VehicleID);
                if (_fleetVehicles.ContainsKey(t))
                {
                    v = _fleetVehicles[t];
                }

                messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.FleetID.ToString()));
                if (string.IsNullOrEmpty(item.FleetName) && v != null)
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, v.FleetName));
                }
                else
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.FleetName));
                }
                messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.VehicleID.ToString()));

                if (item.SerialNumber == null && v != null)
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, v.SerialNumber));
                }
                else if (item.SerialNumber == null)
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, "Unavailable"));
                }
                else
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.SerialNumber));
                }

                if (string.IsNullOrEmpty(item.VehicleDisplayName) && v != null)
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, v.DisplayName));
                }
                else
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.VehicleDisplayName));
                }

                messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.Direction
                    == LogDirection.InBound ? "In" : "Out"));

                messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.MessageType));

                messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.IPAddress));

                if (pkt2 != null)
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, pkt2.mCurrentClock.ToString()));
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, this.BuildFlags(pkt2, item.Direction)));
                }
                else
                {
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, "Unknown"));
                    messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, "Unknown"));
                }


                messageItem.SubItems.Add(new ListViewItem.ListViewSubItem(messageItem, item.LogDate.ToLocalTime().ToString()));

                messageItem.Tag = item;

                if (pkt2 != null)
                {
                    messageItem.ForeColor = (pkt2.bUsingSecondaryServer ? Color.Blue : Color.Green);
                }

                lvActivity.Items.Insert(0, messageItem);
            }
            catch (Exception exp)
            {
                _log.Error(string.Format("Unable to add packet to list for item {0}", item.ListenerMessage), exp);
            }
            
        }

        private string BuildFlags(MTData.Transport.Gateway.Packet.GeneralGPPacket gppkt, LogDirection direction)
        {
            string retString = string.Empty;

            if (gppkt.bAckImmediately)
            {
                retString += " (ARQ)";
            }

            if (gppkt.bAckRegardless)
            {
                retString += " (RGD)";
            }

            if (direction == LogDirection.Outbound && gppkt.bSendFlash)
            {
                retString += " (FLA)";

            }
            else if (gppkt.bReceivedSendFlash)
            {
                retString += " (FLA)";
            }
            
            if (gppkt.bArchivalData)
            {
                retString += " (ARV)";
            }

            if (gppkt.bUsingSecondaryServer)
            {
                retString += " (SEC)";
            }

            if (gppkt.bRevertToPrimaryServer)
            {
                retString += " (REV)";
            }

            return retString;
        }

        private void LoadServerBindings()
        {
            MTData.Transport.Service.Listener.GatewayServiceBL.Classes.ServerBinding binding = Helpers.ListenerGateway().GetBindingDetails(Helpers.UserCredentials);

            this.txtBindIP.Text = binding.BindToIP;
            this.txtBindPort.Text = binding.BindToPort;
            this.txtDSN.Text = binding.DSN;
            this.txtLiveUpdatesOn.Text = binding.LiveUpdatePort;
            this.txtRemoteStateUpdatePort.Text = binding.RemoteStatePort;
        }

        private void bulkMessageToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentFunction != 1)
            {
                this.SetupPanel(1);
            }
        }

        private void SetupPanel(int function)
        {
            this.ClearSplitSubPanel2();

            if (function == 1)
            {
                UserControls.ucBulkMessages bm = new UserControls.ucBulkMessages();
                bm.Dock = DockStyle.Fill;
                bm.Visible = true;
                bm.BringToFront();
                this.LoadUserControlToSub(bm);
            }
            else if(function == 2)
            {
                this.ClearSplitSubPanel2();

                UserControls.ucDownloads bm = new UserControls.ucDownloads();
                bm.Dock = DockStyle.Fill;
                bm.Visible = true;
                bm.BringToFront();
                this.LoadUserControlToSub(bm);
            }

            currentFunction = function;
        }

        private void LoadUserControlToSub(UserControl uc)
        {
            splitSub.Panel2.Controls.Add(uc);
        }

        private void ClearSplitSubPanel2()
        {
            if (this.splitSub.Panel2.Controls.Count > 0)
            {
                this.splitSub.Panel2.Controls.Clear();
            }
        }

        private void lvActivity_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                if (lvActivity.FocusedItem != null)
                {
                    if (lvActivity.FocusedItem.Bounds.Contains(e.Location) == true)
                    {
                        cmItemDetails.Items.Find(showDetailsToolStripMenuItem.Name, true)[0].Visible = true;

                        cmItemDetails.Items.Find(clearFiltersToolStripMenuItem.Name, true)[0].Visible = logFilterList.Count != 0;

                        cmItemDetails.Items.Find(addThisVehicleToFilterToolStripMenuItem.Name, true)[0].Visible
                            = logFilterList.FindAll(li => ((li.FleetID == ((ListenerLogItem)lvActivity.FocusedItem.Tag).FleetID)
                                && (li.VehicleID == ((ListenerLogItem)lvActivity.FocusedItem.Tag).VehicleID))).Count == 0;

                        cmItemDetails.Show(Cursor.Position);
                    }
                }
                else
                {
                    cmItemDetails.Items.Find(showDetailsToolStripMenuItem.Name, true)[0].Visible = false;

                    cmItemDetails.Items.Find(clearFiltersToolStripMenuItem.Name, true)[0].Visible = logFilterList.Count != 0;

                    cmItemDetails.Items.Find(addThisVehicleToFilterToolStripMenuItem.Name, true)[0].Visible =false;

                    cmItemDetails.Show(Cursor.Position);
                }
            } 
        }

        private void showDetailsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.ShowDetailsForItem((ListenerLogItem)lvActivity.FocusedItem.Tag);
        }

        private void ShowDetailsForItem(ListenerLogItem logItem)
        {
            UserForms.frmLogDetails logDetails = new UserForms.frmLogDetails(logItem);

            logDetails.StartPosition = FormStartPosition.CenterParent;

            logDetails.ShowDialog(this);

            logDetails.Close();

            logDetails = null;
        }

        private void downloadToUnitsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (currentFunction != 2)
            {
                this.SetupPanel(2);
            }
        }

        private void ListenerForm_Resize(object sender, EventArgs e)
        {
            this.Text = String.Format("Width: {0}, Height: {1}", this.Width, this.Height);
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are you sure you want to exit?", "Exit Program", MessageBoxButtons.YesNo) == System.Windows.Forms.DialogResult.Yes)
            {
                System.Windows.Forms.Application.Exit();
            }
        }

        private void connectToAnotherServerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logTimer.Stop();
            ConnectToNewServer = true;
            Close();
        }

        private void ListenerForm_Load(object sender, EventArgs e)
        {
            this.LoadListViewColumns();
            this.LoadServerBindings();
            LoadFleetVehicles();
            this.StartListenerLog();
        }

        private void LoadFleetVehicles()
        {
            try
            {
                lock (_dataSync)
                {
                    _fleetVehicles.Clear();

                    List<Vehicle> currentVehicles = Helpers.ListenerGateway().GetFleetVehicles(Helpers.UserCredentials);
                    _log.InfoFormat("GetFleetVehicles called, returned {0} vehicle objects", currentVehicles.Count);

                    foreach (Vehicle veh in currentVehicles)
                    {
                        Tuple<int, int> t = new Tuple<int, int>(veh.FleetID, veh.VehicleID);
                        if (!_fleetVehicles.ContainsKey(t))
                        {
                            _fleetVehicles.Add(t, veh);
                        }
                    }
                }
            }
            catch
            { }
        }

        private void addThisVehicleToFilterToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logFilterList.Add((ListenerLogItem)lvActivity.FocusedItem.Tag);

            this.LoadListenerLog(true);
        }

        private void clearFiltersToolStripMenuItem_Click(object sender, EventArgs e)
        {
            logFilterList.Clear();

            this.LoadListenerLog(true);
        }

        private void filterMultipleVehiclesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            UserForms.frmMultiFilter tmpForm = new UserForms.frmMultiFilter(_fleetVehicles);

            tmpForm.StartPosition = FormStartPosition.CenterParent;

            tmpForm.ShowDialog();

            if (tmpForm.OK)
            {
                if (tmpForm.VehicleList.Count > 0)
                {
                    logFilterList.Clear();
                    logFilterList.AddRange(tmpForm.VehicleList);

                    this.LoadListenerLog(true);
                }
            }

            tmpForm.Dispose();
            tmpForm = null;
        }

        private void lvActivity_MouseDown(object sender, MouseEventArgs e)
        {
            lvActivity_MouseClick(sender, e);
        }

        private void exportLogToolStripMenuItem_Click(object sender, EventArgs e)
        {
            saveFileExportDialog.DefaultExt = ".csv";
            if (saveFileExportDialog.ShowDialog() == DialogResult.OK)
            {
                if (saveFileExportDialog.FileName != string.Empty)
                {
                    this.ExportLogToFile(saveFileExportDialog.FileName);
                }
            }
        }

        private void ExportLogToFile(string fileNameAndPath)
        {
            //lvActivity.Columns.Add(Helpers.CreateHeader("logid", "Log ID"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("fleetid", "Fleet ID"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("fleetname", "Fleet Name"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("vehcielid", "Vehicle ID"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("serialNumber", "Serial Number"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("vehiclename", "Display Name"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("direction", "Direction"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("messagetype", "Type"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("ipaddress", "IP Address"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("gpstime", "GPS Time"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("flags", "Flags"));
            //lvActivity.Columns.Add(Helpers.CreateHeader("logdate", "Log Date"));

            StringBuilder sb = new StringBuilder();

            string outputline = "Log ID, Fleet ID, Fleet Name, Vehicle ID, Serial Number, Display Name, Direction, Type, IP Address, GPS Time, ";
            outputline += "Flags, Log Date, PacketData";

            sb.AppendLine(outputline);

            int logId = 1;
            foreach (ListViewItem lvi in lvActivity.Items)
            {
                ListenerLogItem lli = (ListenerLogItem)lvi.Tag;

                Vehicle v = null;
                Tuple<int, int> t = new Tuple<int, int>(lli.FleetID, lli.VehicleID);
                if (_fleetVehicles.ContainsKey(t))
                {
                    v = _fleetVehicles[t];
                }

                MTData.Transport.Gateway.Packet.GatewayProtocolPacket pkt = new Gateway.Packet.GatewayProtocolPacket("dd/MM/yyyy HH:mm:ss");

                pkt.Decode(Helpers.StringToByteArray(lli.ListenerMessage), 0);

                MTData.Transport.Gateway.Packet.GeneralGPPacket pkt2 = new Gateway.Packet.GeneralGPPacket(pkt, "dd/MM/yyyy HH:mm:ss");

                if (v != null)
                {
                    outputline = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12}", logId, lli.FleetID, v.FleetName, lli.VehicleID, v.SerialNumber, 
                        v.DisplayName, (lli.Direction == LogDirection.InBound ? "In" : "Out"), lli.MessageType, lli.IPAddress, pkt2.mCurrentClock.ToString(), 
                        BuildFlags(pkt2, lli.Direction), lli.LogDate.ToLocalTime().ToString(), lli.ListenerMessage);
                }
                else
                {
                    outputline = string.Format("{0},{1}, ,{2},Unavailable, ,{3},{4},{5},{6},{7},{8},{9}", logId, lli.FleetID, lli.VehicleID, 
                        (lli.Direction == LogDirection.InBound ? "In" : "Out"), lli.MessageType, lli.IPAddress, pkt2.mCurrentClock.ToString(),
                        BuildFlags(pkt2, lli.Direction), lli.LogDate.ToLocalTime().ToString(), lli.ListenerMessage);
                }
                sb.AppendLine(outputline);
                logId++;
            }

            System.IO.File.WriteAllText(fileNameAndPath, sb.ToString());

            MessageBox.Show("Data saved to " + fileNameAndPath);
        }

        private void AutoloadLogToolStripMenuItem_CheckedChanged(object sender, EventArgs e)
        {
            this.StartAutoLog = AutoloadLogToolStripMenuItem.Checked;
            Helpers.WriteRegistryKey(@"MTDATA\ListenerTool", "AutoLog", AutoloadLogToolStripMenuItem.Checked.ToString());
        }
    }
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using MTData.Transport.Gateway.Packet;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using log4net;

namespace MTData.Transport.Listener.DatsListenerApp
{
	/// <summary>
	/// Summary description for frmSendVerify.
	/// </summary>
	public class frmSendVerify : System.Windows.Forms.Form
	{
        private const string sClassName = "DATSListenerApp.frmSendVerify.";
        private static ILog _log = LogManager.GetLogger(typeof(frmSendVerify));

		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private System.Windows.Forms.GroupBox groupBox1;
		private System.Windows.Forms.RadioButton rbMDT;
		private System.Windows.Forms.RadioButton rb1035;
		private System.Windows.Forms.RadioButton rb302x;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.NumericUpDown numProgramChecksum;
		private System.Windows.Forms.NumericUpDown numPacketChecksum;
		private System.Windows.Forms.NumericUpDown numPacketCount;
		private System.Windows.Forms.NumericUpDown numProgramBytes;
		private System.Windows.Forms.Button btnSendVerify;
		private System.Windows.Forms.Label lblSendTo;
		private GatewayProtocolPacket mPacket = null;
		private PacketDatabaseInterface mDatabase;
		private string _serverTime_DateFormat = "";
		private AcknowledgementNotificationDelegate del = null;

        public frmSendVerify(GatewayProtocolPacket packet, PacketDatabaseInterface oDatabase, string serverTime_DateFormat)
		{
            try
            {
                InitializeComponent();
                mPacket = packet;
                mDatabase = oDatabase;
                _serverTime_DateFormat = serverTime_DateFormat;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmSendVerify(GatewayProtocolPacket packet, PacketDatabaseInterface oDatabase, string serverTime_DateFormat)", ex);
                throw new System.Exception(sClassName + "frmSendVerify(GatewayProtocolPacket packet, PacketDatabaseInterface oDatabase, string serverTime_DateFormat)", ex);

            }
		}

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			mPacket = null;
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rbMDT = new System.Windows.Forms.RadioButton();
			this.rb1035 = new System.Windows.Forms.RadioButton();
			this.rb302x = new System.Windows.Forms.RadioButton();
			this.numProgramChecksum = new System.Windows.Forms.NumericUpDown();
			this.label5 = new System.Windows.Forms.Label();
			this.numPacketChecksum = new System.Windows.Forms.NumericUpDown();
			this.label1 = new System.Windows.Forms.Label();
			this.numPacketCount = new System.Windows.Forms.NumericUpDown();
			this.label2 = new System.Windows.Forms.Label();
			this.numProgramBytes = new System.Windows.Forms.NumericUpDown();
			this.label3 = new System.Windows.Forms.Label();
			this.btnSendVerify = new System.Windows.Forms.Button();
			this.lblSendTo = new System.Windows.Forms.Label();
			this.groupBox1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.numProgramChecksum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPacketChecksum)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numPacketCount)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.numProgramBytes)).BeginInit();
			this.SuspendLayout();
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rbMDT);
			this.groupBox1.Controls.Add(this.rb1035);
			this.groupBox1.Controls.Add(this.rb302x);
			this.groupBox1.Location = new System.Drawing.Point(8, 32);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(328, 56);
			this.groupBox1.TabIndex = 8;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Download Type";
			// 
			// rbMDT
			// 
			this.rbMDT.Location = new System.Drawing.Point(216, 24);
			this.rbMDT.Name = "rbMDT";
			this.rbMDT.Size = new System.Drawing.Size(88, 16);
			this.rbMDT.TabIndex = 10;
			this.rbMDT.Text = "MDT Image";
			// 
			// rb1035
			// 
			this.rb1035.Location = new System.Drawing.Point(112, 24);
			this.rb1035.Name = "rb1035";
			this.rb1035.Size = new System.Drawing.Size(88, 16);
			this.rb1035.TabIndex = 9;
			this.rb1035.Text = "1035 Image";
			// 
			// rb302x
			// 
			this.rb302x.Checked = true;
			this.rb302x.Location = new System.Drawing.Point(16, 24);
			this.rb302x.Name = "rb302x";
			this.rb302x.Size = new System.Drawing.Size(88, 16);
			this.rb302x.TabIndex = 8;
			this.rb302x.TabStop = true;
			this.rb302x.Text = "302x Image";
			// 
			// numProgramChecksum
			// 
			this.numProgramChecksum.Location = new System.Drawing.Point(120, 100);
			this.numProgramChecksum.Maximum = new System.Decimal(new int[] {
																			   99999999,
																			   0,
																			   0,
																			   0});
			this.numProgramChecksum.Minimum = new System.Decimal(new int[] {
																			   1,
																			   0,
																			   0,
																			   0});
			this.numProgramChecksum.Name = "numProgramChecksum";
			this.numProgramChecksum.Size = new System.Drawing.Size(80, 20);
			this.numProgramChecksum.TabIndex = 17;
			this.numProgramChecksum.Value = new System.Decimal(new int[] {
																			 7915106,
																			 0,
																			 0,
																			 0});
			// 
			// label5
			// 
			this.label5.Location = new System.Drawing.Point(8, 100);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(104, 16);
			this.label5.TabIndex = 18;
			this.label5.Text = "Program Checksum";
			// 
			// numPacketChecksum
			// 
			this.numPacketChecksum.Location = new System.Drawing.Point(120, 124);
			this.numPacketChecksum.Maximum = new System.Decimal(new int[] {
																			  99999999,
																			  0,
																			  0,
																			  0});
			this.numPacketChecksum.Minimum = new System.Decimal(new int[] {
																			  1,
																			  0,
																			  0,
																			  0});
			this.numPacketChecksum.Name = "numPacketChecksum";
			this.numPacketChecksum.Size = new System.Drawing.Size(80, 20);
			this.numPacketChecksum.TabIndex = 19;
			this.numPacketChecksum.Value = new System.Decimal(new int[] {
																			8054219,
																			0,
																			0,
																			0});
			// 
			// label1
			// 
			this.label1.Location = new System.Drawing.Point(8, 124);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(104, 16);
			this.label1.TabIndex = 20;
			this.label1.Text = "Packet Checksum";
			// 
			// numPacketCount
			// 
			this.numPacketCount.Location = new System.Drawing.Point(120, 148);
			this.numPacketCount.Maximum = new System.Decimal(new int[] {
																		   99999999,
																		   0,
																		   0,
																		   0});
			this.numPacketCount.Minimum = new System.Decimal(new int[] {
																		   1,
																		   0,
																		   0,
																		   0});
			this.numPacketCount.Name = "numPacketCount";
			this.numPacketCount.Size = new System.Drawing.Size(80, 20);
			this.numPacketCount.TabIndex = 21;
			this.numPacketCount.Value = new System.Decimal(new int[] {
																		 257,
																		 0,
																		 0,
																		 0});
			// 
			// label2
			// 
			this.label2.Location = new System.Drawing.Point(8, 148);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(104, 16);
			this.label2.TabIndex = 22;
			this.label2.Text = "Packet Count";
			// 
			// numProgramBytes
			// 
			this.numProgramBytes.Location = new System.Drawing.Point(120, 172);
			this.numProgramBytes.Maximum = new System.Decimal(new int[] {
																			99999999,
																			0,
																			0,
																			0});
			this.numProgramBytes.Minimum = new System.Decimal(new int[] {
																			1,
																			0,
																			0,
																			0});
			this.numProgramBytes.Name = "numProgramBytes";
			this.numProgramBytes.Size = new System.Drawing.Size(80, 20);
			this.numProgramBytes.TabIndex = 23;
			this.numProgramBytes.Value = new System.Decimal(new int[] {
																		  64250,
																		  0,
																		  0,
																		  0});
			// 
			// label3
			// 
			this.label3.Location = new System.Drawing.Point(8, 172);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(104, 16);
			this.label3.TabIndex = 24;
			this.label3.Text = "Program Bytes";
			// 
			// btnSendVerify
			// 
			this.btnSendVerify.Location = new System.Drawing.Point(240, 204);
			this.btnSendVerify.Name = "btnSendVerify";
			this.btnSendVerify.Size = new System.Drawing.Size(96, 24);
			this.btnSendVerify.TabIndex = 25;
			this.btnSendVerify.Text = "Send To Units";
			this.btnSendVerify.Click += new System.EventHandler(this.btnSendVerify_Click);
			// 
			// lblSendTo
			// 
			this.lblSendTo.Location = new System.Drawing.Point(8, 8);
			this.lblSendTo.Name = "lblSendTo";
			this.lblSendTo.Size = new System.Drawing.Size(320, 16);
			this.lblSendTo.TabIndex = 26;
			this.lblSendTo.Text = "Send Verify Packet to Fleet 1 Unit 1";
			// 
			// frmSendVerify
			// 
			this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
			this.ClientSize = new System.Drawing.Size(344, 237);
			this.Controls.Add(this.lblSendTo);
			this.Controls.Add(this.btnSendVerify);
			this.Controls.Add(this.numProgramBytes);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.numPacketCount);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.numPacketChecksum);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.numProgramChecksum);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.groupBox1);
			this.Name = "frmSendVerify";
			this.Text = "Send Verify Packet";
			this.Closing += new System.ComponentModel.CancelEventHandler(this.frmSendVerify_Closing);
			this.Load += new System.EventHandler(this.frmSendVerify_Load);
			this.groupBox1.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.numProgramChecksum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPacketChecksum)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numPacketCount)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.numProgramBytes)).EndInit();
			this.ResumeLayout(false);

		}
		#endregion

		private void frmSendVerify_Load(object sender, System.EventArgs e)
		{
            try
            {
                lblSendTo.Text = "Send Download Verify to Fleet " + Convert.ToString((int)mPacket.cFleetId) + " Unit " + Convert.ToString((int)mPacket.iVehicleId);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmSendVerify_Load(object sender, System.EventArgs e)", ex);
                throw new System.Exception(sClassName + "frmSendVerify_Load(object sender, System.EventArgs e)", ex);
            }
		}

		private void btnSendVerify_Click(object sender, System.EventArgs e)
		{
            try
            {
                ProgramVerifyPacket pvp = new ProgramVerifyPacket(mPacket, _serverTime_DateFormat);
                if (rb302x.Checked)
                    pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
                else
                {
                    if (rb1035.Checked)
                        pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_IOBOX_VERIFY;
                    else
                        pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_MDT_VERIFY;
                }
                pvp.cDownloadVersion = (byte)0x06;
                pvp.iProgramChecksumTotal = Convert.ToInt32(numProgramChecksum.Value);
                pvp.iPacketChecksumTotal = Convert.ToInt32(numPacketChecksum.Value);
                pvp.iPacketCountTotal = Convert.ToInt32(numPacketCount.Value);
                pvp.iProgramBytesCountTotal = Convert.ToInt32(numProgramBytes.Value);

                _log.Info("Sending Verify Packet : Program Checksum = " + pvp.iProgramChecksumTotal + ", Packet Checksum : " + pvp.iPacketChecksumTotal + ", Packet Count : " + pvp.iPacketCountTotal + ", Program Bytes Count : " + pvp.iProgramBytesCountTotal);

                del = new AcknowledgementNotificationDelegate(VerifyComplete);
                mDatabase.SendVerifyDownload(pvp, del, 2, 10);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnSendVerify_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "frmSendVerify_Load(object sender, System.EventArgs e)", "Error");
            }
		}

		private void VerifyComplete(bool bLastPacketWasGood)
		{
            try
            {
                if (!bLastPacketWasGood)
                    MessageBox.Show(this, "Verification Failed.", "MTData");
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "VerifyComplete(bool bLastPacketWasGood)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "VerifyComplete(bool bLastPacketWasGood)", "Error");
            }
		}

		private void frmSendVerify_Closing(object sender, System.ComponentModel.CancelEventArgs e)
		{
            try
            {
                del = null;
                mDatabase = null;
                mPacket = null;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmSendVerify_Closing(object sender, System.ComponentModel.CancelEventArgs e)", ex);
                throw new System.Exception(sClassName + "frmSendVerify_Closing(object sender, System.ComponentModel.CancelEventArgs e)", ex);
            }
		}
	}
}

using System;
using System.Collections;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Windows.Forms;
using MTData.Transport.Gateway.Packet;

namespace MTData.Transport.GatewayListener
{
	/// <summary>
	/// Summary description for OrderedTextBox.
	/// </summary>
	public class OrderedTextBox : System.Windows.Forms.RichTextBox
	{
		private delegate void AddMessageDelegate(GatewayProtocolPacket aPacket, 
			string aDirection,
			Color aColor);
		AddMessageDelegate amd;
		
		public delegate void AddMessageErrorDelegate(OrderedTextBox textBox, Exception ex, GatewayProtocolPacket packet, string direction);

		public event AddMessageErrorDelegate AddMessageError;

		private int iMaxLength;
		public override int MaxLength
		{
			get
			{
				return iMaxLength;
			}
			set
			{
				iMaxLength = value;
			}
		}
		/// <summary> 
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.Container components = null;
		private string _serverTime_DateFormat = "";

		public OrderedTextBox(string serverTime_DateFormat) : base()
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			// This call is required by the Windows.Forms Form Designer.
			InitializeComponent();

			// TODO: Add any initialization after the InitForm call
			iMaxLength = 500;

			// Do this once to speed up message insertions
			amd = new AddMessageDelegate(AddMessage);
			
		}

		
		private void AddMessage(GatewayProtocolPacket aPacket, 
			string aDirection,
			Color aColor)
		{
			string s;
            if (aPacket == null)
                return;

			lock(this) // Locking to avoid mishmash
			{
				try
				{
					// Scroll to show this item:
					if (this.Lines.Length > iMaxLength) this.Clear();
					this.SelectionColor = aColor;
				    string sRemoteAddress = "";
                    if(aPacket.mSenderIP != null)
					    sRemoteAddress = " (" + aPacket.mSenderIP.Address.ToString() + ":" + aPacket.mSenderIP.Port.ToString() + ") ";
                    else
                        sRemoteAddress = " (0.0.0.0:0000) ";
					sRemoteAddress = sRemoteAddress.PadRight(23);

					#region Time and source of message
					s = System.DateTime.Now.ToLongTimeString().PadRight(12)
						+ aDirection + " Fleet " + aPacket.cFleetId.ToString().PadRight(4)
						+ " Unit " + aPacket.iVehicleId.ToString().PadRight(8) + sRemoteAddress;
					#endregion

					#region App Ping display
					if (aPacket.cMsgType == GatewayProtocolPacket.GSP_APP_PING)
					{
						this.AppendText(s +	"\tApplication-Level PING\n");
						return;
					}
					#endregion
					#region Type and sequence numbers of message
					if(aDirection == "To  ")
					{
						s += GatewayProtocolLookup.GetMessageType(aPacket.cMsgType).PadRight(25)
							+ "Seq: " + aPacket.cOurSequence.ToString().PadRight(4)
							+ "Ack: " + aPacket.cAckSequence.ToString().PadRight(4);
					}
					else
					{
						s += GatewayProtocolLookup.GetMessageType(aPacket.cMsgType).PadRight(25)
							+ "Ack: " + aPacket.cOurSequence.ToString().PadRight(4)
							+ "Seq: " + aPacket.cAckSequence.ToString().PadRight(4);
					}
					#endregion
					#region Special decodes for Status Reports, Mail and Error messages
					if (aPacket.cMsgType == GeneralGPPacket.STATUS_REPORT || 
						(aPacket.cMsgType == GeneralGPPacket.GEN_EXCESSIVE_IDLE) || 
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE) || 
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART) || 
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_SPEEDING_AT_WP) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_DOCK) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_DOCK) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_WORKSHOP) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_WORKSHOP) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_ARRIVE_NOGO) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_DEPART_NOGO) ||
						(aPacket.cMsgType == RoutePtSetPtGPPacket.SETPT_HOLDING_EXPIRY) ||
						(aPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_ON) || 
						(aPacket.cMsgType == GeneralGPPacket.STATUS_IGNITION_OFF) || 
						aPacket.cMsgType == GeneralGPPacket.STATUS_TRAILER_HITCH || 
						aPacket.cMsgType == GeneralGPPacket.STATUS_TRAILER_DEHITCH || 
						aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_1_START || 
						aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_2_START || 
						aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_3_START || 
						aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_1_END || 
						aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_2_END || 
						aPacket.cMsgType == GeneralGPPacket.OVERSPEED_ZONE_3_END  || 
						aPacket.cMsgType == GeneralGPPacket.ANGEL_GEAR_ALERT_START || 
						aPacket.cMsgType == GeneralGPPacket.ANGEL_GEAR_ALERT_END ||
						aPacket.cMsgType == GeneralGPPacket.FATIGUE_REPORT_IGNON ||
						aPacket.cMsgType == GeneralGPPacket.FATIGUE_REPORT_24 ||
						aPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT ||
                        aPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_START ||
                        aPacket.cMsgType == GeneralGPPacket.STATUS_VEHICLE_BREAK_END ||
                        aPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS ||
                        aPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT ||
                        aPacket.cMsgType == GeneralGPPacket.SYSTEM_GPRS_FAILURE ||
                        aPacket.cMsgType == GeneralGPPacket.GEN_MDT_STATUS_REPORT ||
                        aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_TEST ||
                        aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM ||
                        aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_CLEARED ||
                        aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_ACKD_BY_USER ||
                        aPacket.cMsgType == GeneralGPPacket.PENDENT_ALARM_NOT_ACKD_BY_USER ||
                        aPacket.cMsgType == GeneralGPPacket.STATUS_LOGIN ||
                        aPacket.cMsgType == GeneralGPPacket.STATUS_LOGOUT ||
                        aPacket.cMsgType == GeneralGPPacket.GEN_IF_CONFIG_RESTART ||
                        aPacket.cMsgType == GeneralGPPacket.GEN_ZERO_BYTES_RECEIVED_RESTART ||
                        aPacket.cMsgType == GeneralGPPacket.SHELL_REQUEST_REBOOT ||
                        aPacket.cMsgType == GeneralGPPacket.ECM_HYSTERESIS_ERROR ||
                        aPacket.cMsgType == GeneralGPPacket.ALARM_LOW_BATTERY ||
                        aPacket.cMsgType == GeneralGPPacket.GFORCE_CALIBRATION ||
                        aPacket.cMsgType == GeneralGPPacket.GPS_SETTINGS ||
                        aPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC_LEVEL ||
                        aPacket.cMsgType == GeneralGPPacket.DIAGNOSTIC
                        )
					{
						GeneralGPPacket gpsPacket = new GeneralGPPacket(aPacket, _serverTime_DateFormat);
						// Show GPS Time for STATUS Reports:
						s += "GPS Time: " + gpsPacket.mCurrentClock.ToTimeString();
					}
                    else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_PERFORMANCE_METRICS)
                    {
                        GPDriverPerformanceMetrics dpmPacket = new GPDriverPerformanceMetrics(aPacket, _serverTime_DateFormat);
                        s += "GPS Time: " + dpmPacket.mCurrentClock.ToTimeString();
                    }
                    else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_DATAACCUMLATOR_METRICS)
                    {
                        GPDriverDataAccumulator ddaPacket = new GPDriverDataAccumulator(aPacket, _serverTime_DateFormat);
                        s += "GPS Time: " + ddaPacket.mCurrentClock.ToTimeString();
                    }
					else if (aPacket.cMsgType == MailMessagePacket.MAIL_MESSAGE)
					{
						MailMessagePacket mailPacket = new MailMessagePacket(aPacket, _serverTime_DateFormat);
						s += "  User: " + mailPacket.sTargetUser +
							"  Subject: " + mailPacket.sSubject;
					}
					else if (	((aPacket.cMsgType & 0xF0) == GatewayProtocolPacket.SYSTEM_PACKET_MASK) ||
						(aPacket.cMsgType == GeneralGPPacket.GEN_NAK))	
					{
						// Decode the Reason code if there is one
						s += "  Reason: " + GatewayProtocolLookup.GetErrorCode(aPacket.cMsgType,
							aPacket.mDataField[0]);
					}
					#endregion
					#region Flag Bit decoding
					if (aPacket.bAckImmediately) s += " (ARQ)";
					if (aPacket.bAckRegardless)	 s += " (RGD)";
					if(aDirection == "To  ")
					{
						if (aPacket.bSendFlash)		 s += " (FLA)";
					}
					else
					{
						if (aPacket.bReceivedSendFlash)	 s += " (FLA)";
					}
					if (aPacket.bArchivalData)   s += " (ARV)";
					if (aPacket.bUsingSecondaryServer)	s += " (SEC)";
					if (aPacket.bRevertToPrimaryServer)	s += " (REV)";
					#endregion
					#region Move along
					this.AppendText(s + "\n");
				}
				catch(Exception ex)
				{
					if (AddMessageError != null)
						AddMessageError(this, ex, aPacket, aDirection);
					this.AppendText("Error generating onScreen Message\n");
				}
			#endregion
			}
		}

		// Properly locked Clear():
		public new void Clear()
		{
			lock(this)
			{
				base.Clear();
			}
		}

		public void AddInboundMessage(GatewayProtocolPacket aPacket)
		{
			if (aPacket.bUsingSecondaryServer) 
				this.BeginInvoke(amd, new object[] {aPacket, "From", Color.Blue});
			else
				this.BeginInvoke(amd, new object[] {aPacket, "From", Color.Green});
		}

		public void AddOutboundMessage(GatewayProtocolPacket aPacket)
		{
			this.BeginInvoke(amd, new object[] {aPacket, "To  ", Color.Red});
		}

		/// <summary> 
		/// Clean up any resources being used.
		/// </summary>
		protected override void Dispose( bool disposing )
		{
			if( disposing )
			{
				if(components != null)
				{
					components.Dispose();
				}
			}
			base.Dispose( disposing );
		}

		#region Component Designer generated code
		/// <summary> 
		/// Required method for Designer support - do not modify 
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			components = new System.ComponentModel.Container();
		}
		#endregion
	}
}

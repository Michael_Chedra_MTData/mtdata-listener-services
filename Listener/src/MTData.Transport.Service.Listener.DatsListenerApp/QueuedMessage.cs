﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Listener.DatsListenerApp
{
    public class QueuedMessage
    {
        #region Private Fields
        private DateTime startDate;
        private DateTime expiryDate;
        private String extraData;
        private MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle vehicle;
        private MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem type;
        #endregion

        #region Constructor
        public QueuedMessage(MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle vehicle, MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem messageType, 
            DateTime startDate, DateTime expiryDate, string extraData)
        {
            this.vehicle = vehicle;
            this.type = messageType;
            this.startDate = startDate;
            this.expiryDate = expiryDate;
            this.extraData = extraData;
        }
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        public MTData.Transport.Service.Listener.GatewayServiceBL.Classes.Vehicle Vehicle
        {
            get { return this.vehicle; }
        }

        public MTData.Transport.Service.Listener.GatewayServiceBL.Classes.CustomListItem Type
        {
            get { return this.type; }
        }

        public DateTime StartDate
        {
            get { return this.startDate; }
        }

        public DateTime ExpiryDate
        {
            get { return this.expiryDate; }
        }

        public string ExtraData
        {
            get { return this.extraData; }
        }
        #endregion

        #region Methods

        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion

    }
}

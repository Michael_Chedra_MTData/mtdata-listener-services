using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using System.Data;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Gateway.Packet;
using log4net;
using MTData.Common.Utilities;
using MTData.Common.Threading;

namespace MTData.Transport.Listener.DatsListenerApp
{
    /// <summary>
    /// Summary description for frmChangeUnitID.
    /// </summary>
    public class frmChangeUnitID : System.Windows.Forms.Form
    {
        private static ILog _log = LogManager.GetLogger(typeof(frmChangeUnitID));
        private const string sClassName = "DATSListenerApp.frmChangeUnitID.";
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        private System.Windows.Forms.NumericUpDown numNewVehicleID;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.CheckBox chkMoveDriver;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ComboBox cmbScheduleID;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ComboBox cmbRPIDs;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ComboBox cmbSPIDs;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox cmbConfigID;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbToFleet;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ListBox listFromVehicles;
        private System.Windows.Forms.ComboBox cmbFromFleet;

        private GatewayAdministrator _listener = null;
        private object SyncRoot = null;
        private ArrayList oUpdatePackets = null;
        private Hashtable oDownloadToList = null;
        private Hashtable oWaitForLoginList = null;
        private bool bRefreshingLists = false;
        private System.Data.DataSet oFleetDS = null;
        private System.Data.DataSet oFleetVehicleDS = null;
        private System.Data.DataSet oFleetConfigDS = null;
        private System.Data.DataSet oFleetSetPointDS = null;
        private System.Data.DataSet oFleetRoutePointDS = null;
        private System.Data.DataSet oFleetScheduleDS = null;
        private System.Windows.Forms.RichTextBox txtStatus;
        private string sSendToKey = "";
        private string VehicleUpdateSQLCmd = "";
        private System.Windows.Forms.Button btnMoveUnit;
        private System.Windows.Forms.Button btnRefresh;
        private EnhancedThread tMatchUnit = null;


        public frmChangeUnitID(GatewayAdministrator oListener)
        {
            InitializeComponent();
            try
            {
                SyncRoot = new object();
                _listener = oListener;
                oUpdatePackets = ArrayList.Synchronized(new ArrayList());
                oDownloadToList = Hashtable.Synchronized(new Hashtable());
                oWaitForLoginList = Hashtable.Synchronized(new Hashtable());
                oFleetDS = new System.Data.DataSet();
                oFleetVehicleDS = new System.Data.DataSet();
                oFleetConfigDS = new System.Data.DataSet();
                oFleetSetPointDS = new System.Data.DataSet();
                oFleetRoutePointDS = new System.Data.DataSet();
                oFleetScheduleDS = new System.Data.DataSet();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmChangeUnitID(MTData.Transport.GatewayListener.GatewayAdministrator oListener)", ex);
                throw new System.Exception(sClassName + "frmChangeUnitID(MTData.Transport.GatewayListener.GatewayAdministrator oListener)", ex);
            }
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            try
            {
                if (tMatchUnit != null)
                {
                    if (tMatchUnit.Running)
                        tMatchUnit.Stop();
                }
                tMatchUnit = null;
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (oDownloadToList != null)
                    lock (oDownloadToList.SyncRoot)
                        oDownloadToList.Clear();
                oDownloadToList = null;
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (oWaitForLoginList != null)
                    lock (oWaitForLoginList.SyncRoot)
                        oWaitForLoginList.Clear();
                oWaitForLoginList = null;
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (oUpdatePackets != null)
                {
                    lock (oUpdatePackets.SyncRoot)
                        oUpdatePackets.Clear();
                }
                oUpdatePackets = null;
            }
            catch (System.Exception)
            {
            }

            try
            {
                if (oFleetDS != null) oFleetDS.Dispose();
                oFleetDS = null;
                if (oFleetVehicleDS != null) oFleetVehicleDS.Dispose();
                oFleetVehicleDS = null;
                if (oFleetConfigDS != null) oFleetConfigDS.Dispose();
                oFleetConfigDS = null;
                if (oFleetSetPointDS != null) oFleetSetPointDS.Dispose();
                oFleetSetPointDS = null;
                if (oFleetRoutePointDS != null) oFleetRoutePointDS.Dispose();
                oFleetRoutePointDS = null;
                if (oFleetScheduleDS != null) oFleetScheduleDS.Dispose();
                oFleetScheduleDS = null;
            }
            catch (System.Exception)
            {
            }

            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.numNewVehicleID = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.chkMoveDriver = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbScheduleID = new System.Windows.Forms.ComboBox();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbRPIDs = new System.Windows.Forms.ComboBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbSPIDs = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbConfigID = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbToFleet = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnMoveUnit = new System.Windows.Forms.Button();
            this.listFromVehicles = new System.Windows.Forms.ListBox();
            this.cmbFromFleet = new System.Windows.Forms.ComboBox();
            this.txtStatus = new System.Windows.Forms.RichTextBox();
            this.btnRefresh = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.numNewVehicleID)).BeginInit();
            this.SuspendLayout();
            // 
            // numNewVehicleID
            // 
            this.numNewVehicleID.Location = new System.Drawing.Point(336, 32);
            this.numNewVehicleID.Maximum = new System.Decimal(new int[] {
																			65535,
																			0,
																			0,
																			0});
            this.numNewVehicleID.Minimum = new System.Decimal(new int[] {
																			1,
																			0,
																			0,
																			0});
            this.numNewVehicleID.Name = "numNewVehicleID";
            this.numNewVehicleID.Size = new System.Drawing.Size(56, 20);
            this.numNewVehicleID.TabIndex = 46;
            this.numNewVehicleID.Value = new System.Decimal(new int[] {
																		  1,
																		  0,
																		  0,
																		  0});
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            this.label9.Location = new System.Drawing.Point(8, 8);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 16);
            this.label9.TabIndex = 45;
            this.label9.Text = "Vehicle to Move";
            // 
            // chkMoveDriver
            // 
            this.chkMoveDriver.Location = new System.Drawing.Point(176, 256);
            this.chkMoveDriver.Name = "chkMoveDriver";
            this.chkMoveDriver.Size = new System.Drawing.Size(216, 16);
            this.chkMoveDriver.TabIndex = 44;
            this.chkMoveDriver.Text = "Move Driver Details?";
            // 
            // label8
            // 
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((System.Byte)(0)));
            this.label8.Location = new System.Drawing.Point(176, 8);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(152, 16);
            this.label8.TabIndex = 43;
            this.label8.Text = "New Vehicle Details";
            // 
            // label7
            // 
            this.label7.Location = new System.Drawing.Point(176, 208);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(128, 16);
            this.label7.TabIndex = 42;
            this.label7.Text = "Using Schedule Group";
            // 
            // cmbScheduleID
            // 
            this.cmbScheduleID.Location = new System.Drawing.Point(176, 224);
            this.cmbScheduleID.Name = "cmbScheduleID";
            this.cmbScheduleID.Size = new System.Drawing.Size(216, 21);
            this.cmbScheduleID.TabIndex = 41;
            // 
            // label6
            // 
            this.label6.Location = new System.Drawing.Point(176, 160);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(144, 16);
            this.label6.TabIndex = 40;
            this.label6.Text = "Using Route Group";
            // 
            // cmbRPIDs
            // 
            this.cmbRPIDs.Location = new System.Drawing.Point(176, 176);
            this.cmbRPIDs.Name = "cmbRPIDs";
            this.cmbRPIDs.Size = new System.Drawing.Size(216, 21);
            this.cmbRPIDs.TabIndex = 39;
            // 
            // label5
            // 
            this.label5.Location = new System.Drawing.Point(176, 112);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(160, 16);
            this.label5.TabIndex = 38;
            this.label5.Text = "Using Way Point Group";
            // 
            // cmbSPIDs
            // 
            this.cmbSPIDs.Location = new System.Drawing.Point(176, 128);
            this.cmbSPIDs.Name = "cmbSPIDs";
            this.cmbSPIDs.Size = new System.Drawing.Size(216, 21);
            this.cmbSPIDs.TabIndex = 37;
            // 
            // label4
            // 
            this.label4.Location = new System.Drawing.Point(176, 64);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(104, 16);
            this.label4.TabIndex = 36;
            this.label4.Text = "Using Configuration";
            // 
            // cmbConfigID
            // 
            this.cmbConfigID.Location = new System.Drawing.Point(176, 80);
            this.cmbConfigID.Name = "cmbConfigID";
            this.cmbConfigID.Size = new System.Drawing.Size(216, 21);
            this.cmbConfigID.TabIndex = 35;
            // 
            // label3
            // 
            this.label3.Location = new System.Drawing.Point(288, 40);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 16);
            this.label3.TabIndex = 34;
            this.label3.Text = "Vehicle";
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(176, 40);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 33;
            this.label2.Text = "To Fleet";
            // 
            // cmbToFleet
            // 
            this.cmbToFleet.Location = new System.Drawing.Point(232, 32);
            this.cmbToFleet.Name = "cmbToFleet";
            this.cmbToFleet.Size = new System.Drawing.Size(48, 21);
            this.cmbToFleet.TabIndex = 32;
            this.cmbToFleet.SelectedIndexChanged += new System.EventHandler(this.cmbToFleet_SelectedIndexChanged);
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 40);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(32, 16);
            this.label1.TabIndex = 30;
            this.label1.Text = "Fleet";
            // 
            // btnMoveUnit
            // 
            this.btnMoveUnit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveUnit.Location = new System.Drawing.Point(224, 448);
            this.btnMoveUnit.Name = "btnMoveUnit";
            this.btnMoveUnit.Size = new System.Drawing.Size(168, 24);
            this.btnMoveUnit.TabIndex = 29;
            this.btnMoveUnit.Text = "Listen for Unit Report";
            this.btnMoveUnit.Click += new System.EventHandler(this.btnMoveAllFromTo_Click);
            // 
            // listFromVehicles
            // 
            this.listFromVehicles.Location = new System.Drawing.Point(8, 64);
            this.listFromVehicles.Name = "listFromVehicles";
            this.listFromVehicles.ScrollAlwaysVisible = true;
            this.listFromVehicles.Size = new System.Drawing.Size(160, 238);
            this.listFromVehicles.TabIndex = 28;
            // 
            // cmbFromFleet
            // 
            this.cmbFromFleet.Location = new System.Drawing.Point(64, 32);
            this.cmbFromFleet.Name = "cmbFromFleet";
            this.cmbFromFleet.Size = new System.Drawing.Size(56, 21);
            this.cmbFromFleet.TabIndex = 27;
            this.cmbFromFleet.SelectedIndexChanged += new System.EventHandler(this.cmbFromFleet_SelectedIndexChanged);
            // 
            // txtStatus
            // 
            this.txtStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                | System.Windows.Forms.AnchorStyles.Left)));
            this.txtStatus.Location = new System.Drawing.Point(8, 312);
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Size = new System.Drawing.Size(384, 128);
            this.txtStatus.TabIndex = 47;
            this.txtStatus.Text = "";
            // 
            // btnRefresh
            // 
            this.btnRefresh.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnRefresh.Location = new System.Drawing.Point(48, 448);
            this.btnRefresh.Name = "btnRefresh";
            this.btnRefresh.Size = new System.Drawing.Size(168, 24);
            this.btnRefresh.TabIndex = 48;
            this.btnRefresh.Text = "Refresh";
            this.btnRefresh.Click += new System.EventHandler(this.btnRefresh_Click);
            // 
            // frmChangeUnitID
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(400, 474);
            this.Controls.Add(this.btnRefresh);
            this.Controls.Add(this.txtStatus);
            this.Controls.Add(this.numNewVehicleID);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.chkMoveDriver);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.cmbScheduleID);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.cmbRPIDs);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.cmbSPIDs);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.cmbConfigID);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbToFleet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btnMoveUnit);
            this.Controls.Add(this.listFromVehicles);
            this.Controls.Add(this.cmbFromFleet);
            this.Name = "frmChangeUnitID";
            this.Text = "Move Vehicle Form";
            this.Load += new System.EventHandler(this.frmChangeUnitID_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numNewVehicleID)).EndInit();
            this.ResumeLayout(false);

        }
        #endregion

        private void frmChangeUnitID_Load(object sender, System.EventArgs e)
        {
            try
            {
                RefreshLists();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmChangeUnitID_Load(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "frmChangeUnitID_Load(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void RefreshLists()
        {
            bRefreshingLists = true;
            try
            {
                if (LoadDataSets())
                {
                    listFromVehicles.Items.Clear();
                    cmbFromFleet.Items.Clear();
                    cmbToFleet.Items.Clear();
                    cmbConfigID.Items.Clear();
                    cmbSPIDs.Items.Clear();
                    cmbRPIDs.Items.Clear();
                    cmbScheduleID.Items.Clear();
                    for (int X = 0; X < oFleetDS.Tables[0].Rows.Count; X++)
                    {
                        string sFleetID = Convert.ToString(oFleetDS.Tables[0].Rows[X]["FleetID"]);
                        cmbToFleet.Items.Add(sFleetID);
                        cmbFromFleet.Items.Add(sFleetID);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "RefreshLists()", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "RefreshLists()\r\n\r\n Error Message : " + ex.Message, "Error");
            }
            bRefreshingLists = false;
        }

        public void AddPacketToCheckList(GatewayProtocolPacket packet)
        {
            try
            {
                if (tMatchUnit != null)
                {
                    if (tMatchUnit.Running)
                    {
                        lock (oUpdatePackets.SyncRoot)
                        {
                            oUpdatePackets.Add(packet);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
                throw new System.Exception(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
            }
        }

        private bool LoadDataSets()
        {
            bool bRet = false;
            string SQLCmd = "";
            string prpDSN = "";
            System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.SqlClient.SqlConnection objSQLConn = null;
            try
            {
                #region  Create the SQL objects
                prpDSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                objSQLConn = new System.Data.SqlClient.SqlConnection(prpDSN);
                objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                oFleetDS = new System.Data.DataSet();
                oFleetVehicleDS = new System.Data.DataSet();
                oFleetConfigDS = new System.Data.DataSet();
                oFleetSetPointDS = new System.Data.DataSet();
                oFleetRoutePointDS = new System.Data.DataSet();
                oFleetScheduleDS = new System.Data.DataSet();
                #endregion
                if (objSQLConn != null)
                {
                    // Connect to the SQL server
                    objSQLConn.Open();

                    #region Load the fleet list
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    SQLCmd = "SELECT Distinct gw.FleetID FROM T_GatewayUnit gw WHERE gw.FleetID > 0 AND gw.VehicleID > 0 AND (gw.IMIE <> '' OR gw.CDMAESN <> '' OR gw.SerialNumber <> '') ORDER BY gw.FleetID";
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(oFleetDS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    #endregion

                    #region Load the fleet to vehicle list
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    SQLCmd = "SELECT FleetID, VehicleID FROM T_GatewayUnit WHERE FleetID > 0 AND VehicleID > 0 AND (IMIE <> '' OR CDMAESN <> '' OR SerialNumber <> '') ORDER BY FleetID, VehicleID";
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(oFleetVehicleDS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    #endregion
                    #region Load the Fleet Config List
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    SQLCmd = "SELECT ID, FleetID, Name FROM T_Config WHERE FleetID in (SELECT Distinct FleetID FROM T_GatewayUnit WHERE FleetID > 0 AND VehicleID > 0 AND (IMIE <> '' OR CDMAESN <> '' OR SerialNumber <> '')) ORDER BY FleetID";
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(oFleetConfigDS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    #endregion
                    #region Load the Fleet Set Point List
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    SQLCmd = "SELECT ID, FleetID, Name FROM T_SetPointGroup WHERE FleetID in (SELECT Distinct FleetID FROM T_GatewayUnit WHERE FleetID > 0 AND VehicleID > 0 AND (IMIE <> '' OR CDMAESN <> '' OR SerialNumber <> '')) ORDER BY FleetID";
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(oFleetSetPointDS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    #endregion
                    #region Load the Fleet Route Point List
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    SQLCmd = "SELECT ID, FleetID, Name FROM T_RoutePointGroup WHERE FleetID in (SELECT Distinct FleetID FROM T_GatewayUnit WHERE FleetID > 0 AND VehicleID > 0 AND (IMIE <> '' OR CDMAESN <> '' OR SerialNumber <> '')) ORDER BY FleetID";
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(oFleetRoutePointDS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    #endregion
                    #region Load the Fleet Schedule List
                    // Create the SQL command.
                    objSQLCmd = objSQLConn.CreateCommand();
                    SQLCmd = "SELECT ID, FleetID, Name FROM T_ScheduleGroup WHERE FleetID in (SELECT Distinct FleetID FROM T_GatewayUnit WHERE FleetID > 0 AND VehicleID > 0 AND (IMIE <> '' OR CDMAESN <> '' OR SerialNumber <> '')) ORDER BY FleetID";
                    objSQLCmd.CommandText = SQLCmd;
                    objSQLCmd.CommandTimeout = 3600;
                    // Create the data adapter
                    objSQLDA.SelectCommand = objSQLCmd;
                    // Fill the DataSet
                    objSQLDA.Fill(oFleetScheduleDS);
                    // Clean up SQL objects
                    objSQLCmd.Dispose();
                    objSQLCmd = null;
                    #endregion
                }
                #region Clean up Objects
                if (objSQLDA != null) objSQLDA.Dispose();
                objSQLDA = null;
                if (objSQLConn != null)
                {
                    objSQLConn.Close();
                    objSQLConn.Dispose();
                }
                objSQLConn = null;
                #endregion
                bRet = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "LoadDataSets()", ex);
                throw new System.Exception(sClassName + "LoadDataSets()", ex);
            }
            return bRet;
        }

        private void cmbFromFleet_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            DataRow[] drVehicles = null;

            if (bRefreshingLists) return;
            try
            {
                listFromVehicles.Items.Clear();
                for (int X = listFromVehicles.Items.Count - 1; X >= 0; X--)
                {
                    listFromVehicles.Items.RemoveAt(X);
                }
                drVehicles = oFleetVehicleDS.Tables[0].Select("FleetID = " + cmbFromFleet.Text, "VehicleID");
                for (int X = 0; X < drVehicles.Length; X++)
                {
                    listFromVehicles.Items.Add(Convert.ToString(drVehicles[X]["VehicleID"]));
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "cmbFromFleet_SelectedIndexChanged(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "cmbFromFleet_SelectedIndexChanged(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void cmbToFleet_SelectedIndexChanged(object sender, System.EventArgs e)
        {
            if (bRefreshingLists) return;
            try
            {
                #region Clear the combo lists
                cmbConfigID.Items.Clear();
                cmbSPIDs.Items.Clear();
                cmbRPIDs.Items.Clear();
                cmbScheduleID.Items.Clear();
                #endregion
                #region Load the combo lists
                AddDataToCombo(oFleetConfigDS.Tables[0], cmbConfigID, "Name", "ID", cmbToFleet.Text, true);
                AddDataToCombo(oFleetSetPointDS.Tables[0], cmbSPIDs, "Name", "ID", cmbToFleet.Text, true);
                AddDataToCombo(oFleetRoutePointDS.Tables[0], cmbRPIDs, "Name", "ID", cmbToFleet.Text, true);
                AddDataToCombo(oFleetScheduleDS.Tables[0], cmbScheduleID, "Name", "ID", cmbToFleet.Text, true);
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "cmbToFleet_SelectedIndexChanged(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "cmbToFleet_SelectedIndexChanged(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void AddDataToCombo(DataTable oTable, ComboBox oCombo, string sNameField, string sValueField, string sFleetID, bool bAddNone)
        {
            ClassAddValueComboBox oItem = null;
            DataRow[] oRows = null;
            try
            {
                oCombo.Items.Clear();
                for (int X = oCombo.Items.Count - 1; X >= 0; X--)
                {
                    oCombo.Items.RemoveAt(X);
                }
                oRows = oTable.Select("FleetID = " + sFleetID, sNameField);
                if (bAddNone)
                {
                    oItem = new ClassAddValueComboBox(0, "None");
                    oCombo.Items.Add(oItem);
                }
                for (int X = 0; X < oRows.Length; X++)
                {
                    oItem = new ClassAddValueComboBox(Convert.ToInt32(oRows[X][sValueField]), Convert.ToString(oRows[X][sNameField]));
                    oCombo.Items.Add(oItem);
                }
                oCombo.DisplayMember = "Name";
                oCombo.ValueMember = "ID";
                if (oCombo.Items.Count > 0)
                    oCombo.SelectedIndex = 0;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "AddDataToCombo(DataTable oTable, ComboBox oCombo, string sNameField, string sValueField,  string sFleetID, bool bAddNone)", ex);
                MessageBox.Show(this, "Failed to load vehicle list for fleet " + cmbToFleet.Text + ".", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnMoveAllFromTo_Click(object sender, System.EventArgs e)
        {
            int iFleetID = 0;
            int iVehicle = 0;
            int iToFleetID = 0;
            int iToVehicle = 0;
            int iToVehicleConfigID = 0;
            int iToVehicleSetPointID = 0;
            int iToVehicleRoutePointID = 0;
            int iToVehicleScheduleID = 0;
            int iCounter = 0;
            DataRow[] dsVehicle = null;

            if (btnMoveUnit.Text == "Listen for Unit Report")
            {

                #region Get the from fleet ID
                try
                {
                    iFleetID = Convert.ToInt32(cmbFromFleet.Text);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                    return;
                }
                #endregion
                #region Get the from VehicleID
                try
                {
                    if (listFromVehicles.SelectedItem == null)
                    {
                        MessageBox.Show(this, "Please select a vehicle from the 'Vehicle to Move' vehicle list.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    iVehicle = Convert.ToInt32(listFromVehicles.SelectedItem);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                    return;
                }
                #endregion
                #region Check if the target already exists
                dsVehicle = oFleetVehicleDS.Tables[0].Select("FleetID = " + Convert.ToString(iToFleetID) + " AND VehicleID = " + Convert.ToString(iToVehicle));
                if (dsVehicle.Length > 0)
                {
                    MessageBox.Show(this, "Please select a different target fleet / vehicle.  The unit " + Convert.ToString(iToFleetID) + "/" + Convert.ToString(iToVehicle) + " already exists.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Get the to fleet ID
                try
                {
                    iToFleetID = Convert.ToInt32(cmbToFleet.Text);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Please select a fleet from the 'New Vehicle Details' fleet list.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Get the to Vehicle ID
                try
                {
                    iToVehicle = Convert.ToInt32(numNewVehicleID.Value);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Please select a valid 'To' vehicle ID.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (iToVehicle < 1 || iToVehicle > 65535)
                {
                    MessageBox.Show(this, "Please select a valid 'To' vehicle ID.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Get the to Vehicle Config ID
                try
                {
                    if (cmbConfigID.SelectedItem != null)
                    {
                        iToVehicleConfigID = ((ClassAddValueComboBox)cmbConfigID.SelectedItem).ID;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Please select a valid configuration from the 'New Vehicle Details' config list.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Get the to Vehicle Set Point Group ID
                try
                {
                    if (cmbSPIDs.SelectedItem != null)
                    {
                        iToVehicleSetPointID = ((ClassAddValueComboBox)cmbSPIDs.SelectedItem).ID;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Please select a valid set point group name from the 'New Vehicle Details' set point list.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Get the to Vehicle Route Point Group ID
                try
                {
                    if (cmbRPIDs.SelectedItem != null)
                    {
                        iToVehicleRoutePointID = ((ClassAddValueComboBox)cmbRPIDs.SelectedItem).ID;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Please select a valid route point group name from the 'New Vehicle Details' route point list.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Get the to VehicleSchedule Group ID
                try
                {
                    if (cmbScheduleID.SelectedItem != null)
                    {
                        iToVehicleScheduleID = ((ClassAddValueComboBox)cmbScheduleID.SelectedItem).ID;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Please select a valid schedule group name from the 'New Vehicle Details' schedule group list.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region  Create the SQL Update Command
                try
                {
                    VehicleUpdateSQLCmd = "EXEC SaveMoveFleetDS @FromFleet, @FromVehicle, @ToFleet, @ToVehicle, @NewConfig, @NewSP, @NewRP, @NewSched, @MoveDriver";
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@FromFleet", Convert.ToString(iFleetID));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@FromVehicle", Convert.ToString(iVehicle));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@ToFleet", Convert.ToString(iToFleetID));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@ToVehicle", Convert.ToString(iToVehicle));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@NewConfig", Convert.ToString(iToVehicleConfigID));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@NewSP", Convert.ToString(iToVehicleSetPointID));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@NewRP", Convert.ToString(iToVehicleRoutePointID));
                    VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@NewSched", Convert.ToString(iToVehicleScheduleID));
                    if (chkMoveDriver.Checked)
                        VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@MoveDriver", "1");
                    else
                        VehicleUpdateSQLCmd = VehicleUpdateSQLCmd.Replace("@MoveDriver", "0");
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Failed to create SQL update string..", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                #region Create the monitoring thread
                try
                {
                    sSendToKey = Convert.ToString(iFleetID).PadLeft(4, '0') + "~" + Convert.ToString(iVehicle).PadLeft(4, '0');
                    tMatchUnit = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(MatchUnitThread), null);
                    tMatchUnit.Start();
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show(this, "Failed to create monitoring thread.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                #endregion
                txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Started listening for a report from " + Convert.ToString(iFleetID) + "/" + Convert.ToString(iVehicle) + ".\n";
                btnRefresh.Enabled = false;
                btnMoveUnit.Text = "Stop Listening";
            }
            else
            {
                #region Stop the monitoring thread
                try
                {
                    if (tMatchUnit != null)
                    {
                        tMatchUnit.Stop();
                        while (tMatchUnit.Running && iCounter < 1000)
                        {
                            Thread.Sleep(100);
                        }
                        tMatchUnit = null;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnMoveAllFromTo_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "XXX\r\n\r\n Error Message : " + ex.Message, "Error");
                    return;
                }
                #endregion
                btnRefresh.Enabled = true;
                btnMoveUnit.Text = "Listen for Unit Report";
            }
        }

        private object MatchUnitThread(EnhancedThread sender, object data)
        {
            ArrayList oLocalList = null;
            string prpDSN = "";
            System.Data.SqlClient.SqlCommand objSQLCmd = null;
            System.Data.SqlClient.SqlConnection objSQLConn = null;
            try
            {
                prpDSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                objSQLConn = new System.Data.SqlClient.SqlConnection(prpDSN);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "MatchUnitThread(EnhancedThread sender, object data)", ex);
                sSendToKey = "";
                VehicleUpdateSQLCmd = "";
                sender.Stop();
                btnRefresh.Enabled = true;
                btnMoveUnit.Text = "Listen for Unit Report";
            }

            while (!sender.Stopping)
            {
                try
                {
                    oLocalList = new ArrayList();
                    #region Copy the items waiting to be processed into a local array.
                    lock (oUpdatePackets.SyncRoot)
                    {
                        for (int X = oUpdatePackets.Count - 1; X >= 0; X--)
                        {
                            oLocalList.Add(oUpdatePackets[X]);
                            oUpdatePackets.RemoveAt(X);
                        }
                    }
                    #endregion

                    if (oLocalList.Count > 0)
                    {
                        for (int X = 0; X < oLocalList.Count; X++)
                        {
                            GatewayProtocolPacket packet = (GatewayProtocolPacket)oLocalList[X];
                            // Only start a new download as a response to a status packet.
                            if (packet.cMsgType == (byte)0xC1)
                            {
                                byte[] bConvert = new byte[4];
                                bConvert[0] = packet.cFleetId;
                                string sFleetID = Convert.ToString(BitConverter.ToInt32(bConvert, 0));
                                string sVehicleID = Convert.ToString(Convert.ToInt32(packet.iVehicleId));
                                string sKey = sFleetID.PadLeft(4, '0') + "~" + sVehicleID.PadLeft(4, '0');
                                if (sKey == sSendToKey)
                                {
                                    txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Matched a report from " + sFleetID + "/" + sVehicleID + ".\n";
                                    if (objSQLConn != null)
                                    {
                                        // Connect to the SQL server
                                        objSQLConn.Open();
                                        objSQLCmd = objSQLConn.CreateCommand();
                                        objSQLCmd.CommandText = VehicleUpdateSQLCmd;
                                        objSQLCmd.CommandTimeout = 3600;
                                        objSQLCmd.ExecuteNonQuery();
                                        txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Database updated.\n";
                                        _listener.mDBInterface.RefreshVehicleData("Unit Moved Between Fleets");
                                        txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Listener Datasets Refreshed.\n";
                                        _listener.mDBInterface.SendResetRequest(packet);
                                        txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Sent Reset to unit.\n";
                                        sSendToKey = "";
                                        VehicleUpdateSQLCmd = "";
                                        btnRefresh.Enabled = true;
                                        btnMoveUnit.Text = "Listen for Unit Report";
                                        txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Process Complete.\n";
                                        sender.Stop();
                                        break;
                                    }
                                    else
                                    {
                                        txtStatus.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Database connection failed.  Update aborted.\n";
                                        sSendToKey = "";
                                        VehicleUpdateSQLCmd = "";
                                        btnRefresh.Enabled = true;
                                        btnMoveUnit.Text = "Listen for Unit Report";
                                        sender.Stop();
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "MatchUnitThread(EnhancedThread sender, object data)", ex);
                    sSendToKey = "";
                    VehicleUpdateSQLCmd = "";
                    btnRefresh.Enabled = true;
                    btnMoveUnit.Text = "Listen for Unit Report";
                    sender.Stop();
                }
            }
            try
            {
                if (objSQLConn != null)
                {
                    objSQLConn.Close();
                    objSQLConn.Dispose();
                }
            }
            catch (System.Exception)
            {
            }
            objSQLConn = null;
            return null;
        }

        private void btnRefresh_Click(object sender, System.EventArgs e)
        {
            try
            {
                bRefreshingLists = true;
                numNewVehicleID.Value = 0;
                cmbConfigID.Text = "";
                cmbConfigID.SelectedItem = null;
                cmbConfigID.Items.Clear();
                cmbFromFleet.Text = "";
                cmbFromFleet.SelectedItem = null;
                cmbFromFleet.Items.Clear();
                cmbRPIDs.Text = "";
                cmbRPIDs.SelectedItem = null;
                cmbRPIDs.Items.Clear();
                cmbScheduleID.Text = "";
                cmbScheduleID.SelectedItem = null;
                cmbScheduleID.Items.Clear();
                cmbSPIDs.Text = "";
                cmbSPIDs.SelectedItem = null;
                cmbSPIDs.Items.Clear();
                cmbToFleet.Text = "";
                cmbToFleet.SelectedItem = null;
                cmbToFleet.Items.Clear();
                RefreshLists();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnRefresh_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRefresh_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }
    }
}

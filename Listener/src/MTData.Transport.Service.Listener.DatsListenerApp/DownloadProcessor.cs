using System;
using System.Windows.Forms;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Service.Listener.GatewayListener.DatabaseAccess;
using MTData.Transport.Gateway.Packet;
using log4net;

namespace MTData.Transport.Listener.DatsListenerApp
{
	/// <summary>
	/// Summary description for DownloadProcessor.
	/// </summary>
	
	public enum DownloadType
	{
		TrackingDevice,
		InterfaceBox,
		MobileDataTerminal,
		BinaryFile
	};

	public class DownloadProcessor
	{
        private const string sClassName = "DATSListenerApp.DownloadProcessor.";
        private static ILog _log = LogManager.GetLogger(typeof(DownloadProcessor));

		public string sFleetID = "";
		public string sVehicleID = "";

		#region Events
		/// <summary>
		/// This delegate defined the eUpdateVehicleStatus event
		/// </summary>
		public delegate void UpdateVehicleStatusEvent(string sFleetID, string sVehicleID, string sStatus);
		/// <summary>
		/// This event is raised whenever the class has a message to pass back to the multidownload form
		/// </summary>
		public event UpdateVehicleStatusEvent eUpdateVehicleStatus;

		/// <summary>
		/// This delegate defined the eUpdateVehicleStatus event
		/// </summary>
		public delegate void VehicleDownloadCompleteEvent(string sFleetID, string sVehicleID);
		/// <summary>
		/// This event is raised whenever the class has a message to pass back to the multidownload form
		/// </summary>
		public event VehicleDownloadCompleteEvent eVehicleDownloadComplete;

		#endregion
		#region Variables
		private string _serverTime_DateFormat = "";
		public bool RequireUserToIssueCommands;
		public enum ProcessState : byte
		{
			Running,
			Failed,
			TimedOut,
			Successful
		};

		public ProcessState State;

		
		private DownloadType mDownloadType;
		public DownloadType DownloadType
		{
			get
			{
				return mDownloadType;
			}
		}
		private System.Timers.Timer mVerifyTimer;
		private GatewayProtocolPacket mPacket;
		private PacketDatabaseInterface mDatabase;
		private DownloadProgressForm mDlForm;
		private int iTotalBlockCount;
		private int iBlockCount;
		private byte cDownloadVersion;
		private MotorolaSRecordFile mMotFile;
		private cBinaryFile mBinFile = null;
		public MotorolaSRecordFile MotFile
		{
			set
			{
				mMotFile = value;
			}
			get
			{
				return mMotFile;
			}
		}
		private long lPreviousAddress, lThisAddress, lNextAddress;
		private int iPreferredDataLength = 250;
		private int iDownloadRetryIntervalSecs;
		private int iDownloadRetryMaxCount;
		public bool bUserWantsToContinue;
		

		// Things for the "verify" packet:
		private int iProgramChecksumTotal;
		private int iPacketChecksumTotal;
		private int iProgramBytesCountTotal;
		#endregion

		public delegate void StateChangedDelegate(ProcessState state);
		public event StateChangedDelegate StateChanged;

		public DownloadProcessor(GatewayProtocolPacket packet,
								PacketDatabaseInterface db,
								DownloadType type,
								string serverTime_DateFormat)
		{
			_serverTime_DateFormat = serverTime_DateFormat;
			try
			{
				RequireUserToIssueCommands = true;
				State = ProcessState.Running;
				mPacket = packet;
				mDatabase = db;
				// Set up what we can of the progress window:
				bUserWantsToContinue = true;
				mDlForm = new DownloadProgressForm(this);
				mDlForm.pbProgress.Step = 1;
				mDlForm.pbProgress.Minimum = 1;
				sFleetID = mPacket.cFleetId.ToString();
				sVehicleID = mPacket.iVehicleId.ToString();
				mDlForm.txtFleet.Text = mPacket.cFleetId.ToString();
				mDlForm.txtUnit.Text = mPacket.iVehicleId.ToString();
				this.mDownloadType = type;
				switch  (mDownloadType)
				{
					case DownloadType.InterfaceBox:
						mDlForm.Text = "IO Box Program Download";
						iPreferredDataLength = 256;
						break;
					case DownloadType.MobileDataTerminal:
						mDlForm.Text = "MDT Program Download";
						break;
					case DownloadType.BinaryFile:
						mDlForm.Text = "File Download";
						break;
					default:
						break;
				}
				iTotalBlockCount = 0;
				iBlockCount = 1;

				iProgramChecksumTotal = 0;
				iPacketChecksumTotal = 0;
				iProgramBytesCountTotal = 0;
				mMotFile = null;
				mBinFile = null;
				mVerifyTimer = new System.Timers.Timer();
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "DownloadProcessor(GatewayProtocolPacket packet, PacketDatabaseInterface db, DownloadType type, string serverTime_DateFormat)", ex);
			}
		}


		public void BeginDownload(	byte cDownloadVersion, 
			int iDownloadRetryMaxCount,
			int iDownloadRetryIntervalSecs,
			MotorolaSRecordFile oFile)
		{
			byte[] dataBytes = null;
			bool bContinue = true;
			try
			{
				mMotFile = oFile;
				this.cDownloadVersion = cDownloadVersion;
				this.iDownloadRetryMaxCount = iDownloadRetryMaxCount;
				this.iDownloadRetryIntervalSecs = iDownloadRetryIntervalSecs;
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, MotorolaSRecordFile oFile)", ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
					#region Measure how many packets we'll need to do the download
					// A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
					lThisAddress = mMotFile.GetAddressOfRow(2);
					lNextAddress = 0;
					dataBytes = new byte[0];
			
					while (mMotFile.GetContiguousByteArray(
						lThisAddress,
						iPreferredDataLength, 
						ref dataBytes,
						ref lNextAddress, true, false) == true)
					{
						// Create a count so we can do a pretty little progress bar!
						iTotalBlockCount++;
						// keep looping while there is data
						lThisAddress = lNextAddress;
					}			
					#endregion
				}
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, MotorolaSRecordFile oFile)", ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
					#region Break MOT file into packets of correct size (250B / 256B) and send the first
					lThisAddress = mMotFile.GetAddressOfRow(2);
					lNextAddress = 0;
					dataBytes = new byte[0];
					if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
					{					
						MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;
						switch (mDownloadType)
						{
							case DownloadType.TrackingDevice:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
								break;
							case DownloadType.InterfaceBox:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
								break;
							case DownloadType.MobileDataTerminal:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
								break;
						}
						pdp.cDownloadVersion = cDownloadVersion;
						pdp.iDownloadPacketNumber = iBlockCount;
						pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
						pdp.mDownloadData = dataBytes;
						// We can also do the program checksum for the Verify packet:
						iProgramChecksumTotal += SumOfBytes(dataBytes);
						// And count the program bytes:
						iProgramBytesCountTotal += dataBytes.Length;
						// Begin download and inform the packet ack system
						// that ContinueDownload wants to know when that first
						// download packet has been acked
						AcknowledgementNotificationDelegate del = null;
                        if (iTotalBlockCount > 1)
                        {
                            del = new AcknowledgementNotificationDelegate(ContinueDownloadNoForm);
                        }
                        else
                        {
                            del = new AcknowledgementNotificationDelegate(CompleteDownloadNoForm);
                        }
                        mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
						// Update the packet checksum total:
						iPacketChecksumTotal += pdp.iDownloadChecksum;
						// set up for ContinueDownload() to carry on...
						lThisAddress = lNextAddress;
						iBlockCount++;
					}

					#endregion
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, MotorolaSRecordFile oFile)", ex);
			}
		}
		/// <summary>
		/// Configures a download and sends the first packet
		/// </summary>
		/// <param name="iDownloadVersion"></param>
		public void BeginDownload(	byte cDownloadVersion, 
			int iDownloadRetryMaxCount,
			int iDownloadRetryIntervalSecs)
		{
			bool bContinue = true;
			OpenFileDialog imageFile = null;
			string sFileName = "";
			byte[] dataBytes = null;
			AcknowledgementNotificationDelegate del = null;

			mDlForm.sStatusBarText = "Status : Starting unit download..";
			mDlForm.Refresh();
			try
			{
				this.cDownloadVersion = cDownloadVersion;
				this.iDownloadRetryMaxCount = iDownloadRetryMaxCount;
				this.iDownloadRetryIntervalSecs = iDownloadRetryIntervalSecs;
			}
			catch(System.Exception ex)
			{
				_log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs)", ex);
				bContinue = false;
			}

			try
			{
				if(bContinue)
				{

					#region Stage 1 - Select file to read in
					imageFile = new OpenFileDialog();
					if (mMotFile == null)
					{
						switch  (mDownloadType)
						{
							case DownloadType.InterfaceBox:
							case DownloadType.MobileDataTerminal:
							case DownloadType.TrackingDevice:
								imageFile.CheckFileExists = true;
								imageFile.DefaultExt = ".mot";
								imageFile.Filter = "Motorola S-Record Files (*.mot)|*.mot";
								imageFile.Title = "Select Image File To Download";
								break;
							default:
								imageFile.CheckFileExists = true;
								imageFile.Filter = "All Files (*.*)|*.*";
								imageFile.Title = "Select File To Download";
								break;
						}

						if (imageFile.ShowDialog() == DialogResult.Cancel) return;
					}
					#endregion
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs)", ex);
				bContinue = false;
			}

			try
			{
				if(bContinue)
				{
					#region Stage 2 - Read in and verify the MOT file
					switch  (mDownloadType)
					{
						case DownloadType.InterfaceBox:
						case DownloadType.MobileDataTerminal:
						case DownloadType.TrackingDevice:
							if (mMotFile == null) mMotFile = new MotorolaSRecordFile(imageFile.FileName);
							if (!mMotFile.FileIsGood) 
							{
								_log.Info("File Downloader : File Open Failed. - " + mMotFile.sFileFailureReason);
								MessageBox.Show(mMotFile.sFileFailureReason,
									"File Open Failed",
									MessageBoxButtons.OK,
									MessageBoxIcon.Error);
								UpdateState(ProcessState.Failed);
								return;
							}
							break;
						case DownloadType.BinaryFile:
							if(mBinFile == null) mBinFile = new cBinaryFile(imageFile.FileName, ProgramDownloadPacket.DOWNLOAD_FILE_DATA_LENGTH);
							if(mBinFile.sError != "")
							{
								_log.Info("File Downloader : " + mBinFile.sError);
								bContinue = false;
							}
							break;
						default:
							break;
					}
					#endregion
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs)", ex);
				bContinue = false;
			}

			try
			{
				if(bContinue)
				{
					#region Stage 3 - Measure how many packets we'll need to do the download
					switch  (mDownloadType)
					{
						case DownloadType.InterfaceBox:
						case DownloadType.MobileDataTerminal:
						case DownloadType.TrackingDevice:
							// A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
							lThisAddress = mMotFile.GetAddressOfRow(2);
							lNextAddress = 0;
							dataBytes = new byte[0];
			
							while (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
							{
								// Create a count so we can do a pretty little progress bar!
								iTotalBlockCount++;
								// keep looping while there is data
								lThisAddress = lNextAddress;
							}	
							sFileName = mMotFile.FileName;
							break;
						case DownloadType.BinaryFile:
							sFileName = imageFile.FileName;
							iTotalBlockCount = mBinFile.TotalBlockCount;
							break;
						default:
							// ?
							break;
					}

					mDlForm.pbProgress.Maximum = iTotalBlockCount;
					mDlForm.txtPacketTotal.Text = iTotalBlockCount.ToString();
					mDlForm.txtSource.Text = sFileName;
					mDlForm.Show();	
					#endregion
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs)", ex);
				bContinue = false;
			}

			try
			{
				if(bContinue)
				{
					#region Stage 4 - Break MOT file into packets of correct size (250B / 256B) and send the first
					MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;			
					switch  (mDownloadType)
					{
						case DownloadType.InterfaceBox:
						case DownloadType.MobileDataTerminal:
						case DownloadType.TrackingDevice:
							// A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
							lThisAddress = mMotFile.GetAddressOfRow(2);
							lNextAddress = 0;
							dataBytes = new byte[0];
							if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
							{
								if (mDownloadType == DownloadType.TrackingDevice)
								{
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
								}
								else if(mDownloadType == DownloadType.InterfaceBox)
								{
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
								}
								else if(mDownloadType == DownloadType.MobileDataTerminal)
								{
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
								}
							}
							break;
						case DownloadType.BinaryFile:
							lThisAddress = 0;
							lNextAddress = 0;
							dataBytes = mBinFile.NextBlock;
							pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
							//pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket);
							sFileName = imageFile.FileName;
							break;
						default:
							// ?
							break;
					}
					
					#region Wrap this chunk and send it:				
					// Update progress meter:				
					mDlForm.txtPacketCount.Text = iBlockCount.ToString();
					mDlForm.pbProgress.PerformStep();

					// Update the download packet.
					pdp.cDownloadVersion = cDownloadVersion;
					pdp.iDownloadPacketNumber = iBlockCount;
					pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
					pdp.mDownloadData = dataBytes;

					// We can also do the program checksum for the Verify packet:
					iProgramChecksumTotal += SumOfBytes(dataBytes);
					// And count the program bytes:
					iProgramBytesCountTotal += dataBytes.Length;
				
					// Begin download and inform the packet ack system
					// that ContinueDownload wants to know when that first
					// download packet has been acked
					del = new AcknowledgementNotificationDelegate(ContinueDownload);
					mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);				
					#endregion
					// Update the packet checksum total:
					iPacketChecksumTotal += pdp.iDownloadChecksum;
					// set up for ContinueDownload() to carry on...
					lThisAddress = lNextAddress;
					iBlockCount++;
				}

				#endregion
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "BeginDownload(	byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs)", ex);
				bContinue = false;
			}
		}
        
        public void BeginDownload(byte cDownloadVersion,
            int iDownloadRetryMaxCount,
            int iDownloadRetryIntervalSecs,
            string fileName)
        {
            byte[] dataBytes = null;
            bool bContinue = true;
            try
            {
                this.cDownloadVersion = cDownloadVersion;
                this.iDownloadRetryMaxCount = iDownloadRetryMaxCount;
                this.iDownloadRetryIntervalSecs = iDownloadRetryIntervalSecs;

                if (mBinFile == null) mBinFile = new cBinaryFile(fileName, ProgramDownloadPacket.DOWNLOAD_FILE_DATA_LENGTH);
                if (mBinFile.sError != "")
                {
                    _log.Info("File Downloader : " + mBinFile.sError);
                    bContinue = false;
                }
                iTotalBlockCount = mBinFile.TotalBlockCount;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "BeginDownload(byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, string fileName)", ex);
                bContinue = false;
            }

            try
            {
                if (bContinue)
                {
                    #region Break MOT file into packets of correct size (250B / 256B) and send the first
                    lThisAddress = 0;
                    lNextAddress = 0;
                    dataBytes = new byte[0];

                    MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
                    dataBytes = mBinFile.NextBlock;
                    pdp.cDownloadVersion = cDownloadVersion;
                    pdp.iDownloadPacketNumber = iBlockCount;
                    pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
                    pdp.mDownloadData = dataBytes;
                    // We can also do the program checksum for the Verify packet:
                    iProgramChecksumTotal += SumOfBytes(dataBytes);
                    // And count the program bytes:
                    iProgramBytesCountTotal += dataBytes.Length;
                    // Begin download and inform the packet ack system
                    // that ContinueDownload wants to know when that first
                    // download packet has been acked
                    AcknowledgementNotificationDelegate del = null;
                    if (iTotalBlockCount > 1)
                    {
                        del = new AcknowledgementNotificationDelegate(ContinueDownloadNoForm);
                    }
                    else
                    {
                        del = new AcknowledgementNotificationDelegate(CompleteDownloadNoForm);
                    }
                    mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
                    // Update the packet checksum total:
                    iPacketChecksumTotal += pdp.iDownloadChecksum;
                    // set up for ContinueDownload() to carry on...
                    lThisAddress = lNextAddress;
                    iBlockCount++;


                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "BeginDownload(byte cDownloadVersion, int iDownloadRetryMaxCount, int iDownloadRetryIntervalSecs, string fileName)", ex);
            }
        }

		private void UpdateState(ProcessState state)
		{
			try
			{
				State = state;
				if (StateChanged != null)
				{
					StateChanged(State);
				}
				if (state == ProcessState.Failed || state == ProcessState.TimedOut) 
					mDatabase.SendFlushRequest(mPacket);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "UpdateState(ProcessState state)", ex);
			}
		}

		private static int SumOfBytes(byte[] array)
		{
			int total = 0;

			try
			{
				foreach (byte b in array)
				{
					total += Convert.ToInt32(b);
				}
			}
			catch(System.Exception ex)
			{
				Console.Write(ex.Message);
			}
			return total;
		}

		private void ContinueDownloadNoForm(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			bool bFirstTimeForThisPacket = true;
			AcknowledgementNotificationDelegate del;
			byte[] dataBytes = new byte[0];

			if (!bUserWantsToContinue)
				return;

			try
			{
				if (!bLastPacketWasGood)
				{
					if(eUpdateVehicleStatus != null)
						eUpdateVehicleStatus(sFleetID, sVehicleID, "The packet " + Convert.ToString(iBlockCount) + " was not recieved by the unit.");
					mDatabase.AbortProgramDownload();
					UpdateState(ProcessState.Failed);
					return;
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ContinueDownloadNoForm(bool bLastPacketWasGood)", ex);
				bContinue = false;
			}

			try
			{
				if (bContinue)
				{
					if (eUpdateVehicleStatus != null)
						eUpdateVehicleStatus(sFleetID, sVehicleID, "Downloading packet " + Convert.ToString(iBlockCount) + " of " + Convert.ToString(iTotalBlockCount));

                    if (mDownloadType == DownloadType.BinaryFile)
                    {
                        MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
                        dataBytes = mBinFile.NextBlock;
                        pdp.cDownloadVersion = cDownloadVersion;
                        pdp.iDownloadPacketNumber = iBlockCount;
                        pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
                        pdp.mDownloadData = dataBytes;
                        if (bFirstTimeForThisPacket)
                        {
                            iProgramChecksumTotal += SumOfBytes(dataBytes);
                            iProgramBytesCountTotal += dataBytes.Length;
                        }
                        if (iBlockCount != iTotalBlockCount)
                        {
                            del = new AcknowledgementNotificationDelegate(ContinueDownloadNoForm);
                        }
                        else
                        {
                            del = new AcknowledgementNotificationDelegate(CompleteDownloadNoForm);
                        }
                        mDatabase.SendProgramDownload(pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
                        iPacketChecksumTotal += pdp.iDownloadChecksum;
                        iBlockCount++;
                    }
                    else if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, (iBlockCount == iTotalBlockCount))	== true)
					{
						MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;				
						switch (mDownloadType)
						{
							case DownloadType.TrackingDevice:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
								break;
							case DownloadType.InterfaceBox:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
								break;
							case DownloadType.MobileDataTerminal:
								pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
								break;
                        }
						pdp.cDownloadVersion = cDownloadVersion;
						pdp.iDownloadPacketNumber = iBlockCount;		
						pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
						if (iBlockCount == iTotalBlockCount) pdp.iDownloadAddress = Convert.ToInt32(lNextAddress & 0xFFFFFF);
						pdp.mDownloadData = dataBytes;
						if (bFirstTimeForThisPacket)
						{
							iProgramChecksumTotal += SumOfBytes(dataBytes);
							iProgramBytesCountTotal += dataBytes.Length;
						}
						if (iBlockCount != iTotalBlockCount) 
						{
							del = new AcknowledgementNotificationDelegate(ContinueDownloadNoForm);
						}
						else
						{
							del = new AcknowledgementNotificationDelegate(CompleteDownloadNoForm);
						}
						mDatabase.SendProgramDownload(	pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
						iPacketChecksumTotal += pdp.iDownloadChecksum;
						lPreviousAddress = lThisAddress;
						lThisAddress = lNextAddress;
						iBlockCount++;
					}
					else
					{
						if(eUpdateVehicleStatus != null)
							eUpdateVehicleStatus(sFleetID, sVehicleID, "Failed to read block " + Convert.ToString(iBlockCount) + " fom the MOT file.  Please check this file is not corrupt.");
						mDatabase.AbortProgramDownload();
						UpdateState(ProcessState.Failed);
						return;
					}			
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "ContinueDownloadNoForm(bool bLastPacketWasGood)", ex);
				bContinue = false;
			}
		}

		private void CompleteDownloadNoForm(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			AcknowledgementNotificationDelegate del = null;

			if (!bUserWantsToContinue)
				return;

			if (!bLastPacketWasGood)
			{
				try
				{
					// Last packet was not ACKed!
					if(eUpdateVehicleStatus != null)
						eUpdateVehicleStatus(sFleetID, sVehicleID, "The packet " + Convert.ToString(iBlockCount) + " was not recieved by the unit.");
					mDatabase.AbortProgramDownload();
					UpdateState(ProcessState.Failed);
					return;
				}
				catch(System.Exception ex)
				{
                    _log.Error(sClassName + "CompleteDownloadNoForm(bool bLastPacketWasGood)", ex);
					bContinue = false;
				}
			}
			try
			{
				if (bContinue)
				{
					if(eUpdateVehicleStatus != null)
						eUpdateVehicleStatus(sFleetID, sVehicleID, "Sending verify packet to unit.");
					ProgramVerifyPacket pvp = new ProgramVerifyPacket(mPacket, _serverTime_DateFormat);
					switch (mDownloadType)
					{
						case DownloadType.TrackingDevice:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
							break;
						case DownloadType.InterfaceBox:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_IOBOX_VERIFY;
							break;
						case DownloadType.MobileDataTerminal:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_MDT_VERIFY;
							break;
                        case DownloadType.BinaryFile:
                            pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
                            break;
                        default:
							break;
					}
					pvp.cDownloadVersion = cDownloadVersion;
					pvp.iProgramChecksumTotal = iProgramChecksumTotal;
					pvp.iPacketChecksumTotal = iPacketChecksumTotal;
					pvp.iPacketCountTotal = iTotalBlockCount;
					pvp.iProgramBytesCountTotal = iProgramBytesCountTotal;
					// The next ACK indicates a successful verify!
					del = new AcknowledgementNotificationDelegate(VerifyCompleteNoForm);
					mDatabase.SendVerifyDownload(	pvp, del,	iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
					mVerifyTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.VerifyTimerElapsedNoForm);
					mVerifyTimer.Interval = 20 * 1000; // 20 seconds
					mVerifyTimer.AutoReset = false;
					mVerifyTimer.Start();
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "CompleteDownloadNoForm(bool bLastPacketWasGood)", ex);
				bContinue = false;
			}
		}

		private void VerifyCompleteNoForm(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			if (!bUserWantsToContinue)
				return;

			if (!bLastPacketWasGood)
			{
				try
				{
					mVerifyTimer.Stop();
					mVerifyTimer.Elapsed -= new System.Timers.ElapsedEventHandler(VerifyTimerElapsedNoForm);
					mVerifyTimer = null;
				}
				catch(System.Exception ex)
				{
					bContinue = false;
                    _log.Error(sClassName + "VerifyCompleteNoForm(bool bLastPacketWasGood)", ex);
				}
				try
				{
					if(bContinue)
					{
						if(eUpdateVehicleStatus != null)
							eUpdateVehicleStatus(sFleetID, sVehicleID, "Unit did not verify the download correctly.");
						mDatabase.AbortProgramDownload();
						UpdateState(ProcessState.Failed);
						if (eVehicleDownloadComplete != null)
							eVehicleDownloadComplete(sFleetID, sVehicleID);
						return;
					}
				}
				catch(System.Exception ex)
				{
					bContinue = false;
                    _log.Error(sClassName + "VerifyCompleteNoForm(bool bLastPacketWasGood)", ex);
				}
			}
			try
			{
				if(bContinue)
				{
					UpdateState(ProcessState.Successful);

					if(eUpdateVehicleStatus != null)
						eUpdateVehicleStatus(sFleetID, sVehicleID, "Download Complete.");

					if(eVehicleDownloadComplete != null)
						eVehicleDownloadComplete(sFleetID, sVehicleID);
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "VerifyCompleteNoForm(bool bLastPacketWasGood)", ex);
			}
		}

		private void VerifyTimerElapsedNoForm(object o, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				mVerifyTimer.Stop();
				// Call the VerifyComplete function as if we got an ACK!
				VerifyCompleteNoForm(true);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "VerifyTimerElapsedNoForm(object o, System.Timers.ElapsedEventArgs e)", ex);
			}
		}


		// This function is called when the previous packet
		// has been acknowledged and the far end is ready 
		// to send the next one.
		private void ContinueDownload(bool bLastPacketWasGood)
		{
			try
			{
				mDlForm.Invoke(new AcknowledgementNotificationDelegate(SafeContinueDownload), new object[]{bLastPacketWasGood});
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "VerifyTimerElapsedNoForm(object o, System.Timers.ElapsedEventArgs e)", ex);
			}
		}

		/// <summary>
		/// This method will be called in the GUI thread.
		/// </summary>
		/// <param name="bLastPacketWasGood"></param>
		private void SafeContinueDownload(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			bool bFirstTimeForThisPacket = true;
			AcknowledgementNotificationDelegate del = null;
			MTData.Transport.Gateway.Packet.ProgramDownloadPacket pdp = null;
			byte[] dataBytes = new byte[0];

			try
			{
				if (!bUserWantsToContinue)
				{
					mDlForm.sStatusBarText = "Status : Download Aborted.";
					mDlForm.Refresh();
					mDatabase.AbortProgramDownload();
					State = ProcessState.Failed;
					return;
				}

				if (!bLastPacketWasGood)
				{
					mDlForm.sStatusBarText = "Error : Unit did not ack the last download packet.";
					mDlForm.Refresh();
					mDatabase.AbortProgramDownload();
					UpdateState(ProcessState.Failed);
					return;
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
                _log.Error(sClassName + "SafeContinueDownload(bool bLastPacketWasGood)", ex);
			}

			try
			{
				if (bContinue)
				{
					#region Download next block
					mDlForm.sStatusBarText = "Status : Sending next download packet.";
					mDlForm.Refresh();
					if (mDownloadType == DownloadType.TrackingDevice || mDownloadType ==  DownloadType.InterfaceBox  || mDownloadType == DownloadType.MobileDataTerminal)
					{
						if (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, (iBlockCount == iTotalBlockCount)) == true)
						{
							switch (mDownloadType)
							{
								case DownloadType.TrackingDevice:
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_PROGRAM, mPacket, _serverTime_DateFormat);
									break;
								case DownloadType.InterfaceBox:
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_IOBOX, mPacket, _serverTime_DateFormat);
									break;
								case DownloadType.MobileDataTerminal:
									pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_MDT, mPacket, _serverTime_DateFormat);
									break;
							}
						}
					}
					else
					{
						pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket, _serverTime_DateFormat);
						//pdp = new ProgramDownloadPacket(ProgramDownloadPacket.DOWNLOAD_FILE, mPacket);
						dataBytes = mBinFile.NextBlock;
					}
					pdp.cDownloadVersion = cDownloadVersion;
					pdp.iDownloadPacketNumber = iBlockCount;		
					pdp.iDownloadAddress = Convert.ToInt32(lThisAddress & 0xFFFFFF);
					// For the last page, use the front-padded version:
					if (iBlockCount == iTotalBlockCount) pdp.iDownloadAddress = Convert.ToInt32(lNextAddress & 0xFFFFFF);

					pdp.mDownloadData = dataBytes;

					if (bFirstTimeForThisPacket)
					{
						// We can also do the program checksum for the Verify packet:
						iProgramChecksumTotal += SumOfBytes(dataBytes);
						// And count the program bytes:
						iProgramBytesCountTotal += dataBytes.Length;
					}

					// Begin download and inform the packet ack system
					// that ContinueDownload wants to know when that first
					// download packet has been acked
					if (iBlockCount != iTotalBlockCount) 
					{
						del = new AcknowledgementNotificationDelegate(ContinueDownload);
					}
					else
					{
						// Last packet is about to go!
						// Change the delegate to CompleteDownload to finish
						del = new AcknowledgementNotificationDelegate(CompleteDownload);
					}
					mDatabase.SendProgramDownload(	pdp, del, iDownloadRetryMaxCount, iDownloadRetryIntervalSecs);
					// Update the packet checksum total:
					iPacketChecksumTotal += pdp.iDownloadChecksum;
					// set up for ContinueDownload() to carry on...
					lPreviousAddress = lThisAddress;
					lThisAddress = lNextAddress;
					// Update progress meter:				
					mDlForm.txtPacketCount.Text = iBlockCount.ToString();
					mDlForm.pbProgress.PerformStep();
					// Move to the next block ID.
					iBlockCount++;
					#endregion
				}
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "SafeContinueDownload(bool bLastPacketWasGood)", ex);
			}
		}

		public void AbortDownload()
		{
			try
			{
				bUserWantsToContinue = false;
				if (mDlForm != null)
					mDlForm.Close();
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "AbortDownload()", ex);
			}
		}

		private void CompleteDownload(bool bLastPacketWasGood)
		{
			try
			{
				mDlForm.Invoke(new AcknowledgementNotificationDelegate(SafeCompleteDownload), new object[]{bLastPacketWasGood});
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "CompleteDownload(bool bLastPacketWasGood)", ex);
			}
		}

		// This function is called when the last packet of the 
		// program download has been acknowledged, and all that
		// remains is to send a "Verify" packet which will
		// cause the remote device to verify the download, transfer
		// the program into the execution area and reboot!
		private void SafeCompleteDownload(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			ProgramVerifyPacket pvp = null;
			AcknowledgementNotificationDelegate del = null;

			try
			{
				if (!bLastPacketWasGood)
				{
					if (mDownloadType == DownloadType.MobileDataTerminal)
					{	// We can tolerate a few errors in an MDT download,
						// because the IrDA channel is error-prone:
						SafeContinueDownload(false);
						return;
					}
					else
					{
						mDlForm.sStatusBarText = "Error : Unit did not ack the last download packet.";
						mDlForm.Refresh();
						// Last packet was not ACKed!
//						mDlForm.Close();
						mDatabase.AbortProgramDownload();
						UpdateState(ProcessState.Failed);
						return;
					}
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
                _log.Error(sClassName + "SafeCompleteDownload(bool bLastPacketWasGood)", ex);
			}

			try
			{
				if (bContinue)
				{
					// Modify the download window's button text:
					mDlForm.btnAbort.Text = "Close";
					//	POD - This is rediculous.. The download was successful..
					//	allow it to automatically verify the packet, and continue
					mDlForm.sStatusBarText = "Status : Download Complete - Sending verify packet to the unit.";
					mDlForm.Refresh();
					// Create a "Verify" packet:
					pvp = new ProgramVerifyPacket(mPacket, _serverTime_DateFormat);
					switch (mDownloadType)
					{
						case DownloadType.TrackingDevice:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_VERIFY;
							break;
						case DownloadType.InterfaceBox:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_IOBOX_VERIFY;
							break;
						case DownloadType.MobileDataTerminal:
						case DownloadType.BinaryFile:
							pvp.cMsgType = ProgramVerifyPacket.DOWNLOAD_MDT_VERIFY;
							break;
						default:
							break;
					}
					pvp.cDownloadVersion = cDownloadVersion;
					pvp.iProgramChecksumTotal = iProgramChecksumTotal;
					pvp.iPacketChecksumTotal = iPacketChecksumTotal;
					pvp.iPacketCountTotal = iTotalBlockCount;
					pvp.iProgramBytesCountTotal = iProgramBytesCountTotal;

					_log.Info("Sending Verify Packet : Program Checksum = " + iProgramChecksumTotal + ", Packet Checksum : " + iPacketChecksumTotal + ", Packet Count : " + iTotalBlockCount + ", Program Bytes Count : " + iProgramBytesCountTotal);
					// The next ACK indicates a successful verify!
					del = new AcknowledgementNotificationDelegate(VerifyComplete);
					mDatabase.SendVerifyDownload(	pvp, del, 2, 10);
					// 302x devices don't ack, so if we go 20 seconds without hearing
					// anything, assume success!
					if ((!RequireUserToIssueCommands) && (mDownloadType == DownloadType.TrackingDevice))
					{
						mVerifyTimer.Elapsed += new System.Timers.ElapsedEventHandler(this.VerifyTimerElapsed);
						mVerifyTimer.Interval = 20 * 1000; // 20 seconds
						mVerifyTimer.AutoReset = false;
						mVerifyTimer.Start();
					}
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
                _log.Error(sClassName + "SafeCompleteDownload(bool bLastPacketWasGood)", ex);
			}
		}

		private void VerifyTimerElapsed(object o, System.Timers.ElapsedEventArgs e)
		{
			try
			{
				mVerifyTimer.Stop();
				// Call the VerifyComplete function as if we got an ACK!
				VerifyComplete(true);
			}
			catch(System.Exception ex)
			{
                _log.Error(sClassName + "VerifyTimerElapsed(object o, System.Timers.ElapsedEventArgs e)", ex);
			}
		}

		private void VerifyComplete(bool bLastPacketWasGood)
		{
			bool bContinue = true;
			try
			{
				if (!bLastPacketWasGood)
				{					
					mDlForm.sStatusBarText = "Error : The unit did not  verify the download.";
					mDlForm.Refresh();
					mDatabase.AbortProgramDownload();
					UpdateState(ProcessState.Failed);
					return;
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
                _log.Error(sClassName + "VerifyComplete(bool bLastPacketWasGood)", ex);
			}

			try
			{
				if(bContinue)
				{
					if (RequireUserToIssueCommands)
					{
						mDlForm.sStatusBarText = "Status : Download Complete - The unit verified the download.";
						mDlForm.Refresh();
						mDatabase.AbortProgramDownload();
					}
					UpdateState(ProcessState.Successful);
				}
			}
			catch(System.Exception ex)
			{
				bContinue = false;
                _log.Error(sClassName + "VerifyComplete(bool bLastPacketWasGood)", ex);
			}
		}

	}
}

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using System.Threading;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using MTData.Transport.Service.Listener.GatewayListener;
using MTData.Transport.Gateway.Packet;
using log4net;
using MTData.Common.Threading;
using MTData.Common.Utilities;

namespace MTData.Transport.Listener.DatsListenerApp
{
    /// <summary>
    /// Summary description for frmMultipleDownload.
    /// </summary>
    public class frmMultipleDownload : System.Windows.Forms.Form
    {
        private static ILog _log = LogManager.GetLogger(typeof(frmMultipleDownload));
        private const string sClassName = "DATSListenerApp.frmMultipleDownload.";
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.ListView lvAvailableUnits;
        private System.Windows.Forms.Button btnAddSelected;
        private System.Windows.Forms.Button btnAddAll;
        private System.Windows.Forms.Button btnRemoveAll;
        private System.Windows.Forms.Button btnRemoveSelected;
        private System.Windows.Forms.ListView lvQueued;
        private System.Windows.Forms.Button btnRefershAvaliableList;
        private System.Windows.Forms.Button btnSendToUnits;
        private System.Windows.Forms.Button btnAbort;
        private System.Windows.Forms.RadioButton rb302x;
        private System.Windows.Forms.RadioButton rb1035;
        private System.Windows.Forms.RadioButton rbMDT;
        private System.Windows.Forms.RichTextBox rtbConsole;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.NumericUpDown numTimeoutDownloadMins;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.NumericUpDown numSimultaneousDownloads;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Button btnSelectMotFile;
        private System.Windows.Forms.TextBox txtFilename;
        private System.Windows.Forms.TextBox txtEmailTo;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox txtSMTPServer;
        private System.Windows.Forms.Button btnMoveToNext;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.CheckBox chkContinueWithoutWaiting;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;
        #region Private Vars
        private MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator _listener = null;
        private MotorolaSRecordFile mMotFile = null;
        private EnhancedThread tFindUnits = null;
        private ArrayList oUpdatePackets = null;
        private Hashtable oDownloadToList = null;
        private Hashtable oWaitForLoginList = null;
        private Hashtable oRetryCounts = null;
        private int iRetryCount = 0;
        private int iRetryInterval = 0;
        private object SyncRoot = new object();
        private int iDownloadCount = 0;
        private RadioButton rb4000;
        private string _serverTime_DateFormat = "";
        #endregion
        #region Load and Dispose Events
        public frmMultipleDownload(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener, string serverTime_DateFormat)
        {
            _serverTime_DateFormat = serverTime_DateFormat;
            iDownloadCount = 0;
            InitializeComponent();
            try
            {
                _listener = oListener;
                oUpdatePackets = ArrayList.Synchronized(new ArrayList());
                oDownloadToList = Hashtable.Synchronized(new Hashtable());
                oWaitForLoginList = Hashtable.Synchronized(new Hashtable());
                oRetryCounts = Hashtable.Synchronized(new Hashtable());
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmMultipleDownload(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener, string serverTime_DateFormat)", ex);
                throw new System.Exception(sClassName + "frmMultipleDownload(MTData.Transport.Service.Listener.GatewayListener.GatewayAdministrator oListener, string serverTime_DateFormat)", ex);
            }
        }

        private void frmMultipleDownload_Load(object sender, System.EventArgs e)
        {
            try
            {
                LoadListView();
                txtEmailTo.Text = ConfigurationManager.AppSettings["EMailTargetAddress"];
                txtSMTPServer.Text = ConfigurationManager.AppSettings["EMailSMTPServer"];
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "frmMultipleDownload_Load(object sender, System.EventArgs e)", ex);
                throw new System.Exception(sClassName + "frmMultipleDownload_Load(object sender, System.EventArgs e)", ex);
            }
        }
        protected override void Dispose(bool disposing)
        {
            try
            {
                if (tFindUnits != null)
                {
                    if (tFindUnits.Running)
                        tFindUnits.Stop();
                }
                tFindUnits = null;
            }
            catch (System.Exception)
            {
            }
            try
            {
                StopDownloadThreads();
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (oDownloadToList != null)
                    lock (oDownloadToList.SyncRoot)
                        oDownloadToList.Clear();
                oDownloadToList = null;
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (oWaitForLoginList != null)
                    lock (oWaitForLoginList.SyncRoot)
                        oWaitForLoginList.Clear();
                oWaitForLoginList = null;
            }
            catch (System.Exception)
            {
            }
            try
            {
                if (oUpdatePackets != null)
                {
                    lock (oUpdatePackets.SyncRoot)
                        oUpdatePackets.Clear();
                }
                oUpdatePackets = null;
            }
            catch (System.Exception)
            {
            }

            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #endregion
        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lvAvailableUnits = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader7 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader6 = new System.Windows.Forms.ColumnHeader();
            this.label1 = new System.Windows.Forms.Label();
            this.btnAddSelected = new System.Windows.Forms.Button();
            this.btnAddAll = new System.Windows.Forms.Button();
            this.btnRemoveAll = new System.Windows.Forms.Button();
            this.btnRemoveSelected = new System.Windows.Forms.Button();
            this.lvQueued = new System.Windows.Forms.ListView();
            this.columnHeader3 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader4 = new System.Windows.Forms.ColumnHeader();
            this.columnHeader5 = new System.Windows.Forms.ColumnHeader();
            this.label2 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.rbMDT = new System.Windows.Forms.RadioButton();
            this.rb1035 = new System.Windows.Forms.RadioButton();
            this.rb302x = new System.Windows.Forms.RadioButton();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFilename = new System.Windows.Forms.TextBox();
            this.btnSelectMotFile = new System.Windows.Forms.Button();
            this.btnSendToUnits = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.numTimeoutDownloadMins = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.btnAbort = new System.Windows.Forms.Button();
            this.btnRefershAvaliableList = new System.Windows.Forms.Button();
            this.rtbConsole = new System.Windows.Forms.RichTextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.txtEmailTo = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.numSimultaneousDownloads = new System.Windows.Forms.NumericUpDown();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSMTPServer = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.btnMoveToNext = new System.Windows.Forms.Button();
            this.chkContinueWithoutWaiting = new System.Windows.Forms.CheckBox();
            this.rb4000 = new System.Windows.Forms.RadioButton();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numTimeoutDownloadMins)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSimultaneousDownloads)).BeginInit();
            this.SuspendLayout();
            // 
            // lvAvailableUnits
            // 
            this.lvAvailableUnits.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)));
            this.lvAvailableUnits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader7,
            this.columnHeader6});
            this.lvAvailableUnits.FullRowSelect = true;
            this.lvAvailableUnits.GridLines = true;
            this.lvAvailableUnits.Location = new System.Drawing.Point(8, 24);
            this.lvAvailableUnits.Name = "lvAvailableUnits";
            this.lvAvailableUnits.Size = new System.Drawing.Size(320, 296);
            this.lvAvailableUnits.TabIndex = 0;
            this.lvAvailableUnits.UseCompatibleStateImageBehavior = false;
            this.lvAvailableUnits.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Fleet ID";
            this.columnHeader1.Width = 61;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Vehicle ID";
            this.columnHeader2.Width = 68;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "Serial Number";
            this.columnHeader7.Width = 88;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "Firmware Ver";
            this.columnHeader6.Width = 80;
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(8, 8);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 16);
            this.label1.TabIndex = 1;
            this.label1.Text = "Available Units";
            // 
            // btnAddSelected
            // 
            this.btnAddSelected.Location = new System.Drawing.Point(336, 48);
            this.btnAddSelected.Name = "btnAddSelected";
            this.btnAddSelected.Size = new System.Drawing.Size(32, 24);
            this.btnAddSelected.TabIndex = 1;
            this.btnAddSelected.Text = ">";
            this.btnAddSelected.Click += new System.EventHandler(this.btnAddSelected_Click);
            // 
            // btnAddAll
            // 
            this.btnAddAll.Location = new System.Drawing.Point(336, 80);
            this.btnAddAll.Name = "btnAddAll";
            this.btnAddAll.Size = new System.Drawing.Size(32, 24);
            this.btnAddAll.TabIndex = 2;
            this.btnAddAll.Text = ">>";
            this.btnAddAll.Click += new System.EventHandler(this.btnAddAll_Click);
            // 
            // btnRemoveAll
            // 
            this.btnRemoveAll.Location = new System.Drawing.Point(336, 112);
            this.btnRemoveAll.Name = "btnRemoveAll";
            this.btnRemoveAll.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveAll.TabIndex = 3;
            this.btnRemoveAll.Text = "<<";
            this.btnRemoveAll.Click += new System.EventHandler(this.btnRemoveAll_Click);
            // 
            // btnRemoveSelected
            // 
            this.btnRemoveSelected.Location = new System.Drawing.Point(336, 144);
            this.btnRemoveSelected.Name = "btnRemoveSelected";
            this.btnRemoveSelected.Size = new System.Drawing.Size(32, 24);
            this.btnRemoveSelected.TabIndex = 4;
            this.btnRemoveSelected.Text = "<";
            this.btnRemoveSelected.Click += new System.EventHandler(this.btnRemoveSelected_Click);
            // 
            // lvQueued
            // 
            this.lvQueued.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom)
                        | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.lvQueued.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader5});
            this.lvQueued.FullRowSelect = true;
            this.lvQueued.GridLines = true;
            this.lvQueued.Location = new System.Drawing.Point(376, 24);
            this.lvQueued.Name = "lvQueued";
            this.lvQueued.Size = new System.Drawing.Size(360, 328);
            this.lvQueued.TabIndex = 5;
            this.lvQueued.UseCompatibleStateImageBehavior = false;
            this.lvQueued.View = System.Windows.Forms.View.Details;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Fleet ID";
            this.columnHeader3.Width = 61;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vehicle ID";
            this.columnHeader4.Width = 68;
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Status";
            this.columnHeader5.Width = 204;
            // 
            // label2
            // 
            this.label2.Location = new System.Drawing.Point(376, 8);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 16);
            this.label2.TabIndex = 7;
            this.label2.Text = "Queued for Download";
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.rb4000);
            this.groupBox1.Controls.Add(this.rbMDT);
            this.groupBox1.Controls.Add(this.rb1035);
            this.groupBox1.Controls.Add(this.rb302x);
            this.groupBox1.Location = new System.Drawing.Point(8, 368);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(728, 56);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Download Type";
            // 
            // rbMDT
            // 
            this.rbMDT.Location = new System.Drawing.Point(357, 24);
            this.rbMDT.Name = "rbMDT";
            this.rbMDT.Size = new System.Drawing.Size(88, 16);
            this.rbMDT.TabIndex = 10;
            this.rbMDT.Text = "MDT Image";
            // 
            // rb1035
            // 
            this.rb1035.Location = new System.Drawing.Point(253, 24);
            this.rb1035.Name = "rb1035";
            this.rb1035.Size = new System.Drawing.Size(88, 16);
            this.rb1035.TabIndex = 9;
            this.rb1035.Text = "1035 Image";
            // 
            // rb302x
            // 
            this.rb302x.Checked = true;
            this.rb302x.Location = new System.Drawing.Point(157, 24);
            this.rb302x.Name = "rb302x";
            this.rb302x.Size = new System.Drawing.Size(88, 16);
            this.rb302x.TabIndex = 8;
            this.rb302x.TabStop = true;
            this.rb302x.Text = "302x Image";
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.Location = new System.Drawing.Point(8, 432);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(56, 16);
            this.label3.TabIndex = 9;
            this.label3.Text = "Filename";
            // 
            // txtFilename
            // 
            this.txtFilename.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtFilename.BackColor = System.Drawing.SystemColors.Menu;
            this.txtFilename.Enabled = false;
            this.txtFilename.Location = new System.Drawing.Point(64, 432);
            this.txtFilename.Name = "txtFilename";
            this.txtFilename.Size = new System.Drawing.Size(648, 20);
            this.txtFilename.TabIndex = 11;
            this.txtFilename.TabStop = false;
            // 
            // btnSelectMotFile
            // 
            this.btnSelectMotFile.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSelectMotFile.Location = new System.Drawing.Point(720, 432);
            this.btnSelectMotFile.Name = "btnSelectMotFile";
            this.btnSelectMotFile.Size = new System.Drawing.Size(24, 20);
            this.btnSelectMotFile.TabIndex = 12;
            this.btnSelectMotFile.Text = "..";
            this.btnSelectMotFile.Click += new System.EventHandler(this.btnSelectMotFile_Click);
            // 
            // btnSendToUnits
            // 
            this.btnSendToUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendToUnits.Location = new System.Drawing.Point(640, 576);
            this.btnSendToUnits.Name = "btnSendToUnits";
            this.btnSendToUnits.Size = new System.Drawing.Size(96, 24);
            this.btnSendToUnits.TabIndex = 19;
            this.btnSendToUnits.Text = "Send To Units";
            this.btnSendToUnits.Click += new System.EventHandler(this.btnSendToUnits_Click);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.Location = new System.Drawing.Point(8, 464);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(176, 16);
            this.label5.TabIndex = 16;
            this.label5.Text = "Abort download if not complete in ";
            // 
            // numTimeoutDownloadMins
            // 
            this.numTimeoutDownloadMins.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numTimeoutDownloadMins.Location = new System.Drawing.Point(184, 464);
            this.numTimeoutDownloadMins.Maximum = new decimal(new int[] {
            60,
            0,
            0,
            0});
            this.numTimeoutDownloadMins.Minimum = new decimal(new int[] {
            6,
            0,
            0,
            0});
            this.numTimeoutDownloadMins.Name = "numTimeoutDownloadMins";
            this.numTimeoutDownloadMins.Size = new System.Drawing.Size(56, 20);
            this.numTimeoutDownloadMins.TabIndex = 13;
            this.numTimeoutDownloadMins.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.Location = new System.Drawing.Point(240, 464);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(32, 16);
            this.label6.TabIndex = 18;
            this.label6.Text = "mins.";
            // 
            // btnAbort
            // 
            this.btnAbort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnAbort.Location = new System.Drawing.Point(528, 576);
            this.btnAbort.Name = "btnAbort";
            this.btnAbort.Size = new System.Drawing.Size(104, 24);
            this.btnAbort.TabIndex = 18;
            this.btnAbort.Text = "Abort Downloads";
            this.btnAbort.Click += new System.EventHandler(this.btnAbort_Click);
            // 
            // btnRefershAvaliableList
            // 
            this.btnRefershAvaliableList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnRefershAvaliableList.Location = new System.Drawing.Point(8, 328);
            this.btnRefershAvaliableList.Name = "btnRefershAvaliableList";
            this.btnRefershAvaliableList.Size = new System.Drawing.Size(320, 24);
            this.btnRefershAvaliableList.TabIndex = 6;
            this.btnRefershAvaliableList.Text = "Refresh Avaliable List";
            this.btnRefershAvaliableList.Click += new System.EventHandler(this.btnRefershAvaliableList_Click);
            // 
            // rtbConsole
            // 
            this.rtbConsole.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.rtbConsole.Location = new System.Drawing.Point(0, 610);
            this.rtbConsole.Name = "rtbConsole";
            this.rtbConsole.Size = new System.Drawing.Size(744, 104);
            this.rtbConsole.TabIndex = 20;
            this.rtbConsole.Text = "";
            // 
            // button3
            // 
            this.button3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.button3.Location = new System.Drawing.Point(424, 576);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(96, 24);
            this.button3.TabIndex = 17;
            this.button3.Text = "Close";
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // txtEmailTo
            // 
            this.txtEmailTo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)
                        | System.Windows.Forms.AnchorStyles.Right)));
            this.txtEmailTo.Location = new System.Drawing.Point(96, 488);
            this.txtEmailTo.Name = "txtEmailTo";
            this.txtEmailTo.Size = new System.Drawing.Size(640, 20);
            this.txtEmailTo.TabIndex = 15;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.Location = new System.Drawing.Point(8, 488);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(80, 16);
            this.label8.TabIndex = 20;
            this.label8.Text = "Email Status to";
            // 
            // numSimultaneousDownloads
            // 
            this.numSimultaneousDownloads.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numSimultaneousDownloads.Location = new System.Drawing.Point(184, 488);
            this.numSimultaneousDownloads.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.numSimultaneousDownloads.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSimultaneousDownloads.Name = "numSimultaneousDownloads";
            this.numSimultaneousDownloads.Size = new System.Drawing.Size(56, 20);
            this.numSimultaneousDownloads.TabIndex = 14;
            this.numSimultaneousDownloads.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numSimultaneousDownloads.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.Location = new System.Drawing.Point(8, 488);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(184, 16);
            this.label9.TabIndex = 24;
            this.label9.Text = "Number of simultaneous downloads";
            this.label9.Visible = false;
            // 
            // txtSMTPServer
            // 
            this.txtSMTPServer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtSMTPServer.Location = new System.Drawing.Point(96, 512);
            this.txtSMTPServer.Name = "txtSMTPServer";
            this.txtSMTPServer.Size = new System.Drawing.Size(160, 20);
            this.txtSMTPServer.TabIndex = 16;
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.Location = new System.Drawing.Point(8, 512);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(80, 16);
            this.label4.TabIndex = 26;
            this.label4.Text = "SMTP Server";
            // 
            // btnMoveToNext
            // 
            this.btnMoveToNext.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnMoveToNext.Enabled = false;
            this.btnMoveToNext.Location = new System.Drawing.Point(296, 576);
            this.btnMoveToNext.Name = "btnMoveToNext";
            this.btnMoveToNext.Size = new System.Drawing.Size(120, 24);
            this.btnMoveToNext.TabIndex = 27;
            this.btnMoveToNext.Text = "Move to Next";
            this.btnMoveToNext.Click += new System.EventHandler(this.btnMoveToNext_Click);
            // 
            // chkContinueWithoutWaiting
            // 
            this.chkContinueWithoutWaiting.Location = new System.Drawing.Point(8, 544);
            this.chkContinueWithoutWaiting.Name = "chkContinueWithoutWaiting";
            this.chkContinueWithoutWaiting.Size = new System.Drawing.Size(240, 16);
            this.chkContinueWithoutWaiting.TabIndex = 28;
            this.chkContinueWithoutWaiting.Text = "Continue without waiting for unit to re-login";
            // 
            // rb4000
            // 
            this.rb4000.Location = new System.Drawing.Point(461, 24);
            this.rb4000.Name = "rb4000";
            this.rb4000.Size = new System.Drawing.Size(88, 16);
            this.rb4000.TabIndex = 11;
            this.rb4000.Text = "4000 Image";
            // 
            // frmMultipleDownload
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(744, 714);
            this.Controls.Add(this.chkContinueWithoutWaiting);
            this.Controls.Add(this.btnMoveToNext);
            this.Controls.Add(this.txtSMTPServer);
            this.Controls.Add(this.txtFilename);
            this.Controls.Add(this.txtEmailTo);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.rtbConsole);
            this.Controls.Add(this.btnRefershAvaliableList);
            this.Controls.Add(this.btnAbort);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.numTimeoutDownloadMins);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.btnSendToUnits);
            this.Controls.Add(this.btnSelectMotFile);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lvQueued);
            this.Controls.Add(this.btnRemoveSelected);
            this.Controls.Add(this.btnRemoveAll);
            this.Controls.Add(this.btnAddAll);
            this.Controls.Add(this.btnAddSelected);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.lvAvailableUnits);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.numSimultaneousDownloads);
            this.Controls.Add(this.label9);
            this.Name = "frmMultipleDownload";
            this.Text = "Download to Multiple Units";
            this.Load += new System.EventHandler(this.frmMultipleDownload_Load);
            this.Resize += new System.EventHandler(this.frmMultipleDownload_Resize);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.numTimeoutDownloadMins)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSimultaneousDownloads)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        #region Public Functions
        public void AddPacketToCheckList(GatewayProtocolPacket packet)
        {
            try
            {
                if (tFindUnits != null)
                {
                    if (tFindUnits.Running)
                    {
                        lock (oUpdatePackets.SyncRoot)
                        {
                            oUpdatePackets.Add(packet);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
                throw new System.Exception(sClassName + "AddPacketToCheckList(GatewayProtocolPacket packet)", ex);
            }
        }
        #endregion
        #region List Load and maniplation functions
        private void LoadListView()
        {
            if (lvAvailableUnits.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { LoadListView(); }));
            else
            {
                string SQLCmd = "SELECT Distinct FleetID, VehicleID, ListenPort, SerialNumber from T_GatewayUnit Where FleetID > 0 and VehicleID > 0 ORDER BY FleetID, VehicleID";
                string prpDSN = "";
                System.Data.DataSet objRS = null;
                System.Data.SqlClient.SqlDataAdapter objSQLDA = null;
                System.Data.SqlClient.SqlCommand objSQLCmd = null;
                System.Data.SqlClient.SqlConnection objSQLConn = null;
                try
                {
                    // Clear the list view
                    lvAvailableUnits.Items.Clear();

                    // Create the objects
                    prpDSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                    objSQLConn = new System.Data.SqlClient.SqlConnection(prpDSN);
                    objRS = new System.Data.DataSet();
                    objSQLDA = new System.Data.SqlClient.SqlDataAdapter();
                    if (objSQLConn != null)
                    {
                        // Connect to the SQL server
                        objSQLConn.Open();
                        // Create the SQL command.
                        objSQLCmd = objSQLConn.CreateCommand();
                        objSQLCmd.CommandText = SQLCmd;
                        objSQLCmd.CommandTimeout = 3600;
                        // Create the data adapter
                        objSQLDA.SelectCommand = objSQLCmd;
                        // Fill the DataSet
                        objSQLDA.Fill(objRS);
                        // Clean up SQL objects
                        objSQLCmd.Dispose();
                        objSQLCmd = null;
                        objSQLDA.Dispose();
                        objSQLDA = null;
                        objSQLConn.Close();
                        objSQLConn.Dispose();
                        objSQLConn = null;

                        // Retrieve the data value.
                        if (objRS.Tables.Count > 0)
                        {
                            if (objRS.Tables[0].Rows.Count > 0)
                            {
                                for (int X = 0; X < objRS.Tables[0].Rows.Count; X++)
                                {
                                    string[] sItem = new string[4];
                                    sItem[0] = Convert.ToString(objRS.Tables[0].Rows[X]["FleetID"]);
                                    sItem[1] = Convert.ToString(objRS.Tables[0].Rows[X]["VehicleID"]);
                                    sItem[2] = Convert.ToString(objRS.Tables[0].Rows[X]["SerialNumber"]);
                                    if (objRS.Tables[0].Rows[X]["ListenPort"] == System.DBNull.Value)
                                        sItem[3] = "";
                                    else
                                    {
                                        string sFirmware = Convert.ToString(objRS.Tables[0].Rows[X]["ListenPort"]);
                                        if (sFirmware.IndexOf(",") >= 0)
                                        {
                                            string[] sHWSplit = sFirmware.Split(",".ToCharArray());
                                            if (sHWSplit.Length == 2)
                                                sItem[3] = sHWSplit[1];
                                            else
                                                sItem[3] = "";
                                        }
                                        else
                                            sItem[3] = "";
                                    }
                                    ListViewItem oLVI = new ListViewItem(sItem);
                                    lvAvailableUnits.Items.Add(oLVI);
                                }
                            }
                        }
                        // Clean up Dataset object
                        if (objRS != null)
                            objRS.Dispose();
                        objRS = null;
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "LoadListView()", ex);
                    throw new System.Exception(sClassName + "LoadListView()", ex);
                }
            }
        }

        private void btnRefershAvaliableList_Click(object sender, System.EventArgs e)
        {
            try
            {
                LoadListView();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnRefershAvaliableList_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRefershAvaliableList_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void btnAddAll_Click(object sender, System.EventArgs e)
        {
            if (this.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnAddAll_Click(sender, e); }));
            else
            {
                bool bAddItem = true;
                ListViewItem oLVI = null;
                try
                {
                    lvQueued.Items.Clear();
                    for (int X = 0; X < lvAvailableUnits.Items.Count; X++)
                    {
                        oLVI = lvAvailableUnits.Items[X];
                        string sFleetID = oLVI.SubItems[0].Text;
                        string sVehicleID = oLVI.SubItems[1].Text;
                        string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                        bAddItem = true;
                        // Check if this vehicle is already in the list.
                        for (int Y = 0; Y < lvQueued.Items.Count; Y++)
                        {
                            ListViewItem oLVIQueued = lvQueued.Items[Y];
                            string sQueuedFleetID = oLVIQueued.SubItems[0].Text;
                            string sQueuedVehicleID = oLVIQueued.SubItems[1].Text;
                            if (sQueuedFleetID == sFleetID && sQueuedVehicleID == sVehicleID)
                            {
                                bAddItem = false;
                                break;
                            }
                        }

                        if (bAddItem)
                        {
                            lock (oDownloadToList.SyncRoot)
                            {
                                if (oDownloadToList.ContainsKey(sKey))
                                {
                                    if (oDownloadToList[sKey] != null)
                                    {
                                        EnhancedThread tDownloadToUnit = (EnhancedThread)oDownloadToList[sKey];
                                        if (tDownloadToUnit.Running)
                                        {
                                            tDownloadToUnit.Stop();
                                        }
                                    }
                                    oDownloadToList.Remove(sKey);
                                    string[] sQueuedRecord = new string[3];
                                    sQueuedRecord[0] = sFleetID;
                                    sQueuedRecord[1] = sVehicleID;
                                    sQueuedRecord[2] = "Queued.";
                                    lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                                    EnhancedThread tThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(DownloadToUnit), sKey);
                                    oDownloadToList.Add(sKey, tThread);
                                }
                                else
                                {
                                    if (!oDownloadToList.ContainsKey(sKey))
                                    {
                                        string[] sQueuedRecord = new string[3];
                                        sQueuedRecord[0] = sFleetID;
                                        sQueuedRecord[1] = sVehicleID;
                                        sQueuedRecord[2] = "Queued.";
                                        lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                                        EnhancedThread tDownloadToUnit = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(DownloadToUnit), sKey);
                                        oDownloadToList.Add(sKey, tDownloadToUnit);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnAddAll_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAddAll_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnRemoveAll_Click(object sender, System.EventArgs e)
        {
            if (this.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnRemoveAll_Click(sender, e); }));
            else
            {
                bool bAskAbort = true;
                bool bAbortCurrentDownloads = false;
                try
                {
                    lock (oDownloadToList.SyncRoot)
                    {
                        for (int X = 0; X < lvQueued.Items.Count; X++)
                        {
                            ListViewItem oLVI = lvQueued.Items[X];
                            string sFleetID = oLVI.SubItems[0].Text;
                            string sVehicleID = oLVI.SubItems[1].Text;
                            string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                            lock (oDownloadToList.SyncRoot)
                            {
                                if (oDownloadToList.ContainsKey(sKey))
                                {
                                    if (oDownloadToList[sKey] != null)
                                    {
                                        EnhancedThread tDownloadToUnit = (EnhancedThread)oDownloadToList[sKey];
                                        if (tDownloadToUnit.Running)
                                        {
                                            if (bAskAbort)
                                            {
                                                if (MessageBox.Show(this, "Do you want to cancel the current downloads?", "MTData", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                                {
                                                    bAbortCurrentDownloads = true;
                                                }
                                                bAskAbort = false;
                                            }
                                            if (bAbortCurrentDownloads)
                                            {
                                                tDownloadToUnit.Stop();
                                                oDownloadToList.Remove(sKey);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        oDownloadToList.Remove(sKey);
                                    }
                                }
                            }
                            lvQueued.Items.RemoveAt(X);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnRemoveAll_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRemoveAll_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnAddSelected_Click(object sender, System.EventArgs e)
        {
            if (this.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnAddSelected_Click(sender, e); }));
            else
            {
                try
                {
                    for (int X = 0; X < lvAvailableUnits.Items.Count; X++)
                    {
                        if (lvAvailableUnits.Items[X].Selected)
                        {
                            ListViewItem oLVI = lvAvailableUnits.Items[X];
                            string sFleetID = oLVI.SubItems[0].Text;
                            string sVehicleID = oLVI.SubItems[1].Text;
                            string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');

                            // Check if this vehicle is already in the list.
                            for (int Y = 0; Y < lvQueued.Items.Count; Y++)
                            {
                                ListViewItem oLVIQueued = lvQueued.Items[Y];
                                string sQueuedFleetID = oLVIQueued.SubItems[0].Text;
                                string sQueuedVehicleID = oLVIQueued.SubItems[1].Text;
                                if (sQueuedFleetID == sFleetID && sQueuedVehicleID == sVehicleID)
                                    return;
                            }

                            lock (oDownloadToList.SyncRoot)
                            {
                                if (oDownloadToList.ContainsKey(sKey))
                                {
                                    if (oDownloadToList[sKey] != null)
                                    {
                                        EnhancedThread tDownloadToUnit = (EnhancedThread)oDownloadToList[sKey];
                                        if (tDownloadToUnit.Running)
                                        {
                                            tDownloadToUnit.Stop();
                                        }
                                    }
                                    oDownloadToList.Remove(sKey);
                                    string[] sQueuedRecord = new string[3];
                                    sQueuedRecord[0] = sFleetID;
                                    sQueuedRecord[1] = sVehicleID;
                                    sQueuedRecord[2] = "Queued.";
                                    lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                                    EnhancedThread tThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(DownloadToUnit), sKey);
                                    oDownloadToList.Add(sKey, tThread);
                                }
                                else
                                {
                                    if (!oDownloadToList.ContainsKey(sKey))
                                    {
                                        string[] sQueuedRecord = new string[3];
                                        sQueuedRecord[0] = sFleetID;
                                        sQueuedRecord[1] = sVehicleID;
                                        sQueuedRecord[2] = "Queued.";
                                        lvQueued.Items.Add(new ListViewItem(sQueuedRecord));
                                        EnhancedThread tDownloadToUnit = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(DownloadToUnit), sKey);
                                        oDownloadToList.Add(sKey, tDownloadToUnit);
                                    }
                                }
                            }
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnAddSelected_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAddSelected_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnRemoveSelected_Click(object sender, System.EventArgs e)
        {
            if (this.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnRemoveSelected_Click(sender, e); }));
            else
            {
                bool bAskAbort = true;
                bool bAbortCurrentDownloads = false;

                try
                {
                    for (int X = lvQueued.Items.Count - 1; X >= 0; X--)
                    {
                        if (lvQueued.Items[X].Selected)
                        {
                            ListViewItem oLVI = lvQueued.Items[X];
                            string sFleetID = oLVI.SubItems[0].Text;
                            string sVehicleID = oLVI.SubItems[1].Text;
                            string sKey = Convert.ToString(sFleetID).PadLeft(4, '0') + "~" + Convert.ToString(sVehicleID).PadLeft(4, '0');
                            lock (oDownloadToList.SyncRoot)
                            {
                                if (oDownloadToList.ContainsKey(sKey))
                                {
                                    if (oDownloadToList[sKey] != null)
                                    {
                                        EnhancedThread tDownloadToUnit = (EnhancedThread)oDownloadToList[sKey];
                                        if (tDownloadToUnit.Running)
                                        {
                                            if (bAskAbort)
                                            {
                                                if (MessageBox.Show(this, "Do you want to cancel the current downloads?", "MTData", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                                                {
                                                    bAbortCurrentDownloads = true;
                                                }
                                                bAskAbort = false;
                                            }
                                            if (bAbortCurrentDownloads)
                                            {
                                                tDownloadToUnit.Stop();
                                                oDownloadToList.Remove(sKey);
                                            }
                                        }
                                    }
                                    else
                                    {
                                        oDownloadToList.Remove(sKey);
                                    }
                                }
                            }
                            lvQueued.Items.RemoveAt(X);
                        }
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnRemoveSelected_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnRemoveSelected_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }
        #endregion
        #region Event Code for the buttons
        private void btnSendToUnits_Click(object sender, System.EventArgs e)
        {
            if (this.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { btnSendToUnits_Click(sender, e); }));
            else
            {
                try
                {
                    if (tFindUnits != null)
                    {
                        if (tFindUnits.Running)
                        {
                            rtbConsole.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Waiting for a selected unit to report in. ('Match Unit In Queued List' thread already active)\n";
                            return;
                        }
                    }
                    if (lvQueued.Items.Count > 0)
                    {
                        tFindUnits = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(MatchUnitInQueuedList), null);
                        tFindUnits.Start();
                    }
                    rtbConsole.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Waiting for a selected unit to report in. ('Match Unit In Queued List' thread started)\n";
                    _log.Info("Download to multiple units process started.");
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "btnSendToUnits_Click(object sender, System.EventArgs e)", ex);
                    MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnSendToUnits_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
                }
            }
        }

        private void btnAbort_Click(object sender, System.EventArgs e)
        {
            try
            {
                if (tFindUnits != null)
                {
                    if (tFindUnits.Running)
                        tFindUnits.Stop();
                }
                tFindUnits = null;
                UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Stopping monitoring reports from units ('Match Unit In Queued List' thread stopped)\n");
                _log.Info("Download to multiple units process stopped.");
                StopDownloadThreads();

                if (oUpdatePackets != null)
                {
                    lock (oUpdatePackets.SyncRoot)
                        oUpdatePackets.Clear();
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnAbort_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnAbort_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        private void button3_Click(object sender, System.EventArgs e)
        {
            try
            {
                this.Close();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "button3_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "button3_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        #endregion
        #region Threads
        private void StopDownloadThreads()
        {
            if (oDownloadToList != null)
            {
                lock (oDownloadToList.SyncRoot)
                {
                    ArrayList oKeys = new ArrayList(oDownloadToList.Keys);
                    for (int X = 0; X < oKeys.Count; X++)
                    {
                        string sKey = (string)oKeys[X];
                        if (oDownloadToList.ContainsKey(sKey))
                        {
                            if (oDownloadToList[sKey] != null)
                            {
                                EnhancedThread tDownloadToUnit = (EnhancedThread)oDownloadToList[sKey];
                                oDownloadToList.Remove(sKey);
                                if (tDownloadToUnit.Running)
                                {
                                    tDownloadToUnit.Stop();
                                }
                            }
                        }
                    }
                }
            }
            // Wait for all the threads to terminate.
            Thread.Sleep(200);
        }

        private object MatchUnitInQueuedList(EnhancedThread sender, object data)
        {
            ArrayList oLocalList = null;
            int iDownloadRetryCount = 0;
            bool bLookingForUnitLogins = false;
            bool bIgnitionCheck = false;
            bool bRetryCheckPassed = false;

            while (!sender.Stopping)
            {
                try
                {
                    oLocalList = new ArrayList();
                    bLookingForUnitLogins = false;

                    #region Get a set of packets comming in from the units and copy them to oLocalList
                    if (GetNumberOfActiveDownloads() >= 1)
                    {
                        #region If we are currently downloading to a unit
                        #region Check if we are waiting for a unit to log back in after a download
                        lock (oWaitForLoginList.SyncRoot)
                        {
                            if (oWaitForLoginList.Count > 0)
                            {
                                bLookingForUnitLogins = true;
                            }
                        }
                        #endregion
                        if (!bLookingForUnitLogins)
                        {
                            #region If we are not looking for a unit to log back in and we are currently downloading, just clear the list.
                            lock (oUpdatePackets.SyncRoot)
                                oUpdatePackets.Clear();
                            #endregion
                        }
                        else
                        {
                            #region If we are looking for a unit to log back in
                            #region Copy the list to a local array
                            lock (oUpdatePackets.SyncRoot)
                            {
                                for (int X = oUpdatePackets.Count - 1; X >= 0; X--)
                                {
                                    oLocalList.Add(oUpdatePackets[X]);
                                    oUpdatePackets.RemoveAt(X);
                                }
                            }
                            #endregion
                            #region Check if the unit we are waiting for has logged in again
                            for (int X = 0; X < oLocalList.Count; X++)
                            {
                                GatewayProtocolPacket packet = (GatewayProtocolPacket)oLocalList[X];
                                byte[] bConvert = new byte[4];
                                bConvert[0] = packet.cFleetId;
                                int iFleetID = BitConverter.ToInt32(bConvert, 0);
                                int iVehicleID = Convert.ToInt32(packet.iVehicleId);
                                string sKey = Convert.ToString(iFleetID).PadLeft(4, '0') + "~" + Convert.ToString(iVehicleID).PadLeft(4, '0');
                                lock (oWaitForLoginList.SyncRoot)
                                {
                                    if (oWaitForLoginList.ContainsKey(sKey))
                                    {
                                        #region If the unit we are waiting for has logged back in
                                        oWaitForLoginList.Remove(sKey);
                                        oDP_eUpdateVehicleStatus(Convert.ToString(iFleetID), Convert.ToString(iVehicleID), "Download Complete - Unit has re-logged in.");
                                        string sUnit = "Fleet " + Convert.ToString(iFleetID) + " / Vehicle " + Convert.ToString(iVehicleID);
                                        UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " complete.  Unit has logged back into the listener.\n");
                                        _log.Info("Download to Unit " + Convert.ToString(iFleetID) + "/" + Convert.ToString(iVehicleID) + " complete.  Unit has logged back into the listener.");
                                        if (txtEmailTo.Text.Length > 0 && txtSMTPServer.Text.Length > 0)
                                        {
                                            #region If the email is enabled, send an email update
                                            string sSMTP = txtSMTPServer.Text;
                                            if (sSMTP != "")
                                            {
                                                string sFrom = ConfigurationManager.AppSettings["EMailSourceAddress"];
                                                if (sFrom.Length == 0)
                                                    sFrom = "MTData.Listener@mtdata.com.au";
                                                string sTo = txtEmailTo.Text;
                                                string sSubject = "Multiple Download Email Update - Download to " + sUnit + " complete.";
                                                string sEmailMsg = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " complete.  Unit has been downloaded and logged back into the server.\n";
                                                Email.ProcessEmail(sSMTP, sTo, sFrom, sSubject, sEmailMsg, false);
                                            }
                                            #endregion
                                        }
                                        #endregion
                                    }
                                }
                            }
                            if (oWaitForLoginList.Count == 0)
                            {
                                EnableSendToNextButton(false);
                            }
                            else
                            {
                                if (!chkContinueWithoutWaiting.Checked)
                                    oLocalList.Clear();
                            }
                            #endregion
                            #region Clear the local list
                            oLocalList = new ArrayList();
                            #endregion
                            #endregion
                        }
                        #endregion
                    }
                    else
                    {
                        #region Copy the items waiting to be processed into a local array.
                        lock (oUpdatePackets.SyncRoot)
                        {
                            for (int X = oUpdatePackets.Count - 1; X >= 0; X--)
                            {
                                oLocalList.Add(oUpdatePackets[X]);
                                oUpdatePackets.RemoveAt(X);
                            }
                        }
                        #endregion
                    }
                    #endregion

                    if (oLocalList.Count > 0)
                    {
                        #region Check if we are waiting for a unit to log back in after a download
                        lock (oWaitForLoginList.SyncRoot)
                        {
                            if (oWaitForLoginList.Count > 0)
                            {
                                bLookingForUnitLogins = true;
                            }
                        }
                        #endregion
                        if (bLookingForUnitLogins)
                        {
                            #region If we are looking for a unit to log back in
                            for (int X = 0; X < oLocalList.Count; X++)
                            {
                                GatewayProtocolPacket packet = (GatewayProtocolPacket)oLocalList[X];
                                byte[] bConvert = new byte[4];
                                bConvert[0] = packet.cFleetId;
                                int iFleetID = BitConverter.ToInt32(bConvert, 0);
                                int iVehicleID = Convert.ToInt32(packet.iVehicleId);
                                string sKey = Convert.ToString(iFleetID).PadLeft(4, '0') + "~" + Convert.ToString(iVehicleID).PadLeft(4, '0');
                                lock (oWaitForLoginList.SyncRoot)
                                {
                                    #region Check if the unit we are waiting for has logged in again
                                    if (oWaitForLoginList.ContainsKey(sKey))
                                    {
                                        oWaitForLoginList.Remove(sKey);
                                        oDP_eUpdateVehicleStatus(Convert.ToString(iFleetID), Convert.ToString(iVehicleID), "Download Complete - Unit has re-logged in.");
                                        string sUnit = "Fleet " + Convert.ToString(iFleetID) + " / Vehicle " + Convert.ToString(iVehicleID);
                                        UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " complete.  Unit has logged back into the listener.\n");
                                        _log.Info("Download to Unit " + Convert.ToString(iFleetID) + "/" + Convert.ToString(iVehicleID) + " complete.  Unit has logged back into the listener.");
                                        if (txtEmailTo.Text.Length > 0 && txtSMTPServer.Text.Length > 0)
                                        {
                                            #region If the email is enabled, send an email update
                                            string sSMTP = txtSMTPServer.Text;
                                            if (sSMTP != "")
                                            {
                                                string sFrom = ConfigurationManager.AppSettings["EMailSourceAddress"];
                                                if (sFrom.Length == 0)
                                                    sFrom = "MTData.Listener@mtdata.com.au";
                                                string sTo = txtEmailTo.Text;
                                                string sSubject = "Multiple Download Email Update - Download to " + sUnit + " complete.";
                                                string sEmailMsg = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " complete.  Unit has been downloaded and logged back into the server.\n";
                                                Email.ProcessEmail(sSMTP, sTo, sFrom, sSubject, sEmailMsg, false);
                                                break;
                                            }
                                            #endregion
                                        }
                                    }
                                    #endregion
                                }
                            }
                            if (oWaitForLoginList.Count == 0)
                            {
                                EnableSendToNextButton(false);
                            }
                            else
                            {
                                if (!chkContinueWithoutWaiting.Checked)
                                    oLocalList.Clear();
                            }
                            #endregion
                        }
                        #region Check the list to see if a unit we are waiting to download to has reported in.
                        for (int X = 0; X < oLocalList.Count; X++)
                        {
                            GatewayProtocolPacket packet = (GatewayProtocolPacket)oLocalList[X];
                            // Only start a new download as a response to a status packet.
                            if (packet.cMsgType == (byte)0xC1)
                            {
                                byte[] bConvert = new byte[4];
                                bConvert[0] = packet.cFleetId;
                                int iFleetID = BitConverter.ToInt32(bConvert, 0);
                                int iVehicleID = Convert.ToInt32(packet.iVehicleId);
                                string sKey = Convert.ToString(iFleetID).PadLeft(4, '0') + "~" + Convert.ToString(iVehicleID).PadLeft(4, '0');
                                lock (oDownloadToList.SyncRoot)
                                {
                                    if (oDownloadToList.ContainsKey(sKey))
                                    {
                                        if (GetNumberOfActiveDownloads() == 0)
                                        {
                                            if (oDownloadToList[sKey] != null)
                                            {
                                                #region If this is a 1035 download, check that the ignition is on.
                                                bIgnitionCheck = false;
                                                if (rb1035.Checked)
                                                {
                                                    #region Check that the ignition is on
                                                    GeneralGPPacket oGPPacket = new GeneralGPPacket((GatewayProtocolPacket)packet, "MM/dd/yyyy HH:mm:ss");
                                                    if ((((int)oGPPacket.mStatus.cInputStatus) & 1) == 1)
                                                        bIgnitionCheck = true;
                                                    else
                                                    {
                                                        UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Waiting for " + Convert.ToString(iFleetID) + " / Vehicle " + Convert.ToString(iVehicleID) + " to report with ignition on.\n");
                                                        oDP_eUpdateVehicleStatus(Convert.ToString(iFleetID), Convert.ToString(iVehicleID), "Waiting for unit to report ignition on.");
                                                    }
                                                    #endregion
                                                }
                                                else
                                                {
                                                    bIgnitionCheck = true;
                                                }
                                                #endregion
                                                if (bIgnitionCheck)
                                                {
                                                    #region Download to the unit
                                                    EnhancedThread tDownloadToUnit = (EnhancedThread)oDownloadToList[sKey];
                                                    if (!tDownloadToUnit.Running)
                                                    {
                                                        #region Check that the unit has not failed to download more than 5 times
                                                        bRetryCheckPassed = false;
                                                        lock (oRetryCounts.SyncRoot)
                                                        {
                                                            if (oRetryCounts.ContainsKey(sKey))
                                                            {
                                                                iDownloadRetryCount = (int)oRetryCounts[sKey];
                                                                iDownloadRetryCount++;
                                                                if (iDownloadCount > 5)
                                                                {
                                                                    UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - To many retires for Fleet " + Convert.ToString(iFleetID) + " / Vehicle " + Convert.ToString(iVehicleID) + ", download was not restarted.\n");
                                                                    _log.Info("To many retires for Fleet " + Convert.ToString(iFleetID) + " / Vehicle " + Convert.ToString(iVehicleID) + ", download was not restarted.");
                                                                    oDP_eUpdateVehicleStatus(Convert.ToString(iFleetID), Convert.ToString(iVehicleID), "Download has restarted too many times.");
                                                                }
                                                                else
                                                                {
                                                                    oRetryCounts.Remove(sKey);
                                                                    oRetryCounts.Add(sKey, iDownloadRetryCount);
                                                                    bRetryCheckPassed = true;
                                                                }
                                                            }
                                                            else
                                                            {
                                                                oRetryCounts.Add(sKey, 1);
                                                                bRetryCheckPassed = true;
                                                            }
                                                        }
                                                        #endregion
                                                        if (bRetryCheckPassed)
                                                        {
                                                            // Start the download process
                                                            tDownloadToUnit.DataObject = GetDownloadProcessor(packet);
                                                            tDownloadToUnit.Start();
                                                            lock (this.SyncRoot)
                                                                iDownloadCount++;
                                                            // Clear the local list
                                                            oLocalList.Clear();
                                                            // Clear the list of packets waiting to be matched.
                                                            lock (oUpdatePackets.SyncRoot)
                                                                oUpdatePackets.Clear();
                                                            break;
                                                        }
                                                    }
                                                    #endregion
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        #endregion
                        oLocalList.Clear();
                    }
                    Thread.Sleep(500);
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "MatchUnitInQueuedList(EnhancedThread sender, object data)", ex);
                }
            }
            return null;
        }

        private void UpdateConsoleText(string sStatus)
        {
            if (InvokeRequired)
                rtbConsole.BeginInvoke(new MethodInvoker(delegate() { UpdateConsoleText(sStatus); }));
            else
            {
                try
                {
                    rtbConsole.Text += sStatus;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "UpdateConsoleText(string sStatus)", ex);
                }
            }
        }
        private void EnableSendToNextButton(bool bEnabled)
        {
            if (rtbConsole.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { EnableSendToNextButton(bEnabled); }));
            else
            {
                try
                {
                    btnMoveToNext.Enabled = bEnabled;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "EnableSendToNextButton(bool bEnabled)", ex);
                }
            }
        }

        private string _emailToAddress = "";
        private string _smtpAddress = "";
        private void GetEmailToAddress()
        {
            if (txtEmailTo.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { GetEmailToAddress(); }));
            else
            {
                try
                {
                    _emailToAddress = txtEmailTo.Text;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetEmailToAddress()", ex);
                }
            }
        }
        private void GetSMTPServerAddress()
        {
            if (txtSMTPServer.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { GetSMTPServerAddress(); }));
            else
            {
                try
                {
                    _smtpAddress = txtSMTPServer.Text;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "GetSMTPServerAddress()", ex);
                }
            }
        }

        private int iAckTimer = 0;
        private object DownloadToUnit(EnhancedThread sender, object data)
        {
            string sUnit = "";
            string sKey = "";
            int iTimoutDownload = (Convert.ToInt32(numTimeoutDownloadMins.Value) * 60) * 10;  // Get value in 100ms blocks
            int iTimer = 0;
            int iAckTimeout = 0;
            DownloadProcessor oDP = null;
            try
            {
                iAckTimer = 0;
                oDP = (DownloadProcessor)data;
                // Download v6, 5 retries, n seconds between each retry
                oDP.eUpdateVehicleStatus += new DownloadProcessor.UpdateVehicleStatusEvent(oDP_eUpdateVehicleStatus);
                oDP.eVehicleDownloadComplete += new DownloadProcessor.VehicleDownloadCompleteEvent(oDP_eVehicleDownloadComplete);
                if (rb4000.Checked)
                {
                    oDP.BeginDownload(6, iRetryCount, iRetryInterval, txtFilename.Text);
                }
                else
                {
                    oDP.BeginDownload(6, iRetryCount, iRetryInterval, mMotFile);
                }
                iAckTimeout = ((iRetryInterval * iRetryCount) + 5) * 100;
                sUnit = "Fleet " + oDP.sFleetID + " / Vehicle " + oDP.sVehicleID;
                sKey = oDP.sFleetID.PadLeft(4, '0') + "~" + oDP.sVehicleID.PadLeft(4, '0');
                UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Started Download to " + sUnit + "\n");
                _log.Info("Started Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
            }
            try
            {
                while (!sender.Stopping)
                {
                    Thread.Sleep(100);
                    iTimer++;
                    if (iTimer >= iTimoutDownload)
                    {
                        UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " took longer then the maximum allowed time.\n");
                        _log.Info("Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID + " took longer than the maximum time allowed.");
                        break;
                    }
                    lock (this.SyncRoot)
                        iAckTimer++;
                    if (iAckTimer >= iAckTimeout)
                    {
                        UpdateConsoleText(rtbConsole.Text += DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " failed to acknowledge packet.\n");
                        _log.Info("Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID + " failed.  Unit failed to acknowledge packet.");
                        oDP.State = DownloadProcessor.ProcessState.TimedOut;

                        break;
                    }

                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
            }

            bool bRecreateThread = false;
            bool bWaitForUnitLogin = false;

            if (oDP != null)
            {
                switch (oDP.State)
                {
                    case DownloadProcessor.ProcessState.Failed:
                        try
                        {
                            UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " failed - Will restart download when the unit next reports in.\n");
                            _log.Info("Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID + " failed.  Will restart download when the unit next reports in.");
                            oDP_eUpdateVehicleStatus(oDP.sFleetID, oDP.sVehicleID, "Download failed - Will restart download when the unit next reports in.");
                            bRecreateThread = true;
                            bWaitForUnitLogin = false;
                            GetEmailToAddress();
                            GetSMTPServerAddress();
                            if (_emailToAddress.Length > 0 && _smtpAddress.Length > 0)
                            {
                                string sSMTP = _smtpAddress;
                                if (sSMTP != "")
                                {
                                    string sFrom = ConfigurationManager.AppSettings["EMailSourceAddress"];
                                    if (sFrom.Length == 0)
                                        sFrom = "MTData.Listener@mtdata.com.au";
                                    string sTo = _emailToAddress;
                                    string sSubject = "Multiple Download Email Update -  Download to " + sUnit + " failed.";
                                    string sEmailMsg = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " failed.\nThe server will try to download to the unit again.";
                                    Email.ProcessEmail(sSMTP, sTo, sFrom, sSubject, sEmailMsg, false);
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
                        }
                        break;
                    case DownloadProcessor.ProcessState.Running:
                        try
                        {
                            oDP.AbortDownload();
                            UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " was aborted.\n");
                            _log.Info("Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID + " aborted at user request.");
                            oDP_eUpdateVehicleStatus(oDP.sFleetID, oDP.sVehicleID, "Download aborted.");
                            bRecreateThread = true;
                            bWaitForUnitLogin = false;
                            GetEmailToAddress();
                            GetSMTPServerAddress();
                            if (_emailToAddress.Length > 0 && _smtpAddress.Length > 0)
                            {
                                string sSMTP = _smtpAddress;
                                if (sSMTP != "")
                                {
                                    string sFrom = ConfigurationManager.AppSettings["EMailSourceAddress"];
                                    if (sFrom.Length == 0)
                                        sFrom = "MTData.Listener@mtdata.com.au";
                                    string sTo = _emailToAddress;
                                    string sSubject = "Multiple Download Email Update -  Download to " + sUnit + " failed.";
                                    string sEmailMsg = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " was aborted.\n";
                                    Email.ProcessEmail(sSMTP, sTo, sFrom, sSubject, sEmailMsg, false);
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
                        }
                        break;
                    case DownloadProcessor.ProcessState.Successful:
                        try
                        {
                            UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " was sucessful, waiting for unit to log into server again.\n");
                            _log.Info("Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID + " was sucessful, waiting for unit to log into server again.");
                            oDP_eUpdateVehicleStatus(oDP.sFleetID, oDP.sVehicleID, " Download sucessful, waiting for unit to log into server again.");
                            bRecreateThread = false;
                            bWaitForUnitLogin = true;
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
                        }
                        break;
                    case DownloadProcessor.ProcessState.TimedOut:
                        try
                        {
                            UpdateConsoleText(DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " timed out - Will restart download when the unit next reports in.\n");
                            _log.Info("Download to Unit " + oDP.sFleetID + "/" + oDP.sVehicleID + " timed out - Will restart download when the unit next reports in.");
                            oDP_eUpdateVehicleStatus(oDP.sFleetID, oDP.sVehicleID, "Download timeout - Will restart download when the unit next reports in.");
                            bRecreateThread = true;
                            bWaitForUnitLogin = false;
                            GetEmailToAddress();
                            GetSMTPServerAddress();
                            if (_emailToAddress.Length > 0 && _smtpAddress.Length > 0)
                            {
                                string sSMTP = _smtpAddress;
                                if (sSMTP != "")
                                {
                                    string sFrom = ConfigurationManager.AppSettings["EMailSourceAddress"];
                                    if (sFrom.Length == 0)
                                        sFrom = "MTData.Listener@mtdata.com.au";
                                    string sTo = _emailToAddress;
                                    string sSubject = "Multiple Download Email Update -  Download to " + sUnit + " timed out.";
                                    string sEmailMsg = DateTime.Now.ToString("dd/MM/yyyy HH:mm:ss") + " - Download to " + sUnit + " timed out.\nThe server will try to download to the unit again.";
                                    Email.ProcessEmail(sSMTP, sTo, sFrom, sSubject, sEmailMsg, false);
                                }
                            }
                        }
                        catch (System.Exception ex)
                        {
                            _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
                        }
                        break;
                }
            }

            try
            {
                oDP = null;

                if (oDownloadToList != null)
                {
                    if (bRecreateThread)
                    {
                        lock (oDownloadToList.SyncRoot)
                        {
                            if (oDownloadToList.ContainsKey(sKey))
                            {
                                oDownloadToList.Remove(sKey);
                                EnhancedThread tDownloadToUnit = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(DownloadToUnit), sKey);
                                oDownloadToList.Add(sKey, tDownloadToUnit);
                            }
                        }
                    }
                    else
                    {
                        lock (oDownloadToList.SyncRoot)
                        {
                            if (oDownloadToList.ContainsKey(sKey))
                            {
                                oDownloadToList.Remove(sKey);
                            }
                        }
                        if (bWaitForUnitLogin)
                        {
                            lock (oWaitForLoginList.SyncRoot)
                            {
                                oWaitForLoginList.Add(sKey, null);
                            }
                            EnableSendToNextButton(true);
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "DownloadToUnit(EnhancedThread sender, object data)", ex);
            }
            lock (this.SyncRoot)
                iDownloadCount--;
            return null;
        }

        #endregion
        #region Utils
        private DownloadProcessor GetDownloadProcessor(GatewayProtocolPacket packet)
        {
            DownloadProcessor oDP = null;
            try
            {
                if (packet != null)
                {

                    if (rb302x.Checked)
                    {
                        #region Load 302x download settings
                        oDP = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.TrackingDevice, _serverTime_DateFormat);
                        try
                        {
                            _listener.iTrackingDeviceDownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryInterval"]);
                        }
                        catch (System.Exception)
                        {
                            _listener.iTrackingDeviceDownloadRetryInterval = 2;
                        }
                        if (_listener.iTrackingDeviceDownloadRetryInterval < 2) _listener.iTrackingDeviceDownloadRetryInterval = 2;
                        if (_listener.iTrackingDeviceDownloadRetryInterval > 20) _listener.iTrackingDeviceDownloadRetryInterval = 20;
                        iRetryInterval = _listener.iTrackingDeviceDownloadRetryInterval;
                        try
                        {
                            _listener.iTrackingDeviceDownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["TrackingDeviceDownloadRetryCount"]);
                        }
                        catch (System.Exception)
                        {
                            _listener.iTrackingDeviceDownloadRetryCount = 3;
                        }
                        if (_listener.iTrackingDeviceDownloadRetryCount < 3) _listener.iTrackingDeviceDownloadRetryCount = 3;
                        if (_listener.iTrackingDeviceDownloadRetryCount > 20) _listener.iTrackingDeviceDownloadRetryCount = 20;
                        iRetryCount = _listener.iTrackingDeviceDownloadRetryCount;
                        #endregion
                    }
                    else
                    {
                        if (rb1035.Checked)
                        {
                            #region Load Interface Box download settings.
                            oDP = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.InterfaceBox, _serverTime_DateFormat);
                            try
                            {
                                _listener.iInterfaceBoxDownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["InterfaceBoxDownloadRetryInterval"]);
                            }
                            catch (System.Exception)
                            {
                                _listener.iInterfaceBoxDownloadRetryInterval = 2;
                            }
                            if (_listener.iInterfaceBoxDownloadRetryInterval < 2) _listener.iInterfaceBoxDownloadRetryInterval = 2;
                            if (_listener.iInterfaceBoxDownloadRetryInterval > 10) _listener.iInterfaceBoxDownloadRetryInterval = 10;
                            iRetryInterval = _listener.iInterfaceBoxDownloadRetryInterval;
                            try
                            {
                                _listener.iInterfaceBoxDownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["InterfaceBoxDownloadRetryCount"]);
                            }
                            catch (System.Exception)
                            {
                                _listener.iInterfaceBoxDownloadRetryCount = 3;
                            }
                            if (_listener.iInterfaceBoxDownloadRetryCount < 3) _listener.iInterfaceBoxDownloadRetryCount = 3;
                            if (_listener.iInterfaceBoxDownloadRetryCount > 10) _listener.iInterfaceBoxDownloadRetryCount = 10;
                            iRetryCount = _listener.iInterfaceBoxDownloadRetryCount;
                            #endregion
                        }
                        else if (rbMDT.Checked)
                        {
                            #region Load MDT download settings.
                            oDP = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.MobileDataTerminal, _serverTime_DateFormat);
                            try
                            {
                                _listener.iDataTerminalDownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DataTerminalDownloadRetryInterval"]);
                            }
                            catch (System.Exception)
                            {
                                _listener.iDataTerminalDownloadRetryInterval = 5;
                            }
                            if (_listener.iDataTerminalDownloadRetryInterval < 5) _listener.iDataTerminalDownloadRetryInterval = 5;
                            if (_listener.iDataTerminalDownloadRetryInterval > 10) _listener.iDataTerminalDownloadRetryInterval = 10;
                            iRetryInterval = _listener.iDataTerminalDownloadRetryInterval;
                            try
                            {
                                _listener.iDataTerminalDownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DataTerminalDownloadRetryCount"]);
                            }
                            catch (System.Exception)
                            {
                                _listener.iDataTerminalDownloadRetryCount = 3;
                            }
                            if (_listener.iDataTerminalDownloadRetryCount < 3) _listener.iDataTerminalDownloadRetryCount = 3;
                            if (_listener.iDataTerminalDownloadRetryCount > 10) _listener.iDataTerminalDownloadRetryCount = 10;
                            iRetryCount = _listener.iDataTerminalDownloadRetryCount;
                            #endregion
                        }
                        else
                        {
                            #region Load 4000 download settings.
                            oDP = new DownloadProcessor(packet, _listener.mDBInterface, DownloadType.BinaryFile, _serverTime_DateFormat);
                            try
                            {
                                _listener.i4000DownloadRetryInterval = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["4000DownloadRetryInterval"]);
                            }
                            catch (System.Exception)
                            {
                                _listener.i4000DownloadRetryInterval = 2;
                            }
                            if (_listener.i4000DownloadRetryInterval < 2) _listener.i4000DownloadRetryInterval = 2;
                            if (_listener.i4000DownloadRetryInterval > 20) _listener.i4000DownloadRetryInterval = 20;
                            iRetryInterval = _listener.i4000DownloadRetryInterval;
                            try
                            {
                                _listener.i4000DownloadRetryCount = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["DataTerminalDownloadRetryCount"]);
                            }
                            catch (System.Exception)
                            {
                                _listener.i4000DownloadRetryCount = 3;
                            }
                            if (_listener.i4000DownloadRetryCount < 3) _listener.i4000DownloadRetryCount = 3;
                            if (_listener.i4000DownloadRetryCount > 20) _listener.i4000DownloadRetryCount = 20;
                            iRetryCount = _listener.i4000DownloadRetryCount;
                            #endregion

                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetDownloadProcessor(GatewayProtocolPacket packet)", ex);
            }
            return oDP;
        }

        private int GetNumberOfActiveDownloads()
        {
            int dRet = 0;
            try
            {
                lock (this.SyncRoot)
                    dRet = iDownloadCount;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + " GetNumberOfActiveDownloads()", ex);
            }
            return dRet;
        }
        #endregion
        #region Vehicle Status update events
        private delegate void UpdateDownloadStatusDelegate(string sFleetID, string sVehicleID, string sStatus);
        private void UpdateDownloadStatus(string sFleetID, string sVehicleID, string sStatus)
        {
            if (this.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { UpdateDownloadStatus(sFleetID, sVehicleID, sStatus); }));
            else
            {
                for (int X = 0; X < lvQueued.Items.Count; X++)
                {
                    if (lvQueued.Items[X].SubItems[0].Text == sFleetID && lvQueued.Items[X].SubItems[1].Text == sVehicleID)
                    {
                        lock (this.SyncRoot)
                            iAckTimer = 0;
                        lvQueued.Items[X].SubItems[2].Text = sStatus;
                        break;
                    }
                }
            }
        }
        private void oDP_eUpdateVehicleStatus(string sFleetID, string sVehicleID, string sStatus)
        {
            try
            {
                UpdateDownloadStatus(sFleetID, sVehicleID, sStatus);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "oDP_eUpdateVehicleStatus(string sFleetID, string sVehicleID, string sStatus)", ex);
            }
        }

        private void oDP_eVehicleDownloadComplete(string sFleetID, string sVehicleID)
        {
            string sKey = "";
            try
            {
                sKey = sFleetID.PadLeft(4, '0') + "~" + sVehicleID.PadLeft(4, '0');
                lock (oDownloadToList.SyncRoot)
                {
                    if (oDownloadToList.ContainsKey(sKey))
                    {
                        if (oDownloadToList[sKey] != null)
                        {
                            ((EnhancedThread)oDownloadToList[sKey]).Stop();
                        }
                        oDownloadToList.Remove(sKey);
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "oDP_eVehicleDownloadComplete(string sFleetID, string sVehicleID)", ex);
            }
        }

        #endregion
        #region Buton and form events
        private void btnSelectMotFile_Click(object sender, System.EventArgs e)
        {
            decimal iRunningCount = GetNumberOfActiveDownloads();
            int iTotalBlockCount = 0;
            long lThisAddress = 0;
            long lNextAddress = 0;
            int iPreferredDataLength = 250;

            try
            {
                if (iRunningCount > 0)
                {
                    MessageBox.Show(this, "Please stop active downloads before changing the mot file.", "MTData", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    txtFilename.Text = "";
                    OpenFileDialog imageFile = new OpenFileDialog();
                    mMotFile = null;
                    imageFile.CheckFileExists = true;
                    if (rb4000.Checked)
                    {
                        imageFile.DefaultExt = "gz";
                        imageFile.Filter = "Zipped Shell Script (*.gz)|*.gz|All Files (*.*)|*.*";
                    }
                    else
                    {
                        imageFile.DefaultExt = ".mot";
                        imageFile.Filter = "Motorola S-Record Files (*.mot)|*.mot";
                    }
                    imageFile.Title = "Select Image File To Download";
                    if (imageFile.ShowDialog() == DialogResult.Cancel) return;
                    chkContinueWithoutWaiting.Checked = false;
                    txtFilename.Text = imageFile.FileName;
                    if (!rb4000.Checked)
                    {
                        mMotFile = new MotorolaSRecordFile(imageFile.FileName);
                        if (!mMotFile.FileIsGood)
                        {
                            MessageBox.Show(this, mMotFile.sFileFailureReason, "MTData - File Open Failed", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            mMotFile = null;
                            return;
                        }
                        // A DATS MOT file begins at Line 2 (line 1 is the Start of S Record):
                        lThisAddress = mMotFile.GetAddressOfRow(2);
                        lNextAddress = 0;
                        byte[] dataBytes = new byte[0];

                        while (mMotFile.GetContiguousByteArray(lThisAddress, iPreferredDataLength, ref dataBytes, ref lNextAddress, true, false) == true)
                        {
                            // Create a count so we can do a pretty little progress bar!
                            iTotalBlockCount++;
                            // keep looping while there is data
                            lThisAddress = lNextAddress;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnSelectMotFile_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnSelectMotFile_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }
        private void frmMultipleDownload_Resize(object sender, System.EventArgs e)
        {
            if (lvQueued.InvokeRequired)
                BeginInvoke(new MethodInvoker(delegate() { frmMultipleDownload_Resize(sender, e); }));
            else
            {
                try
                {
                    lvQueued.Columns[2].Width = lvQueued.Width - 148;
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "frmMultipleDownload_Resize(object sender, System.EventArgs e)", ex);
                }
            }
        }

        private void btnMoveToNext_Click(object sender, System.EventArgs e)
        {
            try
            {
                lock (oWaitForLoginList.SyncRoot)
                {
                    oWaitForLoginList.Clear();
                }
                EnableSendToNextButton(false);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnMoveToNext_Click(object sender, System.EventArgs e)", ex);
                MessageBox.Show("Exception Details : \r\n\r\n" + sClassName + "btnMoveToNext_Click(object sender, System.EventArgs e)\r\n\r\n Error Message : " + ex.Message, "Error");
            }
        }

        #endregion
    }
}

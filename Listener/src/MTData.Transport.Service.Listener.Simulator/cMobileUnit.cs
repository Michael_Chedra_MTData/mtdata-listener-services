using System;
using System.Collections;
using System.Data;
using System.Threading;
using MTData.Common.Network;
using MTData.Transport.Gateway.Packet;
using log4net;
using MTData.Common.Queues;
using MTData.Common.Threading;

namespace MTData.Transport.Service.Listener.Simulator
{
    class cMobileUnit
    {
        private const string sClassName = "MTData.Mobile.Unit.Simulator.cMobileUnit.";
        private static ILog _log = LogManager.GetLogger(typeof(cMobileUnit));

        #region Private Vars
        private object SyncRoot = new object();
        private object StatsSyncRoot = new object();
        private EnhancedThread oSimThread = null;
        private EnhancedThread oListernRespThread = null;
        private MobileUnitConnection oConn = null;
        private ArrayList oPackets = null;
        private GatewayProtocolPacket mPacket = null;
        private int iSleepPeriod = 0;
        private string sIP = "";
        private string _sDateFormat = "";
        private int iPort = 0;
        private int iPacketPos = 0;
        private int iResendTimer = 0;
        private StandardQueue toListenerQ = null;
        private StandardQueue fromListenerQ = null;
        private bool bReplayAsCurrentTime = false;
        private bool bReplayAsTodaysDate = false;
        private bool bReplayAsDate = false;
        private DateTime dReplayDate = DateTime.MinValue;
        private bool bDontReplayWPs = false;
        private bool bLoop = false;
        private byte bFleetID = (byte)0x00;
        private uint iVehicleID = 0;
        private byte cOurSequence = (byte) 0x00;
        private byte cAckSequence = (byte) 0x00;
        private byte cConfigFileActive = (byte)0x00;
        private byte cConfigFileActiveVersionMajor = (byte)0x00;
        private byte cConfigFileActiveVersionMinor = (byte)0x00;
        private byte cRouteSetActive = (byte)0x00;
        private byte cRouteSetActiveVersionMajor = (byte)0x00;
        private byte cRouteSetActiveVersionMinor = (byte)0x00;
        private byte cRoutePointNumber = (byte)0x00;
        private byte cSetPointSetActive = (byte)0x00;
        private byte cSetPointSetActiveVersionMajor = (byte)0x00;
        private byte cSetPointSetActiveVersionMinor = (byte)0x00;
        private byte cScheduleActive = (byte)0x00;
        private byte cScheduleActiveVersionMajor = (byte)0x00;
        private byte cScheduleActiveVersionMinor = (byte)0x00;
        private bool bPingMode = false;
        private bool bPingReply = false;
        private int iResendTimeout = 0;
        private int iMaxRetries = 0;
        private byte[] bPriorityPacket = null;
        private bool bWaitForAck = false;
        private bool bWaitForConfigTotalActive = false;
        private bool bReplayAsTodayPlusOffset = false;
        private bool bReplayAsReportedTime = false;
        private int iReplayOffset = 0;
        #endregion
        #region Login Vars
        private int iHardwareType = 0;
        private string sSimNumber = "";
        private int iSlotNumber = 1;
        private int iPreferedSlotNumber = 1;
        private string sNetworkName = "";
        private string sIMIE = "";
        private long lSerialNumber = 0;
        private int iSoftwareVersion = 0;
        private byte cProgramMajor = (byte) 0x00;
        private int iProgramMinor = 0;
        private int iHardwareVersionNumber = 0;
        private long lIOBoxSerialNumber = 0;
        private byte cIOBoxProgramMajor = (byte)0x00;
        private byte cIOBoxProgramMinor = (byte)0x00;
        private int iIOBoxHardwareVersionNumber = 0;
        private long lMDTSerialNumber = 0;
        private int iMDTProgramMajor = (byte)0x00;
        private int iMDTProgramMinor = (byte)0x00;
        private int iMDTHardwareVer = 0;
        private int iLoginFlags = 0;
        #endregion
        #region Status Vars
        private string sCurStatus = "";
        private int iPacketCount = 0;
        private int iFirstRetry = 0;
        private int iSecondRetry = 0;
        private int iThirdRetry = 0;
        private int iFourthRetry = 0;
        private int iFifthRetry = 0;
        private int iPingCount = 0;
        #endregion

        public cMobileUnit(DataRow drUnit, ArrayList oPacketList, string sListenerIP, int iListenPort, int iSendInterval, bool bReplayInCurrentTime, bool bReplayWithTodaysDate, bool bReplayWithSpecifiedDate, DateTime dtReplayDate, bool bDontReplayWPReports, bool bLoopData, string sDateFormat, int iResendPeriod, int iNumberOfRetries, bool ReplayAsReportedTime, bool ReplayAsTodayPlusOffset, int ReplayOffset)
        {
            try
            {
                sIP = sListenerIP;
                iPort = iListenPort;
                iSleepPeriod = iSendInterval;
                oPackets = oPacketList;
                _sDateFormat = sDateFormat;
                bReplayAsCurrentTime = bReplayInCurrentTime;
                bReplayAsTodaysDate = bReplayWithTodaysDate;
                bReplayAsDate = bReplayWithSpecifiedDate;
                dReplayDate = new DateTime(dtReplayDate.Year, dtReplayDate.Month, dtReplayDate.Day, 0, 0, 0);
                bReplayAsReportedTime = ReplayAsReportedTime;
                bReplayAsTodayPlusOffset = ReplayAsTodayPlusOffset;
                iReplayOffset = ReplayOffset;
                bDontReplayWPs = bDontReplayWPReports;
                bLoop = bLoopData;
                iResendTimeout = iResendPeriod;
                iMaxRetries = iNumberOfRetries;
                #region Read the common values from the data row
                bFleetID = (byte)Convert.ToInt32(drUnit["FleetID"]);
                iVehicleID = Convert.ToUInt32(drUnit["VehicleID"]);
                #region Read the config values
                cConfigFileActive = (byte)Convert.ToInt32(drUnit["ConfigFileID"]);
                cConfigFileActiveVersionMajor = (byte)Convert.ToInt32(drUnit["ConfigVerMajor"]);
                cConfigFileActiveVersionMinor = (byte)Convert.ToInt32(drUnit["ConfigVerMinor"]);
                cRouteSetActive = (byte)0x00;
                cRouteSetActiveVersionMajor = (byte)0x00;
                cRouteSetActiveVersionMinor = (byte)0x00;
                cRoutePointNumber = (byte)0x00;
                cSetPointSetActive = (byte)Convert.ToInt32(drUnit["SetPointGroupID"]);
                cSetPointSetActiveVersionMajor = (byte)Convert.ToInt32(drUnit["SetPointVerMajor"]);
                cSetPointSetActiveVersionMinor = (byte)Convert.ToInt32(drUnit["SetPointVerMinor"]);
                cScheduleActive = (byte)0x00;
                cScheduleActiveVersionMajor = (byte)0x00;
                cScheduleActiveVersionMinor = (byte)0x00;
                #endregion
                #region Load the login values
                iHardwareType = Convert.ToInt32(drUnit["HardwareType"]);
                sSimNumber = Convert.ToString(drUnit["SimcardNumber"]);
                iSlotNumber = 1;
                iPreferedSlotNumber = 1;
                sNetworkName = Convert.ToString(drUnit["NetworkName"]);
                sIMIE = Convert.ToString(drUnit["IMIE"]);
                lSerialNumber = (long)Convert.ToInt64(drUnit["SerialNumber"]);
                iSoftwareVersion = Convert.ToInt32(drUnit["SoftwareVersion"]);
                cProgramMajor = (byte)Convert.ToInt32(drUnit["FWVerMajor"]);
                iProgramMinor = (byte)Convert.ToInt32(drUnit["FWVerMinor"]);
                iHardwareVersionNumber = (byte)Convert.ToInt32(drUnit["HWVer"]);
                lIOBoxSerialNumber = 0;
                cIOBoxProgramMajor = (byte)0x00;
                cIOBoxProgramMinor = (byte)0x00;
                iIOBoxHardwareVersionNumber = 0;
                lMDTSerialNumber = 0;
                iMDTProgramMajor = (byte)0x00;
                iMDTProgramMinor = (byte)0x00;
                iMDTHardwareVer = 0;
                iLoginFlags = 0;
                #endregion
                #endregion
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "cMobileUnit(DataRow drUnit, ArrayList oPackets, string sListenerIP, int iListenPort, int iSendInterval, bool bReplayInCurrentTime, bool bReplayWithTodaysDate, bool bReplayWithSpecifiedDate, DateTime dtReplayDate, bool bDontReplayWPReports, bool bLoopData)", ex);
            }
        }
        #region Thread Code
        public void Start()
        {
            try
            {
                if (oSimThread != null)
                {
                    oSimThread.Stop();
                    oSimThread = null;
                }
                if (oListernRespThread != null)
                {
                    oListernRespThread.Stop();
                    oListernRespThread = null;
                }
                if (oConn != null)
                {
                    oConn.Stop();
                    oConn = null;
                }

                iPacketPos = 0;
                toListenerQ = new StandardQueue();
                fromListenerQ = new StandardQueue();
                oConn = new MobileUnitConnection(sIP, iPort, toListenerQ, fromListenerQ);
                oSimThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(SimThread), null);
                oListernRespThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(ListenerRespThread), null);

                oConn.Start();
                oListernRespThread.Start();
                oSimThread.Start();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Start()", ex);
            }
        }
        public void Stop()
        {
            try
            {
                if (oSimThread != null)
                {
                    oSimThread.Stop();
                    oSimThread = null;
                }
                if (oListernRespThread != null)
                {
                    oListernRespThread.Stop();
                    oListernRespThread = null;
                }
                if (oConn != null)
                {
                    oConn.Stop();
                    oConn = null;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Stop()", ex);
            }
        }
        private object SimThread(EnhancedThread sender, object data)
        {
            byte[] bCurrentPacket = null;
            byte[] bPingPacket = null;
            int iRetryCount = 0;
            int iLocalResendTimer = 0;
            bool bLocalWaitFlag = false;
            bool bLocalPingFlag = false;
            bool bLocalPingReplyFlag = false;
            GatewayProtocolPacket oPingPacket = null;

            #region Create a ping packet
            oPingPacket = new GatewayProtocolPacket(_sDateFormat);
            oPingPacket.cMsgType = GatewayProtocolPacket.GSP_APP_PING;
            oPingPacket.cFleetId = bFleetID;
            oPingPacket.iVehicleId = iVehicleID;
            oPingPacket.cOurSequence = cOurSequence;
            oPingPacket.cAckSequence = cAckSequence;
            oPingPacket.mDataField = new byte[60];
            oPingPacket.Encode(ref bPingPacket);
            #endregion

            while (!sender.Stopping)
            {
                try
                {
                    lock (SyncRoot)
                    {
                        bLocalWaitFlag = bWaitForAck;
                        bLocalPingFlag = bPingMode;
                        bLocalPingReplyFlag = bPingReply;
                    }

                    if (!bLocalWaitFlag)
                    {
                        #region Packet sending processing
                        if (bPingMode)
                        {
                            #region Reset the resend timer and send a ping packet
                            lock (SyncRoot)
                            {
                                bWaitForAck = true;
                                iResendTimer = 0;
                            }
                            lock (toListenerQ.SyncRoot)
                                toListenerQ.Enqueue(bPingPacket);
                            lock (StatsSyncRoot)
                            {
                                sCurStatus = "Waiting for Ping Reply";
                                iPingCount++;
                            }
                            #endregion
                        }
                        else
                        {
                            if (bPriorityPacket != null)
                            {
                                lock (SyncRoot)
                                {
                                    if (bPriorityPacket[2] == GeneralGPPacket.GEN_ACK)
                                    {
                                        bWaitForAck = false;
                                        iResendTimer = 0;
                                        iRetryCount = 0;
                                    }
                                    else
                                    {
                                        bWaitForAck = true;
                                        iResendTimer = 0;
                                        iRetryCount = 0;
                                        bCurrentPacket = bPriorityPacket;
                                        bPriorityPacket = null;
                                    }
                                }
                                lock (toListenerQ.SyncRoot)
                                    toListenerQ.Enqueue(bCurrentPacket);
                                lock (StatsSyncRoot)
                                {
                                    sCurStatus = "Waiting for ACK on Priority Packet";
                                    iPacketCount++;
                                }
                            }
                            else
                            {
                                if (iPacketPos >= oPackets.Count)
                                    iPacketPos = 0;
                                mPacket = (GatewayProtocolPacket)oPackets[iPacketPos++];
                                if (bDontReplayWPs && (mPacket.cMsgType & (byte)0xA0) == (byte)0xA0)
                                {
                                    // Don't send WP packets
                                }
                                else
                                {
                                    #region Populate the template packet with this units details.
                                    mPacket.cFleetId = bFleetID;
                                    mPacket.iVehicleId = iVehicleID;
                                    mPacket.cOurSequence = cOurSequence;
                                    mPacket.cAckSequence = cAckSequence;
                                    mPacket.bAckRegardless = true;
                                    cOurSequence = cOurSequence++;
                                    if ((int)cOurSequence > 31)
                                        cOurSequence = (byte)0x00;

                                    if (mPacket is GeneralGPPacket)
                                    {
                                        PopulateAndEncodeGeneralGPPacket(ref bCurrentPacket);
                                    }
                                    else if (mPacket is MassDeclarationPacket)
                                    {
                                        PopulateAndEncodeMassPacket(ref bCurrentPacket);
                                    }
                                    else
                                    {
                                        mPacket.Encode(ref bCurrentPacket);
                                    }

                                    #endregion
                                    #region reset the timeout and retry counters and send the packet.
                                    
                                    if (mPacket.bAckImmediately) bCurrentPacket[2] |= GatewayProtocolPacket.PACKET_FLAG_ACKI;
                                    if (mPacket.bAckRegardless) bCurrentPacket[2] |= GatewayProtocolPacket.PACKET_FLAG_ACK_REGARDLESS;
                                    if (mPacket.bSendFlash) bCurrentPacket[2] |= GatewayProtocolPacket.PACKET_FLAG_SEND_FLASH;
                                    if (mPacket.bArchivalData) bCurrentPacket[2] |= GatewayProtocolPacket.PACKET_FLAG_ARCHIVAL_DATA;
                                    lock (SyncRoot)
                                    {
                                        bWaitForAck = true;
                                        iResendTimer = 0;
                                        iRetryCount = 0;
                                    }
                                    lock (toListenerQ.SyncRoot)
                                        toListenerQ.Enqueue(bCurrentPacket);
                                    lock (StatsSyncRoot)
                                    {
                                        sCurStatus = "Waiting for ACK";
                                        iPacketCount++;
                                    }
                                    #endregion
                                }
                            }
                        }
                        #endregion
                        Thread.Sleep(iSleepPeriod);
                    }
                    else
                    {
                        #region Waiting for Ack processing
                        if (bLocalPingFlag)
                        {
                            if (bLocalPingReplyFlag)
                            {
                                #region The server has replied to a ping request.
                                lock (SyncRoot)
                                {
                                    bPingMode = false;
                                    bPingReply = false;
                                    iResendTimer = 0;
                                    bWaitForAck = true;
                                }
                                lock (toListenerQ.SyncRoot)
                                    toListenerQ.Enqueue(bCurrentPacket);
                                #endregion
                            }
                            else
                            {
                                #region Increment the resend timer and resend the pings when required.
                                lock (SyncRoot)
                                {
                                    iResendTimer++;
                                    if (iResendTimer >= iResendTimeout)
                                    {
                                        iResendTimer = 0;
                                        bWaitForAck = false;
                                    }
                                    lock (StatsSyncRoot)
                                        sCurStatus = "Resending Ping Packet";
                                }
                                #endregion
                                Thread.Sleep(100);
                            }
                        }
                        else
                        {
                            if (bCurrentPacket != null)
                            {
                                #region Resend processing.
                                lock (SyncRoot)
                                {
                                    iResendTimer++;
                                    iLocalResendTimer = iResendTimer;
                                }
                                if (iLocalResendTimer >= iResendTimeout)
                                {
                                    iRetryCount++;
                                    lock (StatsSyncRoot)
                                    {
                                        if (iRetryCount < 6)
                                        {
                                            if (iRetryCount == 1) iFirstRetry++;
                                            else if (iRetryCount == 2) iSecondRetry++;
                                            else if (iRetryCount == 3) iThirdRetry++;
                                            else if (iRetryCount == 4) iFourthRetry++;
                                            else if (iRetryCount == 5) iFifthRetry++;
                                        }
                                        if (iRetryCount >= iMaxRetries)
                                            sCurStatus = "Switching to Ping Mode";
                                        else
                                            sCurStatus = "Resending Data Packet (Retry " + Convert.ToString(iRetryCount) + ")";
                                    }

                                    if (iRetryCount >= iMaxRetries)
                                    {
                                        #region Packet has retried too many times, switching to ping mode
                                        lock (SyncRoot)
                                        {
                                            iResendTimer = 0;
                                            iRetryCount = 0;
                                            bPingMode = true;
                                            bWaitForAck = false;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Resend the last packet again
                                        lock (SyncRoot)
                                        {
                                            iResendTimer = 0;
                                        }
                                        lock (toListenerQ.SyncRoot)
                                            toListenerQ.Enqueue(bCurrentPacket);
                                        #endregion
                                    }
                                }
                                #endregion
                                Thread.Sleep(100);
                            }
                            else
                            {
                                #region There is no current packet, so let the process pickup the next one.
                                lock (SyncRoot)
                                {
                                    bWaitForAck = false;
                                }
                                #endregion
                            }
                        }
                        #endregion
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "SimThread(EnhancedThread sender, object data)", ex);
                }
            }
            return null;
        }

        private void PopulateAndEncodeGeneralGPPacket(ref byte[] bCurrentPacket)
        {
            GeneralGPPacket gpPacket = (GeneralGPPacket)mPacket;
            gpPacket.iHardwareTypeInbound = (GeneralGPPacket.Alive_IMEIESN_HardwareType)iHardwareType;
            if (bReplayAsCurrentTime)
            {
                gpPacket.mFixClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);
                gpPacket.mCurrentClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);
            }
            if (bReplayAsTodaysDate)
            {
                DateTime dtTemp = new DateTime((int)gpPacket.mFixClock.iYear, (int)gpPacket.mFixClock.iMonth, (int)gpPacket.mFixClock.iDay, (int)gpPacket.mFixClock.iHour, (int)gpPacket.mFixClock.iMinute, (int)gpPacket.mFixClock.iSecond);
                dtTemp = dtTemp.ToLocalTime();
                DateTime dtAdjusted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second);
                dtAdjusted = dtAdjusted.ToUniversalTime();
                gpPacket.mFixClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
                gpPacket.mCurrentClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
            }
            if (bReplayAsTodayPlusOffset)
            {
                DateTime dtTemp = new DateTime((int)gpPacket.mFixClock.iYear, (int)gpPacket.mFixClock.iMonth, (int)gpPacket.mFixClock.iDay, (int)gpPacket.mFixClock.iHour, (int)gpPacket.mFixClock.iMinute, (int)gpPacket.mFixClock.iSecond);
                dtTemp = dtTemp.ToLocalTime();
                DateTime dtAdjusted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second);
                dtAdjusted = dtAdjusted.ToUniversalTime();
                dtAdjusted = dtAdjusted.AddMinutes(iReplayOffset);
                gpPacket.mFixClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
                gpPacket.mCurrentClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
            }
            if (bReplayAsDate)
            {
                gpPacket.mFixClock = new GPClock("GPSClock", new DateTime(dReplayDate.Year, dReplayDate.Month, dReplayDate.Day, (int)gpPacket.mFixClock.iHour, (int)gpPacket.mFixClock.iMinute, (int)gpPacket.mFixClock.iSecond), _sDateFormat);
                gpPacket.mCurrentClock = new GPClock("GPSClock", new DateTime(dReplayDate.Year, dReplayDate.Month, dReplayDate.Day, (int)gpPacket.mFixClock.iHour, (int)gpPacket.mFixClock.iMinute, (int)gpPacket.mFixClock.iSecond), _sDateFormat);
            }
            if (mPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_FULL)
            {
                // drUnit - FleetID, VehicleID, Fleet, Vehicle, SerialNumber, IMIE, CDMAESN, SimcardNumber, 
                //          HardwareType, FWVerMajor, FWVerMinor, HWVer, VerDetails, ConfigFileID, ConfigVerMajor, 
                //          ConfigVerMinor, SetPointGroupID, SetPointVerMajor, SetPointVerMinor
                gpPacket.iHardwareTypeInbound = (GeneralGPPacket.Alive_IMEIESN_HardwareType)iHardwareType;
                gpPacket.sSimNumber = sSimNumber;
                gpPacket.iSlotNumber = iSlotNumber;
                gpPacket.iPreferedSlotNumber = iPreferedSlotNumber;
                gpPacket.sNetworkName = sNetworkName;
                gpPacket.sIMEIInbound = sIMIE;
                gpPacket.lSerialNumber = lSerialNumber;
                gpPacket.iSoftwareVersion = iSoftwareVersion;
                gpPacket.cProgramMajor = cProgramMajor;
                gpPacket.iProgramMinor = iProgramMinor;
                gpPacket.iHardwareVersionNumber = iHardwareVersionNumber;
                gpPacket.lIOBoxSerialNumber = lIOBoxSerialNumber;
                gpPacket.cIOBoxProgramMajor = cIOBoxProgramMajor;
                gpPacket.cIOBoxProgramMinor = cIOBoxProgramMinor;
                gpPacket.iIOBoxHardwareVersionNumber = iIOBoxHardwareVersionNumber;
                gpPacket.lMDTSerialNumber = lMDTSerialNumber;
                gpPacket.iMDTProgramMajor = iMDTProgramMajor;
                gpPacket.iMDTProgramMinor = iMDTProgramMinor;
                gpPacket.iMDTHardwareVer = iMDTHardwareVer;
                gpPacket.iLoginFlags = iLoginFlags;
                lock (SyncRoot)
                {
                    bWaitForConfigTotalActive = true;
                }
            }
            gpPacket.Encode(ref bCurrentPacket);
        }

        private void PopulateAndEncodeMassPacket(ref byte[] bCurrentPacket)
        {
            MassDeclarationPacket massPacket = (MassDeclarationPacket)mPacket;
            if (bReplayAsCurrentTime)
            {
                massPacket.mFixClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);
                massPacket.mCurrentClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);
            }
            if (bReplayAsTodaysDate)
            {
                DateTime dtTemp = new DateTime((int)massPacket.mFixClock.iYear, (int)massPacket.mFixClock.iMonth, (int)massPacket.mFixClock.iDay, (int)massPacket.mFixClock.iHour, (int)massPacket.mFixClock.iMinute, (int)massPacket.mFixClock.iSecond);
                dtTemp = dtTemp.ToLocalTime();
                DateTime dtAdjusted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second);
                dtAdjusted = dtAdjusted.ToUniversalTime();
                massPacket.mFixClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
                massPacket.mCurrentClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
            }
            if (bReplayAsTodayPlusOffset)
            {
                DateTime dtTemp = new DateTime((int)massPacket.mFixClock.iYear, (int)massPacket.mFixClock.iMonth, (int)massPacket.mFixClock.iDay, (int)massPacket.mFixClock.iHour, (int)massPacket.mFixClock.iMinute, (int)massPacket.mFixClock.iSecond);
                dtTemp = dtTemp.ToLocalTime();
                DateTime dtAdjusted = new DateTime(DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day, dtTemp.Hour, dtTemp.Minute, dtTemp.Second);
                dtAdjusted = dtAdjusted.ToUniversalTime();
                dtAdjusted = dtAdjusted.AddMinutes(iReplayOffset);
                massPacket.mFixClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
                massPacket.mCurrentClock = new GPClock("GPSClock", dtAdjusted, _sDateFormat);
            }
            if (bReplayAsDate)
            {
                massPacket.mFixClock = new GPClock("GPSClock", new DateTime(dReplayDate.Year, dReplayDate.Month, dReplayDate.Day, (int)massPacket.mFixClock.iHour, (int)massPacket.mFixClock.iMinute, (int)massPacket.mFixClock.iSecond), _sDateFormat);
                massPacket.mCurrentClock = new GPClock("GPSClock", new DateTime(dReplayDate.Year, dReplayDate.Month, dReplayDate.Day, (int)massPacket.mFixClock.iHour, (int)massPacket.mFixClock.iMinute, (int)massPacket.mFixClock.iSecond), _sDateFormat);
            }
            massPacket.Encode(ref bCurrentPacket);
        }

        private object ListenerRespThread(EnhancedThread sender, object data)
        {
            byte[] bData = null;
            UDPPacket oListenerData = null;
            GatewayProtocolPacket oGPPacket = null;
            GatewayProtocolPacket returnGPPacket = null;
            ConfigTotalGPPacket mConfigPacket = null;
            int iConfigRetry = 0;
            int iPos = 0;
            while (!sender.Stopping)
            {
                try
                {
                    #region Check for incomming data
                    lock (fromListenerQ.SyncRoot)
                    {
                        if (fromListenerQ.Count > 0)
                            bData = (byte[])fromListenerQ.Dequeue();
                        else
                            bData = null;
                    }
                    #endregion
                    if (bData != null)
                    {
                        #region Process the incomming data
                        oListenerData = new UDPPacket(_sDateFormat);
                        oListenerData.Decode(bData);
                        for (int X = 0; X < oListenerData.mNumPackets; X++)
                        {
                            oGPPacket = oListenerData.mPackets[X];
                            cAckSequence = oGPPacket.cOurSequence;
                            switch (oGPPacket.cMsgType)
                            {
                                case GeneralGPPacket.GEN_ACK:
                                    lock (SyncRoot)
                                    {
                                        if (!bWaitForConfigTotalActive)
                                        {
                                            bWaitForAck = false;
                                            bPingMode = false;
                                        }
                                        else
                                        {
                                            iResendTimer = 0;
                                            bWaitForAck = true;
                                        }
                                    }
                                    break;
                                case GatewayProtocolPacket.GSP_APP_PING:
                                    lock (SyncRoot)
                                    {
                                        bWaitForAck = true;
                                        bPingReply = true;
                                    }
                                    break;
                                case GeneralGPPacket.GEN_RESET:
                                    cOurSequence = (byte)0x00;
                                    cAckSequence = (byte)0x00;
                                    lock (SyncRoot)
                                    {
                                        iPacketPos = 0;
                                        iResendTimer = 0;
                                        bWaitForAck = false;
                                    }
                                    break;
                                case ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE:
                                    iPos = 0;
                                    cConfigFileActive = oGPPacket.mDataField[iPos++];
                                    cConfigFileActiveVersionMajor = oGPPacket.mDataField[iPos++];
                                    cConfigFileActiveVersionMinor = oGPPacket.mDataField[iPos++];
                                    cRouteSetActive = oGPPacket.mDataField[iPos++];
                                    cRouteSetActiveVersionMajor = oGPPacket.mDataField[iPos++];
                                    cRouteSetActiveVersionMinor = oGPPacket.mDataField[iPos++];
                                    cRoutePointNumber = oGPPacket.mDataField[iPos++];
                                    cSetPointSetActive = oGPPacket.mDataField[iPos++];
                                    cSetPointSetActiveVersionMajor = oGPPacket.mDataField[iPos++];
                                    cSetPointSetActiveVersionMinor = oGPPacket.mDataField[iPos++];
                                    cScheduleActive = oGPPacket.mDataField[iPos++];
                                    cScheduleActiveVersionMajor = oGPPacket.mDataField[iPos++];
                                    cScheduleActiveVersionMinor = oGPPacket.mDataField[iPos++];

                                    mConfigPacket = new ConfigTotalGPPacket(_sDateFormat);
                                    mConfigPacket.cFleetId = bFleetID;
                                    mConfigPacket.iVehicleId = iVehicleID;
                                    mConfigPacket.cOurSequence = cOurSequence;
                                    mConfigPacket.cAckSequence = cAckSequence;
                                    if (iConfigRetry == 0)
                                    {
                                        #region First Time Around - Say we have no config
                                        iConfigRetry++;
                                        mConfigPacket.cConfigFileActive = 0;
                                        mConfigPacket.cConfigFileActiveVersionMajor = 0;
                                        mConfigPacket.cConfigFileActiveVersionMinor = 0;
                                        mConfigPacket.cSetPointSetActive = 0;
                                        mConfigPacket.cSetPointSetActiveVersionMajor = 0;
                                        mConfigPacket.cSetPointSetActiveVersionMinor = 0;
                                        cOurSequence = cOurSequence++;
                                        if ((int)cOurSequence > 31)
                                            cOurSequence = (byte)0x00;
                                        bData = new byte[ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE_LENGTH];
                                        mConfigPacket.Encode(ref bData);
                                        lock (SyncRoot)
                                        {
                                            bPriorityPacket = bData;
                                            bWaitForConfigTotalActive = false;
                                            bWaitForAck = false;
                                            bPingMode = false;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        #region Third Time Around - Say we have the correct config
                                        iConfigRetry = 0;
                                        mConfigPacket.cConfigFileActive = cConfigFileActive;
                                        mConfigPacket.cConfigFileActiveVersionMajor = cConfigFileActiveVersionMajor;
                                        mConfigPacket.cConfigFileActiveVersionMinor = cConfigFileActiveVersionMinor;
                                        mConfigPacket.cSetPointSetActive = cSetPointSetActive;
                                        mConfigPacket.cSetPointSetActiveVersionMajor = cSetPointSetActiveVersionMajor;
                                        mConfigPacket.cSetPointSetActiveVersionMinor = cSetPointSetActiveVersionMinor;
                                        cOurSequence = cOurSequence++;
                                        if ((int)cOurSequence > 31)
                                            cOurSequence = (byte)0x00;
                                        bData = new byte[ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE_LENGTH];
                                        mConfigPacket.Encode(ref bData);
                                        lock (SyncRoot)
                                        {
                                            bPriorityPacket = bData;
                                            bWaitForConfigTotalActive = false;
                                            bWaitForAck = false;
                                            bPingMode = false;
                                        }
                                        #endregion
                                    }
                                    break;
                                case ConfigFileGPPacket.CONFIG_NEW_FILE:
                                case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT:
                                    #region Send a ack for the config packet.
                                    returnGPPacket = new GeneralGPPacket(_sDateFormat);
                                    returnGPPacket.cMsgType = GeneralGPPacket.GEN_ACK;
                                    returnGPPacket.cFleetId = bFleetID;
                                    returnGPPacket.iVehicleId = iVehicleID;
                                    returnGPPacket.cOurSequence = cOurSequence;
                                    cOurSequence = cOurSequence++;
                                    if ((int)cOurSequence > 31)
                                        cOurSequence = (byte)0x00;
                                    returnGPPacket.cAckSequence = cAckSequence;
                                    returnGPPacket.Encode(ref bData);
                                    bData[2] = (byte)0x00;
                                    lock (toListenerQ.SyncRoot)
                                        toListenerQ.Enqueue(bData);
                                    #endregion

                                    if (iConfigRetry == 1)
                                    {
                                        #region Second Time Around - Say we have the correct config, but not Setpoint.
                                        iConfigRetry++;
                                        mConfigPacket.cConfigFileActive = cConfigFileActive;
                                        mConfigPacket.cConfigFileActiveVersionMajor = cConfigFileActiveVersionMajor;
                                        mConfigPacket.cConfigFileActiveVersionMinor = cConfigFileActiveVersionMinor;
                                        mConfigPacket.cSetPointSetActive = 0;
                                        mConfigPacket.cSetPointSetActiveVersionMajor = 0;
                                        mConfigPacket.cSetPointSetActiveVersionMinor = 0;
                                        cOurSequence = cOurSequence++;
                                        if ((int)cOurSequence > 31)
                                            cOurSequence = (byte)0x00;
                                        bData = new byte[ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE_LENGTH];
                                        mConfigPacket.Encode(ref bData);
                                        lock (SyncRoot)
                                        {
                                            bPriorityPacket = bData;
                                            bWaitForAck = false;
                                            bPingMode = false;
                                        }
                                        #endregion
                                    }
                                    else if (iConfigRetry == 2)
                                    {
                                        #region Third Time Around - Say we have the correct configs
                                        iConfigRetry = 0;
                                        mConfigPacket.cConfigFileActive = cConfigFileActive;
                                        mConfigPacket.cConfigFileActiveVersionMajor = cConfigFileActiveVersionMajor;
                                        mConfigPacket.cConfigFileActiveVersionMinor = cConfigFileActiveVersionMinor;
                                        mConfigPacket.cSetPointSetActive = cSetPointSetActive;
                                        mConfigPacket.cSetPointSetActiveVersionMajor = cSetPointSetActiveVersionMajor;
                                        mConfigPacket.cSetPointSetActiveVersionMinor = cSetPointSetActiveVersionMinor;
                                        cOurSequence = cOurSequence++;
                                        if ((int)cOurSequence > 31)
                                            cOurSequence = (byte)0x00;
                                        bData = new byte[ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE_LENGTH];
                                        mConfigPacket.Encode(ref bData);
                                        lock (SyncRoot)
                                        {
                                            bPriorityPacket = bData;
                                            bWaitForAck = false;
                                            bPingMode = false;
                                        }
                                        #endregion
                                    }
                                    else
                                    {
                                        returnGPPacket = new GeneralGPPacket(_sDateFormat);
                                        returnGPPacket.cMsgType = GeneralGPPacket.GEN_ACK;
                                        returnGPPacket.cFleetId = bFleetID;
                                        returnGPPacket.iVehicleId = iVehicleID;
                                        returnGPPacket.cOurSequence = cOurSequence;
                                        cOurSequence = cOurSequence++;
                                        if ((int)cOurSequence > 31)
                                            cOurSequence = (byte)0x00;
                                        returnGPPacket.cAckSequence = cAckSequence;
                                        returnGPPacket.Encode(ref bData);
                                        bData[2] = (byte)0x00;
                                        lock (SyncRoot)
                                        {
                                            bPriorityPacket = bData;
                                            bWaitForAck = false;
                                        }
                                    }
                                    break;
                                default:
                                    returnGPPacket = new GeneralGPPacket(_sDateFormat);
                                    returnGPPacket.cMsgType = GeneralGPPacket.GEN_ACK;
                                    returnGPPacket.cFleetId = bFleetID;
                                    returnGPPacket.iVehicleId = iVehicleID;
                                    returnGPPacket.cOurSequence = cOurSequence;
                                    cOurSequence = cOurSequence++;
                                    if ((int)cOurSequence > 31)
                                        cOurSequence = (byte)0x00;
                                    returnGPPacket.cAckSequence = cAckSequence;
                                    returnGPPacket.Encode(ref bData);
                                    bData[2] = (byte)0x00;
                                    lock (SyncRoot)
                                    {
                                        bPriorityPacket = bData;
                                        bWaitForAck = false;
                                    }
                                    break;
                            }
                        }
                        #endregion
                    }
                    else
                    {
                        Thread.Sleep(1000);
                    }
                }
                catch (System.Exception ex)
                {
                    _log.Error(sClassName + "ListenerRespThread(EnhancedThread sender, object data)", ex);
                }
            }
            return null;
        }
        #endregion

        public ulong GetHashKey()
        {
            ulong iRet = 0;
            try
            {
                iRet = cMobileUnit.CreateHashKey((int)bFleetID, Convert.ToInt32(iVehicleID));
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetHashKey()", ex);
            }
            return iRet;
        }
        public static ulong CreateHashKey(string sFleetID, string sVehicleID)
        {
            ulong iRet = 0;
            try
            {
                iRet = CreateHashKey(Convert.ToInt32(sFleetID), Convert.ToInt32(sVehicleID));
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return iRet;
        }
        public static ulong CreateHashKey(int iFleetID, int iVehicleID)
        {
            ulong iRet = 0;
            byte[] bResult = null;
            byte[] bConvert = null;
            try
            {
                bResult = new byte[8];
                bConvert = BitConverter.GetBytes(iVehicleID);
                for (int X = 0; X < 4; X++)
                    bResult[X] = bConvert[X];
                bConvert = BitConverter.GetBytes(iFleetID);
                for (int X = 0; X < 4; X++)
                    bResult[X + 4] = bConvert[X];
                iRet = BitConverter.ToUInt64(bResult, 0);
            }
            catch (System.Exception ex)
            {
                throw (ex);
            }
            return iRet;
        }
        public object[] GetSimStats()
        {
            object[] oRet = null;
            try
            {
                oRet = new object[8];
                lock (StatsSyncRoot)
                {
                    oRet[0] = sCurStatus;
                    oRet[1] = iPacketCount;
                    oRet[2] = iFirstRetry;
                    oRet[3] = iSecondRetry;
                    oRet[4] = iThirdRetry;
                    oRet[5] = iFourthRetry;
                    oRet[6] = iFifthRetry;
                    oRet[7] = iPingCount;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GetSimStats()", ex);
            }
            return oRet;
        }
        public void ResetSimStats()
        {
            try
            {
                lock (StatsSyncRoot)
                {
                    sCurStatus = "";
                    iPacketCount = 0;
                    iFirstRetry = 0;
                    iSecondRetry = 0;
                    iThirdRetry = 0;
                    iFourthRetry = 0;
                    iFifthRetry = 0;
                    iPingCount = 0;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "ResetSimStats()", ex);
            }
        }
    }
}

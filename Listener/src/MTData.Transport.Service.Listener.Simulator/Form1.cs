﻿using System;
using System.Collections;
using System.Data;
using System.IO;
using System.Net.Sockets;
using System.Windows.Forms;
using System.Threading;
using MTData.Transport.Gateway.Packet;
using MTData.Common.Utilities;
using log4net;
using MTData.Common.Threading;
using MTData.Common.Database;

namespace MTData.Transport.Service.Listener.Simulator
{
    public partial class Form1 : Form
    {
        private const string sClassName = "MTData.Mobile.Unit.Simulator.Form1.";
        private static ILog _log = LogManager.GetLogger(typeof(Form1));

        public delegate void RefreshListViewEvent();
        public event RefreshListViewEvent RefreshListView;
        private object SyncRoot = new object();
        private int i302xLoginFlags = 0;
        private int iIOBoxFWMajor = 0;
        private int iIOBoxFWMinor = 0;
        private int iHardwareVersionNumber = 0;
        private string _sDateFormat = "";
        private string sNetworkName = "";
        //private GatewayProtocolPacket mPacket = null;
        private DataTable dtVehicles = null;
        private ArrayList oPackets = null;
        #region GUI to Sims vars
        private EnhancedThread oLaunchSimThread = null;
        private EnhancedThread oSimStatsThread = null;
        private ArrayList oSimUnits = null;
        private Hashtable oSimStats = null;
        #endregion

        private enum LoginType
        {
            GEN_IM_ALIVE,
            IMEI_Login,
            CDMAESN_Login
        }

        public Form1()
        {
            InitializeComponent();
            log4net.Config.XmlConfigurator.Configure();
        }

        private void Form1_FormClosed(object sender, FormClosedEventArgs e)
        {

        }

        private void Form1_Load(object sender, EventArgs e)
        {
            int iSourceFleet = 0;
            int iSourceVehicle = 0;
            try
            {
                #region Read the config file values
                txtListenerIP.Text = System.Configuration.ConfigurationManager.AppSettings["ListenerServer"];
                numListenPort.Value = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["ListenerPort"]);
                _sDateFormat = System.Configuration.ConfigurationManager.AppSettings["DateTimeFormat"];
                txtDSN.Text = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["ConnectString"]);
                txtSourceDataDSN.Text = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["SourceConnectString"]);
                numSendInterval.Value = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["ReportInterval"]);
                numRetryPeriod.Value = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["RetryInterval"]);
                numNumOfRetries.Value = Convert.ToDecimal(System.Configuration.ConfigurationManager.AppSettings["RetryCount"]);
                if (System.Configuration.ConfigurationManager.AppSettings["ReplayAsCurrentTime"] == "true")
                {
                    chkReplayInCurrentTime.Checked = true;
                    chkReplayWithTodaysDate.Checked = false;
                    chkReplayWithSpecifiedDate.Checked = false;
                }
                else
                {
                    chkReplayInCurrentTime.Checked = false;
                }
                if (System.Configuration.ConfigurationManager.AppSettings["ReplayAsTodaysDate"] == "true")
                {
                    chkReplayInCurrentTime.Checked = false;
                    chkReplayWithTodaysDate.Checked = true;
                    chkReplayWithSpecifiedDate.Checked = false;
                }
                else
                {
                    chkReplayWithTodaysDate.Checked = false;
                }
                if (System.Configuration.ConfigurationManager.AppSettings["ReplayAsThisDate"] == "true")
                {
                    chkReplayInCurrentTime.Checked = false;
                    chkReplayWithTodaysDate.Checked = false;
                    chkReplayWithSpecifiedDate.Checked = true;
                }
                else
                {
                    chkReplayWithSpecifiedDate.Checked = false;
                }
                if (System.Configuration.ConfigurationManager.AppSettings["DontReplayArriveDeparts"] == "true")
                    chkDontReplayWPReports.Checked = true;
                else
                    chkDontReplayWPReports.Checked = false;

                if (System.Configuration.ConfigurationManager.AppSettings["LoopReplay"] == "true")
                    chkLoop.Checked = true;
                else
                    chkLoop.Checked = false;

                dtSourceEnd.Value = DateTime.Now;
                dtSourceStart.Value = dtSourceEnd.Value.AddDays(-3);
                #endregion


                if (System.Configuration.ConfigurationManager.AppSettings["AutoConnect"] == "true")
                {
                    #region Connect to the db and load the list settings
                    if (txtDSN.Text != "")
                        btnDBConnect_Click(null, null);
                    if (txtSourceDataDSN.Text != "")
                        btnConnectSourceDB_Click(null, null);

                    iSourceFleet = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SourceFleet"]);
                    iSourceVehicle = Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["SourceVehicle"]);
                    if (iSourceFleet > 0 && cmbFleet.Items.Count > 0)
                    {
                        for (int X = 0; X < cmbFleet.Items.Count; X++)
                        {
                            if (((NameIdPairing)cmbFleet.Items[X]).Id == iSourceFleet)
                            {
                                cmbFleet.SelectedIndex = X;
                                break;
                            }
                        }

                        if (cmbFleet.SelectedIndex >= 0 && iSourceVehicle > 0 && cmbVehicle.Items.Count > 0)
                        {
                            for (int X = 0; X < cmbVehicle.Items.Count; X++)
                            {
                                if (((NameIdPairing)cmbVehicle.Items[X]).Id == iSourceVehicle)
                                {
                                    cmbVehicle.SelectedIndex = X;
                                    break;
                                }
                            }
                            if (cmbFleet.SelectedIndex >= 0 && cmbVehicle.SelectedIndex >= 0)
                            {
                                btnLoadReplayData_Click(null, null);
                            }
                        }
                    }
                    #endregion
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Form1_Load(object sender, EventArgs e)", ex);
            }
        }

        private void cmbFleet_SelectedIndexChanged(object sender, EventArgs e)
        {
            NameIdPairing oItem = null;
            int iFleetID = 0;
            DataRow[] drVehicles = null;
            try
            {
                if (cmbFleet.SelectedItem != null && dtVehicles != null)
                {
                    cmbVehicle.Items.Clear();
                    cmbVehicle.SelectedItem = null;
                    cmbVehicle.Text = "";

                    oItem = (NameIdPairing)cmbFleet.SelectedItem;
                    iFleetID = oItem.Id;
                    drVehicles = dtVehicles.Select("FleetID = " + Convert.ToString(iFleetID), "DisplayName");
                    for (int X = 0; X < drVehicles.Length; X++)
                    {
                        oItem = new NameIdPairing(Convert.ToInt32(drVehicles[X]["ID"]), Convert.ToString(drVehicles[X]["DisplayName"]));
                        cmbVehicle.Items.Add(oItem);
                    }
                    if (cmbVehicle.Items.Count > 0)
                        cmbVehicle.SelectedIndex = 0;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "cmbFleet_SelectedIndexChanged(object sender, EventArgs e)", ex);
            }

        }

        private void btnDBConnect_Click(object sender, EventArgs e)
        {
            string[] sItem = null;
            string[] sVerDetails = null;
            ListViewItem oLVI = null;
            //NameIdPairing oItem = null;

            try
            {
                DataTable dtUnits = SingleDBConnection.ExecuteDT(txtDSN.Text, "EXEC [usp_GetTrackingUnitDetails]");
                lvUnits.Items.Clear();
                if (dtUnits != null)
                {
                    for (int X = 0; X < dtUnits.Rows.Count; X++)
                    {
                        try
                        {
                            sItem = new string[23];
                            sItem[1] = Convert.ToString(dtUnits.Rows[X]["FleetID"]);
                            sItem[2] = Convert.ToString(dtUnits.Rows[X]["VehicleID"]);
                            sItem[3] = Convert.ToString(dtUnits.Rows[X]["Fleet"]);
                            sItem[4] = Convert.ToString(dtUnits.Rows[X]["Vehicle"]);
                            sItem[5] = "";
                            sItem[6] = "0";
                            sItem[7] = "0";
                            sItem[8] = "0";
                            sItem[9] = "0";
                            sItem[10] = "0";
                            sItem[11] = "0";
                            sItem[12] = "0";
                            sItem[13] = Convert.ToString(dtUnits.Rows[X]["SerialNumber"]);
                            sItem[14] = Convert.ToString(dtUnits.Rows[X]["IMIE"]);
                            sItem[15] = Convert.ToString(dtUnits.Rows[X]["CDMAESN"]);
                            sItem[16] = Convert.ToString(dtUnits.Rows[X]["SimcardNumber"]);
                            sItem[17] = Convert.ToString(dtUnits.Rows[X]["HardwareType"]);
                            sItem[18] = Convert.ToString(dtUnits.Rows[X]["FWVerMajor"]) + "." + Convert.ToString(dtUnits.Rows[X]["FWVerMinor"]);
                            sItem[19] = Convert.ToString(dtUnits.Rows[X]["HWVer"]);
                            sVerDetails = Convert.ToString(dtUnits.Rows[X]["VerDetails"]).Split(",".ToCharArray());
                            
                            dtUnits.Rows[X]["ConfigFileID"] = 0;
                            dtUnits.Rows[X]["ConfigVerMajor"] = 0;
                            dtUnits.Rows[X]["ConfigVerMinor"] = 0;
                            dtUnits.Rows[X]["SetPointGroupID"] = 0;
                            dtUnits.Rows[X]["SetPointVerMajor"] = 0;
                            dtUnits.Rows[X]["SetPointVerMinor"] = 0;

                            if (sVerDetails.Length > 11)
                            {
                                dtUnits.Rows[X]["ConfigFileID"] = (sVerDetails[0].Trim() == "" ? 0 : Convert.ToInt32(sVerDetails[0]));
                                dtUnits.Rows[X]["ConfigVerMajor"] = (sVerDetails[1].Trim() == "" ? 0 : Convert.ToInt32(sVerDetails[1]));
                                dtUnits.Rows[X]["ConfigVerMinor"] = (sVerDetails[2].Trim() == "" ? 0 : Convert.ToInt32(sVerDetails[2]));
                                dtUnits.Rows[X]["SetPointGroupID"] = (sVerDetails[6].Trim() == "" ? 0 : Convert.ToInt32(sVerDetails[6]));
                                dtUnits.Rows[X]["SetPointVerMajor"] = (sVerDetails[7].Trim() == "" ? 0 : Convert.ToInt32(sVerDetails[7]));
                                dtUnits.Rows[X]["SetPointVerMinor"] = (sVerDetails[8].Trim() == "" ? 0 : Convert.ToInt32(sVerDetails[8]));
                                sItem[20] = sVerDetails[0] + " : " + sVerDetails[1] + "." + sVerDetails[2];
                                sItem[21] = sVerDetails[6] + " : " + sVerDetails[7] + "." + sVerDetails[8];
                                sItem[22] = sVerDetails[9] + " : " + sVerDetails[10] + "." + sVerDetails[11];
                            }
                            oLVI = new ListViewItem(sItem);
                            dtUnits.Rows[X].AcceptChanges();
                            oLVI.Tag = dtUnits.Rows[X];
                            lvUnits.Items.Add(oLVI);

                        }
                        catch (Exception)
                        {
                        }
                    }
                }
                grpReplayOptions.Enabled = true;
                btnDBConnect.Enabled = false;
                btnDBDisonnect.Enabled = true;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnDBConnect_Click(object sender, EventArgs e)", ex);
            }
        }

        private void btnDBDisonnect_Click(object sender, EventArgs e)
        {
            try
            {
                lvUnits.Items.Clear();
                btnDBConnect.Enabled = true;
                btnDBDisonnect.Enabled = false;
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnDBDisonnect_Click(object sender, EventArgs e)", ex);
            }
        }

        private void btnLoadTestData_Click(object sender, EventArgs e)
        {
            int iRestart = 1;
            DataTable dtReports = null;
            DataTable dtReasons = null;
            GatewayProtocolPacket mPacket = null;
            string sSQL = "";
            string sFleetID = "";
            string sVehicleID = "";
            string sStartTime = "";
            string sEndTime = "";
            DataRow drNew = null;

            try
            {
                iRestart = Convert.ToInt32(numSendGenImAlive.Value);
                oPackets = new ArrayList();
                sFleetID = Convert.ToString(((NameIdPairing)cmbFleet.SelectedItem).Id);
                sVehicleID = Convert.ToString(((NameIdPairing)cmbVehicle.SelectedItem).Id);
                sStartTime = dtSourceStart.Value.ToString("MM/dd/yyyy HH:mm:ss");
                sEndTime = dtSourceEnd.Value.ToString("MM/dd/yyyy HH:mm:ss");

                sSQL = "select vni.GPSTime, vni.ReasonID, isnull(vn.Speed, 0) as Speed, isnull(vn.Direction, 0) as Direction, isnull( vn.Latitude, 0) as Latitude, isnull( vn.Longitude, 0) as Longitude, isnull( vn.ButtonStatus, 0) as ButtonStatus, isnull( vn.InputStatus, 0) as InputStatus, isnull( vn.LightStatus, 0) as LightStatus, isnull( vn.OutputStatus, 0) as OutputStatus, isnull( vn.Status, 0) as Status, ";
                sSQL += "isnull(vn.MaxSpeed, 0) as MaxSpeed, isnull( vn.Samples, 0) as Samples, isnull( vn.SpeedAcc, 0) as SpeedAcc, isnull( vn.MaxF, 0) as MaxF, isnull( vn.MaxB, 0) as MaxB, isnull( vn.MaxLR, 0) as MaxLR, isnull( vn.BrakeHits, 0) as BrakeHits, isnull( vn.MaxEngCoolTemp, 0) as MaxEngCoolTemp, isnull( vn.GearPosition, 0) as GearPosition, isnull( vn.MaxEngSpeed, 0) as MaxEngSpeed, isnull( vn.MinOilPressure, 0) as MinOilPressure, isnull( vn.MaxEngOilTemp, 0) as MaxEngOilTemp, ";
                sSQL += "isnull(vn.FuelEconomy, 0) as FuelEconomy, isnull( vn.BatteryVolts, 0) as BatteryVolts, isnull( vn.Odometer, 0) as Odometer, isnull( vn.TotalEngHours, 0) as TotalEngHours, isnull( vn.TotalFuelUsed, 0) as TotalFuelUsed, isnull( vn.TripFuel, 0) as TripFuel, isnull( vn.Spare1, '0%0%0%0') as Spare1, isnull( vn.StatusVehicle, 0) as StatusVehicle, isnull( vn.BrakeUsageSeconds, 0) as BrakeUsageSeconds, ";
                sSQL += "isnull(vn.RPMZone1, 0) as RPMZone1, isnull( vn.RPMZone2, 0) as RPMZone2, isnull( vn.RPMZone3, 0) as RPMZone3, isnull( vn.RPMZone4, 0) as RPMZone4, isnull( vn.SpeedZone1, 0) as SpeedZone1, isnull( vn.SpeedZone2, 0) as SpeedZone2, isnull( vn.SpeedZone3, 0) as SpeedZone3, isnull(vn.SpeedZone4, 0) as SpeedZone4, isnull(vn.CANVehicleSpeed, 0) as CANVehicleSpeed ";
                sSQL += "from T_VehicleNotification vn INNER JOIN T_VN_Index vni on vn.ID = vni.TableID ";
                sSQL += "WHERE vni.FleetID = " + sFleetID + " and vni.VehicleID = " + sVehicleID + " AND vni.GPSTime BETWEEN cast('" + sStartTime + "' as datetime) AND cast('" + sEndTime + "' as datetime)";

                dtReports = SingleDBConnection.ExecuteDT(txtDSN.Text, sSQL);

                sSQL = "select ID from t_VehicleNotificationReason WHERE ID > 97 AND ID < 255 AND ID not In (110, 111, 191, 192, 200, 228)";
                dtReasons = SingleDBConnection.ExecuteDT(txtDSN.Text, sSQL);

                if (dtReports.Rows.Count > 0)
                {
                    #region Take a copy of the first record and create the Gen_StartUp, Gen_Im_Alive_Full and Config_Total_Active packets
                    drNew = dtReports.NewRow();
                    for (int X = 0; X < drNew.Table.Columns.Count; X++)
                    {
                        drNew[X] = dtReports.Rows[0][X];
                    }
                    drNew["ReasonID"] = (int)GeneralGPPacket.GEN_STARTUP;
                    mPacket = GeneratePacketData(drNew);
                    if (mPacket != null) oPackets.Add(mPacket);
                    drNew["ReasonID"] = (int)GeneralGPPacket.GEN_IM_ALIVE_FULL;
                    mPacket = GeneratePacketData(drNew);
                    if (mPacket != null) oPackets.Add(mPacket);
                    #endregion

                    for (int X = 0; X < dtReasons.Rows.Count; X++)
                    {
                        drNew["ReasonID"] = Convert.ToInt32(dtReasons.Rows[X]["ID"]);
                        mPacket = GeneratePacketData(drNew);
                        if (mPacket != null) oPackets.Add(mPacket);
                    }

                    MessageBox.Show(this, "Loaded " + Convert.ToString(dtReports.Rows.Count) + " records and created " + Convert.ToString(oPackets.Count) + " packets.");
                }
                else
                {
                    sStartTime = dtSourceStart.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    sEndTime = dtSourceEnd.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    MessageBox.Show(this, "No data was found for fleet " + sFleetID + " vehicle " + sVehicleID + " between " + sStartTime + " and " + sEndTime);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnLoadReplayData_Click(object sender, EventArgs e)", ex);
            }
        }

        private void btnLoadReplayData_Click(object sender, EventArgs e)
        {
            int iRestart = 1;
            int iRestartCounter = 1;
            DataTable dtReports = null;
            GatewayProtocolPacket mPacket = null;
            string sSQL = "";
            string sFleetID = "";
            string sVehicleID = "";
            string sStartTime = "";
            string sEndTime = "";
            DataRow drNew = null;

            try
            {
                iRestart = Convert.ToInt32(numSendGenImAlive.Value);
                oPackets = new ArrayList();
                sFleetID = Convert.ToString(((NameIdPairing)cmbFleet.SelectedItem).Id);
                sVehicleID = Convert.ToString(((NameIdPairing)cmbVehicle.SelectedItem).Id);
                sStartTime = dtSourceStart.Value.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss");
                sEndTime = dtSourceEnd.Value.ToUniversalTime().ToString("MM/dd/yyyy HH:mm:ss");

                sSQL = "select vni.GPSTime, vni.ReasonID, isnull(vn.Speed, 0) as Speed, isnull(vn.Direction, 0) as Direction, isnull( vn.Latitude, 0) as Latitude, isnull( vn.Longitude, 0) as Longitude, isnull( vn.ButtonStatus, 0) as ButtonStatus, isnull( vn.InputStatus, 0) as InputStatus, isnull( vn.LightStatus, 0) as LightStatus, isnull( vn.OutputStatus, 0) as OutputStatus, isnull( vn.Status, 0) as Status, ";
                sSQL += "isnull(vn.MaxSpeed, 0) as MaxSpeed, isnull( vn.Samples, 0) as Samples, isnull( vn.SpeedAcc, 0) as SpeedAcc, isnull( vn.MaxF, 0) as MaxF, isnull( vn.MaxB, 0) as MaxB, isnull( vn.MaxLR, 0) as MaxLR, isnull( vn.BrakeHits, 0) as BrakeHits, isnull( vn.MaxEngCoolTemp, 0) as MaxEngCoolTemp, isnull( vn.GearPosition, 0) as GearPosition, isnull( vn.MaxEngSpeed, 0) as MaxEngSpeed, isnull( vn.MinOilPressure, 0) as MinOilPressure, isnull( vn.MaxEngOilTemp, 0) as MaxEngOilTemp, ";
                sSQL += "isnull(vn.FuelEconomy, 0) as FuelEconomy, isnull( vn.BatteryVolts, 0) as BatteryVolts, isnull( vn.Odometer, 0) as Odometer, isnull( vn.TotalEngHours, 0) as TotalEngHours, isnull( vn.TotalFuelUsed, 0) as TotalFuelUsed, isnull( vn.TripFuel, 0) as TripFuel, isnull( vn.Spare1, '0%0%0%0') as Spare1, isnull( vn.StatusVehicle, 0) as StatusVehicle, isnull( vn.BrakeUsageSeconds, 0) as BrakeUsageSeconds, ";
                sSQL += "isnull(vn.RPMZone1, 0) as RPMZone1, isnull( vn.RPMZone2, 0) as RPMZone2, isnull( vn.RPMZone3, 0) as RPMZone3, isnull( vn.RPMZone4, 0) as RPMZone4, isnull( vn.SpeedZone1, 0) as SpeedZone1, isnull( vn.SpeedZone2, 0) as SpeedZone2, isnull( vn.SpeedZone3, 0) as SpeedZone3, isnull(vn.SpeedZone4, 0) as SpeedZone4, isnull(vn.CANVehicleSpeed, 0) as CANVehicleSpeed ";
                sSQL += "from T_VehicleNotification vn INNER JOIN T_VN_Index vni on vn.ID = vni.TableID ";
                sSQL += "WHERE vni.FleetID = " + sFleetID + " and vni.VehicleID = " + sVehicleID + " AND vni.GPSTime BETWEEN cast('" + sStartTime + "' as datetime) AND cast('" + sEndTime + "' as datetime)";

                dtReports = SingleDBConnection.ExecuteDT(txtSourceDataDSN.Text, sSQL);
                if (dtReports.Rows.Count > 0)
                {
                    #region Take a copy of the first record and create the Gen_StartUp, Gen_Im_Alive_Full and Config_Total_Active packets
                    drNew = dtReports.NewRow();
                    for (int X = 0; X < drNew.Table.Columns.Count; X++)
                    {
                        drNew[X] = dtReports.Rows[0][X];
                    }
                    drNew["ReasonID"] = (int)GeneralGPPacket.GEN_STARTUP;
                    mPacket = GeneratePacketData(drNew);
                    if (mPacket != null) oPackets.Add(mPacket);
                    drNew["ReasonID"] = (int)GeneralGPPacket.GEN_IM_ALIVE_FULL;
                    mPacket = GeneratePacketData(drNew);
                    if (mPacket != null) oPackets.Add(mPacket);
                    #endregion

                    for (int X = 0; X < dtReports.Rows.Count; X++)
                    {
                        #region Every X records, take a copy of the record and create the Gen_StartUp, Gen_Im_Alive_Full and Config_Total_Active packets
                        if (iRestart > 0 && iRestartCounter >= iRestart)
                        {
                            iRestartCounter = 0;
                            drNew = dtReports.NewRow();
                            for (int Y = 0; Y < drNew.Table.Columns.Count; Y++)
                            {
                                drNew[Y] = dtReports.Rows[X][Y];
                            }
                            drNew["ReasonID"] = (int)GeneralGPPacket.GEN_STARTUP;
                            mPacket = GeneratePacketData(drNew);
                            if (mPacket != null) oPackets.Add(mPacket);
                            drNew["ReasonID"] = (int)GeneralGPPacket.GEN_IM_ALIVE_FULL;
                            mPacket = GeneratePacketData(drNew);
                            if (mPacket != null) oPackets.Add(mPacket);
                        }
                        iRestartCounter++;
                        #endregion
                        #region Generate the packet form the values in the database.
                        mPacket = GeneratePacketData(dtReports.Rows[X]);
                        if (mPacket != null) oPackets.Add(mPacket);
                        #endregion
                    }
                    MessageBox.Show(this, "Loaded " + Convert.ToString(dtReports.Rows.Count) + " records and created " + Convert.ToString(oPackets.Count) + " packets.");
                }
                else
                {
                    sStartTime = dtSourceStart.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    sEndTime = dtSourceEnd.Value.ToString("dd/MM/yyyy HH:mm:ss");
                    MessageBox.Show(this, "No data was found for fleet " + sFleetID + " vehicle " + sVehicleID + " between " + sStartTime + " and " + sEndTime);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnLoadReplayData_Click(object sender, EventArgs e)", ex);
            }
        }

        private GatewayProtocolPacket GeneratePacketData(DataRow drData)
        {
            string sTemp = "";
            string[] sIOValues = null;

            try
            {
                byte messageId = (byte)Convert.ToInt32(drData["ReasonID"]);
                if (messageId == MassDeclarationPacket.MASS_DECLARATION)
                {
                    return CreateMassPacket(drData);
                }
                else
                {
                    #region Create a new packet
                    GeneralGPPacket mPacket = new GeneralGPPacket(_sDateFormat);
                    mPacket.cOurSequence = 0;
                    mPacket.cAckSequence = 0;
                    mPacket.cFleetId = 0;
                    mPacket.iVehicleId = 0;
                    mPacket.bAckImmediately = true;
                    mPacket.bAckRegardless = true;
                    mPacket.bArchivalData = false;
                    mPacket.bSendFlash = true;
                    mPacket.cCannedMessageNumber = (byte)0x00;
                    mPacket._cProgramMajor = (byte)0x00;
                    mPacket._iProgramMinor = (byte)0x00;
                    mPacket.cIOBoxProgramMajor = (byte)iIOBoxFWMajor;
                    mPacket.cIOBoxProgramMinor = (byte)iIOBoxFWMinor;
                    mPacket.iHardwareTypeInbound = GeneralGPPacket.Alive_IMEIESN_HardwareType.Platform_3026_IMEI;
                    mPacket.iHardwareVersionNumber = iHardwareVersionNumber;
                    mPacket.iLoginFlags = i302xLoginFlags;
                    mPacket.iPreferedSlotNumber = 1;
                    mPacket.lSerialNumber = 1;
                    mPacket.sNetworkName = sNetworkName;
                    mPacket.sSimNumber = "";

                    mPacket.cMsgType = (byte)Convert.ToInt32(drData["ReasonID"]);
                    mPacket.mFixClock = new GPClock("GPSClock", Convert.ToDateTime(drData["GPSTime"]), _sDateFormat);
                    mPacket.mCurrentClock = new GPClock("GPSClock", Convert.ToDateTime(drData["GPSTime"]), _sDateFormat);

                    //mPacket.mFixClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);
                    //mPacket.mCurrentClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);

                    mPacket.mFix = new GPPositionFix();
                    mPacket.mFix.iDirection = Convert.ToInt32(drData["Direction"]);
                    mPacket.mFix.cSpeed = (byte)Convert.ToInt32(drData["Speed"]);
                    mPacket.mFix.dLatitude = Convert.ToDecimal(drData["Latitude"]);
                    mPacket.mFix.dLongitude = Convert.ToDecimal(drData["Longitude"]);
                    mPacket.mFix.cFlags = (byte)0x00;

                    mPacket.mStatus = new GPStatus();
                    mPacket.mStatus.cButtonStatus = (byte)Convert.ToInt32(drData["ButtonStatus"]);
                    mPacket.mStatus.cExtendedStatusFlag = (byte)0x00;
                    mPacket.mStatus.cInputStatus = (byte)Convert.ToInt32(drData["InputStatus"]);
                    mPacket.mStatus.cLightStatus = (byte)Convert.ToInt32(drData["LightStatus"]);
                    mPacket.mStatus.cOutputStatus = (byte)Convert.ToInt32(drData["OutputStatus"]);
                    mPacket.mStatus.cSpareStatus = (byte)0x00;
                    mPacket.mStatus.cStatusFlag = (byte)Convert.ToInt32(drData["Status"]);
                    mPacket.cButtonNumber = (byte)0x00;
                    mPacket.cInputNumber = (byte)0x00;


                    mPacket.mDistance = new GPDistance();
                    mPacket.mDistance.cMaxSpeed = (byte)Convert.ToInt32(drData["MaxSpeed"]);
                    mPacket.mDistance.iSamples = Convert.ToInt32(drData["Samples"]);
                    mPacket.mDistance.lSpeedAccumulator = Convert.ToInt64(drData["SpeedAcc"]);

                    mPacket.mEngineData = new GPEngineData();
                    mPacket.mEngineData.fGForceBack = (float)Convert.ToDouble(drData["MaxF"]);
                    mPacket.mEngineData.fGForceFront = (float)Convert.ToDouble(drData["MaxB"]);
                    mPacket.mEngineData.fGForceLeftRight = (float)Convert.ToDouble(drData["MaxLR"]);
                    mPacket.mEngineData.iBrakeApplications = Convert.ToInt32(drData["BrakeHits"]);
                    mPacket.mEngineData.iCoolantTemperature = Convert.ToInt32(drData["MaxEngCoolTemp"]);
                    mPacket.mEngineData.iGear = Convert.ToInt32(drData["GearPosition"]);
                    mPacket.mEngineData.iMaxRPM = Convert.ToInt32(drData["MaxEngSpeed"]);
                    mPacket.mEngineData.iOilPressure = Convert.ToInt32(drData["MinOilPressure"]);
                    mPacket.mEngineData.iOilTemperature = Convert.ToInt32(drData["MaxEngOilTemp"]);
                    //            if (mPacket.mEngineData.fGForceBack == 0 && mPacket.mEngineData.fGForceFront == 0 && mPacket.mEngineData.fGForceLeftRight == 0
                    //                && mPacket.mEngineData.iBrakeApplications == 0 && mPacket.mEngineData.iCoolantTemperature == 0 && mPacket.mEngineData.iGear == 0
                    //                && mPacket.mEngineData.iMaxRPM == 0 && mPacket.mEngineData.iOilPressure == 0 && mPacket.mEngineData.iOilTemperature == 0)


                    mPacket.mEngineSummaryData = new GPEngineSummaryData();
                    mPacket.mEngineSummaryData.fFuelEconomy = Convert.ToDouble(drData["FuelEconomy"]);
                    mPacket.mEngineSummaryData.iBatteryVoltage = Convert.ToInt32(drData["BatteryVolts"]);
                    mPacket.mEngineSummaryData.iOdometer = Convert.ToInt32(drData["Odometer"]);
                    mPacket.mEngineSummaryData.iTotalEngineHours = Convert.ToInt32(drData["TotalEngHours"]);
                    mPacket.mEngineSummaryData.iTotalFuel = Convert.ToInt32(drData["TotalFuelUsed"]);
                    mPacket.mEngineSummaryData.iTripFuel = Convert.ToInt32(drData["TripFuel"]);

                    mPacket.mExtendedValues = new GPExtendedValues();
                    sTemp = Convert.ToString(drData["Spare1"]);
                    if (sTemp.Length > 0 && sTemp.IndexOf("%") > 0)
                    {
                        sIOValues = sTemp.Split("%".ToCharArray());
                        mPacket.mExtendedValues.iValue1 = Convert.ToInt32(sIOValues[0]);
                        mPacket.mExtendedValues.iValue2 = Convert.ToInt32(sIOValues[1]);
                        mPacket.mExtendedValues.iValue3 = Convert.ToInt32(sIOValues[2]);
                        mPacket.mExtendedValues.iValue4 = Convert.ToInt32(sIOValues[3]);
                    }

                    mPacket.mExtendedVariableDetails = new GBVariablePacketExtended59();
                    mPacket.mExtendedVariableDetails.CurrentRouteCheckPointIndex = 0;
                    mPacket.mExtendedVariableDetails.CurrentSetPointGroup = 0;
                    mPacket.mExtendedVariableDetails.CurrentSetPointNumber = 0;
                    mPacket.mExtendedVariableDetails.CurrentVehicleRouteScheduleID = 0;
                    mPacket.mExtendedVariableDetails.UnitStatus = Convert.ToInt64(drData["StatusVehicle"]);
                    mPacket.mExtendedVariableDetails.Refrigeration = null;

                    mPacket.mImieData = new cIMIEData();
                    mPacket.mImieData.i_FleetID = 0;
                    mPacket.mImieData.i_VehicleID = 0;
                    mPacket.mImieData.i_HardwareType = 0;
                    mPacket.mImieData.i_IOBoxVerMajor = 0;
                    mPacket.mImieData.i_IOBoxVerMinor = 0;
                    mPacket.mImieData.i_SoftwareVer = 0;
                    mPacket.mImieData.i_SoftwareVerMajor = 0;
                    mPacket.mImieData.i_SoftwareVerMinor = 0;
                    mPacket.mImieData.s_CDMAESN = "";
                    mPacket.mImieData.s_IMIE = "";
                    mPacket.mImieData.s_SimNumber = "";

                    mPacket.mTrailerTrack = new GPTrailerTrack();
                    mPacket.mTrailerTrack.iTrailerCount = 0;

                    mPacket.mTransportExtraDetails = new GPTransportExtraDetails();
                    mPacket.mTransportExtraDetails.fFuelEconomy = (float)Convert.ToDouble(drData["FuelEconomy"]);
                    mPacket.mTransportExtraDetails.iBatteryVolts = Convert.ToInt32(drData["BatteryVolts"]);
                    mPacket.mTransportExtraDetails.iBrakeUsageSeconds = Convert.ToInt32(drData["BrakeUsageSeconds"]);
                    mPacket.mTransportExtraDetails.iCANVehicleSpeed = Convert.ToInt32(drData["CANVehicleSpeed"]);
                    mPacket.mTransportExtraDetails.iEngineHours = Convert.ToInt32(drData["TotalEngHours"]);
                    mPacket.mTransportExtraDetails.iOdometer = Convert.ToUInt32(drData["Odometer"]);
                    mPacket.mTransportExtraDetails.iRPMZone1 = Convert.ToInt32(drData["RPMZone1"]);
                    mPacket.mTransportExtraDetails.iRPMZone2 = Convert.ToInt32(drData["RPMZone2"]);
                    mPacket.mTransportExtraDetails.iRPMZone3 = Convert.ToInt32(drData["RPMZone3"]);
                    mPacket.mTransportExtraDetails.iRPMZone4 = Convert.ToInt32(drData["RPMZone4"]);
                    mPacket.mTransportExtraDetails.iSpeedZone1 = Convert.ToInt32(drData["SpeedZone1"]);
                    mPacket.mTransportExtraDetails.iSpeedZone2 = Convert.ToInt32(drData["SpeedZone2"]);
                    mPacket.mTransportExtraDetails.iSpeedZone3 = Convert.ToInt32(drData["SpeedZone3"]);
                    mPacket.mTransportExtraDetails.iSpeedZone4 = Convert.ToInt32(drData["SpeedZone4"]);
                    mPacket.mTransportExtraDetails.iTotalFuelUsed = Convert.ToInt32(drData["TotalFuelUsed"]);
                    #endregion
                    return mPacket;
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "GeneratePacketData(DataRow drData)", ex);
            }
            return null;
        }

        private GatewayProtocolPacket CreateMassPacket(DataRow drData)
        {
            MassDeclarationPacket mPacket = new MassDeclarationPacket(_sDateFormat);
            mPacket.cOurSequence = 0;
            mPacket.cAckSequence = 0;
            mPacket.cFleetId = 0;
            mPacket.iVehicleId = 0;
            mPacket.bAckImmediately = true;
            mPacket.bAckRegardless = true;
            mPacket.bArchivalData = false;
            mPacket.bSendFlash = true;

            mPacket.cMsgType = (byte)Convert.ToInt32(drData["ReasonID"]);
            mPacket.mFixClock = new GPClock("GPSClock", Convert.ToDateTime(drData["GPSTime"]), _sDateFormat);
            mPacket.mCurrentClock = new GPClock("GPSClock", Convert.ToDateTime(drData["GPSTime"]), _sDateFormat);

            //mPacket.mFixClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);
            //mPacket.mCurrentClock = new GPClock("GPSClock", System.DateTime.Now.ToUniversalTime(), _sDateFormat);

            mPacket.mFix = new GPPositionFix();
            mPacket.mFix.iDirection = Convert.ToInt32(drData["Direction"]);
            mPacket.mFix.cSpeed = (byte)Convert.ToInt32(drData["Speed"]);
            mPacket.mFix.dLatitude = Convert.ToDecimal(drData["Latitude"]);
            mPacket.mFix.dLongitude = Convert.ToDecimal(drData["Longitude"]);
            mPacket.mFix.cFlags = (byte)0x00;

            mPacket.mStatus = new GPStatus();
            mPacket.mStatus.cButtonStatus = (byte)Convert.ToInt32(drData["ButtonStatus"]);
            mPacket.mStatus.cExtendedStatusFlag = (byte)0x00;
            mPacket.mStatus.cInputStatus = (byte)Convert.ToInt32(drData["InputStatus"]);
            mPacket.mStatus.cLightStatus = (byte)Convert.ToInt32(drData["LightStatus"]);
            mPacket.mStatus.cOutputStatus = (byte)Convert.ToInt32(drData["OutputStatus"]);
            mPacket.mStatus.cSpareStatus = (byte)0x00;
            mPacket.mStatus.cStatusFlag = (byte)Convert.ToInt32(drData["Status"]);

            mPacket.mDistance = new GPDistance();
            mPacket.mDistance.cMaxSpeed = (byte)Convert.ToInt32(drData["MaxSpeed"]);
            mPacket.mDistance.iSamples = Convert.ToInt32(drData["Samples"]);
            mPacket.mDistance.lSpeedAccumulator = Convert.ToInt64(drData["SpeedAcc"]);

            string sSQL = string.Format("select * from T_MassDeclaration where FleetID = {0} and VehicleID = {1} and GPSTime = '{2}'",
                ((NameIdPairing)cmbFleet.SelectedItem).Id, ((NameIdPairing)cmbVehicle.SelectedItem).Id, Convert.ToDateTime(drData["GPSTime"]).ToString("MM/dd/yy HH:mm:ss"));

            DataTable massTable = SingleDBConnection.ExecuteDT(txtDSN.Text, sSQL);
            if (massTable.Rows.Count > 0)
            {
                DataRow row = massTable.Rows[0];
                mPacket.ScalesState = (MassDeclarationPacket.ScaleState)Convert.ToDouble(row["ScalesState"]);
                mPacket.ScalesType = (MassDeclarationPacket.ScaleType)Convert.ToDouble(row["ScalesType"]);
                mPacket.TotalMass = (float)Convert.ToDouble(row["TotalMass"]);
                mPacket.MassAxleGroup1 = (float)Convert.ToDouble(row["MassAxleGroup1"]);
                mPacket.MassAxleGroup2 = (float)Convert.ToDouble(row["MassAxleGroup2"]);
                mPacket.MassAxleGroup3 = (float)Convert.ToDouble(row["MassAxleGroup3"]);
                mPacket.MassAxleGroup4 = (float)Convert.ToDouble(row["MassAxleGroup4"]);
                mPacket.MassAxleGroup5 = (float)Convert.ToDouble(row["MassAxleGroup5"]);
                mPacket.MassAxleGroup6 = (float)Convert.ToDouble(row["MassAxleGroup6"]);
                mPacket.MassAxleGroup7 = (float)Convert.ToDouble(row["MassAxleGroup7"]);
                mPacket.AxleGroupType1 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType1"]);
                mPacket.AxleGroupType2 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType2"]);
                mPacket.AxleGroupType3 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType3"]);
                mPacket.AxleGroupType4 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType4"]);
                mPacket.AxleGroupType5 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType5"]);
                mPacket.AxleGroupType6 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType6"]);
                mPacket.AxleGroupType7 = (MassDeclarationPacket.SectionType)Convert.ToDouble(row["AxleGroupType7"]);
                mPacket.TicketId = row["TicketId"].ToString();
                mPacket.LegID = (row["LegID"] == DBNull.Value ? -1 : Convert.ToInt32(row["LegID"]));
            }
            return mPacket;
        }

        private void btnSend_Click(object sender, EventArgs e)
        {
            ArrayList oUnits = null;
            DataRow drUnit = null;
            cMobileUnit oSimUnit = null;

            try
            {
                for (int X = 0; X < lvUnits.Items.Count; X++)
                {
                    if (lvUnits.Items[X].Checked)
                    {
                        drUnit = (DataRow)lvUnits.Items[X].Tag;
                        oSimUnit = new cMobileUnit(drUnit, oPackets, txtListenerIP.Text, (int)numListenPort.Value, (int)numSendInterval.Value, chkReplayInCurrentTime.Checked, chkReplayWithTodaysDate.Checked, chkReplayWithSpecifiedDate.Checked, dtReplayDate.Value, chkDontReplayWPReports.Checked, chkLoop.Checked, _sDateFormat, ((int)numRetryPeriod.Value / 100), (int)numNumOfRetries.Value, chkReplayAsReportedTime.Checked, chkReplayAsTodayPlusOffset.Checked, Convert.ToInt32(numReplayOffset.Value));
                        if (oUnits == null)
                            oUnits = new ArrayList();
                        oUnits.Add(oSimUnit);
                    }
                }

                if (oSimStatsThread != null)
                {
                    oSimStatsThread.Stop();
                    oSimStatsThread = null;
                }

                oLaunchSimThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(StartSimulatorsThread), oUnits);
                oLaunchSimThread.Start();

                if (RefreshListView != null)
                    RefreshListView -= new RefreshListViewEvent(Form1_RefreshListView);
                RefreshListView += new RefreshListViewEvent(Form1_RefreshListView);

                oSimStatsThread = new EnhancedThread(new EnhancedThread.EnhancedThreadStart(MonitorUnitStatsThread), null);
                oSimStatsThread.Start();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnSend_Click(object sender, EventArgs e)", ex);
            }
        }

        void Form1_RefreshListView()
        {
            try
            {
                this.Refresh();
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Form1_RefreshListView()", ex);
            }

        }

        private object StartSimulatorsThread(EnhancedThread sender, object data)
        {
            ArrayList oNewUnits = null;
            cMobileUnit oSimUnit = null;
            System.Random mRandomGenerator = null;

            try
            {
                oNewUnits = (ArrayList)data;
                mRandomGenerator = new Random();

                lock (SyncRoot)
                {
                    if (oSimUnits != null)
                    {
                        if (oSimUnits.Count > 0)
                        {
                            for (int X = oSimUnits.Count - 1; X >= 0; X--)
                            {
                                _log.Info("Stopping Unit Sim Thread #" + Convert.ToString(X));
                                oSimUnit = (cMobileUnit)oSimUnits[X];
                                oSimUnits.RemoveAt(X);
                                oSimUnit.Stop();
                                oSimUnit = null;
                            }
                        }
                    }
                    oSimUnits = new ArrayList();
                    for (int X = 0; X < oNewUnits.Count; X++)
                    {
                        oSimUnits.Add((cMobileUnit)oNewUnits[X]);
                    }
                }

                for (int X = 0; X < oSimUnits.Count; X++)
                {
                    _log.Info("Starting Unit Sim Thread #" + Convert.ToString(X) + " of " + Convert.ToString(oSimUnits.Count));
                    ((cMobileUnit)oSimUnits[X]).Start();
                    int iWaitPeriod = mRandomGenerator.Next();
                    iWaitPeriod = (iWaitPeriod % 10000);
                    if (iWaitPeriod < 1000)
                        iWaitPeriod = 1000;
                    Thread.Sleep(iWaitPeriod);
                }

            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "StartSimulatorsThread(EnhancedThread sender, object data)", ex);
            }
            return null;
        }

        private object MonitorUnitStatsThread(EnhancedThread sender, object data)
        {
            object[] iUnitStats = null;
            ulong iUnitHashKey = 0;
            bool bUpdate = false;
            try
            {
                while (!sender.Stopping)
                {
                    try
                    {
                        bUpdate = false;
                        lock (SyncRoot)
                        {
                            if (oSimUnits != null)
                            {
                                if (oSimStats == null)
                                    oSimStats = new Hashtable();
                                for (int X = 0; X < oSimUnits.Count; X++)
                                {
                                    iUnitStats = ((cMobileUnit)oSimUnits[X]).GetSimStats();
                                    iUnitHashKey = ((cMobileUnit)oSimUnits[X]).GetHashKey();
                                    if (oSimStats.ContainsKey(iUnitHashKey))
                                    {
                                        oSimStats.Remove(iUnitHashKey);
                                    }
                                    oSimStats.Add(iUnitHashKey, iUnitStats);
                                    bUpdate = true;
                                }
                            }
                        }
                    }
                    catch (System.Exception exThread)
                    {
                        _log.Error(sClassName + "MonitorUnitStatsThread(EnhancedThread sender, object data) - Thread", exThread);
                    }
                    if (bUpdate && RefreshListView != null)
                        this.Invalidate();
                    Thread.Sleep(1000);
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "MonitorUnitStatsThread(EnhancedThread sender, object data)", ex);
            }
            return null;
        }

        private void Form1_Paint(object sender, PaintEventArgs e)
        {
            ulong iHashKey = 0;
            object[] iValues = null;

            try
            {
                lock (SyncRoot)
                {
                    if (oSimStats != null)
                    {
                        if (oSimStats.Count > 0)
                        {
                            for (int X = 0; X < lvUnits.Items.Count; X++)
                            {
                                iHashKey = cMobileUnit.CreateHashKey(lvUnits.Items[X].SubItems[1].Text, lvUnits.Items[X].SubItems[2].Text);
                                if (oSimStats.ContainsKey(iHashKey))
                                {
                                    iValues = (object[])oSimStats[iHashKey];
                                    lvUnits.Items[X].SubItems[5].Text = (string)iValues[0];
                                    lvUnits.Items[X].SubItems[6].Text = Convert.ToString(iValues[1]);
                                    lvUnits.Items[X].SubItems[7].Text = Convert.ToString(iValues[2]);
                                    lvUnits.Items[X].SubItems[8].Text = Convert.ToString(iValues[3]);
                                    lvUnits.Items[X].SubItems[9].Text = Convert.ToString(iValues[4]);
                                    lvUnits.Items[X].SubItems[10].Text = Convert.ToString(iValues[5]);
                                    lvUnits.Items[X].SubItems[11].Text = Convert.ToString(iValues[6]);
                                    lvUnits.Items[X].SubItems[12].Text = Convert.ToString(iValues[7]);
                                }
                            }
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "Form1_Paint(object sender, PaintEventArgs e)", ex);
            }
        }

        private void btnStopSim_Click(object sender, EventArgs e)
        {
            cMobileUnit oSimUnit = null;
            try
            {
                if (oSimStatsThread != null)
                {
                    oSimStatsThread.Stop();
                    oSimStatsThread = null;
                }
                if (oSimUnits != null)
                {
                    if (oSimUnits.Count > 0)
                    {
                        for (int X = oSimUnits.Count - 1; X >= 0; X--)
                        {
                            _log.Info("Stopping Unit Sim Thread #" + Convert.ToString(X));
                            oSimUnit = (cMobileUnit)oSimUnits[X];
                            oSimUnits.RemoveAt(X);
                            oSimUnit.Stop();
                            oSimUnit = null;
                        }
                    }
                }
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnStopSim_Click(object sender, EventArgs e)", ex);
            }
        }

        private void chkReplayWithTodaysDate_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkReplayAsReportedTime_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void chkReplayInCurrentTime_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void btnSelectAll_Click(object sender, EventArgs e)
        {
            for (int X = 0; X < lvUnits.Items.Count; X++)
            {
                lvUnits.Items[X].Checked = true;
            }
        }

        private void btnUnSelectAll_Click(object sender, EventArgs e)
        {
            for (int X = 0; X < lvUnits.Items.Count; X++)
            {
                lvUnits.Items[X].Checked = false;
            }
        }

        private void btnSendUnitPacket_Click(object sender, EventArgs e)
        {
            SendBytes(txtRawBytes.Text, txtIPAddress.Text, Convert.ToInt32(numPort.Value));
        }

        private void btnSendString_Click(object sender, EventArgs e)
        {
            byte[] bData = System.Text.ASCIIEncoding.ASCII.GetBytes(txtUpdateString.Text);
            UdpClient oUDP = new UdpClient();
            oUDP.Send(bData, bData.Length, txtRSListenerIP.Text, Convert.ToInt32(numRSListenerPort.Value));
            oUDP = null;
        }

        private void btnSendBytes_Click(object sender, EventArgs e)
        {
            SendBytes(txtRSUpdateBytes.Text, txtRSListenerIP.Text, Convert.ToInt32(numRSListenerPort.Value));
        }

        private void SendBytes(string sData, string listenerIP, int listenerPort)
        {
            if (sData.Contains(","))
                sData = sData.Replace(",", "-");
            string[] sByteArray = sData.Split("-".ToCharArray());
            byte[] bData = new byte[sByteArray.Length];

            for (int X = 0; X < sByteArray.Length; X++)
            {
                byte[] bConvert = new byte[4];
                bConvert = BitConverter.GetBytes(Convert.ToInt32(sByteArray[X], 16));
                bData[X] = bConvert[0];
            }
            UdpClient oUDP = new UdpClient();
            oUDP.Send(bData, bData.Length, listenerIP, listenerPort);
            oUDP = null;
        }
        private void btnProcessPacket_Click(object sender, EventArgs e)
        {
            GatewayProtocolPacket aPacket = null;
            UDPPacket latestUdpPacket = null;

            string[] sByteArray = null;
            byte[] bData = null;
            byte[] bConvert = new byte[4];
            int iState = 0;
            string sTemp = "";
            string sTempNum = "";
            MemoryStream oMS = null;

            if (!txtRawBytes.Text.Contains(",") && !txtRawBytes.Text.Contains("-"))
            {
                for (int X = 0; X < txtRawBytes.Text.Length; X += 2)
                {
                    sTemp += txtRawBytes.Text.Substring(X, 2) + "-";
                }
                if (sTemp.Length > 0)
                    txtRawBytes.Text = sTemp.Substring(0, sTemp.Length - 1);
                else
                    txtRawBytes.Text = "";
                sTemp = "";
            }
            if (txtRawBytes.Text.Contains(",") && !txtRawBytes.Text.Contains("-"))
                txtRawBytes.Text = txtRawBytes.Text.Replace(",", "-");
            if (txtRawBytes.Text.Substring(0, 3) == "[1]")
            {
                sTemp = txtRawBytes.Text;
                oMS = new MemoryStream();
                for (int X = 0; X < sTemp.Length; X++)
                {
                    switch (iState)
                    {
                        #region State Machine
                        case 0: //Looking for '[' or a char value
                            if (sTemp.Substring(X, 1) == "[")
                                iState = 1;
                            else
                            {
                                oMS.WriteByte((byte)Convert.ToChar(sTemp.Substring(X, 1)));
                                X++;
                                iState = 3;
                            }
                            break;
                        case 1: // Looking for a number value or a space
                            sTempNum = "";
                            if (sTemp.Substring(X, 1) == " ")
                            {
                                oMS.WriteByte((byte)Convert.ToChar("["));
                                iState = 3;
                            }
                            else
                            {
                                sTempNum += sTemp.Substring(X, 1);
                                iState = 2;
                            }
                            break;
                        case 2: // Convert the char to byte value
                            if (sTemp.Substring(X, 1) == "]")
                            {
                                oMS.WriteByte((byte)Convert.ToInt32(sTempNum));
                                X++;
                                iState = 3;
                            }
                            else
                            {
                                sTempNum += sTemp.Substring(X, 1);
                            }
                            break;
                        case 3: // Looking for the - seperating char
                            if (sTemp.Substring(X, 1) == "-")
                            {
                                iState = 0;
                                X++;
                            }
                            break;
                        #endregion
                    }
                }
                bData = oMS.ToArray();
            }
            else
            {
                sByteArray = txtRawBytes.Text.Split("-".ToCharArray());
                bData = new byte[sByteArray.Length];

                for (int X = 0; X < sByteArray.Length; X++)
                {
                    bConvert = new byte[4];
                    bConvert = BitConverter.GetBytes(Convert.ToInt32(sByteArray[X].Trim(), 16));
                    bData[X] = bConvert[0];
                }
            }
            latestUdpPacket = new UDPPacket("dd/MM/yyyy HH:mm:ss");
            string errMsg = latestUdpPacket.Decode(bData);
            if (errMsg != null)
            {
                txtResult.Text = errMsg;
            }
            else
            {
                aPacket = latestUdpPacket.mPackets[0];

                switch (aPacket.cMsgType & 0xF0)
                {
                    case GeneralGPPacket.GEN_NO_GPS_PACKET_MASK:
                    case GeneralGPPacket.GEN_PACKET_MASK:
                    case GeneralGPPacket.STATUS_PACKET_MASK:
                    case GeneralGPPacket.ENGINE_PACKET_MASK:
                    case GeneralGPPacket.BARREL_PACKET_MASK:
                    case GeneralGPPacket.EXTENDED_IO_PACKET_MASK:
                    case GeneralGPPacket.ZONE_ALERT_MESSAGES:
                    case GeneralGPPacket.STATUS_VEHICLE_PACKET_MASK:
                        if (aPacket.cMsgType == DriverPointsConfigError.DRIVER_POINTS_CONFIG_ERROR)
                        {
                            DriverPointsConfigError errorPacket = new DriverPointsConfigError(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = "Driver Points Config Error: " + errorPacket.Error;
                        }
                        else
                        {
                            GeneralGPPacket gpPacket = new GeneralGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            if (gpPacket.bIsPopulated)
                            {
                                txtResult.Text = gpPacket.ToDisplayString();
                            }
                            else
                            {
                                // find out the problem
                                txtResult.Text = gpPacket.sConstructorDecodeError;
                            }
                        }
                        break;
                    case RoutePtSetPtGPPacket.SETPT_PACKET_MASK:
                        RoutePtSetPtGPPacket pointPacket = new RoutePtSetPtGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                        if (pointPacket.bIsPopulated)
                        {
                            txtResult.Text = pointPacket.ToDisplayString();
                        }
                        else
                            txtResult.Text = pointPacket.sConstructorDecodeError; // find out the problem
                        break;
                    case RoutingModuleGPPacket.ROUTES_PACKET_MASK:
                        RoutingModuleGPPacket routePacket = new RoutingModuleGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                        if (routePacket.bIsPopulated)
                        {
                            txtResult.Text = routePacket.ToDisplayString();
                        }
                        else
                            txtResult.Text = routePacket.sConstructorDecodeError; // find out the problem
                        break;
                    case ConfigTotalGPPacket.CONFIG_PACKET_MASK:

                        switch (aPacket.cMsgType)
                        {
                            case ConfigTotalGPPacket.CONFIG_TOTAL_ACTIVE:
                                ConfigTotalGPPacket confPacket = new ConfigTotalGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                                if (confPacket.bIsPopulated)
                                    txtResult.Text = confPacket.ToDisplayString();
                                else
                                    txtResult.Text = confPacket.sConstructorDecodeError; // find out the problem
                                break;

                            case ConfigNetworkInfoGPPacket.CONFIG_NETWORK_INFO:
                                txtResult.Text = "Unit acknowledged Change of IP Address packet.";
                                break;

                            case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON:
                            case ConfigNewSetPtGPPacket.CONFIG_NEW_SETPT_POLYGON_OLD:
                                ConfigNewSetPtGPPacket SetPointPolygonSegment = new ConfigNewSetPtGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss", (byte)0, (byte)0);
                                txtResult.Text = SetPointPolygonSegment.ToDisplayString();
                                break;
                            case DriverPointsRuleRequest.DRIVER_POINTS_RULE_REQUEST:
                                DriverPointsRuleRequest dr = new DriverPointsRuleRequest(aPacket, "dd/MM/yyyy HH:mm:ss");
                                if (dr.bIsPopulated)
                                {
                                    txtResult.Text = dr.ToDisplayString();
                                }
                                else
                                {
                                    txtResult.Text = dr.ConstructorDecodeError;
                                }
                                break;
                            default:
                                txtResult.Text = "Unhandled Config Message Type";
                                break;
                        }
                        break;

                    case GatewayProtocolPacket.SYSTEM_PACKET_MASK:
                        txtResult.Text = aPacket.ToDisplayString();
                        break;

                    case GPSHistoryGPPacket.GPS_HISTORY_PACKET_MASK:
                        if (aPacket.cMsgType == ConfigWatchList.CONFIG_WATCH_LIST)
                        {
                            ConfigWatchList wlPacket = new ConfigWatchList(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = wlPacket.ToDisplayString();
                        }
                        else if (aPacket.cMsgType == GPSHistoryGPPacket.GPS_HISTORY)
                        {
                            GPSHistoryGPPacket hstPacket = new GPSHistoryGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = hstPacket.ToDisplayString();
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_DATAACCUMLATOR_METRICS)
                        {
                            GPDriverDataAccumulator ddaPacket = new GPDriverDataAccumulator(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = ddaPacket.ToDisplayString();
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_PERFORMANCE_METRICS)
                        {
                            GPDriverPerformanceMetrics dpmPacket = new GPDriverPerformanceMetrics(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = dpmPacket.ToDisplayString();
                        }
                        else if (aPacket.cMsgType == GeneralGPPacket.DRIVER_OVERSPEED_REPORT || aPacket.cMsgType == GeneralGPPacket.DRIVER_PERSONAL_KMS || aPacket.cMsgType == GeneralGPPacket.GEN_IM_ALIVE_IMIE || aPacket.cMsgType == GeneralGPPacket.REFRIG_REPORT || aPacket.cMsgType == GeneralGPPacket.GEN_TAMPER || aPacket.cMsgType == GeneralGPPacket.GEN_ECM_OVERSPEED)
                        {
                            GeneralGPPacket gpRPacket = new GeneralGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            if (gpRPacket.bIsPopulated)
                                txtResult.Text = gpRPacket.ToDisplayString();
                            else
                                txtResult.Text = gpRPacket.sConstructorDecodeError;
                        }
                        break;
                    case MassDeclarationPacket.MASS_PACKET_MASK:
                        if (aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_START || aPacket.cMsgType == GeneralGPPacket.GEN_G_FORCES_SUSTAINED_END)
                        {
                            GeneralGPPacket gpPacket = new GeneralGPPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            if (gpPacket.bIsPopulated)
                                txtResult.Text = gpPacket.ToDisplayString();
                            else
                                txtResult.Text = gpPacket.sConstructorDecodeError;
                        }
                        else if (aPacket.cMsgType == DriverPointsRuleBreakPacket.DRIVER_POINTS_RULE_BROKEN)
                        {
                            DriverPointsRuleBreakPacket dp = new DriverPointsRuleBreakPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = dp.ToDisplayString();
                        }
                        else if (aPacket.cMsgType == GenericPacket.GENERIC_PACKET)
                        {
                            GenericPacket packet = new GenericPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = packet.ToDisplayString();
                        }
                        else if (aPacket.cMsgType == MassDeclarationPacket.MASS_DECLARATION)
                        {
                            MassDeclarationPacket mass = new MassDeclarationPacket(aPacket, "dd/MM/yyyy HH:mm:ss");
                            txtResult.Text = mass.ToDisplayString();
                        }
                        break;
                    default:
                        txtResult.Text = "Unknown Message Type";
                        break;
                }
            }
        }

        private void btnDisconnectSourceDSN_Click(object sender, EventArgs e)
        {
            grpReplayOptions.Enabled = false;

        }

        private void btnConnectSourceDB_Click(object sender, EventArgs e)
        {
            //string[] sItem = null;
            //string[] sVerDetails = null;
            //ListViewItem oLVI = null;
            NameIdPairing oItem = null;

            try
            {
                dtVehicles = null;
                cmbFleet.Items.Clear();
                cmbFleet.SelectedItem = null;
                cmbFleet.Text = "";
                DataTable dtFleets = SingleDBConnection.ExecuteDT(txtSourceDataDSN.Text, "SELECT ID, Name FROM T_Fleet WHERE ID > 0 ORDER BY Name");
                if (dtFleets != null)
                {
                    for (int X = 0; X < dtFleets.Rows.Count; X++)
                    {
                        oItem = new NameIdPairing(Convert.ToInt32(dtFleets.Rows[X]["ID"]), Convert.ToString(dtFleets.Rows[X]["Name"]));
                        cmbFleet.Items.Add(oItem);
                    }
                    if (cmbFleet.Items.Count > 0)
                        cmbFleet.SelectedIndex = 0;
                }
                dtVehicles = SingleDBConnection.ExecuteDT(txtSourceDataDSN.Text, "SELECT FleetID, ID, DisplayName FROM T_Vehicle WHERE FleetID > 0 ORDER BY DisplayName");
                cmbFleet_SelectedIndexChanged(null, null);
            }
            catch (System.Exception ex)
            {
                _log.Error(sClassName + "btnDBConnect_Click(object sender, EventArgs e)", ex);
            }
        }
    }
}
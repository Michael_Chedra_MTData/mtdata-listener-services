﻿namespace MTData.Transport.Service.Listener.Simulator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.grpReplayOptions = new System.Windows.Forms.GroupBox();
            this.btnDisconnectSourceDSN = new System.Windows.Forms.Button();
            this.btnConnectSourceDB = new System.Windows.Forms.Button();
            this.txtSourceDataDSN = new System.Windows.Forms.TextBox();
            this.label17 = new System.Windows.Forms.Label();
            this.btnUnSelectAll = new System.Windows.Forms.Button();
            this.btnSelectAll = new System.Windows.Forms.Button();
            this.numReplayOffset = new System.Windows.Forms.NumericUpDown();
            this.chkReplayAsTodayPlusOffset = new System.Windows.Forms.CheckBox();
            this.btnLoadTestData = new System.Windows.Forms.Button();
            this.numNumOfRetries = new System.Windows.Forms.NumericUpDown();
            this.label11 = new System.Windows.Forms.Label();
            this.numRetryPeriod = new System.Windows.Forms.NumericUpDown();
            this.label10 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.numSendGenImAlive = new System.Windows.Forms.NumericUpDown();
            this.label4 = new System.Windows.Forms.Label();
            this.cmbVehicle = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbFleet = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.lvUnits = new System.Windows.Forms.ListView();
            this.columnCheck = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader1 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader2 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader3 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader4 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader14 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader15 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader16 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader17 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader18 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader19 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader20 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader21 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader5 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader6 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader7 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader8 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader9 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader10 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader11 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader12 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.columnHeader13 = ((System.Windows.Forms.ColumnHeader)(new System.Windows.Forms.ColumnHeader()));
            this.numSendInterval = new System.Windows.Forms.NumericUpDown();
            this.btnLoadReplayData = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.btnClearReplayData = new System.Windows.Forms.Button();
            this.chkLoop = new System.Windows.Forms.CheckBox();
            this.numListenPort = new System.Windows.Forms.NumericUpDown();
            this.txtListenerIP = new System.Windows.Forms.TextBox();
            this.chkDontReplayWPReports = new System.Windows.Forms.CheckBox();
            this.label8 = new System.Windows.Forms.Label();
            this.dtReplayDate = new System.Windows.Forms.DateTimePicker();
            this.dtSourceStart = new System.Windows.Forms.DateTimePicker();
            this.chkReplayWithSpecifiedDate = new System.Windows.Forms.CheckBox();
            this.dtSourceEnd = new System.Windows.Forms.DateTimePicker();
            this.chkReplayWithTodaysDate = new System.Windows.Forms.CheckBox();
            this.chkReplayAsReportedTime = new System.Windows.Forms.CheckBox();
            this.label5 = new System.Windows.Forms.Label();
            this.chkReplayInCurrentTime = new System.Windows.Forms.CheckBox();
            this.label6 = new System.Windows.Forms.Label();
            this.btnDBDisonnect = new System.Windows.Forms.Button();
            this.btnDBConnect = new System.Windows.Forms.Button();
            this.txtDSN = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnSend = new System.Windows.Forms.Button();
            this.btnStopSim = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.txtResult = new System.Windows.Forms.RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtRawBytes = new System.Windows.Forms.TextBox();
            this.numPort = new System.Windows.Forms.NumericUpDown();
            this.label13 = new System.Windows.Forms.Label();
            this.txtIPAddress = new System.Windows.Forms.TextBox();
            this.btnProcessPacket = new System.Windows.Forms.Button();
            this.btnSendUnitPacket = new System.Windows.Forms.Button();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.btnSendString = new System.Windows.Forms.Button();
            this.label16 = new System.Windows.Forms.Label();
            this.txtRSUpdateBytes = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUpdateString = new System.Windows.Forms.TextBox();
            this.numRSListenerPort = new System.Windows.Forms.NumericUpDown();
            this.txtRSListenerIP = new System.Windows.Forms.TextBox();
            this.btnSendBytes = new System.Windows.Forms.Button();
            this.groupBox1.SuspendLayout();
            this.grpReplayOptions.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReplayOffset)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNumOfRetries)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRetryPeriod)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSendGenImAlive)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSendInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numListenPort)).BeginInit();
            this.tabControl1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).BeginInit();
            this.tabPage3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRSListenerPort)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.grpReplayOptions);
            this.groupBox1.Controls.Add(this.btnDBDisonnect);
            this.groupBox1.Controls.Add(this.btnDBConnect);
            this.groupBox1.Controls.Add(this.txtDSN);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1315, 407);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // grpReplayOptions
            // 
            this.grpReplayOptions.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.grpReplayOptions.Controls.Add(this.btnDisconnectSourceDSN);
            this.grpReplayOptions.Controls.Add(this.btnConnectSourceDB);
            this.grpReplayOptions.Controls.Add(this.txtSourceDataDSN);
            this.grpReplayOptions.Controls.Add(this.label17);
            this.grpReplayOptions.Controls.Add(this.btnUnSelectAll);
            this.grpReplayOptions.Controls.Add(this.btnSelectAll);
            this.grpReplayOptions.Controls.Add(this.numReplayOffset);
            this.grpReplayOptions.Controls.Add(this.chkReplayAsTodayPlusOffset);
            this.grpReplayOptions.Controls.Add(this.btnLoadTestData);
            this.grpReplayOptions.Controls.Add(this.numNumOfRetries);
            this.grpReplayOptions.Controls.Add(this.label11);
            this.grpReplayOptions.Controls.Add(this.numRetryPeriod);
            this.grpReplayOptions.Controls.Add(this.label10);
            this.grpReplayOptions.Controls.Add(this.label7);
            this.grpReplayOptions.Controls.Add(this.numSendGenImAlive);
            this.grpReplayOptions.Controls.Add(this.label4);
            this.grpReplayOptions.Controls.Add(this.cmbVehicle);
            this.grpReplayOptions.Controls.Add(this.label3);
            this.grpReplayOptions.Controls.Add(this.cmbFleet);
            this.grpReplayOptions.Controls.Add(this.label2);
            this.grpReplayOptions.Controls.Add(this.lvUnits);
            this.grpReplayOptions.Controls.Add(this.numSendInterval);
            this.grpReplayOptions.Controls.Add(this.btnLoadReplayData);
            this.grpReplayOptions.Controls.Add(this.label9);
            this.grpReplayOptions.Controls.Add(this.btnClearReplayData);
            this.grpReplayOptions.Controls.Add(this.chkLoop);
            this.grpReplayOptions.Controls.Add(this.numListenPort);
            this.grpReplayOptions.Controls.Add(this.txtListenerIP);
            this.grpReplayOptions.Controls.Add(this.chkDontReplayWPReports);
            this.grpReplayOptions.Controls.Add(this.label8);
            this.grpReplayOptions.Controls.Add(this.dtReplayDate);
            this.grpReplayOptions.Controls.Add(this.dtSourceStart);
            this.grpReplayOptions.Controls.Add(this.chkReplayWithSpecifiedDate);
            this.grpReplayOptions.Controls.Add(this.dtSourceEnd);
            this.grpReplayOptions.Controls.Add(this.chkReplayWithTodaysDate);
            this.grpReplayOptions.Controls.Add(this.chkReplayAsReportedTime);
            this.grpReplayOptions.Controls.Add(this.label5);
            this.grpReplayOptions.Controls.Add(this.chkReplayInCurrentTime);
            this.grpReplayOptions.Controls.Add(this.label6);
            this.grpReplayOptions.Enabled = false;
            this.grpReplayOptions.Location = new System.Drawing.Point(13, 48);
            this.grpReplayOptions.Name = "grpReplayOptions";
            this.grpReplayOptions.Size = new System.Drawing.Size(1297, 353);
            this.grpReplayOptions.TabIndex = 3;
            this.grpReplayOptions.TabStop = false;
            // 
            // btnDisconnectSourceDSN
            // 
            this.btnDisconnectSourceDSN.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDisconnectSourceDSN.Enabled = false;
            this.btnDisconnectSourceDSN.Location = new System.Drawing.Point(1143, 214);
            this.btnDisconnectSourceDSN.Name = "btnDisconnectSourceDSN";
            this.btnDisconnectSourceDSN.Size = new System.Drawing.Size(71, 22);
            this.btnDisconnectSourceDSN.TabIndex = 45;
            this.btnDisconnectSourceDSN.Text = "Disconnect";
            this.btnDisconnectSourceDSN.UseVisualStyleBackColor = true;
            this.btnDisconnectSourceDSN.Click += new System.EventHandler(this.btnDisconnectSourceDSN_Click);
            // 
            // btnConnectSourceDB
            // 
            this.btnConnectSourceDB.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnConnectSourceDB.Location = new System.Drawing.Point(1220, 214);
            this.btnConnectSourceDB.Name = "btnConnectSourceDB";
            this.btnConnectSourceDB.Size = new System.Drawing.Size(71, 22);
            this.btnConnectSourceDB.TabIndex = 44;
            this.btnConnectSourceDB.Text = "Connect";
            this.btnConnectSourceDB.UseVisualStyleBackColor = true;
            this.btnConnectSourceDB.Click += new System.EventHandler(this.btnConnectSourceDB_Click);
            // 
            // txtSourceDataDSN
            // 
            this.txtSourceDataDSN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtSourceDataDSN.Location = new System.Drawing.Point(56, 216);
            this.txtSourceDataDSN.Name = "txtSourceDataDSN";
            this.txtSourceDataDSN.Size = new System.Drawing.Size(1081, 20);
            this.txtSourceDataDSN.TabIndex = 43;
            this.txtSourceDataDSN.Text = "server=hera;user=dats_sa;password=20miles;database=TransportAppv60_Test";
            // 
            // label17
            // 
            this.label17.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(8, 219);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(30, 13);
            this.label17.TabIndex = 42;
            this.label17.Text = "DSN";
            // 
            // btnUnSelectAll
            // 
            this.btnUnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnUnSelectAll.Location = new System.Drawing.Point(106, 185);
            this.btnUnSelectAll.Name = "btnUnSelectAll";
            this.btnUnSelectAll.Size = new System.Drawing.Size(94, 22);
            this.btnUnSelectAll.TabIndex = 41;
            this.btnUnSelectAll.Text = "Un-Select All";
            this.btnUnSelectAll.UseVisualStyleBackColor = true;
            this.btnUnSelectAll.Click += new System.EventHandler(this.btnUnSelectAll_Click);
            // 
            // btnSelectAll
            // 
            this.btnSelectAll.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnSelectAll.Location = new System.Drawing.Point(6, 185);
            this.btnSelectAll.Name = "btnSelectAll";
            this.btnSelectAll.Size = new System.Drawing.Size(94, 22);
            this.btnSelectAll.TabIndex = 40;
            this.btnSelectAll.Text = "Select All";
            this.btnSelectAll.UseVisualStyleBackColor = true;
            this.btnSelectAll.Click += new System.EventHandler(this.btnSelectAll_Click);
            // 
            // numReplayOffset
            // 
            this.numReplayOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numReplayOffset.Location = new System.Drawing.Point(914, 278);
            this.numReplayOffset.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numReplayOffset.Minimum = new decimal(new int[] {
            999999,
            0,
            0,
            -2147483648});
            this.numReplayOffset.Name = "numReplayOffset";
            this.numReplayOffset.Size = new System.Drawing.Size(73, 20);
            this.numReplayOffset.TabIndex = 39;
            // 
            // chkReplayAsTodayPlusOffset
            // 
            this.chkReplayAsTodayPlusOffset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkReplayAsTodayPlusOffset.AutoSize = true;
            this.chkReplayAsTodayPlusOffset.Location = new System.Drawing.Point(702, 279);
            this.chkReplayAsTodayPlusOffset.Name = "chkReplayAsTodayPlusOffset";
            this.chkReplayAsTodayPlusOffset.Size = new System.Drawing.Size(209, 17);
            this.chkReplayAsTodayPlusOffset.TabIndex = 38;
            this.chkReplayAsTodayPlusOffset.Text = "Replay data as Today Plus Offset Mins";
            this.chkReplayAsTodayPlusOffset.UseVisualStyleBackColor = true;
            // 
            // btnLoadTestData
            // 
            this.btnLoadTestData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadTestData.Location = new System.Drawing.Point(860, 242);
            this.btnLoadTestData.Name = "btnLoadTestData";
            this.btnLoadTestData.Size = new System.Drawing.Size(96, 22);
            this.btnLoadTestData.TabIndex = 37;
            this.btnLoadTestData.Text = "Load Test Data";
            this.btnLoadTestData.UseVisualStyleBackColor = true;
            this.btnLoadTestData.Click += new System.EventHandler(this.btnLoadTestData_Click);
            // 
            // numNumOfRetries
            // 
            this.numNumOfRetries.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numNumOfRetries.Location = new System.Drawing.Point(721, 329);
            this.numNumOfRetries.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numNumOfRetries.Name = "numNumOfRetries";
            this.numNumOfRetries.Size = new System.Drawing.Size(73, 20);
            this.numNumOfRetries.TabIndex = 35;
            // 
            // label11
            // 
            this.label11.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(638, 333);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 13);
            this.label11.TabIndex = 36;
            this.label11.Text = "Num of Retries ";
            // 
            // numRetryPeriod
            // 
            this.numRetryPeriod.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numRetryPeriod.Location = new System.Drawing.Point(554, 329);
            this.numRetryPeriod.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numRetryPeriod.Name = "numRetryPeriod";
            this.numRetryPeriod.Size = new System.Drawing.Size(73, 20);
            this.numRetryPeriod.TabIndex = 33;
            // 
            // label10
            // 
            this.label10.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(461, 333);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(87, 13);
            this.label10.TabIndex = 34;
            this.label10.Text = "Retry Period (ms)";
            // 
            // label7
            // 
            this.label7.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(740, 307);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(42, 13);
            this.label7.TabIndex = 32;
            this.label7.Text = "reports.";
            // 
            // numSendGenImAlive
            // 
            this.numSendGenImAlive.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numSendGenImAlive.Location = new System.Drawing.Point(661, 303);
            this.numSendGenImAlive.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numSendGenImAlive.Name = "numSendGenImAlive";
            this.numSendGenImAlive.Size = new System.Drawing.Size(73, 20);
            this.numSendGenImAlive.TabIndex = 14;
            this.numSendGenImAlive.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // label4
            // 
            this.label4.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(518, 307);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(141, 13);
            this.label4.TabIndex = 30;
            this.label4.Text = "Send GEN_IM_ALIVE every";
            // 
            // cmbVehicle
            // 
            this.cmbVehicle.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbVehicle.FormattingEnabled = true;
            this.cmbVehicle.Location = new System.Drawing.Point(248, 243);
            this.cmbVehicle.Name = "cmbVehicle";
            this.cmbVehicle.Size = new System.Drawing.Size(133, 21);
            this.cmbVehicle.TabIndex = 2;
            // 
            // label3
            // 
            this.label3.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(200, 247);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(42, 13);
            this.label3.TabIndex = 29;
            this.label3.Text = "Vehicle";
            // 
            // cmbFleet
            // 
            this.cmbFleet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.cmbFleet.FormattingEnabled = true;
            this.cmbFleet.Location = new System.Drawing.Point(56, 243);
            this.cmbFleet.Name = "cmbFleet";
            this.cmbFleet.Size = new System.Drawing.Size(144, 21);
            this.cmbFleet.TabIndex = 1;
            this.cmbFleet.SelectedIndexChanged += new System.EventHandler(this.cmbFleet_SelectedIndexChanged);
            // 
            // label2
            // 
            this.label2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 247);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(30, 13);
            this.label2.TabIndex = 27;
            this.label2.Text = "Fleet";
            // 
            // lvUnits
            // 
            this.lvUnits.Activation = System.Windows.Forms.ItemActivation.OneClick;
            this.lvUnits.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.lvUnits.CheckBoxes = true;
            this.lvUnits.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnCheck,
            this.columnHeader1,
            this.columnHeader2,
            this.columnHeader3,
            this.columnHeader4,
            this.columnHeader14,
            this.columnHeader15,
            this.columnHeader16,
            this.columnHeader17,
            this.columnHeader18,
            this.columnHeader19,
            this.columnHeader20,
            this.columnHeader21,
            this.columnHeader5,
            this.columnHeader6,
            this.columnHeader7,
            this.columnHeader8,
            this.columHeader9,
            this.columnHeader9,
            this.columnHeader10,
            this.columnHeader11,
            this.columnHeader12,
            this.columnHeader13});
            this.lvUnits.FullRowSelect = true;
            this.lvUnits.HoverSelection = true;
            this.lvUnits.Location = new System.Drawing.Point(6, 15);
            this.lvUnits.MultiSelect = false;
            this.lvUnits.Name = "lvUnits";
            this.lvUnits.Size = new System.Drawing.Size(1285, 164);
            this.lvUnits.TabIndex = 0;
            this.lvUnits.UseCompatibleStateImageBehavior = false;
            this.lvUnits.View = System.Windows.Forms.View.Details;
            // 
            // columnCheck
            // 
            this.columnCheck.Text = "";
            this.columnCheck.Width = 22;
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "FleetID";
            this.columnHeader1.Width = 0;
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "VehicleID";
            this.columnHeader2.Width = 0;
            // 
            // columnHeader3
            // 
            this.columnHeader3.Text = "Fleet";
            this.columnHeader3.Width = 83;
            // 
            // columnHeader4
            // 
            this.columnHeader4.Text = "Vehicle";
            this.columnHeader4.Width = 98;
            // 
            // columnHeader14
            // 
            this.columnHeader14.Text = "Status";
            this.columnHeader14.Width = 120;
            // 
            // columnHeader15
            // 
            this.columnHeader15.Text = "Packet Sent";
            this.columnHeader15.Width = 78;
            // 
            // columnHeader16
            // 
            this.columnHeader16.Text = "1st Retry";
            // 
            // columnHeader17
            // 
            this.columnHeader17.Text = "2nd Retry";
            // 
            // columnHeader18
            // 
            this.columnHeader18.Text = "3rd Retry";
            // 
            // columnHeader19
            // 
            this.columnHeader19.Text = "4th Retry";
            // 
            // columnHeader20
            // 
            this.columnHeader20.Text = "5th Retry";
            // 
            // columnHeader21
            // 
            this.columnHeader21.Text = "Pings";
            // 
            // columnHeader5
            // 
            this.columnHeader5.Text = "Serial Number";
            this.columnHeader5.Width = 88;
            // 
            // columnHeader6
            // 
            this.columnHeader6.Text = "IMIE";
            this.columnHeader6.Width = 66;
            // 
            // columnHeader7
            // 
            this.columnHeader7.Text = "CDMA";
            this.columnHeader7.Width = 64;
            // 
            // columnHeader8
            // 
            this.columnHeader8.Text = "SIM #";
            this.columnHeader8.Width = 76;
            // 
            // columHeader9
            // 
            this.columHeader9.Text = "Hardware Type";
            this.columHeader9.Width = 90;
            // 
            // columnHeader9
            // 
            this.columnHeader9.Text = "FW Version";
            this.columnHeader9.Width = 72;
            // 
            // columnHeader10
            // 
            this.columnHeader10.Text = "HW Version";
            this.columnHeader10.Width = 85;
            // 
            // columnHeader11
            // 
            this.columnHeader11.Text = "Config Ver";
            // 
            // columnHeader12
            // 
            this.columnHeader12.Text = "Set Point Ver";
            // 
            // columnHeader13
            // 
            this.columnHeader13.Text = "Route Ver";
            // 
            // numSendInterval
            // 
            this.numSendInterval.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numSendInterval.Location = new System.Drawing.Point(382, 329);
            this.numSendInterval.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numSendInterval.Name = "numSendInterval";
            this.numSendInterval.Size = new System.Drawing.Size(73, 20);
            this.numSendInterval.TabIndex = 17;
            // 
            // btnLoadReplayData
            // 
            this.btnLoadReplayData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnLoadReplayData.Location = new System.Drawing.Point(783, 242);
            this.btnLoadReplayData.Name = "btnLoadReplayData";
            this.btnLoadReplayData.Size = new System.Drawing.Size(71, 22);
            this.btnLoadReplayData.TabIndex = 6;
            this.btnLoadReplayData.Text = "Load Data";
            this.btnLoadReplayData.UseVisualStyleBackColor = true;
            this.btnLoadReplayData.Click += new System.EventHandler(this.btnLoadReplayData_Click);
            // 
            // label9
            // 
            this.label9.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(285, 333);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(92, 13);
            this.label9.TabIndex = 19;
            this.label9.Text = "Send Interval (ms)";
            // 
            // btnClearReplayData
            // 
            this.btnClearReplayData.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnClearReplayData.Location = new System.Drawing.Point(707, 242);
            this.btnClearReplayData.Name = "btnClearReplayData";
            this.btnClearReplayData.Size = new System.Drawing.Size(71, 22);
            this.btnClearReplayData.TabIndex = 5;
            this.btnClearReplayData.Text = "Clear Data";
            this.btnClearReplayData.UseVisualStyleBackColor = true;
            // 
            // chkLoop
            // 
            this.chkLoop.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkLoop.AutoSize = true;
            this.chkLoop.Location = new System.Drawing.Point(202, 305);
            this.chkLoop.Name = "chkLoop";
            this.chkLoop.Size = new System.Drawing.Size(310, 17);
            this.chkLoop.TabIndex = 13;
            this.chkLoop.Text = "Replay data in loop (i.e when on last record go to back start)";
            this.chkLoop.UseVisualStyleBackColor = true;
            // 
            // numListenPort
            // 
            this.numListenPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.numListenPort.Location = new System.Drawing.Point(197, 329);
            this.numListenPort.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
            this.numListenPort.Name = "numListenPort";
            this.numListenPort.Size = new System.Drawing.Size(73, 20);
            this.numListenPort.TabIndex = 16;
            // 
            // txtListenerIP
            // 
            this.txtListenerIP.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.txtListenerIP.Location = new System.Drawing.Point(56, 329);
            this.txtListenerIP.Name = "txtListenerIP";
            this.txtListenerIP.Size = new System.Drawing.Size(135, 20);
            this.txtListenerIP.TabIndex = 15;
            // 
            // chkDontReplayWPReports
            // 
            this.chkDontReplayWPReports.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkDontReplayWPReports.AutoSize = true;
            this.chkDontReplayWPReports.Location = new System.Drawing.Point(12, 305);
            this.chkDontReplayWPReports.Name = "chkDontReplayWPReports";
            this.chkDontReplayWPReports.Size = new System.Drawing.Size(186, 17);
            this.chkDontReplayWPReports.TabIndex = 12;
            this.chkDontReplayWPReports.Text = "Don\'t Replay WP Arrive / Departs";
            this.chkDontReplayWPReports.UseVisualStyleBackColor = true;
            // 
            // label8
            // 
            this.label8.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 333);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(44, 13);
            this.label8.TabIndex = 2;
            this.label8.Text = "Listener";
            // 
            // dtReplayDate
            // 
            this.dtReplayDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtReplayDate.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtReplayDate.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtReplayDate.Location = new System.Drawing.Point(569, 277);
            this.dtReplayDate.Name = "dtReplayDate";
            this.dtReplayDate.Size = new System.Drawing.Size(127, 20);
            this.dtReplayDate.TabIndex = 11;
            // 
            // dtSourceStart
            // 
            this.dtSourceStart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtSourceStart.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtSourceStart.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtSourceStart.Location = new System.Drawing.Point(415, 243);
            this.dtSourceStart.Name = "dtSourceStart";
            this.dtSourceStart.Size = new System.Drawing.Size(127, 20);
            this.dtSourceStart.TabIndex = 3;
            // 
            // chkReplayWithSpecifiedDate
            // 
            this.chkReplayWithSpecifiedDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkReplayWithSpecifiedDate.AutoSize = true;
            this.chkReplayWithSpecifiedDate.Location = new System.Drawing.Point(465, 279);
            this.chkReplayWithSpecifiedDate.Name = "chkReplayWithSpecifiedDate";
            this.chkReplayWithSpecifiedDate.Size = new System.Drawing.Size(98, 17);
            this.chkReplayWithSpecifiedDate.TabIndex = 10;
            this.chkReplayWithSpecifiedDate.Text = "Replay data on";
            this.chkReplayWithSpecifiedDate.UseVisualStyleBackColor = true;
            // 
            // dtSourceEnd
            // 
            this.dtSourceEnd.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.dtSourceEnd.CustomFormat = "dd/MM/yyyy HH:mm:ss";
            this.dtSourceEnd.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtSourceEnd.Location = new System.Drawing.Point(564, 243);
            this.dtSourceEnd.Name = "dtSourceEnd";
            this.dtSourceEnd.Size = new System.Drawing.Size(127, 20);
            this.dtSourceEnd.TabIndex = 4;
            // 
            // chkReplayWithTodaysDate
            // 
            this.chkReplayWithTodaysDate.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkReplayWithTodaysDate.AutoSize = true;
            this.chkReplayWithTodaysDate.Location = new System.Drawing.Point(314, 279);
            this.chkReplayWithTodaysDate.Name = "chkReplayWithTodaysDate";
            this.chkReplayWithTodaysDate.Size = new System.Drawing.Size(138, 17);
            this.chkReplayWithTodaysDate.TabIndex = 9;
            this.chkReplayWithTodaysDate.Text = "Replay As Todays Date";
            this.chkReplayWithTodaysDate.UseVisualStyleBackColor = true;
            this.chkReplayWithTodaysDate.CheckedChanged += new System.EventHandler(this.chkReplayWithTodaysDate_CheckedChanged);
            // 
            // chkReplayAsReportedTime
            // 
            this.chkReplayAsReportedTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkReplayAsReportedTime.AutoSize = true;
            this.chkReplayAsReportedTime.Location = new System.Drawing.Point(148, 279);
            this.chkReplayAsReportedTime.Name = "chkReplayAsReportedTime";
            this.chkReplayAsReportedTime.Size = new System.Drawing.Size(147, 17);
            this.chkReplayAsReportedTime.TabIndex = 8;
            this.chkReplayAsReportedTime.Text = "Replay As Reported Time";
            this.chkReplayAsReportedTime.UseVisualStyleBackColor = true;
            this.chkReplayAsReportedTime.CheckedChanged += new System.EventHandler(this.chkReplayAsReportedTime_CheckedChanged);
            // 
            // label5
            // 
            this.label5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(379, 247);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(30, 13);
            this.label5.TabIndex = 11;
            this.label5.Text = "From";
            // 
            // chkReplayInCurrentTime
            // 
            this.chkReplayInCurrentTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.chkReplayInCurrentTime.AutoSize = true;
            this.chkReplayInCurrentTime.Location = new System.Drawing.Point(12, 279);
            this.chkReplayInCurrentTime.Name = "chkReplayInCurrentTime";
            this.chkReplayInCurrentTime.Size = new System.Drawing.Size(136, 17);
            this.chkReplayInCurrentTime.TabIndex = 7;
            this.chkReplayInCurrentTime.Text = "Replay as Current Time";
            this.chkReplayInCurrentTime.UseVisualStyleBackColor = true;
            this.chkReplayInCurrentTime.CheckedChanged += new System.EventHandler(this.chkReplayInCurrentTime_CheckedChanged);
            // 
            // label6
            // 
            this.label6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(542, 247);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(20, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "To";
            // 
            // btnDBDisonnect
            // 
            this.btnDBDisonnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDBDisonnect.Enabled = false;
            this.btnDBDisonnect.Location = new System.Drawing.Point(1143, 22);
            this.btnDBDisonnect.Name = "btnDBDisonnect";
            this.btnDBDisonnect.Size = new System.Drawing.Size(71, 22);
            this.btnDBDisonnect.TabIndex = 3;
            this.btnDBDisonnect.Text = "Disconnect";
            this.btnDBDisonnect.UseVisualStyleBackColor = true;
            this.btnDBDisonnect.Click += new System.EventHandler(this.btnDBDisonnect_Click);
            // 
            // btnDBConnect
            // 
            this.btnDBConnect.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnDBConnect.Location = new System.Drawing.Point(1220, 22);
            this.btnDBConnect.Name = "btnDBConnect";
            this.btnDBConnect.Size = new System.Drawing.Size(71, 22);
            this.btnDBConnect.TabIndex = 2;
            this.btnDBConnect.Text = "Connect";
            this.btnDBConnect.UseVisualStyleBackColor = true;
            this.btnDBConnect.Click += new System.EventHandler(this.btnDBConnect_Click);
            // 
            // txtDSN
            // 
            this.txtDSN.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDSN.Location = new System.Drawing.Point(56, 22);
            this.txtDSN.Name = "txtDSN";
            this.txtDSN.Size = new System.Drawing.Size(1081, 20);
            this.txtDSN.TabIndex = 1;
            this.txtDSN.Text = "server=hera;user=dats_sa;password=20miles;database=TransportAppv60_Test";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(10, 25);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(30, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "DSN";
            // 
            // btnSend
            // 
            this.btnSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSend.Location = new System.Drawing.Point(1242, 416);
            this.btnSend.Name = "btnSend";
            this.btnSend.Size = new System.Drawing.Size(71, 22);
            this.btnSend.TabIndex = 19;
            this.btnSend.Text = "Start";
            this.btnSend.UseVisualStyleBackColor = true;
            this.btnSend.Click += new System.EventHandler(this.btnSend_Click);
            // 
            // btnStopSim
            // 
            this.btnStopSim.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnStopSim.Location = new System.Drawing.Point(1165, 416);
            this.btnStopSim.Name = "btnStopSim";
            this.btnStopSim.Size = new System.Drawing.Size(71, 22);
            this.btnStopSim.TabIndex = 18;
            this.btnStopSim.Text = "Stop";
            this.btnStopSim.UseVisualStyleBackColor = true;
            this.btnStopSim.Click += new System.EventHandler(this.btnStopSim_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Location = new System.Drawing.Point(3, 1);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1328, 470);
            this.tabControl1.TabIndex = 20;
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.btnSend);
            this.tabPage1.Controls.Add(this.btnStopSim);
            this.tabPage1.Controls.Add(this.groupBox1);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1320, 444);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "Unit Simulator";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.txtResult);
            this.tabPage2.Controls.Add(this.label12);
            this.tabPage2.Controls.Add(this.txtRawBytes);
            this.tabPage2.Controls.Add(this.numPort);
            this.tabPage2.Controls.Add(this.label13);
            this.tabPage2.Controls.Add(this.txtIPAddress);
            this.tabPage2.Controls.Add(this.btnProcessPacket);
            this.tabPage2.Controls.Add(this.btnSendUnitPacket);
            this.tabPage2.Location = new System.Drawing.Point(4, 22);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1320, 444);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Parse/Send Tracking Data";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // txtResult
            // 
            this.txtResult.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtResult.Location = new System.Drawing.Point(14, 66);
            this.txtResult.Name = "txtResult";
            this.txtResult.Size = new System.Drawing.Size(1023, 311);
            this.txtResult.TabIndex = 10;
            this.txtResult.Text = "";
            // 
            // label12
            // 
            this.label12.Location = new System.Drawing.Point(15, 44);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(88, 16);
            this.label12.TabIndex = 15;
            this.label12.Text = "Send Bytes To";
            // 
            // txtRawBytes
            // 
            this.txtRawBytes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRawBytes.Location = new System.Drawing.Point(104, 10);
            this.txtRawBytes.Name = "txtRawBytes";
            this.txtRawBytes.Size = new System.Drawing.Size(933, 20);
            this.txtRawBytes.TabIndex = 9;
            // 
            // numPort
            // 
            this.numPort.Location = new System.Drawing.Point(246, 40);
            this.numPort.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numPort.Name = "numPort";
            this.numPort.Size = new System.Drawing.Size(48, 20);
            this.numPort.TabIndex = 14;
            this.numPort.Value = new decimal(new int[] {
            5555,
            0,
            0,
            0});
            // 
            // label13
            // 
            this.label13.Location = new System.Drawing.Point(15, 14);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(64, 16);
            this.label13.TabIndex = 8;
            this.label13.Text = "Raw Bytes";
            // 
            // txtIPAddress
            // 
            this.txtIPAddress.Location = new System.Drawing.Point(104, 40);
            this.txtIPAddress.Name = "txtIPAddress";
            this.txtIPAddress.Size = new System.Drawing.Size(136, 20);
            this.txtIPAddress.TabIndex = 13;
            this.txtIPAddress.Text = "192.168.1.145";
            // 
            // btnProcessPacket
            // 
            this.btnProcessPacket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnProcessPacket.Location = new System.Drawing.Point(916, 36);
            this.btnProcessPacket.Name = "btnProcessPacket";
            this.btnProcessPacket.Size = new System.Drawing.Size(120, 24);
            this.btnProcessPacket.TabIndex = 11;
            this.btnProcessPacket.Text = "Process Packet";
            this.btnProcessPacket.Click += new System.EventHandler(this.btnProcessPacket_Click);
            // 
            // btnSendUnitPacket
            // 
            this.btnSendUnitPacket.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendUnitPacket.Location = new System.Drawing.Point(756, 36);
            this.btnSendUnitPacket.Name = "btnSendUnitPacket";
            this.btnSendUnitPacket.Size = new System.Drawing.Size(144, 24);
            this.btnSendUnitPacket.TabIndex = 12;
            this.btnSendUnitPacket.Text = "Send Bytes";
            this.btnSendUnitPacket.Click += new System.EventHandler(this.btnSendUnitPacket_Click);
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.btnSendString);
            this.tabPage3.Controls.Add(this.label16);
            this.tabPage3.Controls.Add(this.txtRSUpdateBytes);
            this.tabPage3.Controls.Add(this.label15);
            this.tabPage3.Controls.Add(this.label14);
            this.tabPage3.Controls.Add(this.txtUpdateString);
            this.tabPage3.Controls.Add(this.numRSListenerPort);
            this.tabPage3.Controls.Add(this.txtRSListenerIP);
            this.tabPage3.Controls.Add(this.btnSendBytes);
            this.tabPage3.Location = new System.Drawing.Point(4, 22);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1320, 444);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Listener Remote State Update";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // btnSendString
            // 
            this.btnSendString.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendString.Location = new System.Drawing.Point(885, 7);
            this.btnSendString.Name = "btnSendString";
            this.btnSendString.Size = new System.Drawing.Size(144, 24);
            this.btnSendString.TabIndex = 25;
            this.btnSendString.Text = "Send String";
            this.btnSendString.Click += new System.EventHandler(this.btnSendString_Click);
            // 
            // label16
            // 
            this.label16.Location = new System.Drawing.Point(15, 40);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(83, 16);
            this.label16.TabIndex = 24;
            this.label16.Text = "Byte Command";
            // 
            // txtRSUpdateBytes
            // 
            this.txtRSUpdateBytes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRSUpdateBytes.Location = new System.Drawing.Point(104, 36);
            this.txtRSUpdateBytes.Name = "txtRSUpdateBytes";
            this.txtRSUpdateBytes.Size = new System.Drawing.Size(766, 20);
            this.txtRSUpdateBytes.TabIndex = 23;
            // 
            // label15
            // 
            this.label15.Location = new System.Drawing.Point(15, 14);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(83, 16);
            this.label15.TabIndex = 22;
            this.label15.Text = "Update String";
            // 
            // label14
            // 
            this.label14.Location = new System.Drawing.Point(15, 68);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(88, 16);
            this.label14.TabIndex = 21;
            this.label14.Text = "Send Bytes To";
            // 
            // txtUpdateString
            // 
            this.txtUpdateString.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtUpdateString.Location = new System.Drawing.Point(104, 10);
            this.txtUpdateString.Name = "txtUpdateString";
            this.txtUpdateString.Size = new System.Drawing.Size(766, 20);
            this.txtUpdateString.TabIndex = 17;
            // 
            // numRSListenerPort
            // 
            this.numRSListenerPort.Location = new System.Drawing.Point(246, 66);
            this.numRSListenerPort.Maximum = new decimal(new int[] {
            99999,
            0,
            0,
            0});
            this.numRSListenerPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
            this.numRSListenerPort.Name = "numRSListenerPort";
            this.numRSListenerPort.Size = new System.Drawing.Size(48, 20);
            this.numRSListenerPort.TabIndex = 20;
            this.numRSListenerPort.Value = new decimal(new int[] {
            3000,
            0,
            0,
            0});
            // 
            // txtRSListenerIP
            // 
            this.txtRSListenerIP.Location = new System.Drawing.Point(104, 66);
            this.txtRSListenerIP.Name = "txtRSListenerIP";
            this.txtRSListenerIP.Size = new System.Drawing.Size(136, 20);
            this.txtRSListenerIP.TabIndex = 19;
            this.txtRSListenerIP.Text = "192.168.1.145";
            // 
            // btnSendBytes
            // 
            this.btnSendBytes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnSendBytes.Location = new System.Drawing.Point(885, 39);
            this.btnSendBytes.Name = "btnSendBytes";
            this.btnSendBytes.Size = new System.Drawing.Size(144, 24);
            this.btnSendBytes.TabIndex = 18;
            this.btnSendBytes.Text = "Send Bytes";
            this.btnSendBytes.Click += new System.EventHandler(this.btnSendBytes_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1330, 474);
            this.Controls.Add(this.tabControl1);
            this.Location = new System.Drawing.Point(10, 10);
            this.MinimumSize = new System.Drawing.Size(1050, 513);
            this.Name = "Form1";
            this.Text = "MTData Mobile Unit Simulator";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.Load += new System.EventHandler(this.Form1_Load);
            this.Paint += new System.Windows.Forms.PaintEventHandler(this.Form1_Paint);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.grpReplayOptions.ResumeLayout(false);
            this.grpReplayOptions.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numReplayOffset)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numNumOfRetries)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numRetryPeriod)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSendGenImAlive)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numSendInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numListenPort)).EndInit();
            this.tabControl1.ResumeLayout(false);
            this.tabPage1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tabPage2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numPort)).EndInit();
            this.tabPage3.ResumeLayout(false);
            this.tabPage3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numRSListenerPort)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.DateTimePicker dtSourceEnd;
        private System.Windows.Forms.DateTimePicker dtSourceStart;
        private System.Windows.Forms.Button btnDBDisonnect;
        private System.Windows.Forms.Button btnDBConnect;
        private System.Windows.Forms.TextBox txtDSN;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox chkLoop;
        private System.Windows.Forms.CheckBox chkDontReplayWPReports;
        private System.Windows.Forms.DateTimePicker dtReplayDate;
        private System.Windows.Forms.CheckBox chkReplayWithSpecifiedDate;
        private System.Windows.Forms.CheckBox chkReplayWithTodaysDate;
        private System.Windows.Forms.CheckBox chkReplayAsReportedTime;
        private System.Windows.Forms.CheckBox chkReplayInCurrentTime;
        private System.Windows.Forms.Button btnLoadReplayData;
        private System.Windows.Forms.Button btnClearReplayData;
        private System.Windows.Forms.NumericUpDown numSendInterval;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.NumericUpDown numListenPort;
        private System.Windows.Forms.TextBox txtListenerIP;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Button btnSend;
        private System.Windows.Forms.Button btnStopSim;
        private System.Windows.Forms.GroupBox grpReplayOptions;
        private System.Windows.Forms.ListView lvUnits;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.ColumnHeader columnHeader3;
        private System.Windows.Forms.ColumnHeader columnHeader4;
        private System.Windows.Forms.ColumnHeader columnHeader5;
        private System.Windows.Forms.ColumnHeader columnHeader6;
        private System.Windows.Forms.ColumnHeader columnHeader7;
        private System.Windows.Forms.ColumnHeader columnHeader8;
        private System.Windows.Forms.ColumnHeader columHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader9;
        private System.Windows.Forms.ColumnHeader columnHeader10;
        private System.Windows.Forms.ColumnHeader columnCheck;
        private System.Windows.Forms.ColumnHeader columnHeader11;
        private System.Windows.Forms.ColumnHeader columnHeader12;
        private System.Windows.Forms.ColumnHeader columnHeader13;
        private System.Windows.Forms.ComboBox cmbVehicle;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ComboBox cmbFleet;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ColumnHeader columnHeader14;
        private System.Windows.Forms.ColumnHeader columnHeader15;
        private System.Windows.Forms.ColumnHeader columnHeader16;
        private System.Windows.Forms.ColumnHeader columnHeader17;
        private System.Windows.Forms.ColumnHeader columnHeader18;
        private System.Windows.Forms.ColumnHeader columnHeader19;
        private System.Windows.Forms.ColumnHeader columnHeader20;
        private System.Windows.Forms.ColumnHeader columnHeader21;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.NumericUpDown numSendGenImAlive;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numRetryPeriod;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.NumericUpDown numNumOfRetries;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Button btnLoadTestData;
        private System.Windows.Forms.NumericUpDown numReplayOffset;
        private System.Windows.Forms.CheckBox chkReplayAsTodayPlusOffset;
        private System.Windows.Forms.Button btnSelectAll;
        private System.Windows.Forms.Button btnUnSelectAll;
        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.RichTextBox txtResult;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtRawBytes;
        private System.Windows.Forms.NumericUpDown numPort;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtIPAddress;
        private System.Windows.Forms.Button btnProcessPacket;
        private System.Windows.Forms.Button btnSendUnitPacket;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtUpdateString;
        private System.Windows.Forms.NumericUpDown numRSListenerPort;
        private System.Windows.Forms.TextBox txtRSListenerIP;
        private System.Windows.Forms.Button btnSendBytes;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Button btnSendString;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtRSUpdateBytes;
        private System.Windows.Forms.Button btnDisconnectSourceDSN;
        private System.Windows.Forms.Button btnConnectSourceDB;
        private System.Windows.Forms.TextBox txtSourceDataDSN;
        private System.Windows.Forms.Label label17;
    }
}


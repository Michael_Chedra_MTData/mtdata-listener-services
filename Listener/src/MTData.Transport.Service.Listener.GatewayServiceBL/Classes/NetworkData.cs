﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    public class NetworkData
    {
        #region Private Fields
        private bool useHostName;
        private string hostIP;
        private int ipPort;
        private int secIPPort;
        private string secIP;
        private int pingRetries;
        private string pingIP;
        #endregion

        #region Constructor
        public NetworkData(string xmlDataToLoad)
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            doc.LoadXml(xmlDataToLoad);

            System.Xml.XmlElement baseElement = doc.DocumentElement;

            this.LoadDataFromElement(baseElement);
        }

        public NetworkData(System.Xml.XmlElement elementToLoad)
        {
            this.LoadDataFromElement(elementToLoad);
        }
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        public bool UseHostName
        {
            get { return this.useHostName; }
        }

        public string HostIP
        {
            get
            {
                return this.hostIP;
            }
        }

        public int IPPort
        {
            get
            {
                return this.ipPort;
            }
        }

        public int SecIPPort
        {
            get
            {
                return this.secIPPort;
            }
        }

        public string SecIP
        {
            get
            {
                return this.secIP;
            }
        }

        public int PingRetries
        {
            get
            {
                return this.pingRetries;
            }
        }

        public string NewIPPort
        {
            get
            {
                return this.ipPort.ToString();
            }
        }

        public string PingIP
        {
            get
            {
                return this.pingIP;
            }
        }
        #endregion

        #region Methods
        private void LoadDataFromElement(System.Xml.XmlElement baseElement)
        {
            this.useHostName = bool.Parse(baseElement.GetAttribute("hostName"));
            this.hostIP = baseElement.GetAttribute("hostIP");
            this.ipPort = int.Parse(baseElement.GetAttribute("ipPort"));
            this.secIPPort = int.Parse(baseElement.GetAttribute("secIPPort"));
            this.secIP = baseElement.GetAttribute("secIP");
            this.pingRetries = int.Parse(baseElement.GetAttribute("pingRetries"));
            this.pingIP = baseElement.GetAttribute("pingIP");
        }
        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion
    }
}

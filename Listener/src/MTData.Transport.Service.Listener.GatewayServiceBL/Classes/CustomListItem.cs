﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class CustomListItem
    {
        private int id;
        private string description;

        public CustomListItem(System.Data.SqlClient.SqlDataReader dr)
        {
            this.id = dr.GetInt32(dr.GetOrdinal("id"));
            this.description = dr.GetString(dr.GetOrdinal("description"));
        }

        public CustomListItem(int id, string description)
        {
            this.id = id;
            this.description = description;
        }

        [DataMember]
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }
    }
}
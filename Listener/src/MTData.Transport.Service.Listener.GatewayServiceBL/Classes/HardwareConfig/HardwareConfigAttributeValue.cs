﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class HardwareConfigAttributeValue
    {
        private string displayMember;
        private string valueMember;
        private bool isDefault;

        public HardwareConfigAttributeValue(string displayMember, string valueMember, bool isDefault)
        {
            this.isDefault = isDefault;
            this.displayMember = displayMember;
            this.valueMember = valueMember;
        }

        public HardwareConfigAttributeValue(SqlDataReader sdr)
        {
            this.displayMember = sdr.GetString(sdr.GetOrdinal("DisplayValue"));
            this.valueMember = sdr.GetString(sdr.GetOrdinal("MemberValue"));
            this.isDefault = sdr.GetBoolean(sdr.GetOrdinal("Default"));
        }

        [DataMember]
        public bool IsDefault
        {
            get { return this.isDefault; }
            set { this.isDefault = value; }
        }

        [DataMember]
        public string DisplayMember
        {
            get { return this.displayMember; }
            set { this.displayMember = value; }
        }

        [DataMember]
        public string ValueMember
        {
            get { return this.valueMember; }
            set { this.valueMember = value; }
        }
    }
}

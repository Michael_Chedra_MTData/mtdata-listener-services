﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class HardwareConfigAttribute
    {
        private int hardwareConfigAttributeTypeID;
        private int id;
        private string name;
        private int hardwareConfigValueAttributeDataTypeID;
        private int minValue;
        private int maxValue;
        private int controlType;
        private int groupTypeID;
        private List<HardwareConfigAttributeValue> valueList = new List<HardwareConfigAttributeValue>();

        public HardwareConfigAttribute(SqlDataReader sdr, string connString, int groupTypeID)
        {
            this.groupTypeID = groupTypeID;
            this.hardwareConfigAttributeTypeID = sdr.GetInt32(sdr.GetOrdinal("hwConfigAttributeTypeID"));
            this.id = sdr.GetInt32(sdr.GetOrdinal("id"));
            this.name = sdr.GetString(sdr.GetOrdinal("name"));
            this.hardwareConfigValueAttributeDataTypeID = sdr.GetInt32(sdr.GetOrdinal("hwConfigValueAttributeDataTypeID"));
            this.minValue = sdr.IsDBNull(sdr.GetOrdinal("minValue")) ? 0 : sdr.GetInt32(sdr.GetOrdinal("minValue"));
            this.maxValue = sdr.IsDBNull(sdr.GetOrdinal("maxValue")) ? 0 : sdr.GetInt32(sdr.GetOrdinal("maxValue"));
            this.controlType = sdr.IsDBNull(sdr.GetOrdinal("controlType")) ? 0 : sdr.GetInt32(sdr.GetOrdinal("controlType"));

            if (sdr.IsDBNull(sdr.GetOrdinal("minValue")) || sdr.IsDBNull(sdr.GetOrdinal("maxValue")))
            {
                this.LoadAttributeValueList(connString);
            }
            else
            {
                for (int i = minValue; i < maxValue + 1; i++)
                {
                    this.valueList.Add(new HardwareConfigAttributeValue(i.ToString(), i.ToString(), i == minValue ? true : false));
                }
            }
        }

        [DataMember]
        public int HWConfigAttributeTypeID
        {
            get { return this.hardwareConfigAttributeTypeID; }
            set { this.id = value; }
        }

        [DataMember]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        [DataMember]
        public int HWConfigValueAttributeDataTypeID
        {
            get { return this.hardwareConfigValueAttributeDataTypeID; }
            set { this.hardwareConfigValueAttributeDataTypeID = value; }
        }

        [DataMember]
        public int MinValue
        {
            get { return this.minValue; }
            set { this.minValue = value; }
        }

        [DataMember]
        public int MaxValue
        {
            get { return this.maxValue; }
            set { this.maxValue = value; }
        }

        [DataMember]
        public int ControlType
        {
            get { return this.controlType; }
            set { this.controlType = value; }
        }

        [DataMember]
        public List<HardwareConfigAttributeValue> ValueList
        {
            get { return this.valueList; }
            set { this.valueList = value; }
        }

        private void LoadAttributeValueList(string connString)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand("GetConfigAttributeListValues"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@groupTypeID", this.groupTypeID);
                comm.Parameters.AddWithValue("@attributeTypeID", this.hardwareConfigAttributeTypeID);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        this.valueList.Add(new HardwareConfigAttributeValue(dr));
                    }
                }
            }
        }
    }
}

﻿using System.Collections.Generic;
using System.Data.SqlClient;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class HardwareConfigGroup
    {
        private int id;
        private string description;
        private bool active;
        private List<HardwareConfigAttribute> attributes = new List<HardwareConfigAttribute>();

        public HardwareConfigGroup(SqlDataReader sdr, string connString)
        {
            this.id = sdr.GetInt32(sdr.GetOrdinal("id"));
            this.description = sdr.GetString(sdr.GetOrdinal("description"));
            this.active = sdr.GetBoolean(sdr.GetOrdinal("active"));

            this.LoadAttributes(connString);
        }

        [DataMember]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        public bool Active
        {
            get { return this.active; }
        }

        [DataMember]
        public List<HardwareConfigAttribute> Attributes
        {
            get { return this.attributes; }
            set { this.attributes = value; }
        }

        private void LoadAttributes(string connString)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand("GetConfigAttributesByGroupID"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", this.id);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        this.attributes.Add(new HardwareConfigAttribute(dr, connString, this.id));
                    }
                }
            }
        }
    }
}

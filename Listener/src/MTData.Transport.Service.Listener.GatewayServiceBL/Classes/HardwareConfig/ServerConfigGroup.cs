﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class ServerConfigGroup
    {
        private int id;
        private int hardwareConfigValueGroupTypeID;
        private string description;
        private bool active;
        private List<ServerConfigGroupAttribute> attributeValues = new List<ServerConfigGroupAttribute>();
        private bool templateOnlyGroup = false;

        internal ServerConfigGroup(int groupTypeID)
        {
            // As we are only a holder at this point only need the groupTypeID for saving
            this.hardwareConfigValueGroupTypeID = groupTypeID;
        }

        internal ServerConfigGroup(SqlDataReader sdr, string connString)
        {
            this.id = sdr.GetInt32(sdr.GetOrdinal("id"));
            this.hardwareConfigValueGroupTypeID = sdr.GetInt32(sdr.GetOrdinal("HWConfigGroupTypeID"));
            this.description = sdr.GetString(sdr.GetOrdinal("Description"));
            this.active = sdr.GetBoolean(sdr.GetOrdinal("Active"));

            this.LoadAttributes(connString);
        }

        internal ServerConfigGroup(HardwareConfigGroup group, string connString)
        {
            this.id = group.Id;
            this.hardwareConfigValueGroupTypeID = group.Id;
            this.description = group.Description;
            this.active = group.Active;
            this.templateOnlyGroup = true;

            this.LoadAttributes(connString);
        }

        [DataMember]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public int HWConfigValueGroupTypeID
        {
            get { return this.hardwareConfigValueGroupTypeID; }
            set { this.hardwareConfigValueGroupTypeID = value; }
        }

        [DataMember]
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }

        [DataMember]
        public bool Active
        {
            get { return this.active; }
            set { this.active = value; }
        }

        [DataMember]
        public List<ServerConfigGroupAttribute> Attributes
        {
            get { return this.attributeValues; }
            set { this.attributeValues = value; }
        }

        internal bool SaveGroup(int serverConfigID, string connString)
        {
            this.SaveGroupSettings(serverConfigID, connString);

            foreach (ServerConfigGroupAttribute att in this.attributeValues)
            {
                att.SaveAttribute(this.id, connString);
            }

            return true;
        }

        internal void SaveGroupSettings(int serverConfigID, string connString)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand("SaveServerGroup"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@HWConfigServerID", serverConfigID);
                comm.Parameters.AddWithValue("@HWConfigGroupTypeID", this.HWConfigValueGroupTypeID);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();

                if (dr.HasRows)
                {
                    if (dr.HasRows)
                    {
                        dr.Read();
                        this.id = dr.GetInt32(dr.GetOrdinal("id"));
                    }
                }
            }
        }

        private void LoadAttributes(string connString)
        {
            string sqlString = this.templateOnlyGroup ? "GetConfigAttributesByGroupID" : "GetServerConfigAttributesByGroupID";
            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand(sqlString))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", this.id);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        this.attributeValues.Add(new ServerConfigGroupAttribute(dr, connString, this.hardwareConfigValueGroupTypeID, this.templateOnlyGroup));
                    }
                }
            }
        }
    }
}

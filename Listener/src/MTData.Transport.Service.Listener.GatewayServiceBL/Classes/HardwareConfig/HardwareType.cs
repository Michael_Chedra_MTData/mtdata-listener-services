using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class HardwareType
    {
        #region Constructor

        public HardwareType(System.Data.SqlClient.SqlDataReader sdr)
        {
            ID = sdr.GetInt32(sdr.GetOrdinal("ID"));
            Description = sdr.GetString(sdr.GetOrdinal("Description"));
            SerialNumRangeMin = sdr.GetInt64(sdr.GetOrdinal("SerialNumRangeMin"));
            SerialNumRangeMax = sdr.GetInt64(sdr.GetOrdinal("SerialNumRangeMax"));
        }

        #endregion

        #region Public Properties

        [DataMember]
        public int ID
        {
            get; set;
        }

        [DataMember]
        public string Description
        {
            get; set;
        }

        [DataMember]
        public long SerialNumRangeMin
        {
            get; set;
        }

        [DataMember]
        public long SerialNumRangeMax
        {
            get; set;
        }

        #endregion
    }
}
﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class HardwareConfig
    {
        private int id;
        private int configTypeID;
        private string name;
        private int versionNumber;
        private int createdUserID;
        private DateTime createdDate;
        private bool currentActive;
        private List<ServerConfigGroup> serverConfigGroups = new List<ServerConfigGroup>();

        #region Constructor
        public HardwareConfig(System.Data.SqlClient.SqlDataReader sdr, string connString, bool includeDetails = true)
        {
            this.id = sdr.GetInt32(sdr.GetOrdinal("ID"));
            this.configTypeID = sdr.GetInt32(sdr.GetOrdinal("HWConfigTypeID"));
            this.name = sdr.GetString(sdr.GetOrdinal("name"));
            this.versionNumber = sdr.GetInt32(sdr.GetOrdinal("VersionNumber"));
            this.createdUserID = sdr.GetInt32(sdr.GetOrdinal("CreatedUserID"));
            this.createdDate = sdr.GetDateTime(sdr.GetOrdinal("CreatedDate"));
            this.currentActive = sdr.GetBoolean(sdr.GetOrdinal("CurrentActive"));

            if (includeDetails)
            {
                this.LoadGroups(connString);
            }
        }
        #endregion

        public bool IsNew
        {
            get { return this.id == 0; }
        }

        [DataMember]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public int ConfigTypeID
        {
            get { return this.configTypeID; }
            set { this.configTypeID = value; }
        }

        [DataMember]
        public string Name
        {
            get { return this.name; }
            set { this.name = value; }
        }

        [DataMember]
        public int VersionNumber
        {
            get { return this.versionNumber; }
            set { this.versionNumber = value; }
        }

        [DataMember]
        public int CreatedUserID
        {
            get { return this.createdUserID; }
            set { this.createdUserID = value; }
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get { return this.createdDate; }
            set { this.createdDate = value; }
        }

        [DataMember]
        public bool CurrentActive
        {
            get { return this.currentActive; }
            set { this.currentActive = value; }
        }

        [DataMember]
        public List<ServerConfigGroup> GroupValues
        {
            get { return this.serverConfigGroups; }
            set { this.serverConfigGroups = value; }
        }

        #region private methods
        private void LoadGroups(string connString)
        {
            List<HardwareConfigGroup> hardwareConfigGroups = new List<HardwareConfigGroup>();

            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand("GetConfigGroupsByConfigID"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", this.id);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        hardwareConfigGroups.Add(new HardwareConfigGroup(dr, connString));
                    }
                }
            }

            LoadGroups(connString, hardwareConfigGroups);
        }

        private void LoadGroups(string connString, List<HardwareConfigGroup> configTypeGroups)
        {
            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand("GetConfigGroupsByServerConfigID"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", this.id);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();

                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        this.serverConfigGroups.Add(new ServerConfigGroup(dr, connString));
                    }
                }
            }

            // Now add any groups and attributes that the saved config hasn't got as we have added new attributes or groups
            foreach (HardwareConfigGroup group in configTypeGroups)
            {
                ServerConfigGroup tmpGroup = this.serverConfigGroups.Find(g => g.HWConfigValueGroupTypeID == group.Id);

                if (tmpGroup == null)
                {
                    this.serverConfigGroups.Add(new ServerConfigGroup(group, connString));
                }
            }
        }
        #endregion
    }
}

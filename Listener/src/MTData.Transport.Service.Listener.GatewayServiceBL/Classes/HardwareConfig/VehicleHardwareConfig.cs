﻿using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class VehicleHardwareConfig
    {
        private List<HardwareConfigAttributeValue> hardwareConfigAttributeValues = new List<HardwareConfigAttributeValue>();

        [DataMember]
        public List<HardwareConfigAttributeValue> HardwareConfigAttributeValues
        {
            get { return this.hardwareConfigAttributeValues; }
            set { this.hardwareConfigAttributeValues = value; }
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    public enum MessageStatus
    {
        Pending = 0,
        InProgress = 1,
        Failed = 2,
        Aborted = 3,
        Complete = 4,
        Expired = 5
    }

    public enum ListenerDownloadType
    {
        TrackingDevice = 0,
        InterfaceBox = 1,
        MobileDataTerminal = 2,
        BinaryFile = 3
    }

    public enum ListenerMessageTypes
    {
        Unknown = -2,
        All = -1,
        ChangeIPAddress = 1,
        Flush = 2,
        FlushThenReset = 3,
        GForceAutoCalibrate = 4,
        Reset = 5,
        Output1On = 6,
        Output1Off = 7,
        Output2On = 8,
        Output2Off = 9,
        AccidentBuffer1 = 10,
        AccidentBuffer2 = 11,
        AccidentBuffer3 = 12,
        AccidentBuffer4 = 13,
        SendScriptFile = 14,
        IAPDataBlocks = 15
    }

    public class Global
    {
    }
}

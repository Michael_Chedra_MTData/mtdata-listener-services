﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using MTData.Transport.Application.Communication;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    public enum HardwareConfigType
    {
        HardwareConfig = 1,
        SwipeCardConfig = 2,
        ECMMode22Type = 3,
    }

    public enum HardwareConfigItems
    {
        ShowAll = -1
    }

    public class Manager
    {
        public static ServerBinding GetBinding()
        {
            System.Xml.XmlDocument doc = new System.Xml.XmlDocument();

            try
            {
                ServerBinding tmpBinding = new ServerBinding();
                tmpBinding.BindToIP = System.Configuration.ConfigurationManager.AppSettings["BindAddress"];
                tmpBinding.BindToPort = System.Configuration.ConfigurationManager.AppSettings["ConnectPort"];
                tmpBinding.LiveUpdatePort = System.Configuration.ConfigurationManager.AppSettings["ClientNotifyPort"];
                tmpBinding.DSN = System.Configuration.ConfigurationManager.AppSettings["ConnectString"];
                tmpBinding.RemoteStatePort = System.Configuration.ConfigurationManager.AppSettings["RemoteStateUpdatePort"];

                return tmpBinding;
            }
            catch (Exception ex)
            {
                return new ServerBinding();
            }
        }

        public static List<Classes.CustomListItem> GetMessageTypes()
        {
            List<Classes.CustomListItem> tmpList = new List<CustomListItem>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("GetMessageTypes"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new CustomListItem(dr));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static List<Classes.DownloadType> GetDownloadTypes()
        {
            List<Classes.DownloadType> tmpList = new List<DownloadType>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("GetDownloadTypes"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new DownloadType(dr));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static List<Classes.DownloadFileImage> GetFileImagesForDownloadType(int downloadType)
        {
            List<Classes.DownloadFileImage> tmpList = new List<DownloadFileImage>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("GetFileImagesForDownloadType"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@downloadTypeID", downloadType);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new DownloadFileImage(dr));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static List<Vehicle> GetFleetVehicles(int userID, bool useAttached)
        {
            List<Vehicle> tmpList = new List<Vehicle>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("GetAvailableVehicles"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@userID", userID);
                comm.Parameters.AddWithValue("@AttachedDevices", useAttached);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new Vehicle(dr));
                        }
                    }

                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new Vehicle(dr));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static List<QueuedMessage> GetCurrentQueuedMessages(int fleetID, int vehicleID, DateTime startDate, DateTime endDate, int statusID, bool listenerOnly)
        {
            List<QueuedMessage> tmpList = new List<QueuedMessage>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("GetListenerQueuedMessages"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@fleetID", fleetID);
                comm.Parameters.AddWithValue("@vehicleID", vehicleID);

                if (startDate != DateTime.MinValue)
                {
                    comm.Parameters.AddWithValue("@startDate", startDate);
                }

                if (endDate != DateTime.MinValue)
                {
                    comm.Parameters.AddWithValue("@endDate", endDate);
                }

                comm.Parameters.AddWithValue("@statusID", statusID);
                if (listenerOnly)
                {
                    comm.Parameters.AddWithValue("@SendByListener", 1);
                }
                else
                {
                    comm.Parameters.AddWithValue("@SendByListener", -1);
                }

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new QueuedMessage(dr));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static List<QueuedDownload> GetCurrentQueuedDownloads(int fleetID, int vehicleID, DateTime startDate, DateTime endDate,
            bool includeCompleted, int statusID)
        {
            List<QueuedDownload> tmpList = new List<QueuedDownload>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("GetListenerQueuedDownloads"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@fleetID", fleetID);
                comm.Parameters.AddWithValue("@vehicleID", vehicleID);

                if (startDate != DateTime.MinValue)
                {
                    comm.Parameters.AddWithValue("@startDate", startDate);
                }

                if (endDate != DateTime.MinValue)
                {
                    comm.Parameters.AddWithValue("@endDate", endDate);
                }

                comm.Parameters.AddWithValue("@includeCompleted", includeCompleted);

                if (@statusID != -1)
                {
                    comm.Parameters.AddWithValue("@statusID", statusID);
                }

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new QueuedDownload(dr, false));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static List<QueuedDownload> GetCurrentQueuedDownloads(bool includeCompleted, bool isService)
        {
            List<QueuedDownload> tmpList = new List<QueuedDownload>();
            string sql = string.Empty;
            if (isService)
            {
                sql = "GetListenerQueuedDownloads";
            }
            else
            {
                sql = "GetListenerQueuedDownloadsUI";
            }

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand(sql))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                if (!isService)
                {
                    comm.Parameters.AddWithValue("@includeCompleted", includeCompleted);
                }

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            tmpList.Add(new QueuedDownload(dr, isService));
                        }
                    }
                }
            }

            return tmpList;
        }

        public static int SendMessageToQueue(Classes.QueuedMessage message, Credentials userCreds)
        {
            int retVal = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("AddMessageToListenerQueue"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@fleetId", message.FleetId);
                comm.Parameters.AddWithValue("@vehicleId", message.VehicleId);
                comm.Parameters.AddWithValue("@messageTypeId", message.MessageTypeId);
                comm.Parameters.AddWithValue("@createdDate", message.CreatedDate);
                comm.Parameters.AddWithValue("@createdUserId", userCreds.UserID);
                comm.Parameters.AddWithValue("@startDate", message.StartDate.ToUniversalTime());
                comm.Parameters.AddWithValue("@expiryDate", message.ExpiryDate.ToUniversalTime());

                if (message.ExtraData != string.Empty)
                {
                    comm.Parameters.AddWithValue("@extraData", message.ExtraData);
                }

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();

                        retVal = Convert.ToInt32(dr["retVal"]);
                        if (retVal == 0)
                        {
                            if (Convert.ToBoolean(dr["SentByListener"]))
                            {
                            }
                            else
                            {
                            }
                        }
                    }
                }
            }

            return retVal;
        }

        public static int SendDownloadToQueue(Classes.QueuedDownload download, Credentials userCreds)
        {
            int retVal = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("AddDownloadToListenerQueue"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@fleetId", download.FleetID);
                comm.Parameters.AddWithValue("@vehicleId", download.VehicleId);
                comm.Parameters.AddWithValue("@definitionTypeId", download.DefinitionID);
                comm.Parameters.AddWithValue("@CreatedDate", download.TimeCreated);
                comm.Parameters.AddWithValue("@createdUserId", userCreds.UserID);
                comm.Parameters.AddWithValue("@startDate", download.StartDate.ToUniversalTime());
                comm.Parameters.AddWithValue("@expiryDate", download.ExpiryDate.ToUniversalTime());
                comm.Parameters.AddWithValue("@abortTimeout", download.AbortTimeout);
                comm.Parameters.AddWithValue("@continueWithoutRelogin", download.ContinueWithoutRelogin);
                comm.Parameters.AddWithValue("@SMTPEmail", download.SMTPEmailAddress == null ? "NULL" : download.SMTPEmailAddress);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();

                        retVal = Convert.ToInt32(dr["retVal"]);
                    }
                }
            }

            return retVal;
        }

        public static int AbortMessageInQueue(Classes.QueuedMessage message, Credentials userCreds)
        {
            int retVal = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("AbortMessageInListenerQueue"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", message.Id);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();

                        retVal = Convert.ToInt32(dr["retVal"]);
                    }
                }
            }

            return retVal;
        }

        public static int AbortDownloadInQueue(Classes.QueuedDownload download, Credentials userCreds)
        {
            int retVal = 0;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("AbortDownloadInListenerQueue"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", download.ID);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        dr.Read();

                        retVal = Convert.ToInt32(dr["retVal"]);
                    }
                }
            }

            return retVal;
        }

        public static void StorePacket(QueuedMessage qm, string packetData)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("StorePacketDataForMessage"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", qm.Id);
                comm.Parameters.AddWithValue("@packet", packetData);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                comm.ExecuteNonQuery();
            }
        }

        public static void UpdateMessageStatus(QueuedMessage qm, MessageStatus status)
        {
            UpdateMessageStatus(qm.Id, status);
        }

        public static void UpdateMessageStatus(int messageId, MessageStatus status)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("UpdateMessageStatus"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", messageId);
                comm.Parameters.AddWithValue("@statusID", (int)status);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                comm.ExecuteNonQuery();
            }
        }

        public static void UpdateDownloadStatus(int downloadID, int bytesSent, int totalBytes, int updateType)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("UpdateDownloadStatus"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@id", downloadID);
                comm.Parameters.AddWithValue("@totalBytes", totalBytes);
                comm.Parameters.AddWithValue("@bytesSent", bytesSent);
                comm.Parameters.AddWithValue("@updateType", updateType);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                comm.ExecuteNonQuery();
            }
        }

        public static List<ListenerLogItem> GetListenerLogs(DateTime lastTime)
        {
            return CurrentLogItems.Instance.GetListenerLogs(lastTime);
        }

        public static List<ListenerLogItem> GetListenerLogs(int fleetId, int vehicleId, DateTime lastTime)
        {
            return CurrentLogItems.Instance.GetListenerLogs(fleetId, vehicleId, lastTime);
        }

        public static SqlCommand AddListenerLogMessage(ListenerLogItem item)
        {
            int logSizeMinutes = 30;
            try
            {
                logSizeMinutes = int.Parse(System.Configuration.ConfigurationManager.AppSettings["LogSizeMinutes"]);
            }
            catch
            {
            }

            string sql = "AddListenerLogMessage";

            SqlCommand comm = new SqlCommand(sql);
            comm.CommandType = System.Data.CommandType.StoredProcedure;
            comm.Parameters.AddWithValue("@fleetID", item.FleetID);
            comm.Parameters.AddWithValue("@vehicleID", item.VehicleID);
            comm.Parameters.AddWithValue("@listenerMessage", item.ListenerMessage);
            comm.Parameters.AddWithValue("@direction", (int)item.Direction);
            comm.Parameters.AddWithValue("@messageType", item.MessageType);
            comm.Parameters.AddWithValue("@ipAddress", item.IPAddress);
            comm.Parameters.AddWithValue("@logSizeMinutes", logSizeMinutes);

            return comm;
        }

        public static int CheckUsernamePassword(string userName, string password)
        {
            int ret = -1;
            int counter = 0;
            try
            {
                string str = "SELECT [ID] FROM T_User WHERE Username='" + userName + "' AND Password='" + password + "'";

                using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
                using (SqlCommand comm = new SqlCommand(str, conn))
                {
                    conn.Open();
                    object o = comm.ExecuteScalar();

                    counter = Convert.ToInt16(o);
                }
            }
            catch 
            {
                ret = -1;
            }
            finally
            {
                if (counter > 0)
                { 
                    ret = counter;
                }
            }

            return ret;
        }

        /// <summary>
        /// Get the list of download definitions
        /// </summary>
        /// <param name="updateType">Type of update to filter by</param>
        /// <param name="showInactive">Whether to include inactive definitions</param>
        /// <param name="fleetId">Id of the fleet (set to -1 for all fleets)</param>
        /// <returns>The list of download definitions</returns>
        public static List<DownloadsDefinition> GetDownloadsDefinition(DownloadsUpdateType updateType, bool showInactive, int fleetId)
        {
            List<DownloadsDefinition> result = new List<DownloadsDefinition>();

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand("dsp_DownloadsGetDispatchDefinition"))
            {
                comm.CommandType = CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@UpdateTypeId", (int)updateType);
                comm.Parameters.AddWithValue("@ShowInactive", showInactive ? 1 : 0);
                comm.Parameters.AddWithValue("@FleetId", fleetId);

                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    // Download definitions are defined in second table result
                    if (dr.NextResult())
                    {
                        while (dr.Read())
                        {
                            result.Add(new DownloadsDefinition(dr));
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Assign the downloads to the vehicles
        /// </summary>
        /// <param name="fleetId">Id of the fleet</param>
        /// <param name="vehicleIds">Id of the vehicles</param>
        /// <param name="definitionIds">List of definition ids to assign</param>
        /// <param name="forceUpdate">Whether to force the update</param>
        /// <returns>Whether the assignment was successful</returns>
        public static bool DownloadsAssignToVehicle(int fleetId, int[] vehicleIds, int[] definitionIds, bool forceUpdate)
        {
            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            {
                foreach (int vehicleId in vehicleIds)
                {
                    foreach (int definitionId in definitionIds)
                    {
                        using (SqlCommand comm = new SqlCommand("dsp_DownloadsAssginToVehicle"))
                        {
                            comm.CommandType = CommandType.StoredProcedure;
                            comm.Connection = conn;

                            comm.Parameters.AddWithValue("@DefinitionID", definitionId);
                            comm.Parameters.AddWithValue("@FleetID", fleetId);
                            comm.Parameters.AddWithValue("@VehicleID", vehicleId);
                            comm.Parameters.AddWithValue("@ForceUpdate", forceUpdate ? 1 : 0);

                            if (conn.State != ConnectionState.Open)
                            {
                                conn.Open();
                            }

                            using (SqlDataReader dr = comm.ExecuteReader())
                            {
                                if (dr.HasRows)
                                {
                                    dr.Read();
                                }
                            }
                        }
                    }
                }
            }

            return true;
        }

        /// <summary>
        /// Get Hardware configuration
        /// </summary>
        /// <param name="configTypeId">Id of the Config Type</param>
        /// <param name="configId">Id of the config</param>
        /// <param name="serialNo">Serial No</param>
        /// <param name="includeDetails">Include Details</param>
        /// <returns>Returns the list of hardware configuration</returns>
        public static List<HardwareConfig> GetHardwareConfigs(int configTypeId, int configId = -1, long serialNo = 0, bool includeDetails = true)
        {
            List<HardwareConfig> tmpList = new List<HardwareConfig>();
            string connString = ConfigurationManager.AppSettings["ConnectString"];

            using (SqlConnection conn = new SqlConnection(connString))
            {
                using (SqlCommand comm = new SqlCommand("[GetServerConfigsByTypeIDSerialNo]"))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    comm.Connection = conn;

                    comm.Parameters.AddWithValue("@typeID", configTypeId);
                    comm.Parameters.AddWithValue("@serialNo", serialNo);

                    if (conn.State != System.Data.ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    using (SqlDataReader dr = comm.ExecuteReader())
                    {
                        if (dr.HasRows)
                        {
                            while (dr.Read())
                            {
                                if (dr.GetInt32(dr.GetOrdinal("ID")) == configId || configId == -1)
                                {
                                    tmpList.Add(new HardwareConfig(dr, connString, includeDetails));
                                    if (configId != -1)
                                    {
                                        break;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return tmpList;
        }

        /// <summary>
        /// Get the detail of a specific config id from vehicle hardware config
        /// </summary>
        /// <param name="configId">Id of the Config</param>
        /// <returns>Returns the detail of a Config Id</returns>
        public static VehicleHardwareConfig GetVehicleHardwareConfigDetails(int configId)
        {
            VehicleHardwareConfig vehicleHardwareConfig = new VehicleHardwareConfig();
            string connString = ConfigurationManager.AppSettings["ConnectString"];
            HardwareConfigAttributeValue item;

            using (SqlConnection conn = new SqlConnection(connString))
            using (SqlCommand comm = new SqlCommand("GetVehicleHardwareConfigDetails"))
            {
                comm.CommandType = System.Data.CommandType.StoredProcedure;
                comm.Connection = conn;

                comm.Parameters.AddWithValue("@configID", configId);

                if (conn.State != System.Data.ConnectionState.Open)
                {
                    conn.Open();
                }

                SqlDataReader dr = comm.ExecuteReader();
                if (dr.HasRows)
                {
                    while (dr.Read())
                    {
                        item = new HardwareConfigAttributeValue(dr.GetString(dr.GetOrdinal("Description")), dr.GetString(dr.GetOrdinal("DisplayValue")), false);
                        vehicleHardwareConfig.HardwareConfigAttributeValues.Add(item);
                    }
                }

            }

            return vehicleHardwareConfig;
        }

        /// <summary>
        /// Get the hardware types
        /// </summary>
        /// <returns>Returns the list of hardware types</returns>
        public static List<HardwareType> GetHardwareTypes()
        {
            List<HardwareType> result = new List<HardwareType>();

            string sql = "SELECT ID, Description, SerialNumRangeMin, SerialNumRangeMax FROM T_Hardware_Type";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand(sql, conn))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.Add(new HardwareType(dr));
                        }
                    }
                }
            }

            return result;
        }

        /// <summary>
        /// Assign a hardware config to a vehicle
        /// </summary>
        /// <param name="fleetId">Id of the fleet</param>
        /// <param name="vehicleId">Id of the vehicle</param>
        /// <param name="configId">config Id to assign</param>
        /// <param name="userID">user Id</param>
        /// <returns>Whether the assignment was successful</returns>
        public static bool AssignHardwareConfigToVehicle(int fleetId, int vehicleId, int configId, int userID)
        {
            bool retVal = false;

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            {
                using (SqlCommand comm = new SqlCommand("SetHardwareConfigToUnit"))
                {
                    comm.CommandType = System.Data.CommandType.StoredProcedure;
                    comm.Parameters.AddWithValue("@HWConfigID", configId);
                    comm.Parameters.AddWithValue("@fleetID", fleetId);
                    comm.Parameters.AddWithValue("@vehicleID", vehicleId);
                    comm.Parameters.AddWithValue("@userID", userID);

                    comm.Connection = conn;

                    if (conn.State != System.Data.ConnectionState.Open)
                    {
                        conn.Open();
                    }

                    SqlDataReader sdr = comm.ExecuteReader();

                    if (sdr.HasRows)
                    {
                        sdr.Read();

                        retVal = sdr.GetBoolean(sdr.GetOrdinal("RetVal"));
                    }
                }
            }

            return retVal;
        }

        public static List<HardwareType> GetAssetHardwareTypes()
        {
            List<HardwareType> result = new List<HardwareType>();

            string sql = "SELECT AssetTypeID as ID, Description, SerialNumRangeMin, SerialNumRangeMax FROM T_AssetHardwareTypes";

            using (SqlConnection conn = new SqlConnection(ConfigurationManager.AppSettings["ConnectString"]))
            using (SqlCommand comm = new SqlCommand(sql, conn))
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }

                using (SqlDataReader dr = comm.ExecuteReader())
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            result.Add(new HardwareType(dr));
                        }
                    }
                }
            }

            return result;
        }
    }
}
﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class DownloadType
    {
        #region Private Fields
        private int id;
        private string description;
        #endregion

        #region Constructor
        public DownloadType(System.Data.SqlClient.SqlDataReader sdr)
        {
            this.id = sdr.GetInt32(sdr.GetOrdinal("id"));
            this.description = sdr.GetString(sdr.GetOrdinal("description"));
        }
        #endregion

        #region Public Events/Delegates/Enums

        #endregion

        #region Public Properties
        [DataMember]
        public int ID
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public string Description
        {
            get { return this.description; }
            set { this.description = value; }
        }
        #endregion

        #region Methods

        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class ServerBinding
    {
        #region Private Fields
        private string bindToIP;
        private string bindToPort;
        private string liveUpdatePort;
        private string remoteStatePort;
        private string dsn;
        #endregion

        #region Constructor
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        [DataMember]
        public string BindToIP
        {
            get { return this.bindToIP; }
            set { this.bindToIP = value; }
        }

        [DataMember]
        public string BindToPort
        {
            get { return this.bindToPort; }
            set { this.bindToPort = value; }
        }

        [DataMember]
        public string LiveUpdatePort
        {
            get { return this.liveUpdatePort; }
            set { this.liveUpdatePort = value; }
        }

        [DataMember]
        public string RemoteStatePort
        {
            get { return this.remoteStatePort; }
            set { this.remoteStatePort = value; }
        }

        [DataMember]
        public string DSN
        {
            get { return this.dsn; }
            set { this.dsn = value; }
        }
        #endregion

        #region Methods
       
        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion
    }
}
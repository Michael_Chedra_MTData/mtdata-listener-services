﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class Vehicle
    {
        #region Private Fields
        private int fleetID;
        private string fleetName;
        private int vehicleID;
        private string displayName;
        private string hardwareVersion;
        private string softwareVersion;
        private bool hasCDMASupport;
        private bool hasGPRS;
        private bool hasMDTSupport;
        private bool hasDetatchMDTSupport;
        private bool hasPhoneKit;
        private bool hasRS232;
        private bool hasConcreteSupport;
        private bool hasRefrigeration;
        private int assetTypeID;
        private string serialNumber;
        #endregion

        #region Constructor
        public Vehicle(System.Data.SqlClient.SqlDataReader sdr)
        {
            this.fleetID = sdr.GetInt32(sdr.GetOrdinal("FleetID"));
            this.fleetName = sdr.GetString(sdr.GetOrdinal("FleetName"));
            this.vehicleID = sdr.GetInt32(sdr.GetOrdinal("VehicleID"));
            this.displayName = sdr.GetString(sdr.GetOrdinal("DisplayName"));
            this.hardwareVersion = sdr.IsDBNull(sdr.GetOrdinal("HWVersion")) ? "N/A" : sdr.GetString(sdr.GetOrdinal("HWVersion"));
            this.softwareVersion = sdr.IsDBNull(sdr.GetOrdinal("SWVersion")) ? "N/A" : sdr.GetString(sdr.GetOrdinal("SWVersion"));
            this.hasCDMASupport = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("CDMASupport")));
            this.hasGPRS = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("GPRS")));
            this.hasMDTSupport = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("MDTSupport")));
            this.hasDetatchMDTSupport = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("DetachMDTSupport")));
            this.hasPhoneKit = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("PhoneKit")));
            this.hasRS232 = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("RS232")));
            this.hasConcreteSupport = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("ConcreteSupport")));
            this.hasRefrigeration = Convert.ToBoolean(sdr.GetInt32(sdr.GetOrdinal("Refrigeration")));
            this.assetTypeID = sdr.GetInt32(sdr.GetOrdinal("assetTypeID"));

            try
            {
                this.serialNumber = sdr.GetInt64(sdr.GetOrdinal("SerialNumber")).ToString();
            }
            catch (Exception)
            {
                try
                {
                    this.serialNumber = sdr.GetString(sdr.GetOrdinal("SerialNumber"));
                }
                catch (Exception)
                {
                    this.serialNumber = "Unavailable";
                }
            }
        }
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        [DataMember]
        public int FleetID
        {
            get { return this.fleetID; }
            set { this.fleetID = value; }
        }

        [DataMember]
        public string FleetName
        {
            get { return this.fleetName; }
            set { this.fleetName = value; }
        }

        [DataMember]
        public int VehicleID
        {
            get { return this.vehicleID; }
            set { this.vehicleID = value; }
        }

        [DataMember]
        public string DisplayName
        {
            get { return this.displayName; }
            set { this.displayName = value; }
        }

        [DataMember]
        public string HWVersion
        {
            get
            {
                if (this.hardwareVersion.Length == 0)
                {
                    return "Not Set";
                }
                else
                {
                    return this.hardwareVersion;
                }
            }

            set
            {
                this.hardwareVersion = value;
            }
        }

        [DataMember]
        public string SWVersion
        {
            get
            {
                if (this.softwareVersion.Length == 0)
                {
                    return "Not Set";
                }
                else
                {
                    return this.softwareVersion;
                }
            }

            set
            {
                this.softwareVersion = value;
            }
        }

        [DataMember]
        public bool CDMASupport
        {
            get { return this.hasCDMASupport; }
            set { this.hasCDMASupport = value; }
        }

        [DataMember]
        public bool GPRS
        {
            get { return this.hasGPRS; }
            set { this.hasGPRS = value; }
        }

        [DataMember]
        public bool MDTSupport
        {
            get { return this.hasMDTSupport; }
            set { this.hasMDTSupport = value; }
        }

        [DataMember]
        public bool DetatchMDTSupport
        {
            get { return this.hasDetatchMDTSupport; }
            set { this.hasDetatchMDTSupport = value; }
        }

        [DataMember]
        public bool PhoneKit
        {
            get { return this.hasPhoneKit; }
            set { this.hasPhoneKit = value; }
        }

        [DataMember]
        public bool RS232
        {
            get { return this.hasRS232; }
            set { this.hasRS232 = value; }
        }

        [DataMember]
        public bool ConcreteSupport
        {
            get { return this.hasConcreteSupport; }
            set { this.hasConcreteSupport = value; }
        }

        [DataMember]
        public bool Refrigeration
        {
            get { return this.hasRefrigeration; }
            set { this.hasRefrigeration = value; }
        }

        [DataMember]
        public int AssetTypeID
        {
            get { return this.assetTypeID; }
            set { this.assetTypeID = value; }
        }

        [DataMember]
        public string SerialNumber
        {
            get { return this.serialNumber; }
            set { this.serialNumber = value; }
        }
        #endregion

        #region Methods

        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion
    }
}
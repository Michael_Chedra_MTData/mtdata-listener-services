﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Xml;

namespace MTData.Transport.Service.Listener.GatewayServiceBL.Classes
{
    [DataContract]
    public class QueuedMessage
    {
        #region Private Fields
        private int id;
        private int fleetId;
        private string fleetName;
        private int vehicleId;
        private string vehicleDisplayName;
        private ListenerMessageTypes messageTypeId;
        private string messageTypeDescription;
        private string packetData;
        private MessageStatus statusId;
        private string statusDescription;
        private DateTime createdDate;
        private int createdUser;
        private string createdUserName;
        private DateTime modifiedDate;
        private int modifiedUser;
        private string modifiedUserName;
        private DateTime startDate;
        private DateTime expiryDate;
        private string extraData;
        private int sequence;
        private XmlNode _extraNode;
        #endregion

        #region Constructor
        public QueuedMessage()
        {
        }

        public QueuedMessage(System.Data.SqlClient.SqlDataReader dr)
        {
            this.id = dr.GetInt32(dr.GetOrdinal("id"));
            this.fleetId = dr.GetInt32(dr.GetOrdinal("fleetId"));
            this.fleetName = dr.GetString(dr.GetOrdinal("fleetName"));
            this.vehicleId = dr.GetInt32(dr.GetOrdinal("vehicleId"));
            this.vehicleDisplayName = dr.GetString(dr.GetOrdinal("DisplayName"));
            this.messageTypeId = (ListenerMessageTypes)dr.GetInt32(dr.GetOrdinal("messageTypeId"));
            this.messageTypeDescription = dr.GetString(dr.GetOrdinal("TypeDescription"));
            this.packetData = dr.IsDBNull(dr.GetOrdinal("packetData")) ? string.Empty : dr.GetString(dr.GetOrdinal("packetData"));
            this.statusId = (MessageStatus)dr.GetInt32(dr.GetOrdinal("statusId"));
            this.statusDescription = this.LookUpStatusDescription();
            this.createdDate = dr.GetDateTime(dr.GetOrdinal("createdDate"));
            this.createdUser = dr.GetInt32(dr.GetOrdinal("createdUser"));
            this.createdUserName = dr.GetString(dr.GetOrdinal("createdUserName"));
            this.modifiedDate = dr.IsDBNull(dr.GetOrdinal("modifiedDate")) ? DateTime.MinValue : dr.GetDateTime(dr.GetOrdinal("modifiedDate"));
            this.modifiedUser = dr.IsDBNull(dr.GetOrdinal("modifiedUser")) ? 0 : dr.GetInt32(dr.GetOrdinal("modifiedUser"));
            this.sequence = dr.GetInt32(dr.GetOrdinal("sequence"));

            this.modifiedUserName = this.modifiedUser != 0 ? dr.GetString(dr.GetOrdinal("modifiedUserName")) : "";

            this.startDate = dr.GetDateTime(dr.GetOrdinal("startDate"));
            this.expiryDate = dr.IsDBNull(dr.GetOrdinal("expiryDate")) ? DateTime.MinValue : dr.GetDateTime(dr.GetOrdinal("expiryDate"));

            this.extraData = dr.IsDBNull(dr.GetOrdinal("extraData")) ? string.Empty : dr.GetString(dr.GetOrdinal("extraData"));
        }

        public QueuedMessage(System.Data.DataRow dr)
        {
            this.id = Convert.ToInt32(dr["id"]);
            this.fleetId = Convert.ToInt32(dr["fleetId"]);
            this.fleetName = dr["fleetName"].ToString();
            this.vehicleId = Convert.ToInt32(dr["vehicleId"]);
            this.vehicleDisplayName = dr["DisplayName"].ToString();
            this.messageTypeId = (ListenerMessageTypes)Convert.ToInt32(dr["messageTypeId"]);
            this.messageTypeDescription = dr["TypeDescription"].ToString();
            this.packetData = DBNull.Value == dr["packetData"] ? string.Empty : dr["packetData"].ToString();
            this.statusId = (MessageStatus)Convert.ToInt32(dr["statusId"]);
            this.statusDescription = this.LookUpStatusDescription();
            this.createdDate = Convert.ToDateTime(dr["createdDate"]);
            this.createdUser = Convert.ToInt32(dr["createdUser"]);
            this.createdUserName = dr["createdUserName"].ToString();
            this.modifiedDate = DBNull.Value == dr["modifiedDate"] ? DateTime.MinValue : Convert.ToDateTime(dr["modifiedDate"]);
            this.modifiedUser = DBNull.Value == dr["modifiedUser"] ? 0 : Convert.ToInt32(dr["modifiedUser"]);
            this.sequence = Convert.ToInt32(dr["sequence"]);

            this.modifiedUserName = this.modifiedUser != 0 ? dr["modifiedUserName"].ToString() : "";

            this.startDate = Convert.ToDateTime(dr["startDate"]);
            this.expiryDate = DBNull.Value == dr["expiryDate"] ? DateTime.MinValue : Convert.ToDateTime(dr["expiryDate"]);

            this.extraData = DBNull.Value == dr["extraData"] ? string.Empty : Convert.ToString(dr["extraData"]); 
        }
        #endregion

        #region Public Events/Delegates

        #endregion

        #region Public Properties
        [DataMember]
        public int Id
        {
            get { return this.id; }
            set { this.id = value; }
        }

        [DataMember]
        public int FleetId
        {
            get { return this.fleetId; }
            set { this.fleetId = value; }
        }

        [DataMember]
        public string FleetName
        {
            get { return this.fleetName; }
            set { this.fleetName = value; }
        }

        [DataMember]
        public int VehicleId
        {
            get { return this.vehicleId; }
            set { this.vehicleId = value; }
        }

        [DataMember]
        public string VehicleDisplayName
        {
            get { return this.vehicleDisplayName; }
            set { this.vehicleDisplayName = value; }
        }

        [DataMember]
        public ListenerMessageTypes MessageTypeId
        {
            get { return this.messageTypeId; }
            set { this.messageTypeId = value; }
        }

        [DataMember]
        public string MessageTypeDescription
        {
            get { return this.messageTypeDescription; }
            set { this.messageTypeDescription = value; }
        }

        [DataMember]
        public string PacketData
        {
            get { return this.packetData; }
            set { this.packetData = value; }
        }

        [DataMember]
        public int StatusId
        {
            get { return (int)this.statusId; }
            set { this.statusId = (MessageStatus)Enum.Parse(typeof(MessageStatus), value.ToString()); }
        }

        [DataMember]
        public string StatusDescription
        {
            get { return this.statusDescription; }
            set { this.statusDescription = value; }
        }

        [DataMember]
        public DateTime CreatedDate
        {
            get { return this.createdDate; }
            set { this.createdDate = value; }
        }

        [DataMember]
        public int CreatedUser
        {
            get { return this.createdUser; }
            set { this.createdUser = value; }
        }

        [DataMember]
        public string CreatedUserName
        {
            get { return this.createdUserName; }
            set { this.createdUserName = value; }
        }

        [DataMember]
        public DateTime ModifiedDate
        {
            get { return this.modifiedDate; }
            set { this.modifiedDate = value; }
        }

        [DataMember]
        public int ModifiedUser
        {
            get { return this.modifiedUser; }
            set { this.modifiedUser = value; }
        }

        [DataMember]
        public string ModifiedUserName
        {
            get { return this.modifiedUserName; }
            set { this.modifiedUserName = value; }
        }

        [DataMember]
        public DateTime StartDate
        {
            get { return this.startDate; }
            set { this.startDate = value; }
        }

        [DataMember]
        public DateTime ExpiryDate
        {
            get { return this.expiryDate; }
            set { this.expiryDate = value; }
        }

        [DataMember]
        public string ExtraData
        {
            get { return this.extraData; }
            set { this.extraData = value; }
        }

        [DataMember]
        public int Sequence
        {
            get { return this.sequence; }
            set { this.sequence = value; }
        }

        public NetworkData ExtraDataAsIP
        {
            get
            {
                return new NetworkData(this.extraData);
            }
        }

        public XmlNode ExtraDataAsXmlNode
        {
            get
            {
                if (_extraNode == null)
                {
                    XmlDocument doc = new XmlDocument();
                    doc.LoadXml(extraData);
                    _extraNode = doc.DocumentElement;
                }

                return _extraNode;
            }
        }
        #endregion

        #region Methods
        private string LookUpStatusDescription()
        {
            switch (this.statusId)
            {
                case MessageStatus.Pending:
                    return "Pending";
                case MessageStatus.InProgress:
                    return "In Progress";
                case MessageStatus.Failed:
                    return "Failed";
                case MessageStatus.Aborted:
                    return "Aborted";
                case MessageStatus.Complete:
                    return "Complete";
                default:
                    return "Pending";
            }
        }
        #endregion

        #region Override Methods

        #endregion

        #region Interface Methods
        #endregion
    }
}